
Le nettoyage des entrées biblios se fait via bibclean:

bibclean -max-width 0 -no-fix-names -error-log truc.log Bibliography.bib >Bibliography2.bib

Les options en questions mettent les longs titres (ou autres champs)
sur une même ligne, ne tranforme pas dans les auteurs "Nom, P." en
"P. Nom" mais ne fait pas la transformation inverse. Assurez-vous donc
que les entrées que vous ajoutez sont bien dans le format "Nom, P." ou
"Nom, Prénom and Nom, Prénom and ...". Il sauve les erreurs de parsing
du fichier dans truc.log (à regarder pour voir ce qu'il faut corriger)
et sauvegarde le nouveau fichier dans Bibliography2.bib.


make-citations crée un fichier citations.tex qui ne contient que des
\cite{...} sur toutes les entrées biblios. Utile pour voir les champs
manquants signalés par bibtex.

Donc, ce qu'il faut faire:
./make-citations
make
Observer les messages d'erreurs
Regarder le main.pdf produit pour voir si la biblio ressort bien.

