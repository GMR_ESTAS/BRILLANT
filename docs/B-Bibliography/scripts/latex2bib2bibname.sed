#!/bin/sed -f

# This script transforms LaTeX accents (macros) in their 
# equivalent html entity, for bibtex2html and bib2bib
# Limited to latin-9

# Bugs: not complete (missing oe, ae, ss,...)

s/{\\'[ \t]*A}/\&Aacute;/g
s/{\\`[ \t]*A}/\&Agrave;/g
s/{\\^[ \t]*A}/\&Acirc;/g
s/{\\~[ \t]*A}/\&Atilde;/g
s/{\\"[ \t]*A}/\&Auml;/g

s/{\\c[ \t]*C}/\&Ccedil;/g
s/{\\c[ \t]*{C}}/\&Ccedil;/g

s/{\\'[ \t]*E}/\&Eacute;/g
s/{\\`[ \t]*E}/\&Egrave;/g
s/{\\^[ \t]*E}/\&Ecirc;/g
s/{\\"[ \t]*E}/\&Euml;/g

s/{\\'[ \t]*\(\\\)\?I}/\&Iacute;/g
s/{\\`[ \t]*\(\\\)\?I}/\&Igrave;/g
s/{\\^[ \t]*\(\\\)\?I}/\&Icirc;/g
s/{\\"[ \t]*\(\\\)\?I}/\&Iuml;/g

s/{\\'[ \t]*O}/\&Oacute;/g
s/{\\`[ \t]*O}/\&Ograve;/g
s/{\\^[ \t]*O}/\&Ocirc;/g
s/{\\~[ \t]*O}/\&Otilde;/g
s/{\\"[ \t]*O}/\&Ouml;/g

s/{\\'[ \t]*U}/\&Uacute;/g
s/{\\`[ \t]*U}/\&Ugrave;/g
s/{\\^[ \t]*U}/\&Ucirc;/g
s/{\\~[ \t]*U}/\&Utilde;/g
s/{\\"[ \t]*U}/\&Uuml;/g


s/{\\~[ \t]*N}/\&Ntilde;/g

s/{\\'[ \t]*a}/\&aacute;/g
s/{\\`[ \t]*a}/\&agrave;/g
s/{\\^[ \t]*a}/\&acirc;/g
s/{\\~[ \t]*a}/\&atilde;/g
s/{\\"[ \t]*a}/\&auml;/g

s/{\\c[ \t]*c}/\&ccedil;/g
s/{\\c[ \t]*{c}}/\&ccedil;/g

s/{\\'[ \t]*e}/\&eacute;/g
s/{\\`[ \t]*e}/\&egrave;/g
s/{\\^[ \t]*e}/\&ecirc;/g
s/{\\"[ \t]*e}/\&euml;/g

s/{\\'[ \t]*\(\\\)\?i}/\&iacute;/g
s/{\\`[ \t]*\(\\\)\?i}/\&igrave;/g
s/{\\^[ \t]*\(\\\)\?i}/\&icirc;/g
s/{\\"[ \t]*\(\\\)\?i}/\&iuml;/g

s/{\\'[ \t]*o}/\&oacute;/g
s/{\\`[ \t]*o}/\&ograve;/g
s/{\\^[ \t]*o}/\&ocirc;/g
s/{\\~[ \t]*o}/\&otilde;/g
s/{\\"[ \t]*o}/\&ouml;/g

s/{\\'[ \t]*u}/\&uacute;/g
s/{\\`[ \t]*u}/\&ugrave;/g
s/{\\^[ \t]*u}/\&ucirc;/g
s/{\\~[ \t]*u}/\&utilde;/g
s/{\\"[ \t]*u}/\&uuml;/g

s/{\\~[ \t]*n}/\&ntilde;/g

s/{\([a-zA-Z]\+\)}/\1/g

