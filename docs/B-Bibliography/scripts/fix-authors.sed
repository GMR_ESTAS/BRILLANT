#!/bin/sed -f

# This script tries to put the authors and editors in a Bibtex entry
# in the "Name, Firstnames" unambiguous format. It will put an XXX
# onto entries that it can not resolve (single-worded entries also)

# Bugs: the result will not be what you want if the editor (or the
# author) is an entity rather than a person: "Springer Verlag" will
# become "Verlag, Springer", for instance. Thus maybe you are better
# off with not handling editors (remove "\|editor" in the address
# field below, then)

/[ \t]*\(author\|editor\)[ \t]*=[ \t]*".*",$/ {

# First, transform the "and" into ";;"

  s/[ \t]\+and[ \t]\+/;;/g

# Put also ";" at beginning and end

  s/\([ \t]*\(author\|editor\)[ \t]*=[ \t]*"\)/\1;/
  s/",$/;",/

# At this point, entries look like ...=";author;;author;;author;",
# Now we do the name reversals, one format at a time
# {John,J.,J.-P.,J-P} {Doe, Doe-Smith} (no point in the name)

  s/;\(\([[:alpha:].-]\|{\\.[ \t]\?\\\?.}\)\+\)[ \t]\+\(\([[:alpha:]-]\|{\\.[ \t]\?\\\?.}\)\+\);/;\3, \1;/g

# {John, J., J.-P.} {P,P.,P. M.} {Doe, Doe-Smith} (british sure like
# their middle initials)

  s/;\(\([[:alpha:].-]\|{\\.[ \t]\?\\\?.}\)\+\)[ \t]\+\(\([A-Z]\.\?[ \t]*\)\+\)[ \t]\+\(\([[:alpha:]-]\|{\\.[ \t]\?\\\?.}\)\+\);/;\5, \1 \3;/g

# Now we put a tag for looking for entries that have not be
# transformed (they have no ",") for easier search/replace. The tag is
# XXX

  s/;\([^,;]\+\);/;XXX\1;/g

# Transfrom back the ";;" into "and"

  s/;;/ and /g
  s/";/"/
  s/;"/"/
}
