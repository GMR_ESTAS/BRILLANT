#!/bin/sed -f

# This script transforms LaTeX accents (macros) in their 
# equivalent html entity, for bibtex2html and bib2bib
# Limited to latin-9

# Bugs: not complete (missing oe, ae, ss,...)

s/{\\'[ \t]*A}/Á/g
s/{\\`[ \t]*A}/À/g
s/{\\^[ \t]*A}/Â/g
s/{\\~[ \t]*A}/Ã/g
s/{\\"[ \t]*A}/Ä/g

s/{\\c[ \t]*C}/Ç/g
s/{\\c[ \t]*{C}}/Ç/g

s/{\\'[ \t]*E}/É/g
s/{\\`[ \t]*E}/È/g
s/{\\^[ \t]*E}/Ê/g
s/{\\"[ \t]*E}/Ë/g

s/{\\'[ \t]*\(\\\)\?I}/Í/g
s/{\\`[ \t]*\(\\\)\?I}/Ì/g
s/{\\^[ \t]*\(\\\)\?I}/Î/g
s/{\\"[ \t]*\(\\\)\?I}/Ï/g

s/{\\'[ \t]*O}/Ó/g
s/{\\`[ \t]*O}/Ò/g
s/{\\^[ \t]*O}/Ô/g
s/{\\~[ \t]*O}/Õ/g
s/{\\"[ \t]*O}/Ö/g

s/{\\'[ \t]*U}/Ú/g
s/{\\`[ \t]*U}/Ù/g
s/{\\^[ \t]*U}/Û/g
s/{\\"[ \t]*U}/Ü/g

s/{\\~[ \t]*N}/Ñ/g

s/{\\'[ \t]*a}/á/g
s/{\\`[ \t]*a}/à/g
s/{\\^[ \t]*a}/â/g
s/{\\~[ \t]*a}/ã/g
s/{\\"[ \t]*a}/ä/g

s/{\\'[ \t]*e}/é/g
s/{\\`[ \t]*e}/è/g
s/{\\^[ \t]*e}/ê/g
s/{\\"[ \t]*e}/ë/g

s/{\\'[ \t]*\(\\\)\?i}/í/g
s/{\\`[ \t]*\(\\\)\?i}/ì/g
s/{\\^[ \t]*\(\\\)\?i}/î/g
s/{\\"[ \t]*\(\\\)\?i}/ï/g

s/{\\'[ \t]*o}/ó/g
s/{\\`[ \t]*o}/ò/g
s/{\\^[ \t]*o}/ô/g
s/{\\~[ \t]*o}/õ/g
s/{\\"[ \t]*o}/ö/g

s/{\\'[ \t]*u}/ú/g
s/{\\`[ \t]*u}/ù/g
s/{\\^[ \t]*u}/û/g
s/{\\"[ \t]*u}/ü/g

s/{\\c[ \t]*c}/ç/g
s/{\\c[ \t]*{c}}/ç/g

s/{\\~[ \t]*n}/ñ/g

