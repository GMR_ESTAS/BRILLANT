\slidetitle {Exemple de machine}
  \footnotesize{
\begin{verbatim}
MACHINE        SquareRoot_0
DEFINITIONS    square (x)      ==      x*x
OPERATIONS
sqrt <-- SquareRoot (xx) =
PRE        xx : NAT
THEN
      ANY
              yy
      WHERE
             (yy : NAT) & (square(yy) <= xx) & (yy <= square(xx+1))
      THEN
             sqrt := yy
      END
END
\end{verbatim}
    }
  \vfill
  \end{frame}


\section{Raffinement}

Le {\em raffinement} $SP_{1}$ doit permettre d'introduire la d�finition
op�rationnelle d'une sp�cification $SP_0$.

Cette d�finition doit \^etre coh�rente.

Le raffinement {\tt $SP_{n+1}$} d'un composant {\tt $SP_n$}
doit permettre de pr�ciser

\begin{itemize}
\item La repr�sentation des donn�es,
\item Les choix des algorithmiques,
\item La d�composition des op�rations,
\item Les contraintes globales.
\end{itemize}

On peut distinguer diff�rentes formes de raffinement :
\begin{itemize}
\item Raffinement qui pr�cise le composant pr�c�dant.
\item Raffinement qui introduit un nouvel objet, on �conomise
  l'introduction d'une sp�cification.
\end{itemize}


\begin{frame}
  \slidetitle{Raffinement}

  Le raffinement a �t� d�fini par C. Morgan

  {
    \footnotesize{
      \begin{center}
        C. Morgan

        Deriving programs from specifications

        Prentice Hall International, 1990
      \end{center}
      }
    }


  \textbf{Id�e :} Le raffinement consiste � transformer un mod�le \textit{abstrait} en
  un autre fonctionnellement �quivalent mais plus concret.

  \vfill

