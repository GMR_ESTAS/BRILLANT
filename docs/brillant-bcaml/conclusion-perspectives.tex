% -*- coding: iso-8859-1 -*-


\subsection{Comparison with other works}
\label{sec:comparison}


Several other formal methods also employ the same kinds of tools, and
were developed similarly to use either open-source high-level
languages or formats, or both. Many of them can be found in the www
formal methods' virtual library \cite{FMlibrary}, along with links to
the formal methods they implement.  In addition, freely available (but
not open-source) tools exist for the Bmethod: B4Free \cite{B4Free}
(distributed by \emph{Clearsy}) and ProB \cite{ProB}.  The former is a
parser, type-checker, proof obligation generator and prover programmed
in the B0 language.  The latter is an animator and model-checker
programmed in Prolog, and uses the \xml files produced by the jBTools
\cite{jBTools} as input.


\BRILLANT can also be compared to projects of a similar nature, that
have similar ambitions and/or designs: Rodin \cite{RodinBSharp} (for
$B\#$), Overture \cite{Overture} (for VDM++) and the Community Z Tools
~\cite{CZT} (\CZT). These projects share several common points. For
example, they all use XML-based interchange formats, and they all have
similar architectures, in which the core tools (e.g., parsers,
typecheckers, testers/validators, plug-ins and/or plug-outs) are clearly
separated. Some are driven by research needs (\BRILLANT, \CZT), some
by industrial interests (Rodin), and some by both (Overture).

However, despite their similar architectures, the tools' underlying
implementations are quite different.  Rodin and Overture are based on
the Eclipse IDE \cite{Eclipse} and thus provide a very consistent
development environment. Writing the mandatory parts of such a
framework (e.g., parsing, compiling, graphic interfaces, test suites)
is therefore more scalable and reusable, if not any easier. \CZT and
\BRILLANT, on the other hand, are more or less a loosely connected set
of tools: the first was developed using Java (their edition of Z
specifications is even supported by jEdit), while most of the
\BRILLANT tools were developed in OCaml.

Because \CZT and \BRILLANT appear to have more points in common, it is
appropriate to focus this comparison on them. \BRILLANT was developed
before the others, although the ideas behind \CZT appeared around
the same time\footnote{A sketch of a formal developement platform can 
be found in reference \cite{Mariano-Phd}, ch-2, p-29, published in 1997.
 The CZT initiative was launched around September 2001}. 

The developers of \CZT have provided information about
the design decisions behind their tools.  In the following paragraphs,
these decisions are analyzed by comparing them with similar
decisions that have been made at one time or another during the
development of \BRILLANT.  (All citations come from reference \cite{CZT}.)


\bcaml, \BRILLANT 's most important tool, uses what in \CZT
terminology \cite{CZT} is called an immutable type, which helps
\BRILLANT to avoid the problems described by the developers of \CZT.
Initially, these developers implemented a "mutable" Abstract Syntax Tree (AST)
 approach, then moved to a combined approach called "a 'Both' approach", 
but finally ended up shifting gradually towards an "immutable" approach.
 This means that, initially, the AST (i.e., the type representing the Z
specifications) could be modified during tool execution, which allowed
more efficient algorithms to be used, but made the sharing of common
subtrees (i.e., common pieces of specifications) difficult. In
addition, given the initial ``mutability'' of the AST, the programmers had to
ensure that it did not change during certain specific parts of the
execution, which required a comprehensive knowledge of the overall
tools. The 'Both' approach tried to combine the advantages of the
mutable approach and the immutable approach, but only succeeded in
increasing complexity. \CZT has now moved towards the immutable
approach, both because it is less error-prone and because it makes
things simpler for the developer.  Thus, by using OCaml, \BRILLANT
avoided the pitfalls of optimization for the immutable type of the
AST.  This made sense to us for 2 reasons: 1) the literature usually
advocates using a static tree to represent abstract data
structures, and 2) the OCaml language actually encourages such an
implementation.



When developing \BRILLANT, we wanted to make it possible to add B-HLL
to \bcaml at some later date, which required insuring identifier
unicity.  The \CZT developers also faced this problem, which arises
when an abstract structure defines binders, either as universal or
existential quantifiers in predicate calculus, or as local vs. global
variables in programming languages. In \CZT, three common solutions to
this problem were tried: renaming bound variables when necessary,
which is a traditional solution, but one that makes it ``incredibly
easy to make subtle errors''; using De Bruijn indices, which is
``easier to get right'', but leads to unreadability and complexity; or
using unique names for all bound variables, which is safer, but
requires unique names for all models of a Z project, and ends up
making the output less readable. They finally chose the last solution
for \CZT, with the additional benefit that the unique attributes of
the identifiers could be exported in the \xml AST in order to use the
ID/IDREF (unique identifiers/symbolic link to unique identifiers)
standard attributes.  For \BRILLANT, we chose a similar solution in
\bcaml: we decided to institute a scoping phase after the parsing
phase in order to associate each identifier with a unique identity.
Later on, the processing of new variables was facilitated not only by
that unicity, but also by the recursive nature of the syntax trees,
which allows all free variables to be retrieved. Indeed, several
libraries that provide functions for high-level data structures (e.g.,
lists, sets, hash tables) exist in OCaml: and we were able to benefit from
using  these libraries immediately.
 
Another frequent problem is slow processing times. However, both teams
managed to avoid this problem: like the
tools in \CZT, the \bcaml tools in \BRILLANT, 
 interact via the abstract syntax tree directly instead
of \xml exchange files, which speeds up the processing.


All the above problems have been described in publications dealing
with \Overture \cite{Overture}, although the problem seems to have
been more connected to their choice of compiler generator than to the
compilation technique itself. Since the Rodin project is still in the
development stage, we would encourage its developers to look at the
projects discussed above, among others, in order to benefit from their
experience.

 From our perspective, \BRILLANT has a very relevant
  process for developing for formal methods. 
Implementing the B method with our tools helps to highlight the
weaknesses of the mathematical definition of the method, such as the
lack of documentation for handling variable instanciations in the
weakest precondition calculus, or
shortcomings of its more technical definition, such as the problem of
parsing B models with standard Lex and Yacc tools.
Moreover, like UNIX, \BRILLANT adheres to a philosophy of separate
 but interconnected tools. 
Not surprisingly then, the consequence is that
  extensions and extra tools are easily added to the
  platform.
From that perspective, we think that \BRILLANT is a more than adequate
tool.  Since most development projects are done by PhD students and
young interns who are not yet full-time engineers, the simplicity of
programming language chosen for \BRILLANT (OCaml) is a definite plus.
Its powerful, well-documented, high-level libraries, and its ability
to blend various programming design paradigms, already well-known in
the academic scientific community, make it a most appropriate choice.
In addition, the by-the-book formalism implemented in \BRILLANT makes
integrating extensions, such as the B-HLL module system, much easier
than the other possibilities.


\subsection{Conclusion}
\label{sec:conclusion}

The \BRILLANT platform design has two principal advantages: it uses
open and standardized formats, and the source codes for its tools
(OCaml and/or Java so far) are openly available. In addition, it can
be used to test and/or validate \B -related experiments, and in fact,
we were the first users of many of the prototypes now available for
the platform (e.g., bparser, bgop, btyper, bphox). We have been
working to finetune the platform to help it meet the needs of other
theoretical research projects, including but not limited to extending
the \B language, improving the current tools, providing couplings with
other provers (e.g., Coq, Harvey), and offering other validation
formalisms (e.g., model-checking).

From the information presented in this article, it would appear that
we have reached our goal of providing an open and standardised format
(\xml) for a platform that has become a testbed for several other
fundamental research projects (e.g., UML/OCL/Bcoupling in section
\ref{sec:from-umlocl-models-to-b}, proofs in
section~\ref{sec:from-b-proof-to-correctness}, code generation in
section~\ref{sec:from-b-spec-to-code}). All the source code examples
in the article (i.e., \B machines, logical formulas, \xml, \phoX) either
come from \BRILLANT, or derive from its use: The \B machines are
\LaTeX{} files in the \BRILLANT style, and the \LaTeX{} files were
obtained by applying an \xsl stylesheet to \xml abstract machines,
themselves obtained by parsing ASCII \B machines.

% find . -regex '.*\.\(ml\|mli\|cd\|xsl\|mly\|mll\|phx\)' -print0 | xargs -0 cat | wc -l
% Dans les bons sous-répertoires de bcaml
The following table provides some figures about the size of the project:

\begin{tabular}{|l|l|p{0.4\linewidth}|}
  \hline
  Component & Lines of code & Comments\\
  \hline
  \hline
  kernel & 8900 & \\
  \hline
  parser & 3200 & \\
  \hline
  type-checker & 12500 & (it has some duplicate code with the kernel) \\
  \hline
  PO generator & 4000 & (GSL libraries included) \\
  \hline
  flattening tool & 1700 & \\
  \hline
  bhll & 2400 & \\
  \hline
  bphox & 4200 & (\phox libraries and stylesheets) \\
  \hline
  uml plug-in & 20300 & \\
  \hline
\end{tabular}



\subsection{Perspectives}
\label{sec:perspectives}


In the future, we will work to integrate technologies that endorse the
use of open formats into the \BRILLANT platform.  The following
modifications are planned: the use of \xml schemas \cite{xsd} instead
of DTDs for validating \xml files; the use of some recent promising results
to replace XSLT processors with OCamlDuce\cite{OCamlDuce}; the use of
\xml flexibility to increase traceability between UML models, \B
machines, proof obligations and other derived models (e.g., generated
code, test cases); the representation of \B models as projects
databases using XPath and XML-Query \cite{xquery}; and a distributed
platform architecture using XML-RPC \cite{xmlrpc}, which will allow
the parser and prover to be represented as servers to which \B
projects can be sent for parsing, validation or other tasks (in a kind
of "B-forge").


We also plan to finalize the integration of ABTOOLS \cite{ABTOOL05,ACSD03-Boulanger} 
within \bcaml.
Now part of \BRILLANT, both of these tools were initially developped
independently.  ABTOOLS, which provides an open environment based on ANTLR
and JAVA, makes it easier to design and test extensions
of the \B language. Lastly, we hope to define an ergonomic
interaction mode for the various platform tools, by proposing a
graphic interface suitable for the underlying platform technologies.
This interaction will, consequently, rely heavily on XML technologies.


Several other projects, these more related to the fundamental research
currently under way, also offer interesting perspectives for the
future, such as UML/OCL/B coupling \cite{Marcano02a}, temporal
extensions for \B \cite{B-DC}, and safe software component generation
\cite{petit2003sdps}. Much work remains to be done, and the platform
developers will be happy to provide their assistance to those who would
like to try to use the tools in the context of their own research. All
the necessary resources for building \BRILLANT should be available on the web
site dedicated to collaborative free software development
\cite{BRILLANT}.



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "body"
%%% End: 
