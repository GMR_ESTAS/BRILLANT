% -*- coding: iso-8859-1 -*-

During the last decade of the previous millennium, theoretical
research produced the \B method, which is based on the same
fundamentals as Z \cite{Zstandard} and VDM \cite{ISOVDM96}.  This
method reconciles the pragmatic constraints of industrial development
of critical software with the strict theoretical requirements inherent
to mathematical formalism.  The \B method is one of the rare
successful formal methods used in industry, and it supports multiple
paradigms: \emph{Substitutions} as %VP a 
means for describing dynamic
behavior naturally; \emph{Formulas} in a simple yet efficient logical
framework (set theory); \emph{Composition mechanisms} that simplify
development; and \emph{Refinements} that provide a safe and efficient
way to obtain secure computer code from abstract specifications.



The \B method was used in the METEOR project
\cite{FM99-MATRA-nocross}, as well as in less well-known development
projects \cite{CDDM92,AFM-DeMe} and even non-critical development
projects \cite{Tatibouet2003}.  The software industry adopted \B
largely because of the availability of software tools supporting all
phases of the \B development process (semantics verification,
refinement, proving, automatic code generation).  Unlike most software
tools, \B support tools resulted from prototypes developed by industry
rather than by the academic community. In fact, from 1993 to 1999, the
``Atelier B'' development project was funded by the ``Convention B'',
which was a collaborative effort of the RATP (Parisian Autonomous
Transportation Company), SNCF (the French National Railway Society),
INRETS (the National Institute for Transport and Safety Research) and
Matra Transport (now Siemens Transportation Systems), among others.




A computer scientist in the field of formal methods will perceive
certain paradoxes in the implementation of the \B method. For
instance, the \B method uses programming languages, such as \B kernel,
that are not well documented or specified and that are not
particularly well-suited to \B's high level of abstraction.  In
addition, some have observed that the \B module language is rather
complex and hard to understand. G. N. Watson \cite {watson97} goes as
far as to call the proliferation of constructs in the \B module
system, and the rules associated with them, somewhat
\textit{daunting}.  Certainly, the semantics of the \B module system
has been studied by several research teams.  Bert \textit{et al.}
\cite{B96-Bert} have developed a component algebra to express the
INCLUDES and USES clauses.  Potet \textit{et al.} \cite{B98-Potet}
have studied the IMPORTS and SEES links, demonstrating that, in \B, it
is possible to build an erroneous project that appears to be correct;
they have also proposed criteria derived from the dependency graph,
which must be verified to eliminate erroneous architectures.
Dimitrakos \textit{et al.} \cite{ZB00-Dimitrakos} have studied the
various composition clauses, focusing on the conditions that must be
verified to preserve the various component properties.  Of course, it
can be argued that \B modularity is complex because there are various
kinds of restrictions to ensure correctness. In any case, the inherent
complexity of the \B method makes it important to provide a synthetic way
to understand these modular constructs %VP important
\ajout{(i.e., a way with just a few rules rather than \emph{in extenso} as done by Abrial 
  \cite{BBook})}.



Though these apparent paradoxes can be explained by the industrial origins of
the method, the fact remains that the industrial tools
currently available for \B are often inappropriate for scientific
research, and thus do not provide effective support for efforts to
extend the use of the \B method.


\begin{implementation}
To remedy this problem, we have proposed the \BRILLANT\cite{BRILLANT}
framework, showing the feasibility of a safe software development
system that ranges from semi-formal specification (UML) to
contract-equipped code generation\ajout{(i.e., equip\-ped with assertions from the
abstract model)}.  This fra\-me\-work provides a
central core into which various components can be plugged, including a
central component, %\GM{?which one?}
 a UML plug-in, a proof plug-out, and a code
generator plug-out.  
% \retrait{car trop implantation}{Because we agree with Bert \textit{et al.}
% \cite{B96-Bert} that the modularity of the \B language can be
% separated from the base/core language, we have adopted the powerful
% Harper-Lillibridge-Leroy modular module (\textbf{HLL}) system
% \cite{harper94typetheoretic,Leroy-modular-modules-jfp}, which builds a
% module language from a given core language without modularity
% constructions. HLL has the advantage of separating the semantics of
% the core language from that of the module language.  Moreover, the
% precise semantic provided by instanciating HLL in the \B language
% guarantees the preservation of the specified visibility rules at no
% extra cost to the system.
%   }
\end{implementation}


% \begin{Retrait}{d�placement de la partie dans BHLL}
% The ability to view \B modularity through an HLL semantic is a
% theoretical result, established by Petit in his 2003 dissertation
% \cite{PetitPhd}; it does not change the work of the \B specifications
% writer, who should continue to specify \B machines in the usual
% manner.  However, it may shed light on the problem and thus facilitate
% the modular design of \B specifications.  In this way, the central
% core addresses the problem of obscurity in current \B language modular
% constructs.
% \end{Retrait}



% \retrait{cr�ation d'une section case study} {
% \begin{Buser}
% This paper is a revised and enhanced version of the article presented
% in 2005 \cite{BRILLANT05a}.  In this revised version, we use a
% specification of the level crossing problem inspired by Einer
% \textit{et al.}.  \cite{pneiner} as a case study, although some parts
% of the specifications (the traffic lights, for example) are specific
% to French systems.  Our example concerns a single railroad line
% system. This system is composed of:
% \begin{itemize}
% \item sensors placed on the railroad line to detect the beginning or
%   ending of a level-crossing protection procedure,
% \item barriers and road traffic lights to stop cars from passing,
% \item an onboard train system to activate certain train functions
%   (e.g., braking), and
% \item a sequencer to specify the various states of the system and the
%   transitions.
% \end{itemize}
% This problem is illustrated in figure \ref{fig:railroad}.  The top of
% the figure presents a schematic view of the overall problem, and the
% bottom shows a schematic representing the automata for the various
% system states.  The state \textit{External System} characterises the
% initial and final state of the automata.
% \end{Buser}  


% \begin{figure}
%   \centering \fbox{
%     \begin{minipage}[c]{.8\columnwidth}
%       \begin{tabular}{c}
%         \fbox{\includegraphics[width=0.9\columnwidth]{figures/shema_pn}}
%       \end{tabular}
%       \begin{tabular}{c}
%         \fbox{\includegraphics[width=0.9\columnwidth]{figures/pn_automata}}
%       \end{tabular}
%       \normalsize{}%
%     \end{minipage}
%   }
%   \caption{The level crossing problem}
%   \label{fig:railroad}
% \end{figure}

% }

This paper is a revised and enhanced version of the article presented
in \cite{BRILLANT05a}. %\marginpar{� revoir}
Section~\ref{sec:overview} gives an overview of the BRILLANT effort and the
BCaml part of the platform. Section~\ref{sec:casestudy} briefly introduces the case study used to illustrate %VP ouar approach,
our approach,
and section~\ref{sec:from-b-spec-to-xml} presents the central component of
\BRILLANT, which allows the components of a \B project to be
manipulated. The parsing of \B machines is examined in
section~\ref{sec:grammar-b}, highlighting the central role of XML as
an exchange format. 
% Abstract syntax tree manipulations, such as
% specification flattening, are examined in
% section~\ref{sec:kernel-astmanipulation}, and proof obligation
% generation, in section~\ref{sec:bcaml-pog}.
Section~\ref{sec:from-b-proof-to-correctness} presents the proof
plug-out used to validate \B proof obligations, and
section~\ref{sec:from-b-spec-to-code} describes a code generator
plug-out that supports several target languages and is able to embed
contracts into the code.  Section~\ref{sec:from-umlocl-models-to-b}
describes the UML plug-in used to translate UML projects into \B so as
to validate them, as described by Marcano \& Levy \cite{Marcano02a}
and Laleau \& Polack \cite{ZB02-Laleau}.  Finally,
section~\ref{sec:conclusions} presents our conclusions based on the
results of our experiments with the implementation and use of the
\BRILLANT platform.



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "body"
%%% End: 

