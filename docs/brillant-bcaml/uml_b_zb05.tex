% -*- coding: iso-8859-1 -*-

\begin{figure}[t]
  \centerline{\fbox{
      \includegraphics[width=.85\columnwidth]{figures/uml2b_process.eps}
    }}
  \caption{The proposed process}
  \label{process}
\end{figure}

The various plugins/plugouts mentioned in the introduction revolve around
the BCaml core described above, starting with a translator that changes
UML/OCL specifications to B specifications. Our work continues the work begun
by Marcano \& Levy \cite{Marcano02a} on combining UML and B for consistency
checking, while also taking OCL annotations into account \cite{OCL03b}. Adding
OCL constraints is a useful way to capture the key safety properties of the
system being constructed. The main purpose of our work is to facilitate the
construction of a \B formal specification, using automated tool support. Our
process, which is shown in figure~\ref{process}, breaks down into the three
steps described below:

\paragraph{From UML to XML} The Poseidon \cite{Poseidon} modelling tool was
chosen for drawing the UML model and generating its associated XMI file
(model.xmi). A transformation file (xmi2uml) is written in the XSLT language
to translate the XMI file into a XML file (model.xml) that represents the
original UML file (hence, the name xmi2uml rather than xmi2xml).
\paragraph{From XML to UML-parsed models} The IOXML processor parses UML
models elements of the XML file into OCaml-compliant data types accordingly to
the UML abstract syntax tree definition (uml.ml). Therefore the resulting file
(model.ml) can be used to generate the B specification.
\paragraph{From UML models to B specifications}  The uml2b module translates
UML classes, state diagrams and OCL constraints into \B specifications. The
translation rules are implemented in OCaml as mappings of the UML abstract
syntax (uml.ml) into the B abstract syntax (b.ml).

We chose to connect the tool directly to the abstract syntax tree of BCaml
rather than producing \B concrete specifications in order to obtain a smoother
integration for both tools. Though the same programming language is employed
(OCaml), it is still possible to produce \B concrete specifications by using
the XML output plus a stylesheet to generate \B ASCII files.


\subsection{UML-based modelling}
\label{sec:uml-uml-based-modelling}

In this section, the construction of a B specification from a UML/OCL model is
illustrated using the example of a radio-based Railway Level Crossing (RLC)
\cite{FFB96}. (A complete description of the traffic control system considered
here can be found in Jansen \& Schnieder \cite{Jansen00}). 


\subsubsection{Class and state diagrams}
\label{sec:class-state-diagrams}

The Level Crossing Control system (LCC) controls the traffic lights and
barriers of a level crossing. It interacts with the vehicle sensors, the
train-borne control system and the operations centre. When the system is
activated at the approach of a train, it must perform a series of actions, as
illustrated by the state diagram in figure~\ref{statediag0}. Several actions
have specific timing constraints: for instance, switching from yellow lights
to red lights shall happen after 3 seconds. In figure~\ref{statediag0}, time
expirations following the LCC's activation are denoted by the events prefixed
{\tt timeOut}. A full class diagram of this system can be found in
\cite{FORMS2004}.

\begin{figure}[t]
  \centerline{\fbox{%
      \includegraphics[width=.80\linewidth]{figures/statediag0.eps}}}
  \caption{State diagram of the LCC system}
  \label{statediag0}
\end{figure}


\subsubsection{adding OCL constraints}
\label{sec:adding-ocl-constr}

Without going into detail, the OCL constraints helps specifying safety
properties and ensuring these properties are not lost between the abstract
specification and the implementation phases. These constraints are naturally
found later in the invariant of the generated \B machines. Additional OCL
constraints also help adding supplementary information that can not be found
in the state diagrams. For instance, the closeBarrier operation, raised by the
event \code{timeOut\_2}, is specified as follows:

\begin{OCL}
  context LCC\_System::closeBarrier\\
  pre: \tab self.yellowLight.state=On\\
  \hspace{5ex}and self.theBarrier.state=Opened\\
  post: \tab self.yellowLight.state=Off \\
  \hspace{5ex}and self.redLight.state=On \\
  \hspace{5ex}and self.theBarrier.state=Closed
\end{OCL}


\subsection{Generating the B specification}
\label{sec:uml-generate-b-spec}

The B specification resulting from the steps described above is composed of
abstract machines representing each class. A root abstract machine specifies
the whole system's structure and introduces all the associations between
classes. 

\subsubsection{Formalisation of class and state diagrams}
\label{sec:form-class-state}

An abstract machine formalising a class describes the deferred set of all the
possible instances of the class (i.e.,  {\small BARRIER}), as well as the
subset of its existing instances (i.e., barrier). Each attribute is formalised
by a variable defined as a total function between the set of instances and the
attribute type. Associations between classes are expressed in \B as binary
relations between the existing class instances (figure~\ref{amn-transition0}).
These relations can be expressed precisely by using the wide spectrum of
relation definitions in \B, and by stating additional properties on their
domains or ranges.

\begin{figure}[t]
  \begin{center}
    \input{figures/fig-amn-transition0}
  \end{center}
  \caption{Formalisation of state diagrams}
  \label{amn-transition0}
\end{figure}

Each transition is formalised by a \B operation whose name is the name of the
incoming event concatenated with the name of the action. The precondition of
the operation is deduced from the transition guard and the substitution
describes the transition to the new state.


\subsubsection{Formalisation of OCL constraints}
\label{sec:form-ocl-constr}

As depicted in section~\ref{sec:adding-ocl-constr}, two kinds of OCL
constraints make their way into the resulting \B project~:

\begin{mitemize}
  \mitem The OCL constraints that specify class invariants are combined with
  the invariant of the related \B machines

  \mitem The OCL constraints that complete the information of state diagrams
  are translated into \B preconditions, for the precondition part (see
  section~\ref{sec:adding-ocl-constr}) and \B substitutions, such as the
  predicate statement, for the postcondition part. For instance, the call to
  red and yellow light operations from the \code{timeOut\_1\_redLightOn}
  operation has been obtained by translation of the following OCL
  constraint's postcondition:

  \begin{OCL}
    self.yellowLight.state=Off and self.redLight.state=On and\\
    self.theBarrier.state=Closed
  \end{OCL}
\end{mitemize}

The closing of the barrier does not appear in this operation, but in another
\code{timeOut} operation in order to conform with the translation of the state
diagram. In the next section, we introduce \Bphox, a proof plugout for the
\BRILLANT platform.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "body"
%%% End: 

