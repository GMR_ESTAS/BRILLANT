% -*- coding: iso-8859-1 -*-


\subsection{From requirements to UML models}
\label{sec:from-reqs-to-uml}


The "elicitation problem" (i.e., the creation of a valid initial
description of the required system properties) is essential to ensure
the correctness of the future system. The consistency of the system
depends on the developer's ability to understand and incorporate key
safety properties.  Therefore, knowledge of the context in which the
system operates plays an important role in eliciting system
requirements. Our approach to requirement analysis is based on the
approach taken by Marcano \textit{et al.} \cite{FORMS04-Marcano}, in
which both the static and dynamic properties of the system are taken
into account through various UML diagrams.

Describing the entities involved and their invariants facilitates
comprehension of the static properties.  Describing the way that
system actors will interact with each other and the system leads to a
full comprehension of the dynamic properties of the future system. The
Object Constraint Language (OCL) is used to express all the properties
that cannot be expressed through diagrammatic notation alone (i.e.,
hypotheses and facts related to subsystems, classes, attributes and
associations).  OCL is also used to describe the system safety
conditions.  Thus, system modeling can be divided into two steps:
\begin{itemize}
\item First, UML sequence diagrams must be defined in order to
  describe both correct operating scenarios and failure scenarios
\item Second, the expected behavior of the system must be described
  completely as a set of OCL pre- and post-operational conditions
\end{itemize}
UML state diagrams will be defined for the combined operations of the
entire system in order to describe the overall interaction of the
system with each actor in its environment.


% \retrait{D�placement dans case study}{
% \subsection{A Railway Level-Crossing system}


% The traffic control system considered in this paper has been described
% in detail by Jansen and Schnieder \cite{Jansen00}. They include
% knowledge about the railway domain as a basis for the formal
% specifications. The problem is to specify a radio-based Railway
% Level-Crossing (RLC) application developed for the German Railways
% \cite{FFB96}.  This application is distributed over three sub-systems:
% a train-borne control system (on-board system), a level-crossing
% control system, and an operations center system.  The level crossing
% is situated on a single-track railway line at a point where the line
% crosses a road on the same level. The intersection of the road and the
% railway line is considered the danger zone, since train traffic and
% road traffic must not enter it at the same time.  Note that this is
% the main safety constraint that must be taken into account when
% describing the system.

% }

\subsection{UML-based-modelling}
\label{sec:uml-based-modelling}


In the railway example described in section \ref{sec:casestudy}, 
the central system is the level-crossing control (LCC) system, 
the two others-a train-borne control (TC) system and an operations 
center (OC) system-being cooperating actors that make use of the LCC. 
To create a UML model, first, it is necessary to identify the main entities
 that must be
modelled to determine possible LCC system failure conditions. A
primary cause of such failure conditions could be malfunctioning
sensors or actuators. Defects leading to failures may be detected in
the main physical structures or in the control systems themselves. In
this case study, only a limited number of failures are considered:
failures of the yellow or red traffic lights (which are considered
separately), the barriers, and the vehicle sensor, and the delay or loss
of messages sent through the radio network.  For any of these failures, the
following objects that interact with the LCC system
(Fig.\ref{classdiag1}) are examined: the lights, the barriers, the
vehicle sensors, the train-borne control system, and the operations
center.


\begin{figure}[htbp]
  \centerline{\fbox{\includegraphics[width=.97\columnwidth]{\Figure{classdiag1}}}}
  \caption{RLC system : Class diagram}
  \label{classdiag1}
\end{figure}

\retrait{D�placement dans case study}{

The traffic lights and barriers at the level crossing are controlled
by the LCC system.  This system must be activated when a train
approaches the level crossing.  In the activated mode, the LCC
performs a sequence of timed actions in order to safely close the
crossing and to ensure that the danger zone is free of road traffic.
First, the yellow traffic lights are switched on, and after 3 seconds,
they are switched to red. After another 9 seconds, the barriers begin
to lower. If the barriers have been completely lowered within a
maximum time of 6 seconds, the LCC system signals the safe state of
the level crossing, thus allowing the train to pass through the
intersection.


The level crossing is opened to road traffic again once the train has
passed completely through the crossing area, and the LLC system
switches back into the deactivated mode.


Trains approaching the level crossing are detected via a process of
continuous self-localisation on the part of the train and radio-based
communication between the train and the LCC system.  The vehicle
sensor situated on the far side of the crossing triggers the
re-opening of the barriers and tells the the traffic lights to switch
off.  During the activated mode, the LCC system can be in one of the
following substates (Fig.\ref{statediag0}): yellow light showing;
barrier closing; barrier closed; or barrier opening.  Note that the
time expirations occurring after the LCC is activated are denoted by
the events \code{timeOut\_1} (3 seconds later) and \code{timeOut\_2}
(9 seconds later).

\textit{}
\begin{figure}[htbp]
  \centerline{%
\fbox{\includegraphics[width=.9\columnwidth]{\Figure{statediag0}}}}
  \caption{State diagram of the LCC system}
  \label{statediag0}
\end{figure}



}


\subsection{Adding OCL constraints}
\label{sec:adding-ocl-constraints}


%SC: paragraphe difficile � comprendre, quand m�me, sans exemple
Using a standard formal language for constraint specification is an
important step towards formalizing complex models, particularly in the
context of critical safety systems. The purpose of OCL is to allow the
constraints related to system objects to be formally specified,
preserving the comprehensibility and readability of the UML models.
OCL facilitates the statement of the properties and the invariants of
the objects, as well as that of the pre/post-conditions for the
operations. OCL also provides a navigation mechanism that allows
attributes, operations and associations to be referenced in the
context of a class or an object (a class variable), and query
operators that permit a set of elements to be selected and/or
modified. Each OCL expression has a specific type and belongs to a
specific context. The context of an OCL expression determines its
scope. Only the visible elements in the context of the expression can
be referenced by means of navigation expressions.

Safety properties are included in the system invariants in order to
propagate them from the abstract specification phase to the
implementation phase. The main property of the LCC system is to
prevent both road and rail traffic from entering the danger zone at
the same time; to do so, the control specifications for the crossing
area and its barrier, as well as any trains that may pass through the
level crossing at any time, must be modelled at a high level of
abstraction.  For these reasons, the following OCL invariants are
specified for these classes, as shown in Fig.\ref{classdiag0}:
 
\begin{enumerate}
\item \requirement{If there is a train crossing the danger zone then the
 barrier is closed}
  \begin{ocl}
context CrossingArea inv: \\
    not(self.train->isEmpty()) implies self.barrier.state=Closed     
  \end{ocl}

\item \requirement{If the barrier of the crossing area is open, then no
 train is approaching the danger zone}
  \begin{ocl}
    context Barrier inv:\\
    self.state=Opened implies self.guards.train->isEmpty()
  \end{ocl}

\item \requirement{If the barrier of the crossing area is closed, then a train is 
crossing the intersection}
  \begin{ocl}
    context PhysicalTrain inv:\\
    self.crosses.barrier=Closed     
  \end{ocl}
\end{enumerate}



\textit{}
\begin{figure}[htbp]
  \centerline{%
\fbox{\includegraphics[width=.9\columnwidth]{\Figure{classdiag0.eps}}}}
  \caption{Constraints related to the danger zone}
  \label{classdiag0}
\end{figure}


These constraints must hold true for a more detailed design once
decisions have been made about the actual type of hardware to be used
in an implementation. In this case study, the notion of "train passing
through the intersection" is connected to the activation of the
railway level crossing.  In order to accomplish this task, the front
and the rear of the train must somehow be detected.  We assume that
the train can be detected directly through use of abstract vehicle
sensors.  The barrier state is detected by introducing a barrier
sensor.  In light of these assumptions, the previous OCL invariants
can be refined by adding the following LCC system constraints for the
class shown in Fig.\ref{classdiag1}:
 

\begin{enumerate}
\item \requirement{The red light is switched on whenever the barrier
    is closed, and the yellow light is switched on when the barrier is
    closing. If both the yellow and the red lights are switched off,
    then the barrier is open.}

  \begin{ocl}
    context LCC\_System inv:\\
    self.theBarrier.state=Closed implies self.redLight.state=On\\
    and self.theBarrier.state=Closing implies self.yellowLight.state=On\\
    and self.yellowLight.state=Off and self.redLight.state=Off\\
    implies self.theBarrier.state=Opened
  \end{ocl}

\item \requirement{If a train is in the danger zone, the level
    crossing is in an activated state composed of four substates
    (WaitingAck, Closing, Closed, Opening).}

  \begin{ocl}
    context LCC\_System inv:\\
    not(self.train->isEmpty()) implies self.state=Activated and\\
    
    Set(Activated)=Set(WaitingAck->Union(Closing)->Union(Closed)->Union(Opening))
  \end{ocl}


\item \requirement{ If the LCC system is in the activated state while
    the barrier is open, then the level crossing is in an unsafe mode.}

 \begin{ocl}
    context LCC\_System inv:\\
    self.state=Activated and self.bSensor.state=Opened implies
    self.mode=Unsafe    
  \end{ocl}


\item \requirement{If the registered state of the barrier is closed
    and the trigger sensor indicates that it is open, then the level
    crossing is in an unsafe mode. This is the case when the barrier
    is in the closing state; the LCC remains unsafe until the barrier
    is completely closed. }

  \begin{ocl}
    context LCC\_System inv:\\
    self.bSensor.state=Opened and self.theBarrier.state=Closed)
    implies self.mode=Unsafe 
  \end{ocl}

\end{enumerate}


The operations of the LCC class are specified with OCL pre- and
post-conditions.  OCL is also used in sequence diagrams to complete
the preconditions and invariants related to operations.  Although
state diagrams are used to derive the initial specification of each
operation (i.e., the description of a state transition), OCL
constraints are needed to add supplementary information that can not
be retrieved from the state diagrams.

Consider the closing of the barrier raised by the event
\code{timeOut\_1}.  The precondition of the operation
\code{closeBarrier} ensures that the yellow light is switched on
before sending the closeBarrier order, in addition to ensuring that
the barrier has not yet been closed.  The postcondition ensures that
the state of the yellow light is off, the state of the red light is
on, and the state of the barrier is closed. The operation is specified
as follows:


\begin{ocl}
  context LCC\_System::closeBarrier\\
  pre: self.yellowLight.state=On and self.theBarrier.state=Opened \\
  post: self.yellowLight.state=Off and self.redLight.state=On and \\
  self.theBarrier.state=Closed 
\end{ocl}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "body"
%%% End: 
