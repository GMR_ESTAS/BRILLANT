#!/usr/bin/make
#
# $Id: Makefile.source,v 1.31 2006/02/23 10:27:35 mariano Exp $
# Georges MARIANO <georges.mariano@inrets.fr>

# $Log: Makefile.source,v $
# Revision 1.31  2006/02/23 10:27:35  mariano
# (GM) Ajout de la cible 'deblatex'
# Par 'make deblatex', un fichier (.deb) est produit contenant les noms et versions des paquets debian utilis�s lors de la compilation latex.
# Cette cible n�cessite l'installation du paquet 'devscripts'.
#
# Revision 1.30  2005/08/17 15:41:51  mariano
# (MG) latex-purge / purge
# ajout de TEXMODE
#
# Revision 1.29  2004/11/15 10:14:01  colin
# Rajout des fichiers de compil minitoc en nettoyage
#
# Revision 1.28  2004/09/13 13:52:14  colin
# Plus propre, plus unifi�, plus partag�
#
# Revision 1.27  2004/09/03 14:07:49  mariano
# (MG) fiche: ajout du fichier de donn�es pour enregistrement
#
# Revision 1.26  2004/09/01 09:59:34  mariano
# (MG) Cf ChangeLog
#

DATE = $(shell date)
DATEISO = $(shell date --iso-8601)
HOSTNAME= $(shell hostname)

LOCK = chmod -R u-rw
UNLOCK = chmod -R u+r

CLASS = article

FIGDIR = figures
#FIGDIR =
OCAMLWEBSTY = /usr/share/texmf/tex/latex/misc/ocamlweb.sty

TEXDIR = .
HTMLDIR = $(CURDIR)/html
HTMLFILES = $(FILE)*.html

ESTASTEX=$(HOME)/texmf/tex/latex/estas

RM = rm -f
#
WEBFILES = *.html 
TEXFILES = $(wildcard $(TEXDIR)/*.tex)

HVALOCALDIR = $(HOME)/texmf/hva

LATEXOPTS =  -interaction=nonstopmode
#LATEXOPTS =  -interaction=batchmode

HEVEA = @cd $(TEXDIR) ; hevea
HACHA = @cd $(TEXDIR) ; hacha 
LATEX = @cd $(TEXDIR) ; latex $(LATEXOPTS)
BIBTEX = @cd $(TEXDIR) ; bibtex
PDF = ps2pdf
PDFLATEX = @cd $(TEXDIR) ; pdflatex

IDX_OPTS=

MAKEINDEX = cd $(TEXDIR) ; makeindex $(IDX_OPTS)


XARGS = xargs --no-run-if-empty

DVIPS_OPTS = -t a4
#(MG BUG) DVIPS_OPTS+= -P pdf


DVIPS = cd $(TEXDIR) ; $(DEBUG) dvips $(DVIPS_OPTS) 

# param�trage pour le debug par variable d'env.

KDEBUG = KPATHSEA_DEBUG=-1 
DEBUG = $(KDEBUG)
DEBUG = 

EDITOR=emacs

HEVEA_OPTS =
HEVEA_OPTS+=  -v -exec xxdate.exe 
HEVEA_OPTS+=  -fix
HEVEA_OPTS+=  $(HVALOCALDIR)/trouble.hva  
HEVEA_OPTS+=  $(CLASS).hva 
HEVEA_OPTS+=  $(FILE).hva 
HEVEA_OPTS+= -I $(HVALOCALDIR)


#Not all documents are french ones
#HEVEA_OPTS+=  -francais
#Not all documents are made by GM ;-)
#HEVEA_OPTS+=  $(HVALOCALDIR)/head.hva
#HEVEA_OPTS+=  fancysection.hva 



HACHA_OPTS = -tocbis # -o $(FILE).html

IMAGEN_FLIP = pnmflip -cw
IMAGEN_OPTS = 

EXTRA = -extra "$(IMAGEN_OPTS)" 

IMAGEN = imagen $(EXTRA)

NICE = nice -n 15

PS2PDF = $(NICE) ps2pdf


LATEXSRC = *.tex *.sty *.bib *.fig
LATEXCLEAN = *.idx *.bbl *.aux $(FILE)*.log *.blg *.toc *.out
LATEXCLEAN+= *.mtc* *.ind *.bmt *.ilg *.ptc* *.lof *.lot .latex-done
LATEXCLEAN+= *.mlf* *.mlt* 

LATEXPURGE = *.ps *.dvi *.pdf *.log 

UTILS = $(THISMAKE) 

COMPRESS = @gzip --force

TAR = tar --exclude-from=archive-exclude -cvf  

TAG = $(HOSTNAME)-$(DATEISO)

# int�gration d'un (�ventuel) fichier de configuration perso
-include ~/texmf/make.perso

#
TEXMODE=latex

latex-all:  			# La totale LaTeX
	-$(MAKE) $(TEXMODE)
	-$(MAKE) bibtex
	-$(MAKE) index
	$(MAKE) $(TEXMODE)
	$(MAKE) $(TEXMODE)

deblatex:
	@dpkg-depcheck -b  -o $(FILE)-latex.lst make latex
	@tail -n +2 $(FILE)-latex.lst | xargs dpkg -l | tail -n +6 | sort > $(FILE)-latex-$(TAG).lst

latex-lock:
	$(LOCK) $(LATEXSRC) Makefile*

pdflatex:						# Compilation PDFLateX
	-$(PDFLATEX) $(FILE)

latex:							# Compilation LaTeX
	$(LATEX) $(FILE)
	@echo "$(DATE)" >>  .latex-done			# Compilation reussie!
	-@find $(TEXDIR) -name "*.bbl" -size 0 | $(XARGS) rm

prosper: 				# Compilation Prosper
	-$(MAKE) latex-all
	-$(MAKE) dvips
	-$(MAKE) pdf
	-cp $(FILE).pdf $(FILE)-prosper.pdf

bibtex:							# Compilation bibTeX
	$(BIBTEX) -min-crossrefs=2 -terse $(FILE)


index:							# Compilation makeindex
	if test -f $(TEXDIR)/$(FILE).idx ;\
	then $(MAKEINDEX) $(INDEXOPTS) $(FILE).idx;\
	fi

glossaire: latex
	-$(GLOSSTEX)  $(FILE) $(FILE).gdf -v5
	-makeindex $(FILE).gxs -o $(FILE).glx -s glosstex.ist
	-$(MAKE) latex

dvips: $(FILE).ps

pdf: dvips
	cd $(TEXDIR) ; $(PDF) $(FILE).ps  $(FILE).pdf
 
latex-clean: clean					# Nettoyage LaTeX
	-cd $(TEXDIR) ; $(RM) $(LATEXCLEAN)

latex-purge: latex-clean
	-cd $(TEXDIR) ; $(RM) $(LATEXPURGE)

latex-archive: archive-latex

purge: latex-purge

# Section HeVeA

hevea-all: hevea hacha 0images 				# La totale HeVeA

hevea:
	touch $(FILE).hva
	$(HEVEA) $(HEVEA_OPTS) $(FILE)

texte: hevea-txt

hevea-txt:
	$(HEVEA) -text $(HEVEA_OPTS) -w 80 $(FILE)
hacha:
	$(HACHA) $(HACHA_OPTS) $(FILE).html

hevea-clean:
	-$(RM) *.hidx *.htoc *.haux *.hind $(FILE).image.* 

imagen-clean:
	-$(RM) $(FILE)*.png $(FILE)*.gif

hevea-archive: archive-hevea

0images:
	if test -f $(FILE).image.tex;\
	then $(IMAGEN)  $(FILE);\
	fi

# Section archivage

archives-lock:
	$(LOCK) *.tar

archives:  archive-hevea archive-latex archive-files
	-rm -rf $(CURDIR)/saved-$(DATEISO)
	-mkdir -p $(CURDIR)/saved-$(DATEISO)
	-@mv  $(FILE)-$(DATEISO)*.tar.gz saved-$(DATEISO)
	-ls -l  saved-$(DATEISO)/*.gz

archive-hevea: hevea-clean
	-$(TAR) $(FILE)-$(DATEISO)-web.tar $(HTMLDIR)/* $(FIGDIR)
	$(COMPRESS) $(FILE)-$(DATEISO)-web.tar

archive-latex: source latex-clean
	-mv $(FILE)-$(DATEISO)-src.tar $(FILE)-$(DATEISO)-src.tar.old
	-$(TAR) $(FILE)-$(DATEISO)-src.tar \
	Makefile* \
	*.tex *.bib *.cls *.sty *.eps $(UTILS) $(FIGDIR)
	$(COMPRESS) $(FILE)-$(DATEISO)-src.tar

archive-files:
	-$(TAR) $(FILE)-$(DATEISO).tar $(FILE).ps $(FILE).dvi $(FILE).pdf *.eps
	$(COMPRESS) $(FILE)-$(DATEISO).tar


0move:
	-@mkdir -p $(HTMLDIR)
	cp $(WEBFILES) $(HTMLDIR) 
	@cd $(TEXDIR) ;\
	mv $(HTMLFILES) $(FILE)*.gif index.html *motif.gif $(HTMLDIR)/ ;\
	mv $(FILE)*.png *motif.png $(HTMLDIR)/ ;\
	cp *.css $(HTMLDIR)/ 
	@#YP#

web: hevea-all 0move

0noimages: hevea hacha 


# Fiche enregistrement

fiche:
	cp $(ESTASTEX)/publication.tex $(FILE)-fiche.tex
	cp $(ESTASTEX)/fiche-data.tex fiche-data.tex

# Recompilation globale
# G�n�ration de la fiche d'enregistrement
publication: latex-all prosper
	make -f Makefile  FILE=$(FILE)-fiche latex-all 
	make -f Makefile  FILE=$(FILE)-fiche prosper
#	make -f Makefile  FILE=$(FILE)-fiche hevea
#	latex2rtf $(FILE)-fiche.tex 
	xdvi $(FILE)-fiche &


very-clean:
	for f in $(TEXFILES); do\
	echo $$f ;\
	$(MAKE) FILE=$$f all-clean;\
	done 

all-clean: latex-clean hevea-clean imagen-clean

clean:
	-$(RM) *~

0lock:	latex-lock archives-lock	# Verrouillage des sources

dateiso: 
	@echo $(DATEISO)


session:
	@if test -f $(FILE).tex; then $(EDITOR) $(FILE).tex ; fi;
	@if test -d CVS; then tkcvs ; fi;

rtf:
	latex2rtf $(FILE).tex 

-include $(HOME)/texmf/Makefile.cvs

# sauvegarde de ~/texmf/Makefile.source 
source:
	-cp $(HOME)/texmf/Makefile.source  ./Makefile.source.home

.SUFFIXES: .phx .tex .dvi .ps

.dvi.ps:
	$(DVIPS) -o $@ $<

.tex.dvi: 
	$(LATEX) $<

.phx.tex: 
	$(PHOX) $(PHOX_OPTIONS) $(FILE).$(PHOX_EXTENSION)

.PHONY: fullclean
