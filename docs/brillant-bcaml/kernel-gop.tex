% -*- coding: iso-8859-1 -*-

\subsection{Generating proof obligations}
\label{sec:bcaml-pog}

In this section, we first describe the method used to implement the
existing calculus for the weakest precondition.  Then, we show how
this calculus can be used to generate the proof obligations of a B
project.  Last, we present the various options available for exporting
these proof obligations to other formats and other tools.


\subsubsection{Generalized Substitution Language (GSL)}
\label{sec:GSL}
\paragraph{ }

In order to generate proof obligations for \B machines, we must be
able to calculate the weakest preconditions of the substitutions. In a
nutshell, the weakest precondition calculus allows the minimal state
for a given program, or \emph{substitution}, to be calculated in order
to verify a given predicate, or \emph{postcondition}. Hence, when we
use ``calculated'' or ``uncalculated'' here, we refer to the state of
a weakest precondition calculation. A proof obligation (PO) is
calculated if all the weakest precondition calculations and all the
variables instanciations have taken place. Thus, a calculated PO looks
like a regular predicate, while an uncalculated PO still contains the
substitutions expressed in a B component.

We chose to follow the approach defined in the B-Book \cite{BBook}:
reducing \B substitutions to their smallest syntactic and semantic set
(i.e., generalized substitutions).  In the following paragraphs, we
use \emph{GSL} to denote both the syntactic set and the substitutions
that compose it. Following the B-Book \cite[B.3]{BBook}, we define the
\emph{GSL} in \bcaml as an abstract data type, with the following
notable exceptions:
\begin{itemize}
\item The assignment is defined as a multiple substitution; it serves
  as a basic construct once the parallel substitutions have been
  reduced, and thus can be viewed as an optimisation.

\item The repetition substitution $\mbox{" }\widehat{}\mbox{ "}$ is
  used to reason about the semantics of the \emph{while substitution}
  and to propose a sufficient predicate \cite[E.7]{BBook} that could
  be used instead of the necessary and sufficient predicate.
\item The instanciation ($[variable:=expression]substitution$) of a
  substitution variable (the parameters of an operation, for instance)
  is reduced before transforming the substitution. (This idea is not
  documented precisely by Abrial \cite{BBook}, and thus it corresponds
  to an extrapolation on our part.)
\end{itemize}


The first and the third exception are rather straightforward, but the
second requires a more detailed explanation.  The real weakest
precondition can be only obtained from a fixpoint application over
this \emph{repetition substitution}. This fixpoint can not be
calculated in general by programming it. Thus, although the repetition
GSL appears in the \Bbook, it never appears in the actual calculations
of the proof obligations, so we chose not to include it. Nonetheless,
should the need arise, it could be introduced, because its semantics
have been defined precisely.





With the help of the abstract data type, proof obligations can be
generated according to the rules described in the B-Book
\cite[appendix E]{BBook}. The corresponding \bcaml code was written
with readability in mind, making it easy to match the code with the
rule from which it is derived.


% \GM{\fbox{un exemple de code de gop serait envisageable ?}}  DP :
% Pas utile je pense pour ne pas tomber trop dans l'implantation
 
\subsubsection{Proof Obligation Generation}
\label{sec:POG}
\paragraph{ }

The main steps for generating proof obligations from a project can be
divided into precise steps.  These steps are described in more detail
below:


\paragraph{Parsing}

First, the machine and all the machines it depends on are parsed.  To
improve efficiency, we decided to directly use the \bcaml kernel
libraries directly for parsing rather than reading the XML files
produced by the parser.


\begin{implementation}
  \retrait{pour clarifier}{This parsing phase is followed by a scoping
    phase in which all unique identifiers that represent the same
    variable name, machine name or operation name are made equal.  In
    fact, prior to the parsing phase, all identifiers are associated
    with a single stamp; however, when the parsing is finished, all
    the identifiers have different stamps.  The scoping phase acts to
    make the stamps for those identifiers that represent the same
    variable, machine or operation name equal in terms of visibility.
  }
\end{implementation}


% \begin{wrapfigure}{l}{0pt}
\begin{minipage}{0.5\linewidth}
  \lstinputlisting[language=Bmethod,basicstyle={\scriptsize
    \sffamily},frame=single,framesep=1ex]{figures/po-timeOut_1_showRlight.amn}
\end{minipage}
\caption{Uncalculated proof obligation for the
  \textbf{timeOut\_1\_showRlight} operation}
\label{fig:po-timeOut_1_showRlight}
% \end{wrapfigure}

\paragraph{Generation of formulas}

The formula generation step is based on the B-Book \cite[appendix
F]{BBook}, resulting in proof obligations with the following form:
$[Instanciation]Hypothesis$ $\Rightarrow$ $[substitution]Goal$.

This generation method allows more handling flexibility later on, for
instance when debugging the proof obligation generator, or when
showing students how proof obligations are generated, or when the
proof tool applies the substitution to the goal.
Figure~\ref{fig:po-timeOut_1_showRlight} shows an example of an
uncalculated proof obligation, derived from the \B project presented
in section~\ref{sec:from-umlocl-models-to-b}.



\paragraph{Optimizations}

Several additional optimisations, or processing procedures, can be
applied to the generated formulas.  For example, formulas can be
calculated, resulting in predicates that contain no substitutions.  It
is also possible to split the goal, by splitting the formula into as
many formulas as there are members of the conjunction in the goal:

$$ 
(H \Rightarrow G_1 \wedge \dots \wedge G_n) \leadsto (H \Rightarrow
G_1), \dots, (H \Rightarrow G_n)
$$

Other possible optimizations that were however not implemented include
removing formulas when the goal is trivially true or appears in the
hypotheses, or changing the form of the formula to adapt it to a
particular theorem prover. Certainly, it is sometimes easier to apply
such transformations to the abstract syntax tree than to XML files
using stylesheets. We did not implement these optimisations because we
do not believe that semantic interpretation is the province of the PO
generator, but rather of the prover, despite the fact that this
semantic interpretation can be easily related to the abstract syntax
(e.g., like a goal appearing in the hypotheses).


% DP
% \begin{verbatim}
% 4. An interesting question is whether additional optimizations for the
% proof generator can be inserted as plug-ins
% \end{verbatim}

\paragraph{Final files and trace information}

Once the formulas have been generated, some trace information is
embedded into the resulting file.  Trace information can be found in
the absolute name of the file, which reflects the kind of proof
obligation that is in the file, and the machine from which it is
generated.  This trace information can be used later to find
problematic parts of a B project if the corresponding proof cannot be
achieved.  The XML information in the file contains not only the
predicate itself, but also a root tag named (for obvious reasons)
\texttt{ProofObligation}.  In addition, the file contains a tag that
includes all the free variables of the formula because some theorem
provers require that all variables be bound.  This tag helps the
stylesheet to generate a file for such theorem provers more easily.



\subsubsection{Exporting to other tools}
\label{sec:exporting-to-other-tools}
\paragraph{ }


Once the proof obligations in the XML format are available, the XSL
stylesheets allow them to be exported to other tools.  For instance,
the proof obligations can be converted into \LaTeX files
(figure~\ref{fig:po-timeOut_1_showRlight} is an example of the results
obtained); into text files, which are easily read by humans; into HTML
files, which improve the readability of the formulas; or into a format
suitable for a prover, in order to verify the proof obligations.

Figure \ref{fig:po-phox-timeout1} in section \ref{sec:bphox-xsl}
presents the result of an XSL stylesheet application to the proof
obligation shown in figure~\ref{fig:po-timeOut_1_showRlight}.
%
% \begin{Retrait}{trop implentation}
%   \begin{implementation}
%     First, the header of the file is inserted, which provokes the
%     loading of the appropriate PhoX library and finetunes the power
%     of the prover (the \texttt{flag} commands).  Then, the free
%     variables in the formula (the identifiers after the \verb+/\+
%     quantifier) are quantified.  The formula itself is inserted into
%     the BPhoX syntax by replacing the conjunctions of the hypotheses
%     with implications, which facilitates the work of PhoX. Finally,
%     the command that is given to the prover is added to start the
%     proof (\code{Try intros ;; auto.}).
%   \end{implementation}
% \end{Retrait}
%
In the next step, the theorem prover is fed
% \GM{with?} DP Je ne pense pas, on va voir avec Lisa
the generated proof obligations file (see
section~\ref{sec:from-b-proof-to-correctness}).  All of these steps
(including replacing the conjunctions in the hypotheses with
implications) are done via the XSL stylesheet, demonstrating the
\emph{ad hoc} suitability of this technology designed for simple
treatments involving recursion.

% \GM{iteration on tree-like structures ?}.

More complex transformations might be doable with stylesheets, but it
would run the risk of becoming uselessly wordy and, more importantly,
less maintainable. For this reason, we advise using stylesheets only
for translations that preserve the overall structure.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "body"
%%% End: 
