% -*- coding: iso-8859-1 -*-
%DP

\subsection{BCaml input and output}
\label{sec:kernel-bcaml-input-output}

\begin{toolbuilder}
The kernel of the \bcaml platform is made up of the first bricks that
were developed when the platform was created.  These bricks specify
the concrete grammar (section~\ref{sec:grammar-b}) that defines the \B
language and the abstract syntax
(section~\ref{sec:abstract-syntax-xml}) that defines the type used to
manipulate the specifications.  This may seem obvious, but it is
nonetheless important because it makes collaboration with other
developers possible.  One of these collaborative projects led to the
development of another brick in the \bcaml kernel, called the
\emph{Btyper}.  This brick is not described in this paper, but more
details can be found in Bodeveix \& Filali \cite{ZB02-Bodeveix}.
  
\end{toolbuilder}

\subsubsection{A concrete grammar for \B}
\label{sec:grammar-b}
\paragraph{ }


Several \B grammars have been introduced over the years, coming from a
variety of sources: \emph{Clearsy} (prev. \emph{Steria}), which
corresponds to the grammar used in \emph{Atelier B}; \emph{B-Core},
which corresponds to the grammar used in \emph{B-Toolkit}; and
Mariano's PhD dissertation \cite{Mariano-Phd}, based on the
\emph{B-Core grammar}, which was introduced to do metrics on \B
specifications.


In order to build a tool that would be as useful as possible, we
needed to define a grammar that would take into account the criticisms
of the grammars presented above. Our \BCaml choices had to respect the
following constraints:
 \begin{itemize}
 \item They had to be as compatible as possible with the machines that
   can be correctly parsed by the commercial tools mentioned above
   (i.e., \emph{Atelier B}, \emph{B-Toolkit}),
 \item they had to comply with the standard \emph{Lex} and \emph{Yacc}
   tools that allow \emph{LALR} grammars to be defined, and
 \item they had to cause as few conflicts as possible.
\end{itemize}

We chose OCaml \cite{ocaml309} as a support tool for our \B
development for several reasons.  Not only does it allow symbolic
notations to be handled easily, but in addition, it also implements efficiently
 and comes bundled with tools that allow the
parsing of \emph{LALR} grammars.  Using these tools, we defined the \B
language in \emph{LALR}.  \retrait{pour etre coherent avec le retrait
  qui suis}{ Some of the problems we encountered in doing so are
  described below.  }


\retrait{trop implantation}{
\paragraph{Peculiarities in the parsing of \B machines}
\label{sec:parsing-scoping}

\begin{toolbuilder}
Certain \B language features made defining an adequate grammar for the
\B method difficult, including:
 \begin{itemize}
 \item The \textbf{records}, whose \verb+;+ separator causes a
   conflict with the separator currently used to define the sequence
   of two substitutions.  We decided to give priority to the correct
   parsing of substitution sequences, to the detriment of a correct
   parsing of records.
 \item The \textbf{definitions}, designed to lighten repetitive,
   cumbersome notations, which cause a partial conflict with the
   \emph{LALR} grammars. We decided not to expand the definitions and
   to keep them in the abstract syntax trees used in the original
   text, thus restricting the definitions to a subset that is
   expressive enough to allow them to be implemented with the
   \emph{LALR} parser without losing too much of their usefulness.
 \item Several so-called \emph{reduce/reduce} and \emph{shift/reduce}
   \textbf{conflicts}, which represent ambiguous notations. We decided
   to remove the \emph{reduce/reduce} conflicts, but to defer our
   decision about the \emph{shift/reduce} conflicts.
\end{itemize}
  
\end{toolbuilder}
}



\subsubsection{Abstract syntax and XML syntax---two isomorphic formats}
\label{sec:abstract-syntax-xml}
\paragraph{ }

We used our definition of abstract syntax to directly infer an XML
representation for \B formal specifications.  (Due to lack of space,
this abstract syntax is not described here.)  This XML encoding is
called "B/XML" and is stored in an XML DTD file.  Such abstract syntax
is, as could be expected, more tolerant than concrete syntax, and
contains elements that facilitate the handling of the syntax
structure.  For instance, the $[substitution]predicate$ and
$[variable\_instanciation]substitution$ constructions appear in this
abstract syntax. The first construction corresponds to the form of a
formula prior to a calculation of its weakest precondition. The second
construction appears, for instance, when the parameters and
operations have to be instanciated. The existence of these
constructions in the abstract syntax mean that the structure can be
manipulated to bring it closer to the matching mathematical
definitions given in the B-Book \cite{BBook}.

We chose XML as our pivot format because of its flexibility and its
ease-of-use with third-party tools.  Using it makes our tools as
independent of one another as possible, allowing a researcher to use
our parser, but someone else's proof tool, for example. This
flexibility is insured by the following aspects:
\begin{itemize}
\item XSL style sheets can be used to formulate simple recursive
  treatments of the XML structure, mostly transformations into other
  structured formats (e.g., \LaTeX, HTML, or PhoX, as mentioned in
  section~\ref{sec:exporting-to-other-tools}); and
\item other programming languages can
  be easily used for more complex manipulations because most of the time 
these other languages are able to read XML data. 
This means that researchers can use their preferred programming
  language, as long as it has libraries for reading an XML format.
\end{itemize}



\subsubsection{How \BCaml exploits the Abstract Syntax Tree (AST)}
\label{sec:how-bcaml-exploits-ast}
\paragraph{ }

\BCaml takes advantage of both of the features described above.
The XML approach extends the platform's plug-in/plug-out ability greatly, 
while the use of a well-defined, efficient meta-language as the core 
implementation language leads to a formal standard definition of the B grammar 
and allows the provision of more efficient components, easily understandable 
by others. 
The main drawback of this choice of formats is the difficulty of making both
 formats evolve together. When designing a component interface to be plugged in,
 one natural guideline is to use the core implementation language if high 
efficiency is required, in order to avoid a translation step 
(which is the choice made for the \emph{BTyper}, the \emph{POG} and the AST
 manipulation tools, for example), and to rely on the XML exchange format
 in all other cases (which is the alternative chosen for \BphoX). 




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "body"
%%% End: 
