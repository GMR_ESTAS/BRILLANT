% -*- coding: iso-8859-1 -*-


\bcaml provides the first two important types of \B tools, presented
in Abrial's $B\#$ \cite[section 4]{Abrial03}.  The first includes the
lexer, parser and typer; the second, the proof obligation generator.
The third and last important \B tool is the automatic, interactive
prover.  We chose not to develop such a tool with \bcaml for a
pragmatic reason: building a \B prover according to our specifications
takes much more time than developing dedicated libraries for an
already existing prover.  Instead, we built a replaceable add-on. We
included the \phox proof checker \cite{PhoX} because 1) it can be
extended to the \B mathematical foundations; 2) its GPL licence
permits distribution along with \bcaml; 3) its developers were willing to
work closely with us; and 4) its intuitive syntax minimises library
development time.


Our contributions consist of libraries dedicated to set theory for the
\phox proof assistant and the necessary extensions to make the 
proving task automatable. These extensions include:
\begin{itemize}
\item \textbf{A time-out process controller} The \phox proof assistant is
  interactive but can be used on the command-line to
  ``compile'' (i.e., replay) proof scripts. We decided to add time-out
  limits in order to identify difficult proofs more quickly so they can be
  handled by a human.

\item \textbf{XSL stylesheets} These stylesheets are used for translating 
\B proof obligations, saved
  as XML files, to \BphoX proof obligations.

\item \textbf{A GNU Make script} These scripts re used to handle the whole
  chain from \B proof
  obligations to proved formulas.
\end{itemize}
The result is a plugout for \B projects, defined on top of the \phox
proof assistant, for automatically and interactively proving \B proof
obligations. 


\retrait{trop implementation}{
\subsection{The \emph{bphox} \Makefile script}
\label{sec:bphox-makefile}

\begin{implementation}
A \B prover is designed to verify whether each proof obligation is a
theorem or not. In \bcaml, every B/XML obligation has to be translated
into the \phoX syntax and has to be proved. \phoX produces a po.pho
file from a po.phx translated proof obligation when the proof is
successful. The \BphoX session involves a one-step (.phx to .pho) or a
two-step (.xml to .phx then .phx to .pho) transformation, depending on
the file extension.  This process corresponds exactly to the GNU Make
transformation using a suffix schemata, and can be copied and
configured to link \bcaml to other theorem provers. As proved by
Rocheteau \textit{et al.}  \cite{RocheteauColinMarianoPoirriez04}, the
principal property that must be preserved throughout this process is
that every sentence is a \B theorem, if and only if its translation is
a \BphoX theorem.
\end{implementation}

}

\subsection{The \emph{bgop2phox} \xsl style sheet}
\label{sec:bphox-xsl}


%\begin{wrapfigure}{l}{0pt}
  \begin{minipage}{0.45\linewidth}
    \lstinputlisting[language=PhoX,basicstyle={\scriptsize \sffamily},frame=single,framesep=1ex]{figures/po-phox-timeout1.phx}
  \end{minipage}
  \caption{One of the exploded proof obligations for \textbf{timeOut\_1\_showRlight},  converted to B/Phox}
  \label{fig:po-phox-timeout1}
%\end{wrapfigure}

During the translation step, our \xsl \emph{bgop2phox} stylesheet is
applied to the \Bxml \POs using a \xslt processor.  The \xsl
transformation schema allows recursive mapping. Thus, our translation
is also defined recursively.  A first-order language \emph{� la B} is
composed of various symbols for functions, relations, connectors and
quantifiers.  Figure~\ref{fig:po-phox-timeout1} is the output of the
\xsl stylesheet applied to the formula in
figure~\ref{fig:po-timeOut_1_showRlight}, after it was calculated and
saved in an \xml file.


A high-order language \emph{� la \phoX} is a simply-typed lambda
calculus with some typed constants.  Our translation is based on
associating every first-order \B symbol $S$ with a \BphoX expression
$S^\dag$, such that its extension to the first-order terms formulae is
simply defined by an inductive commutation. We showed
\cite{RocheteauColinMarianoPoirriez04} that under reasonable
assumptions (basic constants and functions are similar up to the
$^\dag$ translation) proofs made with \BphoX are equivalent to \B
proofs. As a consequence, using the \phoX system of
simple types makes our translation sound.  Moreover, every
non-freeness rule and every substitution rule can be easily obtained
through the $\lambda$ binder properties.


%DP non, trop implantation \GM{un bout de la feuille bgop2phox ?}


\subsection{The \emph{blib} \phox library}
\label{sec:bphox-blib}

The PhoX library for \B reflects the first three chapters of the
B-Book.  The content of the library is outlined briefly here because
the process for embedding \B into \phoX is based on it.  (More details
are available in Rocheteau \textit{et al.}
\cite{RocheteauColinMarianoPoirriez04}).  In fact, the library is a
collection of successive libraries for predicate calculus with
equality, the boolean domain, cartesian products, set operators,
binary relations, functions, arithmetic theory and finite sequencing.


\subsection{The \BphoX proof process}
\label{sec:bphox-proofprocess}

% \retrait{trop implantation}{\begin{implementation}
% The proof step consists of producing a \textsf{po.pho} file from a
% \textsf{po.phx} one.  The existence of the \textsf{po.pho} file means
% that the required obligation holds. The absence of this file can mean
% that the proof obligation does not hold. It can also mean that the
% proof can not be completed due to lack of time or space, causing the
% proof session to loop endlessly. In order to deal with this problem,
% every \phoX call is accompanied by a process killer named
% \emph{chronos}.
  
% \end{implementation}
% }

A successful proof for a \B project is built using a fixed-point
process involving the time-out controller.
The whole set of generated proof obligations is run through a first
session of the prover, with proofs that take longer than an arbitrary
value being forcibly stopped via the time-out controller. 
A second session is then run, but only for those proof obligations 
that could not be proved, with a greater time-out value. 
All subsequent sessions follow the same logic, each time
increasing the time-out value, until all proof obligations have been
 proved or until the engineer decides to stop everything and to prove
 the problematic formulas interactively. 
There is one drawback to this method: because PhoX is used as a "black box",
 the state of the ongoing proof is not saved at that moment it is stopped; 
thus the next session will have to replay the proof from the beginning. 

Table \ref{tab:proof-boiler} shows the result of a proof session for the
famous "Boiler" B project. This table shows that all the reasonably easy
proofs are finished within a small time-out value. The results are similar
to the results for the same B project using the Atelier B tool, which
demonstrates the viability of our approach using an interactive prover
for automated proving. 

\begin{table}[htb]
  \begin{tabular}{p{4cm}>{\hfill}r>{\hfill}r>{\hfill}r}
    Time-out & 1 s. & 5 s. & 60 s. \\ 
    \hline
    Generated \POs & & 2295 & \\
    Successful & 1823 & 1955 & 1971 \\
    Failed & 0 & 0 & 0 \\
    Stopped & 462 & 340 & 324 \\
    \hline
    Proof Rate & 79\% & 85\% & 85\% \\ 
  \end{tabular}
  \caption{Proof results for the Boiler}
  \label{tab:proof-boiler}
\end{table}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "body"
%%% End:

