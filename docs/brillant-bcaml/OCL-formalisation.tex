% -*- coding: iso-8859-1 -*-
%

\subsection{Formalization of OCL constraints}
\label{sec:form-ocl-constraints}


In this section, we explain how OCL expressions are translated into \B
expressions, using the rules defined for the OCL
  meta-model in Marcano's PhD dissertation~\cite{Marcano-Phd}.

The Object Constraint Language has thus far been defined semi-formally
using textual descriptions, a grammar that specifies the concrete
syntax, and examples that illustrate the semantics of the expressions.
Such a presentation style is adequate for illustrating OCL concepts,
but it is not sufficient for providing a rigourous semantic.  This
semi-formal nature of the OCL definition, which often leads users to
interpret the UML models ambiguously, restricts its use in critical
safety applications.  This difficulty is increased by the lack of
tools supporting the analysis of OCL expressions and the "proof" of
complete UML models.

Recent work proposing a precise semantic for OCL has been carried out
by Richters and Gogolla~\cite{Richters98b}. In addition, both these
authors~\cite{Richters00} and Jackson \textit{et al.}~\cite{Jackson00}
have published research related to tools for verifying UML designs.
The first publication proposes an approach for validating UML models
using simulation, and the second proposes an object model analyzer
that uses Alloy, which is based on Z.  Two of the authors of the
present paper have also previously worked to formalize OCL with \B,
using a system of translation rules between the abstract syntaxes of
both languages~\cite{Marcano02b}.


In the \BRILLANT plug-in, two types of OCL constraints are taken into
account. The first type of constraint specifies an invariant of a
class, and the second type specifies a precondition and/or a
postcondition of an operation. In the first case, translating the OCL
constraint consists of combining a new predicate with the invariant of
the related \B machine, whereas in the second case, it requires
completing an operation of the machine. The formalization of the LCC
system's OCL invariant is shown in Fig.\ref{invariant}.


\begin{figure}[ht]
  \begin{center}
    \begin{minipage}{0.95\linewidth}
      \lstinputlisting[language=Bmethod,basicstyle={\scriptsize \sffamily},frame=single,framesep=1ex]{LCC_System-OCL.amn}
    \end{minipage}
  \end{center}
  \caption{Formalization of OCL invariants}
  \label{invariant}
\end{figure}


In the same way that OCL predicates enrich the UML model, OCL pre- and
post-conditions are used to enrich \B machine operations.  In
Fig.\ref{transition0}, the pre-condition of the operation
\code{timeOut\_1} not only requires that the LCC system be in the
\code{yellow\-Light} state (which is generated from the state diagram)
but also that the red light be switched on and the barrier be closed.
These three contraints together constitute the translation
of the OCL predicate: 

%{\sffamily\small self.yellowLight.state=On and self.theBarrier.state=Opened}.
\begin{ocl}
  self.yellowLight.state=On and self.theBarrier.state=Opened.
\end{ocl}

The post-condition of the operation initially includes only the substitution 
\code{state(obj):=ClosingB}
that sets the new state of the LCC instance (obj).  This
post-condition is completed by translating the OCL post-condition

%{\sffamily\small self.yellowLight.state=Off and
%  self.redLight.state=On and self.theBarrier.state=Closed}
\begin{ocl}
self.yellowLight.state=Off and
self.redLight.state=On and self.theBarrier.state=Closed  
\end{ocl}
into \B, which generates the following parallel substitutions: 

\code{closeBarrier (lcc\_barrier(obj))\linebreak|| Yellow.switchOff
  (yellowLight(obj))\linebreak|| Red.switchOn (redLight(obj)) }

%\ajout{
Please note that although OCL constraints can be sufficient for
expressing the behavior of the model, it can be very difficult to
extract this behavior in order  to express it in terms of B
operations. Thus, state diagrams are necessary to provide the
skeleton for the behavior, upon which additional information can be
imported from the OCL pre- and post-conditions.
%}
%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "sosym06"
%%% End: 
