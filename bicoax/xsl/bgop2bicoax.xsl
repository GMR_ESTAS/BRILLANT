<!-- $Id$ -->

<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns="http://www.w3.org/TR/REC-html40">

<xsl:output method="text" omit-xml-declaration="yes"/>

<xsl:template match="ProofObligation">
<xsl:text> 

Require Import Blib.

Theorem op:
</xsl:text>
 <xsl:apply-templates select="child::IdList" mode="variables"/>
 <xsl:apply-templates select="child::Predicate" mode="top"/>
 <xsl:text>.
try intuition 
   || unfold BBOOL ; intuition ; apply Full_intro
   || red ;  red ; red ; omega.
Qed.
 </xsl:text>
</xsl:template>

<xsl:template match="IdList" mode="variables">
 <xsl:if test="count(child::node())>0">
   <xsl:text>forall </xsl:text>
   <xsl:for-each select="child::Id" mode="variables">
       <xsl:text>_</xsl:text>
     <xsl:value-of select="." />
     <xsl:if test="position()!=last()">
       <xsl:text> </xsl:text>
     </xsl:if>
   </xsl:for-each>
   <xsl:text>, </xsl:text>
 </xsl:if>
</xsl:template>

<xsl:template match="Id" mode="variables">
  <xsl:value-of select="self::node()" />
</xsl:template>

<!-- We want to replace the big conjunction in hypothesis (if there's
one) with a list of implications, to ease PhoX's automatic proving -->

<xsl:template match="Predicate" mode="top">
 <xsl:choose>
  <xsl:when test=" name(child::node())= 'Implies'">
   <xsl:apply-templates select="child::Implies" mode="split"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:apply-templates select="child::node()"/>
  </xsl:otherwise>
 </xsl:choose>
</xsl:template>


<xsl:template match="Implies" mode="split">
 <xsl:text>( 
 </xsl:text>
 <xsl:apply-templates select="child::node()[position()=1]" mode="hypothesis"/>
 <xsl:text>
 </xsl:text>
 <xsl:text> -> (
 </xsl:text>
 <xsl:apply-templates select="child::node()[position()=2]"/>
 <xsl:text> ) )
 </xsl:text>
</xsl:template>


<xsl:template match="And" mode="hypothesis">
 <xsl:apply-templates select="child::node()[position()=1]" mode="hypothesis"/>
 <xsl:text> -> 
</xsl:text>
 <xsl:apply-templates select="child::node()[position()=2]" mode="hypothesis"/>
</xsl:template>

<xsl:template match="PredParen" mode="hypothesis">
 <xsl:choose>  
  <xsl:when test ='child::node()=And' >
   <xsl:apply-templates select="child::node()" mode="hypothesis"/>
  </xsl:when>
  <xsl:when test ='child::node()=PredParen' >
   <xsl:apply-templates select="child::node()" mode="hypothesis"/>
  </xsl:when>
  <xsl:otherwise>
   <xsl:apply-templates select="self::node()"/>
  </xsl:otherwise>
 </xsl:choose> 
</xsl:template>

<xsl:template match="*" mode="hypothesis">
 <xsl:apply-templates select="self::node()"/>
</xsl:template>

<!-- Now "naive" handling of the xml structure -->

<xsl:template match="PredParen">
 <xsl:apply-templates select="child::node()"/>
</xsl:template>

<xsl:template match="Neg">
 <xsl:text>~ (</xsl:text>
 <xsl:apply-templates select="child::node()"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="And">
 <xsl:text>(</xsl:text>
 <xsl:apply-templates select="child::node()[position()=1]"/>
 <xsl:text> /\ </xsl:text>
 <xsl:apply-templates select="child::node()[position()=2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Or">
 <xsl:text>(</xsl:text>
 <xsl:apply-templates select="child::node()[position()=1]"/>
 <xsl:text> \/ </xsl:text>
 <xsl:apply-templates select="child::node()[position()=2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Implies">
 <xsl:text>(</xsl:text>
 <xsl:apply-templates select="child::node()[position()=1]"/>
 <xsl:text> -> </xsl:text>
 <xsl:apply-templates select="child::node()[position()=2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Equiv">
 <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::node()[position()=1]"/>
  <xsl:text>  &lt;-&gt;  </xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Exists">
  <xsl:apply-templates select="IdList" mode="exists"/>
  <xsl:text>, (</xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ForAll">
  <xsl:text>forall </xsl:text>
  <xsl:apply-templates select="IdList" mode="nocomma"/>
  <xsl:text>, (</xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Equal">
  <xsl:text>(</xsl:text>
   <xsl:apply-templates select="child::node()[position()=1]"/>
   <xsl:text> = </xsl:text>
   <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="NotEqual">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::node()[position()=1]"/>
  <xsl:text> &lt;&gt; </xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="In">
  <xsl:text>(</xsl:text>
  <xsl:text>In </xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::node()[position()=1]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="NotIn">
  <xsl:text>(</xsl:text>
  <xsl:text>NotIn </xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::node()[position()=1]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="SubSet">
  <xsl:text>(</xsl:text>
  <xsl:text>Included </xsl:text>
  <xsl:apply-templates select="child::node()[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="StrictSubSet">
  <xsl:text>(</xsl:text>
  <xsl:text>Strict_Included </xsl:text>
  <xsl:apply-templates select="child::node()[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="NotSubSet">
  <xsl:text>(</xsl:text>
  <xsl:text>NotIncluded </xsl:text>
  <xsl:apply-templates select="child::node()[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="NotStrictSubSet">
  <xsl:text>(</xsl:text>
  <xsl:text>NotStrict_Included </xsl:text>
  <xsl:apply-templates select="child::node()[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="LessEqual">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::node()[position()=1]"/>
  <xsl:text> &lt;= </xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Less">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::node()[position()=1]"/>
  <xsl:text> &lt; </xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="GreaterEqual">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::node()[position()=1]"/>
  <xsl:text> &gt;= </xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]" />
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Greater">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::node()[position()=1]"/>
  <xsl:text> &gt; </xsl:text>
  <xsl:apply-templates select="child::node()[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Predicate">
 <xsl:text>(</xsl:text>
 <xsl:apply-templates select="child::node()"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Expr">
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="ExprParen">
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="MAXINT">
  <xsl:text>MAXINT</xsl:text>
</xsl:template>

<xsl:template match="MININT">
  <xsl:text>MININT</xsl:text>
</xsl:template>

<xsl:template match="Minus"> 
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> - </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="UMinus">
  <xsl:text>(- </xsl:text>
  <xsl:apply-templates select="Expr"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Plus">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> + </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Mul">
 <xsl:text>(</xsl:text>
 <xsl:apply-templates select="child::*[position()=1]"/>
 <xsl:text> * </xsl:text>
 <xsl:apply-templates select="child::*[position()=2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Div">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> / </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Modulo">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> mod </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Puissance">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> ^ </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Succ">
  <xsl:text>(Zsucc </xsl:text>
  <xsl:apply-templates select="Expr"/> 
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Min">
  <xsl:text>(Bmin </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Max">
  <xsl:text>(Bmax </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Card">
  <xsl:text>(card _ </xsl:text>
  <xsl:apply-templates select="Expr"/>
<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="PI">
  <xsl:text>(Pi </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="SIGMA">
  <xsl:text>(Sigma </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="LAMBDA">
  <xsl:text>( </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Maplet">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>,</xsl:text>
    </xsl:if>
  </xsl:for-each>
  <!--  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="position()=1">
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text> \BMap </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each> -->
</xsl:template>

<xsl:template match="SetPredefined">
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="NAT">
  <xsl:text>BN</xsl:text>
</xsl:template>

<xsl:template match="INT">
  <xsl:text>BZ</xsl:text>
</xsl:template>

<xsl:template match="BOOL">
  <xsl:text>BBOOL</xsl:text>
</xsl:template>

<xsl:template match="INTEGER">
  <xsl:text>BConcreteIntegers</xsl:text>
</xsl:template>

<xsl:template match="NATURAL">
  <xsl:text>BConcreteNaturals</xsl:text>
</xsl:template>

<xsl:template match="NAT1">
  <xsl:text>BN1</xsl:text>
</xsl:template>

<xsl:template match="NATURAL1">
  <xsl:text>BConcreteNaturals1</xsl:text>
</xsl:template>

<xsl:template match="SetEmpty">
  <xsl:text>(Empty_set _)</xsl:text>
</xsl:template>

<xsl:template match="STRING">
</xsl:template>

<xsl:template match="SetCompr">
  <xsl:text>{= </xsl:text>
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text> =}</xsl:text>
</xsl:template>

<xsl:template match="SetEnum">
  <xsl:text>(fun o => (</xsl:text>
  <xsl:for-each select="*">
    <xsl:text>(o=</xsl:text>
    <xsl:apply-templates select="."/>
    <xsl:text>)</xsl:text>
    <xsl:if test="position()!=last()">
      <xsl:text> \/ </xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text>))</xsl:text>
</xsl:template>

<xsl:template match="Pow">
  <xsl:text>(Power_set </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="PowOne">
  <xsl:text>(Power_set1 </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Fin">
  <xsl:text>(Power_finite </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="FinOne">
  <xsl:text>(Power_finite1 </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="InterSets">
  <xsl:text>(</xsl:text>
  <xsl:text>Intersection </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="UnionSets">
  <xsl:text>(</xsl:text>
  <xsl:text>Union </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="SetMinus"> 
  <xsl:text>(</xsl:text>
  <xsl:text>Setminus </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="InterQ">
  <xsl:text>(INTER </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="UnionQ">
  <xsl:text>(UNION </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="UnionG">
  <xsl:text>(UnionG </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="InterG">
  <xsl:text>(InterG </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>


<xsl:template match="CartesianProd">
 <xsl:text>(</xsl:text>
 <xsl:text> times </xsl:text>
 <xsl:apply-templates select="child::*[position()=1]"/>
 <xsl:text> </xsl:text>
 <xsl:apply-templates select="child::*[position()=2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RelSet">
  <xsl:text>(relation </xsl:text>
<xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Identity">
  <xsl:text>(id </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Tilde"><!-- GMR -->
  <xsl:text>(inverse </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="PrjOne">
  <xsl:text>(prj1 </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text> )</xsl:text>
</xsl:template>

<xsl:template match="PrjTwo">
  <xsl:text>(prj2 </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RelSeqComp">
  <xsl:text>(</xsl:text>
  <xsl:text>composition </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ParallelComp">
  <xsl:text>(</xsl:text>
  <xsl:text>parallel_product </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RelProd">
  <xsl:text>(</xsl:text>
  <xsl:text>direct_product </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<!-- TODO: the first argument is the set the iteration is defined upon -->
<xsl:template match="RelIter">
  <xsl:text>(iter _ </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Closure">
  <xsl:text>(closure </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ClosureOne">
  <xsl:text>(closure1 </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Dom"> 
  <xsl:text>(domain </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Ran"> 
  <xsl:text>(range </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="DomSubstract">
  <xsl:text>(domain_subtraction </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RanSubstract">
  <xsl:text>(range_subtraction </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="DomRestrict">
  <xsl:text>(domain_restriction </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RanRestrict">
  <xsl:text>(range_restriction </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="PartialFunc">
  <xsl:text>(partial_function </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="TotalFunc">
  <xsl:text>(total_function </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> )</xsl:text> 
</xsl:template>

<xsl:template match="PartialInj">
  <xsl:text>(partial_injection </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text> 
</xsl:template>

<xsl:template match="TotalInj">
  <xsl:text>(total_injection </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text> 
</xsl:template>

<xsl:template match="PartialSurj">
  <xsl:text>(partial_surjection </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text> 
</xsl:template>

<xsl:template match="TotalSurj">
  <xsl:text>(total_surjection </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text> 
</xsl:template>

<xsl:template match="PartialBij">
  <xsl:text>(partial_bijection </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text> 
</xsl:template>

<xsl:template match="TotalBij">
  <xsl:text>(total_bijection </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text> 
</xsl:template>

<!-- SC: normally this tag should never appear. Left for compatibility -->
<xsl:template match="BoolEvaluation">
 <xsl:apply-templates select="child::*"/>
</xsl:template>

<xsl:template match="Lambda">
  <xsl:text>(lambda </xsl:text>
  <xsl:text>(fun c => match c with | (</xsl:text>
  <xsl:apply-templates select="IdList"/>
  <xsl:text>) => (</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>) end)</xsl:text>
  <xsl:text> (fun c => match c with | (</xsl:text>
  <xsl:apply-templates select="IdList"/>
  <xsl:text>) => (</xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>) end)</xsl:text>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ExprFunCall">
  <xsl:text>(app </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> (</xsl:text>
  <xsl:for-each select="child::ExprList/*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>,</xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text>) _)</xsl:text>
</xsl:template>

<xsl:template match="RelFnc">
  <xsl:text>(relation.of.function </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="FncRel">
  <xsl:text>(function.of.relation </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Seq">
  <xsl:text>(seq </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="SeqOne">
  <xsl:text>(seq1 </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ISeq">
  <xsl:text>(iseq </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ISeqOne">
  <xsl:text>(iseq1 </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Perm">
  <xsl:text>(perm </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Size">
  <xsl:text>(length </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="First">
  <xsl:text>(head </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Last">
  <xsl:text>(last </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Tail">
  <xsl:text>(tail </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Front">
  <xsl:text>(front </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Rev">
  <xsl:text>(rev </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="AppendSeq">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> gets </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ConcatSeq">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> @ </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="PrependSeq">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> :: </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="PrefixSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \SeqPref </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="SuffixSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \SeqSuff </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Conc">
  <xsl:text>(conc </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Tree">
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="BTree">
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Const">
  <xsl:text>(const </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Top">
  <xsl:text>(top </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Sons">
  <xsl:text>(sons </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Prefix">
  <xsl:text>(prefix </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Postfix">
  <xsl:text>(postfix </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="SizeT">
  <xsl:text>(sizet </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Mirror">
  <xsl:text>(mirror </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Rank">
  <xsl:text>(rank </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Father">
  <xsl:text>(father </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Son">
  <xsl:text>(son </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Subtree">
  <xsl:text>(subtree </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Arity">
  <xsl:text>(arity </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Bin">
  <xsl:text>(bin </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Left">
  <xsl:text>(left </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Right">
  <xsl:text>(right </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Infix">
  <xsl:text>(infix </xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ExprSequence">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>,</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NatSetRange">
 <xsl:text>(interval </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="OverRideFwd">
  <xsl:text>(override </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="OverRideBck">
  <xsl:text>(override </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ExprId">
  <xsl:apply-templates select="."/>
</xsl:template>

<xsl:template match="ExprList">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>,</xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="Assign">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> := </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!--
<xsl:template match="Records">
    <xsl:text> rec(</xsl:text>
    <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
    <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RecordAccess">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>'</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="RecWithFields">
  <xsl:text>
struct(
  </xsl:text>
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RecordItem">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Field">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="SetEqualRecord">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> := </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>
-->

<xsl:template match="Id">
    <xsl:text>_</xsl:text>
  <xsl:value-of select="self::node()" />
</xsl:template>

<xsl:template match="IdList" mode="exists">
  <xsl:for-each select="child::Id">
    <xsl:text>exists </xsl:text>
    <xsl:text>_</xsl:text>
    <xsl:value-of select="." />
    <xsl:if test="position()!=last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="IdList" mode="nocomma">
  <xsl:for-each select="child::Id">
      <xsl:text>_</xsl:text>
      <xsl:value-of select="." />
    <xsl:if test="position()!=last()">
      <xsl:text> </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="IdList">
  <xsl:for-each select="child::Id">
<!-- GMR  typo?      <xsl:text>, </xsl:text> -->
      <xsl:text>_</xsl:text>
    <xsl:value-of select="." />
    <xsl:if test="position()!=last()">
      <xsl:text>,</xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="Number">
  <xsl:value-of select="self::node()" />
</xsl:template>

<xsl:template match="TRUE">
  <xsl:text>true</xsl:text>
</xsl:template>

<xsl:template match="FALSE">
  <xsl:text>false</xsl:text>
</xsl:template>


</xsl:stylesheet>
