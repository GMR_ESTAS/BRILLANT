
What is BiCoax ?
================

BiCoax is the transcription of the efforts done in BPhoX into Coq. It
could have been named BiCoq but:

- The name is already taken (though I [SC] still wait to see it -- it
  seems that every effort of coding B in Coq is never displayed. Let
  BiCoax be the exception !)

- BiCoax means it follows the same "axe" as BPhoX.

The requirements of BiCoax are Coq 8.2 and xsltproc if you want to try to
convert xml proof obligations into the BiCoax format (this step is still
very "alpha" at the moment).


What is the status of BiCoax ?
==============================

Without being especially precise, it can be said that all properties in the
BBook, from the beginning of chapter 1 to the middle of chapter (transitive
closures of a relation) inclusively, are implemented.

See the STATUS file to get more details.


Can I use it ?
==============

Yes, the types of the various operators are set, or close enough. What
might change though:

- The name of the operators ("rel" might be renamed "relation", for
  instance)

How can I help ?
================

See what is left to be done in the STATUS file.

What is left to do in BiCoax ?
==============================

Same as above. Additional notes:

- Defining the sequences: as set constructs or coercions from Coq's
  lists to sets. The latter would be the preferred approach of BPhoX. Keep
  in mind that at the moment we preferred avoiding coercions to have a
  direct set-equivalence of definitions

- Automated tactics: these will become more crucial as the number of
  theorems increase. They also might be necessary to make handling
  function application easier in day-to-day proving.


Speaking of proof obligations...
================================

Thanks to type inference, BPhoX could take universally-quantified formulas
where bound variables had no type specifications: PhoX inferred them.

With BiCoax it is not that simple, because for some constructs have
non-inferrable parameters (function application is a good example of this:
it is dependent upon some properties about the passed function and element).

At the moment this "non-inferability" of those parts is a strong argument
for having a typer in the toolchain. 

