(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Export Bgeneralized_union_inter.
Require Export Bfixpoints.
Require Export Bfinite_subsets.
Require Export Binfinite_subsets.
Require Export Bnaturals.
Require Export Bintegers.
Require Export Bbools.
Require Export Bsequences.
Require Export Btrees.
Require Export Blabelled_trees.
Require Export Bbinary_trees.
Require Export Bwell_founded.
