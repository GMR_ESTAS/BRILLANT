(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Export Membership_laws.
Require Export Monotonicity_laws.
Require Export Inclusion_laws.
Require Export Equalities_inverse.
Require Export Equalities_domain.
Require Export Equalities_range.
Require Export Equalities_composition.
Require Export Equalities_identity.
Require Export Equalities_domain_restriction.
Require Export Equalities_domain_subtraction.
Require Export Equalities_image.
Require Export Equalities_overriding.
Require Export Equalities_evaluation.
