(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bbasic.
Require Import Brelations.
Require Import Binclusion_props.

(**
  * Functions: first series definitions
  *)

(**
  partial function, BBook: section 2.5.1, p.86, array 1, row 1
  *)
Inductive partial_function (U V: Type) (A: Ensemble U) (B: Ensemble V): Ensemble (Ensemble (U*V)) :=
  pfun_intro : forall (f: Ensemble (U*V)),
    In (relation A B) f
  -> (forall (x: U) (y: V), In f (x,y) -> (forall (z: V), In f (x,z) -> y=z))
  -> partial_function U V A B f.
Implicit Arguments partial_function [U V].
Notation "x ⇸ y" := (partial_function x y) (at level 66, right associativity): eB_scope.

(**
  total function, BBook: section 2.5.1, p.86, array 1, row 2
  *)
Inductive total_function (U V: Type) (A: Ensemble U) (B: Ensemble V): Ensemble (Ensemble (U*V)) :=
  tfun_intro: forall (f: Ensemble (U*V)),
    In (partial_function A B) f
  -> domain f = A
  -> total_function U V A B f.
Implicit Arguments total_function [U V].
Notation "x → y" := (total_function x y) (at level 66, right associativity): eB_scope.

(**
  partial injection, BBook: section 2.5.1, p.86, array 1, row 3
  *)
Inductive partial_injection (U V: Type) (A: Ensemble U) (B: Ensemble V): Ensemble (Ensemble (U*V)) :=
  pinj_intro: forall (f: Ensemble (U*V)),
    In (partial_function A B) f
  -> (forall (x z: U) (y:V), In f (x,y) -> In f (z,y) -> x=z)
  -> partial_injection U V A B f.
Implicit Arguments partial_injection [U V].
Notation "x ⤔ y" := (partial_injection x y) (at level 66, right associativity): eB_scope.

(**
  total injection, BBook: section 2.5.1, p.86, array 1, row 4
  *)
Inductive total_injection (U V: Type) (A: Ensemble U) (B: Ensemble V): Ensemble (Ensemble (U*V)) :=
  tinj_intro: forall (f: Ensemble (U*V)),
    In (total_function A B) f
  -> In (partial_injection A B) f
  -> total_injection U V A B f.
Implicit Arguments total_injection [U V].
Notation "x ↣ y" := (total_injection x y) (at level 66, right associativity): eB_scope.

(**
  partial surjection, BBook: section 2.5.1, p.86, array 1, row 5
  *)
Inductive partial_surjection (U V: Type) (A: Ensemble U) (B: Ensemble V): Ensemble (Ensemble (U*V)) :=
  psurj_intro: forall (f: Ensemble (U*V)),
    In (partial_function A B) f
  -> (forall (y:V), (In B y -> exists x:U, In f (x,y)))
  -> partial_surjection U V A B f.
Implicit Arguments partial_surjection [U V].
Notation "x ⤀ y" := (partial_surjection x y) (at level 66, right associativity): eB_scope.

(**
  total surjection, BBook: section 2.5.1, p.86, array 1, row 6
  *)
Inductive total_surjection (U V: Type) (A: Ensemble U) (B: Ensemble V): Ensemble (Ensemble (U*V)) :=
  tsurj_intro: forall (f: Ensemble (U*V)),
    In (total_function A B) f
  -> In (partial_surjection A B) f
  -> total_surjection U V A B f.
Implicit Arguments total_surjection [U V].
Notation "x ↠ y" := (total_surjection x y) (at level 66, right associativity): eB_scope.

(**
  partial bijection, BBook: section 2.5.1, p.86, array 1, row 7
  *)
Inductive partial_bijection (U V: Type) (A: Ensemble U) (B: Ensemble V): Ensemble (Ensemble (U*V)) :=
  pbij_intro: forall (f: Ensemble (U*V)),
    In (partial_injection A B) f -> In (partial_surjection A B) f ->
    partial_bijection U V A B f.
Implicit Arguments partial_bijection [U V].
Notation "x ⤗ y" := (partial_bijection x y) (at level 66, right associativity): eB_scope.

(**
  total bijection, BBook: section 2.5.1, p.86, array 1, row 8
  *)
Inductive total_bijection (U V: Type) (A: Ensemble U) (B: Ensemble V): Ensemble (Ensemble (U*V)) :=
  tbij_intro: forall (f: Ensemble (U*V)),
    In (total_function A B) f
  -> In (partial_bijection A B) f
  -> total_bijection U V A B f.
Implicit Arguments total_bijection [U V].
Notation "x ⤖ y" := (total_bijection x y) (at level 66, right associativity): eB_scope.

(**
  * Functions: second series definitions
  *)


(**
  lambda abstraction, BBook: section 2.5.2, p.89, array 1, row 1

  also:

  lambda abstraction with restricting property, BBook: section 2.5.2, p.89, array 1, row 2

  Set belonging is just a particular predicate, hence we can have a general form such as below.
  *)
Inductive lambda (S T: Type) (s: Ensemble S) (f: S -> T): Ensemble (S*T) :=
  lambda_intro: forall (x: S) (y: T), In s x -> y = (f x) -> lambda S T s f (x,y).
Implicit Arguments lambda [S T].
Notation "'λ' x · ( K | E )" := (lambda (fun x => K) (fun x => E)) (at level 200, no associativity): eB_scope.

(*
Theorem plop: (λ x · ( x ∈ (Full_set nat) | x ))%eB = (id (Full_set nat)).
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0.
constructor.
rewrite H0; constructor; intuition.
rewrite H0; reflexivity.
induction H0.
inversion H.
constructor.
intuition.
intuition.
Qed.
*)

(**
  The following theorems are required for the definition of function application
  *)
Theorem non_empty_partial_function_is_uniquely_inhabited: forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
  forall (f: Ensemble (U*V)) (x: U),
  In (partial_function A B) f -> In (domain f) x ->
  Inhabited V (fun y : V => f (x,y) /\ (forall z : V, f (x,z) -> z = y)).
Proof.
intros.
inversion H.
inversion H1.
inversion H0.
induction H6.
constructor 1 with (x0).
split.
intuition.
intros.
apply H2 with x.
intuition.
intuition.
Qed.

Definition Inhabitation: forall (V: Type) (P:V -> Prop), Inhabited V P -> inhabited V.
Proof.
intros.
cut (ex (fun y => P y)).
intros.
elim H0.
intros.
constructor 1.
apply x.
elim H.
intros.
exists x; intuition.
Qed.

Definition codomain_unique_inhabitation: forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
  forall (f: Ensemble (U*V)) (x: U),
  In (partial_function A B) f /\ In (domain f) x ->
   inhabited V.
Proof.
intros.
apply Inhabitation with (fun y : V => f (x,y) /\ (forall z : V, f (x,z) -> z = y)).
apply non_empty_partial_function_is_uniquely_inhabited with A B; intuition.
Qed.

(**
  function application, BBook: section 2.5.2, p.89, array 1, row 3

  This definition of app is more restrictive than the "app" of BPhoX:

  - You must give a proof that the given relation is a partial
    function

  - You must give a proof that the given element inhabits the domain
    (implying that the domain is non-empty, btw)

  This corresponds exactly to the definition of function application
  of the BBook.  It will probably be a bit more cumbersome to use but
  the right theorems and a few tactics should alleviate this problem
  elegantly.
  *)
Definition app (U V:Type) (A: Ensemble U) (B: Ensemble V)(f: Ensemble (U*V)) (x:U)
  (applicability: In (partial_function A B) f /\ In (domain f) x) :=
  iota V
     (codomain_unique_inhabitation U V A B f x applicability)
     (fun y:V => f (x,y)).
Implicit Arguments app [U V A B].

Inductive app_property (U V:Type) (A: Ensemble U) (B: Ensemble V)(f: Ensemble (U*V)) (x:U) (P: V -> Prop): Prop :=
  app_intro:
  forall (applicability: In (partial_function A B) f /\ In (domain f) x),
   (P (@app U V A B f x applicability)) ->
   app_property U V A B f x P.

(**
  * Functions: first series validation
  *)

(**
  partial function, BBook: section 2.5.1, p.86, array 1, row 1
  *)
Theorem valid_partial_function: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), partial_function s t = (Comprehension (fun r => In (relation s t) r /\ Included (composition (inverse r) r) (id t))).
Proof.
intros S T s t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros f H.
induction H.
constructor.
intuition.
intros x H1.
induction x.
inversion H1.
inversion H3.
constructor.
repeat (induction H).
elim H5; intros H6 H7.
inversion H6.
generalize (H (x,b) H7); generalize (H (x,a) H9); intros H11 H12.
inversion H11; inversion H12; constructor; intuition.
apply H0 with x.
elim H5; intros H6 H7.
inversion H6; intuition.
intuition.
induction H.
constructor; [ intuition | ].
intros x y H1 z H2.
cut (In (composition (inverse f) f) (y,z)).
intros H3.
generalize (H0 (y,z) H3); intros H4.
inversion H4; intuition.
constructor.
exists x.
split; [ | intuition ].
constructor; intuition.
Qed.

(**
  total function, BBook: section 2.5.1, p.86, array 1, row 2
  *)
Theorem valid_total_function: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), total_function s t = (Comprehension (fun r => In (partial_function s t) r /\ (domain r) = s)).
Proof.
intros S T s t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros f H.
induction H.
constructor; intuition.
induction H.
constructor; intuition.
Qed.

(**
  partial injection, BBook: section 2.5.1, p.86, array 1, row 3
  *)
Theorem valid_partial_injection: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), partial_injection s t = (Comprehension (fun r => In (partial_function s t) r /\ In (partial_function t s) (inverse r))).
Proof.
intros S T s t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros f H.
induction H.
constructor; [intuition | ].
repeat (induction H).
repeat constructor.
intros a H9.
induction a.
induction H9.
generalize (H (a0,b0) H2); intros H10; inversion H10; constructor; intuition.
intros.
apply H0 with x.
inversion H2; intuition.
inversion H3; intuition.
induction H.
constructor; [ intuition | ].
inversion H0.
intros; apply H2 with y.
constructor; intuition.
constructor; intuition.
Qed.

(**
  total injection, BBook: section 2.5.1, p.86, array 1, row 4
  *)
Theorem valid_total_injection: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), total_injection s t = (Intersection (partial_injection s t) (total_function s t)).
Proof.
intros S T s t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros f H.
induction H.
constructor; intuition.
induction H.
constructor; intuition.
Qed.

(**
  partial surjection, BBook: section 2.5.1, p.86, array 1, row 5
  *)
Theorem valid_partial_surjection: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), partial_surjection s t = (Comprehension (fun r => In (partial_function s t) r /\ (range r) = t)).
Proof.
intros S T s t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros f H.
induction H.
constructor; [ intuition | ].
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros g H1.
inversion H1.
inversion H2.
repeat (induction H).
generalize (H (x,g) H4); intros H6; inversion H6; intuition.
constructor.
generalize (H0 g H1); intros H2; inversion H2.
exists x; intuition.
induction H.
constructor; [ intuition | ].
intros y H1.
induction H0.
induction H1.
induction H0.
exists x; intuition.
Qed.

(**
  total surjection, BBook: section 2.5.1, p.86, array 1, row 6
  *)
Theorem valid_total_surjection: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), total_surjection s t = (Intersection (partial_surjection s t) (total_function s t)).
Proof.
intros S T s t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros f H.
induction H.
constructor; intuition.
induction H.
constructor; intuition.
Qed.

(**
  partial bijection, BBook: section 2.5.1, p.86, array 1, row 7
  *)
Theorem valid_partial_bijection: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), partial_bijection s t = (Intersection (partial_injection s t) (partial_surjection s t)).
Proof.
intros S T s t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros f H.
induction H.
constructor; intuition.
induction H.
constructor; intuition.
Qed.

(**
  total bijection, BBook: section 2.5.1, p.86, array 1, row 8
  *)
Theorem valid_total_bijection: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), total_bijection s t = (Intersection (total_injection s t) (total_surjection s t)).
Proof.
intros S T s t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros f H.
induction H.
induction H0.
constructor; constructor; intuition.
induction H.
induction H; induction H0.
constructor; [ intuition | constructor; intuition ].
Qed.

(**
  * Functions: second series validation
  *)


(**
  lambda abstraction, BBook: section 2.5.2, p.89, array 1, row 1
  *)
Theorem valid_lambda: forall (S T: Type) (f: S -> T) (s: Ensemble S) (t: Ensemble T),
(forall (x:S), In s x -> (In t (f x))) ->
lambda s f = (Comprehension (fun C => match C with | (x,y) => (exists t:(Ensemble T), In (times s t) (x,y)) /\ y = (f x) end)).
Proof.
intros S T f s t H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0.
constructor.
exists t.
constructor; [ intuition | ].
generalize (H x H0); intros H2.
rewrite H1; intuition.
intuition.
induction x.
induction H0.
constructor.
inversion H0.
inversion H2; intuition.
intuition.
Qed.

(**
  lambda abstraction with restricting property, BBook: section 2.5.2, p.89, array 1, row 2
  *)
Theorem valid_lambda2: forall (S T: Type) (f: S -> T) (s: Ensemble S) (t: Ensemble T) (P: Prop),
(forall (x:S), In s x -> P -> (In t (f x))) ->
lambda (fun x => In s x /\ P) f = (Comprehension (fun C => match C with | (x,y) => (exists t:(Ensemble T), In (times s t) (x,y)) /\ P /\ y = (f x) end)).
Proof.
intros S T f s t P H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0.
constructor.
exists t.
constructor; [ intuition | ].
elim H0; intros H2 H3.
intuition.
elim H0; intros H2 H3.
rewrite H1; intuition.
elim H0; intuition.
induction x.
induction H0.
constructor.
inversion H0.
inversion H2; intuition.
intuition.
Qed.

(**
  function application, BBook: section 2.5.2, p.89, array 1, row 3
  *)
Theorem valid_app: forall (U V:Type) (A: Ensemble U) (B: Ensemble V)(f: Ensemble (U*V)) (E:U),
  In (partial_function A B) f /\ In (domain f) E -> exists applicability:(In (partial_function A B) f /\ In (domain f) E), app f E applicability = Bchoice V (codomain_unique_inhabitation U V A B f E applicability) (image f (Singleton E)).
Proof.
intros U V A B f E H.
exists H.
unfold app.
apply (iota_ind V (codomain_unique_inhabitation U V A B f E H) (fun y:V => f(E,y))).
intros b H0.
unfold Bchoice.
apply epsilon_ind.
exists b.
induction H0.
constructor 1 with E.
intuition.
intros.
apply (proj2 H0).
induction H1.
elim H1; intros H2 H3.
induction H2.
intuition.
elim H; intros H0 H1.
induction H0.
induction H1.
induction H1.
exists x.
split; [ intuition | intros x' H3; apply H2 with a; intuition].
Qed.

(**
  Useful theorems for manipulating functions and function application
  *)

Theorem unique_image:  forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
  forall (f: Ensemble (U*V)) (x: U),
  In (partial_function A B) f -> In (domain f) x -> forall (y:V), In f (x,y) -> unique (fun y => f (x,y)) y.
Proof.
intros.
split.
intuition.
induction H.
intros; apply H2 with x; intuition.
Qed.

Theorem identifying_app: forall (U V: Type) (A: Ensemble U) (B: Ensemble V) (f: Ensemble (U*V)) (x:U)
  (applicable: In (partial_function A B) f /\ In (domain f) x),
  forall (y:V), In f (x,y) -> y = (app f x applicable).
Proof.
intros.
unfold app.
elim applicable; intros.
induction H0.
apply (iota_ind V (codomain_unique_inhabitation U V A B f x applicable) (fun y0:V => f(x,y0))).
intros.
elim H3; intros.
symmetry; apply H5.
intuition.
exists y.
split.
intuition.
firstorder.
Qed.

Theorem application_in_codomain: forall (U V: Type) (A: Ensemble U) (B: Ensemble V) (f: Ensemble (U*V)) (x: U)
(applicable: In (partial_function A B) f /\ In (domain f) x),
In B (app f x applicable).
intros.
elim applicable; intros.
repeat (induction H).
repeat (induction H0).
replace (app X a applicable) with x.
generalize (H (a,x) H0); intros.
inversion H2; assumption.
apply H1 with a.
intuition.
unfold app; apply (iota_ind V (codomain_unique_inhabitation U V A B X a applicable) (fun y0:V => X (a,y0))).
firstorder.
exists x.
firstorder.
Qed.

Theorem app_trivial_property:  forall (U V: Type) (A: Ensemble U) (B: Ensemble V) (f: Ensemble (U*V)) (x: U)
(applicable: In (partial_function A B) f /\ In (domain f) x),
In f (x, app f x applicable).
intros.
elim applicable; intros.
unfold app; apply (iota_ind V (codomain_unique_inhabitation U V A B f x applicable) (fun y0:V => f (x,y0))).
intros.
induction H1; assumption.
repeat (induction H0).
induction H.
exists x.
split; [ assumption | ].
intros.
apply H1 with a; intuition.
Qed.

Theorem composition_is_partial: forall (U V W: Type) (A: Ensemble U)  (B: Ensemble V) (C: Ensemble W) f g,
 In (partial_function A B) f -> In (partial_function B C) g -> In (partial_function A C) (composition f g).
intros.
repeat constructor.
repeat (induction H; induction H0).
intros x H3.
induction x.
inversion H3.
inversion H5.
generalize (H (a,x) (proj1 H7)); intros.
generalize (H0 (x,b) (proj2 H7)); intros.
inversion H8; inversion H9; constructor; intuition.
intros.
repeat (induction H; induction H0).
inversion H1; inversion H2.
inversion H6.
inversion H9.
cut (x1= x0); intros.
apply H4 with x0.
intuition.
rewrite <- H13; intuition.
apply H3 with x; intuition.
Qed.

Theorem composition_domain: forall (U V W: Type) (A: Ensemble U)  (B: Ensemble V) (C: Ensemble W) (f: Ensemble (U*V)) (g: Ensemble (V*W)) (x: U),
(exists H:(In (partial_function A B) f /\ In (domain f) x), In (domain g) (app f x H)) -> In (domain (composition f g)) x.
intros.
induction H.
constructor.
inversion H.
inversion H0.
exists x1.
constructor.
exists (app f x x0).
split; [ apply app_trivial_property | intuition ].
Qed.

Theorem lambda_is_total: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: S -> T), (forall (x:S), In s x -> In t (f x)) -> In (total_function s t) (lambda s f).
Proof.
intros S T s t f H.
constructor.
constructor.
constructor.
constructor.
red; intros x H0.
induction H0.
constructor; [ assumption | rewrite H1; apply H; assumption ].
intros.
inversion H0; inversion H1.
rewrite H5; rewrite H9; reflexivity.
apply Extensionality_Ensembles; split; red; intros x H0.
induction H0.
induction H0.
inversion H0; assumption.
constructor.
exists (f x).
constructor; [ assumption | reflexivity ].
Qed.

Theorem abstract_lambda:  forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: S -> T) (P: T -> Prop) (x: S)
(appli_lambda: In (partial_function s t) (lambda s f) /\ In (domain (lambda s f)) x),
P (f x) -> P (app (lambda s f) x appli_lambda).
Proof.
intros S T s t f P x appli_lambda H.
replace (app (lambda s f) x appli_lambda) with (f x).
assumption.
apply identifying_app.
constructor; [ | reflexivity ].
induction appli_lambda.
induction H1.
induction H1.
inversion H1; assumption.
Qed.

(**
  A very interesting theorem showing that the behaviour of a function does not depend on its domain and codomain
  *)
Theorem irrelevant_applicability: forall (S T: Type) (s s': Ensemble S) (t t': Ensemble T) (f: Ensemble (S*T)) (x: S)
(appli0: In (partial_function s t) f /\ In (domain f) x) (appli1: In (partial_function s' t') f /\ In (domain f) x),
 app f x appli0 = app f x appli1.
Proof.
intros S T s s' t t' f x appli0 appli1.
unfold app.
apply iota_ind.
intros.
apply iota_ind.
intros.
induction H.
induction H0.
apply H1; intuition.
induction H.
exists b.
split.
intuition.
intuition.
induction appli0; induction appli1.
induction H; induction H1.
induction H0.
induction H0.
exists x.
split.
intuition.
intros.
apply H4 with a; intuition.
Qed.

Theorem total_function_domres_singleton_left: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)),
  In (total_function s t) f ->
  (forall (x:S), In s x -> In (total_function (Singleton x) t) (domain_restriction f (Singleton x))).
Proof.
intros S T s t f Htotf x Hxs.
rewrite valid_total_function in Htotf.
do 2 (red in Htotf).
destruct Htotf as [ Hpartf Hdomf ].
rewrite valid_partial_function in Hpartf.
do 2 (red in Hpartf).
destruct Hpartf as [ Hrelf Hidf ].
rewrite valid_total_function; do 2 red.
split.
rewrite valid_partial_function; do 2 red.
split.
do 2 constructor; red; intros.
destruct H as [ y z H0 H1 ].
split; [ assumption | ].
do 2 (induction Hrelf as [ f Hrelf ]).
specialize (Hrelf (y,z) H0); inversion Hrelf; assumption.
red; intros x0 Hcompx.
destruct Hcompx as [ a b Hcompx ].
destruct Hcompx as [ b0 Hcompx ].
destruct Hcompx as [ Hinvfx Hfx ].
apply Hidf.
inversion Hinvfx as [ c d Hdomresfx (H0, H1) ].
inversion Hdomresfx as [ c0 d0 Hf Hsingx (H2, H3) ].
constructor; exists x.
destruct Hsingx.
split; [ constructor; assumption | ].
destruct Hfx; assumption.
apply Extensionality_Ensembles; split; red; intros.
destruct H as [ a H ].
destruct H as [ b H ].
inversion H; assumption.
destruct H.
rewrite <- Hdomf in Hxs.
destruct Hxs as [ a Hxs ].
destruct Hxs as [ b Hxs ].
constructor; exists b.
constructor; intuition.
Qed.

(**
  error: in the BBook, this implication is likely to be false.

  Counterexample:
  Let f = {0|-1, 0|-2, 1|-2} and s = {1} and t = {0, 1, 2, ...}.
  The hypothesis is true:
  1 ∈ s. {1}◁f ∈ {1} → t ?
  dom({1}◁f) = {1} hence it is total.
  ({1}◁f)~; {1}◁f = {2|-1};{1|-2} = {2|-2} ⊆ (id t) hence it is functional
  {1}◁f ∈ {1} ↔ t ? i.e.
  {1}◁f ⊆ {1}×t ? {1|-2} ⊆ {1|-0, 1|-1, 1|-2,...} ? Yes
  Hence we proved that the hypothesis is true. Now, do we have:
  f ∈ s→ t ?
  dom(f) = {0,1} ≠ {1} = s. First problem.
  f~;f = {1|-0, 2|-0, 2|-1};f = {1|-1, 1|-2, 2|-1, 2|-2} ⊈ (id t). Second problem.
  f ∈ s↔ t ? i.e.
  {0|-1, 0|-2, 1|-2} ⊆ {1|-0, 1|-1,...} ? No, third problem.

Theorem total_function_domres_singleton_right: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)),
  (forall (x:S), In s x -> In (total_function (Singleton x) t) (domain_restriction f (Singleton x))) ->
  In (total_function s t) f.
  *)

(**
  property 2.5.1, BBook: section 2.5.1, p.87

  error: likely not true, one implication (right to left, see above) can not be proved
  Note/TODO: get confirmation from someone else

 Theorem total_function_domres_singleton_left: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)),
  In (total_function s t) f <->
  (forall (x:S), In s x -> In (total_function (Singleton x) t) (domain_restriction f (Singleton x))).
 *)

(**
  property 2.5.2, BBook: section 2.5.4, p.91

  This illustrates how function application should appear in proof obligations. Other than
  that it was already proved in app_trivial_property
  *)
Theorem app_trivial_property_BBook: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (x: S),
  In (partial_function s t) f /\ In (domain f) x ->
  exists H:(In (partial_function s t) f /\ In (domain f) x), In f (x, app f x H).
Proof.
intros S T s t f x H.
exists H; apply app_trivial_property.
Qed.

(**
  property 2.5.3, BBook: section 2.5.4, p.92
  *)
Theorem lambda_is_total_BBook: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (E: S -> T),
  (forall (x:S), In s x -> In t (E x)) ->
  In (total_function s t) (lambda (fun x => In s x) (fun x => E x)).
Proof.
intros S T s t E H.
replace (fun x => In s x) with s.
apply lambda_is_total; assumption.
apply Extensionality_Ensembles; split; red; intros; assumption.
Qed.

(**
  theorem 2.5.1, BBook: section 2.5.4, p.92
  *)
Theorem calculate_lambda: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (E: S -> T) (V: S),
  (forall (x:S), In s x -> In t (E x)) /\ In s V ->
  exists H:(In (partial_function s t) (lambda (fun x => In s x) (fun x => E x)) /\ In (domain (lambda (fun x => In s x) (fun x => E x))) V),
    app (lambda (fun x => In s x) (fun x => E x)) V H = (E V).
Proof.
intros S T s t E V H.
destruct H as [ H H0 ].
specialize (lambda_is_total_BBook S T s t E H); intros H1.
inversion H1 as [ f H2 H3 H4 ].
assert (In (partial_function s t) (lambda (fun x : S => In s x) (fun x : S => E x)) /\
  In (domain (lambda (fun x : S => In s x) (fun x : S => E x))) V).
rewrite H3; intuition.
exists H5.
symmetry; apply identifying_app.
constructor; [ assumption | reflexivity ].
Qed.


(**
  Theorem 2.5.2, BBook: section 2.5.4, p.93
  *)
Theorem calculate_predicative_lambda: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (P: S -> Prop) (E: S -> T) (V: S),
  (forall (x:S), In s x -> In t (E x)) /\ In s V /\ P V ->
  exists H:(In (partial_function s t) (lambda (fun x => In s x /\ P x) (fun x => E x)) /\ In (domain (lambda (fun x => In s x /\ P x) (fun x => E x))) V),
    app (lambda (fun x => In s x /\ P x) (fun x => E x)) V H = (E V).
Proof.
intros S T s t P E V H.
destruct H as [ H (H0, H1)].
assert (forall (x:S), In (fun x0 => In s x0 /\ P x0) x -> In t (E x)) as H2.
intros x H2; red in H2; intuition.
specialize (lambda_is_total_BBook S T (fun x => In s x /\ P x) t E H2); intros H3.
inversion H3 as [ f H4 H5 H6 ].
assert ((fun x => In s x /\ P x) = (fun x => In (fun x0 => In s x0 /\ P x0) x)) as H7.
apply Extensionality_Ensembles; split; red; intros; assumption.
assert (In (partial_function s t) (lambda (fun x : S => In s x /\ P x) (fun x : S => E x)) /\
  In (domain (lambda (fun x : S => In s x /\ P x) (fun x : S => E x))) V)
  as H8.
split.
rewrite <- H7 in H4.
inversion H4 as [ g H8 H9 H10 ].
constructor; [ | assumption ].
do 2 (induction H8).
do 2 constructor.
apply inclusion_transitivity with (times (fun x => In s x /\ P x) t).
assumption.
red; intros y H11.
destruct H11 as [ a b H11 H12 ]; red in H11; constructor; intuition.
rewrite H7; rewrite H5; intuition.
exists H8.
symmetry; apply identifying_app.
constructor; [ intuition | reflexivity ].
Qed.

(**
  property 2.5.4, BBook: section 2.5.4, p.93
  *)
Theorem functional_element_characterisation: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (x: S) (y: T),
  In (partial_function s t) f ->
  (In f (x,y) <-> (In (domain f) x /\ exists H:(In (partial_function s t) f /\ In (domain f) x), y = app f x H)).
Proof.
intros S T s t f x y H.
split; intros H0.

split; [ constructor; exists y; assumption | ].
assert (In (partial_function s t) f /\ In (domain f) x) as H1.
split; [ exact H | constructor; exists y; assumption ].
exists H1.
apply identifying_app; assumption.

destruct H0 as [ H0 H1 ].
destruct H1 as [ Happ H1 ].
rewrite H1; apply app_trivial_property.
Qed.

(**
  * Functions: old theorems inherited from BPhoX
  *)

(* TODO: move, remove or rename these in the relevant files *)

Theorem fun001: forall (U V: Type) (A: Ensemble U) (B:Ensemble V) (f: Ensemble (U*V)) (x: U)
  (applicable: In (partial_function A B) f /\ In (domain f) x),
  exists! y, In B y /\ y = (app f x applicable).
Proof.
intros.
exists (app f x applicable).
split.
 split; [ | reflexivity ].
 unfold app.
 apply (iota_ind V (codomain_unique_inhabitation U V A B f x applicable) (fun y0:V => f (x,y0))).
 induction applicable.
 repeat (induction H).
 intros.
 induction H2.
 generalize (H (x,b) H2).
 intros.
 inversion H4; intuition.
 induction applicable.
 induction H.
 induction H.
 compute in H.
 repeat (induction H0).
 exists x.
 apply unique_image with A B.
 constructor.
 constructor.
 exact H.
 exact H1.
 constructor; exists x; assumption.
 intuition.
 firstorder.
Qed.

Theorem fun002: forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
 Included (total_bijection A B) (total_injection A B).
Proof.
intros U V A B x H.
induction H.
constructor; intuition.
induction H0; intuition.
Qed.


Theorem fun003: forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
 Included (total_bijection A B) (total_surjection A B).
Proof.
intros U V A B x H.
induction H.
constructor; intuition.
induction H0; intuition.
Qed.

Theorem fun004: forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
 (total_bijection A B) = (Intersection (total_injection A B) (total_surjection A B)).
Proof.
intros U V A B.
apply Extensionality_Ensembles; unfold Same_set; unfold Included; split; intros x H.
induction H.
induction H0.
split.
constructor; intuition.
constructor; intuition.
induction H.
induction H.
induction H0.
constructor; [ intuition | constructor; intuition ].
Qed.

Theorem fun005: forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
 Included (total_injection A B) (total_function A B).
Proof.
intros U V A B x H.
induction H.
intuition.
Qed.

Theorem fun006: forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
 Included (total_injection A B) (partial_injection A B).
Proof.
intros U V A B x H.
induction H.
intuition.
Qed.

Theorem fun007: forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
 Included (total_surjection A B) (total_function A B).
Proof.
intros U V A B x H.
induction H.
intuition.
Qed.

Theorem fun008: forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
 Included (total_surjection A B) (partial_surjection A B).
Proof.
intros U V A B x H.
induction H.
intuition.
Qed.

Theorem fun009: forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
 Included (total_function A B) (partial_function A B).
Proof.
intros U V A B x H.
induction H.
intuition.
Qed.

Theorem fun010: forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
 Included (partial_injection A B) (partial_function A B).
Proof.
intros U V A B x H.
induction H.
intuition.
Qed.

Theorem fun011: forall (U V:Type) (A: Ensemble U) (B: Ensemble V),
 Included (partial_surjection A B) (partial_function A B).
Proof.
intros U V A B x H.
induction H.
intuition.
Qed.

Theorem fun012: forall (U V:Type) (A: Ensemble U) (B: Ensemble V) (f: Ensemble (U*V)),
 In (partial_function A B) f -> In (relation A B) f.
Proof.
intros U V A B x H.
induction H.
intuition.
Qed.

(*
claim fun013 /\A,B /\f:(partial.injective A B) (inv f) in (partial.injective B A).
claim fun014 /\A,B /\f:(total.surjective A B) (inv f) in (total.surjective B A).
claim fun015 /\A,B /\f:(total.injective A B) (inv f) in (total.injective B A).
claim fun016 /\A,B,C /\f:(partial.function A B) /\g:(partial.function B C) (partial.function A C) (f ; g).
claim fun017 /\A,B,C /\f:(partial.injective A B) /\g:(partial.injective B C) (partial.injective A C) (f ; g).
claim fun018 /\A,B,C /\f:(partial.surjective A B) /\g:(partial.surjective B C) (partial.surjective A C) (f ; g).
claim fun019 /\A,B,C /\f:(total.surjective A B) /\g:(total.function B C) (total.function A C) (f ; g).
claim fun020 /\A,B,C /\f:(total.bijective A B) /\g:(total.injective B C) (total.injective A C) (f ; g).
claim fun021 /\A,B,C /\f:(total.surjective A B) /\g:(total.surjective B C) (total.surjective A C) (f ; g).
claim fun022 /\A,B,C /\f:(total.bijective A B) /\g:(total.bijective B C) (total.bijective A C) (f ; g).
claim fun023 /\A (total.bijective A A) (id A).
claim fun024 /\A,B /\f,g:(partial.function A B) (partial.function A B) (f union g).
*)

(*
claim fun025 /\A,B,C /\f:(total.function A B) /\g:(total.function A C) ((f >< g) A = (B times C)).
claim fun026 /\A,B,C /\f:(total.function A B) /\g:(total.function A C) (prj1 ((f >< g) A) = B).
claim fun027 /\A,B,C /\f:(total.function A B) /\g:(total.function A C) (prj2 ((f >< g) A) = C).
claim fun028 /\A,B,C,D /\h:(total.function A B) /\k:(total.function C D) (((h || k) (A times C)) = (B times D)).
claim fun029 /\A,B,C,D /\h:(total.function A B) /\k:(total.function C D) (prj1 ((h || k) (A times C)) = B).
claim fun030 /\A,B,C,D /\h:(total.function A B) /\k:(total.function C D) (prj2 ((h || k) (A times C)) = D).
*)
