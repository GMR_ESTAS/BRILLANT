(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Classical_sets.
Require Import Bderived_constructs.
Require Import Brelations.
Require Import Bfunctions.

Open Scope eB_scope.

(**
  BBook: section 2.6.4, p.100, array 2, row 1
  *)
Theorem Equality_laws_dom_01: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)), (f ∈ (s → t)) ⇒(domain(f) = s).
Proof.
intros S T s t f H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
repeat (induction H).
generalize (H (a,x) H0); intros H3.
inversion H3; intuition.

induction H.
induction H1; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 2
  *)
Theorem Equality_laws_dom_02: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t)) ⇒(domain(r∼) = range(r)).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H0.
constructor; exists x; intuition.

induction H0; induction H0.
constructor; exists x; constructor; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 3
  *)
Theorem Equality_laws_dom_03: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (t ↔ u)) ⇒(domain(p;q) = p∼[domain(q)]).
Proof.
intros S T U s t u p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H0.
induction H2.
compute; apply image_intro with x0.
split; [ constructor; exists x; intuition | constructor; intuition ].

induction H0.
induction H0.
inversion H1.
induction H0.
inversion H0.
constructor; exists x.
constructor; exists a0; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 4
  *)
Theorem Equality_laws_dom_04: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (t ↔ u) ∧ range(p) ⊆ domain(q)) ⇒(domain(p;q) = domain(p)).
Proof.
intros S T U s t u p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H0.
induction H2.
constructor; exists x0; intuition.

induction H.
constructor.
induction H0.
inversion H0.
assert (In (range p) x).
constructor; exists a; intuition.
generalize (H1 x H3); intros H4.
induction H4.
inversion H4.
exists x.
constructor; exists a0; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 5
  *)
Theorem Equality_laws_dom_05: forall (S: Type) (s: Ensemble S), (domain(id(s)) = s).
Proof.
intros S s.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.

induction H.
induction H.
inversion_clear H.
inversion_clear H0.
intuition.

constructor; exists x; constructor; [ constructor; assumption | reflexivity ].
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 6
  *)
Theorem Equality_laws_dom_06: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t)) ⇒(domain(u ◁ r) = (u ∩ domain(r))).
Proof.
intros S T s t u r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
split; [ assumption | constructor; exists x; assumption ].

induction H0.
induction H1.
inversion H1.
constructor; exists x.
induction H.
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 7
  *)
Theorem Equality_laws_dom_07: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (v: Ensemble T), (r ∈ (s ↔ t) ∧ v ⊆ t) ⇒(domain(r ▷ v) = r∼[v]).
Proof.
intros S T s t r v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
compute; apply image_intro with x.
split; [ assumption | constructor; assumption ].

repeat (induction H).
induction H0.
induction H0.
inversion H2.
constructor; exists x.
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 8
  *)
Theorem Equality_laws_dom_08: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t)) ⇒(domain(u ⩤ r) = (domain(r) ∖ u)).
Proof.
intros S T s t u r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
constructor; [ constructor; exists x; assumption | assumption ].

induction H0.
induction H0.
inversion H0.
constructor; exists x.
induction H.
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 9
  *)
Theorem Equality_laws_dom_09: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (v: Ensemble T), (f ∈ (s ⇸ t) ∧ v ⊆ t) ⇒(domain(f ⩥ v) = (domain(f) ∖ (f∼[v]))).
Proof.
intros S T s t f v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
constructor.
constructor; exists x; assumption.
intros H6; apply H5.
inversion H6.
induction H7.
inversion H9.
induction H.
induction H.
assert (x=x1); [ apply H14 with a; assumption | ].
rewrite H15; assumption.

induction H0.
induction H0.
inversion H0.
constructor.
exists x.
constructor.
assumption.
intros H3; apply H1.
compute; apply image_intro with x.
split; [ assumption | constructor; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 10
  *)
Theorem Equality_laws_dom_10: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒(domain(p ⥷ q) = domain(p) ∪ domain(q)).
Proof.
intros S T s t p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
induction H3.
left; constructor; exists x; intuition.
right; constructor; exists x; assumption.

assert (~(In (domain q) x) -> In (domain p) x).
intros.
induction H0; [ assumption | contradiction ].
assert (In (domain q) x \/ ~ (In (domain q) x)); [ apply classic | ].
induction H2.
induction H2; inversion H2.
constructor; exists x; constructor; right; assumption.
assert (In (domain p) x); [ apply H1; assumption | ].
induction H3; inversion H3.
constructor; exists x; constructor; left; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 11
  *)
Theorem Equality_laws_dom_11: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (S*U)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ u)) ⇒(domain(p ⊗ q) = domain(p) ∩ domain(q)).
Proof.
intros S T U s t u p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
split; [ constructor; exists y; assumption | constructor; exists z; assumption ].

induction H0.
induction H0; induction H1.
inversion H0; inversion H1.
constructor; exists (x,x0).
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 12
  *)
Theorem Equality_laws_dom_12: forall (S T U V: Type)(s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V)(p: Ensemble (S*T)) (q: Ensemble (U*V)), (p ∈ (s ↔ t) ∧ q ∈ (u ↔ v)) ⇒(domain(p ∥ q) = domain(p) × domain(q)).
Proof.
intros S T U V s t u v p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
split; [ constructor; exists z; assumption | constructor; exists w; assumption ].

induction H0.
induction H0; induction H1.
inversion H0; inversion H1.
constructor; exists (x,x0).
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 13
  *)
Theorem Equality_laws_dom_13: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒(domain(p ∪ q) = domain(p) ∪ domain(q)).
Proof.
intros S T s t p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
left; constructor; exists x; assumption.
right; constructor; exists x; assumption.

induction H0; induction H0; inversion H0.
constructor; exists x; left; assumption.
constructor; exists x; right; assumption.
Qed.

(**
  BBook: section 2.6.4, p.100, array 2, row 14
  *)
Theorem Equality_laws_dom_14: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s ⇸ t) ∧ g ∈ (s ⇸ t) ∧ domain(f) ◁ g = domain(g) ◁ f) ⇒(domain(f ∩ g) = domain(f) ∩ domain(g)).
Proof.
intros S T s t f g H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
split; constructor; exists x; assumption.

induction H0.
induction H0; induction H1.
inversion H0; inversion H1.
assert (x = x0).
induction H.
induction H.
induction H; induction H5.
apply H6 with a.
assumption.
assert (In (domain_restriction f (domain f0)) (a,x0)).
rewrite <- H4.
constructor.
assumption.
constructor; exists x; assumption.
inversion H8.
assumption.
constructor; exists x; split; [ assumption | rewrite H4; assumption ].
Qed.

(**
  property 2.6.2, BBook: section 2.6.4, p.101, array 1, row 1
  *)
Theorem Equality_laws_dom_15: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s ⇸ t) ∧ g ∈ (s ⇸ t) ∧ domain(f) ◁ g = domain(g) ◁ f) ⇒(domain(f ∖ g) = domain(f) ∖ domain(g)).
Proof.
intros S T s t f g H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
constructor.
constructor; exists x; assumption.
intros H4; apply H3.
assert (In (domain_restriction g (domain f)) (a,x)).
induction H.
rewrite H5.
constructor; assumption.
induction H5.
assumption.

induction H0.
induction H0.
inversion H0.
constructor.
exists x.
constructor.
assumption.
intros H3; apply H1.
constructor; exists x; assumption.
Qed.

(**
  BBook: section 2.6.4, p.101, array 1, row 2
  *)
Theorem Equality_laws_dom_16: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (x: S) (y: T), (x ∈ s ∧ y ∈ t) ⇒(domain((Singleton (x ↦ y))) = (Singleton (x))).
Proof.
intros S T s t x y H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros a H0.

induction H0.
inversion H0.
inversion H1.
intuition.

constructor.
exists y.
induction H0.
intuition.
Qed.

(**
  BBook: section 2.6.4, p.101, array 1, row 3
  *)
Theorem Equality_laws_dom_17: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ∧ r = ∅) ⇒(domain(r) = ∅).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0; inversion H0.
induction H.
assert (In (Empty_set _) (a,x)).
rewrite <- H2; assumption.
contradiction.

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.101, array 1, row 4
  *)
Theorem Equality_laws_dom_18: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), (t <> ∅) ⇒(domain(s × t) = s).
Proof.
intros S T s t H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0; inversion H0.
inversion H1; assumption.

constructor.
generalize (not_empty_Inhabited T t H); intros H1.
induction H1.
exists x0; constructor; assumption.
Qed.

Close Scope eB_scope.
