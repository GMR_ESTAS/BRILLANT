(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

(* Library about definitions for sequences *)

Require Import Bchapter2.

(* TODO: decide here to use the Lists of Coq and add a coercion to partial injection of N to whatever
the elements of the list are, or redefine the sequences as per the BBook *)

(* Import list. *)

(* seq = List *)

(* [] = nil *)

(* -> = :: *)

(* iseq *)

(* perm *)

(* interval *)

(* size *)

(* concatenation = @ *)

(* appending = gets *)

(*
Cst Infix[4.6] l "gets" e : list['a] -> 'a -> list['a].

claim insert.nil /\D /\x:D nil gets x  = x :: nil.
claim insert.cons /\D /\x,y:D /\l:(List D) x::l gets y = x :: (l gets y).
*)

(* revesing = rev *)

(*
Cst Prefix[4.6] "rev" l : list['a] -> list['a].

claim rev.nil rev nil = nil.
claim rev.cons /\D /\x:D /\l:(List D) rev x::l = (rev l) gets x.
*)

(* generalized concatenation *)

(* sum *)

(* prod *)

(* keep *)

(* cut *)

(* first = head *)

(* last *)

(* tail = tail *)

(* front *)
