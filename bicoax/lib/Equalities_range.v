(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Classical_sets.
Require Import Bderived_constructs.
Require Import Brelations.
Require Import Bfunctions.

Open Scope eB_scope.

(**
  BBook: section 2.6.4, p.102, array 1, row 1
  *)
Theorem Equality_laws_ran_01: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)), (f ∈ (s ⤀ t)) ⇒(range(f) = t).
Proof.
intros S T s t f H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
repeat (induction H).
generalize (H (x,b) H0); intros H3.
inversion H3; intuition.

induction H.
constructor; apply H1; assumption.
Qed.

(**
  BBook: section 2.6.4, p.102, array 1, row 2
  *)
Theorem Equality_laws_ran_02: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t)) ⇒(range(r∼) = domain(r)).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H0.
constructor; exists x; intuition.

induction H0; induction H0.
constructor; exists x; constructor; intuition.
Qed.

(**
  BBook: section 2.6.4, p.102, array 1, row 3
  *)
Theorem Equality_laws_ran_03: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (t ↔ u)) ⇒(range(p;q) = q[range(p)]).
Proof.
intros S T U s t u p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H0.
induction H2.
compute; apply image_intro with x0.
split; [ constructor; exists x; intuition | intuition ].

induction H0.
induction H0.
inversion H0.
induction H2.
constructor; exists x0.
constructor; exists x; intuition.
Qed.

(**
  BBook: section 2.6.4, p.102, array 1, row 4
  *)
Theorem Equality_laws_ran_04: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (t ↔ u) ∧ domain(q) ⊆ range(p)) ⇒(range(p;q) = range(q)).
Proof.
intros S T U s t u p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H0.
induction H2.
constructor; exists x0; intuition.

induction H.
constructor.
induction H0.
inversion H0.
assert (In (domain q) x).
constructor; exists b; intuition.
generalize (H1 x H3); intros H4.
induction H4.
inversion H4.
exists x.
constructor; exists b0; intuition.
Qed.

(**
  BBook: section 2.6.4, p.102, array 1, row 5
  *)
Theorem Equality_laws_ran_05: forall (S: Type) (s: Ensemble S), (range(id(s)) = s).
Proof.
intros S s.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.

induction H.
induction H.
inversion_clear H.
inversion_clear H0.
intuition.

constructor; exists x; constructor; [ constructor; assumption | reflexivity ].
Qed.

(**
  BBook: section 2.6.4, p.102, array 1, row 6
  *)
Theorem Equality_laws_ran_06: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t)) ⇒(range(u ◁ r) = r[u]).
Proof.
intros S T s t u r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
compute; apply image_intro with x.
split; assumption.

induction H0.
induction H0.
constructor.
exists x.
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.103, array 1, row 1
  *)
Theorem Equality_laws_ran_07: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (v: Ensemble T), (r ∈ (s ↔ t) ∧ v ⊆ t) ⇒(range(r ▷ v) = range(r) ∩ v).
Proof.
intros S T s t r v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
split; [ constructor; exists x; assumption | assumption ].

induction H0.
induction H0.
inversion H0.
constructor; exists x.
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.103, array 1, row 2
  *)
Theorem Equality_laws_ran_08: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ r∼ ∈ (t ⇸ s)) ⇒(range(u ⩤ r) = range(r) ∖ (r[u])).
Proof.
intros S T s t u r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
constructor.
constructor; exists x; assumption.
intros H6; apply H5.
inversion H6.
assert (x1 = x).
induction H.
inversion H9.
apply H11 with b.
constructor; intuition.
constructor; intuition.
rewrite <- H9; intuition.

induction H0.
induction H0.
inversion H0.
constructor; exists x.
constructor.
assumption.
intros H3; apply H1.
compute; apply image_intro with x; intuition.
Qed.

(**
  BBook: section 2.6.4, p.103, array 1, row 3
  *)
Theorem Equality_laws_ran_09: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (v: Ensemble T), (r ∈ (s ↔ t) ∧ v ⊆ t) ⇒(range(r ⩥ v) = range(r) ∖ v).
Proof.
intros S T s t r v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
constructor; [ constructor; exists x; assumption | assumption ].

induction H0.
induction H0.
inversion H0.
constructor; exists x.
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.103, array 1, row 4
  *)
Theorem Equality_laws_ran_10: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒(range(p ⥷ q) = range(domain(q) ⩤ p) ∪ range(q)).
Proof.
intros S T s t p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
induction H3.
left; constructor; exists x; constructor; intuition.
right; constructor; exists x; assumption.

assert (~(In (range q) x) -> In (range (domain_subtraction p (domain q))) x).
intros.
induction H0; [ assumption | contradiction ].
assert (In (range q) x \/ ~ (In (range q) x)); [ apply classic | ].
induction H2.
induction H2; inversion H2.
constructor; exists x; constructor; right; assumption.
assert (In (range (domain_subtraction p (domain q))) x); [ apply H1; assumption | ].
induction H3; inversion H3.
inversion H4.
constructor; exists x; constructor; left; intuition.
Qed.

(**
  BBook: section 2.6.4, p.103, array 1, row 5
  *)
Theorem Equality_laws_ran_11: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (S*U)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ u)) ⇒(range(p ⊗ q) = p∼ ; q).
Proof.
intros S T U s t u p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
constructor; exists x.
split; [ constructor; assumption | assumption ].

induction H0.
inversion H0.
induction H1.
inversion H1.
constructor; exists x.
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.103, array 1, row 6
  *)
Theorem Equality_laws_ran_12: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (p: Ensemble (S*T)) (q: Ensemble (U*V)), (p ∈ (s ↔ t) ∧ q ∈ (u ↔ v)) ⇒(range(p ∥ q) = range(p) × range(q)).
Proof.
intros S T U V s t u v p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
split; [ constructor; exists x0; assumption | constructor; exists y; assumption ].

induction H0.
induction H0; induction H1.
inversion H0; inversion H1.
constructor; exists (x,x0).
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.103, array 1, row 7
  *)
Theorem Equality_laws_ran_13: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒(range(p ∪ q) = range(p) ∪ range(q)).
Proof.
intros S T s t p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
left; constructor; exists x; assumption.
right; constructor; exists x; assumption.

induction H0; induction H0; inversion H0.
constructor; exists x; left; assumption.
constructor; exists x; right; assumption.
Qed.

(**
  BBook: section 2.6.4, p.103, array 1, row 8
  *)
Theorem Equality_laws_ran_14: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p∼ ∈ (t ⇸ s) ∧ q∼ ∈ (t ⇸ s) ∧ q ▷ range(p) = p ▷ range(q)) ⇒(range(p ∩ q) = range(p) ∩ range(q)).
Proof.
intros S T s t p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
split; constructor; exists x; assumption.

induction H0.
induction H0; induction H1.
inversion H0; inversion H1.
assert (x = x0).
induction H.
induction H.
inversion H; inversion H5.
apply H7 with b.
constructor; assumption.
assert (In (range_restriction p (range q)) (x0,b)).
rewrite <- H4.
constructor.
assumption.
constructor; exists x; assumption.
inversion H12.
constructor; assumption.
constructor; exists x; split; [ assumption | rewrite H4; assumption ].
Qed.

(**
  property 2.6.3, BBook: section 2.6.4, p.103, array 1, row 9
  *)
Theorem Equality_laws_ran_15: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p∼ ∈ (t ⇸ s) ∧ q∼ ∈ (t ⇸ s) ∧ q ▷ range(p) = p ▷ range(q)) ⇒(range(p ∖ q) = range(p) ∖ range(q)).
Proof.
intros S T s t f g H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
constructor.
constructor; exists x; assumption.
intros H4; apply H3.
assert (In (range_restriction g (range f)) (x,b)).
induction H.
rewrite H5.
constructor; assumption.
induction H5.
assumption.

induction H0.
induction H0.
inversion H0.
constructor.
exists x.
constructor.
assumption.
intros H3; apply H1.
constructor; exists x; assumption.
Qed.

(**
  BBook: section 2.6.4, p.103, array 1, row 10
  *)
Theorem Equality_laws_ran_16: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (x: S) (y: T), (x ∈ s ∧ y ∈ t) ⇒(range((Singleton (x ↦ y))) = (Singleton (y))).
Proof.
intros S T s t x y H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros a H0.

induction H0.
inversion H0.
inversion H1.
intuition.

constructor.
exists x.
induction H0.
intuition.
Qed.

(**
  BBook: section 2.6.4, p.103, array 1, row 11
  *)
Theorem Equality_laws_ran_17: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ∧ r = ∅) ⇒(range(r) = ∅).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0; inversion H0.
induction H.
assert (In (Empty_set _) (x,b)).
rewrite <- H2; assumption.
contradiction.

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.103, array 1, row 12
  *)
Theorem Equality_laws_ran_18: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), (s <> ∅) ⇒(range(s × t) = t).
Proof.
intros S T s t H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0; inversion H0.
inversion H1; assumption.

constructor.
generalize (not_empty_Inhabited S s H); intros H1.
induction H1.
exists x0; constructor; assumption.
Qed.


Close Scope eB_scope.
