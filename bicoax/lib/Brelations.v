(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bbasic.
Require Import Bderived_constructs.


(**
  * Binary relations constructs: first series definitions
  *)

(**
  Here we depart from Coq's definitions in Relations_1, because it is
  not general enough: in Relations_1, Relation is defined upon two
  arguments of the same datatype, while in general it is not the case
  (see {0 -> true, 1 -> false, 2 -> true} for instance.
  *)

(**
  TODO: definitions of injective and surjective relations here, for eventB
  compatibility ?
  *)

(**
  relation, BBook: section 2.4.1, p.78, array 1, row 1

  We *must* keep the form "one argument -> Prop" because in B, Relations are sets too
  *)
Inductive relation (U V: Type) (u: Ensemble U) (v: Ensemble V): Ensemble (Ensemble (U*V)) :=
  relation_intro: forall (r: Ensemble (U*V)), In (Power_set (times u v)) r -> relation U V u v r.
Implicit Arguments relation [U V].
Notation "x ↔ y" := (relation x y) (at level 66, right associativity): eB_scope.

(**
  inverse, BBook: section 2.4.1, p.78, array 1, row 2
  *)
Inductive inverse (U V: Type) (p: Ensemble (U*V)): Ensemble (V*U) :=
  inverse_intro: forall (a: U) (b: V), In p (a,b) -> inverse U V p (b,a).
Implicit Arguments inverse [U V].
Notation "x ∼" := (inverse x) (at level 16, no associativity): eB_scope.

(**
  domain, BBook: section 2.4.1, p.78, array 1, row 3
  *)
Inductive domain (U V: Type) (p: Ensemble (U*V)): Ensemble U :=
  domain_intro: forall (a: U), (exists b:V, In p (a,b)) -> domain U V p a.
Implicit Arguments domain [U V].

(**
  range, BBook: section 2.4.1, p.78, array 1, row 4
  *)
Inductive range (U V: Type) (p: Ensemble (U*V)): Ensemble V :=
  range_intro: forall (b: V), (exists a:U, In p (a,b)) -> range U V p b.
Implicit Arguments range [U V].

(**
  total relation, Rodin D7: section 5.6, p.94(19), array 1, row 4
  *)
Inductive total_relation (U V: Type) (u: Ensemble U) (v: Ensemble V): Ensemble (Ensemble (U*V)) :=
  total_rel_intro: forall (r: Ensemble (U*V)),
    In (relation u v) r ->
    (forall (x: U), In u x -> (exists y, In r (x,y))) ->
    total_relation U V u v r.
Implicit Arguments total_relation [U V].
Notation "x ⇤ y" := (total_relation x y) (at level 66, right associativity): eB_scope.

(**
  surjective relation, Rodin D7: section 5.6, p.94(19), array 1, row 5
  *)
Inductive surjective_relation (U V: Type) (u: Ensemble U) (v: Ensemble V): Ensemble (Ensemble (U*V)) :=
  surjective_rel_intro: forall (r: Ensemble (U*V)),
    In (relation u v) r ->
    (forall (y: V), In v y -> (exists x, In r (x,y))) ->
    surjective_relation U V u v r.
Implicit Arguments surjective_relation [U V].
Notation "x ⇥ y" := (surjective_relation x y) (at level 66, right associativity): eB_scope.

(**
  total surjective relation, Rodin D7: section 5.6, p.94(19), array 1, row 6
  *)
Inductive total_surjective_relation (U V: Type) (u: Ensemble U) (v: Ensemble V): Ensemble (Ensemble (U*V)) :=
  total_surj_rel_intro: forall (r: Ensemble (U*V)),
    In (total_relation u v) r ->
    In (surjective_relation u v) r ->
    total_surjective_relation U V u v r.
Implicit Arguments total_surjective_relation [U V].
Notation "x ↹ y" := (total_surjective_relation x y) (at level 66, right associativity): eB_scope.

(**
  composition, BBook: section 2.4.1, p.78, array 1, row 5-6
  *)
Inductive composition (U V W: Type) (p: Ensemble (U * V)) (q: Ensemble (V * W)): Ensemble (U*W) :=
  composition_intro: forall (a: U) (c: W), (exists b:V, In p (a,b) /\ In q (b,c)) -> composition U V W p q (a,c).
Implicit Arguments composition [U V W].
Notation "x ; y" := (composition x y) (at level 51, right associativity): eB_scope.
(**
  This notation is now reserved by Coq (as of 8.2rc2 in Coq.Program.Basics), for
  composition also. Hence we use the reverse semicolon, which should make sense
  for someone with a "classical B" background. We keep the priority and the associativity
  of the corresponding event-B symbol.

  Notation "x ∘ y" := (composition y x) (at level 61, right associativity): eB_scope.
  *)
Notation "x ⁏ y" := (composition y x) (at level 61, right associativity): eB_scope.

(**
  identity, BBook: section 2.4.1, p.78, array 1, row 7
  *)
Inductive id (U: Type) (u: Ensemble U): Ensemble (prod U U) :=
  id_intro: forall (a b: U), In (times u u) (a,b) -> a=b -> id U u (pair a b).
Implicit Arguments id [U].

(**
  domain restriction, BBook: section 2.4.1, p.78, array 1, row 8
  *)
Inductive domain_restriction (U V: Type) (r: Ensemble (U * V)) (u: Ensemble U) : Ensemble (U*V) :=
  domain_restriction_intro: forall (x: U) (y: V), In r (x,y) -> In u x -> domain_restriction U V r u (x,y).
Implicit Arguments domain_restriction [U V].
Notation "x ◁ y" := (domain_restriction y x) (at level 56, right associativity): eB_scope.

(**
  range restriction, BBook: section 2.4.1, p.78, array 1, row 9
  *)
Inductive range_restriction (U V: Type) (r: Ensemble (U * V)) (v: Ensemble V) : Ensemble (U*V) :=
  range_restriction_intro: forall (x: U) (y: V), In r (x,y) -> In v y -> range_restriction U V r v (x,y).
Implicit Arguments range_restriction [U V].
Notation "x ▷ y" := (range_restriction x y) (at level 46, right associativity): eB_scope.

(**
  domain subtraction, BBook: section 2.4.1, p.78, array 1, row 10
  *)
Inductive domain_subtraction (U V: Type) (r: Ensemble (U * V)) (u: Ensemble U) : Ensemble (U*V) :=
  domain_subtraction_intro: forall (x: U) (y: V), In r (x,y) -> NotIn u x -> domain_subtraction U V r u (x,y).
Implicit Arguments domain_subtraction [U V].
Notation "x ⩤ y" := (domain_subtraction y x) (at level 56, right associativity): eB_scope.

(**
  range subtraction, BBook: section 2.4.1, p.78, array 1, row 11
  *)
Inductive range_subtraction (U V: Type) (r: Ensemble (U * V)) (v: Ensemble V) : Ensemble (U*V) :=
  range_subtraction_intro: forall (x: U) (y: V), In r (x,y) -> NotIn v y -> range_subtraction U V r v (x,y).
Implicit Arguments range_subtraction [U V].
Notation "x ⩥ y" := (range_subtraction x y) (at level 46, right associativity): eB_scope.


(**
  * Binary relations: second series definitions
  *)

(**
  image, BBook: section 2.4.2, p.80, array 1, row 1
  *)
Inductive image (U V: Type) (r: Ensemble (U * V)) (w: Ensemble U) : Ensemble V :=
  image_intro: forall (x: U) (y: V), In w x /\ In r (x,y) -> image U V r w y.
Implicit Arguments image [U V].
Notation "x [ y ]" := (image x y) (at level 21, right associativity): eB_scope.

(**
  overriding, BBook: section 2.4.2, p.80, array 1, row 2

  Small typo in the BBook.
  *)
Inductive override (U V: Type) (q: Ensemble (U*V)) (r: Ensemble (U*V)) : Ensemble (U*V) :=
  override_intro: forall (x: U) (y: V), (In q (x,y) /\ NotIn (domain r) x) \/ In r (x,y) -> override U V q r (x,y).
Implicit Arguments override [U V].
Notation "x ⥷ y" := (override x y) (at level 61, right associativity): eB_scope.

(**
  direct product, BBook: section 2.4.2, p.80, array 1, row 3
  *)
Inductive direct_product (S U V: Type) (f: Ensemble (S*U)) (g: Ensemble (S*V)): Ensemble (S*(U*V)) :=
  direct_product_intro: forall (x:S) (y: U) (z: V), In f (x,y) -> In g (x,z) -> direct_product S U V f g (x,(y,z)).
Implicit Arguments direct_product [S U V].
Notation "x ⊗ y" := (direct_product x y) (at level 51, right associativity): eB_scope.

(**
  left projection, BBook: section 2.4.2, p.80, array 1, row 4
  *)
Inductive prj1 (S T: Type) (C: (Ensemble S)*(Ensemble T)): Ensemble ((S*T)*S) :=
  prj1_intro: forall (x: S) (y: T) (z: S), In (times (times (fst C) (snd C)) (fst C)) ((x,y),z) -> z=x -> prj1 S T C ((x,y),z).
Implicit Arguments prj1 [S T].

(**
  right projection, BBook: section 2.4.2, p.80, array 1, row 5
  *)
Inductive prj2 (S T: Type) (C: (Ensemble S)*(Ensemble T)): Ensemble ((S*T)*T) :=
  prj2_intro: forall (x: S) (y: T) (z: T), In (times (times (fst C) (snd C)) (snd C)) ((x,y),z) -> z=y -> prj2 S T C ((x,y),z).
Implicit Arguments prj2 [S T].

(**
  parallel product, BBook: section 2.4.2, p.80, array 1, row 6
  *)
Inductive parallel_product (S T U V: Type) (h: Ensemble (S*U)) (k: Ensemble (T*V)): Ensemble ((S*T)*(U*V)) :=
  parallel_product_intro: forall (x: S) (y: T) (z: U) (w: V), In h (x,z) -> In k (y,w) -> parallel_product S T U V h k ((x,y),(z,w)).
Implicit Arguments parallel_product [S T U V].
Notation "x ∥ y" := (parallel_product x y) (at level 61, right associativity): eB_scope.


(**
  * Binary relations: first series validation
  *)

(**
  relation, BBook: section 2.4.1, p.78, array 1, row 1
  *)
Theorem valid_relation: forall (U V: Type) (u: Ensemble U) (v: Ensemble V), (relation u v) = Power_set (times u v).
Proof.
intros U V u v.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1; intuition.
constructor; assumption.
Qed.

(**
  inverse, BBook: section 2.4.1, p.78, array 1, row 2
  *)
Theorem valid_inverse: forall (U V: Type) (u: Ensemble U) (v: Ensemble V) (p: Ensemble (U*V)), In (relation u v) p -> (inverse p) = (Comprehension (fun C => match C with | (b,a) => (In (times v u) (b,a) /\ In p (a,b)) end)).
Proof.
intros U V u v p H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction x; induction H.
inversion H1 as [ a0 b0 H2 H0].
compute in H.
induction H.
generalize (H (b,a) H3); intros H4.
split.
inversion H4; constructor; intuition.
assumption.
induction x; induction H1; constructor; intuition.
Qed.

(**
  domain, BBook: section 2.4.1, p.78, array 1, row 3
  *)
Theorem valid_domain: forall (U V: Type) (u: Ensemble U) (v: Ensemble V) (p: Ensemble (U*V)), In (relation u v) p -> domain p = (Comprehension (fun a:U => In u a /\ exists b, (In v b /\ In p (a,b)))).
Proof.
intros U V u v p H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
repeat (induction H).
repeat (induction H0).
split.
generalize (H (a,x) H0); intros H1.
inversion H1; intuition.
exists x.
split; [ | assumption ].
generalize (H (a,x) H0); intros H1.
inversion H1; intuition.
repeat (induction H).
repeat (induction H0).
induction H1.
constructor.
exists x0; intuition.
Qed.

(**
  range, BBook: section 2.4.1, p.78, array 1, row 4
  *)
Theorem valid_range:forall (U V: Type) (u: Ensemble U) (v: Ensemble V) (p: Ensemble (U*V)), In (relation u v) p -> range p = domain (inverse p).
Proof.
intros U V u v p H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
repeat (induction H).
repeat (induction H0).
constructor.
exists x.
constructor.
assumption.
repeat (induction H).
inversion H0.
inversion H1.
inversion H3.
constructor.
exists x0.
intuition.
Qed.

(**
  total relation, Rodin D7: section 5.6, p.94(19), array 1, row 4
  *)
Theorem valid_total_relation:  forall (U V: Type) (u: Ensemble U) (v: Ensemble V) (r: Ensemble (U*V)),
  In (total_relation u v) r <-> In (relation u v) r /\ domain r = u.
Proof.
intros U V u v r.
split; intros.
induction H as [ r H H0 ].
split; [ intuition | ].
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
destruct H1 as [ x H1 ].
destruct H1 as [ b H1 ].
do 2 (induction H); specialize (H (x,b) H1); inversion H; assumption.
specialize (H0 x H1); constructor; assumption.
constructor; [ intuition | ].
destruct H as [ H H0 ].
rewrite <- H0; intros x H1; destruct H1; assumption.
Qed.

(**
  surjective relation, Rodin D7: section 5.6, p.94(19), array 1, row 5
  *)
Theorem valid_surjective_relation:  forall (U V: Type) (u: Ensemble U) (v: Ensemble V) (r: Ensemble (U*V)),
  In (surjective_relation u v) r <-> In (relation u v) r /\ range r = v.
Proof.
intros U V u v r.
split; intros.
induction H as [ r H H0 ].
split; [ intuition | ].
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
destruct H1 as [ x H1 ].
destruct H1 as [ a H1 ].
do 2 (induction H); specialize (H (a,x) H1); inversion H; assumption.
specialize (H0 x H1); constructor; assumption.
constructor; [ intuition | ].
destruct H as [ H H0 ].
rewrite <- H0; intros x H1; destruct H1; assumption.
Qed.

(**
  total surjective relation, Rodin D7: section 5.6, p.94(19), array 1, row 6
  *)
Theorem valid_total_surjective_relation:  forall (U V: Type) (u: Ensemble U) (v: Ensemble V) (r: Ensemble (U*V)),
  In (total_surjective_relation u v) r <-> In (total_relation u v) r /\ In (surjective_relation u v) r.
Proof.
intros U V u v r.
split.
intros H; destruct H; intuition.
intros; constructor; intuition.
Qed.

(**
  composition, BBook: section 2.4.1, p.78, array 1, row 5-6
  *)
Theorem valid_composition: forall (U V W: Type) (u: Ensemble U) (v: Ensemble V) (w: Ensemble W) (p: Ensemble (U*V)) (q: Ensemble (V*W)), In (relation u v) p -> In (relation v w) q -> composition p q = (Comprehension (fun C => match C with | (a,c) => In (times u w) (a,c) /\ (exists b, In v b /\ In p (a,b) /\ In q (b,c)) end )).
Proof.
intros U V W u v w p q H H0.
repeat (induction H).
repeat (induction H0).
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
repeat (induction H1).
split.
generalize (H (a,x) H1); generalize (H0 (x,c) H2).
intros H3 H4; inversion H3; inversion H4.
constructor; intuition.
exists x; split; [ | intuition].
generalize (H0 (x,c) H2); intros H3; inversion H3; intuition.
induction x.
induction H1 as [H2].
inversion H1.
constructor.
exists x; intuition.
Qed.

(**
  identity, BBook: section 2.4.1, p.78, array 1, row 7
  *)
Theorem valid_id: forall (U: Type) (u: Ensemble U), id u = (Comprehension (fun C => match C with | (a,b) => In (times u u) (a,b) /\ a=b end)).
Proof.
intros U u.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
repeat (induction H1).
split; intuition.
induction x.
induction H1.
constructor; intuition.
Qed.


(**
  domain restriction, BBook: section 2.4.1, p.78, array 1, row 8
  *)
Theorem valid_domain_restriction: forall (U V: Type) (r: Ensemble (U*V)) (s u: Ensemble U) (t: Ensemble V), In (relation s t) r -> Included u s -> domain_restriction r u = composition (id u) r.
Proof.
intros U V r s u t H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction x; induction H1.
constructor.
exists x; split.
constructor; constructor; intuition.
assumption.
induction H1.
inversion H1.
elim H2; intros H3 H4.
replace a with x.
constructor.
intuition.
inversion H3.
inversion H7.
intuition.
inversion H3; intuition.
Qed.

(**
  range restriction, BBook: section 2.4.1, p.78, array 1, row 9
  *)
Theorem valid_range_restriction: forall (U V: Type) (r: Ensemble (U*V))(s: Ensemble U) (t v: Ensemble V), In (relation s t) r -> Included v t -> range_restriction r v = composition r (id v).
Proof.
intros U V r s u t H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction x; induction H1.
constructor.
exists y; split.
assumption.
constructor; constructor; intuition.
induction H1.
inversion H1.
elim H2; intros H3 H4.
replace c with x.
constructor.
intuition.
inversion H4.
inversion H7.
intuition.
inversion H4; intuition.
Qed.

(**
  domain subtraction, BBook: section 2.4.1, p.78, array 1, row 10
  *)
Theorem valid_domain_subtraction: forall (U V: Type) (r: Ensemble (U*V))(s u: Ensemble U) (t: Ensemble V), In (relation s t) r -> Included u s -> domain_subtraction r u = domain_restriction r (Setminus (domain r) u).
Proof.
intros U V r s u t H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction x; induction H1.
constructor.
assumption.
constructor.
constructor.
exists y; assumption.
intuition.
induction H1.
inversion H2.
constructor.
assumption.
intuition.
Qed.

(**
  range subtraction, BBook: section 2.4.1, p.78, array 1, row 11
  *)
Theorem valid_range_subtraction: forall (U V: Type) (r: Ensemble (U*V))(s: Ensemble U) (t v: Ensemble V), In (relation s t) r -> Included v t -> range_subtraction r v = range_restriction r (Setminus (range r) v).
Proof.
intros U V r s u t H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction x; induction H1.
constructor.
assumption.
constructor.
constructor.
exists x; assumption.
intuition.
induction H1.
inversion H2.
constructor.
assumption.
intuition.
Qed.

(**
  range, BBook: section 2.4.1, p.78, array 2, row 1
  *)
Theorem valid_range2:forall (U V: Type) (u: Ensemble U) (v: Ensemble V) (p: Ensemble (U*V)), In (relation u v) p -> range p = (Comprehension (fun b:V => In v b /\ exists a, (In u a /\ In p (a,b)))).
Proof.
intros U V u v p H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
repeat (induction H).
repeat (induction H0).
split.
generalize (H (x,b) H0); intros H1.
inversion H1; intuition.
exists x.
split; [ | assumption ].
generalize (H (x,b) H0); intros H1.
inversion H1; intuition.
repeat (induction H).
repeat (induction H0).
induction H1.
constructor.
exists x0; intuition.
Qed.


(**
  domain restriction, BBook: section 2.4.1, p.78, array 2, row 2
  *)
Theorem valid_domain_restriction2: forall (U V: Type) (r: Ensemble (U*V)) (s u: Ensemble U) (t: Ensemble V), In (relation s t) r -> Included u s -> domain_restriction r u = (Comprehension (fun C:U*V => match C with | (a,b) => In r (a,b) /\ In u a end)).
Proof.
intros U V r s u t H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction x.
induction H1.
split; intuition.
induction x; induction H1.
constructor; intuition.
Qed.

(**
  range restriction, BBook: section 2.4.1, p.78, array 2, row 3
  *)
Theorem valid_range_restriction2: forall (U V: Type) (r: Ensemble (U*V))(s: Ensemble U) (t v: Ensemble V), In (relation s t) r -> Included v t -> range_restriction r v = (Comprehension (fun C:U*V => match C with | (a,b) => In r (a,b) /\ In v b end)).
Proof.
intros U V r s u t H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction x.
induction H1.
split; intuition.
induction x; induction H1.
constructor; intuition.
Qed.

(**
  domain subtraction, BBook: section 2.4.1, p.78, array 2, row 4
  *)
Theorem valid_domain_subtraction2: forall (U V: Type) (r: Ensemble (U*V))(s u: Ensemble U) (t: Ensemble V), In (relation s t) r -> Included u s -> domain_subtraction r u = (Comprehension (fun C:U*V => match C with | (a,b) => In r (a,b) /\ NotIn u a end)).
Proof.
intros U V r s u t H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction x.
induction H1.
split; intuition.
induction x; induction H1.
constructor; intuition.
Qed.

(**
  range subtraction, BBook: section 2.4.1, p.79, array 1, row 1
  *)
Theorem valid_range_subtraction2: forall (U V: Type) (r: Ensemble (U*V))(s: Ensemble U) (t v: Ensemble V), In (relation s t) r -> Included v t -> range_subtraction r v = (Comprehension (fun C:U*V => match C with | (a,b) => In r (a,b) /\ NotIn v b end)).
Proof.
intros U V r s u t H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction x.
induction H1.
split; intuition.
induction x; induction H1.
constructor; intuition.
Qed.

(**
  * Binary relations: second series validation
  *)

(**
  image, BBook: section 2.4.2, p.80, array 1, row 1
  *)
Theorem valid_image: forall (S T: Type) (s w: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), In (relation s t) r -> Included w s -> image r w = range (domain_restriction r w).
Proof.
intros S T s w t r H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
constructor.
exists x.
constructor; intuition.
inversion H1.
inversion H2.
inversion H4.
constructor 1 with (x0); intuition.
Qed.


(**
  overriding, BBook: section 2.4.2, p.80, array 1, row 2

  error: in the BBook, replace p with r or replace all r with p
  *)
Theorem valid_override: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (q r: Ensemble (S*T)), In (relation s t) q -> In (relation s t) r -> override q r = (Union (domain_subtraction q (domain r)) r).
Proof.
intros S T s t q r H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
elim H1; intros H2; [ left | right ].
constructor; intuition.
intuition.
induction H1.
induction H1.
constructor; left; intuition.
induction x.
constructor; right; intuition.
Qed.

(**
  direct product, BBook: section 2.4.2, p.80, array 1, row 3
  *)
Theorem valid_direct_product: forall (S U V: Type) (s: Ensemble S) (u: Ensemble U) (v: Ensemble V) (f: Ensemble (S*U)) (g: Ensemble (S*V)), In (relation s u) f -> In (relation s v) g -> direct_product f g = (Comprehension (fun C => match C with | (x,(y,z)) => In (times s (times u v)) (x,(y,z)) /\ In f (x,y) /\ In g (x,z) end)).
Proof.
intros S U V s u v f g  H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
constructor.
repeat (induction H); repeat (induction H0).
generalize (H (x,y) H1); generalize (H0 (x,z) H2); intros H3 H4.
inversion H3; inversion H4.
constructor; intuition.
constructor; intuition.
intuition.
induction x.
induction b.
induction H1.
constructor; intuition.
Qed.

(**
  left projection, BBook: section 2.4.2, p.80, array 1, row 4
  *)
Theorem valid_prj1: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), prj1(s,t) = (inverse (direct_product (id s) (times s t))).
Proof.
intros S T s t .
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
inversion H.
inversion H3.
constructor.
constructor.
constructor.
constructor; intuition.
intuition.
constructor; intuition.
induction x.
induction a.
inversion H1.
inversion H0.
constructor.
constructor.
constructor.
inversion H5.
inversion H10.
intuition.
inversion H7.
intuition.
inversion H5.
inversion H10.
intuition.
inversion H5.
intuition.
Qed.

(**
  right projection, BBook: section 2.4.2, p.80, array 1, row 5
  *)
Theorem valid_prj2: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), prj2(s,t) = (inverse (direct_product (times t s) (id t))).
Proof.
intros S T s t .
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
inversion H.
inversion H3.
constructor.
constructor.
constructor.
intuition.
intuition.
constructor.
constructor; intuition.
intuition.
induction x.
induction a.
inversion H1.
inversion H0.
constructor.
constructor.
constructor.
inversion H5.
intuition.
inversion H7.
inversion H10.
intuition.
inversion H5.
intuition.
inversion H7.
intuition.
Qed.

(**
  parallel product, BBook: section 2.4.2, p.80, array 1, row 6
  *)
Theorem valid_parallel_product: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (h: Ensemble (S*U)) (k: Ensemble (T*V)), In (relation s u) h -> In (relation t v) k -> parallel_product h k = (direct_product (composition (prj1(s,t)) h) (composition (prj2(s,t)) k)).
Proof.
intros S T U V s t u v h k H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
constructor.
constructor.
exists x.
split; [ | intuition ].
constructor.
repeat (induction H).
repeat (induction H0).
generalize (H (x,z) H1).
generalize (H0 (y,w) H2).
intros H3 H4.
inversion H3.
inversion H4.
repeat constructor; intuition.
reflexivity.
constructor.
exists y.
repeat (induction H).
repeat (induction H0).
generalize (H (x,z) H1).
generalize (H0 (y,w) H2).
intros H3 H4.
inversion H3.
inversion H4.
repeat constructor; intuition.
induction x.
induction a; induction b.
induction H1.
induction x.
inversion H1.
inversion H2.
inversion H4.
inversion H7.
constructor.
replace a1 with x; [ intuition | ].
elim H9; intros H11.
inversion H11; intuition.
replace b1 with x0; [ intuition | ].
elim H10; intros H11.
inversion H11; intuition.
Qed.

(**
  image, BBook: section 2.4.2, p.80, array 2, row 1

  error: in the BBook, replace x with a and y with b, or a with x and b with y (as below)
  *)
Theorem valid_image2: forall (S T: Type) (s w: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), In (relation s t) r -> Included w s -> image r w = (Comprehension (fun y:T => In t y /\ exists x, In w x /\ In r (x,y))).
Proof.
intros S T s w t r H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
constructor.
repeat (induction H).
generalize (H (x,y) (proj2 H1)); intros H2; inversion H2; intuition.
exists x; intuition.
induction H1.
elim H2; intros x0 H4.
constructor 1 with (x0).
intuition.
Qed.

(**
  overriding, BBook: section 2.4.2, p.80, array 2, row 2
  *)
Theorem valid_override2: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (q r: Ensemble (S*T)), In (relation s t) q -> In (relation s t) r -> override q r = (Comprehension (fun C => match C with | (x,y) => In (times s t) (x,y) /\ ((In q (x,y) /\ NotIn (domain r) x) \/ In r (x,y)) end)).
Proof.
intros S T s t q r H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
split.
elim H1; intros H2.
repeat (induction H).
generalize (H (x,y) (proj1 H2)); intuition.
repeat (induction H0).
generalize (H0 (x,y) H2); intuition.
intuition.
induction x.
induction H1.
elim H2; intros.
constructor; left; intuition.
constructor; right; intuition.
Qed.

(**
  left projection, BBook: section 2.4.2, p.80, array 2, row 3
  *)
Theorem valid_prj1_2: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), prj1(s,t) = (Comprehension (fun C => match C with | ((x,y),z) => In (times (times s t) s)  ((x,y),z)/\ z=x end)).
intros S T s t .
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
constructor; intuition.
induction x.
induction a.
induction H1.
constructor; intuition.
Qed.

(**
  right projection, BBook: section 2.4.2, p.80, array 2, row 4
  *)
Theorem valid_prj2_2: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), prj2(s,t) = (Comprehension (fun C => match C with | ((x,y),z) => In (times (times s t) t)  ((x,y),z)/\ z=y end)).
intros S T s t .
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
constructor; intuition.
induction x.
induction a.
induction H1.
constructor; intuition.
Qed.

(**
  parallel product, BBook: section 2.4.2, p.80, array 2, row 5
  *)
Theorem valid_parallel_product2: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (h: Ensemble (S*U)) (k: Ensemble (T*V)), In (relation s u) h -> In (relation t v) k -> parallel_product h k = (Comprehension (fun C => match C with | ((x,y),(z,w)) => In (times (times s t) (times u v)) ((x,y),(z,w))/\ In h (x,z) /\ In k (y,w) end )).
Proof.
intros S T U V s t u v h k H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
repeat (induction H).
repeat (induction H0).
generalize (H (x,z) H1).
generalize (H0 (y,w) H2).
intros H3 H4.
inversion H3; inversion H4.
repeat constructor; intuition.
induction x.
induction a; induction b.
induction H1.
constructor; intuition.
Qed.

(**
  * Binary relations: old theorems inherited from BPhoX
  *)

(* TODO: move, remove or rename these in the relevant files *)

Theorem rel001: forall (U V: Type) (A: Ensemble U) (B: Ensemble V) (R: Ensemble (U*V)), In (relation A B) R -> In (relation B A) (inverse R).
Proof.
intros.
repeat (induction H).
repeat constructor.
intros x H0.
induction x.
inversion H0.
generalize (H (b,a) H2); intros H4.
inversion H4; constructor; intuition.
Qed.

Theorem rel005: forall (U V: Type) (A: Ensemble U) (B:Ensemble V) (R: Ensemble (U*V)), In (relation A B) R -> Included (domain R) A.
Proof.
intros.
repeat (induction H).
intros x H0.
repeat (induction H0).
generalize (H (a,x) H0).
intros H1; inversion H1.
intuition.
Qed.

Theorem rel006: forall (U V: Type) (A: Ensemble U) (B:Ensemble V) (R: Ensemble (U*V)), In (relation A B) R -> Included (range R) B.
Proof.
intros.
repeat (induction H).
intros x H0.
repeat (induction H0).
generalize (H (x,b) H0).
intros H1; inversion H1.
intuition.
Qed.

(* TODO: well-foundedness. The following commented claims from bphox seem a bit rushed :-P
The well-foundedness theorems of Coq seem general enough to deal directly with it
*)

(*
claim rel00 /\R (well.founded R -> /\X (\/x X x -> \/x (X x & /\y (X y -> ~ R (y,x))))).
*)

(*
claim rel007 /\A,B,C /\R:(relation A B) /\S:(relation B C) (relation A C) (R ; S).
*)

(* lexical order is well founded

claim rel008 /\R,S (well.founded R -> well.founded S -> well.founded (lexical.order R S)).
*)

(* prj1 *)

(* untested
SC: not the correct type, thus should'nt work
theorem prj1001 /\A,B (prj1 A times B = A).
*)

(* prj2 *)

(* untested
SC: see remark for prj1001
theorem prj2001 /\A,B (prj2 A times B = B).
*)
