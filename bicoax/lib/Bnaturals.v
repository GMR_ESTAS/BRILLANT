(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

(**

  The section of the BBook about natural numbers is split in several files
  matching subsections, sometimes with a finer granularity. A big part of
  the theorems is also devoted to checking the consistency of theorems for
  the BBook definition of natural numbers, which are subsets of BIG. This
  definition is not practical, especially in Coq, hence it is normal to want
  to use Coq's nat datatype. We though must check that this datatype is
  compatible with BBook natural numbers, and that the arithmetic operators of
  Coq match those of B. If not, we must state what parts match. For instance,
  most operators in Coq are total (pred 0 = 0) while it is not the case in B.

  I also tried to put some more effort in naming consistently some
  definitions and theorems, as follows:

  - the "bb" prefix means that it corresponds to a BBook definition. This is
  for avoiding name clashes with Coq (because there will be several "plus",
  for instance)

  - bbX means the definition has a strong relationship with the X set (in
  the B sense) or datatype

  - the "_y" suffix is for applicability theorems. This module contains
  a lot of function applications and expressions can grow especially big,
  hence the need for some kind of abbreviated guideline

  - the "_unary" of "_bin" suffix indicates a definition based on a function
  application and that does not require an additional applicability term (it
  means it can be deduced from the other terms)

  - the "_c1" means that the first argument is currified. "_c2" would mean
  the two first are currified and so on, but at the moment the case has not 
  appeared yet.

  Some examples:
  - bbN_of_nat transforms a Coq's nat into a subset of BIG (corresponding to
    a BBook natural number)
  - bbN_succ_y is an applicability theorem of a given natural number to the 
    BBook succ function
  - bbnat_plus_bin is the addition defined "à la B", as a binary operator

*)

Require Export Bnaturals_basics.
Require Export Bnaturals_ensembles.
Require Export Bnaturals_min.
Require Export Bnaturals_max.
Require Export Bnaturals_recfuns.
Require Export coq_missing_arith.
Require Export Bnaturals_arithmetic.
Require Export Bnaturals_arithmetic_inv.
Require Export Bnaturals_arithmetic_nat.
Require Export Bnaturals_arithmetic_inv_nat.
Require Export Bnaturals_iterate.
Require Export Bnaturals_cardinal.
Require Export Bnaturals_closure.
