(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bchapter2.
Require Import Classical_sets.
Require Import Bfinite_subsets.
Require Import Bintegers_basics.
Require Import Bintegers_min.
Require Import ZArith.

Open Local Scope Z_scope.

(**
  * Maximum: definitions
  *)

Definition Bmax (s: Ensemble Z) := iota _ inhabited_Z (fun x => In s x /\ (forall y, In s y -> x >= y)).

(**
  * Maximum: theorems
  *)

Theorem maximum_property_implies_is_maximum: forall (s: Ensemble Z) (x: Z),
  In s x -> (forall (n: Z), In s n -> x >= n) -> x = Bmax s.
Proof.
intros s x H H0.
unfold Bmax; apply iota_ind_weak.
intros.
destruct H1.
apply Zge_antisym; intuition.
exists x.
split; [ intuition | ].
intros.
destruct H1.
apply Zge_antisym; intuition.
Qed.

Theorem Bmax_is_maximal: forall (s: Ensemble Z), In (Power_finite BZ) (Intersection s BN) ->
  (forall (n:Z), In s n -> Bmax s >= n).
Proof.
intros s H0 n H1.
assert (forall (z: Z), In s z \/ ~(In s z)); [ intros z; apply classic | ].
assert (exists supremum, forall n, In s n -> Zge supremum n); [ apply upwards_finite_has_supremum; assumption | ].
assert (exists x, In s x); [ exists n; assumption | ].
generalize (dec_inh_Z_subset_with_supremum_has_unique_greatest_element s H H2 H3); intros.
assert (In s (Bmax s) /\ (forall (y: Z), In s y -> Bmax s >= y)).
pattern (Bmax s).
unfold Bmax; apply iota_ind_weak.
intuition.
unfold Bintegers_min.has_unique_least_element in H4.
exact H4.
destruct H5 as [ H5 H6 ].
apply H6; assumption.
Qed.

Theorem Bmax_in_its_argument: forall (s: Ensemble Z),
  s <> (Empty_set Z) ->
  In (Power_finite BZ) (Intersection s BN) ->
  In s (Bmax s).
Proof.
intros s H H0.
assert (forall (z: Z), In s z \/ ~(In s z)); [ intros z; apply classic | ].
assert (exists supremum, forall n, In s n -> Zge supremum n); [ apply upwards_finite_has_supremum; assumption | ].
assert (exists x, In s x).
specialize (not_empty_Inhabited Z s H); intros H3; destruct H3 as [ x H3 ]; exists x; assumption.
generalize (dec_inh_Z_subset_with_supremum_has_unique_greatest_element s H1 H2 H3); intros.
assert (In s (Bmax s) /\ (forall (y: Z), In s y -> Bmax s >= y)).
pattern (Bmax s).
unfold Bmax; apply iota_ind_weak.
intuition.
unfold Bintegers_min.has_unique_least_element in H4.
exact H4.
intuition.
Qed.


(**
  * Minimum: miscellaneous properties
  *)
Theorem Bmax_singleton: forall (a: Z), Bmax (Singleton a) = a.
Proof.
intros a.
unfold Bmax; apply iota_ind_weak.
intros b H.
destruct H as [ H H0 ]; destruct H; reflexivity.
exists a.
split.
split; [ intuition | intros y H1; destruct H1; intuition ].
intros.
destruct H as [ H H0 ]; destruct H; reflexivity.
Qed.

Theorem Bmax_doubleton1: forall (a b:Z), a >= b -> Bmax {= a, b =} = a.
Proof.
intros a b H.
unfold Bmax; apply iota_ind_weak.
intros b0 H0.
destruct H0 as [ H0 H1 ].
destruct H0.
destruct H0; reflexivity.
destruct H0.
specialize H1 with a.
apply Zge_antisym; [ apply H1; intuition | assumption ].
exists a.
split.
split; [ intuition | ].
intros y H0.
destruct H0; destruct H0; intuition.
intros.
destruct H0 as [ H0 H1 ].
destruct H0.
destruct H0; reflexivity.
destruct H0.
specialize H1 with a.
apply Zge_antisym; [ assumption | apply H1; intuition ].
Qed.

Theorem Bmax_doubleton2: forall (a b:Z), b >= a -> Bmax {= a, b =} = b.
Proof.
intros a b H.
rewrite commutativity_1.
apply Bmax_doubleton1; assumption.
Qed.

Theorem Bmax_union_aux: forall (a b: Ensemble Z),
  a <> (Empty_set Z) -> b <> (Empty_set Z) ->
  In (Power_finite BZ) (Intersection a BN) -> In (Power_finite BZ) (Intersection b BN) ->
  Bmax a >= Bmax b ->
  Bmax (Union a b) = (Bmax {= (Bmax a), (Bmax b) =}).
Proof.
intros a b Ha Hb Hupa Hupb Hasupb.
rewrite Bmax_doubleton1; [ | assumption ].
apply Zle_antisym; apply Zge_le.
unfold Bmax at 2; apply iota_ind_weak.
intros b0 H0.
destruct H0 as [ H0 H1 ].
destruct H0 as [ b0 H0 | b0 H0 ].
apply Bmax_is_maximal; assumption.
apply Zge_trans with (Bmax b).
assumption.
apply Bmax_is_maximal; assumption.
exists (Bmax a).
split.
split; [ left | ].
apply Bmax_in_its_argument; assumption.
intros y H0; destruct H0 as [ y H0 | y H0 ].
apply Bmax_is_maximal; assumption.
apply Zge_trans with (Bmax b).
assumption.
apply Bmax_is_maximal; assumption.
intros x' H0.
destruct H0 as [ H0 H1 ].
apply Zge_antisym.
destruct H0 as [ x' H0 | x' H0 ].
apply Bmax_is_maximal; assumption.
apply Zge_trans with (Bmax b).
assumption.
apply Bmax_is_maximal; assumption.
apply H1; left; apply Bmax_in_its_argument; assumption.
apply Bmax_is_maximal.
rewrite commutativity_2; rewrite distributivity_1.
apply finite_union.
constructor; rewrite commutativity_2; assumption.
left; apply Bmax_in_its_argument; assumption.
Qed.

Theorem Bmax_union: forall (a b: Ensemble Z),
  a <> (Empty_set Z) -> b <> (Empty_set Z) ->
  In (Power_finite BZ) (Intersection a BN) -> In (Power_finite BZ) (Intersection b BN) ->
  Bmax (Union a b) = (Bmax {= (Bmax a), (Bmax b) =}).
Proof.
intros a b Ha Hb Hupa Hupb.
assert (Bmax a >= Bmax b \/ Bmax b >= Bmax a) as H.
assert (Bmax a <= Bmax b \/ Bmax b < Bmax a) as H0.
apply Zle_or_lt.
intuition.
destruct H as [ H | H ].
apply Bmax_union_aux; assumption.
replace (Union (Singleton (Bmax a)) (Singleton (Bmax b))) with
  (Union (Singleton (Bmax b)) (Singleton (Bmax a))).
rewrite commutativity_1.
apply Bmax_union_aux; assumption.
apply commutativity_1.
Qed.


