(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import ZArith.
Require Import Bintegers_basics.

(**
  * Integers: old theorems inherited from BPhoX
  *)

(** TODO: The Z version involves more complicated stuff *)
Theorem nat001: forall n: N, N0 <> (Nsucc n).
Proof.
intros.
case n.
discriminate.
intros; simpl; discriminate.
Qed.

Theorem nat002: forall (m n: N), Nsucc m = Nsucc n -> m = n.
Proof.
apply Nsucc_inj.
Qed.

Theorem int001: forall (m n: Z), Zsucc m = Zsucc n -> m = n.
Proof.
apply Zsucc_inj.
Qed.

Theorem int002: forall (m n: Z), Zpred m = Zpred n -> m = n.
Proof.
intros.
change (Zpos 1 + Zneg 1 + m = Zpos 1 + Zneg 1 + n) in |- *.
do 2 rewrite <- Zplus_assoc; do 2 rewrite (Zplus_comm (Zneg 1)).
unfold Zpred in H.
rewrite H.
reflexivity.
Qed.

Theorem int003: forall (n: Z), Zsucc n <> Zpred n.
Proof.
intros.
unfold Zsucc; unfold Zpred.
omega.
Qed.

Theorem int004: forall (n: Z), Zpred (Zsucc n) = n.
Proof.
intros.
unfold Zsucc; unfold Zpred.
ring.
Qed.


(* zero *)

(*
theorem nat010 N zero.
theorem int010 Z zero.
Not necessary: Z0 belongs to all constructs, thanks to coercions
*)

(* successor *)

(*
theorem nat011 /\n:N N (succ n).
theorem int011 /\n:Z Z (succ n).
theorem succ001 /\X /\x,y:Z ((x = y -> X) -> succ x = succ y -> X).
*)

Theorem succ001: forall (X: Prop) (x y:Z), (x = y -> X) -> Zsucc x = Zsucc y -> X.
Proof.
intros.
apply H; apply Zsucc_inj.
tauto.
Qed.

(* predecessor *)

(*
theorem int012 /\n:Z Z (pred n).
theorem pred001 /\X /\x,y:Z ((x = y -> X) -> pred x = pred y -> X).
*)

Theorem pred001: forall (X: Prop) (x y:Z), (x = y -> X) -> Zpred x = Zpred y -> X.
Proof.
intros.
apply H; apply int002.
tauto.
Qed.

(*
naturals are part of integers
theorem int013 /\n:N Z n.
*)


(* integers normal form *)

Theorem int014: forall (X:Z -> Prop) (n: Z), (X 0 -> (forall (m:Z), X m -> X (Zsucc m)) -> (forall (m:Z), X m -> X (Zpred m)) -> X n).
Proof.
intros.
induction n.
tauto.
apply (Pind (fun p:_ => X (Zpos p))).
apply (H0 (Z0)).
assumption.
intros.
replace (Zpos (Psucc p0)) with (Zsucc (Zpos p0)).
apply H0; assumption.
symmetry; apply Zpos_succ_morphism.
apply (Pind (fun p:_ => X (Zneg p))).
unfold Zpred in H1.
replace (-1) with (0 + Zneg 1).
apply H1; assumption.
ring.
intros.
replace (Zneg (Psucc p0)) with (Zpred (Zneg p0)).
apply H1; assumption.
simpl.
replace (Pplus p0 1) with (Psucc p0).
reflexivity.
apply Pplus_one_succ_r.
Qed.

(* SC: Does not make much sense ?!
theorem int015 /\X /\n ((n = zero -> X zero) -> /\m:Z  (n = succ m -> X (succ m)) -> /\m:Z  (n = pred m -> X (pred m)) -> Z n -> X n).
*)

(* unicity of the last element of a relation defined by induction on integers *)

(*
claim recint000 /\c,f,g /\n:Z \/!z (recintP c f g n z).
TODO
*)

(* function defined by induction on integers *)

(* TODO
theorem recint001 /\f,g,c recint c f g zero = c.
theorem recint002 /\f,g,c /\n:Z recint c f g (succ n) = f n (recint c f g n).
theorem recint003 /\f,g,c /\n:Z recint c f g (pred n) = g n (recint c f g n).
unsolved
theorem recint004 /\X,Y /\f,g:(Z => X => X) /\c:X /\n:Z X (recint c f g n).
*)


(* successor and predecessor *)

Theorem succpred001: forall (n:Z), Zsucc (Zpred n) = Zpred (Zsucc n).
Proof.
intros.
rewrite <- Zsucc_pred.
rewrite <- Zpred_succ.
reflexivity.
Qed.

Theorem succpred002: forall (n:Z), Zsucc n <> n.
Proof.
intros.
cut (n <> Zsucc n).
intuition.
apply Zsucc_discr.
Qed.

Theorem succpred003: forall (n:Z), Zpred n <> n.
Proof.
intros.
apply Zlt_not_eq.
apply Zlt_pred.
Qed.

Theorem succpred004: forall (n:Z), Zsucc (Zsucc n) <> n.
Proof.
intros.
cut (n <> Zsucc (Zsucc n)).
intuition.
apply Zlt_not_eq.
apply Zlt_lt_succ.
apply Zlt_succ.
Qed.

Theorem succpred005: forall (n:Z), Zpred (Zpred n) <> n.
Proof.
intros.
apply Zlt_not_eq.
apply Zlt_trans with (Zpred n).
apply Zlt_pred.
apply Zlt_pred.
Qed.


(* one *)

(*
theorem one001 Z one.
*)

Theorem one002: 1 <> 0.
Proof.
discriminate.
Qed.

(* two *)

(*
theorem two001 Z two.
*)

Theorem two002: 2 <> 1.
Proof.
discriminate.
Qed.

Theorem two003: 2 <> 0.
Proof.
discriminate.
Qed.

(* addition *)

Theorem plus001: forall (n:Z), n + 0 = n.
Proof.
intros.
ring.
Qed.

Theorem plus002: forall (n m:Z), (n + Zsucc m = Zsucc (n + m)).
Proof.
intros.
ring.
Qed.

Theorem plus003: forall (n m:Z), (n + Zpred m = Zpred (n + m)).
Proof.
intros.
unfold Zpred.
ring.
Qed.

(* Not useful
theorem plus004 /\n,m:Z Z (n plus m).
*)

Theorem plus005: forall (n:Z), 0 + n = n.
Proof.
intros.
ring.
Qed.

Theorem plus006: forall (n m:Z), (Zsucc m + n = Zsucc (m + n)).
Proof.
intros.
ring.
Qed.

Theorem plus007: forall (n m:Z), (Zpred m + n = Zpred (m + n)).
Proof.
intros.
unfold Zpred.
ring.
Qed.

Theorem plus008: forall (n m:Z), (n + m = m + n).
Proof.
intros.
ring.
Qed.

Theorem plus010: forall (m n v:Z), (v + m = v + n -> m = n).
Proof.
intros.
apply Zplus_reg_l with v.
exact H.
Qed.

Theorem plus011: forall (X: Prop) (m n v:Z), ((m = n -> X) -> v + m = v + n -> X).
Proof.
intros.
apply H; apply plus010 with v.
exact H0.
Qed.

Theorem plus012: forall (m n v:Z), (m + v = n + v -> m = n).
Proof.
intros.
cut (-v + (m+v) = -v + (n+v)).
rewrite Zplus_permute.
rewrite (Zplus_comm (- v) v).
rewrite Zplus_opp_r.
rewrite Zplus_permute.
rewrite (Zplus_comm (- v) v).
rewrite Zplus_opp_r.
do 2 rewrite Zplus_0_r.
trivial.
apply Zplus_eq_compat.
reflexivity.
assumption.
Qed.

Theorem plus013: forall (X: Prop) (m n v:Z), ((m = n -> X) -> m + v = n + v -> X).
Proof.
intros.
apply H; apply plus012 with v.
exact H0.
Qed.

Theorem plus014: forall (x:Z), x + 1 = Zsucc x.
Proof.
intros.
ring.
Qed.

Theorem plus015: forall (x:Z), 1 + x = Zsucc x.
Proof.
intros.
ring.
Qed.

Theorem plus016: forall (x:Z), x + 2 = Zsucc (Zsucc x).
Proof.
intros.
ring.
Qed.

Theorem plus017: forall (x:Z), 2 + x = Zsucc (Zsucc x).
Proof.
intros.
ring.
Qed.

Theorem plus018: forall (x y z:Z), (x = y -> x + z = y + z).
Proof.
intros.
apply Zplus_eq_compat; [ assumption | reflexivity ].
Qed.

Theorem plus019: forall (x y z:Z), (x = y -> z + x = z + y).
Proof.
intros.
apply Zplus_eq_compat; [ reflexivity | assumption ].
Qed.

Theorem plus020: forall (m n v:Z), m + (n + v) = (m + n) + v.
Proof.
intros.
ring.
Qed.

Theorem plus021: forall (m n v:Z), m + (n + v) = (n + m) + v.
Proof.
intros.
ring.
Qed.

Theorem plus022: forall (m n v:Z), m + (n + v) = (m + v) + n.
Proof.
intros.
ring.
Qed.

Theorem plus023: forall (m n v:Z), m + (n + v) = (v + m) + n.
Proof.
intros.
ring.
Qed.

Theorem plus024: forall (m n v:Z), m + (n + v) = (n + v) + m.
Proof.
intros.
ring.
Qed.

Theorem plus025: forall (m n v:Z), m + (n + v) = (v + n) + m.
Proof.
intros.
ring.
Qed.

Theorem plus026: forall (m n v:Z), (m + n + v = m + v + n).
Proof.
intros.
ring.
Qed.

Theorem plus027: forall (m n v:Z), (m + n + v = n + m + v).
Proof.
intros.
ring.
Qed.

Theorem plus028: forall (m n v:Z), (m + n + v = n + v + m).
Proof.
intros.
ring.
Qed.

Theorem plus029: forall (m n v:Z), (m + n + v = v + m + n).
Proof.
intros.
ring.
Qed.

Theorem plus030: forall (m n v:Z), (m + n + v = v + n + m).
Proof.
intros.
ring.
Qed.


(* substraction *)

Theorem sub001:forall (n:Z), n - 0 = n.
Proof.
intros.
ring.
Qed.

Theorem sub002:forall (n m:Z), (n - Zsucc m = Zpred (n - m)).
Proof.
intros.
unfold Zpred; unfold Zsucc.
ring.
Qed.

Theorem sub003:forall (n m:Z), (n - Zpred m = Zsucc (n - m)).
Proof.
intros.
unfold Zpred; unfold Zsucc.
ring.
Qed.

(* Not useful
theorem sub004 /\n,m:Z, Z (n sub m).
*)

Theorem sub005:forall (n:Z), 0 - n = - n.
Proof.
intros.
ring.
Qed.

Theorem sub006:forall (m n:Z), Zsucc m - n = Zsucc (m - n).
Proof.
intros.
unfold Zsucc.
ring.
Qed.

Theorem sub007:forall (m n:Z), Zpred m - n = Zpred (m - n).
Proof.
intros.
unfold Zpred.
ring.
Qed.

Theorem sub008:forall (m n v:Z), (m + n) - v = (m - v) + n.
Proof.
intros.
ring.
Qed.

Theorem sub009:forall (m n v:Z), (m - n) + v = m - (n - v).
Proof.
intros.
ring.
Qed.

Theorem sub010:forall (m n v:Z), (m - n) - v = m - (n + v).
Proof.
intros.
ring.
Qed.

Theorem sub011:forall (m n v:Z), (v - m - n = v - n - m).
Proof.
intros.
ring.
Qed.

Theorem sub012:forall (m n v:Z), (v - (m - n) = v - m + n).
Proof.
intros.
ring.
Qed.

Theorem sub013:forall (m n v:Z), (v - (m + n) = v - m - n).
Proof.
intros.
ring.
Qed.

Theorem sub014:forall (m n v:Z), (v + (m - n) = v + m - n).
Proof.
intros.
ring.
Qed.

Theorem sub015:forall (m n v:Z), (m = n -> m - v = n - v).
Proof.
intros.
intuition.
Qed.

Theorem sub016:forall (m n v:Z), (m = n -> m - v = n - v).
Proof.
intros.
intuition.
Qed.


(* multiplication *)

Theorem mult001:forall (n:Z), n * 0 = 0.
Proof.
intros.
ring.
Qed.

Theorem mult002:forall (n m:Z), (n * Zsucc m = n * m + n).
Proof.
intros.
unfold Zsucc.
ring.
Qed.

Theorem mult003:forall (n m:Z), (n * Zpred m = n * m - n).
Proof.
intros.
unfold Zpred.
ring.
Qed.

(* Not useful
theorem mult004 /\n,m:Z, Z (n mult m).
*)

Theorem mult005:forall (n:Z), (0 * n = 0).
Proof.
intros.
ring.
Qed.

Theorem mult006:forall (m n:Z), (Zsucc m * n = m * n + n).
Proof.
intros.
unfold Zsucc.
ring.
Qed.

Theorem mult007:forall (m n:Z), (Zpred m * n = m * n - n).
Proof.
intros.
unfold Zpred.
ring.
Qed.

Theorem mult008:forall (m n:Z), (m * n = n * m).
Proof.
intros.
ring.
Qed.

Theorem mult009:forall (m n v:Z), m * (n + v) = m * n + m * v.
Proof.
intros.
ring.
Qed.

Theorem mult010:forall (m n v:Z), (n + v) * m = n * m + v * m.
Proof.
intros.
ring.
Qed.

Theorem mult011:forall (m n v:Z), m * (n - v) = m * n - m * v.
Proof.
intros.
ring.
Qed.

Theorem mult012:forall (m n v:Z), (m - n) * v = m * v - n * v.
Proof.
intros.
ring.
Qed.

Theorem mult013:forall (m n v:Z), m * (n * v) = (m * n) * v.
Proof.
intros.
ring.
Qed.

Theorem mult014:forall (m n v:Z), m * (n * v) = (m * v) * n.
Proof.
intros.
ring.
Qed.

Theorem mult015:forall (m n v:Z), m * (n * v) = (n * m) * v.
Proof.
intros.
ring.
Qed.

Theorem mult016:forall (m n v:Z), m * (n * v) = (n * v) * m.
Proof.
intros.
ring.
Qed.

Theorem mult017:forall (m n v:Z), m * (n * v) = (v * m) * n.
Proof.
intros.
ring.
Qed.

Theorem mult018:forall (m n v:Z), m * (n * v) = (v * n) * m.
Proof.
intros.
ring.
Qed.

Theorem mult019:forall (m n v:Z), m * n * v = m * v * n.
Proof.
intros.
ring.
Qed.

Theorem mult020:forall (m n v:Z), m * n * v = n * m * v.
Proof.
intros.
ring.
Qed.

Theorem mult021:forall (m n v:Z), m * n * v = n * v * m.
Proof.
intros.
ring.
Qed.

Theorem mult022:forall (m n v:Z), m * n * v = v * m * n.
Proof.
intros.
ring.
Qed.

Theorem mult023:forall (m n v:Z), m * n * v = v * n * m.
Proof.
intros.
ring.
Qed.

(* opposite *)

Theorem opp001: - 0 = 0.
Proof.
intros.
ring.
Qed.

Theorem opp002: forall (n:Z), - Zsucc n = Zpred (- n).
Proof.
intros.
unfold Zsucc; unfold Zpred.
ring.
Qed.

Theorem opp003: forall (n:Z), - Zpred n = Zsucc (- n).
Proof.
intros.
unfold Zsucc; unfold Zpred.
ring.
Qed.


(* Not useful
theorem opp004 /\n:Z Z (- n).
*)

Theorem opp005: forall (n:Z), - n = 0 - n.
Proof.
intros.
ring.
Qed.

Theorem opp006: forall (n:Z), - (- n) = n.
Proof.
intros.
ring.
Qed.

Theorem opp007: forall (m n:Z), (- m) + (- n) = - (m + n).
Proof.
intros.
ring.
Qed.

Theorem opp008: forall (m n:Z), - m + n = - (m - n).
Proof.
intros.
ring.
Qed.

Theorem opp009: forall (m n:Z), m + - n = m - n.
Proof.
intros.
ring.
Qed.

Theorem opp010: forall (m n:Z), - m - - n = n - m.
Proof.
intros.
ring.
Qed.

(*
Theorem opp011: forall (m n:Z), - m - n = m + n.
Proof.
Oops: this "claim" was wrong in bphox, whatever the priority of opp
*)

(*
Theorem opp012: forall (m n:Z), m - - n = - (m + n).
Proof.
Oops: same as above.
*)

Theorem opp013: forall (m n:Z), - m * - n = m * n.
Proof.
intros.
ring.
Qed.

Theorem opp014: forall (m n:Z), - m * n = - (m * n).
Proof.
intros.
ring.
Qed.

Theorem opp015: forall (m n:Z), m * - n = - (m * n).
Proof.
intros.
ring.
Qed.

(* division *)

(* TODO
claim div000 /\x,y:Z \/!z:Z (divP x y z).
*)

(* exponentiation *)

Theorem exp001: forall (n:Z), n ^ 0 = 1.
Proof.
intros.
ring.
Qed.

Theorem exp002: forall (n m:Z), m >= 0 -> n ^ (Zsucc m) = (n ^ m) * n.
Proof.
intros.
unfold Zsucc.
replace (n^(m+1)) with (n^m * n^1).
ring.
symmetry; apply Zpower_exp.
assumption.
intuition.
Qed.

(* This one is hard, not much theorems to tackle in Coq's ZArith library
TODO interesting
Theorem exp003: forall (n m:Z), n ^ (Zpred m) = (n ^ m) / n.
Proof.
*)

(* modulo *)

(* odd *)

(* even *)

(* TODO: move card to the theorems about sets
card
claim card001 card emptyset = zero.
claim card002 card emptyset <= one.
claim card003 /\x (card (singleton x) = one).
claim card004 /\X /\n:N ((card X = n) or (card X <= n) -> card X <= (succ n)).
*)

(* <= *)

Theorem leq001: forall (n:Z), n <= n.
Proof.
intuition.
Qed.

Theorem leq002: forall (m n:Z), (0 <= 1).
Proof.
intuition.
Qed.

Theorem leq003: forall (m n:Z), (1 <= 2).
Proof.
intuition.
Qed.

Theorem leq004: forall (m n:Z), (m <= n -> Zsucc m <= Zsucc n).
Proof.
intros.
omega.
Qed.

Theorem leq005: forall (m n:Z), (m <= n -> Zpred m <= Zpred n).
Proof.
intros.
unfold Zpred.
omega.
Qed.

Theorem leq006: forall (m n:Z), (m <= n -> m <= Zsucc n).
Proof.
intros.
omega.
Qed.

Theorem leq007: forall (m n:Z), (m <= n -> Zpred m <= n).
Proof.
intros.
unfold Zpred.
omega.
Qed.

Theorem leq008: forall (m n v:Z), (m <= n -> n <= v -> m <= v).
Proof.
intros.
omega.
Qed.

Theorem leq009: forall (m n:Z), (m <= n \/ n <= m).
Proof.
intros.
elim (Ztrichotomy m n).
intuition.
intuition.
Qed.


(* < *)

Theorem lneq001: forall (m n:Z), (0 < 1).
Proof.
intros.
omega.
Qed.

Theorem lneq002: forall (m n:Z), (1 < 2).
Proof.
intros.
omega.
Qed.


(*TODO: stuff
inf
claim inf001 /\X \/!x (infP X x).

sup
claim sup001 /\X \/!x (supP X x).

min
claim min001 /\x,y:Z (x <= y -> (min x y = x)).

max
claim max001 /\x,y:Z (x <= y -> (max x y = y)).

interval
maximum implementable integer
minimum implementable integer

set of implementables naturals
set of implementables naturals differents from zero
set of implementables integers
set of implementables integers differents from zero
TODO: set-related integer theorems go in a more integrated library
*)
