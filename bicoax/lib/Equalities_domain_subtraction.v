(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Classical_sets.
Require Import Bderived_constructs.
Require Import Brelations.
Require Import Bfunctions.

Open Scope eB_scope.

(**
  BBook: section 2.6.4, p.109, array 1, row 1
  *)
Theorem Equality_laws_domsub_01: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ domain(r) ∩  u = ∅) ⇒(u ⩤ r = r).
Proof.
intros S T s t r u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0; assumption.

induction H.
induction x; constructor.
assumption.
assert (~(In (Intersection (domain r) u) a)).
rewrite H1; intros H2; contradiction.
intros H3; apply H2.
split; [ constructor; exists b; assumption | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.109, array 1, row 2
  *)
Theorem Equality_laws_domsub_02: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ r∼ ∈ (t ⇸ s)) ⇒(u ⩤ r = r ⩥ (r[u])).
Proof.
intros S T s t u r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
constructor; [ assumption | ].
intros H2; apply H1.
inversion H2.
assert (x=x0).
induction H.
inversion H5.
apply H7 with y.
constructor; assumption.
constructor; intuition.
rewrite H5; intuition.

induction H0.
constructor; [ assumption | ].
intros H2; apply H1.
compute; apply image_intro with x; intuition.
Qed.

(**
  BBook: section 2.6.4, p.109, array 1, row 3
  *)
Theorem Equality_laws_domsub_03: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (T*S)) (r: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ p ∈ (t ↔ u)) ⇒(u ⩤ (r;p) = ((u ⩤ r);p)).
Proof.
intros S T s t p r u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H3.
constructor; exists x0.
split; [ | intuition ].
constructor; intuition.

induction H0.
inversion H0.
induction H1.
inversion H1.
constructor; [ | assumption ].
constructor; exists x; intuition.
Qed.

(**
  BBook: section 2.6.4, p.109, array 1, row 4
  *)
Theorem Equality_laws_domsub_04: forall (S: Type) (s: Ensemble S) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s) ⇒(u ⩤ id(v) = id(v ∖ u)).
Proof.
intros S s u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H4.
constructor; [ | assumption ].
constructor.
constructor; [ assumption | assumption ].
constructor; [ assumption | rewrite <- H5; assumption ].

induction H0.
inversion H0.
inversion H4; inversion H5.
constructor; [ | assumption ].
constructor; [ | assumption ].
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.109, array 1, row 5
  *)
Theorem Equality_laws_domsub_05: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ v ⊆ s ∧ r ∈ (s ↔ t)) ⇒(u ⩤ ( v ◁ r) = (v ∖ u) ◁ r).
Proof.
intros S T s t u v r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; [ assumption | ].
constructor; assumption.

induction H0.
inversion H1.
constructor; [ | assumption ].
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.109, array 1, row 6
  *)
Theorem Equality_laws_domsub_06: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (w: Ensemble T) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ w ⊆ t) ⇒(u ⩤ (r ▷ w) = (u ⩤ r) ▷ w).
Proof.
intros S T s t u w r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; [ | assumption ].
constructor; assumption.

induction H0.
inversion H0.
constructor; [ | assumption ].
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.109, array 1, row 7
  *)
Theorem Equality_laws_domsub_07: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ v ⊆ s ∧ r ∈ (s ↔ t)) ⇒(u ⩤ ( v ⩤ r) = (u ∪ v) ⩤ r).
Proof.
intros S T s t u v r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; [ assumption | ].
intros H6; inversion H6.
apply H1; assumption.
apply H5; assumption.

induction H0.
constructor.
constructor.
assumption.
intros H2; apply H1; right; assumption.
intros H2; apply H1; left; assumption.
Qed.

(**
  BBook: section 2.6.4, p.109, array 1, row 8
  *)
Theorem Equality_laws_domsub_08: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (w: Ensemble T) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ w ⊆ t) ⇒(u ⩤ (r ⩥ w) = (u ⩤ r) ⩥ w).
Proof.
intros S T s t u w r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; [ | assumption ].
constructor; assumption.

induction H0.
inversion H0.
constructor; [ | assumption ].
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.109, array 1, row 9
  *)
Theorem Equality_laws_domsub_09: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒(u ⩤ (p ⥷ q) = (u ⩤ p) ⥷ (u ⩤ q)).
Proof.
intros S T s t p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H3.
constructor; left.
split; [ constructor; intuition | ].
intros H5; apply (proj2 H3).
induction H5.
inversion H5.
inversion H6.
constructor; exists x; assumption.
constructor; right.
constructor; assumption.

induction H0.
induction H0.
induction H0.
inversion H0.
constructor; [ | assumption ].
constructor.
left; split; [ assumption | ].
intros H6; apply H1.
induction H6; inversion H6.
constructor; exists x.
constructor; assumption.
induction H0.
constructor; [ | assumption ].
constructor; right; assumption.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 1
  *)
Theorem Equality_laws_domsub_10: forall (S T V: Type) (s: Ensemble S) (t: Ensemble T) (v: Ensemble V) (p: Ensemble (S*T)) (q: Ensemble (S*V)) (u: Ensemble S), (u ⊆ s ∧ p ∈ (s ↔ t) ∧ q ∈ (s ↔ v)) ⇒((u ⩤ p) ⊗ (u ⩤ q) = u ⩤ (p ⊗ q)).
Proof.
intros S T V s t v p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0; inversion H1.
constructor; [ | assumption ].
constructor; assumption.


induction H0.
inversion H0.
constructor; constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 2
  *)
Theorem Equality_laws_domsub_11: forall (S T Z: Type) (s: Ensemble S) (t: Ensemble T) (z: Ensemble Z) (p: Ensemble (S*T)) (q: Ensemble (S*Z)) (u: Ensemble T) (v: Ensemble Z), (u ⊆ t ∧ v ⊆ z ∧ p ∈ (s ↔ t) ∧ q ∈ (s ↔ z)) ⇒(((p ⩥ u) ⊗ (q ⩥ v)) ⊆ ((p ⊗ q) ⩥ (u×v))).
Proof.
intros S T Z  s t z p q u v H.
intros x H0.

induction H0.
inversion H0.
inversion H1.
constructor.
constructor; assumption.
intros H10.
inversion H10.
contradiction.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 3
  *)
Theorem Equality_laws_domsub_12: forall (S T W Z: Type) (s: Ensemble S) (t: Ensemble T) (w: Ensemble W) (z: Ensemble Z) (p: Ensemble (S*W)) (q: Ensemble (T*Z)) (u: Ensemble S) (v: Ensemble T), (u ⊆ s ∧ v ⊆ t ∧ p ∈ (s ↔ w) ∧ q ∈ (t ↔ z)) ⇒(((u ⩤ p) ∥ (v ⩤ q)) ⊆ ((u×v) ⩤ (p ∥ q))).
Proof.
intros S T W Z  s t w z p q u v H.
intros x H0.

induction H0.
inversion H0.
inversion H1.
constructor.
constructor; assumption.
intros H10.
inversion H10.
contradiction.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 4
  *)
Theorem Equality_laws_domsub_13: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒(u ⩤ (p ∪ q) = (u ⩤ p) ∪ (u ⩤ q)).
Proof.
intros S T s t p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
left; constructor; assumption.
right; constructor; assumption.

induction H0; inversion H0.
constructor; [ | assumption ].
left; assumption.
constructor; [ | assumption ].
right; assumption.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 5
  *)
Theorem Equality_laws_domsub_14: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒(u ⩤ (p ∩ q) = (u ⩤ p) ∩ (u ⩤ q)).
Proof.
intros S T s t p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
split; constructor; assumption.

induction H0.
inversion H0; inversion H1.
constructor; [ | assumption ].
split; [ assumption | ].
rewrite H4; rewrite <- H7; assumption.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 6
  *)
Theorem Equality_laws_domsub_15: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒(u ⩤ (p ∖ q) = (u ⩤ p) ∖ q).
Proof.
intros S T s t p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; [ | assumption ].
constructor; assumption.

induction H0.
induction H0.
constructor; [ | assumption ].
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 7
  *)
Theorem Equality_laws_domsub_16: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (x: S) (y: T), (u ⊆ s ∧ x ∈ s ∧ y ∈ t ∧ x ∉ u) ⇒(u ⩤ (Singleton (x↦y)) = (Singleton (x↦y))).
Proof.
intros S T s t u x y H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

induction H0.
assumption.

induction H0.
constructor; intuition.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 8
  *)
Theorem Equality_laws_domsub_17: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (x: S) (y: T), (u ⊆ s ∧ x ∈ s ∧ y ∈ t ∧ x ∈ u) ⇒(u ⩤ (Singleton (x↦y)) = ∅).
Proof.
intros S T s t u x y H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

induction z.
inversion H0.
assert (a=x).
inversion H3; reflexivity.
assert (~(In u x)); [ rewrite <- H5; assumption | ].
induction H; contradiction.

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 9
  *)
Theorem Equality_laws_domsub_18: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ domain(r) ⊆ u) ⇒(u ⩤ r = ∅).
Proof.
intros S T s t u r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H.
absurd (In u x).
assumption.
apply H2; constructor; exists y; assumption.

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 10
  *)
Theorem Equality_laws_domsub_19: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s) ⇒(u ⩤ (v×t) = (v ∖ u) × t).
Proof.
intros S T s t u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; [ split; assumption | assumption ].

induction H0.
induction H0.
constructor; [ constructor; assumption | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 11
  *)
Theorem Equality_laws_domsub_20: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s ∧ r ∈ (s ↔ t) ) ⇒((u ∪ v) ⩤ r = (u ⩤ r) ∩ (v ⩤ r)).
Proof.
intros S T s t r u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
split.
constructor; [ assumption | ].
intros H2; apply H1; left; assumption.
constructor; [ assumption | ].
intros H2; apply H1; right; assumption.

induction H0.
inversion H0; inversion H1.
assert ((x0,y)=(x1,y0)).
rewrite H4; rewrite H7; reflexivity.
injection H8; intros.
constructor.
assumption.
intros H11; induction H11.
apply H3; assumption.
apply H6; rewrite <- H10; assumption.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 12
  *)
Theorem Equality_laws_domsub_21: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s ∧ r ∈ (s ↔ t) ) ⇒((u ∩ v) ⩤ r = (u ⩤ r) ∪ (v ⩤ r)).
Proof.
intros S T s t r u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
assert (~(In u x) \/ ~(In v x)).
apply not_and_or.
intros H2; apply H1.
constructor; intuition.
induction H2.
left; constructor; assumption.
right; constructor; assumption.

induction H0; inversion H0.
constructor; [ assumption | ].
intros H4; apply H2.
inversion H4; assumption.
constructor; [ assumption | ].
intros H4; apply H2.
inversion H4; assumption.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 13
  *)
Theorem Equality_laws_domsub_22: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s ∧ r ∈ (s ↔ t) ) ⇒((u ∖ v) ⩤ r = (u ⩤ r) ∪ (v ◁ r)).
Proof.
intros S T s t r u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
assert (~(In u x) \/ In v x).
assert (~(In u x) \/ ~~(In v x)).
apply not_and_or.
intros H2; apply H1; constructor; intuition.
induction H2; [ left; assumption | right; apply NNPP; assumption ].
induction H2.
left; constructor; assumption.
right; constructor; assumption.

induction H0; inversion H0.
constructor; [ assumption | ].
intros H4; apply H2; inversion H4; assumption.
constructor; [ assumption | ].
intros H4; inversion H4; contradiction.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 14
  *)
Theorem Equality_laws_domsub_23: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ) ⇒(domain(r) ⩤ r = ∅).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
absurd (In (domain r) x).
assumption.
constructor; exists y; assumption.

contradiction.
Qed.

(**
  property 2.6.7, BBook: section 2.6.4, p.110, array 1, row 15
  *)
Theorem Equality_laws_domsub_24: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (v: Ensemble T), (f ∈ (s ⇸ t) ∧ v ⊆ t) ⇒(f∼[v] ⩤ f = f ⩥ v).
Proof.
intros S T s t f v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
constructor.
assumption.
intros H2; apply H1; compute; apply image_intro with y.
split; [ assumption | constructor; assumption ].

induction H0.
constructor.
assumption.
intros H2; apply H1.
induction H2.
induction H2; inversion H3.
assert (x=y).
induction H; induction H.
apply H8 with y0; assumption.
rewrite <- H7; assumption.
Qed.

(**
  BBook: section 2.6.4, p.110, array 1, row 16
  *)
Theorem Equality_laws_domsub_25: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ) ⇒(∅ ⩤ r = r).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
assumption.

induction x.
constructor.
assumption.
intros H1; contradiction.
Qed.


Close Scope eB_scope.
