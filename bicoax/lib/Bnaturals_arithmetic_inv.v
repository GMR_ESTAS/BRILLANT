(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bchapter2.
Require Import Constructive_sets.
Require Import Bnaturals_basics.
Require Import Bnaturals_min.
Require Import Arith.
Require Import Euclid.
Require Import coq_missing_arith.
Require Import Bnaturals_arithmetic.

Open Local Scope nat_scope.

(**
  * Pure BBook version of inverse arithmetic operations
  *)

(**
  ** Subtraction
  *)

Theorem BBook_plus_le: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n),
  bbN_le m (app (bbN_plus_c1 m) n (bbN_plus_c1_applicable m n H H0)).
Proof.
intros m n H H0.
apply valid_bbN_le_r.
assumption.
apply application_in_codomain.
replace (app (bbN_plus_c1 m) n (bbN_plus_c1_applicable m n H H0))
  with (app (app bbN_plus m (bbN_plus_y m H)) n (indirect_bbN_plus_c1_applicable m n H H0)).
rewrite valid_bbN_plus_nat.
apply le_plus_l.
symmetry; apply identifying_app.
replace (app bbN_plus m (bbN_plus_y m H)) with (bbN_plus_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
constructor.
assumption.
Qed.

Definition above_set (m: Ensemble BIG_type) := (Comprehension (fun n => In bbN n /\ bbN_le m n)).

Theorem bbN_plus_c1_total_function_le: forall (m: Ensemble BIG_type) (H: In bbN m),
  In (total_function bbN (above_set m)) (bbN_plus_c1 m).
Proof.
intros m H.
generalize (bbN_plus_c1_total m H); intros.
inversion H0.
constructor; [ | assumption ].

constructor.
constructor.
constructor.
red; intros.
induction x.
assert (In bbN a).
rewrite <- H2; constructor; exists b; assumption.
assert (In bbN b).
repeat (induction H1).
generalize (H1 _ H4); intros.
inversion H7; assumption.
split.
assumption.
split; [ assumption | ].
replace b with (app (bbN_plus_c1 m) a (bbN_plus_c1_applicable m a H H5)).
apply BBook_plus_le.
symmetry; apply identifying_app; assumption.
inversion H1.
assumption.
Qed.

(**
  property 3.5.13, BBook: section 3.5.7, p.163

  Note/TODO: try a "purely B" approach for the proof of this theorem
  *)
Theorem bbN_plus_c1_total_bijection_le: forall (m: Ensemble BIG_type) (H: In bbN m),
  In (total_bijection bbN (above_set m)) (bbN_plus_c1 m).
Proof.
intros m H.
generalize (bbN_plus_c1_total_function_le m H); intros.
constructor.
assumption.
constructor.
constructor.
inversion H0; assumption.
intros.
assert (In bbN x /\ In bbN z /\ In bbN y).
generalize (bbN_plus_c1_total m H); intros.
repeat (induction H3).
generalize (H3 _ H1); generalize (H3 _ H2); intros.
inversion H6; inversion H7; intuition.
decompose [and] H3.
apply valid_bbN_eq_r; [ assumption | assumption | ].
apply plus_reg_l with (nat_of_bbN m).
rewrite <- valid_bbN_plus_nat with m z H H6.
rewrite <- valid_bbN_plus_nat with m x H H4.
apply valid_bbN_eq_l.
apply application_in_codomain.
apply application_in_codomain.
assert (y = (app (bbN_plus_c1 m) x (bbN_plus_c1_applicable m x H H4))).
apply identifying_app; assumption.
assert (y = (app (bbN_plus_c1 m) z (bbN_plus_c1_applicable m z H H6))).
apply identifying_app; assumption.
replace (app (app bbN_plus m (bbN_plus_y m H)) x (indirect_bbN_plus_c1_applicable m x H H4))
  with (app (bbN_plus_c1 m) x (bbN_plus_c1_applicable m x H H4)).
replace (app (app bbN_plus m (bbN_plus_y m H)) z (indirect_bbN_plus_c1_applicable m z H H6))
  with (app (bbN_plus_c1 m) z (bbN_plus_c1_applicable m z H H6)).
rewrite <- H5; rewrite <- H8; reflexivity.
apply identifying_app.
replace (app bbN_plus m (bbN_plus_y m H)) with (bbN_plus_c1 m).
apply app_trivial_property.
apply identifying_app; constructor; [ | assumption ]; constructor.
apply identifying_app.
replace (app bbN_plus m (bbN_plus_y m H)) with (bbN_plus_c1 m).
apply app_trivial_property.
apply identifying_app; constructor; [ | assumption ]; constructor.
constructor.
inversion H0; assumption.
intros.
induction H1.
cut (exists x, exists H3:(In bbN x), y = bbN_plus_bin m x H H3).
intros.
induction H3.
induction H3.
exists x.
rewrite H3.
replace (bbN_plus_bin m x H x0) with (app (bbN_plus_c1 m) x (bbN_plus_c1_applicable m x H x0)).
apply app_trivial_property.
unfold bbN_plus_bin.
apply identifying_app.
replace (app bbN_plus m (bbN_plus_y m H)) with (bbN_plus_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
constructor.
assumption.
cut (exists x, exists H3:(In bbN x), nat_of_bbN y = nat_of_bbN (bbN_plus_bin m x H H3)).
intros.
induction H3; induction H3.
exists x; exists x0.
apply valid_bbN_eq_r.
assumption.
unfold bbN_plus_bin; apply application_in_codomain.
assumption.
(* This might be considered cheating but, well, the homomorphisms have been established :-) *)
exists (bbN_of_nat ((nat_of_bbN y) - (nat_of_bbN m))).
assert (In bbN (bbN_of_nat ((nat_of_bbN y) - (nat_of_bbN m)))).
apply bbN_of_nat_in_bbN.
exists H3.
unfold bbN_plus_bin; rewrite valid_bbN_plus_nat.
rewrite nat_2coercions_identity.
apply le_plus_minus.
apply valid_bbN_le_l; assumption.
Qed.

Section BBook_subtraction.
Variable m: Ensemble BIG_type.
Variable m_in_bbN: In bbN m.

(**
  definition of subtraction, BBook: section 3.5.7, p.163, array 2
  *)
Definition bbN_minus_c1 := (domain_restriction (inverse (bbN_plus_c1 m)) (above_set m)).

Theorem bbN_minus_c1_total: In (total_function (above_set m) bbN) bbN_minus_c1.
Proof.
generalize (bbN_plus_c1_total_bijection_le m m_in_bbN); intros.
unfold bbN_minus_c1.
constructor.
constructor.
constructor.
constructor.
intros x H0.
inversion H0.
inversion H1.
repeat (induction H).
generalize (H _ H5); intros.
inversion H10; split; assumption.
intros.
inversion H0; inversion H1.
inversion H4; inversion H8.
inversion H.
inversion H17.
inversion H19.
apply H23 with x; assumption.
rewrite Equality_laws_dom_06 with
  (Ensemble BIG_type) (Ensemble BIG_type) (above_set m) bbN (above_set m) (inverse (bbN_plus_c1 m)).
rewrite Equality_laws_dom_02 with
  (Ensemble BIG_type) (Ensemble BIG_type) bbN (above_set m) (bbN_plus_c1 m).
rewrite Equality_laws_ran_01 with
  (Ensemble BIG_type) (Ensemble BIG_type) bbN (above_set m) (bbN_plus_c1 m).
rewrite neutral_element_2.
reflexivity.
intuition.
inversion H.
inversion H1; assumption.
inversion H.
inversion H0.
inversion H3.
assumption.
split.
intuition.
do 2 constructor.
intros x H0.
inversion H0.
repeat (induction H).
generalize (H _ H1); intros.
inversion H6; split; assumption.
Qed.

End BBook_subtraction.

Definition bbN_minus_bb: Ensemble (Ensemble BIG_type * (Ensemble (Ensemble BIG_type * Ensemble BIG_type))) :=
   fun cpl => match cpl with | (m, invplus_m_n) => (bbN_minus_c1 m) = invplus_m_n end.
Definition bbN_minus := (domain_restriction bbN_minus_bb bbN).

(**
  ** Subtraction: Validity and properties
  *)

Theorem bbN_minus_y: forall (m: Ensemble BIG_type), In bbN m ->
  In (partial_function bbN (partial_function bbN bbN)) bbN_minus /\ In (domain bbN_minus) m.
Proof.
intros.
split.
constructor.

do 2 constructor.
red; intros.
induction x.
inversion H0.
split.
assumption.
inversion H3.
generalize (bbN_minus_c1_total a H4); intros.
do 4 (induction H6).
constructor.
do 2 constructor.
red; intros.
generalize (H6 _ H9).
intros.
induction x0.
inversion H10; induction H13; split; assumption.
assumption.

intros.
inversion H0; inversion H1.
inversion H4; inversion H8.
reflexivity.

constructor; exists (bbN_minus_c1 m).
constructor.
constructor.
assumption.
Qed.

Theorem bbN_minus_c1_y: forall (m n: Ensemble BIG_type) (H: In bbN m), In bbN n -> bbN_le m n ->
  In (partial_function bbN bbN) (bbN_minus_c1 m)
/\ In (domain (bbN_minus_c1 m)) n.
Proof.
intros.
generalize (bbN_minus_c1_total m H); intros.
inversion H2.
split.
assert (Included (above_set m) bbN /\ Included bbN bbN).
split.
red; intros.
induction H6; assumption.
intuition.
apply (Monotonicity_laws_02 (Ensemble BIG_type) (Ensemble BIG_type) bbN bbN (above_set m) bbN H6 _ H3).
rewrite H4.
split; assumption.
Qed.

Theorem indirect_bbN_minus_y: forall (m n: Ensemble BIG_type) (H: In bbN m), In bbN n -> bbN_le m n ->
  In (partial_function bbN bbN) (app bbN_minus m (bbN_minus_y m H))
/\ In (domain (app bbN_minus m (bbN_minus_y m H))) n.
Proof.
intros.
replace (app bbN_minus m (bbN_minus_y m H)) with (bbN_minus_c1 m).
apply bbN_minus_c1_y; assumption.
apply identifying_app.
unfold bbN_minus.
constructor.
constructor.
assumption.
Qed.


Theorem valid_bbN_minus_nat: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n) (H1: bbN_le m n),
  (nat_of_bbN (app (app bbN_minus m (bbN_minus_y m H)) n (indirect_bbN_minus_y m n H H0 H1)))
= (minus (nat_of_bbN n) (nat_of_bbN m)).
Proof.
intros m n H H0 H1.
replace (nat_of_bbN
  (app (app bbN_minus m (bbN_minus_y m H)) n (indirect_bbN_minus_y m n H H0 H1)))
with (nat_of_bbN (app (bbN_minus_c1 m) n (bbN_minus_c1_y m n H H0 H1))).
symmetry.
apply valid_nat_eq_r.
rewrite bbN_of_nat_of_bbN.
apply identifying_app.
constructor.
constructor.
replace ((bbN_of_nat (nat_of_bbN n - nat_of_bbN m), n))
  with ((bbN_of_nat (nat_of_bbN n - nat_of_bbN m),
    app (bbN_plus_c1 m)
       (bbN_of_nat (nat_of_bbN n - nat_of_bbN m))
       (bbN_plus_c1_applicable m (bbN_of_nat (nat_of_bbN n - nat_of_bbN m)) H (bbN_of_nat_in_bbN (nat_of_bbN n - nat_of_bbN m))))
    ).
apply app_trivial_property.
apply injective_projections.
simpl; reflexivity.
simpl.
replace (app (bbN_plus_c1 m) (bbN_of_nat (nat_of_bbN n - nat_of_bbN m))
  (bbN_plus_c1_applicable m
     (bbN_of_nat (nat_of_bbN n - nat_of_bbN m)) H
     (bbN_of_nat_in_bbN (nat_of_bbN n - nat_of_bbN m))))
 with (bbN_plus_bin m (bbN_of_nat (nat_of_bbN n - nat_of_bbN m)) H (bbN_of_nat_in_bbN (nat_of_bbN n - nat_of_bbN m))).
unfold bbN_plus_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
assumption.
rewrite valid_bbN_plus_nat.
rewrite nat_2coercions_identity.
apply le_plus_minus_r.
apply valid_nat_le_r.
rewrite bbN_of_nat_of_bbN.
rewrite bbN_of_nat_of_bbN.
assumption.
assumption.
assumption.
unfold bbN_plus_bin.
symmetry; apply identifying_app.
replace (app bbN_plus m (bbN_plus_y m H)) with (bbN_plus_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor; [ constructor | assumption ].
split; assumption.
apply application_in_codomain.
apply valid_bbN_eq_l.
apply application_in_codomain.
apply application_in_codomain.
apply identifying_app.
replace (app bbN_minus m (bbN_minus_y m H)) with (bbN_minus_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
constructor.
assumption.
Qed.

Theorem valid_bbN_minus_bbBook: forall (m n: nat) (H: m <= n),
  bbN_of_nat (minus n m) =
(app
  (app bbN_minus (bbN_of_nat m) (bbN_minus_y (bbN_of_nat m) (bbN_of_nat_in_bbN m)))
   (bbN_of_nat n)
   (indirect_bbN_minus_y (bbN_of_nat m) (bbN_of_nat n) (bbN_of_nat_in_bbN m) (bbN_of_nat_in_bbN n) (valid_nat_le_l m n H)
)).
Proof.
intros m n H.
apply valid_bbN_eq_r.
apply bbN_of_nat_in_bbN.
apply application_in_codomain.
rewrite valid_bbN_minus_nat.
do 3 (rewrite  nat_2coercions_identity).
reflexivity.
Qed.

Theorem valid_bbnat_minus_c1: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n) (H1: bbN_le m n),
  (nat_of_bbN (app (bbN_minus_c1 m) n (bbN_minus_c1_y m n H H0 H1)))
= (minus (nat_of_bbN n) (nat_of_bbN m)).
Proof.
intros m n H H0 H1.
replace (app (bbN_minus_c1 m) n (bbN_minus_c1_y m n H H0 H1)) with
  (app (app bbN_minus m (bbN_minus_y m H)) n (indirect_bbN_minus_y m n H H0 H1)).
rewrite valid_bbN_minus_nat.
reflexivity.
symmetry.
apply identifying_app.
replace (app bbN_minus m (bbN_minus_y m H)) with (bbN_minus_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
constructor.
assumption.
Qed.

Theorem valid_bbN_minus_BBook: forall (m n: nat) (H: m <= n),
  bbN_of_nat (minus n m) =
(app (bbN_minus_c1 (bbN_of_nat m))
   (bbN_of_nat n)
   (bbN_minus_c1_y (bbN_of_nat m) (bbN_of_nat n) (bbN_of_nat_in_bbN m) (bbN_of_nat_in_bbN n) (valid_nat_le_l m n H)
)).
Proof.
intros m n H.
apply valid_bbN_eq_r.
apply bbN_of_nat_in_bbN.
apply application_in_codomain.
rewrite valid_bbnat_minus_c1.
do 3 (rewrite  nat_2coercions_identity).
reflexivity.
Qed.

Definition bbN_minus_in_bbN (m n: Ensemble BIG_type) (H:In bbN m) (H0:In bbN n) (H1: bbN_le m n) :=
  (application_in_codomain _ _ bbN bbN (app bbN_minus m (bbN_minus_y m H)) n (indirect_bbN_minus_y m n H H0 H1)).
Hint Unfold bbN_minus_in_bbN.

(** Note: beware, now the order of the parameters is the "natural" order for subtraction *)
Definition bbN_minus_bin (n m: Ensemble BIG_type) (H:In bbN n) (H0:In bbN m) (H1: bbN_le m n) :=
  (app (app bbN_minus m (bbN_minus_y m H0)) n (indirect_bbN_minus_y m n H0 H H1)).
Hint Unfold bbN_minus_bin.

(**
  proof at the bottom of the page, BBook: section 3.5.7, p.163
  *)
Theorem BBook_plus_minus_r: forall (m n: Ensemble BIG_type) (H:In bbN m) (H0:In bbN n) (H1: bbN_le m n),
  n = bbN_plus_bin m (bbN_minus_bin n m H0 H H1) H (bbN_minus_in_bbN m n H H0 H1).
Proof.
intros m n H H0 H1.
unfold bbN_plus_bin.
apply valid_bbN_eq_r.
assumption.
apply application_in_codomain.
rewrite valid_bbN_plus_nat.
unfold bbN_minus_bin.
rewrite valid_bbN_minus_nat.
apply le_plus_minus.
apply valid_nat_le_r.
rewrite bbN_of_nat_of_bbN.
rewrite bbN_of_nat_of_bbN.
assumption.
assumption.
assumption.
Qed.

(**
  proof at the bottom of the page, BBook: section 3.5.7, p.163
  The BBook version of the proof, to check it
  *)
Theorem BBook_plus_minus_r2: forall (m n: Ensemble BIG_type) (H:In bbN m) (H0:In bbN n) (H1: bbN_le m n),
  n = bbN_plus_bin m (bbN_minus_bin n m H0 H H1) H (bbN_minus_in_bbN m n H H0 H1).
Proof.
intros m n H H0 H1.
unfold bbN_plus_bin.
replace (app (app bbN_plus m (bbN_plus_y m H)) (bbN_minus_bin n m H0 H H1)
  (indirect_bbN_plus_c1_applicable m (bbN_minus_bin n m H0 H H1) H
     (bbN_minus_in_bbN m n H H0 H1)))
  with (app (bbN_plus_c1 m) (bbN_minus_bin n m H0 H H1) (bbN_plus_c1_applicable m (bbN_minus_bin n m H0 H H1) H (bbN_minus_in_bbN m n H H0 H1))).
apply identifying_app.
unfold bbN_minus_bin.
cut (In (inverse (bbN_plus_c1 m)) (n, (app (app bbN_minus m (bbN_minus_y m H)) n (indirect_bbN_minus_y m n H H0 H1)))).
intros.
inversion H2; assumption.
replace (app (app bbN_minus m (bbN_minus_y m H)) n (indirect_bbN_minus_y m n H H0 H1))
  with (app (bbN_minus_c1 m) n (bbN_minus_c1_y m n H H0 H1)).
assert (Included (bbN_minus_c1 m) (inverse (bbN_plus_c1 m))).
red; intros.
induction H2; assumption.
apply H2; apply app_trivial_property.
apply identifying_app.
replace (app bbN_minus m (bbN_minus_y m H)) with (bbN_minus_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor; [ constructor | assumption ].
apply identifying_app.
replace (app bbN_plus m (bbN_plus_y m H)) with (bbN_plus_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor; [ constructor | assumption ].
Qed.

(**
  ** Division
  *)

(**
  definition of division, BBook: section 3.5.7, p.164, array 2, row 1

  Note/TODO: change the H0 into "In bbN m" because the non-nullity
  matters only in the theorems, not the definition
  *)
Definition bbN_div_bin (n m: Ensemble BIG_type) (H: In bbN n) (H0: In bbN1 m) :=
  bbN_min (Comprehension (fun x =>
         exists H1:(In bbN x), bbN_lt n (bbN_mult_bin m (app bbN_succ x (bbN_succ_y x H1)) (proj1 H0) (bbN_succ_in_bbN x H1)))
  ).


(**
  proof left to the reader, BBook: section 3.5.7, p.164
  *)
Theorem defining_set_of_div_not_empty: forall (n m: Ensemble BIG_type) (H: In bbN n) (H0: In bbN1 m),
  (Comprehension (fun x =>
         exists H1:(In bbN x), bbN_lt n (bbN_mult_bin m (app bbN_succ x (bbN_succ_y x H1)) (proj1 H0) (bbN_succ_in_bbN x H1)))
  ) <> (Empty_set (Ensemble BIG_type)).
Proof.
intros n m H H0.
simpl.
apply (Inhabited_not_empty _ (Comprehension (fun x =>
         exists H1:(In bbN x), bbN_lt n (bbN_mult_bin m (app bbN_succ x (bbN_succ_y x H1)) (proj1 H0) (bbN_succ_in_bbN x H1)))
  )).
pattern n.
apply BBook_principle_of_mathematical_induction.
apply Inhabited_intro with bbN_0.
exists bbN_0_in_bbN.
rewrite BBmult_one_r.
split.
induction H0.
intros H2; apply H1.
rewrite H2; intuition.
unfold bbN_0; rewrite empty_set_1; intuition.
intros.
induction H2.
induction H2.
apply Inhabited_intro with (app bbN_succ x (bbN_succ_y x x0)).
exists (bbN_succ_in_bbN x x0).
(* SC: Once again I cheat by using Coq naturals, but at least one gets the idea of the proof more easily *)
apply valid_bbN_lt_r.
apply bbN_succ_in_bbN.
unfold bbN_mult_bin; apply application_in_codomain.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
unfold bbN_mult_bin.
rewrite valid_bbN_mult_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
assert (In bbN  (bbN_mult_bin m (app bbN_succ x (bbN_succ_y x x0))
          (proj1 H0) (bbN_succ_in_bbN x x0))).
unfold bbN_mult_bin; apply application_in_codomain.
generalize (valid_bbN_lt_l _ _ H1 H3 H2); intros.
unfold bbN_mult_bin in H4.
rewrite valid_bbN_mult_nat in H4.
rewrite bbN_succ_nat_S in H4.
rewrite nat_2coercions_identity in H4.
assert (0 < nat_of_bbN m).
apply valid_nat_lt_r.
rewrite <- bbN_0_nat_zero.
rewrite bbN_of_nat_of_bbN.
split.
inversion H0.
intros H7; apply H6; intuition.
unfold bbN_0; rewrite empty_set_1; red; intros; contradiction.
inversion H0; assumption.
apply lt_le_trans with (S (nat_of_bbN m * S (nat_of_bbN x))).
apply lt_n_S; assumption.
rewrite <- mult_n_Sm.
rewrite <- mult_n_Sm.
rewrite mult_n_Sm.
(* note: here the omega tactic finally works. I don't use it to avoid too much dependencies... *)
replace (S (nat_of_bbN m * S (nat_of_bbN x))) with
  (nat_of_bbN m * S (nat_of_bbN x) + 1).
apply plus_le_compat.
intuition.
intuition.
ring.
assumption.
Qed.

Theorem bbN_div_bin_in_bbN: forall (n m: Ensemble BIG_type) (H: In bbN n) (H0: In bbN1 m),
  In bbN (bbN_div_bin n m H H0).
Proof.
intros n m H H0.
unfold bbN_div_bin.
apply bbN_min_in_bbN.
split.
constructor.
red; intros.
induction H1; assumption.
intros H1.
inversion H1.
symmetry in H3; generalize H3.
apply defining_set_of_div_not_empty; assumption.
Qed.

(**
  property (2), BBook: section 3.5.7, p.163, array 1, row 2

  n < m * succ(n/m)
  *)
Theorem bbN_div_property_2: forall (n m: Ensemble BIG_type) (H: In bbN n) (H0: In bbN1 m),
  bbN_lt n
  (bbN_mult_bin
    m
    (app bbN_succ (bbN_div_bin n m H H0) (bbN_succ_y (bbN_div_bin n m H H0) (bbN_div_bin_in_bbN n m H H0)))
    (proj1 H0)
    (bbN_succ_in_bbN (bbN_div_bin n m H H0) (bbN_div_bin_in_bbN n m H H0))
  )
.
Proof.
intros n m H H0.
assert (In (Comprehension (fun x =>
         exists H1:(In bbN x), bbN_lt n (bbN_mult_bin m (app bbN_succ x (bbN_succ_y x H1)) (proj1 H0) (bbN_succ_in_bbN x H1))))
  (bbN_div_bin n m H H0)).
unfold bbN_div_bin.
apply bbN_min_in_its_argument.
split.
constructor.
red; intros.
induction H1; assumption.
generalize (defining_set_of_div_not_empty n m H H0); intros.
intros H2; apply H1.
inversion H2; reflexivity.
induction H1.
(* SC: "cheating" by using nat terms rather than the more cumbersome bbN terms *)
apply valid_bbN_lt_r.
assumption.
unfold bbN_mult_bin; apply application_in_codomain.
unfold bbN_mult_bin.
rewrite valid_bbN_mult_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
assert (In bbN
 (bbN_mult_bin m
          (app bbN_succ (bbN_div_bin n m H H0)
             (bbN_succ_y (bbN_div_bin n m H H0) x)) (proj1 H0)
          (bbN_succ_in_bbN (bbN_div_bin n m H H0) x))
).
unfold bbN_mult_bin; apply application_in_codomain.
generalize (valid_bbN_lt_l _ _ H H2 H1); intros.
unfold bbN_mult_bin in H3.
rewrite valid_bbN_mult_nat in H3.
rewrite bbN_succ_nat_S in H3.
rewrite nat_2coercions_identity in H3.
assumption.
Qed.

(**
  property (1), BBook: section 3.5.7, p.163, array 1, row 1

  m * (n/m) <= n
  *)
Theorem bbN_div_property_1: forall (n m: Ensemble BIG_type) (H: In bbN n) (H0: In bbN1 m),
  bbN_le
  (bbN_mult_bin
    m
    (bbN_div_bin n m H H0)
    (proj1 H0)
    (bbN_div_bin_in_bbN n m H H0)
  )
    n.
Proof.
intros n m H H0.
assert ((bbN_div_bin n m H H0) = bbN_0 \/ (bbN_div_bin n m H H0) <> bbN_0).
apply classic.
induction H1.
unfold bbN_mult_bin.
apply valid_bbN_le_r.
apply application_in_codomain.
assumption.
rewrite valid_bbN_mult_nat.
rewrite H1.
rewrite <- nat_zero_bbN_0.
replace (nat_of_bbN m * 0) with 0.
intuition.
ring.
unfold bbN_mult_bin.
apply valid_bbN_le_r.
apply application_in_codomain.
assumption.
rewrite valid_bbN_mult_nat.
assert (In bbN1 (bbN_div_bin n m H H0)).
split.
apply bbN_div_bin_in_bbN.
intros H2; apply H1.
inversion H2; reflexivity.
replace (nat_of_bbN (bbN_div_bin n m H H0)) with
  (nat_of_bbN
     (app bbN_succ
         (app bbN_pred (bbN_div_bin n m H H0) (bbN_pred_y (bbN_div_bin n m H H0) H2))
         (bbN_succ_y
             (app bbN_pred (bbN_div_bin n m H H0) (bbN_pred_y (bbN_div_bin n m H H0) H2))
             (bbN_pred_in_bbN (bbN_div_bin n m H H0) H2)
         )
     )
  ).
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite valid_bbN_pred.
rewrite nat_2coercions_identity.
cut (pred (nat_of_bbN (bbN_div_bin n m H H0)) < (nat_of_bbN (bbN_div_bin n m H H0))).
generalize (pred (nat_of_bbN (bbN_div_bin n m H H0))).
intros x.
apply (proj2 (contraposition_1 (x < nat_of_bbN (bbN_div_bin n m H H0))
  (nat_of_bbN m * S x <= nat_of_bbN n)
)).
intros.
apply le_not_lt.
generalize (not_le _ _ H3); intros.
unfold bbN_div_bin.
apply valid_nat_le_r.
rewrite bbN_of_nat_of_bbN.
apply bbN_min_is_minimal.
split.
constructor; red; intros.
induction H5; assumption.
intros H5.
inversion H5.
symmetry in H7; generalize H7.
apply defining_set_of_div_not_empty; assumption.
do 2 red.
exists (bbN_of_nat_in_bbN x).
apply valid_bbN_lt_r.
assumption.
unfold bbN_mult_bin; apply application_in_codomain.
unfold bbN_mult_bin; rewrite valid_bbN_mult_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite nat_2coercions_identity.
intuition.
apply bbN_min_in_bbN.
(* TODO: theorem bbN_div_bin in Power_set1 N (copy/paste of above) *)
split.
constructor; red; intros.
induction H5; assumption.
intros H5.
inversion H5.
symmetry in H7; generalize H7.
apply defining_set_of_div_not_empty; assumption.
apply lt_pred_n_n.
apply neq_O_lt.
intros H3; apply H1.
apply valid_bbN_eq_r.
apply bbN_div_bin_in_bbN.
apply bbN_0_in_bbN.
rewrite <- nat_zero_bbN_0.
rewrite H3; reflexivity.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite valid_bbN_pred.
rewrite nat_2coercions_identity.
rewrite <- S_pred with (nat_of_bbN (bbN_div_bin n m H H0)) 0.
reflexivity.
apply neq_O_lt.
intros H3; apply H1.
apply valid_bbN_eq_r.
apply bbN_div_bin_in_bbN.
apply bbN_0_in_bbN.
rewrite <- nat_zero_bbN_0.
rewrite H3; reflexivity.
Qed.

Theorem greatest_lower_multiple_in_bbN: forall (n m: Ensemble BIG_type) (H: In bbN n) (H0: In bbN1 m),
  In bbN (bbN_mult_bin m (bbN_div_bin n m H H0) (proj1 H0) (bbN_div_bin_in_bbN n m H H0)).
Proof.
intros n m H H0.
unfold bbN_mult_bin; apply application_in_codomain.
Qed.

(**
  definition of modulo, BBook: section 3.5.7, p.164, array 2, row 2
  *)
Definition bbN_mod_bin (n m: Ensemble BIG_type) (H: In bbN n) (H0: In bbN1 m) :=
  app
    (bbN_minus_c1 (bbN_mult_bin m (bbN_div_bin n m H H0) (proj1 H0) (bbN_div_bin_in_bbN n m H H0)))
    n
    (bbN_minus_c1_y
        (bbN_mult_bin m (bbN_div_bin n m H H0) (proj1 H0) (bbN_div_bin_in_bbN n m H H0))
        n
        (greatest_lower_multiple_in_bbN n m H H0)
        H
        (bbN_div_property_1 n m H H0)
    ).

(**
  The following theorem is implicitly true by definition, but it does
  not hurt to express it.  Moreover it might be needed for to/from nat
  homomorphisms
  *)
Theorem bbN_mod_bin_in_bbN: forall (n m: Ensemble BIG_type) (H: In bbN n) (H0: In bbN1 m),
  In bbN (bbN_mod_bin n m H H0).
Proof.
intros n m H H0.
unfold bbN_mod_bin; apply application_in_codomain.
Qed.

Theorem positive_nat_coercion: forall (n: Ensemble BIG_type), In bbN1 n ->
  nat_of_bbN n > 0.
Proof.
intros n H.
induction H.
apply not_le.
intros H1; apply H0.
inversion H1.
generalize (valid_nat_eq_l _ _ H3); intros.
rewrite bbN_of_nat_of_bbN in H2.
rewrite <- bbN_0_nat_zero in H2.
rewrite H2; intuition.
assumption.
Qed.

(**
  ** Division: validity
  *)

Theorem valid_bbN_div_bin_div_nat: forall (n m: Ensemble BIG_type) (H: In bbN n) (H0: In bbN1 m),
  nat_of_bbN (bbN_div_bin n m H H0) =
  proj1_sig (quotient (nat_of_bbN m) (positive_nat_coercion m H0) (nat_of_bbN n)).
Proof.
intros n m H H0.
unfold bbN_div_bin.
rewrite valid_bbN_min.
rewrite nat_2coercions_identity.
unfold Bmin; apply iota_ind.
intros.
induction H1.
induction H1.
induction H1.
induction H1.
apply H2.
split.
do 2 red.
induction H1.
exists x.
split.
do 2 red.
exists x0.
apply valid_bbN_lt_r.
assumption.
unfold bbN_mult_bin; apply application_in_codomain.
unfold bbN_mult_bin; rewrite valid_bbN_mult_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
assert (In bbN (bbN_mult_bin m (app bbN_succ x (bbN_succ_y x x0)) (proj1 H0) (bbN_succ_in_bbN x x0))).
unfold bbN_mult_bin; apply application_in_codomain.
generalize (valid_bbN_lt_l _ _ H H5 H1); intros.
unfold bbN_mult_bin in H6; rewrite valid_bbN_mult_nat in H6.
rewrite bbN_succ_nat_S in H6.
rewrite nat_2coercions_identity in H6.
assumption.
symmetry; apply min_multiple_is_quotient.
assert (In bbN (bbN_mult_bin m (app bbN_succ x (bbN_succ_y x x0)) (proj1 H0) (bbN_succ_in_bbN x x0))).
unfold bbN_mult_bin; apply application_in_codomain.
generalize (valid_bbN_lt_l _ _ H H5 H1); intros.
unfold bbN_mult_bin in H6; rewrite valid_bbN_mult_nat in H6.
rewrite bbN_succ_nat_S in H6.
rewrite nat_2coercions_identity in H6.
assumption.
intros.
rewrite <- H4.
apply H3.
do 2 red.
exists (bbN_of_nat p).
split.
do 2 red.
exists (bbN_of_nat_in_bbN p).
apply valid_bbN_lt_r.
assumption.
unfold bbN_mult_bin; apply application_in_codomain.
unfold bbN_mult_bin; rewrite valid_bbN_mult_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite nat_2coercions_identity.
assumption.
(* TODO: hmmm, to many repetitive steps, something is weird *)
rewrite nat_2coercions_identity; reflexivity.
intros.
apply quotient_has_min_multiple_property.
induction H5.
induction H5.
induction H5.
assert (In bbN (bbN_mult_bin m (app bbN_succ x0 (bbN_succ_y x0 x1)) (proj1 H0) (bbN_succ_in_bbN x0 x1))).
unfold bbN_mult_bin; apply application_in_codomain.
generalize (valid_bbN_lt_l _ _ H H7 H5); intros.
unfold bbN_mult_bin in H8; rewrite valid_bbN_mult_nat in H8.
rewrite bbN_succ_nat_S in H8.
rewrite nat_2coercions_identity in H8.
rewrite <- H6 in H8; assumption.
(* Now unique existence of the quotient *)
exists (proj1_sig (quotient (nat_of_bbN m) (positive_nat_coercion m H0) (nat_of_bbN n))).
split.
split.
do 2 red.
exists (bbN_of_nat (proj1_sig (quotient (nat_of_bbN m) (positive_nat_coercion m H0) (nat_of_bbN n)))).
split.
do 2 red.
exists (bbN_of_nat_in_bbN (proj1_sig (quotient (nat_of_bbN m) (positive_nat_coercion m H0) (nat_of_bbN n)))).
apply valid_bbN_lt_r.
assumption.
unfold bbN_mult_bin; apply application_in_codomain.
unfold bbN_mult_bin; rewrite valid_bbN_mult_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite nat_2coercions_identity.
apply multiples_higher_than_quotient_over_divided_value.
rewrite nat_2coercions_identity; reflexivity.
intros.
apply quotient_has_min_multiple_property.
induction H1.
induction H1.
induction H1.
assert (In bbN (bbN_mult_bin m (app bbN_succ x (bbN_succ_y x x0)) (proj1 H0) (bbN_succ_in_bbN x x0))).
unfold bbN_mult_bin; apply application_in_codomain.
generalize (valid_bbN_lt_l _ _ H H3 H1); intros.
unfold bbN_mult_bin in H4; rewrite valid_bbN_mult_nat in H4.
rewrite bbN_succ_nat_S in H4.
rewrite nat_2coercions_identity in H4.
rewrite H2; assumption.
intros.
induction H1.
induction H1.
induction H1.
induction H1.
symmetry; apply min_multiple_is_quotient.
assert (In bbN (bbN_mult_bin m (app bbN_succ x (bbN_succ_y x x0)) (proj1 H0) (bbN_succ_in_bbN x x0))).
unfold bbN_mult_bin; apply application_in_codomain.
generalize (valid_bbN_lt_l _ _ H H4 H1); intros.
unfold bbN_mult_bin in H5; rewrite valid_bbN_mult_nat in H5.
rewrite bbN_succ_nat_S in H5.
rewrite nat_2coercions_identity in H5.
rewrite H3; assumption.
intros.
apply H2.
do 2 red.
exists (bbN_of_nat p).
split.
do 2 red.
exists (bbN_of_nat_in_bbN p).
apply valid_bbN_lt_r.
assumption.
unfold bbN_mult_bin; apply application_in_codomain.
unfold bbN_mult_bin; rewrite valid_bbN_mult_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite nat_2coercions_identity.
assumption.
rewrite nat_2coercions_identity; reflexivity.
split.
constructor; red; intros.
induction H1; assumption.
generalize (defining_set_of_div_not_empty n m H H0).
intros.
intros H2; apply H1.
inversion H2; intuition.
Qed.

Theorem bbN_of_pos_nat_in_bbN1: forall (m: nat) (H: m > 0),
  In bbN1 (bbN_of_nat m).
Proof.
intros m H.
split.
apply bbN_of_nat_in_bbN.
assert (m <> 0).
intros H0.
symmetry in H0.
generalize H0; apply lt_O_neq.
intuition.
intros H1; apply H0.
apply valid_nat_eq_r.
rewrite <- bbN_0_nat_zero.
inversion H1; reflexivity.
Qed.

Theorem valid_div_nat_bbN_div_bin: forall (n m: nat) (H: m > 0),
  bbN_of_nat (proj1_sig (quotient m H n)) =
  bbN_div_bin (bbN_of_nat n) (bbN_of_nat m) (bbN_of_nat_in_bbN n) (bbN_of_pos_nat_in_bbN1 m H).
Proof.
intros n m H.
apply valid_bbN_eq_r.
apply bbN_of_nat_in_bbN.
apply bbN_div_bin_in_bbN.
rewrite valid_bbN_div_bin_div_nat.
rewrite nat_2coercions_identity.
rewrite nat_2coercions_identity.
apply quotient_irrelevant_divisor_positivity.
rewrite nat_2coercions_identity.
reflexivity.
Qed.

Theorem valid_bbN_mod_bin_mod_nat: forall (n m: Ensemble BIG_type) (H: In bbN n) (H0: In bbN1 m),
  nat_of_bbN (bbN_mod_bin n m H H0) =
  proj1_sig (modulo (nat_of_bbN m) (positive_nat_coercion m H0) (nat_of_bbN n)).
Proof.
intros n m H H0.
unfold bbN_mod_bin.
rewrite valid_bbnat_minus_c1.
unfold bbN_mult_bin.
rewrite valid_bbN_mult_nat.
symmetry; apply plus_minus.
rewrite mult_comm.
replace (nat_of_bbN (bbN_div_bin n m H H0)) with
  (proj1_sig (quotient (nat_of_bbN m) (positive_nat_coercion m H0) (nat_of_bbN n))).
generalize (sigma_euclid (nat_of_bbN n) (nat_of_bbN m) (positive_nat_coercion m H0)); intros.
induction H1; assumption.
rewrite valid_bbN_div_bin_div_nat.
reflexivity.
Qed.

Theorem valid_mod_nat_bbN_mod_bin: forall (n m: nat) (H: m > 0),
  bbN_of_nat (proj1_sig (modulo m H n)) =
  bbN_mod_bin (bbN_of_nat n) (bbN_of_nat m) (bbN_of_nat_in_bbN n) (bbN_of_pos_nat_in_bbN1 m H).
Proof.
intros n m H.
apply valid_bbN_eq_r.
apply bbN_of_nat_in_bbN.
unfold bbN_mod_bin; apply application_in_codomain.
rewrite nat_2coercions_identity.
rewrite valid_bbN_mod_bin_mod_nat.
rewrite nat_2coercions_identity.
apply modulo_irrelevant_divisor_positivity.
rewrite nat_2coercions_identity.
reflexivity.
Qed.

(**
  ** Logarithms
  *)


(**
  definition of log, BBook: section 3.5.7, p.165, array 1
  *)
Definition BBook_log_bin (n m: Ensemble BIG_type) (H: In bbN n) (H0: In bbN m) :=
    bbN_min (Comprehension (fun x =>
         exists H1:(In bbN x), bbN_lt n (bbN_exp_bin m (app bbN_succ x (bbN_succ_y x H1)) H0 (bbN_succ_in_bbN x H1)))
  ).

(**
  definition of LOG, BBook: section 3.5.7, p.165, array 3
  *)
Definition BBook_LOG_bin (n m: Ensemble BIG_type) (H: In bbN n) (H0: In bbN m) :=
    bbN_min (Comprehension (fun x =>
         exists H1:(In bbN x), bbN_le n (bbN_exp_bin m x H0 H1))
  ).

(** log m (n) :
  min ({ x | x : N & n < exp m (succ x)})    m > 1
with l = log m (n):
TODO  l = 0 => n < m
TODO  l > 0 => exp m l <= n < exp m (l+1)
*)

(** LOG m (n):
  min ({x | x : N & n <= exp m x}) m > 1
with L = LOG m (n)
TODO  L = 0 => n <= 1
TODO  L > 0 => exp m (L-1) < n <= exp m L
*)

(** Relations :
TODO  L = l = 0 when n = 0
TODO  L = l when exp m l = n
TODO  L = l + 1 when exp m l < n
*)
