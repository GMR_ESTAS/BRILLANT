(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Bderived_constructs.
Require Import Brelations.
Require Import Bfunctions.

Open Scope eB_scope.

(**
  BBook: section 2.6.4, p.100, array 1, row 1
  *)
Theorem Equality_laws_inverse_01: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t)) ⇒((r∼)∼ = r).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0; inversion H0; assumption.
induction x; constructor; constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 2
  *)
Theorem Equality_laws_inverse_02: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (t ↔ u)) ⇒((p;q)∼ = (q∼;p∼)).
Proof.
intros S T U s t u p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H2.
constructor; exists x; split; constructor; intuition.

induction H0.
induction H0.
induction H0; inversion H0; inversion H1.
constructor; constructor; exists x; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 3
  *)
Theorem Equality_laws_inverse_03: forall (S: Type) (s: Ensemble S), (id(s))∼ = (id(s)).
Proof.
intros S s.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.

induction H.
inversion H.
inversion H2.
constructor; rewrite <- H3; [ constructor; intuition | intuition ].

induction H.
inversion H.
constructor; rewrite <- H0; constructor; [ constructor; intuition | reflexivity ].
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 4
  *)
Theorem Equality_laws_inverse_04: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t)) ⇒((u ◁ r)∼ = r∼ ▷ u).
Proof.
intros S T s t u r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor.
constructor; intuition.
intuition.

induction H0.
inversion H0.
constructor.
constructor; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 5
  *)
Theorem Equality_laws_inverse_05: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (v: Ensemble T), (r ∈ (s ↔ t) ∧ v ⊆ t) ⇒((r ▷ v)∼ = v ◁ r∼).
Proof.
intros S T s t r v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor.
constructor; intuition.
intuition.

induction H0.
inversion H0.
constructor.
constructor; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 6
  *)
Theorem Equality_laws_inverse_06: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t)) ⇒((u ⩤ r)∼ = r∼ ⩥ u).
Proof.
intros S T s t u r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor.
constructor; intuition.
intuition.

induction H0.
inversion H0.
constructor.
constructor; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 7
  *)
Theorem Equality_laws_inverse_07: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (v: Ensemble T), (r ∈ (s ↔ t) ∧ v ⊆ t) ⇒((r ⩥ v)∼ = v ⩤ r∼).
Proof.
intros S T s t r v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor.
constructor; intuition.
intuition.

induction H0.
inversion H0.
constructor.
constructor; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 8
  *)
Theorem Equality_laws_inverse_08: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒((p ⥷ q)∼ = (p∼ ⩥ domain(q)) ∪ q∼).
Proof.
intros S T s t p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H2.
left; constructor; [ constructor; intuition | intuition ].
right; constructor; intuition.

induction H0.
inversion H0.
inversion H1.
constructor.
constructor.
left; intuition.
inversion H0.
constructor.
constructor.
right; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 9
  *)
Theorem Equality_laws_inverse_09: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (p: Ensemble (S*T)) (q: Ensemble (U*V)), (p ∈ (s ↔ t) ∧ q ∈ (u ↔ v)) ⇒((p ∥ q)∼ = (p∼ ∥ q∼)).
Proof.
intros S T U V s t u v p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; constructor; intuition.

induction H0.
inversion H0; inversion H1.
constructor; constructor; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 10
  *)
Theorem Equality_laws_inverse_10: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒((p ∪ q)∼ = p∼ ∪ q∼).
Proof.
intros S T s t p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
left; constructor; assumption.
right; constructor; assumption.

induction H0; inversion H0.
constructor; left; intuition.
constructor; right; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 11
  *)
Theorem Equality_laws_inverse_11: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒((p ∩ q)∼ = p∼ ∩ q∼).
Proof.
intros S T s t p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
split; constructor; assumption.

induction H0; induction x.
constructor; split.
inversion H0; intuition.
inversion H1; intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 12
  *)
Theorem Equality_laws_inverse_12: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒((p ∖ q)∼ = p∼ ∖ q∼).
Proof.
intros S T s t p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
split.
constructor; intuition.
intros H3; apply H2; inversion H3; assumption.

induction H0; induction x.
constructor; split.
inversion H0; intuition.
intros H2; apply H1; constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 13
  *)
Theorem Equality_laws_inverse_13: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (x: S) (y: T), (x ∈ s ∧ y ∈ t) ⇒(((Singleton (x↦y)))∼ = (Singleton (y↦x))).
Proof.
intros S T s t x y H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

induction H0.
inversion H0.
intuition.

induction H0.
constructor.
intuition.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 14
  *)
Theorem Equality_laws_inverse_14: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ∧ r = ∅) ⇒(r∼ = ∅).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H.
induction H0.
assert (In (Empty_set _) (a,b)).
rewrite <- H1; assumption.
contradiction.

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.100, array 1, row 15
  *)
Theorem Equality_laws_inverse_15: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), (s×t)∼ = t×s.
Proof.
intros S T s t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H.
constructor; intuition.

induction H0; constructor; constructor; intuition.
Qed.


Close Scope eB_scope.
