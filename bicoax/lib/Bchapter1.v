(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)


(* This file contains notations and maybe theorems showing the equivalence
between B definitions and Coq's logical operators and such *)
Require Export Classical_Prop.
Require Export Classical_Pred_Type.

Delimit Scope eB_scope with eB.

Notation "∀ x · P" := (forall x , P) (at level 200, x ident) : eB_scope.
Notation "∃ x · P" := (exists x , P) (at level 200, x ident) : eB_scope.
Notation "x ∨ y" := (x \/ y) (at level 86, left associativity) : eB_scope.
Notation "x ∧ y" := (x /\ y) (at level 81, left associativity) : eB_scope.
Notation "x ⇒ y" := (x -> y) (at level 91, no associativity): eB_scope.
Notation "x ⇔ y" := (x <-> y) (at level 96, no associativity): eB_scope.
Notation "⌉ x" := (~x) (at level 76, right associativity) : eB_scope.
Notation "'⊤'" := True (at level 5): eB_scope.
Notation "'⊥'" := False (at level 5): eB_scope.

(* A couple is also noted (x,y) *)
Notation "x ↦ y" := (pair x y) (at level 71, right associativity): eB_scope.

(* Now theorems for traceability *)

(* TODO: tidy up this file and add missing theorems of the BBook *)

(**
  commutativity of disjunction, BBook: section 1.2.6, p.28, array 1, row 1, line 1
  *)
Theorem or_commutativity: forall (P Q: Prop), P \/ Q <-> Q \/ P.
Proof.
intuition.
Qed.

(**
  commutativity of conjunction, BBook: section 1.2.6, p.28, array 1, row 1, line 2
  *)
Theorem and_commutativity: forall (P Q: Prop), P /\ Q <-> Q /\ P.
Proof.
intuition.
Qed.

(**
  commutativity of equivalence, BBook: section 1.2.6, p.28, array 1, row 1, line 3
  *)
Theorem equiv_commutativity: forall (P Q: Prop), (P <-> Q) <-> (Q <-> P).
Proof.
intuition.
Qed.

(**
  associativity of disjunction, BBook: section 1.2.6, p.28, array 1, row 2, line 1
  *)
Theorem or_associativity: forall (P Q R: Prop),  (P \/ Q) \/ R<-> P \/ (Q \/ R).
Proof.
intuition.
Qed.

(**
  associativity of conjunction, BBook: section 1.2.6, p.28, array 1, row 2, line 2
  *)
Theorem and_associativity: forall (P Q R: Prop),  (P /\ Q) /\ R<-> P /\ (Q /\ R).
Proof.
intuition.
Qed.

(**
  associativity of equivalence, BBook: section 1.2.6, p.28, array 1, row 2, line 3

  Interesting fact: this theorem seems to require the excluded middle
  *)
Theorem equiv_associativity: forall (P Q R: Prop),  ((P <-> Q) <-> R) <-> (P <-> (Q <-> R)).
Proof.
intros P Q R.
split; intros H; induction H as [H H0].
split; intros H1.
intuition.
apply Peirce; intuition.
split; intros H1.
apply Peirce; intuition.
intuition.
Qed.

(**
  distributivity of conjunction over disjunction, BBook: section 1.2.6, p.28, array 1, row 3, line 1
  *)
Theorem or_distributivity: forall (P Q R: Prop),  R /\ (P \/ Q) <-> (R /\ P) \/ (R /\ Q).
Proof.
intuition.
Qed.

(**
  distributivity of disjunction over conjunction, BBook: section 1.2.6, p.28, array 1, row 3, line 2
  *)
Theorem and_distributivity: forall (P Q R: Prop),  R \/ (P /\ Q) <-> (R \/ P) /\ (R \/ Q).
Proof.
intuition.
Qed.

(**
  distributivity of implication over conjunction, BBook: section 1.2.6, p.28, array 1, row 3, line 3
  *)
Theorem equiv_distributivity: forall (P Q R: Prop), (R -> (P /\ Q)) <-> (R -> P) /\ (R -> Q).
Proof.
intuition.
Qed.

(**
  law of excluded middle, BBook: section 1.2.6, p.28, array 1, row 4, line 1
  *)
Theorem excluded_middle: forall (P: Prop), P \/ ~P.
Proof.
apply classic.
Qed.

(**
  idempotence of disjunction, BBook: section 1.2.6, p.28, array 1, row 5, line 1
  *)
Theorem or_idempotence: forall (P: Prop), P \/ P <-> P.
Proof.
intuition.
Qed.

(**
  idempotence of conjunction, BBook: section 1.2.6, p.28, array 1, row 5, line 2
  *)
Theorem and_idempotence: forall (P: Prop), P /\ P <-> P.
Proof.
intuition.
Qed.

(**
  absorption of disjunction, BBook: section 1.2.6, p.28, array 1, row 6, line 1
  *)
Theorem or_absorption:forall (P Q: Prop), (P \/ Q) /\ P <-> P.
Proof.
intuition.
Qed.

(**
  absorption of conjunction, BBook: section 1.2.6, p.28, array 1, row 6, line 2
  *)
Theorem and_absorption: forall (P Q: Prop), (P /\ Q) \/ P <-> P.
Proof.
intuition.
Qed.

(**
  de Morgan laws (disjunction and conjunction), BBook: section 1.2.6, p.28, array 1, row 7, line 1
  *)
Theorem de_Morgan_not_or_and: forall (P Q: Prop), ~(P \/ Q) <-> ~P /\ ~Q.
Proof.
intuition.
Qed.

(**
  de Morgan laws (conjunction and disjunction), BBook: section 1.2.6, p.28, array 1, row 7, line 2
  *)
Theorem de_Morgan_not_and_or: forall (P Q: Prop), ~(P /\ Q) <-> ~P \/ ~Q.
Proof.
intros P Q.
split; intros H.
apply not_and_or; assumption.
apply or_not_and; assumption.
Qed.

(**
  de Morgan laws (conjunction and implication), BBook: section 1.2.6, p.28, array 1, row 7, line 3
  *)
Theorem de_Morgan_not_and_implies: forall (P Q: Prop), ~(P /\ Q) <-> (P -> ~Q).
Proof.
intuition.
Qed.

(**
  de Morgan laws (implication and conjuction), BBook: section 1.2.6, p.28, array 1, row 7, line 4
  *)
Theorem de_Morgan_not_implies_and: forall (P Q: Prop), ~(P -> Q) <-> P /\ ~Q.
Proof.
intros P Q.
split; intros H.
apply imply_to_and; assumption.
intuition.
Qed.

(**
  contraposition 1, BBook: section 1.2.6, p.28, array 1, row 8, line 1
  *)
Theorem contraposition_1: forall (P Q: Prop), (P -> Q) <-> (~Q -> ~P).
Proof.
intros P Q; split; intros H.
intuition.
intros H0.
apply Peirce; intuition.
Qed.

(**
  contraposition 2, BBook: section 1.2.6, p.28, array 1, row 8, line 2
  *)
Theorem contraposition_2: forall (P Q: Prop), (~P -> Q) <-> (~Q -> P).
Proof.
intros P Q; split; intros H H0; apply Peirce; intuition.
Qed.

(**
  contraposition 3, BBook: section 1.2.6, p.28, array 1, row 8, line 3
  *)
Theorem contraposition_3: forall (P Q: Prop), (P -> ~Q) <-> (Q -> ~P).
Proof.
intuition.
Qed.

(**
  double negation, BBook: section 1.2.6, p.28, array 1, row 9, line 1
  *)
Theorem double_negation: forall (P: Prop), P <-> ~~P.
Proof.
intros P; split; intros H.
intuition.
apply NNPP; assumption.
Qed.

(**
  transitivity of implication, BBook: section 1.2.6, p.28, array 1, row 10, line 1
  *)
Theorem implies_transitivity: forall (P Q R: Prop), (P -> Q) /\ (Q -> R) -> (P -> R).
Proof.
intuition.
Qed.

(**
  monotonicity of conjunction, BBook: section 1.2.6, p.29, array 1, row 1, line 1
  *)
Theorem implies_monotonicity_and: forall (P Q R: Prop), (P -> Q) -> (P /\ R -> Q /\ R).
Proof.
intuition.
Qed.

(**
  monotonicity of disjunction, BBook: section 1.2.6, p.29, array 1, row 1, line 2
  *)
Theorem implies_monotonicity_or: forall (P Q R: Prop), (P -> Q) -> (P \/ R -> Q \/ R).
Proof.
intuition.
Qed.

(**
  monotonicity of implication (left), BBook: section 1.2.6, p.29, array 1, row 1, line 3
  *)
Theorem implies_monotonicity_implies_left: forall (P Q R: Prop), (P -> Q) -> ((R -> P) -> (R -> Q)).
Proof.
intuition.
Qed.

(**
  monotonicity of implication (right), BBook: section 1.2.6, p.29, array 1, row 1, line 4
  *)
Theorem implies_monotonicity_implies_right: forall (P Q R: Prop), (P -> Q) -> ((Q -> R) -> (P -> R)).
Proof.
intuition.
Qed.

(**
  monotonicity of negation, BBook: section 1.2.6, p.29, array 1, row 1, line 5
  *)
Theorem implies_monotonicity_not: forall (P Q: Prop), (P -> Q) -> (~Q -> ~P).
Proof.
intuition.
Qed.

(**
  equivalence of conjuctions, BBook: section 1.2.6, p.29, array 1, row 2, line 1
  *)
Theorem equivalence_and: forall (P Q R: Prop), (P <-> Q) -> (P /\ R <-> Q /\ R).
Proof.
intuition.
Qed.

(**
  equivalence of disjunctions, BBook: section 1.2.6, p.29, array 1, row 2, line 2
  *)
Theorem equivalence_or: forall (P Q R: Prop), (P <-> Q) -> (P \/ R <-> Q \/ R).
Proof.
intuition.
Qed.

(**
  equivalence of implications (left), BBook: section 1.2.6, p.29, array 1, row 2, line 3
  *)
Theorem equivalence_implies_left: forall (P Q R: Prop), (P <-> Q) -> ((R ->P) <-> (R -> Q)).
Proof.
intuition.
Qed.

(**
  equivalence of implication (right), BBook: section 1.2.6, p.29, array 1, row 2, line 4
  *)
Theorem equivalence_implies_right: forall (P Q R: Prop), (P <-> Q) -> ((P -> R) <-> (Q -> R)).
Proof.
intuition.
Qed.

(**
  equivalence of negations, BBook: section 1.2.6, p.29, array 1, row 2, line 5
  *)
Theorem equivalence_not: forall (P Q R: Prop), (P <-> Q) -> (~P <-> ~Q).
Proof.
intuition.
Qed.


(**
  commutativity of universal quantification, BBook: section 1.3.9, p.41, array 1, row 1, line 1
  *)
Theorem forall_commutativity: forall (T U: Type) (P: T -> U -> Prop), (forall (x: T), forall (y: U), P x y) <-> (forall (y: U), forall (x: T), P x y).
Proof.
intuition.
Qed.

(**
  commutativity of existential quantification, BBook: section 1.3.9, p.41, array 1, row 1, line 2
  *)
Theorem exists_commutativity: forall (T U: Type) (P: T -> U -> Prop), (exists x: T, exists y: U, P x y) <-> (exists y: U, exists x: T, P x y).
Proof.
intros T U P; split; intros H; elim H; intros x H0; elim H0; intros y H1; exists y; exists x; assumption.
Qed.

(**
  associativity of universal quantification, BBook: section 1.3.9, p.41, array 1, row 2, line 1
  *)
Theorem forall_associativity: forall (T: Type) (P Q: T -> Prop), (forall (x:T), P x /\ Q x) <-> ((forall (x:T), P x) /\ (forall (x:T), Q x)).
Proof.
intros T P Q; split; intros H.
split; intros x; elim H with x; intuition.
intuition.
Qed.

(**
  associativity of existential quantification, BBook: section 1.3.9, p.41, array 1, row 2, line 2
  *)
Theorem exists_associativity: forall (T: Type) (P Q: T -> Prop), (exists x:T, P x \/ Q x) <-> ((exists x:T, P x) \/ (exists x:T, Q x)).
Proof.
intros T P Q; split; intros H.
elim H; intros x H0; elim H0.
intros; left; exists x; assumption.
intros; right; exists x; assumption.
elim H; intros H0; elim H0; intros x H1.
exists x; left; assumption.
exists x; right; assumption.
Qed.

(**
  distributivity of univ. quantification over disjunction, BBook: section 1.3.9, p.42, array 1, row 1, line 1

  Interesting fact: this theorem seems to require the excluded middle
  *)
Theorem forall_distributivity: forall (T: Type) (P: Prop) (Q: T -> Prop), (P \/ (forall (x:T), Q x)) <-> (forall (x:T), P \/ Q x).
Proof.
intros T P Q.
split; intros H.
elim H; intros H0 x.
left; assumption.
right; apply H0.
cut (~(~P /\ ~(forall (x:T), Q x))).
intros H0.
elim (not_and_or (~P) (~(forall (x:T),  Q x))).
intros; left; apply NNPP; assumption.
intros; right; apply NNPP; assumption.
assumption.
intro H0.
cut (~ (exists x, ~(P \/ Q x))).
intros H1.
apply H1.
elim H0; intros H2 H3.
elim (not_all_ex_not T Q).
intros x H4.
exists x; intuition.
assumption.
apply all_not_not_ex.
intros x.
elim (H x); intuition.
Qed.

(**
  distributivity of exist. quantification over disjunction, BBook: section 1.3.9, p.42, array 1, row 1, line 2
  *)
Theorem exists_distributivity: forall (T: Type) (P: Prop) (Q: T -> Prop), (P /\ (exists x:T, Q x)) <-> (exists x:T, P /\ Q x).
Proof.
intros T P Q.
split; intros H.
induction H as [ H0 H1 ].
elim H1; intros x H2; exists x; intuition.
elim H; intros x H1; split; [ intuition | exists x; intuition].
Qed.

(**
  distributivity of univ. quantification over implication, BBook: section 1.3.9, p.42, array 1, row 1, line 3
  *)
Theorem forall_distributivity_implies: forall (T: Type) (P: Prop) (Q: T -> Prop), (P -> (forall (x:T), Q x)) <-> (forall (x:T), P -> Q x).
Proof.
intuition.
Qed.

(**
  de Morgan laws (univ. quantification and exist. quantification), BBook: section 1.3.9, p.42, array 1, row 2, line 1
  *)
Theorem de_Morgan_not_forall_exists: forall (T: Type) (P: T-> Prop), ~(forall (x:T), P x) <-> (exists x:T, ~(P x)).
Proof.
intros T P; split; intros H.
apply not_all_ex_not; intuition.
apply ex_not_not_all; intuition.
Qed.

(**
  de Morgan laws (exist. quantification and univ. quantification), BBook: section 1.3.9, p.42, array 1, row 2, line 2
  *)
Theorem de_Morgan_not_exists_forall: forall (T: Type) (P: T -> Prop), ~(exists x:T, P x) <-> (forall (x:T), ~(P x)).
Proof.
intros T P; split; intros H.
apply not_ex_all_not; assumption.
apply all_not_not_ex; assumption.
Qed.

(**
  de Morgan laws (univ. quantification+implication and exist. quantification+conjunction), BBook: section 1.3.9, p.42, array 1, row 2, line 3
  *)
Theorem de_Morgan_not_forall_implies_exists_and: forall (T: Type) (P Q: T -> Prop), ~(forall (x:T), P x -> Q x) <-> (exists x:T, P x /\ ~(Q x)).
Proof.
intros T P Q; split; intros H.
cut (exists x, ~~(P x /\ ~ Q x)).
intros H0; elim H0; intros x H1.
exists x; apply NNPP; assumption.
apply not_all_ex_not.
intro H0; apply H.
intros x H1; apply or_to_imply with (P x); [ | assumption ].
cut ( ~(P x) \/ ~~(Q x)).
intros H2; elim H2.
left; assumption.
right; apply NNPP; assumption.
apply not_and_or; apply H0.
elim H; intros x H0.
apply ex_not_not_all; exists x.
intuition.
Qed.

(**
  de Morgan laws (exist. quantification+conjunction and univ. quantification+implication), BBook: section 1.3.9, p.42, array 1, row 2, line 4
  *)
Theorem de_Morgan_not_exists_and_forall_implies: forall (T: Type) (P Q: T -> Prop), ~(exists x:T, P x /\ Q x) <-> (forall (x:T), P x -> ~(Q x)).
Proof.
intros T P Q; split; intros H.
intros x H0.
cut (forall x:T, ~(P x /\ Q x)).
intros H1.
intro H2.
elim H1 with x; intuition.
apply not_ex_all_not; assumption.
intro H0.
elim H0; intros x H1.
apply (H x); intuition.
Qed.

(**
  monotonicity of univ. quantification, BBook: section 1.3.9, p.42, array 1, row 3, line 1
  *)
Theorem implies_monotonicity_forall: forall (T: Type) (P Q: T -> Prop), (forall (x:T), P x -> Q x) -> ((forall (x:T), P x) -> (forall (x:T), Q x)).
Proof.
intuition.
Qed.

(**
  monotonicity of exist. quantification, BBook: section 1.3.9, p.42, array 1, row 3, line 2
  *)
Theorem implies_monotonicity_exists: forall (T: Type) (P Q: T -> Prop), (forall (x:T), P x -> Q x) -> ((exists x:T, P x) -> (exists x:T, Q x)).
Proof.
intros T P Q H H0.
elim H0; intros x H1.
exists x; intuition.
Qed.

(**
  equivalence of univ. quantification, BBook: section 1.3.9, p.42, array 1, row 4, line 1
  *)
Theorem equivalence_forall: forall (T: Type) (P Q: T -> Prop), (forall (x:T), P x <-> Q x) -> ((forall (x:T), P x) <-> (forall (x:T), Q x)).
Proof.
intros T P Q H.
split; intros H0 x.
apply (proj1 (H x)); apply H0.
apply (proj2 (H x)); apply H0.
Qed.

(**
  equivalence of exist. quantification, BBook: section 1.3.9, p.42, array 1, row 4, line 2
  *)
Theorem equivalence_exists: forall (T: Type) (P Q: T -> Prop), (forall (x:T), P x <-> Q x) -> ((exists x:T, P x) <-> (exists x:T, Q x)).
Proof.
intros T P Q H.
split; intros H0; elim H0; intros x H1.
exists x; apply (proj1 (H x)); assumption.
exists x; apply (proj2 (H x)); assumption.
Qed.

