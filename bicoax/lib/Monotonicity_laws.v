(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Brelations.
Require Import Bfunctions.

Open Scope eB_scope.

(**
  BBook: section 2.6.2, p.96, array 2, row 1
  *)
Theorem Monotonicity_laws_01: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble T), ((u ⊆ s ∧ v ⊆ t) ⇒ ((u ↔ v) ⊆ (s ↔ t))).
Proof.
intros S T s t u v H.
intros x H0.
induction H.
constructor.
constructor.
intros y H2.
induction y.
repeat (induction H0).
generalize (H0 (a,b) H2); intros H3; inversion H3.
constructor; [ apply H; intuition | apply H1; intuition ].
Qed.

(**
  BBook: section 2.6.2, p.96, array 2, row 2
  *)
Theorem Monotonicity_laws_02: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble T), ((u ⊆ s ∧ v ⊆ t) ⇒ ((u ⇸ v) ⊆ (s ⇸ t))).
Proof.
intros S T s t u v H.
intros x H0.
induction H0.
constructor.
apply Monotonicity_laws_01 with u v; intuition.
intuition.
Qed.

(**
  BBook: section 2.6.2, p.96, array 2, row 3
  *)
Theorem Monotonicity_laws_03: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble T), ((u ⊆ s ∧ v ⊆ t) ⇒ ((u ⤔ v) ⊆ (s ⤔ t))).
Proof.
intros S T s t u v H.
intros x H0.
induction H0.
constructor.
apply Monotonicity_laws_02 with u v; intuition.
intuition.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 1

  TODO: check the corresponding functional test machines to remove the Typing_set constraint, in bbench/
  *)
Theorem Monotonicity_laws_04: forall (S T: Type) (s: Ensemble S) (t v: Ensemble T), ((v ⊆ t) ⇒ (s → v) ⊆ (s → t)).
Proof.
intros S T s t v H.
intros x H0.
induction H0.
constructor.
apply Monotonicity_laws_02 with s v; intuition.
intuition.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 2
  *)
Theorem Monotonicity_laws_05: forall (S T: Type) (s: Ensemble S) (t v: Ensemble T), ((v ⊆ t) ⇒ ((s ↣ v) ⊆ (s ↣ t))).
Proof.
intros S T s t v H.
intros x H0.
induction H0.
constructor.
apply (Monotonicity_laws_04 _ _ s t v H); intuition.
induction H1.
constructor.
apply Monotonicity_laws_02 with s v; intuition.
intuition.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 3
  *)
Theorem Monotonicity_laws_06: forall (S T: Type) (s u: Ensemble S) (t: Ensemble T), ((u ⊆ s) ⇒ ((u ⤀ t) ⊆ (s ⤀ t))).
Proof.
intros S T s t u H.
intros x H0.
induction H0; constructor.
apply Monotonicity_laws_02 with t u; intuition.
intuition.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 4
  *)
Theorem Monotonicity_laws_07: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (p: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ r ∈ (s ↔ t)) ⇒((r ⊆ p) ⇒ ((r∼) ⊆ (p∼))).
Proof.
intros S T s t r p H H0.
intros x H1.
induction H1; constructor.
apply H0; intuition.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 5
  *)
Theorem Monotonicity_laws_08: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (p: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ r ∈ (s ↔ t)) ⇒((r ⊆ p) ⇒ (domain(r) ⊆ domain(p))).
Proof.
intros S T s t r p H H0.
intros x H1.
induction H1; constructor.
induction H1.
exists x; apply H0; intuition.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 6
  *)
Theorem Monotonicity_laws_09: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (p: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ r ∈ (s ↔ t)) ⇒((r ⊆ p) ⇒ (range(r) ⊆ range(p))).
Proof.
intros S T s t r p H H0.
intros x H1.
induction H1; constructor.
induction H1.
exists x; apply H0; intuition.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 7
  *)
Theorem Monotonicity_laws_10: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (T*U)) (h: Ensemble (S*T)) (k: Ensemble (T*U)), (h ∈ (s ↔ t) ∧ k ∈ (t ↔ u) ∧ p ∈ (s ↔ t) ∧ q ∈ (t ↔ u)) ⇒((p ⊆ h ∧ q ⊆ k) ⇒ ((p;q) ⊆ (h;k))).
Proof.
intros S T U s t u p q h k H H0.
intros x H1.
induction H1; constructor.
induction H1.
exists x; split; [ apply (proj1 H0); intuition | apply (proj2 H0); intuition ].
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 8
  *)
Theorem Monotonicity_laws_11: forall (S: Type) (s: Ensemble S) (u: Ensemble S), ((u ⊆ s) ⇒ id(u) ⊆ id(s)).
Proof.
intros S s u H.
intros x H0.
induction H0; constructor.
inversion H0; constructor; apply H; intuition.
assumption.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 9
  *)
Theorem Monotonicity_laws_12: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S), (p ∈ (s ↔ t) ∧ v ⊆ s ∧ r ∈ (s ↔ t) ∧ u ⊆ v) ⇒((u ⊆ v ∧ r ⊆ p) ⇒ ((u◁r) ⊆ (v◁p))).
Proof.
intros S T s t p r u v H H0.
intros x H1.
induction H1; constructor.
apply (proj2 H0); assumption.
apply (proj1 H0); assumption.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 10
  *)
Theorem Monotonicity_laws_13: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble T) (v: Ensemble T) (p: Ensemble (S*T)) (r: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ v ⊆ t ∧ r ∈ (s ↔ t) ∧ u ⊆ t) ⇒((r ⊆ p ∧ u ⊆ v) ⇒ ((r▷u) ⊆ (p▷v))).
Proof.
intros S T s t p r u v H H0.
intros x H1.
induction H1; constructor.
apply (proj1 H0); assumption.
apply (proj2 H0); assumption.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 11
  *)
Theorem Monotonicity_laws_14: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble S) (r: Ensemble (S*T)) (p: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ u ⊆ s ∧ r ∈ (s ↔ t) ∧ v ⊆ s) ⇒((v ⊆ u ∧ r ⊆ p) ⇒ ((u⩤r) ⊆ (v⩤p))).
Proof.
intros S T s t p r u v H H0.
intros x H1.
induction H1; constructor.
apply (proj2 H0); assumption.
intros H3; apply H2; apply (proj1 H0); assumption.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 12
  *)
Theorem Monotonicity_laws_15: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (r: Ensemble (S*T)) (u: Ensemble T) (v: Ensemble T), (p ∈ (s ↔ t) ∧ u ⊆ t ∧ r ∈ (s ↔ t) ∧ v ⊆ u) ⇒((r ⊆ p ∧ v ⊆ u) ⇒ ((r⩥u) ⊆ (p⩥v))).
Proof.
intros S T s t p r u v H H0.
intros x H1.
induction H1; constructor.
apply (proj1 H0); assumption.
intros H3; apply H2; apply (proj2 H0); assumption.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 13
  *)
Theorem Monotonicity_laws_16: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S), (p ∈ (s ↔ t) ∧ v ⊆ s) ⇒((r ⊆ p ∧ u ⊆ v) ⇒ ((r[u]) ⊆ (p[v]))).
Proof.
intros S T s t p r u v H H0.
intros x H1.
induction H1.
compute.
apply image_intro with x.
split; [ apply (proj2 H0); intuition | apply (proj1 H0); intuition ].
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 14
  *)
Theorem Monotonicity_laws_17: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (h: Ensemble (S*T)) (q: Ensemble (S*T)) (p: Ensemble (S*T)), (h ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒((p ⊆ h) ⇒ ((p⥷q) ⊆ (h⥷q))).
Proof.
intros S T s t h q p H H0.
intros x H1.
induction H1; constructor.
induction H1.
left; split; [ apply H0; intuition | intuition ].
right; intuition.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 15
  *)
Theorem Monotonicity_laws_18: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (S*U)) (h: Ensemble (S*T)) (k: Ensemble (S*U)), (h ∈ (s ↔ t) ∧ k ∈ (s ↔ u)) ⇒((p ⊆ h ∧ q ⊆ k) ⇒ ((p⊗q) ⊆ (h⊗k))).
Proof.
intros S T U s t u p q h k H H0.
intros x H1.
induction H1; constructor.
apply (proj1 H0); intuition.
apply (proj2 H0); intuition.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 16
  *)
Theorem Monotonicity_laws_19: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble T), ((u ⊆ s ∧ v ⊆ t) ⇒ (prj1(u,v) ⊆ prj1(s,t))).
Proof.
intros S T s t u v H.
intros x H0.
induction H0; constructor.
compute in H0 |- *.
inversion H0.
induction H4.
constructor; [ constructor; intuition | intuition ].
assumption.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 17
  *)
Theorem Monotonicity_laws_20: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble T), ((u ⊆ s ∧ v ⊆ t) ⇒ (prj2(u,v) ⊆ prj2(s,t))).
Proof.
intros S T s t u v H.
intros x H0.
induction H0; constructor.
compute in H0 |- *.
inversion H0.
induction H4.
constructor; [ constructor; intuition | intuition ].
assumption.
Qed.

(**
  BBook: section 2.6.2, p.97, array 1, row 18
  *)
Theorem Monotonicity_laws_21: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (h: Ensemble (S*T)) (k: Ensemble (U*V)) (p: Ensemble (S*T)) (q: Ensemble (U*V)), (h ∈ (s ↔ t) ∧ k ∈ (u ↔ v)) ⇒((p ⊆ h ∧ q ⊆ k) ⇒ ((p∥q) ⊆ (h∥k))).
Proof.
intros S T U V s t u v h k p q H H0.
intros x H1.
induction H1; constructor.
apply (proj1 H0); intuition.
apply (proj2 H0); intuition.
Qed.


Close Scope eB_scope.
