(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter2.
Require Import Bfinite_subsets.
Require Import Bnaturals_basics.
Require Import Bnaturals_recfuns.
Require Import Arith.
Require Import coq_missing_arith.

Open Local Scope nat_scope.

(**
  * Pure BBook version of arithmetic operations: definitions
  *)

(**
  This is not plus which is defined, but plus(m). Hence it is not
  exactly a triple but rather a relation parameterized by a natural
  number. At least this modelization is easier
  *)

Theorem total_bbN_succ: In (total_function (Power_finite BIG) (Power_finite BIG)) bbN_succ.
Proof.
unfold bbN_succ.
apply lambda_is_total.
intros.
apply augmented_set_in_finite_sets.
constructor.
constructor.
assumption.
Qed.

Theorem total_bbN_succ2: In (total_function bbN bbN) (domain_restriction bbN_succ bbN).
Proof.
constructor.
constructor.

do 2 constructor.
red; intros.
induction H.
inversion H.
split.
assumption.
do 2 constructor; intros.
induction H5.
apply H5.
unfold genat; apply abstract_lambda.
right.
rewrite H4.
red; apply image_intro with x.
split.
induction H0.
induction H0.
apply H0.
exists x1; assumption.
constructor; [ assumption | reflexivity ].

intros.
inversion H; inversion H0.
inversion H3; inversion H7.
rewrite H12; rewrite H16; reflexivity.
apply Extensionality_Ensembles; split; red; intros.
induction H.
induction H.
inversion H.
assumption.
constructor; exists (app bbN_succ x (bbN_succ_y x H)).
constructor.
apply app_trivial_property.
assumption.
Qed.

(**
  ** Addition
  *)

Section BBook_addition.
(* a is m *)
(* a_in_s is m_in_bbN *)
(* g is bbN_succ with restricted to bbN (SC: this is a difference with the BBook ?)
    Remember that bbN_succ is more than the sole successor over natural numbers.
    It is also the function upon which natural numbers are built. *)
(* g_total is total_bbN_succ2 *)
Variable m: Ensemble BIG_type.
Variable m_in_bbN: In bbN m.

(**
  definition of plus(m), BBook: section 3.5.7, p.161, array 1, row 1

  This definition is different. Let Bigs be the set of finite subsets of BIG. By definition,
  succ ∈ Bigs → Bigs (and *not* ℕ → ℕ). This means that the "g" (see section 3.5.6) is a
  total function Bigs → Bigs. We have "a" (i.e. m in this case) ∈ ℕ but also a ∈ Bigs.
  Hence the built "f" function is: f ∈ ℕ → Bigs. Hence plus(m) ∈ ℕ → Bigs, hence
  plus ∈ ℕ →(ℕ → Bigs), at best, which contradicts the desired property for "plus".

  There are two possible fixes for this:
  - Assess that the codomain of "f" is actually a subset of the definition set of "g", but I
    think the section 3.5.6 is too general for that. An alternative would be to realize this
    proof for plus only and from there it should be okay.
  - Build "plus" upon "ℕ ◁ succ", which is what we did here, hence we can reuse directly
    the results of section 3.5.6.
  *)
Definition bbN_plus_c1 := bbN_f (Ensemble BIG_type) bbN m (domain_restriction bbN_succ bbN).

Theorem bbN_plus_c1_total: In (total_function bbN bbN) bbN_plus_c1.
Proof.
unfold bbN_plus_c1.
apply bbN_f_is_total.
assumption.
apply total_bbN_succ2.
Qed.

End BBook_addition.

(**
  ** Multiplication
  *)

Section BBook_multiplication.
(* a is bbN_0 *)
(* a_in_s in bbN_0_in_bbN *)
(* g is bbN_plus_c1 *)
(* g_total is bbN_plus_c1_total *)
Variable m: Ensemble BIG_type.
Variable m_in_bbN: In bbN m.

(**
  definition of mult(m), BBook: section 3.5.7, p.161, array 1, row 2
  *)
Definition bbN_mult_c1 := bbN_f (Ensemble BIG_type) bbN bbN_0 (bbN_plus_c1 m).

Theorem bbN_mult_c1_total: In (total_function bbN bbN) bbN_mult_c1.
Proof.
unfold bbN_mult_c1.
apply bbN_f_is_total.
apply bbN_0_in_bbN.
apply bbN_plus_c1_total.
assumption.
Qed.

End BBook_multiplication.

(**
  ** Exponentiation
  *)

Section BBook_exponentiation.
(* a is the successor of bbN_0 (BBook one) *)
(* a_in_s in (bbN_succ_in_bbN bbN_0_in_bbN) *)
(* g is bbN_mult_c1 *)
(* g_total is bbN_mult_c1_total *)
Variable m: Ensemble BIG_type.
Variable m_in_bbN: In bbN m.

(**
  definition of exp(m), BBook: section 3.5.7, p.161, array 1, row 3
  *)
Definition bbN_exp_c1 := bbN_f (Ensemble BIG_type) bbN (app bbN_succ bbN_0 (bbN_succ_y bbN_0 bbN_0_in_bbN)) (bbN_mult_c1 m).

Theorem bbN_exp_c1_total: In (total_function bbN bbN) bbN_exp_c1.
Proof.
unfold bbN_exp_c1.
apply bbN_f_is_total.
apply bbN_succ_in_bbN.
apply bbN_mult_c1_total.
assumption.
Qed.

End BBook_exponentiation.

(**
  relational definition of plus, BBook: section 3.5.7, p.161
  *)
Definition bbN_plus_bb: Ensemble (Ensemble BIG_type * (Ensemble (Ensemble BIG_type * Ensemble BIG_type))) :=
   fun cpl => match cpl with | (m, plus_m_n) => (bbN_plus_c1 m) = plus_m_n end.

Definition bbN_plus := (domain_restriction bbN_plus_bb bbN).

(**
  relational definition of mult, BBook: section 3.5.7, p.161
  *)
Definition bbN_mult_bb: Ensemble (Ensemble BIG_type * (Ensemble (Ensemble BIG_type * Ensemble BIG_type))) :=
   fun cpl => match cpl with | (m, mult_m_n) => (bbN_mult_c1 m) = mult_m_n end.

Definition bbN_mult := (domain_restriction bbN_mult_bb bbN).

(**
  relational definition of exp, BBook: section 3.5.7, p.161
  *)
Definition bbN_exp_bb: Ensemble (Ensemble BIG_type * (Ensemble (Ensemble BIG_type * Ensemble BIG_type))) :=
   fun cpl => match cpl with | (m, exp_m_n) => (bbN_exp_c1 m) = exp_m_n end.

Definition bbN_exp := (domain_restriction bbN_exp_bb bbN).

(**
  Note: the above definitions are slightly different from the BBook
  because there is an additional domain restriction to bbN. Why ?
  Because actually the [m] might actually not be a natural at all,
  just any set of BIG elements. Remember that succ is defined for any
  set of BIG elements, not oney the ones produced through the use of
  the choice operator to create bbN. I don't know if it can be
  seen as a shortcoming of the definitions of natural numbers in the
  BBook, but not constraining the domain to bbN has the
  consequence that plus is not a total function over bbN but
  rather over (Full_set BIG_type).
*)


(**
  * Pure BBook version of arithmetic operations: validity
  *)

(**
  ** Addition
  *)

Theorem bbN_plus_y: forall (m: Ensemble BIG_type), In bbN m ->
  In (partial_function bbN (partial_function bbN bbN)) bbN_plus /\ In (domain bbN_plus) m.
Proof.
intros.
split.
constructor.

do 2 constructor.
red; intros.
induction x.
inversion H0.
split.
assumption.
inversion H3.
generalize (bbN_plus_c1_total a H4); intros.
induction H6; assumption.

intros.
inversion H0; inversion H1.
inversion H4; inversion H8.
reflexivity.

constructor; exists (bbN_plus_c1 m).
constructor.
constructor.
assumption.
Qed.

Theorem bbN_plus_c1_applicable: forall (m n: Ensemble BIG_type) (H: In bbN m), In bbN n ->
  In (partial_function bbN bbN) (bbN_plus_c1 m)
/\ In (domain (bbN_plus_c1 m)) n.
Proof.
intros.
generalize (bbN_plus_c1_total m H).
intros.
inversion H1.
split; [ assumption | rewrite H3; assumption ].
Qed.

Theorem indirect_bbN_plus_c1_applicable: forall (m n: Ensemble BIG_type) (H: In bbN m), In bbN n ->
  In (partial_function bbN bbN) (app bbN_plus m (bbN_plus_y m H))
/\ In (domain (app bbN_plus m (bbN_plus_y m H))) n.
Proof.
intros.
replace (app bbN_plus m (bbN_plus_y m H)) with (bbN_plus_c1 m).
apply bbN_plus_c1_applicable; assumption.
apply identifying_app.
unfold bbN_plus.
constructor.
constructor.
assumption.
Qed.

(**
  We will use the more usual shape f(a ++ b) = f(a) + f(b), f being the homomorphism
  *)
Theorem valid_bbN_plus_nat: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n),
  (nat_of_bbN (app (app bbN_plus m (bbN_plus_y m H)) n (indirect_bbN_plus_c1_applicable m n H H0)))
= (plus (nat_of_bbN m) (nat_of_bbN n)).
Proof.
intros m n H H0.
replace (nat_of_bbN
  (app (app bbN_plus m (bbN_plus_y m H)) n (indirect_bbN_plus_c1_applicable m n H H0)))
with (nat_of_bbN (app (bbN_plus_c1 m) n (bbN_plus_c1_applicable m n H H0))).
symmetry.
apply valid_nat_eq_r.
rewrite bbN_of_nat_of_bbN.
apply identifying_app.
pattern n.
apply BBook_principle_of_mathematical_induction.
rewrite <- nat_zero_bbN_0.
replace (nat_of_bbN m + 0) with (nat_of_bbN m); [ | ring ].
rewrite bbN_of_nat_of_bbN; [ | assumption ].
unfold bbN_plus_c1.
rewrite bbN_f_lfp.
left.
intuition.
assumption.
apply total_bbN_succ2.
intros.
unfold bbN_plus_c1.
rewrite bbN_f_lfp.
right.
constructor.
exists n0; split.
constructor.
constructor; apply app_trivial_property.
assumption.
constructor.
exists (bbN_of_nat (nat_of_bbN m + nat_of_bbN n0)).
split.
assumption.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite <- plus_Snm_nSm.
rewrite plus_Sn_m.
rewrite nat_S_bbN_succ.
rewrite bbN_of_nat_of_bbN.
constructor.
apply app_trivial_property.
apply bbN_of_nat_in_bbN.
apply bbN_succ_in_bbN.
assumption.
apply total_bbN_succ2.
assumption.
apply application_in_codomain.
assert (In bbN (app (bbN_plus_c1 m) n (bbN_plus_c1_applicable m n H H0))).
apply application_in_codomain.
assert (In bbN  (app (app bbN_plus m (bbN_plus_y m H)) n
     (indirect_bbN_plus_c1_applicable m n H H0))).
apply application_in_codomain.
apply valid_bbN_eq_l.
assumption.
assumption.
apply identifying_app.
replace (app bbN_plus m (bbN_plus_y m H)) with (bbN_plus_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
constructor.
assumption.
Qed.

Theorem valid_bbN_plus_bbBook: forall (m n: nat),
  bbN_of_nat (plus m n) =
(app
  (app bbN_plus (bbN_of_nat m) (bbN_plus_y (bbN_of_nat m) (bbN_of_nat_in_bbN m)))
   (bbN_of_nat n)
   (indirect_bbN_plus_c1_applicable (bbN_of_nat m) (bbN_of_nat n) (bbN_of_nat_in_bbN m) (bbN_of_nat_in_bbN n)
)).
Proof.
intros m n.
apply identifying_app.
replace (app bbN_plus (bbN_of_nat m) (bbN_plus_y (bbN_of_nat m) (bbN_of_nat_in_bbN m)))
with (bbN_plus_c1 (bbN_of_nat m)).
induction n.
unfold bbN_plus_c1.
rewrite bbN_f_lfp.
left.
rewrite <- bbN_0_nat_zero.
rewrite plus_0_r.
intuition.
apply bbN_of_nat_in_bbN.
apply total_bbN_succ2.
unfold bbN_plus_c1.
rewrite bbN_f_lfp.
right.
constructor.
exists (bbN_of_nat n).
split.
constructor.
constructor.
rewrite nat_S_bbN_succ.
rewrite bbN_of_nat_of_bbN.
apply app_trivial_property.
apply bbN_succ_in_bbN.
apply bbN_of_nat_in_bbN.
constructor.
exists (bbN_of_nat (m+n)).
split.
assumption.
rewrite <- plus_Snm_nSm.
rewrite plus_Sn_m.
rewrite nat_S_bbN_succ.
rewrite bbN_of_nat_of_bbN.
constructor.
apply app_trivial_property.
apply bbN_of_nat_in_bbN.
apply bbN_succ_in_bbN.
apply bbN_of_nat_in_bbN.
apply total_bbN_succ2.
apply identifying_app.
constructor.
constructor.
apply bbN_of_nat_in_bbN.
Qed.

(** We need this later on for multiplication *)
Theorem bbN_plus_in_bbN_plus_c1: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n),
  In (bbN_plus_c1 m) (n, app (app bbN_plus m (bbN_plus_y m H)) n (indirect_bbN_plus_c1_applicable m n H H0)).
Proof.
intros m n H H0.
replace (app (app bbN_plus m (bbN_plus_y m H)) n (indirect_bbN_plus_c1_applicable m n H H0))
  with (app (bbN_plus_c1 m) n (bbN_plus_c1_applicable m n H H0)).
apply app_trivial_property.
apply identifying_app.
replace (app bbN_plus m (bbN_plus_y m H)) with (bbN_plus_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
constructor.
assumption.
Qed.

(**
  ** Multiplication
  *)

Theorem bbN_mult_y: forall (m: Ensemble BIG_type), In bbN m ->
  In (partial_function bbN (partial_function bbN bbN)) bbN_mult /\ In (domain bbN_mult) m.
Proof.
intros.
split.
constructor.

do 2 constructor.
red; intros.
induction x.
inversion H0.
split.
assumption.
inversion H3.
generalize (bbN_mult_c1_total a H4); intros.
induction H6; assumption.

intros.
inversion H0; inversion H1.
inversion H4; inversion H8.
reflexivity.

constructor; exists (bbN_mult_c1 m).
constructor.
constructor.
assumption.
Qed.

Theorem bbN_mult_c1_y: forall (m n: Ensemble BIG_type) (H: In bbN m), In bbN n ->
  In (partial_function bbN bbN) (bbN_mult_c1 m)
/\ In (domain (bbN_mult_c1 m)) n.
Proof.
intros.
generalize (bbN_mult_c1_total m H).
intros.
inversion H1.
split; [ assumption | rewrite H3; assumption ].
Qed.

Theorem indirect_bbN_mult_c1_y: forall (m n: Ensemble BIG_type) (H: In bbN m), In bbN n ->
  In (partial_function bbN bbN) (app bbN_mult m (bbN_mult_y m H))
/\ In (domain (app bbN_mult m (bbN_mult_y m H))) n.
Proof.
intros.
replace (app bbN_mult m (bbN_mult_y m H)) with (bbN_mult_c1 m).
apply bbN_mult_c1_y; assumption.
apply identifying_app.
unfold bbN_mult.
constructor.
constructor.
assumption.
Qed.



(** We will use the more usual shape f(a ++ b) = f(a) + f(b), f being the homomorphism *)
Theorem valid_bbN_mult_nat: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n),
  (nat_of_bbN (app (app bbN_mult m (bbN_mult_y m H)) n (indirect_bbN_mult_c1_y m n H H0)))
= (mult (nat_of_bbN m) (nat_of_bbN n)).
Proof.
intros m n H H0.
replace (nat_of_bbN
  (app (app bbN_mult m (bbN_mult_y m H)) n (indirect_bbN_mult_c1_y m n H H0)))
with (nat_of_bbN (app (bbN_mult_c1 m) n (bbN_mult_c1_y m n H H0))).
symmetry.
apply valid_nat_eq_r.
rewrite bbN_of_nat_of_bbN.
apply identifying_app.
pattern n.
apply BBook_principle_of_mathematical_induction.
rewrite <- nat_zero_bbN_0.
replace (nat_of_bbN m * 0) with 0; [ | ring ].
rewrite <- bbN_0_nat_zero.
unfold bbN_mult_c1.
rewrite bbN_f_lfp.
left.
intuition.
apply bbN_0_in_bbN.
apply bbN_plus_c1_total; assumption.
intros.
unfold bbN_mult_c1.
rewrite bbN_f_lfp.
right.
constructor.
exists n0; split.
constructor.
constructor; apply app_trivial_property.
assumption.
constructor.
exists (bbN_of_nat (nat_of_bbN m * nat_of_bbN n0)).
split.
assumption.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite <- mult_n_Sm.
rewrite plus_comm.
replace (nat_of_bbN m * nat_of_bbN n0) with
  (nat_of_bbN (app (bbN_mult_c1 m) n0 (bbN_mult_c1_y m n0 H H1))).
assert (In bbN (app (bbN_mult_c1 m) n0 (bbN_mult_c1_y m n0 H H1))).
apply application_in_codomain.
rewrite <- valid_bbN_plus_nat with m (app (bbN_mult_c1 m) n0 (bbN_mult_c1_y m n0 H H1)) H H3.
rewrite bbN_of_nat_of_bbN.
rewrite bbN_of_nat_of_bbN.
apply bbN_plus_in_bbN_plus_c1.
apply application_in_codomain.
apply application_in_codomain.
apply valid_nat_eq_r.
rewrite bbN_of_nat_of_bbN.
symmetry.
apply identifying_app.
assumption.
apply application_in_codomain.
apply bbN_0_in_bbN.
apply bbN_plus_c1_total; assumption.
assumption.
apply application_in_codomain.
assert (In bbN (app (bbN_mult_c1 m) n (bbN_mult_c1_y m n H H0))).
apply application_in_codomain.
assert (In bbN (app (app bbN_mult m (bbN_mult_y m H)) n (indirect_bbN_mult_c1_y m n H H0))).
apply application_in_codomain.
apply valid_bbN_eq_l.
assumption.
assumption.
apply identifying_app.
replace (app bbN_mult m (bbN_mult_y m H)) with (bbN_mult_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
constructor.
assumption.
Qed.

Theorem valid_bbN_mult_BBook: forall (m n: nat),
  bbN_of_nat (mult m n) =
(app
  (app bbN_mult (bbN_of_nat m) (bbN_mult_y (bbN_of_nat m) (bbN_of_nat_in_bbN m)))
   (bbN_of_nat n)
   (indirect_bbN_mult_c1_y (bbN_of_nat m) (bbN_of_nat n) (bbN_of_nat_in_bbN m) (bbN_of_nat_in_bbN n)
)).
Proof.
intros m n.
apply identifying_app.
replace (app bbN_mult (bbN_of_nat m) (bbN_mult_y (bbN_of_nat m) (bbN_of_nat_in_bbN m)))
with (bbN_mult_c1 (bbN_of_nat m)).
induction n.
unfold bbN_mult_c1.
rewrite bbN_f_lfp.
left.
rewrite <- bbN_0_nat_zero.
rewrite mult_0_r.
rewrite <- bbN_0_nat_zero.
intuition.
apply bbN_0_in_bbN.
apply bbN_plus_c1_total.
apply bbN_of_nat_in_bbN.
unfold bbN_mult_c1.
rewrite bbN_f_lfp.
right.
constructor.
exists (bbN_of_nat n).
split.
constructor.
constructor.
rewrite nat_S_bbN_succ.
rewrite bbN_of_nat_of_bbN.
apply app_trivial_property.
apply bbN_succ_in_bbN.
apply bbN_of_nat_in_bbN.
constructor.
exists (bbN_of_nat (m*n)).
split.
assumption.
rewrite <- mult_n_Sm.
rewrite plus_comm.
rewrite valid_bbN_plus_bbBook.
apply bbN_plus_in_bbN_plus_c1.
apply bbN_0_in_bbN.
apply bbN_plus_c1_total.
apply bbN_of_nat_in_bbN.
apply identifying_app.
constructor.
constructor.
apply bbN_of_nat_in_bbN.
Qed.

Theorem valid_bbnat_mult_c1: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n),
  (nat_of_bbN (app (bbN_mult_c1 m) n (bbN_mult_c1_y m n H H0)))
= (mult (nat_of_bbN m) (nat_of_bbN n)).
Proof.
intros m n H H0.
replace (app (bbN_mult_c1 m) n (bbN_mult_c1_y m n H H0)) with
  (app (app bbN_mult m (bbN_mult_y m H)) n (indirect_bbN_mult_c1_y m n H H0)).
rewrite valid_bbN_mult_nat.
reflexivity.
symmetry.
apply identifying_app.
replace (app bbN_mult m (bbN_mult_y m H)) with (bbN_mult_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
constructor.
assumption.
Qed.

Theorem valid_bbN_mult_c1_BBook: forall (m n: nat),
  bbN_of_nat (mult m n) =
(app (bbN_mult_c1 (bbN_of_nat m))
   (bbN_of_nat n)
   (bbN_mult_c1_y (bbN_of_nat m) (bbN_of_nat n) (bbN_of_nat_in_bbN m) (bbN_of_nat_in_bbN n)
)).
Proof.
intros m n.
replace (app (bbN_mult_c1 (bbN_of_nat m)) (bbN_of_nat n)
  (bbN_mult_c1_y (bbN_of_nat m) (bbN_of_nat n)
     (bbN_of_nat_in_bbN m) (bbN_of_nat_in_bbN n))) with
  (app
    (app bbN_mult (bbN_of_nat m) (bbN_mult_y (bbN_of_nat m) (bbN_of_nat_in_bbN m)))
     (bbN_of_nat n)
     (indirect_bbN_mult_c1_y (bbN_of_nat m) (bbN_of_nat n) (bbN_of_nat_in_bbN m) (bbN_of_nat_in_bbN n)
)).
rewrite valid_bbN_mult_BBook.
reflexivity.
symmetry.
apply identifying_app.
replace (app bbN_mult (bbN_of_nat m) (bbN_mult_y (bbN_of_nat m) (bbN_of_nat_in_bbN m))) with
  (bbN_mult_c1 (bbN_of_nat m)).
apply app_trivial_property.
apply identifying_app.
constructor.
constructor.
apply bbN_of_nat_in_bbN.
Qed.

(** We need this later on for exponentiation  *)
Theorem bbN_mult_in_bbN_mult_c1: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n),
  In (bbN_mult_c1 m) (n, app (app bbN_mult m (bbN_mult_y m H)) n (indirect_bbN_mult_c1_y m n H H0)).
Proof.
intros m n H H0.
replace (app (app bbN_mult m (bbN_mult_y m H)) n (indirect_bbN_mult_c1_y m n H H0))
  with (app (bbN_mult_c1 m) n (bbN_mult_c1_y m n H H0)).
apply app_trivial_property.
apply identifying_app.
replace (app bbN_mult m (bbN_mult_y m H)) with (bbN_mult_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
constructor.
assumption.
Qed.

(**
  ** Exponentiation
  *)

Theorem bbN_exp_y: forall (m: Ensemble BIG_type), In bbN m ->
  In (partial_function bbN (partial_function bbN bbN)) bbN_exp /\ In (domain bbN_exp) m.
Proof.
intros.
split.
constructor.

do 2 constructor.
red; intros.
induction x.
inversion H0.
split.
assumption.
inversion H3.
generalize (bbN_exp_c1_total a H4); intros.
induction H6; assumption.

intros.
inversion H0; inversion H1.
inversion H4; inversion H8.
reflexivity.

constructor; exists (bbN_exp_c1 m).
constructor.
constructor.
assumption.
Qed.

Theorem bbN_exp_c1_applicable: forall (m n: Ensemble BIG_type) (H: In bbN m), In bbN n ->
  In (partial_function bbN bbN) (bbN_exp_c1 m)
/\ In (domain (bbN_exp_c1 m)) n.
Proof.
intros.
generalize (bbN_exp_c1_total m H).
intros.
inversion H1.
split; [ assumption | rewrite H3; assumption ].
Qed.

Theorem indirect_bbN_exp_c1_applicable: forall (m n: Ensemble BIG_type) (H: In bbN m), In bbN n ->
  In (partial_function bbN bbN) (app bbN_exp m (bbN_exp_y m H))
/\ In (domain (app bbN_exp m (bbN_exp_y m H))) n.
Proof.
intros.
replace (app bbN_exp m (bbN_exp_y m H)) with (bbN_exp_c1 m).
apply bbN_exp_c1_applicable; assumption.
apply identifying_app.
unfold bbN_mult.
constructor.
constructor.
assumption.
Qed.

Theorem valid_bbN_exp_nat: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n),
  (nat_of_bbN (app (app bbN_exp m (bbN_exp_y m H)) n (indirect_bbN_exp_c1_applicable m n H H0)))
= (exp (nat_of_bbN m) (nat_of_bbN n)).
Proof.
intros m n H H0.
replace (nat_of_bbN
  (app (app bbN_exp m (bbN_exp_y m H)) n (indirect_bbN_exp_c1_applicable m n H H0)))
with (nat_of_bbN (app (bbN_exp_c1 m) n (bbN_exp_c1_applicable m n H H0))).
symmetry.
apply valid_nat_eq_r.
rewrite bbN_of_nat_of_bbN.
apply identifying_app.
pattern n.
apply BBook_principle_of_mathematical_induction.
rewrite <- nat_zero_bbN_0.
simpl exp.
rewrite nat_S_bbN_succ.
rewrite bbN_of_nat_of_bbN.
unfold bbN_exp_c1.
rewrite bbN_f_lfp.
left.
replace (app bbN_succ (bbN_of_nat 0) (bbN_succ_y (bbN_of_nat 0) (bbN_of_nat_in_bbN 0)))
 with (app bbN_succ bbN_0 (bbN_succ_y bbN_0 bbN_0_in_bbN)).
intuition.
apply identifying_app.
apply app_trivial_property.
apply bbN_succ_in_bbN.
apply bbN_mult_c1_total; assumption.
apply bbN_succ_in_bbN.
intros.
unfold bbN_exp_c1.
rewrite bbN_f_lfp.
right.
constructor.
exists n0; split.
constructor.
constructor; apply app_trivial_property.
assumption.
constructor.
exists (bbN_of_nat (exp (nat_of_bbN m) (nat_of_bbN n0))).
split.
assumption.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
simpl exp.
replace (exp (nat_of_bbN m) (nat_of_bbN n0)) with
  (nat_of_bbN (app (bbN_exp_c1 m) n0 (bbN_exp_c1_applicable m n0 H H1))).
assert (In bbN (app (bbN_exp_c1 m) n0 (bbN_exp_c1_applicable m n0 H H1))).
apply application_in_codomain.
rewrite <- valid_bbN_mult_nat with m (app (bbN_exp_c1 m) n0 (bbN_exp_c1_applicable m n0 H H1)) H H3.
rewrite bbN_of_nat_of_bbN.
rewrite bbN_of_nat_of_bbN.
apply bbN_mult_in_bbN_mult_c1.
apply application_in_codomain.
apply application_in_codomain.
apply valid_nat_eq_r.
rewrite bbN_of_nat_of_bbN.
symmetry.
apply identifying_app.
assumption.
apply application_in_codomain.
apply bbN_succ_in_bbN.
apply bbN_mult_c1_total; assumption.
assumption.
apply application_in_codomain.
assert (In bbN (app (bbN_exp_c1 m) n (bbN_exp_c1_applicable m n H H0))).
apply application_in_codomain.
assert (In bbN (app (app bbN_exp m (bbN_exp_y m H)) n (indirect_bbN_exp_c1_applicable m n H H0))).
apply application_in_codomain.
apply valid_bbN_eq_l.
assumption.
assumption.
apply identifying_app.
replace (app bbN_exp m (bbN_exp_y m H)) with (bbN_exp_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
constructor.
assumption.
Qed.

Theorem valid_bbN_exp_bbBook: forall (m n: nat),
  bbN_of_nat (exp m n) =
(app
  (app bbN_exp (bbN_of_nat m) (bbN_exp_y (bbN_of_nat m) (bbN_of_nat_in_bbN m)))
   (bbN_of_nat n)
   (indirect_bbN_exp_c1_applicable (bbN_of_nat m) (bbN_of_nat n) (bbN_of_nat_in_bbN m) (bbN_of_nat_in_bbN n)
)).
Proof.
intros m n.
apply identifying_app.
replace (app bbN_exp (bbN_of_nat m) (bbN_exp_y (bbN_of_nat m) (bbN_of_nat_in_bbN m)))
with (bbN_exp_c1 (bbN_of_nat m)).
induction n.
unfold bbN_exp_c1.
rewrite bbN_f_lfp.
left.
rewrite <- bbN_0_nat_zero.
simpl exp.
rewrite nat_S_bbN_succ.
rewrite bbN_of_nat_of_bbN.
replace (app bbN_succ (bbN_of_nat 0) (bbN_succ_y (bbN_of_nat 0) (bbN_of_nat_in_bbN 0)))
 with (app bbN_succ bbN_0 (bbN_succ_y bbN_0 bbN_0_in_bbN)).
intuition.
apply identifying_app.
apply app_trivial_property.
apply bbN_succ_in_bbN.
apply bbN_succ_in_bbN.
apply bbN_mult_c1_total; apply bbN_of_nat_in_bbN.
unfold bbN_exp_c1.
rewrite bbN_f_lfp.
right.
constructor.
exists (bbN_of_nat n).
split.
constructor.
constructor.
rewrite nat_S_bbN_succ.
rewrite bbN_of_nat_of_bbN.
apply app_trivial_property.
apply bbN_succ_in_bbN.
apply bbN_of_nat_in_bbN.
constructor.
exists (bbN_of_nat (exp m n)).
split.
assumption.
simpl exp.
rewrite valid_bbN_mult_BBook.
apply bbN_mult_in_bbN_mult_c1.
apply bbN_succ_in_bbN.
apply bbN_mult_c1_total; apply bbN_of_nat_in_bbN.
apply identifying_app.
constructor.
constructor.
apply bbN_of_nat_in_bbN.
Qed.


Definition bbN_succ_unary (m: Ensemble BIG_type) (H:In bbN m) :=
  (app bbN_succ m (bbN_succ_y m H)).
Hint Unfold bbN_succ_unary.

Definition bbN_plus_in_bbN (m n: Ensemble BIG_type) (H:In bbN m) (H0:In bbN n) :=
  (application_in_codomain _ _ bbN bbN (app bbN_plus m (bbN_plus_y m H)) n (indirect_bbN_plus_c1_applicable m n H H0)).
Hint Unfold bbN_plus_in_bbN.

(**
  definition of +, BBook: section 3.5.7, p.162, array 1, row 1
  *)
Definition bbN_plus_bin (m n: Ensemble BIG_type) (H:In bbN m) (H0:In bbN n) :=
  (app (app bbN_plus m (bbN_plus_y m H)) n (indirect_bbN_plus_c1_applicable m n H H0)).
Hint Unfold bbN_plus_bin.

Definition bbN_mult_in_bbN (m n: Ensemble BIG_type) (H:In bbN m) (H0:In bbN n) :=
  (application_in_codomain _ _ bbN bbN (app bbN_mult m (bbN_mult_y m H)) n (indirect_bbN_mult_c1_y m n H H0)).
Hint Unfold bbN_mult_in_bbN.

(**
  definition of *, BBook: section 3.5.7, p.162, array 1, row 2
  *)
Definition bbN_mult_bin (m n: Ensemble BIG_type) (H:In bbN m) (H0:In bbN n) :=
  (app (app bbN_mult m (bbN_mult_y m H)) n (indirect_bbN_mult_c1_y m n H H0)).
Hint Unfold bbN_mult_bin.

Definition bbN_exp_in_bbN (m n: Ensemble BIG_type) (H:In bbN m) (H0:In bbN n) :=
  (application_in_codomain _ _ bbN bbN (app bbN_exp m (bbN_exp_y m H)) n (indirect_bbN_exp_c1_applicable m n H H0)).
Hint Unfold bbN_exp_in_bbN.

(**
  definition of ^, BBook: section 3.5.7, p.162, array 1, row 3
  *)
Definition bbN_exp_bin (m n: Ensemble BIG_type) (H:In bbN m) (H0:In bbN n) :=
  (app (app bbN_exp m (bbN_exp_y m H)) n (indirect_bbN_exp_c1_applicable m n H H0)).
Hint Unfold bbN_exp_bin.

(**
  Note/TODO: show that +,*,^ are total functions under the shape of
  BBook p.161 (should be easy with the previous theorems)
*)


(**
  * Pure BBook version of arithmetic operations: properties
  *)

(**
  Some, if not most of the following proofs, exploit the transcription
  to Coq's nat because it makes things much easier
  *)

(** m + 0 = 0, BBook: section 3.5.7, p.162, array 2 *)
Theorem BBplus_zero_r: forall (m: Ensemble BIG_type) (H:In bbN m),
  bbN_plus_bin m bbN_0 H bbN_0_in_bbN
  = m.
Proof.
intros m H.
unfold bbN_plus_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
assumption.
rewrite valid_bbN_plus_nat.
rewrite <- nat_zero_bbN_0.
ring.
Qed.

(** m + succ(n) = succ(m+n), BBook: section 3.5.7, p.162, array 2 *)
Theorem BBplus_succ_r: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n),
  bbN_plus_bin m (bbN_succ_unary n H0) H (bbN_succ_in_bbN n H0)
  = bbN_succ_unary (bbN_plus_bin m n H H0) (bbN_plus_in_bbN m n H H0).
Proof.
intros m n H H0.
unfold bbN_succ_unary; unfold bbN_plus_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply bbN_succ_in_bbN.
rewrite valid_bbN_plus_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite valid_bbN_plus_nat.
ring.
Qed.

(** m * 0 = 0, BBook: section 3.5.7, p.162, array 2 *)
Theorem BBmult_zero_r: forall (m: Ensemble BIG_type) (H:In bbN m),
  bbN_mult_bin m bbN_0 H bbN_0_in_bbN
  = bbN_0.
Proof.
intros m H.
unfold bbN_mult_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply bbN_0_in_bbN.
rewrite valid_bbN_mult_nat.
rewrite <- nat_zero_bbN_0.
ring.
Qed.

(** m * succ(n) = m + ( m * n ), BBook: section 3.5.7, p.162, array 2 *)
Theorem BBmult_succ_r: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n),
  bbN_mult_bin m (bbN_succ_unary n H0) H (bbN_succ_in_bbN n H0)
  = bbN_plus_bin m (bbN_mult_bin m n H H0) H (bbN_mult_in_bbN m n H H0).
Proof.
intros m n H H0.
unfold bbN_succ_unary, bbN_mult_bin, bbN_plus_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply application_in_codomain.
rewrite valid_bbN_mult_nat.
rewrite valid_bbN_plus_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite valid_bbN_mult_nat.
ring.
Qed.

(** m + 1 = succ(m), BBook: section 3.5.7, p.162, array 2 *)
Theorem BBplus_one_r: forall (m: Ensemble BIG_type) (H: In bbN m),
  bbN_plus_bin m (bbN_succ_unary bbN_0 bbN_0_in_bbN) H (bbN_succ_in_bbN bbN_0 bbN_0_in_bbN)
  = bbN_succ_unary m H.
Proof.
intros m H.
unfold bbN_plus_bin, bbN_succ_unary.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply bbN_succ_in_bbN.
rewrite valid_bbN_plus_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite <- nat_zero_bbN_0.
ring.
Qed.

(** 1 + m = succ(m), BBook: section 3.5.7, p.162, array 2 *)
Theorem BBplus_one_l: forall (m: Ensemble BIG_type) (H: In bbN m),
  bbN_plus_bin (bbN_succ_unary bbN_0 bbN_0_in_bbN) m (bbN_succ_in_bbN bbN_0 bbN_0_in_bbN) H
  = bbN_succ_unary m H.
Proof.
intros m H.
unfold bbN_plus_bin, bbN_succ_unary.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply bbN_succ_in_bbN.
rewrite valid_bbN_plus_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite <- nat_zero_bbN_0.
ring.
Qed.

(** 0 + m = m, BBook: section 3.5.7, p.162, array 2 *)
Theorem BBplus_zero_l: forall (m: Ensemble BIG_type) (H:In bbN m),
  bbN_plus_bin bbN_0 m bbN_0_in_bbN H
  = m.
Proof.
intros m H.
unfold bbN_plus_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
assumption.
rewrite valid_bbN_plus_nat.
rewrite <- nat_zero_bbN_0.
ring.
Qed.

(* m + n = n + m*)
Theorem BBplus_commutative: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n),
  bbN_plus_bin m n H H0
  = bbN_plus_bin n m H0 H.
Proof.
intros m n H H0.
unfold bbN_plus_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply application_in_codomain.
rewrite valid_bbN_plus_nat.
rewrite valid_bbN_plus_nat.
ring.
Qed.

(** m + ( n + p ) = ( m + n ) + p, BBook: section 3.5.7, p.162, array 2 *)
Theorem BBplus_associative: forall (m n p: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n) (H1: In bbN p),
  bbN_plus_bin m (bbN_plus_bin n p H0 H1) H (bbN_plus_in_bbN n p H0 H1)
  = bbN_plus_bin (bbN_plus_bin m n H H0) p (bbN_plus_in_bbN m n H H0) H1.
Proof.
intros m n p H H0 H1.
unfold bbN_plus_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply application_in_codomain.
rewrite valid_bbN_plus_nat.
rewrite valid_bbN_plus_nat.
rewrite valid_bbN_plus_nat.
rewrite valid_bbN_plus_nat.
ring.
Qed.

(** 0 * m = 0, BBook: section 3.5.7, p.162, array 2 *)
Theorem BBmult_zero_l: forall (m: Ensemble BIG_type) (H:In bbN m),
  bbN_mult_bin bbN_0 m bbN_0_in_bbN H
  = bbN_0.
Proof.
intros m H.
unfold bbN_mult_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply bbN_0_in_bbN.
rewrite valid_bbN_mult_nat.
rewrite <- nat_zero_bbN_0.
ring.
Qed.

(** m * 1 = m, BBook: section 3.5.7, p.162, array 2 *)
Theorem BBmult_one_r: forall (m: Ensemble BIG_type) (H: In bbN m),
  bbN_mult_bin m (bbN_succ_unary bbN_0 bbN_0_in_bbN) H (bbN_succ_in_bbN bbN_0 bbN_0_in_bbN)
  = m.
Proof.
intros m H.
unfold bbN_mult_bin, bbN_succ_unary.
apply valid_bbN_eq_r.
apply application_in_codomain.
assumption.
rewrite valid_bbN_mult_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite <- nat_zero_bbN_0.
ring.
Qed.

(** 1 * m = m, BBook: section 3.5.7, p.162, array 2 *)
Theorem BBmult_one_l: forall (m: Ensemble BIG_type) (H: In bbN m),
  bbN_mult_bin (bbN_succ_unary bbN_0 bbN_0_in_bbN) m (bbN_succ_in_bbN bbN_0 bbN_0_in_bbN) H
  = m.
Proof.
intros m H.
unfold bbN_mult_bin, bbN_succ_unary.
apply valid_bbN_eq_r.
apply application_in_codomain.
assumption.
rewrite valid_bbN_mult_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite <- nat_zero_bbN_0.
ring.
Qed.

(** m * n = n * m, BBook: section 3.5.7, p.162, array 2 *)
Theorem BBmult_commutative: forall (m n: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n),
  bbN_mult_bin m n H H0
  = bbN_mult_bin n m H0 H.
Proof.
intros m n H H0.
unfold bbN_mult_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply application_in_codomain.
rewrite valid_bbN_mult_nat.
rewrite valid_bbN_mult_nat.
ring.
Qed.

(** m * ( n * p ) = ( m * n ) * p, BBook: section 3.5.7, p.162, array 2 *)
Theorem BBmult_associative: forall (m n p: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n) (H1: In bbN p),
  bbN_mult_bin m (bbN_mult_bin n p H0 H1) H (bbN_mult_in_bbN n p H0 H1)
  = bbN_mult_bin (bbN_mult_bin m n H H0) p (bbN_mult_in_bbN m n H H0) H1.
Proof.
intros m n p H H0 H1.
unfold bbN_mult_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply application_in_codomain.
rewrite valid_bbN_mult_nat.
rewrite valid_bbN_mult_nat.
rewrite valid_bbN_mult_nat.
rewrite valid_bbN_mult_nat.
ring.
Qed.

(** m * ( n + p ) = ( m * n ) + ( m * p ), BBook: section 3.5.7, p.162, array 2 *)
Theorem BBmult_distributive: forall (m n p: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n) (H1: In bbN p),
  bbN_mult_bin m (bbN_plus_bin n p H0 H1) H (bbN_plus_in_bbN n p H0 H1)
  = bbN_plus_bin (bbN_mult_bin m n H H0) (bbN_mult_bin m p H H1) (bbN_mult_in_bbN m n H H0) (bbN_mult_in_bbN m p H H1).
Proof.
intros m n p H H0 H1.
unfold bbN_mult_bin, bbN_plus_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply application_in_codomain.
rewrite valid_bbN_mult_nat.
rewrite valid_bbN_plus_nat.
rewrite valid_bbN_plus_nat.
rewrite valid_bbN_mult_nat.
rewrite valid_bbN_mult_nat.
ring.
Qed.

(** n <> 0 -> 0^n = 0, BBook: section 3.5.7, p.163, array 1 *)
Theorem BBexp_zero_l: forall (m: Ensemble BIG_type) (H: In bbN m), bbN_lt bbN_0 m ->
  bbN_exp_bin bbN_0 m bbN_0_in_bbN H
  = bbN_0.
Proof.
intros m H H0.
unfold bbN_exp_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply bbN_0_in_bbN.
rewrite valid_bbN_exp_nat.
rewrite <- nat_zero_bbN_0.
apply exp_0_l.
apply valid_nat_gt_r.
unfold bbN_gt.
rewrite bbN_of_nat_of_bbN; [ | assumption ].
rewrite <- bbN_0_nat_zero; assumption.
Qed.

(** 1^m = 1, BBook: section 3.5.7, p.163, array 1 *)
Theorem BBexp_one_l: forall (m: Ensemble BIG_type) (H: In bbN m),
  bbN_exp_bin (bbN_succ_unary bbN_0 bbN_0_in_bbN) m (bbN_succ_in_bbN bbN_0 bbN_0_in_bbN) H
  = (bbN_succ_unary bbN_0 bbN_0_in_bbN).
Proof.
intros m H.
unfold bbN_exp_bin, bbN_succ_unary.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply bbN_succ_in_bbN.
rewrite valid_bbN_exp_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite <- nat_zero_bbN_0.
apply exp_1_l.
Qed.

(** m^1 = m, BBook: section 3.5.7, p.163, array 1 *)
Theorem BBexp_one_r: forall (m: Ensemble BIG_type) (H: In bbN m),
  bbN_exp_bin m (bbN_succ_unary bbN_0 bbN_0_in_bbN) H (bbN_succ_in_bbN bbN_0 bbN_0_in_bbN)
  = m.
Proof.
intros m H.
unfold bbN_exp_bin, bbN_succ_unary.
apply valid_bbN_eq_r.
apply application_in_codomain.
assumption.
rewrite valid_bbN_exp_nat.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
rewrite <- nat_zero_bbN_0.
apply exp_1_r.
Qed.

(** m^( n + p ) = m^n * m^p, BBook: section 3.5.7, p.163, array 1 *)
Theorem BBexp_distributive_l: forall (m n p: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n) (H1: In bbN p),
  bbN_exp_bin m (bbN_plus_bin n p H0 H1) H (bbN_plus_in_bbN n p H0 H1)
  = bbN_mult_bin (bbN_exp_bin m n H H0) (bbN_exp_bin m p H H1) (bbN_exp_in_bbN m n H H0) (bbN_exp_in_bbN m p H H1).
Proof.
intros m n p H H0 H1.
unfold bbN_exp_bin, bbN_mult_bin, bbN_plus_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply application_in_codomain.
rewrite valid_bbN_exp_nat.
rewrite valid_bbN_mult_nat.
rewrite valid_bbN_plus_nat.
rewrite valid_bbN_exp_nat.
rewrite valid_bbN_exp_nat.
apply exp_plus_distr_l.
Qed.


(** m^( n * p ) = (m^n)^p, BBook: section 3.5.7, p.163, array 1 *)
Theorem BBexp_assoc: forall (m n p: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n) (H1: In bbN p),
  bbN_exp_bin m (bbN_mult_bin n p H0 H1) H (bbN_mult_in_bbN n p H0 H1)
  = bbN_exp_bin (bbN_exp_bin m n H H0) p (bbN_exp_in_bbN m n H H0) H1.
Proof.
intros m n p H H0 H1.
unfold bbN_exp_bin, bbN_mult_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply application_in_codomain.
rewrite valid_bbN_exp_nat.
rewrite valid_bbN_mult_nat.
rewrite valid_bbN_exp_nat.
rewrite valid_bbN_exp_nat.
apply exp_assoc.
Qed.

(** ( m * n )^p = m^p * n^p, BBook: section 3.5.7, p.163, array 1 *)
Theorem BBexp_distributive_r: forall (m n p: Ensemble BIG_type) (H: In bbN m) (H0: In bbN n) (H1: In bbN p),
  bbN_exp_bin (bbN_mult_bin m n H H0) p (bbN_mult_in_bbN m n H H0) H1
  = bbN_mult_bin (bbN_exp_bin m p H H1) (bbN_exp_bin n p H0 H1) (bbN_exp_in_bbN m p H H1) (bbN_exp_in_bbN n p H0 H1).
Proof.
intros m n p H H0 H1.
unfold bbN_exp_bin, bbN_mult_bin.
apply valid_bbN_eq_r.
apply application_in_codomain.
apply application_in_codomain.
rewrite valid_bbN_exp_nat.
rewrite valid_bbN_mult_nat.
rewrite valid_bbN_mult_nat.
rewrite valid_bbN_exp_nat.
rewrite valid_bbN_exp_nat.
apply exp_mult_distr_r.
Qed.
