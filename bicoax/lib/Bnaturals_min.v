(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bchapter2.
Require Import Classical_sets.
Require Import Bgeneralized_union_inter.
Require Import Bnaturals_basics.
Require Import Bnaturals_ensembles.
Require Import Arith.

Open Local Scope nat_scope.

(**
  * Minimum: definitions
  *)


(**
  min, BBook: section 3.5.3, p.153, array 1
  *)
Definition bbN_min (s: Ensemble (Ensemble BIG_type)) := GenIntersection s.

Definition Bmin (s: Ensemble nat) := iota _ inhabited_nat (fun x => In s x /\ (forall y, In s y -> x <= y)).

(**
  * Minimum: theorems
  *)

(**
  property 3.5.7, BBook: section 3.5.3, p.153
  *)
Theorem bbN_min_is_minimal: forall (s: Ensemble (Ensemble BIG_type)), In (Power_set1 bbN) s ->
  forall (n: Ensemble BIG_type), In s n -> bbN_le (bbN_min s) n.
Proof.
intros.
unfold bbN_le; unfold bbN_min.
apply GenIntersection_lower_bound.
assumption.
Qed.

(** Version without the useless hypotheses *)
Theorem bbN_min_is_minimal2: forall (s: Ensemble (Ensemble BIG_type)),
  forall (n: Ensemble BIG_type), In s n -> bbN_le (bbN_min s) n.
Proof.
intros.
unfold bbN_le; unfold bbN_min.
apply GenIntersection_lower_bound.
assumption.
Qed.

(**
  property 3.5.8, BBook: section 3.5.3, p.153
  *)
Theorem paradoxical_bbN_min: forall (s: Ensemble (Ensemble BIG_type)),
  In (Power_set1 bbN) s ->
  NotIn s (bbN_min s) ->
  forall (n: Ensemble BIG_type), In bbN n -> forall (m: Ensemble BIG_type), In s m -> bbN_le n m.
Proof.
intros s H H0 n H1.
pattern n; apply BBook_principle_of_mathematical_induction.

intros.
unfold bbN_le; unfold bbN_0; rewrite empty_set_1; red; intros; contradiction.

intros n0 H2 H3 m H4.
apply  preliminary_to_trichotomy2.
induction H.
induction H.
apply H; assumption.
assert (In s n0 \/ ~(In s n0)); [ apply classic | ].
induction H5.
assert (bbN_le n0 (bbN_min s)).
unfold bbN_le; unfold bbN_min; apply GenIntersection_greatest_lower_bound with m.
apply H3; assumption.
intros; apply H3; assumption.
assert (bbN_le (bbN_min s) n0).
apply bbN_min_is_minimal; assumption.
assert (n0 = (bbN_min s)).
apply Extensionality_Ensembles; split; assumption.
rewrite H8 in H5.
contradiction.
generalize (H3 m H4); intros.
split.
intros H7; rewrite H7 in H5; contradiction.
exact H6.

assumption.
Qed.

(**
  property 3.5.9, BBook: section 3.5.3, p.155
  *)
Theorem bbN_min_in_its_argument: forall (s: Ensemble (Ensemble BIG_type)),
  In (Power_set1 bbN) s -> In s (bbN_min s).
Proof.
intros s H.
assert (exists k, In bbN k /\ In s k).
assert (Inhabited (Ensemble BIG_type) s).
apply not_empty_Inhabited.
induction H.
intuition.
induction H0.
exists x.
split.
induction H.
induction H.
apply H; assumption.
assumption.
induction H0.
apply NNPP; intros H1.
generalize (paradoxical_bbN_min s H H1); intros.
generalize (bbN_succ_in_bbN x (proj1 H0)); intros.
generalize (H2 _ H3); intros.
generalize (H4 x (proj2 H0)); intros.
rewrite (bbN_succ_nat_S x (proj1 H0)) in H5.
assert (bbN_le (bbN_of_nat (S (nat_of_bbN x))) (bbN_of_nat (nat_of_bbN x))).
rewrite bbN_of_nat_of_bbN; [ assumption | ].
inversion H0; assumption.
generalize (valid_nat_le_r _ _ H6).
apply le_Sn_n.
Qed.

(**
  * Minimum: validity
  *)

Theorem bbN_min_in_bbN: forall (s: Ensemble (Ensemble BIG_type)),
  In (Power_set1 bbN) s -> In bbN (bbN_min s).
Proof.
intros.
inversion H.
induction H0.
apply H0.
apply bbN_min_in_its_argument.
assumption.
Qed.

Theorem bbN_min_in_its_argument_coerced: forall (s: Ensemble (Ensemble BIG_type)), In (Power_set1 bbN) s ->
  In (Ensemble_nat_of_bbN s) (nat_of_bbN (bbN_min s)).
Proof.
intros s H.
assert (In (Power_set bbN) s); [ induction H; assumption | ].
apply (valid_Ensemble_bbN_belonging_l s (bbN_min s) H0 (bbN_min_in_bbN s H)).
apply bbN_min_in_its_argument.
assumption.
Qed.

Theorem bbN_min_is_minimal_coerced: forall (s: Ensemble (Ensemble BIG_type)), In (Power_set1 bbN) s ->
  forall (y:nat), In (Ensemble_nat_of_bbN s) y -> nat_of_bbN (bbN_min s) <= y.
Proof.
intros s H.
intros.
induction H0.
rewrite <- nat_2coercions_identity.
apply valid_bbN_le_l.
apply bbN_min_in_bbN; assumption.
apply bbN_of_nat_in_bbN.
apply bbN_min_is_minimal.
assumption.
rewrite (proj2 H0).
rewrite bbN_of_nat_of_bbN; [  intuition | ].
induction H; induction H.
apply H; intuition.
Qed.

Theorem valid_bbN_min: forall (s: Ensemble (Ensemble BIG_type)), In (Power_set1 bbN) s ->
  bbN_min s = bbN_of_nat (Bmin (Ensemble_nat_of_bbN s)).
Proof.
intros s H.
unfold Bmin; apply iota_ind.

intros.
induction H0.
rewrite valid_bbN_eq_r with (bbN_min s) (bbN_of_nat b).
reflexivity.
apply bbN_min_in_bbN; assumption.
apply bbN_of_nat_in_bbN.
rewrite nat_2coercions_identity.
symmetry; apply H1.
split.
apply bbN_min_in_its_argument_coerced; assumption.
apply bbN_min_is_minimal_coerced; assumption.

exists (nat_of_bbN (bbN_min s)).
split.
split.
apply bbN_min_in_its_argument_coerced; assumption.
apply bbN_min_is_minimal_coerced; assumption.
intros.
induction H0.
induction H0.
induction H0.
apply le_antisym.
rewrite H2.
assert (In bbN x).
induction H; induction H.
apply H; assumption.
apply valid_bbN_le_l.
apply bbN_min_in_bbN; assumption.
assumption.
apply bbN_min_is_minimal; assumption.
apply H1.
apply bbN_min_in_its_argument_coerced; assumption.
Qed.

Theorem valid_nat_min: forall (s: Ensemble nat), In (Power_set1 (Full_set nat)) s ->
  Bmin s = nat_of_bbN (bbN_min (Ensemble_bbN_of_nat s)).
Proof.
intros s H.
apply valid_nat_eq_r.
rewrite bbN_of_nat_of_bbN.
rewrite valid_bbN_min.
rewrite <- valid_Ensemble_bbN_of_nat.
reflexivity.
apply Power_set1_Ensemble_bbN_of_nat; assumption.
apply bbN_min_in_bbN.
apply Power_set1_Ensemble_bbN_of_nat; assumption.
Qed.

(**
  * Minimum: miscellaneous properties
  *)

(**
  We use the translation to bbN to do the proofs, but thanks to
  the translation from nat to bbN, doing the proof in N would be
  as much valid. Bmin being a iota though, I preferred the bbN_min
  version.

  Note/TODO: try proofs purely in nat
*)


(**
  BBook: section 3.5.3, p.156, array 1, row 1
  *)
Theorem Bmin_singleton: forall (a: nat), Bmin (Singleton a) = a.
Proof.
intros.
assert (Bmin (Singleton a) = nat_of_bbN (bbN_of_nat a)).
rewrite valid_nat_min.
assert (In bbN (bbN_min (Ensemble_bbN_of_nat (Singleton a)))).
apply bbN_min_in_bbN.
apply Power_set1_Ensemble_bbN_of_nat.
apply Power_set1_singleton_nat.
apply valid_bbN_eq_l.
assumption.
apply bbN_of_nat_in_bbN.
unfold bbN_min.
replace (Ensemble_bbN_of_nat (Singleton a)) with (Singleton (bbN_of_nat a)).
apply GenIntersection_Singleton.
apply valid_Ensemble_bbN_of_nat_singleton.
apply Power_set1_singleton_nat.
rewrite H.
rewrite nat_2coercions_identity; reflexivity.
Qed.

(**
  BBook: section 3.5.3, p.156, array 1, row 2
  *)
Theorem Bmin_doubleton1: forall (a b:nat), a <= b -> Bmin {= a, b =} = a.
Proof.
intros a b H.
assert (Bmin {= a, b =} = nat_of_bbN (bbN_of_nat a)).
rewrite valid_nat_min.
assert (In bbN (bbN_min (Ensemble_bbN_of_nat {= a, b =}))).
apply bbN_min_in_bbN.
apply Power_set1_Ensemble_bbN_of_nat.
apply Power_set1_union_nat.
apply Power_set1_singleton_nat.
apply Power_set1_singleton_nat.
apply valid_bbN_eq_l.
assumption.
apply bbN_of_nat_in_bbN.
replace (bbN_of_nat a) with (Intersection (bbN_of_nat a) (bbN_of_nat b)).
unfold bbN_min.
replace (Ensemble_bbN_of_nat (Union (Singleton a) (Singleton b))) with (Union (Singleton (bbN_of_nat a)) (Singleton (bbN_of_nat b))).
apply GenIntersection_Doubleton.
rewrite valid_Ensemble_bbN_of_nat_Union.
apply Extensionality_Ensembles; split; red; intros.
induction H1; induction H1.
left; apply valid_Ensemble_nat_belonging_l; intuition.
right; apply valid_Ensemble_nat_belonging_l; intuition.
induction H1.
left; rewrite valid_Ensemble_bbN_of_nat_singleton; assumption.
right; rewrite valid_Ensemble_bbN_of_nat_singleton; assumption.
apply Extensionality_Ensembles; split; red; intros.
induction H1; assumption.
assert (bbN_le (bbN_of_nat a) (bbN_of_nat b)).
apply valid_nat_le_l; assumption.
split; [ | apply H2 ]; assumption.
apply Power_set1_union_nat.
apply Power_set1_singleton_nat.
apply Power_set1_singleton_nat.
rewrite nat_2coercions_identity in H0.
assumption.
Qed.

(**
  BBook: section 3.5.3, p.156, array 1, row 3
  *)
Theorem Bmin_doubleton2: forall (a b:nat), b <= a -> Bmin {= a, b =} = b.
Proof.
intros a b H.
assert ( {= a, b =} = {= b, a =} ).
intuition.
rewrite H0.
apply Bmin_doubleton1; assumption.
Qed.


(**
  BBook: section 3.5.3, p.156, array 1, row 4
  *)
Theorem Bmin_union: forall (a b: Ensemble nat), In (Power_set1 (Full_set nat)) a -> In (Power_set1 (Full_set nat)) b ->
Bmin (Union a b) = (Bmin {= (Bmin a), (Bmin b) =}).
Proof.
intros a b H H0.
rewrite (valid_nat_min (Union a b)).
rewrite (valid_nat_min {= (Bmin a), (Bmin b) =}).
assert (
(bbN_min (Ensemble_bbN_of_nat (Union a b))) =
(bbN_min (Ensemble_bbN_of_nat {=Bmin a, Bmin b=}))
).
rewrite valid_Ensemble_bbN_of_nat_Union.
replace (Ensemble_bbN_of_nat {=Bmin a, Bmin b=})
  with ({= bbN_min (Ensemble_bbN_of_nat a), bbN_min (Ensemble_bbN_of_nat b) =}).
unfold bbN_min.
apply GenIntersection_Union2 with BIG.
constructor.
constructor; red; intros; constructor; red; intros; constructor.
assert (~((Ensemble_bbN_of_nat a) = (Empty_set _))).
rewrite <- Ensemble_bbN_of_nat_empty_coercion.
induction H.
intros H2; apply H1.
assert (a = (Empty_set nat)).
apply Ensemble_bbN_of_nat_injective; assumption.
intuition.
intros H2; apply H1; induction H2; intuition.
constructor.
constructor; red; intros; constructor; red; intros; constructor.
assert (~((Ensemble_bbN_of_nat b) = (Empty_set _))).
rewrite <- Ensemble_bbN_of_nat_empty_coercion.
induction H0.
intros H2; apply H1.
assert (b = (Empty_set nat)).
apply Ensemble_bbN_of_nat_injective; assumption.
intuition.
intros H2; apply H1; induction H2; intuition.
rewrite valid_Ensemble_bbN_of_nat_Union.
rewrite <- valid_Ensemble_bbN_of_nat_singleton.
rewrite <- valid_Ensemble_bbN_of_nat_singleton.
rewrite valid_bbN_min.
rewrite valid_bbN_min.
rewrite <-  valid_Ensemble_bbN_of_nat.
rewrite <-  valid_Ensemble_bbN_of_nat.
reflexivity.
apply Power_set1_Ensemble_bbN_of_nat; assumption.
apply Power_set1_Ensemble_bbN_of_nat; assumption.
rewrite H1; reflexivity.
apply Power_set1_union_nat.
apply Power_set1_singleton_nat.
apply Power_set1_singleton_nat.
apply Power_set1_union_nat; assumption.
Qed.

(**
  BBook: section 3.5.3, p.156, array 1, row 5

  This definition is similar to the definition of Add, too
  *)
Theorem Bmin_union_singleton: forall (a: Ensemble nat) (x: nat), In (Power_set1 (Full_set nat)) a ->
Bmin (Union a (Singleton x)) = (Bmin {= (Bmin a), x =}).
Proof.
intros a x H.
replace ({=Bmin a, x=}) with ({=Bmin a, (Bmin (Singleton x))=}).
apply Bmin_union.
assumption.
constructor.
constructor.
red; intros; constructor.
intros H0.
assert ((Singleton x) <> (Empty_set nat)).
apply Inhabited_not_empty.
apply Inhabited_intro with (x := x); intuition.
induction H0.
apply H1; reflexivity.
replace (Bmin (Singleton x)) with x.
reflexivity.
symmetry; apply Bmin_singleton.
Qed.

(**
  * Strong induction principle
  *)

Require Import Wf_nat.

(** Now the strong induction principle is terribly easy to prove :-) *)

(**
  theorem 3.5.2, BBook: section 3.5.4, p.156

  Super-easy version
  *)
Theorem strong_induction_principle: forall (P: nat -> Prop),
  (forall (n:nat), (forall (m: nat), m < n -> (P m)) -> (P n)) -> (forall (n:nat), P n).
Proof.
intros.
apply lt_wf_ind; assumption.
Qed.

(**
  theorem 3.5.2, BBook: section 3.5.4, p.156

  BBook version but using translation to nat
  *)
Theorem strong_induction_principle_BBook: forall (P: Ensemble BIG_type -> Prop),
  (forall (n:Ensemble BIG_type), In bbN n -> (forall (m: Ensemble BIG_type), In bbN m -> bbN_lt m n -> (P m)) -> (P n)) ->
  (forall (n:Ensemble BIG_type), In bbN n -> P n).
Proof.
intros P H.
intros n H0.
assert (P (bbN_of_nat (nat_of_bbN n))).
apply strong_induction_principle with (P := fun x => P (bbN_of_nat x)).
intros.
apply H.
apply bbN_of_nat_in_bbN.
intros.
rewrite <- bbN_of_nat_of_bbN; [ | assumption ].
apply H1.
rewrite <- nat_2coercions_identity.
assert (In bbN (bbN_of_nat n0)); [ apply bbN_of_nat_in_bbN | ].
apply valid_bbN_lt_l; assumption.
rewrite <- bbN_of_nat_of_bbN; assumption.
Qed.

(**
  theorem 3.5.2, BBook: section 3.5.4, p.156

  BBook version (for checking the proof)
  *)
Theorem strong_induction_principle_BBook_just_checking: forall (P: Ensemble BIG_type -> Prop),
  (forall (n:Ensemble BIG_type), In bbN n -> (forall (m: Ensemble BIG_type), In bbN m -> bbN_lt m n -> (P m)) -> (P n)) ->
  (forall (n:Ensemble BIG_type), In bbN n -> P n).
Proof.
intros P H.
apply NNPP; intros H0.
generalize (not_all_ex_not _ _ H0).
intros.
assert (exists n, In bbN n /\ ~(P n)).
induction H1.
exists x.
apply imply_to_and; assumption.
clear H0 H1.
assert ((Comprehension (fun n => In bbN n /\ ~(P n))) <> (Empty_set (Ensemble BIG_type))).
apply (Inhabited_not_empty _ (Comprehension (fun n => In bbN n /\ ~(P n)))).
induction H2.
apply Inhabited_intro with x.
assumption.
pose (z := (Comprehension (fun n => In bbN n /\ ~(P n)))).
cut (In (Power_set1 bbN) z); [ intros | ].
assert (In bbN (bbN_min z)).
apply bbN_min_in_bbN; assumption.
generalize (H (bbN_min z) H3); intros.
generalize (proj1 (contraposition_1 _ _) H4).
intros.
assert (~(P (bbN_min z)) -> (exists m, In z m /\ bbN_lt m (bbN_min z))).
intros.
generalize (H5 H6); intros.
generalize (not_all_ex_not _ _ H7); intros.
induction H8.
exists x.
generalize (proj1 (de_Morgan_not_implies_and _ _) H8); intros.
induction H9.
generalize (proj1 (de_Morgan_not_implies_and _ _) H10); intros.
induction H11.
split; [ | assumption ].
red; red; red.
split; assumption.
assert (In z (bbN_min z)).
apply bbN_min_in_its_argument; assumption.
do 3 (red in H7).
induction H7.
generalize (H6 H8); intros.
induction H9.
induction H9.
cut (forall n, In z n -> bbN_le (bbN_min z) n).
apply ex_not_not_all.
exists x.
intros H11.
generalize (H11 H9); intros.
unfold bbN_lt in H10.
induction H10.
unfold bbN_le in H12.
assert ((bbN_min z) = x).
apply Extensionality_Ensembles; split; intuition.
symmetry in H14; contradiction.
intros.
apply bbN_min_is_minimal; assumption.
constructor.
constructor.
red; intros.
induction H1; assumption.
assert (z <> (Empty_set (Ensemble BIG_type))).
assumption.
intros H3; apply H1.
inversion H3; reflexivity.
Qed.
