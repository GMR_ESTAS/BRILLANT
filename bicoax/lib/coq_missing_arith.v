(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is distributed under the terms of the
    GNU Lesser General Public License Version 2.1

    I made an exception here wrt the rest of BiCoax so as its content can
    be integrated into Coq.
*)

(**
  Note: all modules loaded do not appear in the header of this file but are put
  throughout it, grep for "Require"
  *)
Require Import Arith.

(**
  * Addition
  *)

(** This part shall go into Arith/Plus.v *)

Lemma plus_reg_r : forall n m p, n + p = m + p -> n = m.
Proof.
  intros n m p H.
  rewrite plus_comm in H; symmetry in H; rewrite plus_comm in H; symmetry in H.
  apply plus_reg_l with p; assumption.
Qed.

(**
  * Multiplication
  *)

(** This part shall go into Arith/Mult.v *)

Lemma mult_reg_l : forall n m p, p > 0 -> p * n = p * m -> n = m.
Proof.
  double induction n m.
  reflexivity.
  intros.
  rewrite mult_0_r in H1.
  rewrite mult_comm in H1.
  simpl in H1.
  symmetry in H1; generalize (plus_is_O _ _ H1); intros H2; induction H2.
  absurd (p <= 0); [ apply gt_not_le; assumption | rewrite H2; intuition ].
  intros.
  rewrite mult_0_r in H1.
  rewrite mult_comm in H1.
  simpl in H1.
  generalize (plus_is_O _ _ H1); intros H2; induction H2.
  absurd (p <= 0); [ apply gt_not_le; assumption | rewrite H2; intuition ].
  intros.
  assert (n1 = n0).
  apply H0 with p.
  assumption.
  apply plus_reg_l with p.
  replace (p * S n0) with (p + p * n0) in H2.
  rewrite mult_comm in H2; simpl in H2.
  rewrite mult_comm in H2; assumption.
  symmetry; rewrite mult_comm; simpl.
  rewrite mult_comm; reflexivity.
  rewrite H3; reflexivity.
Qed.

Lemma mult_reg_r : forall n m p, p > 0 -> n * p = m * p -> n = m.
Proof.
  intros n m p H H0.
  rewrite mult_comm in H0; symmetry in H0; rewrite mult_comm in H0; symmetry in H0.
  apply mult_reg_l with p; assumption.
Qed.


(**
  * Exponentiation
  *)

(** This part shall go into Init/Peano.v *)

Fixpoint exp (m n:nat) {struct n} : nat :=
  match n with
  | O => 1
  | S p => m * (m ^ p)
  end
where "m ^ n" := (exp m n) : nat_scope.

Hint Resolve (f_equal2 exp): core v62.

Lemma exp_n_O : forall n:nat, 1 = n ^ 0.
Proof.
  induction n; simpl in |- *; auto.
Qed.
Hint Resolve exp_n_O: core v62.

Lemma exp_n_Sm : forall n m:nat, n * m + n = n * S m.
Proof.
  intros; induction n as [| p H]; simpl in |- *; auto.
  destruct H; rewrite <- plus_n_Sm; apply (f_equal S).
  pattern m at 1 3 in |- *; elim m; simpl in |- *; auto.
Qed.
Hint Resolve exp_n_Sm: core v62.

(** This part shall go into Arith/Exp.v *)

Open Local Scope nat_scope.

Implicit Types m n p : nat.

(** ** Zero property *)

Lemma exp_0_r : forall n, n ^ 0 = 1.
Proof.
  intro; symmetry  in |- *; apply exp_n_O.
Qed.

Lemma exp_0_l : forall n, n > 0 -> 0 ^ n = 0.
Proof.
  intros; induction n as [| p H0].
  absurd (0 > 0); [ apply gt_irrefl | assumption ].
  simpl in |- *; reflexivity.
Qed.

(** ** 1 properties *)

Lemma exp_1_l : forall n, 1 ^ n = 1.
Proof.
  intros; induction n as [| p H].
  apply exp_0_r.
  simpl in |- *; rewrite plus_0_r; assumption.
Qed.
Hint Resolve exp_1_l: arith v62.

Lemma exp_1_r : forall n, n ^ 1 = n.
Proof.
  simpl; apply mult_1_r.
Qed.
Hint Resolve exp_1_r: arith v62.

(** ** Distributivity *)

Lemma exp_plus_distr_l : forall n m p, n ^ (m + p) = n ^ m * n ^ p.
Proof.
  intros n m p.
  induction m.
  rewrite plus_0_l; rewrite exp_0_r; rewrite mult_1_l; reflexivity.
  simpl; rewrite IHm; auto with arith.
Qed.

Lemma exp_mult_distr_r : forall n m p, (n * m) ^ p = n ^ p * m ^ p.
Proof.
  intros n m p.
  induction p.
  do 3 rewrite exp_0_r; auto with arith.
  simpl; rewrite IHp.
  rewrite mult_assoc with (n * n^p) m (m^p).
  rewrite mult_assoc_reverse with n (n^p) m.
  rewrite mult_comm with (n^p) m.
  rewrite mult_assoc with n m (n^p).
  auto with arith.
Qed.

(** ** Associativity *)

Lemma exp_assoc : forall n m p, n ^ (m * p) = (n ^ m) ^ p.
Proof.
  intros; induction m; intros; simpl in |- *.
  symmetry; apply exp_1_l.
  rewrite exp_mult_distr_r.
  rewrite exp_plus_distr_l.
  rewrite IHm.
  reflexivity.
Qed.
Hint Resolve exp_assoc: arith v62.

Theorem positive_exp: forall n m, n > 0 -> 0 < exp n m.
Proof.
intros n m H.
induction m.
rewrite exp_0_r; intuition.
simpl.
replace 0 with (0 * (exp n m)).
apply mult_lt_compat_r; intuition.
intuition.
Qed.


(** This part shall go into Euclid.v *)

Require Import Euclid.

(**
  * Division
  *)

Definition is_euclidian_division (n m q r: nat) := n = q * m + r /\ m > r.

Theorem unique_quotient: forall (n m q1 r1 q2 r2: nat),
  is_euclidian_division n m q1 r1 ->
  is_euclidian_division n m q2 r2 ->
  q1 = q2.
Proof.
intros n m q1 r1 q2 r2 H H0.
induction H; induction H0.
assert (q1 <= q2 \/ q2 < q1); [ apply le_or_lt | ].
induction H3.
assert (q2 <= q1 \/ q1 < q2); [ apply le_or_lt | ].
induction H4.
intuition.
absurd (m > r1) ; [ apply le_not_gt | assumption ].
apply plus_le_reg_l with (q1 * m).
rewrite <- H; rewrite H0.
apply le_trans with (q2 * m + 0).
replace (q1 * m + m) with (S q1 * m).
replace (q2 * m + 0) with (q2 * m).
apply mult_le_compat_r.
apply lt_le_S; assumption.
intuition.
replace (q1 * m) with (m * q1).
rewrite mult_n_Sm.
rewrite mult_comm; reflexivity.
rewrite mult_comm; reflexivity.
intuition.
absurd (m > r2) ; [ apply le_not_gt | assumption ].
apply plus_le_reg_l with (q2 * m).
rewrite <- H0; rewrite H.
apply le_trans with (q1 * m + 0).
replace (q2 * m + m) with (S q2 * m).
replace (q1 * m + 0) with (q1 * m).
apply mult_le_compat_r.
apply lt_le_S; assumption.
intuition.
replace (q2 * m) with (m * q2).
rewrite mult_n_Sm.
rewrite mult_comm; reflexivity.
rewrite mult_comm; reflexivity.
intuition.
Qed.

Theorem unique_modulo: forall (n m q1 r1 q2 r2: nat),
  is_euclidian_division n m q1 r1 ->
  is_euclidian_division n m q2 r2 ->
  r1 = r2.
Proof.
intros n m q1 r1 q2 r2 H H0.
generalize (unique_quotient n m q1 r1 q2 r2 H H0); intros.
induction H; induction H0.
apply plus_reg_l with (q2 * m).
replace (q2 * m + r1) with (q1 * m + r1).
rewrite <- H; rewrite <- H0; reflexivity.
rewrite H1; reflexivity.
Qed.

Theorem exact_euclidian_division: forall (m q r: nat), m > r -> is_euclidian_division (q * m + r) m q r.
Proof.
intros m q r H.
split; [ reflexivity | assumption ].
Qed.

Theorem explicit_euclidian: forall (m q r q0 r0: nat),
  m > r ->
  is_euclidian_division (q * m + r) m q0 r0 ->
  q = q0 /\ r = r0.
Proof.
intros m q r q0 r0 H H0.
generalize (exact_euclidian_division m q r H); intros.
split.
apply unique_quotient with (q*m+r) m r r0.
apply exact_euclidian_division; assumption.
assumption.
apply unique_modulo with (q*m+r) m q q0.
apply exact_euclidian_division; assumption.
assumption.
Qed.

Theorem sigma_euclid: forall (n m: nat) (H: m > 0),
  is_euclidian_division n m (proj1_sig (quotient m H n)) (proj1_sig (modulo m H n)).
Proof.
intros n m H.
assert (exists r : nat, n = (proj1_sig (quotient m H n)) * m + r /\ m > r).
apply  (proj2_sig (quotient m H n)).
assert (exists q : nat, n = q * m + (proj1_sig (modulo m H n)) /\ m > (proj1_sig (modulo m H n))).
apply  (proj2_sig (modulo m H n)).
do 2 (induction H0).
do 2 (induction H1).
split.
assert (is_euclidian_division n m (proj1_sig (quotient m H n)) x).
split; assumption.
assert (is_euclidian_division n m x0 (proj1_sig (modulo m H n))).
split; assumption.
assert ((proj1_sig (quotient m H n)) = x0).
apply unique_quotient with n m x (proj1_sig (modulo m H n)); assumption.
rewrite H6; assumption.
assumption.
Qed.

Theorem multiples_higher_than_quotient_over_divided_value: forall (n m: nat) (H: m > 0),
  n < m * (S (proj1_sig (quotient m H n))).
Proof.
intros n m H.
generalize (sigma_euclid n m H); intros.
induction H0.
assert (proj1_sig (quotient m H n) * m + proj1_sig (modulo m H n) < m * S (proj1_sig (quotient m H n))).
rewrite <- mult_n_Sm.
rewrite mult_comm.
apply plus_lt_compat_l.
intuition.
rewrite <- H0 in H2; assumption.
Qed.

Theorem quotient_has_min_multiple_property: forall (n m: nat) (H: m > 0),
  (forall (p: nat), n < m * (S p) -> (proj1_sig (quotient m H n)) <= p).
Proof.
intros n m H p H0.
case (le_or_lt p (proj1_sig (quotient m H n))); intros H1.
case (le_lt_or_eq _ _ H1); intros H2; auto.
absurd (m * (proj1_sig (quotient m H n)) <= n).
apply lt_not_le.
apply lt_le_trans with (m * S p); [ assumption | ].
apply mult_le_compat_l.
apply lt_le_S; assumption.
generalize (sigma_euclid n m H); intros.
induction H3.
assert (m * proj1_sig (quotient m H n) <= proj1_sig (quotient m H n) * m + proj1_sig (modulo m H n)).
rewrite mult_comm.
apply le_plus_l.
rewrite <- H3 in H5; assumption.
rewrite H2; intuition.
intuition.
Qed.

Theorem min_multiple_is_quotient: forall (n m q: nat) (H: m > 0),
  n < m * (S q) ->
  (forall (p: nat), n < m * (S p) -> q <= p) ->
  q = (proj1_sig (quotient m H n)).
Proof.
intros n m q H H0 H1.
apply le_antisym.
apply H1.
apply multiples_higher_than_quotient_over_divided_value.
apply quotient_has_min_multiple_property.
assumption.
Qed.

Theorem quotient_irrelevant_divisor_positivity: forall (n m1 m2: nat) (H: m1 > 0) (H0: m2 > 0),
  m1 = m2 -> (proj1_sig (quotient m1 H n)) = (proj1_sig (quotient m2 H0 n)).
Proof.
intros n m1 m2 H H0 H1.
assert (exists r : nat, n = (proj1_sig (quotient m1 H n)) * m1 + r /\ m1 > r).
apply  (proj2_sig (quotient m1 H n)).
induction H2; induction H2.
assert (exists r : nat, n = (proj1_sig (quotient m2 H0 n)) * m2 + r /\ m2 > r).
apply  (proj2_sig (quotient m2 H0 n)).
induction H4; induction H4.
assert (proj1_sig (quotient m1 H n) * m1 + x = proj1_sig (quotient m2 H0 n) * m2 + x0).
rewrite <- H2; rewrite <- H4; reflexivity.
assert (x = x0).
apply unique_modulo with n m2 (proj1_sig (quotient m1 H n)) (proj1_sig (quotient m2 H0 n)).
split.
replace (proj1_sig (quotient m1 H n) * m1) with
  (proj1_sig (quotient m1 H n) * m2) in H2.
rewrite <- H2; reflexivity.
intuition.
rewrite <- H1; assumption.
split.
rewrite <- H4; reflexivity.
assumption.
rewrite H7 in H6.
replace (proj1_sig (quotient m1 H n) * m1) with
  (proj1_sig (quotient m1 H n) * m2) in H6.
apply mult_reg_r with m2; [ assumption | ].
apply plus_reg_r with x0; assumption.
rewrite <- H1; reflexivity.
Qed.

Theorem modulo_irrelevant_divisor_positivity: forall (n m1 m2: nat) (H: m1 > 0) (H0: m2 > 0),
  m1 = m2 -> (proj1_sig (modulo m1 H n)) = (proj1_sig (modulo m2 H0 n)).
Proof.
intros n m1 m2 H H0 H1.
generalize (quotient_irrelevant_divisor_positivity n m1 m2 H H0 H1); intros.
generalize (sigma_euclid n m1 H); intros.
generalize (sigma_euclid n m2 H0); intros.
induction H3; induction H4.
apply plus_reg_l with (proj1_sig (quotient m2 H0 n) * m2).
rewrite <- H4.
rewrite <- H2.
rewrite <- H1.
symmetry; assumption.
Qed.

Theorem quotient_lt_dividend: forall (n m: nat) (H: m > 0), n >= m -> m > 1 ->
  (proj1_sig (quotient m H n)) < n.
Proof.
intros n m H H0 H1.
generalize (sigma_euclid n m H); intros.
induction H2.
set (y := proj1_sig (quotient m H n)).
replace n with (S (pred n)).
apply le_lt_n_Sm.
unfold y.
apply quotient_has_min_multiple_property.
rewrite <- S_pred with n 0.
set (z := m * n).
replace n with (1 * n).
unfold z.
apply mult_lt_compat_r.
intuition.
apply lt_le_trans with m; intuition.
intuition.
apply lt_le_trans with m; intuition.
rewrite <- S_pred with n 0.
reflexivity.
apply lt_le_trans with m; intuition.
Qed.

Theorem quotient_gt_0: forall (n m: nat) (H: m > 0), n >= m ->
  (proj1_sig (quotient m H n)) > 0.
Proof.
intros n m H H0.
generalize (sigma_euclid n m H); intros.
induction H1.
rewrite H1 in H0.
apply le_S_gt.
apply lt_le_S.
apply neq_O_lt.
intros H3.
rewrite <- H3 in H0.
rewrite mult_0_l in H0.
rewrite plus_0_l in H0.
generalize (gt_not_le _ _ H2); intros.
apply H4; intuition.
Qed.


(**
  * Logarithms
  *)

(** This part should go into Arith/Log.v *)

Lemma nat_log_inf :
  forall m, m > 1 ->
  forall n:nat, n > 0 ->
  {q : nat |  exists r : nat, n = exp m q + r /\ r < (exp m q) * (m - 1)}.
Proof.
intros m H n.
pattern n in |- *.
apply gt_wf_rec.
intros k H0.
elim (le_gt_dec m k).

intros lemk H1.
assert (m > 0).
intuition.
elim (H0 (proj1_sig (quotient m H2 k))).
intros.
exists (S x).
elim p.
intros r Hr.
exists (r * m + (proj1_sig (modulo m H2 k))).
induction Hr.
generalize (sigma_euclid k m H2); intros.
induction H5.
split.
set (y := proj1_sig (modulo m H2 k)).
rewrite H5.
unfold y.
rewrite H3.
simpl.
ring.
generalize (lt_le_S _ _ H4); intros.
generalize (gt_le_S _ _ H6); intros.
generalize (mult_le_compat_r _ _ m H7); intros.
generalize (plus_le_compat _ _ _ _ H9 H8); intros.
simpl in H10.
apply lt_le_trans with (r * m + (S (proj1_sig (modulo m H2 k)))).
intuition.
apply plus_le_reg_l with m.
rewrite plus_assoc.
replace (m + (exp m (S x)) * (m - 1)) with ((exp m x) * (m - 1) * m + m).
assumption.
simpl; ring.
generalize (sigma_euclid k m H2); intros.
induction H3.
apply le_S_gt.
apply lt_n_Sm_le.
apply lt_n_S.
apply quotient_lt_dividend; intuition.
apply quotient_gt_0.
intuition.

intros gtmk H1.
exists 0.
exists (k - 1).
split.
simpl.
rewrite <- pred_of_minus.
rewrite <- S_pred with k 0; intuition.
simpl.
rewrite plus_0_r.
do 2 (rewrite <- pred_of_minus).
apply lt_S_n.
rewrite <- S_pred with k 0; intuition.
rewrite <- S_pred with m 0; intuition.
Qed.


