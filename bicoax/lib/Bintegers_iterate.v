(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Powerset_facts.
Require Import ZArith.
Require Import Bchapter2.
Require Import Bnaturals_iterate.
Require Import Bintegers_basics.

(**
  * Iterate of a relation (integer version)
  *)

(**
  ** Iterate (integer version): definitions

  We propose two definitions, one total (iter) and one partial (iter'). Normally iteration
  is not defined for negative numbers. In terms of model consistency, it is the same to
  say that a negative iteration is the empty set (hence false) or that it is not well-defined,
  hence can not be reasoned upon. The difference is in matching the BBook, hence in that
  regard, iter' is more "correct".
  *)
Fixpoint iter (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (n: Z) {struct n} :=
  match n with
  | Z0 => id s
  | Zpos p => Bnaturals_iterate.iter s r (nat_of_P p)
  | Zneg p => Empty_set (S*S)
end.
Implicit Arguments iter [S].

Inductive iter' (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (n: Z): Ensemble (S*S) :=
  | iter_Z0: forall (x:S), n = Z0 -> In s x -> iter' S s r n (x,x)
  | iter_Zsucc: forall (x y z:S) (m: Z),
      m >= 0 -> n = Zsucc m -> In r (x,z) -> In (iter' S s r m) (z,y) ->  iter' S s r n (x,y).
Implicit Arguments iter' [S].

Theorem iter_is_a_relation: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (n: Z),
  In (relation s s) r -> In (relation s s) (iter s r n).
Proof.
intros S s r n H.
induction n.
simpl.
specialize (Membership_laws_15 S s); intros H0.
do 3 induction H0; assumption.
simpl; apply Bnaturals_iterate.iter_is_a_relation; assumption.
simpl; do 2 constructor; red; intros; contradiction.
Qed.

(**
  ** Iterate: equalities of the various definitions
  *)

(**
  We check that both definitions above at least agree when the number of iterations
  is positive
  *)
Theorem iter_iter': forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (n: Z),
  n >= 0 -> iter s r n = iter' s r n.
Proof.
intros S s r n H.
induction n as [ H | p H | p H ].
(* n = Z0 *)
simpl.
apply Extensionality_Ensembles; split; red; intros x H0.
destruct H0 as [ x0 x1 H0 H1 ].
rewrite H1; left.
reflexivity.
inversion H0; assumption.
destruct H0 as [ y H0 H1 | x y z m H0 H1 H2 H3 ].
constructor; [ constructor; assumption | reflexivity ].
rewrite H1 in H0.
specialize (Zgt_succ m); assert (Zsucc m <= m); [ intuition | ]; contradiction.
simpl.
(* n = Zpos p *)
pattern p; apply Pind.
(* p = 1 *)
simpl.
apply Extensionality_Ensembles; split; red; intros x H0.
destruct H0 as [ x0 x1 (x2, (H0, H1)) ].
right with x2 Z0.
intuition.
intuition.
intuition.
destruct H1 as [ a b H1 H2 ].
rewrite H2; left.
reflexivity.
inversion H1; assumption.
inversion H0 as [ y H1 H2 H3 | ].
assert (1 <> 0); [ discriminate | contradiction ].
constructor; exists z; split; [ assumption | ].
assert (m = 0); [ intuition | ].
rewrite H6 in H4.
destruct H4 as [ z0 H7 H8 | ].
constructor; [ constructor; assumption | reflexivity ].
assert (m0 = Zpred 0) as H10 ; [ intuition | ].
rewrite H10 in H4.
specialize (Zlt_pred Z0); contradiction.
(* p = Psucc q, with induction hypothesis *)
intros q H0.
rewrite nat_of_P_succ_morphism.
rewrite Zpos_succ_morphism.
apply Extensionality_Ensembles; split; red; intros x H1.
simpl in H1.
rewrite H0 in H1.
destruct H1 as [ a c (b, (H1, H2)) ].
right with b (Zpos q).
intuition.
reflexivity.
assumption.
assumption.
simpl; rewrite H0.
destruct H1.
assert (Zsucc (Zpos q) <> 0) as H3.
assert (0 <> Zsucc (Zpos q)) as H3.
apply Zlt_not_eq.
apply Zle_lt_succ; intuition.
intuition.
contradiction.
constructor; exists z.
specialize (Zsucc_inj _ _ H2); intros H5.
rewrite H5; intuition.
specialize (Zlt_neg_0 p); contradiction.
Qed.

Theorem iter_nat_of_Zpos: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (n: positive),
  iter s r (Zpos n) = Bnaturals_iterate.iter s r (nat_of_P n).
Proof.
intros S s r n.
simpl; reflexivity.
Qed.

Theorem iter_nat_of_Z: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (n: Z),
  n >= 0 -> exists p:nat, iter s r n = Bnaturals_iterate.iter s r p /\ n = Z_of_nat p.
Proof.
intros S s r n H.
destruct n.
exists 0%nat.
simpl; intuition.
exists (nat_of_P p).
simpl.
rewrite Zpos_eq_Z_of_nat_o_nat_of_P; intuition.
specialize (Zlt_neg_0 p); contradiction.
Qed.

Theorem iter_Z_of_nat: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (n: nat),
  Bnaturals_iterate.iter s r n = iter s r (Z_of_nat n).
Proof.
intros S s r n.
induction n.
simpl; reflexivity.
simpl.
rewrite nat_of_P_o_P_of_succ_nat_eq_succ; simpl; reflexivity.
Qed.

(**
  ** Iterate: properties
  *)

Definition iterate_relation := iter_is_a_relation.

(**
  BBook: section 3.5.8, p.167, array 2, row 1
  *)
Theorem iterate_partial_function: forall (S: Type) (s: Ensemble S) (f: Ensemble (S*S)) (_: In (partial_function s s) f) (n: Z),
  n >= 0 -> In (partial_function s s) (iter s f n).
Proof.
intros S s f H n H0.
specialize (iter_nat_of_Z S s f n H0); intros H1.
destruct H1 as [ p H1 ].
destruct H1 as [ H1 H2 ].
rewrite H1.
apply Bnaturals_iterate.iterate_partial_function; assumption.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 2
  *)
Theorem iterate_total_function: forall (S: Type) (s: Ensemble S) (f: Ensemble (S*S)) (_: In (total_function s s) f) (n: Z),
  n >= 0 -> In (total_function s s) (iter s f n).
Proof.
intros S s f H n H0.
specialize (iter_nat_of_Z S s f n H0); intros H1.
destruct H1 as [ p H1 ].
destruct H1 as [ H1 H2 ].
rewrite H1.
apply Bnaturals_iterate.iterate_total_function; assumption.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 3
  *)
Theorem iterate_partial_injection: forall (S: Type) (s: Ensemble S) (f: Ensemble (S*S)) (_: In (partial_injection s s) f) (n: Z),
  n>=0 -> In (partial_injection s s) (iter s f n).
Proof.
intros S s f H n H0.
specialize (iter_nat_of_Z S s f n H0); intros H1.
destruct H1 as [ p H1 ].
destruct H1 as [ H1 H2 ].
rewrite H1.
apply Bnaturals_iterate.iterate_partial_injection; assumption.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 4
  *)
Theorem iterate_total_surjection: forall (S: Type) (s: Ensemble S) (f: Ensemble (S*S)) (_: In (total_surjection s s) f) (n: Z),
  n >=0 -> In (total_surjection s s) (iter s f n).
Proof.
intros S s f H n H0.
specialize (iter_nat_of_Z S s f n H0); intros H1.
destruct H1 as [ p H1 ].
destruct H1 as [ H1 H2 ].
rewrite H1.
apply Bnaturals_iterate.iterate_total_surjection; assumption.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 5
  *)
Theorem iterate_one: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r),
  (iter s r 1) = r.
Proof.
intros S s r H.
simpl.
apply Equality_laws_compose_05 with s; assumption.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 6
  *)
Theorem iterate_successor: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (n: Z),
  n >= 0 -> iter s r (Zsucc n) = composition (iter s r n) r.
Proof.
intros S s r H n H0.
specialize (iter_nat_of_Z S s r n H0); intros H1.
destruct H1 as [ p (H1, H2) ].
destruct n as [ | q | q ].
simpl.
rewrite Equality_laws_compose_05 with S S s s r; [ | assumption ].
rewrite Equality_laws_compose_19 with S S s s r; [ | assumption ].
reflexivity.
rewrite <- Zpos_succ_morphism.
simpl.
rewrite nat_of_P_succ_morphism; simpl.
rewrite Bnaturals_iterate.iterate_composition_r_commutes; [ reflexivity | assumption ].
specialize (Zlt_neg_0 q); contradiction.
Qed.

Theorem iterate_composition_r_commutes: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (n: Z),
  composition (iter s r n) r = composition r (iter s r n).
Proof.
intros S s r H n.
destruct n as [ | p | p ].
simpl.
rewrite Equality_laws_compose_05 with S S s s r; [ | assumption ].
rewrite Equality_laws_compose_19 with S S s s r; [ | assumption ].
reflexivity.
rewrite iter_nat_of_Zpos; apply Bnaturals_iterate.iterate_composition_r_commutes; assumption.
simpl.
assert (In (relation s s) (Empty_set (S*S))) as H0; [ do 2 constructor; red; intros; contradiction | ].
rewrite Equality_laws_compose_29 with S S S s s s (Empty_set (S*S)) r; [ | intuition ].
rewrite Equality_laws_compose_15 with S S S s s s (Empty_set (S*S)) r; [ | intuition ].
reflexivity.
Qed.

Theorem iterate_composition_commutative: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (m n: Z),
  (composition (iter s r m) (iter s r n) = composition (iter s r n) (iter s r m)).
Proof.
intros S s r H m n.
destruct m as [ | m0 | m0 ].
simpl.
specialize (iter_is_a_relation S s r n H); intros H0.
rewrite Equality_laws_compose_05 with S S s s (iter s r n); [ | assumption ].
rewrite Equality_laws_compose_19 with S S s s (iter s r n); [ | assumption ].
reflexivity.
destruct n as [ | n0 H0 | n0 H0 ].
simpl.
specialize (Bnaturals_iterate.iter_is_a_relation S s r (nat_of_P m0) H); intros H0.
rewrite Equality_laws_compose_05 with S S s s (Bnaturals_iterate.iter s r (nat_of_P m0)); [ | assumption ].
rewrite Equality_laws_compose_19 with S S s s (Bnaturals_iterate.iter s r (nat_of_P m0)); [ | assumption ].
reflexivity.
simpl; apply Bnaturals_iterate.iterate_composition_commutative; assumption.
simpl.
specialize (Bnaturals_iterate.iter_is_a_relation S s r (nat_of_P m0) H); intros H0.
assert (In (relation s s) (Empty_set (S*S))) as H1; [ do 2 constructor; red; intros; contradiction | ].
rewrite Equality_laws_compose_29 with S S S s s s (Empty_set (S*S)) (Bnaturals_iterate.iter s r (nat_of_P m0)); [ | intuition ].
rewrite Equality_laws_compose_15 with S S S s s s (Empty_set (S*S)) (Bnaturals_iterate.iter s r (nat_of_P m0)); [ | intuition ].
reflexivity.
simpl.
specialize (iter_is_a_relation S s r n H); intros H0.
assert (In (relation s s) (Empty_set (S*S))) as H1; [ do 2 constructor; red; intros; contradiction | ].
rewrite Equality_laws_compose_29 with S S S s s s (Empty_set (S*S)) (iter s r n); [ | intuition ].
rewrite Equality_laws_compose_15 with S S S s s s (Empty_set (S*S)) (iter s r n); [ | intuition ].
reflexivity.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 7
  *)
Theorem iterate_inverse: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (n: Z),
  (inverse (iter s r n)) = (iter s (inverse r) n).
Proof.
intros S s r H n.
destruct n as [ | n0 | n0 ].
simpl.
rewrite Equality_laws_inverse_03; reflexivity.
simpl.
apply Bnaturals_iterate.iterate_inverse; assumption.
simpl.
assert (In (relation s s) (Empty_set (S*S))) as H0; [ do 2 constructor; red; intros; contradiction | ].
rewrite Equality_laws_inverse_14 with S S s s (Empty_set (S*S)); [ reflexivity | intuition ].
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 8

  This is not true with our definition in general: r(1);r(-1) = r;{} = {} while r(1+ -1)=r(0)=id(s), hence the
  need to restrict it to positive integers. This should be true with the definition of Bintegers_iterz, though.
  *)
Theorem iterate_addition: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (m n: Z),
  m >= 0 -> n >= 0 -> composition (iter s r m) (iter s r n) = iter s r (m+n).
Proof.
intros S s r H m n H0 H1.
specialize (iter_nat_of_Z S s r m H0); intros H2.
destruct H2 as [ p (H2, H3) ].
specialize (iter_nat_of_Z S s r n H1); intros H4.
destruct H4 as [ q (H4, H5) ].
assert (m+n >= 0) as H6; [ intuition | ].
specialize (iter_nat_of_Z S s r (m+n) H6); intros H7.
destruct H7 as [ pq (H7, H8) ].
rewrite H2; rewrite H4; rewrite H7.
assert ((pq = p + q)%nat).
apply inj_eq_rev.
rewrite inj_plus.
rewrite <- H3; rewrite <- H5; rewrite <- H8; reflexivity.
rewrite H9; apply Bnaturals_iterate.iterate_addition; assumption.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 9

  Surprisingly, this theorem is also valid when one of the iteration is negative. It is only invalid when
  both iterations are negative, hence the "weird" additional hypothesis.
  *)
Theorem iterate_multiplication: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (m n: Z),
  n >= 0 \/ ((m = 0 /\ n >= 0) \/ m > 0) -> iter s (iter s r m) n = iter s r (m*n).
Proof.
intros S s r H m n H0.
destruct H0.
destruct m as [ | m0 | m0 ].
simpl.
specialize (iter_nat_of_Z S s (id s) n H0); intros H1.
destruct H1 as [ p (H1, H2) ].
rewrite H1.
generalize p; intros p0.
induction p0 as [ | p0 IHp0 ].
simpl; reflexivity.
simpl; rewrite IHp0.
rewrite Equality_laws_compose_05 with S S s s (id s); [ reflexivity | ].
specialize (Membership_laws_15 S s); intros H3.
do 3 (induction H3); assumption.
destruct n as [ | n0 | n0 ].
simpl; reflexivity.
rewrite iter_nat_of_Zpos.
rewrite iter_nat_of_Zpos.
simpl.
rewrite nat_of_P_mult_morphism.
apply Bnaturals_iterate.iterate_multiplication; assumption.
simpl; reflexivity.
destruct n as [ | n0 | n0 ].
simpl; reflexivity.
simpl.
assert (In (relation s s) (Empty_set (S*S))) as H1; [ do 2 constructor; red; intros; contradiction | ].
pattern n0; apply Pind.
simpl.
rewrite Equality_laws_compose_05 with S S s s (Empty_set (S*S)); [ reflexivity | assumption ].
intros.
rewrite nat_of_P_succ_morphism; simpl.
rewrite H2.
rewrite Equality_laws_compose_29 with S S S s s s (Empty_set (S*S)) (Empty_set (S*S)); [ reflexivity | intuition ].
specialize (Zlt_neg_0 n0); intros; contradiction.
destruct H0 as [ (H0, H1) | ].
rewrite H0; simpl.
destruct n as [ | n0 | n0 ].
simpl; reflexivity.
simpl.
assert (In (relation s s) (id s)) as H2.
specialize (Membership_laws_15 S s); intros H2; do 3 (induction H2); assumption.
pattern n0; apply Pind.
simpl.
rewrite Equality_laws_compose_05 with S S s s (id s); [ reflexivity | assumption ].
intros p H3.
rewrite nat_of_P_succ_morphism; simpl.
rewrite H3.
rewrite Equality_laws_compose_05 with S S s s (id s); [ reflexivity | assumption ].
specialize (Zlt_neg_0 n0); intros; contradiction.
destruct m as [ | m0 | m0 ].
assert (0 <= 0); [ intuition | contradiction ].
destruct n as [ | n0 | n0 ].
simpl; reflexivity.
simpl.
rewrite nat_of_P_mult_morphism.
apply Bnaturals_iterate.iterate_multiplication; assumption.
simpl; reflexivity.
specialize (Zlt_neg_0 m0); intros.
assert (Zneg m0 <= 0); [ intuition | contradiction ].
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 10
  *)
Theorem iterate_inclusion: forall (S: Type) (s: Ensemble S) (p q: Ensemble (S*S)) (_: In (relation s s) p) (_: In (relation s s) q) (n: Z),
  Included p q -> Included (iter s p n) (iter s q n).
Proof.
intros S s p q H H0 n H1.
destruct n.
simpl; intuition.
do 2 (rewrite iter_nat_of_Zpos).
apply Bnaturals_iterate.iterate_inclusion; assumption.
simpl; intuition.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 11
  *)
Theorem iterate_image: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (a: Ensemble S) (_: Included a s) (n: Z),
  Included (image r a) a -> Included (image (iter s r n) a) a.
Proof.
intros S s r H a H0 n H1.
destruct n as [ | n0 | n0 ].
simpl.
red; intros x H2.
destruct H2 as [ x y (H2, H3) ].
inversion H3 as [ u v H4 H5 H6 ].
rewrite <- H7; assumption.
rewrite iter_nat_of_Zpos.
apply Bnaturals_iterate.iterate_image; assumption.
simpl; red; intros.
destruct H2 as [ x y (H2, H3) ]; contradiction.
Qed.
