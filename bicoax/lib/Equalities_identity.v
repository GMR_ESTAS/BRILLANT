(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Bderived_constructs.
Require Import Brelations.

Open Scope eB_scope.

(**
  BBook: section 2.6.4, p.107, array 1, row 1
  *)
Theorem Equality_laws_id_01: forall (S: Type) (u: Ensemble S) (v: Ensemble S), (id(u ∪ v) = id(u) ∪ id(v)).
Proof.
intros S u v.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H.
induction H3.
left; constructor.
constructor; [ assumption | rewrite <- H0; assumption ].
assumption.
right; constructor.
constructor; [ assumption | rewrite <- H0; assumption ].
assumption.

induction H0; induction H; inversion H.
constructor; [ | assumption ].
constructor; left; assumption.
constructor; [ | assumption ].
constructor; right; assumption.
Qed.

(**
  BBook: section 2.6.4, p.107, array 1, row 2
  *)
Theorem Equality_laws_id_02: forall (S: Type) (u: Ensemble S) (v: Ensemble S), (id(u ∩ v) = id(u) ∩ id(v)).
Proof.
intros S u v.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.

induction H.
inversion H.
inversion H3; inversion H4.
split; constructor.
constructor; assumption.
assumption.
constructor; assumption.
assumption.

induction H.
inversion H; inversion H0.
assert ((a,b)=(a0,b0)); [ rewrite H3; rewrite H6; reflexivity | ].
injection H7; intros.
inversion H1; inversion H4.
constructor; [ | assumption ].
constructor; split.
assumption.
rewrite H9; assumption.
assumption.
rewrite H8; assumption.
Qed.

(**
  BBook: section 2.6.4, p.107, array 1, row 3
  *)
Theorem Equality_laws_id_03: forall (S: Type) (u: Ensemble S) (v: Ensemble S), (id(u ∖ v) = id(u) ∖ id(v)).
Proof.
intros S u v.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.

induction H.
inversion H.
induction H3; induction H4.
constructor.
constructor; [ constructor; assumption | assumption ].
intros H7.
inversion H7.
inversion H10; contradiction.

induction H.
induction H.
inversion H.
constructor; [ | assumption ].
constructor.
split; [ assumption | ].
intros H6; apply H0.
constructor; [ | assumption ].
constructor.
assumption.
rewrite <- H1; assumption.
split; [ assumption | ].
intros H6; apply H0.
constructor; [ | assumption ].
constructor.
rewrite H1; assumption.
assumption.
Qed.

(**
  BBook: section 2.6.4, p.107, array 1, row 4
  *)
Theorem Equality_laws_id_04: forall (S: Type) (x: S) (s: Ensemble S), (x ∈ s) ⇒(id((Singleton (x))) = (Singleton (x↦x)) ).
Proof.
intros S x s H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

induction H0.
inversion H0.
inversion H4; inversion H5.
rewrite <- H1; intuition.

induction z.
inversion H0.
constructor; [ constructor; intuition | reflexivity ].
Qed.

(**
  BBook: section 2.6.4, p.107, array 1, row 5
  *)
Theorem Equality_laws_id_05: forall (S: Type) (u: Ensemble S),(u = ∅) ⇒(id(u) = ∅).
Proof.
intros S u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
assert (In (Empty_set _) a).
rewrite <- H; assumption.
contradiction.

contradiction.
Qed.


Close Scope eB_scope.
