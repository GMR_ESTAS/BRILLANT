(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter2.
Require Import Bnaturals_recfuns.
Require Import Arith.
Require Import coq_missing_arith.

Open Local Scope nat_scope.

(**
  * nat version of arithmetic operations: definitions
  *)

Definition bbnat_succ (c: nat * nat) := match c with | (x,y) => y = (S x) end.

Theorem bbnat_succ_total: In (total_function (Full_set nat) (Full_set nat)) bbnat_succ.
Proof.
constructor.
constructor.

do 2 constructor.
red; intros.
induction x; inversion H.
constructor; constructor.

intros.
inversion H; inversion H0.
reflexivity.

apply Extensionality_Ensembles; split; red; intros.
constructor.
constructor; exists (S x).
constructor.
Qed.

Theorem bbnat_succ_y: forall (n:nat),
  In (partial_function (Full_set nat) (Full_set nat)) bbnat_succ /\ In (domain bbnat_succ) n.
Proof.
intros n.
generalize bbnat_succ_total; intros.
inversion H.
split; [ assumption | rewrite H1; constructor ].
Qed.

Theorem bbnat_succ_nat_S: forall (n:nat),
  app bbnat_succ n (bbnat_succ_y n) = S n.
Proof.
intros n.
symmetry; apply identifying_app.
constructor.
Qed.

(**
  ** Addition
  *)

Section BBook_addition_nat.
(* a is m *)
(* a_in_s is (In (Full_set nat) m ) *)
(* g is bbnat_succ (see below) *)
(* g_total is the totality of bbnat_succ  (proved below) *)
Variable m: nat.

(**
  definition of plus(m), BBook: section 3.5.7, p.161, array 1, row 1
  *)
Definition bbnat_plus_c1 := nat_f nat (Full_set nat) m bbnat_succ.

Theorem bbnat_plus_c1_total: In (total_function (Full_set nat) (Full_set nat)) bbnat_plus_c1.
Proof.
unfold bbnat_plus_c1.
apply nat_f_is_total.
constructor.
apply bbnat_succ_total.
Qed.

End BBook_addition_nat.

(**
  ** Multiplication
  *)

Section BBook_multiplication_nat.
(* a is 0 *)
(* a_in_s is (In (Full_set nat) 0 ) *)
(* g is bbnat_plus_c1 *)
(* g_total is bbnat_plus_c1_total *)
Variable m: nat.

(**
  definition of mult(m), BBook: section 3.5.7, p.161, array 1, row 2
  *)
Definition bbnat_mult_c1 := nat_f nat (Full_set nat) 0 (bbnat_plus_c1 m).

Theorem bbnat_mult_c1_total: In (total_function (Full_set nat) (Full_set nat)) bbnat_mult_c1.
Proof.
unfold bbnat_mult_c1.
apply nat_f_is_total.
constructor.
apply bbnat_plus_c1_total.
Qed.

End BBook_multiplication_nat.

(**
  ** Exponentiation
  *)

Section BBook_exponentiation_nat.
(* a is 1 *)
(* a_in_s is (In (Full_set nat) 1 ) *)
(* g is bbnat_mult_c1 *)
(* g_total is bbnat_mult_c1_total *)
Variable m: nat.

(**
  definition of exp(m), BBook: section 3.5.7, p.161, array 1, row 3
  *)
Definition bbnat_exp_c1 := nat_f nat (Full_set nat) 1 (bbnat_mult_c1 m).

Theorem bbnat_exp_c1_total: In (total_function (Full_set nat) (Full_set nat)) bbnat_exp_c1.
Proof.
unfold bbnat_exp_c1.
apply nat_f_is_total.
constructor.
apply bbnat_mult_c1_total.
Qed.

End BBook_exponentiation_nat.

(**
  relational definition of plus, BBook: section 3.5.7, p.161
  *)
Definition bbnat_plus: Ensemble (nat * (Ensemble (nat * nat))) :=
   fun cpl => match cpl with | (m, plus_m_n) => (bbnat_plus_c1 m) = plus_m_n end.

(**
  relational definition of mult, BBook: section 3.5.7, p.161
  *)
Definition bbnat_mult: Ensemble (nat * (Ensemble (nat * nat))) :=
   fun cpl => match cpl with | (m, mult_m_n) => (bbnat_mult_c1 m) = mult_m_n end.

(**
  relational definition of exp, BBook: section 3.5.7, p.161
  *)
Definition bbnat_exp: Ensemble (nat * (Ensemble (nat * nat))) :=
   fun cpl => match cpl with | (m, exp_m_n) => (bbnat_exp_c1 m) = exp_m_n end.


(**
  * nat version of arithmetic operations: validity
  *)

(**
  ** Addition
  *)

Theorem bbnat_plus_applicable: forall (m: nat),
  In (partial_function (Full_set nat) (partial_function (Full_set nat) (Full_set nat))) bbnat_plus /\ In (domain bbnat_plus) m.
Proof.
intros.
split.
constructor.

do 2 constructor.
red; intros.
induction x.
induction H.
split.
constructor.
generalize (bbnat_plus_c1_total a); intros.
induction H; assumption.

intros.
inversion H; inversion H0.
reflexivity.

constructor; exists (bbnat_plus_c1 m).
constructor.
Qed.

Theorem bbnat_plus_c1_applicable: forall (m n: nat),
  In (partial_function (Full_set nat) (Full_set nat)) (bbnat_plus_c1 m)
/\ In (domain (bbnat_plus_c1 m)) n.
Proof.
intros.
generalize (bbnat_plus_c1_total m).
intros.
inversion H.
split; [ assumption | rewrite H1; constructor ].
Qed.

Theorem indirect_bbnat_plus_c1_applicable: forall (m n: nat),
  In (partial_function (Full_set nat) (Full_set nat)) (app bbnat_plus m (bbnat_plus_applicable m))
/\ In (domain (app bbnat_plus m (bbnat_plus_applicable m))) n.
Proof.
intros.
replace (app bbnat_plus m (bbnat_plus_applicable m)) with (bbnat_plus_c1 m).
apply bbnat_plus_c1_applicable; assumption.
apply identifying_app.
constructor.
Qed.

Theorem valid_bbnat_plus_nat_curry: forall (m n: nat),
  (app (bbnat_plus_c1 m) n (bbnat_plus_c1_applicable m n))
= (plus m n).
Proof.
intros m n.
symmetry.
apply identifying_app.
induction n.
unfold bbnat_plus_c1.
rewrite nat_f_lfp.
left.
intuition.
constructor.
apply bbnat_succ_total.
unfold bbnat_plus_c1.
rewrite nat_f_lfp.
right.
constructor.
exists n.
split.
constructor.
constructor.
exists (m+n).
split.
assumption.
rewrite <- plus_n_Sm.
constructor.
constructor.
apply bbnat_succ_total.
Qed.

Theorem valid_bbnat_plus_nat: forall (m n: nat),
  (app (app bbnat_plus m (bbnat_plus_applicable m)) n (indirect_bbnat_plus_c1_applicable m n))
= (plus m n).
Proof.
intros m n.
replace (app (app bbnat_plus m (bbnat_plus_applicable m)) n (indirect_bbnat_plus_c1_applicable m n))
  with (app (bbnat_plus_c1 m) n (bbnat_plus_c1_applicable m n)).
apply valid_bbnat_plus_nat_curry.
apply identifying_app.
replace (app bbnat_plus m (bbnat_plus_applicable m)) with (bbnat_plus_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
Qed.

(**
  ** Multiplication
  *)

Theorem bbnat_mult_applicable: forall (m: nat),
  In (partial_function (Full_set nat) (partial_function (Full_set nat) (Full_set nat))) bbnat_mult /\ In (domain bbnat_mult) m.
Proof.
intros.
split.
constructor.

do 2 constructor.
red; intros.
induction x.
induction H.
split.
constructor.
generalize (bbnat_mult_c1_total a); intros.
induction H; assumption.

intros.
inversion H; inversion H0.
reflexivity.

constructor; exists (bbnat_mult_c1 m).
constructor.
Qed.

Theorem bbnat_mult_c1_applicable: forall (m n: nat),
  In (partial_function (Full_set nat) (Full_set nat)) (bbnat_mult_c1 m)
/\ In (domain (bbnat_mult_c1 m)) n.
Proof.
intros.
generalize (bbnat_mult_c1_total m).
intros.
inversion H.
split; [ assumption | rewrite H1; constructor ].
Qed.

Theorem indirect_bbnat_mult_c1_applicable: forall (m n: nat),
  In (partial_function (Full_set nat) (Full_set nat)) (app bbnat_mult m (bbnat_mult_applicable m))
/\ In (domain (app bbnat_mult m (bbnat_mult_applicable m))) n.
Proof.
intros.
replace (app bbnat_mult m (bbnat_mult_applicable m)) with (bbnat_mult_c1 m).
apply bbnat_mult_c1_applicable; assumption.
apply identifying_app.
constructor.
Qed.

Theorem valid_bbnat_mult_nat_curry: forall (m n: nat),
  (app (bbnat_mult_c1 m) n (bbnat_mult_c1_applicable m n))
= (mult m n).
Proof.
intros m n.
symmetry.
apply identifying_app.
induction n.
unfold bbnat_mult_c1.
rewrite nat_f_lfp.
left.
intuition.
constructor.
apply bbnat_plus_c1_total.
unfold bbnat_mult_c1.
rewrite nat_f_lfp.
right.
constructor.
exists n.
split.
constructor.
constructor.
exists (m*n).
split.
assumption.
rewrite <- mult_n_Sm.
rewrite plus_comm.
rewrite <- valid_bbnat_plus_nat_curry.
apply app_trivial_property.
constructor.
apply bbnat_plus_c1_total.
Qed.

Theorem valid_bbnat_mult_nat: forall (m n: nat),
  (app (app bbnat_mult m (bbnat_mult_applicable m)) n (indirect_bbnat_mult_c1_applicable m n))
= (mult m n).
Proof.
intros m n.
replace (app (app bbnat_mult m (bbnat_mult_applicable m)) n (indirect_bbnat_mult_c1_applicable m n))
  with (app (bbnat_mult_c1 m) n (bbnat_mult_c1_applicable m n)).
apply valid_bbnat_mult_nat_curry.
apply identifying_app.
replace (app bbnat_mult m (bbnat_mult_applicable m)) with (bbnat_mult_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
Qed.

(**
  ** Exponentiation
  *)

Theorem bbnat_exp_applicable: forall (m: nat),
  In (partial_function (Full_set nat) (partial_function (Full_set nat) (Full_set nat))) bbnat_exp /\ In (domain bbnat_exp) m.
Proof.
intros.
split.
constructor.

do 2 constructor.
red; intros.
induction x.
induction H.
split.
constructor.
generalize (bbnat_exp_c1_total a); intros.
induction H; assumption.

intros.
inversion H; inversion H0.
reflexivity.

constructor; exists (bbnat_exp_c1 m).
constructor.
Qed.

Theorem bbnat_exp_c1_applicable: forall (m n: nat),
  In (partial_function (Full_set nat) (Full_set nat)) (bbnat_exp_c1 m)
/\ In (domain (bbnat_exp_c1 m)) n.
Proof.
intros.
generalize (bbnat_exp_c1_total m).
intros.
inversion H.
split; [ assumption | rewrite H1; constructor ].
Qed.

Theorem indirect_bbnat_exp_c1_applicable: forall (m n: nat),
  In (partial_function (Full_set nat) (Full_set nat)) (app bbnat_exp m (bbnat_exp_applicable m))
/\ In (domain (app bbnat_exp m (bbnat_exp_applicable m))) n.
Proof.
intros.
replace (app bbnat_exp m (bbnat_exp_applicable m)) with (bbnat_exp_c1 m).
apply bbnat_exp_c1_applicable; assumption.
apply identifying_app.
constructor.
Qed.

Theorem valid_bbnat_exp_nat_curry: forall (m n: nat),
  (app (bbnat_exp_c1 m) n (bbnat_exp_c1_applicable m n))
= (exp m n).
Proof.
intros m n.
symmetry.
apply identifying_app.
induction n.
unfold bbnat_exp_c1.
rewrite nat_f_lfp.
left.
simpl exp.
intuition.
constructor.
apply bbnat_mult_c1_total.
unfold bbnat_exp_c1.
rewrite nat_f_lfp.
right.
constructor.
exists n.
split.
constructor.
constructor.
exists (exp m n).
split.
assumption.
simpl exp.
rewrite <- valid_bbnat_mult_nat_curry.
apply app_trivial_property.
constructor.
apply bbnat_mult_c1_total.
Qed.

Theorem valid_bbnat_exp_nat: forall (m n: nat),
  (app (app bbnat_exp m (bbnat_exp_applicable m)) n (indirect_bbnat_exp_c1_applicable m n))
= (exp m n).
Proof.
intros m n.
replace (app (app bbnat_exp m (bbnat_exp_applicable m)) n (indirect_bbnat_exp_c1_applicable m n))
  with (app (bbnat_exp_c1 m) n (bbnat_exp_c1_applicable m n)).
apply valid_bbnat_exp_nat_curry.
apply identifying_app.
replace (app bbnat_exp m (bbnat_exp_applicable m)) with (bbnat_exp_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
Qed.

Definition bbnat_succ_unary (m: nat) :=
  (app bbnat_succ m (bbnat_succ_y m)).
Hint Unfold bbnat_succ_unary.

(**
  definition of +, BBook: section 3.5.7, p.162, array 1, row 1
  *)
Definition bbnat_plus_bin (m n: nat) :=
  (app (app bbnat_plus m (bbnat_plus_applicable m )) n (indirect_bbnat_plus_c1_applicable m n)).
Hint Unfold bbnat_plus_bin.

(**
  definition of *, BBook: section 3.5.7, p.162, array 1, row 2
  *)
Definition bbnat_mult_bin (m n: nat) :=
  (app (app bbnat_mult m (bbnat_mult_applicable m)) n (indirect_bbnat_mult_c1_applicable m n)).
Hint Unfold bbnat_mult_bin.

(**
  definition of ^, BBook: section 3.5.7, p.162, array 1, row 3
  *)
Definition bbnat_exp_bin (m n: nat) :=
  (app (app bbnat_exp m (bbnat_exp_applicable m)) n (indirect_bbnat_exp_c1_applicable m n)).
Hint Unfold bbnat_exp_bin.

(**
  Note/TODO: show that +,*,^ are total functions under the shape of
  BBook p.161 (should be easy with the previous theorems)
*)

(**
  * nat version of arithmetic operations: properties
  *)

(** m + 0 = 0, BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatplus_zero_r: forall (m: nat),
  bbnat_plus_bin m 0 = m.
Proof.
intros m.
unfold bbnat_plus_bin.
rewrite valid_bbnat_plus_nat.
ring.
Qed.

(** m + succ(n) = succ(m+n), BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatplus_succ_r: forall (m n: nat),
  bbnat_plus_bin m (bbnat_succ_unary n) = bbnat_succ_unary (bbnat_plus_bin m n).
Proof.
intros m n.
unfold bbnat_succ_unary; unfold bbnat_plus_bin.
rewrite valid_bbnat_plus_nat.
rewrite bbnat_succ_nat_S.
rewrite bbnat_succ_nat_S.
rewrite valid_bbnat_plus_nat.
ring.
Qed.

(** m * 0 = 0, BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatmult_zero_r: forall (m: nat),
  bbnat_mult_bin m 0 = 0.
Proof.
intros m.
unfold bbnat_mult_bin.
rewrite valid_bbnat_mult_nat.
ring.
Qed.

(** m * succ(n) = m + ( m * n ), BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatmult_succ_r: forall (m n: nat),
  bbnat_mult_bin m (bbnat_succ_unary n) = bbnat_plus_bin m (bbnat_mult_bin m n).
Proof.
intros m n.
unfold bbnat_succ_unary, bbnat_mult_bin, bbnat_plus_bin.
rewrite valid_bbnat_mult_nat.
rewrite valid_bbnat_plus_nat.
rewrite bbnat_succ_nat_S.
rewrite valid_bbnat_mult_nat.
ring.
Qed.

(** m + 1 = succ(m), BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatplus_one_r: forall (m: nat),
  bbnat_plus_bin m 1 = bbnat_succ_unary m.
Proof.
intros m.
unfold bbnat_plus_bin, bbnat_succ_unary.
rewrite valid_bbnat_plus_nat.
rewrite bbnat_succ_nat_S.
ring.
Qed.

(** 1 + m = succ(m), BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatplus_one_l: forall (m: nat),
  bbnat_plus_bin 1 m = bbnat_succ_unary m.
Proof.
intros m.
unfold bbnat_plus_bin, bbnat_succ_unary.
rewrite valid_bbnat_plus_nat.
rewrite bbnat_succ_nat_S.
ring.
Qed.

(** 0 + m = m, BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatplus_zero_l: forall (m: nat),
  bbnat_plus_bin 0 m = m.
Proof.
intros m.
unfold bbnat_plus_bin.
rewrite valid_bbnat_plus_nat.
ring.
Qed.

(** m + n = n + m, BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatplus_commutative: forall (m n: nat),
  bbnat_plus_bin m n = bbnat_plus_bin n m.
Proof.
intros m n.
unfold bbnat_plus_bin.
rewrite valid_bbnat_plus_nat.
rewrite valid_bbnat_plus_nat.
ring.
Qed.

(** m + ( n + p ) = ( m + n ) + p, BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatplus_associative: forall (m n p: nat),
  bbnat_plus_bin m (bbnat_plus_bin n p) = bbnat_plus_bin (bbnat_plus_bin m n) p.
Proof.
intros m n p.
unfold bbnat_plus_bin.
rewrite valid_bbnat_plus_nat.
rewrite valid_bbnat_plus_nat.
rewrite valid_bbnat_plus_nat.
rewrite valid_bbnat_plus_nat.
ring.
Qed.

(** 0 * m = 0, BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatmult_zero_l: forall (m: nat),
  bbnat_mult_bin 0 m = 0.
Proof.
intros m.
unfold bbnat_mult_bin.
rewrite valid_bbnat_mult_nat.
ring.
Qed.

(** m * 1 = m, BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatmult_one_r: forall (m: nat),
  bbnat_mult_bin m 1 = m.
Proof.
intros m.
unfold bbnat_mult_bin, bbnat_succ_unary.
rewrite valid_bbnat_mult_nat.
ring.
Qed.

(** 1 * m = m, BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatmult_one_l: forall (m: nat),
  bbnat_mult_bin 1 m = m.
Proof.
intros m.
unfold bbnat_mult_bin, bbnat_succ_unary.
rewrite valid_bbnat_mult_nat.
ring.
Qed.

(** m * n = n * m, BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatmult_commutative: forall (m n: nat),
  bbnat_mult_bin m n = bbnat_mult_bin n m.
Proof.
intros m n.
unfold bbnat_mult_bin.
rewrite valid_bbnat_mult_nat.
rewrite valid_bbnat_mult_nat.
ring.
Qed.

(** m * ( n * p ) = ( m * n ) * p, BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatmult_associative: forall (m n p: nat),
  bbnat_mult_bin m (bbnat_mult_bin n p) = bbnat_mult_bin (bbnat_mult_bin m n) p.
Proof.
intros m n p.
unfold bbnat_mult_bin.
rewrite valid_bbnat_mult_nat.
rewrite valid_bbnat_mult_nat.
rewrite valid_bbnat_mult_nat.
rewrite valid_bbnat_mult_nat.
ring.
Qed.

(** m * ( n + p ) = ( m * n ) + ( m * p ), BBook: section 3.5.7, p.162, array 2 *)
Theorem Bnatmult_distributive: forall (m n p: nat),
  bbnat_mult_bin m (bbnat_plus_bin n p) = bbnat_plus_bin (bbnat_mult_bin m n) (bbnat_mult_bin m p).
Proof.
intros m n p.
unfold bbnat_mult_bin, bbnat_plus_bin.
rewrite valid_bbnat_mult_nat.
rewrite valid_bbnat_plus_nat.
rewrite valid_bbnat_plus_nat.
rewrite valid_bbnat_mult_nat.
rewrite valid_bbnat_mult_nat.
ring.
Qed.

(** n <> 0 -> 0^n = 0, BBook: section 3.5.7, p.163, array 1 *)
Theorem Bnatexp_zero_l: forall (m: nat), 0 < m ->
  bbnat_exp_bin 0 m = 0.
Proof.
intros m H.
unfold bbnat_exp_bin.
rewrite valid_bbnat_exp_nat.
apply exp_0_l.
assumption.
Qed.

(** 1^m = 1, BBook: section 3.5.7, p.163, array 1 *)
Theorem Bnatexp_one_l: forall (m: nat),
  bbnat_exp_bin 1 m = 1.
Proof.
intros m.
unfold bbnat_exp_bin.
rewrite valid_bbnat_exp_nat.
apply exp_1_l.
Qed.

(** m^1 = m, BBook: section 3.5.7, p.163, array 1 *)
Theorem Bnatexp_one_r: forall (m: nat),
  bbnat_exp_bin m 1 = m.
Proof.
intros m.
unfold bbnat_exp_bin, bbnat_succ_unary.
rewrite valid_bbnat_exp_nat.
apply exp_1_r.
Qed.

(** m^( n + p ) = m^n * m^p, BBook: section 3.5.7, p.163, array 1 *)
Theorem Bnatexp_distributive_l: forall (m n p: nat),
  bbnat_exp_bin m (bbnat_plus_bin n p) = bbnat_mult_bin (bbnat_exp_bin m n) (bbnat_exp_bin m p).
Proof.
intros m n p.
unfold bbnat_exp_bin, bbnat_mult_bin, bbnat_plus_bin.
rewrite valid_bbnat_exp_nat.
rewrite valid_bbnat_mult_nat.
rewrite valid_bbnat_plus_nat.
rewrite valid_bbnat_exp_nat.
rewrite valid_bbnat_exp_nat.
apply exp_plus_distr_l.
Qed.


(** m^( n * p ) = (m^n)^p, BBook: section 3.5.7, p.163, array 1 *)
Theorem Bnatexp_assoc: forall (m n p: nat),
  bbnat_exp_bin m (bbnat_mult_bin n p) = bbnat_exp_bin (bbnat_exp_bin m n) p.
Proof.
intros m n p.
unfold bbnat_exp_bin, bbnat_mult_bin.
rewrite valid_bbnat_exp_nat.
rewrite valid_bbnat_mult_nat.
rewrite valid_bbnat_exp_nat.
rewrite valid_bbnat_exp_nat.
apply exp_assoc.
Qed.

(** ( m * n )^p = m^p * n^p, BBook: section 3.5.7, p.163, array 1 *)
Theorem Bnatexp_distributive_r: forall (m n p: nat),
  bbnat_exp_bin (bbnat_mult_bin m n) p = bbnat_mult_bin (bbnat_exp_bin m p) (bbnat_exp_bin n p).
Proof.
intros m n p.
unfold bbnat_exp_bin, bbnat_mult_bin.
rewrite valid_bbnat_exp_nat.
rewrite valid_bbnat_mult_nat.
rewrite valid_bbnat_mult_nat.
rewrite valid_bbnat_exp_nat.
rewrite valid_bbnat_exp_nat.
apply exp_mult_distr_r.
Qed.
