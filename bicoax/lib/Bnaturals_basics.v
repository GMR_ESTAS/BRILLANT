(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bchapter2.
Require Import Bfinite_subsets.
Require Import Binfinite_subsets.
Require Import Infinite_sets.
Require Import Bfixpoints.
Require Import Arith.

Open Local Scope nat_scope.

(**
  This library will just be used for BBook validation purposes: it
  does not make sense to use several numeric datatypes when one is
  enough. Hence we check the BBook's theorems here but, normally, this
  library shall never be used in production.

  We shall use [Full_set nat] for describing sets of natural numbers.

*)

(**
  * Natural numbers: definitions and "axioms"
  *)

(**
  Zero, BBook: section 3.5.1, p.146, array 1, row 1
  *)
Definition bbN_0 := Setminus BIG BIG.

(** Note/TODO: move this theorem in Bbasic ? *)
Theorem inhabited_BIG: inhabited BIG_type.
Proof.
assert (Inhabited _ (Setminus BIG (Empty_set _))).
apply make_new_approximant.
apply SET_6.
constructor.
left.
red; intros; contradiction.
induction H.
induction H.
exact (inhabits x).
Qed.

(**
  succ, BBook: section 3.5.1, p.146, array 1, row 2
  *)
Definition bbN_succ :=
  lambda (Power_finite BIG)
    (fun n => Union (Singleton (Bchoice _ inhabited_BIG (Setminus BIG n))) n).

(**
  genat, BBook: section 3.5.1, p.146, array 2, row 1
  *)
Definition genat :=
  lambda (Power_set (Power_finite BIG))
  (fun s => Union (Singleton bbN_0) (image bbN_succ s)).

(**
  Set of natural numbers, BBook: section 3.5.1, p.146, array 2, row 2
  *)
Definition bbN := Bfix (Power_finite BIG) genat.

(**
  property 3.5.1, BBook: section 3.5.1, p.146
  also:
  Peano 1, BBook: section 3.5.2, p.148, array 1, row 1
  *)
Theorem bbN_0_in_bbN: In bbN bbN_0.
Proof.
constructor.
constructor.
intros.
induction H.
apply H.
unfold genat; apply abstract_lambda.
left; intuition.
Qed.

Theorem bbN_succ_y: forall (n: Ensemble BIG_type), In bbN n -> In (partial_function (Power_finite BIG) (Power_finite BIG)) bbN_succ /\ In (domain bbN_succ) n.
Proof.
intros.
split.
repeat (constructor).
red; intros.
induction H0.
constructor; [ assumption | ].
rewrite H1.
apply finite_union.
constructor.
constructor.
(* TODO: a theorem for finitude of a singleton *)
replace (Singleton (Bchoice _ inhabited_BIG (Setminus BIG x))) with (Add _ (Empty_set _) (Bchoice _ inhabited_BIG (Setminus BIG x))).
apply Add_preserves_Finite; left.
rewrite (Empty_set_zero'); reflexivity.
red; intros z H2.
constructor.
assumption.
intros.
inversion H0; inversion H1.
rewrite H5; rewrite H9; reflexivity.
constructor.
exists (Union (Singleton (Bchoice _ inhabited_BIG (Setminus BIG n))) n).
constructor; [ | reflexivity ].
induction H.
induction H.
apply H.
assert (In (total_function (Power_set (Power_finite BIG)) (Power_set (Power_finite BIG))) genat).
unfold genat; apply lambda_is_total.
intros x0 H0; constructor; red; intros z H1.
induction H1.
induction H1.
unfold bbN_0; rewrite (empty_set_1 _ BIG).
constructor.
left.
red; intros; contradiction.
induction H1.
induction H1.
inversion H2.
rewrite H6.
apply finite_union.
constructor.
constructor.
replace (Singleton (Bchoice _ inhabited_BIG (Setminus BIG x1))) with (Add _ (Empty_set _) (Bchoice _ inhabited_BIG (Setminus BIG x1))).
apply Add_preserves_Finite; left.
rewrite (Empty_set_zero'); reflexivity.
red; intros z H7; constructor.
assumption.
assert (In (partial_function (Power_set (Power_finite BIG)) (Power_set (Power_finite BIG))) genat /\ In (domain genat) (Power_finite BIG)).
split.
induction H0; assumption.
constructor.
exists (Union (Singleton bbN_0) (image bbN_succ (Power_finite BIG))).
constructor.
constructor; intuition.
reflexivity.
exists H1.
(* TODO: hmmm, there are some repetitive steps, but I fail at spotting them *)
unfold genat in H1 |- *.
apply abstract_lambda.
red; intros z H2.
induction H2.
induction H2.
unfold bbN_0; rewrite (empty_set_1 _ BIG).
constructor.
left.
red; intros; contradiction.
induction H2.
induction H2.
inversion H3.
rewrite H7.
apply finite_union.
constructor.
constructor.
replace (Singleton (Bchoice _ inhabited_BIG (Setminus BIG x0))) with (Add _ (Empty_set _) (Bchoice _ inhabited_BIG (Setminus BIG x0))).
apply Add_preserves_Finite; left.
rewrite (Empty_set_zero'); reflexivity.
red; intros z H8; constructor.
assumption.
Qed.

(**
  property 3.5.2, BBook: section 3.5.1, p.146
  also:
  Peano 2, BBook: section 3.5.2, p.148, array 1, row 2
  *)
Theorem bbN_succ_in_bbN: forall (n: Ensemble BIG_type) (H:In bbN n), In bbN (app bbN_succ n (bbN_succ_y n H)).
Proof.
intros n H.
constructor.
constructor.
intros.
induction H0.
apply H0.
unfold genat.
unfold bbN_succ.
apply abstract_lambda.
apply abstract_lambda.
right.
red; apply image_intro with n.
split.
induction H.
induction H.
apply H.
exists x; assumption.
constructor; [ | reflexivity ].
induction H.
induction H.
apply H.
assert (In (partial_function (Power_set (Power_finite BIG)) (Power_set (Power_finite BIG))) genat /\ In (domain genat) (Power_finite BIG)).
inversion x.
split; [ intuition | ].
constructor; exists (Union (Singleton bbN_0) (image bbN_succ (Power_finite BIG))).
constructor.
constructor; intuition.
reflexivity.
exists H1.
assert (In (Power_set (Power_finite BIG)) (app genat (Power_finite BIG) H1)).
apply application_in_codomain.
induction H2; assumption.
Qed.

(**
  theorem 3.5.1, BBook: section 3.5.1, p.146
  also:
  Peano 5, BBook: section 3.5.2, p.148, array 1, row 5
  *)
Theorem BBook_principle_of_mathematical_induction: forall (P: (Ensemble BIG_type) -> Prop),
  P (bbN_0) ->
  (forall (n: Ensemble BIG_type) (H: (In bbN n)), (P n) -> (P (app bbN_succ n (bbN_succ_y _ H)))) ->
  (forall (n: Ensemble BIG_type), In bbN n -> (P n)).
Proof.
intros P H H0 n H1.
apply first_special_case_theorem with (Ensemble BIG_type) (Power_finite BIG) bbN_0 bbN_succ genat.
unfold bbN_0.
rewrite (empty_set_1 _ BIG).
constructor.
left.
intros z H2; contradiction.
unfold bbN_succ; apply lambda_is_total.
intros x H2.
apply finite_union.
constructor.
constructor.
replace (Singleton (Bchoice _ inhabited_BIG (Setminus BIG x))) with (Add _ (Empty_set _) (Bchoice _ inhabited_BIG (Setminus BIG x))).
apply Add_preserves_Finite; left.
rewrite (Empty_set_zero'); reflexivity.
red; intros z H3.
constructor.
assumption.
reflexivity.
assumption.
intros x H2.
assert (In bbN x); intuition.
generalize (H0 x H3 H5); intros.
exists (bbN_succ_y x H3); assumption.
assumption.
Qed.

(**
  One, BBook: section 3.5.1, p.147, array 1, row 1
  *)
Definition bbN_one:= app bbN_succ bbN_0 (bbN_succ_y bbN_0 bbN_0_in_bbN).

(**
  Set of natural numbers without zero, BBook: section 3.5.1, p.147, array 1, row 2
  *)
Definition bbN1:= Setminus bbN (Singleton bbN_0).

(**
  Predecessor, BBook: section 3.5.1, p.147, array 1, row 3
  *)
Definition bbN_pred := range_restriction (inverse bbN_succ) bbN.

(**
  Inequalities over natural numbers, BBook: section 3.5.1, p.147, array 2
  *)
Definition bbN_le (n: Ensemble BIG_type) (m: Ensemble BIG_type) := Included n m.
Definition bbN_lt (n: Ensemble BIG_type) (m: Ensemble BIG_type) := n <> m /\ Included n m.
Definition bbN_ge (n: Ensemble BIG_type) (m: Ensemble BIG_type) := bbN_le m n.
Definition bbN_gt (n: Ensemble BIG_type) (m: Ensemble BIG_type) := bbN_lt m n.

(** Note/TODO: binary relations defined upon inequalities: gtr, geq, lss, leq *)


Theorem Add_of_Union: forall (S: Type) (s: Ensemble S) (x: Ensemble S) (inhabited_s: inhabited S),
Union x (Singleton (Bchoice S inhabited_s (Setminus s x))) = Add S x (Bchoice S inhabited_s (Setminus s x)).
Proof.
intros S s x inhabited_s.
apply Extensionality_Ensembles; split.

intros z H.
induction H.
left; assumption.
right; assumption.

intros z H.
induction H.
left; assumption.
right; assumption.
Qed.

(**
  Peano 3, BBook: section 3.5.2, p.148, array 1, row 3
  *)
Theorem bbN_succ_is_not_bbN_0: forall (n: Ensemble BIG_type) (H: In bbN n), app bbN_succ n (bbN_succ_y n H) <> bbN_0.
Proof.
intros n H.
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative.
rewrite Add_of_Union.
unfold bbN_0; rewrite empty_set_1.
apply Add_not_Empty.
Qed.


(** Note/TODO: move this theorem elsewhere (where Setminus is, I think) *)
Theorem chosen_not_in_setminus: forall (S: Type) (s: Ensemble S) (x: Ensemble S) (inhabited_S: inhabited S),
~(Setminus s x = (Empty_set S)) -> ~(In x (Bchoice S inhabited_S (Setminus s x))).
Proof.
intros S s x inhabited_S H.
unfold Bchoice; apply epsilon_ind.
assert (Inhabited S (Setminus s x)).
apply not_empty_Inhabited; assumption.
induction H0.
exists x0; assumption.
intros x0 H0.
induction H0; assumption.
Qed.

Theorem BIG_chosen_not_in_setminus: forall (x: Ensemble BIG_type), Finite x -> ~(In x (Bchoice BIG_type inhabited_BIG (Setminus BIG x))).
Proof.
intros x H.
apply chosen_not_in_setminus.
apply Inhabited_not_empty.
apply make_new_approximant.
apply SET_6.
constructor.
assumption.
red; intros; constructor.
Qed.

Theorem bbN_natural_is_finite: forall (n: Ensemble BIG_type), In bbN n -> Finite n.
Proof.
intros n H.
apply BBook_principle_of_mathematical_induction.

unfold bbN_0; rewrite empty_set_1; left.
intros n0 H0 H1.
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative.
rewrite Add_of_Union.
right.
assumption.
unfold Bchoice; apply epsilon_ind.
assert (Inhabited _ (Setminus BIG n0)).
apply make_new_approximant.
apply SET_6.
constructor; [ assumption | red; intros; constructor].
induction H2.
exists x; assumption.
intros.
induction H2; assumption.
assumption.
Qed.

Theorem bbN_lt_succ: forall (m: Ensemble BIG_type) (H: In bbN m), bbN_lt m (app bbN_succ m (bbN_succ_y m H)).
Proof.
intros m H.
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative.
rewrite Add_of_Union.
unfold bbN_lt.
split.
intros H0.
assert (Same_set _ m (Add BIG_type m (Bchoice BIG_type inhabited_BIG (Setminus BIG m)))).
rewrite <- H0; intuition.
induction H1.
assert (exists x, In (Add BIG_type m (Bchoice BIG_type inhabited_BIG (Setminus BIG m))) x /\ ~(In m x)).
exists (Bchoice BIG_type inhabited_BIG (Setminus BIG m)).
split.
right; intuition.
apply BIG_chosen_not_in_setminus.
apply bbN_natural_is_finite; assumption.
generalize H3.
apply all_not_not_ex.
intros n.
intuition.
red; intros; left; assumption.
Qed.

(**
  property 3.5.6, BBook: section 3.5.2, p.149
  *)
Theorem preliminary_to_trichotomy1: forall (m: Ensemble BIG_type)
  (in_bbN_m: In bbN m),
  (forall (n: Ensemble BIG_type) (in_bbN_n:In bbN n), bbN_lt n m -> bbN_le (app bbN_succ n (bbN_succ_y n in_bbN_n)) m) ->
  (forall (n: Ensemble BIG_type), In bbN n -> ((bbN_le n m) \/ (bbN_le (app bbN_succ m (bbN_succ_y m in_bbN_m)) n))).
Proof.
intros m H H0.
intros n H1.
pattern n.
apply BBook_principle_of_mathematical_induction.

left.
unfold bbN_le; unfold bbN_0; rewrite empty_set_1; red; intros; contradiction.

intros n0 H2.
intros H3.
induction H3.
assert (bbN_lt n0 m \/ n0 = m).
unfold bbN_lt; unfold bbN_le in H3.
assert (n0 = m \/ (n0 <> m /\ Included n0 m)).
assert (~n0 <> m \/ (n0 <> m /\ Included n0 m)).
apply imply_to_or.
intuition.
induction H4.
left; apply NNPP; assumption.
right; assumption.
intuition.
induction H4.
left.
induction H4.
apply H0.
unfold bbN_lt; intuition.
right.
unfold bbN_succ; do 2 (apply abstract_lambda).
unfold bbN_le; rewrite H4; intuition.
right.
assert (bbN_lt n0 (app bbN_succ n0 (bbN_succ_y n0 H2))).
apply bbN_lt_succ.
unfold bbN_le in H3 |- *.
unfold bbN_lt in H4.
induction H4.
apply inclusion_transitivity with n0; assumption.

assumption.
Qed.

(**
  property 3.5.5, BBook: section 3.5.2, p.149
  *)
Theorem preliminary_to_trichotomy2: forall (m: Ensemble BIG_type),
  In bbN m ->
  (forall (n: Ensemble BIG_type) (in_bbN_n:In bbN n), bbN_lt n m -> bbN_le (app bbN_succ n (bbN_succ_y n in_bbN_n)) m).
Proof.
intros m H.
pattern m.
apply BBook_principle_of_mathematical_induction.

intros n in_bbN_n H0.
unfold bbN_lt in H0.
induction H0.
assert (n = Empty_set BIG_type).
apply less_than_empty.
rewrite <- (empty_set_1 BIG_type BIG).
unfold bbN_0 in H1; assumption.
assert (n <> Empty_set _).
rewrite <- (empty_set_1 BIG_type BIG).
unfold bbN_0 in H1; assumption.
contradiction.

intros n in_bbN_n H1.
intros n0 in_bbN_n0 H2.
(* SC: automatic renaming of Coq makes following the BBook a bit difficult *)
assert ((bbN_le n0 n) \/ (bbN_le (app bbN_succ n (bbN_succ_y n in_bbN_n)) n0)).
apply preliminary_to_trichotomy1.
intros.
apply H1; assumption.
assumption.
induction H0.
assert (bbN_lt n0 n \/ n0 = n).
(* SC: hop, copy/paste from the proof above *)
unfold bbN_lt; unfold bbN_le in H0.
assert (n0 = n \/ (n0 <> n /\ Included n0 n)).
assert (~n0 <> n \/ (n0 <> n /\ Included n0 n)).
apply imply_to_or.
intuition.
induction H3.
left; apply NNPP; assumption.
right; assumption.
intuition.
induction H3.
generalize (H1 n0 in_bbN_n0 H3).
intros.
unfold bbN_le; apply inclusion_transitivity with n.
unfold bbN_le in H4; assumption.
unfold bbN_succ; apply abstract_lambda.
red; intros; right; assumption.
unfold bbN_succ; do 2 (apply abstract_lambda).
rewrite H3; unfold bbN_le; intuition.
unfold bbN_lt in H2.
unfold bbN_le in H0 |- *.
induction H2.
assert (n0 = (app bbN_succ n (bbN_succ_y n in_bbN_n))).
apply Extensionality_Ensembles; constructor; intuition.
contradiction.

assumption.
Qed.

(**
  property 3.5.4, BBook: section 3.5.2, p.149
  *)
Theorem bbN_inequality_trichotomy: forall (m n: Ensemble BIG_type), In (times bbN bbN) (m,n) -> (bbN_lt n m \/ n = m \/ bbN_lt m n).
intros m n H.
assert (~(n<>m) \/ (bbN_lt n m) \/ bbN_lt m n).
apply imply_to_or.
intros.
unfold bbN_lt.
assert (Included n m \/ Included m n).
inversion H.
assert (bbN_le n m \/ bbN_le (app bbN_succ m (bbN_succ_y m H3)) n).
apply preliminary_to_trichotomy1.
intros.
apply preliminary_to_trichotomy2; assumption.
assumption.
unfold bbN_le in H5; induction H5.
left; assumption.
right.
red; intros; apply H5.
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative; rewrite Add_of_Union.
left; assumption.
intuition.
unfold bbN_lt in * |- *.
induction H0.
right; left; apply NNPP; assumption.
induction H0.
left; assumption.
right; right; assumption.
Qed.

(**
  Peano 4, BBook: section 3.5.2, p.148, array 1, row 4

  error: in the BBook, a universal quantification for m is missing
  *)
Theorem bbN_succ_injective: forall (n m: Ensemble BIG_type)
  (in_bbN_n: In bbN n) (in_bbN_m: In bbN m),
  (app bbN_succ n (bbN_succ_y n in_bbN_n) = app bbN_succ m (bbN_succ_y m in_bbN_m)) ->
  n = m.
Proof.
intros n m In_N_n In_N_m.
unfold bbN_succ.
do 2 (apply abstract_lambda).
intros.
apply (proj2 (contraposition_1 ((Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG n))) n)=(Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG m))) m)) (n=m))); [ | assumption ].
intros.
assert (In (times bbN bbN) (n,m)); [ constructor; assumption | ].
generalize (bbN_inequality_trichotomy n m H1); intros.
induction H2.
(* SC: another weirdness with dependent hypotheses *)
assert (In bbN n); [ exact In_N_n | ].
assert (In bbN m); [ exact In_N_m | ].
assert (bbN_le (app bbN_succ m (bbN_succ_y m H4)) n).
apply preliminary_to_trichotomy2; assumption.
unfold bbN_le in H5.
assert (Included (Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG m))) m) n).
assert ((Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG m))) m) = (app bbN_succ m (bbN_succ_y m H4))).
unfold bbN_succ; apply abstract_lambda; reflexivity.
rewrite H6.
assumption.
intros H7.
rewrite <- H7 in H6.
generalize H6.
unfold Included; apply ex_not_not_all.
exists (Bchoice BIG_type inhabited_BIG (Setminus BIG n)).
apply (proj2 (de_Morgan_not_implies_and
 (In (Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG n))) n) (Bchoice BIG_type inhabited_BIG (Setminus BIG n)))
 (In n (Bchoice BIG_type inhabited_BIG (Setminus BIG n)))
)).
split.
left; intuition.
apply BIG_chosen_not_in_setminus.
apply bbN_natural_is_finite; assumption.

induction H2.
assert (n=m); [ rewrite H2; reflexivity | ].
contradiction.

assert (In bbN n); [ exact In_N_n | ].
assert (In bbN m); [ exact In_N_m | ].
assert (bbN_le (app bbN_succ n (bbN_succ_y n H3)) m).
apply preliminary_to_trichotomy2; assumption.
unfold bbN_le in H5.
assert (Included (Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG n))) n) m).
assert ((Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG n))) n) = (app bbN_succ n (bbN_succ_y n H3))).
unfold bbN_succ; apply abstract_lambda; reflexivity.
rewrite H6.
assumption.
intros H7.
rewrite  H7 in H6.
generalize H6.
unfold Included; apply ex_not_not_all.
exists (Bchoice BIG_type inhabited_BIG (Setminus BIG m)).
apply (proj2 (de_Morgan_not_implies_and
 (In (Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG m))) m) (Bchoice BIG_type inhabited_BIG (Setminus BIG m)))
 (In m (Bchoice BIG_type inhabited_BIG (Setminus BIG m)))
)).
split.
left; intuition.
apply BIG_chosen_not_in_setminus.
apply bbN_natural_is_finite; assumption.
Qed.

(**
  * Natural numbers: validity and morphisms to Coq's nat
  *)

Theorem inhabited_nat: inhabited nat.
Proof.
exact (inhabits 0%nat).
Qed.

Definition Bcardinal (S: Type) (s: Ensemble S) :=
iota _ inhabited_nat (fun n => cardinal S s n).

Definition nat_of_bbN (n: Ensemble BIG_type) := Bcardinal BIG_type n.

Theorem inhabited_Ensemble_BIG_type: inhabited (Ensemble BIG_type).
Proof.
exact (inhabits (Empty_set BIG_type)).
Qed.

(**
  This definition has me to do a remark: the "choice" function works
  here because it chooses consistently, i.e. we can prove (n = m ->
  Bchoice n = Bchoice m) because the "choice" is done the same way on
  the two sides of the equality. Is this definition of choice more
  "valid" than an unstable choice ? More a philosophy question,
  but relevant nonetheless.
*)

Fixpoint bbN_of_nat (n: nat) :=
match n with
| 0%nat => Setminus BIG BIG
| (S m) =>
   let B_m := bbN_of_nat m in
   Union B_m (Singleton (Bchoice _ inhabited_BIG (Setminus BIG B_m)))
end.


Theorem bbN_0_nat_zero: bbN_0 = bbN_of_nat 0.
Proof.
simpl.
reflexivity.
Qed.

Theorem nat_zero_bbN_0: 0%nat = (nat_of_bbN bbN_0).
Proof.
unfold bbN_0; rewrite (empty_set_1 _ BIG).
unfold nat_of_bbN.
unfold Bcardinal.
apply iota_ind.
intros.
induction H.
symmetry; apply H0.
left.
exists 0%nat.
split.
left.
intros.
apply cardinal_unicity with BIG_type (Empty_set BIG_type).
left.
assumption.
Qed.

Theorem bbN_0_cardinality: cardinal BIG_type bbN_0 (nat_of_bbN bbN_0).
Proof.
rewrite <- nat_zero_bbN_0.
unfold bbN_0; rewrite empty_set_1; left.
Qed.

Theorem bbN_succ_cardinality: forall (n: Ensemble BIG_type),
  In bbN n ->
  cardinal BIG_type n (nat_of_bbN n) ->
  cardinal BIG_type (Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG n))) n) (S (nat_of_bbN n)).
Proof.
intros n H H0.
rewrite Union_commutative; rewrite Add_of_Union.
right.
assumption.
apply BIG_chosen_not_in_setminus.
apply bbN_natural_is_finite.
assumption.
Qed.

Theorem cardinal_bbN_nat_of_bbN: forall (n: Ensemble BIG_type),
  In bbN n ->
  cardinal BIG_type n (nat_of_bbN n).
Proof.
intros n H.
pattern n.
apply BBook_principle_of_mathematical_induction.

apply bbN_0_cardinality.
intros n0 H0 H1.
unfold bbN_succ; apply abstract_lambda.
unfold nat_of_bbN; unfold Bcardinal; apply iota_ind.
intros.
induction H2.
assumption.
exists (S (nat_of_bbN n0)).
split.
apply bbN_succ_cardinality; assumption.
intros.
apply cardinal_unicity with BIG_type (Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG n0))) n0).
apply bbN_succ_cardinality; assumption.
assumption.
assumption.
Qed.

Theorem bbN_naturalural_unicity: forall (n: Ensemble BIG_type), In bbN n ->
forall (m: Ensemble BIG_type) (card: nat), In bbN m -> cardinal BIG_type n card ->cardinal BIG_type m card -> m = n.
Proof.
intros n H.
pattern n.
apply BBook_principle_of_mathematical_induction.

intros m card H0 H1.
pattern m.
apply BBook_principle_of_mathematical_induction.
reflexivity.
unfold bbN_succ.
intros n0 H2 H3.
apply abstract_lambda.
intros H4.
assert (card > 0).
apply inh_card_gt_O with BIG_type (Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG n0))) n0).
rewrite Union_commutative; rewrite Add_of_Union.
apply Inhabited_add.
assumption.
assert (card = 0).
symmetry; apply cardinal_Empty with BIG_type.
unfold bbN_0 in H1; rewrite empty_set_1 in H1; assumption.
induction H5.
assert (0 <> 1).
apply O_S.
assert (0 = 1); [ rewrite H6; reflexivity | contradiction ].
assert (0 <> (S m0)); [ apply O_S | ].
assert (0 = S m0); [ rewrite H6; reflexivity | contradiction ].
assumption.

intros n0 H0 H1.
intros m card H2.
unfold bbN_succ; apply abstract_lambda.
intros H3.
pattern m.
apply BBook_principle_of_mathematical_induction.
intros.
assert (card > 0).
apply inh_card_gt_O with BIG_type (Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG n0))) n0).
rewrite Union_commutative; rewrite Add_of_Union.
apply Inhabited_add.
assumption.
assert (card = 0).
symmetry; apply cardinal_Empty with BIG_type.
unfold bbN_0 in H4; rewrite empty_set_1 in H4; assumption.
induction H5.
assert (0 <> 1).
apply O_S.
assert (0 = 1); [ rewrite H6; reflexivity | contradiction ].
assert (0 <> (S m0)); [ apply O_S | ].
assert (0 = S m0); [ rewrite H6; reflexivity | contradiction ].
intros n1 H4 H5.
unfold bbN_succ; apply abstract_lambda.
intros H6.
assert (n1 = n0).
apply H1 with (pred card).
assumption.
replace n0 with (Subtract _ (Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG n0))) n0) (Bchoice BIG_type inhabited_BIG (Setminus BIG n0))).
apply card_soustr_1.
assumption.
intuition.
unfold Subtract.
apply Extensionality_Ensembles; split; red; intros.
induction H7.
induction H7.
contradiction.
assumption.
red.
constructor.
right; assumption.
unfold Bchoice.
apply epsilon_ind.
assert (Inhabited _ (Setminus BIG n0)).
apply make_new_approximant.
apply SET_6.
constructor.
apply bbN_natural_is_finite; assumption.
red; intros; constructor.
induction H8.
exists x0; assumption.
intros.
induction H8.
intros H10; induction H10; contradiction.
(* TODO: Hmmm, some copy/paste again ? *)
replace n1 with (Subtract _ (Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG n1))) n1) (Bchoice BIG_type inhabited_BIG (Setminus BIG n1))).
apply card_soustr_1.
assumption.
intuition.
unfold Subtract.
apply Extensionality_Ensembles; split; red; intros.
induction H7.
induction H7.
contradiction.
assumption.
red.
constructor.
right; assumption.
unfold Bchoice.
apply epsilon_ind.
assert (Inhabited _ (Setminus BIG n1)).
apply make_new_approximant.
apply SET_6.
constructor.
apply bbN_natural_is_finite; assumption.
red; intros; constructor.
induction H8.
exists x0; assumption.
intros.
induction H8.
intros H10; induction H10; contradiction.
rewrite H7; reflexivity.
assumption.

assumption.
Qed.

Theorem bbN_of_nat_of_bbN: forall (n: Ensemble BIG_type), In bbN n ->
  (bbN_of_nat (nat_of_bbN n)) = n.
Proof.
intros n H.
pattern n.
apply BBook_principle_of_mathematical_induction.

rewrite <- nat_zero_bbN_0.
rewrite <- bbN_0_nat_zero.
reflexivity.

intros n0 H0 H1.
assert (
   (nat_of_bbN (app bbN_succ n0 (bbN_succ_y n0 H0)))
= (S (nat_of_bbN n0))).
unfold bbN_succ; apply abstract_lambda.
unfold nat_of_bbN; unfold Bcardinal.
apply iota_ind.
intros.
induction H2.
apply H3.
rewrite Union_commutative; rewrite Add_of_Union.
right.
assert (nat_of_bbN n0 =  (iota nat inhabited_nat (fun n1 : nat => cardinal BIG_type n0 n1))).
unfold nat_of_bbN; unfold Bcardinal.
reflexivity.
rewrite <- H4.
apply cardinal_bbN_nat_of_bbN.
assumption.
apply BIG_chosen_not_in_setminus.
apply bbN_natural_is_finite.
assumption.
exists (S (Bcardinal _ n0)).
split.
rewrite Union_commutative; rewrite Add_of_Union.
right.
apply cardinal_bbN_nat_of_bbN.
assumption.
apply BIG_chosen_not_in_setminus.
apply bbN_natural_is_finite.
assumption.
intros.
apply cardinal_unicity with BIG_type (Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG n0))) n0).
rewrite Union_commutative; rewrite Add_of_Union.
right.
apply cardinal_bbN_nat_of_bbN.
assumption.
apply BIG_chosen_not_in_setminus.
apply bbN_natural_is_finite.
assumption.
assumption.
rewrite H2.
simpl.
rewrite H1.
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative; reflexivity.

assumption.
Qed.

Theorem bbN_of_nat_in_bbN: forall (n: nat), In bbN (bbN_of_nat n).
Proof.
intros n.
induction n.

simpl; apply bbN_0_in_bbN.

simpl.
replace (
  Union (bbN_of_nat n)
(Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG (bbN_of_nat n)))))
with
  (app bbN_succ (bbN_of_nat n) (bbN_succ_y (bbN_of_nat n) IHn)).
apply bbN_succ_in_bbN.
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative; reflexivity.
Qed.

Theorem nat_cardinal_matches_coercion: forall (n:nat), cardinal BIG_type (bbN_of_nat n) n.
Proof.
intros n.
induction n.

simpl; rewrite empty_set_1; left.

simpl.
rewrite Add_of_Union; right.
assumption.
apply BIG_chosen_not_in_setminus.
apply bbN_natural_is_finite.
apply bbN_of_nat_in_bbN.
Qed.

Theorem nat_2coercions_identity: forall (n: nat), (nat_of_bbN (bbN_of_nat n)) = n.
Proof.
intros n.
induction n.

simpl.
symmetry; apply nat_zero_bbN_0.

simpl.
apply cardinal_unicity with BIG_type
   (Union (bbN_of_nat n) (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG (bbN_of_nat n))))).
apply cardinal_bbN_nat_of_bbN.
replace (Union (bbN_of_nat n) (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG (bbN_of_nat n))))) with
(app bbN_succ (bbN_of_nat n) (bbN_succ_y (bbN_of_nat n) (bbN_of_nat_in_bbN n))).
apply bbN_succ_in_bbN.
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative; reflexivity.
rewrite Add_of_Union; right.
apply nat_cardinal_matches_coercion.
apply BIG_chosen_not_in_setminus.
apply bbN_natural_is_finite.
apply bbN_of_nat_in_bbN.
Qed.

Theorem bbN_succ_nat_S: forall (n: Ensemble BIG_type) (H:In bbN n), app bbN_succ n (bbN_succ_y n H) = bbN_of_nat (S (nat_of_bbN n)).
Proof.
intros n H.
simpl.
unfold bbN_succ; apply abstract_lambda.
pattern n.
apply BBook_principle_of_mathematical_induction.

rewrite <- nat_zero_bbN_0.
rewrite <- bbN_0_nat_zero.
rewrite Union_commutative; reflexivity.

intros n0 H0 H1.
rewrite bbN_of_nat_of_bbN.
rewrite Union_commutative; reflexivity.
apply bbN_succ_in_bbN.

assumption.
Qed.

(**
  The "type-dependent on the belonging of n in bbN" version is
  hard on Coq's tactics :-/
*)
Theorem bbnat_succ_S_nondependent: forall (n: Ensemble BIG_type),
  In bbN n ->
  Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG n))) n =
  bbN_of_nat (S (nat_of_bbN n)).
Proof.
intros n H.
generalize (bbN_succ_nat_S n H); intros.
rewrite <- H0.
unfold bbN_succ; apply abstract_lambda; reflexivity.
Qed.

Theorem nat_S_bbN_succ: forall (n: nat),
(S n) =
(nat_of_bbN (app bbN_succ (bbN_of_nat n) (bbN_succ_y (bbN_of_nat n) (bbN_of_nat_in_bbN n)))).
Proof.
intros.
induction n.

unfold bbN_succ; apply abstract_lambda.
rewrite <- bbN_0_nat_zero.
apply cardinal_unicity with BIG_type (app bbN_succ bbN_0 (bbN_succ_y bbN_0 bbN_0_in_bbN)).
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative; rewrite Add_of_Union; right.
unfold bbN_0; rewrite empty_set_1; left.
apply BIG_chosen_not_in_setminus.
apply bbN_natural_is_finite.
apply bbN_0_in_bbN.
unfold bbN_succ; apply abstract_lambda.
apply cardinal_bbN_nat_of_bbN.
replace (Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG bbN_0))) bbN_0) with
(app bbN_succ bbN_0 (bbN_succ_y bbN_0 bbN_0_in_bbN)).
apply bbN_succ_in_bbN.
unfold bbN_succ; apply abstract_lambda.
reflexivity.

unfold bbN_succ; apply abstract_lambda.
rewrite bbnat_succ_S_nondependent.
apply cardinal_unicity with BIG_type
(app bbN_succ (bbN_of_nat (S n)) (bbN_succ_y (bbN_of_nat (S n)) (bbN_of_nat_in_bbN (S n)))).
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative; rewrite Add_of_Union; right.
apply nat_cardinal_matches_coercion.
apply BIG_chosen_not_in_setminus.
apply bbN_natural_is_finite.
apply bbN_of_nat_in_bbN.
replace (bbN_of_nat (S (nat_of_bbN (bbN_of_nat (S n))))) with
  (app bbN_succ (bbN_of_nat (S n)) (bbN_succ_y (bbN_of_nat (S n)) (bbN_of_nat_in_bbN (S n)))).
apply cardinal_bbN_nat_of_bbN.
apply bbN_succ_in_bbN.
unfold bbN_succ; apply abstract_lambda.
simpl.
rewrite Union_commutative.
rewrite bbN_of_nat_of_bbN.
reflexivity.
replace (Union (bbN_of_nat n) (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG (bbN_of_nat n))))) with
(app bbN_succ (bbN_of_nat n) (bbN_succ_y (bbN_of_nat n) (bbN_of_nat_in_bbN n))).
apply bbN_succ_in_bbN.
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative; reflexivity.
apply bbN_of_nat_in_bbN.
Qed.

Theorem nat_S_bbN_succ_nondependent: forall (n: nat),
  (S n) =
  (nat_of_bbN ((Union (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG (bbN_of_nat n)))) (bbN_of_nat n)))).
Proof.
intros n.
rewrite (nat_S_bbN_succ n).
unfold bbN_succ; apply abstract_lambda; reflexivity.
Qed.

(**
  We just proved that there is an isomorphism wrt S (or bbN_succ),
  hence any proof involving bbN_succ can be made with S, and
  vice-versa (same for zero). We can go from BBook naturals to Coq's
  nat, which means proofs easier and an access to the whole corpus of
  theorems of Arith. But we must still prove that this isomorphism
  preserves the behaviour of the other mathematical constructs: pred,
  min, max, plus, etc...  This is the goal of the next theorems:
  define the construct and show its Coq counterpart is equivalent
  modulo the "bbN to nat" morphism
*)


(**
  * Natural numbers: properties over other constructions
  *)

Theorem bbN_pred_partial_function: In (partial_function bbN1 bbN) bbN_pred.
Proof.
split.
split.

split.
red; intros.
induction H.
inversion H.
split.
assert (x = app bbN_succ y (bbN_succ_y y H0)).
apply identifying_app; assumption.
rewrite H4.
split.
apply bbN_succ_in_bbN.
intros H5.
assert ((app bbN_succ y (bbN_succ_y y H0)) = bbN_0).
induction H5; reflexivity.
generalize H6;apply bbN_succ_is_not_bbN_0.
assumption.

intros.
inversion H; inversion H0.
inversion H3; inversion H7.
inversion H10; inversion H13.
apply bbN_succ_injective with H4 H8.
unfold bbN_succ; do 2 (apply abstract_lambda).
rewrite <- H18; rewrite <- H22; reflexivity.
Qed.


(**
  property 3.5.3, BBook: section 3.5.2, p.148
  *)
Theorem bbN_pred_total_bijection: In (total_bijection bbN1 bbN) bbN_pred.
Proof.
split.
split.

apply bbN_pred_partial_function.

apply Extensionality_Ensembles; split.
red; intros.
induction H.
induction H.
inversion H.
inversion H2.
assert (a = app bbN_succ x (bbN_succ_y x H3)).
apply identifying_app; assumption.
rewrite H7.
split.
apply bbN_succ_in_bbN.
intros H8.
assert ((app bbN_succ x (bbN_succ_y x H3)) = bbN_0).
induction H8; reflexivity.
generalize H9;apply bbN_succ_is_not_bbN_0.

red; intros.
induction H.
assert (~(In (Singleton bbN_0) x) -> In (domain bbN_pred) x).
pattern x.
apply BBook_principle_of_mathematical_induction.
intros.
absurd (In (Singleton bbN_0) bbN_0); [ assumption | intuition ].
intros.
constructor.
exists n.
constructor.
constructor.
apply app_trivial_property.
assumption.
assumption.
intuition.

split.
split.
apply bbN_pred_partial_function.
intros.
inversion H; inversion H0.
inversion H3; inversion H7.
inversion H10; inversion H13.
rewrite H18; rewrite H22; reflexivity.

split.
apply bbN_pred_partial_function.
intros.
exists (app bbN_succ y (bbN_succ_y y H)).
constructor.
constructor.
apply app_trivial_property.
assumption.
Qed.

(**
  * Natural numbers: validity
  *)

Definition valid_bbN_0 := bbN_0_nat_zero.
Definition valid_nat_O := nat_zero_bbN_0.
Definition valid_bbN_succ := bbN_succ_nat_S.
Definition valid_nat_S :=nat_S_bbN_succ.

Theorem bbN_pred_y: forall (n: Ensemble BIG_type), In bbN1 n ->
In (partial_function bbN1 bbN) bbN_pred /\ In (domain bbN_pred) n.
Proof.
intros n H.
split.
apply bbN_pred_partial_function.
generalize bbN_pred_total_bijection; intros.
induction H0.
induction H0.
rewrite H2; assumption.
Qed.

Theorem bbN_of_pos_nat_in_bbN1: forall (n: nat), n > 0 -> In bbN1 (bbN_of_nat n).
Proof.
intros.
split.
apply bbN_of_nat_in_bbN.
intros H0.
assert (bbN_of_nat n = bbN_0).
induction H0; reflexivity.
assert (exists m, n = (S m)).
exists (pred n).
apply S_pred with 0.
assumption.
induction H2.
rewrite H2 in H1.
assert (Same_set _ (bbN_of_nat (S x)) bbN_0).
rewrite H1; intuition.
induction H3.
assert (In  (bbN_of_nat (S x)) (Bchoice BIG_type inhabited_BIG (Setminus BIG (bbN_of_nat x)))).
simpl; right; intuition.
generalize (H3 (Bchoice BIG_type inhabited_BIG (Setminus BIG (bbN_of_nat x))) H5).
unfold bbN_0; rewrite empty_set_1; intros; contradiction.
Qed.

Theorem bbN_succ_in_bbN1: forall (n: Ensemble BIG_type) (H: In bbN n),
  In bbN1 (app bbN_succ n (bbN_succ_y n H)).
Proof.
intros n H.
constructor.
apply bbN_succ_in_bbN.
intros H0.
assert ((app bbN_succ n (bbN_succ_y n H)) = bbN_0).
induction H0; reflexivity.
assert (Same_set _ (app bbN_succ n (bbN_succ_y n H)) bbN_0).
rewrite H1; intuition.
induction H2.
assert (In (app bbN_succ n (bbN_succ_y n H)) (Bchoice BIG_type inhabited_BIG (Setminus BIG n))).
unfold bbN_succ; apply abstract_lambda; left; intuition.
generalize (H2 _ H4).
unfold bbN_0; rewrite empty_set_1; intros; contradiction.
Qed.

Theorem bbN_pred_succ: forall (n: Ensemble BIG_type) (H: In bbN n),
app bbN_pred
(app bbN_succ n (bbN_succ_y n H))
(bbN_pred_y
     (app bbN_succ n (bbN_succ_y n H))
     (bbN_succ_in_bbN1 n H))
= n.
Proof.
intros n H.
symmetry; apply identifying_app.
constructor.
constructor.
apply app_trivial_property.
assumption.
Qed.

Theorem valid_pred_nat: forall (n:nat) (H: n > 0), (pred n) = nat_of_bbN (app bbN_pred (bbN_of_nat n) (bbN_pred_y (bbN_of_nat n) (bbN_of_pos_nat_in_bbN1 n H))).
Proof.
intros n H.
induction n.

absurd (0 < 0).
apply lt_irrefl.
unfold gt in H; assumption.
replace (app bbN_pred (bbN_of_nat (S n)) (bbN_pred_y (bbN_of_nat (S n)) (bbN_of_pos_nat_in_bbN1 (S n) H)))
with (bbN_of_nat n).
simpl.
symmetry; apply nat_2coercions_identity.
apply identifying_app.
simpl.
split.
constructor.
assert (In bbN (bbN_of_nat n)).
apply bbN_of_nat_in_bbN.
replace (Union (bbN_of_nat n) (Singleton (Bchoice BIG_type inhabited_BIG (Setminus BIG (bbN_of_nat n)))))
with
  (app bbN_succ (bbN_of_nat n) (bbN_succ_y (bbN_of_nat n) H0))
.
apply app_trivial_property.
unfold bbN_succ; apply abstract_lambda.
rewrite Union_commutative; reflexivity.
apply bbN_of_nat_in_bbN.
Qed.

Theorem bbN_pred_in_bbN: forall (n: Ensemble BIG_type) (H: In bbN1 n),
  In bbN (app bbN_pred n (bbN_pred_y n H)).
Proof.
intros n H.
assert (In bbN_pred (n, (app bbN_pred n (bbN_pred_y n H)))).
apply app_trivial_property.
inversion H0.
assumption.
Qed.

Theorem valid_bbN_pred: forall (n: Ensemble BIG_type) (H: In bbN1 n), app bbN_pred n (bbN_pred_y n H) = bbN_of_nat (pred (nat_of_bbN n)).
Proof.
intros n H.
assert ((nat_of_bbN n) > 0).
unfold gt.
induction H.
generalize H0; pattern n.
apply BBook_principle_of_mathematical_induction.
intros.
assert False; [ apply H1; intuition | contradiction ].
intros.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity.
apply lt_O_Sn.
assumption.
assert (
(nat_of_bbN (app bbN_pred n (bbN_pred_y n H))) =
(nat_of_bbN (bbN_of_nat (pred (nat_of_bbN n))))
).
rewrite nat_2coercions_identity.
symmetry.
rewrite (valid_pred_nat (nat_of_bbN n) H0).
assert ( ((app bbN_pred (bbN_of_nat (nat_of_bbN n))
     (bbN_pred_y (bbN_of_nat (nat_of_bbN n))
        (bbN_of_pos_nat_in_bbN1 (nat_of_bbN n) H0))))
= (app bbN_pred n (bbN_pred_y n H))).
symmetry; apply identifying_app.
rewrite bbN_of_nat_of_bbN.
apply app_trivial_property.
induction H; assumption.
rewrite H1; reflexivity.
rewrite nat_2coercions_identity in H1.
rewrite <- H1.
rewrite bbN_of_nat_of_bbN.
reflexivity.
apply bbN_pred_in_bbN.
Qed.

Theorem valid_nat_eq_l: forall (n m: nat),
  n = m -> (bbN_of_nat n) = (bbN_of_nat m).
Proof.
intros n m H.
rewrite H; reflexivity.
Qed.

Theorem valid_nat_eq_r: forall (n m: nat),
  (bbN_of_nat n) = (bbN_of_nat m) -> n = m.
Proof.
intros n m H.
rewrite <- (nat_2coercions_identity n).
rewrite <- (nat_2coercions_identity m).
rewrite H; reflexivity.
Qed.

Theorem valid_nat_eq: forall (n m: nat),
  n = m <-> (bbN_of_nat n) = (bbN_of_nat m).
Proof.
intros; split.
apply valid_nat_eq_l.
apply valid_nat_eq_r.
Qed.

Theorem valid_bbN_eq_l: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
 (n = m) -> (nat_of_bbN n) = (nat_of_bbN m).
Proof.
intros n m H H0 H1.
rewrite H1; reflexivity.
Qed.

Theorem valid_bbN_eq_r: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
 (nat_of_bbN n) = (nat_of_bbN m) -> (n = m).
Proof.
intros n m H H0 H1.
rewrite <- (bbN_of_nat_of_bbN n H).
rewrite <- (bbN_of_nat_of_bbN m H0).
rewrite H1; reflexivity.
Qed.

Theorem valid_bbN_eq: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
  ((n = m) <-> (nat_of_bbN n) = (nat_of_bbN m)).
Proof.
intros; split.
apply valid_bbN_eq_l; assumption.
apply valid_bbN_eq_r; assumption.
Qed.

Theorem valid_nat_le_l: forall (n m: nat),
  n <= m -> (bbN_le (bbN_of_nat n) (bbN_of_nat m)).
Proof.
intros n m H.
induction H.
unfold bbN_le; intuition.
unfold bbN_le in IHle |- *.
apply inclusion_transitivity with (bbN_of_nat m).
assumption.
simpl; red; intros.
left; assumption.
Qed.

Theorem valid_nat_le_r: forall (n m: nat),
  (bbN_le (bbN_of_nat n) (bbN_of_nat m)) -> n <= m.
Proof.
unfold bbN_le.
intros n m H.
apply incl_card_le with BIG_type (bbN_of_nat n) (bbN_of_nat m).
apply nat_cardinal_matches_coercion.
apply nat_cardinal_matches_coercion.
assumption.
Qed.

Theorem valid_nat_le: forall (n m: nat),
  n <= m <-> (bbN_le (bbN_of_nat n) (bbN_of_nat m)).
Proof.
intros; split.
apply valid_nat_le_l.
apply valid_nat_le_r.
Qed.

Theorem valid_bbN_le_l: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
  (bbN_le n m) -> (nat_of_bbN n) <= (nat_of_bbN m).
Proof.
intros n m H H0 H1.
unfold bbN_le in H1.
apply incl_card_le with BIG_type n m.
apply cardinal_bbN_nat_of_bbN; assumption.
apply cardinal_bbN_nat_of_bbN; assumption.
assumption.
Qed.

Theorem valid_bbN_le_r: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
  (nat_of_bbN n) <= (nat_of_bbN m) -> (bbN_le n m).
Proof.
intros n m H H0 H1.
rewrite <- (bbN_of_nat_of_bbN n H).
rewrite <- (bbN_of_nat_of_bbN m H0).
apply valid_nat_le_l; assumption.
Qed.

Theorem valid_bbN_le: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
  ((bbN_le n m) <-> (nat_of_bbN n) <= (nat_of_bbN m)).
Proof.
intros; split.
apply valid_bbN_le_l; assumption.
apply valid_bbN_le_r; assumption.
Qed.

Theorem valid_nat_lt_l: forall (n m: nat),
  n < m -> (bbN_lt (bbN_of_nat n) (bbN_of_nat m)).
Proof.
intros n m H.
induction H.
unfold bbN_lt.
split.
intros H.
generalize (valid_nat_eq_r _ _ H).
apply n_Sn.
red; intros; simpl; left; assumption.
split.
intros H0.
rewrite H0 in IHle.
induction IHle.
generalize (valid_nat_le_r _ _ H2).
apply le_Sn_n.
induction IHle.
apply inclusion_transitivity with (bbN_of_nat m).
assumption.
red; intros; simpl; left; assumption.
Qed.

Theorem valid_nat_lt_r: forall (n m: nat),
  (bbN_lt (bbN_of_nat n) (bbN_of_nat m)) -> n < m.
Proof.
intros n m H.
induction H.
assert (n <> m).
intros H1; apply H; apply valid_nat_eq_l; assumption.
assert (n <= m).
rewrite <- (nat_2coercions_identity n).
rewrite <- (nat_2coercions_identity m).
apply (valid_bbN_le_l _ _ (bbN_of_nat_in_bbN n) (bbN_of_nat_in_bbN m)).
assumption.
generalize (nat_total_order _ _ H1); intros.
induction H3.
assumption.
generalize (gt_not_le _ _ H3); intros.
contradiction.
Qed.

Theorem valid_nat_lt: forall (n m: nat),
  n < m <-> (bbN_lt (bbN_of_nat n) (bbN_of_nat m)).
Proof.
intros; split.
apply valid_nat_lt_l.
apply valid_nat_lt_r.
Qed.

Theorem valid_bbN_lt_l: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
  (bbN_lt n m) -> (nat_of_bbN n) < (nat_of_bbN m).
Proof.
intros n m H H0 H1.
rewrite <- (bbN_of_nat_of_bbN n H) in H1.
rewrite <- (bbN_of_nat_of_bbN m H0) in H1.
apply valid_nat_lt_r; assumption.
Qed.

Theorem valid_bbN_lt_r: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
  (nat_of_bbN n) < (nat_of_bbN m) -> (bbN_lt n m) .
Proof.
intros n m H H0 H1.
rewrite <- (bbN_of_nat_of_bbN n H).
rewrite <- (bbN_of_nat_of_bbN m H0).
apply valid_nat_lt_l; assumption.
Qed.

Theorem valid_bbN_lt: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
  ((bbN_lt n m) <-> (nat_of_bbN n) < (nat_of_bbN m)).
Proof.
intros; split.
apply valid_bbN_lt_l; assumption.
apply valid_bbN_lt_r; assumption.
Qed.

Theorem valid_nat_ge_l: forall (n m: nat),
  n >= m -> (bbN_ge (bbN_of_nat n) (bbN_of_nat m)).
Proof.
intros.
unfold bbN_ge.
assert (m <= n); [ intuition | ].
apply valid_nat_le_l; assumption.
Qed.

Theorem valid_nat_ge_r: forall (n m: nat),
  (bbN_ge (bbN_of_nat n) (bbN_of_nat m)) -> n >= m.
Proof.
intros.
unfold bbN_ge in H.
assert (m <= n).
apply valid_nat_le_r; assumption.
intuition.
Qed.

Theorem valid_nat_ge: forall (n m: nat),
  n >= m <-> (bbN_ge (bbN_of_nat n) (bbN_of_nat m)).
Proof.
intros; split.
apply valid_nat_ge_l.
apply valid_nat_ge_r.
Qed.

Theorem valid_bbN_ge_l: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
  ((bbN_ge n m) -> (nat_of_bbN n) >= (nat_of_bbN m)).
Proof.
unfold bbN_ge; intros.
assert (nat_of_bbN m <= nat_of_bbN n).
apply valid_bbN_le_l; assumption.
intuition.
Qed.

Theorem valid_bbN_ge_r: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
  (nat_of_bbN n) >= (nat_of_bbN m) -> (bbN_ge n m).
Proof.
unfold bbN_ge; intros.
assert (nat_of_bbN m <= nat_of_bbN n); [ intuition | ].
apply valid_bbN_le_r; assumption.
Qed.

Theorem valid_bbN_ge: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
  ((bbN_ge n m) <-> (nat_of_bbN n) >= (nat_of_bbN m)).
Proof.
intros; split.
apply valid_bbN_ge_l; assumption.
apply valid_bbN_ge_r; assumption.
Qed.

Theorem valid_nat_gt_l: forall (n m: nat),
  n > m -> (bbN_gt (bbN_of_nat n) (bbN_of_nat m)).
Proof.
intros.
unfold bbN_gt.
assert (m < n); [ intuition | ].
apply valid_nat_lt_l; assumption.
Qed.

Theorem valid_nat_gt_r: forall (n m: nat),
  (bbN_gt (bbN_of_nat n) (bbN_of_nat m)) -> n > m.
Proof.
intros.
unfold bbN_gt in H.
assert (m < n).
apply valid_nat_lt_r; assumption.
intuition.
Qed.

Theorem valid_nat_gt: forall (n m: nat),
  n > m <-> (bbN_gt (bbN_of_nat n) (bbN_of_nat m)).
Proof.
intros; split.
apply valid_nat_gt_l.
apply valid_nat_gt_r.
Qed.

Theorem valid_bbN_gt_l: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
  (bbN_gt n m) -> (nat_of_bbN n) > (nat_of_bbN m).
Proof.
unfold bbN_gt; intros.
assert (nat_of_bbN m < nat_of_bbN n).
apply valid_bbN_lt_l; assumption.
intuition.
Qed.

Theorem valid_bbN_gt_r: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
   (nat_of_bbN n) > (nat_of_bbN m) -> (bbN_gt n m).
Proof.
unfold bbN_gt; intros.
assert (nat_of_bbN m < nat_of_bbN n); [ intuition | ].
apply valid_bbN_lt_r; assumption.
Qed.

Theorem valid_bbN_gt: forall (n m: Ensemble BIG_type), In bbN n -> In bbN m ->
  ((bbN_gt n m) <-> (nat_of_bbN n) > (nat_of_bbN m)).
Proof.
intros; split.
apply valid_bbN_gt_l; assumption.
apply valid_bbN_gt_r; assumption.
Qed.

