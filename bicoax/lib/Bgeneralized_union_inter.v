(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bchapter2.
Require Import Classical_sets.

(**
  * Generalized intersection and union: definitions
  *)

(**
  Generalized Intersection, BBook: section 3.1, p.124, array 1, row 1
  *)
Inductive GenIntersection (U:Type) (u: Ensemble (Ensemble U)): Ensemble U :=
  GenIntersection_intro: forall (x:U), (forall (y:Ensemble U), In u y -> In y x) -> GenIntersection U u x.
Implicit Arguments GenIntersection [U].
Notation "'inter' u" := (GenIntersection u) (at level 6, no associativity): eB_scope.

(**
  Generalized Union, BBook: section 3.1, p.124, array 1, row 2
  *)
Inductive GenUnion (U:Type) (u: Ensemble (Ensemble U)): Ensemble U :=
  GenUnion_intro: forall (x: U), (exists y, In u y /\ In y x) -> GenUnion U u x.
Implicit Arguments GenUnion [U].
Notation "'union' u" := (GenUnion u) (at level 6, no associativity): eB_scope.

(**
  Quantified Intersection, BBook: section 3.1, p.124, array 2, row 1, line 1

  also:

  Quantified Intersection with restricting property, BBook: section 3.1, p.124, array 2, row 1, line 2

  (the reason is the same as for the lambda abstraction)
  *)
Inductive QuantifiedIntersection (S T: Type) (s: Ensemble S) (f: S -> (Ensemble T)): Ensemble T :=
  QuantifiedIntersection_intro: forall (y: T),  (forall (x:S), In s x -> In (f x) y) -> QuantifiedIntersection S T s f y.
Implicit Arguments QuantifiedIntersection [S T].
Notation "⋂ x · ( K | E )" := (QuantifiedIntersection (fun x => K) (fun x => E)) (at level 200, no associativity): eB_scope.

(**
  Quantified Union, BBook: section 3.1, p.124, array 2, row 2, line 1

  also:

  Quantified Union with restricting property, BBook: section 3.1, p.124, array 2, row 2, line 2

  (the reason is the same as for the lambda abstraction)
  *)
Inductive QuantifiedUnion (S T: Type) (s: Ensemble S) (f: S -> (Ensemble T)): Ensemble T :=
  QuantifiedUnion_intro: forall (y: T),  (exists x, In s x /\ In (f x) y) -> QuantifiedUnion S T s f y.
Implicit Arguments QuantifiedUnion [S T].
Notation "⋃ x · ( K | E )" := (QuantifiedIntersection (fun x => K) (fun x => E)) (at level 200, no associativity): eB_scope.

(**
  * Generalized intersection and union: validation
  *)

(** TODO: move this theorem somewhere in chapter 2 *)
Theorem not_in_emptyset_singleton_inhabited: forall (U: Type) (X: Ensemble U), ~ (In (Singleton (Empty_set U)) X) -> Inhabited U X.
Proof.
intros.
cut (X <> (Empty_set _)).
intros.
generalize (not_empty_Inhabited _ X H0); intros H1.
assumption.
intros H0; apply H.
intuition.
Qed.

(**
  Generalized Intersection, BBook: section 3.1, p.124, array 1, row 1
  *)
Theorem valid_GenIntersection: forall (U: Type) (s: Ensemble U) (u: Ensemble (Ensemble U)),
In (Power_set1 (Power_set s)) u ->
GenIntersection u = (Comprehension (fun x => In s x /\ (forall (y:Ensemble U), In u y -> In y x))).
Proof.
intros U s u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0.
constructor; [ | intuition ].
repeat (induction H).
generalize (not_in_emptyset_singleton_inhabited (Ensemble U) X H1).
intros.
induction H2.
cut (Included x0 s); intros y.
intuition.
intros.
generalize (H x0 H2); intros.
induction H4.
apply H4; intuition.
induction H0.
constructor.
intuition.
Qed.

(**
  Generalized Union, BBook: section 3.1, p.124, array 1, row 2
  *)
Theorem valid_GenUnion: forall (U: Type) (s: Ensemble U) (u: Ensemble (Ensemble U)),
In (Power_set (Power_set s)) u ->
GenUnion u = (Comprehension (fun x => In s x /\ (exists y:(Ensemble U), In u y /\ In y x))).
Proof.
intros U s u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0.
induction H0.
repeat (induction H).
constructor.
generalize (H x0 (proj1 H0)); intros.
repeat (induction H1).
apply H1; intuition.
exists x0; intuition.
induction H0.
constructor.
intuition.
Qed.

(**
  Quantified Intersection, BBook: section 3.1, p.124, array 2, row 1, line 1

  TODO: ensure that these hypotheses match the BBook (f corresponds to E, but...)
  TODO: Quantified Intersection with restricting property
  *)
Theorem valid_QuantifiedIntersection: forall (S T: Type) (s: Ensemble S) (f: S -> (Ensemble T)) (t: Ensemble T),
(forall (x: S), In s x -> Included (f x) t) -> (exists x, In s x) ->
QuantifiedIntersection s f = (Comprehension (fun y => In t y /\ (forall (x:S), In s x -> In (f x) y))).
Proof.
intros S T s f t H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
constructor; [ | intuition ].
induction H0.
generalize (H x H0); intros H2.
generalize (H1 x H0); intros H3.
generalize (H2 y H3); intuition.
induction H1.
constructor.
intuition.
Qed.

(**
  Quantified Union, BBook: section 3.1, p.124, array 2, row 2, line 1

  TODO: ensure that these hypotheses match the BBook (f corresponds to E, but...)
  TODO: Quantified Union with restricting property
  *)
Theorem valid_QuantifiedUnion: forall (S T: Type) (s: Ensemble S) (f: S -> (Ensemble T)) (t: Ensemble T),
(forall (x: S), In s x -> Included (f x) t) ->
QuantifiedUnion s f = (Comprehension (fun y => In t y /\ (exists x:S, In s x /\ In (f x) y))).
Proof.
intros S T s f t H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0.
constructor.
induction H0.
generalize (H x (proj1 H0)); intuition.
intuition.
induction H0.
constructor.
intuition.
Qed.

(**
  * Generalized intersection and union: theorems
  *)


(**
  Generalized Intersection as a Quantified Intersection, BBook: section 3.1, p.126, array 1, row 1
*)
Theorem  GenIntersection_QuantifiedIntersection: forall (U: Type) (u: Ensemble (Ensemble U)), GenIntersection u = QuantifiedIntersection u (fun y => y).
Proof.
intros U u.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
constructor.
intros x0 H0.
induction H.
apply (H x0 H0).
induction H.
constructor.
intros y0 H0.
apply H; intuition.
Qed.

(**
  Generalized Intersection as a Quantified Union, BBook: section 3.1, p.126, array 1, row 2
*)
Theorem GenUnion_QuantifiedUnion: forall (U: Type) (u: Ensemble (Ensemble U)), GenUnion u = QuantifiedUnion u (fun y => y).
Proof.
intros U u.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
constructor.
induction H.
induction H.
exists x0; intuition.
induction H.
induction H.
constructor.
exists x; intuition.
Qed.

(**
  Quantified Intersection as a Generalized Intersection, BBook: section 3.1, p.126, array 1, row 3
*)
Theorem QuantifiedIntersection_GenIntersection: forall (S T: Type) (s:Ensemble S) (f: S -> Ensemble T), QuantifiedIntersection s f = GenIntersection (range (lambda  s f)).
Proof.
intros S T s f.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
constructor.
intros x0 H0.
induction H.
induction H0.
elim H0; intros x0 H1.
inversion H1.
rewrite H5; apply H; intuition.
induction H.
constructor.
intros x0 H0.
apply H.
constructor.
exists x0.
constructor; intuition.
Qed.

(**
  Quantified Union as a Generalized Union, BBook: section 3.1, p.126, array 1, row 4
*)
Theorem QuantifiedUnion_GenUnion: forall (S T: Type) (s: Ensemble S) (f: S -> Ensemble T), QuantifiedUnion s f = GenUnion (range (lambda  s f)).
Proof.
intros S T s f.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
constructor.
induction H.
induction H.
exists (f x).
split; [ constructor; exists x; constructor; intuition | intuition ].
induction H.
induction H.
elim H; intros H0 H1.
induction H0.
induction H0.
constructor.
exists x0.
inversion H0.
split; [ intuition | rewrite <- H5; intuition].
Qed.

(**
  associativity of Quantified Intersection and Quantified Union, BBook: section 3.1, p.126, array 2, row 3
*)
Theorem QuantifiedIntersection_QuantifiedUnion_assoc: forall (S T U: Type) (s: Ensemble S) (E: S -> Ensemble T) (F: T -> Ensemble U),
 (QuantifiedIntersection (QuantifiedUnion s E) F) =
 (QuantifiedIntersection s (fun x => (QuantifiedIntersection (E x) F))).
Proof.
intros S T U s E F.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
constructor; intros x H0.
constructor.
intros x0 H1.
apply H.
constructor.
exists x; intuition.
induction H.
constructor.
intros x H0.
induction H0.
induction H0.
generalize (H x (proj1 H0)).
intros H1.
induction H1.
apply H1; intuition.
Qed.

(**
  associativity of Generalized Intersection and Quantified Union, BBook: section 3.1, p.126, array 2, row 1
*)
Theorem GenIntersection_QuantifiedUnion_assoc: forall (S T: Type) (s: Ensemble S) (E: S -> Ensemble (Ensemble T)),
  (GenIntersection (QuantifiedUnion s E)) = QuantifiedIntersection s (fun x => GenIntersection (E x)).
Proof.
intros S T s E.
rewrite GenIntersection_QuantifiedIntersection.
rewrite QuantifiedIntersection_QuantifiedUnion_assoc.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0.
constructor.
intros.
rewrite GenIntersection_QuantifiedIntersection.
apply H; intuition.
induction H0.
constructor.
intros.
rewrite <- GenIntersection_QuantifiedIntersection.
apply H; intuition.
Qed.

(**
  associativity of Quantified Union and Quantified Union, BBook: section 3.1, p.126, array 2, row 3
*)
Theorem QuantifiedUnion_QuantifiedUnion_assoc: forall (S T U: Type) (s: Ensemble S) (E: S -> Ensemble T) (F: T -> Ensemble U),
 (QuantifiedUnion (QuantifiedUnion s E) F) =
 (QuantifiedUnion s (fun x => (QuantifiedUnion (E x) F))).
Proof.
intros S T U s E F.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H.
elim H; intros H0 H1.
induction H0.
induction H0.
constructor.
exists x.
split; [ intuition | constructor; exists y0; intuition].
induction H.
induction H.
elim H; intros H0 H1; induction H1.
induction H1.
constructor.
exists x0.
split; [ constructor; exists x; intuition | intuition ].
Qed.

(**
  associativity of Generalized Union and Quantified Union, BBook: section 3.1, p.126, array 2, row 2
*)
Theorem GenUnion_QuantifiedUnion_assoc: forall (S T: Type) (s: Ensemble S) (E: S -> Ensemble (Ensemble T)),
  (GenUnion (QuantifiedUnion s E)) = QuantifiedUnion s (fun x => GenUnion (E x)).
Proof.
intros S T s E.
rewrite GenUnion_QuantifiedUnion.
rewrite QuantifiedUnion_QuantifiedUnion_assoc.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0.
constructor.
induction H.
exists x.
split; [ intuition | ].
rewrite GenUnion_QuantifiedUnion.
apply (proj2 H); intuition.
induction H0.
constructor.
induction H.
exists x.
split; [ intuition | ].
rewrite <- GenUnion_QuantifiedUnion.
intuition.
Qed.

(**
  distributivity of Quantified Intersection over union, BBook: section 3.1, p.127, array 1, row 1

  Note: this proof is oddly long and uses a lot of demorgan laws. It
  would be interesting to look into this It looks like I forgot a
  non-emptiness hypothesis accidentally, but I could prove this
  theorem. This actually makes this theorem much more interesting.
  *)
Theorem QuantifiedIntersection_Union_distr: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (E: S -> Ensemble U) (F: T -> Ensemble U),
  Union (QuantifiedIntersection s E) (QuantifiedIntersection t F) =
  QuantifiedIntersection (times s t) (fun c => match c with | (x,y) => Union (E x) (F y) end).
Proof.
intros S T U s t E F.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H; induction H; constructor; intros.
induction H0.
left; apply H; intuition.
induction H0.
right; apply H; intuition.
induction H.
cut (forall x, In (times s t) x -> In (let (x0, _):=x in E x0) y \/ In (let (_,x1):=x in F x1) y).
intros.
cut ( (forall x, In (times s t) x -> In (let (x0, _):=x in E x0) y)
     \/ (forall x, In (times s t) x -> In (let (_,x1):=x in F x1) y)).
intros.
cut ( (forall x0, In s x0 -> In (E x0) y)
     \/ (forall x1, In t x1 -> In (F x1) y)).
intros.
induction H2.
left; compute; constructor; exact H2.
right; compute; constructor; exact H2.
(* TODO: remove this comment
SC: this corresponds to an experimentation trying to base the proof upon
non-emptiness of the not-mentioned part (t when proving for E, for instance)
but it seems to require the inhabitation of the base datatype

induction H1.
left; intros.
assert (t = Empty_set _ \/ t <> Empty_set _); [ apply classic | ].
induction H3.
assert ((times s t) = Empty_set _).
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H4.
induction H4.
assert (In (Empty_set _) b).
rewrite <- H3; assumption.
contradiction.
contradiction.
assert (forall (P:Prop) x, In (times s t) x -> P).
intros.
assert (In (Empty_set _) x); [ rewrite <- H4; assumption | ].
contradiction.



induction H4.

inversion H3.

induction H3.



apply H1.

*)
apply NNPP.
intros H2.
cut (~~
( (forall x, In (times s t) x -> In (let (x0, _):=x in E x0) y)
  \/ (forall x, In (times s t) x -> In (let (_,x1):=x in F x1) y))).
intros H3.
apply H3.
apply and_not_or.
generalize (not_or_and _ _ H2); intros.
induction H4.
generalize (not_all_ex_not _ _ H4).
generalize (not_all_ex_not _ _ H5).
intros.
induction H6; induction H7.
generalize ((proj1 (de_Morgan_not_implies_and _ _)) H6).
generalize ((proj1 (de_Morgan_not_implies_and _ _)) H7).
intros.
assert (In (times s t) (x0,x)); [ constructor; intuition | ].
split.
intros H11; generalize (H11 (x0,x) H10); intuition.
intros H11; generalize (H11 (x0,x) H10); intuition.
intros H3; apply H3; intuition.
apply NNPP.
intros H1.
generalize (not_or_and  _ _ H1).
intros.
induction H2.
generalize (not_all_ex_not _ _ H2); intros.
generalize (not_all_ex_not _ _ H3); intros.
induction H4.
generalize ((proj1 (de_Morgan_not_implies_and _ _)) H4).
induction H5.
generalize ((proj1 (de_Morgan_not_implies_and _ _)) H5).
intros.
induction H6; induction H7.
induction H6; induction H7.
assert (In (times s t) (a0,b)); [ constructor; assumption | ].
generalize (H0 (a0,b) H12).
intuition.
intros.
generalize (H x H0); intros.
induction H0; induction H1; intuition.
Qed.

(**
  distributivity of Quantified Union over intersection, BBook: section 3.1, p.127, array 1, row 2
  *)
Theorem QuantifiedUnion_Intersection_distr: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (E: S -> Ensemble U) (F: T -> Ensemble U),
  Intersection (QuantifiedUnion s E) (QuantifiedUnion t F) =
  QuantifiedUnion (times s t) (fun c => match c with | (x,y) => Intersection (E x) (F y) end).
Proof.
intros S T U s t E F.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H; induction H.
induction H0; induction H0.
constructor.
exists (x, x0).
split; [ constructor; intuition | constructor; intuition ].
induction H.
induction H.
induction H.
induction H.
induction H0.
split.
constructor; exists a; intuition.
constructor; exists b; intuition.
Qed.

(**
  distributivity of Quantified Intersection over cartesian product, BBook: section 3.1, p.127, array 1, row 3

  Note: oddly enough, this theorem seem to require a non-emptiness
  hypothesis, while the other distributivity theorem about quantified
  intersection does not.
  *)
Theorem QuantifiedIntersection_times_distr: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (E: S -> Ensemble U) (F: T -> Ensemble V) (times_non_empty: (times s t) <> (Empty_set (S*T))),
  times (QuantifiedIntersection s E) (QuantifiedIntersection t F) =
  QuantifiedIntersection (times s t) (fun c => match c with | (x,y) => times (E x) (F y) end).
Proof.
intros S T U V s t E F times_not_empty.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.

induction H.
induction H.
induction H0.
constructor.
intros.
induction H1.
constructor; [ apply H; intuition | apply H0; intuition ].

induction H.
induction y.
generalize (not_empty_Inhabited _ _ times_not_empty); intros.
induction H0.
induction x.
induction H0.
constructor.
constructor; intros.
assert (In (times s t) (x,b1)); [ constructor; assumption | ].
generalize (H (x,b1) H3); intros.
inversion H4; intuition.
constructor; intros.
assert (In (times s t) (a1,x)); [ constructor; assumption | ].
generalize (H (a1,x) H3); intros.
inversion H4; intuition.
Qed.

(**
  distributivity of Quantified Union over cartesian product, BBook: section 3.1, p.127, array 1, row 4
  *)
Theorem QuantifiedUnion_times_distr: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (E: S -> Ensemble U) (F: T -> Ensemble U),
  times (QuantifiedUnion s E) (QuantifiedUnion t F) =
  QuantifiedUnion (times s t) (fun c => match c with | (x,y) => times (E x) (F y) end).
Proof.
intros S T U s t E F.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H; induction H.
induction H0; induction H0.
constructor.
exists (x, x0).
split; [ constructor; intuition | constructor; intuition ].
induction H.
induction H.
induction H.
induction H.
induction H0.
split.
constructor; exists a; intuition.
constructor; exists b; intuition.
Qed.

(**
  * Generalized intersection and union: various properties
  *)

(**
  Generalized Intersection of a singleton, BBook: section 3.1, p.127, array 2, row 1
 *)
Theorem GenIntersection_Singleton: forall (S: Type) (a: Ensemble S), GenIntersection (Singleton a) = a.
Proof.
intros S a.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
apply H; intuition.
constructor.
intros y H0.
induction H0.
intuition.
Qed.

(**
  Generalized Union of a singleton, BBook: section 3.1, p.127, array 2, row 2
 *)
Theorem GenUnion_Singleton: forall (S: Type) (a: Ensemble S), GenUnion (Singleton a) = a.
Proof.
intros S a.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
repeat (induction H).
intuition.
constructor.
exists a.
intuition.
Qed.

(**
  Generalized Intersection of a doubleton, BBook: section 3.1, p.127, array 2, row 3
 *)
Theorem GenIntersection_Doubleton: forall (S: Type) (a b: Ensemble S), GenIntersection (Union (Singleton a) (Singleton b)) = Intersection a b.
Proof.
intros S a b.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
repeat (induction H).
intuition.
induction H.
constructor.
intros y H1; repeat (induction H1); intuition.
Qed.

(**
  Generalized Union of a doubleton, BBook: section 3.1, p.127, array 2, row 4
 *)
Theorem GenUnion_Doubleton: forall (S: Type) (a b: Ensemble S), GenUnion (Union (Singleton a) (Singleton b)) = Union a b.
Proof.
intros S a b.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
repeat (induction H).
left; intuition.
right; intuition.
constructor.
induction H.
exists a; intuition.
exists b; intuition.
Qed.

(**
  Generalized Intersection of a union, BBook: section 3.1, p.127, array 2, row 5
 *)
Theorem GenIntersection_Union: forall (S: Type) (s: Ensemble S) (a b: Ensemble (Ensemble S)), In (Power_set1 (Power_set s)) a -> In (Power_set1 (Power_set s)) b -> GenIntersection (Union a b) = Intersection (GenIntersection a) (GenIntersection b).
Proof.
intros S s a b H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
repeat (induction H).
repeat (induction H0).
generalize (not_in_emptyset_singleton_inhabited (Ensemble S) X0 H3).
intros H4; induction H4.
generalize (H0 x0 H4); intros H5.
induction H5.
split.
constructor; intros; apply H1; left; intuition.
constructor; intros; apply H1; right; intuition.
induction H1.
constructor; intros.
induction H1; induction H2.
elim H3.
intros; apply H1; intuition.
intros; apply H2; intuition.
Qed.


(**
  Generalized Union of a union, BBook: section 3.1, p.127, array 2, row 6
 *)
Theorem GenUnion_Union: forall (S: Type) (a b: Ensemble (Ensemble S)), GenUnion (Union a b) = Union (GenUnion a) (GenUnion b).
Proof.
intros S a b.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
repeat (induction H).
left; constructor; exists x0; intuition.
right; constructor; exists x0; intuition.
repeat (induction H).
constructor; exists x0; split ; [ left; intuition | intuition ].
constructor; exists x0; split ; [ right; intuition | intuition ].
Qed.


(**
  Generalized Intersection of a union as a doubleton, BBook: section 3.1, p.127, array 2, row 7
 *)
Theorem GenIntersection_Union2: forall (S: Type) (s: Ensemble S) (a b: Ensemble (Ensemble S)), In (Power_set1 (Power_set s)) a -> In (Power_set1 (Power_set s)) b -> GenIntersection (Union a b) = GenIntersection (Union (Singleton (GenIntersection a)) (Singleton (GenIntersection b))).
Proof.
intros S s a b H H0.
rewrite GenIntersection_Doubleton.
rewrite (GenIntersection_Union S s a b); intuition.
Qed.

(**
  Generalized Union of a union as a doubleton, BBook: section 3.1, p.127, array 2, row 8
 *)
Theorem GenUnion_Union2: forall (S: Type) (a b: Ensemble (Ensemble S)), GenUnion (Union a b) = GenUnion (Union (Singleton (GenUnion a)) (Singleton (GenUnion b))).
Proof.
intros S a b.
rewrite GenUnion_Doubleton.
rewrite GenUnion_Union.
reflexivity.
Qed.

(**
  * Generalized intersection and union: bound properties
  *)

(**
  Generalized Intersection is a lower bound (theorem 3.1.1), BBook: section 3.1, p.127

  [u] can contain the empty set only apparently, probably an artifact of our definition
  *)
Theorem GenIntersection_lower_bound: forall (S: Type) (t: Ensemble S) (u: Ensemble (Ensemble S)), In u t -> Included (GenIntersection u) t.
Proof.
intros S t u H0.
intros x H.
induction H; apply H; intuition.
Qed.

(**
  Generalized Intersection is the greatest lower bound (theorem 3.1.2), BBook: section 3.1, p.128

  finally this theorem stays true even if the set of sets contains only the empty set
  *)
Theorem GenIntersection_greatest_lower_bound: forall (S: Type) (u: Ensemble (Ensemble S)) (s v: Ensemble S), Included v s -> (forall (t: Ensemble S), In u t -> Included v t) -> Included v (GenIntersection u).
Proof.
intros S u s v H H0.
intros x H1.
constructor.
intros y H2.
apply (H0 y H2).
intuition.
Qed.

(**
  Generalized Union is an upper bound (theorem 3.1.3), BBook: section 3.1, p.129
  *)
Theorem GenUnion_upper_bound: forall (S: Type) (u: Ensemble (Ensemble S)) (t: Ensemble S), In u t -> Included t (GenUnion u).
Proof.
intros S u t H.
intros x H1.
constructor; exists t; intuition.
Qed.

(**
  Generalized Intersection is the least upper bound (theorem 3.1.4), BBook: section 3.1, p.130
  *)
Theorem GenUnion_least_upper_bound: forall (S: Type) (u: Ensemble (Ensemble S)) (v s: Ensemble S), Included v s -> (forall (t: Ensemble S), In u t -> Included t v) -> Included (GenUnion u) v.
Proof.
intros S u v s H H0.
intros x H1.
induction H1.
induction H1.
apply H0 with x0; intuition.
Qed.
