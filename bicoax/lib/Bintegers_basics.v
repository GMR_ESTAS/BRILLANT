(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter2.
Require Import Bnaturals.
Require Import ZArith.
(** Note: the axiom of indefinite description is used for decidability on bbZ *)
Require Import ClassicalEpsilon.

Open Scope eB_scope.
Open Scope Z_scope.

(**
  * Integers
  *)

(**
  The atelierB manual introduces concrete naturals and integers. Moreover, the BBook
  considers naturals as a subset of integers, while all numerical sets in the atelierB manual
  are considered subsets of integers. In Coq, natural numbers and integers are different
  constructed types. Hence we need to provide two things: all previous definitions defined
  upon bbN or Bnat redefined upon bbZ or BZ, and provided morphisms between
  these types.
  *)

(**
  ** Integers: definitions
  *)

(**
  *** Integers: definitions of base sets
  *)

Definition MAXINT := 2147483647.
Definition MININT := -2147483648.

Definition BN: Ensemble Z := Comprehension (fun x => x >= 0).
Transparent BN.
Notation "'ℕ'" := BN (at level 6): eB_scope.

Definition BN1: Ensemble Z := Comprehension (fun x => x > 0).
Transparent BN1.
Notation "'ℕ1'" := BN1 (at level 6): eB_scope. (* or ℕ₁ *)

Definition interval (x y:Z) := Comprehension (fun z => x <= z /\ z <= y).
Transparent interval.
Notation "x ‥ y" := (interval x y) (at level 41, no associativity): eB_scope.

Definition BConcreteNaturals := interval 0 MAXINT.
Transparent BConcreteNaturals.
Notation "'NAT'" := BConcreteNaturals (at level 6): eB_scope.

Definition BConcreteNaturals1 := interval 1 MAXINT.
Transparent BConcreteNaturals1.
Notation "'NAT1'" := BConcreteNaturals1 (at level 6): eB_scope.

Definition BZ:=Full_set Z.
Notation "'ℤ'" := BZ (at level 6): eB_scope.

Definition BConcreteIntegers: Ensemble Z:=interval MININT MAXINT.
Notation "'INT'" := BConcreteIntegers (at level 6): eB_scope.

Theorem inhabited_Z: inhabited Z.
Proof.
exact (inhabits Z0).
Qed.

(**
  *** Integers: uminus definition
  *)

Variable bbZ: Ensemble (Ensemble BIG_type).
(**
  BBook: section 3.6, p.171
  *)
Axiom bbN_included_in_bbZ: Included bbN bbZ.
Definition bbZ1 := Setminus bbZ bbN.

(**
  Small exception to the naming guideline expressed in Bnaturals: "uminus" should not cause
  any conflict, and it is used so much that it does not hurt to have the shorter name here
  *)
Variable uminus: Ensemble ((Ensemble BIG_type) * (Ensemble BIG_type)).
(**
  BBook: section 3.6, p.171
  *)
Axiom uminus_total_bijection: In (total_bijection bbZ bbZ) uminus.
(** Note: I think this axiom above could be deduced from the 3 next axioms +
  one stating that the only possible image of zero by uminus is zero. Well, just
  thinking out loud.
  *)

(**
  three axioms, BBook: section 3.6, p.171
  *)
Axiom uminus_zero: In uminus (bbN_0, bbN_0).
Axiom uminus_total_bijection_negative: In (total_bijection bbZ1 bbN1) (domain_restriction uminus bbZ1).
Axiom uminus_positive_restricted_equality: (domain_restriction uminus bbN1) = (Brelations.inverse (domain_restriction uminus bbZ1)).

Theorem uminus_total_bijection_positive: In (total_bijection bbN1 bbZ1) (domain_restriction uminus bbN1).
Proof.
rewrite uminus_positive_restricted_equality.
apply Membership_laws_04.
exact uminus_total_bijection_negative.
Qed.

Theorem bbZ_decomposition: bbZ = Union bbZ1 (Union bbN1 (Singleton bbN_0)).
Proof.
replace (Union bbN1 (Singleton bbN_0)) with bbN.
unfold bbZ1.
rewrite commutativity_1.
symmetry; apply excluded_middle_1.
exact bbN_included_in_bbZ.
unfold bbN1.
rewrite commutativity_1; symmetry; apply excluded_middle_1.
red; intros x H.
inversion H; exact bbN_0_in_bbN.
Qed.

Theorem bbZ_decomposition_simple: bbZ = Union bbZ1 bbN.
Proof.
unfold bbZ1.
rewrite commutativity_1.
symmetry; apply excluded_middle_1.
exact bbN_included_in_bbZ.
Qed.

Lemma bbZ1_included_in_bbZ: Included bbZ1 bbZ.
Proof.
red; intros x H; induction H; assumption.
Qed.

Lemma bbN1_included_in_bbZ: Included bbN1 bbZ.
Proof.
red; intros x H; induction H; apply bbN_included_in_bbZ; assumption.
Qed.

Lemma uminus_applicability_zero: In (partial_function bbZ bbZ) uminus /\ In (domain uminus) bbN_0.
Proof.
generalize uminus_total_bijection; intros.
do 2 (induction H).
rewrite H1; split; [ assumption | ].
apply bbN_included_in_bbZ.
apply bbN_0_in_bbN.
Qed.

(**
  reader-friendly version of the axiom about uminus(0), BBook: section 3.6, p.171
  *)
Theorem uminus_zero_is_zero: app uminus bbN_0 uminus_applicability_zero = bbN_0.
Proof.
symmetry; apply identifying_app.
exact uminus_zero.
Qed.

(**
  *** Integers: definitions of extended constructs
  *)

Theorem app_uminus_Z: forall (n: Ensemble BIG_type), In bbZ n ->
  In (partial_function bbZ bbZ) uminus /\ In (domain uminus) n.
Proof.
intros n H.
generalize uminus_total_bijection; intros H0.
inversion H0 as [ f H1 H2 H3 ].
inversion H1 as [ f0 H4 H5 H6 ].
rewrite H5.
split; assumption.
Qed.

Theorem uminus_in_bbZ: forall (n: Ensemble BIG_type) (H: In bbZ n),
  In bbZ (app uminus n (app_uminus_Z n H)).
Proof.
intros n H.
apply application_in_codomain.
Qed.

Theorem bbZ_of_bbN: forall (n: Ensemble BIG_type), In bbN n -> In bbZ n.
Proof.
intros n H; apply bbN_included_in_bbZ; assumption.
Qed.

Theorem bbZ_of_bbN1: forall (n: Ensemble BIG_type), In bbN1 n -> In bbZ n.
Proof.
intros n H; induction H; apply bbN_included_in_bbZ; assumption.
Qed.

Theorem bbZ_of_bbZ1: forall (n: Ensemble BIG_type), In bbZ1 n -> In bbZ n.
Proof.
intros n H; induction H; assumption.
Qed.

Theorem bbN_of_bbN1: forall (n: Ensemble BIG_type), In bbN1 n -> In bbN n.
Proof.
intros n H; induction H; assumption.
Qed.

Definition uminus_unary (n: Ensemble BIG_type) (H: In bbZ n) :=
  app uminus n (app_uminus_Z n H).

(** TODO: move in chapter 2 *)
Theorem Easy_laws_funct_04: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (u: Ensemble S) (x: S),
  Included u s -> In (partial_function s t) f -> In (Intersection u (domain f)) x ->
  forall (applicable_f:(In (partial_function s t) f /\ In (domain f) x)) (applicable_domresf:(In (partial_function s t) (domain_restriction f u) /\ In (domain (domain_restriction f u)) x)),
  app (domain_restriction f u) x applicable_domresf = app f x applicable_f.
Proof.
intros S T s t f u x H H0 H1 applicable_f applicable_domresf.
assert ((Included u s /\ In (partial_function s t) f) /\ In (Intersection u (domain f)) x).
intuition.
generalize (Equality_laws_funct_04 S T s t f u x H2); intros.
induction H3.
induction H3.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ x applicable_f x0).
rewrite (irrelevant_applicability _ _ _ _ _ _ _ x applicable_domresf x1).
assumption.
Qed.

Theorem uminus_in_bbN1: forall (n: Ensemble BIG_type) (H: In bbZ1 n),
  In bbN1 (uminus_unary n (bbZ_of_bbZ1 n H)).
Proof.
intros n H.
unfold uminus_unary.
generalize uminus_total_bijection_negative; intros.
assert (In (partial_function bbZ  bbZ) uminus /\ In (domain uminus) n).
generalize uminus_total_bijection; intros.
do 2 (induction H1).
rewrite H3; split; [ assumption | ].
apply bbZ_of_bbZ1; assumption.
assert (In (partial_function bbZ  bbZ) (domain_restriction uminus bbZ1) /\ In (domain (domain_restriction uminus bbZ1)) n).
inversion H0.
inversion H2.
rewrite H6.
split; [ | assumption ].
apply (Monotonicity_laws_02 (Ensemble BIG_type) (Ensemble BIG_type) bbZ bbZ bbZ1 bbN1).
split; red; intros; [ apply bbZ_of_bbZ1 | apply bbZ_of_bbN1 ]; assumption.
assumption.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ n (app_uminus_Z n (bbZ_of_bbZ1 n H)) H1).
(** Doesn't work ???
  rewrite <- Easy_laws_funct_04 with (Ensemble BIG_type) (Ensemble BIG_type)
  bbZ1 bbN1 uminus bbZ1 n H1 H2.
  *)
assert (app (domain_restriction uminus bbZ1) n H2 = app uminus n H1).
apply Easy_laws_funct_04.
red; intros; apply bbZ_of_bbZ1; assumption.
intuition.
split; [ assumption | ].
generalize uminus_total_bijection; intros.
do 2 (induction H3).
rewrite H5; apply bbZ_of_bbZ1; assumption.
rewrite <- H3.
assert (In (domain_restriction uminus bbZ1) (n, app (domain_restriction uminus bbZ1) n H2)).
apply app_trivial_property.
do 5 (induction H0).
generalize (H0 _ H4); intros H8; inversion H8; assumption.
Qed.

Theorem uminus_in_bbN: forall (n: Ensemble BIG_type) (H: In bbZ1 n),
  In bbN (uminus_unary n (bbZ_of_bbZ1 n H)).
Proof.
intros n H; apply bbN_of_bbN1; apply uminus_in_bbN1.
Qed.

(** Note/TODO: shameless copy/paste of above *)
Theorem uminus_in_bbZ1: forall (n: Ensemble BIG_type) (H: In bbN1 n),
  In bbZ1 (uminus_unary n (bbZ_of_bbN1 n H)).
Proof.
intros n H.
unfold uminus_unary.
generalize uminus_total_bijection_positive; intros.
assert (In (partial_function bbZ  bbZ) uminus /\ In (domain uminus) n).
generalize uminus_total_bijection; intros.
do 2 (induction H1).
rewrite H3; split; [ assumption | ].
apply bbZ_of_bbN1; assumption.
assert (In (partial_function bbZ  bbZ) (domain_restriction uminus bbN1) /\ In (domain (domain_restriction uminus bbN1)) n).
inversion H0.
inversion H2.
rewrite H6.
split; [ | assumption ].
apply (Monotonicity_laws_02 (Ensemble BIG_type) (Ensemble BIG_type) bbZ bbZ bbN1 bbZ1).
split; red; intros; [ apply bbZ_of_bbN1 | apply bbZ_of_bbZ1 ]; assumption.
assumption.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ n (app_uminus_Z n (bbZ_of_bbN1 n H)) H1).
(** Doesn't work ???
  rewrite <- Easy_laws_funct_04 with (Ensemble BIG_type) (Ensemble BIG_type)
  bbZ1 bbN1 uminus bbZ1 n H1 H2.
  *)
assert (app (domain_restriction uminus bbN1) n H2 = app uminus n H1).
apply Easy_laws_funct_04.
red; intros; apply bbZ_of_bbN1; assumption.
intuition.
split; [ assumption | ].
generalize uminus_total_bijection; intros.
do 2 (induction H3).
rewrite H5; apply bbZ_of_bbN1; assumption.
rewrite <- H3.
assert (In (domain_restriction uminus bbN1) (n, app (domain_restriction uminus bbN1) n H2)).
apply app_trivial_property.
do 5 (induction H0).
generalize (H0 _ H4); intros H8; inversion H8; assumption.
Qed.

Lemma bbZ_dec_aux: forall (x: Ensemble BIG_type) (H: In bbZ x),
  {In bbZ1 x} + {In bbN x}.
Proof.
intros x H.
assert (In bbZ1 x \/ In bbN x).
rewrite bbZ_decomposition in H.
destruct H.
left; assumption.
right; destruct H.
unfold bbN in H; destruct H; assumption.
destruct H; apply bbN_0_in_bbN.
pose (select := fun b:bool => if b then (In bbZ1 x) else (In bbN x)).
assert { b:bool | select b } as ([|],HP).
apply constructive_indefinite_description.
destruct H0.
exists true; trivial.
exists false; trivial.
left; trivial.
right; trivial.
Defined.

Lemma bbN_dec: forall (x: Ensemble BIG_type) (H: In bbN x),
  {x = bbN_0} + {In bbN1 x}.
Proof.
intros x H.
assert (x = bbN_0 \/ In bbN1 x).
assert (In (Union (Singleton bbN_0) bbN1) x).
unfold bbN1.
rewrite excluded_middle_1; [ assumption | ].
red; intros y H0; destruct H0; apply bbN_0_in_bbN.
destruct H0.
left; destruct H0; intuition.
right; assumption.
pose (select := fun b:bool => if b then (x = bbN_0) else (In bbN1 x)).
assert { b:bool | select b } as ([|],HP).
apply constructive_indefinite_description.
destruct H0.
exists true; trivial.
exists false; trivial.
left; trivial.
right; trivial.
Defined.

Lemma bbZ_dec: forall (x: Ensemble BIG_type) (H: In bbZ x),
  {x = bbN_0 } + {In bbZ1 x} + {In bbN1 x}.
Proof.
intros.
generalize (bbZ_dec_aux x H); intros.
destruct H0.
left.
right; assumption.
generalize (bbN_dec x i); intros.
destruct H0.
left; left; assumption.
right; assumption.
Defined.

Lemma PowerBIG_dec: forall (x: Ensemble BIG_type), {In bbZ x} + {In (Setminus (Power_set BIG) bbZ) x}.
Proof.
intros x.
assert (In bbZ x \/ In (Setminus (Power_set BIG) bbZ) x).
assert (In (Power_set BIG) x) as H.
constructor; red; constructor.
rewrite <- excluded_middle_1 with (Ensemble BIG_type) (Power_set BIG) bbZ in H.
destruct H; intuition.
red; intros; constructor; red; constructor.
pose (select := fun b:bool => if b then (In bbZ x) else (In (Setminus (Power_set BIG) bbZ) x)).
assert { b:bool | select b } as ([|],HP).
apply constructive_indefinite_description.
destruct H.
exists true; trivial.
exists false; trivial.
left; trivial.
right; trivial.
Qed.

(**
  TODO: BBook: section 3.6, p.171, array 1, row 1

  --n = n
*)
Hypothesis double_uminus: forall (n: Ensemble BIG_type) (H: In bbN n),
  let In_Z_n := (bbZ_of_bbN n H) in
  let uminus_n_in_bbZ := (uminus_in_bbZ n In_Z_n) in
    uminus_unary (uminus_unary n In_Z_n) uminus_n_in_bbZ = n.

(**
  ** Integers: validity
  *)

(**
  Recapitulating the existing morphisms so far:
  bbN -> nat : nat_of_bbN  (as Bcardinal)
  nat -> bbN : bbN_of_nat  (as a fixpoint building upon Bchoice)

  nat -> Z : Z_of_nat
  Z -> nat : nat_of_P (x:Z == Zpos(p) or Zneg(p), nat_of_P applying to p)

  Hence we need:
  bbN -> bbZ
  bbZ -> bbN (the purpose of section 3.6 of the BBook)
  and:
  bbZ -> Z
  Z -> bbZ (it should be easy with the above definitions)
  *)

Definition bbZ_of_Z (x:Z) :=
  match x with
    | Z0 => bbN_0
    | Zpos p => bbN_of_nat (nat_of_P p)
    | Zneg p =>
        let p_in_bbZ := bbZ_of_bbN (bbN_of_nat (nat_of_P p)) (bbN_of_nat_in_bbN (nat_of_P p)) in
          uminus_unary (bbN_of_nat (nat_of_P p)) p_in_bbZ
  end.

Definition Z_of_bbZ (x: Ensemble BIG_type) (H: In bbZ x): Z.
Proof.
generalize (bbZ_dec x H); intros.
destruct H0.
destruct s.
refine Z0.
refine (Zopp (Z_of_nat (nat_of_bbN (uminus_unary x H)))).
refine (Z_of_nat (nat_of_bbN x)).
Defined.

Theorem valid_uminus_Z:forall (x: Ensemble BIG_type) (H: In bbZ x),
  Z_of_bbZ (uminus_unary x H) (uminus_in_bbZ x H) = Zopp (Z_of_bbZ x H).
Proof.
intros x H.
assert (In bbZ x); [ assumption | intros ].
unfold Z_of_bbZ.
destruct (bbZ_dec x H).
destruct (bbZ_dec (uminus_unary x H) (uminus_in_bbZ x H)).
destruct s.
simpl.
destruct s0.
reflexivity.
assert ((uminus_unary x H)  = bbN_0).
unfold uminus_unary.
symmetry; apply identifying_app.
rewrite e.
exact uminus_zero.
rewrite H1 in i; destruct i.
generalize bbN_0_in_bbN; intros; contradiction.
destruct s0.
assert (x = bbN_0).
generalize uminus_total_bijection; intros.
inversion H1.
inversion H3.
inversion H5.
apply H9 with bbN_0.
rewrite <- e; apply app_trivial_property.
exact uminus_zero.
destruct i.
rewrite H1 in H3.
generalize bbN_0_in_bbN; intros; contradiction.
generalize (uminus_in_bbN1 x i); intros.
assert (uminus_unary x H = uminus_unary x (bbZ_of_bbZ1 x i)).
generalize uminus_total_bijection; intros.
inversion H2.
inversion H3.
inversion H6.
apply H10 with x; apply app_trivial_property.
rewrite <- H2 in H1.
destruct i0; destruct H1; contradiction.
destruct s.
assert ((uminus_unary x H)  = bbN_0).
unfold uminus_unary.
symmetry; apply identifying_app.
rewrite e.
exact uminus_zero.
rewrite H1 in i; destruct i.
absurd (In (Singleton bbN_0) bbN_0); intuition.
rewrite Zopp_involutive; reflexivity.
destruct (bbZ_dec (uminus_unary x H) (uminus_in_bbZ x H)).
destruct s.
assert (x = bbN_0).
generalize uminus_total_bijection; intros.
inversion H1.
inversion H3.
inversion H5.
apply H9 with bbN_0.
rewrite <- e; apply app_trivial_property.
exact uminus_zero.
rewrite H1 in i; destruct i.
absurd (In (Singleton bbN_0) bbN_0); intuition.
assert (In bbN x); [ destruct i; assumption | ].
generalize (double_uminus x H1); intros.
simpl in H2.
replace (uminus_unary (uminus_unary x H) (uminus_in_bbZ x H)) with
  (uminus_unary (uminus_unary x (bbZ_of_bbN x H1)) (uminus_in_bbZ x (bbZ_of_bbN x H1))).
rewrite H2; reflexivity.
unfold uminus_unary in H2 |- *.
symmetry; apply identifying_app.
replace (app uminus x (app_uminus_Z x (bbZ_of_bbN x H1))) with
  (app uminus x (app_uminus_Z x H)).
apply app_trivial_property.
apply irrelevant_applicability.
generalize (uminus_in_bbZ1 x i); intros.
assert (uminus_unary x H = uminus_unary x (bbZ_of_bbN1 x i)).
generalize uminus_total_bijection; intros.
inversion H2.
inversion H3.
inversion H6.
apply H10 with x; apply app_trivial_property.
rewrite <- H2 in H1.
destruct i0; destruct H1; contradiction.
Qed.

Theorem positive_in_bbN1: forall (p: positive), In bbN1 (bbN_of_nat (nat_of_P p)).
Proof.
intros p.
constructor; [ apply bbN_of_nat_in_bbN | ].
assert (nat_of_P p <> 0%nat).
destruct (ZL4 p).
rewrite H; discriminate.
intros H0; apply H.
inversion H0.
apply valid_nat_eq_r.
rewrite <- bbN_0_nat_zero; symmetry; assumption.
Qed.

Theorem Zcoercion_in_bbZ: forall (x:Z), In bbZ (bbZ_of_Z x).
Proof.
intros x.
rewrite bbZ_decomposition_simple.
induction x; simpl.
right; apply bbN_0_in_bbN.
right; apply bbN_of_nat_in_bbN.
left.
generalize (positive_in_bbN1 p); intros.
generalize (uminus_in_bbZ1 (bbN_of_nat (nat_of_P p)) H); intros.
assert (
  (uminus_unary (bbN_of_nat (nat_of_P p)) (bbZ_of_bbN1 (bbN_of_nat (nat_of_P p)) H))
  = (uminus_unary (bbN_of_nat (nat_of_P p)) (bbZ_of_bbN (bbN_of_nat (nat_of_P p)) (bbN_of_nat_in_bbN (nat_of_P p))))
).
apply irrelevant_applicability.
rewrite <- H1; assumption.
Qed.

Theorem valid_uminus_bbZ:forall (x: Z),
  bbZ_of_Z (Zopp x) = uminus_unary (bbZ_of_Z x) (Zcoercion_in_bbZ x).
Proof.
intros x.
induction x.
simpl.
unfold uminus_unary; apply identifying_app; exact uminus_zero.
simpl.
unfold uminus_unary; apply identifying_app; apply app_trivial_property.
simpl.
generalize (positive_in_bbN1 p); intros H.
assert (In bbN (bbN_of_nat (nat_of_P p))); [ destruct H; assumption | ].
generalize (double_uminus (bbN_of_nat (nat_of_P p)) H0); intros H1.
simpl in H1.
rewrite <- H1 at 1.
unfold uminus_unary at 3; apply identifying_app.
assert (
  (uminus_unary (bbN_of_nat (nat_of_P p)) (bbZ_of_bbN (bbN_of_nat (nat_of_P p)) (bbN_of_nat_in_bbN (nat_of_P p))))
  = (uminus_unary (bbN_of_nat (nat_of_P p)) (bbZ_of_bbN (bbN_of_nat (nat_of_P p)) H0))
).
unfold uminus_unary at 2; apply identifying_app.
apply app_trivial_property.
rewrite H2; apply app_trivial_property.
Qed.

Theorem uminus_inj: forall (x y: Ensemble BIG_type) (Hx: In bbZ x) (Hy: In bbZ y),
  uminus_unary x Hx = uminus_unary y Hy -> x = y.
Proof.
intros x y Hx Hy.
specialize (uminus_total_bijection); intros H H0.
inversion H as [ f H1 H2 H3 ].
inversion H2 as [ f0 H4 H5 H6 ].
inversion H4 as [ f1 H7 H8 H9 ].
apply H8 with (uminus_unary x Hx).
unfold uminus_unary; apply app_trivial_property.
rewrite H0; unfold uminus_unary; apply app_trivial_property.
Qed.

Theorem negative_as_uminus: forall (x: Ensemble BIG_type) (H: In bbZ1 x),
  exists n, exists H0:(In bbN n), x = uminus_unary n (bbZ_of_bbN n H0).
Proof.
intros x H.
exists (uminus_unary x (bbZ_of_bbZ1 x H)).
exists (uminus_in_bbN x H).
assert (In bbZ x) as Hx; [ apply bbZ_of_bbZ1; assumption | ].
assert (In bbZ
  (uminus_unary (uminus_unary x (bbZ_of_bbZ1 x H))
    (bbZ_of_bbN (uminus_unary x (bbZ_of_bbZ1 x H)) (uminus_in_bbN x H)))
  )
as Hmmx.
unfold uminus_unary; apply uminus_in_bbZ.
apply uminus_inj with Hx Hmmx.
unfold uminus_unary.
set (n := app uminus x (app_uminus_Z x (bbZ_of_bbZ1 x H))).
assert (In bbN n) as H0.
unfold n.
apply uminus_in_bbN.
specialize (double_uminus n H0); intros H1.
unfold uminus_unary in H1.
assert (
  (app uminus (app uminus n (app_uminus_Z n (bbZ_of_bbN n H0)))
    (app_uminus_Z (app uminus n (app_uminus_Z n (bbZ_of_bbN n H0)))
       (uminus_in_bbZ n (bbZ_of_bbN n H0))))
  =
  (app uminus (app uminus n (app_uminus_Z n (bbZ_of_bbN n (uminus_in_bbN x H))))
    (app_uminus_Z
      (app uminus n (app_uminus_Z n (bbZ_of_bbN n (uminus_in_bbN x H)))) Hmmx))
).
assert (
  (app uminus n (app_uminus_Z n (bbZ_of_bbN n (uminus_in_bbN x H)))) =
  (app uminus n (app_uminus_Z n (bbZ_of_bbN n H0)))
).
apply identifying_app.
apply app_trivial_property.
apply identifying_app.
rewrite H2.
apply app_trivial_property.
rewrite <- H2.
rewrite H1.
unfold n.
apply irrelevant_applicability.
Qed.

Variable bbZ_succ: Ensemble (Ensemble BIG_type * Ensemble BIG_type).

Hypothesis bbZ_succ_all: forall (m n: Ensemble BIG_type) (_: In bbZ_succ (m, n)),
  (forall (H:In bbN n), m = app bbN_succ n (bbN_succ_y n H)) /\
  (forall (H: In bbZ1 n),
    m =
uminus_unary
  (app bbN_pred (uminus_unary n (bbZ_of_bbZ1 n H)) (bbN_pred_y (uminus_unary n (bbZ_of_bbZ1 n H)) (uminus_in_bbN1 n H)))
  (bbZ_of_bbN
    (app bbN_pred (uminus_unary n (bbZ_of_bbZ1 n H)) (bbN_pred_y (uminus_unary n (bbZ_of_bbZ1 n H)) (uminus_in_bbN1 n H)))
    (bbN_pred_in_bbN (uminus_unary n (bbZ_of_bbZ1 n H)) (uminus_in_bbN1 n H))
  )
).

Hypothesis bbZ_succ_for_bbN: forall (n: Ensemble BIG_type) (H: In bbN n),
  In bbZ_succ (n, app bbN_succ n (bbN_succ_y n H)).
(**
  succ(-n) = - pred(n), BBook: section 3.6, p.171, array 1, row 2

  Note/TODO: minor BBook mistake: pred takes strictly positive naturals, hence the
  hypothesis upon n is that it belongs to bbN1
  *)
Hypothesis bbZ_succ_for_bbZ1: forall (n: Ensemble BIG_type) (H: In bbN1 n),
  In bbZ_succ
    ( uminus_unary n (bbZ_of_bbN1 n H),
      uminus_unary (app bbN_pred n (bbN_pred_y n H)) (bbZ_of_bbN (app bbN_pred n (bbN_pred_y n H)) (bbN_pred_in_bbN n H))
    ).

(**
  A third, implicit hypothesis: on everything that is *not* an integer, bbZ_succ is not defined.
  *)
Hypothesis bbZ_succ_for_PBIG: Included (image bbZ_succ (Setminus (Power_set BIG) bbZ)) (Empty_set (Ensemble BIG_type)).

(*
Theorem bbZ_succ_total: In (total_function bbZ bbZ) bbZ_succ.
Proof.
constructor.
constructor.
do 2 constructor; red; intros cpl H0.
destruct cpl as [ x Sx ].
assert (In (Power_set BIG) x).
constructor; red; intros; constructor.
rewrite <- excluded_middle_1 with (Ensemble BIG_type) (Power_set BIG) bbZ in H.
destruct H as [ x H | x H ].
rewrite bbZ_decomposition_simple in H.
destruct H as [ x H | x H ].
specialize (negative_as_uminus x H); intros H1.
destruct H1 as [ n H1 ].
destruct H1 as [ H1 H2 ].
split.
rewrite H2; apply uminus_in_bbZ.


replace Sx with (app bbN_pred n (bbN_pred_y n H1)).
replace Sx with (uminus_unary x (bbZ_of_bbZ1 x H)).
apply uminus_in_bbZ.
symmetry; apply identifying_app.

specialize uminus_total_bijection; intros H3.
inversion




Qed.

Theorem bbZ_succ_y: forall (x: Ensemble BIG_type) (H: In bbZ x),
  In (partial_function bbZ bbZ) bbZ_succ /\ In (domain bbZ_succ) x.
Proof.
intros x H.
specialize bbZ_succ_for_PBIG; intros H0.


split.
constructor.
do 2 constructor; red; intros succ H0.
destruct succ.



rewrite bbZ_decomposition_simple in H.
destruct (bbZ_dec_aux x H) as [ H0 | H0 ].



Qed.

Theorem valid_bbZ_succ_bb:forall (x: Ensemble BIG_type) (H: In bbZ x),
  Z_of_bbZ (app bbZ_succ uminus_un x H) (uminus_in_bbZ x H) = Zopp (Z_of_bbZ x H).
*)



(*
Axioms left to express:
  pred(-n) = -succ(n)
  -n <= m
  n <= -m <-> n=0 /\ m=0
  -n <= -m <-> m <= n
  -m+n = n-m
  m+(-n) = m-n
  -m+-n = -(m+n)
  (-m)*n = -(m*n)
  m*(-n) = -(m*n)
  (-m)*(-n) = m*n
  (-m)-n = -(m+n)
  m-(-n) = m+n
  -m-(-n) = n-m
  n>m -> m-n = -(n-m)
  (-n)/m = -(n/m)
  n/(-m) = -(n/m)
  (-n)/(-m) = n/m
*)
