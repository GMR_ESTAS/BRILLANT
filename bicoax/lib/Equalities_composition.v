(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Bderived_constructs.
Require Import Brelations.
Require Import Bfunctions.

Open Scope eB_scope.

(**
  SC: it would be more interesting to have weaker versions of 01 and 02
  *)


(**
  BBook: section 2.6.4, p.104, array 1, row 1
  *)
Theorem Equality_laws_compose_01: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U)(p: Ensemble (T*U)) (q: Ensemble (T*U)) (r: Ensemble (S*T)), (p ∈ (t ↔ u) ∧ q ∈ (t ↔ u) ∧  r ∈ (s ⤀ t) ∧ (r;p) = (r;q)) ⇒(p = q).
Proof.
intros S T U s t u p q r H.
decompose [and] H; clear H.
assert (Same_set (S*U) (composition r p) (composition r q)).
rewrite H1; intuition.
induction H.
induction H3.

apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H6.

induction x.
induction H0; induction H0.
generalize (H0 (a,b) H6); intros.
inversion H7.
generalize (H5 a H10); intros.
inversion H12.
assert (In (composition f X) (x,b)).
constructor; exists a; intuition.
generalize (H (x,b) H14); intros.
inversion H15.
inversion H17.
assert (x0=a).
induction H3.
apply H20 with x; intuition.
rewrite <- H20; intuition.

induction x.
induction H4; induction H4.
generalize (H4 (a,b) H6); intros.
inversion H7.
generalize (H5 a H10); intros.
inversion H12.
assert (In (composition f X) (x,b)).
constructor; exists a; intuition.
generalize (H2 (x,b) H14); intros.
inversion H15.
inversion H17.
assert (x0=a).
induction H3.
apply H20 with x; intuition.
rewrite <- H20; intuition.
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 2
  *)
Theorem Equality_laws_compose_02: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U)(p: Ensemble (S*T)) (q: Ensemble (S*T)) (r: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ r ∈ (t ↔ u) ∧ r∼ ∈ (u ⤀ t) ∧ (p;r) = (q;r)) ⇒(p = q).
Proof.
intros S T U s t u p q r H.
decompose [and] H; clear H.
assert (Same_set (S*U) (composition p r) (composition q r)).
rewrite H1; intuition.
induction H.
inversion_clear H3.

apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H8.

induction x.
induction H2; induction H2.
generalize (H2 (a,b) H8); intros.
inversion H3.
generalize (H7 b H12); intros.
inversion H13.
assert (In (composition X r) (a,x)).
inversion H14; constructor; exists b; intuition.
generalize (H (a,x) H15); intros.
inversion H16.
inversion H18.
assert (x0=b).
inversion H6.
apply H22 with x; [ constructor; intuition | intuition ].
rewrite <- H21; intuition.

induction x.
induction H5; induction H3.
generalize (H3 (a,b) H8); intros.
inversion H5.
generalize (H7 b H12); intros.
inversion H13.
assert (In (composition X r) (a,x)).
inversion H14; constructor; exists b; intuition.
generalize (H0 (a,x) H15); intros.
inversion H16.
inversion H18.
assert (x0=b).
inversion H6.
apply H22 with x; [ constructor; intuition | intuition ].
rewrite <- H21; intuition.
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 3
  *)
Theorem Equality_laws_compose_03: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (p: Ensemble (T*U)) (q: Ensemble (U*V)) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ∧ p ∈ (t ↔ u) ∧ q ∈ (u ↔ v)) ⇒((r;(p;q)) = ((r;p);q)).
Proof.
intros S T U V s t u v p q r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H2.
inversion H4.
constructor; exists x0; split; [ constructor; exists x; intuition | intuition ].

induction H0.
inversion H0.
induction H1.
inversion H1.
inversion H4.
constructor; exists x0; split; [ intuition | constructor; exists x; intuition ].
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 4

  Note/TODO: I (SC) weakened the hypotheses here and it still works
  *)
Theorem Equality_laws_compose_04: forall (S T: Type) (v: Ensemble T) (r: Ensemble (S*T)),((r;id(v)) = r ▷ v).
Proof.
intros S T v r.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.

induction H.
inversion H.
induction H0.
inversion H1.
inversion H4.
constructor; [ rewrite <- H5; assumption | assumption ].

induction H.
constructor; exists y.
split; [ assumption | constructor; [ constructor; assumption | reflexivity ]].
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 5

  Note/TODO: same as above
  *)
Theorem Equality_laws_compose_05: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ⇒ (r;id(t)) = r).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H2.
rewrite <- H6; assumption.

induction x.
constructor; exists b.
split; [ assumption | ].
constructor.
repeat (induction H).
generalize (H (a,b) H0); intros H1; inversion H1; constructor; assumption.
reflexivity.
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 6
  *)
Theorem Equality_laws_compose_06: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (T*U)) (r: Ensemble (S*T)) (v: Ensemble T), (r ∈ (s ↔ t) ∧ v ⊆ t ∧ p ∈ (t ↔ u)) ⇒((r;(v ◁ p)) = ((r ▷ v);p )).
Proof.
intros S T U s t u p r v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H2.
constructor; exists x.
split; [ constructor; intuition | assumption ].

induction H0.
inversion H0.
induction H1.
inversion H1.
constructor; exists x.
split; [ assumption | constructor; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 7
  *)
Theorem Equality_laws_compose_07: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (r: Ensemble (S*T)) (p: Ensemble (T*U)) (w: Ensemble U), (r ∈ (s ↔ t) ∧ p ∈ (t ↔ u) ∧ w ⊆ u) ⇒((r;(p ▷ w)) = (r;p) ▷ w ).
Proof.
intros S T U s t u r p w H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H2.
constructor; [ constructor; exists x; intuition | assumption ].

induction H0.
inversion H0.
inversion H3.
constructor; exists x0.
split; [ intuition | constructor; intuition ].
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 8
  *)
Theorem Equality_laws_compose_08: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (T*U)) (r: Ensemble (S*T)) (v: Ensemble T), (r ∈ (s ↔ t) ∧ v ⊆ t ∧ p ∈ (t ↔ u)) ⇒(r;(v ⩤ p)) = ((r ⩥ v);p).
Proof.
intros S T U s t u p r v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H2.
constructor; exists x.
split; [ constructor; intuition | assumption ].

induction H0.
inversion H0.
induction H1.
inversion H1.
constructor; exists x.
split; [ assumption | constructor; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 9
  *)
Theorem Equality_laws_compose_09: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (r: Ensemble (S*T)) (p: Ensemble (T*U)) (w: Ensemble U), (r ∈ (s ↔ t) ∧ p ∈ (t ↔ u) ∧ w ⊆ u) ⇒((r;(p ⩥ w)) = (r;p) ⩥ w ).
Proof.
intros S T U s t u r p w H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H2.
constructor; [ constructor; exists x; intuition | assumption ].

induction H0.
inversion H0.
inversion H3.
constructor; exists x0.
split; [ intuition | constructor; intuition ].
Qed.

(**
  property 2.6.4, BBook: section 2.6.4, p.104, array 1, row 10
  *)
Theorem Equality_laws_compose_10: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (p: Ensemble (T*U)) (q: Ensemble (T*U)), (f ∈ (s ⇸ t) ∧ p ∈ (t ↔ u) ∧ q ∈ (t ↔ u)) ⇒((f;(p ⥷ q)) = (f;p) ⥷ (f;q) ).
Proof.
intros S T U s t u f p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H2.
induction H4.
constructor.
left.
split.
constructor; exists x; intuition.
intros H6; apply (proj2 H4).
induction H6; inversion H6.
constructor; exists c.
inversion H7.
inversion H9.
assert (x=x2).
decompose [and] H.
induction H14.
apply H14 with a; intuition.
assert (In (domain q) x); [ constructor; exists x1; rewrite H12; intuition | ].
induction H4; contradiction.
constructor.
right.
constructor; exists x; intuition.

induction H0.
induction H0.
induction H0.
inversion H0.
inversion H3.
constructor; exists x0.
split; [ intuition | constructor ].
left.
split; [ intuition | ].
intros H6; apply H1.
constructor; exists y.
constructor; exists x0.
split; [ intuition | ].
induction H6.
inversion H6.
assert (In (domain (composition f q)) x).
constructor; exists x0.
constructor; exists a0; intuition.
contradiction.
induction H0.
inversion H0.
constructor; exists x0.
split; [ intuition | constructor; right; intuition ].
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 11
  *)
Theorem Equality_laws_compose_11: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (r: Ensemble (S*T)) (p: Ensemble (T*U)) (q: Ensemble (T*U)), (r ∈ (s ↔ t) ∧ p ∈ (t ↔ u) ∧ q ∈ (t ↔ u)) ⇒((r;(p ∪ q)) = (r;p) ∪ (r;q) ).
Proof.
intros S T U s t u r p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H2.
left; constructor; exists x; intuition.
right; constructor; exists x; intuition.

induction H0; induction H0; inversion H0; constructor; exists x.
split; [ intuition | left; intuition ].
split; [ intuition | right; intuition ].
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 12
  *)
Theorem Equality_laws_compose_12: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (p: Ensemble (T*U)) (q: Ensemble (T*U)), (f ∈ (s ⇸ t) ∧ p ∈ (t ↔ u) ∧ q ∈ (t ↔ u)) ⇒((f;(p ∩ q)) = (f;p) ∩ (f;q) ).
Proof.
intros S T U s t u f p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H2.
split; constructor; exists x; intuition.

induction H0.
decompose [and] H.
induction H4.
inversion H0; inversion H1.
inversion H6; inversion H8.
assert ((a,c) = (a0,c0)).
rewrite H7; rewrite H9; reflexivity.
injection H12; intros.
assert (x0 = x1).
apply H4 with a.
intuition.
rewrite H14; intuition.
constructor; exists x0.
split; [ intuition | ].
split; [ intuition | rewrite H15; rewrite H13; intuition ].
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 13
  *)
Theorem Equality_laws_compose_13: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (p: Ensemble (T*U)) (q: Ensemble (T*U)), (f ∈ (s ⇸ t) ∧ p ∈ (t ↔ u) ∧ q ∈ (t ↔ u)) ⇒((f;(p ∖ q)) = (f;p) ∖ (f;q) ).
Proof.
intros S T U s t u f p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
induction H2.
split.
constructor; exists x; intuition.
intros H4; apply H3.
inversion H4.
inversion H6.
assert (x=x0).
decompose [and] H.
induction H11.
apply H11 with a; intuition.
rewrite H9; intuition.

induction H0.
induction H0.
inversion H0.
constructor; exists x.
split; [ intuition | ].
constructor.
intuition.
intros H3; apply H1.
constructor; exists x; intuition.
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 14

  error: in the BBook, replace x ∈ s with x ∈ t
  *)
Theorem Equality_laws_compose_14: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (x: T) (y: U) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ∧ x ∈ t ∧ y ∈ u) ⇒((composition r (Singleton (x↦y)) = r∼[(Singleton x)] × (Singleton y))).
Proof.
intros S T U s t u x y r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

inversion H0.
inversion H1.
induction H3.
inversion H4.
constructor.
compute; apply image_intro with x0.
split; [ intuition | constructor; assumption ].
intuition.

induction H0.
inversion H1.
induction H0.
induction H0.
inversion H0.
inversion H3.
constructor; exists x0.
intuition.
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 15
  *)
Theorem Equality_laws_compose_15: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (T*U)) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ∧ p ∈ (t ↔ u) ∧ p = ∅) ⇒((r;p) = ∅).
Proof.
intros S T U s t u p r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

decompose [and] H.
induction H0.
inversion H0.
assert (In (Empty_set _) (x,c)).
rewrite <- H2; intuition.
contradiction.

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 16
  *)
Theorem Equality_laws_compose_16: forall (S T V: Type) (s: Ensemble S) (t: Ensemble T) (v: Ensemble V) (r: Ensemble (S*T)) (u: Ensemble T), (r ∈ (s ↔ t) ∧ u ⊆ t) ⇒((r;(u × v)) = r∼[u] × v ).
Proof.
intros S T V s t v r u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H2.
constructor; [ compute; apply image_intro with x | assumption ].
split; [ assumption | constructor; assumption ].

induction H0.
induction H0.
induction H0.
inversion H2.
constructor; exists x.
split; [ assumption | constructor; assumption ].
Qed.

(**
  property 2.6.5, BBook: section 2.6.4, p.104, array 1, row 17
  *)
Theorem Equality_laws_compose_17: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V)(p: Ensemble (T*U)) (q: Ensemble (T*V)) (f: Ensemble (S*T)), (f ∈ (s ⇸ t) ∧ p ∈ (t ↔ u) ∧ q ∈ (t ↔ v) ) ⇒((f;(p ⊗ q)) = (f;p) ⊗ (f;q) ).
Proof.
intros S T U V s t u v p q f H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H2.
constructor; constructor; exists x; intuition.

decompose [and] H.
induction H3.
induction H0.
inversion H0; inversion H5.
inversion H7; inversion H10.
assert (x0 = x1); [ apply H3 with x; intuition | ].
constructor; exists x0.
split; [ intuition | ].
constructor; [ intuition | rewrite H14; intuition ].
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 18
  *)
Theorem Equality_laws_compose_18: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ r ∈ (s ↔ t) ) ⇒((id(u);r) = u ◁ r).
Proof.
intros S T s t r u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H1.
inversion H5.
constructor; [ rewrite H6; assumption | assumption ].

induction H0.
constructor; exists x.
split; [ constructor; [ constructor; assumption | reflexivity ] | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 19
  *)
Theorem Equality_laws_compose_19: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ) ⇒((id(s);r) = r).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H1.
rewrite H6; assumption.

induction x.
constructor; exists a.
split; [ | assumption ].
constructor.
repeat (induction H).
generalize (H (a,b) H0); intros H1; inversion H1; constructor; assumption.
reflexivity.
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 20
  *)
Theorem Equality_laws_compose_20: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (r: Ensemble (T*S)) (u: Ensemble S), (u ⊆ s ∧ p ∈ (s ↔ t) ∧ r ∈ (t ↔ u) ) ⇒(((u ◁ p);r) = u ◁ (p;r)).
Proof.
intros S T s t p r u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H1.
constructor; [ constructor; exists x; intuition | assumption ].

induction H0.
inversion H0.
inversion H3.
constructor; exists x0.
split; [ constructor; intuition | intuition ].
Qed.

(**
  BBook: section 2.6.4, p.104, array 1, row 21
  *)
Theorem Equality_laws_compose_21: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (r: Ensemble (T*U)) (v: Ensemble T), (p ∈ (s ↔ t) ∧ v ⊆ t ∧ r ∈ (t ↔ u) ) ⇒(((p ▷ v);r) = (p;(v ◁ r))).
Proof.
intros S T U s t u p r v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H1.
constructor; exists x.
split; [ assumption | constructor; intuition ].

induction H0.
inversion H0.
induction H1.
inversion H2.
constructor; exists x.
split; [ constructor; assumption | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 1
  *)
Theorem Equality_laws_compose_22: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (r: Ensemble (T*S)) (u: Ensemble S), (u ⊆ s ∧ p ∈ (s ↔ t) ∧ r ∈ (t ↔ u) ) ⇒(((u ⩤ p);r) = u ⩤ (p;r)).
Proof.
intros S T s t p r u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H1.
constructor; [ constructor; exists x; intuition | assumption ].

induction H0.
inversion H0.
inversion H3.
constructor; exists x0.
split; [ constructor; intuition | intuition ].
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 2
  *)
Theorem Equality_laws_compose_23: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (r: Ensemble (T*U)) (v: Ensemble T), (p ∈ (s ↔ t) ∧ v ⊆ t ∧ r ∈ (t ↔ u) ) ⇒(((p ⩥ v);r) = (p;(v ⩤ r))).
Proof.
intros S T U s t u p r v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H1.
constructor; exists x.
split; [ assumption | constructor; intuition ].

induction H0.
inversion H0.
induction H1.
inversion H2.
constructor; exists x.
split; [ constructor; assumption | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 3
  *)
Theorem Equality_laws_compose_24: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (r: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ r ∈ (t ↔ u) ∧ range(q) ⊆ domain(r)) ⇒(((p ⥷ q);r) = (p;r) ⥷ (q;r)).
Proof.
intros S T U s t u p q r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H1.
induction H4.
constructor.
left.
split.
constructor; exists x; intuition.
intros H6; apply (proj2 H4).
induction H6; inversion H6.
inversion H7.
inversion H9.
constructor; exists x2; intuition.
constructor.
right.
constructor; exists x; intuition.

induction H0; induction H0.
induction H0.
inversion H0.
inversion H3.
constructor; exists x0.
split; [ constructor | intuition ].
left.
split; [ intuition | ].
intros H6; apply H1.
induction H6.
inversion H6.
assert (In (domain r) x).
decompose [and] H.
apply H9.
constructor; exists a0; assumption.
induction H8.
inversion H8.
constructor; exists x.
constructor; exists a1; intuition.
induction H0.
inversion H0.
constructor; exists x0.
split; [ | intuition ].
constructor; right; intuition.
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 4
  *)
Theorem Equality_laws_compose_25: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (r: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ r ∈ (t ↔ u)) ⇒(((p ∪ q);r) = (p;r) ∪ (q;r)).
Proof.
intros S T U s t u r p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H1.
left; constructor; exists x; intuition.
right; constructor; exists x; intuition.

induction H0; induction H0; inversion H0; constructor; exists x.
split; [ left; intuition | intuition ].
split; [ right; intuition | intuition ].
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 5
  *)
Theorem Equality_laws_compose_26: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (r: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ r ∈ (t ↔ u) ∧ r∼ ∈ (u ⇸ t)) ⇒(((p ∩ q);r) = (p;r) ∩ (q;r)).
Proof.
intros S T U s t u p q r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H1.
split; constructor; exists x; intuition.

induction H0.
decompose [and] H.
inversion H3.
inversion H0; inversion H1.
inversion H9; inversion H11.
assert ((a,c) = (a0,c0)).
rewrite H12; rewrite H10; reflexivity.
injection H15; intros.
assert (x0 = x1).
apply H7 with c.
intuition.
constructor; intuition.
rewrite H16; constructor; intuition.
constructor; exists x0.
split; [ | intuition ].
split; [ intuition | rewrite H17; rewrite H18; intuition ].
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 6
  *)
Theorem Equality_laws_compose_27: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (r: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ r ∈ (t ↔ u) ∧ r∼ ∈ (u ⇸ t)) ⇒(((p ∖ q);r) = (p;r) ∖ (q;r)).
Proof.
intros S T U s t u p q r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
induction H1.
split.
constructor; exists x; intuition.
intros H4; apply H3.
inversion H4.
inversion H6.
assert (x=x0).
decompose [and] H.
inversion H10.
apply H14 with c.
constructor; assumption.
constructor; intuition.
rewrite H9; intuition.

induction H0.
induction H0.
inversion H0.
constructor; exists x.
split; [ | intuition ].
constructor.
intuition.
intros H3; apply H1.
constructor; exists x; intuition.
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 7
  *)
Theorem Equality_laws_compose_28: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (x: S) (y: T) (r: Ensemble (T*U)), (x ∈ s ∧ y ∈ t ∧ r ∈ (t ↔ u)) ⇒(((Singleton (x↦y));r )= ((Singleton x) × r[(Singleton y)])).
Proof.
intros S T U s t u x y r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

inversion H0.
inversion H1.
induction H3.
inversion H3.
constructor.
intuition.
compute; apply image_intro with x0.
split; [ intuition | assumption ].

induction H0.
inversion H1.
induction H2.
inversion H2.
inversion H0.
constructor; exists x0.
split; [ intuition | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 8
  *)
Theorem Equality_laws_compose_29: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (r: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ r ∈ (t ↔ u) ∧ p = ∅) ⇒((p;r) = ∅).
Proof.
intros S T U s t u p r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

decompose [and] H.
induction H0.
inversion H0.
assert (In (Empty_set _) (a,x)).
rewrite <- H2; intuition.
contradiction.

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 9
  *)
Theorem Equality_laws_compose_30: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble U) (v: Ensemble S), (v ⊆ s ∧ r ∈ (s ↔ t) ) ⇒(((u × v);r) = u × r[v]).
Proof.
intros S T V s t r u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H1.
constructor; [ assumption | compute; apply image_intro with x ].
intuition.

induction H0.
induction H1.
induction H1.
constructor; exists x.
split; [ constructor; assumption | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 10
  *)
Theorem Equality_laws_compose_31: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)), (f ∈ (s ⇸ t)) ⇒((f∼;f) = id(range(f)) ).
Proof.
intros S T s t f H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H1.
constructor; [ constructor; constructor; exists x; assumption | ].
induction H.
apply H6 with x; intuition.

induction H0.
inversion H0.
induction H4; induction H5.
inversion H4; inversion H5.
rewrite <- H1.
constructor; exists x.
split; [ constructor; assumption | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 11
  *)
Theorem Equality_laws_compose_32: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ∧ r∼ ∈ (t ⇸ s)) ⇒((r;r∼) = id(domain(r))).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H2.
constructor; [ constructor; constructor; exists x; assumption | ].
induction H.
inversion H6.
apply H8 with x.
constructor; assumption.
assumption.

induction H0.
inversion H0.
induction H4; induction H5.
inversion H4; inversion H5.
rewrite <- H1.
constructor; exists x.
split; [ assumption | constructor; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 12
  *)
Theorem Equality_laws_compose_33: forall (S: Type) (s: Ensemble S)(u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s) ⇒((id(u);id(v)) =  id(u ∩ v)).
Proof.
intros S s u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H1.
inversion H1; inversion H2.
inversion H5; inversion H9.
constructor.
constructor.
split; [ assumption | rewrite H6; assumption ].
split; [ rewrite <- H10; assumption | assumption ].
transitivity x; assumption.

induction H0.
inversion H0.
induction H4; induction H5.
constructor; exists a0.
split.
constructor.
constructor; [assumption | rewrite H2; assumption ].
rewrite H2; reflexivity.
constructor.
constructor; [ rewrite H2; assumption | assumption ].
rewrite H2; rewrite <- H1; reflexivity.
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 13
  *)
Theorem Equality_laws_compose_34: forall (S T U V W Z: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (w: Ensemble W) (z: Ensemble Z) (p: Ensemble (S*T)) (q: Ensemble (U*V)) (h: Ensemble (T*W)) (k: Ensemble (V*Z)), (p ∈ (s ↔ t) ∧ q ∈ (u ↔ v) ∧ h ∈ (t ↔ w) ∧ k ∈ (v ↔ z) ) ⇒(((p ∥ q);(h ∥ k)) = ((p;h) ∥ (q;k))).
Proof.
intros S T U V W Z s t u v w z p q h k H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

inversion H0.
inversion H1.
induction H3.
inversion H3; inversion H4.
assert ((z0,w0)=(x2,y0)).
rewrite H6; rewrite H9; reflexivity.
injection H13; intros.
constructor.
constructor; exists x2.
split; [ rewrite <- H15; assumption | assumption ].
constructor; exists y0.
split; [ rewrite <- H14; assumption | assumption ].

inversion H0.
inversion H1; inversion H2.
inversion H5; inversion H8.
induction H10; induction H11.
constructor.
exists (x1, x2).
split.
constructor; assumption.
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.105, array 1, row 14
  *)
Theorem Equality_laws_compose_35: forall (S T U V W: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (w: Ensemble W) (p: Ensemble (S*T)) (q: Ensemble (S*U)) (h: Ensemble (T*V)) (k: Ensemble (U*W)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ u) ∧ h ∈ (t ↔ v) ∧ k ∈ (u ↔ w)) ⇒(((p ⊗ q);(h ∥ k)) = (p;h) ⊗ (q;k)).
Proof.
intros S T U V W s t u v w p q h k H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

inversion H0.
inversion H1.
induction H3.
inversion H3; inversion H4.
assert ((y,z) = (x2, y0)).
rewrite H6; rewrite H9; reflexivity.
injection H13; intros.
constructor.
constructor; exists x2.
split; [ rewrite <- H15; assumption | assumption ].
constructor; exists y0.
split; [ rewrite <- H14; assumption | assumption ].

inversion H0.
inversion H1; inversion H2.
inversion H5; inversion H8.
induction H10; induction H11.
constructor.
exists (x1, x2).
split.
constructor; assumption.
constructor; assumption.
Qed.


Close Scope eB_scope.
