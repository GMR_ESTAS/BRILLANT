(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter2.
Require Import Bgeneralized_union_inter.
Require Import Bfixpoints.
Require Import Bnaturals_iterate.
Require Import Arith.

Open Local Scope nat_scope.

(**
  * Transitive closures (reflexive and not) of a relation
  *)

(**
  ** Transitive closures: definitions
  *)

(**
  definition of reflexive transitive closure, BBook: section 3.5.10, p.169, array 1

  Note/TODO: these definitions may not be easiest definitions to handle. To confront
  with practice, I guess
  *)
Inductive closure (S: Type) (s: Ensemble S) (r: Ensemble (S*S)): Ensemble (S*S) :=
  | rtc_init: forall (x: S), In s x -> closure S s r (x, x)
  | rtc_comp: forall (x y z:S), In r (x,z) -> In (closure S s r) (z,y) -> closure S s r (x,y).
Implicit Arguments closure [S].

(**
  definition of reflexive transitive closure, BBook: section 3.5.10, p.169, array 3
  *)
Inductive closure1 (S: Type) (s: Ensemble S) (r: Ensemble (S*S)): Ensemble (S*S) :=
  | tc_init: forall (x y: S), In r (x,y) -> closure1 S s r (x, y)
  | tc_comp: forall (x y z:S),  In r (x,z) -> In (closure1 S s r) (z,y) -> closure1 S s r (x,y).
Implicit Arguments closure1 [S].

(**
  definition of reflexive transitive closure, BBook: section 3.5.10, p.169, array 1
  *)
Definition genr (S: Type) (s: Ensemble S) (r: Ensemble (S*S)):=
  lambda (Power_set (times s s)) (fun h => Union (id s) (composition r h)).
Definition bbN_rstar (S: Type) (s: Ensemble S) (r: Ensemble (S*S)):= Bfix (times s s) (genr S s r).

(**
  definition of reflexive transitive closure, BBook: section 3.5.10, p.169, array 3
  *)
Definition genrp (S: Type) (s: Ensemble S) (r: Ensemble (S*S)):=
  lambda (Power_set (times s s)) (fun h => Union r (composition r h)).
Definition bbN_rplus (S: Type) (s: Ensemble S) (r: Ensemble (S*S)):= Bfix (times s s) (genrp S s r).

(**
  ** Transitive closures: validity
  *)

Theorem valid_closure: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  closure s r = bbN_rstar S s r.
Proof.
intros S s r H.
apply Extensionality_Ensembles; split; red; intros.

(** The shape is different from the shapes of first_special_case or second_special_case,
  hence we have to use the "old" method *)
induction H0.
do 2 constructor.
intros.
induction H1.
apply H1.
unfold genr; apply abstract_lambda.
left.
constructor.
constructor; assumption.
reflexivity.
do 2 constructor.
intros.
induction H2.
apply H2.
unfold genr; apply abstract_lambda.
right.
constructor; exists z.
split; [ assumption | ].
inversion IHclosure.
inversion H3.
apply H5.
do 2 red.
exists x0; assumption.

induction H0.
induction H0.
apply H0.
do 2 red.
assert (In (total_function (Power_set (times s s)) (Power_set (times s s))) (genr S s r)).
unfold genr; apply lambda_is_total.
intros.
induction H1.
constructor; red; intros.
induction H2.
inversion H2; assumption.
do 3 (induction H2).
do 2 (induction H).
generalize (H _ H2); generalize (H1 _ H3); intros.
inversion H4; inversion H5; constructor; assumption.
assert (In (partial_function (Power_set (times s s)) (Power_set (times s s))) (genr S s r) /\ In (domain (genr S s r)) (closure s r)).
inversion_clear H1.
split; [ assumption | ].
rewrite H3.
constructor; red; intros.
induction H1.
constructor; assumption.
do 2 (induction H).
generalize (H _ H1); intros.
inversion IHclosure; inversion H5; constructor; assumption.
exists H2.
unfold genr.
apply abstract_lambda.
red; intros.
induction H3.
induction x0.
induction H3.
rewrite H4; left.
inversion H3; assumption.
do 3 (induction H3).
right with x0; assumption.
Qed.

Theorem valid_closure1: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  closure1 s r = bbN_rplus S s r.
Proof.
intros S s r H.
apply Extensionality_Ensembles; split; red; intros.

(** The proof is very similar with the proof of validity for closure *)
induction H0.
do 2 constructor.
intros.
induction H1.
apply H1.
unfold genrp; apply abstract_lambda.
left.
assumption.
do 2 constructor.
intros.
induction H2.
apply H2.
unfold genrp; apply abstract_lambda.
right.
constructor; exists z.
split; [ assumption | ].
inversion IHclosure1.
inversion H3.
apply H5.
do 2 red.
exists x0; assumption.

induction H0.
induction H0.
apply H0.
do 2 red.
assert (In (total_function (Power_set (times s s)) (Power_set (times s s))) (genrp S s r)).
unfold genrp; apply lambda_is_total.
intros.
induction H1.
constructor; red; intros.
induction H2.
do 2 (induction H).
apply H; assumption.
do 3 (induction H2).
do 2 (induction H).
generalize (H _ H2); generalize (H1 _ H3); intros.
inversion H4; inversion H5; constructor; assumption.
assert (In (partial_function (Power_set (times s s)) (Power_set (times s s))) (genrp S s r) /\ In (domain (genrp S s r)) (closure1 s r)).
inversion_clear H1.
split; [ assumption | ].
rewrite H3.
constructor; red; intros.
induction H1.
do 2 (induction H).
generalize (H _ H1); tauto.
do 2 (induction H).
generalize (H _ H1); intros.
inversion IHclosure1; inversion H5; constructor; assumption.
exists H2.
unfold genrp; apply abstract_lambda.
red; intros.
induction H3.
induction x0.
left; assumption.
do 3 (induction H3).
right with x0; assumption.
Qed.

(**
  ** Transitive closures: properties
  *)

(**
  *** Reflexive transitive closure: properties
  *)

Theorem any_iterate_in_closure: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  (forall (n:nat) (x: S*S), In (iter s r n) x -> In (closure s r) x).
Proof.
intros S s r H n.
induction n.
simpl; intros.
induction H0.
rewrite H1 in H0 |- *; left; inversion H0; assumption.
simpl; intros.
do 3 (induction H0).
right with x.
assumption.
apply IHn; assumption.
Qed.

(**
  property in the text, BBook: section 3.5.10, p.168
  *)
Theorem closure_expansion: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  closure s r = Union (id s) (composition r (closure s r)).
Proof.
intros S s r H.
(** Using the BBook definition, it would involve the Knaster-Tarski theorem *)
apply Extensionality_Ensembles; split; red; intros.

induction H0.
left; constructor.
constructor; assumption.
reflexivity.
right; constructor; exists z.
intuition.

induction H0.
induction H0.
rewrite H1 in H0 |- *; left; inversion H0; assumption.
do 3 (induction H0).
right with x; assumption.
Qed.

(**
  BBook: section 3.5.10, p.169, array 2, row 1
  *)
Theorem closure_QuantifiedUnion_of_iterates: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  closure s r = QuantifiedUnion (Full_set nat) (fun n => iter s r n).
Proof.
intros S s r H.
apply Extensionality_Ensembles; split; red; intros.

constructor.
induction H0.
exists 0.
split; [ constructor | ].
simpl; constructor.
constructor; assumption.
reflexivity.
induction IHclosure.
induction H2.
exists (Datatypes.S x0).
split; [ constructor | ].
simpl.
constructor; exists z; intuition.

do 3 (induction H0).
apply any_iterate_in_closure with x; assumption.
Qed.

(**
  BBook: section 3.5.10, p.169, array 2, row 2
  *)
Theorem composition_of_closure_is_closure: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  closure s r = composition (closure s r) (closure s r).
Proof.
intros S s r H.
apply Extensionality_Ensembles; split; red; intros.

induction H0.
constructor; exists x.
split; left; assumption.
constructor; exists z.
split.
right with z.
assumption.
left.
do 2 (induction H); generalize (H _ H0); intros H2; inversion H2; assumption.
assumption.

do 3 (induction H0).
rewrite closure_QuantifiedUnion_of_iterates in H0, H1 |- *; [ | assumption | assumption | assumption ].
inversion H0; inversion H1.
do 2 (induction H2).
do 2 (induction H4).
constructor; exists (x0 + x1).
split; [ constructor | ].
rewrite <- iterate_addition; [ | assumption ].
constructor; exists x; split; assumption.
Qed.

(**
  BBook: section 3.5.10, p.169, array 2, row 3
  *)
Theorem closure_of_closure_is_closure: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  closure s r = closure s (closure s r).
Proof.
intros S s r H.
apply Extensionality_Ensembles; split; red; intros.

induction H0.
left; assumption.
right with z.
right with z.
assumption.
left.
do 2 (induction H); generalize (H _ H0); intros H2; inversion H2; assumption.
assumption.

induction H0.
left; assumption.
rewrite composition_of_closure_is_closure; [ | assumption ].
constructor; exists z; split; assumption.
Qed.

Theorem closure_relation: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  In (relation s s) (closure s r).
Proof.
intros S s r H.
do 2 constructor; red; intros.
induction H0.
constructor; assumption.
do 2 (induction H); generalize (H _ H0); intros H2.
inversion H2; inversion IHclosure; constructor; assumption.
Qed.

(** Note/TODO: move this in Bnaturals_iterate *)
Theorem iterate_id: forall (S: Type) (s: Ensemble S) (n: nat), iter s (id s) n = id s.
Proof.
intros S s n.
induction n.
simpl; reflexivity.
simpl.
rewrite IHn.
rewrite Equality_laws_compose_05 with S S s s (id s).
reflexivity.
do 2 constructor; red; intros.
induction H; assumption.
Qed.

Theorem closure_id: forall (S: Type) (s: Ensemble S), closure s (id s) = id s.
Proof.
intros S s.
apply Extensionality_Ensembles; split; red; intros.
induction H.
constructor.
constructor; assumption.
reflexivity.
inversion IHclosure.
rewrite <- H4; assumption.
induction H.
rewrite H0 in H |- *; left; inversion H; assumption.
Qed.

(**
  BBook: section 3.5.10, p.169, array 2, row 4
  *)
Theorem closure_inverse: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  inverse (closure s r) = closure s (inverse r).
Proof.
intros S s r H.
apply Extensionality_Ensembles; split; red; intros.

induction H0.
rewrite closure_QuantifiedUnion_of_iterates in H0; [ | assumption ].
rewrite closure_QuantifiedUnion_of_iterates ; [ | apply Membership_laws_01; assumption ].
inversion H0.
do 2 (induction H1).
constructor.
exists x; split; [ constructor | ].
rewrite <- iterate_inverse; [ | assumption ].
constructor; assumption.

induction H0.
constructor.
left; assumption.
inversion IHclosure.
constructor.
rewrite composition_of_closure_is_closure; [ | assumption ].
constructor; exists z.
split; [ assumption | ].
assert (In s x).
assert (In (relation s s) (inverse r)).
apply Membership_laws_01; assumption.
do 2 (induction H5); generalize (H5 _ H0); intros H6; inversion H6; assumption.
right with x.
inversion H0; assumption.
left; assumption.
Qed.

(**
  BBook: section 3.5.10, p.169, array 2, row 5
  *)
Theorem closure_inclusion: forall (S: Type) (s: Ensemble S) (p q: Ensemble (S*S)), In (relation s s) p -> In (relation s s) q ->
  Included p q -> Included (closure s p) (closure s q).
Proof.
intros S s p q H H0 H1.
red; intros.
induction H2.
left; assumption.
right with z.
apply H1; assumption.
assumption.
Qed.

(**
  BBook: section 3.5.10, p.169, array 2, row 6
  *)
Theorem closure_image_as_fixpoint: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  (forall (a: Ensemble S), Included a s -> image (closure s r) a = Bfix s (lambda (Power_set s) (fun x => (Union a (image r x))))).
Proof.
intros S s r H a H0.
apply Extensionality_Ensembles; split; red; intros.

(** Note:This proof is weirdly complicated, probably for two reasons : the necessary use of inversion
  and induction at the same time for x, and the lack of particular theorems for the fixpoint expression.
  *)
do 2 (destruct H1).
replace y with (snd (x,y)); [ | simpl; reflexivity ].
replace x with (fst (x,y)) in H1; [ | simpl; reflexivity ].
generalize (x,y) H1 H2.
intros.
rewrite closure_QuantifiedUnion_of_iterates in H4; [ | assumption ].
do 3 (destruct H4).
move H3 after x0.
clear H4.
generalize y0 H3 H5; clear H3 H5.
induction x0.
intros.
simpl in H5.
induction H5.
simpl.
rewrite <- H5; do 2 constructor; intros x1 H6; induction H6; apply H6.
apply abstract_lambda; left; assumption.
intros.
rewrite iterate_successor in H5; [ | assumption ].
destruct H5.
do 2 (destruct H4).
simpl; do 2 constructor; intros x2 H6; induction H6; apply H6.
apply abstract_lambda; right.
apply image_intro with x1; split; [ | assumption ].
generalize (IHx0 (a0, x1) H3 H4).
simpl; intros.
assert (In (total_function (Power_set s) (Power_set s)) (lambda (Power_set s) (fun x => (Union a (image r x))))).
apply lambda_is_total.
intros.
destruct H8.
constructor; red; intros.
induction H9.
apply H0; assumption.
do 2 (destruct H9).
do 2 (induction H).
generalize (H _ H10); intros H11; inversion H11; assumption.
assert (Included x2 s).
inversion x3.
inversion H8.
rewrite H12 in H10.
inversion H10; assumption.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ x2 x3 (fixpoint_applicability S s x2 (lambda (Power_set s) (fun x => (Union a (image r x)))) H9 H8)) in H6.
generalize (fix_lower_bound S s x2 (lambda (Power_set s) (fun x => (Union a (image r x)))) H8 H9 H6).
intros.
apply H10; assumption.

induction H1.
induction H1.
apply H1.
do 2 red.
assert (In (total_function (Power_set s) (Power_set s)) (lambda (Power_set s) (fun x => (Union a (image r x))))).
apply lambda_is_total.
intros; constructor; red; intros.
induction H3.
apply H0; assumption.
induction H3.
induction H3.
do 2 induction H.
generalize (H _ H4); intros.
inversion H5; assumption.
assert (
  In (partial_function (Power_set s) (Power_set s)) (lambda (Power_set s) (fun x => (Union a (image r x))))
  /\ In (domain (lambda (Power_set s) (fun x => (Union a (image r x))))) (image (closure s r) a)
).
split; [ induction H2; assumption | ].
induction H2; rewrite H3.
constructor; red; intros.
induction H4.
induction H4.
assert (In (relation s s) (closure s r)).
apply closure_relation; assumption.
do 2 (induction H6).
generalize (H6 _ H5); intros H7; inversion H7; assumption.
exists H3.
apply abstract_lambda.
rewrite <- Equality_laws_image_01 with S S S s s s (closure s r) r a.
red; intros.
induction H4.
apply image_intro with x0.
split; [ assumption | left; apply H0; assumption ].
do 2 (destruct H4).
inversion H5.
do 2 (destruct H7).
apply image_intro with x0.
split; [ assumption | ].
rewrite composition_of_closure_is_closure; [ | assumption ].
constructor; exists x1; split.
assumption.
right with y; [ assumption | ].
left; do 2 (induction H); generalize (H _ H9); intros H10; inversion H10; assumption.
split; [ | assumption ].
split; [ apply closure_relation; assumption | assumption ].
Qed.

(**
  BBook: section 3.5.10, p.169, array 2, row 7
  *)
Theorem closure_image_reversed: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  (forall (a: Ensemble S), Included a s -> image (closure s r) a = Union a (image (composition (closure s r) r) a)).
Proof.
intros S s r H a H0.
apply Extensionality_Ensembles; split; red; intros.

do 2 (destruct H1).
replace x with (fst (x,y)) in H1; [ | simpl; reflexivity ].
replace y with (snd (x,y)); [ | simpl; reflexivity ].
move H1 after H2.
rewrite closure_QuantifiedUnion_of_iterates in H2; [ | assumption ].
do 3 (destruct H2).
generalize y0 H1 H3; clear H2 H3 H1.
induction x0.
intros.
induction H3.
rewrite <- H3 in H1 |- *.
left; simpl in H1 |- *; assumption.
intros.
rewrite iterate_successor in H3; [ | assumption ].
destruct H3.
do 2 (destruct H2).
right.
generalize (IHx0 (a0,x1) H1 H2); intros.
simpl in H1, H4 |- *.
apply image_intro with a0.
split; [ assumption | ].
constructor; exists x1.
split; [ | assumption ].
apply any_iterate_in_closure with x0; assumption.

induction H1.
apply image_intro with x.
split; [ assumption | left; apply H0; assumption ].
do 2 (destruct H1).
inversion H2.
do 2 (destruct H4).
apply image_intro with x.
split; [ assumption | ].
rewrite composition_of_closure_is_closure; [ | assumption ].
constructor; exists x0; split; [ assumption | ].
right with y; [ assumption | ].
left.
do 2 (induction H); generalize (H _ H6); intros H7; inversion H7; assumption.
Qed.

(**
  BBook: section 3.5.10, p.169, array 2, row 8
  *)
Theorem closure_image_QuantifiedUnion_images: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  (forall (a: Ensemble S), Included a s -> image (closure s r) a = QuantifiedUnion (Full_set nat) (fun n => image (iter s r n) a)).
Proof.
intros S s r H a H0.
rewrite closure_QuantifiedUnion_of_iterates; [ | assumption ].
apply Extensionality_Ensembles; split; red; intros.

do 2 (destruct H1).
inversion H2.
do 2 (destruct H3).
constructor; exists x0.
split; [ constructor | ].
apply image_intro with x.
split; assumption.

do 3 (destruct H1).
do 2 (destruct H2).
apply image_intro with x0.
split; [ assumption | ].
constructor; exists x.
split; [ constructor | assumption ].
Qed.

(**
  BBook: section 3.5.10, p.169, array 2, row 9
  *)
Theorem image_inclusion_implies_equality_of_image_of_closure: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  (forall (a: Ensemble S), Included a s -> Included (image r a) a -> image (closure s r) a = a).
Proof.
intros S s r H a H0 H1.
apply Extensionality_Ensembles; split; red; intros.

do 2 (destruct H2).
replace x with (fst (x,y)) in H2; [ | simpl; reflexivity ].
replace y with (snd (x,y)); [ | simpl; reflexivity ].
move H2 after H3.
generalize (x,y) H3 H2; clear H2 H3.
intros.
induction H3.
simpl in H2 |- *; assumption.
simpl in IHclosure, H2 |- *.
apply IHclosure.
apply H1.
apply image_intro with x0.
intuition.

rewrite closure_image_reversed; [ | assumption | assumption ].
left; assumption.
Qed.

(**
  *** Irreflexive transitive closure: properties
  *)

Theorem positive_iterate_in_closure1: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  (forall (n:nat) (x: S*S), n > 0 -> In (iter s r n) x -> In (closure1 s r) x).
Proof.
intros S s r H n x.
destruct n; intros H0.
inversion H0.
generalize x.
induction n.
rewrite iterate_one; [ | assumption ]; intros.
destruct x0; left; assumption.
intros.
induction H1.
do 2 (destruct H1).
right with x0; [ assumption | ].
apply IHn.
intuition.
assumption.
Qed.

(**
  property in the text, BBook: section 3.5.10, p.169
  *)
Theorem closure1_expansion: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  closure1 s r = Union r (composition r (closure1 s r)).
Proof.
intros S s r H.
apply Extensionality_Ensembles; split; red; intros.

induction H0.
left; assumption.
right; constructor; exists z.
intuition.

induction H0.
destruct x; left; assumption.
do 3 (induction H0).
right with x; assumption.
Qed.

(**
  BBook: section 3.5.10, p.170, array 1, row 1
  *)
Theorem closure1_QuantifiedUnion_of_iterates: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  closure1 s r = QuantifiedUnion (fun n => n > 0) (fun n => iter s r n).
Proof.
intros S s r H.
apply Extensionality_Ensembles; split; red; intros.

constructor.
induction H0.
exists 1.
split; [ constructor | ].
rewrite iterate_one; assumption.
induction IHclosure1.
induction H2.
exists (Datatypes.S x0).
do 2 (red in H2).
split.
apply le_gt_S.
apply lt_le_weak; assumption.
simpl.
constructor; exists z; intuition.

do 3 (induction H0).
apply positive_iterate_in_closure1 with x; assumption.
Qed.

(**
  BBook: section 3.5.10, p.170, array 1, row 2
  *)
Theorem composition_of_closure1_included_in_closure1: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  Included (composition (closure1 s r) (closure1 s r)) (closure1 s r).
Proof.
intros S s r H.
red; intros.

do 3 (induction H0).
rewrite closure1_QuantifiedUnion_of_iterates in H0, H1 |- *; [ | assumption | assumption | assumption ].
inversion H0; inversion H1.
do 2 (induction H2).
do 2 (induction H4).
constructor; exists (x0 + x1).
split.
do 2 (red in H2, H4).
do 2 red.
auto with arith.
rewrite <- iterate_addition; [ | assumption ].
constructor; exists x; split; assumption.
Qed.

(**
  BBook: section 3.5.10, p.170, array 1, row 3
  *)
Theorem closure1_of_closure1_is_closure1: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  closure1 s r = closure1 s (closure1 s r).
Proof.
intros S s r H.
apply Extensionality_Ensembles; split; red; intros.

induction H0.
left; left; assumption.
right with z.
left; assumption.
assumption.

induction H0.
assumption.
apply composition_of_closure1_included_in_closure1; [ assumption | ].
constructor; exists z; intuition.
Qed.

Theorem closure1_relation: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  In (relation s s) (closure1 s r).
Proof.
intros S s r H.
do 2 constructor; red; intros.
induction H0.
do 2 (induction H); generalize (H _ H0); intros H1; inversion H1; constructor; assumption.
do 2 (induction H); generalize (H _ H0); intros H2.
inversion H2; inversion IHclosure1; constructor; assumption.
Qed.

Theorem closure1_id: forall (S: Type) (s: Ensemble S), closure1 s (id s) = id s.
Proof.
intros S s.
apply Extensionality_Ensembles; split; red; intros.
induction H.
assumption.
inversion IHclosure1.
rewrite <- H4; assumption.
induction H.
left; constructor; assumption.
Qed.

(**
  BBook: section 3.5.10, p.170, array 1, row 4
  *)
Theorem closure1_inverse: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  inverse (closure1 s r) = closure1 s (inverse r).
Proof.
intros S s r H.
apply Extensionality_Ensembles; split; red; intros.

induction H0.
rewrite closure1_QuantifiedUnion_of_iterates in H0; [ | assumption ].
rewrite closure1_QuantifiedUnion_of_iterates ; [ | apply Membership_laws_01; assumption ].
inversion H0.
do 2 (induction H1).
constructor.
exists x; split; [ assumption | ].
rewrite <- iterate_inverse; [ | assumption ].
constructor; assumption.

induction H0.
constructor.
left; inversion H0; assumption.
inversion IHclosure1.
constructor.
apply composition_of_closure1_included_in_closure1; [ assumption | ].
constructor; exists z.
split; [ assumption | ].
left; inversion H0; assumption.
Qed.

(**
  BBook: section 3.5.10, p.170, array 1, row 5
  *)
Theorem closure1_inclusion: forall (S: Type) (s: Ensemble S) (p q: Ensemble (S*S)), In (relation s s) p -> In (relation s s) q ->
  Included p q -> Included (closure1 s p) (closure1 s q).
Proof.
intros S s p q H H0 H1.
red; intros.
induction H2.
left; apply H1; assumption.
right with z.
apply H1; assumption.
assumption.
Qed.

(**
  BBook: section 3.5.10, p.170, array 1, row 6
  *)
Theorem closure1_image_as_fixpoint: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  (forall (a: Ensemble S), Included a s -> image (closure1 s r) a = Bfix s (lambda (Power_set s) (fun x => (Union (image r a) (image r x))))).
Proof.
intros S s r H a H0.
apply Extensionality_Ensembles; split; red; intros.

(** Note: same note as the corresponding proof for closure *)
do 2 (destruct H1).
replace y with (snd (x,y)); [ | simpl; reflexivity ].
replace x with (fst (x,y)) in H1; [ | simpl; reflexivity ].
generalize (x,y) H1 H2.
intros.
rewrite closure1_QuantifiedUnion_of_iterates in H4; [ | assumption ].
do 3 (destruct H4).
move H3 after x0.
destruct x0.
inversion H4.
generalize y0 H3 H5; clear H3 H4 H5.
induction x0.
intros.
rewrite iterate_one in H5; [ | assumption ].
destruct y1; simpl.
do 2 constructor; intros x1 H6; induction H6.
apply H4.
apply abstract_lambda; left.
apply image_intro with s0; intuition.
intros.
rewrite iterate_successor in H5; [ | assumption ].
destruct H5.
do 2 (destruct H4).
simpl; do 2 constructor; intros x2 H6; induction H6; apply H6.
apply abstract_lambda; right.
apply image_intro with x1; split; [ | assumption ].
generalize (IHx0 (a0, x1) H3 H4).
simpl; intros.
assert (In (total_function (Power_set s) (Power_set s)) (lambda (Power_set s) (fun x => (Union (image r a) (image r x))))).
apply lambda_is_total.
intros.
destruct H8.
constructor; red; intros.
induction H9; ( do 2 (destruct H9); do 2 (induction H); generalize (H _ H10); intros H11; inversion H11; assumption).
assert (Included x2 s).
inversion x3.
inversion H8.
rewrite H12 in H10.
inversion H10; assumption.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ x2 x3 (fixpoint_applicability S s x2 (lambda (Power_set s) (fun x => (Union (image r a) (image r x)))) H9 H8)) in H6.
generalize (fix_lower_bound S s x2 (lambda (Power_set s) (fun x => (Union (image r a) (image r x)))) H8 H9 H6).
intros.
apply H10; assumption.

induction H1.
induction H1.
apply H1.
do 2 red.
assert (In (total_function (Power_set s) (Power_set s)) (lambda (Power_set s) (fun x => (Union (image r a) (image r x))))).
apply lambda_is_total.
intros; constructor; red; intros.
induction H3; ( do 2 (destruct H3); do 2 (induction H); generalize (H _ H4); intros H5; inversion H5; assumption).
assert (
  In (partial_function (Power_set s) (Power_set s)) (lambda (Power_set s) (fun x => (Union (image r a) (image r x))))
  /\ In (domain (lambda (Power_set s) (fun x => (Union (image r a) (image r x))))) (image (closure1 s r) a)
).
split; [ induction H2; assumption | ].
induction H2; rewrite H3.
constructor; red; intros.
induction H4.
induction H4.
assert (In (relation s s) (closure1 s r)).
apply closure1_relation; assumption.
do 2 (induction H6).
generalize (H6 _ H5); intros H7; inversion H7; assumption.
exists H3.
apply abstract_lambda.
rewrite <- Equality_laws_image_01 with S S S s s s (closure1 s r) r a.
red; intros.
induction H4.
do 2 (destruct H4).
apply image_intro with x0.
split; [ assumption | left; assumption ].
do 2 (destruct H4).
inversion H5.
do 2 (destruct H7).
apply image_intro with x0.
split; [ assumption | ].
apply composition_of_closure1_included_in_closure1; [ assumption | ].
constructor; exists x1; split.
assumption.
left; assumption.
split; [ | assumption ].
split; [ apply closure1_relation; assumption | assumption ].
Qed.

(**
  BBook: section 3.5.10, p.170, array 1, row 7
  *)
Theorem closure1_image_reversed: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  (forall (a: Ensemble S), Included a s -> image (closure1 s r) a = Union (image r a) (image (composition (closure1 s r) r) a)).
Proof.
intros S s r H a H0.
apply Extensionality_Ensembles; split; red; intros.

do 2 (destruct H1).
replace x with (fst (x,y)) in H1; [ | simpl; reflexivity ].
replace y with (snd (x,y)); [ | simpl; reflexivity ].
move H1 after H2.
rewrite closure1_QuantifiedUnion_of_iterates in H2; [ | assumption ].
do 3 (destruct H2).
destruct x0.
inversion H2.
generalize y0 H1 H3; clear H2 H3 H1.
induction x0.
intros.
rewrite iterate_one in H3; [ | assumption ].
left.
destruct y1; simpl in H1 |- *.
apply image_intro with s0; intuition.
intros.
rewrite iterate_successor in H3; [ | assumption ].
destruct H3.
do 2 (destruct H2).
right.
generalize (IHx0 (a0,x1) H1 H2); intros.
simpl in H1, H4 |- *.
apply image_intro with a0.
split; [ assumption | ].
constructor; exists x1.
split; [ | assumption ].
apply positive_iterate_in_closure1 with (Datatypes.S x0); [ assumption | | assumption ].
intuition.

induction H1.
do 2 (destruct H1).
apply image_intro with x.
split; [ assumption | left; assumption ].
do 2 (destruct H1).
inversion H2.
do 2 (destruct H4).
apply image_intro with x.
split; [ assumption | ].
apply composition_of_closure1_included_in_closure1; [ assumption | ].
constructor; exists x0; split; [ assumption | ].
left; assumption.
Qed.

(**
  BBook: section 3.5.10, p.170, array 1, row 8
  *)
Theorem closure1_image_QuantifiedUnion_images: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  (forall (a: Ensemble S), Included a s -> image (closure1 s r) a = QuantifiedUnion (fun n => n > 0) (fun n => image (iter s r n) a)).
Proof.
intros S s r H a H0.
rewrite closure1_QuantifiedUnion_of_iterates; [ | assumption ].
apply Extensionality_Ensembles; split; red; intros.

do 2 (destruct H1).
inversion H2.
do 2 (destruct H3).
constructor; exists x0.
split; [ assumption | ].
apply image_intro with x.
split; assumption.

do 3 (destruct H1).
do 2 (destruct H2).
apply image_intro with x0.
split; [ assumption | ].
constructor; exists x.
split; assumption.
Qed.

(**
  BBook: section 3.5.10, p.170, array 1, row 9
  *)
Theorem image_inclusion_implies_inclusion_of_image_of_closure1: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)), In (relation s s) r ->
  (forall (a: Ensemble S), Included a s -> Included (image r a) a -> Included (image (closure1 s r) a) a).
Proof.
intros S s r H a H0 H1 x H2.

do 2 (destruct H2).
replace x with (fst (x,y)) in H2; [ | simpl; reflexivity ].
replace y with (snd (x,y)); [ | simpl; reflexivity ].
move H2 after H3.
generalize (x,y) H3 H2; clear H2 H3.
intros.
induction H3.
simpl in H2 |- *; apply H1; apply image_intro with x0; intuition.
simpl in IHclosure1, H2 |- *.
apply IHclosure1.
apply H1.
apply image_intro with x0.
intuition.
Qed.

(** Note/TODO: do the various equivalences of the end of this section, namely:
  gtr = succ+
  geq = succ*
  lss = pred+
  leq = pred*
  gtr = lss~
  geq = leq~
  *)
