(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Powerset_facts.
Require Import ZArith.
Require Import Bchapter2.
Require Import Bnaturals_iterate.
Require Import Bintegers_basics.

(**
  * Iterate of a relation (total integer-consistent version)
  *)

(**
  ** Iterate (total integer-consistent version): definitions
  *)

(**
  When defining the iteration with integers, the temptation is to make the definition total, i.e.
  to define the negative iterate of a relation. When looking at the notations and the theorems for
  the "nat" iteration, the only definition that makes sense with integers would be that a negative iteration
  is the positive iterate of the inverse of the relation. This definition should be matched with the theorems
  that should not change.

  If we are to propose a new definition though, we go full way and also remove the need for a definition
  ensemble: it is taken to be the domain of the relation. This choice is confirmed when being confronted
  to the relation between composition of iterations and iteration of the addition of the arguments
  (see iteration_addition below and take 1 and -1 as examples).
  *)
Fixpoint iterz (S: Type) (r: Ensemble (S*S)) (n: Z) {struct n} :=
  match n with
  | Z0 => id (Union (domain r) (range r))
  | Zpos p => Bnaturals_iterate.iter (Union (domain r) (range r)) r (nat_of_P p)
  | Zneg p => Bnaturals_iterate.iter (Union (domain r) (range r)) (Brelations.inverse r) (nat_of_P p)
end.
Implicit Arguments iterz [S].

Fixpoint iterz' (S: Type) (r: Ensemble (S*S)) (n: Z) {struct n} :=
  match n with
  | Z0 => id (Union (domain r) (range r))
  | Zpos p => Bnaturals_iterate.iter (Union (domain r) (range r)) r (nat_of_P p)
  | Zneg p => Brelations.inverse (Bnaturals_iterate.iter (Union (domain r) (range r)) r (nat_of_P p))
  end.
Implicit Arguments iterz' [S].

Theorem any_relation_in_its_limiting_sets: forall (S: Type) (r: Ensemble (S*S)),
  In (relation (Union (domain r) (range r)) (Union (domain r) (range r))) r.
Proof.
intros S r.
do 2 constructor; red; intros x H.
destruct x as [ s t H ].
constructor.
left; constructor; exists t; assumption.
right; constructor; exists s; assumption.
Qed.

Theorem iterz_iterz': forall (S: Type) (r: Ensemble (S*S)) (n: Z), iterz r n = iterz' r n.
Proof.
intros S r n.
induction n.
simpl; reflexivity.
simpl; reflexivity.
simpl.
rewrite Bnaturals_iterate.iterate_inverse; [ reflexivity | apply any_relation_in_its_limiting_sets ].
Qed.

Theorem iterz_is_a_relation: forall (S: Type) (r: Ensemble (S*S)) (n: Z),
  In (relation (Union (domain r) (range r)) (Union (domain r) (range r))) (iterz r n).
Proof.
intros S r n.
induction n.
simpl.
specialize (Membership_laws_15 S (Union (domain r) (range r))); intros H.
do 3 induction H; assumption.
simpl; apply Bnaturals_iterate.iter_is_a_relation.
apply any_relation_in_its_limiting_sets.
rewrite iterz_iterz'; simpl.
apply Membership_laws_01; apply Bnaturals_iterate.iter_is_a_relation.
apply any_relation_in_its_limiting_sets.
Qed.

Theorem iterz_nat_of_Zpos: forall (S: Type) (r: Ensemble (S*S)) (n: positive),
  iterz r (Zpos n) = Bnaturals_iterate.iter (Union (domain r) (range r)) r (nat_of_P n).
Proof.
intros S r n.
simpl; reflexivity.
Qed.

Theorem iterz_nat_of_Z: forall (S: Type) (r: Ensemble (S*S)) (n: Z),
  n >= 0 -> exists p:nat, iterz r n = Bnaturals_iterate.iter (Union (domain r) (range r)) r p /\ n = Z_of_nat p.
Proof.
intros S r n H.
destruct n.
exists 0%nat.
simpl; intuition.
exists (nat_of_P p).
simpl.
rewrite Zpos_eq_Z_of_nat_o_nat_of_P; intuition.
specialize (Zlt_neg_0 p); contradiction.
Qed.


(**
  ** Iterate (total-integer consistent version): properties
  *)

(**
  The general guideline is the BBook, but this definition not being part of the BBook, some properties
  are likely to be different. We still try to match the organization of properties of the BBook for iteration
  as closely as possible.
  *)

(**
  *** What kind of relation iterz is, depending on the properties of the relation it is based on
  *)

Definition iterzate_relation := iterz_is_a_relation.

Theorem iterzate_partial_function_pos: forall (S: Type) (f: Ensemble (S*S)) (_: In (partial_function (Union (domain f) (range f)) (Union (domain f) (range f))) f) (n: Z),
  n >= 0 -> In (partial_function (Union (domain f) (range f)) (Union (domain f) (range f))) (iterz f n).
Proof.
intros S f H n H0.
destruct n as [ | n0 | n0 ].
simpl.
specialize (Membership_laws_15 S (Union (domain f) (range f))); intros H1.
do 2 induction H1; assumption.
simpl.
apply Bnaturals_iterate.iterate_partial_function; assumption.
specialize (Zlt_neg_0 n0); contradiction.
Qed.

Theorem iterzate_partial_function: forall (S: Type) (f: Ensemble (S*S)) (_: In (partial_injection (Union (domain f) (range f)) (Union (domain f) (range f))) f) (n: Z),
  In (partial_function (Union (domain f) (range f)) (Union (domain f) (range f))) (iterz f n).
Proof.
intros S f H n.
specialize (Zle_or_lt 0 n); intros H0.
destruct H0 as [ H0 | H0 ].
assert (n >= 0) as H1; [ intuition | apply iterzate_partial_function_pos ].
inversion H; assumption.
intuition.
destruct n.
assert (0 >= 0); [ intuition | contradiction ].
specialize (Zgt_pos_0 p); intros H1; assert (Zpos p >= 0); [ intuition | contradiction ].
rewrite iterz_iterz'; simpl.
apply Membership_laws_02.
apply Bnaturals_iterate.iterate_partial_injection.
assumption.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 2

  TODO: For it to be true in general, it would need to be something like a surjective relation,
  probably with an additional constraint.
  *)
Theorem iterzate_total_function: forall (S: Type) (s: Ensemble S) (f: Ensemble (S*S)) (_: In (total_function (Union (domain f) (range f)) (Union (domain f) (range f))) f) (n: Z),
  n >= 0 -> In (total_function (Union (domain f) (range f)) (Union (domain f) (range f))) (iterz f n).
Proof.
intros S s f H n H0.
destruct n as [ | n0 | n0 ].
simpl.
specialize (Membership_laws_15 S (Union (domain f) (range f))); intros H1.
do 2 induction H1.
constructor; assumption.
simpl; apply Bnaturals_iterate.iterate_total_function; assumption.
specialize (Zlt_neg_0 n0); contradiction.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 3
  *)
Theorem iterzate_partial_injection: forall (S: Type) (s: Ensemble S) (f: Ensemble (S*S)) (_: In (partial_injection (Union (domain f) (range f)) (Union (domain f) (range f))) f) (n: Z),
  In (partial_injection (Union (domain f) (range f)) (Union (domain f) (range f))) (iterz f n).
Proof.
intros S s f H n.
destruct n as [ | n0 | n0 ].
simpl.
specialize (Membership_laws_15 S (Union (domain f) (range f))); intros H0.
induction H0.
induction H1.
assumption.
simpl.
apply Bnaturals_iterate.iterate_partial_injection; assumption.
rewrite iterz_iterz'; simpl.
apply Membership_laws_03.
apply Bnaturals_iterate.iterate_partial_injection; assumption.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 4

  For it to be true in general, it would need to be a total injective relation, or something
  akin to it.
  *)
Theorem iterzate_total_surjection: forall (S: Type) (s: Ensemble S) (f: Ensemble (S*S)) (_: In (total_surjection (Union (domain f) (range f)) (Union (domain f) (range f))) f) (n: Z),
  n >=0 -> In (total_surjection (Union (domain f) (range f)) (Union (domain f) (range f))) (iterz f n).
Proof.
intros S s f H n H0.
destruct n as [ | n0 | n0 ].
simpl.
specialize (Membership_laws_15 S (Union (domain f) (range f))); intros H1.
induction H1.
constructor.
assumption.
induction H2; assumption.
simpl.
apply Bnaturals_iterate.iterate_total_surjection; assumption.
specialize (Zlt_neg_0 n0); contradiction.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 5
  *)
Theorem iterzate_one: forall (S: Type) (r: Ensemble (S*S)),
  (iterz r 1) = r.
Proof.
intros S r.
simpl.
apply Equality_laws_compose_05 with (Union (domain r) (range r)).
apply any_relation_in_its_limiting_sets.
Qed.

(**
  And now the (-1) superscript notation for the inverse of a relation finally makes sense !
  *)
Theorem iterzate_minus_one: forall (S: Type) (r: Ensemble (S*S)),
  (iterz r (-1)) = inverse r.
Proof.
intros S r.
simpl.
apply Equality_laws_compose_05 with (Union (domain r) (range r)).
apply Membership_laws_01.
apply any_relation_in_its_limiting_sets.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 6

  Not true in general: with n = -1, it requires proving "id (Union ...) = r~;r", which is only true if r is functional
  (see such equalities in Equalities_composition)
  *)
Theorem iterzate_successor: forall (S: Type) (r: Ensemble (S*S)) (n: Z),
  n >= 0 -> iterz r (Zsucc n) = composition (iterz r n) r.
Proof.
intros S r n H.
destruct n as [ | n0 | n0 ].
simpl.
rewrite Equality_laws_compose_05 with S S (Union (domain r) (range r)) (Union (domain r) (range r)) r.
symmetry; apply Equality_laws_compose_19 with (Union (domain r) (range r)).
apply any_relation_in_its_limiting_sets.
apply any_relation_in_its_limiting_sets.
rewrite <- Zpos_succ_morphism.
do 2 (rewrite iterz_nat_of_Zpos).
rewrite nat_of_P_succ_morphism.
apply iterate_successor.
apply any_relation_in_its_limiting_sets.
specialize (Zlt_neg_0 n0); contradiction.
Qed.

Theorem iterzate_predecessor: forall (S: Type) (r: Ensemble (S*S)) (n: Z),
  n <= 0 -> iterz r (Zpred n) = composition (iterz r n) (inverse r).
Proof.
intros S r n H.
destruct n as [ | n0 | n0 ].
simpl.
rewrite Equality_laws_compose_05 with S S (Union (domain r) (range r)) (Union (domain r) (range r)) (inverse r).
symmetry; apply Equality_laws_compose_19 with (Union (domain r) (range r)).
apply Membership_laws_01.
apply any_relation_in_its_limiting_sets.
apply Membership_laws_01.
apply any_relation_in_its_limiting_sets.
specialize (Zgt_pos_0 n0); contradiction.
simpl.
rewrite <- Pplus_one_succ_r.
rewrite nat_of_P_succ_morphism.
simpl.
apply iterate_successor.
apply Membership_laws_01.
apply any_relation_in_its_limiting_sets.
Qed.

Theorem iterzate_composition_r_commutes: forall (S: Type) (r: Ensemble (S*S)) (n: Z),
  n >= 0 -> composition (iterz r n) r = composition r (iterz r n).
Proof.
intros S r n H.
specialize (any_relation_in_its_limiting_sets S r); intros H0.
destruct n as [ | n0 | n0 ].
simpl.
rewrite Equality_laws_compose_05 with S S (Union (domain r) (range r)) (Union (domain r) (range r)) r; [ | assumption ].
apply Equality_laws_compose_19 with (Union (domain r) (range r)); assumption.
simpl.
apply iterate_composition_r_commutes; assumption.
specialize (Zlt_neg_0 n0); contradiction.
Qed.

Theorem iterzate_composition_inv_r_commutes: forall (S: Type) (r: Ensemble (S*S)) (n: Z),
  n <= 0 -> composition (iterz r n) (inverse r) = composition (inverse r) (iterz r n).
Proof.
intros S r n H.
specialize (any_relation_in_its_limiting_sets S r); intros H0.
specialize (Membership_laws_01 S S (Union (domain r) (range r)) (Union (domain r) (range r)) r H0); intros H1.
destruct n as [ | n0 | n0 ].
simpl.
rewrite Equality_laws_compose_05 with S S (Union (domain r) (range r)) (Union (domain r) (range r)) (inverse r); [ | assumption ].
apply Equality_laws_compose_19 with (Union (domain r) (range r)); assumption.
specialize (Zgt_pos_0 n0); contradiction.
simpl.
apply iterate_composition_r_commutes; assumption.
Qed.

Theorem iterzate_composition_commutative_pos: forall (S: Type) (r: Ensemble (S*S)) (m n: Z),
  m >= 0 -> n >= 0 -> (composition (iterz r m) (iterz r n) = composition (iterz r n) (iterz r m)).
Proof.
intros S r m n H H0.
destruct m as [ | m0 | m0 ].
simpl.
rewrite Equality_laws_compose_05 with S S (Union (domain r) (range r)) (Union (domain r) (range r)) (iterz r n).
apply Equality_laws_compose_19 with (Union (domain r) (range r)).
apply iterz_is_a_relation.
apply iterz_is_a_relation.
destruct n as [ | n0 | n0 ].
simpl (iterz r 0).
rewrite Equality_laws_compose_05 with S S (Union (domain r) (range r)) (Union (domain r) (range r)) (iterz r (Zpos m0)).
symmetry; apply Equality_laws_compose_19 with (Union (domain r) (range r)).
apply iterz_is_a_relation.
apply iterz_is_a_relation.
simpl.
apply iterate_composition_commutative.
apply any_relation_in_its_limiting_sets.
specialize (Zlt_neg_0 n0); contradiction.
specialize (Zlt_neg_0 m0); contradiction.
Qed.

Theorem iterzate_composition_commutative_neg: forall (S: Type) (r: Ensemble (S*S)) (m n: Z),
  m <= 0 -> n <= 0 -> (composition (iterz r m) (iterz r n) = composition (iterz r n) (iterz r m)).
Proof.
intros S r m n H H0.
destruct m as [ | m0 | m0 ].
simpl.
rewrite Equality_laws_compose_05 with S S (Union (domain r) (range r)) (Union (domain r) (range r)) (iterz r n).
apply Equality_laws_compose_19 with (Union (domain r) (range r)).
apply iterz_is_a_relation.
apply iterz_is_a_relation.
specialize (Zgt_pos_0 m0); contradiction.
destruct n as [ | n0 | n0 ].
simpl (iterz r 0).
rewrite Equality_laws_compose_05 with S S (Union (domain r) (range r)) (Union (domain r) (range r)) (iterz r (Zneg m0)).
symmetry; apply Equality_laws_compose_19 with (Union (domain r) (range r)).
apply iterz_is_a_relation.
apply iterz_is_a_relation.
specialize (Zgt_pos_0 n0); contradiction.
simpl.
apply iterate_composition_commutative.
apply Membership_laws_01.
apply any_relation_in_its_limiting_sets.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 7
  *)
Theorem iterzate_inverse: forall (S: Type) (r: Ensemble (S*S)) (n: Z),
  (inverse (iterz r n)) = (iterz (inverse r) n).
Proof.
intros S r n.
assert ((Union (domain r) (range r)) = (Union (domain (inverse r)) (range (inverse r)))).
rewrite Equality_laws_dom_02 with S S (Union (domain r) (range r)) (Union (domain r) (range r)) r.
rewrite Equality_laws_ran_02 with S S (Union (domain r) (range r)) (Union (domain r) (range r)) r.
rewrite commutativity_1; reflexivity.
apply any_relation_in_its_limiting_sets.
apply any_relation_in_its_limiting_sets.
destruct n.
simpl.
rewrite Equality_laws_inverse_03; rewrite H; reflexivity.
simpl; rewrite <- H; apply iterate_inverse.
apply any_relation_in_its_limiting_sets.
simpl.
rewrite <- H.
apply iterate_inverse.
apply Membership_laws_01; apply any_relation_in_its_limiting_sets.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 8
  *)
Theorem iterzate_addition_pos: forall (S: Type) (r: Ensemble (S*S)) (m n: Z),
  m >= 0 -> n >= 0 -> composition (iterz r m) (iterz r n) = iterz r (m+n).
Proof.
intros S r m n H H0.
destruct m as [ | m0 | m0 ].
simpl.
apply Equality_laws_compose_19 with (Union (domain r) (range r)).
apply iterz_is_a_relation.
destruct n as [ | n0 | n0 ].
simpl (iterz r 0).
rewrite Equality_laws_compose_05 with S S (Union (domain r) (range r)) (Union (domain r) (range r)) (iterz r (Zpos m0)).
simpl; reflexivity.
apply iterz_is_a_relation.
simpl.
rewrite nat_of_P_plus_morphism; apply iterate_addition.
apply any_relation_in_its_limiting_sets.
specialize (Zlt_neg_0 n0); contradiction.
specialize (Zlt_neg_0 m0); contradiction.
Qed.

Theorem iterzate_addition_neg: forall (S: Type) (r: Ensemble (S*S)) (m n: Z),
  m <= 0 -> n <= 0 -> composition (iterz r m) (iterz r n) = iterz r (m+n).
Proof.
intros S r m n H H0.
destruct m as [ | m0 | m0 ].
simpl.
apply Equality_laws_compose_19 with (Union (domain r) (range r)).
apply iterz_is_a_relation.
specialize (Zgt_pos_0 m0); contradiction.
destruct n as [ | n0 | n0 ].
simpl (iterz r 0).
rewrite Equality_laws_compose_05 with S S (Union (domain r) (range r)) (Union (domain r) (range r)) (iterz r (Zneg m0)).
simpl; reflexivity.
apply iterz_is_a_relation.
specialize (Zgt_pos_0 n0); contradiction.
simpl.
rewrite nat_of_P_plus_morphism; apply iterate_addition.
apply Membership_laws_01.
apply any_relation_in_its_limiting_sets.
Qed.

(**
  Other theorems are left as an exercise, as this kind of iteration is a bit weird. For instance, multiplication
  of iterations over Z is true for very specific case. This module was anyway an interesting example of
  introducing new operators, specifying their properties and proving them
  *)
