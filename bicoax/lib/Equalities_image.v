(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Classical_sets.
Require Import Bderived_constructs.
Require Import Brelations.
Require Import Bfunctions.

Open Scope eB_scope.

(**
  BBook: section 2.6.4, p.111, array 1, row 1
  *)
Theorem Equality_laws_image_01: forall (S T V: Type) (s: Ensemble S) (t: Ensemble T) (v: Ensemble V) (p: Ensemble (S*T)) (q: Ensemble (T*V)) (u: Ensemble S), (p ∈ (s ↔ t) ∧ q ∈ (t ↔ v) ∧ u ⊆ s ) ⇒((p;q)[u] = q[p[u]]).
Proof.
intros S T V s t v p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

inversion H0.
induction H1.
inversion H3.
inversion H5.
compute; apply image_intro with x1.
split; [ | intuition ].
compute; apply image_intro with x0.
intuition.

inversion H0.
induction H1.
inversion H1.
induction H4.
compute; apply image_intro with x1.
split; [ assumption | ].
constructor; exists x0; intuition.
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 2
  *)
Theorem Equality_laws_image_02: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S), (r ∈ (s ↔ t) ∧ r∼ ∈ (t ⇸ s) ∧ u ⊆ domain(r)) ⇒((r;r∼)[u] = u ).
Proof.
intros S T s t r u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

inversion H0.
induction H1.
inversion H3.
inversion H5.
assert (x=x0).
decompose [and] H.
inversion H11.
apply H12 with x1; [ | constructor] ; intuition.
rewrite H8; assumption.

compute; apply image_intro with x.
split; [ assumption | ].
decompose [and] H.
generalize (H2 x H0); intros H5.
inversion H5.
inversion H1.
constructor; exists x0; split; [ | constructor ]; assumption.
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 3
  *)
Theorem Equality_laws_image_03: forall (S: Type) (s: Ensemble S) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s) ⇒(id(u)[v] = u ∩ v).
Proof.
intros S s u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H1.
inversion H4.
split; [ assumption | rewrite <- H5; assumption ].

induction H0.
compute; apply image_intro with x.
split; [ assumption | constructor; [ constructor; assumption | reflexivity ] ].
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 4
  *)
Theorem Equality_laws_image_04: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ v ⊆ s) ⇒((u ◁ r)[v] = r[u ∩ v]).
Proof.
intros S T s t r u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H1.
compute; apply image_intro with x.
split; [ split; assumption | assumption ].

induction H0.
induction H0.
induction H0.
compute; apply image_intro with x.
split; [ assumption | constructor; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 5
  *)
Theorem Equality_laws_image_05: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble T), (r ∈ (s ↔ t) ∧ v ⊆ t ∧ u ⊆ s) ⇒((r ▷ v)[u] = r[u] ∩ v).
Proof.
intros S T s t r u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H1.
split; [ compute; apply image_intro with x; intuition | assumption ].

induction H0.
induction H0.
induction H0.
compute; apply image_intro with x.
split; [ assumption | constructor; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 6

  error: in the BBook, replace r[u \ v] with r[v \ u]
  *)
Theorem Equality_laws_image_06: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ v ⊆ s) ⇒((u ⩤ r)[v] = r[v ∖ u]).
Proof.
intros S T s t r u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H1.
red; apply image_intro with x.
split; [ constructor; assumption | assumption ].

induction H0.
induction H0.
induction H0.
red; apply image_intro with x.
split; [ assumption | constructor; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 7
  *)
Theorem Equality_laws_image_07: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble T), (r ∈ (s ↔ t) ∧ v ⊆ t ∧ u ⊆ s) ⇒((r ⩥ v)[u] = r[u] ∖ v).
Proof.
intros S T s t r u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H1.
constructor; [ | assumption ].
red; apply image_intro with x; intuition.

induction H0.
induction H0.
induction H0.
red; apply image_intro with x.
split; [ assumption | constructor; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 8
  *)
Theorem Equality_laws_image_08: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (u: Ensemble S), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ u ⊆ s ) ⇒((p ⥷ q)[u] = (domain(q)⩤p)[u] ∪ q[u]).
Proof.
intros S T s t p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H1.
induction H3.
left.
red; apply image_intro with x.
split; [ assumption | constructor; intuition ].
right.
red; apply image_intro with x; intuition.

induction H0; induction H0.
induction H0.
inversion H1.
red; apply image_intro with x.
split; [ assumption | constructor ].
left; intuition.
red; apply image_intro with x.
split; [ intuition | constructor ].
right; intuition.
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 9
  *)
Theorem Equality_laws_image_09: forall (S T V: Type) (s: Ensemble S) (t: Ensemble T) (v: Ensemble V) (p: Ensemble (S*T)) (q: Ensemble (S*V)) (u: Ensemble S), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ v) ∧ u ⊆ s ) ⇒((p ⊗ q)[u] = (p∼;(u◁q))).
Proof.
intros S T V s t v p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H1.
constructor; exists x.
split; constructor; assumption.

induction H0.
inversion H0.
induction H1.
inversion H1.
inversion H2.
red; apply image_intro with x.
split; [ assumption | constructor; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 10
  *)
Theorem Equality_laws_image_10: forall (S T W Z: Type) (s: Ensemble S) (t: Ensemble T) (w: Ensemble W) (z: Ensemble Z) (p: Ensemble (S*T)) (q: Ensemble (W*Z)) (u: Ensemble S) (v: Ensemble W), (p ∈ (s ↔ t) ∧ q ∈ (w ↔ z) ∧ u ⊆ s ∧ v ⊆ w) ⇒((p ∥ q)[u×v] = p[u] × q[v]).
Proof.
intros S T W Z s t w z p q u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
inversion H2.
assert ((x0,y0)=(a,b)).
rewrite H5; rewrite H6; reflexivity.
injection H10; intros.
constructor.
red; apply image_intro with a; split; [ assumption | rewrite <- H12; assumption ].
red; apply image_intro with b; split; [ assumption | rewrite <- H11; assumption ].

induction H0.
inversion H0; inversion H1.
red; apply image_intro with (x,x0).
split; [ constructor; intuition  | ].
constructor; intuition.
Qed.

(**
  property 2.6.8, BBook: section 2.6.4, p.111, array 1, row 11
  *)
Theorem Equality_laws_image_11: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (t ↔ u)) ⇒((id(s) ∥ q)[p] = (p;q)).
Proof.
intros S T U s t u p q H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H1.
inversion H4.
inversion H8.
constructor; exists y0.
split.
rewrite <- H9; rewrite H2; assumption.
assumption.

induction H0.
inversion H0.
induction H1.
red; apply image_intro with (a,x).
split; [ assumption | ].
constructor.
induction H.
repeat (induction H).
generalize (H (a,x) H1); intros.
inversion H4.
constructor; [ constructor; assumption | reflexivity ].
assumption.
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 12
  *)
Theorem Equality_laws_image_12: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (u: Ensemble S), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ u ⊆ s) ⇒((p ∪ q)[u] = p[u] ∪ q[u]).
Proof.
intros S T s t p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H1.
left; red; apply image_intro with x; split; assumption.
right; red; apply image_intro with x; split; assumption.

induction H0; inversion H0; induction H1; red; apply image_intro with x0.
split; [ assumption | left; assumption ].
split; [ assumption | right; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 13
  *)
Theorem Equality_laws_image_13: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (u: Ensemble S), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ p∼ ∈ (t ⇸ s) ∧ q∼ ∈ (t ⇸ s) ∧ u ⊆ s ∧ (p ▷ range(q)) = (q ▷ range(p))) ⇒((p ∩ q)[u] = (p[u]) ∩ (q[u])).
Proof.
intros S T s t p q u H.
decompose [and] H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H7.

induction H7.
induction H2.
inversion H7.
split; red; apply image_intro with x; split; assumption.

induction H7.
inversion H2; inversion H7.
assert (x0=x1).
assert (In (range_restriction q (range p)) (x0,x)).
rewrite <- H1.
constructor; [ intuition | constructor; exists x1; intuition ].
inversion H4.
apply H14 with x.
inversion H12; constructor; assumption.
constructor; intuition.
red; apply image_intro with x0.
split; [ intuition | split; [ intuition | rewrite H12; intuition ] ].
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 14

  also(?):
  property 2.6.9, BBook: section 2.6.4, p.113

  Note/TODO: this might also be property 2.6.9 save for the difference in the hypotheses
  *)
Theorem Equality_laws_image_14: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (u: Ensemble S), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t) ∧ p∼ ∈ (t ⇸ s) ∧ q∼ ∈ (t ⇸ s) ∧ u ⊆ s ∧ (p ▷ range(q)) = (q ▷ range(p))) ⇒((p ∖ q)[u] = (p[u]) ∖ (q[u])).
Proof.
intros S T s t p q u H.
decompose [and] H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H2.

induction H2.
induction H2.
induction H7.
constructor.
red; apply image_intro with x; intuition.
intros H9; apply H8.
inversion H9.
assert (x0=x).
assert (In (range_restriction q (range p)) (x,y)).
rewrite <- H1.
constructor; [ intuition | constructor; exists x0; intuition ].
inversion H4.
apply H14 with y; inversion H12; constructor; intuition.
rewrite <- H12; intuition.

induction H2.
inversion H2.
red; apply image_intro with x0.
split; [ intuition | ].
constructor.
intuition.
intros H10; apply H7; red; apply image_intro with x0; intuition.
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 15
  *)
Theorem Equality_laws_image_15: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (x: S) (y: T) (u: Ensemble S), (x ∈ s ∧ y ∈ t ∧ u ⊆ s ∧ x ∈ u) ⇒((Singleton (x↦y))[u]= (Singleton (y))).
Proof.
intros S T s t x y u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

induction H0.
induction H0.
inversion H1.
intuition.

induction H0.
red; apply image_intro with x.
intuition.
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 16
  *)
Theorem Equality_laws_image_16: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (x: S) (y: T) (u: Ensemble S), (x ∈ s ∧ y ∈ t ∧ u ⊆ s ∧ x ∉ u) ⇒((Singleton (x↦y))[u]= ∅).
Proof.
intros S T s t x y u H.
decompose [and] H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H2.

induction H2.
induction H2.
inversion H5.
assert (In u x); [ rewrite H7; assumption | contradiction ].

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 17
  *)
Theorem Equality_laws_image_17: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S), (r ∈ (s ↔ t) ∧ u ⊆ s ∧ (domain(r) ∩ u) = ∅) ⇒(r[u] = ∅).
Proof.
intros S T s t r u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H.
induction H0.
assert (In (Empty_set _) x).
rewrite <- H1.
split; [ constructor; exists y; intuition | intuition ].
contradiction.

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 18
  *)
Theorem Equality_laws_image_18: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s ∧ u ∩ v <> ∅) ⇒((u×t)[v] = t).
Proof.
intros S T s t u v H.
decompose [and] H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H4.
assumption.

assert (Inhabited _ (Intersection u v)).
apply not_empty_Inhabited.
assumption.
induction H4.
induction H4.
red; apply image_intro with x0.
split; [ assumption | constructor; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 19
  *)
Theorem Equality_laws_image_19: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s ∧ u ∩ v = ∅) ⇒((u×t)[v] = ∅).
Proof.
intros S T s t u v H.
decompose [and] H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H4.
assert (In (Empty_set _) x).
rewrite <- H1.
split; assumption.
contradiction.

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 20
  *)
Theorem Equality_laws_image_20: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S), (r ∈ (s ↔ t) ∧ u ⊆ s ∧ v ⊆ s) ⇒(r[u ∪ v] = r[u] ∪ r[v]).
Proof.
intros S T s t r u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
induction H0; [ left | right ]; red; apply image_intro with x; intuition.

induction H0; induction H0; red; apply image_intro with x.
split; [ left | ]; intuition.
split; [ right | ]; intuition.
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 21
  *)
Theorem Equality_laws_image_21: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S), (r ∈ (s ↔ t) ∧ r∼ ∈ (t ⇸ s) ∧ u ⊆ s ∧ v ⊆ s) ⇒(r[u ∩ v] = (r[u]) ∩ (r[v])).
Proof.
intros S T s t r u v H.
decompose [and] H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H2.

induction H2.
induction H2; induction H2.
split; red; apply image_intro with x; split; assumption.

induction H2.
inversion H2; inversion H5.
assert (x0=x1).
inversion H4.
apply H11 with x; constructor; intuition.
red; apply image_intro with x0.
split; [ split; [ intuition | rewrite H10; intuition ] | intuition ].
Qed.

(**
  BBook: section 2.6.4, p.111, array 1, row 22
  *)
Theorem Equality_laws_image_22: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S),(r ∈ (s ↔ t) ∧ r∼ ∈ (t ⇸ s) ∧ u ⊆ s ∧ v ⊆ s) ⇒(r[u ∖ v] = (r[u]) ∖ (r[v])).
Proof.
intros S T s t r u v H.
decompose [and] H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H2.

induction H2.
induction H2; induction H2.
split.
red; apply image_intro with x; split; assumption.
intros H7; apply H6.
inversion H7.
assert (x=x0).
inversion H4.
apply H11 with y; constructor; intuition.
rewrite H10; intuition.

induction H2.
inversion H2.
red; apply image_intro with x0.
split; [ constructor; [ intuition | ] | intuition ].
intros H8; apply H5; red; apply image_intro with x0.
intuition.
Qed.

(**
  BBook: section 2.6.4, p.112, array 1, row 1
  *)
Theorem Equality_laws_image_23: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (x: S), (f ∈ (s ⇸ t) ∧ x ∈ domain(f)) ⇒(exists applicable_f:(In (partial_function s t) f /\ In (domain f) x), f[(Singleton (x))] = (Singleton (app f x applicable_f))).
Proof.
intros S T s t v x H.
exists H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

induction H0.
induction H0.
induction H0.
assert (y = app v x H).
apply identifying_app; assumption.
intuition.

induction H0.
red; apply image_intro with x.
split; [ intuition | apply app_trivial_property ].
Qed.

(**
  BBook: section 2.6.4, p.112, array 1, row 2
  *)
Theorem Equality_laws_image_24: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ) ⇒(r[∅] = ∅).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
contradiction.

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.112, array 1, row 3
  *)
Theorem Equality_laws_image_25: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ) ⇒(r[domain(r)] = range(r)).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
constructor; exists x; assumption.

induction H0.
inversion H0.
red; apply image_intro with x.
split; [ constructor; exists b; assumption | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.112, array 1, row 4
  *)
Theorem Equality_laws_image_26: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ) ⇒(r∼[range(r)] = domain(r)).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
inversion H1.
constructor; exists x; assumption.

induction H0.
inversion H0.
red; apply image_intro with x.
split; [ constructor; exists a; assumption | constructor; assumption ].
Qed.


Close Scope eB_scope.
