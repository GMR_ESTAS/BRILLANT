(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Export Blib.

(*
This definition for given sets seems better to me than the one of the BBook:

- It prevents casting from these sets to subsets of INT. Indeed these sets are numerable and
  their element are distincts, meaning that there is an isomorphism with (a subset of) INT, but
  why equating to a subset of INT when it is not necessary ? The isomorphism can even
  be introduced in the refinement if it is needed.

- The Set sort of Coq has the desired property: the resulting datatype has its inhabitants different,
  and numerable. Plus Set is predicative, meaning that an algorithm for building elements of it
  exist and are different.
*)

(* Enumerated set: stuff={a,b,c,d} *)

Inductive stuffT: Set :=
 | a: stuffT
 | b: stuffT
 | c: stuffT
 | d: stuffT.

Definition stuff := Full_set stuffT.

Theorem stuff1: In stuff a.
constructor.
Qed.

(* Abstract set: A *)

Parameter AT: Set.
Definition A:= Full_set AT.

