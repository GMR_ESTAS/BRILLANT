(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter2.
Require Import Powerset_facts.
Require Import Bnaturals_basics.
Require Import Bnaturals_recfuns.
Require Import Arith.

Open Local Scope nat_scope.

(**
  * Iterate of a relation
  *)

(**
  ** Iterate: definitions
  *)

Fixpoint iter (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (n: nat) {struct n} :=
  match n with
  | O => id s
  | (S m) => composition r (iter S s r m)
  end.
Implicit Arguments iter [S].

Theorem iter_is_a_relation: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (n: nat),
  In (relation s s) r -> In (relation s s) (iter s r n).
Proof.
intros S s r n H.
induction n.
simpl.
generalize (Membership_laws_15 _ s); intros.
do 3 (induction H0); assumption.
simpl.
apply Membership_laws_07 with s; intuition.
Qed.

Section BBook_iterations.

  Variable S: Type.
  Variable s: Ensemble S.
  Variable r: Ensemble (S*S).
  Hypothesis r_relation_over_s: In (relation s s) r.

  Definition compose_r :=
    lambda (relation s s) (fun t => composition r t).


Theorem compose_r_total: In (total_function (relation s s) (relation s s)) compose_r.
Proof.
unfold compose_r; apply lambda_is_total.
intros.
do 2 constructor; red; intros.
inversion H; inversion r_relation_over_s.
inversion H1; inversion H3.
inversion H0.
do 2 (induction H9).
generalize (H7 _ H9); generalize (H5 _ H11); intros.
inversion H12; inversion H13; split; assumption.
Qed.

  Definition bbN_iter := bbN_f (Ensemble (S*S)) (relation s s) (id(s)) compose_r.

  Definition Bnat_iter := nat_f (Ensemble (S*S)) (relation s s) (id(s)) compose_r.

(**
  ** Iterate: validity
  *)

Theorem bbN_iter_total: In (total_function bbN (relation s s)) bbN_iter.
Proof.
unfold bbN_iter.
apply bbN_f_is_total.
do 2 constructor; red; intros.
induction H; assumption.
apply compose_r_total.
Qed.

Theorem bbN_iter_y: forall (n: Ensemble BIG_type), In bbN n ->
  In (partial_function bbN (relation s s)) bbN_iter /\ In (domain bbN_iter) n.
Proof.
intros n H.
generalize (bbN_iter_total); intros.
induction H0.
split; [ assumption | rewrite H1; assumption ].
Qed.

Theorem valid_iter_from_BBook: forall (n: Ensemble BIG_type) (H: In bbN n),
  iter s r (nat_of_bbN n) =
  app bbN_iter n (bbN_iter_y n H).
Proof.
intros n H.
apply identifying_app.
pattern n.
apply BBook_principle_of_mathematical_induction.
rewrite <- nat_zero_bbN_0.
simpl.
unfold bbN_iter.
rewrite bbN_f_lfp.
left; intuition.
do 2 constructor; red; intros.
inversion H0; assumption.
apply compose_r_total.
intros.
replace (nat_of_bbN (app bbN_succ n0 (bbN_succ_y n0 H0))) with
  (Datatypes.S (nat_of_bbN n0)).
simpl.
unfold bbN_iter.
rewrite bbN_f_lfp.
right.
constructor.
exists n0; split.
constructor.
constructor; apply app_trivial_property.
assumption.
constructor.
exists (iter s r (nat_of_bbN n0)).
split.
assumption.
constructor.
apply iter_is_a_relation; assumption.
reflexivity.
do 2 constructor; red; intros.
induction H2; assumption.
apply compose_r_total.
rewrite bbN_succ_nat_S.
rewrite nat_2coercions_identity; reflexivity.
assumption.
Qed.

Theorem valid_iter_from_nat: forall (n: nat),
  iter s r n =
  app bbN_iter (bbN_of_nat n) (bbN_iter_y (bbN_of_nat n) (bbN_of_nat_in_bbN n)).
Proof.
intros n.
induction n.
simpl.
apply identifying_app.
fold bbN_0.
unfold bbN_iter.
rewrite bbN_f_lfp.
left; intuition.
do 2 constructor; red; intros.
inversion H; assumption.
apply compose_r_total.
simpl.
apply identifying_app.
rewrite Union_commutative.
rewrite bbnat_succ_S_nondependent.
rewrite nat_2coercions_identity.
unfold bbN_iter.
rewrite bbN_f_lfp.
right.
constructor.
exists (bbN_of_nat n).
split.
rewrite nat_S_bbN_succ.
rewrite bbN_of_nat_of_bbN.
constructor.
constructor; apply app_trivial_property.
apply bbN_of_nat_in_bbN.
apply bbN_succ_in_bbN.
constructor.
exists (iter s r n).
split.
rewrite IHn.
apply app_trivial_property.
constructor.
apply iter_is_a_relation; assumption.
reflexivity.
do 2 constructor; red; intros.
induction H; assumption.
apply compose_r_total.
apply bbN_of_nat_in_bbN.
Qed.


Theorem Bnat_iter_total: In (total_function (Full_set nat) (relation s s)) Bnat_iter.
Proof.
unfold Bnat_iter.
apply nat_f_is_total.
do 2 constructor; red; intros.
induction H; assumption.
apply compose_r_total.
Qed.

Theorem Bnat_iter_applicable: forall (n: nat),
  In (partial_function (Full_set nat) (relation s s)) Bnat_iter /\ In (domain Bnat_iter) n.
Proof.
intros n.
generalize (Bnat_iter_total); intros.
induction H.
split; [ assumption | rewrite H0; constructor ].
Qed.

Theorem valid_Bnat_iter: forall (n: nat),
  iter s r n =
  app Bnat_iter n (Bnat_iter_applicable n).
Proof.
intros n.
apply identifying_app.
induction n.
simpl.
unfold Bnat_iter.
rewrite nat_f_lfp.
left; intuition.
do 2 constructor; red; intros.
inversion H; assumption.
apply compose_r_total.
intros.
simpl.
unfold Bnat_iter.
rewrite nat_f_lfp.
right.
constructor.
exists n; split.
constructor.
constructor.
exists (iter s r n).
split.
assumption.
constructor.
apply iter_is_a_relation; assumption.
reflexivity.
do 2 constructor; red; intros.
induction H; assumption.
apply compose_r_total.
Qed.

End BBook_iterations.

(**
  ** Iterate: properties
  *)

Definition iterate_relation := iter_is_a_relation.

(**
  BBook: section 3.5.8, p.167, array 2, row 1
  *)
Theorem iterate_partial_function: forall (S: Type) (s: Ensemble S) (f: Ensemble (S*S)) (_: In (partial_function s s) f) (n: nat),
  In (partial_function s s) (iter s f n).
Proof.
intros S s f H n.
induction n.
simpl.
generalize (Membership_laws_15 _ s); intros.
induction H0.
induction H0; assumption.
simpl.
apply Membership_laws_08 with s; intuition.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 2
  *)
Theorem iterate_total_function: forall (S: Type) (s: Ensemble S) (f: Ensemble (S*S)) (_: In (total_function s s) f) (n: nat),
  In (total_function s s) (iter s f n).
Proof.
intros S s f H n.
induction n.
simpl.
generalize (Membership_laws_15 _ s); intros.
induction H0; assumption.
simpl.
(* cannot apply Membership_laws_11 because it seems too general for this case *)
constructor.
apply Membership_laws_08 with s.
induction H; induction IHn; intuition.
rewrite (Equality_laws_dom_04 S S S s s s f (iter s f n)).
induction H; assumption.
split.
do 2 (induction H).
induction IHn; induction H2; intuition.
inversion IHn.
rewrite H1.
red; intros.
induction H3.
induction H3.
do 4 (induction H).
generalize (H _ H3); intros.
inversion H6; assumption.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 3
  *)
Theorem iterate_partial_injection: forall (S: Type) (s: Ensemble S) (f: Ensemble (S*S)) (_: In (partial_injection s s) f) (n: nat),
  In (partial_injection s s) (iter s f n).
Proof.
intros S s f H n.
induction n.
simpl.
generalize (Membership_laws_15 _ s); intros.
induction H0; induction H1; assumption.
simpl.
apply Membership_laws_09 with s; intuition.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 4
  *)
Theorem iterate_total_surjection: forall (S: Type) (s: Ensemble S) (f: Ensemble (S*S)) (_: In (total_surjection s s) f) (n: nat),
  In (total_surjection s s) (iter s f n).
Proof.
intros S s f H n.
induction n.
simpl.
generalize (Membership_laws_15 _ s); intros.
induction H0.
constructor.
assumption.
induction H1; assumption.
simpl.
apply Membership_laws_13 with s; intuition.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 5
  *)
Theorem iterate_one: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r),
  (iter s r 1) = r.
Proof.
intros S s r H.
simpl.
apply Equality_laws_compose_05 with s; assumption.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 6
  *)
Theorem iterate_successor: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (n: nat),
  iter s r (Datatypes.S n) = composition (iter s r n) r.
Proof.
intros S s r H n.
induction n.
rewrite iterate_one; [ | assumption ].
simpl.
symmetry.
apply Equality_laws_compose_19 with s; assumption.
simpl.
rewrite <- (Equality_laws_compose_03 S S S S s s s s (iter s r n) r r).
simpl in IHn.
rewrite IHn; reflexivity.
split; [ | assumption ].
split; [ assumption | ].
apply iter_is_a_relation; assumption.
Qed.

Theorem iterate_composition_r_commutes: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (n: nat),
  composition (iter s r n) r = composition r (iter s r n).
Proof.
intros S s r H n.
rewrite <- iterate_successor.
intuition.
assumption.
Qed.

Theorem iterate_composition_commutative: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (m n: nat),
  (composition (iter s r m) (iter s r n) = composition (iter s r n) (iter s r m)).
Proof.
intros S s r H.
double induction m n.
simpl; reflexivity.
intros.
simpl in H0 |- *.
rewrite Equality_laws_compose_19 with S S s s (composition r (iter s r n0)).
rewrite Equality_laws_compose_05 with S S s s (composition r (iter s r n0)).
reflexivity.
apply Membership_laws_07 with s.
split; [ | apply iter_is_a_relation ]; assumption.
apply Membership_laws_07 with s.
split; [ | apply iter_is_a_relation ]; assumption.
intros.
simpl.
rewrite Equality_laws_compose_19 with S S s s (composition r (iter s r n)).
rewrite Equality_laws_compose_05 with S S s s (composition r (iter s r n)).
reflexivity.
apply Membership_laws_07 with s.
split; [ | apply iter_is_a_relation ]; assumption.
apply Membership_laws_07 with s.
split; [ | apply iter_is_a_relation ]; assumption.
intros.
simpl.
generalize (iter_is_a_relation S s r n H); intros.
generalize (iter_is_a_relation S s r n1 H); intros.
assert (In (relation s s) (composition r (iter s r n))).
apply Membership_laws_07 with s; intuition.
assert (In (relation s s) (composition r (iter s r n1))).
apply Membership_laws_07 with s; intuition.
assert (In (relation s s) (composition (iter s r n) r)).
apply Membership_laws_07 with s; intuition.
assert (In (relation s s) (composition (iter s r n1) r)).
apply Membership_laws_07 with s; intuition.
rewrite <- Equality_laws_compose_03 with
  S S S S s s s s (iter s r n1) (composition r (iter s r n)) r; [ | intuition ].
rewrite <- Equality_laws_compose_03 with
  S S S S s s s s (iter s r n) (composition r (iter s r n1)) r; [ | intuition ].
rewrite <- iterate_composition_r_commutes; [ | intuition ].
rewrite <- iterate_composition_r_commutes; [ | intuition ].
rewrite Equality_laws_compose_03 with
  S S S S s s s s (iter s r n) r (iter s r n1); [ | intuition ].
rewrite Equality_laws_compose_03 with
  S S S S s s s s (iter s r n1) r (iter s r n); [ | intuition ].
rewrite H1.
reflexivity.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 7
  *)
Theorem iterate_inverse: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (n: nat),
  (inverse (iter s r n)) = (iter s (inverse r) n).
Proof.
induction n.
simpl.
rewrite Equality_laws_inverse_03; reflexivity.
rewrite iterate_successor; [ | assumption ].
rewrite Equality_laws_inverse_02 with S S S s s s (iter s r n) r; [ | split; [ apply iterate_relation; assumption | assumption ]].
simpl.
rewrite IHn; reflexivity.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 8
  *)
Theorem iterate_addition: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (m n: nat),
  composition (iter s r m) (iter s r n) = iter s r (m+n).
Proof.
intros S s r H m n.
induction m.
simpl.
apply Equality_laws_compose_19 with s.
apply iter_is_a_relation; assumption.
simpl.
rewrite <- IHm.
rewrite Equality_laws_compose_03 with
  S S S S s s s s (iter s r m) (iter s r n) r.
reflexivity.
generalize (iter_is_a_relation S s r n H); intros.
generalize (iter_is_a_relation S s r m H); intros.
intuition.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 9
  *)
Theorem iterate_multiplication: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (m n: nat),
  iter s (iter s r m) n = iter s r (m*n).
Proof.
intros S s r H.
double induction m n.
simpl; reflexivity.
intros.
simpl in H0.
simpl.
rewrite H0.
apply Equality_laws_compose_19 with s.
generalize (Membership_laws_15 _ s); intros.
do 3 (induction H1); assumption.
intros.
rewrite mult_0_r.
simpl; reflexivity.
intros.
generalize (H0 n1 H1); intros.
generalize (H1 (Datatypes.S n)); intros.
simpl in H2, H3 |- *.
rewrite mult_comm in H3 |- *.
simpl in H3 |- *.
rewrite H2.
rewrite <- iterate_addition; [ | assumption ].
rewrite <- iterate_addition; [ | assumption ].
rewrite <- iterate_addition; [ | assumption ].
rewrite mult_comm.
generalize (iter_is_a_relation S s r n H); intros.
generalize (iter_is_a_relation S s r n1 H); intros.
assert (In (relation s s) (composition r (iter s r n))).
apply Membership_laws_07 with s; intuition.
assert (In (relation s s) (composition r (iter s r n1))).
apply Membership_laws_07 with s; intuition.
assert (In (relation s s) (iter s r (n*n1))).
apply iter_is_a_relation; assumption.
assert (In (relation s s) (composition (iter s r n) (iter s r (n*n1)))).
apply Membership_laws_07 with s; intuition.
rewrite <- Equality_laws_compose_03 with
  S S S S s s s s (iter s r n1) (composition (iter s r n) (iter s r (n*n1))) r; [ | intuition ].
rewrite Equality_laws_compose_03 with
  S S S S s s s s (iter s r n1) (iter s r (n*n1)) (iter s r n); [ | intuition ].
rewrite Equality_laws_compose_03 with
  S S S S s s s s (iter s r n) (iter s r (n*n1)) (iter s r n1); [ | intuition ].
rewrite iterate_composition_commutative with S s r n n1.
reflexivity.
assumption.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 10
  *)
Theorem iterate_inclusion: forall (S: Type) (s: Ensemble S) (p q: Ensemble (S*S)) (_: In (relation s s) p) (_: In (relation s s) q) (n: nat),
  Included p q -> Included (iter s p n) (iter s q n).
Proof.
intros S s p q Hp Hq n H.
induction n.
simpl; intuition.
simpl.
generalize (iter_is_a_relation S s p n Hp); intros.
generalize (iter_is_a_relation S s q n Hq); intros.
apply Monotonicity_laws_10 with s s s; intuition.
Qed.

(**
  BBook: section 3.5.8, p.167, array 2, row 11

  Note/TODO: the BBook misses some details about a, for instance (easy to guess, though)
  *)
Theorem iterate_image: forall (S: Type) (s: Ensemble S) (r: Ensemble (S*S)) (_: In (relation s s) r) (a: Ensemble S) (_: Included a s) (n: nat),
  Included (image r a) a -> Included (image (iter s r n) a) a.
Proof.
intros S s r H a Ha n H0.
induction n.
simpl.
rewrite Equality_laws_image_03 with S s s a.
rewrite commutativity_2.
rewrite neutral_element_2; intuition.
intuition.
simpl.
red; intros.
inversion H1.
induction H2.
inversion H4.
induction H6.
induction H6.
apply IHn.
red; apply image_intro with x1.
split; [ | intuition ].
apply H0.
red; apply image_intro with x0.
intuition.
Qed.


