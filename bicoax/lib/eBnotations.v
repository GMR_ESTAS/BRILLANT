(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)


(* As notations have been put together with the corresponding definitions,
   this files serves as a notation reference *)

(* Logic *)
Notation "∀ x · P" := (forall x , P) (at level 200, x ident) : eB_scope.
Notation "∃ x · P" := (exists x , P) (at level 200, x ident) : eB_scope.
Notation "x ∨ y" := (x \/ y) (at level 86, left associativity) : eB_scope.
Notation "x ∧ y" := (x /\ y) (at level 81, left associativity) : eB_scope.
Notation "x ⇒ y" := (x -> y) (at level 91, no associativity): eB_scope.
Notation "x ⇔ y" := (x <-> y) (at level 96, no associativity): eB_scope.
Notation "⌉ x" := (~x) (at level 76, right associativity) : eB_scope.
Notation "'⊤'" := True (at level 5): eB_scope.
Notation "'⊥'" := False (at level 5): eB_scope.
(* The middle dot: · *)

(* Datatypes *)

(*
+ - 35
* / mod 30
^ 25
*)

(* Sets and derived constructs *)

Notation "'ℤ'" := BZ (at level 6): eB_scope.
Notation "'ℕ'" := BN (at level 6): eB_scope.
Notation "'ℕ1'" := BN1 (at level 6): eB_scope. (* or ℕ₁ *)
Notation "'INT'" := BConcreteIntegers (at level 6): eB_scope.
Notation "'NAT'" := BConcreteNaturals (at level 6): eB_scope.
Notation "'NAT1'" := BConcreteNaturals1 (at level 6): eB_scope.
Notation "x ‥ y" := (interval x y) (at level 41, no associativity): eB_scope.

Notation "x ∈ y" := (In y x) (at level 11, no associativity): eB_scope.
Notation "x ∉ y" := (NotIn y x) (at level 11, no associativity): eB_scope.
Notation "x ⊆ y" := (Included x y) (at level 11, no associativity): eB_scope.
Notation "x ⊂ y" := (Strict_Included x y) (at level 11, no associativity): eB_scope.
Notation "x ⊈ y" := (NotIncluded x y) (at level 11, no associativity): eB_scope.
Notation "x ⊄ y" := (NotStrict_Included x y) (at level 11, no associativity): eB_scope.
Notation "∅" := (Empty_set _) (at level 10, no associativity): eB_scope.

Notation "x ∪ y" := (Union x y) (at level 61, right associativity): eB_scope.
Notation "x ∩ y" := (Intersection x y) (at level 51, right associativity): eB_scope.
Notation "x ∖ y" := (Setminus x y) (at level 51, right associativity): eB_scope.

Notation "ℙ( x )" := (Power_set x) (at level 6, no associativity): eB_scope.
Notation "ℙ1( x )" := (Power_set1 x) (at level 6, no associativity): eB_scope.
Notation "'FIN' ( x )" := (Power_finite x) (at level 6, no associativity): eB_scope.
Notation "'FIN1' ( x )" := (Power_finite1 x) (at level 6, no associativity): eB_scope.

Notation "x ↦ y" := (pair x y) (at level 71, right associativity): eB_scope.
Notation "x × y" := (times x y) (at level 61, right associativity): eB_scope.

(* Relations *)

Notation "x ↔ y" := (relation x y) (at level 66, right associativity): eB_scope.
Notation "x ∼" := (inverse x) (at level 16, no associativity): eB_scope.
Notation "x ; y" := (composition x y) (at level 51, right associativity): eB_scope.
Notation "x ∘ y" := (composition y x) (at level 61, right associativity): eB_scope.
Notation "x ◁ y" := (domain_restriction y x) (at level 56, right associativity): eB_scope.
Notation "x ▷ y" := (range_restriction x y) (at level 46, right associativity): eB_scope.
Notation "x ⩤ y" := (domain_subtraction y x) (at level 56, right associativity): eB_scope.
Notation "x ⩥ y" := (range_subtraction x y) (at level 46, right associativity): eB_scope.
Notation "x ⊗ y" := (direct_product x y) (at level 51, right associativity): eB_scope.
Notation "x ∥ y" := (parallel_product x y) (at level 61, right associativity): eB_scope.
Notation "x [ y ]" := (image x y) (at level 61, right associativity): eB_scope.
Notation "x ⥷ y" := (override x y) (at level 21, right associativity): eB_scope.

(* Functions *)

Notation "x ⇸ y" := (partial_function x y) (at level 66, right associativity): eB_scope.
Notation "x ⤔ y" := (partial_injection x y) (at level 66, right associativity): eB_scope.
Notation "x ⤀ y" := (partial_surjection x y) (at level 66, right associativity): eB_scope.
Notation "x ⤗ y" := (partial_bijection x y) (at level 66, right associativity): eB_scope.
Notation "x → y" := (total_function x y) (at level 66, right associativity): eB_scope.
Notation "x ↣ y" := (total_injection x y) (at level 66, right associativity): eB_scope.
Notation "x ↠ y" := (total_surjection x y) (at level 66, right associativity): eB_scope.
Notation "x ⤖ y" := (total_bijection x y) (at level 66, right associativity): eB_scope.

