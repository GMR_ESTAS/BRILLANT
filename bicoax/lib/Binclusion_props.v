(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bbasic.

(**
  reflexivity of inclusion, BBook: section 2.1.3, p.62, array 2, row 1
  *)
Theorem inclusion_reflexivity: forall (S: Type) (s: Ensemble S), Included s s.
Proof.
intuition.
Qed.

(**
  transitivity of inclusion, BBook: section 2.1.3, p.62, array 2, row 2
  *)
Theorem inclusion_transitivity: forall (S: Type) (s t u: Ensemble S), Included s t -> Included t u -> Included s u.
Proof.
intuition.
Qed.

(**
  anti-symmetry of inclusion, BBook: section 2.1.3, p.62, array 2, row 3
  *)
Theorem inclusion_antisymmetry: forall (S: Type) (s t: Ensemble S), Included s t -> Included t s -> s=t.
Proof.
intuition.
Qed.

(**
  monotonicity of pair over inclusion, BBook: section 2.1.3, p.62, array 2, row 4, line 1
  *)
Theorem inclusion_monotonicity_pair: forall (S: Type) (s t u v: Ensemble S), Included u s -> Included v t -> Included (times u v) (times s t).
Proof.
intros S s t u v H0 H1.
unfold Included.
intros H2 H3.
induction H3.
constructor; intuition.
Qed.

(**
  monotonicity of powerset over inclusion, BBook: section 2.1.3, p.62, array 2, row 4, line 2

  also:

  property 2.1.1, BBook: section 2.1.3, p.63
  *)
Theorem inclusion_monotonicity_powerset: forall (S: Type) (s t: Ensemble S), Included s t -> Included (Power_set s) (Power_set t).
Proof.
unfold Included.
intros S s t H0 x H1.
induction H1.
constructor.
intuition.
Qed.

(**
  monotonicity of set comprehension over inclusion, BBook: section 2.1.3, p.62, array 2, row 4, line 3
  *)
Theorem inclusion_monotonicity_comprehension: forall (S: Type) (s t: Ensemble S) (P: S -> Prop), Included s t -> Included (Comprehension (fun x => In s x /\ (P x))) (Comprehension (fun x => In t x /\ (P x))).
Proof.
unfold Included.
intros S s t P H0 x H1.
elim H1; intros H2 H3.
split; [ apply H0; assumption | assumption ].
Qed.

(**
  inclusion of powerset, BBook: section 2.1.3, p.62, array 2, row 5, line 1
  *)
Theorem inclusion_subproperty: forall (S: Type) (s: Ensemble S) (P: S -> Prop), Included (Comprehension (fun x => In s x /\ (P x))) s.
Proof.
unfold Included.
intros S s P x H0.
elim H0; tauto.
Qed.

(**
  inclusion and belonging element, BBook: section 2.1.3, p.62, array 2, row 5, line 2
  *)
Theorem inclusion_belonging_transitivity: forall (S: Type) (s t: Ensemble S) (E: S), In s E -> Included s t -> In t E.
Proof.
intuition.
Qed.
