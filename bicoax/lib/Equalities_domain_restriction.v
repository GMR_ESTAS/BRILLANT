(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Bderived_constructs.
Require Import Brelations.
Require Import Bfunctions.

Open Scope eB_scope.

(**
  BBook: section 2.6.4, p.108, array 1, row 1
  *)
Theorem Equality_laws_domres_01: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ domain(r) ⊆ u) ⇒(u ◁ r = r).
Proof.
intros S T s t r u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0; assumption.

induction H.
induction x; constructor.
assumption.
apply H1; constructor; exists b; assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 2
  *)
Theorem Equality_laws_domres_02: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ r∼ ∈ (t ⇸ s)) ⇒(u ◁ r = r ▷ (r[u])).
Proof.
intros S T s t u r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
constructor.
assumption.
compute; apply image_intro with x.
split; assumption.

induction H0.
inversion H1.
constructor.
assumption.
induction H.
inversion H4.
assert (x=x0); [ | rewrite H8; intuition ].
apply H6 with y.
constructor; assumption.
constructor; intuition.
Qed.

(* This is the same as law 20 for composition *)
(**
  BBook: section 2.6.4, p.108, array 1, row 3
  *)
Theorem Equality_laws_domres_03: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (T*S)) (r: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ p ∈ (t ↔ u)) ⇒(u ◁ (r;p) = (u ◁ r);p).
Proof.
intros S T s t p r u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H3.
constructor; exists x0.
split; [ constructor; intuition | intuition ].

induction H0.
inversion H0.
induction H1.
inversion H1.
constructor; [ constructor; exists x; intuition | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 4
  *)
Theorem Equality_laws_domres_04: forall (S: Type) (s: Ensemble S) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s) ⇒(u ◁ id(v) = id(u ∩ v)).
Proof.
intros S s u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H4.
constructor; [ | assumption ].
constructor; [ split; assumption | split; [ rewrite <- H5; assumption | assumption ] ].

induction H0.
inversion H0.
induction H4; induction H5.
constructor.
constructor; [ constructor; assumption | assumption ].
assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 5
  *)
Theorem Equality_laws_domres_05: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ v ⊆ s ∧ r ∈ (s ↔ t)) ⇒(u ◁ ( v ◁ r) = (u ∩ v) ◁ r).
Proof.
intros S T s t u v r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor;[ assumption | split; assumption ].

induction H0.
induction H1.
constructor; [ | assumption ].
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 6
  *)
Theorem Equality_laws_domres_06: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (w: Ensemble T) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ w ⊆ t) ⇒(u ◁ (r ▷ w) = (u ◁ r) ▷ w).
Proof.
intros S T s t u w r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; [ | assumption ].
constructor; assumption.

induction H0.
inversion H0.
constructor; [ | assumption ].
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 7
  *)
Theorem Equality_laws_domres_07: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ v ⊆ s ∧ r ∈ (s ↔ t)) ⇒(u ◁ ( v ⩤ r) = (u ∖ v) ◁ r).
Proof.
intros S T s t u v r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; [ assumption | ].
constructor; assumption.

induction H0.
inversion H1.
constructor; [ | assumption ].
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 8
  *)
Theorem Equality_laws_domres_08: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (w: Ensemble T) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ w ⊆ t) ⇒(u ◁ (r ⩥ w) = (u ◁ r) ⩥ w).
Proof.
intros S T s t u w r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; [ | assumption ].
constructor; assumption.

induction H0.
inversion H0.
constructor; [ | assumption ].
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 9
  *)
Theorem Equality_laws_domres_09: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒(u ◁ (p ⥷ q) = (u ◁ p) ⥷ (u ◁ q)).
Proof.
intros S T s t p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
induction H3.
constructor; left.
split; [ constructor; intuition | ].
intros H5; apply (proj2 H3).
induction H5.
inversion H5.
inversion H6.
constructor; exists x; assumption.
constructor; right.
constructor; assumption.

induction H0.
induction H0.
induction H0.
inversion H0.
constructor; [ | assumption ].
constructor.
left; split; [ assumption | ].
intros H6; apply H1.
induction H6; inversion H6.
constructor; exists x.
constructor; assumption.
induction H0.
constructor; [ | assumption ].
constructor; right; assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 10
  *)
Theorem Equality_laws_domres_10: forall (S T V: Type) (s: Ensemble S) (t: Ensemble T) (v: Ensemble V) (p: Ensemble (S*T)) (q: Ensemble (S*V)) (u: Ensemble S), (u ⊆ s ∧ p ∈ (s ↔ t) ∧ q ∈ (s ↔ v)) ⇒(u ◁ (p ⊗ q) = (u ◁ p) ⊗ (u ◁ q)).
Proof.
intros S T V s t v p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; constructor; assumption.

induction H0.
inversion H0; inversion H1.
constructor; [ | assumption ].
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 11
  *)
Theorem Equality_laws_domres_11: forall (S T W Z: Type) (s: Ensemble S) (t: Ensemble T) (w: Ensemble W) (z: Ensemble Z) (p: Ensemble (S*W)) (q: Ensemble (T*Z)) (u: Ensemble S) (v: Ensemble T), (u ⊆ s ∧ v ⊆ t ∧ p ∈ (s ↔ w) ∧ q ∈ (t ↔ z)) ⇒((u×v) ◁ (p ∥ q) = ((u ◁ p) ∥ (v ◁ q))).
Proof.
intros S T W Z  s t w z p q u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
inversion H1.
assert ((x0,y0)=(a,b)).
rewrite H2; rewrite H8; reflexivity.
injection H9; intros.
constructor.
constructor; [ assumption | rewrite H11; assumption ].
constructor; [ assumption | rewrite H10; assumption ].

induction H0.
inversion H0; inversion H1.
constructor; constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 12
  *)
Theorem Equality_laws_domres_12: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒(u ◁ (p ∪ q) = (u ◁ p) ∪ (u ◁ q)).
Proof.
intros S T s t p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
left; constructor; assumption.
right; constructor; assumption.

induction H0; inversion H0.
constructor; [ | assumption ].
left; assumption.
constructor; [ | assumption ].
right; assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 13
  *)
Theorem Equality_laws_domres_13: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒(u ◁ (p ∩ q) = (u ◁ p) ∩ (u ◁ q)).
Proof.
intros S T s t p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
split; constructor; assumption.

induction H0.
inversion H0; inversion H1.
constructor; [ | assumption ].
split; [ assumption | ].
rewrite H4; rewrite <- H7; assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 14
  *)
Theorem Equality_laws_domres_14: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)) (u: Ensemble S), (u ⊆ s ∧ p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒(u ◁ (p ∖ q) = (u ◁ p) ∖ q).
Proof.
intros S T s t p q u H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; [ | assumption ].
constructor; assumption.

induction H0.
induction H0.
constructor; [ | assumption ].
constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 15
  *)
Theorem Equality_laws_domres_15: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (x: S) (y: T), (u ⊆ s ∧ x ∈ s ∧ y ∈ t ∧ x ∈ u) ⇒(u ◁ (Singleton (x↦y)) = (Singleton (x↦y))).
Proof.
intros S T s t u x y H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

induction H0.
assumption.

induction H0.
constructor; intuition.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 16
  *)
Theorem Equality_laws_domres_16: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (x: S) (y: T), (u ⊆ s ∧ x ∈ s ∧ y ∈ t ∧ x ∉ u) ⇒(u ◁ (Singleton (x↦y)) = ∅).
Proof.
intros S T s t u x y H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

induction z.
inversion H0.
assert (a=x).
inversion H3; reflexivity.
assert (In u x); [ rewrite <- H5; assumption | ].
induction H; contradiction.

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 17
  *)
Theorem Equality_laws_domres_17: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t) ∧ domain(r) ∩ u = ∅) ⇒(u ◁ r = ∅).
Proof.
intros S T s t u r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H.
assert (In (Empty_set _) x).
rewrite <- H2.
split; [ constructor; exists y; assumption | assumption ].
contradiction.

contradiction.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 18
  *)
Theorem Equality_laws_domres_18: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s) ⇒(u ◁ (v×t) = (u∩v) × t).
Proof.
intros S T s t u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H0.
constructor; [ split; assumption | assumption ].

induction H0.
induction H0.
constructor; [ constructor; assumption | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 19
  *)
Theorem Equality_laws_domres_19: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s ∧ r ∈ (s ↔ t) ) ⇒((u ∪ v) ◁ r = (u ◁ r) ∪ (v ◁ r)).
Proof.
intros S T s t r u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H1.
left; constructor; assumption.
right; constructor; assumption.

induction H0; inversion H0.
constructor; [ assumption | left; assumption ].
constructor; [ assumption | right; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 20
  *)
Theorem Equality_laws_domres_20: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s ∧ r ∈ (s ↔ t) ) ⇒((u ∩ v) ◁ r = (u ◁ r) ∩ (v ◁ r)).
Proof.
intros S T s t r u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H1.
split; constructor; assumption.

induction H0.
inversion H0; inversion H1.
assert ((x0,y)=(x1,y0)).
rewrite H4; rewrite H7; reflexivity.
injection H8; intros.
constructor.
assumption.
split; [ assumption | rewrite H10; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 21
  *)
Theorem Equality_laws_domres_21: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (u: Ensemble S) (v: Ensemble S), (u ⊆ s ∧ v ⊆ s ∧ r ∈ (s ↔ t) ) ⇒((u ∖ v) ◁ r = (u ◁ r) ∖ (v ◁ r)).
Proof.
intros S T s t r u v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H1.
split.
constructor; assumption.
intros H3; apply H2; inversion H3; assumption.

induction H0.
inversion H0.
constructor.
assumption.
constructor.
assumption.
intros H5; apply H1.
rewrite <- H4; constructor; assumption.
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 22
  *)
Theorem Equality_laws_domres_22: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ) ⇒(domain(r) ◁ r = r).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0; assumption.

induction x.
constructor;[ assumption | constructor; exists b; assumption ].
Qed.

(**
  property 2.6.6, BBook: section 2.6.4, p.108, array 1, row 23
  *)
Theorem Equality_laws_domres_23: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (v: Ensemble T), (f ∈ (s ⇸ t) ∧ v ⊆ t) ⇒(f∼[v] ◁ f = f ▷ v).
Proof.
intros S T s t f v H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
inversion H1.
induction H2.
inversion H4.
constructor.
assumption.
assert (y=x0).
induction H.
induction H.
apply H9 with x; assumption.
rewrite H8; assumption.

induction H0.
constructor.
assumption.
compute; apply image_intro with y.
split; [ assumption | constructor; assumption ].
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 24
  *)
Theorem Equality_laws_domres_24: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)) (x: S), (x ∈ s ∧ r ∈ (s ↔ t) ) ⇒((Singleton (x)) ◁ r = (Singleton (x)) × r[(Singleton (x))]).
Proof.
intros S T s t r x H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros z H0.

induction H0.
induction H1.
constructor; [ intuition | ].
compute; apply image_intro with x; intuition.

induction H0.
induction H0.
inversion H1.
induction H0.
induction H0.
constructor; [ assumption | intuition ].
Qed.

(**
  BBook: section 2.6.4, p.108, array 1, row 25
  *)
Theorem Equality_laws_domres_25: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ) ⇒(∅ ◁ r = ∅).
Proof.
intros S T s t r H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
contradiction.

contradiction.
Qed.


Close Scope eB_scope.
