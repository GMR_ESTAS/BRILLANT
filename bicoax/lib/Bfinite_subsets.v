(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter2.
Require Import Bfixpoints.
Require Import Infinite_sets.

(**
  * Finite subsets: definitions
  *)

(**
  Add an element to a subset, BBook: section 3.3, p.141, array 2, row 1

  I used the lambda notation because:
  - add is supposed to be functional
  - It makes sense for the lambda definitions to be defined with our lambda
  This remark is also valid for genfin
  *)
Definition add (S: Type) (s: Ensemble S) :=
  lambda (times s (Power_set s)) (fun c => match c with | (u,x) => Union (Singleton u) x end).
Implicit Arguments add [S].

(* Genfin *)

(**
  Genfin, BBook: section 3.3, p.141, array 2, row 2
  *)
Definition genfin (S: Type) (s: Ensemble S) :=
  lambda (Power_set (Power_set s)) (fun z => Union (Singleton (Empty_set S)) (image (add s) (times s z))).
Implicit Arguments genfin [S].

(** Just a small verification to check how this definition is used *)
Theorem verif1_chapter3_section3:
exists H:(In (partial_function (times {= 1, 2, 3 =} (Power_set {= 1, 2, 3 =})) (Power_set {= 1, 2, 3 =})) (add {= 1, 2, 3 =}) /\
               In (domain (add {= 1, 2, 3 =})) (3, {= 1 =})),
app (add {= 1, 2, 3 =}) (3, {= 1 =}) H = {= 1, 3 =}.
Proof.
assert (In (partial_function (times {= 1, 2, 3 =} (Power_set {= 1, 2, 3 =})) (Power_set {= 1, 2, 3 =})) (add {= 1, 2, 3 =}) /\
               In (domain (add {= 1, 2, 3 =})) (3, {= 1 =})).
split.
repeat constructor.
red; intros.
induction H.
constructor.
assumption.
induction x.
inversion H.
rewrite H0.
constructor.
red; intros.
induction H5.
induction H5; assumption.
induction H4.
apply H4; assumption.
intros.
inversion H; inversion H0.
rewrite H4; rewrite H8; reflexivity.
constructor.
exists ({= 3, 1 =}).
constructor.
constructor.
intuition.
constructor.
intuition.
reflexivity.
exists H.
symmetry.
apply identifying_app.
constructor.
constructor.
intuition.
constructor; intuition.
intuition.
Qed.

(** Note/TODO: do some tactics for exists, app and lambda *)

(**
  Finite, BBook: section 3.3, p.141, array 3, row 1
  *)
Definition Power_finite (S:Type) (s: Ensemble S) := Approximant S s.
Implicit Arguments Power_finite [S].
Notation "'FIN' ( x )" := (Power_finite x) (at level 6, no associativity): eB_scope.

(**
  Finite1, BBook: section 3.3, p.141, array 3, row 2
  *)
Definition Power_finite1 (U:Type) (A: Ensemble U) := Subtract (Ensemble U) (Power_finite A) (Empty_set U).
Implicit Arguments Power_finite1 [U].
Notation "'FIN1' ( x )" := (Power_finite1 x) (at level 6, no associativity): eB_scope.

(**
  * Finite subsets: validity
  *)

(** Shorthand notation *)
Definition Bfinite (S:Type) (s: Ensemble S) := Bfix (Power_set s) (genfin s).
Definition Bfinite1 (S:Type) (s: Ensemble S) := Setminus (Bfinite S s) (Singleton (Empty_set S)).


Theorem Bfinite_is_total: forall (S: Type) (s: Ensemble S),
  (In (total_function (Power_set (Power_set s)) (Power_set (Power_set s))) (genfin s)).
Proof.
intros S s.
unfold genfin; apply lambda_is_total.
intros x H.
constructor; red; intros z H0.
induction H0.
induction H0.
constructor; intros z H0; contradiction.
induction H0.
inversion H0.
inversion H2.
induction x0.
constructor.
rewrite H6; red; intros z H7.
induction H7.
induction H7; inversion H1; assumption.
inversion H5.
induction H11; apply H11; assumption.
Qed.


Theorem Bfinite_included_Power_finite: forall (S: Type) (s: Ensemble S),
Included (Bfinite S s) (Power_finite s).
Proof.
intros S s.
intros x H.
assert (In (total_function (Power_set (Power_set s)) (Power_set (Power_set s))) (genfin s)).
apply Bfinite_is_total.

apply induction_principle with (Ensemble S) (Power_set s) (genfin s).
assumption.
assert (In (partial_function (Power_set (Power_set s)) (Power_set (Power_set s))) (genfin s) /\ In (domain (genfin s)) (Comprehension (fun x => In (Bfix (Power_set s) (genfin s)) x /\ In (Power_finite s) x))).
split.
induction H0; assumption.
constructor.
exists (Union (Singleton (Empty_set S)) (image (add s) (times s (Comprehension (fun x => In (Bfix (Power_set s) (genfin s)) x /\ In (Power_finite s) x))))).
constructor; [ | reflexivity ].
constructor; intros z H1.
induction H1.
apply (fix_included_in_its_base_set (Ensemble S) (Power_set s) (genfin s) H0).
assumption.
exists H1.
assert (app (genfin s) (Comprehension (fun x => (In (Bfix (Power_set s) (genfin s)) x /\ In (Power_finite s) x))) H1
 =   (Union (Singleton (Empty_set S)) (image (add s) (times s (Comprehension (fun x => In (Bfix (Power_set s) (genfin s)) x /\ In (Power_finite s) x)))))).
symmetry; apply identifying_app.
constructor.
inversion H0.
inversion H1.
rewrite <- H3; intuition.
reflexivity.
rewrite H2.
intros z H3.
induction H3.
induction H3.
constructor.
constructor.
constructor.
intros.
inversion H3.
apply H4.
assert (app (genfin s) y x0 = (Union (Singleton (Empty_set S)) (image (add s) (times s y)))).
inversion H1.
symmetry; apply identifying_app.
constructor.
inversion x0; inversion H0.
rewrite <- H10; assumption.
reflexivity.
rewrite H5; left; intuition.
constructor.
constructor.
red; intros; contradiction.
inversion H3.
induction x1.
induction H4.
inversion H4.
inversion H6.
constructor.
constructor.
constructor.
intros.
inversion H15.
apply H16.
assert (app (genfin s) y1 x2 = (Union (Singleton (Empty_set S)) (image (add s) (times s y1)))).
inversion H1.
inversion H17.
apply H20 with y1; [ apply app_trivial_property | ].
constructor.
inversion x2.
inversion H0.
rewrite <- H25; assumption.
reflexivity.
rewrite H17.
right.
rewrite H14.
red; apply image_intro with (a,b).
split.
constructor; [ assumption | ].
inversion H10.
inversion H18.
inversion H20.
apply H22; assumption.
constructor; [ assumption | reflexivity ].
constructor.
(* SC: an interesting difference between definitions of finite sets *)
assert (In b a \/ ~(In b a)); [ apply classic | ].
induction H15.
assert (Union b (Singleton a) = Union (Singleton a) b).
intuition.
assert (x0 = b).
rewrite H14.
rewrite <- H16; apply absorption_4.
red; intros.
induction H17; assumption.
rewrite H17.
inversion H10; inversion H19; assumption.
cut (x0 = (Add S b a)); intros.
rewrite H16.
constructor.
inversion H10; inversion H18; assumption.
assumption.
rewrite H14.
apply Extensionality_Ensembles; unfold Same_set; split.
red; intros.
induction H16.
right; assumption.
left; assumption.
red; intros.
induction H16.
right; assumption.
left; assumption.
inversion H13.
inversion H18.
rewrite H14.
red; intros.
induction H21.
induction H21; assumption.
apply H19; assumption.

constructor.
induction H.
assumption.
Qed.

(**
  This proof alone show how much the Coq inductive definition of finite sets is easier
  to manipulate than the B inductive definition
  *)
Theorem Power_finite_included_Bfinite: forall (S: Type) (s: Ensemble S),
Included (Power_finite s) (Bfinite S s).
Proof.
intros S s.
intros x H.
induction H.
generalize (Bfinite_is_total S s); intros.
induction H.

constructor; constructor.
intros.
induction H.
apply H.
assert (app (genfin s) y x = (Union (Singleton (Empty_set S)) (image (add s) (times s y)))).
symmetry; apply identifying_app.
constructor.
induction H1.
inversion x; rewrite <- H2; intuition.
reflexivity.
rewrite H2.
left; intuition.

constructor; constructor.
intros.
induction H3.
apply H3.
assert (app (genfin s) y x0 = (Union (Singleton (Empty_set S)) (image (add s) (times s y)))).
symmetry; apply identifying_app.
constructor.
induction H1.
inversion x0; rewrite <- H4; intuition.
reflexivity.
rewrite H4.
right.
red; apply image_intro with (x,A).
split.
constructor.
apply H0; right; intuition.
assert (Included A s).
red; intros; apply H0.
left; assumption.
generalize (IHFinite H5); intros.
induction H6.
inversion H6.
apply H7.
exists x0; assumption.
constructor.
constructor.
apply H0; right; intuition.
constructor; red; intros; apply H0.
left; assumption.
apply Extensionality_Ensembles; split.
red; intros.
induction H5.
right; assumption.
left; assumption.
red; intros.
induction H5.
right; assumption.
left; assumption.
Qed.

(**
  Finite, BBook: section 3.3, p.141, array 3, row 1
  *)
Theorem valid_Power_finite: forall (S: Type) (s: Ensemble S),
Power_finite s = Bfinite S s.
Proof.
intros S s.
apply (Extensionality_Ensembles _ (Power_finite s) (Bfinite S s)); split.
apply Power_finite_included_Bfinite.
apply Bfinite_included_Power_finite.
Qed.

(**
  Finite1, BBook: section 3.3, p.141, array 3, row 2
  *)
Theorem valid_Power_finite1: forall (S: Type) (s: Ensemble S),
Power_finite1 s = Bfinite1 S s.
Proof.
intros S s.
apply (Extensionality_Ensembles _ (Power_finite1 s) (Bfinite1 S s)); split.

red; intros.
induction H.
constructor.
apply Power_finite_included_Bfinite; assumption.
intuition.

red; intros.
induction H.
constructor.
apply Bfinite_included_Power_finite; assumption.
intuition.
Qed.

(**
  * Finite subsets: properties
  *)

(**
  Because we showed the equivalence between the Coq version and the B version,
  we use the Coq version from now on
  *)

(**
  property 3.3.1, BBook: section 3.3, p.142
  *)
Theorem empty_in_finite_sets: forall (S: Type) (s: Ensemble S),
  In (Power_finite s) (Empty_set S).
Proof.
intros S s.
constructor.
left.
red; intros; contradiction.
Qed.


(**
  property 3.3.2, BBook: section 3.3, p.142
  *)
Theorem augmented_set_in_finite_sets: forall (S: Type) (s: Ensemble S),
  forall (u: S) (x: Ensemble S), In (times s (Power_finite s)) (u,x) -> In (Power_finite s) (Union (Singleton u) x).
Proof.
intros S s u x H.
constructor.
assert (In x u \/ ~(In x u)); [ apply classic | ].
induction H0.
replace (Union (Singleton u) x) with x.
inversion H; inversion H4; assumption.
apply Extensionality_Ensembles; split.
red; intros z H1; right; assumption.
red; intros z H1.
induction H1.
induction H1; assumption.
assumption.
assert ( Union (Singleton u) x = Add S x u).
apply Extensionality_Ensembles; split.
red; intros z H1; induction H1.
right; assumption.
left; assumption.
red; intros z H1; induction H1.
right; assumption.
left; assumption.
rewrite H1; constructor.
inversion H.
inversion H5; assumption.
assumption.
red; intros z H0.
inversion H.
inversion H4.
inversion H0.
induction H7; assumption.
apply H6; assumption.
Qed.


(**
  theorem 3.3.1, BBook: section 3.3, p.142
  *)
Theorem finite_sets_induction_principle: forall (S: Type) (s: Ensemble S) (P: Ensemble S -> Prop),
  P (Empty_set S) ->
  (forall (x: Ensemble S), (In (Power_finite s) x /\ P x) ->  (forall (u:S), In s u -> P (Union (Singleton u) x))) ->
  forall (x: Ensemble S), In (Power_finite s) x -> P x.
Proof.
intros S s P base_case ind_hyp.
intros x H.
induction H.
induction H.
assumption.
assert ( Union (Singleton x) A = Add S A x).
apply Extensionality_Ensembles; split.
red; intros z H2; induction H2.
right; assumption.
left; assumption.
red; intros z H2; induction H2.
right; assumption.
left; assumption.
rewrite <- H2; apply ind_hyp.
split.
constructor.
assumption.
red; intros z H3; apply H0.
left; assumption.
apply IHFinite; red; intros z H3; apply H0.
left; assumption.
apply H0; right; intuition.
Qed.

(**
  property 3.3.3, BBook: section 3.3, p.142
  *)
Theorem finite_union: forall (S: Type) (s: Ensemble S),
  forall (x y: Ensemble S), In (times (Power_finite s) (Power_finite s)) (x,y) -> In (Power_finite s) (Union x y).
Proof.
intros S s.
intros x y H.
inversion H.
inversion H2; inversion H3.
constructor.
apply Union_preserves_Finite; assumption.
red; intros z H8; induction H8.
apply H5; assumption.
apply H7; assumption.
Qed.


(**
  property 3.3.4, BBook: section 3.3, p.142
  *)
Theorem finite_intersection: forall (S: Type) (s: Ensemble S),
  forall (x y: Ensemble S), In (times (Power_finite s) (Power_finite s)) (x,y) -> In (Power_finite s) (Intersection x y).
Proof.
intros S s.
intros x y H.
inversion H.
inversion H2; inversion H3.
constructor.
apply Intersection_preserves_finite; assumption.
red; intros z H8; induction H8.
apply H5; assumption.
Qed.

(**
  property 3.3.5, BBook: section 3.3, p.142
  *)
Theorem finite_image: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)),
  In (partial_function s t) f -> forall (x: Ensemble S), In (Power_finite s) x -> In (Power_finite t) (image f x).
Proof.
intros S T s t f H x H0.
induction H0.
induction H0.

constructor.
assert (image f (Empty_set _) = Empty_set _).
apply Extensionality_Ensembles; split.
red; intros z H2.
induction H2.
induction H0; contradiction.
red; intros z H2; contradiction.
rewrite H0; left.
red; intros x H2.
inversion H2.
induction H0; contradiction.

constructor.
assert (In (domain f) x \/ ~(In (domain f) x)); [ apply classic | ].
induction H3.
(* x in (domain f) *)
inversion H3.
inversion H4.
assert (In (image f A) x0 \/ ~(In (image f A) x0)); [ apply classic | ].
induction H7.
(* x in (domain f) and f(x) in f[A] *)
assert (image f (Add S A x) = image f A).
apply Extensionality_Ensembles; split.
red; intros z H8.
inversion H8.
induction H9.
inversion H9.
red; apply image_intro with x1; intuition.
induction H10.
induction H12.
assert (y = x0).
induction H.
apply H10 with x; assumption.
rewrite H10; assumption.
red; intros z H8.
inversion H8.
induction H9.
red; apply image_intro with x1.
split; [ left; assumption | assumption ].
rewrite H8.
assert (Included A s).
red; intros z H9; apply H1.
left; assumption.
generalize (IHFinite H9); intros H10.
induction H10; assumption.
(* x in domain f and f(x) not in f[A] *)
assert (image f (Add S A x) = Add T (image f A) x0).
apply Extensionality_Ensembles; split.
red; intros z H8.
inversion H8.
induction H9.
inversion H9.
left.
red; apply image_intro with x1; intuition.
induction H12.
right.
induction H.
assert (z=x0).
apply H12 with x; assumption.
rewrite H14; intuition.
red; intros z H8.
inversion H8.
inversion H9.
red; apply image_intro with x2.
split; [ left | ]; intuition.
induction H9.
red; apply image_intro with x.
split; [ right; intuition | assumption ].
rewrite H8.
right.
assert (Included A s).
red; intros z H9; apply H1.
left; assumption.
generalize (IHFinite H9); intros H10.
induction H10; assumption.
assumption.
(* x not in (domain f) *)
assert (image f (Add S A x) = image f A).
apply Extensionality_Ensembles; split.
red; intros z H4.
inversion H4.
induction H5.
inversion H5.
red; apply image_intro with x0; intuition.
induction H8.
absurd (In (domain f) x).
assumption.
constructor; exists z; assumption.
red; intros z H4.
inversion H4.
induction H5.
red; apply image_intro with x0.
split; [ left; assumption | assumption ].
rewrite H4.
assert (Included A s).
red; intros z H5; apply H1.
left; assumption.
generalize (IHFinite H5); intros H6.
induction H6; assumption.
red; intros z H3.
inversion H3.
induction H4.
repeat (induction H).
generalize (H (x0,z) H6); intros H8.
inversion H8; assumption.
Qed.

(**
  * Finite subsets: old theorems inherited from BPhoX
  *)

(* TODO: move, remove or rename these in the relevant files *)

(* Union *)

(* TODO Theorem Union001: forall (U: Type) (A: Ensemble U) (x: U), ((Union A) x) = (exists B:Ensemble U, B x). *)
(* Inter *)

(* TODO Theorem Inter001: forall (U: Type) (A: Ensemble U) (x: U), ((Inter A) x = /\B:A (B x)). *)
(* part *)

(* finite *)

(* These theorems were untyped and commented out in BPhoX. There also
seem to have been some confusion between being finite and being a set
of finite parts. To study and correct: TODO
Theorem finite001: forall (X Y: Power_finite U), In (Ensemble U) (Power_finite U) (Union X Y).

Theorem finite002: forall (X: Ensemble (Ensemble U)) (Y: Power_finite U), Included X Y -> Power_finite U X.

Theorem finite003: forall (X: Power_finite U) (Y: Ensemble U), Power_finite (Intersection X Y).

*)

(* finite1 *)

(* SC: Same remark as for part1002 *)
Theorem finite1001: forall (U: Type) (A: Ensemble U), (Power_finite1 A) = Setminus (Power_finite A) (Singleton (Empty_set U)) .
Proof.
intros.
auto with sets.
Qed.

