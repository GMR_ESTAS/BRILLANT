(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter2.
Require Import Bnaturals_basics.
Require Import Bnaturals_cardinal.
Require Import Constructive_sets.
Require Import Finite_sets_facts.
Require Import Bfinite_subsets.
Require Import ZArith.
Require Import Bintegers_basics.

(**
  * Cardinal (integer version): definitions
  *)

Inductive cardinal (U: Type): Ensemble U -> Z -> Prop :=
  | card_empty : cardinal U (Empty_set U) Z0
  | card_add :
    forall (A:Ensemble U) (n:Z),
      cardinal U A n -> forall x:U, ~ In A x -> cardinal U (Add U A x) (Zsucc n).

Lemma cardinal_invert : forall (U: Type) (X:Ensemble U) (p:Z),
  cardinal U X p ->
    match p with
      | Z0 => X = Empty_set U
      | Zpos n =>
        exists A : _,
          (exists x : _, X = Add U A x /\ ~ In A x /\ cardinal U A (Zpred p))
      | Zneg n => False
    end.
Proof.
  induction 1.
  reflexivity.
  induction n.
  exists A; exists x; auto.
  exists A; exists x; rewrite <- Zpred_succ; auto.
  contradiction.
Qed.

Lemma cardinal_elim : forall (U: Type) (X:Ensemble U) (p:Z),
    cardinal U X p ->
    match p with
      | Z0 => X = Empty_set U
      | Zpos n => Inhabited U X
      | Zneg n => False
    end.
Proof.
  intros U X p C; elim C.
  reflexivity.
  induction n.
  simpl in |- *; trivial with sets.
  simpl in |- *; trivial with sets.
  contradiction.
Qed.

(**
  This is the final definition of cardinal we will use
  *)
Definition card (S: Type) (s: Ensemble S) := iota _ inhabited_Z (fun n => cardinal S s n).

(**
  * Cardinal (integer version): lemmas, theorems and properties
  *)
Theorem cardinal_Z_of_nat_l: forall (U: Type) (u: Ensemble U) (n: nat),
  Finite_sets.cardinal U u n -> cardinal U u (Z_of_nat n).
Proof.
intros U u n H.
induction H.
left.
rewrite inj_S; right; assumption.
Qed.

Theorem cardinal_Z_of_nat_r: forall (U: Type) (u: Ensemble U) (n: nat),
  cardinal U u (Z_of_nat n) -> Finite_sets.cardinal U u n.
Proof.
intros U u n.
generalize u.
induction n.
intros u0; rewrite inj_0; intros H; specialize (cardinal_invert U u0 0 H); intros H0.
rewrite H0; left.
intros u0; rewrite inj_S; rewrite <- Zpos_P_of_succ_nat; intros H.
specialize (cardinal_invert U u0 (Zpos (P_of_succ_nat n)) H); intros H0.
destruct H0 as [ A H0 ].
destruct H0 as [ x H0 ].
destruct H0 as [ H0 H1 ].
destruct H1 as [ H1 H2 ].
rewrite H0; right; [ | assumption ].
rewrite Zpos_P_of_succ_nat in H2; rewrite <- Zpred_succ in H2.
intuition.
Qed.

Theorem cardinal_Z_of_nat: forall (U: Type) (u: Ensemble U) (n: nat),
  Finite_sets.cardinal U u n <-> cardinal U u (Z_of_nat n).
Proof.
intros U u n.
split.
apply cardinal_Z_of_nat_l.
apply cardinal_Z_of_nat_r.
Qed.

Theorem cardinal_nat_of_Z_r: forall (U: Type) (u: Ensemble U) (n: Z),
  (exists p: nat, Finite_sets.cardinal U u p /\ n = Z_of_nat p) -> cardinal U u n.
Proof.
intros U u n H.
destruct H as [ p H ].
destruct H as [ H H0 ].
destruct p.
simpl in H0; rewrite H0.
specialize (cardinalO_empty U u H); intros H1; rewrite H1; left.
destruct H.
simpl in H0; rewrite H0; left.
rewrite H0; rewrite inj_S; right.
apply cardinal_Z_of_nat_l; assumption.
assumption.
Qed.

Theorem cardinal_nat_of_Z_l: forall (U: Type) (u: Ensemble U) (n: Z),
  cardinal U u n -> exists p: nat, Finite_sets.cardinal U u p /\ n = Z_of_nat p.
Proof.
intros U u n H.
induction H as [ | A x H0 IHcardinal x0 H1 ].
exists 0%nat.
split; [ left | simpl; reflexivity ].
destruct IHcardinal as [ x1 H2 ].
destruct H2 as [ H2 H3 ].
exists (S x1).
split; [ right; assumption | ].
rewrite inj_S.
apply Zsucc_eq_compat; assumption.
Qed.

Theorem cardinal_nat_of_Z: forall (U: Type) (u: Ensemble U) (n: Z),
  cardinal U u n <-> exists p: nat, Finite_sets.cardinal U u p /\ n = Z_of_nat p.
Proof.
intros U u n; split; [ apply cardinal_nat_of_Z_l | apply cardinal_nat_of_Z_r]; assumption.
Qed.

Theorem cardinal_nat_of_Z_l_weak: forall (U: Type) (u: Ensemble U) (n: Z),
  cardinal U u n -> exists p: nat, Finite_sets.cardinal U u p.
Proof.
intros U u n H.
specialize (cardinal_nat_of_Z_l U u n H); intros H0.
destruct H0 as [ p H0 ]; exists p; intuition.
Qed.

Theorem cardinal_Z_finite: forall (U: Type) (u: Ensemble U) (n: Z),
  cardinal U u n -> Finite u.
Proof.
intros U u n H.
specialize (cardinal_nat_of_Z_l_weak U u n H); intros H0.
destruct H0 as [ p H0 ].
apply cardinal_finite with p; assumption.
Qed.

Lemma cardinalZ0_empty: forall (U: Type) (X:Ensemble U),
  cardinal U X 0 -> X = Empty_set U.
Proof.
intros U X H; apply (cardinal_invert U X 0); trivial with sets.
Qed.

Lemma cardinal_Empty : forall (U: Type) (m:Z),
  cardinal U (Empty_set U) m -> 0 = m.
Proof.
intros U m Cm; specialize (cardinal_invert U (Empty_set U) m Cm).
elim m.
intuition.
intros p H.
destruct H as [ A H ].
destruct H as [ x H ].
destruct H as [ H H0 ].
destruct H0 as [ H0 H1 ].
specialize (not_Empty_Add U A x H); contradiction.
contradiction.
Qed.

Lemma cardinal_is_functional: forall (U: Type) (X:Ensemble U) (c1:Z),
  cardinal U X c1 ->
  forall (Y:Ensemble U) (c2:Z), cardinal U Y c2 -> X = Y -> c1 = c2.
Proof.
intros U X c1 H Y c2 H0 H1.
specialize (cardinal_nat_of_Z_l U X c1 H); intros H2.
specialize (cardinal_nat_of_Z_l U Y c2 H0); intros H3.
destruct H2 as [ p1 (H2, H2') ].
destruct H3 as [ p2 (H3, H3') ].
rewrite H2'; rewrite H3'.
apply inj_eq; apply Finite_sets_facts.cardinal_is_functional with U X Y; assumption.
Qed.

Lemma cardinal_unicity: forall (U: Type) (X:Ensemble U) (n:Z),
  cardinal U X n -> forall (m:Z), cardinal U X m -> n = m.
Proof.
intros; apply cardinal_is_functional with U X X; auto with sets.
Qed.

Theorem cardinal_card: forall (U: Type) (u: Ensemble U),
  Finite u -> cardinal U u (card U u).
Proof.
intros U u H.
induction H.
replace (card U (Empty_set U)) with 0.
left.
unfold card; apply iota_ind_weak.
apply cardinal_Empty.
exists 0.
split; [ left | ].
apply cardinal_Empty.
replace (card U (Add U A x)) with (Zsucc (card U A)).
right; assumption.
unfold card at 2; apply iota_ind.
intros b H1.
induction H1 as [ H1 H2 ].
symmetry; apply H2.
right; assumption.
exists (Zsucc (card U A)).
split.
right; assumption.
intros x' H1.
apply cardinal_unicity with U (Add U A x).
right; assumption.
assumption.
Qed.

Theorem card_is_Bcardinal_in_Z: forall (U: Type) (u: Ensemble U),
  Finite u -> card U u = Z_of_nat (Bcardinal U u).
Proof.
intros U u H.
unfold card; apply iota_ind_weak.
induction H as [ | ].
intros b H.
specialize (cardinal_Empty U b H); intros H0.
rewrite <- H0.
rewrite Bcardinal_O; simpl; reflexivity.
intros b H1.
apply cardinal_unicity with U (Add U A x).
assumption.
specialize (cardinal_nat_of_Z_l U (Add U A x) b H1); intros H2.
destruct H2 as [ p H2 ].
destruct H2 as [ H2 H3 ].
assert (Finite (Add U A x)) as H4; [ right; assumption | ].
specialize (cardinal_implies_Bcardinal U (Add U A x) p H4 H2); intros H5.
rewrite H5; rewrite <- H3; assumption.
exists (card U u).
split.
apply cardinal_card; assumption.
intros.
apply cardinal_unicity with U u; [ apply cardinal_card | ]; assumption.
Qed.

Theorem cardinal_card_B: forall (S: Type) (s t: Ensemble S),
  In (Power_finite s) t -> cardinal S t (card S t).
Proof.
intros S s t H.
destruct H.
apply cardinal_card; assumption.
Qed.

Theorem card_implies_cardinal: forall (S: Type) (s: Ensemble S) (n: Z),
  Finite s -> card S s = n -> cardinal S s n.
Proof.
intros S s n H H0.
rewrite <- H0; apply cardinal_card.
assumption.
Qed.

Theorem card_implies_cardinal_B: forall (S: Type) (s t: Ensemble S) (n: Z),
  In (Power_finite s) t -> card S t = n -> cardinal S t n.
Proof.
intros S s t n H H0.
destruct H; apply card_implies_cardinal; assumption.
Qed.

Theorem cardinal_implies_card: forall (S: Type) (s: Ensemble S) (n: Z),
  Finite s -> cardinal S s n -> card S s = n.
Proof.
intros S s n H H0.
apply cardinal_unicity with S s.
apply cardinal_card; assumption.
assumption.
Qed.

Theorem cardinal_implies_card_B: forall (S: Type) (s t: Ensemble S) (n: Z),
  In (Power_finite s) t -> cardinal S t n -> card S t = n.
Proof.
intros S s t n H H0.
destruct H.
apply cardinal_implies_card; assumption.
Qed.

Theorem card_0: forall (U: Type), card U (Empty_set U) = 0.
Proof.
intros U.
apply cardinal_implies_card; left.
Qed.

Theorem card_Zsucc: forall (U: Type) (u: Ensemble U) (n: Z),
  Finite u -> card U u = n -> forall (x: U), ~(In u x) -> card U (Add U u x) = (Zsucc n).
Proof.
intros U u n H H0 x H1.
apply cardinal_implies_card.
right; assumption.
right; [ | assumption ].
apply card_implies_cardinal; assumption.
Qed.
