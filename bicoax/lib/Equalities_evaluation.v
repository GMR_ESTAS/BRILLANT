(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Bderived_constructs.
Require Import Brelations.
Require Import Bfunctions.
Require Import Membership_laws.
Require Import Monotonicity_laws.

Open Scope eB_scope.

(**
  BBook: section 2.6.4, p.115, array 1, row 1
  *)
Theorem Equality_laws_funct_01: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (x: S), (f ∈ (s ⤔ t) ∧ x ∈ domain(f)) ⇒( exists applicable_f:(In (partial_function s t) f /\ In (domain f) x), exists applicable_invf:(In (partial_function t s) (inverse f) /\ In (domain (inverse f)) (app f x applicable_f)), app (f∼) (app f x applicable_f) applicable_invf = x).
Proof.
intros S T s t f x H.
assert (In (partial_function s t) f /\ In (domain f) x).
split; [ induction H; induction H; assumption | intuition ].

exists H0.
assert (In (partial_function t s) (inverse f) /\ In (domain (inverse f)) (app f x H0)).
split.
apply Membership_laws_02; intuition.
constructor; exists x; constructor; apply app_trivial_property.

exists H1.
symmetry; apply identifying_app.
constructor; apply app_trivial_property.
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 2
  *)
Theorem Equality_laws_funct_02: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (T*U)) (x: S), (p ∈ (s ⇸ t) ∧ q ∈ (t ⇸ u) ∧ x ∈ domain(p;q)) ⇒( exists applicable_pq:(In (partial_function s u) (composition p q) /\ In (domain (composition p q)) x), exists applicable_p:(In (partial_function s t) p /\ In (domain p) x), exists applicable_q:(In (partial_function t u) q /\ In (domain q) (app p x applicable_p)), app (p;q) x applicable_pq = app q (app p x applicable_p) applicable_q).
Proof.
intros S T U s t u p q x H.
assert (In (partial_function s t) p /\ In (domain p) x).
split; [ intuition | ].
induction H.
induction H0; inversion H0.
inversion H1.
inversion H3.
constructor; exists x0; intuition.
assert (In (partial_function t u) q /\ In (domain q) (app p x H0)).
split; [ intuition | ].
induction H; inversion H0.
induction H1.
induction H1; inversion H1.
inversion H5.
assert (x0 = app p a H0).
induction H2.
apply H8 with a; [ intuition | apply app_trivial_property].
rewrite <- H8.
constructor; exists x; intuition.
assert (In (partial_function s u) (composition p q) /\ In (domain (composition p q)) x).
split; [ | intuition ].
apply Membership_laws_08 with t; intuition.

exists H2; exists H0; exists H1.
symmetry; apply identifying_app.
constructor; exists (app p x H0).
split.
apply app_trivial_property.
apply app_trivial_property.
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 3

  Note: might also be used as a "functionalisation" of id ?
  *)
Theorem Equality_laws_funct_03: forall (S: Type) (s: Ensemble S) (x: S), (x ∈ s) ⇒(exists applicable_id:(In (partial_function s s) (id s) /\ In (domain (id s)) x), app (id(s)) x applicable_id = x).
Proof.
intros S s x H.
assert (In (partial_function s s) (id s) /\ In (domain (id s)) x).
split.
constructor.
constructor; constructor.
intros z H0; induction H0; induction H0.
constructor; assumption.
intros.
inversion H0; inversion H1.
rewrite <- H5; rewrite <- H9; reflexivity.
constructor; exists x.
constructor; [ constructor; assumption | reflexivity ].

exists H0.
symmetry; apply identifying_app.
constructor; [ constructor; assumption | reflexivity ].
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 4
  *)
Theorem Equality_laws_funct_04: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (u: Ensemble S) (x: S), (u ⊆ s ∧ f ∈ (s ⇸ t) ∧ x ∈ (u ∩ domain(f))) ⇒(exists applicable_f:(In (partial_function s t) f /\ In (domain f) x), exists applicable_domresf:(In (partial_function s t) (u ◁ f) /\ In (domain (u ◁ f)) x), app (u ◁ f) x applicable_domresf = app f x applicable_f).
Proof.
intros S T s t f u x H.
assert (In (partial_function s t) f /\ In (domain f) x).
split; [ intuition | ].
decompose [and] H.
induction H1; assumption.
assert (In (partial_function s t) (domain_restriction f u) /\ In (domain (domain_restriction f u)) x).
decompose [and] H.
split.
constructor.
assert (In (relation u t) (domain_restriction f u)).
apply Membership_laws_16 with s; inversion H4; intuition.
apply (Monotonicity_laws_01 _ _ s t u t); intuition.
intros.
inversion H1; inversion H5.
induction H4.
apply H14 with x0; assumption.
induction H2.
induction H2; inversion H2.
constructor; exists x; constructor; intuition.

exists H0; exists H1.
symmetry; apply identifying_app.
decompose [and] H.
induction H3.
constructor; [ apply app_trivial_property | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 5
  *)
Theorem Equality_laws_funct_05: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (v: Ensemble T) (x: S), (f ∈ (s ⇸ t) ∧ v ⊆ t ∧ x ∈ (f∼[v])) ⇒(exists applicable_f:(In (partial_function s t) f /\ In (domain f) x), exists applicable_ranresf:(In (partial_function s t) (f ▷ v) /\ In (domain (f ▷ v)) x), app (f ▷ v) x applicable_ranresf = app f x applicable_f).
Proof.
intros S T s t f v x H.
assert (In (partial_function s t) f /\ In (domain f) x).
split; [ intuition | ].
decompose [and] H.
induction H1.
induction H0.
inversion H1.
constructor; exists x; assumption.
assert (In (partial_function s t) (range_restriction f v) /\ In (domain (range_restriction f v)) x).
decompose [and] H.
split.
constructor.
assert (In (relation s v) (range_restriction f v)).
apply Membership_laws_17 with t; inversion H3; intuition.
apply (Monotonicity_laws_01 _ _ s t s v); intuition.
intros.
inversion H1; inversion H5.
induction H3.
apply H14 with x0; assumption.
induction H2.
induction H1; inversion H2.
constructor; exists x; constructor; intuition.

exists H0; exists H1.
symmetry; apply identifying_app.
constructor; [ apply app_trivial_property | ].
induction H1.
inversion H2.
inversion H3.
inversion H5.
assert (x0 = app f x H0).
apply identifying_app; assumption.
rewrite <- H10; assumption.
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 6
  *)
Theorem Equality_laws_funct_06: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (u: Ensemble S) (x: S), (u ⊆ s ∧ f ∈ (s ⇸ t) ∧ x ∈ (domain(f)∖u)) ⇒(exists applicable_f:(In (partial_function s t) f /\ In (domain f) x), exists applicable_domsubf:(In (partial_function s t) (u ⩤ f) /\ In (domain (u ⩤ f)) x), app (u ⩤ f) x applicable_domsubf = app f x applicable_f).
Proof.
intros S T s t f u x H.
assert (In (partial_function s t) f /\ In (domain f) x).
split; [ intuition | ].
decompose [and] H.
induction H1; assumption.
assert (In (partial_function s t) (domain_subtraction f u) /\ In (domain (domain_subtraction f u)) x).
decompose [and] H.
split.
constructor.
assert (In (relation (Setminus s u) t) (domain_subtraction f u)).
apply Membership_laws_18; inversion H4; intuition.
apply (Monotonicity_laws_01 _ _ s t (Setminus s u) t).
split; [ intros z H5; induction H5; assumption | intuition ].
assumption.
intros.
inversion H1; inversion H5.
induction H4.
apply H14 with x0; assumption.
induction H2.
induction H1; inversion H1.
constructor; exists x; constructor; intuition.

exists H0; exists H1.
symmetry; apply identifying_app.
decompose [and] H.
induction H3.
constructor; [ apply app_trivial_property | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 7
  *)
Theorem Equality_laws_funct_07: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (v: Ensemble T) (x: S), (f ∈ (s ⇸ t) ∧ v ⊆ t ∧ x ∈ (domain(f) ∖ (f∼[v]))) ⇒(exists applicable_f:(In (partial_function s t) f /\ In (domain f) x), exists applicable_ransubf:(In (partial_function s t) (f ⩥ v) /\ In (domain (f ⩥ v)) x), app (f ⩥ v) x applicable_ransubf = app f x applicable_f).
Proof.
intros S T s t f v x H.
assert (In (partial_function s t) f /\ In (domain f) x).
split; [ intuition | ].
decompose [and] H.
induction H1.
induction H0.
inversion H0.
constructor; exists x; assumption.
assert (In (partial_function s t) (range_subtraction f v) /\ In (domain (range_subtraction f v)) x).
decompose [and] H.
split.
constructor.
assert (In (relation s (Setminus t v)) (range_subtraction f v)).
apply Membership_laws_19; inversion H3; intuition.
apply (Monotonicity_laws_01 _ _ s t s (Setminus t v)).
split; [ intuition | intros z H5; induction H5; assumption ].
assumption.
intros.
inversion H1; inversion H5.
induction H3.
apply H14 with x0; assumption.
induction H2.
induction H1; inversion H1.
constructor; exists x; constructor.
assumption.
intros H6; apply H2.
red; apply image_intro with x; split; [ assumption | constructor; assumption ].

exists H0; exists H1.
symmetry; apply identifying_app.
constructor; [ apply app_trivial_property | ].
induction H1.
inversion H2.
inversion H3.
inversion H5.
assert (x0 = app f x H0).
apply identifying_app; assumption.
rewrite <- H10; assumption.
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 8
  *)
Theorem Equality_laws_funct_08: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)) (x: S), (f ∈ (s ⇸ t) ∧ g ∈ (s ⇸ t)  ∧ x ∈ (domain(f)∖domain(g))) ⇒(exists applicable_f:(In (partial_function s t) f /\ In (domain f) x), exists applicable_overf:(In (partial_function s t) (f ⥷ g) /\ In (domain (f ⥷ g)) x), app (f ⥷ g) x applicable_overf = app f x applicable_f).
Proof.
intros S T s t f g x H.
decompose [and] H.
assert (In (partial_function s t) f /\ In (domain f) x).
split; [ intuition | ].
induction H1; assumption.
assert (In (partial_function s t) (override f g) /\ In (domain (override f g)) x).
split.
apply Membership_laws_22; intuition.
induction H1.
inversion H1; inversion H5.
constructor; exists x0.
constructor; left; intuition.

exists H0; exists H4.
symmetry; apply identifying_app.
constructor; left.
induction H1; split; [ apply app_trivial_property | assumption ].
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 9
  *)
Theorem Equality_laws_funct_09: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)) (x: S), (f ∈ (s ⇸ t) ∧ g ∈ (s ⇸ t)  ∧ x ∈ domain(g)) ⇒(exists applicable_g:(In (partial_function s t) g /\ In (domain g) x), exists applicable_overf:(In (partial_function s t) (f ⥷ g) /\ In (domain (f ⥷ g)) x), app (f ⥷ g) x applicable_overf = app g x applicable_g).
Proof.
intros S T s t f g x H.
decompose [and] H.
assert (In (partial_function s t) g /\ In (domain g) x).
split; [ intuition | assumption ].
assert (In (partial_function s t) (override f g) /\ In (domain (override f g)) x).
split.
apply Membership_laws_22; intuition.
induction H1.
inversion H1.
constructor; exists x.
constructor; right; assumption.

exists H0; exists H4.
symmetry; apply identifying_app.
constructor; right; apply app_trivial_property.
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 10
  *)
Theorem Equality_laws_funct_10: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (S*U)) (x: S), (f ∈ (s ⇸ t) ∧ g ∈ (s ⇸ u)  ∧ x ∈ (domain(f) ∩ domain(g))) ⇒(exists applicable_fg:(In (partial_function s (times t u)) (direct_product f g) /\ In (domain (direct_product f g)) x), exists applicable_f:(In (partial_function s t) f /\ In (domain f) x), exists applicable_g:(In (partial_function s u) g /\ In (domain g) x), app (f ⊗ g) x applicable_fg = ((app f x applicable_f) ↦ (app g x applicable_g))).
Proof.
intros S T U s t u f g x H.
decompose [and] H.
induction H1.
assert (In (partial_function s t) f /\ In (domain f) x).
split; [ intuition | assumption ].
assert (In (partial_function s u) g /\ In (domain g) x).
split; [ intuition | assumption ].
assert (In (partial_function s (times t u)) (direct_product f g) /\ In (domain (direct_product f g)) x).
split.
apply Membership_laws_29; intuition.
(* SC: I could have used Equalities_domain here, but I want to clean the hypotheses before *)
induction H0; induction H1.
inversion H0; inversion H1.
constructor; exists (x,x0); constructor; assumption.

exists H6; exists H4; exists H5.
symmetry; apply identifying_app.
constructor; apply app_trivial_property.
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 11

  Note/TODO: we might need a proof of partiality (or totality) for functions such as prj1, prj2, etc
  *)
Theorem Equality_laws_funct_11: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (x: S) (y: T), (x ∈ s ∧ y ∈ t) ⇒(exists applicable_prj1:(In (partial_function (times s t) s) (prj1(s,t)) /\ In (domain (prj1(s,t))) (x,y)), app (prj1(s,t)) (x,y) applicable_prj1 = x).
Proof.
intros S T s t x y H.
induction H.
assert (In (partial_function (times s t) s) (prj1 (s,t)) /\ In (domain (prj1 (s,t))) (x,y)).
split.
constructor.
constructor.
constructor.
intros z H1.
induction z.
induction a.
inversion H1.
compute in H4.
inversion H4.
inversion H9.
constructor; [ constructor | ]; assumption.
intros.
inversion H1; inversion H2.
assert ((x1, y1)=(x2,y2)).
rewrite H3 ; rewrite H7; reflexivity.
injection H11; intros.
rewrite H6; rewrite H13; rewrite <- H10; reflexivity.
constructor; exists x.
constructor.
constructor; [ constructor | ]; intuition.
reflexivity.

exists H1.
symmetry; apply identifying_app.
constructor.
constructor; [ constructor | ]; intuition.
reflexivity.
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 12

  error: in the BBook, replace prj1 with prj2
  *)
Theorem Equality_laws_funct_12: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (x: S) (y: T), (x ∈ s ∧ y ∈ t) ⇒(exists applicable_prj2:(In (partial_function (times s t) t) (prj2(s,t)) /\ In (domain (prj2(s,t))) (x,y)), app (prj2(s,t)) (x,y) applicable_prj2 = y).
Proof.
intros S T s t x y H.
induction H.
assert (In (partial_function (times s t) t) (prj2 (s,t)) /\ In (domain (prj2 (s,t))) (x,y)).
split.
constructor.
constructor.
constructor.
intros z H1.
induction z.
induction a.
inversion H1.
compute in H4.
inversion H4.
inversion H9.
constructor; [ constructor | ]; assumption.
intros.
inversion H1; inversion H2.
assert ((x1, y1)=(x2,y2)).
rewrite H3 ; rewrite H7; reflexivity.
injection H11; intros.
rewrite H6; rewrite H12; rewrite <- H10; reflexivity.
constructor; exists y.
constructor.
constructor; [ constructor | ]; intuition.
reflexivity.

exists H1.
symmetry; apply identifying_app.
constructor.
constructor; [ constructor | ]; intuition.
reflexivity.
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 13
  *)
Theorem Equality_laws_funct_13: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (f: Ensemble (S*T)) (g: Ensemble (U*V)) (x: S) (y:U), (f ∈ (s ⇸ t) ∧ g ∈ (u ⇸ v)  ∧ x ∈ domain(f) ∧ y ∈ domain(g)) ⇒(exists applicable_fg:(In (partial_function (times s u) (times t v)) (parallel_product f g) /\ In (domain (parallel_product f g)) (x,y)), exists applicable_f:(In (partial_function s t) f /\ In (domain f) x), exists applicable_g:(In (partial_function u v) g /\ In (domain g) y), app (f ∥ g) (x,y) applicable_fg = ((app f x applicable_f) ↦ (app g y applicable_g))).
Proof.
intros S T U V s t u v f g x y H.
decompose [and] H.
assert (In (partial_function s t) f /\ In (domain f) x).
split; [ intuition | assumption ].
assert (In (partial_function u v) g /\ In (domain g) y).
split; [ intuition | assumption ].
assert (In (partial_function (times s u) (times t v)) (parallel_product f g) /\ In (domain (parallel_product f g)) (x,y)).
split.
apply Membership_laws_38; intuition.
(* SC: same as for the direct product. I guess *)
induction H1; induction H3.
inversion H1; inversion H3.
constructor; exists (x0,x); constructor; assumption.

exists H6; exists H2; exists H5.
symmetry; apply identifying_app.
constructor; apply app_trivial_property.
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 14
  *)
Theorem Equality_laws_funct_14: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)) (x: S), (f ∈ (s ⇸ t) ∧ g ∈ (s ⇸ t)  ∧ domain(g)◁f = domain(f)◁g ∧ x ∈ domain(f)) ⇒(exists applicable_f:(In (partial_function s t) f /\ In (domain f) x), exists applicable_fug:(In (partial_function s t) (f ∪ g) /\ In (domain (f ∪ g)) x), app (f ∪ g) x applicable_fug = app f x applicable_f).
Proof.
intros S T s t f g x H.
decompose [and] H.
assert (In (partial_function s t) f /\ In (domain f) x).
split; [ intuition | assumption ].
assert (In (partial_function s t) (Union f g) /\ In (domain (Union f g)) x).
split.
apply Membership_laws_45; intuition.
induction H1; inversion H1.
constructor; exists x; left; assumption.

exists H2; exists H5.
symmetry; apply identifying_app.
left; apply app_trivial_property.
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 15
  *)
Theorem Equality_laws_funct_15: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)) (x: S), (f ∈ (s ⇸ t) ∧ g ∈ (s ↔ t)  ∧ x ∈ domain(f∖g)) ⇒(exists applicable_f:(In (partial_function s t) f /\ In (domain f) x), exists applicable_fming:(In (partial_function s t) (f ∖ g) /\ In (domain (f ∖ g)) x), app (f ∖ g) x applicable_fming = app f x applicable_f).
Proof.
intros S T s t f g x H.
decompose [and] H.
assert (In (partial_function s t) f /\ In (domain f) x).
split; [ intuition | ].
induction H1.
inversion H0.
induction H1.
constructor; exists x; assumption.
assert (In (partial_function s t) (Setminus f g) /\ In (domain (Setminus f g)) x).
split.
(* SC: Wait. No corresponding Membership law ? Awwww :-( *)
constructor.
repeat (constructor).
intros z H4.
induction H4.
repeat (induction H2).
generalize (H2 z H4); intros.
assumption.
intros.
inversion H4; inversion H5.
induction H2.
apply H10 with x0; assumption.
assumption.

exists H0; exists H4.
symmetry; apply identifying_app.
constructor.
apply app_trivial_property.
inversion H1.
inversion H5.
inversion H7.
intros H10; apply H9.
assert (x0 = app f x H0).
induction H2.
apply H11 with x; [ assumption | apply app_trivial_property ].
rewrite H11; assumption.
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 16
  *)
Theorem Equality_laws_funct_16: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (x: S) (y: T), (x ∈ s ∧ y ∈ t) ⇒(exists applicable_sing:(In (partial_function s t) (Singleton (x↦y)) /\ In (domain (Singleton (x↦y))) x), app (Singleton (x↦y)) x applicable_sing = y).
Proof.
intros S T s t x y H.
assert (In (partial_function s t) (Singleton (x,y)) /\ In (domain (Singleton (x,y))) x).
split.
apply Membership_laws_50; intuition.
constructor; exists y; intuition.

exists H0.
symmetry; apply identifying_app.
intuition.
Qed.

(**
  BBook: section 2.6.4, p.115, array 1, row 17
  *)
Theorem Equality_laws_funct_17: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (x: S) (y: T) (u: Ensemble S), (u ⊆ s ∧ x ∈ u ∧ y ∈ t) ⇒(exists applicable_using:(In (partial_function s t) (u×(Singleton (y))) /\ In (domain (u×(Singleton (y)))) x), app (u×(Singleton (y))) x applicable_using = y).
Proof.
intros S T s t x y u H.
decompose [and] H.
assert (In (partial_function s t) (times u (Singleton y)) /\ In (domain (times u (Singleton y))) x).
split.
constructor.
repeat constructor.
intros z H4.
inversion H4.
induction H5.
constructor; [ apply H2; assumption | assumption ].
intros.
inversion H0; inversion H4.
induction H8; induction H12; reflexivity.
constructor; exists y; constructor; intuition.

exists H0.
symmetry; apply identifying_app.
constructor; intuition.
Qed.


Close Scope eB_scope.
