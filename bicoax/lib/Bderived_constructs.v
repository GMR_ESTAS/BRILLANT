(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

(* Library of definitions for set theory *)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Constructive_sets.
Require Import Powerset_facts.

(**
  * Definition of derived constructs (mostly notations)
  *)

(**
  union, BBook: section 2.3.1, p.72, array 1, row 1
  *)
Implicit Arguments Union [U].
Notation "x ∪ y" := (Union x y) (at level 61, right associativity): eB_scope.

(**
  intersection, BBook: section 2.3.1, p.72, array 1, row 2
  *)
Implicit Arguments Intersection [U].
Notation "x ∩ y" := (Intersection x y) (at level 51, right associativity): eB_scope.

(**
  set difference, BBook: section 2.3.1, p.72, array 1, row 3
  *)
Implicit Arguments Setminus [U].
Notation "x ∖ y" := (Setminus x y) (at level 51, right associativity): eB_scope.

(**
  singleton notation, BBook: section 2.3.1, p.72, array 1, row 4
  *)
Implicit Arguments Singleton [U].
Notation "{= x =}" := (Singleton x)  (at level 6, no associativity).

(**
  set defined in extension, BBook: section 2.3.1, p.72, array 1, row 5
  *)
Notation "{= x , y , .. , z =}" := (Union .. (Union (Singleton x) (Singleton y)) .. (Singleton z)) (at level 6, no associativity).

(* TODO: abstract sets, maybe ? *)

(**
  set difference, BBook: section 2.3.1, p.72, array 1, row 6

Implicit arguments can not be set for the empty set because the type
inference algorithm has no basis for finding the correct type it is
based upon.

It illustrates another interesting property of the empty set has denoted in the BBook, that
the empty set must be the empty set of a type. This gives way to the following remarks:

  - The proof obligation generator will require the type of each empty
    set to generate the "correct" empty set here. For instance
    [Empty_set Z] will correspond to all numeric sets.

  - This will also require tricky definitions for abstract sets,
    whether they are defined in extension, such as A= {elt1, etl2} or
    kept abstract. See the part about those sets below.
  *)
Notation "∅" := (Empty_set _) (at level 10, no associativity): eB_scope.


(**
  power set excluding the empty set, BBook: section 2.3.1, p.72, array 1, row 7
  *)
Definition Power_set1 (U:Type) (A:Ensemble U) := Subtract (Ensemble U) (Power_set A) (Empty_set U).
Implicit Arguments Power_set1 [U].
Notation "ℙ1( x )" := (Power_set1 x) (at level 6, no associativity): eB_scope.

(* Example time:
Theorem easy: forall (U:Type) (A: Ensemble U), Included (Power_set1 A) (Power_set A).
Proof.
intros.
unfold Included.
unfold Power_set1.
unfold Subtract.
compute.
intuition.
Qed.
*)

(**
  * Validity of the derived constructs definitions
  *)

Theorem valid_union: forall (S: Type) (s t u: Ensemble S), Included s u -> Included t u -> (Union s t) = (Comprehension (fun a => In u a /\ (In s a \/ In t a))).
Proof.
intros S s t u H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
compute; split; [ apply H; intuition | left; intuition].
compute; split; [ apply H0; intuition | right; intuition].
elim H1; intros H2 H3.
elim H3; [ left; intuition | right; intuition].
Qed.

Theorem valid_intersection: forall (S: Type) (s t u: Ensemble S), Included s u -> Included t u -> (Intersection s t) = (Comprehension (fun a => In u a /\ (In s a /\ In t a))).
Proof.
intros S s t u H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1; compute; split; [ apply H; intuition | intuition ].
elim H1; intuition.
Qed.

Theorem valid_setminus: forall (S: Type) (s t u: Ensemble S), Included s u -> Included t u -> (Setminus s t) = (Comprehension (fun a => In u a /\ (In s a /\ NotIn t a))).
Proof.
intros S s t u H H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1; compute; split; [ apply H; intuition | intuition ].
elim H1; intuition.
Qed.

Theorem valid_singleton: forall (S: Type) (u: Ensemble S) (E: S), In u E -> Singleton E = (Comprehension (fun a => In u a /\ a = E)).
Proof.
intros S u E H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1; compute; split; [ intuition | reflexivity].
elim H1; intuition.
Qed.

(**
  The definition of a set in extension is more a syntactic
  construct. Putting it together with more semantic constructs in the
  same table in the BBook does not make much sense
  *)
Theorem valid_set_in_extension: forall (S: Type) (u: Ensemble S) (L E: S), {= L, E =} = (Union {= L =} {= E =}).
Proof.
reflexivity.
Qed.

Theorem valid_emptyset: forall (S: Type) (s: Ensemble S), Empty_set _ = Setminus s s.
Proof.
intros S s.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
contradiction.
elim H1; intuition.
Qed.

Theorem valid_powerset1: forall (S: Type) (s: Ensemble S), Power_set1 s = (Setminus (Power_set s) (Singleton (Empty_set _))).
intros S s.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
elim H1; intuition.
intuition.
Qed.

(**
  * Derived constructs: old theorems inherited from BPhoX
  *)

(* TODO: move, remove or rename these in the relevant files *)

(* emptyset *)

Theorem emptyset001: forall (U: Type) (x:U), ~ In (Empty_set U) x.
Proof.
intros.
auto with sets.
Qed.

(* emptyset + subseteq *)

Theorem emptyset002: forall (U: Type) (A: Ensemble U), Included (Empty_set U) A.
Proof.
intros.
auto with sets.
Qed.

(* singleton *)

(* TODO: this is actually an equality *)
Theorem singleton001: forall (U: Type) (x y: U), In (Singleton y) x <-> (x=y).
Proof.
intros.
compute.
constructor.
intros.
elim H; reflexivity.
intros; rewrite H.
constructor.
Qed.

Theorem singleton002: forall (U: Type) (A: Ensemble U) (x: U), Included (Singleton x) A <-> In A x.
Proof.
intros.
compute.
split.
intros.
apply H; intros; constructor.
intros.
elim H0; assumption.
Qed.

(* singleton + emptyset *)

Theorem singleton003: forall (U: Type) (x: U), Singleton x <> Empty_set U.
Proof.
intros.
apply Inhabited_not_empty.
constructor 1 with x.
auto with sets.
Qed.

Theorem singleton004: forall (U: Type) (A: Ensemble U) (x: U), (A x -> Included (Singleton x) A).
Proof.
intros.
compute.
intros.
elim H0.
assumption.
Qed.

(* union *)

Theorem union001: forall (U: Type) (A B: Ensemble U) (x: U),  (In (Union A B) x) <-> (A x \/ B x).
Proof.
compute.
split.
intros.
elim H; intuition.
intros.
elim H.
left.
intuition.
right; intuition.
Qed.

Theorem union002: forall (U: Type) (A B: Ensemble U), (Union A B) = (Union B A).
Proof.
intros.
auto with sets.
Qed.

Theorem union003: forall (U: Type) (A B C: Ensemble U),  (Union A (Union B C)) = (Union (Union A B) C).
Proof.
intros.
auto with sets.
Qed.

Theorem union004: forall (U: Type) (A: Ensemble U), (Union A A) = A.
Proof.
apply Union_idempotent.
Qed.

(* union + emptyset *)

Theorem union005: forall (U: Type) (A: Ensemble U), (Union A (Empty_set U)) = A.
Proof.
intros.
auto 6 with sets.
Qed.

Theorem union006: forall (U: Type) (A: Ensemble U), (Union (Empty_set U) A) = A.
Proof.
intros.
auto with sets.
Qed.

(* union + subseteq *)

Theorem union007: forall (U: Type) (A B: Ensemble U), Included A (Union A B).
Proof.
intros.
auto with sets.
Qed.

Theorem union008: forall (U: Type) (A B: Ensemble U), Included B (Union A B).
Proof.
intros.
auto with sets.
Qed.

Theorem union009: forall (U: Type) (A B C: Ensemble U),  (Included A C) -> (Included B C) -> Included (Union A B) C.
Proof.
intros.
auto with sets.
Qed.

Theorem union010: forall (U: Type) (A B C: Ensemble U),  (Included B C) -> Included (Union A B) (Union A C).
Proof.
intros.
auto with sets.
Qed.

(* inter *)

Theorem inter001: forall (U: Type) (A B: Ensemble U) (x: U), (In (Intersection A B) x) <-> (A x /\ B x).
Proof.
intros.
compute.
split.
intros; elim H; intuition.
intros; constructor; intuition.
Qed.

Theorem inter002: forall (U: Type) (A B: Ensemble U), (Intersection A B) = (Intersection B A).
Proof.
apply Intersection_commutative.
Qed.

Theorem inter003: forall (U: Type) (A B C: Ensemble U),  (Intersection A (Intersection B C)) = (Intersection (Intersection A B) C).
Proof.
intros.
apply Extensionality_Ensembles.
compute.
split.
intros.
elim H; intros.
constructor.
compute.
constructor.
assumption.
elim H1; intuition.
elim H1; intuition.
intros.
elim H; intros.
constructor.
elim H0; intuition.
constructor.
elim H0; intuition.
assumption.
Qed.

Theorem inter004: forall (U: Type) (A: Ensemble U), (Intersection A A) = A.
Proof.
intros.
auto 7 with sets.
Qed.

(* inter  + emptyset *)

Theorem inter005: forall (U: Type) (A: Ensemble U), (Intersection A (Empty_set U)) = (Empty_set U).
Proof.
intros.
auto with sets.
Qed.

Theorem inter006: forall (U: Type) (A: Ensemble U), (Intersection (Empty_set U) A) = (Empty_set U).
Proof.
intros.
auto with sets.
Qed.

(* inter + = *)

Theorem inter016: forall (U: Type) (A B C: Ensemble U), B = C -> ((Intersection A B) = (Intersection A C)).
Proof.
intros.
rewrite H; intuition.
Qed.

Theorem inter017: forall (U: Type) (A B C: Ensemble U), B = C -> ((Intersection B A) = (Intersection C A)).
Proof.
intros.
rewrite H; intuition.
Qed.

(* inter + singleton + emptyset *)

Theorem inter018: forall (U: Type) (A: Ensemble U) (x: U), A = (Empty_set U) -> Intersection A (Singleton x) = (Empty_set U).
Proof.
intros.
rewrite H.
apply Extensionality_Ensembles.
split.
unfold Included; intros.
compute in H0.
elim H0; intuition.
apply Included_Empty.
Qed.

Theorem inter019: forall (U: Type) (A: Ensemble U) (x: U), A = (Empty_set U) -> Intersection (Singleton x) A = (Empty_set U).
Proof.
intros.
rewrite H.
apply Extensionality_Ensembles.
split.
unfold Included; intros.
compute in H0.
elim H0; intuition.
apply Included_Empty.
Qed.

(* inter + subseteq *)

Theorem inter007: forall (U: Type) (A B: Ensemble U), Included (Intersection A B) A.
Proof.
intros.
auto with sets.
Qed.

Theorem inter008: forall (U: Type) (A B: Ensemble U), Included (Intersection A B) B.
Proof.
intros.
auto with sets.
Qed.

Theorem inter009: forall (U: Type) (A B C: Ensemble U), (Included A C) -> Included (Intersection A B) C.
Proof.
unfold Included.
intros.
apply H; elim H0; intuition.
Qed.

Theorem inter010: forall (U: Type) (A B C: Ensemble U), (Included B C) -> Included (Intersection A B) C.
Proof.
unfold Included.
intros.
apply H; elim H0; intuition.
Qed.

Theorem inter011: forall (U: Type) (A B C: Ensemble U), (Included B C) -> Included (Intersection A B) (Intersection A C).
Proof.
unfold Included.
intros.
split.
elim H0; intuition.
apply H; elim H0; intuition.
Qed.

(* union + inter *)

Theorem inter012: forall (U: Type) (A B: Ensemble U), (Intersection A (Union A B)) = A.
Proof.
intros.
apply Extensionality_Ensembles; unfold Same_set; unfold Included; split.
intros.
elim H; intuition.
intros; compute.
constructor.
assumption.
left.
assumption.
Qed.

Theorem inter013: forall (U: Type) (A B: Ensemble U), (Union A (Intersection A B)) = A.
Proof.
intros.
apply Extensionality_Ensembles; unfold Same_set; unfold Included; split.
intros.
elim H; intros.
assumption.
elim H0; intuition.
intros.
left; intuition.
Qed.

Theorem inter014: forall (U: Type) (A B C: Ensemble U), (Union (Intersection A B) (Intersection A C)) = (Intersection A (Union B C)).
Proof.
intros.
apply Extensionality_Ensembles; unfold Same_set; unfold Included; split.
intros.
elim H; intros.
elim H0; intuition.
elim H0; intuition.
intros.
elim H.
intros.
induction H1.
left.
auto with sets.
right.
auto with sets.
Qed.


Theorem inter015: forall (U: Type) (A B C: Ensemble U), (Intersection (Union A B) (Union A C)) = (Union A (Intersection B C)).
Proof.
intros.
apply Extensionality_Ensembles; unfold Same_set; unfold Included; split.
intros.
elim H; intros.
induction H0.
auto with sets.
induction H1.
auto with sets.
right.
auto with sets.
intros.
induction H.
auto with sets.
induction H.
auto with sets.
Qed.

(* minus *)

Theorem minus001: forall (U: Type) (A B: Ensemble U) (x: U), In (Setminus A B) x = (A x /\ ~ (B x)).
Proof.
intros.
auto with sets.
Qed.

Theorem minus002: forall (U: Type) (A B C: Ensemble U), (Setminus A (Setminus B C)) = (Union (Intersection A C) (Setminus A B)).
Proof.
intros.
apply Extensionality_Ensembles.
unfold Same_set; unfold Included.
split.
intros.
induction H.
unfold In in H0.
unfold Setminus in H0.
cut (~ (In B x) \/ (In C x)).
intros.
elim H1; intros.
right; intuition.
left; intuition.
(* SC: does it really require a classical logic theorem ? *)
cut (~ In B x \/ ~~ In C x).
intros.
elim H1; [ left; assumption | right; apply NNPP; assumption].
apply not_and_or; assumption.
intros.
unfold In in * |- *.
unfold Setminus in * |- *.
induction H.
split.
  elim H; intuition.
compute.
cut (~ (In B x) \/ (In C x)).
intuition.
right; elim H; intuition.
compute.
induction H.
intuition.
Qed.

(* minus + emptyset *)

Theorem minus003: forall (U: Type) (A: Ensemble U), (Setminus A A) = (Empty_set U).
Proof.
intros.
apply Extensionality_Ensembles.
unfold Same_set; unfold Included; compute; split.
intuition.
intuition.
Qed.

Theorem minus004: forall (U: Type) (A: Ensemble U), (Setminus A (Empty_set U)) = A.
Proof.
intros.
apply Extensionality_Ensembles.
unfold Same_set; unfold Included; compute; split; intuition.
Qed.

Theorem minus005: forall (U: Type) (A: Ensemble U), (Setminus (Empty_set U) A) = (Empty_set U).
Proof.
intros.
apply Extensionality_Ensembles.
unfold Same_set; unfold Included; compute; split; intuition.
Qed.

(* minus + subseteq *)

Theorem minus006: forall (U: Type) (A B C: Ensemble U),  (Included B C) -> Included (Setminus B A) (Setminus C A).
Proof.
unfold Included; compute.
intuition.
Qed.

Theorem minus007: forall (U: Type) (A B C: Ensemble U),  (Included B C) -> Included (Setminus A C) (Setminus A B).
Proof.
unfold Included; compute.
intuition.
Qed.

Theorem minus008: forall (U: Type) (A B C: Ensemble U),  (Included A C) -> Included (Setminus A B) C.
Proof.
unfold Included; compute.
intuition.
Qed.

(* minus + union *)

Theorem minus009: forall (U: Type) (A B C: Ensemble U),  (Setminus A (Union B C)) = (Setminus (Setminus A B) C).
Proof.
intros.
apply Extensionality_Ensembles.
unfold Same_set; unfold Included; compute; split.
intros.
elim H; intros.
split.
split.
assumption.
intros.
apply H1; left; assumption.
intros.
apply H1; right; assumption.
intros.
decompose [and] H.
split; [assumption | intro H4; induction H4; [apply H3; assumption | apply H1; assumption]].
Qed.

(* minus + inter *)

Theorem minus010: forall (U: Type) (A B C: Ensemble U),  (Intersection A (Setminus B C)) = (Setminus (Intersection A B) C).
Proof.
intros.
apply Extensionality_Ensembles.
unfold Same_set; unfold Included; split.
intros.
induction H.
induction H0.
split.
intuition.
intuition.
intros.
induction H.
induction H.
intuition.
Qed.

Theorem minus011: forall (U: Type) (A B: Ensemble U), (Intersection (Setminus A B) B) = (Empty_set U).
Proof.
intros.
apply Extensionality_Ensembles.
unfold Same_set; unfold Included; split.
intros.
induction H.
induction H.
intuition.
intros.
elim H.
Qed.

(* part1 *)

Theorem part1001: forall (U: Type) (A X: Ensemble U), In (Power_set1 A) X <-> ((In (Power_set A) X) /\ X <> (Empty_set U)).
Proof.
intros.
unfold Power_set1.
unfold Subtract.
unfold Setminus.
split.
intros.
induction H.
split; [ assumption | ].
intro H1; apply H0; intros.
intuition.
intros.
induction H; intros.
red.
split; [ intuition | ].
compute.
intros H2; apply H0; intuition.
Qed.


(**
  The BPhoX (unsolved) version had [Setminus (Power_set A) (Empty_set (Ensemble U))].

  It was wrong because of a subtlety: in the solved case it tried to
  remove nothing, while in the correct case below, it removes the
  element that is the empty_set over elements of U. Subtle, I told
  you.
  *)
Theorem part1002: forall (U: Type) (A: Ensemble U), (Power_set1 A) = Setminus (Power_set A) (Singleton (Empty_set U)).
Proof.
intuition.
Qed.

