(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bchapter2.
Require Import Constructive_sets.
Require Import Classical_sets.
Require Import Bgeneralized_union_inter.
Require Import Bfinite_subsets.
Require Import Finite_sets_facts.
Require Import Bnaturals_basics.
Require Import Bnaturals_min.
Require Import Bnaturals_iterate.
Require Import Arith.

Open Local Scope nat_scope.

(**
  * Cardinal
  *)

(**
  The cardinal is defined as per Bcardinal in Bnaturals_basics. Reminder:

  [Definition Bcardinal (S: Type) (s: Ensemble S) :=
    iota _ inhabited_nat (fun n => cardinal S s n).]
*)

Theorem iterated_genfin_applicable: forall (S: Type) (s: Ensemble S) (t: Ensemble (Ensemble S)) (n: nat),
  In (domain (genfin s)) t ->
  In (partial_function (Power_set (Power_set s)) (Power_set (Power_set s))) (iter (Power_set (Power_set s)) (genfin s) n)
/\ In (domain (iter (Power_set (Power_set s)) (genfin s) n)) t.
Proof.
intros S s t n H.
generalize (Bfinite_is_total S s); intros.
generalize (iterate_total_function _ _ (genfin s)  H0 n); intros.
inversion H1.
rewrite H3.
split.
assumption.
inversion H.
induction H5.
inversion H5.
assumption.
Qed.

Theorem empty_set_singleton_in_genfin_domain: forall (S: Type) (s: Ensemble S),
  In (domain (genfin s)) (Singleton (Empty_set S)).
Proof.
intros S s.
constructor.
exists (Union (Singleton (Empty_set S)) (image (add s) (times s (Singleton (Empty_set S))))).
constructor.
constructor; red; intros.
inversion H.
constructor; red; intros; contradiction.
reflexivity.
Qed.

(** Another version of previous_theorems *)

Theorem Easy_laws_funct_02: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U)
  (p: Ensemble (S*T)) (q: Ensemble (T*U)) (x: S),
  In (partial_function s t) p -> In (partial_function t u) q -> In (domain (composition p q)) x ->
  forall (applicable_pq : (In (partial_function s u) (composition p q) /\ In (domain (composition p q)) x))
  (applicable_p:(In (partial_function s t) p /\ In (domain p) x))
  (applicable_q:(In (partial_function t u) q /\ In (domain q) (app p x applicable_p))),
  app (composition p q) x applicable_pq = app q (app p x applicable_p) applicable_q.
Proof.
intros S T U s t u p q x H H0 H1 Hpq Hp Hq.
symmetry; apply identifying_app.
constructor; exists (app p x Hp).
split.
apply app_trivial_property.
apply app_trivial_property.
Qed.

Theorem Easy_laws_funct_03: forall (S: Type) (s: Ensemble S) (x: S)
  (_: In s x) (applicable_id:(In (partial_function s s) (id s) /\ In (domain (id s)) x)),
  app (id(s)) x applicable_id = x.
Proof.
intros S s x H H0.
symmetry; apply identifying_app.
constructor; [ constructor; assumption | reflexivity ].
Qed.

Theorem genfin_application_adds_elements: forall (S: Type) (r: Ensemble (Ensemble S)) (s y: Ensemble S) (x: S),
  Included r (Power_set s) -> In r y -> Included (Add S y x) s ->
  exists H:(In (partial_function (Power_set (Power_set s)) (Power_set (Power_set s))) (genfin s) /\ In (domain (genfin s)) r),
  In (app (genfin s) r H) (Add S y x).
Proof.
intros S r s y x H H0 H1.
generalize (Bfinite_is_total S s); intros.
inversion H2.
assert (In (partial_function (Power_set (Power_set s)) (Power_set (Power_set s))) (genfin s) /\ In (domain (genfin s)) r).
rewrite H4.
split; [ intuition | constructor; assumption ].
exists H6.
unfold genfin.
apply abstract_lambda.
right.
red; apply image_intro with (x,y).
split.
constructor.
apply H1.
right; intuition.
assumption.
constructor.
constructor.
apply H1; right; intuition.
apply H; assumption.
apply Extensionality_Ensembles; split; red; intros.
induction H7.
right; assumption.
left; assumption.
induction H7.
right; assumption.
left; assumption.
Qed.

Theorem genfin_application_adds_elements_noex: forall (S: Type) (r: Ensemble (Ensemble S)) (s y: Ensemble S) (x: S)
  (H: (In (partial_function (Power_set (Power_set s)) (Power_set (Power_set s))) (genfin s) /\ In (domain (genfin s)) r)),
  Included r (Power_set s) -> In r y -> Included (Add S y x) s ->
  In (app (genfin s) r H) (Add S y x).
Proof.
intros S r s y x H H0 H1 H2.
generalize (genfin_application_adds_elements S r s y x H0 H1 H2); intros.
induction H3.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ r H x0).
assumption.
Qed.

Theorem genfin_iterated_zero: forall (S: Type) (s: Ensemble S),
   app (iter (Power_set (Power_set s)) (genfin s) 0) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) 0 (empty_set_singleton_in_genfin_domain S s))
  = (Singleton (Empty_set S)).
Proof.
intros S s.
simpl.
rewrite Easy_laws_funct_03.
reflexivity.
constructor; red; intros.
inversion H.
constructor; red; intros; contradiction.
Qed.

Theorem genfin_iterated_succ_end: forall (S: Type) (s: Ensemble S) (n: nat),
   app (iter (Power_set (Power_set s)) (genfin s) (Datatypes.S n)) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) (Datatypes.S n) (empty_set_singleton_in_genfin_domain S s))
  =
  Union (Singleton (Empty_set S))
    (image (add s) (times s (app (iter (Power_set (Power_set s)) (genfin s) n) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) n (empty_set_singleton_in_genfin_domain S s)))))
.
Proof.
intros S s n.
generalize (Bfinite_is_total S s); intros.
assert (In (partial_function (Power_set (Power_set s)) (Power_set (Power_set s))) (genfin s) /\ In (domain (genfin s)) (Singleton (Empty_set S))).
inversion H.
rewrite H1.
split; [ assumption | ].
constructor; red; intros.
inversion H3.
constructor; red; intros; contradiction.
assert (
  In (partial_function (Power_set (Power_set s)) (Power_set (Power_set s)))
    (iter (Power_set (Power_set s)) (genfin s) n)
/\ In (domain (iter (Power_set (Power_set s)) (genfin s) n)) (Singleton (Empty_set S))).
split.
apply iterate_partial_function; intuition.
generalize (iterate_total_function _ (Power_set (Power_set s)) (genfin s) H n); intros.
inversion H1.
rewrite H3.
constructor; red; intros.
inversion H5.
constructor; red; intros; contradiction.
assert (
  In (partial_function (Power_set (Power_set s)) (Power_set (Power_set s))) (genfin s)
/\ In (domain (genfin s)) (app (iter (Power_set (Power_set s)) (genfin s) n) (Singleton (Empty_set S)) H1)).
split.
exact (proj1 H0).
inversion H; rewrite H3.
apply application_in_codomain.
assert (
  In (partial_function (Power_set (Power_set s)) (Power_set (Power_set s)))
    (composition (iter (Power_set (Power_set s)) (genfin s) n) (genfin s))
/\ In (domain (composition (iter (Power_set (Power_set s)) (genfin s) n) (genfin s))) (Singleton (Empty_set S))).
split.
apply Membership_laws_08 with (Power_set (Power_set s)).
split.
apply iterate_partial_function.
exact (proj1 H0).
exact (proj1 H0).
assert (domain (composition (iter (Power_set (Power_set s)) (genfin s) n) (genfin s)) = domain (iter (Power_set (Power_set s)) (genfin s) n)).
apply Equality_laws_dom_04 with (Power_set (Power_set s)) (Power_set (Power_set s)) (Power_set (Power_set s)).
split.
split.
inversion H1.
inversion H3; assumption.
inversion H0.
inversion H3; assumption.
inversion H.
rewrite H4.
red; intros.
inversion H6.
induction H7.
inversion H1.
inversion H9.
inversion H11.
inversion H14.
generalize (H16 (x0, x) H7); intros.
inversion H18.
assumption.
rewrite H3.
exact (proj2 H1).
replace
  (app (iter (Power_set (Power_set s)) (genfin s) (Datatypes.S n)) (Singleton (Empty_set S))
     (iterated_genfin_applicable S s (Singleton (Empty_set S)) (Datatypes.S n) (empty_set_singleton_in_genfin_domain S s)))
  with
  (app (composition (iter (Power_set (Power_set s)) (genfin s) n) (genfin s)) (Singleton (Empty_set S)) H3)
.
rewrite Easy_laws_funct_02 with
  (Ensemble (Ensemble S)) (Ensemble (Ensemble S)) (Ensemble (Ensemble S))
  (Power_set (Power_set s)) (Power_set (Power_set s)) (Power_set (Power_set s))
  (iter (Power_set (Power_set s)) (genfin s) n)
  (genfin s)
  (Singleton (Empty_set S))
  H3
  H1
  H2.
unfold genfin at 1; apply abstract_lambda.
rewrite (irrelevant_applicability  _ _ _ _ _ _ _ (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) n (empty_set_singleton_in_genfin_domain S s)) H1).
reflexivity.
exact (proj1 H1).
exact (proj1 H0).
exact (proj2 H3).
apply identifying_app.
rewrite iterate_successor.
apply app_trivial_property.
inversion H0.
inversion H4; assumption.
Qed.

Theorem finite_subsets_QuantifiedUnion_left: forall (S: Type) (s: Ensemble S),
  Included (Power_finite s)
  (QuantifiedUnion (Full_set nat)
  (fun n =>
    app (iter (Power_set (Power_set s)) (genfin s) n) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) n (empty_set_singleton_in_genfin_domain S s)))).
Proof.
intros S s.
red; intros.
induction H.
induction H.
constructor.
exists 0.
split.
constructor.
rewrite genfin_iterated_zero; intuition.
assert (Included A s); [ intuition | ].
generalize (IHFinite H2); intros.
constructor.
do 3 (induction H3).
exists (Datatypes.S x0).
split; [ constructor | ].
rewrite genfin_iterated_succ_end.
right.
apply image_intro with (x, y).
split.
constructor; intuition.
constructor.
constructor; [ | constructor ]; intuition.
unfold Add; rewrite commutativity_1; reflexivity.
Qed.

Theorem any_iteration_of_genfin_is_finite: forall (S: Type) (s: Ensemble S) (n: nat),
  forall (y: Ensemble S),
  In (app (iter (Power_set (Power_set s)) (genfin s) n) (Singleton (Empty_set S))
         (iterated_genfin_applicable S s (Singleton (Empty_set S)) n (empty_set_singleton_in_genfin_domain S s))
      ) y ->
  In (Power_finite s) y.
Proof.
intros S s n.
induction n.
rewrite genfin_iterated_zero; intuition.
inversion H.
constructor.
left.
intuition.
intros y H; rewrite genfin_iterated_succ_end in H.
constructor.
induction H.
inversion H; left.
induction H.
inversion H.
inversion H1.
inversion H0.
assert (In b a \/ ~(In b a)); [ apply classic | ].
induction H9.
replace y with b.
generalize (IHn b H7); intros.
induction H10; assumption.
rewrite H5.
rewrite <- H8; simpl.
symmetry; rewrite commutativity_1.
apply absorption_4.
red; intros.
inversion H10.
rewrite <- H11; assumption.
replace y with (Add S b a).
right.
generalize (IHn b H7); intros.
induction H10; assumption.
assumption.
rewrite H5; unfold Add; rewrite commutativity_1.
rewrite <- H8; simpl; reflexivity.
induction H.
inversion H; red; intros; contradiction.
induction H.
induction H.
inversion H0.
inversion H3.
rewrite <- H7 in H4; simpl in H4.
rewrite H4; red; intros.
induction H8.
inversion H8; rewrite <- H9; assumption.
induction H6; apply H6; assumption.
Qed.


Theorem finite_subsets_QuantifiedUnion_right: forall (S: Type) (s: Ensemble S),
  Included
  (QuantifiedUnion (Full_set nat)
   (fun n =>
     app (iter (Power_set (Power_set s)) (genfin s) n) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) n (empty_set_singleton_in_genfin_domain S s))))
  (Power_finite s).
Proof.
intros S s.
red; intros.
do 3 (induction H).
apply any_iteration_of_genfin_is_finite with x.
assumption.
Qed.

(**
  property 3.5.14, BBook: section 3.5.9, p.167
  *)
Theorem finite_subsets_QuantifiedUnion: forall (S: Type) (s: Ensemble S),
  Power_finite s =
  QuantifiedUnion (Full_set nat)
  (fun n =>
    app (iter (Power_set (Power_set s)) (genfin s) n) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) n (empty_set_singleton_in_genfin_domain S s))).
Proof.
intros S s.
(* not specifying the arguments does not work, go figure... *)
apply (Extensionality_Ensembles _ (Power_finite s) (QuantifiedUnion (Full_set nat)
  (fun n =>
    app (iter (Power_set (Power_set s)) (genfin s) n) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) n (empty_set_singleton_in_genfin_domain S s))))).
split; red; intros.
apply finite_subsets_QuantifiedUnion_left; assumption.
apply finite_subsets_QuantifiedUnion_right; assumption.
Qed.

(**
  consequence of property 3.5.14, BBook: section 3.5.9, p.168
  *)
Theorem genfin_iterations_not_empty_for_given_finite_set: forall (S: Type) (s t: Ensemble S),
  In (Power_finite s) t
  <->
  (Comprehension (fun n => In (app (iter (Power_set (Power_set s)) (genfin s) n) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) n (empty_set_singleton_in_genfin_domain S s))) t))
  <> (Empty_set nat).
Proof.
intros S s.
split.
intros.
rewrite finite_subsets_QuantifiedUnion in H.
do 3 (induction H).
apply (Inhabited_not_empty nat).
apply Inhabited_intro with x.
assumption.
intros.
rewrite finite_subsets_QuantifiedUnion.
constructor.
generalize (not_empty_Inhabited nat _ H); intros.
induction H0.
exists x.
split.
constructor.
assumption.
Qed.

(**
  ** Cardinal: validity
  *)

Theorem Bcardinal_cardinal: forall (S: Type) (s: Ensemble S),
  Finite s -> cardinal S s (Bcardinal S s).
Proof.
intros S s H.
induction H.
replace (Bcardinal S (Empty_set S)) with O.
left.
unfold Bcardinal; apply iota_ind.
intros b H.
induction H as [ H H0 ].
symmetry; apply H0.
left.
exists O.
split.
left.
intros x' H.
apply cardinal_Empty with S; assumption.
replace (Bcardinal S (Add S A x)) with (Datatypes.S (Bcardinal S A)).
right; assumption.
unfold Bcardinal at 2; apply iota_ind.
intros b H1.
induction H1 as [ H1 H2 ].
symmetry; apply H2.
right; assumption.
exists (Datatypes.S (Bcardinal S A)).
split.
right; assumption.
intros x' H1.
apply cardinal_unicity with S (Add S A x).
right; assumption.
assumption.
Qed.

Theorem Bcardinal_cardinal_B: forall (S: Type) (s t: Ensemble S),
  In (Power_finite s) t -> cardinal S t (Bcardinal S t).
Proof.
intros S s t H.
destruct H.
apply Bcardinal_cardinal; assumption.
Qed.

Theorem Bcardinal_implies_cardinal: forall (S: Type) (s: Ensemble S) (n: nat),
  Finite s -> Bcardinal S s = n -> cardinal S s n.
Proof.
intros S s n H H0.
rewrite <- H0; apply Bcardinal_cardinal.
assumption.
Qed.

Theorem Bcardinal_implies_cardinal_B: forall (S: Type) (s t: Ensemble S) (n: nat),
  In (Power_finite s) t -> Bcardinal S t = n -> cardinal S t n.
Proof.
intros S s t n H H0.
destruct H; apply Bcardinal_implies_cardinal; assumption.
Qed.

Theorem cardinal_implies_Bcardinal: forall (S: Type) (s: Ensemble S) (n: nat),
  Finite s -> cardinal S s n -> Bcardinal S s = n.
Proof.
intros S s n H H0.
apply cardinal_unicity with S s.
apply Bcardinal_cardinal; assumption.
assumption.
Qed.

Theorem cardinal_implies_Bcardinal_B: forall (S: Type) (s t: Ensemble S) (n: nat),
  In (Power_finite s) t -> cardinal S t n -> Bcardinal S t = n.
Proof.
intros S s t n H H0.
destruct H.
apply cardinal_implies_Bcardinal; assumption.
Qed.

Theorem Bcardinal_O: forall (U: Type), Bcardinal U (Empty_set U) = 0%nat.
Proof.
intros U.
apply cardinal_implies_Bcardinal; left.
Qed.

Theorem Bcardinal_S: forall (U: Type) (u: Ensemble U) (n: nat),
  Finite u -> Bcardinal U u = n -> forall (x: U), ~(In u x) -> Bcardinal U (Add U u x) = (S n).
Proof.
intros U u n H H0 x H1.
apply cardinal_implies_Bcardinal.
right; assumption.
right; [ | assumption ].
apply Bcardinal_implies_cardinal; assumption.
Qed.

(**
  Note/TODO: the following theorems about Bmin should rather appear in
  Bnaturals_min.
  *)

Theorem minimum_property_implies_is_minimum: forall (s: Ensemble nat) (x: nat),
  In s x -> (forall (n: nat), In s n -> x <= n) -> x = Bmin s.
Proof.
intros s x H H0.
unfold Bmin; apply iota_ind.
intros.
induction H1.
symmetry; apply H2; intuition.
exists x.
split.
intuition.
intros.
induction H1.
apply le_antisym.
apply H0; assumption.
apply H2; assumption.
Qed.


(**
  Note: this theorem depends on the dec_inh_nat_subset_has_unique_least_element
  lemma put in Arith/Wf_nat as of coq 8.2 rc2. Use with coq 8.1 will probably
  require the inclusion of the Logic/ChoiceFacts module, where this lemma
  was defined initially.
  *)
Theorem minimum_implies_minimum_property: forall (s: Ensemble nat) (x: nat),
  In s x -> x = Bmin s -> (forall (n: nat), In s n -> x <= n).
Proof.
intros s x H H0.
assert (forall (z: nat), In s z \/ ~(In s z)); [ intros z; apply classic | ].
assert (exists x, In s x); [ exists x; assumption | ].
generalize (dec_inh_nat_subset_has_unique_least_element s H1 H2); intros.
assert (In s (Bmin s) /\ (forall (y: nat), In s y -> Bmin s <= y)).
pattern (Bmin s).
unfold Bmin; apply iota_ind.
intros.
induction H5.
assumption.
unfold has_unique_least_element in H3.
exact H3.
induction H5.
rewrite H0; apply H6.
assumption.
Qed.

Theorem valid_cardinal_aux1: forall (S: Type) (s t: Ensemble S), In (Power_finite s) t ->
  In (app (iter (Power_set (Power_set s)) (genfin s) (Bcardinal S t)) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) (Bcardinal S t) (empty_set_singleton_in_genfin_domain S s))) t.
Proof.
intros S s t H.
induction H.
induction H.
replace (Bcardinal S (Empty_set S)) with O.
rewrite genfin_iterated_zero; intuition.
apply cardinal_Empty with S.
apply Bcardinal_cardinal.
left.
replace (Bcardinal S (Add S A x)) with (Datatypes.S (Bcardinal S A)).
rewrite genfin_iterated_succ_end.
right.
apply image_intro with (x, A).
constructor.
constructor.
intuition.
apply IHFinite.
intuition.
constructor.
constructor; [ | constructor ]; intuition.
unfold Add; rewrite commutativity_1; reflexivity.
symmetry; apply cardinal_implies_Bcardinal.
right; assumption.
right.
apply Bcardinal_cardinal.
assumption.
assumption.
Qed.

Theorem valid_cardinal_aux2: forall (S: Type) (s t: Ensemble S) (n: nat),
  In (Power_finite s) t ->
  Included
    (app (iter (Power_set (Power_set s)) (genfin s) n) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) n (empty_set_singleton_in_genfin_domain S s)))
    (app (iter (Power_set (Power_set s)) (genfin s) (Datatypes.S n)) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) (Datatypes.S n) (empty_set_singleton_in_genfin_domain S s)))
.
Proof.
intros S s t n H.
induction n.
rewrite genfin_iterated_succ_end.
rewrite genfin_iterated_zero.
intuition.
red; intros.
rewrite genfin_iterated_succ_end in H0.
induction H0.
rewrite genfin_iterated_succ_end.
left.
assumption.
induction H0.
induction H0.
induction H0.
generalize (IHn b H2); intros.
rewrite genfin_iterated_succ_end.
right.
apply image_intro with (a, b).
split ; [ | assumption ].
constructor; assumption.
Qed.

Theorem valid_cardinal_aux3: forall (S: Type) (s t: Ensemble S) (n m: nat),
  In (Power_finite s) t ->
  n <= m ->
  Included
    (app (iter (Power_set (Power_set s)) (genfin s) n) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) n (empty_set_singleton_in_genfin_domain S s)))
    (app (iter (Power_set (Power_set s)) (genfin s) m) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) m (empty_set_singleton_in_genfin_domain S s)))
.
Proof.
intros S s t n m H H0.
induction H0.
intuition.
red; intros.
generalize (IHle _ H1); intros.
generalize (valid_cardinal_aux2 S s t m H _ H2).
intuition.
Qed.

Theorem valid_cardinal_aux4: forall (S: Type) (s: Ensemble S) (n: nat),
  (forall (t: Ensemble S),
  In (Power_finite s) t ->
  In (app (iter (Power_set (Power_set s)) (genfin s) n) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) n (empty_set_singleton_in_genfin_domain S s))) t
  -> Bcardinal S t <= n).
Proof.
intros S s n.
induction n.
rewrite genfin_iterated_zero.
intros.
inversion H0.
replace (Bcardinal S (Empty_set S)) with O.
intuition.
apply cardinal_Empty with S.
apply Bcardinal_cardinal.
left.
rewrite genfin_iterated_succ_end; intros.
inversion H0.
inversion H1.
replace (Bcardinal S (Empty_set S)) with O.
intuition.
apply cardinal_Empty with S.
apply Bcardinal_cardinal.
left.
inversion H1.
induction H3.
induction H3.
assert (Bcardinal S b <= n).
apply IHn.
apply any_iteration_of_genfin_is_finite with n.
assumption.
assumption.
inversion H5.
assert (In b a \/ ~(In b a)); [ apply classic | ].
induction H12.
replace t with b.
apply le_trans with n; intuition.
rewrite H11; symmetry; rewrite commutativity_1.
apply absorption_4.
red; intros; inversion H13.
rewrite <- H14; assumption.
replace (Bcardinal S t) with (Datatypes.S (Bcardinal S b)).
intuition.
symmetry; apply cardinal_implies_Bcardinal.
destruct H; assumption.
replace t with (Add S b a).
right.
apply Bcardinal_cardinal.
specialize (any_iteration_of_genfin_is_finite S s n b H6); intros H13; destruct H13; assumption.
assumption.
unfold Add; rewrite H11; rewrite commutativity_1; reflexivity.
Qed.

(**
  BBook: section 3.5.9, p.168, array 1

  Note/TODO: I am not that much happy with this proof, I might have built too much
  helping theorems, or there is a more elegant proof using them (exploiting
  "n=m <-> (n <= m /\ n >=m)" or something along those lines).
*)
Theorem valid_cardinal: forall (S: Type) (s t: Ensemble S), In (Power_finite s) t ->
  Bcardinal S t = Bmin (Comprehension (fun n => In (app (iter (Power_set (Power_set s)) (genfin s) n) (Singleton (Empty_set S)) (iterated_genfin_applicable S s (Singleton (Empty_set S)) n (empty_set_singleton_in_genfin_domain S s))) t)).
Proof.
intros S s t H.
induction H.
induction H.
replace (Bcardinal S (Empty_set S)) with O.
apply minimum_property_implies_is_minimum.
do 2 red.
rewrite genfin_iterated_zero; intuition.
intros; intuition.
apply cardinal_Empty with S.
apply Bcardinal_cardinal.
left.
assert (Included A s); [ intuition | ].
generalize (IHFinite H2); intros; clear IHFinite.
assert ((Bcardinal S (Add S A x)) = (Datatypes.S (Bcardinal S A))).
apply cardinal_implies_Bcardinal.
right; assumption.
right; [ | assumption ].
apply Bcardinal_cardinal; assumption.
rewrite H4.
rewrite H3.
apply minimum_property_implies_is_minimum.
do 2 red.
rewrite genfin_iterated_succ_end.
right.
apply image_intro with (x, A).
constructor.
constructor.
intuition.
rewrite <- H3.
apply valid_cardinal_aux1.
constructor; intuition.
constructor.
constructor; [ | constructor ]; intuition.
unfold Add; rewrite commutativity_1; reflexivity.
intros.
do 2 (red in H5).
rewrite <- H3.
rewrite <- H4.
apply valid_cardinal_aux4 with s.
constructor.
right; assumption.
assumption.
assumption.
Qed.

