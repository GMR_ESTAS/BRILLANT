(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Bderived_constructs.

Open Scope eB_scope.

(**
  commutativity of union, BBook: section 2.3.4, p.75, array 1, row 1, line 1
  *)
Theorem commutativity_1: forall (U: Type) (a b: Ensemble U), (a ∪ b) = (b ∪ a).
Proof.
intros U a b.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H; [ right; intuition | left; intuition ].
induction H; [ right; intuition | left; intuition ].
Qed.

(**
  commutativity of intersection, BBook: section 2.3.4, p.75, array 1, row 1, line 2
  *)
Theorem commutativity_2: forall (U: Type) (a b: Ensemble U), (a ∩ b) = (b ∩ a).
Proof.
intros U a b.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H; constructor; intuition.
induction H; constructor; intuition.
Qed.

(**
  associativity of union, BBook: section 2.3.4, p.75, array 1, row 2, line 1
  *)
Theorem associativity_1: forall (U: Type) (a b c: Ensemble U), ((a ∪ b) ∪ c) = (a ∪ (b ∪ c)).
Proof.
intros U a b c.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H.
left; intuition.
right; left; intuition.
right; right; intuition.
induction H.
left; left; intuition.
induction H.
left; right; intuition.
right; intuition.
Qed.

(**
  associativity of intersection, BBook: section 2.3.4, p.75, array 1, row 2, line 2
  *)
Theorem associativity_2: forall (U: Type) (a b c: Ensemble U), ((a ∩ b) ∩ c) = (a ∩ (b ∩ c)).
Proof.
intros U a b c.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H.
constructor; [ intuition | constructor; intuition ].
induction H.
induction H0.
constructor; [ constructor; intuition | intuition ].
Qed.

(**
  distributivity of intersection over union, BBook: section 2.3.4, p.75, array 1, row 3, line 1
  *)
Theorem distributivity_1: forall (U: Type) (a b c: Ensemble U), (a ∩ (b ∪ c)) = ((a ∩ b) ∪ (a ∩ c)).
Proof.
intros U a b c.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H0.
left; constructor; intuition.
right; constructor; intuition.
induction H; induction H.
constructor; [ intuition | left; intuition ].
constructor; [ intuition | right; intuition ].
Qed.

(**
  distributivity of union over intersection, BBook: section 2.3.4, p.75, array 1, row 3, line 2
  *)
Theorem distributivity_2: forall (U: Type) (a b c: Ensemble U), (a ∪ (b ∩ c)) = ((a ∪ b) ∩ (a ∪ c)).
Proof.
intros U a b c.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
constructor; left; intuition.
induction H.
constructor; right; intuition.
induction H.
induction H.
left; intuition.
induction H0.
left; intuition.
right; constructor; intuition.
Qed.

(**
  pseudo-distributivity of set difference over union, BBook: section 2.3.4, p.75, array 1, row 3, line 3
  *)
Theorem distributivity_3: forall (U: Type) (a b c: Ensemble U), (a ∖ (b ∪ c)) = ((a ∖ b) ∩ (a ∖ c)).
Proof.
intros U a b c.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
constructor; constructor.
intuition.
intros H1; apply H0; left; intuition.
intuition.
intros H1; apply H0; right; intuition.
induction H.
induction H; induction H0.
constructor.
intuition.
intros H3; induction H3; [ apply H1; intuition | apply H2; intuition ].
Qed.

(**
  pseudo-distributivity of set difference over intersection, BBook: section 2.3.4, p.75, array 1, row 3, line 4
  *)
Theorem distributivity_4: forall (U: Type) (a b c: Ensemble U), (a ∖ (b ∩ c)) = ((a ∖ b) ∪ (a ∖ c)).
Proof.
intros U a b c.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
assert ( ~(In b x) \/ ~(In c x)).
apply not_and_or.
intros H1; apply H0; constructor; intuition.
induction H1.
left; constructor; intuition.
right; constructor; intuition.
induction H; induction H.
constructor.
intuition.
intros H1; induction H1; apply H0; intuition.
constructor.
intuition.
intros H1; induction H1; apply H0; intuition.
Qed.

(**
  pseudo-distributivity of set difference over set difference, BBook: section 2.3.4, p.75, array 1, row 3, line 5
  *)
Theorem distributivity_5: forall (U: Type) (a b c: Ensemble U), (a ∖ (b ∖ c)) = ((a ∖ b) ∪ (a ∩ c)).
Proof.
intros U a b c.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
assert (~(In b x) \/ (In c x)).
apply imply_to_or.
intros.
apply NNPP; intros H2.
apply H0.
constructor; intuition.
induction H1.
left; constructor; intuition.
right; constructor; intuition.
induction H; induction H.
constructor.
intuition.
intros H1; apply H0.
induction H1; intuition.
constructor.
intuition.
intros H1.
induction H1; contradiction.
Qed.

(**
  pseudo-distributivity of union over set difference, BBook: section 2.3.4, p.75, array 1, row 3, line 6
  *)
Theorem distributivity_6: forall (U: Type) (a b c: Ensemble U), ((a ∪ b) ∖ c) = ((a ∖ c) ∪ (b ∖ c)).
Proof.
intros U a b c.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H.
left; constructor; intuition.
right; constructor; intuition.
induction H; induction H.
constructor.
left; intuition.
intuition.
constructor.
right; intuition.
intuition.
Qed.

(**
  pseudo-distributivity of intersection over set difference, BBook: section 2.3.4, p.75, array 1, row 3, line 7
  *)
Theorem distributivity_7: forall (U: Type) (a b c: Ensemble U), ((a ∩ b) ∖ c) = ((a ∖ c) ∩ b).
Proof.
intros U a b c.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H; induction H.
constructor; [ constructor; intuition | intuition].
induction H; induction H.
constructor; [ constructor; intuition | intuition ].
Qed.

(**
  distributivity of pair over union (left), BBook: section 2.3.4, p.75, array 1, row 3, line 8
  *)
Theorem distributivity_8: forall (U V: Type) (a b: Ensemble U) (t: Ensemble V), ((a ∪ b) × t) = ((a × t) ∪ (b × t)).
Proof.
intros U V a b t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H.
left; constructor; intuition.
right; constructor; intuition.
induction H.
induction H; constructor; [ left; intuition | intuition ].
induction H; constructor; [ right; intuition | intuition ].
Qed.

(**
  distributivity of pair over intersection (left), BBook: section 2.3.4, p.75, array 1, row 3, line 9
  *)
Theorem distributivity_9: forall (U V: Type) (a b: Ensemble U) (t: Ensemble V), ((a ∩ b) × t) = ((a × t) ∩ (b × t)).
Proof.
intros U V a b t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H.
constructor; constructor; intuition.
induction H.
inversion H.
inversion H0.
assert (a0 = a1).
assert ((a0, b0) = (a1, b1)).
rewrite H3; rewrite H6; intuition.
injection H7; intuition.
constructor.
constructor; [ intuition | rewrite H7; intuition ].
intuition.
Qed.

(**
  distributivity of pair over set difference (left), BBook: section 2.3.4, p.75, array 1, row 3, line 10
  *)
Theorem distributivity_10: forall (U V: Type) (a b: Ensemble U) (t: Ensemble V), ((a ∖ b) × t) = ((a × t) ∖ (b × t)).
Proof.
intros U V a b t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H.
constructor.
constructor; intuition.
intros H2; apply H1; inversion H2; intuition.
induction H.
induction H.
constructor.
constructor.
intuition.
intros H2; apply H0; constructor; intuition.
intuition.
Qed.

(**
  distributivity of pair over union (right), BBook: section 2.3.4, p.75, array 1, row 3, line 11
  *)
Theorem distributivity_11: forall (U V: Type) (a b: Ensemble U) (t: Ensemble V), (t × (a ∪ b)) = ((t × a) ∪ (t × b)).
Proof.
intros U V a b t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H0.
left; constructor; intuition.
right; constructor; intuition.
induction H; induction H.
constructor; [ intuition | left; intuition ].
constructor; [ intuition | right; intuition ].
Qed.

(**
  distributivity of pair over intersection (right), BBook: section 2.3.4, p.75, array 1, row 3, line 12
  *)
Theorem distributivity_12: forall (U V: Type) (a b: Ensemble U) (t: Ensemble V), (t × (a ∩ b)) = ((t × a) ∩ (t × b)).
Proof.
intros U V a b t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H0.
constructor; constructor; intuition.
induction H.
induction H; inversion H0.
constructor; intuition.
Qed.

(**
  distributivity of pair over set difference (right), BBook: section 2.3.4, p.75, array 1, row 3, line 13
  *)
Theorem distributivity_13: forall (U V: Type) (a b: Ensemble U) (t: Ensemble V), (t × (a ∖ b)) = ((t × a) ∖ (t × b)).
Proof.
intros U V a b t.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
induction H0.
constructor.
constructor; intuition.
intros H2; apply H1; inversion H2; intuition.
induction H.
induction H.
constructor.
intuition.
constructor.
intuition.
intros H2; apply H0; constructor; intuition.
Qed.

(**
  excluded middle with union, BBook: section 2.3.4, p.76, array 1, row 1, line 1
  *)
Theorem excluded_middle_1: forall (U: Type) (s a: Ensemble U), Included a s -> (a ∪ (s ∖ a)) = s.
Proof.
intros U s a H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0.
apply H; intuition.
induction H0; intuition.
assert (In a x \/ In (Setminus s a) x).
apply NNPP; intros H1.
generalize (not_or_and (In a x) (In (Setminus s a) x) H1); intros H2.
induction H2.
apply H3; constructor; intuition.
induction H1.
left; intuition.
right; intuition.
Qed.

(**
  excluded middle with intersection, BBook: section 2.3.4, p.76, array 1, row 1, line 2
  *)
Theorem excluded_middle_2: forall (U: Type) (s a: Ensemble U), Included a s -> (a ∩ (s ∖ a)) = ∅.
Proof.
intros U s a H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0.
induction H1; contradiction.
contradiction.
Qed.

(**
  idempotence of union, BBook: section 2.3.4, p.76, array 1, row 2, line 1
  *)
Theorem idempotence_1: forall (U: Type) (a: Ensemble U), (a ∪ a) = a.
Proof.
intros U a.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H; intuition.
left; intuition.
Qed.

(**
  idempotence of intersection, BBook: section 2.3.4, p.76, array 1, row 2, line 2
  *)
Theorem idempotence_2: forall (U: Type) (a: Ensemble U), (a ∩ a) = a.
Proof.
intros U a.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H; intuition.
split; intuition.
Qed.

(**
  neutral element of union, BBook: section 2.3.4, p.76, array 1, row 3, line 1
  *)
Theorem neutral_element_1: forall (U: Type) (a: Ensemble U), (a ∪ ∅) = a.
Proof.
intros U a.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H; [ intuition | contradiction ].
left; intuition.
Qed.

(**
  neutral element of intersection, BBook: section 2.3.4, p.76, array 1, row 3, line 2
  *)
Theorem neutral_element_2: forall (U: Type) (s a: Ensemble U), Included a s -> (a ∩ s) = a.
Proof.
intros U s a H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0; intuition.
intuition.
Qed.

(**
  absorption of union, BBook: section 2.3.4, p.76, array 1, row 4, line 1
  *)
Theorem absorption_1: forall (U: Type) (a b: Ensemble U), (a ∩ (a ∪ b)) = a.
Proof.
intros U a b.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H; intuition.
constructor; [ intuition | left; intuition].
Qed.

(**
  absorption of intersection, BBook: section 2.3.4, p.76, array 1, row 4, line 2
  *)
Theorem absorption_2: forall (U: Type) (a b: Ensemble U), (a ∪ (a ∩ b)) = a.
Proof.
intros U a b.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
intuition.
induction H; intuition.
left; intuition.
Qed.

(**
  absorption of intersection by empty set, BBook: section 2.3.4, p.76, array 1, row 4, line 3
  *)
Theorem absorption_3: forall (U: Type) (a: Ensemble U), (∅ ∩ a) = ∅.
Proof.
intros U a.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H; intuition.
contradiction.
Qed.

(**
  absorption of union by superset, BBook: section 2.3.4, p.76, array 1, row 4, line 4
  *)
Theorem absorption_4: forall (U: Type) (s a: Ensemble U), Included a s -> (s ∪ a) = s.
Proof.
intros U a b H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
induction H0.
intuition.
apply H; intuition.
left; intuition.
Qed.

(**
  absorption of set difference by empty set, BBook: section 2.3.4, p.76, array 1, row 4, line 5
  *)
Theorem absorption_5: forall (U: Type) (a: Ensemble U), (∅ ∖ a) = ∅.
Proof.
intros U a.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
intuition.
contradiction.
Qed.

(**
  absorption of set difference by non-empty set, BBook: section 2.3.4, p.76, array 1, row 4, line 6
  *)
Theorem absorption_6: forall (U: Type) (a: Ensemble U), (a ∖ ∅) = a.
Proof.
intros U a.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H; intuition.
constructor.
intuition.
intros H0; contradiction.
Qed.

(**
  set difference of self is the empty set, BBook: section 2.3.4, p.76, array 1, row 5, line 1
  *)
Theorem empty_set_1: forall (U: Type) (s: Ensemble U), (s ∖ s) = ∅.
Proof.
intros U s.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H; contradiction.
contradiction.
Qed.

(**
  double complementation, BBook: section 2.3.4, p.76, array 1, row 6, line 1
  *)
Theorem double_complementation_1: forall (U: Type) (s a: Ensemble U), Included a s -> a = (s ∖ (s ∖ a)).
Proof.
intros U s a H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.
constructor.
apply H; intuition.
intros H1; induction H1; contradiction.
induction H0.
apply NNPP; intros H2; apply H1; constructor; intuition.
Qed.

(**
  monotonicity of intersection, BBook: section 2.3.4, p.76, array 1, row 7, line 1
  *)
Theorem monotonicity_1: forall (U: Type) (a b c: Ensemble U), (b ⊆ c) ⇒ ((a ∩ b) ⊆ (a ∩ c)).
Proof.
intros U a b c H.
intros x H0.
induction H0.
intuition.
Qed.

(**
  monotonicity of union, BBook: section 2.3.4, p.76, array 1, row 7, line 2
  *)
Theorem monotonicity_2: forall (U: Type) (a b c: Ensemble U), (b ⊆ c) ⇒ ((a ∪ b) ⊆ (a ∪ c)).
Proof.
intros U a b c H.
intros x H0.
induction H0; intuition.
Qed.

(**
  monotonicity of set difference (left), BBook: section 2.3.4, p.76, array 1, row 7, line 3
  *)
Theorem monotonicity_3: forall (U: Type) (a b c: Ensemble U), (b ⊆ c) ⇒ ((b ∖ a) ⊆ (c ∖ a)).
Proof.
intros U a b c H.
intros x H0.
induction H0; intuition.
Qed.

(**
  monotonicity of set difference (righ), BBook: section 2.3.4, p.76, array 1, row 7, line 4
  *)
Theorem monotonicity_4: forall (U: Type) (a b c: Ensemble U), (b ⊆ c) ⇒ ((a ∖ c) ⊆ (a ∖ b)).
Proof.
intros U a b c H.
intros x H0.
induction H0; intuition.
Qed.

(**
  monotonicity of complementation, BBook: section 2.3.4, p.76, array 1, row 7, line 5
  *)
Theorem monotonicity_5: forall (U: Type) (s b c: Ensemble U), (b ⊆ c) ⇒ ((s ∖ c) ⊆ (s ∖ b)).
Proof.
intros U a b c H.
intros x H0.
induction H0; intuition.
Qed.

(**
  de Morgan law for union, BBook: section 2.3.4, p.76, array 1, row 8, line 1
  *)
Theorem de_Morgan_1: forall (U: Type) (s a b: Ensemble U), (s ∖ (a ∪ b)) = ((s ∖ a) ∩ (s ∖ b)).
Proof.
intros U s a b.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
constructor.
constructor.
intuition.
intros H1; apply H0; left; intuition.
constructor.
intuition.
intros H1; apply H0; right; intuition.
induction H.
induction H; induction H0.
constructor.
intuition.
intros H3; induction H3.
apply H1; intuition.
apply H2; intuition.
Qed.

(**
  de Morgan law for intersection, BBook: section 2.3.4, p.76, array 1, row 8, line 2
  *)
Theorem de_Morgan_2: forall (U: Type) (s a b: Ensemble U), (s ∖ (a ∩ b)) = ((s ∖ a) ∪ (s ∖ b)).
Proof.
intros U s a b.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H.
induction H.
assert ( ~(In a x) \/ ~(In b x)).
apply not_and_or.
intros H1; apply H0; constructor; intuition.
induction H1.
left; constructor; intuition.
right; constructor; intuition.
induction H; induction H.
constructor.
intuition.
intros H1; apply H0; induction H1; intuition.
constructor.
intuition.
intros H1; apply H0; induction H1; intuition.
Qed.

(**
  inclusion into union, BBook: section 2.3.4, p.76, array 1, row 9, line 1
  *)
Theorem inclusion_1: forall (U: Type) (a b: Ensemble U), a ⊆ (a ∪ b).
Proof.
intros U a b.
intros x H.
left; intuition.
Qed.

(**
  inclusion upon intersection, BBook: section 2.3.4, p.76, array 1, row 9, line 2
  *)
Theorem inclusion_2: forall (U: Type) (a b: Ensemble U), (a ∩ b) ⊆ a.
Proof.
intros U a b.
intros x H.
induction H; intuition.
Qed.

(**
  inclusion left intersection to right union, BBook: section 2.3.4, p.76, array 1, row 9, line 3
  *)
Theorem inclusion_3: forall (U: Type) (s a b c: Ensemble U), Included a s -> Included b s -> Included c s -> (((a ∩ b) ⊆ c) ⇔ (a ⊆ ((s ∖ b) ∪ c))).
Proof.
intros U s a b c H H0 H1.
split; intros H2.
intros x H3.
assert (In b x \/ ~(In b x)).
apply classic.
induction H4.
right.
apply H2; constructor; intuition.
left.
constructor; intuition.
intros x H3.
induction H3.
generalize (H2 x H3); intros H5.
induction H5.
induction H5.
contradiction.
intuition.
Qed.

Close Scope eB_scope.
