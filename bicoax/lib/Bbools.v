(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import Bool.
Require Import Bchapter2.
	
(* Wrt to the bphox library, "TRUE" is noted "true" (all lowercase), same goes for "false" *)


(* We use the definitions of bool as of Coq: cf theories/Init/Datatypes.v. *)
Definition BBOOL := Full_set bool.

(*
goal /\b (b -> B b).
goal /\b (~ b -> B b).
goal /\X /\b ((~b -> b = FALSE -> X FALSE) -> (b -> b = TRUE -> X TRUE) -> B b -> X b).
The bool datatype here is not the same as Prop.
*)

(*
goal TRUE != FALSE.
See the diff_true_false Lemma.
*)

Theorem absurd_true_false1: forall (X: Prop), true = false -> X.
Proof.
intros.
absurd (true = false).
intuition.
assumption.
Qed.

Theorem absurd_true_false2: forall (X: Prop), false = true -> X.
Proof.
intros.
absurd (false = true).
intuition.
assumption.
Qed.

(*
goal /\X/\b ((b -> X) -> b = TRUE -> X).
goal /\X/\b ((b -> X) -> TRUE = b -> X).
goal /\X/\b ((~b -> X) -> b = FALSE -> X).
goal /\X/\b ((~b -> X) -> FALSE = b -> X).
goal /\X/\b ((b -> b = TRUE -> X) -> (~ b -> b = FALSE -> X) -> B b -> X).
goal /\x (B x <-> x or ~ x).
The bool datatype here is not the same as Prop.
*)

(*
goal equal.decidable B.
See the bool_dec Lemma.
*)

(*
goal /\b:B/\x,y\/!z ifP b x y z.
goal /\X/\c1/\c2 (X -> if X then c1 else c2 = c1).
goal /\X/\c1/\c2 (~X -> if X then c1 else c2 = c2).
goal /\X/\b:B/\c1,c2:X X(if b then c1 else c2).
goal /\X/\b:B/\c1,c2 ((b -> X c1) -> (~ b -> X c2) -> X(if b then c1 else c2)).
The "if then else" construct was commented out in the PhoX files. Plus it shall never appear in proof obligations anyway.
*)

(*
goal /\x,y:B B(x & y).
goal /\x ((TRUE & x) = x).
goal /\x:B ((x & TRUE) = x).
goal /\x ((FALSE & x) = FALSE).
goal /\x:B ((x & FALSE) = FALSE).
goal /\x,y:B B(x or y).
goal /\x:B B ~ x.
goal (~ TRUE) = FALSE.
goal (~ FALSE) = TRUE.
goal /\x ((FALSE or x) = x).
goal /\x:B ((x or FALSE) = x).
goal /\x ((TRUE or x) = TRUE).
goal /\x:B ((x or TRUE) = TRUE).
goal /\x,y:B (B (x=y)).
The bool datatype here is not the same as Prop. Plus, see Bool.v for De Morgan laws for the bool datatype.
*)

