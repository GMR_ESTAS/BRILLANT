(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import Bchapter1.
Require Import Bchapter2.
Require Import Powerset_facts.
Require Import Bgeneralized_union_inter.
Require Import Bfinite_subsets.
Require Import Finite_sets_facts.
Require Import Bnaturals_basics.
Require Import Bnaturals_ensembles.
Require Import Arith.

Open Local Scope nat_scope.

(**
  * Maximum: definitions
  *)

(**
  max, BBook: section 3.5.5, p.158, array 1
  *)
Definition bbN_max (s: Ensemble (Ensemble BIG_type)) := GenUnion s.

Definition Bmax (s: Ensemble nat) := iota _ inhabited_nat (fun x => In s x /\ (forall y, In s y -> y <= x)).

(**
  * Maximum: theorems
  *)

(**
  property 3.5.10, BBook: section 3.5.5, p.158
  *)
Theorem bbN_max_is_maximal: forall (s: Ensemble (Ensemble BIG_type)), In (Power_finite1 bbN) s ->
  forall (n: Ensemble BIG_type), In s n -> bbN_le n (bbN_max s).
Proof.
intros.
unfold bbN_le; unfold bbN_max.
apply GenUnion_upper_bound.
assumption.
Qed.

Theorem bbN_max_in_bbN: forall (s: Ensemble (Ensemble BIG_type)),
  In (Power_finite1 bbN) s -> In bbN (bbN_max s).
Proof.
intros.
inversion H.
generalize H1.
pattern s.
apply finite_sets_induction_principle with (Ensemble BIG_type) bbN.
assert (In (Singleton (Empty_set (Ensemble BIG_type))) (Empty_set (Ensemble BIG_type))).
intuition.
contradiction.
intros.
unfold bbN_max.
rewrite GenUnion_Union.
rewrite GenUnion_Singleton.
assert ((GenUnion x)=bbN_0 \/ (GenUnion x) <> bbN_0); [ apply classic | ].
induction H5.
rewrite H5; unfold bbN_0; rewrite empty_set_1; rewrite neutral_element_1; assumption.
unfold bbN_0 in H5; rewrite empty_set_1 in H5.
assert (x <> (Empty_set _)).
intros H6; apply H5.
rewrite H6.
apply Extensionality_Ensembles; split; red; intros.
do 3 (induction H7); contradiction.
contradiction.
induction H2.
assert (In bbN (bbN_max x)).
apply H7.
intros H8; apply H6; induction H8; intuition.
assert (In (times bbN bbN) (u, bbN_max x)); [ split; assumption | ].
generalize (bbN_inequality_trichotomy u (bbN_max x) H9); intros.
induction H10.
induction H10.
rewrite absorption_4; assumption.
induction H10.
unfold bbN_max in H10.
rewrite H10.
rewrite Union_idempotent; assumption.
induction H10.
rewrite Union_commutative.
rewrite absorption_4; assumption.
assumption.
Qed.

(**
  property 3.5.11, BBook: section 3.5.5, p.158
  *)
Theorem bbN_max_in_its_argument: forall (s: Ensemble (Ensemble BIG_type)),
  In (Power_finite1 bbN) s -> In s (bbN_max s).
Proof.
intros s H.
assert (In bbN (bbN_max s)).
apply bbN_max_in_bbN; assumption.
induction H.
generalize H1.
pattern s.
apply finite_sets_induction_principle with (Ensemble BIG_type) bbN.

assert (In (Singleton (Empty_set (Ensemble BIG_type))) (Empty_set (Ensemble BIG_type))).
intuition.
contradiction.

intros.
assert ((bbN_max x)=bbN_0 \/ (bbN_max x) <> bbN_0); [ apply classic | ].
induction H5.
unfold bbN_max.
rewrite GenUnion_Union.
rewrite GenUnion_Singleton.
unfold bbN_max in H5.
rewrite H5; unfold bbN_0; rewrite empty_set_1; rewrite neutral_element_1.
left; intuition.
assert (~(In (Singleton (Empty_set _)) x)).
intros H6; apply H5.
induction H6.
unfold bbN_0; rewrite empty_set_1.
apply Extensionality_Ensembles; split; red; intros.
do 3 (induction H6); contradiction.
contradiction.
induction H2.
generalize (H7 H6); intros.
unfold bbN_max.
rewrite GenUnion_Union.
rewrite GenUnion_Singleton.
assert (In bbN (bbN_max x)).
apply bbN_max_in_bbN.
constructor; assumption.
assert (In (times bbN bbN) (u, bbN_max x)); [ split; assumption | ].
generalize (bbN_inequality_trichotomy u (bbN_max x) H10); intros.
induction H11.
induction H11.
rewrite (absorption_4 _ u (GenUnion x)).
left; intuition.
unfold bbN_max in H12; assumption.
induction H11.
unfold bbN_max in H11.
rewrite H11.
rewrite Union_idempotent; left; intuition.
induction H11.
rewrite (Union_commutative _ u (GenUnion x)).
rewrite (absorption_4 _ (GenUnion x) u).
unfold bbN_max in H8 , H12.
right; assumption.
assumption.
assumption.
Qed.

(**
  * Maximum: validity
  *)

Theorem bbN_max_in_its_argument_coerced: forall (s: Ensemble (Ensemble BIG_type)), In (Power_finite1 bbN) s ->
  In (Ensemble_nat_of_bbN s) (nat_of_bbN (bbN_max s)).
Proof.
intros s H.
assert (In (Power_set bbN) s).
induction H.
induction H.
constructor; assumption.
apply (valid_Ensemble_bbN_belonging_l s (bbN_max s) H0 (bbN_max_in_bbN s H)).
apply bbN_max_in_its_argument.
assumption.
Qed.

Theorem bbN_max_is_maximal_coerced: forall (s: Ensemble (Ensemble BIG_type)), In (Power_finite1 bbN) s ->
  forall (y:nat), In (Ensemble_nat_of_bbN s) y -> y <= nat_of_bbN (bbN_max s).
Proof.
intros s H.
intros.
induction H0.
replace y with (nat_of_bbN (bbN_of_nat y)).
apply valid_bbN_le_l.
apply bbN_of_nat_in_bbN.
apply bbN_max_in_bbN; assumption.
apply bbN_max_is_maximal.
assumption.
rewrite (proj2 H0).
rewrite bbN_of_nat_of_bbN; [  intuition | ].
induction H; induction H.
apply H2; intuition.
rewrite nat_2coercions_identity; reflexivity.
Qed.

Theorem valid_bbN_max: forall (s: Ensemble (Ensemble BIG_type)), In (Power_finite1 bbN) s ->
  bbN_max s = bbN_of_nat (Bmax (Ensemble_nat_of_bbN s)).
Proof.
intros s H.
unfold Bmax; apply iota_ind.

intros.
induction H0.
rewrite valid_bbN_eq_r with (bbN_of_nat b) (bbN_max s).
reflexivity.
apply bbN_of_nat_in_bbN.
apply bbN_max_in_bbN; assumption.
rewrite nat_2coercions_identity.
apply H1.
split.
apply bbN_max_in_its_argument_coerced; assumption.
apply bbN_max_is_maximal_coerced; assumption.

exists (nat_of_bbN (bbN_max s)).
split.
split.
apply bbN_max_in_its_argument_coerced; assumption.
apply bbN_max_is_maximal_coerced; assumption.
intros.
induction H0.
induction H0.
induction H0.
apply le_antisym.
apply H1.
apply bbN_max_in_its_argument_coerced; assumption.
rewrite H2.
assert (In bbN x).
induction H; induction H.
apply H4; assumption.
apply valid_bbN_le_l.
assumption.
apply bbN_max_in_bbN; assumption.
apply bbN_max_is_maximal; assumption.
Qed.

Theorem valid_nat_max: forall (s: Ensemble nat), In (Power_finite1 (Full_set nat)) s ->
  Bmax s = nat_of_bbN (bbN_max (Ensemble_bbN_of_nat s)).
Proof.
intros s H.
apply valid_nat_eq_r.
rewrite bbN_of_nat_of_bbN.
rewrite valid_bbN_max.
rewrite <- valid_Ensemble_bbN_of_nat.
reflexivity.
apply Power_finite1_Ensemble_bbN_of_nat; assumption.
apply bbN_max_in_bbN.
apply Power_finite1_Ensemble_bbN_of_nat; assumption.
Qed.

(**
  * Maximum: properties
  *)

(** Note/TODO: same remark as for the minimum at the same place in the module *)

(**
  BBook: section 3.5.5, p.158, array 1, row 1
  *)
Theorem Bmax_singleton: forall (a: nat), Bmax (Singleton a) = a.
Proof.
intros.
assert (Bmax (Singleton a) = nat_of_bbN (bbN_of_nat a)).
rewrite valid_nat_max.
assert (In bbN (bbN_max (Ensemble_bbN_of_nat (Singleton a)))).
apply bbN_max_in_bbN.
apply Power_finite1_Ensemble_bbN_of_nat.
apply Power_finite1_singleton_nat.
apply valid_bbN_eq_l.
assumption.
apply bbN_of_nat_in_bbN.
unfold bbN_max.
replace (Ensemble_bbN_of_nat (Singleton a)) with (Singleton (bbN_of_nat a)).
apply GenUnion_Singleton.
apply valid_Ensemble_bbN_of_nat_singleton.
apply Power_finite1_singleton_nat.
rewrite H.
rewrite nat_2coercions_identity; reflexivity.
Qed.

(**
  BBook: section 3.5.5, p.158, array 1, row 2
  *)
Theorem Bmax_doubleton1: forall (a b:nat), b <= a -> Bmax {= a, b =} = a.
Proof.
intros a b H.
assert (Bmax {= a, b =} = nat_of_bbN (bbN_of_nat a)).
rewrite valid_nat_max.
assert (In bbN (bbN_max (Ensemble_bbN_of_nat {= a, b =}))).
apply bbN_max_in_bbN.
apply Power_finite1_Ensemble_bbN_of_nat.
apply Power_finite1_union_nat.
apply Power_finite1_singleton_nat.
apply Power_finite1_singleton_nat.
apply valid_bbN_eq_l.
assumption.
apply bbN_of_nat_in_bbN.
replace (bbN_of_nat a) with (Union (bbN_of_nat a) (bbN_of_nat b)).
unfold bbN_max.
replace (Ensemble_bbN_of_nat (Union (Singleton a) (Singleton b))) with (Union (Singleton (bbN_of_nat a)) (Singleton (bbN_of_nat b))).
apply GenUnion_Doubleton.
rewrite valid_Ensemble_bbN_of_nat_Union.
apply Extensionality_Ensembles; split; red; intros.
induction H1; induction H1.
left; apply valid_Ensemble_nat_belonging_l; intuition.
right; apply valid_Ensemble_nat_belonging_l; intuition.
induction H1.
left; rewrite valid_Ensemble_bbN_of_nat_singleton; assumption.
right; rewrite valid_Ensemble_bbN_of_nat_singleton; assumption.
apply Extensionality_Ensembles; split; red; intros.
induction H1.
assumption.
assert (bbN_le (bbN_of_nat b) (bbN_of_nat a)).
apply valid_nat_le_l; assumption.
apply H2; assumption.
left; assumption.
apply Power_finite1_union_nat.
apply Power_finite1_singleton_nat.
apply Power_finite1_singleton_nat.
rewrite nat_2coercions_identity in H0.
assumption.
Qed.

(**
  BBook: section 3.5.5, p.158, array 1, row 3
  *)
Theorem Bmax_doubleton2: forall (a b:nat), a <= b -> Bmax {= a, b =} = b.
Proof.
intros a b H.
assert ( {= a, b =} = {= b, a =} ).
intuition.
rewrite H0.
apply Bmax_doubleton1; assumption.
Qed.

(**
  BBook: section 3.5.5, p.158, array 1, row 4
  *)
Theorem Bmax_union: forall (a b: Ensemble nat), In (Power_finite1 (Full_set nat)) a -> In (Power_finite1 (Full_set nat)) b ->
Bmax (Union a b) = (Bmax {= (Bmax a), (Bmax b) =}).
Proof.
intros a b H H0.
rewrite (valid_nat_max (Union a b)).
rewrite (valid_nat_max {= (Bmax a), (Bmax b) =}).
assert (
(bbN_max (Ensemble_bbN_of_nat (Union a b))) =
(bbN_max (Ensemble_bbN_of_nat {=Bmax a, Bmax b=}))
).
rewrite valid_Ensemble_bbN_of_nat_Union.
replace (Ensemble_bbN_of_nat {=Bmax a, Bmax b=})
  with ({= bbN_max (Ensemble_bbN_of_nat a), bbN_max (Ensemble_bbN_of_nat b) =}).
unfold bbN_max.
apply GenUnion_Union2.
rewrite valid_Ensemble_bbN_of_nat_Union.
rewrite <- valid_Ensemble_bbN_of_nat_singleton.
rewrite <- valid_Ensemble_bbN_of_nat_singleton.
rewrite valid_bbN_max.
rewrite valid_bbN_max.
rewrite <-  valid_Ensemble_bbN_of_nat.
rewrite <-  valid_Ensemble_bbN_of_nat.
reflexivity.
apply Power_finite1_Ensemble_bbN_of_nat; assumption.
apply Power_finite1_Ensemble_bbN_of_nat; assumption.
rewrite H1; reflexivity.
apply Power_finite1_union_nat.
apply Power_finite1_singleton_nat.
apply Power_finite1_singleton_nat.
apply Power_finite1_union_nat; assumption.
Qed.

(**
  BBook: section 3.5.5, p.158, array 1, row 5
  *)
Theorem Bmax_union_singleton: forall (a: Ensemble nat) (x: nat), In (Power_finite1 (Full_set nat)) a ->
Bmax (Union a (Singleton x)) = (Bmax {= (Bmax a), x =}).
Proof.
intros a x H.
replace ({=Bmax a, x=}) with ({=Bmax a, (Bmax (Singleton x))=}).
apply Bmax_union.
assumption.
constructor.
constructor.
apply Singleton_is_finite.
red; intros; constructor.
intros H0.
assert ((Singleton x) <> (Empty_set nat)).
apply Inhabited_not_empty.
apply Inhabited_intro with (x := x); intuition.
induction H0.
apply H1; reflexivity.
replace (Bmax (Singleton x)) with x.
reflexivity.
symmetry; apply Bmax_singleton.
Qed.
