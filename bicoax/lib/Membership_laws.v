(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bbasic.
Require Import Bderived_constructs.
Require Import Brelations.
Require Import Bfunctions.

Open Scope eB_scope.

(**
  BBook: section 2.6.1, p.95, array 1, row 1
  *)
Theorem Membership_laws_01: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t)) ⇒(r∼ ∈ (t ↔ s)).
Proof.
intros S T s t r H.
repeat (induction H).
constructor.
constructor.
intros x H0.
induction H0.
generalize (H (a,b) H0).
intros.
inversion H1; constructor; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 2
  *)
Theorem Membership_laws_02: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)), (f ∈ (s ⤔ t)) ⇒(f∼ ∈ (t ⇸ s)).
Proof.
intros S T s t r H.
repeat (induction H).
constructor.
apply Membership_laws_01.
constructor; constructor; intuition.
intros.
inversion H2; inversion H3.
apply H0 with x; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 3
  *)
Theorem Membership_laws_03: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)), (f ∈ (s ⤔ t)) ⇒(f∼ ∈ (t ⤔ s)).
Proof.
intros S T s t f H.
constructor.
apply Membership_laws_02; intuition.
induction H.
induction H.
intros.
inversion H2; inversion H3.
apply H1 with y; intuition.
Qed.

Theorem Membership_laws_04_bis: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)), (f ∈ (s ↣ t)) ⇒(f∼ ∈ (t ⤗ s)).
Proof.
intros S T s t f H.
constructor.
induction H.
apply Membership_laws_03; assumption.
constructor.
apply Membership_laws_02; induction H; intuition.
intros y H0.
induction H as [ f H H1 ].
induction H as [ f H H2 ].
induction H2.
destruct H0 as [ a H0 ].
destruct H0 as [ b H0 ]; exists b; constructor; assumption.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 4
  *)
Theorem Membership_laws_04: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)), (f ∈ (s ⤖ t)) ⇒(f∼ ∈ (t ⤖ s)).
Proof.
intros S T s t f H.
constructor.
induction H.
constructor.
induction H0.
apply Membership_laws_02; intuition.
induction H0.
induction H1.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H3.
induction H3.
induction H3.
inversion H3.
induction H1.
induction H1.
induction H1.
generalize (H1 (x,a) H5); intros H8.
inversion H8; intuition.
constructor.
generalize (H2 x H3); intros H4.
induction H4.
exists x0; constructor; intuition.
apply Membership_laws_04_bis.
constructor.
destruct H; assumption.
destruct H as [ f H H0 ].
destruct H0; assumption.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 5
  *)
Theorem Membership_laws_05: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t)) ⇒(domain(r) ⊆ s).
Proof.
intros S T s t r H.
repeat (induction H).
intros x H0.
induction H0.
induction H0.
generalize (H (a,x) H0); intros H1.
inversion H1; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 6
  *)
Theorem Membership_laws_06: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t)) ⇒(range(r) ⊆ t).
Proof.
intros S T s t r H.
repeat (induction H).
intros x H0.
induction H0.
induction H0.
generalize (H (x,b) H0); intros H1.
inversion H1; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 7
  *)
Theorem Membership_laws_07: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (T*U)), (p ∈ (s ↔ t) ∧ q ∈ (t ↔ u)) ⇒((p;q) ∈ (s ↔ u)).
Proof.
intros S T U s t u p q H.
induction H.
repeat (induction H); repeat (induction H0).
constructor.
constructor.
intros x H1.
induction H1.
induction H1.
induction H1.
generalize (H (a,x) H1); intros H3.
generalize (H0 (x,c) H2); intros H4.
inversion H3; inversion H4.
constructor; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 8
  *)
Theorem Membership_laws_08: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (T*U)), (f ∈ (s ⇸ t) ∧ g ∈ (t ⇸ u)) ⇒((f;g) ∈ (s ⇸ u)).
Proof.
intros S T U s t u f g H.
induction H.
induction H.
induction H0.
constructor.
apply Membership_laws_07 with t; intuition.
intros x y H3 z H4.
inversion H3; inversion H4.
inversion H6.
inversion H9.
assert (x0=x1).
apply H1 with x; intuition.
apply H2 with x0.
intuition.
rewrite H13; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 9
  *)
Theorem Membership_laws_09: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (T*U)), (f ∈ (s ⤔ t) ∧ g ∈ (t ⤔ u)) ⇒((f;g) ∈ (s ⤔ u)).
Proof.
intros S T U s t u f g H.
induction H.
induction H.
induction H0.
constructor.
apply Membership_laws_08 with t; intuition.
intros x z y H3 H4.
inversion H3; inversion H4.
inversion H6; inversion H9.
assert (x0=x1).
apply H2 with y; intuition.
apply H1 with x0.
intuition.
rewrite H13; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 10
  *)
Theorem Membership_laws_10: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (T*U)), (f ∈ (s ⤀ t) ∧ g ∈ (t ⤀ u)) ⇒((f;g) ∈ (s ⤀ u)).
Proof.
intros S T U s t u f g H.
induction H.
induction H.
induction H0.
constructor.
apply Membership_laws_08 with t; intuition.
intros y H3.
generalize (H2 y H3); intros H4.
induction H4.
induction H0; induction H0; induction H0.
generalize (H0 (x,y) H4); intros H6.
inversion H6.
generalize (H1 x H9); intros H11.
inversion H11.
exists x0.
constructor.
exists x.
intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 11

  TODO: see if it is possible *not* to use app in this proof
  *)
Theorem Membership_laws_11: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (T*U)), (f ∈ (s ↠ t) ∧ g ∈ (t → u)) ⇒((f;g) ∈ (s → u)).
Proof.
intros S T U s t u f g H.
induction H.
constructor.
apply Membership_laws_08 with t; intuition.
induction H; induction H; intuition.
induction H0; intuition.
induction H; induction H.
induction H1.
induction H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H5.
induction H4.
induction H2.
inversion H5.
inversion H2.
inversion H6.
inversion H8.
constructor.
exists x1; intuition.
constructor.
assert (In (partial_function s t) f /\ In (domain f) x).
rewrite H2; intuition.
assert (In t (app f x H6)).
apply application_in_codomain.
assert (In (domain f0) (app f x H6)).
induction H4; assumption.
assert (In (partial_function t u) f0 /\ In (domain f0) (app f x H6)).
intuition.
exists (app f0 (app f x H6) H9).
constructor.
exists (app f x H6).
split.
apply app_trivial_property.
apply app_trivial_property.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 12
  *)
Theorem Membership_laws_12: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (T*U)), (f ∈ (s ⤖ t) ∧ g ∈ (t ↣ u)) ⇒((f;g) ∈ (s ↣ u)).
Proof.
intros S T U s t u f g H.
induction H.
constructor.
induction H.
induction H1.
induction H0.
apply Membership_laws_11 with t; intuition.
constructor; intuition.
apply Membership_laws_09 with t.
induction H.
induction H0.
induction H1.
intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 13
  *)
Theorem Membership_laws_13: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (T*U)), (f ∈ (s ↠ t) ∧ g ∈ (t ↠ u)) ⇒((f;g) ∈ (s ↠ u)).
Proof.
intros S T U s t u f g H.
induction H.
constructor.
apply Membership_laws_11 with t.
induction H0.
intuition.
apply Membership_laws_10 with t.
induction H; induction H0; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 14
  *)
Theorem Membership_laws_14: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (T*U)), (f ∈ (s ⤖ t) ∧ g ∈ (t ⤖ u)) ⇒((f;g) ∈ (s ⤖ u)).
Proof.
intros S T U s t u f g H.
induction H.
constructor.
apply Membership_laws_11 with t.
induction H; induction H0.
induction H1.
split; [ constructor; intuition | intuition ].
constructor.
apply Membership_laws_09 with t.
induction H; induction H0.
induction H1; induction H2.
intuition.
apply Membership_laws_10 with t.
induction H; induction H0.
induction H1; induction H2.
intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 15
  *)
Theorem Membership_laws_15: forall (S: Type) (s: Ensemble S), (id(s) ∈ (s ⤖ s)).
Proof.
intros S s.
assert (In (relation s s) (id(s)) ).
constructor.
constructor.
intros x H.
induction H; intuition.
assert (In (partial_function s s) (id(s))).
constructor.
intuition.
intros.
inversion H0.
inversion H1.
rewrite <- H9; rewrite <- H5; reflexivity.
constructor.
constructor.
intuition.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
induction H1.
inversion H1.
inversion H4; intuition.
constructor.
exists x.
constructor.
constructor; intuition.
reflexivity.
constructor.
constructor.
intuition.
intros.
inversion H1.
inversion H2.
rewrite H6; rewrite H10; intuition.
constructor.
intuition.
intros.
exists y.
constructor.
constructor; intuition.
reflexivity.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 16
  *)
Theorem Membership_laws_16: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t)) ⇒((u ◁ r) ∈ (u ↔ t)).
Proof.
intros S T s t u r H.
constructor.
constructor.
intros x H0.
induction H.
induction H0.
constructor; [ intuition | ].
induction H1.
induction H1.
generalize (H1 (x,y) H0).
intros H3; inversion H3; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 17
  *)
Theorem Membership_laws_17: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (v: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ∧ v ⊆ t) ⇒((r ▷ v) ∈ (s ↔ v)).
Proof.
intros S T s t v r H.
constructor.
constructor.
intros x H0.
induction H.
induction H0.
constructor; [ | intuition ].
induction H.
induction H.
generalize (H (x,y) H0).
intros H3; inversion H3; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 18
  *)
Theorem Membership_laws_18: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (u ⊆ s ∧ r ∈ (s ↔ t)) ⇒((u ⩤ r) ∈ ((s ∖ u) ↔ t)).
Proof.
intros S T s t u r H.
constructor.
constructor.
intros x H0.
induction H.
induction H0.
induction H1.
induction H1.
generalize (H1 (x,y) H0).
intros H3; inversion H3.
constructor; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 19
  *)
Theorem Membership_laws_19: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (v: Ensemble T) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ∧ v ⊆ t) ⇒((r ⩥ v) ∈ (s ↔ (t ∖ v))).
Proof.
intros S T s t v r H.
constructor.
constructor.
intros x H0.
induction H.
induction H0.
induction H.
induction H.
generalize (H (x,y) H0).
intros H3; inversion H3.
constructor; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 20
  *)
Theorem Membership_laws_20: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble S) (r: Ensemble (S*T)), (r ∈ (s ↔ t) ∧ u ⊆ s) ⇒(r[u] ⊆ t).
Proof.
intros S T s t u r H.
induction H.
intros x H1.
induction H1.
induction H1.
repeat (induction H).
generalize (H (x,y) H2); intros H3; inversion H3; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 21
  *)
Theorem Membership_laws_21: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (p: Ensemble (S*T)) (q: Ensemble (S*T)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ t)) ⇒((p⥷q) ∈ (s ↔ t)).
Proof.
intros S T s t p q H.
induction H.
constructor.
constructor.
intros x H1.
induction H1.
induction H1.
induction H1.
repeat (induction H).
apply H; intuition.
repeat (induction H0).
apply H0; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 22
  *)
Theorem Membership_laws_22: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s ⇸ t) ∧ g ∈ (s ⇸ t)) ⇒((f⥷g) ∈ (s ⇸ t)).
Proof.
intros S T s t f g H.
induction H.
constructor.
apply Membership_laws_21.
induction H; induction H0; intuition.
intros.
inversion H1.
induction H4.
inversion H2.
induction H7.
induction H.
apply H9 with x; intuition.
absurd (In (domain g) x).
intuition.
constructor.
exists z; intuition.
inversion H2.
induction H7.
absurd (In (domain g) x).
intuition.
constructor; exists y; intuition.
induction H0.
apply H9 with x; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 23
  *)
Theorem Membership_laws_23: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s ⤔ t) ∧ g ∈ (s ⤔ t) ∧  range(g) ∩ range(domain(g)⩤f) = ∅) ⇒((f⥷g) ∈ (s ⤔ t)).
Proof.
intros S T s t f g H.
decompose [and] H; clear H.
constructor.
apply Membership_laws_22.
induction H2; induction H3; intuition.
intros.
inversion H.
induction H5.
inversion H0.
induction H8.
induction H2.
apply H10 with y; intuition.
assert (Same_set T (Intersection (range g) (range (domain_subtraction f (domain g)))) (Empty_set T)).
rewrite H1; intuition.
induction H10.
unfold Included in H10, H11.
assert (In (Intersection (range g) (range (domain_subtraction f (domain g)))) y).
constructor.
constructor.
exists z; intuition.
constructor.
exists x.
constructor; intuition.
generalize (H10 y H12); contradiction.
inversion H0.
induction H8.
assert (Same_set T (Intersection (range g) (range (domain_subtraction f (domain g)))) (Empty_set T)).
rewrite H1; intuition.
induction H10.
unfold Included in H10, H11.
assert (In (Intersection (range g) (range (domain_subtraction f (domain g)))) y).
constructor.
constructor.
exists x; intuition.
constructor.
exists z.
constructor; intuition.
generalize (H10 y H12); contradiction.
induction H3.
apply H10 with y; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 24
  *)
Theorem Membership_laws_24: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s ⇸ t) ∧ g ∈ (s ⤀ t)) ⇒((f⥷g) ∈ (s ⤀ t)).
Proof.
intros S T s t f g H.
induction H.
constructor.
apply Membership_laws_22.
induction H0; intuition.
intros.
induction H0.
generalize (H2 y H1); intros H3.
induction H3.
exists x.
constructor.
right; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 25
  *)
Theorem Membership_laws_25: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s ⇸ t) ∧ g ∈ (s → t) ) ⇒((f⥷g) ∈ (s → t)).
Proof.
intros S T s t f g H.
induction H.
constructor.
apply Membership_laws_22.
induction H0; intuition.
induction H0.
induction H1.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
induction H1.
inversion H1.
constructor.
induction H3.
repeat (induction H).
generalize (H (a,x) (proj1 H3)).
intros.
inversion H6.
induction H3; contradiction.
exists x; intuition.
induction H1.
induction H1.
constructor.
exists x.
constructor.
right; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 26

  TODO: see if it is possible not to use app
  *)
Theorem Membership_laws_26: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s → t) ∧ g ∈ (s ⇸ t)) ⇒((f⥷g) ∈ (s → t)).
Proof.
intros S T s t f g H.
induction H.
constructor.
apply Membership_laws_22.
induction H; intuition.
induction H.
induction H1.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H1.
induction H1.
induction H1.
inversion H1.
constructor.
induction H3.
exists x; intuition.
repeat (induction H0).
generalize (H0 (a,x) H3).
intros.
inversion H6.
induction H9.
induction H9.
exists x1; intuition.
induction H1.
induction H1.
constructor.
assert (In (domain g) a \/ ~(In (domain g) a)).
apply classic.
induction H2.
assert (In (partial_function (domain f) t) g /\ In (domain g) a).
intuition.
exists (app g a H3).
constructor.
right; apply app_trivial_property.
exists x.
constructor.
left; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 27
  *)
Theorem Membership_laws_27: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s ⇸ t) ∧ g ∈ (s ↠ t)) ⇒((f⥷g) ∈ (s ↠ t)).
Proof.
intros S T s t f g H.
induction H.
constructor.
apply Membership_laws_25.
induction H0; intuition.
apply Membership_laws_24.
induction H0; intuition.
Qed.

(**
  BBook: section 2.6.1, p.95, array 1, row 28
  *)
Theorem Membership_laws_28: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (p: Ensemble (S*T)) (q: Ensemble (S*U)), (p ∈ (s ↔ t) ∧ q ∈ (s ↔ u)) ⇒((p⊗q) ∈ (s ↔ (t×u))).
Proof.
intros S T U s t u p q H.
induction H.
constructor.
constructor.
intros x H1.
induction x.
induction b.
induction H1.
repeat (induction H); repeat (induction H0).
generalize (H (x,y) H1); intros.
generalize (H0 (x,z) H2); intros.
inversion H3; inversion H4.
constructor; [ intuition | constructor; intuition ].
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 1
  *)
Theorem Membership_laws_29: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (S*U)), (f ∈ (s ⇸ t) ∧ g ∈ (s ⇸ u)) ⇒((f⊗g) ∈ (s ⇸ (t×u))).
Proof.
intros S T U s t u p q H.
constructor.
induction H.
induction H; induction H0.
apply Membership_laws_28; intuition.
induction H.
induction H; induction H0.
intros.
inversion H3.
inversion H4.
apply injective_projections.
compute.
apply H1 with x; intuition.
compute.
apply H2 with x; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 2
  *)
Theorem Membership_laws_30: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (S*U)), (f ∈ (s ⤔ t) ∧ g ∈ (s ⤔ u)) ⇒((f⊗g) ∈ (s ⤔ (t×u))).
Proof.
intros S T U s t u p q H.
constructor.
induction H.
induction H; induction H0.
apply Membership_laws_29; intuition.
induction H.
induction H; induction H0.
intros.
inversion H3.
inversion H4.
assert ( (y0, z0) = (y1, z1)).
rewrite H6; rewrite H10; intuition.
assert (y0 = y1 /\ z0 = z1).
injection H13; intuition.
apply H2 with z1.
rewrite <- (proj2 H14); intuition.
intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 3
  *)
Theorem Membership_laws_31: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (S*U)), (f ∈ (s → t) ∧ g ∈ (s → u)) ⇒((f⊗g) ∈ (s → (t×u))).
Proof.
intros S T U s t u p q H.
constructor.
induction H.
induction H; induction H0.
apply Membership_laws_29; intuition.
induction H.
induction H; induction H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H3.
induction H3.
induction H3.
induction H1.
inversion H3.
constructor.
exists y; intuition.
induction H1.
constructor.
assert (In (domain f0) x); [ rewrite H2; intuition | ].
induction H3.
induction H1.
induction H3; induction H1.
exists (x, x0).
constructor; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 4
  *)
Theorem Membership_laws_32: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (S*U)), (f ∈ (s ↣ t) ∧ g ∈ (s ↣ u)) ⇒((f⊗g) ∈ (s ↣ (t×u))).
Proof.
intros S T U s t u p q H.
constructor.
induction H.
induction H; induction H0.
apply Membership_laws_31; intuition.
induction H.
induction H; induction H0.
apply Membership_laws_30; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 5

  error: this theorem is false.

  Counterexample:

f={(1,a), (2,b), (3, c), (4,c)}
g={{(1,alpha), (2,alpha), (3, beta), (4,gamma)}
f is a total surjection of 1..4 -> a..c
g is a total surjection of 1..4 -> alpha..gamma
Their direct product:
{1 |-> (a,alpha), 2 |-> (b,alpha), 3 |-> (c,beta), 4 |-> (c,gamma) }
is not a surjection over a..c >< alpha..gamma.
Indeed, for instance the couple (a,gamma) is in the aforementioned product set,
but has no antecedent in 1..4.

Theorem Membership_laws_33: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (S*U)), (f ∈ (s ↠ t) ∧ g ∈ (s ↠ u)) ⇒((f⊗g) ∈ (s ↠ (t×u))).
*)


(**
  BBook: section 2.6.1, p.96, array 1, row 6

  error: this theorem is false.

  See Membership_laws_33 for an explanation, which would be similar here

Theorem Membership_laws_34: forall (S T U: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (f: Ensemble (S*T)) (g: Ensemble (S*U)), (f ∈ (s ⤖ t) ∧ g ∈ (s ⤖ u)) ⇒((f⊗g) ∈ (s ⤖ (t×u))).
*)

(**
  BBook: section 2.6.1, p.96, array 1, row 7
  *)
Theorem Membership_laws_35: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), (prj1(s,t) ∈ ((s×t) → s)).
Proof.
intros S T s t.
constructor.
constructor.
constructor.
constructor; intros x H.
induction H.
intuition.
intros x y H z H0.
induction x.
inversion H.
inversion H0.
rewrite H5; rewrite H10; reflexivity.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H3.
induction H3.
induction a.
induction H.
inversion H.
inversion H2.
inversion H7.
intuition.
induction H3.
constructor.
exists a.
constructor; [ | reflexivity ].
constructor; [ constructor; intuition | intuition ].
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 8
  *)
Theorem Membership_laws_36: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), (prj2(s,t) ∈ ((s×t) → t)).
Proof.
intros S T s t.
constructor.
constructor.
constructor.
constructor; intros x H.
induction H.
intuition.
intros x y H z H0.
induction x.
inversion H.
inversion H0.
rewrite H5; rewrite H10; reflexivity.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H3.
induction H3.
induction a.
induction H.
inversion H.
inversion H2.
inversion H7.
intuition.
induction H3.
constructor.
exists b.
constructor; [ | reflexivity ].
constructor; [ constructor; intuition | intuition ].
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 9
  *)
Theorem Membership_laws_37: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (p: Ensemble (S*T)) (q: Ensemble (U*V)), (p ∈ (s ↔ t) ∧ q ∈ (u ↔ v)) ⇒((p∥q) ∈ ((s×u) ↔ (t×v))).
Proof.
intros S T U V s t u v p q H.
induction H.
constructor.
constructor.
intros x H1.
induction x.
induction a.
induction b.
induction H1.
repeat (induction H); repeat (induction H0).
generalize (H (x,z) H1); intros.
generalize (H0 (y,w) H2); intros.
inversion H3; inversion H4.
constructor; [ constructor; intuition | constructor; intuition ].
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 10
  *)
Theorem Membership_laws_38: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (f: Ensemble (S*T)) (g: Ensemble (U*V)), (f ∈ (s ⇸ t) ∧ g ∈ (u ⇸ v)) ⇒((f∥g) ∈ ((s×u) ⇸ (t×v))).
Proof.
intros S T U V s t u v p q H.
constructor.
induction H.
induction H; induction H0.
apply Membership_laws_37; intuition.
induction H.
induction H; induction H0.
intros.
induction x.
induction y.
induction z.
inversion H3.
inversion H4.
apply injective_projections; compute.
apply H1 with a; intuition.
apply H2 with b; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 11
  *)
Theorem Membership_laws_39: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (f: Ensemble (S*T)) (g: Ensemble (U*V)), (f ∈ (s ⤔ t) ∧ g ∈ (u ⤔ v)) ⇒((f∥g) ∈ ((s×u) ⤔ (t×v))).
Proof.
intros S T U V s t u v p q H.
constructor.
induction H.
induction H; induction H0.
apply Membership_laws_38; intuition.
induction H.
induction H; induction H0.
intros.
induction x; induction y; induction z.
inversion H3.
inversion H4.
apply injective_projections; compute.
apply H1 with a0; intuition.
apply H2 with b0; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 12
  *)
Theorem Membership_laws_40: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (f: Ensemble (S*T)) (g: Ensemble (U*V)), (f ∈ (s ⤀ t) ∧ g ∈ (u ⤀ v)) ⇒((f∥g) ∈ ((s×u) ⤀ (t×v))).
Proof.
intros S T U V s t u v p q H.
constructor.
induction H.
induction H; induction H0.
apply Membership_laws_38; intuition.
induction H.
induction H; induction H0.
intros.
induction y.
induction H3.
generalize (H1 a0 H3); intros.
generalize (H2 b0 H4); intros.
inversion H5; inversion H6.
exists (x,x0).
constructor; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 13
  *)
Theorem Membership_laws_41: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (f: Ensemble (S*T)) (g: Ensemble (U*V)), (f ∈ (s → t) ∧ g ∈ (u → v)) ⇒((f∥g) ∈ ((s×u) → (t×v))).
Proof.
intros S T U V s t u v p q H.
constructor.
induction H.
induction H; induction H0.
apply Membership_laws_38; intuition.
induction H.
induction H; induction H0.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H3.
induction H3.
induction H3.
induction H1.
induction H2.
inversion H3.
constructor.
constructor.
exists z; intuition.
constructor.
exists w; intuition.
induction H1.
induction H2.
constructor.
induction H3.
induction H1; induction H2.
induction H1; induction H2.
exists (x,x0); constructor; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 14
  *)
Theorem Membership_laws_42: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (f: Ensemble (S*T)) (g: Ensemble (U*V)), (f ∈ (s ↣ t) ∧ g ∈ (u ↣ v)) ⇒((f∥g) ∈ ((s×u) ↣ (t×v))).
Proof.
intros S T U V s t u v p q H.
constructor.
induction H.
induction H; induction H0.
apply Membership_laws_41; intuition.
induction H.
induction H; induction H0.
apply Membership_laws_39; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 15
  *)
Theorem Membership_laws_43: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (f: Ensemble (S*T)) (g: Ensemble (U*V)), (f ∈ (s ↠ t) ∧ g ∈ (u ↠ v)) ⇒((f∥g) ∈ ((s×u) ↠ (t×v))).
Proof.
intros S T U V s t u v p q H.
constructor.
induction H.
induction H; induction H0.
apply Membership_laws_41; intuition.
induction H.
induction H; induction H0.
apply Membership_laws_40; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 16
  *)
Theorem Membership_laws_44: forall (S T U V: Type) (s: Ensemble S) (t: Ensemble T) (u: Ensemble U) (v: Ensemble V) (f: Ensemble (S*T)) (g: Ensemble (U*V)), (f ∈ (s ⤖ t) ∧ g ∈ (u ⤖ v)) ⇒((f∥g) ∈ ((s×u) ⤖ (t×v))).
Proof.
intros S T U V s t u v p q H.
constructor.
induction H.
induction H; induction H0.
apply Membership_laws_41; intuition.
induction H.
induction H; induction H0.
induction H1; induction H2.
constructor.
apply Membership_laws_39; intuition.
apply Membership_laws_40; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 17
  *)
Theorem Membership_laws_45: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s ⇸ t) ∧ g ∈ (s ⇸ t) ∧ (domain(g)◁ f) = (domain(f)◁ g)) ⇒((f ∪ g) ∈ (s ⇸ t)).
Proof.
intros S T s t f g H.
decompose [and] H.
constructor.
constructor.
constructor.
intros x H4.
induction H4.
repeat (induction H2).
apply H2; intuition.
repeat (induction H3).
apply H3; intuition.
intros.
assert (Same_set (S*T) (domain_restriction f (domain g)) (domain_restriction g (domain f))).
rewrite H1; intuition.
induction H5.
inversion H0.
inversion H4.
induction H2.
apply H11 with x; intuition.
assert (In (domain_restriction g (domain f)) (x,z)).
constructor.
intuition.
constructor; exists y; intuition.
generalize (H6 (x,z) H11); intros.
inversion H12.
induction H2.
apply H17 with x; intuition.
inversion H4.
assert (In (domain_restriction g (domain f)) (x,y)).
constructor.
intuition.
constructor; exists z; intuition.
generalize (H6 (x,y) H11); intros.
inversion H12.
induction H2.
apply H17 with x; intuition.
induction H3.
apply H11 with x; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 18
  *)
Theorem Membership_laws_46: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s ⤔ t) ∧ g ∈ (s ⤔ t) ∧ (domain(g)◁ f) = (domain(f)◁ g) ∧ (f ▷ range(g)) = (g ▷ range(f))) ⇒((f ∪ g) ∈ (s ⤔ t)).
Proof.
intros S T s t f g H.
decompose [and] H.
constructor.
induction H0; induction H4.
apply Membership_laws_45; intuition.
intros.
assert (Same_set (S*T) (range_restriction f (range g)) (range_restriction g (range f))).
rewrite H1; intuition.
induction H6.
inversion H2.
inversion H5.
induction H0.
apply H12 with y; intuition.
assert (In (range_restriction g (range f)) (z,y)).
constructor.
intuition.
constructor; exists x; intuition.
generalize (H7 (z,y) H12); intros.
inversion H13.
induction H0.
apply H18 with y; intuition.
inversion H5.
assert (In (range_restriction g (range f)) (x,y)).
constructor.
intuition.
constructor; exists z; intuition.
generalize (H7 (x,y) H12); intros.
inversion H13.
induction H0.
apply H18 with y; intuition.
induction H4.
apply H12 with y; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 19
  *)
Theorem Membership_laws_47: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s ⇸ t) ∧ g ∈ (s ↔ t)) ⇒((f ∩ g) ∈ (s ⇸ t)).
Proof.
intros S T s t f g H.
decompose [and] H.
constructor.
constructor.
constructor.
intros x H2.
induction H2.
repeat (induction H1).
apply H1; intuition.
intros.
inversion H2; inversion H3.
induction H0.
apply H10 with x; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 20
  *)
Theorem Membership_laws_48: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s ⤔ t) ∧ g ∈ (s ↔ t)) ⇒((f ∩ g) ∈ (s ⤔ t)).
Proof.
intros S T s t f g H.
decompose [and] H.
constructor.
induction H0.
apply Membership_laws_47; intuition.
intros.
inversion H2; inversion H3.
induction H0.
eapply H10 with y; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 21
  *)
Theorem Membership_laws_49: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (g: Ensemble (S*T)), (f ∈ (s ⤔ t) ∧ g ∈ (s ↔ t)) ⇒((f ∖ g) ∈ (s ⤔ t)).
Proof.
intros S T s t f g H.
decompose [and] H.
constructor.
constructor.
constructor.
constructor.
intros x H2.
induction H2.
repeat (induction H0).
apply H0; intuition.
intros.
inversion H2; inversion H3.
induction H0.
induction H0.
apply H9 with x; intuition.
intros.
inversion H2; inversion H3.
induction H0.
apply H8 with y; intuition.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 22
  *)
Theorem Membership_laws_50: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (x: S) (y: T), (x ∈ s ∧ y ∈ t) ⇒((Singleton (x↦y)) ∈ (s ⇸ t)).
Proof.
intros S T s t x y H.
induction H.
constructor.
constructor.
constructor.
intros f H1.
induction H1.
constructor; intuition.
intros.
inversion H1.
inversion H2.
rewrite <- H5; rewrite <- H7; reflexivity.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 23
  *)
Theorem Membership_laws_51: forall (S T: Type) (s: Ensemble S) (t: Ensemble T), ((s×t) ∈ (s ↔ t)).
Proof.
intros S T s t.
constructor.
constructor.
intros x H.
assumption.
Qed.

(**
  BBook: section 2.6.1, p.96, array 1, row 24
  *)
Theorem Membership_laws_52: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (x: S), (f ∈ (s ⇸ t) ∧ x ∈ domain(f)) ⇒(exists applicable_f:(In (partial_function s t) f /\ In (domain f) x), app f x applicable_f ∈ t).
Proof.
intros S T s t f x H.
exists H.
apply application_in_codomain.
Qed.


Close Scope eB_scope.
