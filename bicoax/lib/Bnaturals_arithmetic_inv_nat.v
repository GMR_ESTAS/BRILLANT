(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)

Require Import Bchapter1.
Require Import Bchapter2.
Require Import Constructive_sets.
Require Import Bnaturals_basics.
Require Import Bnaturals_ensembles.
Require Import Bnaturals_min.
Require Import Arith.
Require Import Euclid.
Require Import coq_missing_arith.
Require Import Bnaturals_arithmetic_nat.

Open Local Scope nat_scope.

(**
  * nat version of inverse arithmetic operations
  *)

(**
  ** Subtraction
  *)

Definition above_set_nat (m: nat) := (Comprehension (fun n => m <= n)).

Theorem bbnat_plus_c1_total_function_le: forall (m: nat),
  In (total_function (Full_set nat) (above_set_nat m)) (bbnat_plus_c1 m).
Proof.
intros m.
generalize (bbnat_plus_c1_total m); intros.
constructor.
constructor.

constructor.
constructor.
red; intros.
induction x.
constructor; [ constructor | ].
do 3 red.
replace b with (a + m) in H0 |- *.
intuition.
inversion H.
inversion H1.
apply H5 with a.
rewrite plus_comm; rewrite <- valid_bbnat_plus_nat_curry.
apply app_trivial_property.
assumption.

induction H; induction H; assumption.

induction H; assumption.
Qed.

(**
  property 3.5.13, BBook: section 3.5.7, p.163
  *)
Theorem bbnat_plus_c1_total_bijection_le: forall (m: nat),
  In (total_bijection (Full_set nat) (above_set_nat m)) (bbnat_plus_c1 m).
Proof.
intros m.
generalize (bbnat_plus_c1_total_function_le m); intros.
constructor.
assumption.
constructor.
constructor.
induction H; assumption.
intros.
inversion H.
inversion H2.
assert (y = m + x).
apply H6 with x.
assumption.
rewrite <- valid_bbnat_plus_nat_curry; apply app_trivial_property.
assert (y = m + z).
apply H6 with z.
assumption.
rewrite <- valid_bbnat_plus_nat_curry; apply app_trivial_property.
apply plus_reg_l with m.
rewrite <- H8; rewrite <- H9; reflexivity.
constructor.
induction H; assumption.
intros.
do 3 (red in H0).
exists (y - m).
replace (y-m,y) with (y-m, m+(y-m)).
rewrite <- valid_bbnat_plus_nat_curry; apply app_trivial_property.
intuition.
Qed.


Section BBook_subtraction_nat.
Variable m: nat.

(**
  definition of subtraction, BBook: section 3.5.7, p.163, array 2
  *)
Definition bbnat_minus_c1 := (domain_restriction (inverse (bbnat_plus_c1 m)) (above_set_nat m)).

Theorem bbnat_minus_c1_total: In (total_function (above_set_nat m) (Full_set nat)) bbnat_minus_c1.
Proof.
generalize (bbnat_plus_c1_total_bijection_le m); intros.
constructor.
constructor.

do 2 constructor.
red; intros.
induction x.
split.
inversion H0.
assumption.
constructor.

intros.
inversion H0; inversion H1.
inversion H4; inversion H8.
inversion H.
inversion H17.
inversion H19.
apply H23 with x; assumption.

unfold bbnat_minus_c1.
rewrite Equality_laws_dom_06 with
  nat nat (above_set_nat m) (Full_set nat) (above_set_nat m) (inverse (bbnat_plus_c1 m)).
rewrite Equality_laws_dom_02 with
  nat nat (Full_set nat) (above_set_nat m) (bbnat_plus_c1 m).
rewrite Equality_laws_ran_01 with
   nat nat (Full_set nat) (above_set_nat m) (bbnat_plus_c1 m).
rewrite neutral_element_2.
reflexivity.
intuition.
induction H.
induction H.
induction H0.
assumption.
do 3 (induction H); assumption.
split; [ intuition | ].
apply Membership_laws_01.
do 3 (induction H); assumption.
Qed.

End BBook_subtraction_nat.

Definition bbnat_minus: Ensemble (nat * (Ensemble (nat * nat))) :=
   fun cpl => match cpl with | (m, invplus_m_n) => (bbnat_minus_c1 m) = invplus_m_n end.

(**
  ** Subtraction: Validity and properties
  *)

Theorem bbnat_minus_y: forall (m: nat),
  In (partial_function (Full_set nat) (partial_function (Full_set nat) (Full_set nat))) bbnat_minus /\ In (domain bbnat_minus) m.
Proof.
intros m.
split.
constructor.

do 2 constructor.
red; intros.
induction x.
constructor.
constructor.
generalize (bbnat_minus_c1_total a); intros.
inversion H.
inversion H0.
assert (Included (above_set_nat a) (Full_set nat) /\ Included (Full_set nat) (Full_set nat)).
split.
red; intros; constructor.
intuition.
apply (Monotonicity_laws_02 _ _ (Full_set nat) (Full_set nat) (above_set_nat a) (Full_set nat) H5).
assumption.

intros.
inversion H; inversion H0.
reflexivity.

constructor.
exists (bbnat_minus_c1 m).
constructor.
Qed.

Theorem bbnat_minus_c1_y: forall (m n: nat), m <= n ->
  In (partial_function (Full_set nat) (Full_set nat)) (bbnat_minus_c1 m)
/\ In (domain (bbnat_minus_c1 m)) n.
Proof.
intros m n H.
split.
constructor.

do 2 constructor.
red; intros.
induction x; split; constructor.

intros.
generalize (bbnat_minus_c1_total m); intros.
induction H2; induction H2.
apply H4 with x; assumption.

constructor.
exists (n - m).
constructor.
constructor.
replace (n - m, n) with (n - m, m + (n - m)).
rewrite <- valid_bbnat_plus_nat_curry; apply app_trivial_property.
intuition.

do 3 red; assumption.
Qed.

Theorem indirect_bbnat_minus_c1_y: forall (m n: nat), m <= n ->
  In (partial_function (Full_set nat) (Full_set nat)) (app bbnat_minus m (bbnat_minus_y m))
/\ In (domain (app bbnat_minus m (bbnat_minus_y m))) n.
Proof.
intros m n H.
replace (app bbnat_minus m (bbnat_minus_y m)) with (bbnat_minus_c1 m).
apply bbnat_minus_c1_y; assumption.
apply identifying_app.
constructor.
Qed.

Theorem valid_bbnat_minus_nat_curry: forall (m n: nat) (H: m <= n),
  (app (bbnat_minus_c1 m) n (bbnat_minus_c1_y m n H))
= (minus n m).
Proof.
intros m n H.
symmetry; apply identifying_app.
constructor.
constructor.
replace (n - m, n) with (n - m, m + (n - m)).
rewrite <- valid_bbnat_plus_nat_curry; apply app_trivial_property.
intuition.
do 3 red; assumption.
Qed.

Theorem valid_bbnat_minus_nat: forall (m n: nat) (H: m <= n),
  (app (app bbnat_minus m (bbnat_minus_y m)) n (indirect_bbnat_minus_c1_y m n H))
= (minus n m).
Proof.
intros m n H.
replace (app (app bbnat_minus m (bbnat_minus_y m)) n (indirect_bbnat_minus_c1_y m n H))
  with (app (bbnat_minus_c1 m) n (bbnat_minus_c1_y m n H)).
apply valid_bbnat_minus_nat_curry.
apply identifying_app.
replace (app bbnat_minus m (bbnat_minus_y m)) with (bbnat_minus_c1 m).
apply app_trivial_property.
apply identifying_app.
constructor.
Qed.

(** Note: beware, now the order of the parameters is the "natural" order for subtraction *)
Definition bbnat_minus_bin (n m: nat) (H: m <= n) :=
  (app (app bbnat_minus m (bbnat_minus_y m)) n (indirect_bbnat_minus_c1_y m n H)).
Hint Unfold bbnat_minus_bin.

(**
  proof at the bottom of the page, BBook: section 3.5.7, p.163
  *)
Theorem BBook_plus_minus_nat_r: forall (m n: nat) (H: m <= n),
  n = bbnat_plus_bin m (bbnat_minus_bin n m H).
Proof.
intros m n H.
unfold bbnat_minus_bin.
rewrite valid_bbnat_minus_nat.
unfold bbnat_plus_bin.
rewrite valid_bbnat_plus_nat.
intuition.
Qed.

(**
  ** Division
  *)

(**
  definition of division, BBook: section 3.5.7, p.164, array 2, row 1
  *)
Definition Bnat_div_bin (n m: nat) :=
  Bmin (Comprehension (fun x => n < m * (S x))).

(**
  proof left to the reader, BBook: section 3.5.7, p.164
  *)
Theorem defining_set_of_div_not_empty_nat: forall (n m: nat), m > 0 ->
  (Comprehension (fun x => n < m * (S x))) <> (Empty_set nat).
Proof.
intros n m H.
apply (Inhabited_not_empty _ (Comprehension (fun x => n < m * (S x)))).
induction n.
apply Inhabited_intro with 0.
do 2 red.
rewrite mult_comm; simpl.
rewrite plus_0_r.
induction H; apply lt_O_Sn.
induction IHn.
do 2 (red  in H0).
apply Inhabited_intro with (S x).
do 2 red.
assert (0 < m).
induction H; apply lt_O_Sn.
apply lt_le_trans with (S (m * S x)).
apply lt_n_S; assumption.
rewrite <- mult_n_Sm.
rewrite <- mult_n_Sm.
rewrite mult_n_Sm.
replace (S (m * S x)) with (m * S x + 1).
apply plus_le_compat.
intuition.
intuition.
ring.
Qed.

(**
  property (2), BBook: section 3.5.7, p.163, array 1, row 2

  n < m * succ(n/m)
  *)
Theorem Bnat_div_property_nat_2: forall (n m: nat) (H: m > 0),
  n < m * (S (Bnat_div_bin n m)).
Proof.
intros n m H.
assert (In (Power_set1 bbN) (Ensemble_bbN_of_nat (Comprehension (fun x => n < m * (S x))))).
apply Power_set1_Ensemble_bbN_of_nat.
split.
constructor; red; intros; constructor.
generalize (defining_set_of_div_not_empty_nat n m H); intros.
intros H1; inversion H1.
apply H0; rewrite H3; reflexivity.
assert (In (Comprehension (fun x => n < m * (S x))) (Bnat_div_bin n m)).
unfold Bnat_div_bin.
apply valid_Ensemble_nat_belonging_r.
rewrite valid_nat_min.
rewrite bbN_of_nat_of_bbN.
apply bbN_min_in_its_argument.
assumption.
apply bbN_min_in_bbN.
assumption.
split.
constructor; red; intros; constructor.
generalize (defining_set_of_div_not_empty_nat n m H); intros.
intros H2; inversion H2.
apply H1; rewrite H4; reflexivity.
do 2 (red in H1).
assumption.
Qed.

Theorem Bnat_div_smallest_property: forall (n m: nat) (H: m > 0),
  forall (p:nat), n < m * S p -> (Bnat_div_bin n m) <= p.
Proof.
intros n m H.
assert (In (Power_set1 (Full_set nat)) (Comprehension (fun x => n < m * (S x)))).
split.
constructor; red; intros; constructor.
generalize (defining_set_of_div_not_empty_nat n m H); intros.
intros H1; inversion H1.
apply H0; rewrite H3; reflexivity.
assert (In (Power_set1 bbN) (Ensemble_bbN_of_nat (Comprehension (fun x => n < m * (S x))))).
apply Power_set1_Ensemble_bbN_of_nat; assumption.
intros.
unfold Bnat_div_bin.
apply valid_nat_le_r.
rewrite valid_nat_min.
rewrite bbN_of_nat_of_bbN.
apply bbN_min_is_minimal.
assumption.
apply valid_Ensemble_nat_belonging_l.
do 2 red; intuition.
apply bbN_min_in_bbN.
assumption.
assumption.
Qed.

(**
  property (1), BBook: section 3.5.7, p.163, array 1, row 1

  m * (n/m) <= n
  *)
Theorem Bnat_div_property_nat_1: forall (n m: nat) (H: m > 0),
  m * (Bnat_div_bin n m) <= n.
Proof.
intros n m H.
assert (In (Power_set1 (Full_set nat)) (Comprehension (fun x => n < m * (S x)))).
split.
constructor; red; intros; constructor.
generalize (defining_set_of_div_not_empty_nat n m H); intros.
intros H1; inversion H1.
apply H0; rewrite H3; reflexivity.
assert (In (Power_set1 bbN) (Ensemble_bbN_of_nat (Comprehension (fun x => n < m * (S x))))).
apply Power_set1_Ensemble_bbN_of_nat; assumption.
assert (Bnat_div_bin n m = 0 \/ ~(Bnat_div_bin n m = 0)); [ apply classic | ].
induction H2.
rewrite H2.
rewrite mult_0_r.
intuition.
replace (Bnat_div_bin n m) with (S (pred (Bnat_div_bin n m))).
cut ((pred (Bnat_div_bin n m)) < (Bnat_div_bin n m)).
generalize (pred (Bnat_div_bin n m)).
intros x.
apply (proj2 (contraposition_1 (x < (Bnat_div_bin n m)) (m * S x <= n))).
intros.
apply le_not_lt.
generalize (not_le _ _ H3); intros.
apply Bnat_div_smallest_property; assumption.
apply lt_pred_n_n.
apply neq_O_lt; intuition.
symmetry; apply S_pred with 0.
apply neq_O_lt; intuition.
Qed.

(**
  definition of modulo, BBook: section 3.5.7, p.164, array 2, row 2
  *)
Definition Bnat_mod_bin (n m: nat) := n - m * (Bnat_div_bin n m).

(**
  ** Division: validity
  *)

Theorem valid_Bnat_div_bin: forall (n m: nat) (H: m > 0),
  (proj1_sig (quotient m H n)) = Bnat_div_bin n m.
Proof.
intros n m H.
symmetry; apply min_multiple_is_quotient.
apply Bnat_div_property_nat_2; assumption.
apply Bnat_div_smallest_property; assumption.
Qed.

Theorem valid_Bnat_mod_bin: forall (n m: nat) (H: m > 0),
  (proj1_sig (modulo m H n)) = Bnat_mod_bin n m.
Proof.
intros n m H.
unfold Bnat_mod_bin.
rewrite <- valid_Bnat_div_bin with n m H.
generalize (sigma_euclid n m H); intros.
induction H0.
rewrite mult_comm.
intuition.
Qed.

(**
  ** Logarithms
  *)

(**
  definition of log, BBook: section 3.5.7, p.165, array 1
  *)
Definition Bnat_log_bin (n m: nat) :=
  Bmin (Comprehension (fun x => n < exp m (S x))).

(**
  definition of LOG, BBook: section 3.5.7, p.165, array 3
  *)
Definition Bnat_LOG_bin (n m: nat) :=
  Bmin (Comprehension (fun x => n <= exp m x)).

(** log m (n) :
  min ({ x | x : N & n < exp m (succ x)})    m > 1
with l = log m (n):
TODO  l = 0 => n < m
TODO  l > 0 => exp m l <= n < exp m (l+1)
*)

(** LOG m (n):
  min ({x | x : N & n <= exp m x}) m > 1
with L = LOG m (n)
TODO  L = 0 => n <= 1
TODO  L > 0 => exp m (L-1) < n <= exp m L
*)

(** Relations :
TODO  L = l = 0 when n = 0
TODO  L = l when exp m l = n
TODO  L = l + 1 when exp m l < n
*)
