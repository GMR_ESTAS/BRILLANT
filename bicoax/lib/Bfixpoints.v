(*
    Copyright (c) 2008,2009 Samuel Colin

    This file is part of BiCoax.

    BiCoax is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    BiCoax is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with BiCoax.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import Bchapter2.
Require Import Bgeneralized_union_inter.

(**
  * Fixpoints: definitions
  *)


(**
  Least fixpoint, BBook: section 3.2.2, p.133, array 1, row 1

  Contrary to earlier constructs, we specify here the set over which
  the fixpoint is applied.  In earlier constructs, the property of the
  construct was independent enough from a given set that it could be
  supplied in theorems' hypotheses. Here the fixpoint construction
  seem way more dependent on it. Earlier tests of proving the validity
  of the definitions without this set failed because, roughly told,
  the fixpoint property over a set was not necessarily for another set
  of the same datatype.
  *)
Inductive Bfix (S: Type) (s: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S)): Ensemble S :=
Bfix_intro: forall (x:S),
In (GenIntersection (Comprehension (fun x =>
(exists applicable_f:(In (partial_function (Power_set s) (Power_set s)) f /\ (In (domain f) x)), Included (app f x applicable_f) x)))) x
 -> Bfix S s f x.
Implicit Arguments Bfix [S].

(**
  Greatest fixpoint, BBook: section 3.2.2, p.133, array 1, row 2
  *)
Inductive BFIX (S: Type) (s: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S)): Ensemble S :=
BFIX_intro: forall (x:S),
In (GenUnion (Comprehension (fun x =>
(exists applicable_f:(In (partial_function (Power_set s) (Power_set s)) f /\ (In (domain f) x)), Included x (app f x applicable_f))))) x
 -> BFIX S s f x.
Implicit Arguments BFIX [S].

(**
  * Fixpoints: validity
  *)

(**
  Least fixpoint, BBook: section 3.2.2, p.133, array 1, row 1
  *)
Theorem valid_Bfix: forall (S: Type) (s: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S)),
  In (total_function (Power_set s) (Power_set s)) f ->
  Bfix s f =
  GenIntersection (Comprehension (fun x => In (Power_set s) x /\ (exists applicable_f:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) x), Included (app f x applicable_f) x))).
Proof.
intros S s f H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
constructor.
intros y H1.
apply H0.
induction H1.
induction H2.
exists x0.
intros z H3.
apply H2; intuition.

induction H0.
constructor.
constructor.
intros y H1.
apply H0.
induction H1.
constructor.
inversion x0.
induction H.
rewrite <- H4; assumption.
exists x0.
intros x1 H2.
apply H1; assumption.
Qed.

(**
  Greatest fixpoint, BBook: section 3.2.2, p.133, array 1, row 2
  *)
Theorem valid_BFIX: forall (S: Type) (s: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S)),
  In (total_function (Power_set s) (Power_set s)) f ->
  BFIX s f =
  GenUnion (Comprehension (fun x => In (Power_set s) x /\ (exists applicable_f:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) x), Included x (app f x applicable_f)))).
Proof.
intros S s f H.
apply Extensionality_Ensembles; unfold Same_set, Included; split; intros x H0.

induction H0.
induction H0.
induction H0.
constructor.
exists x0.
induction H0.
induction H0.
split; [ | assumption ].
constructor.
inversion x1.
induction H.
rewrite <- H4; assumption.
exists x1.
intuition.

induction H0.
induction H0.
constructor.
constructor.
exists x0.
induction H0.
induction H0.
constructor; [ | assumption ].
induction H2.
exists x1.
intuition.
Qed.

(**
  * Fixpoints: theorems
  *)

Theorem fixpoint_applicability: forall (S:Type) (s t: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S)),
Included t s -> In (total_function (Power_set s) (Power_set s)) f -> In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) t.
Proof.
intros S s t f H H0.
induction H0.
split; [ assumption | ].
rewrite H1.
constructor.
assumption.
Qed.


(**
  fix is a lower bound (theorem 3.2.1), BBook: section 3.2.2, p.133
  *)
Theorem fix_lower_bound: forall (S:Type) (s t: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S))
  (total_f: (In (total_function (Power_set s) (Power_set s)) f)) (t_inc_s: (Included t s)), Included (app f t (fixpoint_applicability S s t f t_inc_s total_f)) t ->
  Included (Bfix s f) t.
Proof.
intros S s t f total_f t_inc_s H.

assert (Included (GenIntersection (Comprehension (fun x => exists applicable_f:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) x), Included (app f x applicable_f) x))) t).
apply GenIntersection_lower_bound.
assert (In (domain f) t).
inversion total_f.
rewrite H1.
constructor.
assumption.
assert (In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) t).
inversion total_f.
intuition.
exists H1.
assert (app f t H1 = app f t (fixpoint_applicability S s t f t_inc_s total_f)).
inversion H1.
induction H2.
apply H4 with t; apply app_trivial_property.
rewrite H2; assumption.
intros z H1; apply H0.
inversion H1.
assumption.
Qed.

(** This goal is needed often in the next theorems *)
Theorem fix_included_in_its_base_set: forall (S:Type) (s: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S))
  (total_f: (In (total_function (Power_set s) (Power_set s)) f)), Included (Bfix s f) s.
Proof.
intros S s f H.
assert (Included s s); [ intuition | ].
apply fix_lower_bound with H H0.
assert (In (Power_set s) (app f s (fixpoint_applicability S s s f H0 H))).
apply application_in_codomain.
induction H1; assumption.
Qed.


(**
  fix is the greatest lower bound (theorem 3.2.2), BBook: section 3.2.2, p.133

  This definition is a transcription of the same theorem for "inter",
  but remember that our definition of fix removes the need for t to be
  in "Power_set s", hence we remove it also here in the transcription.
  *)
Theorem fix_greatest_lower_bound: forall (S:Type) (s v: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S))
  (total_f: (In (total_function (Power_set s) (Power_set s)) f)) (v_inc_s: (Included v s)),
  (forall (t: Ensemble S), (exists applicable_f:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) t), Included (app f t applicable_f) t) -> Included v t) ->
  Included v (Bfix s f).
Proof.
intros S s v f H v_inc_s H0.

cut (Included v (GenIntersection (Comprehension (fun x => exists applicable_f:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) x), Included (app f x applicable_f) x)))).
intros.
intros x H2.
constructor; intuition.
apply (GenIntersection_greatest_lower_bound _ _ _ _ v_inc_s H0).
Qed.

(**
  FIX is an upper bound (theorem 3.2.3), BBook: section 3.2.2, p.134
  *)
Theorem FIX_upper_bound: forall (S:Type) (s t: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S))
  (total_f: (In (total_function (Power_set s) (Power_set s)) f)) (t_inc_s: (Included t s)), Included t (app f t (fixpoint_applicability S s t f t_inc_s total_f)) ->
  Included t (BFIX s f).
Proof.
intros S s t f total_f t_inc_s H.

assert (Included t (GenUnion (Comprehension (fun x => exists applicable_f:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) x), Included x (app f x applicable_f))))).
apply GenUnion_upper_bound.
assert (In (domain f) t).
inversion total_f.
rewrite H1.
constructor.
assumption.
assert (In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) t).
inversion total_f.
intuition.
exists H1.
assert (app f t H1 = app f t (fixpoint_applicability S s t f t_inc_s total_f)).
inversion H1.
induction H2.
apply H4 with t; apply app_trivial_property.
rewrite H2; assumption.
intros z H1; constructor.
apply H0; assumption.
Qed.

(**
  FIX is the least upper bound (theorem 3.2.4), BBook: section 3.2.2, p.134
  *)
Theorem FIX_least_upper_bound: forall (S:Type) (s v: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S))
  (total_f: (In (total_function (Power_set s) (Power_set s)) f)) (v_inc_s: (Included v s)),
  (forall (t: Ensemble S), (exists applicable_f:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) t), Included t (app f t applicable_f)) -> Included t v) ->
  Included (BFIX s f) v.
Proof.
intros S s v f H v_inc_s H0.

cut (Included (GenUnion (Comprehension (fun x => exists applicable_f:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) x), Included x (app f x applicable_f)))) v).
intros.
intros x H2.
apply H1; induction H2; intuition.
apply (GenUnion_least_upper_bound _ _ _ _ v_inc_s H0).
Qed.

(** This goal is needed often in the next theorems *)
Theorem FIX_included_in_its_base_set: forall (S:Type) (s: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S))
  (total_f: (In (total_function (Power_set s) (Power_set s)) f)), Included (BFIX s f) s.
Proof.
intros S s f H.
apply FIX_least_upper_bound.
assumption.
intuition.
intros t H0.
inversion H0.
inversion x.
assert (In (Power_set s) t).
induction H.
rewrite <- H4; assumption.
induction H4; assumption.
Qed.


(** We need this to avoid having an "exists" in Knaster-Tarski's theorem *)
Theorem fix_applicability: forall (S:Type) (s: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S)),
 In (total_function (Power_set s) (Power_set s)) f -> In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) (Bfix s f).
Proof.
intros S s f H.
induction H.
split; [ intuition | rewrite H0; constructor; apply fix_included_in_its_base_set ].
constructor; assumption.
Qed.

(** same as above *)
Theorem FIX_applicability: forall (S:Type) (s: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S)),
 In (total_function (Power_set s) (Power_set s)) f -> In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) (BFIX s f).
Proof.
intros S s f H.
induction H.
split; [ intuition | rewrite H0; constructor; apply FIX_included_in_its_base_set ].
constructor; assumption.
Qed.


(**
  Knaster-Tarski theorem for fix (theorem 3.2.5), BBook: section 3.2.2, p.134
  *)
Theorem fix_Knaster_Tarski: forall (S:Type) (s: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S))
  (total_f: In (total_function (Power_set s) (Power_set s)) f),
  (forall (x y: Ensemble S), In (times (Power_set s) (Power_set s)) (x,y) /\ Included x y -> exists appli_x:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) x), exists appli_y:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) y), Included (app f x appli_x) (app f y appli_y)) ->
  (app f (Bfix s f) (fix_applicability S s f total_f)) = (Bfix s f).
Proof.
intros S s f H H0.
apply Extensionality_Ensembles; unfold Same_set; split.

apply fix_greatest_lower_bound.
assumption.
assert (In (Power_set s) (app f (Bfix s f) (fix_applicability S s f H))).
apply application_in_codomain.
induction H1; assumption.
intros t H1.
induction H1.
apply inclusion_transitivity with (app f t x).
assert (In (times (Power_set s) (Power_set s)) (Bfix s f, t) /\ Included (Bfix s f) t).
inversion H.
rewrite <- H3.
split.
inversion x; constructor;  [ rewrite H3; constructor; apply fix_included_in_its_base_set; assumption | intuition ].
assert (Included t s).
assert (In (Power_set s) t); inversion x; [ rewrite <- H3; intuition | ].
induction H5; assumption.
apply fix_lower_bound with H H5.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ t (fixpoint_applicability S s t f H5 H) x).
assumption.
generalize (H0 (Bfix s f) t H2); intros.
inversion H3.
inversion H4.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ (Bfix s f) (fix_applicability S s f H) x0).
rewrite (irrelevant_applicability _ _ _ _ _ _ _ t x x1).
assumption.
assumption.

assert (Included (app f (Bfix s f) (fix_applicability S s f H)) s).
inversion H; inversion H1.
assert (In (Power_set s) (app f (Bfix s f) (fix_applicability S s f H))).
apply application_in_codomain.
induction H7; assumption.
apply fix_lower_bound with H H1.
assert (In (times (Power_set s) (Power_set s)) (app f (Bfix s f) (fix_applicability S s f H), Bfix s f) /\ Included (app f (Bfix s f) (fix_applicability S s f H)) (Bfix s f)).
split.
constructor.
constructor; assumption.
constructor; apply fix_included_in_its_base_set; assumption.
apply fix_greatest_lower_bound.
assumption.
assumption.
intros t H2.
induction H2.
apply inclusion_transitivity with (app f t x).
assert (In (times (Power_set s) (Power_set s)) (Bfix s f, t) /\ Included (Bfix s f) t).
split.
inversion H.
constructor; [ constructor; apply fix_included_in_its_base_set; assumption | rewrite <- H4; inversion x; assumption ].
assert (Included t s).
inversion H; inversion x.
assert (In (Power_set s) t).
rewrite <- H4; assumption.
induction H8; assumption.
apply fix_lower_bound with H H3.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ t (fixpoint_applicability S s t f H3 H) x).
assumption.
generalize (H0 (Bfix s f) t H3); intros.
inversion H4.
inversion H5.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ (Bfix s f) (fix_applicability S s f H) x0).
rewrite (irrelevant_applicability _ _ _ _ _ _ _ t x x1).
assumption.
assumption.
generalize (H0 (app f (Bfix s f) (fix_applicability S s f H)) (Bfix s f) H2); intros.
inversion H3; inversion H4.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ (app f (Bfix s f) (fix_applicability S s f H))  (fixpoint_applicability S s (app f (Bfix s f) (fix_applicability S s f H)) f H1 H) x).
rewrite (irrelevant_applicability _ _ _ _ _ _ _ (Bfix s f) (fix_applicability S s f H) x0).
assumption.
Qed.

(**
  Knaster-Tarski theorem for FIX (theorem 3.2.6), BBook: section 3.2.2, p.136
  *)
Theorem FIX_Knaster_Tarski: forall (S:Type) (s: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S))
  (total_f: In (total_function (Power_set s) (Power_set s)) f),
  (forall (x y: Ensemble S), In (times (Power_set s) (Power_set s)) (x,y) /\ Included x y -> exists appli_x:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) x), exists appli_y:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) y), Included (app f x appli_x) (app f y appli_y)) ->
  (app f (BFIX s f) (FIX_applicability S s f total_f)) = (BFIX s f).
Proof.
intros S s f H H0.
apply Extensionality_Ensembles; unfold Same_set; split.

assert (Included (app f (BFIX s f) (FIX_applicability S s f H)) s).
inversion H; inversion H1.
assert (In (Power_set s) (app f (BFIX s f) (FIX_applicability S s f H))).
apply application_in_codomain.
induction H7; assumption.
apply FIX_upper_bound with H H1.
assert (In (times (Power_set s) (Power_set s)) (BFIX s f, app f (BFIX s f) (FIX_applicability S s f H)) /\ Included (BFIX s f) (app f (BFIX s f) (FIX_applicability S s f H)) ).
split.
constructor.
constructor; apply FIX_included_in_its_base_set; assumption.
constructor; assumption.
apply FIX_least_upper_bound.
assumption.
assumption.
intros t H2.
induction H2.
apply inclusion_transitivity with (app f t x).
assumption.
assert (In (times (Power_set s) (Power_set s)) (t, BFIX s f) /\ Included t (BFIX s f)).
split.
constructor.
inversion H.
rewrite <- H4; inversion x; assumption.
constructor; apply FIX_included_in_its_base_set; assumption.
assert (Included t s).
inversion H; inversion x.
apply (proj2 (valid_inclusion S t s)).
rewrite <- H4; assumption.
apply FIX_upper_bound with H H3.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ t (fixpoint_applicability S s t f H3 H) x).
assumption.
generalize (H0 t (BFIX s f) H3); intros.
inversion H4.
inversion H5.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ t x x0).
rewrite (irrelevant_applicability _ _ _ _ _ _ _ (BFIX s f) (FIX_applicability S s f H) x1).
assumption.
generalize (H0 (BFIX s f) (app f (BFIX s f) (FIX_applicability S s f H)) H2); intros.
inversion H3; inversion H4.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ (app f (BFIX s f) (FIX_applicability S s f H))  (fixpoint_applicability S s (app f (BFIX s f) (FIX_applicability S s f H)) f H1 H) x0).
(* SC: Coq has problems here with second-order unification. Weird *)
intros z H6.
apply H5.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ (BFIX s f) x (FIX_applicability S s f H)).
assumption.

apply FIX_least_upper_bound.
assumption.
assert (In (Power_set s) (app f (BFIX s f) (FIX_applicability S s f H))).
apply application_in_codomain.
induction H1; assumption.
intros t H1.
induction H1.
apply inclusion_transitivity with (app f t x).
assumption.
assert (In (times (Power_set s) (Power_set s)) (t, BFIX s f) /\ Included t (BFIX s f)).
split.
inversion H.
constructor; [ rewrite <- H3; inversion x; assumption | constructor; apply FIX_included_in_its_base_set; assumption ].
assert (Included t s).
apply (proj2 (valid_inclusion S t s)); inversion x; inversion H; rewrite <- H5; assumption.
apply FIX_upper_bound with H H2.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ t (fixpoint_applicability S s t f H2 H) x).
assumption.
generalize (H0 t (BFIX s f) H2); intros.
inversion H3.
inversion H4.
rewrite (irrelevant_applicability _ _ _ _ _ _ _ (BFIX s f) (FIX_applicability S s f H) x1).
rewrite (irrelevant_applicability _ _ _ _ _ _ _ t x x0).
assumption.
Qed.

(**
  * Fixpoints: introduction of the induction principle
  *)

(**
  General induction principle (theorem 3.2.7), BBook: section 3.2.3, p.137
  *)
Theorem induction_principle: forall (S:Type) (s: Ensemble S) (f: Ensemble (Ensemble S * Ensemble S)) (P: S -> Prop),   In (total_function (Power_set s) (Power_set s)) f ->
  (exists appli_f:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) (Comprehension (fun x => (In (Bfix s f) x /\ P x)))), Included (app f (Comprehension (fun x => (In (Bfix s f) x /\ P x))) appli_f) (Comprehension (fun x => (In (Bfix s f) x /\ P x)))) ->
  forall (x: S), In (Bfix s f) x -> P x.
Proof.
intros S s f P H H0.
assert (forall x, In (Bfix s f) x -> (In (Bfix s f) x /\ P x)).
assert (Included (Bfix s f) (Comprehension (fun x => In (Bfix s f) x /\ P x))).
assert (Included  (Comprehension (fun x => In (Bfix s f) x /\ P x)) s).
assert (In (Power_set s) (Comprehension (fun x => In (Bfix s f) x /\ P x))).
inversion H0.
inversion x; inversion H.
rewrite <- H5; assumption.
induction H1; assumption.
apply fix_lower_bound with H H1.
inversion H0.
rewrite (irrelevant_applicability _ _ _ _ _ _ f (Comprehension (fun x => In (Bfix s f) x /\ P x)) (fixpoint_applicability S s (Comprehension (fun x => In (Bfix s f) x /\ P x)) f H1 H) x).
assumption.
intros; apply H1; assumption.
intros.
generalize (H1 x H2); intuition.
Qed.

(**
  ** First special case for having a simpler induction principle
  *)

Theorem first_special_case_is_total: forall (S: Type) (s: Ensemble S) (a: S) (g: Ensemble (S*S)) (f: Ensemble (Ensemble S * Ensemble S)),
In s a -> In (total_function s s) g ->
f = lambda (Power_set s) (fun z => (Union (Singleton a) (image g z))) ->
In (total_function (Power_set s) (Power_set s)) f.
Proof.
intros S s a g f H H0 H1.
rewrite H1.
apply lambda_is_total.
intros x H2.
constructor.
red; intros z H3.
induction H3.
induction H3; assumption.
induction H3.
induction H3.
(* TODO: need a tactic for this... *)
repeat (induction H0).
generalize (H0 (x0,y) H4); intros.
inversion H7; assumption.
Qed.

(**
  Description of the first special case, BBook: section 3.2.3, p.137
  *)
Theorem first_special_case: forall (S: Type) (s: Ensemble S) (a: S) (g: Ensemble (S*S)) (f: Ensemble (Ensemble S * Ensemble S)),
In s a -> In (total_function s s) g ->
f = lambda (Power_set s) (fun z => (Union (Singleton a) (image g z))) ->
Bfix s f = Union (Singleton a) (image g (Bfix s f)).
Proof.
intros S s a g f H H0 H1.
generalize (first_special_case_is_total S s a g f H H0 H1); intros.
assert (forall x y, In (times (Power_set s) (Power_set s)) (x,y) /\ Included x y -> (exists appli_x:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) x), exists appli_y:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) y), Included (app f x appli_x) (app f y appli_y))).
intros.
assert (In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) x).
induction H2.
induction H3; inversion H3.
split; [ assumption | rewrite H4; assumption ].
assert (In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) y).
induction H2.
induction H3; inversion H3.
split; [ assumption | rewrite H5; assumption ].
rewrite H1.
assert (In (partial_function (Power_set s) (Power_set s)) (lambda (Power_set s) (fun z => (Union (Singleton a) (image g z)))) /\ In (domain (lambda (Power_set s) (fun z => (Union (Singleton a) (image g z))))) x).
rewrite <- H1; assumption.
assert (In (partial_function (Power_set s) (Power_set s)) (lambda (Power_set s) (fun z => (Union (Singleton a) (image g z)))) /\ In (domain (lambda (Power_set s) (fun z => (Union (Singleton a) (image g z))))) y).
rewrite <- H1; assumption.
exists H6; exists H7.
assert ( app (lambda (Power_set s) (fun z => (Union (Singleton a) (image g z)))) x H6 = (Union (Singleton a) (image g x))).
symmetry; apply identifying_app.
constructor.
induction H3; inversion H3; assumption.
reflexivity.
assert ( app (lambda (Power_set s) (fun z => (Union (Singleton a) (image g z)))) y H7 = (Union (Singleton a) (image g y))).
symmetry; apply identifying_app.
constructor.
induction H3; inversion H3; assumption.
reflexivity.
rewrite H8; rewrite H9.
intros z H10.
induction H10.
left; induction H10; intuition.
right.
inversion H10.
red; apply image_intro with x1.
induction H3.
split; [ apply H13 | ]; intuition.
assert ((Union (Singleton a) (image g (Bfix s f))) = app f (Bfix s f) (fix_applicability S s f H2)).
apply identifying_app.
assert (In (lambda (Power_set s) (fun z => (Union (Singleton a) (image g z)))) (Bfix s f, Union (Singleton a) (image g (Bfix s f)))).
constructor.
constructor; apply fix_included_in_its_base_set; assumption.
reflexivity.
rewrite H1 in H4 |- *; assumption.
rewrite H4.
generalize (fix_Knaster_Tarski _ _ _ H2 H3).
intros.
symmetry; assumption.
Qed.

(**
  Property 3.2.1, BBook: section 3.2.3, p.137
  *)
Theorem first_special_case_prop1: forall (S: Type) (s: Ensemble S) (a: S) (g: Ensemble (S*S)) (f: Ensemble (Ensemble S * Ensemble S)),
In s a -> In (total_function s s) g ->
f = lambda (Power_set s) (fun z => (Union (Singleton a) (image g z))) ->
In (Bfix s f) a.
Proof.
intros S s a g f H H0 H1.
rewrite (first_special_case _ _ _ _ _ H H0 H1).
left; intuition.
Qed.

(**
  Property 3.2.2, BBook: section 3.2.3, p.137
  *)
Theorem first_special_case_prop2: forall (S: Type) (s: Ensemble S) (a: S) (g: Ensemble (S*S)) (f: Ensemble (Ensemble S * Ensemble S)),
In s a -> In (total_function s s) g ->
f = lambda (Power_set s) (fun z => (Union (Singleton a) (image g z))) ->
(forall x, In (Bfix s f) x -> exists appli_g:(In (partial_function s s) g /\ In (domain g) x), In (Bfix s f) (app g x appli_g)).
Proof.
intros S s a g f H H0 H1.
intros x H2.
rewrite (first_special_case _ _ _ _ _ H H0 H1).
assert (In (partial_function s s) g /\ In (domain g) x).
split.
induction H0; assumption.
assert (Included (Bfix s f) s).
apply fix_included_in_its_base_set.
apply first_special_case_is_total with a g; assumption.
induction H0.
rewrite H4; apply H3; assumption.
exists H3.
right.
red; apply image_intro with x.
split; [ assumption | apply app_trivial_property ].
Qed.

(** Note/TODO: move this theorem somewhere in Bfunctions ? *)
Theorem total_applicability: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (f: Ensemble (S*T)) (x: S),
  In s x -> In (total_function s t) f -> In (partial_function s t) f /\ In (domain f) x.
Proof.
intros S T s t f x H H0.
split.
induction H0; assumption.
induction H0.
rewrite H1; assumption.
Qed.

(**
  Theorem 3.2.8, BBook: section 3.2.3, p.138

  I would have avoided the existential, but it makes too many terms dependent
  *)
Theorem first_special_case_theorem: forall (S: Type) (s: Ensemble S) (a: S) (g: Ensemble (S*S)) (P: S -> Prop) (f: Ensemble (Ensemble S * Ensemble S)),
  In s a -> In (total_function s s) g ->
  f = lambda (Power_set s) (fun z => (Union (Singleton a) (image g z))) ->
  (P a) ->
  (forall (x:S), (In (Bfix s f) x /\ (P x)) -> exists appli_g:(In (partial_function s s) g /\ In (domain g) x), (P (app g x appli_g))) ->
  (forall (x:S), (In (Bfix s f) x) -> (P x)).
Proof.
intros S s a g P f a_in_s total_g f_def base_case induction_hyp.
generalize (first_special_case_is_total S s a g f a_in_s total_g f_def); intros H.
intros.
apply induction_principle with S s f; [ assumption | | assumption ].
generalize (fix_applicability S s f H); intros.
assert (In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) (Comprehension (fun x => (In (Bfix s f) x /\ (P x))))).
split; [ intuition | ].
induction H1.
inversion H.
rewrite H4.
constructor.
intros z H6.
inversion H6.
generalize (fix_included_in_its_base_set S s f H z); intuition.
exists H2.
(* SC:  Here you can find the proof of the BBook *)
replace (app f (Comprehension (fun x0 => (In (Bfix s f) x0 /\ (P x0)))) H2) with (Union (Singleton a) (image g (Comprehension (fun x0 => (In (Bfix s f) x0 /\ (P x0)))))).
assert (In (Comprehension (fun x0 => (In (Bfix s f) x0 /\ (P x0)))) a /\ Included (image g (Comprehension (fun x0 => (In (Bfix s f) x0 /\ (P x0))))) (Comprehension (fun x0 => (In (Bfix s f) x0 /\ (P x0))))).
split.
constructor.
apply first_special_case_prop1 with g; assumption.
assumption.
(* At this step of the proof, we try to transform image application into functional application *)
intros x0 H3.
inversion H3.
induction H4.
generalize (induction_hyp x1 H4).
intros.
inversion H7.
constructor.
generalize (first_special_case_prop2 S s a g f a_in_s total_g f_def).
intros.
inversion H4.
generalize (H9 x1 H10).
intros.
inversion H12.
assert (x0 = (app g x1 x3)).
induction total_g.
induction H14.
apply H16 with x1; [ assumption | apply app_trivial_property ].
rewrite H14; assumption.
assert (x0 = (app g x1 x2)).
induction total_g.
induction H9.
apply H11 with x1; [ assumption | apply app_trivial_property ].
rewrite H9; assumption.
induction H3.
intros z H5.
induction H5.
induction H5; assumption.
apply H4; assumption.
apply identifying_app.
assert (In (lambda (Power_set s) (fun z => (Union (Singleton a) (image g z)))) ((Comprehension (fun x => In (Bfix s f) x /\ P x)), (Union (Singleton a) (image g (Comprehension (fun x => In (Bfix s f) x /\ P x)))))).
constructor; [ | reflexivity ].
constructor.
intros z H3.
induction H3.
generalize (fix_included_in_its_base_set S s f H).
intros H5; apply H5; assumption.
rewrite f_def in H3 |- *; assumption.
Qed.

(**
  ** Second special case for having a simpler induction principle
  *)

Theorem second_special_case_is_total: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (a: S) (g: Ensemble ((T*S)*S)) (f: Ensemble (Ensemble S * Ensemble S)),
In s a -> In (total_function (times t s) s) g ->
f = lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z)))) ->
In (total_function (Power_set s) (Power_set s)) f.
Proof.
intros S T s t a g f H H0 H1.
rewrite H1.
apply lambda_is_total.
intros x H2.
constructor.
red; intros z H3.
induction H3.
induction H3; assumption.
induction H3.
induction H3.
(* TODO: need a tactic for this... *)
repeat (induction H0).
generalize (H0 (x0,y) H4); intros.
inversion H7; assumption.
Qed.

(**
  Description of the second special case, BBook: section 3.2.3, p.139
  *)
Theorem second_special_case: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (a: S) (g: Ensemble ((T*S)*S)) (f: Ensemble (Ensemble S * Ensemble S)),
In s a -> In (total_function (times t s) s) g ->
f = lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z)))) ->
Bfix s f = Union (Singleton a) (image g (times t (Bfix s f))).
Proof.
intros S T s t a g f H H0 H1.
generalize (second_special_case_is_total S T s t a g f H H0 H1); intros.
assert (forall x y, In (times (Power_set s) (Power_set s)) (x,y) /\ Included x y -> (exists appli_x:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) x), exists appli_y:(In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) y), Included (app f x appli_x) (app f y appli_y))).
intros.
assert (In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) x).
induction H2.
induction H3; inversion H3.
split; [ assumption | rewrite H4; assumption ].
assert (In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) y).
induction H2.
induction H3; inversion H3.
split; [ assumption | rewrite H5; assumption ].
rewrite H1.
assert (In (partial_function (Power_set s) (Power_set s)) (lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z))))) /\ In (domain (lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z)))))) x).
rewrite <- H1; assumption.
assert (In (partial_function (Power_set s) (Power_set s)) (lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z))))) /\ In (domain (lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z)))))) y).
rewrite <- H1; assumption.
exists H6; exists H7.
assert ( app (lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z))))) x H6 = (Union (Singleton a) (image g (times t x)))).
symmetry; apply identifying_app.
constructor.
induction H3; inversion H3; assumption.
reflexivity.
assert ( app (lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z))))) y H7 = (Union (Singleton a) (image g (times t y)))).
symmetry; apply identifying_app.
constructor.
induction H3; inversion H3; assumption.
reflexivity.
rewrite H8; rewrite H9.
intros z H10.
induction H10.
left; induction H10; intuition.
right.
inversion H10.
red; apply image_intro with x1.
induction H3.
split; [ induction H11; inversion H11; constructor ; [ assumption | apply H13 ] | ]; intuition.
assert ((Union (Singleton a) (image g (times t (Bfix s f)))) = app f (Bfix s f) (fix_applicability S s f H2)).
apply identifying_app.
assert (In (lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z))))) (Bfix s f, Union (Singleton a) (image g (times t (Bfix s f))))).
constructor.
constructor; apply fix_included_in_its_base_set; assumption.
reflexivity.
rewrite H1 in H4 |- *; assumption.
rewrite H4.
generalize (fix_Knaster_Tarski _ _ _ H2 H3).
intros.
symmetry; assumption.
Qed.

(**
  Property 3.2.3, BBook: section 3.2.3, p.139
  *)
Theorem second_special_case_prop1: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (a: S) (g: Ensemble ((T*S)*S)) (f: Ensemble (Ensemble S * Ensemble S)),
In s a -> In (total_function (times t s) s) g ->
f = lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z)))) ->
In (Bfix s f) a.
Proof.
intros S T s t a g f H H0 H1.
rewrite (second_special_case _ _ _ _ _ _ _ H H0 H1).
left; intuition.
Qed.

(**
  Property 3.2.4, BBook: section 3.2.3, p.139
  *)
Theorem second_special_case_prop2: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (a: S) (g: Ensemble ((T*S)*S)) (f: Ensemble (Ensemble S * Ensemble S)),
In s a -> In (total_function (times t s) s) g ->
f = lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z)))) ->
(forall u x, In (times t (Bfix s f)) (u,x) -> exists appli_g:(In (partial_function (times t s ) s) g /\ In (domain g) (u,x)), In (Bfix s f) (app g (u,x) appli_g)).
Proof.
intros S T s t a g f H H0 H1.
intros u x H2.
rewrite (second_special_case _ _ _ _ _ _ _ H H0 H1).
assert (In (partial_function (times t s) s) g /\ In (domain g) (u,x)).
split.
induction H0; assumption.
assert (Included (Bfix s f) s).
apply fix_included_in_its_base_set.
apply second_special_case_is_total with T t a g; assumption.
induction H0.
rewrite H4.
inversion H2; constructor; [ | apply H3 ]; assumption.
exists H3.
right.
red; apply image_intro with (u,x).
split; [ assumption | apply app_trivial_property ].
Qed.

(**
  Theorem 3.2.9, BBook: section 3.2.3, p.140

  I would have avoided the existential, but it makes too many terms dependent
  *)
Theorem second_special_case_theorem: forall (S T: Type) (s: Ensemble S) (t: Ensemble T) (a: S) (g: Ensemble ((T*S)*S)) (P: S -> Prop) (f: Ensemble (Ensemble S * Ensemble S)),
  In s a -> In (total_function (times t s) s) g ->
  f = lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z)))) ->
  (P a) ->
  (forall (x:S), (In (Bfix s f) x /\ (P x)) -> (forall (u:T), In t u -> exists appli_g:(In (partial_function (times t s) s) g /\ In (domain g) (u,x)), (P (app g (u,x) appli_g)))) ->
  (forall (x:S), (In (Bfix s f) x) -> (P x)).
Proof.
intros S T s t a g P f a_in_s total_g f_def base_case induction_hyp.
generalize (second_special_case_is_total S T s t a g f a_in_s total_g f_def); intros H.
intros.
apply induction_principle with S s f; [ assumption | | assumption ].
generalize (fix_applicability S s f H); intros.
assert (In (partial_function (Power_set s) (Power_set s)) f /\ In (domain f) (Comprehension (fun x => (In (Bfix s f) x /\ (P x))))).
split; [ intuition | ].
induction H1.
inversion H.
rewrite H4.
constructor.
intros z H6.
inversion H6.
generalize (fix_included_in_its_base_set S s f H z); intuition.
exists H2.
(* SC:  Here you can find the proof of the BBook *)
replace (app f (Comprehension (fun x0 => (In (Bfix s f) x0 /\ (P x0)))) H2) with (Union (Singleton a) (image g (times t (Comprehension (fun x0 => (In (Bfix s f) x0 /\ (P x0))))))).
assert (In (Comprehension (fun x0 => (In (Bfix s f) x0 /\ (P x0)))) a /\ Included (image g (times t (Comprehension (fun x0 => (In (Bfix s f) x0 /\ (P x0)))))) (Comprehension (fun x0 => (In (Bfix s f) x0 /\ (P x0))))).
split.
constructor.
apply second_special_case_prop1 with T t g; assumption.
assumption.
(* At this step of the proof, we try to transform image application into functional application *)
intros x0 H3.
inversion H3.
induction H4.
inversion x1; inversion H4.
generalize (induction_hyp b H8); intros.
generalize (H10 a0 H7); intros.
inversion H11.
constructor.
generalize (second_special_case_prop2 S T s t a g f a_in_s total_g f_def).
intros.
induction H8.
assert (In (times t (Bfix s f)) (a0, b)); [ constructor; assumption | ].
generalize (H13 a0 b H15).
intros.
inversion H16.
assert (x0 = (app g (a0,b) x3)).
induction total_g.
induction H18.
apply H20 with x1; [ assumption | rewrite <- H9; apply app_trivial_property ].
rewrite H18; assumption.
assert (x0 = (app g (a0,b) x2)).
induction total_g.
induction H13.
apply H15 with x1; [ assumption | rewrite <- H9; apply app_trivial_property ].
rewrite H13; assumption.
induction H3.
intros z H5.
induction H5.
induction H5; assumption.
apply H4; assumption.
apply identifying_app.
assert (In (lambda (Power_set s) (fun z => (Union (Singleton a) (image g (times t z))))) ((Comprehension (fun x => In (Bfix s f) x /\ P x)), (Union (Singleton a) (image g (times t (Comprehension (fun x => In (Bfix s f) x /\ P x))))))).
constructor; [ | reflexivity ].
constructor.
intros z H3.
induction H3.
generalize (fix_included_in_its_base_set S s f H).
intros H5; apply H5; assumption.
rewrite f_def in H3 |- *; assumption.
Qed.
