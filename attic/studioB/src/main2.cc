/****************************************************
 *			Test de connexion avec AtelierB
 *
 *****************************************************/
 
//
// Main2.cc


#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <gtkmm.h>

typedef std::vector<Glib::ustring> SVector;

SVector split(Glib::ustring sInit) {
	Glib::ustring s(sInit) ;
	SVector result;
	int b = 0;
	while (b != -1) {
		b = s.find_first_of("\n",0);
		if (b != -1) {
			result.push_back(s.substr(0,b));
			s.erase(0,b+1);
		} else {
			result.push_back(s.substr(0,s.size()));
		}
	}
	return result;
}

int main (int argc, char *argv[])
{
	printf("Test\n");
	Glib::ustring u1("toto \n tata \n titi");
	SVector tab = split(u1);
	printf("tab.size = %i\n",tab.size());
	for (int i = 0; i< tab.size(); i++) {
		printf("<%s>\n",tab[i].c_str());
	}
	printf("Fin test\n");
  return 0;
}

