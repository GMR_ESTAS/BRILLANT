//
// voir pageEditeurB.h
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "pageEditeurB.h"
#include <iostream>
#include <fstream>
#include <string.h>

PageEditeurB::PageEditeurB()
	: VBox(false,0)
{
	// Initialisation
	set_spacing(0);
	set_homogeneous(false);

	pack_start(m_ScrWindow, Gtk::PACK_EXPAND_WIDGET);
	m_ScrWindow.add(m_TextEdit);
	m_bufferBEditor = Gtk::TextBuffer::create();
	m_TextEdit.set_buffer(m_bufferBEditor);
	m_bufferBEditor->set_text("");

	// Mise en place de la barre de statut
	pack_start(m_Statusbar, Gtk::PACK_SHRINK);

	// Initialisation des procedures de sivies des modifications de texte
	m_bufferBEditor->signal_changed().connect(sigc::mem_fun(*this, &PageEditeurB::text_changed));
 	m_bufferBEditor->signal_mark_set().connect(sigc::mem_fun(*this, &PageEditeurB::text_mark_set));
 	text_changed();
	
	_modifications = 0;
}

PageEditeurB::~PageEditeurB() {
}

void PageEditeurB::rafraichirBar() {
	m_Statusbar.pop();

	gint count = m_bufferBEditor->get_char_count();

	Gtk::TextIter iter = m_bufferBEditor->get_iter_at_mark(m_bufferBEditor->get_insert());

	gint row = iter.get_line() + 1;
	gint col = iter.get_line_offset() + 1;

	gchar* msg = g_strdup_printf ("Curseur à la ligne %d et colonne %d",
                         row, col);
	m_Statusbar.push(msg);
	g_free (msg);
}

void PageEditeurB::text_changed() {
	rafraichirBar();
	// on pose un flag sur le texte indiquant qu'il a ete modife
	_modifications = 1;
}

void PageEditeurB::text_mark_set(const Gtk::TextIter&, const Glib::RefPtr<Gtk::TextBuffer::Mark>&) {
	rafraichirBar();
}

void PageEditeurB::setText(const char* contenu) {
	// sauvegarde de la variable modification
	int rem = _modifications;
	m_bufferBEditor->set_text(contenu);
	_modifications = rem;
}

Glib::ustring PageEditeurB::getText() {
	return m_bufferBEditor->get_text();
}

std::string PageEditeurB::getFilename() const {
	return _filename;
}

void PageEditeurB::setFilename(const std::string& filename) {
	_filename = filename;
}

int PageEditeurB::getPageSauvegardee() {
	/** retourne 1 si le fichier affiche sur la page est sauvegarde
	et 0 sinon **/
	int retour;
	if (_filename.empty() || (_modifications != 0)) {
		retour = 0;
	}	else {
		retour = 1;
	}
	return retour;
}

void PageEditeurB::setPageSauvegardee(int state) {
	_modifications = state;
}

void PageEditeurB::insererSymbole(const char * symbole) {
	const Glib::ustring s(symbole);
	m_bufferBEditor->insert_at_cursor(s);
}

void PageEditeurB::paste_clipboard(Glib::RefPtr<Gtk::Clipboard> clipboard) {
	m_bufferBEditor->paste_clipboard(clipboard);
}

void PageEditeurB::cut_clipboard(Glib::RefPtr<Gtk::Clipboard> clipboard) {
	m_bufferBEditor->cut_clipboard(clipboard);
}

void PageEditeurB::copy_clipboard(Glib::RefPtr<Gtk::Clipboard> clipboard) {
	m_bufferBEditor->copy_clipboard(clipboard);
}
