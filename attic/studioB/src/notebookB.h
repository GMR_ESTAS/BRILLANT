//
// NotebookB.h
//	environnement graphique
//

#ifndef _NOTEBOOK_H
#define _NOTEBOOK_H

#include <gtkmm.h>

class NotebookB : public Gtk::Notebook
{
public:
	// Constructeur
	NotebookB();
	// Destructeur
	virtual ~NotebookB();

	// Fonction : attribue le texte a l'editeur courant
	void setText(const char* contenu);
	// Fonction : retourne le texte de l'editeur courant
	Glib::ustring getText();
	// Fonction : ajoute un fichier a l'editeur
	void ajouterFeuillet();
	// Fonction : renvoie le numero de page courante
	int getCurrentTab();
	// Fonction : attribue un nom de fichier au ficheri courant
	void setFilename(const std::string& filename);
	// Fonction : retourne le nonm du fichier courant
	std::string getFilename();
	// Fonction : renvoie le nombre de page ouverte dans le notebook
	int getNbPages();
	// Fonction : renoive 1 si la page courante est sauvegarde
	int getPageSauvegardee();
	// Fonction : attribue un etat de sauvegarde a la page courante
	void setPageSauvegardee(int state);
	// Fonction : retire du notebook la page courante
	void fermerFeuillet() ;
	// Fonction : insere un symbole dans la page courante
	void insererSymbole(const char * symbole);
	// Fonction : actions sur la selection de texte
	void paste_clipboard(Glib::RefPtr<Gtk::Clipboard>);
	void cut_clipboard(Glib::RefPtr<Gtk::Clipboard>);
	void copy_clipboard(Glib::RefPtr<Gtk::Clipboard>);

private :
	// Fonction : rafraichie la description contenue dans la barre de statut
	void rafraichirBar();
	// Fonction : fonctions d'ecoute sur le changement dans les pages du notebook
	void on_text_changed();
	void on_text_mark_set();
};

#endif //_NOTEBOOK_H

