//
// voir Fenetre.h
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <iostream>
#include <fstream>
#include <string>

#include <gtkmm/stock.h>

#include "fenetre.h"

Fenetre::Fenetre(const std::string& icon) :
	m_HBox(false,0),
	m_Layout(2,4),
	m_toolBox(false,5),
	m_Button_Copy(Gtk::Stock::COPY),
	m_Button_Cut(Gtk::Stock::CUT),
	m_Button_Paste(Gtk::Stock::PASTE),
	m_tool_new(Gtk::Stock::NEW),
	m_tool_open(Gtk::Stock::OPEN),
	m_tool_save(Gtk::Stock::SAVE),
	m_tool_copy(Gtk::Stock::COPY),
	m_tool_cut(Gtk::Stock::CUT),
	m_tool_paste(Gtk::Stock::PASTE),
	_currentProject(""),
	dir_icon(icon)
{
	set_title("Atelier B ;    ( Pas de projet ouvert )");
	//m_iconFenetre = Gdk::Pixbuf::create_from_file(dir_icon+"/logob.bm");
	//m_iconFenetreAux = Gdk::Pixbuf::create_from_file(dir_icon+"/iconeb.bm");
	set_icon(m_iconFenetre);
	set_default_size(800, 710);
	m_Layout.set_homogeneous(false);
	add(m_Window);
	m_Window.add(m_Layout);
	m_Window.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
	m_Window.set_shadow_type(Gtk::SHADOW_IN);
	
	/** Barre de menu **/
	m_Layout.attach(m_MenuBar, 0, 2, 0, 1, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(0));
	{
		// Menu Fichier
		Gtk::Menu* menuFichier = Gtk::manage(new Gtk::Menu());
		Gtk::Menu::MenuList& list_fichier = menuFichier->items();
		list_fichier.push_back( Gtk::Menu_Helpers::MenuElem("_Nouveau", sigc::mem_fun(*this, &Fenetre::on_menu_fichier_nouveau)) );
		list_fichier.push_back( Gtk::Menu_Helpers::MenuElem("_Ouvrir", Gtk::AccelKey("<control>o"), sigc::mem_fun(*this, &Fenetre::on_menu_fichier_ouvrir)) );
		list_fichier.push_back( Gtk::Menu_Helpers::MenuElem("_Enregistrer", Gtk::AccelKey("<control>s"), sigc::mem_fun(*this, &Fenetre::on_menu_fichier_enregistrer)) );
		list_fichier.push_back( Gtk::Menu_Helpers::MenuElem("En_registrer sous...", sigc::mem_fun(*this, &Fenetre::on_menu_fichier_enregistrer_sous)) );
		list_fichier.push_back(Gtk::Menu_Helpers::SeparatorElem());
		list_fichier.push_back( Gtk::Menu_Helpers::MenuElem("_Quitter", Gtk::AccelKey("<control>q"), sigc::mem_fun(*this, &Fenetre::on_menu_fichier_quitter)) );

		// Menu Fichier
		Gtk::Menu* menuEdition = Gtk::manage(new Gtk::Menu());
		Gtk::Menu::MenuList& list_edition = menuEdition->items();
		list_edition.push_back( Gtk::Menu_Helpers::MenuElem("_Copie (Ctrl + c)", sigc::mem_fun(*this, &Fenetre::on_toolbar_copy)) );
		list_edition.push_back( Gtk::Menu_Helpers::MenuElem("_Coupe (Ctrl + x)", sigc::mem_fun(*this, &Fenetre::on_toolbar_cut)) );
		list_edition.push_back( Gtk::Menu_Helpers::MenuElem("_Colle (Ctrl + v)", sigc::mem_fun(*this, &Fenetre::on_toolbar_paste)) );
		list_edition.push_back(Gtk::Menu_Helpers::SeparatorElem());
		list_edition.push_back( Gtk::Menu_Helpers::MenuElem("_Effacer la console", sigc::mem_fun(*this, &Fenetre::effacerConsole)) );

		// Projet
		Gtk::Menu* menuProjet = Gtk::manage(new Gtk::Menu());
 		Gtk::Menu::MenuList& list_projet = menuProjet->items();
		list_projet.push_back( Gtk::Menu_Helpers::MenuElem("_Nouveau", sigc::mem_fun(*this, &Fenetre::on_menu_projet_nouveau)) );
		list_projet.push_back( Gtk::Menu_Helpers::MenuElem("_Supprimer", sigc::mem_fun(*this, &Fenetre::on_menu_projet_supprimer)) );
		list_projet.push_back( Gtk::Menu_Helpers::MenuElem("_Informations", sigc::mem_fun(*this, &Fenetre::on_menu_projet_informations)) );
		list_projet.push_back(Gtk::Menu_Helpers::SeparatorElem());
		menuProjet_projet = Gtk::manage(new Gtk::Menu());
		list_projet_projet = menuProjet_projet->items();
		list_projet.push_back(Gtk::Menu_Helpers::MenuElem("_Projet", *menuProjet_projet));
		list_projet.push_back(Gtk::Menu_Helpers::SeparatorElem());
		list_projet.push_back( Gtk::Menu_Helpers::MenuElem("_Fermer", sigc::mem_fun(*this, &Fenetre::on_menu_projet_fermer)) );

		// Librairie
		Gtk::Menu* menuLibrairie = Gtk::manage(new Gtk::Menu());
 		Gtk::Menu::MenuList& list_librairie = menuLibrairie->items();
		list_librairie.push_back( Gtk::Menu_Helpers::MenuElem("_Ajouter", sigc::mem_fun(*this, &Fenetre::on_menu_librairie_ajouter)) );
		list_librairie.push_back( Gtk::Menu_Helpers::MenuElem("_Supprimer", sigc::mem_fun(*this, &Fenetre::on_menu_librairie_supprimer)) );

		// Langage B
		Gtk::Menu* menuLangageB = Gtk::manage(new Gtk::Menu());
 		Gtk::Menu::MenuList& list_langageB = menuLangageB->items();
		list_langageB.push_back( Gtk::Menu_Helpers::MenuElem("_Nouveau", Gtk::AccelKey("<control>n"), sigc::mem_fun(*this, &Fenetre::on_menu_langageB_nouveau)) );
		list_langageB.push_back( Gtk::Menu_Helpers::MenuElem("_Supprimer", sigc::mem_fun(*this, &Fenetre::on_menu_langageB_supprimer)) );
		list_langageB.push_back( Gtk::Menu_Helpers::MenuElem("_Editer", Gtk::AccelKey("<control>e"), sigc::mem_fun(*this, &Fenetre::on_menu_langageB_editer)) );
		list_langageB.push_back( Gtk::Menu_Helpers::MenuElem("_Informations", sigc::mem_fun(*this, &Fenetre::on_menu_langageB_informations)) );
		list_langageB.push_back( Gtk::Menu_Helpers::MenuElem("_Statut", sigc::mem_fun(*this, &Fenetre::on_menu_langageB_statut)) );
		Gtk::Menu* menuLangageB_dependance = Gtk::manage(new Gtk::Menu());
		Gtk::Menu::MenuList& list_langageB_dependance = menuLangageB_dependance->items();
		list_langageB_dependance.push_back( Gtk::Menu_Helpers::RadioMenuElem(m_group, "Active", sigc::mem_fun(*this, &Fenetre::on_menu_langageB_activerDependance)) );
		list_langageB_dependance.push_back( Gtk::Menu_Helpers::RadioMenuElem(m_group, "Desactive", sigc::mem_fun(*this, &Fenetre::on_menu_langageB_desactiverDependance)) );
		list_langageB.push_back(Gtk::Menu_Helpers::MenuElem("_Dependance", *menuLangageB_dependance));
		_dependancesActivees = true;

		// Methode B
		Gtk::Menu* menuMethodeB = Gtk::manage(new Gtk::Menu());
 		Gtk::Menu::MenuList& list_methodeB = menuMethodeB->items();
		list_methodeB.push_back( Gtk::Menu_Helpers::MenuElem("_Type Check", Gtk::AccelKey("<control>t"), sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_typeCheck)) );
		Gtk::Menu* menuMethodeB_POGenerate = Gtk::manage(new Gtk::Menu());
		Gtk::Menu::MenuList& list_methodeB_POGenerate = menuMethodeB_POGenerate->items();
		list_methodeB_POGenerate.push_back( Gtk::Menu_Helpers::MenuElem("_Full", sigc::bind (sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_POGenerate), true)) );
		list_methodeB_POGenerate.push_back( Gtk::Menu_Helpers::MenuElem("_Differential", sigc::bind<bool> (sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_POGenerate), false)) );
		list_methodeB.push_back(Gtk::Menu_Helpers::MenuElem("_PO Generate", *menuMethodeB_POGenerate));
		Gtk::Menu* menuMethodeB_preuve = Gtk::manage(new Gtk::Menu());
		Gtk::Menu::MenuList& list_methodeB_preuve = menuMethodeB_preuve->items();
		list_methodeB_preuve.push_back( Gtk::Menu_Helpers::MenuElem("_Force 0", Gtk::AccelKey("F1"), sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_preuve_force0)) );
		list_methodeB_preuve.push_back( Gtk::Menu_Helpers::MenuElem("_Force 1", Gtk::AccelKey("F2"), sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_preuve_force1)) );
		list_methodeB_preuve.push_back( Gtk::Menu_Helpers::MenuElem("_Force 2", Gtk::AccelKey("F3"), sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_preuve_force2)) );
		list_methodeB_preuve.push_back( Gtk::Menu_Helpers::MenuElem("_Force 3", Gtk::AccelKey("F4"), sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_preuve_force3)) );
		list_methodeB.push_back(Gtk::Menu_Helpers::MenuElem("_Preuve", *menuMethodeB_preuve));
		list_methodeB.push_back( Gtk::Menu_Helpers::MenuElem("_BO Check", Gtk::AccelKey("<control>b"), sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_boCheck)) );

		// Utilisateur
		Gtk::Menu* menuUtilisateur = Gtk::manage(new Gtk::Menu());
 		Gtk::Menu::MenuList& list_utilisateur = menuUtilisateur->items();
		list_utilisateur.push_back( Gtk::Menu_Helpers::MenuElem("_Ajouter", sigc::mem_fun(*this, &Fenetre::on_menu_utilisateur_ajouter)) );
		Gtk::Menu* menuUtilisateur_suppression = Gtk::manage(new Gtk::Menu());
		Gtk::Menu::MenuList& list_utilisateur_suppression = menuUtilisateur_suppression->items();
		list_utilisateur_suppression.push_back( Gtk::Menu_Helpers::MenuElem("_Utilisateur", sigc::bind<bool> (sigc::mem_fun(*this, &Fenetre::on_menu_utilisateur_supprimer), true) ) );
		list_utilisateur_suppression.push_back( Gtk::Menu_Helpers::MenuElem("_Lecteur", sigc::bind<bool> (sigc::mem_fun(*this, &Fenetre::on_menu_utilisateur_supprimer), false) ) );
		list_utilisateur.push_back(Gtk::Menu_Helpers::MenuElem("_Supprimer", *menuUtilisateur_suppression));
		list_utilisateur.push_back( Gtk::Menu_Helpers::MenuElem("_Montrer liste", sigc::mem_fun(*this, &Fenetre::on_menu_utilisateur_montrerListe)) );

		// Menu Aide
		Gtk::Menu* menuAide = Gtk::manage(new Gtk::Menu());
 		Gtk::Menu::MenuList& list_aide = menuAide->items();
		list_aide.push_back( Gtk::Menu_Helpers::MenuElem("_A Propos", Gtk::AccelKey("<control>p"), sigc::mem_fun(*this, &Fenetre::on_menu_aide_a_propos)) );

		//Creation de la barre de menu
		Gtk::Menu::MenuList& barre_de_menu = m_MenuBar.items();
		barre_de_menu.push_back(Gtk::Menu_Helpers::MenuElem("_Fichier", *menuFichier));
		barre_de_menu.push_back(Gtk::Menu_Helpers::MenuElem("_Edition", *menuEdition));
		barre_de_menu.push_back(Gtk::Menu_Helpers::MenuElem("_Projet", *menuProjet));
		barre_de_menu.push_back(Gtk::Menu_Helpers::MenuElem("_Librairie", *menuLibrairie	));
		barre_de_menu.push_back(Gtk::Menu_Helpers::MenuElem("_Composant", *menuLangageB));
		barre_de_menu.push_back(Gtk::Menu_Helpers::MenuElem("_Methode B", *menuMethodeB));
		barre_de_menu.push_back(Gtk::Menu_Helpers::MenuElem("_Utilisateur", *menuUtilisateur));
		barre_de_menu.push_back(Gtk::Menu_Helpers::MenuElem("_Aide", *menuAide));
	}

	/** Barre d'outils **/
	m_Layout.attach(m_toolBox, 0, 2, 1, 2, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(0));
	m_toolBox.pack_start(m_Toolbar, Gtk::PACK_SHRINK);
	{
		m_Toolbar.append( m_tool_new, sigc::mem_fun(*this, &Fenetre::on_toolbar_nouveau) );
		m_Toolbar.append( m_tool_open, sigc::mem_fun(*this, &Fenetre::on_toolbar_ouvrir) );
		m_Toolbar.append( m_tool_save, sigc::mem_fun(*this, &Fenetre::on_toolbar_enregistrer) );
		m_Toolbar.append( m_tool_separator );
		m_Toolbar.append( m_tool_copy, sigc::mem_fun(*this, &Fenetre::on_toolbar_copy) );
		m_Toolbar.append( m_tool_cut, sigc::mem_fun(*this, &Fenetre::on_toolbar_cut) );
		m_Toolbar.append( m_tool_paste, sigc::mem_fun(*this, &Fenetre::on_toolbar_paste) );
	}

	m_commandBLabel.set_text("Commande B:");
	m_toolBox.pack_start(m_commandBLabel, Gtk::PACK_SHRINK);
	m_CommandB.set_text("");
	m_CommandB.signal_activate().connect( sigc::mem_fun(*this, &Fenetre::on_entry_commandB));
	m_toolBox.pack_start(m_CommandB, Gtk::PACK_EXPAND_WIDGET);
	m_button_execute.add_pixlabel(dir_icon+"/icons_ed/runcmd.gif","");
	m_button_execute.set_size_request(22,22);
	m_button_execute.signal_clicked().connect( sigc::mem_fun(*this, &Fenetre::on_entry_commandB));
	m_toolBox.pack_start(m_button_execute, Gtk::PACK_SHRINK);

	/** Arbre de projet **/
	m_Layout.attach(m_ScrTree, 0, 1, 2, 3, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
	m_ScrTree.set_size_request(150,420);
	m_ScrTree.add(m_EventBox);
	m_EventBox.signal_button_release_event().connect(sigc::mem_fun(*this, &Fenetre::on_projectTree_click) );
	m_EventBox.add(m_TreeView);

	//Create the Tree model:
	m_refTreeModel = Gtk::TreeStore::create(m_Columns);
	m_TreeView.set_model(m_refTreeModel);
	m_TreeView.append_column("Projet", m_Columns.m_col_name);
	// Gtk::EventBox mref<>  = mtree.getSelection()

	//Fill the TreeView's model
	_implementationBranche = *(m_refTreeModel->append());
	_implementationBranche[m_Columns.m_col_name] = "Implementation";
	_librairieBranche = *(m_refTreeModel->append());
	_librairieBranche[m_Columns.m_col_name] = "Librairie";
	_machineBranche = *(m_refTreeModel->append());
	_machineBranche[m_Columns.m_col_name] = "Machine";
	_raffinementBranche = *(m_refTreeModel->append());
	_raffinementBranche[m_Columns.m_col_name] = "Raffinement";

	/** Editeur de fichiers **/
	m_EditeurB = Gtk::manage(new EditeurB(this,dir_icon));
	m_Layout.attach(*m_EditeurB, 1, 2, 2, 3, Gtk::FILL | Gtk::EXPAND, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(0));

	/** Afficheur de messages **/
	m_Layout.attach(m_scrConsole, 0, 2, 3, 4,  Gtk::FILL , Gtk::FILL , Gtk::AttachOptions(2));
	m_scrConsole.add(m_Console);
	m_scrConsole.set_size_request(650,160);
	m_bufferConsole = Gtk::TextBuffer::create();
	m_Console.set_buffer(m_bufferConsole);
	m_Console.set_size_request(600,150);
	m_Console.set_editable(false);
	creerTags(m_bufferConsole);

	// initialisation du B
	_b = B();
	char *junk = _b.lance();
	delete [] junk;
	
	// creation de la liste des projets
	creerListeProjets();

	// initialisation de l'arbre
	refraichirArbreProjet();
	
	// Construction des menus du pop-up
	Gtk::Menu::MenuList& m_Menu_list_PopupComposant = m_Menu_PopupComposant.items();
	m_Menu_list_PopupComposant.push_back( Gtk::Menu_Helpers::MenuElem("Type Check", sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_typeCheck)) );
	m_Menu_list_PopupComposant.push_back(Gtk::Menu_Helpers::MenuElem("PO Generate (Full)", sigc::bind<bool> (sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_POGenerate), true)) );
	m_Menu_list_PopupComposant.push_back(Gtk::Menu_Helpers::MenuElem("PO Generate (Differential)", sigc::bind<bool> (sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_POGenerate), false)) );
	m_Menu_list_PopupComposant.push_back( Gtk::Menu_Helpers::MenuElem("Preuve (Force 0)", sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_preuve_force0)));
	m_Menu_list_PopupComposant.push_back( Gtk::Menu_Helpers::MenuElem("BO Check",  sigc::mem_fun(*this, &Fenetre::on_menu_methodeB_boCheck)) );
	m_Menu_list_PopupComposant.push_back(Gtk::Menu_Helpers::SeparatorElem());
	m_Menu_list_PopupComposant.push_back( Gtk::Menu_Helpers::MenuElem("Editer", sigc::mem_fun(*this, &Fenetre::on_menu_langageB_editer)) );
	m_Menu_list_PopupComposant.push_back(Gtk::Menu_Helpers::SeparatorElem());
	m_Menu_list_PopupComposant.push_back( Gtk::Menu_Helpers::MenuElem("_Statut", sigc::mem_fun(*this, &Fenetre::on_menu_langageB_statut)) );
	m_Menu_PopupComposant.accelerate(*this);

	show_all();
}

Fenetre::~Fenetre()
{
	_b.q();
}

/************************************* Fonctions  *************************************/

void Fenetre::afficherMessage(char* contenu) {

	m_bufferConsole->set_text(contenu);

}

void Fenetre::afficherMessage(Glib::ustring contenu, int gravite) {
	/*
		Pour la gravite, level de 0 a 3 (du plus faible au plus grave)
	*/

	if (gravite < 0) {
		gravite = 0;
	} else if (gravite > 3) {
		gravite = 3;
	}
	Gtk::TextIter iter = m_bufferConsole->end();
	iter = m_bufferConsole->insert(iter, ">>");
	switch(gravite) {
		case 0:
		 	iter = m_bufferConsole->insert(iter, contenu);
		 	break;
		case 1:
			iter = m_bufferConsole->insert_with_tag(iter, " Information : \n", "yellow_background" );
			iter = m_bufferConsole->insert(iter, ">");
			iter = m_bufferConsole->insert(iter, contenu);
		 	break;
		case 2:
			iter = m_bufferConsole->insert_with_tag(iter, " Warning : \n", "orange_background" );
			iter = m_bufferConsole->insert(iter, ">");
		 	iter = m_bufferConsole->insert_with_tag(iter, contenu, "orange_foreground" );
		 	break;
		case 3:
			iter = m_bufferConsole->insert_with_tag(iter, " Error : \n", "red_background" );
			iter = m_bufferConsole->insert(iter, ">");
		 	iter = m_bufferConsole->insert_with_tag(iter, contenu, "red_foreground" );
		 	break;
	}
	iter = m_bufferConsole->insert(iter, "\n");
	m_scrConsole.get_vadjustment()->set_value(m_scrConsole.get_vadjustment()->get_upper ()  - 20);
	m_scrConsole.get_vadjustment()->value_changed ();
}

void Fenetre::afficherMessage(const char* contenu, int gravite) {
	/*
		Pour la gravite, level de 0 a 3 (du plus faible au plus grave)
	*/

	if (gravite < 0) {
		gravite = 0;
	} else if (gravite > 3) {
		gravite = 3;
	}
	Gtk::TextIter iter = m_bufferConsole->end();
	iter = m_bufferConsole->insert(iter, ">>");
	switch(gravite) {
		case 0:
		 	iter = m_bufferConsole->insert(iter, contenu);
		 	break;
		case 1:
			iter = m_bufferConsole->insert_with_tag(iter, " Information : \n", "yellow_background" );
			iter = m_bufferConsole->insert(iter, ">");
			iter = m_bufferConsole->insert(iter, contenu);
		 	break;
		case 2:
			iter = m_bufferConsole->insert_with_tag(iter, " Warning : \n", "orange_background" );
			iter = m_bufferConsole->insert(iter, ">");
		 	iter = m_bufferConsole->insert_with_tag(iter, contenu, "orange_foreground" );
		 	break;
		case 3:
			iter = m_bufferConsole->insert_with_tag(iter, " Error : \n", "red_background" );
			iter = m_bufferConsole->insert(iter, ">");
		 	iter = m_bufferConsole->insert_with_tag(iter, contenu, "red_foreground" );
		 	break;
	}
	iter = m_bufferConsole->insert(iter, "\n");
	m_scrConsole.get_vadjustment()->set_value(m_scrConsole.get_vadjustment()->get_upper ()  - 20);
	m_scrConsole.get_vadjustment()->value_changed ();
}

void Fenetre::afficherMessage(char* contenu, int gravite) {
	/*
		Pour la gravite, level de 0 a 3 (du plus faible au plus grave)
	*/

	if (gravite < 0) {
		gravite = 0;
	} else if (gravite > 3) {
		gravite = 3;
	}
	Gtk::TextIter iter = m_bufferConsole->end();
	iter = m_bufferConsole->insert(iter, ">>");
	switch(gravite) {
		case 0:
		 	iter = m_bufferConsole->insert(iter, contenu);
		 	break;
		case 1:
			iter = m_bufferConsole->insert_with_tag(iter, " Information : \n", "yellow_background" );
			iter = m_bufferConsole->insert(iter, ">");
			iter = m_bufferConsole->insert(iter, contenu);
		 	break;
		case 2:
			iter = m_bufferConsole->insert_with_tag(iter, " Warning : \n", "orange_background" );
			iter = m_bufferConsole->insert(iter, ">");
		 	iter = m_bufferConsole->insert_with_tag(iter, contenu, "orange_foreground" );
		 	break;
		case 3:
			iter = m_bufferConsole->insert_with_tag(iter, " Error : \n", "red_background" );
			iter = m_bufferConsole->insert(iter, ">");
		 	iter = m_bufferConsole->insert_with_tag(iter, contenu, "red_foreground" );
		 	break;
	}
	iter = m_bufferConsole->insert(iter, "\n");
	m_scrConsole.get_vadjustment()->set_value(m_scrConsole.get_vadjustment()->get_upper ()  - 20);
	m_scrConsole.get_vadjustment()->value_changed ();
}

void Fenetre::afficherMessage(SVector contenu, int gravite) {
	/*
		Pour la gravite, level de 0 a 3 (du plus faible au plus grave)
	*/
	if (gravite < 0) {
		gravite = 0;
	} else if (gravite > 3) {
		gravite = 3;
	}
	Gtk::TextIter iter = m_bufferConsole->end();
	iter = m_bufferConsole->insert(iter, ">>");
	switch(gravite) {
		case 0:
		 	for (int i=0; i<contenu.size(); i++) {
				iter = m_bufferConsole->insert(iter, contenu[i]);
				iter = m_bufferConsole->insert(iter, "\n");
			}
		 	break;
		case 1:
			iter = m_bufferConsole->insert_with_tag(iter, " Information : \n", "yellow_background" );
			iter = m_bufferConsole->insert(iter, ">");
			for (int i=0; i<contenu.size(); i++) {
				iter = m_bufferConsole->insert(iter, contenu[i]);
				iter = m_bufferConsole->insert(iter, "\n");
			}
		 	break;
		case 2:
			iter = m_bufferConsole->insert_with_tag(iter, " Warning : \n", "orange_background" );
			iter = m_bufferConsole->insert(iter, ">");
			for (int i=0; i<contenu.size(); i++) {
				iter = m_bufferConsole->insert_with_tag(iter, contenu[i], "orange_foreground" );
				iter = m_bufferConsole->insert(iter, "\n");
			}
		 	break;
		case 3:
			iter = m_bufferConsole->insert_with_tag(iter, " Error : \n", "red_background" );
			iter = m_bufferConsole->insert(iter, ">");
			for (int i=0; i<contenu.size(); i++) {
				iter = m_bufferConsole->insert_with_tag(iter, contenu[i], "red_foreground" );
				iter = m_bufferConsole->insert(iter, "\n");
			}
		 	break;
	}
	iter = m_bufferConsole->insert(iter, "\n");
	m_scrConsole.get_vadjustment()->set_value(m_scrConsole.get_vadjustment()->get_upper ()  - 20);
	m_scrConsole.get_vadjustment()->value_changed ();
}

void Fenetre::effacerConsole() {
	m_bufferConsole->set_text("");
}

void Fenetre::creerTags(Glib::RefPtr<Gtk::TextBuffer>& refBuffer)
{
	refBuffer->create_tag("blue_foreground")->property_foreground().set_value("blue");
	refBuffer->create_tag("red_foreground")->property_foreground().set_value("red");
	refBuffer->create_tag("orange_foreground")->property_foreground().set_value("orange");
	refBuffer->create_tag("yellow_foreground")->property_foreground().set_value("yellow");
	refBuffer->create_tag("black_foreground")->property_foreground().set_value("black");
	refBuffer->create_tag("blue_background")->property_background().set_value("blue");
	refBuffer->create_tag("red_background")->property_background().set_value("red");
	refBuffer->create_tag("orange_background")->property_background().set_value("orange");
	refBuffer->create_tag("yellow_background")->property_background().set_value("yellow");
}


void Fenetre::ouvertureFichier(const std::string& filename) {

	m_EditeurB->ajouterFeuillet();
	m_EditeurB->setFilename(filename);

	afficherMessage(append("ouverture du fichier :\n\t",m_EditeurB->getFilename()),1);
	FILE * fic = fopen(m_EditeurB->getFilename().c_str(),"r");
	if (fic == NULL) {
		afficherMessage("Probleme a l'ouverture du fichier",3);
	} else {
		fseek(fic,0L,SEEK_END);
		long fin = ftell(fic);
		char * contenu = new char[fin+1];
		fseek(fic,0L,SEEK_SET);
		fread(contenu,fin,1,fic);
		contenu[fin] = '\0';
		m_EditeurB->setText(contenu);
		fclose(fic);
		delete [] contenu;
	}
}


void Fenetre::ecrireFichier(const Glib::ustring& contenu, const Glib::ustring& fichier) {
	using namespace std;
	filebuf fb;
	fb.open(fichier.c_str(), ios_base::in | ios_base::out | ios_base::trunc);
	string l1 = contenu;
	fb.sputn(l1.data(), l1.size());
	fb.pubseekpos(0, ios_base::in | ios_base::out);
	fb.close();
}

/**
	Construction de la liste des projets dans le menu associe
*/
void Fenetre::creerListeProjets() {
	_vProjets = _b.showProjectsList();
	list_projet_projet.clear();
	for (int i = 0; i < _vProjets.size(); i++) {
		list_projet_projet.push_back( Gtk::Menu_Helpers::MenuElem(_vProjets[i], sigc::bind<Glib::ustring>(sigc::mem_fun(*this, &Fenetre::on_menu_projet_projet), _vProjets[i] )));
	}
	show_all();
}

void Fenetre::refraichirArbreProjet() {
	m_refTreeModel->clear();

	_machineBranche = *(m_refTreeModel->append());
	_machineBranche[m_Columns.m_col_name] = "Machine";
	_raffinementBranche = *(m_refTreeModel->append());
	_raffinementBranche[m_Columns.m_col_name] = "Raffinement";
	_implementationBranche = *(m_refTreeModel->append());
	_implementationBranche[m_Columns.m_col_name] = "Implementation";
	_librairieBranche = *(m_refTreeModel->append());
	_librairieBranche[m_Columns.m_col_name] = "Librairie";

	if (_currentProject != "") {
		_vComposantsProjet.clear();

		_vMachines = _b.showAllMachines(_currentProject);
		_vComposantsProjet.push_back(_vMachines);
		for (int i = 0; i < _vMachines.size(); i++) {
			Gtk::TreeModel::Row childrow = *(m_refTreeModel->append(_machineBranche.children()));
			childrow[m_Columns.m_col_name] = _vMachines[i];
		}

		_vRafinements = _b.showAllRafinements(_currentProject);
		_vComposantsProjet.push_back(_vRafinements);
		for (int i = 0; i < _vRafinements.size(); i++) {
			Gtk::TreeModel::Row childrow = *(m_refTreeModel->append(_raffinementBranche.children()));
			childrow[m_Columns.m_col_name] = _vRafinements[i];
		}

		_vImplementations = _b.showAllImplementations(_currentProject);
		_vComposantsProjet.push_back(_vImplementations);
		for (int i = 0; i < _vImplementations.size(); i++) {
			Gtk::TreeModel::Row childrow = *(m_refTreeModel->append(_implementationBranche.children()));
			childrow[m_Columns.m_col_name] = _vImplementations[i];
		}

		_vLibrairies = _b.showAllLibraries(_currentProject);
		_vComposantsProjet.push_back(_vLibrairies);
		for (int i = 0; i < _vLibrairies.size(); i++) {
			Gtk::TreeModel::Row childrow = *(m_refTreeModel->append(_librairieBranche.children()));
			childrow[m_Columns.m_col_name] = _vLibrairies[i];
		}
	} else {
		_vMachines.clear();
		_vRafinements.clear();
		_vImplementations.clear();
		_vLibrairies.clear();
	}
}

/******************************** Fonctions du menu ***********************************/


void Fenetre::on_menu_fichier_nouveau() {
	m_EditeurB->ajouterFeuillet();
	//m_EditeurB->setFilename(filename3);
}

void Fenetre::on_menu_fichier_ouvrir() {
	Gtk::FileSelection dialog("Selectionner un fichier");
	dialog.set_transient_for(*this);
	int result = dialog.run();

	if (result == Gtk::RESPONSE_OK) {
		ouvertureFichier(dialog.get_filename());
	}
}

void Fenetre::on_menu_fichier_enregistrer() {
	afficherMessage(append("Sauvegarde du fichier : \n\t",m_EditeurB->getFilename()),1);
	if (m_EditeurB->getFilename().empty()) {
		on_menu_fichier_enregistrer_sous();
	} else  {
		ecrireFichier(m_EditeurB->getText(), m_EditeurB->getFilename());
		m_EditeurB->setPageSauvegardee();
	}
}

void Fenetre::on_menu_fichier_enregistrer_sous() {
	Gtk::FileSelection dialog("Selectionner un fichier");
	dialog.set_transient_for(*this);
	int result = dialog.run();
	if (result == Gtk::RESPONSE_OK) {
		m_EditeurB->setFilename(dialog.get_filename());
		m_EditeurB->getFilename();
		ecrireFichier(m_EditeurB->getText(),m_EditeurB->getFilename());
		m_EditeurB->setPageSauvegardee();
	}
}

void Fenetre::on_menu_fichier_quitter() {
	// on ferme la connexion avec le B
	_b.q();
	exit(0);
}

void Fenetre::on_menu_projet_nouveau() {
	afficherMessage("Création d'un nouveau projet",1);
	Glib::ustring projet = "NewProject";

	int result;
	using namespace Gtk;
	Window dialogBox;
	dialogBox.set_title("Ajout d'un nouveau projet");
	Table* layout= manage(new Table(3,4,false));
	dialogBox.add(*layout);

	Label nameP("Nom du projet :");
	layout->attach(nameP, 0, 1, 0, 1, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
	Entry namePI;
	layout->attach(namePI, 1, 2, 0, 1, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));

	Label nameR1("Database :");
	layout->attach(nameR1, 0, 1, 1, 2, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
	Entry nameR1I;
	layout->attach(nameR1I, 1, 2, 1, 2, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
	Button searchR1("Rechercher");
	layout->attach(searchR1, 2, 3, 1, 2, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
	searchR1.signal_clicked().connect( sigc::bind<Window*, Entry*>(sigc::mem_fun(*this, &Fenetre::on_button_ChooseRepository), &dialogBox, &nameR1I));

	Label nameR2("Translation :");
	layout->attach(nameR2, 0, 1, 2, 3, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
	Entry nameR2I;
	layout->attach(nameR2I, 1, 2, 2, 3, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
	Button searchR2("Rechercher");
	layout->attach(searchR2, 2, 3, 2, 3, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
	searchR2.signal_clicked().connect( sigc::bind<Window*, Entry*>(sigc::mem_fun(*this, &Fenetre::on_button_ChooseRepository), &dialogBox, &nameR2I));

	HBox buttonBox(true,10);
	layout->attach(buttonBox, 0, 3,  3, 4, FILL, FILL, Gtk::AttachOptions(2));
	Button okButton("OK");
	buttonBox.pack_start(okButton);
	okButton.signal_clicked().connect( sigc::bind<Gtk::Widget*, int*>(sigc::mem_fun(*this, &Fenetre::on_button_OK), &dialogBox, &result));
	Button cancelButton("CANCEL");
	buttonBox.pack_start(cancelButton);
	cancelButton.signal_clicked().connect( sigc::bind<Gtk::Widget*, int*>(sigc::mem_fun(*this, &Fenetre::on_button_CANCEL), &dialogBox, &result));

	dialogBox.show_all();
	dialogBox.set_icon(m_iconFenetreAux);
	int w,h, w2, h2, w3, h3;
	get_position(w,h);
	get_size(w2,h2);
	dialogBox.get_size(w3,h3);
	dialogBox.move (w + w2/2 - w3/2, h + h2/2 - h3/2);
	Main::run(dialogBox);
	//dialogBox.run();
	switch(result) {
		case (Gtk::RESPONSE_OK) :
			if (namePI.get_text()!= "" && nameR1I.get_text() != "" && nameR2I.get_text() != "") {
				afficherMessage(_b.createProject(namePI.get_text() + " " + nameR1I.get_text() + " " + nameR2I.get_text()),1);
				creerListeProjets();
			}
			else afficherMessage("Les informations entrees sont incorrectes",3);
			break;
		case (Gtk::RESPONSE_CANCEL) :
			afficherMessage("Création d'un nouveau projet annulee",2);
			break;
		case (Gtk::RESPONSE_YES) : //pas utilise
			break;
		case (Gtk::RESPONSE_NO) : //pas utilise
			break;
	}
}

void Fenetre::on_menu_projet_supprimer() {
	if (_currentProject != "") {
		_b.closeProject();
		afficherMessage(_b.removeProject(_currentProject),1);
		_currentProject = "";
		set_title("Atelier B ;   ( Pas de projet ouvert )");
		creerListeProjets();
		refraichirArbreProjet();
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_projet_informations() {
	if (_currentProject != "") {
		SVector vInfos = _b.infosProject(_currentProject);
		afficherMessage(vInfos,1);
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_projet_projet(Glib::ustring s) {
	// Construction d'un projet en particulier
	if (_currentProject != "") {
		_b.closeProject();
		_currentProject = "";
	}
	_currentProject = s;
	afficherMessage("Ouverture du projet " + _currentProject,1);
	Glib::ustring title = "Atelier B ;    ";
	title += "Projet : ";
	title += s;
	set_title(title);
	_b.openProject(s);
	// Rafraichissement de l'arbre en fonction du projet
	refraichirArbreProjet();

}

void Fenetre::on_menu_projet_fermer() {
	if (_currentProject != "") {
		afficherMessage("Fermeture du projet "  +_currentProject,1);
		_b.closeProject();
		_currentProject = "";
		set_title("Atelier B ;    ( Pas de projet ouvert )");
		refraichirArbreProjet();
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_librairie_ajouter() {
	if (_currentProject != "") {
		using namespace Gtk;
		int result;
		Window dialogBox;
		dialogBox.set_resizable(false);
		dialogBox.set_title("Ajout d'une librairie au projet");
		Glib::RefPtr<Gtk::TreeStore> refTreeProjectUsersModel;
		Gtk::TreeModel::Row projectUsersBranche;
		Gtk::TreeView treeViewProjectUsers;
		SVector liste;
		Table* layout= manage(new Table(3,4,false));
		dialogBox.add(*layout);

		refTreeProjectUsersModel = Gtk::TreeStore::create(m_Columns);
		treeViewProjectUsers.set_model(refTreeProjectUsersModel);
		treeViewProjectUsers.append_column("Utilisateur(s)", m_Columns.m_col_name);
		Gtk::ScrolledWindow scrWin;
		scrWin.set_size_request(200,170);
		scrWin.add(treeViewProjectUsers);
		layout->attach(scrWin, 0, 3, 0, 3, FILL, FILL, Gtk::AttachOptions(2));

		for (int i = 0; i < _vProjets.size(); i++) {
			projectUsersBranche = *(refTreeProjectUsersModel->append());
			projectUsersBranche[m_Columns.m_col_name] = _vProjets[i];
		}

		HBox buttonBox(true,10);
		layout->attach(buttonBox, 0, 3,  3, 4, EXPAND, SHRINK, Gtk::AttachOptions(2));
		Button okButton("OK");
		buttonBox.pack_start(okButton);
		okButton.signal_clicked().connect( sigc::bind<Gtk::Widget*, int*>(sigc::mem_fun(*this, &Fenetre::on_button_OK), &dialogBox, &result));
		Button cancelButton("CANCEL");
		buttonBox.pack_start(cancelButton);
		cancelButton.signal_clicked().connect( sigc::bind<Gtk::Widget*, int*>(sigc::mem_fun(*this, &Fenetre::on_button_CANCEL), &dialogBox, &result));

		dialogBox.show_all();
		dialogBox.set_icon(m_iconFenetreAux);
		int w,h, w2, h2, w3, h3;
		get_position(w,h);
		get_size(w2,h2);
		dialogBox.get_size(w3,h3);
		dialogBox.move (w + w2/2 - w3/2, h + h2/2 - h3/2);
		Main::run(dialogBox);
		switch(result) {
			case (Gtk::RESPONSE_OK) :
			{
				Glib::RefPtr<Gtk::TreeSelection> refSelection = treeViewProjectUsers.get_selection();
				if(const Gtk::TreeIter iter = refSelection->get_selected()) {
					const Gtk::TreePath path (refTreeProjectUsersModel->get_path(iter));
					const std::vector<int> indices (path.get_indices());
					if(!indices.empty())
					{
						if (_vProjets.size() > indices[0]) {
							SVector result = _b.addLibraries(_currentProject, _vProjets[indices[0]]);
							if (_b.contientErreurs(result) < 1) {
								afficherMessage(result,1);
							} else {
								afficherMessage(result,3);
							}
							refraichirArbreProjet();
						}
					}
				}
				else afficherMessage("Les informations entrees sont incorrectes",3);
				break;
			}
			case (Gtk::RESPONSE_CANCEL) :
				afficherMessage("Ajout d'une nouvelle librairie annule",2);
				break;
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_librairie_supprimer() {
	if (_currentProject != "") {
		Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
		if(const Gtk::TreeIter iter = refSelection->get_selected())
		{
			const Gtk::TreePath path (m_refTreeModel->get_path(iter));
			//cout << "Path : " << path.to_string().c_str() << endl;
			const std::vector<int> indices (path.get_indices());
			if(!indices.empty())
			{
				if (indices[0] == 1) {
					if (_vLibrairies.size() >= indices[1]) {
						SVector result = _b.removeLibraries(_currentProject,stripWhiteSpace(_vLibrairies[indices[1]]));
						if (_b.contientErreurs(result) < 1) {
							afficherMessage(result,1);
						} else {
							afficherMessage(result,3);
						}
						refraichirArbreProjet();
					} else {
						afficherMessage("Probleme rencontre dans la selection de la librairie",3);
					}
				} else {
					afficherMessage("Pas de librairie selectionnee",2);
				}
			}
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_langageB_nouveau() {
	// on peut ajouter du langage B comme suit :
	// .mch, .ref, .mod, .imp
	if (_currentProject != "") {

		int result;
		using namespace Gtk;
		Window dialogBox;
		dialogBox.set_title("Ajout d'un composant");
		Table* layout= manage(new Table(3,4,false));
		dialogBox.add(*layout);


		Label nameR1("Composant :");
		layout->attach(nameR1, 0, 1, 1, 2, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
		Entry nameR1I;
		layout->attach(nameR1I, 1, 2, 1, 2, Gtk::FILL | Gtk::EXPAND, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
		Button searchR1("Rechercher");
		layout->attach(searchR1, 2, 3, 1, 2, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
		searchR1.signal_clicked().connect( sigc::bind<Window*, Entry*>(sigc::mem_fun(*this, &Fenetre::on_button_ChooseFile), &dialogBox, &nameR1I));

		HBox buttonBox(true,10);
		layout->attach(buttonBox, 0, 3,  3, 4, FILL, FILL, Gtk::AttachOptions(2));
		Button okButton("OK");
		buttonBox.pack_start(okButton);
		okButton.signal_clicked().connect( sigc::bind<Gtk::Widget*, int*>(sigc::mem_fun(*this, &Fenetre::on_button_OK), &dialogBox, &result));
		Button cancelButton("CANCEL");
		buttonBox.pack_start(cancelButton);
		cancelButton.signal_clicked().connect( sigc::bind<Gtk::Widget*, int*>(sigc::mem_fun(*this, &Fenetre::on_button_CANCEL), &dialogBox, &result));

		dialogBox.show_all();
		dialogBox.set_icon(m_iconFenetreAux);
		int w,h, w2, h2, w3, h3;
		get_position(w,h);
		get_size(w2,h2);
		dialogBox.get_size(w3,h3);
		dialogBox.move (w + w2/2 - w3/2, h + h2/2 - h3/2);
		Main::run(dialogBox);
		//dialogBox.run();
		switch(result) {
			case (Gtk::RESPONSE_OK) :

				if (nameR1I.get_text() != "") {
					Glib::ustring file = nameR1I.get_text();
					// Traitement sur le fichier selectionne
					Glib::ustring c("/");
					int n = file.find_last_of(c);
					Glib::ustring c2(".");
					int n2 = file.find_last_of(c2);
					// nom du fichier
					Glib::ustring name = file.substr(n+1,file.size()-n-(n-n2));
					// nom du fichier
					Glib::ustring extension = file.substr(file.size()-4,4);
					// Repertoire
					Glib::ustring repository = file.substr(0,n+1);
					//cout << "#### Name : " << name.c_str() << "\nExtension : " << extension.c_str() << "\nRepository : " << repository.c_str() <<  endl;
					// Ajout du composant
					SVector result = _b.addComponent(repository, name);
					if (_b.contientErreurs(result) < 1) {
						afficherMessage(result,1);
					} else {
						afficherMessage(result,3);
					}
					refraichirArbreProjet();
				} else {
					afficherMessage("Les informations entrees sont incorrectes",3);
				}
				break;
			case (Gtk::RESPONSE_CANCEL) :
				afficherMessage("Ajout d'un nouveau composant annule",2);
				break;
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_langageB_supprimer() {
	Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
if(const Gtk::TreeIter iter = refSelection->get_selected())
  {
    const Gtk::TreePath path (m_refTreeModel->get_path(iter));
		const std::vector<int> indices (path.get_indices());
    if(!indices.empty())
    {
			if (indices[0] == 0) {
				if (_vMachines.size() >= indices[1]) {
					SVector result = _b.removeComponent(stripWhiteSpace(_vMachines[indices[1]]));
					if (_b.contientErreurs(result) < 1) {
						afficherMessage(result,1);
					} else {
						afficherMessage(result,3);
					}
					refraichirArbreProjet();
				} else {
					afficherMessage("Probleme rencontre dans la selection du composant",3);
				}
			} else {
				afficherMessage("Pas de composant selectionne",2);
			}
    } else {
			afficherMessage("Pas de composant selectionne",2);
		}

  }
}

void Fenetre::on_menu_langageB_editer() {
	if (_currentProject != "") {
		Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
		if(const Gtk::TreeIter iter = refSelection->get_selected())
		{
			const Gtk::TreePath path (m_refTreeModel->get_path(iter));
			const std::vector<int> indices (path.get_indices());
			if(!indices.empty())
			{
				if (indices[0] < 3) {
					if (_vComposantsProjet[indices[0]].size() > indices[1]) {
						Glib::ustring chemin = _b.findLocation(stripWhiteSpace( (_vComposantsProjet[indices[0]])[indices[1]]));
						if (chemin != "") {
							ouvertureFichier(chemin);
						} else {
							afficherMessage("Probleme rencontre dans l'ouverture du composant",3);
						}
					} else {
						afficherMessage("Pas de composant selectionne",2);
					}
				} else {
					afficherMessage("Il n'est pas possible d'editer le composant selectionne",2);
				}
			}
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_langageB_informations() {
	if (_currentProject != "") {
		Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
		if(const Gtk::TreeIter iter = refSelection->get_selected())
		{
			const Gtk::TreePath path (m_refTreeModel->get_path(iter));
			const std::vector<int> indices (path.get_indices());
			if(!indices.empty())
			{
				if (_vComposantsProjet[indices[0]].size() > indices[1]) {
					afficherMessage(_b.infosComposant(stripWhiteSpace( (_vComposantsProjet[indices[0]])[indices[1]])),1);
				} else {
					afficherMessage("Pas de composant selectionne",2);
				}
			} else {
				afficherMessage("Pas de composant selectionne",2);
			}
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_langageB_statut() {
	if (_currentProject != "") {
		Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
		if(const Gtk::TreeIter iter = refSelection->get_selected())
		{
			const Gtk::TreePath path (m_refTreeModel->get_path(iter));
			const std::vector<int> indices (path.get_indices());
			if(!indices.empty())
			{
				if (_vComposantsProjet[indices[0]].size() > indices[1]) {
					afficherMessage(_b.showComposantStatut(stripWhiteSpace( (_vComposantsProjet[indices[0]])[indices[1]])),1);
				} else {
					afficherMessage("Pas de composant selectionne",2);
				}
			} else {
				afficherMessage("Pas de composant selectionne",2);
			}
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_langageB_activerDependance() {
	if (!_dependancesActivees) {
		afficherMessage(_b.enableDependence(),1);
		_dependancesActivees = !_dependancesActivees;
	}
}

void Fenetre::on_menu_langageB_desactiverDependance() {
	if (_dependancesActivees) {
		afficherMessage(_b.disableDependence(),1);
		_dependancesActivees = !_dependancesActivees;
	}
}

void Fenetre::on_menu_methodeB_typeCheck() {
	if (_currentProject != "") {
		Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
		if(const Gtk::TreeIter iter = refSelection->get_selected())
		{
			const Gtk::TreePath path (m_refTreeModel->get_path(iter));
			const std::vector<int> indices (path.get_indices());
			if(!indices.empty())
			{
				if (_vComposantsProjet[indices[0]].size() > indices[1]) {
					afficherMessage(_b.typeChecking(stripWhiteSpace( (_vComposantsProjet[indices[0]])[indices[1]])),1);
				}
			} else {
				afficherMessage("Pas de composant selectionne",2);
			}
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_methodeB_preuve_force0() {
		if (_currentProject != "") {
		Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
		if(const Gtk::TreeIter iter = refSelection->get_selected())
		{
			const Gtk::TreePath path (m_refTreeModel->get_path(iter));
			const std::vector<int> indices (path.get_indices());
			if(!indices.empty())
			{
				if (_vComposantsProjet[indices[0]].size() > indices[1]) {
					afficherMessage(_b.prove(stripWhiteSpace( (_vComposantsProjet[indices[0]])[indices[1]]), "0"),1);
				} else {
					afficherMessage("Pas de composant selectionne",2);
				}
			} else {
				afficherMessage("Pas de composant selectionne",2);
			}
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_methodeB_preuve_force1() {
	if (_currentProject != "") {
		Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
		if(const Gtk::TreeIter iter = refSelection->get_selected())
		{
			const Gtk::TreePath path (m_refTreeModel->get_path(iter));
			const std::vector<int> indices (path.get_indices());
			if(!indices.empty())
			{
				if (_vComposantsProjet[indices[0]].size() > indices[1]) {
					afficherMessage(_b.prove(stripWhiteSpace( (_vComposantsProjet[indices[0]])[indices[1]]), "1"),1);
				} else {
					afficherMessage("Pas de composant selectionne",2);
				}
			} else {
				afficherMessage("Pas de composant selectionne",2);
			}
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_methodeB_preuve_force2() {
	if (_currentProject != "") {
		Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
		if(const Gtk::TreeIter iter = refSelection->get_selected())
		{
			const Gtk::TreePath path (m_refTreeModel->get_path(iter));
			const std::vector<int> indices (path.get_indices());
			if(!indices.empty())
			{
				if (_vComposantsProjet[indices[0]].size() > indices[1]) {
					afficherMessage(_b.prove(stripWhiteSpace( (_vComposantsProjet[indices[0]])[indices[1]]), "2"),1);
				} else {
					afficherMessage("Pas de composant selectionne",2);
				}
			} else {
				afficherMessage("Pas de composant selectionne",2);
			}
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_methodeB_preuve_force3() {
	if (_currentProject != "") {
		Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
		if(const Gtk::TreeIter iter = refSelection->get_selected())
		{
			const Gtk::TreePath path (m_refTreeModel->get_path(iter));
			const std::vector<int> indices (path.get_indices());
			if(!indices.empty())
			{
				if (_vComposantsProjet[indices[0]].size() > indices[1]) {
					afficherMessage(_b.prove(stripWhiteSpace( (_vComposantsProjet[indices[0]])[indices[1]]), "3"),1);
				} else {
					afficherMessage("Pas de composant selectionne",2);
				}
			} else {
				afficherMessage("Pas de composant selectionne",2);
			}
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_methodeB_boCheck() {
	if (_currentProject != "") {
		Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
		if(const Gtk::TreeIter iter = refSelection->get_selected())
		{
			const Gtk::TreePath path (m_refTreeModel->get_path(iter));
			const std::vector<int> indices (path.get_indices());
			if(!indices.empty())
			{
				if (_vComposantsProjet[indices[0]].size() > indices[1]) {
					afficherMessage(_b.bOCheck(stripWhiteSpace( (_vComposantsProjet[indices[0]])[indices[1]])),1);
				} else {
					afficherMessage("Pas de composant selectionne",2);
				}
			} else {
				afficherMessage("Pas de composant selectionne",2);
			}
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_methodeB_POGenerate(bool full) {
	if (_currentProject != "") {
		Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
		if(const Gtk::TreeIter iter = refSelection->get_selected())
		{
			const Gtk::TreePath path (m_refTreeModel->get_path(iter));
			const std::vector<int> indices (path.get_indices());
			if(!indices.empty())
			{
				if (_vComposantsProjet[indices[0]].size() > indices[1]) {
					if (full) {
						afficherMessage(_b.pOGenerate(stripWhiteSpace( (_vComposantsProjet[indices[0]])[indices[1]]), "0"),1);
					} else {
						afficherMessage(_b.pOGenerate(stripWhiteSpace( (_vComposantsProjet[indices[0]])[indices[1]]), "1"),1);
					}
				} else {
					afficherMessage("Pas de composant selectionne",2);
				}
			} else {
				afficherMessage("Pas de composant selectionne",2);
			}
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_utilisateur_ajouter() {
	if (_currentProject != "") {
		int result;
		using namespace Gtk;
		Window dialogBox;
		dialogBox.set_title("Ajout d'un composant");
		Table* layout= manage(new Table(3,4,false));
		dialogBox.add(*layout);

		Label nameR1("Utilisateur :");
		layout->attach(nameR1, 0, 1, 1, 2, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
		Entry nameR1I;
		layout->attach(nameR1I, 1, 3, 1, 2, Gtk::FILL | Gtk::EXPAND, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
		Label nameR2("Type :");
		layout->attach(nameR2, 0, 1, 2, 3, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
		RadioButton rbReader("Lecteur");
		RadioButton rbUser("Utilisateur");
		rbUser.set_active();
		Gtk::RadioButton::Group group;
		rbReader.set_group(group);
		rbUser.set_group(group);
		layout->attach(rbReader, 1, 2, 2, 3, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));
		layout->attach(rbUser, 2, 3, 2, 3, Gtk::FILL | Gtk::SHRINK, Gtk::FILL | Gtk::EXPAND, Gtk::AttachOptions(2));

		HBox buttonBox(true,10);
		layout->attach(buttonBox, 0, 3,  3, 4, FILL, FILL, Gtk::AttachOptions(2));
		Button okButton("OK");
		buttonBox.pack_start(okButton);
		okButton.signal_clicked().connect( sigc::bind<Gtk::Widget*, int*>(sigc::mem_fun(*this, &Fenetre::on_button_OK), &dialogBox, &result));
		Button cancelButton("CANCEL");
		buttonBox.pack_start(cancelButton);
		cancelButton.signal_clicked().connect( sigc::bind<Gtk::Widget*, int*>(sigc::mem_fun(*this, &Fenetre::on_button_CANCEL), &dialogBox, &result));

		dialogBox.show_all();
		dialogBox.set_icon(m_iconFenetreAux);
		int w,h, w2, h2, w3, h3;
		get_position(w,h);
		get_size(w2,h2);
		dialogBox.get_size(w3,h3);
		dialogBox.move (w + w2/2 - w3/2, h + h2/2 - h3/2);
		Main::run(dialogBox);
		switch(result) {
			case (Gtk::RESPONSE_OK) :
				if (nameR1I.get_text() != "") {
					Glib::ustring name = nameR1I.get_text();
					SVector result;
					if(rbUser.get_active()) {
						result = _b.addProjectUser(_currentProject, name);
					} else {
						result = _b.addProjectReader(_currentProject, name);
					}
					if (_b.contientErreurs(result) < 1) {
						afficherMessage(result,1);
					} else {
						afficherMessage(result,3);
					}
				} else {
					afficherMessage("Les informations entrees sont incorrectes",3);
				}
				break;
			case (Gtk::RESPONSE_CANCEL) :
				afficherMessage("Ajout d'un nouvel utilisateur annule",2);
				break;
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_utilisateur_supprimer(bool user) {
	if (_currentProject != "") {
		int result;
		using namespace Gtk;
		Window dialogBox;
		dialogBox.set_resizable(false);
		Glib::RefPtr<Gtk::TreeStore> refTreeProjectUsersModel;
		TreeModel::Row projectUsersBranche;
		TreeView treeViewProjectUsers;
		SVector liste;
		Table* layout= manage(new Table(3,4,false));
		dialogBox.add(*layout);

		refTreeProjectUsersModel = Gtk::TreeStore::create(m_Columns);
		treeViewProjectUsers.set_model(refTreeProjectUsersModel);
		if (user) {
			dialogBox.set_title("Suppression d'un utilisateur");
			treeViewProjectUsers.append_column("Utilisateur(s)", m_Columns.m_col_name);
			liste = _b.showAllProjectUsers(_currentProject);
		} else {
			dialogBox.set_title("Suppression d'un lecteur");
			treeViewProjectUsers.append_column("Lecteur(s)", m_Columns.m_col_name);
			liste = _b.showAllProjectReaders(_currentProject);
		}
		Gtk::ScrolledWindow scrWin;
		scrWin.set_size_request(200,160);
		scrWin.add(treeViewProjectUsers);
		layout->attach(scrWin, 0, 3, 0, 3, FILL , FILL , AttachOptions(2));


		for (int i = 0; i < liste.size(); i++) {
			projectUsersBranche = *(refTreeProjectUsersModel->append());
			projectUsersBranche[m_Columns.m_col_name] = liste[i];
		}
		HBox buttonBox(true,10);
		layout->attach(buttonBox, 0, 3,  3, 4, EXPAND, SHRINK, Gtk::AttachOptions(2));
		Button okButton("OK");
		buttonBox.pack_start(okButton);
		okButton.signal_clicked().connect( sigc::bind<Gtk::Widget*, int*>(sigc::mem_fun(*this, &Fenetre::on_button_OK), &dialogBox, &result));
		Button cancelButton("CANCEL");
		buttonBox.pack_start(cancelButton);
		cancelButton.signal_clicked().connect( sigc::bind<Gtk::Widget*, int*>(sigc::mem_fun(*this, &Fenetre::on_button_CANCEL), &dialogBox, &result));

		dialogBox.show_all();
		dialogBox.set_icon(m_iconFenetreAux);
		int w,h, w2, h2, w3, h3;
		get_position(w,h);
		get_size(w2,h2);
		dialogBox.get_size(w3,h3);
		dialogBox.move (w + w2/2 - w3/2, h + h2/2 - h3/2);
		Main::run(dialogBox);
		switch(result) {
			case (RESPONSE_OK) :
			{
				Glib::RefPtr<Gtk::TreeSelection> refSelection = treeViewProjectUsers.get_selection();
				if(const Gtk::TreeIter iter = refSelection->get_selected()) {
					const Gtk::TreePath path (refTreeProjectUsersModel->get_path(iter));
					const std::vector<int> indices (path.get_indices());
					if(!indices.empty())
					{
						if (liste.size() > indices[0]) {
							_b.closeProject();
							SVector result = _b.removeProjectUser(_currentProject, liste[indices[0]]);
							_b.openProject(_currentProject);
							if (_b.contientErreurs(result) < 1) {
								afficherMessage(result,1);
							} else {
								afficherMessage(result,3);
							}
						}
					}
				}
				break;
			}
			case (RESPONSE_CANCEL) :
				afficherMessage("Suppression d'un nouvel utilisateur annulee",2);
				break;
		}
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_utilisateur_montrerListe() {
	if (_currentProject != "") {
		int result;
		using namespace Gtk;
		Window dialogBox;
		dialogBox.set_title("Suppression d'un utilisateur");
		dialogBox.set_resizable(false);
		Glib::RefPtr<Gtk::TreeStore> refTreeProjectUsersModel;
		Gtk::TreeModel::Row usersBranche, readersBranche;
		Gtk::TreeView treeViewProjectUsers;
		SVector listeU, listeR;
		Table* layout= manage(new Table(3,4,false));
		dialogBox.add(*layout);

		
		listeU = _b.showAllProjectUsers(_currentProject);
		listeR = _b.showAllProjectReaders(_currentProject);

		refTreeProjectUsersModel = Gtk::TreeStore::create(m_Columns);
		treeViewProjectUsers.set_model(refTreeProjectUsersModel);
		
		usersBranche = *(refTreeProjectUsersModel->append());
		usersBranche[m_Columns.m_col_name] = "Utilisateur(s)";
		readersBranche = *(refTreeProjectUsersModel->append());
		readersBranche[m_Columns.m_col_name] = "Lecteur(s)";

		treeViewProjectUsers.append_column("Listes des ...", m_Columns.m_col_name);

		Gtk::ScrolledWindow scrWin;
		scrWin.set_size_request(200,160);
		scrWin.add(treeViewProjectUsers);
		layout->attach(scrWin, 0, 3, 0, 3, FILL, FILL, Gtk::AttachOptions(2));


		for (int i = 0; i < listeU.size(); i++) {
			Gtk::TreeModel::Row childrow = *(refTreeProjectUsersModel->append(usersBranche.children()));
			childrow[m_Columns.m_col_name] = listeU[i];
		}
		for (int i = 0; i < listeR.size(); i++) {
			Gtk::TreeModel::Row childrow = *(refTreeProjectUsersModel->append(readersBranche.children()));
			childrow[m_Columns.m_col_name] = listeR[i];
		}

		HBox buttonBox(true,10);
		layout->attach(buttonBox, 0, 3,  3, 4, EXPAND, SHRINK, Gtk::AttachOptions(2));
		Button okButton("OK");
		buttonBox.pack_start(okButton);
		okButton.signal_clicked().connect( sigc::bind<Gtk::Widget*, int*>(sigc::mem_fun(*this, &Fenetre::on_button_OK), &dialogBox, &result));

		dialogBox.show_all();
		dialogBox.set_icon(m_iconFenetreAux);
		int w,h, w2, h2, w3, h3;
		get_position(w,h);
		get_size(w2,h2);
		dialogBox.get_size(w3,h3);
		dialogBox.move (w + w2/2 - w3/2, h + h2/2 - h3/2);
		Main::run(dialogBox);
	} else {
		afficherMessage("Operation impossible car il n'y a pas de projet ouvert",3);
	}
}

void Fenetre::on_menu_aide_a_propos() {
	int result;
	using namespace Gtk;
	Window dialogBox;
	dialogBox.set_title("A Propos de l'atelierB");
	dialogBox.set_resizable(false);

	Table* layout = manage(new Table(4,3,false));
	layout->set_row_spacings(10);
	layout->set_col_spacings(10);
	dialogBox.add(*layout);

	Glib::RefPtr<Gdk::Pixbuf> pix;// = Gdk::Pixbuf::create_from_file(dir_icon+"/atelierb.gif");
	Image imageB(pix);
	layout->attach(imageB, 0, 2, 0, 2, FILL, FILL, Gtk::AttachOptions(2));

	Label label("Cree le 06-10-03 \ndans le cadre du projet d'IHM \npar \n\tBOIBESSOT Thomas");
	layout->attach(label, 2, 4, 0, 2, FILL, FILL, Gtk::AttachOptions(2));

	HBox buttonBox(true,10);
	layout->attach(buttonBox, 0, 4,  2, 3, EXPAND, SHRINK, Gtk::AttachOptions(2));
	Button okButton("OK");
	buttonBox.pack_start(okButton);
	okButton.signal_clicked().connect( sigc::bind<Gtk::Widget*, int*>(sigc::mem_fun(*this, &Fenetre::on_button_OK), &dialogBox, &result));

	dialogBox.show_all();
	dialogBox.set_icon(m_iconFenetreAux);
	int w,h, w2, h2, w3, h3;
	get_position(w,h);
	get_size(w2,h2);
	dialogBox.get_size(w3,h3);
	dialogBox.move (w + w2/2 - w3/2, h + h2/2 - h3/2);
	Main::run(dialogBox);
}

/** Fonctions de la barre d'outils **/
void Fenetre::on_toolbar_nouveau() {
	on_menu_fichier_nouveau();
}

void Fenetre::on_toolbar_ouvrir() {
	on_menu_fichier_ouvrir();
}

void Fenetre::on_toolbar_enregistrer() {
	on_menu_fichier_enregistrer();
}

void Fenetre::on_toolbar_copy() {
	m_EditeurB->copy_clipboard(m_clipboard);
}

void Fenetre::on_toolbar_cut() {
	m_EditeurB->cut_clipboard(m_clipboard);
}

void Fenetre::on_toolbar_paste() {
	m_EditeurB->paste_clipboard(m_clipboard);
}

/** fonctions pour les boutons de boites de dialogue **/

void Fenetre::on_button_OK(Gtk::Widget* widget, int* result) {
	widget->hide();
	*result = Gtk::RESPONSE_OK;
}

void Fenetre::on_button_CANCEL(Gtk::Widget* widget, int* result) {
	widget->hide();
	*result = Gtk::RESPONSE_CANCEL;
}

void Fenetre::on_button_NON(Gtk::Widget* widget, int* result) {
	widget->hide();
	*result = Gtk::RESPONSE_YES;
}

void Fenetre::on_button_OUI(Gtk::Widget* widget, int* result) {
	widget->hide();
	*result = Gtk::RESPONSE_NO;
}

void Fenetre::on_button_ChooseRepository(Gtk::Window* parent, Gtk::Entry* entry) {
	int result;
 	Gtk::FileSelection dialog("Selectionner un repertoire");
	dialog.set_transient_for(*parent);
	result = dialog.run();

	if (result == Gtk::RESPONSE_OK) {
		Glib::ustring ch = dialog.get_filename();
		entry->set_text(ch.substr(0, ch.find_last_of("/")+1));
	}
}

void Fenetre::on_button_ChooseFile(Gtk::Window* parent, Gtk::Entry* entry) {
	int result;
 	Gtk::FileSelection dialog("Selectionner un fichier");
	dialog.set_transient_for(*parent);
	result = dialog.run();

	if (result == Gtk::RESPONSE_OK) {
		entry->set_text(dialog.get_filename());
	}
}

void Fenetre::on_entry_commandB() {
	SVector result = _b.executeCommandeB(m_CommandB.get_text());
	result.insert(result.begin(), "Execution de " + m_CommandB.get_text() + "\n");
	afficherMessage(result,1);
}

bool Fenetre::on_projectTree_click(GdkEventButton* event) {
	static const long doubleclickdelay = 250;
	static long lastclick = 0;
	bool result = true;
	if (event->type == GDK_BUTTON_RELEASE)
	{
		if (event->button == 3) {
			Glib::RefPtr<Gtk::TreeSelection> refSelection = m_TreeView.get_selection();
			if(const Gtk::TreeIter iter = refSelection->get_selected()) {
				const Gtk::TreePath path (m_refTreeModel->get_path(iter));
				const std::vector<int> indices (path.get_indices());
				if (indices[0] == 1 || indices[0] == 0) {
					if (indices.size() > 1) {
						m_Menu_PopupComposant.popup(event->button, event->time);
					}
				}
			}
		} else if (event->button == 1) {
			if (lastclick == 0 || (lastclick + doubleclickdelay < event->time)) {
					lastclick = event->time;

			} else {
				on_menu_langageB_editer();
			}
		}
		result = true;
	} else {
		result = false;
	}
	return result;
}
