//
// B.h
//	connexion avec l'atelierB distant
//

#ifndef _B_H
#define _B_H

//#define _GNU_SOURCE 500

#include <gtkmm.h>

#include "annexes.h"

class B
{
private:
	// descriteur de socket pour la connexion avec l'atelierB
	int fd_maitre;
	// tampon de lecture des reponses de l'atelierB
	Glib::ustring tampon;
	// Fonction : lecture de la reponse a une fonction executee sur l'atelierB distant
	void litReponseB(char);
	// Fonction : recherche d'une eventuelle marque d'erreur dans une chaine
	bool erreurTrouve( const Glib::ustring& );
	// Fonction : recherche s'il existe des erreurs dans une liste de chaines
	int erreurTrouve(const Glib::ustring& , SVector );
	// Fonction : envoie une commande a l'atelierB distant
	char* execCmd(const char*);
	// Fonction : envoie une commande a l'atelierB distant
	void execCmd (const Glib::ustring&);

public :

	// Constructeur
	B();

	// Destructeur
	~B();

	// Fonction : initialise la connexion avec l'atelierB distant
	int initB();
	// Fonction : ferme la connexion avec l'atelierB distant
	void q();
	// Fonction : lance la connexion avec l'atelierB distant
	char* lance();

  /************************** Gestion des projets ********************************/
	// Fonction : retourne un nombre > 1 si la liste de chaines contient des erreurs
	int contientErreurs(SVector v);
	// Fonction : retourne la liste des projets
	SVector showProjectsList ();
	// Fonction : ouvre un projet
	void openProject(const Glib::ustring& );
	// Fonction : ferme un projet
	void closeProject( void );
	// Fonction : donne les infos sur un projet
	SVector infosProject( const Glib::ustring&);
	// Fonction : execute une commande en passant par l'atelierB et renvoie son resultat
	SVector executeCommandeB ( const Glib::ustring&);
	// Fonction : cree un projet
	SVector createProject( const Glib::ustring& );
	// Fonction : supprime un projet
	SVector removeProject( const Glib::ustring& );
	// Fonction : ajoute une librairie a un projet
	SVector addLibraries( const Glib::ustring& nProject, const Glib::ustring& nLib);
	// Fonction : supprime une librairie d'un projet
	SVector removeLibraries( const Glib::ustring& nProject, const Glib::ustring& nLib);
	// Fonction : renvoie toutes les librairies du projet
	SVector showAllLibraries( const Glib::ustring& nProjet );
	// Fonction : ajoute un composant a un projet
	SVector addComponent( const Glib::ustring& nD, const Glib::ustring& nF );
	// Fonction : supprime un composant d'un projet
	SVector removeComponent(const Glib::ustring& nC );
	// Fonction : renvoie le liste des machines d'un projet
	SVector showAllMachines(const Glib::ustring& nProjet );
	// Fonction : renvoie le liste des implementation d'un projet
	SVector showAllImplementations(const Glib::ustring& nProjet );
	// Fonction : renovoie la liste des rafinements d'un projet
	SVector showAllRafinements(const Glib::ustring& nProjet );
	// Fonction : typecheck un composant
	SVector typeChecking(const Glib::ustring& cO );
	// Fonction : renvoie les obligations de preuve d'un composant
	SVector showObligationsPreuve (const Glib::ustring& cO );
	// Fonction : prouve un composant (full/differential)
	SVector prove(const Glib::ustring& cO,const Glib::ustring& force );
	// Fonction : controle de BO
	SVector bOCheck(const Glib::ustring& nomC );
	// Fonction : genere les obligations de preuve
	SVector pOGenerate(const Glib::ustring& nomC,const Glib::ustring& way );
	// Fonction : renvoie les infos sur un composant
	SVector infosComposant(const Glib::ustring& nomC);
	// Fonction : trouve le chemin absolu d'un composant
	Glib::ustring findLocation(const Glib::ustring& nomC);
	// Fonction : active la gestion des dependances
	SVector enableDependence();
	// Fonction : desactive la gestion des dependances
	SVector disableDependence();
	// Fonction : donne les droits d'utilisation du projet a une personne
	SVector addProjectUser(const Glib::ustring& nomP,const Glib::ustring& nomU );
	// Fonction : donne des droits de lecture du projet a une personne
	SVector addProjectReader (const Glib::ustring& nomP,const Glib::ustring& nomR );
	// Fonction : supprime les droits d'une personne
	SVector removeProjectUser(const Glib::ustring& nomP,const Glib::ustring& nomR );
	// Fonction : renvoie la liste des personnes ayant des droits d'utilisation sur le projet
	SVector showAllProjectUsers(const Glib::ustring& nomP );
	// Fonction : renvoie la liste des personnes ayant des droits de lecture sur le projet
	SVector showAllProjectReaders(const Glib::ustring& nomP );
	// Fonction : renvoie la liste des informations sur un composant
	SVector showComposantStatut(const Glib::ustring& nomC );
};

#endif

