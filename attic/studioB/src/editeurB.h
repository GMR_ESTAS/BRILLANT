//
// editeurB.h
//	Editeur de machine B
//


#ifndef _EDITEURB_H
#define _EDITEURB_H

#include <gtkmm.h>
#include <string>

#include "notebookB.h"

class EditeurB : public Gtk::Frame
{
public:
	// Contructeur
	EditeurB(Gtk::Widget*, const std::string&);
	// Destructeur
	virtual ~EditeurB();
	// Fonction : attribue le texte a l'editeur courant
	void setText(char* contenu);
	// Fonction : retourne le texte de l'editeur courant
	Glib::ustring getText();
	// Fonction : attribue un nom de fichier au ficheri courant
	void setFilename(const std::string& contenu);
	// Fonction : retourne le nonm du fichier courant
	std::string getFilename();
	// Fonction : ajoute un fichier a l'editeur
	void ajouterFeuillet();
	// Fonction : indique a l'editeur que la page courant est consideree comme sauvegardee
	void setPageSauvegardee();

	// Fonction : actions sur la selection de texte
	void paste_clipboard(Glib::RefPtr<Gtk::Clipboard>);
	void cut_clipboard(Glib::RefPtr<Gtk::Clipboard>);
	void copy_clipboard(Glib::RefPtr<Gtk::Clipboard>);

private:
	// Attributs
	NotebookB* _notebookB;

	// Composants graphique
	Gtk::ScrolledWindow m_ScrWindow;
	Gtk::Widget* m_Parent;
	Gtk::HBox m_HBox;
	Gtk::VBox m_VBox, m_ButtonFrame, m_TextFrame, m_bFrame;
	Gtk::VSeparator m_Separator;
	Gtk::TextView m_TextEdit, m_CelluleInfo;
	Glib::RefPtr<Gtk::TextBuffer> m_bufferBEditor, m_bufferCelluleInfo;
	Gtk::Table* m_Layout;
	Gtk::Statusbar m_Statusbar;
	Gtk::Button* m_bFermer;
	Gtk::Button* m_bFermerTT;

	// Fonction : insere un symbole dans la page coourante de l'editeur
	void insererSymbole(const char * symbole);
	// Fonction : affiche la description du symbole dans un composant texte
	void afficherDescriptionSymbole(const char * infos);

	// Fonctions : demande l'insertion d'un symbole dans la page courante de l'editeur
	void on_symbole_and();
	void on_symbole_appendSequence();
	void on_symbole_becomeEqual();
	void on_symbole_braces();
	void on_symbole_cartesianProduct();
	void on_symbole_composition();
	void on_symbole_concatSequence();
	void on_symbole_curly();
	void on_symbole_definitionOperation();
	void on_symbole_directProduct();
	void on_symbole_domainRestriction();
	void on_symbole_domainSubstraction();
	void on_symbole_emptySet();
	void on_symbole_equivalence();
	void on_symbole_exists();
	void on_symbole_finitePowerSet();
	void on_symbole_forall();
	void on_symbole_greatherThanOrEqual();
	void on_symbole_implication();
	void on_symbole_intergerSet();
	void on_symbole_intersection();
	void on_symbole_interval();
	void on_symbole_inverse();
	void on_symbole_in();
	void on_symbole_lambda();
	void on_symbole_lessThanOrEqual();
	void on_symbole_maplet();
	void on_symbole_naturalSet();
	void on_symbole_notEmptyFinitePowerSet();
	void on_symbole_notEmptyNatural();
	void on_symbole_notEmptySubset();
	void on_symbole_notEqual();
	void on_symbole_notIn();
	void on_symbole_notStrictSubset();
	void on_symbole_notSubset();
	void on_symbole_not();
	void on_symbole_or();
	void on_symbole_override();
	void on_symbole_parallelProduct();
	void on_symbole_parenthesis();
	void on_symbole_partialBijection();
	void on_symbole_partialFunction();
	void on_symbole_partialInjection();
	void on_symbole_partialSurjection();
	void on_symbole_powerSet();
	void on_symbole_prefixSequence();
	void on_symbole_prependSequence();
	void on_symbole_product();
	void on_symbole_quantifiedIntersection();
	void on_symbole_quantifiedUnion();
	void on_symbole_rangeRestriction();
	void on_symbole_rangeSubstraction();
	void on_symbole_relation();
	void on_symbole_resultOperation();
	void on_symbole_setIn();
	void on_symbole_sigma();
	void on_symbole_strictSubset();
	void on_symbole_subset();
	void on_symbole_suffixSequence();
	void on_symbole_totalBijection();
	void on_symbole_totalInjection();
	void on_symbole_totalFunction();
	void on_symbole_totalSurjection();
	void on_symbole_union();

	// Fonction : ferme les pages de l"editeur
	void on_fermer();
	void on_fermerTT();
	
	// Fonction : efface la description liee au bouton dans le composant texte
	void in_symbole_and();
	void in_symbole_appendSequence();
	void in_symbole_becomeEqual();
	void in_symbole_braces();
	void in_symbole_cartesianProduct();
	void in_symbole_composition();
	void in_symbole_concatSequence();
	void in_symbole_curly();
	void in_symbole_definitionOperation();
	void in_symbole_directProduct();
	void in_symbole_domainRestriction();
	void in_symbole_domainSubstraction();
	void in_symbole_emptySet();
	void in_symbole_equivalence();
	void in_symbole_exists();
	void in_symbole_finitePowerSet();
	void in_symbole_forall();
	void in_symbole_greatherThanOrEqual();
	void in_symbole_implication();
	void in_symbole_intergerSet();
	void in_symbole_intersection();
	void in_symbole_interval();
	void in_symbole_inverse();
	void in_symbole_in();
	void in_symbole_lambda();
	void in_symbole_lessThanOrEqual();
	void in_symbole_maplet();
	void in_symbole_naturalSet();
	void in_symbole_notEmptyFinitePowerSet();
	void in_symbole_notEmptyNatural();
	void in_symbole_notEmptySubset();
	void in_symbole_notEqual();
	void in_symbole_notIn();
	void in_symbole_notStrictSubset();
	void in_symbole_notSubset();
	void in_symbole_not();
	void in_symbole_or();
	void in_symbole_override();
	void in_symbole_parallelProduct();
	void in_symbole_parenthesis();
	void in_symbole_partialBijection();
	void in_symbole_partialFunction();
	void in_symbole_partialInjection();
	void in_symbole_partialSurjection();
	void in_symbole_powerSet();
	void in_symbole_prefixSequence();
	void in_symbole_prependSequence();
	void in_symbole_product();
	void in_symbole_quantifiedIntersection();
	void in_symbole_quantifiedUnion();
	void in_symbole_rangeRestriction();
	void in_symbole_rangeSubstraction();
	void in_symbole_relation();
	void in_symbole_resultOperation();
	void in_symbole_setIn();
	void in_symbole_sigma();
	void in_symbole_strictSubset();
	void in_symbole_subset();
	void in_symbole_suffixSequence();
	void in_symbole_totalBijection();
	void in_symbole_totalInjection();
	void in_symbole_totalFunction();
	void in_symbole_totalSurjection();
	void in_symbole_union();
	
	// Fonction : supprime la description du composant texte
	void out_symbole();
};

#endif //_FENETRE_H
