//
// voir editeurB.h
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "editeurB.h"

EditeurB::EditeurB(Gtk::Widget* parent, const std::string& dir_icon) :
	m_VBox (false,3),
	m_HBox(false,3),
	m_bFrame(true,5)
{
	//set_size_request(500, 300);
	// Attribution du parent
	m_Parent = parent;

	/** Conception de l'Interface Graphique **/
	add(m_VBox);
	m_VBox.pack_start(m_HBox, Gtk::PACK_EXPAND_WIDGET);
	m_HBox.pack_start(m_ButtonFrame, Gtk::PACK_SHRINK);
	m_HBox.pack_start(m_TextFrame, Gtk::PACK_EXPAND_WIDGET);

	/** Groupe de boutons d'aide à l'édition B **/
	int height = 20;
	int width = 22;
	int xx = 2;
	int yy = 2;
	m_ButtonFrame.set_size_request(110, 420);
	//m_ButtonFrame.set_default_size(100, 325);
	m_Layout = Gtk::manage(new Gtk::Table(16, 4, true));
	m_ButtonFrame.add(*m_Layout);

	Gtk::Button* andButton = Gtk::manage(new Gtk::Button());;
	andButton->add_pixlabel(dir_icon+"/iconsB/png/And.png","");
	andButton->set_size_request(width,height);
	andButton->set_border_width(0);
	andButton->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_and));
	andButton->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_and));
	andButton->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*andButton, 0, 1, 0, 1, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* appendSequence = Gtk::manage(new Gtk::Button());;
	appendSequence->add_pixlabel(dir_icon+"/iconsB/png/AppendSequence.png","");
	appendSequence->set_size_request(width,height);
	appendSequence->set_border_width(0);
	appendSequence->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_appendSequence));
	appendSequence->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_appendSequence));
	appendSequence->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*appendSequence, 3, 4, 7, 8, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* becomeEqual = Gtk::manage(new Gtk::Button());;
	becomeEqual->add_pixlabel(dir_icon+"/iconsB/png/BecomeEqual.png","");
	becomeEqual->set_size_request(width,height);
	becomeEqual->set_border_width(0);
	becomeEqual->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_becomeEqual));
	becomeEqual->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_becomeEqual));
	becomeEqual->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*becomeEqual, 3, 4, 4, 5, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* braces = Gtk::manage(new Gtk::Button());;
	braces->add_pixlabel(dir_icon+"/iconsB/png/Braces.png","");
	braces->set_size_request(width,height);
	braces->set_border_width(0);
	braces->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_braces));
	braces->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_braces));
	braces->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*braces, 3, 4, 14, 15, Gtk::FILL, Gtk::FILL, xx, yy);
	
	Gtk::Button* cartesianProduct = Gtk::manage(new Gtk::Button());;
	cartesianProduct->add_pixlabel(dir_icon+"/iconsB/png/CartesianProduct.png","");
	cartesianProduct->set_size_request(width,height);
	cartesianProduct->set_border_width(0);
	cartesianProduct->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_cartesianProduct));
	cartesianProduct->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_cartesianProduct));
	cartesianProduct->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*cartesianProduct, 2, 3, 1, 2, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* composition = Gtk::manage(new Gtk::Button());;
	composition->add_pixlabel(dir_icon+"/iconsB/png/Composition.png","");
	composition->set_size_request(width,height);
	composition->set_border_width(0);
	composition->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_composition));
	composition->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_composition));
	composition->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*composition, 2, 3, 3, 4, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* concatSequence = Gtk::manage(new Gtk::Button());;
	concatSequence->add_pixlabel(dir_icon+"/iconsB/png/ConcatSequence.png","");
	concatSequence->set_size_request(width,height);
	concatSequence->set_border_width(0);
	concatSequence->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_concatSequence));
	concatSequence->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_concatSequence));
	concatSequence->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*concatSequence, 3, 4, 1, 2, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* curly = Gtk::manage(new Gtk::Button());;
	curly->add_pixlabel(dir_icon+"/iconsB/png/Curly.png","");
	curly->set_size_request(width,height);
	curly->set_border_width(0);
	curly->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_curly));
	curly->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_curly));
	curly->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*curly, 3, 4, 13, 14, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* definitionOperation = Gtk::manage(new Gtk::Button());;
	definitionOperation->add_pixlabel(dir_icon+"/iconsB/png/DefinitionOperation.png","");
	definitionOperation->set_size_request(width,height);
	definitionOperation->set_border_width(0);
	definitionOperation->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_definitionOperation));
	definitionOperation->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_definitionOperation));
	definitionOperation->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*definitionOperation, 3, 4, 6, 7, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* directProduct = Gtk::manage(new Gtk::Button());;
	directProduct->add_pixlabel(dir_icon+"/iconsB/png/DirectProduct.png","");
	directProduct->set_size_request(width,height);
	directProduct->set_border_width(0);
	directProduct->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_directProduct));
	directProduct->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_directProduct));
	directProduct->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*directProduct, 2, 3, 4, 5, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* domainRestriction = Gtk::manage(new Gtk::Button());;
	domainRestriction->add_pixlabel(dir_icon+"/iconsB/png/DomainRestriction.png","");
	domainRestriction->set_size_request(width,height);
	domainRestriction->set_border_width(0);
	domainRestriction->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_domainRestriction));
	domainRestriction->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_domainRestriction));
	domainRestriction->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*domainRestriction, 1, 2, 14, 15, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* domainSubstraction = Gtk::manage(new Gtk::Button());;
	domainSubstraction->add_pixlabel(dir_icon+"/iconsB/png/DomainSubstraction.png","");
	domainSubstraction->set_size_request(width,height);
	domainSubstraction->set_border_width(0);
	domainSubstraction->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_domainSubstraction));
	domainSubstraction->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_domainSubstraction));
	domainSubstraction->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*domainSubstraction, 1, 2, 15, 16, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* emptySet = Gtk::manage(new Gtk::Button());;
	emptySet->add_pixlabel(dir_icon+"/iconsB/png/EmptySet.png","");
	emptySet->set_size_request(width,height);
	emptySet->set_border_width(0);
	emptySet->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_emptySet));
	emptySet->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_emptySet));
	emptySet->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*emptySet, 1, 2, 1, 2, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* equivalence = Gtk::manage(new Gtk::Button());;
	equivalence->add_pixlabel(dir_icon+"/iconsB/png/Equivalence.png","");
	equivalence->set_size_request(width,height);
	equivalence->set_border_width(0);
	equivalence->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_equivalence));
	equivalence->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_equivalence));
	equivalence->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*equivalence, 0, 1, 6, 7, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* exists = Gtk::manage(new Gtk::Button());;
	exists->add_pixlabel(dir_icon+"/iconsB/png/Exists.png","");
	exists->set_size_request(width,height);
	exists->set_border_width(0);
	exists->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_exists));
	exists->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_exists));
	exists->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*exists, 0, 1, 3, 4, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* finitePowerSet = Gtk::manage(new Gtk::Button());;
	finitePowerSet->add_pixlabel(dir_icon+"/iconsB/png/FinitePowerSet.png","");
	finitePowerSet->set_size_request(width,height);
	finitePowerSet->set_border_width(0);
	finitePowerSet->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_finitePowerSet));
	finitePowerSet->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_finitePowerSet));
	finitePowerSet->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*finitePowerSet, 2, 3, 7, 8, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* forall = Gtk::manage(new Gtk::Button());;
	forall->add_pixlabel(dir_icon+"/iconsB/png/Forall.png","");
	forall->set_size_request(width,height);
	forall->set_border_width(0);
	forall->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_forall));
	forall->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_forall));
	forall->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*forall, 0, 1, 4, 5, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* greatherThanOrEqual = Gtk::manage(new Gtk::Button());;
	greatherThanOrEqual->add_pixlabel(dir_icon+"/iconsB/png/GreatherThanOrEqual.png","");
	greatherThanOrEqual->set_size_request(width,height);
	greatherThanOrEqual->set_border_width(0);
	greatherThanOrEqual->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_greatherThanOrEqual));
	greatherThanOrEqual->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_greatherThanOrEqual));
	greatherThanOrEqual->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*greatherThanOrEqual, 3, 4, 12, 13, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* implication = Gtk::manage(new Gtk::Button());;
	implication->add_pixlabel(dir_icon+"/iconsB/png/Implication.png","");
	implication->set_size_request(width,height);
	implication->set_border_width(0);
	implication->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_implication));
	implication->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_implication));
	implication->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*implication, 0, 1, 7, 8, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* intergerSet = Gtk::manage(new Gtk::Button());;
	intergerSet->add_pixlabel(dir_icon+"/iconsB/png/IntegerSet.png","");
	intergerSet->set_size_request(width,height);
	intergerSet->set_border_width(0);
	intergerSet->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_intergerSet));
	intergerSet->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_intergerSet));
	intergerSet->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*intergerSet, 2, 3, 14, 15, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* intersection = Gtk::manage(new Gtk::Button());;
	intersection->add_pixlabel(dir_icon+"/iconsB/png/Intersection.png","");
	intersection->set_size_request(width,height);
	intersection->set_border_width(0);
	intersection->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_intersection));
	intersection->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_intersection));
	intersection->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*intersection, 1, 2, 5, 6, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* interval = Gtk::manage(new Gtk::Button());;
	interval->add_pixlabel(dir_icon+"/iconsB/png/Interval.png","");
	interval->set_size_request(width,height);
	interval->set_border_width(0);
	interval->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_interval));
	interval->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_interval));
	interval->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*interval, 2, 3, 11, 12, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* inverse = Gtk::manage(new Gtk::Button());;
	inverse->add_pixlabel(dir_icon+"/iconsB/png/Inverse.png","");
	inverse->set_size_request(width,height);
	inverse->set_border_width(0);
	inverse->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_inverse));
	inverse->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_inverse));
	inverse->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*inverse, 2, 3, 5, 6, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* in = Gtk::manage(new Gtk::Button());;
	in->add_pixlabel(dir_icon+"/iconsB/png/In.png","");
	in->set_size_request(width,height);
	in->set_border_width(0);
	in->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_in));
	in->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_in));
	in->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*in, 1, 2, 2, 3, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* lambda = Gtk::manage(new Gtk::Button());;
	lambda->add_pixlabel(dir_icon+"/iconsB/png/Lambda.png","");
	lambda->set_size_request(width,height);
	lambda->set_border_width(0);
	lambda->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_lambda));
	lambda->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_lambda));
	lambda->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*lambda, 2, 3, 6, 7, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* lessThanOrEqual = Gtk::manage(new Gtk::Button());;
	lessThanOrEqual->add_pixlabel(dir_icon+"/iconsB/png/LessThanOrEqual.png","");
	lessThanOrEqual->set_size_request(width,height);
	lessThanOrEqual->set_border_width(0);
	lessThanOrEqual->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_lessThanOrEqual));
	lessThanOrEqual->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_lessThanOrEqual));
	lessThanOrEqual->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*lessThanOrEqual, 3, 4, 11, 12, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* maplet = Gtk::manage(new Gtk::Button());;
	maplet->add_pixlabel(dir_icon+"/iconsB/png/Maplet.png","");
	maplet->set_size_request(width,height);
	maplet->set_border_width(0);
	maplet->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_maplet));
	maplet->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_maplet));
	maplet->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*maplet, 0, 1, 5, 6, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* naturalSet = Gtk::manage(new Gtk::Button());;
	naturalSet->add_pixlabel(dir_icon+"/iconsB/png/NaturalSet.png","");
	naturalSet->set_size_request(width,height);
	naturalSet->set_border_width(0);
	naturalSet->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_naturalSet));
	naturalSet->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_naturalSet));
	naturalSet->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*naturalSet, 2, 3, 12, 13, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* notEmptyFinitePowerSet = Gtk::manage(new Gtk::Button());;
	notEmptyFinitePowerSet->add_pixlabel(dir_icon+"/iconsB/png/NotEmptyFinitePowerSet.png","");
	notEmptyFinitePowerSet->set_size_request(width,height);
	notEmptyFinitePowerSet->set_border_width(0);
	notEmptyFinitePowerSet->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_notEmptyFinitePowerSet));
	notEmptyFinitePowerSet->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_notEmptyFinitePowerSet));
	notEmptyFinitePowerSet->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*notEmptyFinitePowerSet, 2, 3, 8, 9, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* notEmptyNatural = Gtk::manage(new Gtk::Button());;
	notEmptyNatural->add_pixlabel(dir_icon+"/iconsB/png/NotEmptyNatural.png","");
	notEmptyNatural->set_size_request(width,height);
	notEmptyNatural->set_border_width(0);
	notEmptyNatural->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_notEmptyNatural));
	notEmptyNatural->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_notEmptyNatural));
	notEmptyNatural->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*notEmptyNatural, 2, 3, 13, 14, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* notEmptySubset = Gtk::manage(new Gtk::Button());;
	notEmptySubset->add_pixlabel(dir_icon+"/iconsB/png/NotEmptySubset.png","");
	notEmptySubset->set_size_request(width,height);
	notEmptySubset->set_border_width(0);
	notEmptySubset->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_notEmptySubset));
	notEmptySubset->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_notEmptySubset));
	notEmptySubset->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*notEmptySubset, 2, 3, 10, 11, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* notEqual = Gtk::manage(new Gtk::Button());;
	notEqual->add_pixlabel(dir_icon+"/iconsB/png/NotEqual.png","");
	notEqual->set_size_request(width,height);
	notEqual->set_border_width(0);
	notEqual->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_notEqual));
	notEqual->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_notEqual));
	notEqual->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*notEqual, 3, 4, 10, 11, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* notIn = Gtk::manage(new Gtk::Button());;
	notIn->add_pixlabel(dir_icon+"/iconsB/png/NotIn.png","");
	notIn->set_size_request(width,height);
	notIn->set_border_width(0);
	notIn->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_notIn));
	notIn->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_notIn));
	notIn->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*notIn, 1, 2, 3, 4, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* notSubset = Gtk::manage(new Gtk::Button());;
	notSubset->add_pixlabel(dir_icon+"/iconsB/png/NotSubset.png","");
	notSubset->set_size_request(width,height);
	notSubset->set_border_width(0);
	notSubset->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_notSubset));
	notSubset->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_notSubset));
	notSubset->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*notSubset, 1, 2, 9, 10, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* notStrictSubset = Gtk::manage(new Gtk::Button());;
	notStrictSubset->add_pixlabel(dir_icon+"/iconsB/png/NotStrictSubset.png","");
	notStrictSubset->set_size_request(width,height);
	notStrictSubset->set_border_width(0);
	notStrictSubset->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_notStrictSubset));
	notStrictSubset->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_notStrictSubset));
	notStrictSubset->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*notStrictSubset, 1, 2, 8, 9, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* bnot = Gtk::manage(new Gtk::Button());;
	bnot->add_pixlabel(dir_icon+"/iconsB/png/Not.png","");
	bnot->set_size_request(width,height);
	bnot->set_border_width(0);
	bnot->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_not));
	bnot->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_not));
	bnot->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*bnot, 0, 1, 2, 3, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* bor = Gtk::manage(new Gtk::Button());;
	bor->add_pixlabel(dir_icon+"/iconsB/png/Or.png","");
	bor->set_size_request(width,height);
	bor->set_border_width(0);
	bor->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_or));
	bor->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_or));
	bor->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*bor, 0, 1, 1, 2, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* override = Gtk::manage(new Gtk::Button());;
	override->add_pixlabel(dir_icon+"/iconsB/png/Override.png","");
	override->set_size_request(width,height);
	override->set_border_width(0);
	override->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_override));
	override->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_override));
	override->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*override, 2, 3, 0, 1, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* parallelProduct = Gtk::manage(new Gtk::Button());;
	parallelProduct->add_pixlabel(dir_icon+"/iconsB/png/ParallelProduct.png","");
	parallelProduct->set_size_request(width,height);
	parallelProduct->set_border_width(0);
	parallelProduct->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_parallelProduct));
	parallelProduct->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_parallelProduct));
	parallelProduct->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*parallelProduct, 2, 3, 2, 3, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* parenthesis = Gtk::manage(new Gtk::Button());;
	parenthesis->add_pixlabel(dir_icon+"/iconsB/png/Parenthesis.png","");
	parenthesis->set_size_request(width,height);
	parenthesis->set_border_width(0);
	parenthesis->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_parenthesis));
	parenthesis->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_parenthesis));
	parenthesis->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*parenthesis, 3, 4, 15, 16, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* partialBijection = Gtk::manage(new Gtk::Button());;
	partialBijection->add_pixlabel(dir_icon+"/iconsB/png/PartialBijection.png","");
	partialBijection->set_size_request(width,height);
	partialBijection->set_border_width(0);
	partialBijection->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_partialBijection));
	partialBijection->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_partialBijection));
	partialBijection->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*partialBijection, 0, 1, 15, 16, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* partialFunction = Gtk::manage(new Gtk::Button());;
	partialFunction->add_pixlabel(dir_icon+"/iconsB/png/PartialFunction.png","");
	partialFunction->set_size_request(width,height);
	partialFunction->set_border_width(0);
	partialFunction->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_partialFunction));
	partialFunction->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_partialFunction));
	partialFunction->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*partialFunction, 0, 1, 9, 10, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* partialInjection = Gtk::manage(new Gtk::Button());;
	partialInjection->add_pixlabel(dir_icon+"/iconsB/png/PartialInjection.png","");
	partialInjection->set_size_request(width,height);
	partialInjection->set_border_width(0);
	partialInjection->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_partialInjection));
	partialInjection->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_partialInjection));
	partialInjection->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*partialInjection, 0, 1, 11, 12, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* partialSurjection = Gtk::manage(new Gtk::Button());;
	partialSurjection->add_pixlabel(dir_icon+"/iconsB/png/PartialSurjection.png","");
	partialSurjection->set_size_request(width,height);
	partialSurjection->set_border_width(0);
	partialSurjection->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_partialSurjection));
	partialSurjection->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_partialSurjection));
	partialSurjection->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*partialSurjection, 0, 1, 13, 14, Gtk::FILL, Gtk::FILL, xx, yy);
	
	Gtk::Button* powerSet = Gtk::manage(new Gtk::Button());;
	powerSet->add_pixlabel(dir_icon+"/iconsB/png/PowerSet.png","");
	powerSet->set_size_request(width,height);
	powerSet->set_border_width(0);
	powerSet->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_powerSet));
	powerSet->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_powerSet));
	powerSet->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*powerSet, 2, 3, 9, 10, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* prefixSequence = Gtk::manage(new Gtk::Button());;
	prefixSequence->add_pixlabel(dir_icon+"/iconsB/png/PrefixSequence.png","");
	prefixSequence->set_size_request(width,height);
	prefixSequence->set_border_width(0);
	prefixSequence->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_prefixSequence));
	prefixSequence->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_prefixSequence));
	prefixSequence->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*prefixSequence, 3, 4, 2, 3, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* prependSequence = Gtk::manage(new Gtk::Button());;
	prependSequence->add_pixlabel(dir_icon+"/iconsB/png/PrependSequence.png","");
	prependSequence->set_size_request(width,height);
	prependSequence->set_border_width(0);
	prependSequence->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_prependSequence));
	prependSequence->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_prependSequence));
	prependSequence->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*prependSequence, 3, 4, 0, 1, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* product = Gtk::manage(new Gtk::Button());;
	product->add_pixlabel(dir_icon+"/iconsB/png/Product.png","");
	product->set_size_request(width,height);
	product->set_border_width(0);
	product->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_product));
	product->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_product));
	product->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*product, 3, 4, 8, 9, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* quantifiedIntersection = Gtk::manage(new Gtk::Button());;
	quantifiedIntersection->add_pixlabel(dir_icon+"/iconsB/png/QuantifiedIntersection.png","");
	quantifiedIntersection->set_size_request(width,height);
	quantifiedIntersection->set_border_width(0);
	quantifiedIntersection->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_quantifiedIntersection));
	quantifiedIntersection->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_quantifiedIntersection));
	quantifiedIntersection->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*quantifiedIntersection, 1, 2, 11, 12, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* quantifiedUnion = Gtk::manage(new Gtk::Button());;
	quantifiedUnion->add_pixlabel(dir_icon+"/iconsB/png/QuantifiedUnion.png","");
	quantifiedUnion->set_size_request(width,height);
	quantifiedUnion->set_border_width(0);
	quantifiedUnion->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_quantifiedUnion));
	quantifiedUnion->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_quantifiedUnion));
	quantifiedUnion->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*quantifiedUnion, 1, 2, 10, 11, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* rangeRestriction = Gtk::manage(new Gtk::Button());;
	rangeRestriction->add_pixlabel(dir_icon+"/iconsB/png/RangeRestriction.png","");
	rangeRestriction->set_size_request(width,height);
	rangeRestriction->set_border_width(0);
	rangeRestriction->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_rangeRestriction));
	rangeRestriction->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_rangeRestriction));
	rangeRestriction->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*rangeRestriction, 1, 2, 12, 13, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* rangeSubstraction = Gtk::manage(new Gtk::Button());;
	rangeSubstraction->add_pixlabel(dir_icon+"/iconsB/png/RangeSubstraction.png","");
	rangeSubstraction->set_size_request(width,height);
	rangeSubstraction->set_border_width(0);
	rangeSubstraction->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_rangeSubstraction));
	rangeSubstraction->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_rangeSubstraction));
	rangeSubstraction->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*rangeSubstraction, 1, 2, 13, 14, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* relation = Gtk::manage(new Gtk::Button());;
	relation->add_pixlabel(dir_icon+"/iconsB/png/Relation.png","");
	relation->set_size_request(width,height);
	relation->set_border_width(0);
	relation->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_relation));
	relation->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_relation));
	relation->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*relation, 0, 1, 8, 9, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* resultOperation = Gtk::manage(new Gtk::Button());;
	resultOperation->add_pixlabel(dir_icon+"/iconsB/png/ResultOperation.png","");
	resultOperation->set_size_request(width,height);
	resultOperation->set_border_width(0);
	resultOperation->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_resultOperation));
	resultOperation->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_resultOperation));
	resultOperation->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*resultOperation, 2, 3, 15, 16, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* setIn = Gtk::manage(new Gtk::Button());;
	setIn->add_pixlabel(dir_icon+"/iconsB/png/SetIn.png","");
	setIn->set_size_request(width,height);
	setIn->set_border_width(0);
	setIn->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_setIn));
	setIn->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_setIn));
	setIn->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*setIn, 3, 4, 5, 6, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* sigma = Gtk::manage(new Gtk::Button());;
	sigma->add_pixlabel(dir_icon+"/iconsB/png/Sigma.png","");
	sigma->set_size_request(width,height);
	sigma->set_border_width(0);
	sigma->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_sigma));
	sigma->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_sigma));
	sigma->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*sigma, 3, 4, 9, 10, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* strictSubset = Gtk::manage(new Gtk::Button());;
	strictSubset->add_pixlabel(dir_icon+"/iconsB/png/StrictSubset.png","");
	strictSubset->set_size_request(width,height);
	strictSubset->set_border_width(0);
	strictSubset->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_strictSubset));
	strictSubset->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_strictSubset));
	strictSubset->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*strictSubset, 1, 2, 6, 7, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* subSet = Gtk::manage(new Gtk::Button());;
	subSet->add_pixlabel(dir_icon+"/iconsB/png/Subset.png","");
	subSet->set_size_request(width,height);
	subSet->set_border_width(0);
	subSet->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_subset));
	subSet->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_subset));
	subSet->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*subSet, 1, 2, 7, 8, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* suffixSequence = Gtk::manage(new Gtk::Button());;
	suffixSequence->add_pixlabel(dir_icon+"/iconsB/png/SuffixSequence.png","");
	suffixSequence->set_size_request(width,height);
	suffixSequence->set_border_width(0);
	suffixSequence->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_suffixSequence));
	suffixSequence->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_suffixSequence));
	suffixSequence->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*suffixSequence, 3, 4, 3, 4, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* totalBijection = Gtk::manage(new Gtk::Button());;
	totalBijection->add_pixlabel(dir_icon+"/iconsB/png/TotalBijection.png","");
	totalBijection->set_size_request(width,height);
	totalBijection->set_border_width(0);
	totalBijection->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_totalBijection));
	totalBijection->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_totalBijection));
	totalBijection->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*totalBijection, 1, 2, 0, 1, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* totalFunction = Gtk::manage(new Gtk::Button());;
	totalFunction->add_pixlabel(dir_icon+"/iconsB/png/TotalFunction.png","");
	totalFunction->set_size_request(width,height);
	totalFunction->set_border_width(0);
	totalFunction->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_totalFunction));
	totalFunction->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_totalFunction));
	totalFunction->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*totalFunction, 0, 1, 10, 11, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* totalInjection = Gtk::manage(new Gtk::Button());;
	totalInjection->add_pixlabel(dir_icon+"/iconsB/png/TotalInjection.png","");
	totalInjection->set_size_request(width,height);
	totalInjection->set_border_width(0);
	totalInjection->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_totalInjection));
	totalInjection->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_totalInjection));
	totalInjection->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*totalInjection, 0, 1, 12, 13, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* totalSurjection = Gtk::manage(new Gtk::Button());;
	totalSurjection->add_pixlabel(dir_icon+"/iconsB/png/TotalSurjection.png","");
	totalSurjection->set_size_request(width,height);
	totalSurjection->set_border_width(0);
	totalSurjection->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_totalSurjection));
	totalSurjection->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_totalSurjection));
	totalSurjection->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*totalSurjection, 0, 1, 14, 15, Gtk::FILL, Gtk::FILL, xx, yy);

	Gtk::Button* bunion = Gtk::manage(new Gtk::Button());;
	bunion->add_pixlabel(dir_icon+"/iconsB/png/Union.png","");
	bunion->set_size_request(width,height);
	bunion->set_border_width(0);
	bunion->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_symbole_union));
	bunion->signal_enter().connect( sigc::mem_fun(*this, &EditeurB::in_symbole_union));
	bunion->signal_leave().connect( sigc::mem_fun(*this, &EditeurB::out_symbole));
	m_Layout->attach(*bunion, 1, 2, 4, 5, Gtk::FILL, Gtk::FILL, xx, yy);



	m_ButtonFrame.add(m_bFrame);

	m_bFermer = Gtk::manage(new Gtk::Button("Fermer",true));
	//m_bFermer->set_use_underline(true);
	//m_bFermer->set_label("Fermer");
	m_bFermerTT = Gtk::manage(new Gtk::Button());
	m_bFermerTT->set_label("Fermer tout");
	m_bFermer->signal_clicked().connect(sigc::mem_fun(*this, &EditeurB::on_fermer));
	m_bFermerTT->signal_clicked().connect( sigc::mem_fun(*this, &EditeurB::on_fermerTT));
	m_bFrame.pack_start(*m_bFermer,Gtk::PACK_EXPAND_PADDING);
	//m_bFrame.pack_start(*m_bFermerTT,Gtk::PACK_EXPAND_PADDING);

	/** Fenetre d'editeur de texte **/

	_notebookB = Gtk::manage(new NotebookB());
	m_TextFrame.add(*_notebookB);


	/** Fenetre d'affichage des infos sur les symboles **/
	m_VBox.pack_start(m_CelluleInfo, Gtk::PACK_SHRINK);
	m_bufferCelluleInfo = Gtk::TextBuffer::create();
	m_CelluleInfo.set_buffer(m_bufferCelluleInfo);
	m_bufferCelluleInfo->set_text("");
	int w = m_CelluleInfo.get_width ();
	m_CelluleInfo.set_size_request(w,70);

	show_all();
}


EditeurB::~EditeurB() {
}

void EditeurB::setText(char* contenu) {
	_notebookB->setText(contenu);
}

Glib::ustring EditeurB::getText() {
	return _notebookB->getText();
}

void EditeurB::setFilename(const std::string& filename) {
	_notebookB->setFilename(filename);
}

std::string EditeurB::getFilename() {
	return _notebookB->getFilename();
}

void EditeurB::ajouterFeuillet() {
	_notebookB->ajouterFeuillet();
}

void EditeurB::insererSymbole(const char * symbole) {
	_notebookB->insererSymbole(symbole);
}

void EditeurB::afficherDescriptionSymbole(const char * infos) {
	m_bufferCelluleInfo->set_text(infos);
}

void EditeurB::setPageSauvegardee() {
	_notebookB->setPageSauvegardee(1);
}

void EditeurB::paste_clipboard(Glib::RefPtr<Gtk::Clipboard> clipboard) {
	_notebookB->paste_clipboard(clipboard);
}

void EditeurB::cut_clipboard(Glib::RefPtr<Gtk::Clipboard> clipboard) {
	_notebookB->cut_clipboard(clipboard);
}

void EditeurB::copy_clipboard(Glib::RefPtr<Gtk::Clipboard> clipboard) {
	_notebookB->copy_clipboard(clipboard);
}

/** Fonctions liees au signal des boutons **/

void EditeurB::on_symbole_and() {
	insererSymbole("&");
}

void EditeurB::on_symbole_appendSequence() {
	insererSymbole("<-");
}

void EditeurB::on_symbole_becomeEqual() {
	insererSymbole(":=");
}

void EditeurB::on_symbole_braces() {
	insererSymbole("{}");
}

void EditeurB::on_symbole_cartesianProduct() {
	insererSymbole("*");
}

void EditeurB::on_symbole_composition() {
	insererSymbole(";");
}

void EditeurB::on_symbole_concatSequence() {
	insererSymbole("^");
}

void EditeurB::on_symbole_curly() {
	insererSymbole("[]");
}

void EditeurB::on_symbole_definitionOperation() {
	insererSymbole("=");
}

void EditeurB::on_symbole_directProduct() {
	insererSymbole("><");
}

void EditeurB::on_symbole_domainRestriction() {
	insererSymbole("<|");
}

void EditeurB::on_symbole_domainSubstraction() {
	insererSymbole("<<|");
}

void EditeurB::on_symbole_emptySet() {
	insererSymbole("{}");
}

void EditeurB::on_symbole_equivalence() {
	insererSymbole("<=>");
}

void EditeurB::on_symbole_exists() {
	insererSymbole("#().()");
}

void EditeurB::on_symbole_finitePowerSet() {
	insererSymbole("FIN()");
}

void EditeurB::on_symbole_forall() {
	insererSymbole("!().()");
}

void EditeurB::on_symbole_greatherThanOrEqual() {
	insererSymbole(">=");
}

void EditeurB::on_symbole_implication() {
	insererSymbole("=>");
}

void EditeurB::on_symbole_intergerSet() {
	insererSymbole("INTEGER");
}

void EditeurB::on_symbole_interval() {
	insererSymbole("..");
}

void EditeurB::on_symbole_intersection() {
	insererSymbole("/\\");
}

void EditeurB::on_symbole_inverse() {
	insererSymbole("~");
}

void EditeurB::on_symbole_in() {
	insererSymbole(":");
}

void EditeurB::on_symbole_lambda() {
	insererSymbole("%().(|)");
}

void EditeurB::on_symbole_lessThanOrEqual() {
	insererSymbole("<=");
}

void EditeurB::on_symbole_maplet() {
	insererSymbole("|->");
}

void EditeurB::on_symbole_naturalSet() {
	insererSymbole("NATURAL");
}


void EditeurB::on_symbole_notEmptyFinitePowerSet() {
	insererSymbole("FIN1()");
}

void EditeurB::on_symbole_notEmptyNatural() {
	insererSymbole("NATURAL1");
}

void EditeurB::on_symbole_notEmptySubset() {
	insererSymbole("POW1()");
}

void EditeurB::on_symbole_notEqual() {
	insererSymbole("/=");
}

void EditeurB::on_symbole_notIn() {
	insererSymbole("/:");
}

void EditeurB::on_symbole_notSubset() {
	insererSymbole("/<<:");
}

void EditeurB::on_symbole_notStrictSubset() {
	insererSymbole("/<:");
}

void EditeurB::on_symbole_not() {
	insererSymbole("not()");
}

void EditeurB::on_symbole_or() {
	insererSymbole("or");
}

void EditeurB::on_symbole_override() {
	insererSymbole("<+");
}

void EditeurB::on_symbole_parallelProduct() {
	insererSymbole("||");
}

void EditeurB::on_symbole_parenthesis() {
	insererSymbole("()");
}

void EditeurB::on_symbole_partialBijection() {
	insererSymbole(">+>>");
}

void EditeurB::on_symbole_partialInjection() {
	insererSymbole(">+>");
}

void EditeurB::on_symbole_partialFunction() {
	insererSymbole("+->");
}

void EditeurB::on_symbole_partialSurjection() {
	insererSymbole("+->>");
}

void EditeurB::on_symbole_powerSet() {
	insererSymbole("POW()");
}

void EditeurB::on_symbole_prefixSequence() {
	insererSymbole("/|\\");
}

void EditeurB::on_symbole_prependSequence() {
	insererSymbole("->");
}

void EditeurB::on_symbole_product() {
	insererSymbole("PI().(|)");
}

void EditeurB::on_symbole_quantifiedIntersection() {
	insererSymbole("INTER().(|)");
}

void EditeurB::on_symbole_quantifiedUnion() {
	insererSymbole("UNION().(|)");
}

void EditeurB::on_symbole_rangeRestriction() {
	insererSymbole("|>");
}

void EditeurB::on_symbole_rangeSubstraction() {
	insererSymbole("|>>");
}

void EditeurB::on_symbole_relation() {
	insererSymbole("<->");
}

void EditeurB::on_symbole_resultOperation() {
	insererSymbole("<--");
}

void EditeurB::on_symbole_setIn() {
	insererSymbole("::");
}

void EditeurB::on_symbole_sigma() {
	insererSymbole("SIGMA().(|)");
}

void EditeurB::on_symbole_strictSubset() {
	insererSymbole("<:");
}

void EditeurB::on_symbole_subset() {
	insererSymbole("<<:");
}

void EditeurB::on_symbole_suffixSequence() {
	insererSymbole("\\|/");
}

void EditeurB::on_symbole_totalBijection() {
	insererSymbole(">->>");
}

void EditeurB::on_symbole_totalFunction() {
	insererSymbole("-->");
}

void EditeurB::on_symbole_totalInjection() {
	insererSymbole(">->");
}

void EditeurB::on_symbole_totalSurjection() {
	insererSymbole("-->>");
}

void EditeurB::on_symbole_union() {
	insererSymbole("\\/");
}

void EditeurB::on_fermer() {
	if (_notebookB->getPageSauvegardee() != 0) {
		_notebookB->fermerFeuillet();
	} else {
		Gtk::MessageDialog dialog("Le texte n'a pas ete sauvegarde.\n Fermer tout de meme?", false, Gtk::MESSAGE_QUESTION, Gtk::BUTTONS_YES_NO, true);
		int result = dialog.run();
		if (result == Gtk::RESPONSE_YES)
			_notebookB->fermerFeuillet();
	}
}

void EditeurB::on_fermerTT() {
	//_notebookB->fermerTsFeuillet();
}


/** Fonctions d'entree sur un bouton **/

void EditeurB::in_symbole_and() {
	afficherDescriptionSymbole("Conjonction | Conjunction");
}

void EditeurB::in_symbole_appendSequence() {
	afficherDescriptionSymbole("Insertion d'un element en tete d'une sequence \n Insert an element to the beginning of a sequence\n Lune -> <Phobos, Deimos, Titan, Ganymede> = <Lune, Phobos, Deimos, Titan, Ganymede>");
}

void EditeurB::in_symbole_becomeEqual() {
	afficherDescriptionSymbole("Substitution <<Devient egal>> | <<Assignment>>");
}

void EditeurB::in_symbole_braces() {
	afficherDescriptionSymbole("Begin or end set");
}

void EditeurB::in_symbole_cartesianProduct() {
	afficherDescriptionSymbole("Produit cartesien | Cartesian product \n {Lune, Phobos, Deimos} * {Terre, Mars} = {(Lune|->Terre),(Lune|->Mars),(Phobos|->Terre),\n(Phobos|->Mars),(Deimos|->Terre),(Deimos|->Mars)}");
}

void EditeurB::in_symbole_composition() {
	afficherDescriptionSymbole("Substitutions en sequence ou composition de deux relations Sequential\n substitutions or composition of two relations\n R1 = {(0|->1),(0|->2),(1|->4),(2|->3)}, R2 = {(1|->2),(2|->5),(3|->6)}\n R1 ; R2 = {(0|->2),(0|->5)),(2|->6)}");
}

void EditeurB::in_symbole_concatSequence() {
	afficherDescriptionSymbole("Ajout d'un element en queue d'une sequence \n Insert an element to the end of a sequence\n <Lune, Phobos, Deimos, Titan> <- Ganymede = <Lune, Phobos, Deimos, Titan, Ganymede>");
}

void EditeurB::in_symbole_curly() {
	afficherDescriptionSymbole("Begin or end sequence");
}

void EditeurB::in_symbole_definitionOperation() {
	afficherDescriptionSymbole("Operation Declaration or equal");
}

void EditeurB::in_symbole_directProduct() {
	afficherDescriptionSymbole("Produit direct de deux relations | Direct product of two relations\n R1 = {(0|->1),(1|->2),(2|->3)}\n R2 = {(1|->2),(2|->4),(3|->6)}\n R1 >< R2 = {(1|->(2|->2)),(2|->(3|->4))}");
}

void EditeurB::in_symbole_domainRestriction() {
	afficherDescriptionSymbole("Restriction sur le domaine | Domain restriction of a relation by a set\n E= {Lune,Phobos,Deimos}, F = {Terre,Venus,Saturne}\n R = {(Lune|->Terre),(Phobos|->Mars),(Deimos|->Mars),(Titan|->Saturne)}\n E <| R = {(Lune|->Terre),(Phobos|->Mars),(Deimos|->Mars)}");
}

void EditeurB::in_symbole_domainSubstraction() {
	afficherDescriptionSymbole("Soustraction sur le domaine | domain subtraction of a relation by a set\n E= {Lune,Phobos,Deimos}, F = {Terre,Venus,Saturne}\n R = {(Lune|->Terre),(Phobos|->Mars),(Deimos|->Mars),(Titan|->Saturne)}\n E <<| R = {(Titan|->Saturne)}");
}

void EditeurB::in_symbole_emptySet() {
	afficherDescriptionSymbole("Ensemble vide | Empty set");
}

void EditeurB::in_symbole_equivalence() {
	afficherDescriptionSymbole("Equivalence | Equivalence");
}

void EditeurB::in_symbole_exists() {
	afficherDescriptionSymbole("Quantification existencielle | Existential quantification");
}

void EditeurB::in_symbole_finitePowerSet() {
	afficherDescriptionSymbole("Ensemble des sous-ensembles finis | Finite powerset\n FIN({Lune,Phobos,Deimos}) = {{}, {Lune}, {Phobos}, {Deimos}, {Lune, Phobos},\n{Lune, Deimos}, {Phobos, Deimos}, {Lune, Phobos, Deimos}}");
}

void EditeurB::in_symbole_forall() {
	afficherDescriptionSymbole("Quantification universelle | Universal quantification");
}

void EditeurB::in_symbole_greatherThanOrEqual() {
	afficherDescriptionSymbole("Superieur ou egal | greater or equal");
}

void EditeurB::in_symbole_implication() {
	afficherDescriptionSymbole("Implication | Implication");
}

void EditeurB::in_symbole_intergerSet() {
	afficherDescriptionSymbole("Ensemble des entiers relatifs | Set of integers");
}

void EditeurB::in_symbole_interval() {
	afficherDescriptionSymbole("Intervalle | Interval of integers\n 10..15 = {10,11,12,13,14,15}");
}

void EditeurB::in_symbole_intersection() {
	afficherDescriptionSymbole("Intersection de deux ensembles | Intersection of two sets\n S1 = {Lune, Phobos, Deimos}\n S2 = {Lune, Ganymede}\n S1 /\\ S2 = {Lune}");
}

void EditeurB::in_symbole_inverse() {
	afficherDescriptionSymbole("Inverse d'une relation | Inverse of a relation\n R = {(Lune|->Terre),(Phobos|->Mars),(Deimos|->Mars),(Titan|->Saturne)}\n R~ = {(Terre|->Lune),(Mars|->Phobos),(Mars|->Deimos),(Saturne|->Titan)}");
}

void EditeurB::in_symbole_in() {
	afficherDescriptionSymbole("Appartenance  | Membership");
}

void EditeurB::in_symbole_lambda() {
	afficherDescriptionSymbole("Lambda expression | Lambda abstraction\n increment = %(x).(x : NAT | x+1)\n increment(2) = 3");
}

void EditeurB::in_symbole_lessThanOrEqual() {
	afficherDescriptionSymbole("Inferieur ou egal | less or equal");
}

void EditeurB::in_symbole_maplet() {
	afficherDescriptionSymbole("<B>Expression de couple</B><I> |</I> Ordered pair\n (Lune|->Terre)\n ((Lune|->Terre)|->Soleil)");
}

void EditeurB::in_symbole_naturalSet() {
	afficherDescriptionSymbole("Ensemble des entiers naturels | Natural numbers");
}


void EditeurB::in_symbole_notEmptyFinitePowerSet() {
	afficherDescriptionSymbole("Ensemble des sous-ensembles finis non vides | Non empty finite powerset\n FIN1({Lune,Phobos,Deimos}) = {{Lune}, {Phobos}, {Deimos}, {Lune, Phobos},\n{Lune, Deimos}, {Phobos, Deimos}, {Lune, Phobos, Deimos}}");
}

void EditeurB::in_symbole_notEmptyNatural() {
	afficherDescriptionSymbole("Ensemble des entiers naturels non vides | Positive natural numbers");
}

void EditeurB::in_symbole_notEmptySubset() {
	afficherDescriptionSymbole("Ensemble des sous ensembles non vides | Non empty powerset\n POW1({Lune,Phobos,Deimos}) = {{Lune}, {Phobos}, {Deimos}, {Lune, Phobos},\n                               {Lune, Deimos}, {Phobos, Deimos}, {Lune, Phobos, Deimos}}");
}

void EditeurB::in_symbole_notEqual() {
	afficherDescriptionSymbole("Inegalite  | inequality");
}

void EditeurB::in_symbole_notIn() {
	afficherDescriptionSymbole("Non appartenance | Non-membership");
}

void EditeurB::in_symbole_notSubset() {
	afficherDescriptionSymbole("Non inclusion | Not a proper subset");
}

void EditeurB::in_symbole_notStrictSubset() {
	afficherDescriptionSymbole("Non inclusion, non egalite | Not a subset");
}

void EditeurB::in_symbole_not() {
	afficherDescriptionSymbole("Negation | Negation");
}

void EditeurB::in_symbole_or() {
	afficherDescriptionSymbole("Disjonction | disjunction");
}

void EditeurB::in_symbole_override() {
	afficherDescriptionSymbole("Surcharge de relations | Overriding of a relation by another\n R1 = {(Lune|->Terre),(Deimos|->Mars),(Titan|->Saturne)}\n R2 = {(Moon|->Terre),(Ganymede|->Jupiter)}\n R1 <+ R2 = {(Moon|->Terre),(Deimos|->Mars),(Titan|->Saturne),(Ganymede|->Jupiter)}");
}

void EditeurB::in_symbole_parallelProduct() {
	afficherDescriptionSymbole("Substitutions simultanees ou produit parallele de relations\n Parallel substitutions or parallel product of two relations\n R1 = {(0|->1),(1|->8)}, R2 = {(10|->12)}\n R1 || R2 = {((0|->10)|->(1|->12)),((1|->10)|->(8|->12))}");
}

void EditeurB::in_symbole_parenthesis() {
	afficherDescriptionSymbole("Open parenthesis");
}

void EditeurB::in_symbole_partialBijection() {
	afficherDescriptionSymbole("Ensemble des bijections partielles | Set of partial bijections\n if R = {(0|->1),(1|->2),(2|->3)\n then R is a member of {0,1,2,3} >+>> {1,2,3}");
}

void EditeurB::in_symbole_partialInjection() {
	afficherDescriptionSymbole("Ensemble des injections partielles | Set of partial injections\n if R = {(0|->1),(1|->2),(2|->3)\n then R is a member of {0,1,2,3} >+> {0,1,2,3}");
}

void EditeurB::in_symbole_partialFunction() {
	afficherDescriptionSymbole("Ensemble des fonctions partielles | Set of partial functions\n if R = {(0|->1),(1|->2),(2|->2)\n then R is a member of {0,1,2,3} +-> {0,1,2}");
}

void EditeurB::in_symbole_partialSurjection() {
	afficherDescriptionSymbole("Ensemble des surjections partielles | Set of partial surjections\n if R = {(0|->1),(1|->2),(2|->2)\n then R is a member of {0,1,2,3} +->> {1,2}");
}

void EditeurB::in_symbole_powerSet() {
	afficherDescriptionSymbole("Ensemble des sous ensembles | Powerset\n POW({Lune,Phobos,Deimos}) = {{}, {Lune}, {Phobos}, {Deimos}, {Lune, Phobos}, \n                               {Lune, Deimos}, {Phobos, Deimos}, {Lune, Phobos, Deimos}}");
}

void EditeurB::in_symbole_prefixSequence() {
	afficherDescriptionSymbole("Conserver les N premiers elements d'une sequence \n Keeping the N first elements of a sequence\n <Lune, Phobos, Deimos, Titan, Ganymede> /|\\ 3 = <Lune, Phobos, Deimos>");
}

void EditeurB::in_symbole_prependSequence() {
	afficherDescriptionSymbole("Ajout d'un element en queue d'une sequence \n Insert an element to the end of a sequence\n <Lune, Phobos, Deimos, Titan> <- Ganymede = <Lune, Phobos, Deimos, Titan, Ganymede>");
}

void EditeurB::in_symbole_product() {
	afficherDescriptionSymbole("Produit d'expressions entieres | Product of integer expressions\n PI(i).(i : 1..2 | i+1) = (1+1)*(2+1) = 6");
}

void EditeurB::in_symbole_quantifiedIntersection() {
	afficherDescriptionSymbole("Intersection quantifiee | Quantified intersection\n INTER(x).(x : 1..2 | {x, x+1}) = {1, 2} /\\ {2, 3} = {2}");
}

void EditeurB::in_symbole_quantifiedUnion() {
	afficherDescriptionSymbole("Union quantifiee | Quantified union\n UNION(x).(x : 1..2 | {x, x+1}) = {1, 2} \\/ {2, 3} = {1, 2, 3}");
}

void EditeurB::in_symbole_rangeRestriction() {
	afficherDescriptionSymbole("Restriction sur le codomaine | Range restriction of a relation by a set\n E= {Lune,Phobos,Deimos}, F = {Terre,Venus,Saturne}\n R = {(Lune|->Terre),(Phobos|->Mars),(Deimos|->Mars),(Titan|->Saturne)}\n R |> F = {(Lune|->Terre),(Titan|->Saturne)}");
}

void EditeurB::in_symbole_rangeSubstraction() {
	afficherDescriptionSymbole("Soustraction sur le codomaine | range subtraction of a relation by a set\n E= {Lune,Phobos,Deimos}, F = {Terre,Venus,Saturne}\n R = {(Lune|->Terre),(Phobos|->Mars),(Deimos|->Mars),(Titan|->Saturne)}\n R |>> F = {(Phobos|->Mars),(Deimos|->Mars)}");
}

void EditeurB::in_symbole_relation() {
	afficherDescriptionSymbole("Ensemble des relations | Set of relations\n R = {(Lune|->Terre),(Phobos|->Mars),(Deimos|->Mars),(Titan|->Saturne)}\n R is a member of {Lune, Phobos, Deimos, Titan} <-> {Terre,Mars,Venus,Saturne");
}

void EditeurB::in_symbole_resultOperation() {
	afficherDescriptionSymbole("Result operation");
}

void EditeurB::in_symbole_setIn() {
	afficherDescriptionSymbole("Substitution <<Devient element de>> |  <<Becomes a member of>>");
}

void EditeurB::in_symbole_sigma() {
	afficherDescriptionSymbole("Somme d'expressions entieres | Sum of integers expressions\n SIGMA(i).(i : 1..2 | i+1) = (1+1)+(2+1) = 5");
}

void EditeurB::in_symbole_strictSubset() {
	afficherDescriptionSymbole("Inclusion ou egalite  | Subset");
}

void EditeurB::in_symbole_subset() {
	afficherDescriptionSymbole("Inclusion | Proper subset");
}

void EditeurB::in_symbole_suffixSequence() {
	afficherDescriptionSymbole("Eliminer les N premiers elements d'une sequence \n Removing the N first elements of a sequence\n <Lune, Phobos, Deimos, Titan, Ganymede> \\|/ 3 = <Titan, Ganymede>");
}

void EditeurB::in_symbole_totalBijection() {
	afficherDescriptionSymbole("Ensemble des bijections totales | Set of total bijections\n if R = {(0|->1),(1|->2),(2|->3)\n then R is a member of {0,1,2} >->> {1,2,3}");
}

void EditeurB::in_symbole_totalFunction() {
	afficherDescriptionSymbole("Ensemble des fonctions totales  | Set of total functions\n if R = {(0|->1),(1|->2),(2|->2)\n then R is a member of {0,1,2} --> {0,1,2}");
}

void EditeurB::in_symbole_totalInjection() {
	afficherDescriptionSymbole("Ensemble des injections totales  | Set of total injections\n if R = {(0|->1),(1|->2),(2|->3)\n then R is a member of {0,1,2} >-> {0,1,2,3}");
}

void EditeurB::in_symbole_totalSurjection() {
	afficherDescriptionSymbole("Ensemble des surjections totales | Set of total surjections\n if R = {(0|->1),(1|->2),(2|->2)\n then R is a member of {0,1,2} -->> {1,2}");
}

void EditeurB::in_symbole_union() {
	afficherDescriptionSymbole("Union de deux ensembles | Union of two sets\n S1 = {Lune, Phobos, Deimos}\n S2 = {Lune, Ganymede}\n S1 \\/ S2 = {Lune, Phobos, Deimos, Ganymede}");
}

/** Fonction liee a la sortie d'un bouton**/

void EditeurB::out_symbole() {
	afficherDescriptionSymbole("");
}

