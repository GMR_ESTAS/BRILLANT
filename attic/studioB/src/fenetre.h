//
// Fenetre.h
//	environnement graphique
//


#ifndef _FENETRE_H
#define _FENETRE_H

#include <gtkmm.h>
#include <glibmm/ustring.h>

#include "editeurB.h"
#include "B.h"
#include "annexes.h"

class Fenetre : public Gtk::Window
{
public:
	// Constructeur
	Fenetre(const std::string& icon);
	// Desctructeur
	virtual ~Fenetre();

private:
	// Attributs
	B _b;
	Glib::ustring _currentProject;
	SVector _vProjets, _vMachines, _vRafinements, _vImplementations, _vLibrairies ;
	std::vector<SVector> _vComposantsProjet;
	bool _dependancesActivees;
	Glib::RefPtr<Gdk::Pixbuf> m_iconFenetre, m_iconFenetreAux;
	const std::string& dir_icon;

	// Composants graphiques
	Gtk::ScrolledWindow m_Window, m_scrConsole;
	Gtk::Frame m_ArborescenceFrame;
	Gtk::Table m_Layout;
	Gtk::VBox m_VBox_Main, m_VBox;
	Gtk::HBox m_HBox, m_toolBox;
	Gtk::ToolButton m_tool_new, m_tool_open, m_tool_save,
		m_tool_copy, m_tool_cut, m_tool_paste;
	Gtk::SeparatorToolItem m_tool_separator;
	Gtk::MenuBar m_MenuBar;
	Gtk::Toolbar m_Toolbar;
	Gtk::Entry m_CommandB;
	Gtk::TextView m_Console;
	Glib::RefPtr<Gtk::TextBuffer> m_bufferConsole;
	Gtk::OptionMenu m_OptionMenu;
	Gtk::HSeparator m_Separator;
	EditeurB* m_EditeurB;
	Gtk::Button m_button_ouvrir, m_button_execute, m_Button_Copy, m_Button_Cut, m_Button_Paste;
	Gtk::EventBox m_EventBox;
	Gtk::Label m_commandBLabel;
	Glib::RefPtr<Gtk::Clipboard> m_clipboard;
	//Gtk::Clipboard m_clipboard;
	// Pour le menu
	Gtk::Menu* menuProjet_projet;
	Gtk::Menu::MenuList list_projet_projet;
	//Tree model columns:
	class ModelColumns : public Gtk::TreeModel::ColumnRecord
	{
	public:
		ModelColumns()
		{ add(m_col_name); }
		Gtk::TreeModelColumn<Glib::ustring> m_col_name;
	};
	ModelColumns m_Columns;
	Gtk::ScrolledWindow m_ScrTree;
	Gtk::TreeView m_TreeView;
	Gtk::TreeModel::Row _librairieBranche, _implementationBranche, _machineBranche, _raffinementBranche;
	Glib::RefPtr<Gtk::TreeStore> m_refTreeModel;
	Gtk::RadioButton::Group m_group;
	Gtk::Menu m_Menu_PopupComposant;

	// Fonction : cree le menu proposant les projet
	void creerListeProjets();
	// Fonction : rafraichi l'arbre des composant en fonction du projet ouvert
	void refraichirArbreProjet();
	// Fonction : ouvre un ficheir et l'ajoute directement dans l'editeur
	void ouvertureFichier(const std::string&);
	// Fonction : ecrit un contenu dans un ficheir sur le disque dur
	void ecrireFichier(const Glib::ustring& contenu, const Glib::ustring& fichier);
	// Fonction : affiche un message dans la console
	void afficherMessage(char* contenu);
	// Fonction : affiche un message dans la console avec un indicateur colore
	// en fonction de la gravite du message
	void afficherMessage(char* contenu, int gravite);
	void afficherMessage(const char* contenu, int gravite);
	void afficherMessage(Glib::ustring contenu, int gravite);
	// Fonction : affiche une liste de messages dans la console avec un indicateur colore
	// en fonction de la gravite du message
	void afficherMessage(SVector contenu, int gravite);
	// Fonction : supprime tout ce que contient la console
	void effacerConsole();
	// Fonction : cree la liste de tags necessaire a la coloration du texte dans la console
	void creerTags(Glib::RefPtr<Gtk::TextBuffer>& refBuffer);

	// Fonctions : liees au menu ou bar d'outils
	void on_menu_fichier_nouveau();
	void on_menu_fichier_ouvrir();
	void on_menu_fichier_enregistrer();
	void on_menu_fichier_enregistrer_sous();
	void on_menu_fichier_quitter();
	void on_menu_projet_nouveau();
	void on_menu_projet_supprimer();
	void on_menu_projet_projet(Glib::ustring);
	void on_menu_projet_informations();
	void on_menu_projet_fermer();
	void on_menu_librairie_ajouter();
	void on_menu_librairie_supprimer();
	void on_menu_langageB_nouveau();
	void on_menu_langageB_supprimer();
	void on_menu_langageB_editer();
	void on_menu_langageB_informations();
	void on_menu_langageB_statut();
	void on_menu_langageB_activerDependance();
	void on_menu_langageB_desactiverDependance();
	void on_menu_methodeB_typeCheck();
	void on_menu_methodeB_preuve_force0();
	void on_menu_methodeB_preuve_force1();
	void on_menu_methodeB_preuve_force2();
	void on_menu_methodeB_preuve_force3();
	void on_menu_methodeB_boCheck();
	void on_menu_methodeB_POGenerate(bool);
	void on_menu_utilisateur_ajouter();
	void on_menu_utilisateur_supprimer(bool user);
	void on_menu_utilisateur_montrerListe();
	void on_menu_aide_a_propos();

	void on_toolbar_nouveau();
	void on_toolbar_ouvrir();
	void on_toolbar_enregistrer();
	void on_toolbar_copy();
	void on_toolbar_cut();
	void on_toolbar_paste();

	// Fonctions : liees aux boutons utilises dans les boites de dialogue
	void on_button_OK(Gtk::Widget*, int*);
	void on_button_CANCEL(Gtk::Widget*, int*);
	void on_button_NON(Gtk::Widget*, int*);
	void on_button_OUI(Gtk::Widget*, int*);
	void on_button_ChooseRepository(Gtk::Window* ,Gtk::Entry*);
	void on_button_ChooseFile(Gtk::Window* ,Gtk::Entry*);

	// Fonctions : de gestion d'evenement
	void on_entry_commandB();
	bool on_projectTree_click(GdkEventButton*);
};

#endif //_FENETRE_H
