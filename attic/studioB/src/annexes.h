//
// annexes.h
//	fichier contenant les fonctions utiles au projet
//

#include <gtkmm.h>

// Structure de vecteur de chaines de caracteres
typedef std::vector<Glib::ustring> SVector;

// Fonction : decoupage d'une chaine en plusieurs chaines suivant les 
// caracteres de fin de ligne
SVector split(const Glib::ustring& sInit);

// Fonction : concatenation de chaines
Glib::ustring append(const std::string& ch1, const std::string& ch2);

// Fonction : suppression des caracteres blancs dans une chaine
Glib::ustring stripWhiteSpace(const Glib::ustring& s);
