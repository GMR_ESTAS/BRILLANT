//
//
//

#include <fcntl.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <iostream>

#include "B.h"
#include "fenetre.h"

B::B() :
	fd_maitre(-1)
{
}

B::~B() {
	if (fd_maitre != -1) {
		close(fd_maitre);
	}
}

int B::initB ()
{
	int    fd_esclave;
	struct termios termios_stdin;
	struct termios termios_maitre;
	char * nom_esclave;

	if ((fd_maitre = getpt ()) < 0) {
		perror ("pas de Pseudo TTY Unix 98 disponibles \n");
		exit (1);
	}
	grantpt  (fd_maitre);
	unlockpt (fd_maitre);
	nom_esclave = ptsname (fd_maitre);

	tcgetattr (STDIN_FILENO, & termios_stdin);

	int ret;

	switch (fork ()) {
		case -1 :
			perror ("fork");
			exit (1);
		case 0 : // fils
			close (fd_maitre);
			// Dtachement du terminal de contrle prcdent
			setsid ();
			// Ouverture du pseudo-terminal esclave qui devient
			// alors le terminal de contrle de ce processus.
			if ((fd_esclave = open (nom_esclave, O_RDWR)) < 0) {
				perror ("open");
				exit (1);
			}
			tcsetattr (fd_esclave, TCSANOW, & termios_stdin);
			dup2 (fd_esclave, STDIN_FILENO);
			dup2 (fd_esclave, STDOUT_FILENO);
			dup2 (fd_esclave, STDERR_FILENO);
			ret = system ("startBB");
			if (ret == -1) {
				perror("system");
			}
			break;
		default :
            tcgetattr (fd_maitre, & termios_maitre);
			cfmakeraw (& termios_maitre);
			tcsetattr (fd_maitre, TCSANOW, & termios_maitre);
	}
	return(0);
}

char* B::lance ()
{
	//init de B
	if (initB() != 0) {
		perror ("execute_atelierB");
		exit (1);
	}

	setbuf (stdout, NULL);

	char carlu;
	char *temp=new char[4096];

	int octetsLus = read(fd_maitre, &carlu, 1);

	if(octetsLus != -1) {
		int i = 0;
		while (carlu != '>') {
			//cout <<carlu;
			temp[i] = carlu;
			i = i+1;
			octetsLus = read(fd_maitre, &carlu, 1);
		}
		temp[i] = carlu; temp[i+1] = '\0' ;
	}
	return(temp);
}
	
void B::q()
{
	static const char* quit = "quit";
	char carlu;
	char temp[4096];
	int octetsLus;

	write (fd_maitre, quit, strlen(quit));
	write (fd_maitre, "\n", 1);

	int i = 0;
	octetsLus = read(fd_maitre, &carlu, 1);

	while (carlu != ')') {
		temp[i] = carlu;
		i = i+1;
		octetsLus = read(fd_maitre, &carlu, 1);
	}

	temp[i] = carlu; temp[i+1] = '\0' ;
}

void B::execCmd (const Glib::ustring& commande ) {
	write (fd_maitre, commande.c_str(), commande.length());
	if (commande.find('\n',0) == -1) {
		write (fd_maitre, "\n", 1);
	}
	litReponseB ('>');
}

char* B::execCmd (const char* commande) {
	write (fd_maitre, commande, strlen(commande));
	write (fd_maitre, "\n", 1);
	litReponseB ('>');
}

SVector B::executeCommandeB ( const Glib::ustring& commande) {
	char* p1=execCmd(commande.c_str());
	SVector liste = split(tampon);

	return liste;
}

/* Recupere la reponse de l'atelierB jusqu'au prompt 'carFinal'
   et la stocke dans la variable "tampon" */

void B::litReponseB (char carFinal) {
	char carlu;
	int octetsLus;
	char carprec=' '; // caract precedent

	octetsLus = read(fd_maitre, &carlu, 1);;
	tampon = "";
	while (carlu != carFinal) {
		tampon += carlu;
		carprec=carlu;
		octetsLus = read(fd_maitre, &carlu, 1);
		if (carlu==carFinal && carprec=='-') carlu=' ';
	}
	tampon += carlu;
	tampon += '\0';
}

bool B::erreurTrouve( const Glib::ustring& error ) {
	return ( tampon.find( error ) !=-1 );
}

int B::contientErreurs(SVector v) {
	/*
		retour = 0 pas d'erreurs
		retour != 0 il y a des erreurs
	*/

	return ( 
		erreurTrouve( "Unable to" ) +
		erreurTrouve( "Disparity between" ) +
		erreurTrouve( "can not be" ) +
		erreurTrouve( "Someone is" ) +
		erreurTrouve( "is already" ) +
		erreurTrouve( "Allowed only", v ) +
		erreurTrouve( "Cannot", v ) +
		erreurTrouve( "No such project", v ) +
		erreurTrouve( "Someone is", v ));
}


int B::erreurTrouve(const Glib::ustring& error, SVector vect) {
	/*
		retour =0 pas d'erreur
		retour != 0 erreur(s)
	*/
	int result = 0;
	for (int i=0; i < vect.size(); i++) {
		int r =	vect[i].find(error);
		if (r > -1) {
			result++;
		}
	}
	return result;
}

/* Recupere le nom des repertoires de travail d'un projet */
/*
void B::initDirectory(const Glib::ustring& nomP, Glib::ustring* BDP, Glib::ustring* Lang ) {

	executeCommandeB( "infos_project "+nomP );
	SVector liste = split(tampon);
	Glib::ustring ligne;
	for (int i=0; i<liste.size()-1; i++) {
		ligne = liste[i];
		if ( ligne.find( "Database" , 0)>=0 ) {
			ligne.erase(0,ligne.find_first_of('/',0));
			*BDP = ligne;
		}
		if ( ligne.find( "Translation" , 0)>=0 ) {
			ligne.erase(0,ligne.find_first_of('/',0));
			*Lang = ligne;
		}
	}
}
*/
SVector B::infosProject( const Glib::ustring& nomP) {
	return executeCommandeB( "infos_project "+nomP );
}

void B::openProject(const Glib::ustring& nProjet) {
	executeCommandeB( "open_project "+nProjet );
	//initDirectory( nProjet, BDP, Lang );
}

void B::closeProject ( void ) {
	execCmd("close_project");
}

/**SVector
	Retour : SVector contenant la liste des projets
*/
SVector B::showProjectsList () {
	SVector result;
	char* p1=execCmd( "show_projects_list" );
	SVector liste = split(tampon);
	int n = liste.size()-4;
	Glib::ustring nom;
	for ( int i=4; i<n; i++ ) {
		nom = stripWhiteSpace(liste[i]);
		if ( !( nom.empty() ) ) {
			result.push_back(nom);
		}
	}
	return result;
}
 
SVector B::createProject( const Glib::ustring& nP ) {
	return executeCommandeB("create_project "+nP);
}

SVector B::removeProject ( const Glib::ustring& nP ) {
	return executeCommandeB("remove_project "+nP);
}

SVector B::removeLibraries (const Glib::ustring& nProject, const Glib::ustring& nLib) {
	return executeCommandeB( "remove_project_lib "+nProject+" "+nLib );
}

SVector B::addLibraries (const Glib::ustring& nProject, const Glib::ustring& nLib) {
	return executeCommandeB( "add_project_lib "+nProject+" "+nLib );
}

SVector B::showAllLibraries(const Glib::ustring& nProjet ) {

  //listAllLib->clear();// vide la liste de toutes les librairies
	SVector liste = executeCommandeB( "show_project_libs_list "+nProjet );
	//execCmd( "show_projects_list" );
	SVector result;
	int n = liste.size()-3;
	Glib::ustring nom;
	for ( int i=3; i<n; i++ ) {
		nom = stripWhiteSpace(liste[i]);
		if ( !( nom.empty() ) ) {
			result.push_back(nom);
		}
	}
	return result;
}

SVector B::addComponent (const Glib::ustring& nD, const Glib::ustring& nF ) {
	executeCommandeB( "change_directory "+nD );
	executeCommandeB( "list_sources_b " );
	return executeCommandeB( "add_file "+nF );
}

SVector B::removeComponent(const Glib::ustring& nC ) {
	return executeCommandeB( "remove_component " + nC );
}

SVector B::showAllMachines(const Glib::ustring& nProjet ) {
	SVector liste = executeCommandeB( "show_machines_list 1 1 0 0");
	// parametres pour sml : propres=1 machines=1 rafinements=0 implementations=0 nom_de_fichiers=*
	//execCmd( "show_projects_list" );
	SVector result;
	int n = liste.size()-4;
	Glib::ustring nom;
	for ( int i=3; i<n; i++ ) {
		nom = stripWhiteSpace(liste[i]);
		if ( !( nom.empty() ) ) {
			result.push_back(nom);
		}
	}
	return result;
}

SVector B::showAllImplementations(const Glib::ustring& nProjet ) {
	SVector liste = executeCommandeB( "show_machines_list 0 0 0 1");
	// parametres pour sml : propres=1 machines=0 rafinements=0 implementations=1 nom_de_fichiers=*
	//execCmd( "show_projects_list" );
	SVector result;
	int n = liste.size()-4;
	Glib::ustring nom;
	for ( int i=3; i<n; i++ ) {
		nom = stripWhiteSpace(liste[i]);
		if ( !( nom.empty() ) ) {
			result.push_back(nom);
		}
	}
	return result;
}

SVector B::showAllRafinements(const Glib::ustring& nProjet ) {
	SVector liste = executeCommandeB( "show_machines_list 0 0 1 0");
	// parametres pour sml : propres=1 machines=0 rafinements=1 implementations=0 nom_de_fichiers=*
	//execCmd( "show_projects_list" );
	SVector result;
	int n = liste.size()-4;
	Glib::ustring nom;
	for ( int i=3; i<n; i++ ) {
		nom = stripWhiteSpace(liste[i]);
		if ( !( nom.empty() ) ) {
			result.push_back(nom);
		}
	}
	return result;
}

SVector B::typeChecking (const Glib::ustring& cO ) {
	return executeCommandeB( "typecheck " + cO );
}

SVector B::showObligationsPreuve (const Glib::ustring& cO ) {
	return executeCommandeB( "po_view " + cO );
}

SVector B::prove (const Glib::ustring& cO,const Glib::ustring& force ) {
	return executeCommandeB( "prove " + cO + " " + force);
}

SVector B::infosComposant(const Glib::ustring& nomC) {
	return executeCommandeB( "infos_component "+nomC );
}

Glib::ustring B::findLocation(const Glib::ustring& nomC) {
	SVector vect = executeCommandeB( "infos_component "+nomC );
	Glib::ustring result = "";
	for (int i = 0; i < vect.size(); i++) {
		if (vect[i].find("LOCATION") != -1) {
			result = vect[i].substr(vect[i].find_first_of("/",0),vect[i].size());
		}
	}
	return result;
}

SVector B::enableDependence() {
	return executeCommandeB( "enable_dependence_mode" );
}

SVector B::disableDependence() {
	return executeCommandeB( "disable_dependence_mode" );
}

SVector B::addProjectUser(const Glib::ustring& nomP, const Glib::ustring& nomU ) {
	return executeCommandeB( "add_project_user " + nomP + " " + nomU );
}

SVector B::addProjectReader(const Glib::ustring& nomP, const Glib::ustring& nomR ) {
	return executeCommandeB( "add_project_reader " + nomP + " " + nomR );
}

SVector B::removeProjectUser(const Glib::ustring& nomP, const Glib::ustring& nomU ) {
	return executeCommandeB( "remove_project_user " + nomP + " " + nomU );
}

SVector B::showAllProjectUsers(const Glib::ustring& nomP ) {
	SVector liste = executeCommandeB( "show_project_users_list " + nomP );
	SVector result;
	int n = liste.size()-4;
	Glib::ustring nom;
	for ( int i=3; i<n; i++ ) {
		nom = stripWhiteSpace(liste[i]);
		if ( !( nom.empty() ) ) {
			result.push_back(nom);
		}
	}
	return result;
}

SVector B::showAllProjectReaders(const Glib::ustring& nomP ) {
	SVector liste = executeCommandeB( "show_project_readers_list " + nomP );
	SVector result;
	int n = liste.size()-4;
	Glib::ustring nom;
	for ( int i=3; i<n; i++ ) {
		nom = stripWhiteSpace(liste[i]);
		if ( !( nom.empty() ) ) {
			result.push_back(nom);
		}
	}
	return result;
}

SVector B::bOCheck(const Glib::ustring& nomC ) {
	return executeCommandeB( "b0check " + nomC );
}

SVector B::pOGenerate(const Glib::ustring& nomC,const Glib::ustring& way ) {
	return executeCommandeB( "pogenerate " + nomC + " " + way );
}

SVector B::showComposantStatut(const Glib::ustring& nomC ) {
	return executeCommandeB( "status " + nomC );
}
