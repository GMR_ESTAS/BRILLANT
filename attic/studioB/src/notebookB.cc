//
// voir NotebookB.h
//
//

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "notebookB.h"
#include <iostream>
#include <string.h>
#include <fstream>

#include "pageEditeurB.h"

NotebookB::NotebookB() {

	PageEditeurB* newPage = Gtk::manage(new PageEditeurB());
	set_border_width(1);
	set_tab_pos (Gtk::POS_TOP);

	pages().push_back(Gtk::Notebook_Helpers::TabElem(*newPage, "Nouveau"));
	//set_current_page(page_num(newPage));
}

NotebookB::~NotebookB() {
}

void NotebookB::setText(const char* contenu) {
	PageEditeurB* currentPage = (PageEditeurB*)get_nth_page(get_current_page());
	currentPage->setText(contenu);
}


Glib::ustring NotebookB::getText() {
	PageEditeurB* currentPage = (PageEditeurB*)get_nth_page(get_current_page());
	return currentPage->getText();
}

void NotebookB::setFilename(const std::string& filename) {
	PageEditeurB* currentPage = (PageEditeurB*)get_nth_page(get_current_page());
	currentPage->setFilename(filename);

	Glib::ustring name(filename);
	Glib::ustring c("/");
	int n = name.find_last_of(c);
	Glib::ustring c2(".");
	int n2 = name.find_last_of(c2);

	Gtk::Label label(name.substr(n+1,name.size()-n-(n-n2)));
	set_tab_label(*currentPage, label);
}

std::string NotebookB::getFilename() {
	PageEditeurB* currentPage = (PageEditeurB*)get_nth_page(get_current_page());
	return currentPage->getFilename();
}

void NotebookB::ajouterFeuillet() {
	PageEditeurB* pageEditeurB = Gtk::manage(new PageEditeurB());
	pages().push_back(Gtk::Notebook_Helpers::TabElem(*pageEditeurB, "Nouveau"));
	show_all_children();
	set_current_page(page_num(*pageEditeurB));
}

int NotebookB::getNbPages() {
	return pages().size();
}

int NotebookB::getPageSauvegardee() {
	return ((PageEditeurB*)get_nth_page(get_current_page()))->getPageSauvegardee();
}

void NotebookB::setPageSauvegardee(int state) {
	((PageEditeurB*)get_nth_page(get_current_page()))->setPageSauvegardee(state);
}

void NotebookB::fermerFeuillet() {
	remove_page(get_current_page());
	if (getNbPages() == 0) {
		ajouterFeuillet();
	}
}

void NotebookB::insererSymbole(const char * symbole) {
	PageEditeurB* currentPage = (PageEditeurB*)get_nth_page(get_current_page());
	currentPage->insererSymbole(symbole);
}

void NotebookB::paste_clipboard(Glib::RefPtr<Gtk::Clipboard> clipboard) {
	PageEditeurB* currentPage = (PageEditeurB*)get_nth_page(get_current_page());
	currentPage->paste_clipboard(clipboard);
}

void NotebookB::cut_clipboard(Glib::RefPtr<Gtk::Clipboard> clipboard) {
	PageEditeurB* currentPage = (PageEditeurB*)get_nth_page(get_current_page());
	currentPage->cut_clipboard(clipboard);
}

void NotebookB::copy_clipboard(Glib::RefPtr<Gtk::Clipboard> clipboard) {
	PageEditeurB* currentPage = (PageEditeurB*)get_nth_page(get_current_page());
	currentPage->copy_clipboard(clipboard);
}

