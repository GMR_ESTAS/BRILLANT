//
// annexes.cc
//	fichier contenant les fonctions utiles au projet
//

#include "annexes.h"

SVector split(const Glib::ustring& sInit) {
	Glib::ustring s(sInit);
	SVector result;
	int b = 0;
	while (b != -1) {
		b = s.find_first_of("\n",0);
		if (b != -1) {
			result.push_back(s.substr(0,b-1));
			s.erase(0,b+1);
		} else {
			result.push_back(s.substr(0,s.size()));
		}
	}
	return result;
}

Glib::ustring append(const std::string& ch1, const std::string& ch2) {
	Glib::ustring result(ch1);
	Glib::ustring uch2(ch2);
	result += uch2;
	return result;
}

Glib::ustring stripWhiteSpace(const Glib::ustring& s) {
	Glib::ustring result ="";
	int i = 0;
	while (i < s.size()) {
		if (s.at(i) != ' ' && s.at(i) != '\t') {
			result += s[i];
		}
		i++;
	}
	return result;
}
