/****************************************************
 *			Projet d'IHM
 *			du 3/10/2003 au 31/10/2003
 *
 *			par Thomas Boibessot
 *
 *****************************************************/
 
//
// Main.cc
//	fichier principal responsable du lancement de l'application
//

// exemple
// /home/smith0/profs/tatibouet/GuiToolKit/gtkmm-2.0.2/demos/gtk-demo>./demo

#include "gtkmm/main.h"
#include "fenetre.h"

int main (int argc, char *argv[])
{
  Gtk::Main kit(argc, argv);

  if (argc != 0)
    {
      Fenetre fenetre(argv[1]);
      kit.run(fenetre);
      return 0;
    }
  else
    return -1;
}

