
open GMain
open Bbatch_types
open Bbatch
open Bioodget_common


let update_button_state component_status tool_button =
  match tool_button#label with
    | "Remove" -> ()
    | "Status" -> () 
    | "Typecheck" -> 
	tool_button#misc#set_sensitive (not component_status.typechecked)
    | "Generate POs" -> 
	tool_button#misc#set_sensitive (not component_status.pogenerated)
    | "Prove" -> begin
	match component_status.po_percent with
	  | None -> tool_button#misc#set_sensitive false
	  | Some(i) -> 	tool_button#misc#set_sensitive (i <> 100)
      end
    | "Unprove" -> begin
	match component_status.po_percent with
	  | None -> tool_button#misc#set_sensitive false
	  | Some(i) -> 	tool_button#misc#set_sensitive (i <> 0)
      end
    | _ -> ()
  

let update_component_frame project_status component_frame = 
  match component_frame#label with
    | None -> ()
    | Some(name) -> 
	try
	  let component_status = List.assoc name project_status.components_status in
	  let toolbar = 
	    try component_frame#child
	    with Gpointer.Null -> invalid_arg "update_component_frame" 
	  in
	  let tool_buttons = toolbar#children in
	    List.iter (update_button_state component_status) tool_buttons
	with
	    Not_found -> ()



let update_components_states bbatch_session component_box = 
  let project_status = bbatch_session#status_global in
  let component_frames = component_box#children in
    List.iter (update_component_frame project_status) component_frames


let status_button_clicked bbatch_session name () = 
  let status = string_of_component_status (bbatch_session#status name) in
  let status_window = GWindow.message_dialog ~message_type:`INFO ~buttons:GWindow.Buttons.close 
    ~modal:true ~title:"SC:DEBUG" ~message:status () in
  let answer = status_window#run () in
    match answer with
      | `CLOSE | `DELETE_EVENT -> status_window#destroy ()


let type_button_clicked bbatch_session name () =
  let typecheck = bbatch_session#typecheck name in
  let typecheck_window = GWindow.message_dialog ~message_type:`INFO ~buttons:GWindow.Buttons.close 
    ~modal:true ~title:"SC:DEBUG" ~message:typecheck () in
  let answer = typecheck_window#run () in
    match answer with
      | `CLOSE | `DELETE_EVENT -> typecheck_window#destroy ()



let pogenerate_button_clicked bbatch_session name () = ()
let prove_button_clicked bbatch_session name () = ()
let unprove_button_clicked bbatch_session name () = ()
let remove_button_clicked bbatch_session name () = ()


let named_component_box packing bbatch_session parent_window component = 
  let component_frame = GBin.frame ~label:component ~packing:packing () in
  let component_toolbar = GButton.toolbar ~orientation:`HORIZONTAL ~style:`BOTH ~packing:component_frame#add () in
    
  let status_image = GMisc.image ~stock:`DIALOG_QUESTION () in
  let type_image = GMisc.image ~stock:`SPELL_CHECK () in
  let pogenerate_image = GMisc.image ~stock:`CONVERT () in
  let prove_image = GMisc.image ~stock:`EXECUTE () in
  let unprove_image = GMisc.image ~stock:`UNDO () in
  let remove_image = GMisc.image ~stock:`REMOVE () in


  let _ = component_toolbar#insert_button ~icon:status_image#coerce ~text:"Status" ~callback:(status_button_clicked bbatch_session component) () in
  let _ = component_toolbar#insert_button ~icon:type_image#coerce ~text:"Typecheck" ~callback:(type_button_clicked bbatch_session component) () in
  let _ = component_toolbar#insert_button ~icon:pogenerate_image#coerce ~text:"Generate POs" ~callback:(pogenerate_button_clicked bbatch_session component) () in
  let _ = component_toolbar#insert_button ~icon:prove_image#coerce ~text:"Prove" ~callback:(prove_button_clicked bbatch_session component) () in
  let _ = component_toolbar#insert_button ~icon:unprove_image#coerce ~text:"Unprove" ~callback:(unprove_button_clicked bbatch_session component) () in
  let _ = component_toolbar#insert_button ~icon:remove_image#coerce ~text:"Remove" ~callback:(remove_button_clicked bbatch_session component) () in
    ()
      

(* Components importation *)
let import_components_clicked bbatch_session component_box parent_window () = 
  let import_dialog = GWindow.dialog ~parent:parent_window ~title:"Import components" ~modal:true () in
    
  let component_entry = GEdit.entry ~max_length:100 ~editable:true ~show:true ~packing:import_dialog#vbox#add () in

  let () = import_dialog#add_button_stock `OK `OK in
  let () = import_dialog#add_button_stock `CANCEL `CANCEL in
  let () = import_dialog#set_default_response `CANCEL in

  let answer = import_dialog#run () in
  let result = 
    match answer with
      | `OK -> 
	  let name = component_entry#text in
	  let _ = import_dialog#destroy () in
	    Some(name)
      | `CANCEL -> let _ = import_dialog#destroy () in None
      | `DELETE_EVENT -> let _ = import_dialog#destroy () in None
  in
    match result with
	None -> ()
      | Some(component_name) -> 
	  let () = bbatch_session#add_file component_name in
	    named_component_box 
	      ((component_box:GPack.box)#pack ~expand:false ~fill:false ~padding:10) 
	      bbatch_session 
	      parent_window
	      component_name

	    
(* New component creation *)
let new_component_clicked bbatch_session component_box parent_window () = 
  let import_dialog = GWindow.dialog ~parent:parent_window ~title:"Import components" ~modal:true () in
    
  let component_entry = GEdit.entry ~max_length:100 ~editable:true ~show:true ~packing:import_dialog#vbox#add () in

  let () = import_dialog#add_button_stock `OK `OK in
  let () = import_dialog#add_button_stock `CANCEL `CANCEL in
  let () = import_dialog#set_default_response `CANCEL in

  let answer = import_dialog#run () in
  let result = 
    match answer with
      | `OK -> 
	  let name = component_entry#text in
	  let _ = import_dialog#destroy () in
	    Some(name)
      | `CANCEL -> let _ = import_dialog#destroy () in None
      | `DELETE_EVENT -> let _ = import_dialog#destroy () in None
  in
    match result with
	None -> ()
      | Some(component_name) -> 
	  let () = bbatch_session#add_file component_name in
	    named_component_box 
	      ((component_box:GPack.box)#pack ~expand:false ~fill:false ~padding:10) 
	      bbatch_session 
	      parent_window
	      component_name
	    


let project_status_clicked bbatch_session () = 
  let status = string_of_project_status bbatch_session#status_global in
  let status_window = GWindow.message_dialog ~message_type:`INFO ~buttons:GWindow.Buttons.close 
    ~modal:true ~title:"SC:DEBUG" ~message:status () in
  let answer = status_window#run () in
    match answer with
      | `CLOSE | `DELETE_EVENT -> status_window#destroy ()


let anon_components_buttons (box:GButton.toolbar) bbatch_session component_box parent_window = 

  let _ = 
    make_toolbutton 
      ~toolbar:box ~stock:`NEW ~name:"component-create" ~text:"Create" 
      ~callback:(new_component_clicked bbatch_session component_box parent_window) 
      ~tooltip:"Creates a new component" 
  in
  let _ = 
    make_toolbutton 
      ~toolbar:box ~stock:`ADD ~name:"component-import" ~text:"Import" 
      ~callback:(import_components_clicked bbatch_session component_box parent_window) 
      ~tooltip:"Imports a component existing elsewhere" 
  in
  let _ = 
    make_toolbutton 
      ~toolbar:box ~stock:`DIALOG_QUESTION ~name:"project-status" ~text:"Status"
      ~callback:(project_status_clicked bbatch_session) 
      ~tooltip:"Gets the global status (used for debugging purposes)" 
  in
    ()


let quit_components bbatch_session = 
  bbatch_session#close_project


let delete_component_window bbatch_session ev = 
  begin
    quit_components bbatch_session;
    false
  end

let components_window bbatch_session name =
  let () = bbatch_session#open_project name in
  let components_window = GWindow.window ~modal:true ~title:name ~border_width:10 () in
  let _ = components_window#event#connect#delete ~callback:(delete_component_window bbatch_session) in
  let anon_named_box = GPack.hbox ~spacing:10 ~packing:components_window#add () in

  let anon_packing = GButton.toolbar ~orientation:`VERTICAL ~style:`BOTH ~packing:(anon_named_box#pack ~expand:false ~fill:false) () in
  let _ = GMisc.separator `VERTICAL ~packing:anon_named_box#add () in
  let components_packing_scrolled =  GBin.scrolled_window ~hpolicy:`NEVER ~vpolicy:`ALWAYS ~packing:anon_named_box#add () in

  let components_packing = GPack.vbox ~packing:components_packing_scrolled#add_with_viewport () in

  let () = anon_components_buttons anon_packing bbatch_session components_packing components_window in
   
  let rec add_components component_list = 
    match component_list with
      | [] -> ()
      | component::tail ->
	  let () =
	    named_component_box
	      (components_packing#pack ~expand:false ~fill:false ~padding:10) 
	      bbatch_session 
	      components_window
	      component
	  in
	    add_components tail
  in
    
  let components = bbatch_session#show_machines_list in
  let () = add_components components in
(*    components_window#set_transient_for parent_window; *)
    components_window#show ()
      

