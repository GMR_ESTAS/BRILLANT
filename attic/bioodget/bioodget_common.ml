
(** This file contains the type definitions and templates for the
    windowing functions *)

let make_toolbutton ~(toolbar:GButton.toolbar) ~stock ~name ~callback ~text ~tooltip =
  let button = 
    toolbar#insert_button 
      ~icon:((GMisc.image ~stock:stock ())#coerce)
      ~text:text
      ~tooltip:tooltip
      ~callback:callback
      ()
  in
  let () = button#misc#set_name name in
    button


