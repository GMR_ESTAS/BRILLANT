(* Copyright (C) 1998 Ian Zimmerman <itz@rahul.net> *)
(* $Id: pty_imp.ml,v 1.1 2001/04/27 11:00:14 lefessan Exp $ *)

exception None_available

external allocate: unit -> Unix.file_descr = "alloc_pt"
external grant: Unix.file_descr -> unit = "grant_pt"
external unlock: Unix.file_descr -> unit = "unlock_pt"
external pts_name: Unix.file_descr -> string = "pts_name"

let open_master() =
  try
    let descr = allocate () in
    try
      grant descr;
      unlock descr;
      (descr, pts_name descr)
    with
      e -> Unix.close descr; raise e
  with
    Failure(s) ->
      prerr_endline s;
      raise None_available
  | _ ->
      raise None_available

let open_slave fdm slave_name =
  try
    Unix.openfile slave_name [Unix.O_RDWR] 0666
  with Unix.Unix_error(code,_,_) -> Unix.close fdm;
    raise (Unix.Unix_error(code, "Pty.open_slave", slave_name))

type init_params =
    No_tty_copy                    (*do not copy the modes of current tty*)
  | No_tty_init                    (*do not initialize into sane state*)
  | Stty_init of
      (Unix.terminal_io * Unix.terminal_io)(*flags to set, flags to clear*)

let stty modes params =
  let ctl ch = Char.chr ((Char.code ch) land 0x1f) in
  if not (List.mem No_tty_copy params) then begin
    try
      let control_tty =
        Unix.openfile "/dev/tty" [Unix.O_RDONLY] 0666 in
      let control_modes = Unix.tcgetattr control_tty in
      Unix.close control_tty;
      modes.Unix.c_ignbrk <- control_modes.Unix.c_ignbrk;
      modes.Unix.c_brkint <- control_modes.Unix.c_brkint;
      modes.Unix.c_ignpar <- control_modes.Unix.c_ignpar;
      modes.Unix.c_parmrk <- control_modes.Unix.c_parmrk;
      modes.Unix.c_inpck <- control_modes.Unix.c_inpck;
      modes.Unix.c_istrip <- control_modes.Unix.c_istrip;
      modes.Unix.c_inlcr <- control_modes.Unix.c_inlcr;
      modes.Unix.c_igncr <- control_modes.Unix.c_igncr;
      modes.Unix.c_icrnl <- control_modes.Unix.c_icrnl;
      modes.Unix.c_ixon <- control_modes.Unix.c_ixon;
      modes.Unix.c_ixoff <- control_modes.Unix.c_ixoff;
      modes.Unix.c_opost <- control_modes.Unix.c_opost;
      modes.Unix.c_obaud <- control_modes.Unix.c_obaud;
      modes.Unix.c_ibaud <- control_modes.Unix.c_ibaud;
      modes.Unix.c_csize <- control_modes.Unix.c_csize;
      modes.Unix.c_cstopb <- control_modes.Unix.c_cstopb;
      modes.Unix.c_cread <- control_modes.Unix.c_cread;
      modes.Unix.c_parenb <- control_modes.Unix.c_parenb;
      modes.Unix.c_parodd <- control_modes.Unix.c_parodd;
      modes.Unix.c_hupcl <- control_modes.Unix.c_hupcl;
      modes.Unix.c_clocal <- control_modes.Unix.c_clocal;
      modes.Unix.c_isig <- control_modes.Unix.c_isig;
      modes.Unix.c_icanon <- control_modes.Unix.c_icanon;
      modes.Unix.c_noflsh <- control_modes.Unix.c_noflsh;
      modes.Unix.c_echo <- control_modes.Unix.c_echo;
      modes.Unix.c_echoe <- control_modes.Unix.c_echoe;
      modes.Unix.c_echok <- control_modes.Unix.c_echok;
      modes.Unix.c_echonl <- control_modes.Unix.c_echonl;
      modes.Unix.c_vintr <- control_modes.Unix.c_vintr;
      modes.Unix.c_vquit <- control_modes.Unix.c_vquit;
      modes.Unix.c_verase <- control_modes.Unix.c_verase;
      modes.Unix.c_vkill <- control_modes.Unix.c_vkill;
      modes.Unix.c_veof <- control_modes.Unix.c_veof;
      modes.Unix.c_veol <- control_modes.Unix.c_veol;
      modes.Unix.c_vmin <- control_modes.Unix.c_vmin;
      modes.Unix.c_vtime <- control_modes.Unix.c_vtime;
      modes.Unix.c_vstart <- control_modes.Unix.c_vstart;
      modes.Unix.c_vstop <- control_modes.Unix.c_vstop;
    with
      Unix.Unix_error(Unix.ENODEV, _,_) -> ()
    | Unix.Unix_error(Unix.ENXIO, _,_) -> ()
  end;
  if not (List.mem No_tty_init params) then begin
    modes.Unix.c_cread <- true;
    modes.Unix.c_ignbrk <- false;
    modes.Unix.c_brkint <- true;
    modes.Unix.c_inlcr <- false;
    modes.Unix.c_igncr <- false;
    modes.Unix.c_icrnl <- true;
    modes.Unix.c_ixoff <- false;
    modes.Unix.c_opost <- true;
    modes.Unix.c_isig <- true;
    modes.Unix.c_icanon <- true;
    modes.Unix.c_echo <- true;
    modes.Unix.c_echoe <- true;
    modes.Unix.c_echok <- true;
    modes.Unix.c_echonl <- false;
    modes.Unix.c_noflsh <- false;
    modes.Unix.c_vintr <- ctl 'C';
    modes.Unix.c_vquit <- ctl '\\';
    modes.Unix.c_verase <- ctl '~';
    modes.Unix.c_vkill <- ctl 'U';
    modes.Unix.c_veof <- ctl 'D';
    modes.Unix.c_vstart <- ctl 'Q';
    modes.Unix.c_vstop <- ctl 'S';
  end;
  List.iter (function
      No_tty_init -> ()
    | No_tty_copy -> ()
    | Stty_init(to_set, to_clear) ->
        modes.Unix.c_ignbrk <- to_set.Unix.c_ignbrk or modes.Unix.c_ignbrk;
        modes.Unix.c_brkint <- to_set.Unix.c_brkint or modes.Unix.c_brkint;
        modes.Unix.c_ignpar <- to_set.Unix.c_ignpar or modes.Unix.c_ignpar;
        modes.Unix.c_parmrk <- to_set.Unix.c_parmrk or modes.Unix.c_parmrk;
        modes.Unix.c_inpck <- to_set.Unix.c_inpck or modes.Unix.c_inpck;
        modes.Unix.c_istrip <- to_set.Unix.c_istrip or modes.Unix.c_istrip;
        modes.Unix.c_inlcr <- to_set.Unix.c_inlcr or modes.Unix.c_inlcr;
        modes.Unix.c_igncr <- to_set.Unix.c_igncr or modes.Unix.c_igncr;
        modes.Unix.c_icrnl <- to_set.Unix.c_icrnl or modes.Unix.c_icrnl;
        modes.Unix.c_ixon <- to_set.Unix.c_ixon or modes.Unix.c_ixon;
        modes.Unix.c_ixoff <- to_set.Unix.c_ixoff or modes.Unix.c_ixoff;
        modes.Unix.c_opost <- to_set.Unix.c_opost or modes.Unix.c_opost;
        if to_set.Unix.c_obaud <> 0 then 
          modes.Unix.c_obaud <- to_set.Unix.c_obaud;
        if to_set.Unix.c_ibaud <> 0 then
          modes.Unix.c_ibaud <- to_set.Unix.c_ibaud;
        if to_set.Unix.c_csize <> 0 then
          modes.Unix.c_csize <- to_set.Unix.c_csize;
        if to_set.Unix.c_cstopb <> 0 then
          modes.Unix.c_cstopb <- to_set.Unix.c_cstopb;
        modes.Unix.c_cread <- to_set.Unix.c_cread or modes.Unix.c_cread;
        modes.Unix.c_parenb <- to_set.Unix.c_parenb or modes.Unix.c_parenb;
        modes.Unix.c_parodd <- to_set.Unix.c_parodd or modes.Unix.c_parodd;
        modes.Unix.c_hupcl <- to_set.Unix.c_hupcl or modes.Unix.c_hupcl;
        modes.Unix.c_clocal <- to_set.Unix.c_clocal or modes.Unix.c_clocal;
        modes.Unix.c_isig <- to_set.Unix.c_isig or modes.Unix.c_isig;
        modes.Unix.c_icanon <- to_set.Unix.c_icanon or modes.Unix.c_icanon;
        modes.Unix.c_noflsh <- to_set.Unix.c_noflsh or modes.Unix.c_noflsh;
        modes.Unix.c_echo <- to_set.Unix.c_echo or modes.Unix.c_echo;
        modes.Unix.c_echoe <- to_set.Unix.c_echoe or modes.Unix.c_echoe;
        modes.Unix.c_echok <- to_set.Unix.c_echok or modes.Unix.c_echok;
        modes.Unix.c_echonl <- to_set.Unix.c_echonl or modes.Unix.c_echonl;
        if to_set.Unix.c_vintr <> ctl '@' then
          modes.Unix.c_vintr <- to_set.Unix.c_vintr;
        if to_set.Unix.c_vquit <> ctl '@' then
          modes.Unix.c_vquit <- to_set.Unix.c_vquit;
        if to_set.Unix.c_verase <> ctl '@' then
          modes.Unix.c_verase <- to_set.Unix.c_verase;
        if to_set.Unix.c_vkill <> ctl '@' then
          modes.Unix.c_vkill <- to_set.Unix.c_vkill;
        if to_set.Unix.c_veof <> ctl '@' then
          modes.Unix.c_veof <- to_set.Unix.c_veof;
        if to_set.Unix.c_veol <> ctl '@' then
          modes.Unix.c_veol <- to_set.Unix.c_veol;
        if to_set.Unix.c_vmin <> 0 then
          modes.Unix.c_vmin <- to_set.Unix.c_vmin;
        if to_set.Unix.c_vtime <> 0 then
          modes.Unix.c_vtime <- to_set.Unix.c_vtime;
        if to_set.Unix.c_vstart <> ctl '@' then
          modes.Unix.c_vstart <- to_set.Unix.c_vstart;
        if to_set.Unix.c_vstop <> ctl '@' then
          modes.Unix.c_vstop <- to_set.Unix.c_vstop;

        modes.Unix.c_ignbrk <- (not to_clear.Unix.c_ignbrk) && modes.Unix.c_ignbrk;
        modes.Unix.c_brkint <- (not to_clear.Unix.c_brkint) && modes.Unix.c_brkint;
        modes.Unix.c_ignpar <- (not to_clear.Unix.c_ignpar) && modes.Unix.c_ignpar;
        modes.Unix.c_parmrk <- (not to_clear.Unix.c_parmrk) && modes.Unix.c_parmrk;
        modes.Unix.c_inpck <- (not to_clear.Unix.c_inpck) && modes.Unix.c_inpck;
        modes.Unix.c_istrip <- (not to_clear.Unix.c_istrip) && modes.Unix.c_istrip;
        modes.Unix.c_inlcr <- (not to_clear.Unix.c_inlcr) && modes.Unix.c_inlcr;
        modes.Unix.c_igncr <- (not to_clear.Unix.c_igncr) && modes.Unix.c_igncr;
        modes.Unix.c_icrnl <- (not to_clear.Unix.c_icrnl) && modes.Unix.c_icrnl;
        modes.Unix.c_ixon <- (not to_clear.Unix.c_ixon) && modes.Unix.c_ixon;
        modes.Unix.c_ixoff <- (not to_clear.Unix.c_ixoff) && modes.Unix.c_ixoff;
        modes.Unix.c_opost <- (not to_clear.Unix.c_opost) && modes.Unix.c_opost;
        modes.Unix.c_cread <- (not to_clear.Unix.c_cread) && modes.Unix.c_cread;
        modes.Unix.c_parenb <- (not to_clear.Unix.c_parenb) && modes.Unix.c_parenb;
        modes.Unix.c_parodd <- (not to_clear.Unix.c_parodd) && modes.Unix.c_parodd;
        modes.Unix.c_hupcl <- (not to_clear.Unix.c_hupcl) && modes.Unix.c_hupcl;
        modes.Unix.c_clocal <- (not to_clear.Unix.c_clocal) && modes.Unix.c_clocal;
        modes.Unix.c_isig <- (not to_clear.Unix.c_isig) && modes.Unix.c_isig;
        modes.Unix.c_icanon <- (not to_clear.Unix.c_icanon) && modes.Unix.c_icanon;
        modes.Unix.c_noflsh <- (not to_clear.Unix.c_noflsh) && modes.Unix.c_noflsh;
        modes.Unix.c_echo <- (not to_clear.Unix.c_echo) && modes.Unix.c_echo;
        modes.Unix.c_echoe <- (not to_clear.Unix.c_echoe) && modes.Unix.c_echoe;
        modes.Unix.c_echok <- (not to_clear.Unix.c_echok) && modes.Unix.c_echok;
        modes.Unix.c_echonl <- (not to_clear.Unix.c_echonl) && modes.Unix.c_echonl;
        )                          (*ends anonymous function*)
    params
    
let fork params =
  let (fdm, slave_name) = open_master() in
  match Unix.fork() with
    0 ->                           (*child*)
      let _ = Unix.setsid() in ();
      let fds = open_slave fdm slave_name in
      Unix.close fdm;
      let modes = Unix.tcgetattr fds in
      stty modes params;
      Unix.tcsetattr fds Unix.TCSANOW modes;
      Unix.dup2 fds Unix.stdin;
      Unix.dup2 fds Unix.stdout;
      Unix.dup2 fds Unix.stderr;
      if fds <> Unix.stdin && fds <> Unix.stdout && fds <> Unix.stderr
      then Unix.close fds;
      (0, Unix.stdin, slave_name)
  | pid -> (pid, fdm, slave_name) (*parent*)
          
        
    
