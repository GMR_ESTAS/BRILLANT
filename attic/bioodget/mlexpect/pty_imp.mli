(*Copyright (C) 1998 Ian Zimmerman <itz@rahul.net>*)
(*$Id: pty_imp.mli,v 1.1 2001/04/27 11:00:15 lefessan Exp $*)

(*Module Pty: manipulate Unix pseudoterminal devices, for now only BSD*)
(*style is supported*)

exception None_available

val open_master: unit -> (Unix.file_descr * string)
(* Attempt to allocate a master pty device.  Return a pair*)
(* (MASTER_DESCRIPTOR, SLAVE_DEVICE_NAME) if successful; otherwise raise*)
(* No_available_pty *)

val open_slave: Unix.file_descr -> string -> Unix.file_descr
(* ptys_open MASTER_DESCRIPTOR SLAVE_NAME opens the slave side of the*)
(* pty connection after the master side has been opened by ptym_open.*)
(* Returns the slave file descriptor.*)

type init_params =
    No_tty_copy                    (*do not copy the modes of current tty*)
  | No_tty_init                    (*do not initialize into sane state*)
  | Stty_init of (Unix.terminal_io * Unix.terminal_io)

val fork: init_params list -> (int * Unix.file_descr * string)
(* pty_fork is a higher level function that uses ptym_open and*)
(* ptys_open.  It forks a new process and returns a triple *)
(* (CHILD_PID, MASTER_DESCRIPTOR, SLAVE_NAME) to the parent.*)
(* The new child's stdin and stdout are connected to the slave *)
(* side of a pty whose master side is MASTER_DESCRIPTOR.  In the*)
(* child, the triple (0, Unix.stdout, SLAVE_NAME) is returned.*)

(* In addition, pty_fork performs a three-stage initialization of the*)
(* termios modes on the pty device, as directed by the list argument.*)
(* Unless No_tty_copy is specified, the mode of the current*)
(* controlling terminal are first copied onto the pty.  Unless*)
(* No_tty_init is specified, the pty is then subjected to the*)
(* equivalent of "stty sane".  Lastly, if any *)
(* Stty_init(MODES_TO_SET, MODES_TO_CLEAR) items are in the argument*)
(* list, the true and nonzero members of MODES_TO_SET are copied onto*)
(* the pty, and the true members of MODES_TO_CLEAR are cleared on the*)
(* pty.  Only the boolean members of MODES_TO_CLEAR are significant.*)
