/* Copyright (C) 1998 Ian Zimmerman <itz@transbay.net> */
static char rcsid [] = "$Id: pt_chmod.c,v 1.2 2001/04/27 12:24:18 lefessan Exp $";

#include "config.h"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <grp.h>

#if (defined(__linux))
#include <linux/major.h>
#endif

#ifndef PTY_MASTER_MAJOR
#define PTY_MASTER_MAJOR CONFIGURED_PTY_MAJOR
#endif

#if (defined(HAVE_SYS_SYSMACROS_H))
#include <sys/sysmacros.h>
#endif

int
main ()
{
  uid_t real_uid = getuid();
  struct group* tty_group = getgrnam("tty");
  struct stat stst;
  char slave[] = "/dev/tty  ";
  char scratch[10];

  if (0 == tty_group) {
    exit(1);
  } /*if*/
  if (0 > fstat(0, &stst)) {
    exit(2);
  } /*if*/

  if (!S_ISCHR(stst.st_mode)) {
    exit(3);
  } /*if*/

#if (defined(__linux))
  if (major(stst.st_rdev) != PTY_MASTER_MAJOR) {
    exit(3);
  } /*if*/
#endif

  slave[8] = 'p' + minor(stst.st_rdev)/16;
  if (minor(stst.st_rdev)%16 < 10) {
    slave[9] = '0' + minor(stst.st_rdev)%16;
  } else {
    slave[9] = 'a' - 10 + minor(stst.st_rdev)%16;
  } /*if*/

  if (0 > chown(slave, real_uid, tty_group->gr_gid)) {
    exit(5);
  } /*if*/

  if (0 > chmod(slave, S_IRUSR|S_IWUSR|S_IWGRP)) {
    exit(6);
  } /*if*/

  return 0;
}
