(* Copyright (C) 1998 Ian Zimmerman <itz@rahul.net> *)
(* $Id: buffer_imp.ml,v 1.1 2001/04/27 11:00:10 lefessan Exp $ *)

type t = {
    size: int;
    chars: string;
    mutable rear: int;
    mutable front: int;
    mutable eof: bool;
    mutable eof_reported: bool;
    mutable filled: bool;
    mutable timestamp: float;
    group_beginning: int array;
    group_end: int array;
  } 

let create n =
  {size = n; chars = String.create n; rear = 0; 
   group_beginning = Array.create 10 ~-1; group_end = Array.create 10 ~-1;
   front = 0; eof = false; eof_reported = false; filled = true;
   timestamp = Unix.time()} 

let stamp buffer =
  buffer.timestamp <- Unix.time()

external get_file_tags : Unix.file_descr -> Unix.open_flag list = "get_file_tags"

let set_readable buffer p =
  buffer.filled <- not p

let flush buffer =
  let b = buffer.group_beginning.(0) in
  if b <> ~-1 then  
    buffer.rear <- b
  else
    buffer.rear <- max 0 (buffer.front - buffer.size / 2)

let move buffer = 
  let shift = buffer.rear in
  String.blit buffer.chars buffer.rear buffer.chars 0 
    (buffer.front - buffer.rear);
  buffer.front <- buffer.front - shift;
  buffer.rear <- buffer.rear - shift;
  for i = 0 to 9 do
    if buffer.group_beginning.(i) <> ~-1 then
      buffer.group_beginning.(i) <- buffer.group_beginning.(i) - shift;
    if buffer.group_end.(i) <> ~-1 then 
      buffer.group_end.(i) <- buffer.group_end.(i) - shift;
  done    

(*it is unfortunate that the Unix library doesn't provide a means*)
(*to _get_ the nonblock flag of a descriptor.  Even under the*)
(*assumption that I know from a previous select call that the*)
(*descriptor is readable, I cannot reliably read from it more than*)
(*once without risking being blocked.*)

let fill buffer desc = 
  if not buffer.eof && not buffer.filled then begin
    if buffer.size = buffer.front && buffer.rear = 0 then
      flush buffer;
    if buffer.size = buffer.front or
      (buffer.front > buffer.size / 2 &&
      buffer.rear >= buffer.front - (buffer.size / 2)) then move buffer;
    let num_read = Unix.read desc buffer.chars buffer.front
        (buffer.size - buffer.front) in
      buffer.front <- buffer.front + num_read;
    if num_read = 0 then buffer.eof <- true;
    set_readable buffer false;
    stamp buffer
  end

let to_string buffer =
  String.sub buffer.chars buffer.rear (buffer.front - buffer.rear)

let match_position buffer =
  let e = buffer.group_end.(0) in
  if e <> ~-1 then e else buffer.rear

let set_groups buffer offset =
  for i = 0 to 9 do
    (try
      buffer.group_beginning.(i) <- Str.group_beginning i + offset;
    with
      Not_found 
      | Invalid_argument("Str.group_beginning") -> buffer.group_beginning.(i) <- ~-1);
    (try
      buffer.group_end.(i) <- Str.group_end i + offset;
    with
      Not_found 
      | Invalid_argument("Str.group_end") -> buffer.group_end.(i) <- ~-1)
  done

let clear_groups buffer =
  for i = 0 to 9 do
    buffer.group_beginning.(i) <- ~-1;
    buffer.group_end.(i) <- ~-1;
  done

let search_forward buffer re =
  let p = match_position buffer in
  clear_groups buffer;
  try
    let _ = Str.search_forward re buffer.chars p in ();
    if (Str.match_end() <= buffer.front) then
      begin set_groups buffer 0; true end
    else false 
  with
    Not_found -> false

type match_outcomes =
    No_match
  | Partial_match
  | Full_match

let matched_string buffer =
  let b = buffer.group_beginning.(0) and e = buffer.group_end.(0) in
  if not (b <> ~-1 && e <> ~-1) then
    raise Not_found;
  String.sub buffer.chars b (e - b)

let group_beginning buffer i =
  let b0 = buffer.group_beginning.(0) and b = buffer.group_beginning.(i) in
  if not (b0 <> ~-1 && b <> ~-1) then
    raise Not_found;
  b - b0

let group_end buffer i =
  let b0 = buffer.group_beginning.(0) and e = buffer.group_end.(i) in
  if not (b0 <> ~-1 && e <> ~-1) then
    raise Not_found;
  e - b0

let discard buffer =
  let b = buffer.group_beginning.(0) and e = buffer.group_end.(0) in
  if e <> ~-1 then begin
    clear_groups buffer;
    buffer.rear <- e
  end else if b <> ~-1 then
    buffer.rear <- b

let check_timeout buffer delta now =
  if now >= buffer.timestamp +. delta then
    begin stamp buffer; true end
  else false

let absolute_timeout buffer delta =
  buffer.timestamp +. delta

let check_eof buffer =
  if buffer.eof && not buffer.eof_reported then
    begin buffer.eof_reported <- true; true end
  else false

let regexp_table =
  ((Hashtbl.create 101) : (string, Str.regexp) Hashtbl.t)

let search_forward_cached buffer sre =
  let re = try
    Hashtbl.find regexp_table sre
  with
    Not_found ->
      let re = Str.regexp sre in
      Hashtbl.add regexp_table sre re;
      re
  in search_forward buffer re

let search_forward_exact buffer s =
  search_forward_cached buffer (Str.quote s)


  
        
