(* Copyright (C) 1998 Ian Zimmerman <itz@rahul.net> *)
(* $Id: expect_lib.ml,v 1.1 2001/04/27 11:00:11 lefessan Exp $ *)

module type PTY = sig

  exception None_available
  val open_master: unit -> (Unix.file_descr * string)
  val open_slave: Unix.file_descr -> string -> Unix.file_descr

  type init_params =
      No_tty_copy
    | No_tty_init
    | Stty_init of (Unix.terminal_io * Unix.terminal_io)

  val fork: init_params list -> (int * Unix.file_descr * string)

end

module Pty = (Pty_imp: PTY)

module type SPAWN = sig

  val bufsize: int ref

  val trace: bool ref

  type id

  exception Wrong_kind of string

  val to_descr: id -> Unix.file_descr
  val to_pid: id -> int
  val spawn_file: Unix.file_descr -> bool -> id
  val spawn_process: string -> string array -> Pty_imp.init_params list -> id
  val close: id -> unit
  val send: id -> string -> unit
  val matched_string: id -> string
  val group_beginning: id -> int -> int
  val group_end: id -> int -> int

end

module Spawn = (Spawn_imp: SPAWN with type id = Spawn_imp.id)

module type EXPECT = sig

  val trace: bool ref

  val timeout: float ref

  exception Timeout

  type expect_condition =
      Eof
    | Regexp of string
    | Exact of string

  type 't expect_clause = 't * Spawn.id * expect_condition

  val expect: 't expect_clause list -> 't expect_clause

end

module Expect = (Expect_imp: EXPECT)

