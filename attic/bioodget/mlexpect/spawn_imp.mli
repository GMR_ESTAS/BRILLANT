(* Copyright (C) 1998 Ian Zimmerman <itz@rahul.net> *)
(* $Id: spawn_imp.mli,v 1.1 2001/04/27 11:00:16 lefessan Exp $ *)

val bufsize: int ref
(* size of newly created buffers, default: 8092 *)

val trace: bool ref
(* Activate report of the activity of the spawn module *)

type id
(* expect and interact connections are identified by this *)

exception Wrong_kind of string

val to_buffer: id -> Buffer_imp.t
val to_descr: id -> Unix.file_descr
val to_pid: id -> int

val spawn_file: Unix.file_descr -> bool -> id
(* Open connection to a file.  Argument tells if file descriptor*)
(* should be left open when connection is closed.*)

val spawn_process: string -> string array -> Pty_imp.init_params list -> id
(* spawn_process PROG ARGS MODES forks a new process using*)
(* Pty.pty_fork MODES, executes PROG with ARGS in the child and opens*)
(* a connection to the master pty.*)

val close: id -> unit
(* Closes the connection identified by SPAWN_ID.  If the connection is*)
(* of File type and LEAVEOPEN has been specified, closes the*)
(* associated file descriptor as well.*)
    
val send: id -> string -> unit
(* Sends a string to the connection.  send SPAWN_ID STR is essentially*)
(* equivalent to Unix.write (to_descr SPAWN_ID) STR (String.length STR),*)

val matched_string: id -> string
(* Returns the string that was actually matched by last successful*)
(* invocation of expect.*)

val group_beginning: id -> int -> int
(* group_beginning id N returns the offset of the substring matched by*)
(* the Nth parenthesized group from the beginning of the string*)
(* returned by matched_string.  Note this is a different convention*)
(* from Str.group_beginning.*)

val group_end: id -> int -> int
(* Same as group_beginning for the end of the matched substring.*)
