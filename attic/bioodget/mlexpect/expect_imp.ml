(* Copyright (C) 1998 Ian Zimmerman <itz@rahul.net *)
(* $Id: expect_imp.ml,v 1.1 2001/04/27 11:00:11 lefessan Exp $ *)

let timeout = ref 45.0 

let trace = ref false

exception Timeout

type expect_condition =
    Eof
  | Regexp of string
  | Exact of string

type 't expect_clause = 't * Spawn_imp.id * expect_condition

let matches clause =                    
    let (_, id, condition) = clause in
    let buffer = Spawn_imp.to_buffer id in
    match condition with
      Eof -> Buffer_imp.check_eof buffer
    | Regexp(s) -> 
        if !trace then begin
          prerr_string ((String.escaped s) ^ " ~ " ^
                        (String.escaped
                           (Buffer_imp.to_string buffer))
                        ^ " -> ");
	  flush stderr
	end;
        let answer = Buffer_imp.search_forward_cached buffer s in
        if !trace then
          prerr_endline (if answer then "true" else "false");
        answer
    | Exact(s) ->
        if !trace then begin
          prerr_string ((String.escaped s) ^ " <= " ^
                        (String.escaped
                           (Buffer_imp.to_string buffer))
                        ^ " -> ");
	  flush stderr
	end;
        let answer = Buffer_imp.search_forward_exact buffer s in
        if !trace then
          prerr_endline (if answer then "true" else "false");
        answer

let set_readable clauses p =
  List.iter
    (fun (_, id, _) ->
      Buffer_imp.set_readable
        (Spawn_imp.to_buffer id) p)
    clauses

let rec check_clauses = function
    [] -> None
  | (_, id, _) as clause :: rest ->
      let desc = Spawn_imp.to_descr id
      and buffer = Spawn_imp.to_buffer id in
	begin
	  Buffer_imp.fill buffer desc;
	  if matches clause then Some(clause) else check_clauses rest
	end

let discard clauses =
  List.iter
    (function
        (_, id, Regexp(_)) -> Buffer_imp.discard (Spawn_imp.to_buffer id)
      | (_, id, Exact(_)) -> Buffer_imp.discard (Spawn_imp.to_buffer id)
      | _ -> ())
    clauses

(*XX: This should be in standard library*)
let filter f l =
  List.fold_right (fun i l -> if f i then i :: l else l) l []

let expect clauses =
  let absolute_timeout = Unix.time() +. !timeout
  and matched_clause = ref None 
  and ready_set = ref []
  and fdset =
    List.map
      (fun (_, id, _) -> Spawn_imp.to_descr id)
      clauses in
  let fold_clauses() =
    filter
      (fun (_, id, _) -> List.mem (Spawn_imp.to_descr id) !ready_set)
      clauses in
  discard clauses;
  set_readable clauses false;
  while (!matched_clause = None &&
         Unix.time() < absolute_timeout) do
    set_readable (fold_clauses()) true;
    match check_clauses clauses with
      Some(_) as some -> matched_clause := some
    | None ->
        let (rs, _,_) =
          (Unix.select fdset [] [] (absolute_timeout -. Unix.time())) in
        ready_set := rs;
  done;
  match !matched_clause with
    None -> raise Timeout
  | Some(clause) -> clause            
