dnl Copyright (C) 1998 Ian Zimmerman <itz@rahul.net>
dnl $Id: acinclude.m4,v 1.2 2001/04/27 12:24:16 lefessan Exp $
AC_DEFUN(ITZ_SYS_PTY,
[AC_CACHE_CHECK([for SysV style pty device],itz_cv_sys_pty,
[if test -c /dev/ptmx ; then itz_cv_sys_pty=yes ; else itz_cv_sys_pty=no ;fi])
])
AC_DEFUN(ITZ_SYS_DEV_MAJOR,
[AC_CACHE_CHECK([major device number of master ptys],itz_cv_sys_dev_major_$1,
[[itz_cv_sys_dev_major_$1=`ls -l /dev/$1 2>/dev/null |
  sed -e 's:.*[ 	]\([0-9][0-9]*\),.*:\1:'`]])
])
