(* Copyright (C) 1998 Ian Zimmerman <itz@rahul.net> *)
(* $Id: buffer_imp.mli,v 1.1 2001/04/27 11:00:11 lefessan Exp $ *)

type t
(* type of expect buffers *)

val create : int -> t
(* create size *)

val stamp : t -> unit
(* set timestamp to current time *)

val get_file_tags : Unix.file_descr -> Unix.open_flag list
(* get the file tags of the corresponding file descriptor, raise
   Invalid_argument("get_file_tags") in the file descriptor causes a
   problem *)

val set_readable : t -> bool -> unit
(* call set_readable buffer true after a select call indicates the*)
(* associated file descriptor can be read without blocking*)

val fill : t -> Unix.file_descr -> unit
(* read as much data as there is available and fits in the buffer.*)

val to_string : t -> string
(* Returns substring between rear and front *)
    
val search_forward : t -> Str.regexp -> bool
(* Search the contents for a regexp.  Also sets the group indices.*)

val search_forward_cached : t -> string -> bool
(* Like search_forward but accepts regexp in string form.  Regexp*)
(* recompilation is avoided by caching.*)

val search_forward_exact : t -> string -> bool
(* Like search_forward_cached but looks for a fixed string, not a*)
(* regexp.  The input string is transformed into a regexp*)
(* internally.*)

val matched_string : t -> string
val group_beginning : t -> int -> int
val group_end : t -> int -> int
(*beginning and end indices are relative to the matched string,*)
(*ie. (group_beginning 0) == 0*)

val check_timeout : t -> float -> float -> bool
(* check_timeout buffer delta now tests now >= timestamp + delta *)
    
val check_eof : t -> bool
(* has the end of file been read *)
    
val flush : t -> unit
(* discard some or all buffered characters.  Keeps references to*)
(* matched substring and groups.*)

val discard: t -> unit
(* If a previous match has been recorded against this buffer, discard*)
(* the characters up to the end of the match.  Otherwise a no-op.*)

val absolute_timeout: t -> float -> float
(* absolute_timeout buffer delta returns the absolute time when a*)
(* delta timeout on buffer would expire*)

