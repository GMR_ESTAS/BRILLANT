open Unix

let string_of_termio t =
  let string_of_char = Char.escaped in
    "c_ignbrk : " ^ (string_of_bool t.c_ignbrk) ^ "\n" ^
      "c_brkint : " ^ (string_of_bool t.c_brkint) ^ "\n" ^
      "c_ignpar : " ^ (string_of_bool t.c_ignpar) ^ "\n" ^
      "c_parmrk : " ^ (string_of_bool t.c_parmrk) ^ "\n" ^
      "c_inpck : " ^ (string_of_bool t.c_inpck) ^ "\n" ^
      "c_istrip : " ^ (string_of_bool t.c_istrip) ^ "\n" ^
      "c_inlcr : " ^ (string_of_bool t.c_inlcr) ^ "\n" ^
      "c_igncr : " ^ (string_of_bool t.c_igncr) ^ "\n" ^
      "c_icrnl : " ^ (string_of_bool t.c_icrnl) ^ "\n" ^
      "c_ixon : " ^ (string_of_bool t.c_ixon) ^ "\n" ^
      "c_ixoff : " ^ (string_of_bool t.c_ixoff) ^ "\n" ^
      "c_opost : " ^ (string_of_bool t.c_opost) ^ "\n" ^
      "c_obaud : " ^ (string_of_int t.c_obaud) ^ "\n" ^
      "c_ibaud : " ^ (string_of_int t.c_ibaud) ^ "\n" ^
      "c_csize : " ^ (string_of_int t.c_csize) ^ "\n" ^
      "c_cstopb : " ^ (string_of_int t.c_cstopb) ^ "\n" ^
      "c_cread : " ^ (string_of_bool t.c_cread) ^ "\n" ^
      "c_parenb : " ^ (string_of_bool t.c_parenb) ^ "\n" ^
      "c_parodd : " ^ (string_of_bool t.c_parodd) ^ "\n" ^
      "c_hupcl : " ^ (string_of_bool t.c_hupcl) ^ "\n" ^
      "c_clocal : " ^ (string_of_bool t.c_clocal) ^ "\n" ^
      "c_isig : " ^ (string_of_bool t.c_isig) ^ "\n" ^
      "c_icanon : " ^ (string_of_bool t.c_icanon) ^ "\n" ^
      "c_noflsh : " ^ (string_of_bool t.c_noflsh) ^ "\n" ^
      "c_echo : " ^ (string_of_bool t.c_echo) ^ "\n" ^
      "c_echoe : " ^ (string_of_bool t.c_echoe) ^ "\n" ^
      "c_echok : " ^ (string_of_bool t.c_echok) ^ "\n" ^
      "c_echonl : " ^ (string_of_bool t.c_echonl) ^ "\n" ^
      "c_vintr : " ^ (string_of_char t.c_vintr) ^ "\n" ^
      "c_vquit : " ^ (string_of_char t.c_vquit) ^ "\n" ^
      "c_verase : " ^ (string_of_char t.c_verase) ^ "\n" ^
      "c_vkill : " ^ (string_of_char t.c_vkill) ^ "\n" ^
      "c_veof : " ^ (string_of_char t.c_veof) ^ "\n" ^
      "c_veol : " ^ (string_of_char t.c_veol) ^ "\n" ^
      "c_vmin : " ^ (string_of_int t.c_vmin) ^ "\n" ^
      "c_vtime : " ^ (string_of_int t.c_vtime) ^ "\n" ^
      "c_vstart : " ^ (string_of_char t.c_vstart) ^ "\n" ^
      "c_vstop : " ^ (string_of_char t.c_vstop)


