
open Bbatch_types

(** {1 Regular expressions and helping functions} *)

(*SC: due to the complexity/size of some of the answers of bbatch,
  Perl-compatible regexps look tempting, and pcre are installed on
  most linux systems. Updating mlexpect with pcres might be something
  worth considering *)

let lgroup = "\\("
let rgroup = "\\)"
let regroup s = lgroup ^ s ^ rgroup
let ror s t = s ^ "\\|" ^ t
let regroup_or s t = regroup (ror s t)


let rec duplicate n s =
  if n <= 0 then [] else s :: (duplicate (n-1) s)

let repeat_n n s = 
  String.concat "" (duplicate n s)


let spaces = "[ ]*"
let blanks = "[ \t]*"
let blanks1 = "[ \t]+"

let numbers = "[-0-9]+"
let simple_name = "[_a-zA-Z0-9]+"
let printable = "[_a-zA-Z0-9%]+" (* Some are missing, though *)
let file_with_path = "[/._a-zA-Z0-9]+" 


let nl = "\013\n" (* a newline from bbatch : there is a vertical feed *)
let bbatch_prompt = "bbatch>" ^ nl

let catch_something = (regroup (".*" ^ nl)) ^ "+"
let catch_really_all = (regroup (".*" ^ nl)) ^ "*"
let catch_all = catch_really_all ^ bbatch_prompt


(** Regular expressions for the answers of bbatch *)


let beginning_interpretation = "Beginning interpretation ..."
let beginning_answer = beginning_interpretation ^ nl ^ bbatch_prompt


(** {1 Project frame} *)

let change_directory_answer = bbatch_prompt

let show_projects_list_answer = 
  let beginning= "Printing Project list ..."
  and ending = "End of Project list" in
  let dataform = blanks ^ simple_name ^ nl in
    nl
    ^ beginning ^ nl
    ^ nl
    ^ regroup ((regroup dataform ) ^ "*")
    ^ nl
    ^ ending ^ nl
    ^ nl 
    ^ bbatch_prompt
      

let file_form = file_with_path ^ nl
let file_list_answer = 
  lgroup ^ lgroup ^ file_form ^ rgroup ^ "*" ^ rgroup
  ^ bbatch_prompt

let archive_answer = file_list_answer

let restore_answer = file_list_answer

let remove_project_answer = bbatch_prompt

let infos_project_answer = ()

let create_project_answer = bbatch_prompt

let open_project_answer = bbatch_prompt



(** {1 Component frame} (a project is open) *)

let close_project_answer = bbatch_prompt

(*SC: Oddly enough, the machine list contains a tabulation separator,
  the project list doesn't. We factorize through blanks *)
let show_machines_list_answer =
  let beginning= "Printing machine list ..."
  and ending = "End of machine list" in
  let dataform = blanks ^ simple_name ^ nl in
    nl
    ^ beginning ^ nl
    ^ nl
    ^ (regroup ((regroup dataform) ^ "*"))
    ^ nl
    ^ ending ^ nl
    ^ nl 
    ^ bbatch_prompt


let add_file_answer = ()
let remove_component_answer = ()


(* name can be replaced by a regex, though. Beware of the limit number
   of backrefenceable parentheses groups *)

let s_announce name = "Printing the status of " ^ name
let s_state name = blanks ^ name ^ blanks ^ simple_name ^ blanks ^ file_with_path
let s_leaving = "End of Printing the status"
let s_first_row = ["NbObv";"NbPO";"NbPRi";"NbPRa";"NbUn";"%Pr"] 
let s_row_types = [ simple_name ]
let s_regular_row = duplicate 6 numbers 
let s_line = "\\+" ^ (repeat_n 7 "[-]+\\+")
let s_or_row_types = String.concat "\\|" s_row_types
let s_table_first = 
  "|" ^ blanks 
  ^ "|" ^ blanks 
  ^ (String.concat (blanks ^ "|" ^ blanks) s_first_row) 
  ^ blanks ^ "|"
let s_table_normal = 
  "|" ^ (blanks ^ s_or_row_types ^ blanks)
  ^ "|" ^ blanks 
  ^ (String.concat (blanks ^ "|" ^ blanks) s_regular_row) 
  ^ blanks ^ "|"
let s_table_end name = 
  "|" ^ (blanks ^ name ^ blanks)
  ^ "|" ^ blanks 
    ^ (String.concat (blanks ^ "|" ^ blanks) s_regular_row) 
  ^ blanks ^ "|"

    
let status_answer name = 
  nl
  ^ (s_announce name) ^ nl
  ^ nl
  ^ (regroup (s_state name)) ^ nl 
  ^ (regroup (s_line ^ nl
	      ^ s_table_first ^ nl
	      ^ s_line ^ nl
	      ^ (regroup 
		   ((regroup ((regroup (s_table_normal ^ nl)) ^ "*"))
		    ^ s_line ^ nl
		    ^ (s_table_end name) ^ nl
		   )
		)
	      ^ s_line ^ nl)) ^ "?"
  ^ nl
  ^ s_leaving ^ nl
  ^ bbatch_prompt
    

let interpret_state s =
  match s with 
      "Modified" -> Modified
    | "Parsed" -> Parsed
    | "TypeChecked" -> TypeChecked
    | "POGenerated" -> POGenerated
    | "AutoProved" -> AutoProved
    | _ -> invalid_arg "interpret_state"

let interpret_infos s = 
  let infos = Str.split (Str.regexp blanks1) s in
    (List.nth infos 0, 
     interpret_state (List.nth infos 1),
     List.nth infos 2
    )

let interpret_clause table_line = 
  let each_column = Str.split (Str.regexp (blanks ^ "|" ^ blanks)) table_line in 
  let clause = List.nth each_column 0 in
    (clause, 
     { obvious = int_of_string (List.nth each_column 1);
       normal = int_of_string (List.nth each_column 2);
       interactive = int_of_string (List.nth each_column 3);
       automatic = int_of_string (List.nth each_column 4);
       unproved = int_of_string (List.nth each_column 5);
       percent = int_of_string (List.nth each_column 6)
     }
    )
    
let status_analyse name s = 
  if Str.string_match (Str.regexp (status_answer name)) s 0 then begin
    let info_line = Str.matched_group 1 s in
    let table_lines = try Str.matched_group 3 s with Not_found -> "" in
    let (component, state, path) = interpret_infos info_line in
    let proof_stats = 
      if state <= TypeChecked then (if table_lines = "" then None else invalid_arg "status_analyse")
      else 
	match Str.split (Str.regexp (s_line ^ nl)) table_lines with
	  | [inner_lines; last_line] ->
	      let each_inner = Str.split (Str.regexp nl) inner_lines in
	      let inner_stats = List.map interpret_clause (each_inner @ [last_line]) in
		Some(inner_stats)
	  | _ -> invalid_arg "status_analyse"
    in
      { name = component;
	state = state;
	path = path;
	proof_status = proof_stats
      }
  end
  else invalid_arg "status_analyse"
    

(** The sg_ (status_global) values appear in both normal and
    Obv-truncated tables, while sga_ (sg alternate) appear only in
    Obv-truncated tables. Another oddity of bbatch *)
(*SC: attention aux projets où tous les composants sont pogenerated
  mais n'ont pas d'obvious POs (ça existe ? -- apparemment
  non). Aussi, le nom de composant peut avoir des caractéristiques de
  nom de fichier (chemin) *)
let sg_announce = "Project status"
let sg_first_row = ["COMPONENT";"TC";"POG";"Obv";"nPO";"nUn";"%Pr";"B0C"] 
let sga_first_row = ["COMPONENT";"TC";"POG";"nPO";"nUn";"%Pr";"B0C"] 
let sg_row_types = [ simple_name ]
let sg_col_status = (regroup "OK\\|[-]")
let sg_col_numbers = numbers ^ "?"
let sg_table_line = "\\+" ^ (repeat_n 8 "[-]+\\+")
let sga_table_line = "\\+" ^ (repeat_n 7 "[-]+\\+")
let sg_regular_row = (duplicate 2 sg_col_status) @ (duplicate 4 sg_col_numbers) @ [sg_col_status]
let sga_regular_row = (duplicate 2 sg_col_status) @ (duplicate 3 sg_col_numbers) @ [sg_col_status]
let sg_or_row_types = String.concat "\\|" sg_row_types 
let sg_table_first = 
  "|" ^ blanks 
  ^ (String.concat (blanks ^ "|" ^ blanks) sg_first_row) 
  ^ blanks ^ "|"
let sga_table_first = 
  "|" ^ blanks 
  ^ (String.concat (blanks ^ "|" ^ blanks) sga_first_row) 
  ^ blanks ^ "|"
let sg_table_normal = 
  "|" ^ (blanks ^ sg_or_row_types ^ blanks)
  ^ "|" ^ blanks 
  ^ (String.concat (blanks ^ "|" ^ blanks) sg_regular_row) 
  ^ blanks ^ "|"
let sga_table_normal = 
  "|" ^ (blanks ^ sg_or_row_types ^ blanks)
  ^ "|" ^ blanks 
  ^ (String.concat (blanks ^ "|" ^ blanks) sga_regular_row) 
  ^ blanks ^ "|"
let sg_table_last = 
  "|" ^ (blanks ^ "TOTAL" ^ blanks)
  ^ "|" ^ blanks 
  ^ (String.concat (blanks ^ "|" ^ blanks) sg_regular_row) 
  ^ blanks ^ "|"
let sga_table_last = 
  "|" ^ (blanks ^ "TOTAL" ^ blanks)
  ^ "|" ^ blanks 
  ^ (String.concat (blanks ^ "|" ^ blanks) sga_regular_row) 
  ^ blanks ^ "|"


(** We put the parentheses this way so we can extract the interesting
    parts more easily *)
let status_global_answer = 
  nl
  ^ sg_announce ^ nl
  ^ sg_table_line ^ nl
  ^ sg_table_first ^ nl
  ^ sg_table_line ^ nl
  ^ (regroup
       ((regroup ((regroup (sg_table_normal ^ nl)) ^ "*"))
	^ sg_table_line ^ nl
	^ sg_table_last ^ nl
       )
    )
  ^ sg_table_line ^ nl
  ^ bbatch_prompt

(* The Obv-truncated version of the global status *)
let status_global_alt_answer = 
  nl
  ^ sg_announce ^ nl
  ^ sga_table_line ^ nl
  ^ sga_table_first ^ nl
  ^ sga_table_line ^ nl
  ^ (regroup
       ((regroup ((regroup (sga_table_normal ^ nl)) ^ "*"))
	^ sga_table_line ^ nl
	^ sga_table_last ^ nl
       )
    )
  ^ sga_table_line ^ nl
  ^ bbatch_prompt


let po_column_of_string s =
  if s = "" then None else Some(int_of_string s)

let interpret_component table_line = 
  let each_column = Str.split (Str.regexp (blanks ^ "|" ^ blanks)) table_line in 
    begin
      let name = List.nth each_column 0 in
	(name,
	 { typechecked = (List.nth each_column 1) = "OK";
	   pogenerated = (List.nth each_column 2) = "OK";
	   po_obvious = po_column_of_string (List.nth each_column 3);
	   po_normal = po_column_of_string (List.nth each_column 4);
	   po_unproved = po_column_of_string (List.nth each_column 5);
	   po_percent = po_column_of_string (List.nth each_column 6);
	   b0_checked = (List.nth each_column 7) = "OK"; 
	 }
	)
    end

let interpret_component_alt table_line = 
  let each_column = Str.split (Str.regexp (blanks ^ "|" ^ blanks)) table_line in 
    begin
      let name = List.nth each_column 0 in
	(name,
	 { typechecked = (List.nth each_column 1) = "OK";
	   pogenerated = (List.nth each_column 2) = "OK";
	   po_obvious = None;
	   po_normal = po_column_of_string (List.nth each_column 3);
	   po_unproved = po_column_of_string (List.nth each_column 4);
	   po_percent = po_column_of_string (List.nth each_column 5);
	   b0_checked = (List.nth each_column 6) = "OK"; 
	 }
	)
    end

let sg_analyse_template sg_regexp sg_line interpret_sg s =
  if Str.string_match (Str.regexp status_global_answer) s 0 then begin
    let table_lines = Str.matched_group 1 s in
      match Str.split (Str.regexp (sg_line ^ nl)) table_lines with
	| [inner_lines; last_line] ->
	    let each_inner = Str.split (Str.regexp nl) inner_lines in
	    let inner_stats = List.map interpret_sg each_inner in
	    let (_, last_stat) = interpret_sg last_line in
	      { components_status = inner_stats;
		total_status = last_stat
	      }
	| _ -> invalid_arg "sg_analyse_template"
  end
  else invalid_arg "sg_analyse_template"


let status_global_analyse s =
  sg_analyse_template status_global_answer sg_table_line interpret_component s

let status_global_alt_analyse s = 
  sg_analyse_template status_global_alt_answer sga_table_line interpret_component_alt s



let infos_component_answer = ()

  
let typecheck_answer name = 
  "Type Checking machine " ^ name ^ nl
  ^ nl
  ^ lgroup ^ lgroup ^ blanks ^ lgroup ^ printable ^ "\\|" ^ blanks ^ rgroup ^ "+" ^ nl ^ rgroup ^ "+" ^ rgroup 
  ^ nl
  ^ "End of Type checking" ^ nl
  ^ nl 
  ^ bbatch_prompt

let typecheck_error_1 name = 
  name ^ " has already been type checked" ^ nl 
  ^ nl 
  ^ bbatch_prompt


let pogenerate_answer = ()
  
let prove_answer = ()
  
let unprove_answer = ()
  
let browse_answer = ()



(** {1 Proof session frame} *)
