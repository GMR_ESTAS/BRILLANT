
open Bbatch
open Bioodget_common
open Components


let open_project_clicked bbatch_session name () = 
  components_window bbatch_session name

let remove_project_clicked bbatch_session project_frame name () =
  let () = bbatch_session#remove_project name in
    project_frame#destroy ()

let archive_clicked bbatch_session name () = 
  let (sources, proofs, full) = bbatch_session#archive name in
  let result = String.concat "\n#####\n"
    [(String.concat "\n" sources);
     (String.concat "\n" proofs);
     (String.concat "\n" full)]
  in
    debug result


let named_project_box packing bbatch_session project = 
  let project_frame = GBin.frame ~label:project ~packing:packing () in
  let project_toolbar = GButton.toolbar ~orientation:`HORIZONTAL ~style:`BOTH ~packing:project_frame#add () in
    
  let open_image = GMisc.image ~stock:`OPEN () in
  let remove_image = GMisc.image ~stock:`REMOVE () in
  let archive_image = GMisc.image ~stock:`SAVE () in

  let _ = project_toolbar#insert_button ~icon:open_image#coerce ~text:"Open" ~callback:(open_project_clicked bbatch_session project) () in
  let _ = project_toolbar#insert_button ~icon:remove_image#coerce ~text:"Remove" ~callback:(remove_project_clicked bbatch_session project_frame project) () in
  let _ = project_toolbar#insert_button ~icon:archive_image#coerce ~text:"Archive" ~callback:(archive_clicked bbatch_session project) () in
    ()
      
(*
let named_project_box packing bbatch_session parent_window project = 
  let project_frame = GBin.frame ~label:project ~packing:packing () in

  let project_actions = GAction.action_group ~name:(project ^ "-actions") () in
  let () = GAction.add_actions project_actions 
    [ GAction.add_action "Open" ~stock:`OPEN ~tooltip:"Open project" ~callback:(open_project_clicked parent_window#as_window bbatch_session project);
      GAction.add_action "Remove" ~stock:`REMOVE ~tooltip:"Remove project" ~callback:(remove_project_clicked bbatch_session project_frame project);
      GAction.add_action "Archive" ~stock:`SAVE ~tooltip:"Archive project" ~callback:(archive_clicked bbatch_session project);
    ]
  in
  let project_toolbar = GAction.ui_manager () in
  let () = project_toolbar#insert_action_group project_actions 0 in
  let () = project_frame#add (project_toolbar#get_widget ("/" ^ project ^ "-tools")) in
    ()
*)  

let create_project_clicked bbatch_session project_box () = 
  let create_dialog = GWindow.dialog ~title:"Create a new project" ~modal:true () in
    
  let project_entry = GEdit.entry ~max_length:100 ~editable:true ~show:true ~packing:create_dialog#vbox#add () in

  let () = create_dialog#add_button_stock `OK `OK in
  let () = create_dialog#add_button_stock `CANCEL `CANCEL in
  let () = create_dialog#set_default_response `CANCEL in

  let answer = create_dialog#run () in
  let result = 
    match answer with
      | `OK -> 
	  let name = project_entry#text in
	  let _ = create_dialog#destroy () in
	    Some(name)
      | `CANCEL -> let _ = create_dialog#destroy () in None
      | `DELETE_EVENT -> let _ = create_dialog#destroy () in None
  in
    match result with
	None -> ()
      | Some(project_name) -> 
	  let () = bbatch_session#create_project project_name in
	    named_project_box 
	      ((project_box:GPack.box)#pack ~expand:false ~fill:false ~padding:10)
	      bbatch_session 
	      project_name
	    

let default d str_option = 
  match str_option with
    | None -> d
    | Some(s) -> s

(*SC: parachever l'histoire de nom de projet via le nom de fichier *)


let restore_project_clicked bbatch_session () = 
  let restore_dialog = GWindow.file_chooser_dialog ~action:`OPEN ~title:"Select the archive to restore" ~modal:true () in
  let () = restore_dialog#set_select_multiple false in

  let archives_filter = GFile.filter ~name:"Archives" ~patterns:["*.tar"; "*.arc"] () in
  let all_filter = GFile.filter ~name:"All" () in
  let () = all_filter#add_pattern "*" in

  let _ = restore_dialog#add_button_stock `CANCEL `CANCEL in
  let _ = restore_dialog#add_select_button_stock `OPEN `OPEN in
  let _ = restore_dialog#set_current_folder bbatch_session#archives_path  in 
  let () = restore_dialog#add_filter archives_filter in
  let () = restore_dialog#add_filter all_filter in

  let name_frame = GBin.frame ~label:"Name of the project" ~packing:restore_dialog#vbox#add () in
  let name_entry = GEdit.entry ~editable:true ~packing:name_frame#add () in

  let _ = restore_dialog#connect#selection_changed ~callback:(fun () -> name_entry#set_text (default "None" restore_dialog#filename)) in

  let answer = restore_dialog#run () in
  let result = 
    match answer with
      | `OPEN -> restore_dialog#filename
      | `CANCEL | `DELETE_EVENT -> None
  in
  let _ = restore_dialog#destroy () in
    match result with
      | None -> ()
      | Some(f) -> 
	  let restored_files = bbatch_session#restore f name_entry#text in
	    debug (String.concat "\n" restored_files)


let anon_project_buttons (box:GButton.toolbar) bbatch_session project_box = 
  let add_image = GMisc.image ~stock:`ADD () in
  let restore_image = GMisc.image ~stock:`UNDELETE () in

  let _ = box#insert_button ~icon:add_image#coerce ~text:"Create" ~callback:(create_project_clicked bbatch_session project_box) () in
  let _ = box#insert_button ~icon:restore_image#coerce ~text:"Restore" ~callback:(restore_project_clicked bbatch_session) () in
    ()


let projects_window bbatch_session = 
  let projects = bbatch_session#show_projects_list in
  let projects_window = GWindow.window ~title:"Projects" ~border_width:10 ~resizable:false ~height:((Gdk.Screen.height ()) * 9 / 10) () in
  let _ = projects_window#connect#destroy ~callback:(GMain.Main.quit) in
  let anon_named_box = GPack.hbox ~spacing:10 ~packing:projects_window#add () in

  let anon_packing = GButton.toolbar ~orientation:`VERTICAL ~style:`BOTH ~packing:(anon_named_box#pack ~expand:false ~fill:false) () in
  let _ = GMisc.separator `VERTICAL ~packing:(anon_named_box#pack ~expand:false ~fill:false) () in
  let projects_packing_scrolled = GBin.scrolled_window ~hpolicy:`NEVER ~vpolicy:`ALWAYS ~packing:anon_named_box#add () in
  let projects_packing = GPack.vbox ~packing:projects_packing_scrolled#add_with_viewport () in

  let () = anon_project_buttons anon_packing bbatch_session projects_packing in

  let rec add_projects proj_list = 
    match proj_list with
      | [] -> ()
      | project::tail -> 
	  let () = 
	    named_project_box 
	      (projects_packing#pack ~expand:false ~fill:false ~padding:10) 
	      bbatch_session 
	      project 
	  in
	    add_projects tail
  in
  let () = add_projects projects in
    
    projects_window#show ();
    GMain.Main.main ()
