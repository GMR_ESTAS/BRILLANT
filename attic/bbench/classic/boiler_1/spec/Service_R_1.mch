/*? **************************** DIGILOG ************************************
 * Date : @(#) sccs get 6/7/96
 *
 * File : Service_R_1.mch
 *
 * Version : 1.7
 *
 * Directory : /home/IPSN/ebt/DEMON/Control/spec/SCCS/s.Service_R_1.mch
 *
 * Type	:	MACHINE
 *
 * Object : Service concerning the pumps
 *
 * Copyright : (C) 1996 Jean-Raymond ABRIAL / DIGILOG - STERIA Mediterranee
 *
 *   Permission is granted to make and distribute verbatim copies of
 *   this file provided the copyright notice and this permission notice
 *   are preserved on all copies.
 *
 *   Permission is granted to copy and distribute modified versions of
 *   this file under the conditions for verbatim copying, provided also
 *   that the entire resulting derived work is distributed under the terms
 *   of a permission notice identical to this one.
 *
 *   Permission is granted to copy and distribute translations of this
 *   manual into another language, under the above conditions for modified
 *   versions
 *
 *	 This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *   This program can be redistributed or modified under the terms of
 *   the GNU General Public License. The General Public License can be
 *	 found in the file COPYING tha accompanies the source code.
 *
 *---------------------------------------------------------------------------
 *
 * Functions :
 *
 *---------------------------------------------------------------------------
 *
 * Modifications :
 *
 ************************************************************************* ?*/

MACHINE
	Service_R_1(pump_number)

CONSTRAINTS
	pump_number : NAT

SEES
	Acq_1, Constants_L_1

VISIBLE_VARIABLES
	rim,
	rrm,
	rfam,
	rram,
	rr,
	rpop_0,
	rpcl_0,
	rc,
	rfm,
	rok,
	rtk
	

INVARIANT
	rim : BOOL &
	rrm : BOOL &
	rr : BOOL &
	rpop_0 : BOOL &
	rpcl_0 : BOOL &
	rc : BOOL &
	rfm : BOOL &
	rram : BOOL &
	rfam : BOOL &
	rok : BOOL &
	rtk : BOOL 

INITIALISATION
	rim :: BOOL ||
	rrm :: BOOL ||
	rr :: BOOL ||
	rpop_0 :: BOOL ||
	rpcl_0 :: BOOL ||
	rc :: BOOL ||
	rfm :: BOOL ||
	rram :: BOOL ||
	rfam :: BOOL ||
	rok :: BOOL ||
	rtk :: BOOL 

OPERATIONS
	read_pump =
		PRE
			pump_number : 1..NB_PUMP
		THEN
			rim := acq_rim(pump_number) ||
			rrm := acq_rrm(pump_number) ||
			rfam := acq_rfam(pump_number) ||
			rr := acq_rr(pump_number) ||
			rpop_0 := acq_pop_0 ||
			rpcl_0 := acq_pcl_0
		END;

	calculate_previous_pump = 
		BEGIN
			IF (rok = FALSE & rrm = TRUE) THEN
				rc := rr
			ELSIF (rpop_0 = TRUE) THEN
				rc := TRUE
			ELSIF (rpcl_0 = TRUE) THEN
				rc := FALSE
			END
		END;

	pump_test = 
		BEGIN
			rtk := bool (rim = TRUE & 
						(rok = FALSE or rrm = FALSE) &
						(rfam = FALSE or rfm = TRUE)) ||
			rfm := bool (((rr /= rc) &
						(rok = TRUE or rrm = TRUE)) or
						(rfm = TRUE & rfam = FALSE)) ||

			rok := bool ((rr = rc) & 
						 (rok = TRUE or rrm = TRUE)) ||

			rram := rrm
		END
END

