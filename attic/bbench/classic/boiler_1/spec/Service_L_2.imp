/*? **************************** DIGILOG ************************************
 * Date : @(#) sccs get 6/7/96
 *
 * File : Service_L_2.imp
 *
 * Version : 1.7
 *
 * Directory : /home/IPSN/ebt/DEMON/Control/spec/SCCS/s.Service_L_2.imp
 *
 * Type	:	IMPLEMENTATION
 *
 * Object : Service concerning the water level instrument
 *
 * Copyright : (C) 1996 Jean-Raymond ABRIAL / DIGILOG - STERIA Mediterranee
 *
 *   Permission is granted to make and distribute verbatim copies of
 *   this file provided the copyright notice and this permission notice
 *   are preserved on all copies.
 *
 *   Permission is granted to copy and distribute modified versions of
 *   this file under the conditions for verbatim copying, provided also
 *   that the entire resulting derived work is distributed under the terms
 *   of a permission notice identical to this one.
 *
 *   Permission is granted to copy and distribute translations of this
 *   manual into another language, under the above conditions for modified
 *   versions
 *
 *	 This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *   This program can be redistributed or modified under the terms of
 *   the GNU General Public License. The General Public License can be
 *	 found in the file COPYING tha accompanies the source code.
 *
 *---------------------------------------------------------------------------
 *
 * Functions :
 *      read_level
 *      calculate_current_level
 *      level_test
 *      equipment_test
 *      determine_mode
 *      adjust_level
 *
 *---------------------------------------------------------------------------
 *
 * Modifications :
 *
 ************************************************************************* ?*/

IMPLEMENTATION
	Service_L_2

REFINES
	Service_L_1

SEES
    Cycle_S_1, Cycle_Y_1, Cycle_X_1,
	Constants_L_1, Acq_1, MinMax_1, Arithmetic_1

INITIALISATION
	lim := TRUE ;
	lrm := FALSE ;
	lfam := FALSE ;
	qr := 0 ;
	qa1 := N1 ;
	qa2 := N2 ;
	qc1 := N1 ;
	qc2 := N2 ;
	lfm := FALSE ;
	lram := FALSE ;
	lok := TRUE ;
	ltk := FALSE ;
	eqs := TRUE ;
	mode := normal 
	

OPERATIONS
	read_level =
		BEGIN
			lim <-- read_lim;
			lrm <-- read_lrm;
			lfam <-- read_lfam;
			qr <-- read_qr
		END;

	level_test =
		BEGIN
			ltk := bool (lim = TRUE & qr <= CAP &
						 (lok = FALSE or lrm = FALSE) &
						 (lfam = FALSE or lfm = TRUE)) ;

			lfm := bool (((qr < qc1  or qr > qc2) &
						 (lok = TRUE or lrm = TRUE)) or
						 (lfm = TRUE &  lfam = FALSE)) ;

			lok := bool ((qr >= qc1  & qr <= qc2) &
						 (lok = TRUE or lrm = TRUE)) ;

			lram := lrm

		END;

	equipment_test =
		VAR wtk_l, wok_l, rtk_l, sok_l, stk_l
		IN
			wtk_l <-- read_wtk_tot;
			wok_l <-- read_wok_tot;
			rtk_l <-- read_rtk_tot;
			stk_l <-- read_stk;
			sok_l <-- read_sok;
			eqs := bool (ltk = TRUE & stk_l = TRUE & wtk_l = TRUE & 
						 rtk_l = TRUE &
						 (lok = TRUE or sok_l = TRUE) &
						 (lok = TRUE or wok_l = TRUE))
		END;

	determine_mode =
		VAR wok_l, sok_l, rok_l
		IN
			wok_l <-- read_wok_tot;
			rok_l <-- read_rok_tot;
			sok_l <-- read_sok;
			IF (lok = FALSE) THEN
				mode := rescue
			ELSIF (sok_l = FALSE or wok_l = FALSE or rok_l = FALSE) THEN
				mode := degraded
			ELSE
				mode := normal
			END
		END;

	adjust_level =
		BEGIN
			IF (lok = TRUE) THEN
				qa1 <-- ReturnMinimum(qr, CAP);
				qa2 <-- ReturnMinimum(qr, CAP)
			ELSE
				qa1 := qc1 ;
				qa2 := qc2
			END
		END;

	calculate_current_level =
		VAR qmin_l, qmax_l, va1_l, va2_l, pa1_l, pa2_l IN

			va1_l, va2_l <-- read_va;
			pa1_l, pa2_l <-- read_pa_tot;

			qmin_l := qa1 -va2_l*DT - (((U1*DT*DT) + 1)/2) + pa1_l*DT;
			qmax_l := qa2 -va1_l*DT + (((U2*DT*DT) + 1)/2) + pa2_l*DT;

			qc1 <-- minmax(qmin_l, 0, CAP);
			qc2 <-- minmax(qmax_l, 0, CAP)
		END
END

