/*? **************************** DIGILOG ************************************
 * Date : @(#) sccs get 6/7/96
 *
 * File : Service_S_2.imp
 *
 * Version : 1.7
 *
 * Directory : /home/IPSN/ebt/DEMON/Control/spec/SCCS/s.Service_S_2.imp
 *
 * Type	:	IMPLEMENTATION
 *
 * Object : Service concerning the steam level instrument
 *
 * Copyright : (C) 1996 Jean-Raymond ABRIAL / DIGILOG - STERIA Mediterranee
 *
 *   Permission is granted to make and distribute verbatim copies of
 *   this file provided the copyright notice and this permission notice
 *   are preserved on all copies.
 *
 *   Permission is granted to copy and distribute modified versions of
 *   this file under the conditions for verbatim copying, provided also
 *   that the entire resulting derived work is distributed under the terms
 *   of a permission notice identical to this one.
 *
 *   Permission is granted to copy and distribute translations of this
 *   manual into another language, under the above conditions for modified
 *   versions
 *
 *	 This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *   This program can be redistributed or modified under the terms of
 *   the GNU General Public License. The General Public License can be
 *	 found in the file COPYING tha accompanies the source code.
 *
 *---------------------------------------------------------------------------
 *
 * Functions :
 *		read_steam
 *		steam_test
 *		adjust_steam
 *		calculate_current_steam
 *---------------------------------------------------------------------------
 *
 * Modifications :
 *
 ************************************************************************* ?*/

IMPLEMENTATION
	Service_S_2

REFINES
	Service_S_1

SEES
	Constants_L_1, Acq_1, MinMax_1, Arithmetic_1

INITIALISATION
	sim := TRUE ;
	srm := TRUE ;
	sfam := FALSE ;
	vr := 0 ;
	va1 := 0 ;
	va2 := WMAX ;
	vc1 := 0 ;
	vc2 := WMAX ;
	sfm := FALSE ;
	sram := FALSE ;
	sok := TRUE ;
	stk := TRUE

OPERATIONS
	read_steam =
		BEGIN
			sim <-- read_sim;
			srm <-- read_srm;
			sfam <-- read_sfam;
			vr <-- read_vr
		END;

	steam_test = 
		BEGIN
			/* On declare la liaison defaillante si :
				- on n'a pas recu LEVEL (sim=FALSE)
				- ou on a recu LEVEL mais le niveau est incoherent
					 (sim=FALSE et vr > WMAX)
				- ou on a recu un message de reparation (srm=TRUE)
					 et que l'equipement etqit en fonctionnement correct
				- ou on a recu un acquittement de demande de reparation
					(sfam= TRUE) et qu'on n'a pas emis de demande de
					 reparation (sfm=FALSE)
			*/
			stk := bool (sim = TRUE & vr <= WMAX & 
						 (sok = FALSE  or srm = FALSE) &
						 (sfam = FALSE or sfm = TRUE)) ;

			/* On emet un message de demande de reparation si :
					- on a emis une demande de reparation au 
					  cycle precedent et on n'a pas recu
					  d'acquittement
					- le niveau d'eau recu est incorrect alors que
					  soit l'equipement etait en bon fonctionnement(sok=TRUE)
					  soit l'equipement vient d'etre repare (srm = TRUE)
			*/
						
			sfm := bool (((vr > vc2 or vr < vc1) &
						  (sok = TRUE or srm = TRUE)) or
						 (sfm = TRUE & sfam = TRUE)) ;

			sok := bool ((vr <= vc2 & vr >= vc1) &
						 (sok = TRUE or srm = TRUE)) ;

			sram := srm 
		END;


	adjust_steam =
		IF (sok = TRUE) THEN
			va1 <-- ReturnMinimum(vr, WMAX);
			va2 <-- ReturnMinimum(vr, WMAX)
		ELSE
			va1 := vc1 ;
			va2 := vc2
		END;

	calculate_current_steam =
		BEGIN
			vc1 <-- minmax(va1 - (U2*DT), 0, WMAX);
			vc2 <-- minmax(va2 + (U1*DT), 0, WMAX)
		END
END

