/* ----------------------------
 * Copyright (c) 2004 - INRETS
 * ----------------------------
 *  Project: 	RLC - Railway Level Crossing
 *  Created:	Wed Feb 11 11:32:21 2004
 *  Author: 	Rafael MARCANO
 */
MACHINE LCC_System /* Lervel Crossing Control System */

SETS LCC;
    STATE={Deactivated,ShowingYlight,ShowingRlight,ClosingB,OpeningB,ClosedB,Failure};
    MODE={Safe,Unsafe}
        
INCLUDES Barrier, BarrierSensor, Red.Light, TrainborneCS, Yellow.Light

CONSTANTS
    Activated

PROPERTIES
    Activated<:STATE & Activated={ShowingYlight,ShowingRlight,ClosingB,OpeningB,ClosedB}
    
VARIABLES
    lcc, lcc_barrier, lcc_sensor, state, redLight, yellowLight, lcc_train, mode

INVARIANT
    lcc <: LCC &
    lcc_barrier : lcc --> barrier &
    lcc_sensor : lcc --> bSensor &
    state : lcc --> STATE &
    redLight : lcc --> Red.light &
    yellowLight : lcc --> Yellow.light &
    lcc_train : lcc +-> train &
    mode : lcc --> MODE &
    
    !obj.(obj:lcc & bState(lcc_barrier(obj))=Closed
	  => Red.lState(redLight(obj))=On) &
    !obj.(obj:lcc & bState(lcc_barrier(obj))=Closing 
	  => Red.lState(yellowLight(obj))=On) &
    !obj.(obj:lcc & Yellow.lState(yellowLight(obj))=Off & Red.lState(redLight(obj))=Off
	  => bState(lcc_barrier(obj))=Opened) &

    !obj.(obj:lcc & state(obj):Activated & bStatus(lcc_sensor(obj))=Opened 
	  => mode(obj)=Unsafe) &

    !obj.(obj:lcc & bStatus(lcc_sensor(obj))=Opened & bState(lcc_barrier(obj))=Closed
	  => mode(obj)=Unsafe) 

/*GM 
DEFINITIONS
    theBarrier(obj)==lcc_barrier(obj);
    theSensor(obj)==lcc_sensor(obj)
*/
INITIALISATION
    lcc,lcc_barrier,lcc_sensor,state,redLight,yellowLight,lcc_train,mode := {},{},{},{},{},{},{},{}

OPERATIONS
    trainDetectionEntry_startClosingSequence(obj,valTrain) =
    PRE
	obj:lcc &
	valTrain:train &
	state(obj)=Deactivated &
	obj /: dom(lcc_train) &
	valTrain /: ran(lcc_train) &
	bStatus(lcc_sensor(obj))=Opened &
	bState(lcc_barrier(obj))=Opened &
	Red.lState(redLight(obj))=Off &
	Yellow.lState(yellowLight(obj))=Off	
    THEN
	state(obj):=ShowingYlight ||
	lcc_train(obj):=valTrain ||
	Yellow.switchOn(yellowLight(obj)) ||
	mode(obj):=Unsafe
    END;

    timeOut_1_showRlight(obj) =
    PRE
	obj:lcc &
	state(obj)=ShowingYlight &
	bStatus(lcc_sensor(obj))=Opened &
	bState(lcc_barrier(obj))=Opened &
	Red.lState(redLight(obj))=Off &
	Yellow.lState(yellowLight(obj))=On	
    THEN
	state(obj):=ShowingRlight ||
	Yellow.switchOff(yellowLight(obj)) ||
	Red.switchOn(redLight(obj))
    END;

    timeOut_2_closeBarrier(obj)=
    PRE
	obj:lcc &
	state(obj)=ShowingRlight &
	Red.lState(redLight(obj))=On &
	Yellow.lState(yellowLight(obj))=Off	
    THEN
	state(obj):=ClosingB ||
	closeBarrier(lcc_barrier(obj))
    END;
        
    timeOut_3_setMode(obj) =
    PRE
	obj:lcc &
	state(obj)=ClosingB &
	bState(lcc_barrier(obj))=Closed &
	Red.lState(redLight(obj))=On &
	Yellow.lState(yellowLight(obj))=Off	
    THEN
	SELECT bStatus(lcc_sensor(obj))=Closed THEN
	    state(obj):= ClosedB ||
	    mode(obj):=Safe
	WHEN bStatus(lcc_sensor(obj))=Opened THEN
	    state(obj):= Failure
	ELSE skip
	END
    END;

    trainDetectionRear_openBarrier(obj,valTrain) =
    PRE
	obj:lcc &
	valTrain:train &
        (obj|->valTrain):lcc_train &
	state(obj)=ClosedB &
	mode(obj)=Safe &
	bState(lcc_barrier(obj))=Closed
    THEN
	state(obj):=OpeningB ||
	openBarrier(lcc_barrier(obj)) ||
	lcc_train:=lcc_train - {(obj|->valTrain)}
    END;

    deactivate(obj) =
    PRE
	obj:lcc &
	state(obj)=OpeningB &
	bStatus(lcc_sensor(obj))=Opened &
	bState(lcc_barrier(obj))=Opened
    THEN
	state(obj):=Deactivated ||
	Red.switchOff(redLight(obj))
    END;

    failure(obj) =
    PRE
	obj:lcc &
	state(obj)=Deactivated
    THEN
	state(obj):=Failure
    END;

    repair(obj) =
    PRE
	obj:lcc &
	state(obj)=Failure
    THEN
	state(obj):=Deactivated
    END;
        	    
    obj<--createLcc(valBarrier,valSensor,valRlight,valYlight) =
    PRE
	lcc/=LCC &
	valBarrier:barrier &
	valSensor:bSensor &
	valRlight:Red.light &
	valYlight:Yellow.light &
	bState(valBarrier)=Opened &
	Red.lState(valRlight)=Off &
	Yellow.lState(valYlight)=Off
    THEN
	ANY new WHERE new:LCC-lcc	THEN
	    lcc := lcc \/ {new} ||
	    lcc_barrier:= lcc_barrier \/ {(new|->valBarrier)} ||
	    lcc_sensor:= lcc_sensor \/ {(new|->valSensor)} ||
	    state:= state \/ {(new|->Deactivated)} ||
	    redLight:= redLight \/ {(new|->valRlight)} ||
	    yellowLight:= yellowLight \/ {(new|->valYlight)} ||
	    mode:= mode \/ {(new|->Safe)} ||
	    obj:= new
	END
    END;
    
    supLcc(obj) =
    PRE
	obj <: lcc
    THEN
	lcc := lcc - obj ||
	lcc_barrier := obj <<| lcc_barrier ||
	lcc_sensor := obj <<| lcc_sensor ||
	state := obj <<| state ||
	redLight := obj <<| redLight ||
	yellowLight := obj <<| yellowLight	
    END;

    newLccBarrier(valLcc, valBarrier) =
    PRE
	valLcc : lcc &
	valBarrier : barrier
    THEN
	lcc_barrier := lcc_barrier <+ {valLcc |-> valBarrier}
    END   
END
