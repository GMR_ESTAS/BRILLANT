(2003-06-04)

Project status
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| COMPONENT      | TC | POG | Obv  | nPO | nUn | %Pr | B0C |  C  | Ada | C++ |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| Acq_1          | OK | OK  |  710 |  14 |  14 |   0 | OK  |     |     |     |
| Acq_2          | OK | OK  |   40 |  22 |  22 |   0 | OK  |  -  | OK  |  -  |
| Arithmetic_1   | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Arithmetic_2   | OK | OK  |   22 |   4 |   4 |   0 | OK  |  -  | OK  |  -  |
| Constants_L_1  | OK | OK  |    1 |   0 |   0 | 100 | OK  |     |     |     |
| Constants_L_2  | OK | OK  |   33 |   7 |   7 |   0 | OK  |  -  | OK  |  -  |
| Constants_SW_1 | OK | OK  |    1 |   0 |   0 | 100 | OK  |     |     |     |
| Constants_SW_2 | OK | OK  |   13 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_A_1      | OK | OK  |    7 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_A_2      | OK | OK  |   12 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_B_1      | OK | OK  |   19 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_B_2      | OK | OK  |   26 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_L_1      | OK | OK  |   69 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_L_2      | OK | OK  |   98 |   4 |   4 |   0 | OK  |  -  | OK  |  -  |
| Cycle_R_1      | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_R_2      | OK | OK  |   31 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_S_1      | OK | OK  |   44 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_S_2      | OK | OK  |   48 |   2 |   2 |   0 | OK  |  -  | OK  |  -  |
| Cycle_W_1      | OK | OK  |   27 |   2 |   2 |   0 | OK  |     |     |     |
| Cycle_W_2      | OK | OK  |  174 |   6 |   6 |   0 | OK  |  -  | OK  |  -  |
| Cycle_X_1      | OK | OK  |   44 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_X_2      | OK | OK  |   60 |   2 |   2 |   0 | OK  |  -  | OK  |  -  |
| Cycle_Y_1      | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_Y_2      | OK | OK  |   34 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Emis_1         | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Emis_2         | OK | OK  |    4 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| MinMax_1       | OK | OK  |    2 |   0 |   0 | 100 | OK  |     |     |     |
| MinMax_2       | OK | OK  |   14 |   3 |   3 |   0 | OK  |  -  | OK  |  -  |
| Princ_1        | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Princ_2        | OK | OK  |   14 |   7 |   7 |   0 | OK  |  -  | OK  |  -  |
| Service_A_1    | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Service_A_2    | OK | OK  |    8 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_B_1    | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Service_B_2    | OK | OK  |   14 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_L_1    | OK | OK  |  307 |  15 |  15 |   0 | OK  |     |     |     |
| Service_L_2    | OK | OK  |  229 |  63 |  63 |   0 | OK  |  -  | OK  |  -  |
| Service_R_1    | OK | OK  |   64 |   4 |   4 |   0 | OK  |     |     |     |
| Service_R_2    | OK | OK  |   86 |   3 |   3 |   0 | OK  |  -  | OK  |  -  |
| Service_S_1    | OK | OK  |  125 |  14 |  14 |   0 | OK  |     |     |     |
| Service_S_2    | OK | OK  |  101 |  21 |  21 |   0 | OK  |  -  | OK  |  -  |
| Service_W_1    | OK | OK  |  285 |  34 |  34 |   0 | OK  |     |     |     |
| Service_W_2    | OK | OK  |  334 |  24 |  24 |   0 | OK  |  -  | OK  |  -  |
| Service_X_1    | OK | OK  |   11 |   6 |   6 |   0 | OK  |     |     |     |
| Service_X_2    | OK | OK  |   12 |  42 |  42 |   0 | OK  |  -  | OK  |  -  |
| Service_Y_1    | OK | OK  |    5 |   0 |   0 | 100 | OK  |     |     |     |
| Service_Y_2    | OK | OK  |   10 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| TOTAL          | OK | OK  | 3183 | 299 | 299 |   0 | OK  |  -  | OK  |  -  |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+

Force 0

Project status
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| COMPONENT      | TC | POG | Obv  | nPO | nUn | %Pr | B0C |  C  | Ada | C++ |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| Acq_1          | OK | OK  |  710 |  14 |   0 | 100 | OK  |     |     |     |
| Acq_2          | OK | OK  |   40 |  22 |  21 |   4 | OK  |  -  | OK  |  -  |
| Arithmetic_1   | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Arithmetic_2   | OK | OK  |   22 |   4 |   0 | 100 | OK  |  -  | OK  |  -  |
| Constants_L_1  | OK | OK  |    1 |   0 |   0 | 100 | OK  |     |     |     |
| Constants_L_2  | OK | OK  |   33 |   7 |   0 | 100 | OK  |  -  | OK  |  -  |
| Constants_SW_1 | OK | OK  |    1 |   0 |   0 | 100 | OK  |     |     |     |
| Constants_SW_2 | OK | OK  |   13 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_A_1      | OK | OK  |    7 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_A_2      | OK | OK  |   12 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_B_1      | OK | OK  |   19 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_B_2      | OK | OK  |   26 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_L_1      | OK | OK  |   69 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_L_2      | OK | OK  |   98 |   4 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_R_1      | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_R_2      | OK | OK  |   31 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_S_1      | OK | OK  |   44 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_S_2      | OK | OK  |   48 |   2 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_W_1      | OK | OK  |   27 |   2 |   0 | 100 | OK  |     |     |     |
| Cycle_W_2      | OK | OK  |  174 |   6 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_X_1      | OK | OK  |   44 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_X_2      | OK | OK  |   60 |   2 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_Y_1      | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_Y_2      | OK | OK  |   34 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Emis_1         | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Emis_2         | OK | OK  |    4 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| MinMax_1       | OK | OK  |    2 |   0 |   0 | 100 | OK  |     |     |     |
| MinMax_2       | OK | OK  |   14 |   3 |   0 | 100 | OK  |  -  | OK  |  -  |
| Princ_1        | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Princ_2        | OK | OK  |   14 |   7 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_A_1    | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Service_A_2    | OK | OK  |    8 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_B_1    | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Service_B_2    | OK | OK  |   14 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_L_1    | OK | OK  |  307 |  15 |   8 |  46 | OK  |     |     |     |
| Service_L_2    | OK | OK  |  229 |  63 |  21 |  66 | OK  |  -  | OK  |  -  |
| Service_R_1    | OK | OK  |   64 |   4 |   0 | 100 | OK  |     |     |     |
| Service_R_2    | OK | OK  |   86 |   3 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_S_1    | OK | OK  |  125 |  14 |   7 |  50 | OK  |     |     |     |
| Service_S_2    | OK | OK  |  101 |  21 |   3 |  85 | OK  |  -  | OK  |  -  |
| Service_W_1    | OK | OK  |  285 |  34 |   2 |  94 | OK  |     |     |     |
| Service_W_2    | OK | OK  |  334 |  24 |   1 |  95 | OK  |  -  | OK  |  -  |
| Service_X_1    | OK | OK  |   11 |   6 |   4 |  33 | OK  |     |     |     |
| Service_X_2    | OK | OK  |   12 |  42 |  12 |  71 | OK  |  -  | OK  |  -  |
| Service_Y_1    | OK | OK  |    5 |   0 |   0 | 100 | OK  |     |     |     |
| Service_Y_2    | OK | OK  |   10 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| TOTAL          | OK | OK  | 3183 | 299 |  79 |  73 | OK  |  -  | OK  |  -  |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+

Force 1
Project status
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| COMPONENT      | TC | POG | Obv  | nPO | nUn | %Pr | B0C |  C  | Ada | C++ |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| Acq_1          | OK | OK  |  710 |  14 |   0 | 100 | OK  |     |     |     |
| Acq_2          | OK | OK  |   40 |  22 |  20 |   9 | OK  |  -  | OK  |  -  |
| Arithmetic_1   | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Arithmetic_2   | OK | OK  |   22 |   4 |   0 | 100 | OK  |  -  | OK  |  -  |
| Constants_L_1  | OK | OK  |    1 |   0 |   0 | 100 | OK  |     |     |     |
| Constants_L_2  | OK | OK  |   33 |   7 |   0 | 100 | OK  |  -  | OK  |  -  |
| Constants_SW_1 | OK | OK  |    1 |   0 |   0 | 100 | OK  |     |     |     |
| Constants_SW_2 | OK | OK  |   13 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_A_1      | OK | OK  |    7 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_A_2      | OK | OK  |   12 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_B_1      | OK | OK  |   19 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_B_2      | OK | OK  |   26 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_L_1      | OK | OK  |   69 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_L_2      | OK | OK  |   98 |   4 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_R_1      | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_R_2      | OK | OK  |   31 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_S_1      | OK | OK  |   44 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_S_2      | OK | OK  |   48 |   2 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_W_1      | OK | OK  |   27 |   2 |   0 | 100 | OK  |     |     |     |
| Cycle_W_2      | OK | OK  |  174 |   6 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_X_1      | OK | OK  |   44 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_X_2      | OK | OK  |   60 |   2 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_Y_1      | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_Y_2      | OK | OK  |   34 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Emis_1         | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Emis_2         | OK | OK  |    4 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| MinMax_1       | OK | OK  |    2 |   0 |   0 | 100 | OK  |     |     |     |
| MinMax_2       | OK | OK  |   14 |   3 |   0 | 100 | OK  |  -  | OK  |  -  |
| Princ_1        | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Princ_2        | OK | OK  |   14 |   7 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_A_1    | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Service_A_2    | OK | OK  |    8 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_B_1    | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Service_B_2    | OK | OK  |   14 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_L_1    | OK | OK  |  307 |  15 |   4 |  73 | OK  |     |     |     |
| Service_L_2    | OK | OK  |  229 |  63 |  20 |  68 | OK  |  -  | OK  |  -  |
| Service_R_1    | OK | OK  |   64 |   4 |   0 | 100 | OK  |     |     |     |
| Service_R_2    | OK | OK  |   86 |   3 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_S_1    | OK | OK  |  125 |  14 |   4 |  71 | OK  |     |     |     |
| Service_S_2    | OK | OK  |  101 |  21 |   3 |  85 | OK  |  -  | OK  |  -  |
| Service_W_1    | OK | OK  |  285 |  34 |   2 |  94 | OK  |     |     |     |
| Service_W_2    | OK | OK  |  334 |  24 |   1 |  95 | OK  |  -  | OK  |  -  |
| Service_X_1    | OK | OK  |   11 |   6 |   4 |  33 | OK  |     |     |     |
| Service_X_2    | OK | OK  |   12 |  42 |  12 |  71 | OK  |  -  | OK  |  -  |
| Service_Y_1    | OK | OK  |    5 |   0 |   0 | 100 | OK  |     |     |     |
| Service_Y_2    | OK | OK  |   10 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| TOTAL          | OK | OK  | 3183 | 299 |  70 |  76 | OK  |  -  | OK  |  -  |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+

Force 2:
Project status
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| COMPONENT      | TC | POG | Obv  | nPO | nUn | %Pr | B0C |  C  | Ada | C++ |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| Acq_1          | OK | OK  |  710 |  14 |   0 | 100 | OK  |     |     |     |
| Acq_2          | OK | OK  |   40 |  22 |  20 |   9 | OK  |  -  | OK  |  -  |
| Arithmetic_1   | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Arithmetic_2   | OK | OK  |   22 |   4 |   0 | 100 | OK  |  -  | OK  |  -  |
| Constants_L_1  | OK | OK  |    1 |   0 |   0 | 100 | OK  |     |     |     |
| Constants_L_2  | OK | OK  |   33 |   7 |   0 | 100 | OK  |  -  | OK  |  -  |
| Constants_SW_1 | OK | OK  |    1 |   0 |   0 | 100 | OK  |     |     |     |
| Constants_SW_2 | OK | OK  |   13 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_A_1      | OK | OK  |    7 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_A_2      | OK | OK  |   12 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_B_1      | OK | OK  |   19 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_B_2      | OK | OK  |   26 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_L_1      | OK | OK  |   69 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_L_2      | OK | OK  |   98 |   4 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_R_1      | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_R_2      | OK | OK  |   31 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_S_1      | OK | OK  |   44 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_S_2      | OK | OK  |   48 |   2 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_W_1      | OK | OK  |   27 |   2 |   0 | 100 | OK  |     |     |     |
| Cycle_W_2      | OK | OK  |  174 |   6 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_X_1      | OK | OK  |   44 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_X_2      | OK | OK  |   60 |   2 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_Y_1      | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_Y_2      | OK | OK  |   34 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Emis_1         | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Emis_2         | OK | OK  |    4 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| MinMax_1       | OK | OK  |    2 |   0 |   0 | 100 | OK  |     |     |     |
| MinMax_2       | OK | OK  |   14 |   3 |   0 | 100 | OK  |  -  | OK  |  -  |
| Princ_1        | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Princ_2        | OK | OK  |   14 |   7 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_A_1    | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Service_A_2    | OK | OK  |    8 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_B_1    | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Service_B_2    | OK | OK  |   14 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_L_1    | OK | OK  |  307 |  15 |   4 |  73 | OK  |     |     |     |
| Service_L_2    | OK | OK  |  229 |  63 |  20 |  68 | OK  |  -  | OK  |  -  |
| Service_R_1    | OK | OK  |   64 |   4 |   0 | 100 | OK  |     |     |     |
| Service_R_2    | OK | OK  |   86 |   3 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_S_1    | OK | OK  |  125 |  14 |   4 |  71 | OK  |     |     |     |
| Service_S_2    | OK | OK  |  101 |  21 |   3 |  85 | OK  |  -  | OK  |  -  |
| Service_W_1    | OK | OK  |  285 |  34 |   2 |  94 | OK  |     |     |     |
| Service_W_2    | OK | OK  |  334 |  24 |   1 |  95 | OK  |  -  | OK  |  -  |
| Service_X_1    | OK | OK  |   11 |   6 |   4 |  33 | OK  |     |     |     |
| Service_X_2    | OK | OK  |   12 |  42 |  12 |  71 | OK  |  -  | OK  |  -  |
| Service_Y_1    | OK | OK  |    5 |   0 |   0 | 100 | OK  |     |     |     |
| Service_Y_2    | OK | OK  |   10 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| TOTAL          | OK | OK  | 3183 | 299 |  70 |  76 | OK  |  -  | OK  |  -  |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+

Force 3



tatus
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| COMPONENT      | TC | POG | Obv  | nPO | nUn | %Pr | B0C |  C  | Ada | C++ |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| Acq_1          | OK | OK  |  710 |  14 |   0 | 100 | OK  |     |     |     |
| Acq_2          | OK | OK  |   40 |  22 |  20 |   9 | OK  |  -  | OK  |  -  |
| Arithmetic_1   | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Arithmetic_2   | OK | OK  |   22 |   4 |   0 | 100 | OK  |  -  | OK  |  -  |
| Constants_L_1  | OK | OK  |    1 |   0 |   0 | 100 | OK  |     |     |     |
| Constants_L_2  | OK | OK  |   33 |   7 |   0 | 100 | OK  |  -  | OK  |  -  |
| Constants_SW_1 | OK | OK  |    1 |   0 |   0 | 100 | OK  |     |     |     |
| Constants_SW_2 | OK | OK  |   13 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_A_1      | OK | OK  |    7 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_A_2      | OK | OK  |   12 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_B_1      | OK | OK  |   19 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_B_2      | OK | OK  |   26 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_L_1      | OK | OK  |   69 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_L_2      | OK | OK  |   98 |   4 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_R_1      | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_R_2      | OK | OK  |   31 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_S_1      | OK | OK  |   44 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_S_2      | OK | OK  |   48 |   2 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_W_1      | OK | OK  |   27 |   2 |   0 | 100 | OK  |     |     |     |
| Cycle_W_2      | OK | OK  |  174 |   6 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_X_1      | OK | OK  |   44 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_X_2      | OK | OK  |   60 |   2 |   0 | 100 | OK  |  -  | OK  |  -  |
| Cycle_Y_1      | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Cycle_Y_2      | OK | OK  |   34 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Emis_1         | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Emis_2         | OK | OK  |    4 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| MinMax_1       | OK | OK  |    2 |   0 |   0 | 100 | OK  |     |     |     |
| MinMax_2       | OK | OK  |   14 |   3 |   0 | 100 | OK  |  -  | OK  |  -  |
| Princ_1        | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Princ_2        | OK | OK  |   14 |   7 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_A_1    | OK | OK  |    3 |   0 |   0 | 100 | OK  |     |     |     |
| Service_A_2    | OK | OK  |    8 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_B_1    | OK | OK  |   11 |   0 |   0 | 100 | OK  |     |     |     |
| Service_B_2    | OK | OK  |   14 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_L_1    | OK | OK  |  307 |  15 |   4 |  73 | OK  |     |     |     |
| Service_L_2    | OK | OK  |  229 |  63 |  18 |  71 | OK  |  -  | OK  |  -  |
| Service_R_1    | OK | OK  |   64 |   4 |   0 | 100 | OK  |     |     |     |
| Service_R_2    | OK | OK  |   86 |   3 |   0 | 100 | OK  |  -  | OK  |  -  |
| Service_S_1    | OK | OK  |  125 |  14 |   4 |  71 | OK  |     |     |     |
| Service_S_2    | OK | OK  |  101 |  21 |   1 |  95 | OK  |  -  | OK  |  -  |
| Service_W_1    | OK | OK  |  285 |  34 |   2 |  94 | OK  |     |     |     |
| Service_W_2    | OK | OK  |  334 |  24 |   1 |  95 | OK  |  -  | OK  |  -  |
| Service_X_1    | OK | OK  |   11 |   6 |   4 |  33 | OK  |     |     |     |
| Service_X_2    | OK | OK  |   12 |  42 |  12 |  71 | OK  |  -  | OK  |  -  |
| Service_Y_1    | OK | OK  |    5 |   0 |   0 | 100 | OK  |     |     |     |
| Service_Y_2    | OK | OK  |   10 |   0 |   0 | 100 | OK  |  -  | OK  |  -  |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+
| TOTAL          | OK | OK  | 3183 | 299 |  66 |  77 | OK  |  -  | OK  |  -  |
+----------------+----+-----+------+-----+-----+-----+-----+-----+-----+-----+


