/*?
	Definition de la fonction de Ackermann
	Mahematiques Discretes / fonctions recursives

	(c) NaBLa Project - mai 2004
          C. Attiogbe
Rappel :
La fonction Ackermann est une fonction totale, calculable mais non recursive primitive.
Elle demontre (contre-exemple) que toute fonction calculable n'est pas primitive recursive.

Elle croit plus vite que la fonction exponentielle.
?*/

MACHINE
	Ackermann
CONSTANTS
	Ack

PROPERTIES
	Ack : INT * INT --> INT

&	!yy.(yy: INT => Ack(0, yy) = yy+1)
&	!xx.(xx: INT => Ack(xx, 0) = Ack(xx-1, 1))
&	!(xx,yy).(xx : INT & yy : INT & xx /= 0 & yy/= 0 => 
		Ack(xx,yy) = Ack(xx-1, Ack(xx, yy-1)))

OPERATIONS
res <-- Ackerm(xx, yy) = /*? the public operation to compute Ackermann ... ?*/ 
	PRE
		xx : INT
	& 	yy : INT
	THEN
		res := Ack(xx,yy)
	END
END

/*?
Proof status (Atelier B - 3.6)
+------------------+-------+------+-------+-------+------+-----+
|                  | NbObv | NbPO | NbPRi | NbPRa | NbUn | %Pr |
+------------------+-------+------+-------+-------+------+-----+
|   Initialisation |     1 |    0 |     0 |     0 |    0 | 100 |
+------------------+-------+------+-------+-------+------+-----+
| Ackermann        |     1 |    0 |     0 |     0 |    0 | 100 |
+------------------+-------+------+-------+-------+------+-----+
?*/
