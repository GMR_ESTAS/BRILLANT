 

Require Import Blib.

Theorem op:
forall fibo nn x, ( 
 (In (total_function BN BN ) fibo) -> 
((app fibo (0) _) = 0) -> 
((app fibo (1) _) = 1) -> 
forall nn, (((((In BN nn) /\ (nn >= 0)) -> ((app fibo ((nn + 2)) _) = ((app fibo ((nn + 1)) _) + (app fibo (nn) _)))))) -> 
(In BN nn)
  -> (
 (x = x) ) )
 .
intuition.
Qed.
 