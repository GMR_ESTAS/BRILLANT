 

Require Import Blib.

Theorem op:
forall PILE XX elt pile pop_p push pvide top_p, ( 
 (In (Power_set1 BZ) PILE) -> 
(In PILE pvide) -> 
(In (partial_function PILE XX) top_p) -> 
(In (partial_function PILE PILE) pop_p) -> 
(In (total_function (XX * PILE) PILE ) push) -> 
forall elt pp, (((((In XX elt) /\ (In PILE pp)) -> ((app top_p ((app push (elt,pp) _)) _) = elt)))) -> 
forall elt pp, (((((In XX elt) /\ (In PILE pp)) -> ((app pop_p ((app push (elt,pp) _)) _) = pp)))) -> 
(In PILE pile) -> 
(In XX elt)
  -> (
 (In PILE (app push (elt,pile) _)) ) )
 .
intuition.
Qed.
 