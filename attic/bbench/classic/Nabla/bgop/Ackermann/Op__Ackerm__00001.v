 

Require Import Blib.

Theorem op:
forall Ack x xx yy, ( 
 (In (total_function (BZ * BZ) BZ ) Ack) -> 
forall yy, ((((In BZ yy) -> ((app Ack (0,yy) _) = (yy + 1))))) -> 
forall xx, ((((In BZ xx) -> ((app Ack (xx,0) _) = (app Ack ((xx - 1),1) _))))) -> 
forall xx yy, (((((((In BZ xx) /\ (In BZ yy)) /\ (xx <> 0)) /\ (yy <> 0)) -> ((app Ack (xx,yy) _) = (app Ack ((xx - 1),(app Ack (xx,(yy - 1)) _)) _))))) -> 
(In BZ xx) -> 
(In BZ yy)
  -> (
 (x = x) ) )
 .
intuition.
Qed.
 