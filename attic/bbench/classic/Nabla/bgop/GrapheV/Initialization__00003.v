 

Require Import Blib.

Theorem op:
forall Edges NODE Weight, ( 
 (In (Power_set1 BZ) NODE) -> 
(In (relation NODE NODE) Edges) -> 
(In (total_function Edges BN ) Weight)
  -> (
 ((domain (Empty_set _)) = (Empty_set _)) ) )
 .
intuition.
Qed.
 