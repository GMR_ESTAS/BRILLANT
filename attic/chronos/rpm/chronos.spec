Summary:	A process killer
Name:		chronos
Version:	0.1
Release:	0
License:	GPL
Group:		Devel
Source:		%{name}-%{version}.tar.bz2
Url:		http://www3.inrets.fr/estas/colin
BuildRoot:	%_tmppath/%name
BuildRequires:	libc6, libncurses5, ocaml, ocaml-base, debhelper

%description
Launchs commandline during a given time (see --help for possible options)
Note the command line should be protected by quotes or double quotes.


%prep

%setup -q -n %{name}

%build

%make

%install 

%clean

%files 
%defattr(-,root,root,-)
/%{_bindir}/%{name}
/%{_sharedir}/man/man1/%{name}.1.gz

%changelog

* Tue Oct 7 2003 Jerome Rocheteau <rocheteau@phenix>
- Initial Release.
