(*$Id$*)

(* This <<time-killer>> is a hybride version which have been adapted
   for the use of the PhoX program. This adaptation had been made with
   the execution returned codes : to homogenize this execution with the 
   Makefiles filter (various case of file redirection)
*)

open Str             (*split & regexp*)
open Unix            (*launching command*)
open Arg             (*arguments processing*)


let debug_set=ref false

(** Process' time limit definition *)
let timeLimit=ref 300

(** The time's limit initialisation function *)
let setTimeLimit t=
  timeLimit:=t

(** The command line format definition  *)
let usage_message=
  Sys.executable_name ^ " [options] \"commandline\"\n \
   Launchs commandline during a given time (see --help for possible options)\n \
   Note the command line should be protected by quotes or double quotes"

(** Options definitions  *)
let rec options=
  [
    ("--help",
     Arg.Unit(fun () -> 
                Arg.usage options usage_message; raise Exit),
     "\t\t\t displays this help")
    ;(** Help option*)
    ("-help",
     Arg.Unit(fun () -> 
                Arg.usage options usage_message; raise Exit),
     "\t\t\t See --help")
    ; (** Help option too*)
    ( "--time", Int(setTimeLimit),
      ("\t\t\t execute the commandline during at most t seconds " ^ 
       "(defaults to " ^ (string_of_int !timeLimit) ^ " seconds)"))
    ; (** Process' time limit*)
   ("--debug",
    Arg.Set(debug_set),
    ", \t\t\t debug mode (default to false)")
     ;
    ]


let debug msg=
  if !debug_set
  then begin
    print_endline msg; flush Pervasives.stdout
  end


let debugProcessus procStatus=
  if !debug_set
  then begin
    let pid=fst procStatus in
    let status=snd procStatus in
    let statusMsg=
      match status with
	| WEXITED(exitNumb) -> "exited, exit number : " ^ (string_of_int exitNumb)
	| WSIGNALED(sigNumb) -> "killed, signal number : " ^ (string_of_int sigNumb)
	| WSTOPPED(sigNumb) -> "stopped, signal number : " ^ (string_of_int sigNumb)
    in
    let wholeMsg="Process " ^ (string_of_int pid) ^ " " ^ statusMsg in
      print_endline wholeMsg; flush Pervasives.stdout
  end


(** Main process which launch a shell command line and kill it if it doesn't 
    terminated before a time limit.
    By convention, the process will exit with code :
    0 -> in case which the proof process succeded
    1 -> in case which the proof process failed
    2 -> in case which the proof process looped
*)
let process commandline=
  match (Unix.fork ()) with
    | 0 -> 
	(** 1st son : time-keeping *)
	Unix.sleep !timeLimit 
    | pidwait -> 
	debug ("Launched chrono, pid " ^ (string_of_int pidwait));
	match (Unix.fork ()) with
	  | 0 -> 
	      (** 2nd son : command launch *)
	      let cmd_list = Str.split (Str.regexp "[ ]") commandline in
	      let cmd_array = StdLabels.Array.of_list cmd_list in
		ignore (Unix.execvp cmd_array.(0) cmd_array)
	  | pidlaunch ->
	      debug ("Launched command, pid " ^ (string_of_int pidlaunch));
	      let return1st=Unix.wait () in
		debugProcessus return1st;
		match return1st with
		  | (pid, WEXITED (0)) when pid=pidlaunch ->  
		    (** terminating proof process with SOLVED result*)
		      Process.killFamily pidwait;
		      debug ("Just killed chrono, pid " ^ (string_of_int pidwait));
		      let return2nd=Unix.wait () in
			debugProcessus return2nd;
			flush Pervasives.stdout ;
			exit 0
			  
		  | (pid, _) when pid=pidlaunch -> 
		    (** terminating proof process with UNSOLVED result*)
		      Process.killFamily pidwait;
		      debug ("Just killed chrono, pid " ^ (string_of_int pidwait));
		      let return2nd=Unix.wait () in
			debugProcessus return2nd;
			flush Pervasives.stdout ;
			exit 1 
			  
		  | (pid, _) when pid=pidwait -> 
		    (** looping proof process*)
		      Process.killFamily pidlaunch;
		      debug ("Just killed command, pid " ^ (string_of_int pidlaunch));
		      let return2nd=Unix.wait () in
			debugProcessus return2nd;
			flush Pervasives.stdout ;
			exit 2

		  | _ -> 
		    (** other event...*)
		      debug "Something's wrong here...";
		      flush Pervasives.stdout

(** main function*)
let _=
  try
    Arg.parse options process usage_message;
  with Exit -> ()

