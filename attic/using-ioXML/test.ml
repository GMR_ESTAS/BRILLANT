
(* hiw : how ioxml works *)
type hiw =
  | ConstrInt of int
  | ConstrChar of char
  | ConstrTuple of rec1_type * float list
and rec1_type =
  | ConstrArray of bool array
  | ConstrHugeType of int * char * float * bool * string * (int list) * hiw

(* I did not insert polymorphic types, but the ioXML README shows how it is used in this case *)


let example_type =
  ConstrTuple(
    ConstrHugeType(42, 'a', 3.14, true, "hello", [13; 12; 11],
		   ConstrTuple(ConstrArray([|true; false; true|]), [1.1; 2.1; 3.1]))
      ,
    [1.0; 2.0; 3.1])

let _ =
  let ppf = Format.std_formatter in
    IoXML.xprint ppf "@[%a@]@."
      (xprint_hiw) example_type
