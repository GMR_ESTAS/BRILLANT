(*******************************************************)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Basic B Symbol Library *)

type t = 
    Type of int
  | Term of Type.t 
  | Theo of Term.t 

let table = ref ([] : (string * t) list)

let find = fun s -> 
  if List.mem_assoc s !table
  then List.assoc s !table
  else raise Not_found

let find_type = fun s -> 
  try let r = find s in 
    match r with 
	Type n -> n 
      | _ -> assert false
  with Not_found -> raise Not_found 

let find_term = fun s -> 
  try let r = find s in 
    match r with 
	Term n -> n 
      | _ -> assert false
  with Not_found -> raise Not_found 

let find_theo = fun s -> 
  try let r = find s in 
    match r with 
	Theo n -> n 
      | _ -> assert false
  with Not_found -> raise Not_found 

let add = fun s t -> 
  if List.mem_assoc s !table 
  then assert false 
  else table := (s,t) :: !table

let reset = fun () -> table := []

let init = fun () -> 
  let _ = reset () in 
  let _ = add "prop" (Type 0) in 
  let _ = add "int" (Type 0) in 
  let _ = add "fun" (Type 2) in 
  let _ = add "prod" (Type 2) in 
  let _ = add "app" (Term (Type.abs "a" (Type.abs "b" (Type.arr ((Type.arr (Type.var "a",Type.var "b")),(Type.arr (Type.var "a",Type.var "b"))))))) in 
  let _ = add "abs" (Term (Type.abs "a" (Type.abs "b" (Type.arr ((Type.arr (Type.var "a",Type.var "b")),(Type.arr (Type.var "a",Type.var "b"))))))) in 
  let _ = add "true" (Term Type.prop) in 
  let _ = add "false" (Term Type.prop) in 
  let _ = add "not" (Term (Type.arr (Type.prop,Type.prop))) in 
  let _ = add "and" (Term (Type.arr (Type.prop,(Type.arr (Type.prop,Type.prop))))) in 
  let _ = add "or" (Term (Type.arr (Type.prop,(Type.arr (Type.prop,Type.prop))))) in 
  let _ = add "to" (Term (Type.arr (Type.prop,(Type.arr (Type.prop,Type.prop))))) in 
  let _ = add "iff" (Term (Type.arr (Type.prop,(Type.arr (Type.prop,Type.prop))))) in 
  let _ = add "eq" (Term (Type.abs "a" (Type.arr (Type.var "a",(Type.arr (Type.var "a",Type.prop)))))) in 
  let _ = add "forall" (Term (Type.abs "a" (Type.arr (Type.var "a",Type.prop)))) in 
  let _ = add "exists" (Term (Type.abs "a" (Type.arr (Type.var "a",Type.prop)))) in 
    () 

let text = function 
    "abs" -> "\\"
  | "true" -> "true" 
  | "false" -> "false"
  | "not" -> "~"
  | "and" -> "&"
  | "or" -> "||"
  | "to" -> "->"
  | "iff" -> "<->"
  | "eq" -> "="
  | "forall" -> "/\\"
  | "exists" -> "\\/"
  | s -> s

let latex = function 
    "abs" -> "\\lambda "
  | "true" -> "\\top " 
  | "false" -> "\\bot "
  | "not" -> "\\neg "
  | "and" -> "\\wedge"
  | "or" -> "\\vee"
  | "to" -> "\\rightarrow"
  | "iff" -> "\\leftrightarrow"
  | "eq" -> "="
  | "forall" -> "\\forall "
  | "exists" -> "\\exists "
  | s -> "\\" ^ s ^ " "

let phox = function 
    "abs" -> "\\"
  | "true" -> "True" 
  | "false" -> "False"
  | "not" -> "~"
  | "and" -> "&"
  | "to" -> "->"
  | "iff" -> "<->"
  | "eq" -> "="
  | "forall" -> "/\\"
  | "exists" -> "\\/"
  | s -> s
