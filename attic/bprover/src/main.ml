(*******************************************************)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Bprover *)

let usage = "usage: "^(Filename.basename Sys.executable_name)^" [options] <files>" 

let rec options = 
  [
    ("-p",Arg.String(fun x -> Mule.select x),"\t\tselect the default prover")
    ;
    ("-prover",Arg.String(fun x -> Mule.select x),"")
    ;
    ("--prover",Arg.String(fun x -> Mule.select x),"")
    ;
    ("-h",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"\t\tprint this help")
    ;
    ("-help",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"")
    ;
    ("--help",Arg.Unit(fun () -> Arg.usage options usage; exit 0),"")
  ]

let files = ref ([] : string list) 

let extension = ".xml"

let check = fun () -> 
  let rec f = function
      [] -> []
    | x::l -> 
	(try 
	  if Filename.check_suffix x (extension)
	  then x ::(f l)
	  else failwith ("warning: file '" ^ x ^ "' must have a '" ^ (extension) ^ "' extension\n") 
	with Failure msg -> let _ = output_string stderr msg in f l)
  in files := f !files

let rec process = fun () -> 
  let rec f = function 
      [] -> raise Exit
    | x::l -> 
	let ic = open_in x in 
	let oc = open_out ((Filename.chop_suffix x (extension)) ^ (!Mule.extension)) in 
          try Base.init (); Mule.init oc; Scan.iter (Mule.print oc) ic
          with Exit -> close_in ic; Mule.achieve oc; close_out oc; f l
  in f !files
	  
let main = fun () -> 
  try 
    let _ = Arg.parse options (fun x -> files := x :: !files) usage in
    let _ = check () in 
    let _ = process () in ()
  with 
      Exit -> flush stdout; exit 0
    | Failure e -> flush stdout; output_string stderr e; flush stderr; exit 2
	
let _ = main ()
