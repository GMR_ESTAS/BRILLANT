(*******************************************************)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Term Library *)

(** Terms are [t ::= x | c | d = t | t t | \x t]. *)
type t = 
    Var of string
  | Cst of Type.t Symb.t
  | App of t * t
  | Abs of string * t

(** *)
let rec subst = fun f -> function 
    Var x -> if List.mem_assoc x f then List.assoc x f else Var x
  | Cst c -> Cst c
  | App (u,v) -> App ((subst f u),(subst f v)) 
  | Abs (x,t) -> if List.mem_assoc x f then Abs (x,t) else Abs (x,subst f t)

(** *)
let rec close = fun t -> function
    [] -> t
  | x::l -> close (Abs (x,t)) l
    
(** The function [mem x t] is true if [x] is a free variable of [t] and false else. *)
let rec mem = fun e -> function 
    Var x -> x = e 
  | Cst c -> false
  | App (u,v) -> (mem e u) || (mem e v) 
  | Abs (x,t) -> if x = e then false else mem e t

(** The function [base t] returns the free variables list of [t]. *)
let rec base = fun t -> 
  let rec f = function 
      Var x -> if x = "" then [] else [x] 
    | Cst c -> []
    | App (u,v) -> List.append (f u) (f v)
    | Abs (x,t) -> Misc.minus x (base t)
  in Misc.set_of (List.sort String.compare (f t))

(** The function [compare u v] returns 0 if [u = v], -1 if [u < v] and 1 if [u > v]. *)
let compare = 
  let rec f = function 
      Var x -> 
	begin function 
	    Var x' -> String.compare x x'
	  | _ -> -1
	end 
    | Cst c -> 
	begin function 
	    Var x -> 1
	  | Cst c' -> Symb.compare Type.compare c c'
	  | _ -> -1
	end 
    | App (u,v) -> 
	begin function 
	    Var x -> 1 
	  | Cst c -> 1 
	  | App (u',v') -> 
	      begin match f u u' with 
		  0 -> f v v'
		| n -> n
	      end 
	  | _ -> -1
	end 
    | Abs (x,t) -> 
	begin function 
	    Abs (x',t') -> f t (subst [x',Var x] t')
	  | _ -> 1
	end 
  in f 

(** The function [eval t] returns the term [t] normal form. *)
let rec eval = function 
    Var x -> Var x 
  | Cst c -> Cst c
  | Abs (x,t) -> Abs (x,eval t) 
  | App (u,v) -> let r = eval u in 
      begin match r with 
	  Abs (x,t) -> eval (subst [x,v] t)
	| _ -> App (r,eval v)
      end 
  
(** The function [check (gamma,delta,epsilon,lambda)] defines the type checking for all terms in [lambda]. *)
let check = 
  let rec f = fun m -> function 
      (gamma,epsilon,(Var x,a) :: lambda) -> 
	if List.mem_assoc x gamma 
	then f m (gamma,((a,(List.assoc x gamma))::epsilon),lambda) 
	else assert false
    | (gamma,epsilon,(Cst c,a) :: lambda) -> 
	f m (gamma,((a,(Symb.contents_of c))::epsilon),lambda) 
    | (gamma,epsilon,(App (u,v),a) :: lambda) -> 
	let b = Type.Var (string_of_int m) in 
	let c = Type.Var (string_of_int (m+1)) 
	in f (m+2) (gamma,((b,(Type.arr (c,a)))::epsilon),((u,b)::(v,c)::lambda)) 
    | (gamma,epsilon,(Abs (x,t),a)::lambda) -> 
	let b = Type.Var (string_of_int m) in 
	let c = Type.Var (string_of_int (m+1)) 
	in f (m+2) (((x,b)::gamma),((a,(Type.arr (b,c)))::epsilon),((t,c)::lambda)) 
    | (gamma,epsilon,[])-> epsilon 
  in f 0

(** The function [infer t] provides the close term [t] main type. *)
let infer = fun t -> 
  let a = Type.Var (string_of_int (-1)) in 
  let epsilon = check ([],[],[(t,a)]) in 
  let sigma = Type.unify [] epsilon in 
  let ty = Type.subst sigma a 
  in Type.close ty (Type.base ty)
