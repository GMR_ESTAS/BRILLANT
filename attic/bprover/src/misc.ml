(*******************************************************)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Miscellenous Library *)

(** The function [set_of l] returns the list [l] doublon free list. *)
let set_of = fun l -> 
  let rec f = fun r -> function 
      [] -> List.rev r
    | x::l -> if List.mem x r then f r l else f (x::r) l
  in f [] l

let rec minus = fun e -> function 
    [] -> [] 
  | x :: l -> if x = e then minus e l else x :: (minus e l)
