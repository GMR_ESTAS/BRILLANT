(*******************************************************)
(*                                                     *)
(*           Copyright 2004 J�r�me Rocheteau           *)
(*                                                     *)
(*   This file is distributed under the terms of the   *)
(*    GNU Lesser General Public License Version 2.1    *)
(*                                                     *)
(*******************************************************)

(** Xml Library *)

(** The type [token] defines the XML tokens. *)
type token =
    Left
  | Right 
  | Slash
  | Equal
  | Ident of string
  | Int of int
  | Float of float
  | String of string
  | Char of char

(** The function [lex s] returns the token stream from char stream [t]. *)
let lex = fun input -> 
  let initial = String.create 32 in 
  let buffer = ref initial in 
  let bufpos = ref 0 in 
  let reset = fun () -> buffer := initial; bufpos := 0 in 
  let set = fun c -> 
    begin 
      if !bufpos >= String.length !buffer 
      then 
	let newbuffer = String.create (2 * !bufpos) in 
	let _ = String.blit !buffer 0 newbuffer 0 !bufpos 
	in buffer := newbuffer
      else () 
    end; String.set !buffer !bufpos c; incr bufpos
  in 
  let get = fun () -> let s = String.sub !buffer 0 !bufpos in buffer := initial; s in 
  let put = fun c -> (Some c) in 
  let special = parser 
      [< ' (',' | '.' | ';' | ':' | '!' | '?' | '�' | '�' | '�' 
	   | '�' | '�' | '�' | '�' | '�' | '�' 
	   | '�' | '�' | '�' | '�' | '�' | '�' | '+' | '-' | '*' 
	   | '\\' | '\'' | '`' | '"' | '~' | '&' | '@'
	   | '(' | ')' | '[' | ']' | '{' | '}' | '$' | '�' | '%' | '�' | '�' as c) 
      >] -> c
  in 
  let rec next = parser
      [< ' (' '|'\010'|'\013'|'\009'|'\026'|'\012'); s >] -> next s
    | [< ' ('>'); s >] -> put Right
    | [< ' ('/'); s >] -> put Slash
    | [< ' ('='); s >] -> put Equal
    | [< ' ('<'); s >] -> comment s
    | [< ' ('0'..'9' as c); s>] -> reset(); set c; put (number s)
    | [< ' ('-'); s >] -> reset(); put (neg_number s)
    | [< ' ('"'); s >] -> reset(); put (string s)
    | [< ' ('\''); s >] -> reset(); put (char s)
    | [< ' ('A'..'Z'|'a'..'z'  as c); s >] -> reset(); set c; put (ident s)
    | [< c=special; s >] -> reset(); set c; put (ident s)
    | [< >] -> None
  and ident = parser
      [< ' ('A'..'Z'|'a'..'z'|'0'..'9'  as c); s>] -> set c; ident s
    | [< c=special; s >] -> set c; ident s
    | [< >] -> Ident (get())
  and char = parser
      [< ' ('\\'); c = escape; ' ('\'') >] -> Char c
    | [< ' c; ' ('\'') >] -> Char c
  and neg_number = parser
      [< ' ('0'..'9' as c); s >] -> reset(); set '-'; set c; number s
    | [< s >] -> reset(); set '-'; ident s
  and number = parser
      [< ' ('0'..'9' as c); s >] -> set c; number s
    | [< ' ('e'|'E'); s >] -> set 'E'; exponent_part s
    | [< ' ('.' as c); s >] -> 
	begin match (Stream.peek s) with
	    Some ('E'|'e'|'0'..'9') -> set c; decimal_part s
	  | _ ->  Int(int_of_string(get()))
	end 
    | [< >] -> Int(int_of_string(get()))
  and decimal_part = parser
      [< ' ('0'..'9' as c); s >] -> set c; decimal_part s
    | [< ' ('e'|'E'); s >] -> set 'E'; exponent_part s
    | [< >] -> Float(float_of_string(get()))
  and exponent_part = parser
      [< ' ('+'|'-' as c); s >] -> set c; end_exponent_part s
    | [< s >] -> end_exponent_part s
  and end_exponent_part = parser
      [< ' ('0'..'9' as c); s >] -> set c; end_exponent_part s
    | [< >] -> Float(float_of_string(get()))
  and string = parser
      [< ' ('"') >] -> String(get())
    | [< ' ('\\'); c = escape; s >] -> set c; string s 
    | [< ' c; s >] -> set c; string s
  and digit = parser [< ' ('0'..'9' as c) >] -> Char.code c - Char.code '0'
  and escape = parser
      [< ' ('n') >] -> '\n'
    | [< ' ('r') >] -> '\r'
    | [< ' ('t') >] -> '\t'
    | [< a = digit;b = digit; c = digit >] -> Char.chr (a * 100 + b * 10 + c)
    | [< ' c >] -> c
  and comment = parser 
      [< ' ('!'); s>] -> comment0 s
    | [< >] -> put Left
  and comment0 = parser 
      [< ' ('-'); s>] -> comment1 s
    | [< ' c; s >] -> set '!'; put Left
  and comment1 = parser 
      [< ' ('-'); s>] -> comment2 s
    | [< ' c; s >] -> set '!'; set '-'; put Left
  and comment2 = parser 
      [< ' ('-'); s>] -> comment3 s
    | [< ' c; s >] -> comment2 s
    | [< >] -> failwith "comment is not finished\n"
  and comment3 = parser 
      [< ' ('-'); s>] -> comment4 s
    | [< ' c; s >] -> comment2 s
    | [< >] -> failwith "comment is not finished\n"
  and comment4 = parser 
      [< ' ('>'); s>] -> next s
    | [< ' ('-'); s>] -> comment3 s
    | [< ' c; s >] -> comment2 s
    | [< >] -> failwith "comment is not finished\n"
  in Stream.from (fun count -> next input)

(** The type [temp] defines the temporary tags. *)
type temp = 
    Both of string * ((string * string) list) 
  | Begin of string * ((string * string) list) 
  | End of string
  | Other of string

(** The function [parse s] returns the tag stream from token stream [t]. *)
let parse = fun input -> 
  let put = fun x -> Some x in 
  let rec next = parser 
      [< ' Left ; s >] -> next0 s
    | [< ' Ident x; s >] -> put (Other x)
    | [< ' String x; s >] -> put (Other ("\"" ^ x ^ "\""))
    | [< ' Char x; s >] -> put (Other ("'" ^ (Char.escaped x)^ "'"))
    | [< ' Int x; s >] -> put (Other (string_of_int x))
    | [< ' Float x; s >] -> put (Other (string_of_float x))
    | [< >] -> None
  and next0 = parser 
      [< ' Slash; s >] -> next1 s
    | [< ' Ident i; s >] -> next3 i [] s
    | [< >] -> failwith "token '/' or identifier expected\n"
  and next1 = parser 
      [< ' Ident i; s >] -> next2 i s
    | [< >] -> failwith "identifier expected\n"
  and next2 = fun i -> parser 
      [< ' Right; s >] -> put (End i)
    | [< >] -> failwith "token '>' expected\n"
  and next3 = fun i l -> parser 
      [< ' Right; s >] -> put (Begin (i,(List.rev l))) 
    | [< ' Slash; s >] -> next4 i l s
    | [< ' Ident x; ' Equal; s >] -> next5 i l x s
    | [< >] -> failwith "token '>' either '/' or attribut list expected\n"
  and next4 = fun i l -> parser 
      [< ' Right; s >] -> put (Both (i,(List.rev l)))
    | [< >] -> failwith "token '>' expected\n"
  and next5 = fun i l a -> parser 
      [< ' Ident x; s >] -> next3 i ((a,x) :: l) s
    | [< ' String x; s >] -> next3 i ((a,x) :: l) s
    | [< ' Char x; s >] -> next3 i ((a,Char.escaped x) :: l) s
    | [< ' Int x; s >] -> next3 i ((a,string_of_int x) :: l) s
    | [< ' Float x; s >] -> next3 i ((a,(string_of_float x)) :: l) s
    | [< >] -> failwith "identifier, string, char, integer or float expected\n"
  in Stream.from (fun count -> next input)

type tag = 
    Tag of string * (string * string) list * tag list
  | Data of string

let build = fun input -> 
  let rec next = parser 
      [< ' Other i; o = (other i); s >] -> Some (Data o)
    | [< ' Both (i,l); s >] -> Some (Tag (i,l,[]))
    | [< ' Begin (i,l); t = (body []); ' End j; s >] -> 
	if i = j 
	then Some (Tag (i,l,(List.rev t)))
	else failwith ("end tag" ^ i ^ " expected but not " ^ j ^ "\n")
    | [< >] -> None
  and other = fun r -> parser 
      [< ' Other i; s >] -> other (r ^ " " ^ i) s
    | [< >] -> r
  and body = fun r -> parser
      [< ' Other i; o = (other i); s >] -> body ((Data o) :: r) s
    | [< ' Both (i,l); s >] -> body ((Tag (i,l,[])) :: r) s
    | [< ' Begin (i,l); t = (body []); ' End j; s >] -> 
	if i = j 
	then body ((Tag (i,l,(List.rev t))) :: r) s
	else failwith ("end tag '" ^ i ^ "' expected but found '" ^ j ^ "'\n")
    | [< >] -> r
  in Stream.from (fun count -> next input)

(** The function [iter f ic] returns [f (t)] from tag [t] stream input channel [ic]. *)
let iter = fun f i -> 
  let rec next = parser 
      [< ' t; s  >] -> f t; next s
    | [< >] -> raise Exit
  in next (build (parse (lex (Stream.of_channel i))))
