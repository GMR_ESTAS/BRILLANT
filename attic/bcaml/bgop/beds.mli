(* $Id$ *)
(** Functions for POs related to EDS generation
@author Samuel Colin
@author Tisserant Pierre*)


(**Apply the Delta M operator on the given expr *)
val deltaE : Blast.expr -> Blast.expr

(**Apply the Delta M operator on the given predicate *)
val deltaP : Blast.predicate -> Blast.predicate

(**Generate EDS PO related to the clause PROPERTIES 
*)
val properties : Project.t -> Blast.id -> Po_adt.t list

(**Generate EDS PO related to the clause INITIALISATION
*)
val initialization : Project.t -> Blast.id -> Po_adt.t list

(**Generate EDS PO related to the clause INVARIANT 
*)
val invariant : Project.t -> Blast.id -> Po_adt.t list

(**Generate EDS PO related to the clause ASSERTIONS
*)
val assertion : Project.t -> Blast.id -> Po_adt.t list

(**Generate EDS PO related to precondition
*)
val precondition : Project.t -> Blast.id -> Po_adt.t list

(**Generate EDS PO related to an operation
*)
val operations : Project.t -> Blast.id -> Po_adt.t list

(**Generate EDS PO related to refinements' parameters
*)
val eds_ref_params : Project.t -> Blast.id -> Po_adt.t list

(**Generate EDS PO related to refinements' INITIALISATION
*)
val eds_ref_initialization : Project.t -> Blast.id -> Po_adt.t list


(**Generate EDS PO related to refinements' INVARIANT
*)
val eds_ref_invariant : Project.t -> Blast.id -> Po_adt.t list

(**Generate EDS PO related to refinements' OPERATIONS
*)
val eds_ref_operation : Project.t -> Blast.id -> Po_adt.t list
