(*** $Id$ ***)



(** {5 Interface graphique en labltk }
@author 
@version 
*)



open Tk





let _ =

  let top = openTk () in 
    Wm.title_set top "OCL2B";
 
    (* Base frame *)
    let base = Frame.create top in
      pack ~fill:`Both ~expand:true [base];

      (* Menu bar *)
      let bar = Frame.create ~borderwidth:2 ~relief:`Raised  base in 
	pack ~fill:`Both ~expand:false [bar];

	(* Menu and Menubutton *)
	let meb = Menubutton.create ~text:"File" bar
	and meb1 = Menubutton.create ~text:"Parse" bar
	and meb2 = Menubutton.create ~text:"Translate" bar
	and meb3 = Menubutton.create ~text:"View" bar
	and meb4 = Menubutton.create ~text:"Help" bar 
	in

	let men = Menu.create meb 
	and men1= Menu.create meb1 
	and men2= Menu.create meb2 
	and men3= Menu.create meb3 
	and men4= Menu.create meb4 
	in

	  (* Frames *)
	  let base2 = Frame.create base in
	  let left = Frame.create base2 in
	  let right = Frame.create base2 in
	    pack ~fill:`Both ~expand:true [base2];
	    pack ~side:`Left ~fill:`Y ~expand:false [left];
	    pack ~side:`Left ~fill:`Both ~expand:true [right];

	    (* Widgets on left and right *)

	    (* Button *)
	    let but = Button.create left in
	    let img = Imagephoto.create () in
	      Imagephoto.configure ~file:"OCLtoB.gif" img;
	      Button.configure ~image:img but;

	    (* Check button *)
	    let che = Checkbutton.create ~text:"Check syntax" left in

	    (* Entry *)
	    let ent = Entry.create left in 

	    (* Label *)
	    let lab = Label.create ~text:"OCL tokens:" left in

	    (* Button *)
	    let but1 = Button.create ~text:"Add token" left in

	    (* Listbox *)
	    let lis = Listbox.create left in
	      Listbox.insert lis ~index:`End ~texts:["self"; "context"; "inv:"; "pre"; "post"];

	      (* Radio buttons *)
	      let tv = Textvariable.create () in
		Textvariable.set tv "Constraint";
		let radf = Frame.create right in
		let rads = List.map
			     (fun t -> Radiobutton.create ~text:t ~value:t ~variable:tv radf)
			     ["Constraint"; "Query"; "Expression"] in

		(* Text and scrollbar *)
		let texf1 = Frame.create right in 

		(* Text *)
		let tex1 = Text.create ~height:5 ~width:80 ~font:"Arial 12" texf1 in 

		  (* Scrollbar *)
		  let scr1 = Scrollbar.create texf1 in

		  (* Text and Scrollbar widget link *)
		  let scroll_link1 sb tx =
		    Text.configure ~yscrollcommand:(Scrollbar.set sb) tx;
		    Scrollbar.configure ~command:(Text.yview tx) sb in
		    scroll_link1 scr1 tex1;

		    pack ~side:`Right ~fill:`Both [scr1];
		    pack ~side:`Left ~fill:`Both ~expand:true [tex1];

		    (* Text and scrollbar *)
		    let texf = Frame.create right in 

		    (* Text *)
		    let tex = Text.create ~height:20 ~width:80 texf in
		      Text.configure ~font:"Arial 12" tex;
		      (* Scrollbar *)
		      let scr = Scrollbar.create texf in

		      (* Text and Scrollbar widget link *)
		      let scroll_link sb tx =
			Text.configure ~yscrollcommand:(Scrollbar.set sb) tx;
			Scrollbar.configure ~command:(Text.yview tx) sb in
			scroll_link scr tex;

			pack ~side:`Right ~fill:`Both [scr];
			pack ~side:`Left ~fill:`Both ~expand:true [tex];

			(* Pack them *) 
			pack ~side:`Left ~expand:false [meb;meb1;meb2;meb3];
			pack ~side:`Right ~expand:false [meb4];
			pack ~side:`Top ~fill:`None ~expand:false [coe but; coe che];
			pack ~side:`Bottom ~fill:`Both ~expand:true [coe lis]; 
			pack ~side:`Bottom ~fill:`None ~expand:false [coe but1];
			pack ~side:`Bottom ~fill:`Both ~expand:false [coe ent];
			pack ~side:`Bottom ~fill:`Both ~expand:false [coe lab];
			pack ~fill:`Both ~expand:true [coe texf1];
			pack ~fill:`Both ~expand:false [coe radf];
			pack ~fill:`Both ~expand:true [coe texf];
			pack ~side:`Left ~fill:`Both ~expand:true rads;


			(***** parsing an OCL expression and transforming it into a B expression *****)

			let set_colors (tx, b, a) =
			  let acol = (fun background -> Text.configure ~background tx)
			  in
			    bind ~events:[`Enter] ~action:(fun _ -> acol (`Color a)) tx;
			    bind ~events:[`Leave] ~action:(fun _ -> acol (`Color b)) tx
			in

			let get_ocl2b tx_ocl =
			  let ocl_conc = Text.get tx_ocl (`Linechar (1,0),[]) (`End,[]) in
			  let ocl_abs = OclParser.main OclLexer.token (Lexing.from_string ocl_conc) in
                            print_newline(); print_newline();
                            print_string "Arbre de syntaxe abstraite OCL\n";
                            print_string "------------------------------\n";
			    print_string (Ocl2string.expr_str ocl_abs); print_string "\n\n\n"; print_newline(); flush stdout;
		    	    let b_abs = Ocl2b.ocl_b ocl_abs in
			    let b_conc = B2string.exp_droite b_abs in
                              print_string "Arbre de syntaxe abstraite B\n";
                              print_string "----------------------------\n";
			      print_string (B2string.exp_str b_abs); print_newline(); flush stdout;
			      (ocl_conc,ocl_abs,b_abs,b_conc)
			and
			  get_ocl2b_constraint tx_ocl =
			  let ocl_conc = Text.get tx_ocl (`Linechar (1,0),[]) (`End,[]) in
			  let ocl_abs = OclParser.main OclLexer.token (Lexing.from_string ocl_conc) in
                            print_newline(); print_newline();
                            print_string "Arbre de syntaxe abstraite OCL\n";
                            print_string "------------------------------\n";
			    print_string (Ocl2string.expr_str ocl_abs); print_string "\n\n\n"; print_newline(); flush stdout;
		    	    let b_abs = Ocl2b.constraint_conv ocl_abs in
			    let b_conc = B2string.machine b_abs in
                              print_string "Arbre de syntaxe abstraite B\n";
			      print_string "----------------------------\n";
			      print_string (B2string.const_str b_abs); print_newline(); flush stdout;
			      (ocl_conc,ocl_abs,b_abs,b_conc)
			in

			let ocl2b_command tx1 tx2 =
			  match Textvariable.get tv with
			      "Expression" -> let (a,b,c,d) = get_ocl2b tx1 in
				Text.insert ~index:(`End,[]) ~text:(d^"\n") tx2
			    | "Constraint" -> let (a,b,c,d) = get_ocl2b_constraint tx1 in
				Text.insert ~index:(`End,[]) ~text:(d^"\n") tx2
			    | _ -> failwith "ocl2b_command()"
			in

			let display_frame tx1 =
			  match Textvariable.get tv with
			      "Expression" -> 
				let (a,b,c,d) = get_ocl2b tx1 in
				let wind1 = Toplevel.create top in
				let base5 = Frame.create wind1 in
				let tex3 = Text.create ~height:30 ~width:80 ~font:"Arial 12 italic" ~foreground:`Blue base5
				in 
				  pack ~fill:`Both ~expand:true [base5];
				  Text.insert ~index:(`End,[]) ~text:((Ocl2string.expr_str b)^"\n") tex3;
				  pack ~fill:`Both ~expand:true [tex3]
			    | "Constraint" -> 
				let (a,b,c,d) = get_ocl2b_constraint tx1 in
				let wind1 = Toplevel.create top in
				let base5 = Frame.create wind1 in
				let tex3 = Text.create ~height:30 ~width:80 ~font:"Arial 12 italic" ~foreground:`Blue base5
				in 
				  pack ~fill:`Both ~expand:true [base5];
				  Text.insert ~index:(`End,[]) ~text:((Ocl2string.expr_str b)^"\n") tex3;
				  pack ~fill:`Both ~expand:true [tex3]
			    | _ -> failwith "display_frame()"

			in			  

			  set_colors (tex1, "White", "#ffdfdf");
			  set_colors (tex, "White", "#ffdfdf");

			  Menu.add_command ~label:"Save" ~command:(fun () -> ())  men;
			  Menu.add_command ~label:"Exit" ~command:(fun () -> closeTk (); exit 0) men;
			  Menubutton.configure ~menu:men meb;
			  Menu.add_command ~label:"Display abstract syntaxes"  ~command:(fun () -> display_frame tex1) men1;
			  Menu.add_command ~label:"OCL abstract syntax tree" men1;
			  Menu.add_command ~label:"B abstract syntax tree" men1;
			  Menubutton.configure ~menu:men1 meb1;
			  Menu.add_command ~label:"OCL to B" ~command:(fun()->(ocl2b_command tex1 tex)) men2;
			  Menubutton.configure ~menu:men2 meb2;
			  Menu.add_command ~label:"Abstract syntax" men3;
			  Menu.add_command ~label:"Concrete syntax" men3;
			  Menu.add_command ~label:"Properties" men3;
			  Menubutton.configure ~menu:men3 meb3;
			  Menu.add_command ~label:"About" men4;
			  Menubutton.configure ~menu:men4 meb4;

			  Button.configure ~command:(fun()->(ocl2b_command tex1 tex)) but;

			  (* Main Loop *)   
			  Printexc.print mainLoop ()
;;

