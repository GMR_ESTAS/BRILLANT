(*** $Id$ ***)

(** {5 Description du mod�le UML }
@author 
@version 
*)



(** {1 DEFINITION DES TYPES } *)

(** {3 Types li�s au mod�le UML } *)

(** mod�le UML, au sens d'un diagramme de classes *)
type model = Model of
  ( name
    * uml_class list
    * association list )

(** classe d'un diagramme UML *)
and uml_class = Class of
  ( name
    * multiplicity
    * association list
    * inheritance list
    * attribute list
    * operation list
    * ocl_constraint list
    * state_machine list )

(** association de classes *)
and association = Association of
  ( name
    * role list )


(** {3 Types li�s au concept de classe } *)

(** une classe B h�rite d'une classe A ssi A est une g�n�ralisation de B *)
and inheritance = Generalization of parent

(** indique la classe de laquelle on h�rite *)
and parent = Parent of name

(** attribut d'une classe *)
and attribute = Attribute of
  ( name
    * multiplicity
    * attribute_type
    * initial_value )

(** type d'un attribut *)
and attribute_type =
    Striing
  | Char
  | Integer
  | Real
  | Boolean
  | Undefined
  | Att_type of string

(** valeur initiale d'un attribut *)
and initial_value = string

(** op�ration d'une classe *)
and operation = Operation of
  ( name
    * parameter list
    * parameter list )

(** un param�tre est d�fini par son nom et son type *)
and parameter = Parameter of (name * attribute_type)

(** contrainte OCL *)
and ocl_constraint = string


(** {3 Types li�s au concept d'association } *)

(** caract�ristiques d'une extr�mit� d'association  *)
and role = Role of
  ( name
    * aggregation
    * multiplicity
    * role_type )

(** type d'association, caract�risant une extr�mit� d'association *)
and aggregation =
    Aggregate
  | Composite
  | Ass_end

(** type d'un r�le (classe associ�e � ce r�le) *)
and role_type = Role_type of name


(** {3 Types li�s au concept d'automate � �tats-transitions } *)

(** diagramme d'�tats-transitions d'une classe *)
and state_machine = State_machine of
  ( name
    * state list
    * transition list )

(** caract�risation d'un �tat de l'automate *)
and state =
    Simplestate of name
  | Pseudostate of name
  | Finalstate of name

(** transition entre 2 �tats de l'automate *)
and transition = Transition of
  ( name
    * source
    * target
    * event
    * guard
    * action )

(** �tat de d�part d'une transition *)
and source = Source of state

(** �tat d'arriv�e d'une transition *)
and target = Target of state

(** �v�nement d'une transition (stimulus) *)
and event = string

(** condition de d�clenchement d'une transition *)
and guard = string

(** op�ration accompagnant le d�clenchement d'une transition *)
and action = string


(** {3 Types communs � tous les concepts } *)

and name = string

(** on d�finit la multiplicit� comme un intervalle de valeurs possibles,
avec la convention: 0..* pour d�signer n'importe qu'elle valeur *)
and multiplicity = Multiplicity of range

(** un intervalle est compos� d'une borne inf�rieure et d'un borne sup�rieure *)
and range = Range of (lower * upper)

(** borne inf�rieure d'un intervalle *)
and lower = string

(** borne sup�rieure d'un intervalle *)
and upper = string



(** {1 DEFINITION DES FONCTIONS } *)

(** fonction destin�e � s'assurer qu'un identificateur a au moins 2 caract�res
@param a une cha�ne de caract�res
@return aa si a fait 1 caract�re, a sinon  *)
val rename : string -> string


(** {3 Fonctions qui renvoient le nom d'un objet } *)

(** renvoie le nom de la classe
@return le nom de la classe *)
val class_name : uml_class -> name

(** renvoie le nom de l'association
@return le nom de l'association *)
val assoc_name : association -> name

(** renvoie le nom de l'attribut
@return le nom de l'attribut *)
val attr_name : attribute -> name


(** {3 Fonctions qui cherchent un objet � partir de son nom } *)

(** cherche une classe dans un mod�le UML
@param Model(a,b,c) un diagramme de classes
@param d un nom de classe 
@return la classe de nom d, si elle fait partie du diagramme de classes,
        ou une 'classe vide', sinon *)
val get_class : model * name -> uml_class

(** cherche une association dans un mod�le UML
@param Model(a,b,c) un diagramme de classes
@param d un nom
@return l'association r�f�renc�e par d (d d�signant le nom, un r�le
        ou une classe de l'association), si elle fait partie du mod�le,
        ou une 'association vide', sinon *)
val get_assoc : model * name -> association

(** cherche les caract�ristiques d'une association dans une liste d'associations
@param a un nom
@param b une liste d'associations
@return si une association r�f�renc�e par le nom a appartient � la liste :
        la fonction renvoit un couple compos� de l'identificateur de l'association
        et de la multiplicit� associ�e � cet identificateur, avec les r�gles suivantes :
        - si a est un nom de r�le, on garde ce nom comme identificateur
        - si a est un nom de classe, on prend comme identificateur 
          -- soit le nom de l'association, pour aller dans le sens r�le n�1 vers r�le n�2
          -- soit le nom de l'association + tilde (fct r�ciproque en B), pour aller dans l'autre sens
        - sinon, la fonction renvoit un couple ('association vide','multiplicit� vide') *)
val find_assoc : name * association list -> name * multiplicity

(** cherche un attribut dans une liste d'attributs
@param a un nom d'attribut
@param b une liste d'attributs
@return l'attribut de nom a, s'il appartient � la liste b,
        ou un 'attribut vide', sinon *)
val find_attrib1 : name * attribute list -> attribute

(** cherche un attribut dans une liste de classes
@param a un nom d'attribut
@param b une liste de classes
@return l'attribut de nom a, s'il fait partie de l'une des classes de la liste b,
        ou un 'attribut vide', sinon *)
val find_attrib2 : name * uml_class list -> attribute


(** {3 Fonctions qui ajoutent un objet } *)

(** ajoute une classe � un mod�le UML *)
val set_class : model * uml_class -> model

(** ajoute une association � un mod�le UML *)
val set_assoc : model * association -> model

(** ajoute un attribut � une classe *)
val set_attrib : attribute list * uml_class -> uml_class

(** ajoute une op�ration � une classe *)
val set_oper : operation list * uml_class -> uml_class

(** introduit/modifie une classe
@param a une liste de classes
@param b une classe
@return si la liste a poss�de une classe de m�me nom,
        ses propri�t�s sont remplac�es par celles de b,
        sinon, b est ajout�e � la liste a *)
val replace_class : uml_class list * uml_class -> uml_class list

(** introduit/modifie une association
@param a une liste d'associations
@param b une association
@return si la liste a poss�de une association de m�me nom,
        ses propri�t�s sont remplac�es par celles de b,
        sinon, b est ajout�e � la liste a *)
val replace_assoc : association list * association -> association list


(** {3 Fonctions qui cr�ent un nouvel objet } *)

(** ajoute une 'classe vide' � un mod�le UML
@param a le nom de la nouvelle classe
@param b le mod�le UML *)
val new_class : name * model -> model

(** renvoie un attribut
@param a nom de l'attribut � cr�er
@param b la cardinalit� min de l'attribut
@param c la cardinalit� max
@param d le type de l'attribut *)
val new_attrib : name * lower * upper * string -> attribute

