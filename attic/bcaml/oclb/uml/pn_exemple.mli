(*** $Id$ ***)

(** {5 Fichier d'exemples de mod�les UML }
@author 
@version 
*)



(** {3 EXAMPLE 1 (Geometric figures) } *)

val fi_classes : Uml.uml_class list
val fi_assocs : Uml.association list


(** {3 EXAMPLE 2 (Documentation 1.4) } *)

val ee_classes : Uml.uml_class list
val ee_assocs : Uml.association list


(** {3 EXAMPLE 3 (The Royal and Loyal) } *)

val rl_classes : Uml.uml_class list
val rl_assocs : Uml.association list


(** {3 EXAMPLE 4  Company model) } *)

val co_classes : Uml.uml_class list
val co_assocs : Uml.association list


(** {3 PASSAGE A NIVEAU } *)
val pn_classes : Uml.uml_class list
val pn_assocs : Uml.association list



(** {3 D�finition du mod�le UML } *)

val model_classes : Uml.uml_class list
val model_assocs : Uml.association list
val model_sys : Uml.model

