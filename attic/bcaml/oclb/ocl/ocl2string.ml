(*** $Id$ ***)





(** {5 Traduction de l'arbre OCL en un cha�ne de caract�res }
@author 
@version 
*)



open Ocl





(** ??? *)
let rec pathnamelist u =
  match u with
      Name_lit v -> "Name_lit("^v^")"
    | Number_lit v -> "Number_lit("^v^")"
    | Type_lit v -> "Type_lit("^v^")"
    | String_lit v -> "String_lit("^v^")"
    | Const v -> "Constant("^(const v)^")"
    | Tlist (v,w) -> "Tlist("^(pathnamelist v)^(pathnamelist w)^")"
    | Path (v,w) -> "Path("^(pathnamelist v)^(pathnamelist w)^")"
    | Nlist (v,w) -> "Nlist("^(pathnamelist v)^(pathnamelist w)^")"

(** �criture des constantes bool�ennes *)
and const u =
  match u with
      True -> "True"
    | False -> "False"

(** �criture des collections *)
and collkind u =
  match u with
      Set -> "Set"
    | Bag -> "Bag"
    | Sequence -> "Sequence"
    | Collection -> "Collection"

(** �criture des op�rateurs unaires (moins unaire et n�gation logique) *)
and unaryop u =
  match u with
      Minus_op -> "Minus"
    | Not -> "Not"

(** �criture des op�rateurs multiplicatifs (multiplication et division) *)
and multop u =
  match u with
      Multiply -> "Mult"
    | Divide -> "Div"

(** �criture des op�rateurs additifs (addition et soustraction) *)
and addop u =
  match u with
      Plus -> "Plus"
    | Minus -> "Minus"

(** �criture des op�rateurs relationnels (=, >, <, >=, <= et <>) *)
and relop u =
  match u with
      Equal -> "Equal"
    | Sup -> "Sup"
    | Low -> "Low"
    | Supequal -> "Supeq"
    | Lowequal -> "Loweq"
    | Different -> "Diff"

(** �criture des op�rateurs logiques (et, ou, ou exclusif, =>) *)
and logop u =
  match u with
      And -> "And"
    | Or -> "Or"
    | Xor -> "Xor"
    | Implies -> "Implies"

(** �criture de la nature du type (type de base ou collection) *)
and typespec u =
  match u with
      Simp_type v -> "Simp_type("^pathnamelist v^")"
    | Coll_type (v,w) -> "Coll_type("^collkind v^pathnamelist w^")"	
	
(** ??? *)
and declarator u =
  match u with
      Decl1 (v,w) ->
	"Decl1("^(pathnamelist v)^(typespec w)^")"
    | Decl3 (v,w,x,y) ->
	"Decl3("^(pathnamelist v)^(pathnamelist w)
	^(typespec x)^(expr_str y)^")"
    | Decl4 (v,w,x,y,z) ->
	"Decl4("^(pathnamelist v)^(typespec w)
	^(pathnamelist x)^(typespec y)^(expr_str z)^")"
    | _ -> failwith "declarator()"

(** ??? *)
and actplist u =
  match u with
      Act_p_list v -> "Act_p_list("^(expr_str v)^")"
    | Act_p_list1 (v,w) -> "Act_p_list1("^(expr_str v)^(expr_str w)^")"
    | _ -> failwith "actplist()"

(** ??? *)
and propcallparam u =
  match u with
      Prop_call_par1 -> "Prop_call_par1"
    | Prop_call_par2 v -> "Prop_call_par2("^(expr_str v)^")"
    | Prop_call_par3 (v,w) -> "Prop_call_par3("^(expr_str v)^(typespec w)^")"
    | Prop_call_par4 (v,w) -> "Prop_call_par4("^(expr_str v)^(expr_str w)^")"
    | _ -> failwith "propcallparam()"

(** ??? *)
and formalplist u =
  match u with
      Formal_par1 (v,w) -> "Formal_par1("^v^(typespec w)^")"
    | Formal_par2 (v,w,x) -> "Formal_par2("^v^(typespec w)^(formalplist x)^")"

(** �criture d'une expression OCL *)
and expr_str u =
  match u with
      Prim_exp v -> "Prim_exp("^(pathnamelist v)^")"
    | Decl1 (v,w) ->
	"Decl1("^(pathnamelist v)^(typespec w)^")"
    | Decl3 (v,w,x,y) ->
	"Decl3("^(pathnamelist v)^(pathnamelist w)
	^(typespec x)^(expr_str y)^")"
    | Decl4 (v,w,x,y,z) ->
	"Decl4("^(pathnamelist v)^(typespec w)
	^(pathnamelist x)^(typespec y)^(expr_str z)^")"

    | Act_p_list v -> "Act_p_list("^(expr_str v)^")"
    | Act_p_list1 (v,w) -> "Act_p_list1("^(expr_str v)^(expr_str w)^")"

    | Qualif v -> "Qualifiers("^(expr_str v)^")"

    | Prop_call_par1 -> "Prop_call_par1"
    | Prop_call_par2 v -> "Prop_call_par2("^(expr_str v)^")"
    | Prop_call_par3 (v,w) -> "Prop_call_par3("^(expr_str v)
                              ^(typespec w)^")"
    | Prop_call_par4 (v,w) -> "Prop_call_par4("^(expr_str v)
                              ^(expr_str w)^")"

    | Forall v -> "Forall("^(propcallparam v)^")"
    | Exists v -> "Exist("^(propcallparam v)^")"
    | Iterate v -> "Iterate("^(propcallparam v)^")"
    | Reject v -> "Reject("^(propcallparam v)^")"
    | Collect v -> "Collect("^(propcallparam v)^")"
    | Select v -> "Select("^(propcallparam v)^")"

    | Prop_call1 v -> "Prop_call1(" (* ??? ^(pathnamelist v)^")" ??? *)
    | Prop_call2 v -> "Prop_call2("^(pathnamelist v)^")"
    | Prop_call3 (v,w) -> "Prop_call3("^(pathnamelist v)^(expr_str w)^")"
    | Prop_call4 (v,w) -> "Prop_call4("^(pathnamelist v)^(expr_str w)^")"
    | Prop_call5 (v,w) -> "Prop_call5("^(pathnamelist v)^(expr_str w)^")"
    | Prop_call6 (v,w) -> "Prop_call6("^(pathnamelist v)^(expr_str w)^")"
    | Prop_call7 (v,w,x) -> "Prop_call7("^(pathnamelist v)
                            ^(expr_str w)^(expr_str x)^")"
    | Prop_call8 (v,w,x) -> "Prop_call8("^(pathnamelist v)
                            ^(expr_str w)^(expr_str x)^")"

    | Coll_item2 (v,w) -> "Coll_item2("^(expr_str v)^(expr_str w)^")"
    | Lit_coll1 v -> "Lit_coll1("^(collkind v)^")"
    | Lit_coll2 (v,w) -> "Lit_coll2("^(collkind v)^(expr_str w)^")"

    | Postf_exp3 (v,w) -> "Postf_exp3("^(expr_str v)^(expr_str w)^")"
    | Postf_exp4 (v,w) -> "Postf_exp4("^(expr_str v)^(expr_str w)^")"

    | Unary_exp1 (v,w) -> "Unary_exp1("^(unaryop v)^(expr_str w)^")"
    | Mult_exp2 (v,w,x) -> "Mult_exp2("^(multop v)^(expr_str w)^(expr_str x)^")"
    | Add_exp2 (v,w,x) -> "Add_exp2("^(addop v)^(expr_str w)^(expr_str x)^")"    
    | Rel_exp2 (v,w,x) -> "Rel_exp2("^(relop v)^(expr_str w)^(expr_str x)^")"
    | Log_exp2 (v,w,x) -> "Log_exp2("^(logop v)^(expr_str w)^(expr_str x)^")"

    | If_exp (v,w,x) -> "If_exp("^(expr_str v)^(expr_str w)^(expr_str x)^")"

    | Context1 v -> "Context1("^(expr_str v)^")"    
    | Context2 v -> "Context2("^(typespec v)^")"
    | Pre1 (v,w) -> "Pre1("^(pathnamelist v)^(expr_str w)^")"
    | Pre2 v -> "Pre2("^(expr_str v)^")"
    | Pre3 (v,w) -> "Pre3("^(expr_str v)^(expr_str w)^")"
    | Post1 (v,w) -> "Post1("^(pathnamelist v)^(expr_str w)^")"
    | Post2 v -> "Post2("^(expr_str v)^")"
    | Post3 (v,w) -> "Post3("^(expr_str v)^(expr_str w)^")"
    | Inv1 (v,w) -> "Inv1("^(pathnamelist v)^(expr_str w)^")"
    | Inv2 v -> "Inv2("^(expr_str v)^")"
    | Inv3 (v,w) -> "Inv3("^(expr_str v)^(expr_str w)^")"
    | Constraint1 (v,w) -> "Constr1("^(expr_str v)^(expr_str w)^")"
    | Constraint2 (v,w) -> "Constr2("^(expr_str v)^(expr_str w)^")"
    | Constraint3 (v,w,x) -> "Constr3("^(expr_str v)^(expr_str w)^(expr_str x)^")"
    | Constraint4 (v,w) -> "Constr4("^(expr_str v)^(expr_str w)^")"
    | _ -> "UNDEF_TYPE"
;;

