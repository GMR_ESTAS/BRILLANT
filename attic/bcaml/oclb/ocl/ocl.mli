(*** $Id$ ***)

(** {5 Syntaxe abstraite OCL }
@author 
@version 
*)



(** définition d'une expression OCL *)
type expr =
    Log_exp1 of expr
  | Log_exp2 of logical_op * expr * expr
  | Rel_exp1 of expr
  | Rel_exp2 of relational_op * expr * expr
  | Add_exp1 of expr
  | Add_exp2 of add_op * expr * expr
  | Mult_exp1 of expr
  | Mult_exp2 of multiply_op * expr * expr
  | Unary_exp1 of unary_op * expr
  | Unary_exp2 of expr
  | Postf_exp3 of expr * expr
  | Postf_exp4 of expr * expr
  | Postf_exp5 of expr
  | Postf_exp6 of expr * expr
  | If_exp of expr * expr * expr
  | Lit_coll1 of coll_kind
  | Lit_coll2 of coll_kind * expr
  | Coll_item_list1 of expr
  | Coll_item_list2 of expr * expr
  | Coll_item1 of expr
  | Coll_item2 of expr * expr
  | Prop_call1 of path_name_list
  | Prop_call2 of path_name_list
  | Prop_call3 of path_name_list * expr
  | Prop_call4 of path_name_list * expr
  | Prop_call5 of path_name_list * expr
  | Prop_call6 of path_name_list * expr
  | Prop_call7 of path_name_list * expr * expr
  | Prop_call8 of path_name_list * expr * expr
  | Prop_call_par1
  | Prop_call_par2 of expr
  | Prop_call_par3 of expr * type_spec
  | Prop_call_par4 of expr * expr
  | Select of expr
  | Reject of expr
  | Collect of expr
  | Forall of expr
  | Exists of expr
  | Iterate of expr
  | Size
  | Intersection of expr
  | Union of expr
  | Difference of expr
  | Symmdiff of expr
  | Is_empty
  | Is_notempty
  | Including of expr
  | Excluding of expr
  | Includes of expr
  | Includes_all of expr
  | Count
  | First
  | Last
  | At of expr
  | Append of expr
  | Prepend of expr
  | Ocltype
  | Oclistypeof of expr
  | Ocliskindof of expr
  | Oclastype of expr
  | Allinstances of path_name_list
  | Qualif of expr
  | Act_p_list of expr
  | Act_p_list1 of expr * expr
  | Prim_exp of path_name_list
  | Decl1 of path_name_list * type_spec
  | Decl3 of path_name_list * path_name_list * type_spec * expr
  | Decl4 of path_name_list * type_spec * path_name_list * type_spec * expr
  | Context1 of expr
  | Context2 of type_spec
  | Inv1 of path_name_list * expr
  | Inv2 of expr
  | Inv3 of expr * expr
  | Pre1 of path_name_list * expr
  | Pre2 of expr
  | Pre3 of expr * expr
  | Post1 of path_name_list * expr
  | Post2 of expr
  | Post3 of expr * expr
  | Constraint1 of expr * expr
  | Constraint2 of expr * expr
  | Constraint3 of expr * expr * expr
  | Constraint4 of expr * expr


(** {3 Types des opérateurs } *)

(** opérateurs unaires : moins unaire et négation logique *)
and unary_op = Minus_op | Not

(** opérateurs multiplicatifs : multiplication et division *)
and multiply_op = Multiply | Divide

(** opérateurs additifs : addition et soustraction *)
and add_op = Plus | Minus

(** opérateurs relationnels : =, >, <, >=, <= et <> *)
and relational_op = Equal | Sup | Low | Supequal | Lowequal | Different

(** opérateurs logiques binaires : et, ou, ou exclusif et implication *)
and logical_op = And | Or | Xor | Implies


(** caractérisation des collections *)
and coll_kind = Set | Bag | Sequence | Collection


(** ??? *)
and stereotype = Pre | Post | Inv


(** constantes booléennes *)
and constant = True | False


(** ??? *)
and path_name_list =
    Name_lit of string
  | Type_lit of string
  | Number_lit of string
  | String_lit of string
  | Const of constant
  | Tlist of path_name_list * path_name_list
  | Path of path_name_list * path_name_list
  | Nlist of path_name_list * path_name_list


(** nature du type *)
and type_spec =
    Simp_type of path_name_list
  | Coll_type of coll_kind * path_name_list


(** ??? *)
and formal_param_list =
    Formal_par1 of string * type_spec
  | Formal_par2 of string * type_spec * formal_param_list


(** ??? *)
and let_expr =
    Let_exp1 of string * expr
  | Let_exp2 of string * formal_param_list * expr
  | Let_exp3 of string * type_spec * expr
  | Let_exp4 of string * formal_param_list * type_spec * expr
  | Let_exp_list of let_expr * let_expr

