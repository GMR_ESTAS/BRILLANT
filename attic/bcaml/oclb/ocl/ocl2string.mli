(*** $Id$ ***)

(** {5 Traduction de l'arbre OCL en un cha�ne de caract�res }
@author 
@version 
*)



(** ??? *)
val pathnamelist : Ocl.path_name_list -> string

(** �criture des constantes bool�ennes *)
val const : Ocl.constant -> string

(** �criture des collections *)
val collkind : Ocl.coll_kind -> string

(** �criture des op�rateurs unaires (moins unaire et n�gation logique) *)
val unaryop : Ocl.unary_op -> string

(** �criture des op�rateurs multiplicatifs (multiplication et division) *)
val multop : Ocl.multiply_op -> string

(** �criture des op�rateurs additifs (addition et soustraction) *)
val addop : Ocl.add_op -> string

(** �criture des op�rateurs relationnels (=, >, <, >=, <= et <>) *)
val relop : Ocl.relational_op -> string

(** �criture des op�rateurs logiques (et, ou, ou exclusif, =>) *)
val logop : Ocl.logical_op -> string

(** �criture de la nature du type (type de base ou collection) *)
val typespec : Ocl.type_spec -> string

(** ??? *)
val declarator : Ocl.expr -> string

(** ??? *)
val actplist : Ocl.expr -> string

(** ??? *)
val propcallparam : Ocl.expr -> string

(** ??? *)
val formalplist : Ocl.formal_param_list -> string

(** �criture d'une expression OCL *)
val expr_str : Ocl.expr -> string

