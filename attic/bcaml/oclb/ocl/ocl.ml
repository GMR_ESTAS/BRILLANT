(*** $Id$ ***)



(** {5 Syntaxe abstraite OCL }
@author 
@version 
*)





(** d�finition d'une expression OCL *)
type expr =

(* expression logique *)
    Log_exp1 of expr
  | Log_exp2 of logical_op * expr * expr

(* expression relationnelle *)
  | Rel_exp1 of expr
  | Rel_exp2 of relational_op * expr * expr

(* expression additive *)
  | Add_exp1 of expr
  | Add_exp2 of add_op * expr * expr

(* expression multiplicative *)
  | Mult_exp1 of expr
  | Mult_exp2 of multiply_op * expr * expr

(* expression unaire *)
  | Unary_exp1 of unary_op * expr
  | Unary_exp2 of expr

(* ??? *)
  | Postf_exp3 of expr * expr
  | Postf_exp4 of expr * expr
  | Postf_exp5 of expr
  | Postf_exp6 of expr * expr

(* expression if then else *)
  | If_exp of expr * expr * expr

(* ??? *)
  | Lit_coll1 of coll_kind
  | Lit_coll2 of coll_kind * expr

(* ??? *)
  | Coll_item_list1 of expr
  | Coll_item_list2 of expr * expr
  | Coll_item1 of expr
  | Coll_item2 of expr * expr

(* ??? *)
  | Prop_call1 of path_name_list
  | Prop_call2 of path_name_list
  | Prop_call3 of path_name_list * expr
  | Prop_call4 of path_name_list * expr
  | Prop_call5 of path_name_list * expr
  | Prop_call6 of path_name_list * expr
  | Prop_call7 of path_name_list * expr * expr
  | Prop_call8 of path_name_list * expr * expr

  | Prop_call_par1                     (* ??? *)
  | Prop_call_par2 of expr             (* ??? *)
  | Prop_call_par3 of expr * type_spec (* ??? *)
  | Prop_call_par4 of expr * expr      (* ??? *)

(* expressions de requ�te *)
  | Select of expr
      (* s�lection du sous-ensemble des �l�ments v�rifiant un pr�dicat *)
  | Reject of expr
      (* s�lection du sous-ensemble des �l�ments ne v�rifiant pas le pr�dicat *)
  | Collect of expr
      (* r�sultat de l'application d'une expression � tous les �l�ments de la collection source *)
  | Forall of expr
      (* vrai si tous les �l�ments de la collection v�rifient un pr�dicat *)
  | Exists of expr
      (* vrai si au moins un �l�ment de la collection v�rifie le pr�dicat *)
  | Iterate of expr
      (* it�ration d'un calcul (avec accumulateur) sur tous les �l�ments de la collection *)

(* op�rations sur une collection *)
  | Size                     (* nb d'�l�ments de la collection *)
  | Intersection of expr     (* intersection *)
  | Union of expr            (* union *)
  | Difference of expr       (* diff�rence *)
  | Symmdiff of expr         (* diff�rence sym�trique: (A union B) - (A inter B) *)
  | Is_empty                 (* vrai si la collection est vide *)
  | Is_notempty              (* vrai si la collection n'est pas vide *)
  | Including of expr        (* �l�ments de la collection, plus un objet *)
  | Excluding of expr        (* �l�ments de la collection, except� un objet *)
  | Includes of expr         (* appartenance d'un objet � la collection *)
  | Includes_all of expr     (* inclusion: appartenance de tous les objets � la collection *)
  | Count                    (* nb d'occurrences d'un objet dans la collection *)

(* op�rations sur une suite *)
  | First                    (* premier �l�ment *)
  | Last                     (* dernier �l�ment *)
  | At of expr               (* i-�me �l�ment *)
  | Append of expr           (* ajout d'un objet � la fin *)
  | Prepend of expr          (* ajout d'un objet en t�te *)

(* op�rations li�es au types *)
  | Ocltype                        (* type d'un objet *)
  | Oclistypeof of expr            (* vrai si l'objet est de ce type *)
  | Ocliskindof of expr            (* vrai si l'objet est de ce type ou d'un de ses sous-types *)
  | Oclastype of expr              (* ??? renvoie l'objet s'il est de ce type, Undefined sinon ??? *)
  | Allinstances of path_name_list (* ensemble des instances d'un type et de ses sous-types *)

(* ??? *)
  | Qualif of expr

(* ??? *)
  | Act_p_list of expr
  | Act_p_list1 of expr * expr

(* ??? *)
  | Prim_exp of path_name_list
  | Decl1 of path_name_list * type_spec
  | Decl3 of path_name_list * path_name_list * type_spec * expr
  | Decl4 of path_name_list * type_spec * path_name_list * type_spec * expr

(* contexte d'une contrainte *)
  | Context1 of expr               (* ??? *)
  | Context2 of type_spec          (* ??? *)

(* invariant d'une contrainte *)
  | Inv1 of path_name_list * expr  (* ??? *)
  | Inv2 of expr                   (* ??? *)
  | Inv3 of expr * expr            (* ??? *)

(* pr�-condition d'une contrainte *)
  | Pre1 of path_name_list * expr  (* ??? *)
  | Pre2 of expr                   (* ??? *)
  | Pre3 of expr * expr            (* ??? *)

(* post-condition d'une contrainte *)
  | Post1 of path_name_list * expr (* ??? *)
  | Post2 of expr                  (* ??? *)
  | Post3 of expr * expr           (* ??? *)

(* contrainte *)
  | Constraint1 of expr * expr        (* pr�-condition *)
  | Constraint2 of expr * expr        (* post-condition *)
  | Constraint3 of expr * expr * expr (* ??? pr�-condition (-> diff�rence avec Constraint1 ?) ??? *)
  | Constraint4 of expr * expr        (* invariant *)



(*** Types des op�rateurs ***)

(** op�rateurs unaires : moins unaire et n�gation logique *)
and unary_op = Minus_op | Not

(** op�rateurs multiplicatifs : multiplication et division *)
and multiply_op = Multiply | Divide

(** op�rateurs additifs : addition et soustraction *)
and add_op = Plus | Minus

(** op�rateurs relationnels : =, >, <, >=, <= et <> *)
and relational_op = Equal | Sup | Low | Supequal | Lowequal | Different

(** op�rateurs logiques binaires : et, ou, ou exclusif et implication *)
and logical_op = And | Or | Xor | Implies



(** caract�risation des collections *)
and coll_kind =
    Set        (* ensemble, au sens math�matique du terme *)
  | Bag        (* multi-ensemble: plusieurs fois le m�me �l�ment *)
  | Sequence   (* suite: multi-ensemble ordonn� *)
  | Collection (* super-type des autres types de collections *)



(** ??? *)
and stereotype =
    Pre  (* pr�-condition *)
  | Post (* post-condition *)
  | Inv  (* invariant *)



(** constantes bool�ennes *)
and constant = True | False



(** ??? *)
and path_name_list = 
    Name_lit of string
  | Type_lit of string
  | Number_lit of string
  | String_lit of string
  | Const of constant
  | Tlist of path_name_list * path_name_list
  | Path of path_name_list * path_name_list
  | Nlist of path_name_list * path_name_list



(** nature du type *)
and type_spec = 
    Simp_type of path_name_list             (* type simple *)
  | Coll_type of coll_kind * path_name_list (* collection d'objets *)



(** ??? *)
and formal_param_list =
    Formal_par1 of string * type_spec
  | Formal_par2 of string * type_spec * formal_param_list



(** ??? *)
and let_expr =
    Let_exp1 of string * expr
  | Let_exp2 of string * formal_param_list * expr
  | Let_exp3 of string * type_spec * expr
  | Let_exp4 of string * formal_param_list * type_spec * expr
  | Let_exp_list of let_expr * let_expr

