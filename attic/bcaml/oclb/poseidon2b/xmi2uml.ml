(*** $Id$ ***)



(** {5 Conversion du XMI de Poseidon en syntaxe abstraite UML }
@author 
@version 
*)



open Pxp_types
open Pxp_document
open Pxp_yacc





(*** Fonctions g�n�rales d'acc�s � l'arbre XML ***)

(** donne la valeur d'un attribut XML *)
let value_of attrib =
  match attrib with
      Value s -> s
    | Implied_value -> ""
    | _ -> assert false
;;

(** renvoit la valeur de l'attribut 'name' d'un �l�ment
(renvoit 'anonymous' si l'�l�ment n'a pas d'attribut 'name') *)
let get_name n =
  try value_of (n # attribute "name")
  with Not_found -> "anonymous"
;;

(** m�me fonction que get_name, mais qui renvoit
la cha�ne vide si l'�l�ment n'a pas d'attribut 'name' *)
let get_name2 n =
  try value_of (n # attribute "name")
  with Not_found -> ""

(** �tablit la liste des sous-�l�ments d'un certain type
@param nom le nom des sous-�l�ments � conserver
@param base le noeud � partir duquel on effectue la recherche
@param deep bool�en permettant de sp�cifier si la recherche s'effectue
       sur un seul niveau ou r�cursivement (sur plusieurs niveaux de profondeur) *)
let get_list nom base deep =
  try find_all_elements ~deeply:deep nom base
  with Not_found -> []
;;





(*** Fonctions communes aux fonctions d'acc�s aux composants ***)

(** convertit un noeud XML repr�sentant une multiplicit�
en un constructeur du type UML *)
let get_mult n =
  let conv_inf = function "-1" -> "*" | m -> m in
  let um = find_element "UML:Multiplicity" n in
  let umr = find_element "UML:Multiplicity.range" um in
  let umR = find_element "UML:MultiplicityRange" umr in
  let l = value_of (umR # attribute "lower")
  and u = value_of (umR # attribute "upper") in
    Uml.Multiplicity (Uml.Range (conv_inf l, conv_inf u))
;;

(** renvoit le type UML d'un attribut ou d'un param�tre d'une op�ration
@param nom le nom de la cat�gorie d'�l�ment � rechercher :
       - 'StructuralFeature' pour un attribut
       - 'Parameter' pour un param�tre d'op�ration
@param a le noeud de l'attribut ou du param�tre
@param base la base de recherche ('XMI/Model/Namespace.ownedElement') *)
let get_type nom a base =
    let rec process_type_list lst idref = 
      match lst with
	  [] -> raise Not_found
	| t::q ->
	    if value_of (t # attribute "xmi.id") = idref
	    then
	      let nom_classe = get_name t in
		if nom_classe <> ""
		then nom_classe
		else failwith "AnonymousClass"
	    else process_type_list q idref

    (** correspondance entre les valeurs des attributs XML
    et les constructeurs du type UML *)
    and map_type s =
      match s with
	  "boolean" | "Boolean" -> Uml.Boolean
	| "char" | "Character" -> Uml.Char
	| "int" | "Integer" -> Uml.Integer
	| "float" | "Float" -> Uml.Real
	| "double" | "Double" -> Uml.Real
	| "String" -> Uml.Str
	| _ -> Uml.Undefined

    in
      try
	let ust = find_element ("UML:"^nom^".type") a in
	  try
	    (* Recherche d'un type donn�e de base *)
	    let ud = find_element "UML:DataType" ust in
	    let idref = value_of (ud # attribute "xmi.idref")
	    in
	    let up = find_element "UML:Package" base in
	    let lst = get_list "UML:DataType" up true
	    in map_type (process_type_list lst idref)
	  with
	      Not_found ->
		let uc =
		  try
		    find_element "UML:Class" ust
		  with
		      Not_found -> find_element "UML:AssociationClass" ust
		in
		let idref = value_of (uc # attribute "xmi.idref") in
		  try
		    (* Recherche d'un type donn�e complexe (String, etc...) *)
		    let up = find_element "UML:Package" base in
		    let lst1 = get_list "UML:Class" up true
		    and lst2 = get_list "UML:AssociationClass" up true
		    in map_type (process_type_list (lst1 @ lst2) idref)
		  with
		      Not_found ->
			(* Recherche d'un type objet *)
			let lst1 = get_list "UML:Class" base false
			and lst2 = get_list "UML:AssociationClass" base false in
			  Uml.Att_type (process_type_list (lst1 @ lst2) idref)
      with
	  Not_found -> Uml.Undefined
;;

(** renvoit le type d'un r�le, � partir du noeud ae *)
let get_role_type ae base =
  (* R�cup�ration de la r�f�rence � la classe *)
  let uap = find_element "UML:AssociationEnd.participant" ae in
  let uc =
    try
      find_element "UML:Class" uap
    with
	Not_found -> find_element "UML:AssociationClass" uap
  in
  let idref = value_of (uc # attribute "xmi.idref")
  in
  (* Recherche du nom de la classe correspondante *)
  let rec process_list lst = 
    match lst with
	[] -> raise Not_found
      | t::q ->
	  if value_of (t # attribute "xmi.id") = idref
	  then
	    let nom_classe = get_name t in
	      if nom_classe <> ""
	      then nom_classe
	      else failwith "AnonymousClass"
	  else process_list q
  in
  let lst1 = get_list "UML:Class" base false
  and lst2 = get_list "UML:AssociationClass" base false in
    try
      process_list (lst1 @ lst2)
    with
	Not_found ->
	  let p_lst = get_list "UML:Package" base true in
	  let rec process_package lst =
	    match lst with
		[] -> raise Not_found
	      | p::pl ->
		  let uno = find_element "UML:Namespace.ownedElement" p in
		  let lst1 = get_list "UML:Class" uno false
		  and lst2 = get_list "UMl:AssociationClass" uno false in
		    try
		      process_list (lst1 @ lst2)
		    with
			Not_found -> process_package pl
	  in process_package p_lst
;;





(*** Fonctions d'acc�s aux composants d'une association ***)

(** renvoit le nom d'une association 
@return - le nom de l'attribut 'name', s'il existe
        - sinon: "assoc_nomClasseA_nomClasseB",
  o� nomClasseX d�signe le nom de la classe d'un c�t� de l'association *)
let get_association_name n base =
  let nom = get_name n in
    if nom <> "anonymous"
    then nom
    else
      begin
	let uac = find_element "UML:Association.connection" n
	in
	  match (get_list "UML:AssociationEnd" uac false) with
	      ae1::ae2::q ->
		"assoc_"^(get_role_type ae1 base)^"_"^(get_role_type ae2 base)
	    | _ -> raise Not_found
      end
;;

(** renvoit les r�les d'une association (sous la forme d'une liste de 2 �l�ments) *)
let get_role_list n base =

  (** correspondance entre les valeurs des attributs XML
  et les constructeurs du type UML *)
  let get_aggregation ae =
    match value_of (ae # attribute "aggregation") with
	"aggregate" -> Uml.Aggregate
      | "composite" -> Uml.Composite
      | "none" -> Uml.Ass_end
      | _ -> failwith "get_aggregation()"

  (** r�cup�ration de la multiplicit� du r�le *)
  and get_multiplicity ae =
    let uam = find_element "UML:AssociationEnd.multiplicity" ae in
      get_mult uam

  in
  let uac = find_element "UML:Association.connection" n in
    match (get_list "UML:AssociationEnd" uac false) with
	ae1::ae2::q ->
	  let r1 =
	    Uml.Role(
	      get_name ae1,
	      get_aggregation ae1,
	      get_multiplicity ae1,
	      Uml.Role_type (get_role_type ae1 base) )
	  and r2 =
	    Uml.Role (
	      get_name ae2,
	      get_aggregation ae2,
	      get_multiplicity ae2,
	      Uml.Role_type (get_role_type ae2 base) )
	  in
	  let nav =
	    try
	      value_of (ae1 # attribute "isNavigable")
	    with
		Not_found -> "anonymous"
	  in
	    (* choix d'un sens pour les r�les
	    en fonction des possibilit�s de navigation *)
	    if nav <> "true"
	    then [r1; r2]
	    else [r2; r1]
      | _ -> raise Not_found
;;


	  
(** renvoit la liste des associations (les associations sans nom
et en relation avec une classe anonyme ne sont pas trait�es) *)
let rec get_association base =
  let rec process_list lst =
    match lst with
	[] -> []
      | t::q ->
	  try
	    let n = get_association_name t base in
	      print_endline ("\t- Ajout de l'association: "^n);
	      Uml.Association (n, get_role_list t base ) :: process_list q
	  with
	      Failure "AnonymousClass" -> 
		print_endline ("\t- Association non prise en compte "
		^"(pas de nom, et l'une des classes associ�es est anonyme) !");
		process_list q
	    | _ ->
		(* Association li�e � une autre entit� (acteur, use-case, ...) *)
		process_list q
  in
    let lst = get_list "UML:Association" base false
    and p_lst = get_list "UML:Package" base false in
    let rec process_package lst =
      match lst with
	  [] -> []
	| p::pl ->
	    let uno = find_element "UML:Namespace.ownedElement" p in
	      (get_association uno) @ (process_package pl)
    in
      (process_list lst) @ (process_package p_lst)
;;





(*** Fonctions d'acc�s aux composants d'une classe ***)

(** renvoit la liste des associations d'une classe-association *)
let get_class_association_list t base =
  try (* classe-association *)
    let n = get_association_name t base in
      [ Uml.Association (n, get_role_list t base) ]
  with (* classe normale *)
      Not_found -> []


(** renvoit la liste des super-classes dont cette classe h�rite *)
and get_inheritance_list id base =
  let rec get_parent_name idp lp =
    match lp with
	[] -> raise Not_found
      | tp::qp ->
	  if value_of (tp # attribute "xmi.id") = idp
	  then get_name tp
	  else get_parent_name idp qp
  in
  let rec process_list lst =
    match lst with
	[] -> []
      | t::q ->
	  try
	    let ugc = find_element "UML:Generalization.child" t in
	    let ucc =
	      try
		find_element "UML:Class" ugc
	      with
		  Not_found -> find_element "UML:AssociationClass" ugc	  
	    in
	      if value_of (ucc # attribute "xmi.idref") = id
	      then
		let ugp = find_element "UML:Generalization.parent" t in
		let ucp =
		  try
		    find_element "UML:Class" ugp
		  with
		      Not_found -> find_element "UML:AssociationClass" ugp
		in
		let idp = value_of (ucp # attribute "xmi.idref") in
		let lst1 = get_list "UML:Class" base false
		and lst2 = get_list "UML:AssociationClass" base false in
		let n = get_parent_name idp (lst1 @ lst2) in
		  if n <> ""
		  then
		    begin
		      print_endline ("\t\t* H�ritage de la classe: "^n);
		      (Uml.Generalization (Uml.Parent n)) :: process_list q
		    end
		  else
		    begin
		      print_endline ("\t\t* H�ritage non pris en compte "
				     ^"(la super-classe est anonyme) !");
		      process_list q
		    end
	      else process_list q
	  with
	      (* G�n�ralisation li�e � une autre entit� (acteur, use-case, ...) *)
	      _ -> process_list q
  in
    process_list (get_list "UML:Generalization" base false)


(** renvoit la liste des attributs de la classe (les attributs sans nom
ou dont le type est une classe anonyme ne sont pas trait�s) *)
and get_attribute_list n base =

  (** r�cup�ration �ventuelle de la multiplicit� de l'attribut *)
  let get_multiplicity a =
    try
      let usm = find_element "UML:StructuralFeature.multiplicity" a in
	get_mult usm
    with
	Not_found -> Uml.Multiplicity (Uml.Range ("1", "1"))

  (** r�cup�ration �ventuelle de la valeur initiale de l'attribut *)
  and get_initial_value a =
    try
      let uai = find_element "UML:Attribute.initialValue" a in
      let ue = find_element "UML:Expression" uai in
	value_of (ue # attribute "body")
    with
	Not_found -> ""

  in
  let rec process_list lst =
    match lst with
	[] -> []
      | t::q ->
	  let n = get_name t in
	    try
	      if n = ""
	      then failwith "AnonymousAttribute"
	      else
		begin
		  print_endline ("\t\t* Ajout de l'attribut: "^n);
		  Uml.Attribute (
		    n,
		    get_multiplicity t,
		    get_type "StructuralFeature" t base,
		    get_initial_value t ) :: process_list q
		end
	    with
		Failure "AnonymousClass" ->
		  print_endline ("\t\t* Attribut non pris en compte "
		  ^"(son type est une classe anonyme) !");
		  process_list q
	      | Failure "AnonymousAttribute" ->
		  print_endline "\t\t* Attribut anonyme non pris en compte !";
		  process_list q
  in
    process_list (get_list "UML:Attribute" n false)


(** renvoit la liste des op�rations de la classe
(les op�rations sans nom ou pour lesquelles l'un des param�tres
a pour type une classe anonyme ne sont pas trait�es) *)
and get_operation_list n base =

  (** r�cup�ration de la liste des param�tres (arguments et valeurs de retour)
  de l'op�ration (les param�tres sans nom ne sont pas trait�s) *)
  let get_param_lists o =
    let rec process_param_lists lst arg return =
      match lst with
	  [] -> (arg,return)
	| t::q ->
	    let n = get_name t in
	      try
		if n = ""
		then failwith "AnonymousParameter"
		else
		  if value_of (t # attribute "kind") <> "return"
		  then
		    begin
		      print_endline ("\t\t\t- Ajout du param�tre: "
				     ^n^" (argument)");
		      process_param_lists
			q
			((Uml.Parameter (n, get_type "Parameter" t base))::arg)
			return
		    end
		  else
		    begin
		      print_endline ("\t\t\t- Ajout du param�tre: "
				     ^n^" (valeur de retour)");
		      process_param_lists
			q
			arg
			((Uml.Parameter (n, get_type "Parameter" t base))::return)
		    end
	      with
		  Failure "AnonymousParameter" ->
		    print_endline "\t\t\t- Param�tre anonyme non pris en compte !";
		    process_param_lists q arg return
    in
      try
	let ubp = find_element "UML:BehavioralFeature.parameter" o in
	  process_param_lists (get_list "UML:Parameter" ubp false) [] []
      with
	  Not_found -> ([],[])

  in
  let rec process_list lst =
    match lst with
	[] -> []
      | t::q ->
	  let n = get_name t in
	    try
	      if n = ""
	      then failwith "AnonymousOperation"
	      else
		begin
		  print_endline ("\t\t* Ajout de l'op�ration: "^n);
		  let (arg,return) = get_param_lists t in
		    Uml.Operation (n, arg, return) :: process_list q
		end
	    with
		Failure "AnonymousClass" ->
		  print_endline ("\t\t* Op�ration non prise en compte "
		  ^"(un param�tre a pour type une classe anonyme) !");
		  process_list q
	      | Failure "AnonymousOperation" ->
		  print_endline "\t\t* Op�ration anonyme non prise en compte !";
		  process_list q
  in
    process_list (get_list "UML:Operation" n false)


(** renvoit la liste des contraintes OCL associ�es � une classe *)
and get_constraint_list id base =

  (** traite l'ensemble des commentaires attach�s � une classe,
  consid�r�s comme des contraintes OCL *)
  let rec process_comment_list lst =
    match lst with
	[] -> []
      | t::q ->
	  try
	    let uca = find_element "UML:Comment.annotatedElement" t in
	    let uc = 
	      try
		find_element "UML:Class" uca
	      with
		  Not_found -> find_element "UML:AssociationClass" uca
	    in
	      if value_of (uc # attribute "xmi.idref") = id
	      then
		let c = value_of (t # attribute "body") in
		  if c <> ""
		  then
		    begin
		      print_endline ("\t\t* Ajout de la contrainte:\n"^c);
		      c :: process_comment_list q
		    end
		  else
		    begin
		      print_endline "\t\t* Contrainte vide non trait�e";
		      process_comment_list q
		    end
	      else process_comment_list q
	  with
	      (* les commentaires qui ne sont pas reli�s � une classe
		 ne sont pas consid�r�s comme des contraintes OCL *)
	      Not_found -> process_comment_list q

  (** traite l'ensemble des contraintes OCL d'une classe
  (la syntaxe OCL est v�rifi�e par Poseidon (version 2.1)
  au moment de la cr�ation des contraintes) *)
  and process_constraint_list lst =
    match lst with
	[] -> []
      | t::q ->
	  let ucc = find_element "UML:Constraint.constrainedElement" t in
	  let uc =
	    try	    
	      find_element "UML:Class" ucc
	    with
		Not_found -> find_element "UML:AssociationClass" ucc
	  in
	    if value_of (uc # attribute "xmi.idref") = id
	    then
	      let ucb = find_element "UML:Constraint.body" t in
	      let ub = find_element "UML:BooleanExpression" ucb in
	      let c = value_of (ub # attribute "body") in
		print_endline ("\t\t* Ajout de la contrainte:\n"^c);
		c :: process_constraint_list q
	    else process_constraint_list q

  in
  let uml_comment = process_comment_list (get_list "UML:Comment" base false)
  and uml_constraint = process_constraint_list (get_list "UML:Constraint" base false)
  in uml_comment @ uml_constraint
;;


(*** Fonctions d'acc�s aux composants d'un diagramme d'�tats-transitions ***)

(** convertit un noeud XML repr�sentant un �tat en un constructeur du type UML *)
let conv_state t =
  let n = get_name t in
    match t # node_type with
	T_element "UML:Pseudostate" -> Uml.Pseudostate n
      | T_element "UML:SimpleState" -> Uml.Simplestate n
      | T_element "UML:FinalState" -> Uml.Finalstate n
      | _ -> failwith "conv_state()"
;;

(** �tablit la liste des sous-noeuds XML qui repr�sentent un �tat *)
let state_list ucs =
  (get_list "UML:Pseudostate" ucs true)
  @ (get_list "UML:SimpleState" ucs true)
  @ (get_list "UML:FinalState" ucs true)
;;

(** renvoit la liste des �tats d'un diagramme d'�tats-transitions
(les �tats sans nom ne sont pas consid�r�s) *)
let rec get_states s =
  let rec process_states lst =
    match lst with
	[] -> []
      | t::q ->
	  if get_name t <> ""
	  then (conv_state t) :: process_states q
	  else
	    begin
	      print_endline "\t\t\t- Etat anonyme non trait� !";
	      process_states q
	    end
  in
    try
      let ustp = find_element "UML:StateMachine.top" s in
      let uc = find_element "UML:CompositeState" ustp in
      let ucs = find_element "UML:CompositeState.subvertex" uc in
	process_states (state_list ucs)
    with
	Not_found -> []
;;

(** renvoit la liste des transitions d'un diagramme d'�tats-transitions
(les transitions qui comportent un super-�tat ne sont pas prises en compte) *)
let get_transitions s base =
  let rec map_state lst idref =
    match lst with
	[] -> raise Not_found
      | t::q ->
	  if value_of (t # attribute "xmi.id") = idref
	  then conv_state t
	  else map_state q idref
  in

  (** r�cup�re l'�tat associ� � l'extr�mit� d'une transition *)
  let get_transition_end t tr_type =
    let ut = find_element ("UML:Transition."^tr_type) t in
    let idref = value_of ((ut # nth_node 1) # attribute "xmi.idref") in
    let ustp = find_element "UML:StateMachine.top" s in
    let uc = find_element "UML:CompositeState" ustp in
    let ucs = find_element "UML:CompositeState.subvertex" uc in
      map_state (state_list ucs) idref

  in
  let get_source t = Uml.Source (get_transition_end t "source")
  and get_target t = Uml.Target (get_transition_end t "target")

  (** r�cup�re l'�v�nement �ventuellement associ� � une transition *)
  and get_event t =
    let rec map_event lst idref =
      match lst with
	  [] -> raise Not_found
	| te::qe ->
	    if value_of (te # attribute "xmi.id") = idref
	    then get_name2 te
	    else map_event qe idref
    in
      try
	let utt = find_element "UML:Transition.trigger" t in
	let ucE = find_element "UML:CallEvent" utt in
	let idref = value_of (ucE # attribute "xmi.idref") in
	  map_event (get_list "UML:CallEvent" base false) idref
      with
	  Not_found -> ""

  (** r�cup�re l'�ventuelle garde associ�e � une transition *)
  and get_guard t = (* garde *)
    try
      let utg = find_element "UML:Transition.guard" t in
      let ug = find_element "UML:Guard" utg in
	get_name2 ug
    with
	Not_found -> ""

  (** r�cup�re l'�ventuelle action associ�e � une transition *)
  and get_action t =
    try
      let ute = find_element "UML:Transition.effect" t in
      let ucA = find_element "UML:CallAction" ute in
	get_name2 ucA
    with
	Not_found -> ""

  in
  let rec process_transitions lst =
    match lst with
	[] -> []
      | tt::qt ->
	  try
	    let nt = get_name tt
	    and s = get_source tt
	    and t = get_target tt
	    and e = get_event tt
	    and g = get_guard tt
	    and a = get_action tt in
	      print_endline ("\t\t\t- Ajout de la transition: ");
	      ( Uml.Transition(nt, s, t, e, g, a) ) :: process_transitions qt
	  with
	      (* Transition li�e � un super-�tat *)
	      _ -> process_transitions qt
  in
    try
      let ustr = find_element "UML:StateMachine.transitions" s in
	process_transitions (get_list "UML:Transition" ustr false)
    with
	Not_found -> []
;;


(** renvoit la liste des diagrammes d'�tats-transitions associ�s � une classe *)
let get_state_machine id base =
  let rec process_list lst =
    match lst with
	[] -> []
      | t::q -> 
	  let usc = find_element "UML:StateMachine.context" t in
	  let uc =
	    try
	      find_element "UML:Class" usc
	    with
		Not_found -> find_element "UML:AssociationClass" usc
	  in
	    if value_of (uc # attribute "xmi.idref") = id
	    then
	      let n = get_name t in
		print_endline ("\t\t* Ajout du diagramme d'�tats-transitions: "^n);
		[ Uml.State_machine (n, get_states t, get_transitions t base) ]
	    else process_list q

  in
  let xc = find_element "XMI.content" (base # root) in
    process_list (get_list "UML:StateMachine" xc false )
;;




(** renvoit la liste des classes du diagramme
(si une classe n'a pas de nom, elle n'est pas prise en compte) *)
let rec get_class base =
  let rec process_list lst =
    match lst with
	[] -> []
      | t::q ->
	  let n = get_name t in
	    if n <> ""
	    then
	      begin
		  print_endline ("\t- Ajout de la classe: "^n);

		  (* Recherche des contraintes OCL *)
		  let id = value_of (t # attribute "xmi.id") in
		  let classocs = get_class_association_list t base
		  and inheritances = get_inheritance_list id base
		  and ocl_constraints = get_constraint_list id base
		  and state_machines = get_state_machine id base

		  in
		    try (* Recherche des composants de la classe *)
		      let ucf = find_element "UML:Classifier.feature" t in
			Uml.Class (
			  n,
			  Uml.Multiplicity (Uml.Range ("","")),
			  classocs,
			  inheritances,
			  get_attribute_list ucf base,
			  get_operation_list ucf base,
			  ocl_constraints,
			  state_machines ) :: process_list q
		    with
			Not_found ->
			  Uml.Class (
			    n,
			    Uml.Multiplicity (Uml.Range ("","")),
			    classocs,
			    inheritances,
			    [],
			    [],
			    ocl_constraints,
			    state_machines ) :: process_list q
	      end
	    else
	      begin
		print_endline "\t- Classe anonyme non prise en compte !";
		process_list q
	      end
  in
    let lst1 = get_list "UML:Class" base false
    and lst2 = get_list "UML:AssociationClass" base false
    and p_lst = get_list "UML:Package" base false in
    let rec process_package lst =
      match lst with
	  [] -> []
	| p::pl ->
	    let uno = find_element "UML:Namespace.ownedElement" p in
	      (get_class uno) @ (process_package pl)
    in
      (process_list (lst1 @ lst2)) @ (process_package p_lst)
;;





(*** Fonction principale ***)

(** recherche dans le XMI les diff�rents �l�ments du mod�le UML *)
let get_uml_ast base =
  let xc =
    try
      find_element "XMI.content" base
    with
	Not_found ->
	  print_endline "Pas de balise <XMI.content> !";
	  exit 1
  in
  let um =
    try
      find_element "UML:Model" xc
    with
	Not_found ->
	  print_endline "Pas de balise <UML:Model> !";
	  exit 1
  in
  let uno =
    try
      find_element "UML:Namespace.ownedElement" um
    with
	Not_found ->
	  print_endline "Pas de balise <UML:Namespace.ownedElement> !";
	  exit 1
  in
  let n = get_name um in
    print_endline ("Ajout du mod�le: "^n);
    let class_lst =
      print_endline "\n* Traitement des classes";
      get_class uno
    and assoc_lst =
      print_endline "\n* Traitement des associations";
      get_association uno
    in
      Uml.Model (n, class_lst, assoc_lst)
;;

