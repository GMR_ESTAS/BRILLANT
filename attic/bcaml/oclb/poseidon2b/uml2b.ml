(*** $Id$ ***)



(** {5 Traduction du mod�le UML en une machine B }
@author 
@version 
*)





(*** Fonctions auxiliaires ***)

(** met une cha�ne de caract�res en majuscules *)
let uc s = String.uppercase s ;;

(** met une cha�ne de caract�res en minuscules *)
let lc s = String.lowercase s ;;

(** met en majuscule la premi�re lettre d'un mot *)
let cp s = String.capitalize s ;;

(** met en minuscule la premi�re lettre d'un mot *)
let ucp s = String.uncapitalize s ;;



(*** Messages d'erreur (exceptions) ***)

(** ??? *)
let rec print_error e =
  prerr_endline(Pxp_types.string_of_exn e)
;;

(** ??? *)
let run f a b c =
  try f a b c
  with e -> print_error e
;;



(*** Fonctions de traduction du mod�le UML en une machine B ***)

(** ??? *)
let gen_card multip name =
  match multip with
      Uml.Multiplicity (Uml.Range (lower, upper)) ->
	begin
	  if (lower = upper) && ((lower, upper) <> ("", ""))
	  then
	    [B.Predicat (
	       B.Exp_rel(
		 B.Op_un (B.Card, B.Type_base (B.Var2 name)),
		 B.Equal,
		 B.Type_base (B.Var2 upper)) )]
	  else
	    begin
	      let card = (lower,upper)
	      in match card with
	          ("", "") -> []
		| (_, "*") ->
		    [B.Predicat (
		       B.Exp_rel(
			 B.Op_un (B.Card, B.Type_base (B.Var2(lc name))),
			 B.GreaterEqual,
			 B.Type_base (B.Var2 lower)) )]
		| (_,_) ->
		    [B.AND (
		       B.Predicat (
			 B.Exp_rel(
			   B.Op_un (B.Card, B.Type_base (B.Var2(lc name))),
			   B.GreaterEqual,
			   B.Type_base (B.Var2 lower)) ),
		       B.Predicat (
			 B.Exp_rel(
			   B.Op_un (B.Card, B.Type_base (B.Var2(lc name))),
			   B.LessEqual,
			   B.Type_base (B.Var2 upper)) )
		     )]
	    end
	end
;;

(** ??? *)
let gen_invariant_sets n =
  [B.Predicat1 (
     B.Type_base(B.Var2 (lc n)),
     B.Inclus_ou_egal,
     B.Type_base (B.Var2 (uc n)) )]
;;

(** ??? *)
let gen_attributes attribs =
  let rec gen_attrib (Uml.Attribute (name, _, _, _)) = name
  in List.map gen_attrib attribs
;;

(** ??? *)
let gen_init_attributes attribs =
  let rec init_attrib (Uml.Attribute (name, _, _, init)) =
    begin
      if init = ""
      then B.Sub (B.Val (B.def_ident (lc name)), B.Empty_set)
      else B.Sub (B.Val (B.def_ident (lc name)), B.Type_base (B.Var2 init))
    end
  in List.map init_attrib attribs
;;

(** ??? *)
let gen_invariant_attributes attribs class_name =
  let rec invariant_attributes att =
    match att with
	[] -> []
      | Uml.Attribute (name, multip, att_type, _)::q ->
	  begin
	    let predicat2 rel =
	      let gen_att_type =
		match att_type with
		    Uml.Integer -> B.Nat
		  | Uml.Str -> B.String
		  | Uml.Char -> B.String
		  | Uml.Real -> B.Var2 "Undefined"
		  | Uml.Boolean -> B.Bool
		  | Uml.Undefined -> B.Var2 "Undefined"
		  | Uml.Att_type str -> B.Var2 str
	      in
		B.Predicat2 (
		  B.Type_base(B.Var2(lc name)),
		  B.Appartient,
		  B.Type_base(B.Var2(lc class_name)),
		  rel,
		  B.Type_base(gen_att_type) )
	    and predicat3 rel m =
	      B.Predicat (
		B.Forall (
		  [B.def_ident "x"],
		  B.Exp_rel(
		    B.Type_base(B.Var2 "x"),
		    B.In,
		    B.Type_base(B.Var2(lc class_name))),
		  B.Exp_rel(
		    B.Op_un(B.Card,
			    B.Ima3(
			      B.Type_base(B.Var2(lc name)),
			      B.Ens(B.Type_base(B.Var2 "x")))),
		    rel,
		    B.Type_base(B.Var2 m)) ) )
	    in
	      match multip with
		  Uml.Multiplicity (Uml.Range (lower, upper)) ->
		    begin
	              match (lower, upper) with
			  ("", "") -> invariant_attributes q
			| ("0", "*") ->
			    [predicat2 B.Relation]
			    @ invariant_attributes q
			| ("0", "1") ->
			    [predicat2 B.PartialFunc]
			    @ invariant_attributes q
			| ("1", "1") ->
			    [predicat2 B.TotalFunc]
			    @ invariant_attributes q
			| ("0", _) ->
			    [B.AND(
			       predicat2 B.Relation,
			       predicat3 B.LessEqual upper)]
			    @ invariant_attributes q
			| (_, "*") ->
			    [B.AND(
			       predicat2 B.Relation,
			       predicat3 B.GreaterEqual lower)]
			    @ invariant_attributes q 
			| (_, _) ->
			    [predicat2 B.Relation;
			     B.AND(
			       predicat3 B.LessEqual upper,
			       predicat3 B.GreaterEqual lower)]
			    @ invariant_attributes q 
	            end
	  end
  in invariant_attributes attribs
;;

(** ??? *)
let gen_invariant_uses class_name inherits =
  let rec gen_inherits m =
    match m with
	[] -> []
      | (Uml.Generalization(Uml.Parent parent))::q ->
	  [B.Predicat1 (
	     B.Type_base(B.Var2 (lc class_name)),
	     B.Inclus_ou_egal,
	     B.Type_base(B.Var2(lc parent)) )] @ gen_inherits q
  in gen_inherits inherits
;;

(** ??? *)
let create_operation name =
  let gen_precond =
    B.Pre(
      [B.Predicat1 (
	 B.Type_base(B.Var2 (lc name)),
	 B.Different_rel,
	 B.Type_base(B.Var2(uc name)) )])
  and gen_substitution =
    B.Any([B.def_ident "new"],
	  [B.Predicat1 (
	     B.Type_base(B.Var2 "new"),
	     B.Appartient,
	     B.Exp_rel(
	       B.Type_base(B.Var2(uc name)),
	       B.Difference,
	       B.Type_base(B.Var2(lc name))) )],
	  B.Parallel(
	    [B.Sub (
	       B.Val(B.def_ident(lc name)),
	       B.Exp_rel(
		 B.Type_base(B.Var2(lc name)),
		 B.UnionSets,
		 B.Ens(B.Type_base(B.Var2 "new"))) );
	     B.Sub (
	       B.Val(B.def_ident "obj"),
	       B.Type_base(B.Var2 "new") )])
	 )
  in
    B.Oper (
      [B.def_ident "obj"],
      B.def_ident((lc name)^"_create"),
      [],
      gen_precond,
      gen_substitution )
;;

(** ??? *)
let destroy_operation name =
  let gen_precond =
    B.Pre (
      [B.Predicat1(
	 B.Type_base(B.Var2 "obj"),
	 B.Appartient,
	 B.Type_base(B.Var2(lc name)))])
  and gen_substitution =
    B.Sub (
      B.Val(B.def_ident(lc name)),
      B.Exp_rel(
	B.Type_base(B.Var2(lc name)),
	B.Difference,
	B.Ens(B.Type_base(B.Var2 "obj"))) )
  in
    B.Oper (
      [],
      B.def_ident((lc name)^"_destroy"),
      [B.def_ident "obj"],
      gen_precond,
      gen_substitution )
;;

(** ??? *)
let gen_class_opers oper =
  let gen_return_params =
    List.map (fun (Uml.Parameter(param_name, _)) ->
		if (param_name <> "")
		then B.def_ident(lc param_name)
		else B.def_ident "undefined"
	     )
      (match oper with Uml.Operation (_, _, return_list) -> return_list)
  and gen_oper_name =
    match oper with
	Uml.Operation (name, _, _) -> lc name
  and gen_parameters =
    List.map (fun (Uml.Parameter(param_name, _)) -> B.def_ident(lc param_name))
      (match oper with Uml.Operation (_, params_list, _) -> params_list)
  and gen_precond =
    let predicat name att_type =
      B.Predicat1 (
	B.Type_base(B.Var2 name),
	B.Appartient,
	B.Type_base att_type )
    and gen_att_type att_type =
      match att_type with
	  Uml.Integer -> B.Entier
	| Uml.Str -> B.String
	| Uml.Char -> B.String
	| Uml.Real -> B.Var2 "Undefined"
	| Uml.Boolean -> B.Bool
	| Uml.Undefined -> B.Var2 "Undefined"
	| Uml.Att_type str -> B.Var2(lc str)
    in
      B.Pre (List.map (fun (Uml.Parameter(name, att_type)) ->
			 predicat (lc name) (gen_att_type(att_type))
		      )
	       (match oper with Uml.Operation (_, params_list, _) -> params_list))
  and gen_substitution =
    let gen_return_values =
      List.map (fun (Uml.Parameter(param_name, _)) ->
		  B.Type_base(B.Var2(lc param_name))
	       )
        (match oper with Uml.Operation (_, _, return_list) -> return_list)
    in
      if (gen_return_values <> [])
      then B.Subst(B.Par_list(gen_return_values), B.Empty_set)
      else B.SKIP
  in
    B.Oper (
      gen_return_params,
      B.def_ident gen_oper_name,
      gen_parameters,
      gen_precond,
      gen_substitution )
;;

(** ??? *)
let gen_variables_assoc associations =
  let match_assoc agg =
    match agg with
        Uml.Aggregate -> true
      | Uml.Composite -> false
      | Uml.Ass_end -> true
  in let lst =
      let rec process_list l =
        match l with
            [] -> []
          | ( Uml.Association(
		name,
		[Uml.Role(_, agg1, _, _);
		 Uml.Role(_, agg2, _, _)]) )::q ->
	      if (match_assoc agg1) && (match_assoc agg2)
	      then (lc name) ::(process_list q)
	      else process_list q
	  | _ -> []
      in process_list associations
  in B.def_variables lst
;;

(** ??? *)
let gen_init_assoc associations =
  let match_assoc agg =
    match agg with
        Uml.Aggregate -> true
      | Uml.Composite -> false
      | Uml.Ass_end -> true
  in let rec process_list l =
      match l with
          [] -> []
        | ( Uml.Association(
	      name,
	      [Uml.Role(_, agg1, _, _); Uml.Role(_, agg2, _, _)]) )::q ->
	    if (match_assoc agg1) && (match_assoc agg2)
	    then
	      ( B.Sub(
		  B.Val (B.def_ident (lc name)),
		  B.Empty_set) )::(process_list q)
	    else process_list q
	| _ -> []
  in process_list associations
;;

(** ??? *)
let gen_invariant_assoc associations =
  let predicat1 assoc_name rel first second =
    B.Predicat2 (
      B.Type_base(B.Var2 assoc_name),
      B.Appartient,
      B.Type_base(B.Var2 first),
      rel,
      B.Type_base(B.Var2 second) )
  and predicat2 assoc_name rel first second =
    B.Predicat2 (
      B.Tilde(B.Type_base(B.Var2 assoc_name)),
      B.Appartient,
      B.Type_base(B.Var2 first),
      rel,
      B.Type_base(B.Var2 second) )
  and predicat3 assoc_name rel first m =
    B.Predicat (
      B.Forall (
	[B.def_ident "x"],
	B.Exp_rel (
	  B.Type_base(B.Var2 "x"),
	  B.In,
	  B.Type_base(B.Var2 first) ),
	B.Exp_rel(
	  B.Op_un (
	    B.Card,
	    B.Ima3 (
	      B.Type_base(B.Var2 assoc_name),
	      B.Ens(B.Type_base(B.Var2 "x")) )),
	  rel,
	  B.Type_base(B.Var2 m) ) ) )
  and predicat4 assoc_name rel second m =
    B.Predicat (
      B.Forall (
	[B.def_ident "x"],
	B.Exp_rel(
	  B.Type_base(B.Var2 "x"),
	  B.In,
	  B.Type_base(B.Var2 second)),
	B.Exp_rel (
	  B.Op_un (
	    B.Card,
	    B.Ima3(
	      B.Tilde(B.Type_base(B.Var2 assoc_name)),
	      B.Ens(B.Type_base(B.Var2 "x"))) ),
	  rel,
	  B.Type_base(B.Var2 m) ) ) )
  and predicat5 assoc_name rel1 rel2 first n m =
    B.Predicat (
      B.Forall(
	[B.def_ident "x"],
	B.Exp_rel (
	  B.Type_base(B.Var2 "x"),
	  B.In,
	  B.Type_base(B.Var2 first) ),
	B.Exp_rel (
	  B.Op_un(
	    B.Card,
	    B.Exp_rel (
	      B.Ima3 (
		B.Type_base(B.Var2 assoc_name),
		B.Ens(B.Type_base(B.Var2 "x")) ),
	      rel1,
	      B.Type_base(B.Var2 n) )),
          B.And,
	  B.Op_un(
	    B.Card,
	    B.Exp_rel (
	      B.Ima3 (
		B.Type_base(B.Var2 assoc_name),
		B.Ens(B.Type_base(B.Var2 "x")) ),
	      rel2,
	      B.Type_base(B.Var2 m) ))
	) ) )
  and predicat6 assoc_name rel1 rel2 second n m =
    B.Predicat (
      B.Forall (
	[B.def_ident "x"],
	B.Exp_rel (
	  B.Type_base(B.Var2 "x"),
	  B.In,
	  B.Type_base(B.Var2 second) ),
        B.Exp_rel (
	  B.Op_un (
	    B.Card,
	    B.Exp_rel (
	      B.Ima3 (
		B.Tilde(B.Type_base(B.Var2 assoc_name)),
		B.Ens(B.Type_base(B.Var2 "x")) ),
	      rel1,
	      B.Type_base(B.Var2 n) ) ),
	  B.And,
	  B.Op_un (
	    B.Card,
	    B.Exp_rel (
	      B.Ima3 (
		B.Tilde(B.Type_base(B.Var2 assoc_name)),
		B.Ens(B.Type_base(B.Var2 "x")) ),
	      rel2,
	      B.Type_base(B.Var2 m) ) )
	) ) )

  and match_assoc agg =
    match agg with
        Uml.Aggregate -> true
      | Uml.Composite -> false
      | Uml.Ass_end -> true
	  
  in let rec process_list l =
      match l with
          [] -> []
	| ( Uml.Association(
	      name,
	      [Uml.Role(
		 _,
		 agg1,
		 Uml.Multiplicity(Uml.Range(low1, upp1)),
		 Uml.Role_type type1);
	       Uml.Role(
		 _,
		 agg2,
		 Uml.Multiplicity(Uml.Range(low2, upp2)),
		 Uml.Role_type type2)]) )::q ->
	    begin
	      if (match_assoc agg1) && (match_assoc agg2)
	      then
		begin
		  match (low1,upp1),(low2,upp2) with
		      ("0","*"),("0","*") ->
			[predicat1 (lc name) B.Relation (lc type1) (lc type2)]
			@ (process_list q)
		    | ("0","*"),("0","1") ->
			[predicat1 (lc name) B.PartialFunc (lc type1) (lc type2)]
			@ (process_list q)
		    | ("0","*"),("1","1") ->
			[predicat1 (lc name) B.TotalFunc (lc type1) (lc type2)]
			@ (process_list q)
		    | ("0","*"),("","") ->
			[predicat1 (lc name) B.Relation (lc type1) (lc type2)]
			@ (process_list q)
		    | ("0","1"),("0","*") ->
			[predicat1 (lc name) B.Relation (lc type1) (lc type2);
			 predicat2 (lc name) B.PartialFunc (lc type2) (lc type1)]
			@ (process_list q)
		    | ("0","1"),("0","1") ->
			[predicat1 (lc name) B.PartialFunc (lc type1) (lc type2);
			 predicat2 (lc name) B.PartialFunc (lc type2) (lc type1)]
			@ (process_list q)
		    | ("0","1"),("1","1") ->
			[predicat1 (lc name) B.TotalFunc (lc type1) (lc type2);
			 predicat2 (lc name) B.PartialFunc (lc type2) (lc type1)]
			@ (process_list q)
		    | ("0","1"),("","") ->
			[predicat1 (lc name) B.Relation (lc type1) (lc type2);
			 predicat2 (lc name) B.PartialFunc (lc type2) (lc type1)]
			@ (process_list q)
		    | ("1","1"),("0","*") ->
			[predicat1 (lc name) B.Relation (lc type1) (lc type2);
			 predicat2 (lc name) B.TotalFunc (lc type2) (lc type1)]
			@ (process_list q)
		    | ("1","1"),("0","1") ->
			[predicat1 (lc name) B.PartialFunc (lc type1) (lc type2);
			 predicat2 (lc name) B.TotalFunc (lc type2) (lc type1)]
			@ (process_list q)
		    | ("1","1"),("1","1") ->
			[predicat1 (lc name) B.TotalFunc (lc type1) (lc type2);
			 predicat2 (lc name) B.TotalFunc (lc type2) (lc type1)]
			@ (process_list q)
		    | ("1","1"),("","") ->
			[predicat1 (lc name) B.Relation (lc type1) (lc type2);
			 predicat2 (lc name) B.TotalFunc (lc type2) (lc type1)]
			@ (process_list q)
		    | ("",""),("0","*") ->
			[predicat1 (lc name) B.Relation (lc type1) (lc type2)]
			@ (process_list q)
		    | ("",""),("0","1") ->
			[predicat1 (lc name) B.PartialFunc (lc type1) (lc type2)]
			@ (process_list q)
		    | ("",""),("1","1") ->
			[predicat1 (lc name) B.TotalFunc (lc type1) (lc type2)]
			@ (process_list q)
		    | ("0","*"),("0",_) ->
			[predicat3 (lc name) B.LessEqual (lc type1) upp2]
			@ (process_list q)
		    | ("0","*"),(_,"*") ->
			[predicat3 (lc name) B.GreaterEqual (lc type1) low2]
			@ (process_list q)
		    | ("0","*"),(_,_) when low2=upp2 ->
			[predicat3 (lc name) B.Equal (lc type1) low2]
			@ (process_list q)
		    | ("0","*"),(_,_) ->
			[predicat5 (lc name) B.GreaterEqual B.LessEqual (lc type1) low2 upp2]
			@ (process_list q)
		    | ("0",_),("0","*") ->
			[predicat4 (lc name) B.LessEqual (lc type2) upp1]
			@ (process_list q)
		    | ("0",_),("0",_) ->
			[predicat4 (lc name) B.LessEqual (lc type2) upp1;
			 predicat3 (lc name) B.LessEqual (lc type1) upp2]
			@ (process_list q)
		    | ("0",_),(_,"*") ->
			[predicat4 (lc name) B.LessEqual (lc type2) upp1;
			 predicat3 (lc name) B.GreaterEqual (lc type1) low2]
			@ (process_list q)
		    | ("0",_),("","") ->
			[predicat4 (lc name) B.LessEqual (lc type2) upp1]
			@ (process_list q)
		    | ("0",_),(_,_) when low2=upp2 ->
			[predicat4 (lc name) B.LessEqual (lc type2) upp1;
			 predicat3 (lc name) B.Equal (lc type1) low2]
			@ (process_list q)
		    | ("0",_),(_,_) ->
			[predicat4 (lc name) B.LessEqual (lc type2) upp1;
			 predicat5 (lc name) B.GreaterEqual B.LessEqual (lc type1) low2 upp2]
			@ (process_list q)
		    | (_,"*"),("0","*") ->
			[predicat4 (lc name) B.GreaterEqual (lc type2) low1]
			@ (process_list q)
		    | (_,"*"),("0",_) ->
			[predicat4 (lc name) B.GreaterEqual (lc type2) low1;
			 predicat3 (lc name) B.LessEqual (lc type1) upp2]
			@ (process_list q)
		    | (_,"*"),(_,"*") ->
			[predicat4 (lc name) B.GreaterEqual (lc type2) low1;
			 predicat3 (lc name) B.GreaterEqual (lc type1) low2]
			@ (process_list q)
		    | (_,"*"),("","") ->
			[predicat4 (lc name) B.GreaterEqual (lc type2) low1]
			@ (process_list q)
		    | (_,"*"),(_,_) when low2=upp2 ->
			[predicat4 (lc name) B.GreaterEqual (lc type2) low1;
			 predicat3 (lc name) B.Equal (lc type1) low2]
			@ (process_list q)
		    | (_,"*"),(_,_) ->
			[predicat4 (lc name) B.GreaterEqual (lc type2) low1;
			 predicat5 (lc name) B.GreaterEqual B.LessEqual (lc type1) low2 upp2]
			@ (process_list q)
		    | ("",""),("0",_) ->
			[predicat3 (lc name) B.LessEqual (lc type1) upp2]
			@ (process_list q)
		    | ("",""),(_,"*") ->
			[predicat3 (lc name) B.GreaterEqual (lc type1) low2]
			@ (process_list q)
		    | ("",""),("","") ->
			[predicat1 (lc name) B.Relation (lc type1) (lc type2)]
			@ (process_list q)
		    | ("",""),(_,_) when low2=upp2 ->
			[predicat3 (lc name) B.Equal (lc type1) low2]
			@ (process_list q)
		    | ("",""),(_,_) ->
			[predicat5 (lc name) B.GreaterEqual B.LessEqual (lc type1) low2 upp2]
			@ (process_list q)
		    | (_,_),("0","*") when low1=upp1 ->
			[predicat4 (lc name) B.Equal (lc type2) low1]
			@ (process_list q)
		    | (_,_),("0","*") ->
			[predicat6 (lc name) B.GreaterEqual B.LessEqual (lc type2) low1 upp1]
			@ (process_list q)
		    | (_,_),("0",_) when low1=upp1 ->
			[predicat4 (lc name) B.Equal (lc type2) low1;
			 predicat3 (lc name) B.LessEqual (lc type1) upp2]
			@ (process_list q)
		    | (_,_),("0",_) ->
			[predicat6 (lc name) B.GreaterEqual B.LessEqual (lc type2) low1 upp1;
			 predicat3 (lc name) B.LessEqual (lc type1) upp2]
			@ (process_list q)
		    | (_,_),(_,"*") when low1=upp1 ->
			[predicat4 (lc name) B.Equal (lc type2) low1;
			 predicat3 (lc name) B.GreaterEqual (lc type1) low2]
			@ (process_list q)
		    | (_,_),(_,"*") ->
			[predicat6 (lc name) B.GreaterEqual B.LessEqual (lc type2) low1 upp1;
			 predicat3 (lc name) B.GreaterEqual (lc type1) low2]
			@ (process_list q)
		    | (_,_),("","") when low1=upp1 ->
			[predicat4 (lc name) B.Equal (lc type2) low1]
			@ (process_list q)
		    | (_,_),("","") ->
			[predicat6 (lc name) B.GreaterEqual B.LessEqual (lc type2) low1 upp1]
			@ (process_list q)
		    | (_,_),(_,_) when (low1=upp1) && (low2=upp2) ->
			[predicat4 (lc name) B.Equal (lc type2) low1;
			 predicat3 (lc name) B.Equal (lc type1) low2]
			@ (process_list q)
		    | (_,_),(_,_) when low1=upp1 ->
			[predicat4 (lc name) B.Equal (lc type2) low1;
			 predicat5 (lc name) B.GreaterEqual B.LessEqual (lc type1) low2  upp2]
			@ (process_list q)
		    | (_,_),(_,_) when low2=upp2 ->
			[predicat6 (lc name) B.GreaterEqual B.LessEqual (lc type2) low1 upp1;
			 predicat3 (lc name) B.Equal (lc type1) low2]
			@ (process_list q)
		    | (_,_),(_,_) ->
			[predicat6 (lc name) B.GreaterEqual B.LessEqual (lc type2) low1 upp1;
			 predicat5 (lc name) B.GreaterEqual B.LessEqual (lc type1) low2  upp2]
			@ (process_list q)
		end
	      else (process_list q)
	    end
	| _ -> []
  in process_list associations
;;

(** ??? *)
let gen_invariant_aggregate associations =
  let predicat n =
    B.Predicat (
      B.Forall(
	[B.def_ident "a"; B.def_ident "b"],
	B.Exp_rel (
	  B.Tuple(B.Type_base(B.Var2 "a"), B.Type_base(B.Var2 "b")),
	  B.In,
	  B.Op_un(B.Closure,B.Type_base(B.Var2 n)) ),
	B.Op_un (
	  B.Not,
	  B.Exp_rel(B.Type_base(B.Var2 "a"), B.Equal, B.Type_base(B.Var2 "b")) )
      ) )
  in let rec process_list l =
      match l with
          [] -> []
        | Uml.Association(
	    name,
	    [Uml.Role(_, agg1, _, Uml.Role_type type1);
	     Uml.Role(_, agg2, _, Uml.Role_type type2)]) :: q ->
	    begin
	      if type1 = type2
	      then
		begin
		  match (agg1, agg2) with
		      (Uml.Aggregate, _) -> [predicat(lc name)] @ (process_list q)
		    | (_, Uml.Aggregate) -> [predicat (lc name)] @ (process_list q)
		    | (_, _) -> (process_list q)
		end
	      else (process_list q)
	    end
	| _ -> []
  in process_list associations
;;

(** ??? *)
let gen_variables_compose associations class_name =
  let lst =
    let rec process_list l =
      match l with
          [] -> []
        | Uml.Association (
	    name,
	    [Uml.Role(_, agg1, _, Uml.Role_type type1);
	     Uml.Role(_, agg2, _, Uml.Role_type(type2))]) :: q ->
	    begin
	      match (agg1, agg2) with
		  (Uml.Composite, _) ->
		    if class_name = type1
		    then name :: process_list q
		    else process_list q
		| (_, Uml.Composite) ->
		    if class_name = type2
		    then name :: process_list q
		    else process_list q
		| (_, _) -> process_list q
	    end
	| _ -> []
    in process_list associations
  in lst
;;

(** ??? *)
let gen_includes_compose associations class_name =
  let lst =
    let rec process_list l =
      match l with
          [] -> []
        | Uml.Association(
	    name,
	    [Uml.Role(_, agg1, _, Uml.Role_type type1);
	     Uml.Role(_, agg2, _, Uml.Role_type type2)]) :: q ->
	    begin
	      match (agg1, agg2) with
		  (Uml.Composite, _) ->
		    if class_name = type1
		    then type2 :: process_list q
		    else process_list q
		| (_, Uml.Composite) ->
		    if class_name = type2
		    then type1 :: process_list q
		    else process_list q 
		| (_, _) -> process_list q
	    end
	| _ -> []
    in process_list associations
  in lst
;;

(** ??? *)
let gen_init_compose associations class_name =
  let lst =
    let rec process_list l =
      match l with
          [] -> []
        | Uml.Association(
	    name,
	    [Uml.Role(_, agg1, _, Uml.Role_type type1);
	     Uml.Role(_, agg2, _, Uml.Role_type type2)]) :: q ->
	    begin
	      match (agg1, agg2) with
		  (Uml.Composite, _) ->
		    if class_name = type1
		    then (B.Sub (B.Val (B.def_ident name), B.Empty_set)) :: process_list q
		    else process_list q
		| (_, Uml.Composite) ->
		    if class_name = type2
		    then (B.Sub (B.Val (B.def_ident name), B.Empty_set)) :: process_list q
		    else process_list q 
		| (_, _) -> process_list q
	    end
	| _ -> []
    in process_list associations
  in lst
;;

(** ??? *)
let gen_invariant_compose associations class_name =
  let predicat1 assoc_name rel first second =
    B.Predicat2 (
      B.Type_base(B.Var2(lc assoc_name)),
      B.Appartient,
      B.Type_base(B.Var2(lc first)),
      rel,
      B.Type_base(B.Var2(lc second)) )
  and predicat2 assoc_name rel first second =
    B.Predicat2 (
      B.Tilde(B.Type_base(B.Var2(lc assoc_name))),
      B.Appartient,
      B.Type_base(B.Var2(lc first)),
      rel,
      B.Type_base(B.Var2(lc second)) )
  in let rec process_list l =
      match l with
          [] -> []
	| Uml.Association(
	    name,
	    [Uml.Role(
	       _,
	       agg1,
	       Uml.Multiplicity(Uml.Range(low1, upp1)),
	       Uml.Role_type type1);
	     Uml.Role(
	       _,
	       agg2,
	       Uml.Multiplicity(Uml.Range(low2, upp2)),
	       Uml.Role_type type2)]) :: q ->

	    begin
	      match (agg1, agg2) with
		  (Uml.Composite, _) ->
		    if class_name = type1
	            then
	              begin
			match (low1,upp1),(low2,upp2) with
			    (_,_),("","") -> process_list q
			  | ("1","1"),(_,_) ->
			      [B.AND (
				 predicat1 name B.Relation type1 type2,
				 predicat2 name B.TotalFunc type1 type2 )]
			      @ (process_list q)
			  | ("0","1"),(_,_) ->
			      [B.AND (
				 predicat1 name B.Relation type1 type2,
				 predicat2 name B.PartialFunc type1 type2 )]
			      @ (process_list q)
			  | (_,_),(_,_) -> process_list q
		      end
	            else process_list q
		| (_, Uml.Composite) ->
		    if class_name = type2
	            then
	              begin
			match (low1,upp1),(low2,upp2) with
			    ("",""),(_,_) -> process_list q
			  | (_,_),("1","1") ->
			      [B.AND (
				 predicat1 name B.Relation type2 type1,
				 predicat2 name B.TotalFunc type2 type1 )]
			      @ (process_list q)
			  | (_,_),("0","1") ->
			      [B.AND (
				 predicat1 name B.Relation type2 type1,
				 predicat2 name B.PartialFunc type2 type1 )]
			      @ (process_list q)
			  | (_,_),(_,_) -> process_list q
		      end
	            else process_list q
		| (_, _) -> process_list q
	    end
	| _ -> []
  in process_list associations
;;

(** ??? *)
let gen_includes_system associations classes =
  let class_name_list =
    List.map
      (fun x -> match x with Uml.Class(class_name,_,_,_,_,_,_,_) -> class_name)
      classes
  and class_compose_list = 
    let lst =
      let rec process_list l =
        match l with
            [] -> []
          | Uml.Association(
	      _,
	      [Uml.Role(_, agg1, _, Uml.Role_type type1);
	       Uml.Role(_, agg2, _, Uml.Role_type type2)]) :: q ->
	      begin
		match (agg1, agg2) with
		    (Uml.Composite, _) -> type2 :: process_list q
		  | (_, Uml.Composite) -> type1 :: process_list q
		  | (_, _) -> process_list q
	      end
	  | _ -> []
      in process_list associations
    in lst

  in let rec appartient l =
      match l with
	  [] -> []
	| t::q ->
	    if List.exists (fun x -> x = t) class_compose_list
	    then appartient q
	    else t :: appartient q

  in appartient class_name_list
;;

(** ??? *)
let create_association name type1 type2 =
  let gen_precond =
    B.Pre (
      [B.Predicat1(
	 B.Type_base(B.Var2 "obj1"),
	 B.Appartient,
	 B.Type_base(B.Var2 type1));
       B.Predicat1(
	 B.Type_base(B.Var2 "obj2"),
	 B.Appartient,
	 B.Type_base(B.Var2 type2))])
  and gen_substitution =
    B.Sub (
      B.Val(B.def_ident name),
      B.Exp_rel (
	B.Type_base(B.Var2 name),
	B.UnionSets,
	B.Ens (
	  B.Tuple(B.Type_base(B.Var2 "obj1"),
		  B.Type_base(B.Var2 "obj2")) )
      ) )
  in
    B.Oper (
      [],
      B.def_ident(name^"_create"),
      [B.def_ident "obj1"; B.def_ident "obj2"],
      gen_precond,
      gen_substitution )
;;

(** ??? *)
let destroy_association name type1 type2 =
  let gen_precond =
    B.Pre (
      [B.Predicat1 (
	 B.Type_base(B.Var2 "obj1"),
	 B.Appartient,
	 B.Type_base(B.Var2 type1) );
       B.Predicat1 (
	 B.Type_base(B.Var2 "obj2"),
	 B.Appartient,
	 B.Type_base(B.Var2 type2));
       B.Predicat1 (
	 B.Ens (B.Tuple (B.Type_base(B.Var2 "obj1"),B.Type_base(B.Var2 "obj2") ) ),
	 B.Inclus_ou_egal,
	 B.Type_base(B.Var2 name) )] )
  and gen_substitution =
    B.Sub (
      B.Val(B.def_ident name),
      B.Exp_rel (
	B.Type_base(B.Var2 name),
	B.Difference,
	B.Ens( B.Tuple(B.Type_base(B.Var2 "obj1"),B.Type_base(B.Var2 "obj2")) )
      ) )
  in
    B.Oper (
      [],
      B.def_ident(name^"_destroy"),
      [B.def_ident "obj1"; B.def_ident "obj2"],
      gen_precond,
      gen_substitution)
;;

(** ??? *)
let gen_assoc_operations associations =
  let match_assoc agg =
    match agg with
        Uml.Aggregate -> true
      | Uml.Composite -> false
      | Uml.Ass_end -> true
  in let rec process_list l =
      match l with
          [] -> []
        | Uml.Association(
	    name,
	    [Uml.Role(_, agg1, _, Uml.Role_type type1);
	     Uml.Role(_, agg2, _, Uml.Role_type type2)]) :: q ->
	    if (match_assoc agg1) && (match_assoc agg2)
	    then
	      [create_association (lc name) (lc type1) (lc type2);
	       destroy_association (lc name) (lc type1) (lc type2)]
	      @ (process_list q)
	    else process_list q
	| _ -> []
  in process_list associations
;;

(** ??? *)
let rec gen_state_sets class_name states =
  let rec process_list lst = 
    match lst with
	[] -> []
      | t::q -> begin
	  match t with
	      Uml.Simplestate name -> (B.Type_base(B.Var2(lc name)))::process_list q
	    | Uml.Pseudostate name -> (B.Type_base(B.Var2(lc name)))::process_list q
	    | Uml.Finalstate name -> (B.Type_base(B.Var2(lc name)))::process_list q
	end
  in match states with
      [] -> []
    | (Uml.State_machine(_,list_states,_))::q ->
	begin
	  let a = process_list list_states
	  in
	    if a <> []
            then (B.Set2 (
		    B.Exp_rel (
		      B.Type_base(B.Var2("STATE_"^(cp class_name))),
		      B.Equal,
		      B.Ens(B.Par_list a) )) )::gen_state_sets class_name q
            else gen_state_sets class_name q
	end
;;

(** ??? *)
let rec gen_states_variables states class_name =
  match states with
      [] -> []
    | (Uml.State_machine(_,list_states,_))::q ->
	if list_states <> []
        then ("state_"^(lc class_name))::gen_states_variables q class_name
        else gen_states_variables q class_name
;;

(** ??? *)
let rec gen_invariant_states states class_name =
  match states with
      [] -> []
    | (Uml.State_machine(_,list_states,_))::q ->
	if list_states <> []
        then
	  (B.Predicat2 (
	     B.Type_base(B.Var2("state_"^(lc class_name))),
	     B.Appartient,
	     B.Type_base(B.Var2(lc class_name)),
	     B.TotalFunc,
	     B.Type_base(B.Var2("STATE_"^(cp class_name))) )
	  )::gen_invariant_states q class_name
        else gen_invariant_states q class_name
;;

(** ??? *)
let rec gen_init_states states class_name =
  match states with
      [] -> []
    | (Uml.State_machine(_,list_states,_))::q ->
	if list_states <> []
        then (B.Sub (
		B.Val(B.def_ident ("state"^(lc class_name))),
		B.Empty_set) )::gen_init_states q class_name
        else gen_init_states q class_name
;;

(** ??? *)
let match_state stt =
  match stt with
      Uml.Simplestate name -> name
    | Uml.Pseudostate name -> name
    | Uml.Finalstate name -> name
;;

(** ??? *)
let rec gen_oper_states states class_name =
  let rec process_list lst = 
    match lst with
	[] -> []
      | (Uml.Transition(_, Uml.Source src, Uml.Target trg, _, _, _))::q ->
	  B.Oper(
	    [],
	    B.def_ident("transition_"^lc(match_state src)^"_"^lc(match_state trg)),
	    [B.def_ident "obj"],
	    B.Pre(
	      [B.Predicat1 (
		 B.Type_base(B.Var2 "obj"),
		 B.Appartient,
		 B.Type_base(B.Var2 (lc class_name)) );
	       B.Predicat1 (
		 B.Ima2 (
		   B.Type_base(B.Var2("state_"^(lc class_name))),
		   B.Type_base(B.Var2 "obj") ),
		 B.Egal,
		 B.Type_base(B.Var2 (lc (match_state src))) )
	      ]),
	    B.Sub (
	      B.Ima (
		B.def_ident("state_"^(lc class_name)),
		B.def_ident("obj") ),
	      B.Type_base(B.Var2(lc(match_state trg))) )
	  )::process_list q
  in match states with
      [] -> []
    | (Uml.State_machine(_,_,list_transit))::q ->
	begin
	  let a = process_list list_transit
	  in
	    if a <> []
            then a @ gen_oper_states q class_name
            else gen_oper_states q class_name
	end
;;

(** ??? *)
let flat_classes classes =
  List.map 
    (fun (Uml.Class(class_name, _, _, _, _, _, _, states)) -> (class_name, states))
    classes
;;

(** ??? *)
let flat_states class_name lst =
  List.map
    (fun (Uml.State_machine(_,_,transit_list)) -> (class_name, transit_list))
    lst
;;

(** ??? *)
let flat_transit class_name lst =
  List.map (fun x -> (class_name, x)) lst ;;

(** ??? *)
let flat_event classes =
  let rec a lst =
    match lst with
	[] -> []
      | (class_name, transit_list)::q ->(flat_transit class_name transit_list) @ (a q)
  in let rec b lst =
      match lst with
          [] -> [] 
	| (class_name, states)::q -> (flat_states class_name states) @ (b q)
  in a (b (flat_classes classes))
;;

(** ??? *)
let fact_event lst =
  let rec a lst1 =
    match lst1 with
	[] -> []
      | (class_name, Uml.Transition(_, source, target, event, guard, action))::q ->
	  (event, class_name, [source, target, guard, action]) :: (a q)
  in a lst
;;

(** ??? *)
let rec clean_lst_1 lst =
  match lst with
      [] -> []
    | ((event, _, _) as t)::q ->
	if event <> ""
	then t::clean_lst_1 q
	else clean_lst_1 q
;;

(** ??? *)
let rec clean_lst_2 lst =
  match lst with
      [] -> []
    | ((_,Uml.Transition(_,_,_,_,_,action)) as t)::q ->
	if action <> ""
	then t::clean_lst_2 q
	else clean_lst_2 q
;;

(** ??? *)
let gen_oper_event classes =
  let process_list_1 elem =
    let gen_sub_hd class_name lst =
      match lst with (Uml.Source src, Uml.Target trg,guard,action) ->
	(
	  ( if guard <> ""
	    then
	      B.AND (
		B.Predicat1 (
		  B.Ima2 (
		    B.Type_base(B.Var2("state_"^(lc class_name))),
		    B.Type_base(B.Var2 "obj") ),
		  B.Egal,
		  B.Type_base(B.Var2 (lc (match_state src))) ),
		B.Predicat4 guard )
            else
	      B.Predicat1 (
		B.Ima2 (
		  B.Type_base(B.Var2("state_"^(lc class_name))),
		  B.Type_base(B.Var2 "obj") ),
		B.Egal,
		B.Type_base(B.Var2 (lc (match_state src))) ) )
	   ,
	   B.Parallel (
	     [B.Appel1 (
		B.def_ident("transition_"^(lc (match_state src))^"_"^(lc (match_state trg))),
		[B.def_ident "obj"] )]
	     @ ( if action <> ""
		 then
		   [B.If (
		      B.Predicat4(
			"precondition_"^action),
		      B.Appel1(B.def_ident("substitution_"^action), []),
		      B.SKIP )]
		 else [] ) )
	)
    and gen_sub_tl class_name lst =
      match lst with (Uml.Source src, Uml.Target trg,guard,action) ->
	(
	  [B.Predicat1 (
	     B.Ima2 (
	       B.Type_base(B.Var2("state_"^(lc class_name))),
	       B.Type_base(B.Var2 "obj") ),
	     B.Egal,
	     B.Type_base(B.Var2 (lc (match_state src))) )]
	  @ ( if guard <> ""
	      then [B.Predicat4 guard]
	      else [] )
	   ,
	   B.Parallel (
	     [B.Appel1 (
		B.def_ident("transition_"^(lc (match_state src))^"_"^(lc (match_state trg))),
		[B.def_ident "obj"] )]
	     @ ( if action <> ""
		 then
		   [B.If (
		      B.Predicat4("precondition_"^action),
		      B.Appel1(B.def_ident("substitution_"^action), []),
		      B.SKIP )]
		 else []) )
	)
    in
    match elem with (event, class_name, lst) ->
      if 2 <= (List.length lst)
      then
        B.Oper (
	  [],
	  B.def_ident(lc event),
	  [B.def_ident "obj"],
	  B.Pre [B.Predicat1(
		   B.Type_base(B.Var2 "obj"),
		   B.Appartient,
		   B.Type_base(B.Var2 (lc class_name)))],
	  B.SELECT (
	    gen_sub_hd class_name (List.hd lst),
	    (List.map (gen_sub_tl class_name) (List.tl lst))
	    @ ([ ([],B.SKIP) ])
	  ) )
      else
        B.Oper (
	  [],
	  B.def_ident(lc event),
	  [B.def_ident "obj"],
	  B.Pre [B.Predicat1(
		   B.Type_base(B.Var2 "obj"),
		   B.Appartient,
		   B.Type_base(B.Var2 (lc class_name)))],
	  B.SELECT (
	    gen_sub_hd class_name (List.hd lst),
	    [ ([],B.SKIP) ]
	  ) )      
  and process_list_2 elem =
    match elem with (_,Uml.Transition(_,_,_,_,_,action)) ->
      B.Oper (
	[],
	B.def_ident(action),
	[],
	B.Pre[B.Predicat4("precondition_"^action)],
	B.Appel1(B.def_ident("substitution_"^action), []) )
  in
    (List.map process_list_1 (clean_lst_1(fact_event (flat_event classes))))
    @ (List.map process_list_2 (clean_lst_2(flat_event classes)))
;;

(** ??? *)
let rec gen_invariant_constraint consts =
  match consts with
      [] -> []
    | t::q ->
	let ocl_abs = OclParser.main OclLexer.token (Lexing.from_string t) in
	let b_abs =
	  try
	    Ocl2b.constraint_conv ocl_abs
	  with
	      Failure "eval_context()" ->
		
		print_string (
		  "dans la contrainte OCL :\n"^t
		  ^"\n\nAttention : les contraintes OCL doivent respecter"
		  ^"\nles r�gles typographiques suivantes :"
		  ^"\n\t- Classes et types commencent par une majuscule"
		  ^"\n\t- Associations, r�les, attributs, op�rations"
		  ^"\n\t(et leurs param�tres) commencent par une minuscule\n\n");
		let ocl_abs = OclParser.main OclLexer.token (Lexing.from_string "") in
		  Ocl2b.constraint_conv ocl_abs
		    
	in
	let inv = B.acces_inv b_abs in
	  inv @ (gen_invariant_constraint q)
;;

(** ??? *)
let gen_oper_constraint lst consts =
  let rec process_oper consts =
    match consts with
	[] -> []
      | t::q ->
	  let ocl_abs = OclParser.main OclLexer.token (Lexing.from_string t) in
	  let b_abs =
	    try
	      Ocl2b.constraint_conv ocl_abs
	    with
		Failure "eval_context()" ->
		  print_string (
		    "dans la contrainte OCL :\n"^t
		    ^"\n\nAttention : les contraintes OCL doivent respecter"
		    ^"\nles r�gles typographiques suivantes :"
		    ^"\n\t- Classes et types commencent par une majuscule"
		    ^"\n\t- Associations, r�les, attributs, op�rations"
		    ^"\n\t(et leurs param�tres) commencent par une minuscule\n\n");
		  let ocl_abs = OclParser.main OclLexer.token (Lexing.from_string "") in
		    Ocl2b.constraint_conv ocl_abs
	  in
	  let oper = B.acces_opers b_abs in
	    oper @ (process_oper q)
  in
  let merge x y =
    if B.acces_oper_id x = B.acces_oper_id y
    then
      begin
	let (a,b) = match y with
	    B.Oper(_,_,_,B.Pre pre,sub) -> (pre,sub)
	in match x with
	    B.Oper(c,d,e,B.Pre f,g) -> B.Oper(c,d,e,B.Pre(a@f),B.Parallel [g;b])
      end
    else x
  in
  let rec insert_oper oper new_lst =
    match oper with
	[] -> new_lst
      | t::q ->
	  let a =
	    try
	      (List.find (fun x -> (B.acces_oper_id x = B.acces_oper_id t)) new_lst)
	    with
		Not_found -> B.Oper([],B.Var "",[],B.Pre[],B.SKIP)
          in
	    match a with
		B.Oper(_,B.Var "",_,_,B.SKIP) -> insert_oper q (t::new_lst)
	      | B.Oper(_,_,_,_,_) ->
		  insert_oper q (List.map (fun x -> merge a x) new_lst)
  in
    insert_oper (process_oper consts) lst
;;

(** ??? *)
let merge_oper t y =
  match t with
    | B.Operations x -> B.Operations (x@y)
    | _ -> failwith "merge_oper()"
;;



(*** ??? ***)

(** ? *)
let gen_b_ast ast =
  let gen_machines name classes associations =

    let gen_system_machine =
      let gen_name = B.def_ident(cp name)
      and gen_includes =
	let lst = gen_includes_system associations classes
	in B.def_includes lst
      and gen_variables = gen_variables_assoc associations
      and gen_initialisation =
	B.Initialisation (B.Parallel (gen_init_assoc associations))
      and gen_invariant =
	let lst = (gen_invariant_assoc associations)
	          @ (gen_invariant_aggregate associations)
	in B.Invariant lst
      and gen_operations =
	let lst = (gen_assoc_operations associations)
		  @ (gen_oper_event classes)
	in B.Operations lst
      in B.Machine (
	  (gen_name, []),
	  [ B.Sets []; B.Uses []; B.Extends []; gen_includes;
	    B.Promotes []; B.Definitions []; gen_variables;
	    gen_invariant; gen_initialisation;  gen_operations ] )

    and gen_all_machines associations uml_class =
      let class_to_machine class_name multip class_assoc inherits attribs opers consts states =
	let gen_class_name = B.def_ident(cp class_name)
	and gen_sets =
	  let lst = (if (List.length inherits = 0)
		     then [B.Set1 (B.def_ident(uc class_name))]
		     else [])
	            @ (gen_state_sets class_name states)
          in B.Sets lst
	and gen_uses =
	  let rec gen_inherits m =
	    match m with
	        [] -> []
	      | (Uml.Generalization(Uml.Parent parent))::q -> parent::(gen_inherits q)
	  in gen_inherits inherits
        and gen_uses_class_assoc =
	  let rec gen_assocs m =
	    match m with
		[] -> []
	      | ( Uml.Association(_, Uml.Role(_,_,_,Uml.Role_type a)
				    ::Uml.Role(_,_,_,Uml.Role_type b) ::c) )::q ->
		  a :: b :: gen_assocs q
	      | _ -> failwith "gen_assocs()"
	  in gen_assocs class_assoc
        and gen_variables =
	  let lst = [class_name]
	            @ (gen_attributes attribs)
	            @ (gen_variables_compose associations class_name)
		    @ (gen_states_variables states class_name)
          in B.def_variables lst
	and gen_includes =
	  let lst = gen_includes_compose associations class_name
	  in B.def_includes lst
	and gen_invariant =
	  let lst = ( if (List.length inherits = 0) && (class_assoc = [])
		      then gen_invariant_sets class_name
		      else [] )
                    @ ( if List.length class_assoc <> 0
			then gen_invariant_assoc class_assoc
			else [])
	            @ (gen_invariant_attributes attribs class_name)
	            @ (gen_invariant_uses class_name inherits)
	            @ (gen_card multip class_name)
		    @ (gen_invariant_compose associations class_name)
		    @ (gen_invariant_constraint consts)
		    @ (gen_invariant_states states class_name)
	  in B.Invariant lst
        and gen_initialisation =
	  let lst = [B.Sub (B.Val (B.def_ident (lc class_name)), B.Empty_set)]
	            @ (gen_init_attributes attribs)
		    @ (gen_init_compose associations class_name)
		    @ (gen_init_states states class_name)
          in B.Initialisation (B.Parallel lst)
	and gen_operations opers class_assoc =
	  let lst =
	    ( if class_assoc=[]
	      then [create_operation class_name; destroy_operation class_name]
	      else [])
	    @ (List.map gen_class_opers opers)
	    @ (gen_oper_states states class_name)
	  in B.Operations (gen_oper_constraint lst consts)
	in begin
	    if class_assoc = []
            then
	      B.Machine (
		(gen_class_name, []),
		[ gen_sets; B.def_uses gen_uses; B.Extends []; gen_includes;
		  B.Promotes []; B.Definitions []; gen_variables;
		  gen_invariant; gen_initialisation; gen_operations opers class_assoc ] )
            else
	      B.Machine (
		(gen_class_name, []),
		[ B.Sets []; B.def_uses(gen_uses@gen_uses_class_assoc);
		  B.Extends []; gen_includes; B.Promotes []; B.Definitions [];
		  gen_variables; gen_invariant; gen_initialisation;
		  merge_oper
		    (gen_operations opers class_assoc)
		    (gen_assoc_operations class_assoc) ] )
	  end
      in
	match uml_class with
	    Uml.Class (class_name, multip, class_assoc, inherits,
		       attribs, opers, consts, states) ->
	      class_to_machine class_name multip class_assoc inherits attribs opers consts states
    in
      [gen_system_machine] @ (List.map (gen_all_machines associations) classes)
  in
    match ast with
	Uml.Model (name, classes, associations) ->
	  Ocl2b.model_sys := ast;
	  Ocl2b.model_classes := classes;
	  Ocl2b.model_assocs := associations;
	  gen_machines name classes associations
;;



(*** Print B Machines (B Ocaml AST -> B Ocaml CST -> String) ***)

(** ??? *)
let rec store directory lst =
  let rec to_file l =
    match l with
	[] -> ()
      | t::q ->
	  begin
	    let oc =
	      open_out
		(directory ^ "/" ^
		 ( match t with
		       B.Machine ((B.Var name, _), _) -> name ^ ".mch"
		     | B.Refinement ((B.Var name, _), _, _) -> name ^ ".ref"
		     | _ -> failwith "store()" ))
	    in let s = B2string.machine t
	    in output oc s 0 (String.length s); close_out oc; to_file q
	  end
  in to_file lst
;;



(*** Refinement ***)

(** ??? *)
let gen_ref ast1 ast2 =
  match ast2 with
      B.Machine (name, lst)::q ->
	B.Refinement (name, B.acces_nom (List.hd ast1), lst) :: q
    | _ -> []
;;

(** ??? *)
let gen_ref_ast ast1 ast2 =
let classes_names1 =
  List.map
    (fun(Uml.Class(class_name,_,_,_,_,_,_,_)) -> (class_name))
    (match ast1 with Uml.Model(_, classes, _) -> classes)
and process_classes_ast2 classes2 names1 =
  List.map
    ( fun (Uml.Class(class_name,a,b,c,d,e,f,g)) ->
	if List.mem class_name names1
	then (Uml.Class(class_name^"_ref",a,b,c,d,e,f,g))
	else Uml.Class(class_name,a,b,c,d,e,f,g) )
    classes2
and process_assocs_ast2 assocs2 names1 =
  List.map
    ( function
	  (Uml.Association(a, Uml.Role(b1,c1,d1, Uml.Role_type name1)
			     ::Uml.Role(b2,c2,d2, Uml.Role_type name2)::q)) ->
	    if (List.mem name1 names1) && (List.mem name2 names1)
	    then
	      Uml.Association(a, Uml.Role(b1,c1,d1, Uml.Role_type(name1^"_ref"))
				::[Uml.Role(b2,c2,d2, Uml.Role_type(name2^"_ref"))])
	    else
	      if List.mem name1 names1
	      then
		Uml.Association(a, Uml.Role(b1,c1,d1, Uml.Role_type (name1^"_ref"))
				  ::[Uml.Role(b2,c2,d2, Uml.Role_type(name2))])
	      else
		if List.mem name2 names1
		then
		  Uml.Association(a, Uml.Role(b1,c1,d1, Uml.Role_type name1)
				    ::[Uml.Role(b2,c2,d2, Uml.Role_type(name2^"_ref"))])
		else
		  Uml.Association(a, Uml.Role(b1,c1,d1, Uml.Role_type name1)
				    ::[Uml.Role(b2,c2,d2, Uml.Role_type name2)])
	| _ -> failwith "process_assocs_ast2" )
    assocs2
in match ast2 with
    Uml.Model(a, classes, assocs) ->
      Uml.Model (
	a,
	process_classes_ast2 classes classes_names1,
	process_assocs_ast2 assocs classes_names1 )
;;



(*** Ajout d'un pr�fixe devant les noms d'attributs et d'op�rations
qui apparaissent dans plusieurs classes du mod�le ***)

(** prend comme argument une liste de classes
@return un couple form� de la liste des noms qui apparaissent
        dans plusieurs classes, et de la liste de toutes les classes *)
let rec find_identical lst ident cl_list =
  (** cl_n d�signe la classe dans laquelle appara�t n *)
  let add_name n cl_n ident =
    if List.mem_assoc n ident
    then
      let occ = List.assoc n ident in
      let ident2 = List.remove_assoc n ident in
	(n, occ+1)::ident2
    else (n, 1)::ident
  in
  (** ajout d'un nom d'attribut *)
  let rec add_attr cl_n l ident =
    match l with
	[] -> ident
      | (Uml.Attribute (name, m, y, i))::t ->
	  add_attr cl_n t (add_name name cl_n ident)
  (** ajout d'un nom d'op�ration *)
  and add_oper cl_n l ident =
    match l with
	[] -> ident
      | (Uml.Operation (name, pa, pr))::t ->
	  add_oper cl_n t (add_name name cl_n ident)
  in
    match lst with
	[] ->
	  (* � la fin, on ne garde que les associations pour lesquelles
	     la liste contient plusieurs noms de classe *)
	  let elim_single (x,y) l = if y>=2 then x::l else l in
	  let ident2 = List.fold_right elim_single ident [] in
	    (ident2, cl_list)
      | (Uml.Class (name, m, a, i, attr, oper, c, s))::t ->
	  let ident2 = add_attr name attr ident in
	  let ident3 = add_oper name oper ident2 in
	    find_identical t ident3 (name::cl_list)
;;

(** prend comme argument une liste de noms de classes
@return une liste associative (nom_de_classe, pr�fixe_correspondant),
        tous les pr�fixes �tant diff�rents *)
let find_all_prefixes lst =
  let rec find_prefixe n l len =
    match l with
	[] -> (lc (String.sub n 0 len))^"_"
      | h::t ->
	  if (String.length h >= len
	      && String.sub h 0 len = String.sub n 0 len)
	  then find_prefixe n l (len+1)
	  else find_prefixe n t len
  in
  let rec process_list l =
    match l with
	[] -> []
      | h::t -> (h, find_prefixe h t 1)::(process_list t)
  in
  let cmp_length x y = String.length y - String.length x in
  let sorted_lst = List.fast_sort cmp_length lst in
    process_list sorted_lst
;;

(** prend comme argument une liste de classes,
une liste de noms qui apparaissent dans plusieurs classes
et une liste associative (nom_de_classe, pr�fixe_correspondant)
@return la liste de classes, dans lesquelles les noms
        d'attributs et/ou d'op�rations qui apparaissaient
        dans d'autres classes ont �t� pr�fix�s *)
let rec modif_identical lst ident prefixes =
  (** applique les modifications � une liste d'attributs *)
  let rec modif_attr cl_n l =
    match l with
	[] -> []
      | (Uml.Attribute (name, m, y, i) as h)::t ->
	  let new_attr =
	    if List.mem name ident
	    then
	      let new_name = (List.assoc cl_n prefixes)^name in
	      Uml.Attribute (new_name, m, y, i)
	    else h
	  in new_attr::(modif_attr cl_n t)
  (** applique les modifications � une liste d'op�rations *)
  and modif_oper cl_n l =
    match l with
	[] -> []
      | (Uml.Operation (name, pa, pr) as h)::t ->
	  let new_oper =
	    if List.mem name ident
	    then
	      let new_name = (List.assoc cl_n prefixes)^name in
	      Uml.Operation (new_name, pa, pr)
	    else h
	  in new_oper::(modif_oper cl_n t)
in
  match lst with
      [] -> []
    | (Uml.Class (name, m, a, i, attr, oper, c, s))::t ->
	let new_attr = modif_attr name attr
	and new_oper = modif_oper name oper in
	let new_cl = Uml.Class (name, m, a, i, new_attr, new_oper, c, s) in
	  new_cl::(modif_identical t ident prefixes)
;;

(** ajout d'un pr�fixe devant les noms d'attributs et d'op�rations
qui apparaissent de fa�on identique dans plusieurs classes
(afin d'�viter les conflits de noms lors de la traduction en machines B) *)
let resolve_name_conflicts (Uml.Model (name, cl, assoc)) =
  let (ident, all_cl) = find_identical cl [] [] in
  let prefixes = find_all_prefixes all_cl in
  let new_cl = modif_identical cl ident prefixes in
    Uml.Model (name, new_cl, assoc)
;;



(*** Chargement du mod�le UML ***)

(** prend en argument un fichier XMI g�n�r� par Poseidon *)
let uml_to_b filename solution directory =
  let document =
    Pxp_tree_parser.parse_wfcontent_entity
      Pxp_types.default_config
      (Pxp_types.from_file filename)
      Pxp_tree_parser.default_spec in
  let root = document # root in
  let uml_ast = Xmi2uml.get_uml_ast root in
  let uml_ast_renamed = resolve_name_conflicts uml_ast in
  let b_ast = gen_b_ast uml_ast_renamed in
    if solution <> ""
    then
      let document2 =
	Pxp_tree_parser.parse_wfcontent_entity
	  Pxp_types.default_config
	  (Pxp_types.from_file solution)
	  Pxp_tree_parser.default_spec in
      let root2 = document2 # root in
      let uml_ast2 = Xmi2uml.get_uml_ast root2 in
      let uml_ast2_renamed = resolve_name_conflicts uml_ast2 in
      let b_ast2 = gen_b_ast uml_ast2_renamed in
      let b_ast3 = gen_ref b_ast b_ast2 in
	store directory (b_ast @ b_ast3)
    else
      store directory b_ast
;;



(*** Traitement des arguments pass�s en ligne de commande ***)

let _ =
  let file = ref None
  and directory = ref None
  and solution = ref None
  and set_dr directory s = directory := Some s
  and get_sol solution s = solution := Some s
  and version () = print_endline("version 0.1\n")
  in
    Arg.parse
      [("-v", Arg.Unit (version), "current version of uml2b");
       ("-sol", Arg.String (get_sol solution), "solution input");
       ("-d", Arg.String (set_dr directory), "output directory")
      ]
      (fun x -> match !file with
	   None -> file := Some x
         | Some _ -> raise (Arg.Bad "Multiple argument not allowed"))
      "usage : uml2b [-v] [-help] problem.xml [-sol] [solution.xml] [-d] [output directory]";
    let fn =
      match !file with
	  None ->
	    prerr_endline "uml2b : no input";
	    exit 1
	| Some s -> s
    and sol =
      match !solution with
	  None -> ""
	| Some s -> s
    and dr =
      match !directory with
	  None -> ""
	| Some s -> s
    in
      run uml_to_b fn sol dr
;;

