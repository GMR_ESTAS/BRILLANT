(*** $Id$ ***)



(** {5 Conversion du XMI d'Objecteering en syntaxe abstraite UML }
@author 
@version 
*)



open Pxp_types
open Pxp_document
open Pxp_yacc





(** Liste associative des abr�viations des �l�ments du XMI *)
let uml_assoc_xmi = [
  "Model", "Model_Management.Model";
  "name", "Foundation.Core.ModelElement.name";
  "ownedElement", "Foundation.Core.Namespace.ownedElement";
  "Class", "Foundation.Core.Class";
  "feature", "Foundation.Core.Classifier.feature";
  "Attribute", "Foundation.Core.Attribute";
  "generalization", "Foundation.Core.GeneralizableElement.generalization";
  "Generalization", "Foundation.Core.Generalization";
  "parent", "Foundation.Core.Generalization.parent";
  "Operation", "Foundation.Core.Operation";
  "Parameter", "Foundation.Core.Parameter";
  "kind", "Foundation.Core.Parameter.kind";
  "Association", "Foundation.Core.Association";
  "AssociationClass", "Foundation.Core.AssociationClass"; 
  "connection", "Foundation.Core.Association.connection";
  "AssociationEnd", "Foundation.Core.AssociationEnd";
  "aggregation", "Foundation.Core.AssociationEnd.aggregation";
  "multiplicity", "Foundation.Core.AssociationEnd.multiplicity";
  "Multiplicity", "Foundation.Data_Types.Multiplicity";
  "range", "Foundation.Data_Types.Multiplicity.range";
  "MultiplicityRange", "Foundation.Data_Types.MultiplicityRange";
  "lower", "Foundation.Data_Types.MultiplicityRange.lower";
  "upper", "Foundation.Data_Types.MultiplicityRange.upper";
  "type", "Foundation.Core.AssociationEnd.type";
  "DataType", "Foundation.Core.DataType";
  "body", "Foundation.Data_Types.Expression.body";
  "StateMachine", "Behavioral_Elements.State_Machines.StateMachine";
  "subvertex", "Behavioral_Elements.State_Machines.CompositeState.subvertex";
  "outgoing", "Behavioral_Elements.State_Machines.StateVertex.outgoing";
  "incoming", "Behavioral_Elements.State_Machines.StateVertex.incoming";
  "Transition", "Behavioral_Elements.State_Machines.Transition";
  "source", "Behavioral_Elements.State_Machines.Transition.source";
  "target", "Behavioral_Elements.State_Machines.Transition.target";
  "SimpleState", "Behavioral_Elements.State_Machines.SimpleState";
  "Pseudostate", "Behavioral_Elements.State_Machines.Pseudostate";
  "FinalState", "Behavioral_Elements.State_Machines.FinalState";
  "guard", "Behavioral_Elements.State_Machines.Transition.guard";
  "effect", "Behavioral_Elements.State_Machines.Transition.effect";
  "transitions", "Behavioral_Elements.State_Machines.StateMachine.transitions";
  "comment", "Foundation.Core.ModelElement.comment";
  "isNavigable", "Foundation.Core.AssociationEnd.isNavigable";
] ;;

(** acc�s � la liste associative des abr�viations des �l�ments du XMI *)
let elem_xmi elem =
  try List.assoc elem uml_assoc_xmi
  with Not_found -> "Not_found"



(*** Fonctions associ�es � l'arbre XML ***)

(** application d'une fonction sur les fils d'un noeud *)
let process_children f node =
  let children = node # sub_nodes
  in List.iter f children

(** donne la valeur d'un attribut XML *)
let value_of attrib =
  match attrib with
      Value s -> s
    | Implied_value -> ""
    | _ -> assert false
;;

(** donne le fils d'un �l�ment, avec un type sp�cifique *)
let child_of n with_type =
  find_element (elem_xmi with_type) n
;;

(** cherche en profondeur le premier �l�ment de l'arbre pour lequel f est vrai *)
let find_id idref base elem =
  let rec search_deep lst =
    match lst with
	[] -> raise Not_found
      | n :: q ->
	  if (value_of(n # attribute "xmi.id") = idref)
	  then n
	  else search_deep q
  in search_deep (find_all_elements ~deeply:true (elem_xmi elem) base)
;;



(*** Fonctions auxiliaires ***)

(** met une cha�ne de caract�res en majuscules *)
let uc s = String.uppercase s ;;

(** met une cha�ne de caract�res en minuscules *)
let lc s = String.lowercase s ;;

(** met en majuscule la premi�re lettre d'un mot *)
let cp s = String.capitalize s ;;

(** met en minuscule la premi�re lettre d'un mot *)
let ucp s = String.uncapitalize s ;;





(*** ??? Transformation Inheritances (UML Ocaml AST) -> Inheritances (UML Ocaml AST) ??? ***)

(** ??? *)
let get_inheritance_list n root =
  let rec process_list l =
    match l with
	[] -> []
      | t::q ->
	  Uml.Generalization(Uml.Parent(get_parent_name t)) :: process_list q

  and get_parent_name node =
    let generalizable_ref =
      value_of(
	(find_element
	   (elem_xmi "Class")
	   (find_element
	      (elem_xmi "parent")
	      (find_id
		 (value_of(node # attribute "xmi.idref"))
		 root
		 "Generalization")))
		 # attribute "xmi.idref")
    in
      (child_of (find_id generalizable_ref root "Class") "name") # data

  in
  let generalizable_list =
    try
      (find_all_elements
	 (elem_xmi "Generalization")
	 (find_element ~deeply:true (elem_xmi "generalization") n))
    with Not_found -> []
  in process_list generalizable_list
;;

(** ??? Transformation Attributes (UML Ocaml AST) -> Attributes (UML Ocaml AST) ??? *)
let get_attribute_list n root =
  let rec process_list lst =
    match lst with
	[] -> []
      | t::q ->
	  Uml.Attribute (get_attribute_name t,
			 get_multiplicity t,
			 get_attribute_type t,
			 get_initial_value t)::process_list q
  and get_attribute_name n = (child_of n "name") # data
  and get_multiplicity n =
    let multiplicity_lower =
      let a = (find_element ~deeply:true (elem_xmi "lower") n) # data
      in
	if a = "-1"
	then "*"
	else a
    and multiplicity_upper =
      let b = (find_element ~deeply:true (elem_xmi "upper") n) # data in
	if b = "-1"
	then "*"
	else b
    in Uml.Multiplicity (Uml.Range (multiplicity_lower, multiplicity_upper))
  and get_attribute_type n =
    let attribute_ref =
      try
	value_of(
	  (find_element ~deeply:true (elem_xmi "DataType") n)
		   # attribute "xmi.idref")
      with Not_found ->
	value_of(
	  (find_element ~deeply:true (elem_xmi "Class") n)
		   # attribute "xmi.idref")
    in match attribute_ref with
	"a4-15" -> Uml.Undefined
      | "a4-13" -> Uml.Str
      | "a4-7" -> Uml.Char
      | "a4-9" -> Uml.Integer
      | "a4-11" -> Uml.Real
      | "a4-5" -> Uml.Boolean
      | _ ->
	  Uml.Att_type((child_of(find_id attribute_ref root "Class") "name") # data)
  and get_initial_value n =
    try
      (find_element ~deeply:true (elem_xmi "body") n) # data
    with Not_found -> ""
  in let attribute_list =
      try find_all_elements ~deeply:true (elem_xmi "Attribute") n
      with Not_found -> []
  in process_list attribute_list
;;

(** ??? Transformation Operations (UML Ocaml AST) -> Attributes (UML Ocaml AST) ??? *)
let get_operation_list n root =
  let rec process_list lst =
    match lst with
	[] -> []
      | t::q ->
	  Uml.Operation (get_operation_name t, get_parameter_list t, get_return_parameter t)::process_list q
  and get_operation_name n = (child_of n "name") # data
  and get_parameter_list n =
    let rec process_parameter_list lst =
      match lst with
	  [] -> []
	| t::q ->
	    begin
	      if (value_of(
		    (find_element ~deeply:true (elem_xmi "kind") t)
			     # attribute "xmi.value")) <> "return"
	      then Uml.Parameter (get_parameter_name t, get_attribute_type t)::process_parameter_list q
	      else process_parameter_list q
	    end
    and get_parameter_name n = (child_of n "name") # data
    and get_attribute_type n =
      let attribute_ref =
	try
	  value_of((find_element ~deeply:true (elem_xmi "DataType") n) # attribute "xmi.idref")
	with Not_found ->
	  value_of((find_element ~deeply:true (elem_xmi "Class") n) # attribute "xmi.idref")
      in match attribute_ref with
	  "a4-15" -> Uml.Undefined
	| "a4-13" -> Uml.Str
	| "a4-7" -> Uml.Char
	| "a4-9" -> Uml.Integer
	| "a4-11" -> Uml.Real
	| "a4-5" -> Uml.Boolean
	| _ ->
	    Uml.Att_type((child_of(find_id attribute_ref root "Class") "name") # data)
    in let parameter_list =
	try find_all_elements ~deeply:true (elem_xmi "Parameter") n
	with Not_found -> []
    in process_parameter_list parameter_list
  and get_return_parameter n =
    let rec process_return_parameter_list lst =
      match lst with
	  [] -> []
	| t::q ->
	    begin
	      if (value_of(
		    (find_element ~deeply:true (elem_xmi "kind") t)
			     # attribute "xmi.value")) = "return"
	      then Uml.Parameter (get_parameter_name t, get_attribute_type t) :: process_return_parameter_list q
	      else process_return_parameter_list q
	    end
    and get_parameter_name n = (child_of n "name") # data
    and get_attribute_type n =
      let attribute_ref =
	try
	  value_of((find_element ~deeply:true (elem_xmi "DataType") n) # attribute "xmi.idref")
	with Not_found ->
	  value_of((find_element ~deeply:true (elem_xmi "Class") n) # attribute "xmi.idref")
      in match attribute_ref with
	  "a4-15" -> Uml.Undefined
	| "a4-13" -> Uml.Str
	| "a4-7" -> Uml.Char
	| "a4-9" -> Uml.Integer
	| "a4-11" -> Uml.Real
	| "a4-5" -> Uml.Boolean
	| _ ->
	    Uml.Att_type((child_of(find_id attribute_ref root "Class") "name") # data)
    in let parameter_list =
	try find_all_elements ~deeply:true (elem_xmi "Parameter") n
	with Not_found -> []
    in process_return_parameter_list parameter_list
  in let operation_list =
      try find_all_elements (elem_xmi "Operation") (find_element (elem_xmi "feature") n)
      with Not_found -> []
  in process_list operation_list
;;



(*** ??? Transformation Associations (UML Ocaml AST) -> Associations (UML Ocaml AST) ??? ***)

(** ??? *)
let get_association_name n base =
  if ((child_of n "name") # data) <> "undefined"
  then ((child_of n "name") # data)
  else
    begin
      let rec process_a a =
	match a with
	    [] -> ""
	  | t::q -> 
	      begin
		"_" ^ 
		(((child_of 
		     (find_id (value_of (
				 (find_element ~deeply:true (elem_xmi "Class") t)
					   # attribute "xmi.idref")) base "Class") "name")
		    # data))
		^ process_a q
	      end
    in let a_lst = find_all_elements ~deeply:true (elem_xmi "type") n
      in "assoc" ^ process_a a_lst
end
;;

(** ??? *)
let get_role_list n base =
  let rec process_list_role lst =
    match lst with
	[] -> []
      | t::q ->
	  if (value_of (
		(child_of t "isNavigable")
			  # attribute "xmi.value") <> "true")
	  then Uml.Role (get_role_name t, get_aggregation t, get_multiplicity t, get_role_type t)::process_list_role q
	  else (process_list_role q)@[Uml.Role (get_role_name t, get_aggregation t, get_multiplicity t, get_role_type t)]
  and get_role_name n = (child_of n "name") # data
  and get_aggregation n =
    let aggregation = value_of ((find_element ~deeply:true (elem_xmi "aggregation") n)
				  # attribute "xmi.value")
    in
      match aggregation with
	  "aggregate" -> Uml.Aggregate
	| "composite" -> Uml.Composite
	| "none" -> Uml.Ass_end
	| _ -> Uml.Ass_end
  and get_multiplicity n =
    let multiplicity_lower =
      let a = (find_element ~deeply:true (elem_xmi "lower") n) # data
      in
	if a = "-1"
	then "*"
	else a
    and multiplicity_upper =
      let b = (find_element ~deeply:true (elem_xmi "upper") n) # data
      in
	if b = "-1"
	then "*"
	else b
    in Uml.Multiplicity (Uml.Range (multiplicity_lower, multiplicity_upper))

  and get_role_type n =
    let type_ref = value_of ((find_element ~deeply:true (elem_xmi "Class") n)
			       # attribute "xmi.idref")
    in Uml.Role_type ((child_of (find_id type_ref base "Class") "name") # data)
  in let role_list = find_all_elements ~deeply:true (elem_xmi "AssociationEnd") n
  in process_list_role role_list
;;

(** ??? *)
let get_association_list n base =
  let rec process_list lst =
    match lst with
	[] -> []
      | n::q -> Uml.Association (get_association_name n base, get_role_list n base)::process_list q     
  in let association_list =
      try find_all_elements ~deeply:true (elem_xmi "Association") base
      with Not_found -> []
  in process_list association_list
;;

(** ??? *)
let get_class_association_list n base =
  let rec process_list lst =
    match lst with
	[] -> []
      | t::q -> Uml.Association ((child_of n "name") # data, get_role_list t base)::process_list q     
  in let association_list = try find_all_elements ~deeply:true (elem_xmi "connection") n with Not_found -> []
  in process_list association_list
;;



(*** ??? Transformation Constraints (UML Ocaml AST) -> (UML Ocaml AST) ??? ***)

(** ??? *)
let get_constraint_list n =
  let rec process_list l =
    match l with
	[] -> []
      | t::q -> (t # data) :: process_list q
  in let constraint_list =
      try
	(find_all_elements ~deeply:true (elem_xmi "name") (find_element ~deeply:true (elem_xmi "comment") n))
      with Not_found -> []
  in process_list constraint_list
;;



(*** ??? Transformation State machines (UML Ocaml AST) -> (UML Ocaml AST) ??? ***)

(** ??? *)
let get_state_machine n root =
  let rec process_list lst =
    match lst with
	[] -> []
      | t::q ->
	  Uml.State_machine (get_name t, get_states t, get_transitions t)::process_list q
  and get_name n = (child_of n "name") # data
  and get_states n =
    let rec process_list_states lst =
      match lst with
          [] -> []
	| t::q ->
	    begin
	      match (t # nth_node 0) # node_type with
		  T_element name when name = "Behavioral_Elements.State_Machines.SimpleState" ->
		    (Uml.Simplestate(get_name t))::process_list_states q
		| T_element name when name = "Behavioral_Elements.State_Machines.Pseudostate" ->
		    (Uml.Pseudostate ("initial_"^value_of((t # nth_node 0) # attribute "xmi.id")))::process_list_states q
		| T_element name when name = "Behavioral_Elements.State_Machines.FinalState" ->
		    (Uml.Finalstate("final_"^value_of((t # nth_node 0) # attribute "xmi.id")))::process_list_states q
		| _ -> process_list_states q
            end
    and get_name n = (find_element ~deeply:true (elem_xmi "name") n) # data
    in let state_list =
	try find_all_elements ~deeply:true (elem_xmi "subvertex") n
	with Not_found -> []
    in process_list_states state_list
  and get_transitions n =
    let rec process_list_transitions lst =
      match lst with
          [] -> []
	| t::q ->
	    Uml.Transition (get_name t, get_source t, get_target t, get_event t, get_guard t, get_action t)::process_list_transitions q
    and get_name n = (find_element ~deeply:true (elem_xmi "name") n) # data
    and get_source n =
      let the_node = (child_of n "source") # nth_node 0
      in
        match the_node # node_type with
            T_element name when name = "Behavioral_Elements.State_Machines.SimpleState" ->
	      Uml.Source(Uml.Simplestate((child_of (find_id (value_of(the_node # attribute "xmi.idref")) root "SimpleState") "name") # data))
          | T_element name when name = "Behavioral_Elements.State_Machines.Pseudostate" ->
	      Uml.Source(Uml.Pseudostate("initial_" ^ value_of((find_id (value_of(the_node # attribute "xmi.idref")) root "Pseudostate") # attribute "xmi.id")))
	  | T_element name when name = "Behavioral_Elements.State_Machines.FinalState" ->
	      Uml.Source(Uml.Finalstate("final_" ^ value_of((find_id (value_of(the_node # attribute "xmi.idref")) root "FinalState") # attribute "xmi.id")))
	  | _ -> Uml.Source(Uml.Simplestate "exception")
    and get_target n =
      let the_node = (child_of n "target") # nth_node 0
      in
        match the_node # node_type with
            T_element name when name = "Behavioral_Elements.State_Machines.SimpleState" ->
	      Uml.Target(Uml.Simplestate((child_of (find_id (value_of(the_node # attribute "xmi.idref")) root "SimpleState") "name") # data))
          | T_element name when name = "Behavioral_Elements.State_Machines.Pseudostate" ->
	      Uml.Target(Uml.Pseudostate("initial_" ^ value_of((find_id (value_of(the_node # attribute "xmi.idref")) root "Pseudostate") # attribute "xmi.id")))
	  | T_element name when name = "Behavioral_Elements.State_Machines.FinalState" ->
	      Uml.Target(Uml.Finalstate("final_" ^ value_of((find_id (value_of(the_node # attribute "xmi.idref")) root "FinalState") # attribute "xmi.id")))
	  | _ -> Uml.Target(Uml.Simplestate "exception")
    and get_event n =
      let expression = (find_element ~deeply:true (elem_xmi "body") (child_of n "effect")) # data
      in let i =
	  try (String.index expression '^')
	  with Not_found -> String.length expression 
      in String.sub expression 0 i
    and get_guard n =
      try (find_element ~deeply:true (elem_xmi "body") (child_of n "guard")) # data
      with Not_found -> ""
    and get_action n =
      let expression = (find_element ~deeply:true (elem_xmi "body") (child_of n "effect")) # data
      in let i =
	  try (String.index expression '^')
	  with Not_found -> 0
      in
	if i <> 0
	then String.sub expression (i+1) ((String.length expression)-(i+1))
	else ""
    in let transition_list =
	try
	  find_all_elements ~deeply:true (elem_xmi "Transition") (child_of n "transitions")
	with Not_found -> []
    in process_list_transitions transition_list
  in let state_machine_list =
      try find_all_elements ~deeply:true (elem_xmi "StateMachine") n
      with Not_found -> []
  in process_list state_machine_list
;;



(*** ??? Transformation Model (UML XMI AST) -> Model (UML Ocaml AST) ??? ***)

(** ??? *)
let get_uml_ast base =
  let get_model_name =
    try
      (child_of (find_element ~deeply:true (elem_xmi "Model") base) "name") # data
    with Not_found -> print_endline ("Not seems to be a uml model"); exit 1
  and get_class =
    let rec process_list lst =
      match lst with
	  [] -> []
	| t::q ->
	    if (t # data <> "")
	    then Uml.Class (get_class_name t,
			    Uml.Multiplicity (Uml.Range ("","")),
			    get_class_association_list t base,
			    get_inheritance_list t base,
			    get_attribute_list t base,
			    get_operation_list t base,
			    get_constraint_list t,
			    get_state_machine t base)::process_list q
	    else process_list q
    and get_class_name n = (child_of n "name") # data
    in let class_list =
	try find_all_elements ~deeply:true (elem_xmi "Class") base
	with Not_found -> []
    in let class_assoc_list =
	try find_all_elements ~deeply:true (elem_xmi "AssociationClass") base
	with Not_found -> []
    in process_list (class_list @ class_assoc_list)
  and get_association =
    let rec process_list lst =
      match lst with
	  [] -> []
	| t::q ->
	    Uml.Association (get_association_name t base, get_role_list t base)::process_list q     

    in let association_list =
	try find_all_elements ~deeply:true (elem_xmi "Association") base
	with Not_found -> []
    in process_list association_list
  in
    Uml.Model (get_model_name, get_class, get_association)
;;

