(*** $Id$ ***)





(** {5 R�gles de transformation d'UML/OCL vers B }
@author 
@version 
*)





let model_classes = ref []            (* liste des classes du mod�le *)
let model_assocs = ref []             (* liste des associations du mod�le *)
let model_sys = ref (Uml.Model("",[],[])) (* classes + associations *)



(*** Arbre de syntaxe abstraite B (UML2B) ***)

(** teste si une expression OCL d�signe un attribut
faisant partie d'une classe du mod�le UML *)
let rec is_attribute u =
  match u with
      Ocl.Prim_exp(Ocl.Name_lit a) 
    | Ocl.Prop_call2 (Ocl.Name_lit a) -> 
	begin match Uml.find_attrib2(a,!model_classes) with 
	    Uml.Attribute("",_,_,_) -> false
	  | _ -> true
	end
    | _ -> false

(** teste si une expression OCL d�signe un attribut
(faisant partie d'une classe du mod�le UML)
dont la multiplicit� est 1 ou 0..1 *)
and mult_un_zero u =
  match u with 
      Ocl.Prim_exp(Ocl.Name_lit a) 
    | Ocl.Prop_call2 (Ocl.Name_lit a) -> 
	let Uml.Attribute(b,c,d,e) = Uml.find_attrib2(a,!model_classes) in
	  (c=Uml.Multiplicity(Uml.Range("1","")) or c=Uml.Multiplicity(Uml.Range("0","1")))
    | _ -> false

(** teste si une expression OCL fait r�f�rence � un r�le
(faisant partie d'une association du mod�le UML)
dont la multiplicit� est 1 ou 0..1 *)
and is_unary u =
  match u with 
      Ocl.Prim_exp(Ocl.Name_lit v) 
    | Ocl.Prop_call2 (Ocl.Name_lit v) ->
	let (x,y) = Uml.find_assoc(v,!model_assocs) in 
	  begin match y with
	      Uml.Multiplicity(Uml.Range("1",""))
	    | Uml.Multiplicity(Uml.Range("0","1")) -> true
	    | _ -> false
	  end
    | _ -> false

(** ??? *)
and find_assoc_end u =
  match u with
      Ocl.Prim_exp (Ocl.Name_lit v) ->
	let (x,y) = Uml.find_assoc(v,!model_assocs) in 
	  begin match x with 
	      "" -> nlit (Ocl.Name_lit v)
	    | _ -> nlit (Ocl.Name_lit x)
	  end
    | Ocl.Prop_call2 (Ocl.Name_lit v) ->
	let (x,y) = Uml.find_assoc(v,!model_assocs) in 
	  begin match x with 
	      "" -> B.Val_av(nlit (Ocl.Name_lit v))
	    | _ -> B.Val_av(nlit (Ocl.Name_lit x))
	  end
    | Ocl.Prop_call4 _ -> ocl_b u
    | _ -> failwith "find_assoc_end()"

(** convertit une expression OCL en une expression B *)
and ocl_b ocl =
  (** traduction des op�rations sur collection et des expressions de requ�te OCL *)
  let map_property (u,v) =
    (* �quivalence des op�rations sur une collection OCL
    et des op�rateurs B ensemblistes ou sur les suites *)
    let map_op = function
	Ocl.Size  -> B.Card
      | Ocl.Intersection _ -> B.InterSets
      | Ocl.Union _ -> B.UnionSets
      | Ocl.Difference _ -> B.Difference
      | Ocl.Including _ -> B.UnionSets
      | Ocl.Excluding _ -> B.Difference
      | Ocl.Count -> B.Card
      | Ocl.First -> B.Front
      | Ocl.Last -> B.Last
      | Ocl.At _ -> B.At
      | Ocl.Append _ -> B.AppendSeq
      | Ocl.Prepend _ -> B.PrependSeq
      | _ -> failwith "map_property()"
    in
      match u with
	  Ocl.Intersection a 
	| Ocl.Union a 
	| Ocl.Difference a 
	    -> B.Op_bin (ocl_b v,map_op u,ocl_b a)
	| Ocl.Including a -> B.Op_bin (ocl_b v,map_op u, B.Ens(ocl_b a))
	    (* Reformulation / mise en �vidence des r�gles de transformation ?
	       let v1 = ocl_b v in
	       let a1 = ocl_b a in
	       let u1 = map_op u
	       Op_bin (v1, u1, Ens a1)
	    *)
	| Ocl.Size 
	| Ocl.Count 
	| Ocl.First 
	| Ocl.Last 
	    -> B.Op_un (map_op u,ocl_b v)
	| Ocl.Is_empty -> B.Op_bin (ocl_b v, B.Equal, B.Empty_set)
	| Ocl.Is_notempty -> B.Op_bin (ocl_b v, B.NotEqual, B.Empty_set)
	| Ocl.Includes a -> B.Op_bin (ocl_b a, B.In, ocl_b v)
	| Ocl.Includes_all a -> B.Op_bin (ocl_b a, B.SubSet, ocl_b v)
	| Ocl.Select  a -> get_obj(); select (a,v)
	| Ocl.Reject a -> get_obj(); reject (a,v)
	| Ocl.Collect a -> get_obj(); collect2 (a,v)
	| Ocl.Iterate a -> get_obj(); iterate (a,v)
	| Ocl.Exists a -> get_obj(); exists (a,v)
	| Ocl.Forall a -> get_obj(); forall (a,v)
	| _ -> failwith "map_property()"

  (** traduction des op�rateurs relationnels (=, >, <, >=, <=, <>) *)
  and map_rel_exp (u,v,w) =
    let map_op2 = function
	Ocl.Equal -> B.Equal
      | Ocl.Sup -> B.Greater
      | Ocl.Low -> B.Less
      | Ocl.Supequal -> B.GreaterEqual
      | Ocl.Lowequal -> B.LessEqual
      | Ocl.Different -> B.NotEqual
    in
      match u with
	  Ocl.Equal 
	| Ocl.Sup 
	| Ocl.Low 
	| Ocl.Supequal 
	| Ocl.Lowequal 
	| Ocl.Different
	    -> B.Op_bin (ocl_b v, map_op2 u, ocl_b w)
	(* | _ -> failwith "map_rel_exp()" *)

  (** traduction des op�rateurs logiques binaires: et, ou, => *)
  and map_log_exp (u,v,w) =
    let map_op2 = function
	Ocl.And -> B.And
      | Ocl.Or -> B.Or
      | Ocl.Implies -> B.Implies
      | Ocl.Xor -> failwith "map_op2(Xor)" (* ??? (comment traduire XOR en B ?) ??? *)
    in
      match u with
	  Ocl.And | Ocl.Or | Ocl.Implies ->
	    B.Op_bin (ocl_b v, map_op2 u, ocl_b w)
	| _ -> failwith "map_log_exp()"

  (** traduction des op�rateurs additifs (addition et soustraction binaire) *)
  and map_add_exp (u,v,w) =
    let map_op2 = function
	Ocl.Plus -> B.Plus
      | Ocl.Minus -> B.Minus
    in
      match u with
	  Ocl.Plus | Ocl.Minus ->
	    B.Op_bin (ocl_b v, map_op2 u, ocl_b w)
	(* | _ -> failwith"map_add_exp()" *)

  (** traduction des op�rateurs multiplicatifs (multiplication et division) *)
  and map_mult_exp (u,v,w) =
    let map_op2 = function
	Ocl.Multiply -> B.Mul
      | Ocl.Divide -> B.Div
    in
      match u with
	  Ocl.Multiply | Ocl.Divide ->
	    B.Op_bin (ocl_b v, map_op2 u, ocl_b w)
	(* | _ -> failwith "map_mult_exp()" *)

  (** traduction des op�rateurs unaires (moins unaire et n�gation logique) *)
  and map_unary_exp (u,v,w) =
    match u with
	Ocl.Not -> B.Op_un (B.Not, ocl_b v)
      | Ocl.Minus_op -> B.Op_un (B.UMinus, ocl_b v)
(*MG      | _ -> failwith "map_unary_exp()" *)

  in
    (* traduction des expressions OCL compos�es *)
    match ocl with
	Ocl.If_exp (a,b,c) -> B.If_exp (ocl_b a,ocl_b b,ocl_b c)
      | Ocl.Rel_exp2 (a,b,c) -> map_rel_exp (a,b,c)
      | Ocl.Log_exp2 (a,b,c) -> map_log_exp (a,b,c)
      | Ocl.Add_exp2 (a,b,c) -> map_add_exp (a,b,c)
      | Ocl.Mult_exp2 (a,b,c) -> map_mult_exp (a,b,c)
      | Ocl.Unary_exp1 (a,b) -> map_unary_exp (a,b,())
      | Ocl.Prop_call_par2 a -> ocl_b a
      | Ocl.Postf_exp3 (a,b) -> 
	  begin match b with
	      Ocl.Prop_call4 (c,d) ->
		B.Ima2 (find_assoc_end (Ocl.Prim_exp c), B.Par_list([ocl_b d;ocl_b a]))
	    | _ -> 
		if is_attribute(b)
		then 
		  if mult_un_zero(b)
		  then B.Ima2 (ocl_b b,ocl_b a)
		  else B.Ima3 (ocl_b b,B.Ens(ocl_b a))
		else
		  if is_unary(b)
		  then B.Ima2 (find_assoc_end b,ocl_b a)
		  else B.Ima3 (find_assoc_end b,B.Ens(ocl_b a))
	  end
      | Ocl.Postf_exp4 (a,b) -> map_property (b,a)
      | Ocl.Allinstances a -> B.Type_base(B.Var2(class_var a))
      | Ocl.Prim_exp a ->
	  begin match a with
	      Ocl.Name_lit b -> nlit a
	    | Ocl.Number_lit b -> B.Type_base(B.Nat2 b)
	    | Ocl.String_lit b -> B.Type_base(B.Var2 b)
	    | Ocl.Const b -> B.Type_base(B.Var2 (const b))
	    | _ -> nlit a
	  end
      | Ocl.Prop_call2 a ->
	  begin match a with
	      Ocl.Name_lit b -> B.Val_av(nlit a)
	    | Ocl.Number_lit b -> B.Val_av(B.Type_base(B.Nat2 b))
	    | Ocl.String_lit b -> B.Val_av(B.Type_base(B.Var2 b))
	    | _ -> B.Val_av(nlit a)
	  end
      | Ocl.Prop_call4 (a,b) -> B.Ima2 (find_assoc_end (Ocl.Prim_exp a),ocl_b b)
      | _ -> failwith "ocl_b()"

(** traduction des constantes bool�ennes *)
and const v =
  match v with
      Ocl.True -> "true"
    | Ocl.False -> "false"

(** ??? *)
and nlit v =
  match v with
      Ocl.Name_lit u ->
	begin match String.length u with
	    1 -> B.Type_base(B.Var2 (u^u))
	  | _ -> B.Type_base(B.Var2 u)
	end
    | Ocl.Path (u,w) -> nlit u
    | _ -> failwith ("nlit("^(Ocl2string.pathnamelist v)^")")

(** ??? *)
and decl ocl =
  let rec nlist (u,v) =
    match u with
	Ocl.Name_lit w -> B.Predicat1(nlit u,B.Appartient,B.Type_base(B.Var2 (class_var v)))
      | Ocl.Nlist (x,y) -> B.AND (nlist(x,v),nlist(y,v))
      | _ -> failwith "nlist()"
  in	      
    match ocl with
	Ocl.Decl1 (a,Ocl.Simp_type b) -> nlist (a,b) 
      | Ocl.Act_p_list1 (a,b) -> B.AND (decl a, decl b)
      | _ -> failwith "decl()"

(** ??? *)
and decl2 ocl =
  let rec nlist (u,v) =
    match u with
	Ocl.Name_lit w -> B.Exp_rel (nlit u,B.In,B.Type_base(B.Var2 (class_var v)))
      | Ocl.Nlist (x,y) ->  B.Exp_rel (nlist(x,v),B.And,nlist(y,v))
      | _ -> failwith "nlist()"
  in	      
    match ocl with
	Ocl.Decl1 (a,Ocl.Simp_type b) -> nlist (a,b) 
      | Ocl.Decl4 (a,Ocl.Simp_type b,_,_,_) -> nlist (a,b)
      | Ocl.Act_p_list1 (a,b) -> B.Exp_rel (decl2 a,B.And,decl2 b)
      | _ -> failwith "decl2()"

(** ??? *)
and map_decl_prop (c,d) =
  let rec nlist (u,v) =
    match u with
	Ocl.Name_lit w -> B.Exp_rel (nlit u,B.In,ocl_b v)
      | Ocl.Nlist (x,y) -> B.Exp_rel (nlist(x,v),B.And,nlist(y,v))
      | _ -> failwith "nlist()"
  in	      
    match c with
	Ocl.Decl1 (a,Ocl.Simp_type b) -> nlist (a,d) 
      | Ocl.Decl3 (a,_,_,_) -> nlist (a,d)
      | Ocl.Decl4 (a,_,_,_,_) -> nlist (a,d)
      | Ocl.Prim_exp a -> nlist (a,d)
      | Ocl.Act_p_list1 (a,b) -> B.Exp_rel (map_decl_prop(a,d),B.And,map_decl_prop(b,d))
      | _ -> failwith "map_decl_prop()"

(** retourne l'identificateur d'un type OCL *)
and get_id w =
    match w with
	Ocl.Type_lit x -> x
      | Ocl.Tlist (x,y) -> get_id x
      | _ -> failwith "get_id()"

(** conversion des types simples et des types collection *)
and typespec ocl =
  match ocl with
      Ocl.Simp_type a -> 
	begin match get_id a with
	  | "Integer" -> B.Var2 ("INTEGER")
	  | "String" -> B.Var2 ("STRING")
	  | "Natural" -> B.Var2 ("NATURAL")
	  | "Real" -> B.Var2("INTEGER")     (* pas d'ensemble de r�els en B *)
	  | _ -> B.Var2 (get_id a)
	end
    | Ocl.Coll_type (a,b) -> 
	begin match a with
	    Ocl.Set | Ocl.Collection | Ocl.Bag -> B.Pow(B.Var2 (get_id b))
	  | Ocl.Sequence -> B.Seq(B.Var2 (get_id b))
	end

(*MG     | _ -> failwith "typespec()" *)


(** fonction qui cr�� un nouvel identificateur
@param un identificateur (cha�ne de caract�res)
@return un nouvel identificateur, compos� de 2 lettres minuscules :
        - on double la lettre si l'identificateur de d�part ne comporte qu'une seule lettre
        - sinon, on retourne les deux premi�res lettres de l'identificateur de d�part  *)
and new_ident v = 
  let x = get_id v 
  in 
    match String.length(x) with
	1 -> Char.escaped(Char.lowercase(x.[0]))^Char.escaped(Char.lowercase(x.[0])) 
      | _ -> Char.escaped(Char.lowercase(x.[0]))^Char.escaped(Char.lowercase(x.[1]))

(** correspondance entre un type OCL et un nom de type B *)
and class_var v =
  let x = get_id v
  in 
    match x with
	"Integer" -> "INTEGER"
      | "String" -> "STRING"
      | "Natural" -> "NATURAL"
      | "Real" -> "INTEGER"    (* pas d'ensemble de r�els en B *)
      | _ ->                   (* types non pr�d�finis *)
	  begin match String.length(x) with
	      1 -> x.[0]<-Char.lowercase(x.[0]);x^x
	    | _ -> x.[0]<-Char.lowercase(x.[0]);x
	  end

(** extrait d'une expression OCL une liste d'identificateurs B *)
and get_vars v =
  let rec get_vars_nlist u =
    match u with
	    Ocl.Name_lit w ->
	      if String.length w = 1
	      then [B.Var (w^w)]
	      else [B.Var w]
	  | Ocl.Nlist (x,y) -> (get_vars_nlist x)@(get_vars_nlist y)
	  | _ -> failwith "get_vars_nlist()"
  in
    match v with
	Ocl.Decl1 (a,_) -> get_vars_nlist a 
      | Ocl.Decl3 (a,_,_,_) -> get_vars_nlist a
      | Ocl.Decl4 (a,_,_,_,_) -> get_vars_nlist a
      | Ocl.Act_p_list1 (a,b) -> (get_vars a)@(get_vars b)
      | Ocl.Prim_exp a -> get_vars_nlist a
      | _ -> []

(** convertit une contrainte OCL en une machine B, en remplissant :
- soit la clause INVARIANT, si la contrainte est un invariant
- soit la clause OPERATIONS, si la contrainte est une pr� ou post-condition *)
and constraint_conv ocl =
  (** ??? renvoit un nom ? *)
  let rec get_name = function
      (Ocl.Path (Ocl.Name_lit v,_)) -> v 
    | _ -> failwith "get_name()"

  (** ??? renvoit un type ??? *)
  and get_type = function
      (Ocl.Path (_,v)) -> v
    | _ -> failwith "get_type()"

  (** ??? cr�e un nouvel identificateur B  ??? *)
  and new_obj u = B.Var (new_ident(get_type u)) 

  (** ??? *)
  and obj_var u = class_var(get_type u)

  (** invariant de typage en B *)
  and obj_type u =  B.Predicat1(B.Type_base(B.Var2 (new_ident(get_type u))),
			      B.Appartient,B.Type_base(B.Var2 (obj_var u)))

  (** conversion du contexte de la contrainte *)
  and eval_context (a,b) =
    match a with
	Ocl.Context2 (Ocl.Simp_type c) ->
	  let new_exp = map_self_context(c,b)
	  and new_id = new_ident c			     
	  and new_var = class_var c in
	    init_obj();
	    begin match new_exp with
		Ocl.Inv2 c ->
		  B.Qlq ( [B.Var new_id],
			B.Predicat1(B.Type_base(B.Var2 new_id),B.Appartient,B.Type_base(B.Var2 new_var)),
			B.Predicat(ocl_b c) )
	      | Ocl.Inv1 (c,d) -> 
		  B.Qlq ( [B.Var new_id],
			B.Predicat1(B.Type_base(B.Var2 new_id),B.Appartient,B.Type_base(B.Var2 new_var)),
			B.Predicat(ocl_b d) )
	      | Ocl.Inv3 (c,d) -> 
		  B.AND(eval_context(a,c),eval_context(a,d))
	      | _ -> failwith "eval_context()"
	    end
      | _ -> failwith "eval_context()"

  (** conversion d'une pr�-condition OCL *)
  and eval_pre (a,b,i) =
    (** ??? retourne le pr�dicat B correspondant � une pr�-condition OCL ??? *)
    let rec get_pre (u,v) =
      let new_exp = map_self_context(get_type u,v) 
      in
	match new_exp with
	    Ocl.Pre2 w -> B.Predicat(ocl_b w)
	  | Ocl.Pre1 (w,x) -> B.Predicat (ocl_b x)
	  | Ocl.Pre3 (w,x) -> B.AND(get_pre(u,w),get_pre(u,x))
	  | _ -> failwith "get_pre()"

    (** ??? *)
    and post(u,v,w) =
      match w with
	  [] -> B.SKIP
	| [x] -> get_post(u,v,x)
	| _ -> failwith "post()"
    in
      match a with
	  Ocl.Context1 (Ocl.Prop_call4 (c,d)) -> 
	    begin match d with
		Ocl.Prop_call_par1 ->
		  B.Oper ( [],
			   B.Var(get_name c),
			   [new_obj c],
			   B.Pre((obj_type c)::[get_pre (c,b)]),
			   post(c,d,i) )
	      | Ocl.Prop_call_par2 e ->
		  B.Oper ( [],
			   B.Var(get_name c),
			   (new_obj c)::(get_vars e),
			   B.Pre((obj_type c)::[B.AND(decl e,get_pre(c,b))]),
			   post(c,d,i) )
	      | Ocl.Prop_call_par3 (e,f) ->
		  begin match e with
		      Ocl.Prop_call_par1 ->
			B.Oper ( [B.Var "res"],
				 B.Var(get_name c),
				 [new_obj c],
				 B.Pre((obj_type c)::[get_pre(c,b)]),
				 post(c,d,i) )
		    | Ocl.Decl1 (g,h) -> 
			B.Oper ( [B.Var "res"],
				 B.Var(get_name c),
				 (new_obj c)::(get_vars e),
				 B.Pre((obj_type c)::[B.AND(decl e,get_pre(c,b))]),
				 post(c,d,i) )
		    | Ocl.Act_p_list1 (g,h) ->
			B.Oper ( [B.Var "res"],
				 B.Var(get_name c),
				 (new_obj c)::(get_vars e),
				 B.Pre((obj_type c)::[B.AND(decl e,get_pre(c,b))]),
				 post(c,d,i) )
		    | _ -> failwith "eval_pre()"
		  end
	      | _ -> failwith "eval_pre()"
	    end
	| _ -> failwith "eval_pre()"

  (** ??? *)
  and get_result (c,d) = 
    match c with
	Ocl.Prop_call_par1
      | Ocl.Prop_call_par2 _ -> []
      | Ocl.Prop_call_par3 (e,f) -> 
	  begin match get_res d with
	      B.Empty_set ->
		[B.Dev_tq ( [B.Var "res"],
			    B.Op_bin(B.Type_base(B.Var2 "res"),
				     B.In,
				     B.Type_base(typespec f)) )]
	    | g -> 
		[B.Dev_tq ( [B.Var "res"],
			    B.Op_bin(B.Op_bin(B.Type_base(B.Var2 "res"),
					      B.In,
					      B.Type_base(typespec f)),
				     B.And,
				     g) )]
	  end
      | _ -> failwith "get_result()"

  (** ??? *)
  and pred_subst u = 
    match get_subst_pred u with
	B.Empty_set -> []
      | v -> let w = get_subst_vars v in [B.Dev_tq(w,B.Op_bin(get_type_vars(w),B.And,v))]

  (** retourne l'expression d'une post-condition *)
  and get_exp post = 
    match post with
	Ocl.Post2 c -> c 
      | Ocl.Post1 (c,d) -> d 
      | Ocl.Post3 (c,d) -> Ocl.Log_exp2 (Ocl.And,get_exp c,get_exp d)
      | _ -> failwith "get_exp()"

  (** ??? retourne le pr�dicat B correspondant � une post-condition OCL ??? *)
  and get_post (u,v,w) =
    let new_exp =
      begin
	set_context(get_id(get_type u));
	map_self_context(get_type u,w)
      end
    in
      match new_exp with
	  Ocl.Post2 x -> B.Parallel(pred_subst x@get_result (v,x))
	| Ocl.Post1 (x,y) -> B.Parallel(pred_subst y@get_result (v,y))	
	| Ocl.Post3 (x,y) -> B.Parallel(pred_subst (get_exp new_exp)@get_result (v,get_exp new_exp))
	| _ -> failwith "eval_post()"

  (** conversion d'une post-condition OCL *)
  and eval_post (a,b) =
    match a with
	Ocl.Context1 (Ocl.Prop_call4 (c,d)) -> 
	  begin match d with
	      Ocl.Prop_call_par1 ->
		B.Oper ( [],
			 B.Var(get_name c),
			 [new_obj c],
			 B.Pre[obj_type c],
			 get_post(c,d,b) )
	    | Ocl.Prop_call_par2 e ->
		B.Oper ( [],
			 B.Var(get_name c),
			 (new_obj c)::(get_vars e),
			 B.Pre((obj_type c)::[decl e]),
			 get_post(c,d,b) )
	    | Ocl.Prop_call_par3 (e,f) ->
		begin match e with
		    Ocl.Prop_call_par1 ->
		      B.Oper ( [B.Var "res"],
			       B.Var(get_name c),
			       [new_obj c],
			       B.Pre[obj_type c],
			       get_post(c,d,b) )
		  | Ocl.Decl1 (g,h) ->
		      B.Oper ( [B.Var "res"],
			       B.Var(get_name c),
			       (new_obj c)::(get_vars e),
			       B.Pre((obj_type c)::[decl e]),
			       get_post(c,d,b) )
		  | Ocl.Act_p_list1 (g,h) ->
		      B.Oper ( [B.Var "res"],
			       B.Var(get_name c),
			       (new_obj c)::(get_vars e),
			       B.Pre((obj_type c)::[decl e]),
			       get_post(c,d,b) )
		  | _ -> failwith "eval_post()"
		end
	    | _ -> failwith "eval_post()"
	  end
      | _ -> failwith "eval_post()"

  in
    match ocl with
	Ocl.Constraint4 (a,b) -> 
	  B.Machine (
	    (B.def_ident "OCL", []),
	    [ B.Sets []; B.Uses []; B.Extends []; B.Includes []; B.Promotes[];
	      B.Definitions []; B.VariablesAbstract []; B.Invariant [eval_context (a,b)];
	      B.Initialisation(B.Parallel []); B.Operations [] ] )
      | Ocl.Constraint1 (a,b) -> 
	  B.Machine (
	    (B.def_ident "OCL", []),
	    [ B.Sets []; B.Uses []; B.Extends []; B.Includes []; B.Promotes[];
	      B.Definitions []; B.VariablesAbstract []; B.Invariant [];
	      B.Initialisation(B.Parallel []); B.Operations [eval_pre(a,b,[])] ] )
      | Ocl.Constraint2 (a,b) ->
	  B.Machine (
	    (B.def_ident "OCL", []),
	    [ B.Sets []; B.Uses []; B.Extends []; B.Includes []; B.Promotes[];
	      B.Definitions []; B.VariablesAbstract []; B.Invariant [];
	      B.Initialisation(B.Parallel []); B.Operations [eval_post(a,b)] ] )
      | Ocl.Constraint3 (a,b,c) -> 
	  B.Machine (
	    (B.def_ident "OCL", []),
	    [ B.Sets []; B.Uses []; B.Extends []; B.Includes []; B.Promotes[];
	      B.Definitions []; B.VariablesAbstract []; B.Invariant [];
	      B.Initialisation(B.Parallel []); B.Operations [eval_pre(a,b,[c])] ] )
      | _ -> failwith "constraint_conv()"



(*** Fonctions auxiliaires (UML2B) ***)

and obj = ref 0 (* compteur d'objets d'une collection *)

(** ajoute un objet � la collection *)
and get_obj() = incr obj

(** vide une collection d'objets *)
and init_obj() =
  if !obj=0
  then ()
  else
    begin
      decr obj;
      init_obj()
    end

(** �crit le num�ro du dernier objet ajout� *)
and obj_n() = "obj"^string_of_int(!obj)


and context = ref "" (* contexte d'une contrainte OCL *)

(** retourne le contexte de la contrainte OCL *)
and get_context() = !context

(** assigne le contexte de la contrainte OCL *)
and set_context(ctx) = context := ctx

(** ??? *)
and context_from_type u =
  match u with
      Ocl.Type_lit a -> a
    | Ocl.Tlist (a,b) -> context_from_type a
    | _ -> failwith "context_from_type()"

(** remplace les occurrences du mot-cl� 'self' dans une expression OCL,
en fonction du contexte de la contrainte *)
and map_self_context (u,v) = 
  let rec map_self_pnlist (u,v) =
    match v with
	Ocl.Name_lit a ->
	  if a="self"
	  then Ocl.Name_lit (new_ident u)
	  else
	    if a="result"
	    then Ocl.Name_lit "res"
	    else v
      | Ocl.Tlist (a,b) -> Ocl.Tlist (map_self_pnlist (u,a),map_self_pnlist (u,b))
      | Ocl.Path (a,b) -> Ocl.Path (map_self_pnlist (u,a),map_self_pnlist (u,b))
      | Ocl.Nlist (a,b) -> Ocl.Nlist (map_self_pnlist (u,a),map_self_pnlist (u,b))
      | _ -> v
  in
    match v with
	Ocl.Inv1 (a,b) -> Ocl.Inv1 (a,map_self_context (u,b))
      | Ocl.Inv2 a -> Ocl.Inv2 (map_self_context (u,a))
      | Ocl.Inv3 (a,b) -> Ocl.Inv3 (map_self_context (u,a),map_self_context (u,b))
      | Ocl.Log_exp2 (a,b,c) -> Ocl.Log_exp2 (a,map_self_context (u,b),map_self_context (u,c))
      | Ocl.Rel_exp2 (a,b,c) -> Ocl.Rel_exp2 (a,map_self_context (u,b),map_self_context (u,c))
      | Ocl.Add_exp2 (a,b,c) -> Ocl.Add_exp2 (a,map_self_context (u,b),map_self_context (u,c))
      | Ocl.Mult_exp2 (a,b,c) -> Ocl.Mult_exp2 (a,map_self_context (u,b),map_self_context (u,c))
      | Ocl.Unary_exp1 (a,b) -> Ocl.Unary_exp1 (a,map_self_context (u,b))
      | Ocl.Postf_exp3 (a,b) -> Ocl.Postf_exp3 (map_self_context (u,a),map_self_context (u,b))
      | Ocl.Postf_exp4 (a,b) -> Ocl.Postf_exp4 (map_self_context (u,a),map_self_context (u,b))
      | Ocl.Prim_exp a -> Ocl.Prim_exp (map_self_pnlist (u,a))
      | Ocl.If_exp (a,b,c) ->
	  Ocl.If_exp (map_self_context (u,a),map_self_context (u,b),map_self_context (u,c))
      | Ocl.Prop_call2 a ->
	  Ocl.Prop_call2 (map_self_pnlist (u,a))
      | Ocl.Prop_call3 (a,b) ->
	  Ocl.Prop_call3 (map_self_pnlist (u,a),map_self_context (u,b))
      | Ocl.Prop_call4 (a,b) ->
	  Ocl.Prop_call4 (map_self_pnlist (u,a),map_self_context (u,b))
      | Ocl.Prop_call5 (a,b) ->
	  Ocl.Prop_call5 (map_self_pnlist (u,a),map_self_context (u,b))
      | Ocl.Prop_call6 (a,b) ->
	  Ocl.Prop_call6 (map_self_pnlist (u,a),map_self_context (u,b))
      | Ocl.Prop_call7 (a,b,c) ->
	  Ocl.Prop_call7 (map_self_pnlist (u,a),map_self_context (u,b),map_self_context (u,c))
      | Ocl.Prop_call8 (a,b,c) ->
	  Ocl.Prop_call8 (map_self_pnlist (u,a),map_self_context (u,b),map_self_context (u,c))
      | Ocl.Select a -> Ocl.Select (map_self_context (u,a))
      | Ocl.Collect a -> Ocl.Collect (map_self_context (u,a))
      | Ocl.Reject a -> Ocl.Reject (map_self_context (u,a))
      | Ocl.Iterate a -> Ocl.Iterate (map_self_context (u,a))
      | Ocl.Exists a -> Ocl.Exists (map_self_context (u,a))
      | Ocl.Forall a -> Ocl.Forall (map_self_context (u,a))
      | Ocl.Intersection a -> Ocl.Intersection (map_self_context (u,a))
      | Ocl.Union a -> Ocl.Union (map_self_context (u,a))
      | Ocl.Difference a -> Ocl.Difference (map_self_context (u,a))
      | Ocl.Symmdiff a -> Ocl.Symmdiff (map_self_context (u,a))
      | Ocl.Including a -> Ocl.Including (map_self_context (u,a))
      | Ocl.Excluding a -> Ocl.Excluding (map_self_context (u,a))
      | Ocl.Includes a -> Ocl.Includes (map_self_context (u,a))
      | Ocl.Includes_all a -> Ocl.Includes_all (map_self_context (u,a))
      | Ocl.At a -> Ocl.At (map_self_context (u,a))
      | Ocl.Append a -> Ocl.Append (map_self_context (u,a))
      | Ocl.Prepend a -> Ocl.Prepend (map_self_context (u,a))
      | Ocl.Ocliskindof a -> Ocl.Ocliskindof (map_self_context (u,a))
      | Ocl.Oclistypeof a -> Ocl.Oclistypeof (map_self_context (u,a))
      | Ocl.Oclastype a -> Ocl.Oclastype (map_self_context (u,a))
      | Ocl.Prop_call_par2 a ->
	  Ocl.Prop_call_par2 (map_self_context (u,a))
      | Ocl.Prop_call_par3 (a,b) ->
	  Ocl.Prop_call_par3 (map_self_context (u,a),b)
      | Ocl.Prop_call_par4 (a,b) ->
	  Ocl.Prop_call_par4 (map_self_context (u,a),map_self_context (u,b))
      | Ocl.Qualif a -> Ocl.Qualif (map_self_context (u,a))
      | Ocl.Act_p_list1 (a,b) -> Ocl.Act_p_list1 (map_self_context (u,a),map_self_context (u,b))
      | Ocl.Decl1 (a,b) ->
	  Ocl.Decl1 (map_self_pnlist (u,a),b)
      | Ocl.Decl3 (a,b,c,d) ->
	  Ocl.Decl3 (map_self_pnlist (u,a),map_self_pnlist (u,b),c,map_self_context (u,d))
      | Ocl.Decl4 (a,e,b,c,d) ->
	  Ocl.Decl4 (map_self_pnlist (u,a),e,map_self_pnlist (u,b),c,map_self_context (u,d))
      | Ocl.Pre1 (a,b) -> Ocl.Pre1 (map_self_pnlist (u,a),map_self_context (u,b))
      | Ocl.Pre2 a -> Ocl.Pre2 (map_self_context (u,a))
      | Ocl.Pre3 (a,b) -> Ocl.Pre3 (map_self_context (u,a),map_self_context (u,b))
      | Ocl.Post1 (a,b) -> Ocl.Post1 (map_self_pnlist (u,a),map_self_context (u,b))
      | Ocl.Post2 a -> Ocl.Post2 (map_self_context (u,a))
      | Ocl.Post3 (a,b) -> Ocl.Post3 (map_self_context (u,a),map_self_context (u,b))
      | _ -> v

(** effectue la conjonction logique d'une liste d'expressions *)
and and_list v =
  match v with
      [] -> B.Empty_set
    | a::[] -> a
    | a::b -> B.Op_bin(a,B.And,and_list b)

(** ??? *)
and get_subst_pred v =
  let rec subst_pred v =
    match v with
	Ocl.Log_exp2 (Ocl.And,b,c) -> (subst_pred b)@(subst_pred c)
      | Ocl.Prop_call_par2 a -> subst_pred a
      | _ ->
	  if not(is_res v)
	  then [ocl_b v]
	  else []
  in
    and_list(subst_pred v)

(** �limine les doublons d'une liste *)
and elim_doubles v =
  let rec elim (v,w) =
    match w with
	[] -> []
      | a::b ->
	  if a=v
	  then elim(v,b)
	  else a::elim(v,b) 
  in
    match v with
	[] -> []
      | a::b -> a::elim_doubles(elim(a,b))

(** ??? *)
and get_type_vars v =
  let rec type_vars v =
    match v with
	[] -> []
      | (B.Var a)::b -> get_type(!model_sys,a,!context)@(type_vars b)
  in
    and_list(type_vars v)

(** ??? *)
and get_subst_vars v =
  let rec subst_vars v =
    match v with
	B.Exp_rel (a,b,c) -> (get_subst_vars a)@(get_subst_vars c)
      | B.Type_base a -> 
	  begin match a with
	      B.Var2 b ->
		if is_property_of(!model_sys,b,!context)
		then [B.Var b]
		else []
	    | _ -> []
	  end
      | B.Val_av a -> get_subst_vars a
      | B.Dom a -> get_subst_vars a
      | B.Tilde a -> get_subst_vars a
      | B.Ens a -> get_subst_vars a
      | B.Rel(a,b,c) -> (get_subst_vars a)@(get_subst_vars c)
      | B.Tuple(a,b) -> (get_subst_vars a)@(get_subst_vars b)
      | B.Ima2(a,b)
      | B.Ima3(a,b) ->
	  (get_subst_vars a)@(get_subst_vars b)
      | B.Op_bin(a,b,c) -> (get_subst_vars a)@(get_subst_vars c)
      | B.Op_un(a,b) -> get_subst_vars b
      | B.Def_set(a,b) -> get_subst_vars b
      | B.Union_q (a,b,c) -> (get_subst_vars b)@(get_subst_vars c)
      | B.Inter_q (a,b,c) -> (get_subst_vars b)@(get_subst_vars c)
      | B.Sum_g (a,b,c) -> (get_subst_vars b)@(get_subst_vars c)
      | B.Prod_g (a,b,c) -> (get_subst_vars b)@(get_subst_vars c)
      | B.Forall(a,b,c) -> (get_subst_vars b)@(get_subst_vars c)
      | B.Exists(a,b,c) -> (get_subst_vars b)@(get_subst_vars c)
      | _ -> failwith "get_subst_vars()"
  in 
    elim_doubles(subst_vars v)

(** ??? *)
and get_res v =
  match v with
      Ocl.Log_exp2 (Ocl.And,b,c) -> 
	begin match (get_res b,get_res c) with
	    (B.Empty_set,B.Empty_set) -> B.Empty_set
	  | (B.Empty_set,d) -> d
	  | (d,B.Empty_set) -> d
	  | (d,e) -> B.Op_bin(d,B.And,e)
	end
    | Ocl.Prop_call_par2 a -> get_res a
    | _ ->
	if is_res v
	then ocl_b v
	else B.Empty_set

(** ??? teste si une expression OCL est un "r�sultat" ??? *)
and is_res v =
  let rec is_res_pnlist v =
    match v with
	Ocl.Name_lit a -> a="res"
      | Ocl.Tlist (a,b) -> (is_res_pnlist a) or (is_res_pnlist b)
      | Ocl.Path (a,b) -> (is_res_pnlist a) or (is_res_pnlist b)
      | Ocl.Nlist (a,b) -> (is_res_pnlist a) or (is_res_pnlist b)
      | _ -> false
  in
    match v with
	Ocl.Inv1 (a,b) -> is_res b
      | Ocl.Inv2 a -> is_res a
      | Ocl.Inv3 (a,b) -> (is_res a) or (is_res b)
      | Ocl.Log_exp2 (a,b,c) -> (is_res b) or (is_res c)
      | Ocl.Rel_exp2 (a,b,c) -> (is_res b) or (is_res c)
      | Ocl.Add_exp2 (a,b,c) -> (is_res b) or (is_res c)
      | Ocl.Mult_exp2 (a,b,c) -> (is_res b) or (is_res c)
      | Ocl.Unary_exp1 (a,b) -> is_res b
      | Ocl.Postf_exp3 (a,b) -> (is_res a) or (is_res b)
      | Ocl.Postf_exp4 (a,b) -> (is_res a) or (is_res b)
      | Ocl.Prim_exp a -> is_res_pnlist a
      | Ocl.If_exp (a,b,c) -> (is_res a) or (is_res b) or (is_res c)
      | Ocl.Prop_call2 a -> is_res_pnlist a
      | Ocl.Prop_call3 (a,b) -> (is_res_pnlist a) or (is_res b)
      | Ocl.Prop_call4 (a,b) -> (is_res_pnlist a) or (is_res b)
      | Ocl.Prop_call5 (a,b) -> (is_res_pnlist a) or (is_res b)
      | Ocl.Prop_call6 (a,b) -> (is_res_pnlist a) or (is_res b)
      | Ocl.Prop_call7 (a,b,c) -> (is_res_pnlist a) or (is_res b) or (is_res c)
      | Ocl.Prop_call8 (a,b,c) -> (is_res_pnlist a) or (is_res b) or (is_res c)
      | Ocl.Select a -> is_res a
      | Ocl.Collect a -> is_res a
      | Ocl.Reject a -> is_res a
      | Ocl.Iterate a -> is_res a
      | Ocl.Exists a -> is_res a
      | Ocl.Forall a -> is_res a
      | Ocl.Intersection a -> is_res a
      | Ocl.Union a -> is_res a
      | Ocl.Difference a -> is_res a
      | Ocl.Symmdiff a -> is_res a
      | Ocl.Including a -> is_res a
      | Ocl.Excluding a -> is_res a
      | Ocl.Includes a -> is_res a
      | Ocl.Includes_all a -> is_res a
      | Ocl.At a -> is_res a
      | Ocl.Append a -> is_res a
      | Ocl.Prepend a -> is_res a
      | Ocl.Prop_call_par2 a -> is_res a
      | Ocl.Prop_call_par3 (a,b) -> is_res a
      | Ocl.Prop_call_par4 (a,b) -> (is_res a) or (is_res b)
      | Ocl.Qualif a -> is_res a
      | Ocl.Act_p_list1 (a,b) -> (is_res a) or (is_res b)
      | Ocl.Decl1 (a,b) -> is_res_pnlist a
      | Ocl.Decl3 (a,b,c,d) -> (is_res_pnlist a) or (is_res_pnlist b) or (is_res d)
      | Ocl.Decl4 (a,e,b,c,d) -> (is_res_pnlist) a or (is_res_pnlist b) or (is_res d)
      | _ -> false

(** valeur d'une liste d'identificateurs
@return liste de cha�nes de caract�res correspondant aux noms de ces identificateurs *)
and devars w = 
    match w with 
	[] -> []
      | a::b -> (B.devar a)::(devars b)

(** teste si un objet appartient � une liste *)
and is_elem_of (a,b) = List.mem b a
(*
  match a with
      [] -> false
    | c::d -> if c=b then true else is_elem_of(d,b)
*)

(** ??? *)
and map_prop (u,v) = 
  let rec map_prop_pnlist (x,y) =
    match y with
	Ocl.Name_lit a ->
	  if is_elem_of(devars(get_vars x),List.hd(devars(get_vars(Ocl.Prim_exp y))))
	  then Ocl.Prim_exp y
	  else Ocl.Postf_exp3(Ocl.Prim_exp(Ocl.Name_lit(List.hd(devars(get_vars x)))),Ocl.Prim_exp y)
      | _ -> Ocl.Prim_exp y
  in
    match v with
	Ocl.Log_exp2 (a,b,c) -> Ocl.Log_exp2 (a,map_prop (u,b),map_prop (u,c))
      | Ocl.Rel_exp2 (a,b,c) -> Ocl.Rel_exp2 (a,map_prop (u,b),map_prop (u,c))
      | Ocl.Add_exp2 (a,b,c) -> Ocl.Add_exp2 (a,map_prop (u,b),map_prop (u,c))
      | Ocl.Mult_exp2 (a,b,c) -> Ocl.Mult_exp2 (a,map_prop (u,b),map_prop (u,c))
      | Ocl.Unary_exp1 (a,b) -> Ocl.Unary_exp1 (a,map_prop (u,b))
      | Ocl.Prim_exp a -> map_prop_pnlist (u,a)
      | Ocl.Postf_exp4 (a,b) -> Ocl.Postf_exp4 (map_prop (u,a),map_prop (u,b))
      | Ocl.If_exp (a,b,c) -> Ocl.If_exp (map_prop (u,a),map_prop (u,b),map_prop (u,c))
      | Ocl.Intersection a -> Ocl.Intersection (map_prop (u,a))
      | Ocl.Union a -> Ocl.Union (map_prop (u,a))
      | Ocl.Difference a -> Ocl.Difference (map_prop (u,a))
      | Ocl.Symmdiff a -> Ocl.Symmdiff (map_prop (u,a))
      | Ocl.Including a -> Ocl.Including (map_prop (u,a))
      | Ocl.Excluding a -> Ocl.Excluding (map_prop (u,a))
      | Ocl.Includes a -> Ocl.Includes (map_prop (u,a))
      | Ocl.Includes_all a -> Ocl.Includes_all (map_prop (u,a))
      | Ocl.At a -> Ocl.At (map_prop (u,a))
      | Ocl.Append a -> Ocl.Append (map_prop (u,a))
      | Ocl.Prepend a -> Ocl.Prepend (map_prop (u,a))
      | Ocl.Ocliskindof a -> Ocl.Ocliskindof (map_prop (u,a))
      | Ocl.Oclistypeof a -> Ocl.Oclistypeof (map_prop (u,a))
      | Ocl.Oclastype a -> Ocl.Oclastype (map_prop (u,a))
      | Ocl.Prop_call_par2 a -> Ocl.Prop_call_par2 (map_prop (u,a))
      | Ocl.Prop_call_par3 (a,b) -> Ocl.Prop_call_par3 (map_prop (u,a),b)
      | Ocl.Prop_call_par4 (a,b) -> Ocl.Prop_call_par4 (map_prop (u,a),map_prop (u,b))
      | Ocl.Qualif a -> Ocl.Qualif (map_prop (u,a))
      | Ocl.Act_p_list1 (a,b) -> Ocl.Act_p_list1 (map_prop (u,a),map_prop (u,b))
      | _ -> v



(*** Traduction des attributs et des associations ***)

(** teste si un nom est li� � une classe du mod�le UML
@param a le mod�le UML (diagramme de classes)
@param b un nom d'attribut ou d'association (cha�ne de caract�res)
@param c un nom de classe *)
and is_property_of ((Uml.Model(name,x,y) as a),b,c) =
  let Uml.Class(d,e,ee,f,g,h,i,j) = Uml.get_class(a,c) in
    match d with
	"" -> false
      | _ -> Uml.attr_name(Uml.find_attrib1(b,g))<>""
             or Uml.find_assoc(b,y)<>("",Uml.Multiplicity(Uml.Range("","")))

(** choisit la proc�dure de conversion � effectuer sur un concept UML,
suivant qu'il s'agit d'un attribut ou d'une association
@param a le mod�le UML
@param b un nom d'attribut ou d'association
@param c un nom de classe *)
and get_type ((Uml.Model(name,x,y) as a),b,c) =
  let d,e = (* nom et attributs de la classe *)
    match Uml.get_class(a,c) with
	Uml.Class(f,_,_,_,g,_,_,_) -> f,g
(*MG      | _-> failwith "get_type()" *)
  in
    match d with
	"" -> []
      | _ ->
	  let att,ass=Uml.find_attrib1(b,e),Uml.get_assoc(a,b) 
	  in
	    begin match Uml.attr_name(att) with
		"" -> 
		  begin match Uml.assoc_name(ass) with
		      "" -> []
		    | _ -> [assoc ass] (* conversion d'une association *)
		  end
	      | _ -> [attrib (att,d)]  (* conversion d'un attribut *)
	    end

(** traduction des associations UML en relations (ou fonctions) B
@return une relation, une fonction partielle ou une fonction totale,
        selon les multiplicit�s des r�les de l'association *)
and assoc (Uml.Association(a,b)) =
  let c_a,c_b,m_a,m_b =
    match b with
	((Uml.Role(_,_,c,Uml.Role_type d))::(Uml.Role(_,_,e,Uml.Role_type f))::q) -> d,f,c,e
      | _ -> "undef", "undef",Uml.Multiplicity(Uml.Range("","")),Uml.Multiplicity(Uml.Range("",""))
  in
  let u,x,y = 
    B.Type_base(B.Var2 a),
    B.Type_base(B.Var2(class_var(Ocl.Type_lit c_a))),
    B.Type_base(B.Var2(class_var(Ocl.Type_lit c_b)))
  in
    match m_a,m_b with
	Uml.Multiplicity(Uml.Range("0","1")),Uml.Multiplicity(Uml.Range("0","1")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.PartialInj,y))
      | Uml.Multiplicity(Uml.Range("0","1")),Uml.Multiplicity(Uml.Range("1","")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.PartialBij,y))
      | Uml.Multiplicity(Uml.Range("0","1")),Uml.Multiplicity(Uml.Range("0","*")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.PartialFunc,y))
      | Uml.Multiplicity(Uml.Range("0","1")),Uml.Multiplicity(Uml.Range("1","*")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.PartialSurj,y))

      |	Uml.Multiplicity(Uml.Range("1","")),Uml.Multiplicity(Uml.Range("0","1")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.TotalInj,y))
      | Uml.Multiplicity(Uml.Range("1","")),Uml.Multiplicity(Uml.Range("1","")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.TotalBij,y))
      | Uml.Multiplicity(Uml.Range("1","")),Uml.Multiplicity(Uml.Range("0","*")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.TotalFunc,y))
      | Uml.Multiplicity(Uml.Range("1","")),Uml.Multiplicity(Uml.Range("1","*")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.TotalSurj,y))

      |	Uml.Multiplicity(Uml.Range("0","*")),Uml.Multiplicity(Uml.Range("0","1")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.PartialFunc,y))
      | Uml.Multiplicity(Uml.Range("0","*")),Uml.Multiplicity(Uml.Range("1","")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.TotalFunc,y))
      | Uml.Multiplicity(Uml.Range("0","*")),Uml.Multiplicity(Uml.Range("0","*")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.Relation,y))
      | Uml.Multiplicity(Uml.Range("0","*")),Uml.Multiplicity(Uml.Range("1","*")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.Relation,y))

      |	Uml.Multiplicity(Uml.Range("1","*")),Uml.Multiplicity(Uml.Range("0","1")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.PartialSurj,y))
      | Uml.Multiplicity(Uml.Range("1","*")),Uml.Multiplicity(Uml.Range("1","")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.TotalSurj,y))
      | Uml.Multiplicity(Uml.Range("1","*")),Uml.Multiplicity(Uml.Range("0","*")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.Relation,y))
      | Uml.Multiplicity(Uml.Range("1","*")),Uml.Multiplicity(Uml.Range("1","*")) ->
	  B.Exp_rel(u,B.In,B.Rel(x,B.Relation,y))

      |	_ -> B.Exp_rel(u,B.In,B.Rel(x,B.Relation,y))

(** traduction des attributs de classes en relations (ou fonctions) B
@param v un attribut
@param w un nom de classe
@return une relation, une fonction partielle ou une fonction totale,
        suivant la multiplicit� de l'attribut *)
and attrib (v,w) =
  let attribute_type t =
    match t with
	Uml.Striing -> "String"
      | Uml.Char -> "Char"
      | Uml.Integer -> "Integer"
      | Uml.Real -> "Real"           (* ??? (rajout�) ??? *)
      | Uml.Boolean -> "Boolean"
      | Uml.Undefined -> "Undefined"
      | Uml.Att_type x -> x
    
  in let a_n,a_m,a_t,a_i =
    match v with
	Uml.Attribute(a,b,c,d) -> a,b,(attribute_type c),d
(*MG      | _ -> "undef",Uml.Multiplicity(Uml.Range("","")), "Undef", "" *)
  and a_c =
    match w with
	x -> x
  in
  let u,x,y = 
    B.Type_base(B.Var2 a_n),
    B.Type_base(B.Var2(class_var(Ocl.Type_lit a_c))),
    B.Type_base(B.Var2(class_var(Ocl.Type_lit a_t)))
  in
    match a_m with
	Uml.Multiplicity(Uml.Range("*","")) -> B.Exp_rel(u,B.In,B.Rel(x,B.Relation,y))
      | Uml.Multiplicity(Uml.Range("0","*")) -> B.Exp_rel(u,B.In,B.Rel(x,B.Relation,y))
      | Uml.Multiplicity(Uml.Range("0","1")) -> B.Exp_rel(u,B.In,B.Rel(x,B.PartialFunc,y))
      | Uml.Multiplicity(Uml.Range("1","")) -> B.Exp_rel(u,B.In,B.Rel(x,B.TotalFunc,y))
      | Uml.Multiplicity(Uml.Range("1","*")) -> B.Exp_rel(u,B.In,B.Rel(x,B.Relation,y))
      | _ -> B.Exp_rel(u,B.In,B.Rel(x,B.Relation,y))	



(*** Traduction des expressions de requ�tes (OCL2B) ***)

(** conversion de l'expression de requ�te Select
(s�lection du sous-ensemble d'�l�ments v�rifiant un pr�dicat)
@return d�finition d'un ensemble en compr�hension,
        dans lequel le pr�dicat B est la conjonction
        de pr�dicats de typage et du pr�dicat de filtrage *)
and select (u,v) =
  match u with
      Ocl.Prop_call_par2 a ->
	select (Ocl.Prop_call_par4(Ocl.Prim_exp(Ocl.Name_lit (obj_n())),a),v)
    | Ocl.Prop_call_par4 (a,b) ->
	begin match a with
	    Ocl.Decl1 (c,d) ->
	      B.Def_set ( get_vars a,
			  B.Exp_rel (
			    B.Exp_rel(decl2 a,B.And,map_decl_prop(a,v)),
			    B.And,
			    ocl_b(map_prop(a,b)) ) )
	  | Ocl.Prim_exp (c) ->
	      B.Def_set ( get_vars a,
			  B.Exp_rel (
			    map_decl_prop (a,v),
			    B.And,
			    ocl_b(map_prop(a,b)) ) )
	  | _ -> failwith "select()"
	end
    | _ -> failwith "select()"

(** conversion de l'expression de requ�te Reject
(s�lection du sous-ensemble d'�l�ments ne v�rifiant pas un pr�dicat)
@return d�finition d'un ensemble en compr�hension,
        dans lequel le pr�dicat B est la conjonction
        de pr�dicats de typage et de la n�gation du pr�dicat de filtrage *)
and reject (u,v) =
  match u with
      Ocl.Prop_call_par2 a ->
	reject (Ocl.Prop_call_par4(Ocl.Prim_exp(Ocl.Name_lit (obj_n())),a),v)
    | Ocl.Prop_call_par4 (a,b) ->
	begin match a with
	    Ocl.Decl1 (c,d) ->
	      B.Def_set ( get_vars a,
			  B.Exp_rel (
			    B.Exp_rel(decl2 a,B.And,map_decl_prop(a,v)),
			    B.And,
			    B.Op_un(B.Not,ocl_b(map_prop(a,b))) ) )
	  | Ocl.Prim_exp (c) ->
	      B.Def_set ( get_vars a,
			  B.Exp_rel (
			    map_decl_prop(a,v),
			    B.And,
			    B.Op_un(B.Not,ocl_b(map_prop(a,b))) ) )
	  | _ -> failwith "reject()"
	end
    | _ -> failwith "reject()"

(** conversion de l'expression de requ�te Collect 
(r�sultat de l'application d'une expression � tous les �l�ments de la collection source)
@return d�finition d'un ensemble en compr�hension d�crit � l'aide d'une union g�n�ralis�e :
        une expression OCL: source->collect(obj:Type | expression(obj)) sera traduite en B
        par: union(obj).(obj:T\[source\] & T\[expression(obj)\]:Type | \{T\[expression(obj)\]\}),
        T �tant le morphisme de transformation d'une expression OCL en une expression B *)
and collect1 (u,v) =
  match u with
      Ocl.Prop_call_par2 a ->
	collect1 (Ocl.Prop_call_par4(Ocl.Prim_exp(Ocl.Name_lit (obj_n())),a),v)
    | Ocl.Prop_call_par4 (a,b) ->
	begin match a with
	    Ocl.Decl1 (c,d) ->
	      B.Ran ( B.Def_set ( (get_vars a)@[B.Var "res"],
				  B.Exp_rel (
				    decl2 a,
				    B.And,
				    B.Exp_rel (
				      map_decl_prop(a,v),
				      B.And,
				      B.Exp_rel (
					B.Type_base(B.Var2 "res"),
					B.In,
					ocl_b(map_prop(a,b)) ) ) ) ) )
	  | Ocl.Prim_exp (c) ->
	      B.Ran ( B.Def_set ( (get_vars a)@[B.Var "res"],
				  B.Exp_rel (
				    map_decl_prop(a,v),
				    B.And,
				    B.Exp_rel (
				      B.Type_base(B.Var2 "res"),
				      B.In,
				      ocl_b(map_prop(a,b)) ) ) ) )
	  | _ -> failwith "collect()"
	end
    | _ -> failwith "collect()"

(** ??? conversion de l'expression de requ�te Collect (-> diff�rence avec collect1 ?) ??? 
(r�sultat de l'application d'une expression � tous les �l�ments de la collection source)
@return d�finition d'un ensemble en compr�hension d�crit � l'aide d'une union g�n�ralis�e :
        une expression OCL: source->collect(obj:Type | expression(obj)) sera traduite en B
        par: union(obj).(obj:T\[source\] & T\[expression(obj)\]:Type | \{T\[expression(obj)\]\}),
        T �tant le morphisme de transformation d'une expression OCL en une expression B *)
and collect2 (u,v) =
  match u with
      Ocl.Prop_call_par2 a ->
	collect2 (Ocl.Prop_call_par4(Ocl.Prim_exp(Ocl.Name_lit (obj_n())),a),v)
    | Ocl.Prop_call_par4 (a,b) ->
	begin match a with
	    Ocl.Decl1 (c,d) ->
	      B.Union_q ( get_vars a,
			  B.Exp_rel (
			    decl2 a,
			    B.And,
			    map_decl_prop(a,v) ),
			  B.Ens(ocl_b(map_prop(a,b))) )
	  | Ocl.Prim_exp (c) ->
	      B.Union_q ( get_vars a,
			  map_decl_prop(a,v),
			  B.Ens(ocl_b(map_prop(a,b))) )
	  | _ -> failwith "collect()"
	end
    | _ -> failwith "collect()"

(** conversion de l'expression de requ�te Iterate
(it�ration d'un calcul (avec accumulateur) sur tous les �l�ments de la collection)
@return Classe.allInstances()->iterate(cl:Classe; sum:Integer=0 | sum+cl.attribut)
        sera traduite en B par l'expression 0 + SIGMA(cl).(cl:classe | attribut(cl)) *)
and iterate (u,v) =
  let acc = function
      (Ocl.Prim_exp(Ocl.Number_lit x)) -> x
    | _ -> failwith "iterate.acc()"
  and fct_new_b h e i =
    if h = Ocl.Prim_exp e
    then i
    else h
  in
  match u with
    | Ocl.Prop_call_par4 (a,b) ->
	begin match a with
	    Ocl.Decl4 (c,d,e,f,g) ->
	      begin match b with
		  Ocl.Add_exp2(Ocl.Plus,h,i)
		| Ocl.Add_exp2(Ocl.Minus,h,i) -> 
		    let new_b = fct_new_b h e i in
		    let b_exp = 
		      match b 
		      with 
			  Ocl.Add_exp2(Ocl.Minus,_,_) -> Ocl.Unary_exp1(Ocl.Minus_op,new_b) 
			| _ -> new_b in 
		    let sum_exp =
		      B.Sum_g( get_vars a,
			       B.Exp_rel(decl2 a,
					 B.And,map_decl_prop(a,v)),
			       ocl_b(map_prop(a,b_exp))) in
		      if acc g<>"0" 
		      then B.Op_bin(ocl_b g,B.Plus,sum_exp) 
		      else sum_exp
		| Ocl.Mult_exp2(Ocl.Multiply,h,i)
		| Ocl.Mult_exp2(Ocl.Divide,h,i) -> 
		    let new_b = fct_new_b h e i in
		    let b_exp = match b with 
			Ocl.Mult_exp2(Ocl.Divide,_,_) ->
			  Ocl.Mult_exp2(Ocl.Divide,Ocl.Prim_exp(Ocl.Number_lit "1"),new_b)
		      | _ -> new_b in 
		    let mult_exp =
		      B.Prod_g( get_vars a,
				B.Exp_rel(decl2 a,
					  B.And,map_decl_prop(a,v)),
				ocl_b(map_prop(a,b_exp))) in
		      if acc g<>"1" 
		      then B.Op_bin(ocl_b g,B.Mul,mult_exp) 
		      else mult_exp
		| _ -> failwith "iterate()"
	      end
	  | Ocl.Decl3 (c,e,f,g) -> 
	      begin match b with
		  Ocl.Add_exp2(Ocl.Plus,h,i)
		| Ocl.Add_exp2(Ocl.Minus,h,i) -> 
		    let new_b = fct_new_b h e i in
		    let b_exp = match b with 
			Ocl.Add_exp2(Ocl.Minus,_,_) -> Ocl.Unary_exp1(Ocl.Minus_op,new_b)
		      | _ ->  new_b
		    in 
		    let sum_exp = B.Sum_g ( get_vars a,
					    map_decl_prop(a,v),
					    ocl_b(map_prop(a,b_exp)) ) in
		      if acc g<>"0"
		      then B.Op_bin(ocl_b g,B.Plus,sum_exp)
		      else sum_exp
		| Ocl.Mult_exp2(Ocl.Multiply,h,i)
		| Ocl.Mult_exp2(Ocl.Divide,h,i) -> 
		    let new_b = fct_new_b h e i in
		    let b_exp = match b with 
			Ocl.Mult_exp2(Ocl.Divide,_,_) -> 
			  Ocl.Mult_exp2(Ocl.Divide,Ocl.Prim_exp(Ocl.Number_lit "1"),new_b)
		      | _ -> new_b
		    in
		    let mult_exp = B.Prod_g ( get_vars a,
					      map_decl_prop(a,v),
					      ocl_b(map_prop(a,b_exp)) ) in
		      if acc g<>"1" 
		      then B.Op_bin(ocl_b g,B.Mul,mult_exp) 
		      else mult_exp
		| _ -> failwith "iterate()"
	      end
	  | _ -> failwith "iterate()"
	end
    | _ -> failwith "iterate()" 

(** conversion de l'expression de requ�te ForAll
(expression vraie si tous les �l�ments de la collection v�rifient un pr�dicat)
@return la quantification universelle s'exprime en B
        par une expression de la forme !(x).(P_typage(x)=>P_filtrage(x)),
        o� x est une liste d'identificateurs, P_typage et P_filtrage �tant
        des pr�dicats qui contraignent les variables de x *)
and forall (u,v) =
  match u with
      Ocl.Prop_call_par2 a ->
	forall(Ocl.Prop_call_par4(Ocl.Prim_exp(Ocl.Name_lit (obj_n())),a),v)
    | Ocl.Prop_call_par4 (a,b) ->
	begin match a with
	    Ocl.Decl1 (c,d) ->
	      B.Forall ( get_vars a,
			 B.Exp_rel (
			   decl2 a,
			   B.And,
			   map_decl_prop(a,v) ),
			 ocl_b(map_prop(a,b)) )
	  | Ocl.Prim_exp (c) ->
	      B.Forall ( get_vars a, map_decl_prop(a,v), ocl_b(map_prop(a,b)) )
	  | _ -> failwith "forall()"
	end
    | _ -> failwith "forall()"

(** conversion de l'expression de requ�te Exists
(expression vraie si au moins un �l�ment de la collection v�rifie le pr�dicat)
@return la quantification existentielle s'exprime en B
        par une expression de la forme #(x).(P_typage(x)=>P_filtrage(x)),
        o� x est une liste d'identificateurs, P_typage et P_filtrage �tant
        des pr�dicats qui contraignent les variables de x *)
and exists (u,v) =
  match u with
      Ocl.Prop_call_par2 a ->
	exists(Ocl.Prop_call_par4(Ocl.Prim_exp(Ocl.Name_lit (obj_n())),a),v)
    | Ocl.Prop_call_par4 (a,b) ->
	begin match a with
	    Ocl.Decl1 (c,d) ->
	      B.Exists ( get_vars a,
			 B.Exp_rel (
			   decl2 a,
			   B.And,
			   map_decl_prop(a,v) ),
			 ocl_b(map_prop(a,b)) )
	  | Ocl.Prim_exp (c) ->
	      B.Exists ( get_vars a, map_decl_prop(a,v), ocl_b(map_prop(a,b)) )
	  | _ -> failwith "exists()"
	end
    | _ -> failwith "exists()"

