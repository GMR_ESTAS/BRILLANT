(*** $Id$ ***)

(** {5 R�gles de transformation d'UML/OCL vers B }
@author 
@version 
*)



(** liste des classes du mod�le *)
val model_classes : Uml.uml_class list ref

(** liste des associations du mod�le *)
val model_assocs : Uml.association list ref

(** classes + associations *)
val model_sys : Uml.model ref


(** {3 Arbre de syntaxe abstraite B (UML2B) } *)

(** teste si une expression OCL d�signe un attribut
faisant partie d'une classe du mod�le UML *)
val is_attribute : Ocl.expr -> bool

(** teste si une expression OCL d�signe un attribut
(faisant partie d'une classe du mod�le UML)
dont la multiplicit� est 1 ou 0..1 *)
val mult_un_zero : Ocl.expr -> bool

(** teste si une expression OCL fait r�f�rence � un r�le
(faisant partie d'une association du mod�le UML)
dont la multiplicit� est 1 ou 0..1 *)
val is_unary : Ocl.expr -> bool

(** ??? *)
val find_assoc_end : Ocl.expr -> B.expr_right

(** convertit une expression OCL en une expression B *)
val ocl_b : Ocl.expr -> B.expr_right

(** traduction des constantes bool�ennes *)
val const : Ocl.constant -> string

(** ??? *)
val nlit : Ocl.path_name_list -> B.expr_right

(** ??? *)
val decl : Ocl.expr -> B.predicate

(** ??? *)
val decl2 : Ocl.expr -> B.expr_right

(** ??? *)
val map_decl_prop :
  Ocl.expr * Ocl.expr -> B.expr_right

(** retourne l'identificateur d'un type OCL *)
val get_id : Ocl.path_name_list -> string

(** conversion des types simples et des types collection *)
val typespec : Ocl.type_spec -> B.uml_type

(** fonction qui cr�� un nouvel identificateur
@param un identificateur (cha�ne de caract�res)
@return un nouvel identificateur, compos� de 2 lettres minuscules :
        - on double la lettre si l'identificateur de d�part ne comporte qu'une seule lettre
        - sinon, on retourne les deux premi�res lettres de l'identificateur de d�part  *)
val new_ident : Ocl.path_name_list -> string

(** correspondance entre un type OCL et un nom de type B *)
val class_var : Ocl.path_name_list -> string

(** extrait d'une expression OCL une liste d'identificateurs B *)
val get_vars : Ocl.expr -> B.ident list

(** convertit une contrainte OCL en une machine B, en remplissant :
- soit la clause INVARIANT, si la contrainte est un invariant
- soit la clause OPERATIONS, si la contrainte est une pr� ou post-condition *)
val constraint_conv : Ocl.expr -> B.amn


(** {3 Fonctions auxiliaires (UML2B) } *)

(** compteur d'objets d'une collection *)
val obj : int ref

(** ajoute un objet � la collection *)
val get_obj : unit -> unit

(** vide une collection d'objets *)
val init_obj : unit -> unit

(** �crit le num�ro du dernier objet ajout� *)
val obj_n : unit -> string


(** contexte d'une contrainte OCL *)
val context : string ref

(** retourne le contexte de la contrainte OCL *)
val get_context : unit -> string

(** assigne le contexte de la contrainte OCL *)
val set_context : string -> unit

(** ??? *)
val context_from_type : Ocl.path_name_list -> string

(** remplace les occurrences du mot-cl� 'self' dans une expression OCL,
en fonction du contexte de la contrainte *)
val map_self_context :
  Ocl.path_name_list * Ocl.expr -> Ocl.expr

(** effectue la conjonction logique d'une liste d'expressions *)
val and_list : B.expr_right list -> B.expr_right

(** ??? *)
val get_subst_pred : Ocl.expr -> B.expr_right

(** �limine les doublons d'une liste *)
val elim_doubles : B.ident list -> B.ident list

(** ??? *)
val get_type_vars : B.ident list -> B.expr_right

(** ??? *)
val get_subst_vars : B.expr_right -> B.ident list

(** ??? *)
val get_res : Ocl.expr -> B.expr_right

(** ??? teste si une expression OCL est un "r�sultat" ??? *)
val is_res : Ocl.expr -> bool

(** valeur d'une liste d'identificateurs
@return liste de cha�nes de caract�res correspondant aux noms de ces identificateurs *)
val devars : B.ident list -> string list

(** teste si un objet appartient � une liste *)
val is_elem_of : string list * string -> bool

(** ??? *)
val map_prop : Ocl.expr * Ocl.expr -> Ocl.expr


(** {3 Traduction des attributs et des associations } *)

(** teste si un nom est li� � une classe du mod�le UML
@param a le mod�le UML (diagramme de classes)
@param b un nom d'attribut ou d'association (cha�ne de caract�res)
@param c un nom de classe *)
val is_property_of : Uml.model * Uml.name * Uml.name -> bool

(** choisit la proc�dure de conversion � effectuer sur un concept UML,
suivant qu'il s'agit d'un attribut ou d'une association
@param a le mod�le UML
@param b un nom d'attribut ou d'association
@param c un nom de classe *)
val get_type :
  Uml.model * Uml.name * Uml.name -> B.expr_right list

(** traduction des associations UML en relations (ou fonctions) B
@return une relation, une fonction partielle ou une fonction totale,
        selon les multiplicit�s des r�les de l'association *)
val assoc : Uml.association -> B.expr_right

(** traduction des attributs de classes en relations (ou fonctions) B
@param v un attribut
@param w un nom de classe
@return une relation, une fonction partielle ou une fonction totale,
        suivant la multiplicit� de l'attribut *)
val attrib : Uml.attribute * Uml.name -> B.expr_right


(** {3 Traduction des expressions de requ�tes (OCL2B) } *)

(** conversion de l'expression de requ�te Select
(s�lection du sous-ensemble d'�l�ments v�rifiant un pr�dicat)
@return d�finition d'un ensemble en compr�hension,
        dans lequel le pr�dicat B est la conjonction
        de pr�dicats de typage et du pr�dicat de filtrage *)
val select : Ocl.expr * Ocl.expr -> B.expr_right

(** conversion de l'expression de requ�te Reject
(s�lection du sous-ensemble d'�l�ments ne v�rifiant pas un pr�dicat)
@return d�finition d'un ensemble en compr�hension,
        dans lequel le pr�dicat B est la conjonction
        de pr�dicats de typage et de la n�gation du pr�dicat de filtrage *)
val reject : Ocl.expr * Ocl.expr -> B.expr_right

(** conversion de l'expression de requ�te Collect 
(r�sultat de l'application d'une expression � tous les �l�ments de la collection source)
@return d�finition d'un ensemble en compr�hension d�crit � l'aide d'une union g�n�ralis�e :
        une expression OCL: {e source->collect(obj:Type | expression(obj)) } sera traduite en B
        par: {e union(obj).(obj:T\[source\] & T\[expression(obj)\]:Type | \{T\[expression(obj)\]\}) },
        T �tant le morphisme de transformation d'une expression OCL en une expression B *)
val collect1 : Ocl.expr * Ocl.expr -> B.expr_right

(** ??? conversion de l'expression de requ�te Collect (-> diff�rence avec collect1 ?) ??? 
(r�sultat de l'application d'une expression � tous les �l�ments de la collection source)
@return d�finition d'un ensemble en compr�hension d�crit � l'aide d'une union g�n�ralis�e :
        une expression OCL: {e source->collect(obj:Type | expression(obj)) } sera traduite en B
        par: {e union(obj).(obj:T\[source\] & T\[expression(obj)\]:Type | \{T\[expression(obj)\]\}) },
        T �tant le morphisme de transformation d'une expression OCL en une expression B *)
val collect2 : Ocl.expr * Ocl.expr -> B.expr_right

(** conversion de l'expression de requ�te Iterate
(it�ration d'un calcul (avec accumulateur) sur tous les �l�ments de la collection)
@return {e Classe.allInstances()->iterate(cl:Classe; sum:Integer=0 | sum+cl.attribut) }
        sera traduite en B par l'expression {e 0 + SIGMA(cl).(cl:classe | attribut(cl)) } *)
val iterate : Ocl.expr * Ocl.expr -> B.expr_right

(** conversion de l'expression de requ�te ForAll
(expression vraie si tous les �l�ments de la collection v�rifient un pr�dicat)
@return la quantification universelle s'exprime en B
        par une expression de la forme {e !(x).(P_typage(x)=>P_filtrage(x)) },
        o� x est une liste d'identificateurs, P_typage et P_filtrage �tant
        des pr�dicats qui contraignent les variables de x *)
val forall : Ocl.expr * Ocl.expr -> B.expr_right

(** conversion de l'expression de requ�te Exists
(expression vraie si au moins un �l�ment de la collection v�rifie le pr�dicat)
@return la quantification existentielle s'exprime en B
        par une expression de la forme {e #(x).(P_typage(x)=>P_filtrage(x)) },
        o� x est une liste d'identificateurs, P_typage et P_filtrage �tant
        des pr�dicats qui contraignent les variables de x *)
val exists : Ocl.expr * Ocl.expr -> B.expr_right

