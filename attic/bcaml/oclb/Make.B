#!/usr/bin/make -f

# $Id$



# mod�le � convertir
MODELDIR=./examples/PN
MODELNAME=PN
ZUML=$(MODELDIR)/$(MODELNAME).zuml
XMI=$(MODELDIR)/$(MODELNAME).xmi
BDIR=$(MODELDIR)/B


UML2BDIR=./uml2b


# XMI -> format ioXML
ioXML=$(XMI).ioXML
XMI2UML=$(UML2BDIR)/xmi2uml.xsl

# XSLTProc
XSLTPROC=xsltproc
XSLTOPTIONS=-o $(ioXML) $(XMI2UML) $(XMI)
# Xalan
#XSLTPROC=java org.apache.xalan.xslt.Process
#XSLTOPTIONS=-in $(XMI) -xsl $(XMI2UML) -out $(ioXML)


# production des machines B
UML2B=$(UML2BDIR)/uml2b
UML2BOPTIONS=-d $(BDIR) $(ioXML)



all:
	# XMI -> format ioXML
	#unzip $(ZUML) -d $(MODELDIR)  #cr�ation du fichier XMI
	$(XSLTPROC) $(XSLTOPTIONS)

	# cr�ation du r�pertoire de stockage des machines B
	-mkdir $(BDIR)

	# production des machines B
	$(UML2B) $(UML2BOPTIONS)

