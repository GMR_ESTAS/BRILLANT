(* DEFINITION DES TYPES *)

/*** Types li�s au mod�le UML ***/

/** mod�le UML, au sens d'un diagramme de classes */
type model = (`Model,
	( name        	        /* nom du mod�le */
	, [ uml_class* ]     	/* classes du diagramme */
	, [ association* ] 	/* associations du diagramme */
	))

/** classe d'un diagramme UML */
type uml_class = (`Class, 
  ( name                   /* nom de la classe */
    , multiplicity         /* nb d'instances de la classe pouvant �tre cr��es */
    , [association* ]     /* si la classe est une classe-association */
    , [inheritance* ]     /* super-classes dont cette classe h�rite */
    , [attribute* ]       /* attributs de la classe */
    , [operation* ]       /* op�rations de la classe */
    , [ocl_constraint* ]  /* contraintes OCL li�es � cette classe */
    , [state_machine* ] ) /* diagrammes d'�tats-transitions de cette classe */
	)

/** association de classes */
type association = (`Association, 
  ( name         /* nom de l'association */
    , [ role* ]) /* d�finition des r�les de l'association (liste de 2 �l�ments) */
	)


/*** Types li�s au concept de classe ***/

/** une classe B h�rite d'une classe A ssi A est une g�n�ralisation de B */
type inheritance = (`Generalization,  parent)

/** indique la classe de laquelle on h�rite */
type parent = (`Parent, name)

/** attribut d'une classe */
type attribute = (`Attribute, 
  ( name              /* nom de l'attribut */
    , multiplicity    /* nb de valeurs que peut avoir l'attribut (en g�n�ral: 1) */
    , attribute_type  /* type de l'attribut */
    , initial_value ) /* valeur de l'attribut � sa cr�ation */
	)

/** type d'un attribut */
type attribute_type =
    `Str
  | `Char
  | `Integer
  | `Real
  | `Boolean
  | `Undefined          /* type non d�fini */
  | (`Att_type, Latin1) /* type d�fini par l'utilisateur */

/** valeur initiale d'un attribut */
type initial_value = Latin1

/** op�ration d'une classe */
type operation = (`Operation, 
  ( name               /* nom de l'op�ration */
    , [ parameter* ]   /* arguments de l'op�ration */
    , [ parameter* ] ) /* valeurs de retour */
	)

/** un param�tre est d�fini par son nom et son type */
type parameter = (`Parameter,  (name, attribute_type))

/** contrainte OCL */
type ocl_constraint = Latin1


/*** Types li�s au concept d'association ***/

/** caract�ristiques d'une extr�mit� d'association  */
type role = (`Role, 
  ( name           /* nom du r�le */
    , aggregation  /* type d'association de cette extr�mit� */
    , multiplicity /* multiplicit� de cette extr�mit� de l'association */
    , role_type )  /* classe associ�e � ce r�le */
	)

/** type d'association, caract�risant une extr�mit� d'association */
type aggregation =
    `Aggregate /* agr�gation: pour exprimer la notion d'appartenance */
  | `Composite /* composition: agr�gation plus forte (un composant fait
			      partie d'un unique compos�, et le composant est
			      cr�� et d�truit au m�me moment que le compos�) */
  | `Ass_end   /* association simple */

/** type d'un r�le (classe associ�e � ce r�le) */
type role_type = (`Role_type,  name)



/*** Types li�s au concept d'automate � �tats-transitions ***/

/** diagramme d'�tats-transitions d'une classe */
type state_machine = (`State_machine, 
  ( name                /* nom du diagramme d'�tats-transitions */
    , [ state* ]        /* ensemble des �tats de l'automate */
    , [ transition* ] ) /* transitions */
	)

/** caract�risation d'un �tat de l'automate */
type state =
    (`Simplestate,  name) /* �tat ordinaire (atomique) */
  | (`Pseudostate,  name) /* �tat initial */
  | (`Finalstate,  name ) /* �tat final */

/** transition entre 2 �tats de l'automate */
type transition = (`Transition, 
  ( name       /* nom de la transition */
    , source   /* �tat de d�part */
    , target   /* �tat d'arriv�e */
    , event    /* stimulus qui d�clenche la transition */
    , guard    /* condition de d�clenchement (contraint l'ex�cution de la transition) */
    , action ) /* op�ration qui s'ex�cute au moment de la transition */
	)

/** �tat de d�part d'une transition */
type source = ( `Source,  state)

/** �tat d'arriv�e d'une transition */
type target = ( `Target,  state)

/** �v�nement d'une transition (stimulus) */
type event = Latin1

/** condition de d�clenchement d'une transition */
type guard = Latin1

/** op�ration accompagnant le d�clenchement d'une transition */
type action = Latin1



/*** Types communs � tous les concepts ***/

type name = Latin1

/** on d�finit la multiplicit� comme un intervalle de valeurs possibles,
avec la convention: 0..* pour d�signer n'importe qu'elle valeur */
type multiplicity = ( `Multiplicity,  range)

/** un intervalle est compos� d'une borne inf�rieure et d'un borne sup�rieure */
type range = ( `Range,  (lower, upper))
type lower = Latin1
type upper = Latin1



(* DEFINITION DES FONCTIONS *)

/** fonction destin�e � s'assurer qu'un identificateur a au moins 2 caract�res
@param a une cha�ne de caract�res
@return aa si a fait 1 caract�re, a sinon  */
let rename( str : Latin1) : Latin1 =
  match str with
    | [] -> []
    | [ a ] -> [ a a ]
    | _ -> str

/*** Fonctions qui renvoient le nom d'un objet ***/

/** renvoie le nom de la classe
@return le nom de la classe */
let class_name( uml_class -> name ) 
    | (`Class, (a, _, _, _, _, _, _, _)) -> a
				
/** renvoie le nom de l'association
@return le nom de l'association */
let assoc_name( association -> name )
    | (`Association, (a,_)) -> a
				
/** renvoie le nom de l'attribut
@return le nom de l'attribut */
let attr_name ( attribute -> name )
    | (`Attribute, (a, _ , _ , _)) -> a


/*** Fonctions qui cherchent un objet � partir de son nom ***/

/** cherche une classe dans un mod�le UML
@param Model(a,b,c) un diagramme de classes
@param d un nom de classe 
@return la classe de nom d, si elle fait partie du diagramme de classes,
        ou une 'classe vide', sinon */
let get_class ( (model, Latin1) -> uml_class )
    | ((`Model, (a,b,c)) , n) -> match (b, n) with
	                            | ([ e f::_*], name) -> if (class_name(e) = n)
	                                            then e
	                                            else get_class ((`Model, (a,f,c)), n)
				    | _ -> (`Class, ("", (`Multiplicity, ( `Range, ("0","*"))), [], [], [], [], [], []))


/** cherche une association dans un mod�le UML
@param Model(a,b,c) un diagramme de classes
@param d un nom
@return l'association r�f�renc�e par d (d d�signant le nom, un r�le
        ou une classe de l'association), si elle fait partie du mod�le,
        ou une 'association vide', sinon */
let get_assoc ( (model, Latin1) -> association )
    | ((`Model, (a,b,c)) , n) -> match c with
	| [ e & (`Association, (g, [ (`Role, (h1, _, _, (`Role_type, k1))) (`Role, (h2, _, _, (`Role_type, k2))) _*] )) f::_* ] ->
	  if (rename(g)=rename(n)) or /* n est le nom de l'association */
	    (rename(h1)=rename(n)) or /* n est le nom du r�le n�1 */
	    (rename(h2)=rename(n)) or /* n est le nom du r�le n�2 */
	    (rename(k1)=rename(n)) or /* n est le nom de la classe n�1 */
            (rename(k2)=rename(n))    /* n est le nom de la classe n�2 */
	  then e
	  else get_assoc ((`Model, (a,b,f)), n) 
	| [] -> (`Association, ("", []))
	| _ -> raise "get_assoc()"


/** cherche les caract�ristiques d'une association dans une liste d'associations
@param a un nom
@param b une liste d'associations
@return si une association r�f�renc�e par le nom a appartient � la liste :
        la fonction renvoit un couple compos� de l'identificateur de l'association
        et de la multiplicit� associ�e � cet identificateur, avec les r�gles suivantes :
        - si a est un nom de r�le, on garde ce nom comme identificateur
        - si a est un nom de classe, on prend comme identificateur 
          -- soit le nom de l'association, pour aller dans le sens r�le n�1 vers r�le n�2
          -- soit le nom de l'association + tilde (fct r�ciproque en B), pour aller dans l'autre sens
        - sinon, la fonction renvoit un couple ('association vide','multiplicit� vide') */
let find_assoc ((Latin1, [ association* ]) -> (name, multiplicity) )
	| (a,b) -> match b with
	    | [ (`Association, (g, [ (`Role, (h1, _, j1, (`Role_type, k1))) (`Role, (h2, _, j2, (`Role_type, k2))) _* ] )) i::_* ] ->
		if a=h1
		then print(h1);
		  (h1,j1)
		else                       /* a est le nom du r�le n�1 */
		  if a=h2
		  then print(h2);
		    (h2,j2)
		  else                     /* a est le nom du r�le n�2 */
		    if a=k1
		    then print(g);
		      (g,j1)
		    else                   /* a est le nom de la classe n�1 */
		      if a=k2
		      then print(g);
			(g@"~",j2)
		      else find_assoc(a,i) /* a est le nom de la classe n�2 */
	    | [] -> ("", (`Multiplicity, (`Range, ("",""))))
	    | _ -> raise "find_assoc()"


/** cherche un attribut dans une liste d'attributs
@param a un nom d'attribut
@param b une liste d'attributs
@return l'attribut de nom a, s'il appartient � la liste b,
        ou un 'attribut vide', sinon */
let find_attrib1 ( (name, [attribute*]) -> attribute )
	    | (a,b) -> match b with
		| [ (`Attribute, (c,d,e,f)) g::_* ] ->
		    if rename(a)=rename(c)
		    then (`Attribute, (rename(c),d,e,f))
		    else find_attrib1(a,g)
		| [] -> (`Attribute, ("", (`Multiplicity, (`Range, ("",""))), (`Att_type, ""), ""))


/** cherche un attribut dans une liste de classes
@param a un nom d'attribut
@param b une liste de classes
@return l'attribut de nom a, s'il fait partie de l'une des classes de la liste b,
        ou un 'attribut vide', sinon */
let find_attrib2 ( (name, [uml_class*]) -> attribute )
		| (a,b) ->  match b with
		    | [ (`Class, (_,_,_,_,g,_,_,_)) j::_* ] -> 
			let x = find_attrib1(a,g) in          /* on cherche pour chaque classe de la liste */
				( match x with
				    | (`Attribute, ("",_,_,_)) -> find_attrib2(a,j) /* si l'attribut ne fait pas partie */
				    | _ -> x                                        /* d'une classe donn�e, alors on cherche dans la suivante */
				)
		    | [] -> (`Attribute, ("", (`Multiplicity, (`Range, ("",""))), (`Att_type, ""), ""))
		    

/*** Fonctions qui ajoutent un objet ***/

/** ajoute une classe � un mod�le UML */
let set_class ( (model, uml_class) -> model )
    | ((`Model, (a,b,c)) ,d) -> (`Model, (a,[d]@b,c))
			
/** ajoute une association � un mod�le UML */
let set_assoc ( (model, association) -> model )
    | ((`Model, (a,b,c)) ,d) -> (`Model, (a,b,[d]@c))
				   
/** ajoute un attribut � une classe */
let set_attrib ( (attribute, uml_class) -> uml_class )
    | (a, (`Class, (b,c,d,e,f,g,h,i))) -> (`Class, (b,c,d,e,f@[a],g,h,i))

/** ajoute une op�ration � une classe */
let set_oper ( (operation, uml_class) -> uml_class )
    | (a, (`Class, (b,c,d,e,f,g,h,i))) -> (`Class, (b,c,d,e,f,g@[a],h,i))


/** introduit/modifie une classe
@param a une liste de classes
@param b une classe
@return si la liste a poss�de une classe de m�me nom,
        ses propri�t�s sont remplac�es par celles de b,
        sinon, b est ajout�e � la liste a */
let replace_class ( ([uml_class*], uml_class) -> [ uml_class* ])
    | (a,b) -> match a with
	| [ d e::_* ] -> 
	    if class_name(d)=class_name(b) 
	    then [b]@e 
	    else [d]@replace_class(e,b)
	| [] -> [b]


/** introduit/modifie une association
@param a une liste d'associations
@param b une association
@return si la liste a poss�de une association de m�me nom,
        ses propri�t�s sont remplac�es par celles de b,
        sinon, b est ajout�e � la liste a */
let replace_assoc ( ([association*], association) -> [ association* ])
	| (a,b) -> match a with
	    | [ d e::_* ] -> 
		if assoc_name(d)=assoc_name(b) 
		then [b]@e 
		else [d]@replace_assoc(e,b)
	    | [] -> [b]


/*** Fonctions qui cr�ent un nouvel objet ***/

/** ajoute une 'classe vide' � un mod�le UML
@param a le nom de la nouvelle classe
@param b le mod�le UML */
let new_class ( (name, model) -> model) 
	| (a,b) -> set_class(b, (`Class, (a, (`Multiplicity, (`Range, ("0","*"))), [], [], [], [], [], [])))

/** renvoie un attribut
@param a nom de l'attribut � cr�er
@param b la cardinalit� min de l'attribut
@param c la cardinalit� max
@param d le type de l'attribut */
let new_attrib ( (name, Latin1, Latin1, Latin1 ) -> attribute)
  | (a,b,c,d) -> (`Attribute, (a, (`Multiplicity, (`Range, (b,c))), (`Att_type, d), ""))

