/** d�finition d'une expression OCL */
type expr =

/* expression logique */
    (`Log_exp1, expr)
  | (`Log_exp2, (logical_op, expr, expr))

/* expression relationnelle */
  | (`Rel_exp1, expr)
  | (`Rel_exp2, (relational_op, expr, expr))

/* expression additive */
  | (`Add_exp1, expr)
  | (`Add_exp2, (add_op, expr, expr))

/* expression multiplicative */
  | (`Mult_exp1, expr)
  | (`Mult_exp2, (multiply_op, expr, expr))

/* expression unaire */
  | (`Unary_exp1, (unary_op, expr))
  | (`Unary_exp2, expr)

/* ??? */
  | (`Postf_exp3, (expr, expr))
  | (`Postf_exp4, (expr, expr))
  | (`Postf_exp5, expr)
  | (`Postf_exp6, (expr, expr))

/* expression if then else */
  | (`If_exp, (expr, expr, expr))

/* ??? */
  | (`Lit_coll1, coll_kind)
  | (`Lit_coll2, (coll_kind, expr))

/* ??? */
  | (`Coll_item_list1, expr)
  | (`Coll_item_list2, (expr, expr))
  | (`Coll_item1, expr)
  | (`Coll_item2, (expr, expr))

/* ??? */
  | (`Prop_call1, path_name_list)
  | (`Prop_call2, path_name_list)
  | (`Prop_call3, (path_name_list, expr))
  | (`Prop_call4, (path_name_list, expr))
  | (`Prop_call5, (path_name_list, expr))
  | (`Prop_call6, (path_name_list, expr))
  | (`Prop_call7, (path_name_list, expr, expr))
  | (`Prop_call8, (path_name_list, expr, expr))

  | `Prop_call_par1                     /* ??? */
  | (`Prop_call_par2, expr)             /* ??? */
  | (`Prop_call_par3, (expr, type_spec)) /* ??? */
  | (`Prop_call_par4, (expr, expr))      /* ??? */

/* expressions de requ�te */
  | (`Select, expr)
      /* s�lection du sous-ensemble des �l�ments v�rifiant un pr�dicat */
  | (`Reject, expr)
      /* s�lection du sous-ensemble des �l�ments ne v�rifiant pas le pr�dicat */
  | (`Collect, expr)
      /* r�sultat de l'application d'une expression � tous les �l�ments de la collection source */
  | (`Forall, expr)
      /* vrai si tous les �l�ments de la collection v�rifient un pr�dicat */
  | (`Exists, expr)
      /* vrai si au moins un �l�ment de la collection v�rifie le pr�dicat */
  | (`Iterate, expr)
      /* it�ration d'un calcul (avec accumulateur) sur tous les �l�ments de la collection */

/* op�rations sur une collection */
  | `Size                     /* nb d'�l�ments de la collection */
  | (`Intersection, expr)     /* intersection */
  | (`Union, expr)     /* union */
  | (`Difference, expr)    /* diff�rence */
  | (`Symmdiff, expr)   /* diff�rence sym�trique: (A union B) - (A inter B) */
  | `Is_empty                 /* vrai si la collection est vide */
  | `Is_notempty              /* vrai si la collection n'est pas vide */
  | (`Including, expr)        /* �l�ments de la collection, plus un objet */
  | (`Excluding, expr)        /* �l�ments de la collection, except� un objet */
  | (`Includes, expr)       /* appartenance d'un objet � la collection */
  | (`Includes_all, expr)     /* inclusion: appartenance de tous les objets � la collection */
  | `Count                    /* nb d'occurrences d'un objet dans la collection */

/* op�rations sur une suite */
  | `First                    /* premier �l�ment */
  | `Last                     /* dernier �l�ment */
  | (`At, expr)               /* i-�me �l�ment */
  | (`Append, expr)           /* ajout d'un objet � la fin */
  | (`Prepend, expr)          /* ajout d'un objet en t�te */

/* op�rations li�es au types */
  | `Ocltype                        /* type d'un objet */
  | (`Oclistypeof, expr)            /* vrai si l'objet est de ce type */
  | (`Ocliskindof, expr)           /* vrai si l'objet est de ce type ou d'un de ses sous-types */
  | (`Oclastype, expr)          /* ??? renvoie l'objet s'il est de ce type, Undefined sinon ??? */
  | (`Allinstances, path_name_list) /* ensemble des instances d'un type et de ses sous-types */

/* ??? */
  | (`Qualif, expr)

/* ??? */
  | (`Act_p_list, expr)
  | (`Act_p_list1, (expr, expr))

/* ??? */
  | (`Prim_exp, path_name_list)
  | (`Decl1, (path_name_list, type_spec))
  | (`Decl3, (path_name_list, path_name_list, type_spec, expr))
  | (`Decl4, (path_name_list, type_spec, path_name_list, type_spec, expr))

/* contexte d'une contrainte */
  | (`Context1, expr)         /* ??? */
  | (`Context2, type_spec)          /* ??? */

/* invariant d'une contrainte */
  | (`Inv1, (path_name_list, expr))  /* ??? */
  | (`Inv2, expr)                   /* ??? */
  | (`Inv3, (expr, expr))            /* ??? */

/* pr�-condition d'une contrainte */
  | (`Pre1, (path_name_list, expr))  /* ??? */
  | (`Pre2, expr)                   /* ??? */
  | (`Pre3, (expr, expr))            /* ??? */

/* post-condition d'une contrainte */
  | (`Post1, (path_name_list, expr)) /* ??? */
  | (`Post2, expr)                  /* ??? */
  | (`Post3, (expr, expr))           /* ??? */

/* contrainte */
  | (`Constraint1, (expr, expr))        /* pr�-condition */
  | (`Constraint2, (expr, expr))        /* post-condition */
  | (`Constraint3, (expr, expr, expr)) /* ??? pr�-condition (-> diff�rence avec Constraint1 ?) ??? */
  | (`Constraint4, (expr, expr))        /* invariant */



/*** Types des op�rateurs ***/

/** op�rateurs unaires : moins unaire et n�gation logique */
type unary_op = 
  	 `Minus_op 
	| `Not

/** op�rateurs multiplicatifs : multiplication et division */
type multiply_op = 
	 `Multiply 
	| `Divide

/** op�rateurs additifs : addition et soustraction */
type add_op = 
	 `Plus 
	| `Minus

/** op�rateurs relationnels : =, >, <, >=, <= et <> */
type relational_op = 
	 `Equal 
	| `Sup 
	| `Low 
	| `Supequal 
	| `Lowequal 
	| `Different

/** op�rateurs logiques binaires : et, ou, ou exclusif et implication */
type logical_op = 
	 `And 
	| `Or 
	| `Xor 
	| `Implies



/** caract�risation des collections */
type coll_kind =
   `Set        /* ensemble, au sens math�matique du terme */
  | `Bag        /* multi-ensemble: plusieurs fois le m�me �l�ment */
  | `Sequence   /* suite: multi-ensemble ordonn� */
  | `Collection /* super-type des autres types de collections */



/** ??? */
type stereotype =
   `Pre  /* pr�-condition */
  | `Post /* post-condition */
  | `Inv  /* invariant */



/** constantes bool�ennes */
type constant = 
   `True 		
  | `False


/** ??? */
type path_name_list = 
   (`Name_lit, Latin1)
  | (`Type_lit, Latin1)
  | (`Number_lit, Latin1)
  | (`String_lit, Latin1)
  | (`Const, constant)
  | (`Tlist, (path_name_list, path_name_list))
  | (`Path, (path_name_list, path_name_list))
  | (`Nlist, (path_name_list, path_name_list))



/** nature du type */
type type_spec = 
   (`Simp_type, path_name_list)             /* type simple */
  | (`Coll_type, (coll_kind, path_name_list)) /* collection d'objets */



/** ??? */
type formal_param_list =
   (`Formal_par1, (Latin1, type_spec))
  | (`Formal_par2, (Latin1, type_spec, formal_param_list))



/** ??? */
type let_expr =
   (`Let_exp1, (Latin1, expr))
  | (`Let_exp2, (Latin1, formal_param_list, expr))
  | (`Let_exp3, (Latin1, type_spec, expr))
  | (`Let_exp4, (Latin1, formal_param_list, type_spec, expr))
  | (`Let_exp_list, (let_expr, let_expr))
