(*** $Id$ ***)

(** {5 D�compilateur d'expressions B }
@author 
@version 
*)



(** {3 Fonctions auxiliaires } *)

(** ajout d'un s�parateur entre les �l�ments d'une liste
@param k une liste de cha�nes de caract�res
@param t le s�parateur � introduire
@return [ separateur ["a";"b"] "/" = ["a/";"b"] ] *)
val separateur : string list -> string -> string list

(** concat�nation d'une liste de cha�nes de caract�res *)
val list_string : string list -> string

(** �criture d'une liste de cha�nes de caract�res,
le s�parateur d'�l�ments �tant donn� *)
val lsep_str : string list -> string -> string

(** �criture d'une liste de cha�nes de caract�res sous un certain format
@return [ entree ["a";"b"] = "\ta\n\tb\n" ], avec les notations :
        "\t" = tabulation et "\n" = saut de ligne *)
val entree : string list -> string



(** {3 PASSAGE DE LA SYNTAXE ABSTRAITE A LA SYNTAXE CONCRETE B  } *)

(** passage de la syntaxe abstraite � la syntaxe concr�te des types *)
val code_type : B.uml_type -> string

(** codage des relations (fonctions) *)
val fonct : B.type_relation -> string

(** codage des expressions gauches *)
val exp_gauche : B.expr_left -> string

(** codage des op�rateurs *)
val code_op : B.op -> string

(** codage des expressions droites *)
val exp_droite : B.expr_right -> string

(** codage des op�rateurs utilis�s dans la syntaxe des pr�dicats *)
val relation : B.elem_rel -> string

(** codage des pr�dicats *)
val pred : B.predicate -> string

(** codage des d�finitions *)
val code_def : B.def list -> string list

(** codage des substitutions *)
val sub : B.substitution -> string

(** codage d'une op�ration *)
val oper : B.operation -> string

(** application du codage d'une op�ration � une liste d'op�rations *)
val liste_oper : B.operation list -> string

(** codage des ensembles *)
val code_sets : B.set -> string

(** codage des diff�rentes clauses *)
val clauses : B.clause list -> string

(** codage d'une machine *)
val machine : B.amn -> string



(** {3 TRADUCTION DE L'ARBRE B EN UNE CHAINE DE CARACTERES } *)

(** ??? �criture des ensembles de base (-> incomplet ?) ??? *)	
val var_str : B.uml_type -> string

(** ??? �criture des op�rateurs (-> incomplet ?) ??? *)	
val op_str : B.op -> string

(** �criture des expressions gauches *)
val exp_gauche_str : B.expr_left -> string

(** �criture des expressions droites susceptibles de r�sulter de
la traduction d'une expression OCL en une expression B *)
val exp_str : B.expr_right -> string

(** �criture des op�rateurs utilis�s dans la syntaxe des pr�dicats *)
val relation_str : B.elem_rel -> string

(** �criture des pr�dicats *)
val pred_str : B.predicate -> string

(** �criture des alias *)
val def_str : B.def -> string

(** �criture des op�rations *)
val oper_str : B.operation -> string

(** �criture d'une machine B *)
val const_str : B.amn -> string

