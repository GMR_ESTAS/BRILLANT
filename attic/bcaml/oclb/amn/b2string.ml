(*** $Id$ ***)



(** {5 D�compilateur d'expressions B }
@author 
@version 
*)



open B





(*** Fonctions auxiliaires ***)

(** ajout d'un s�parateur entre les �l�ments d'une liste
@param k une liste de cha�nes de caract�res
@param t le s�parateur � introduire
@return [ separateur ["a";"b"] "/" = ["a/";"b"] ] *)
let rec separateur k t =
  match k with
      u::(h::g as v) -> (u^" "^t)::(separateur v t)
    | _ -> k
;;

(** concat�nation d'une liste de cha�nes de caract�res *)
let rec list_string l = List.fold_right (^) l ""
(*
  match l with
      []-> ""
    | u::v -> u^(list_string v)
*)
;;

(* on pourrait remplacer: fun liste sep -> list_string(separateur liste sep) par: *)
(** �criture d'une liste de cha�nes de caract�res,
le s�parateur d'�l�ments �tant donn� *)
let lsep_str liste sep =
  match liste with
    | [] -> ""
    | h::t -> List.fold_left (fun x y -> x^" "^sep^y) h t
;;

(** �criture d'une liste de cha�nes de caract�res sous un certain format
@return [ entree ["a";"b"] = "\ta\n\tb\n" ], avec les notations :
        "\t" = tabulation et "\n" = saut de ligne *)
let rec entree h = List.fold_right (fun u v -> "\t"^u^"\n"^v) h ""
(*
  match h with
      [] -> ""
    | u::v- > "\t"^u^"\n"^(entree v)
*)
;;

(* on pourrait remplacer: fun liste sep -> entree(separateur liste sep) par: *)
(*
let rec entree_liste liste sep =
  match liste with
    | [] -> ""
    | [x] -> "\t"^x^"\n"
    | u::(h::g as v) -> "\t"^u^" "^sep^"\n"^(entree_liste v sep)
;;
*)





(*** PASSAGE DE LA SYNTAXE ABSTRAITE A LA SYNTAXE CONCRETE B ***)

(** passage de la syntaxe abstraite � la syntaxe concr�te des types *)
let rec code_type u =
  match u  with
      String -> "STRING"
    | Nat -> "NAT"
    | Nat1 -> "NAT1"
    | Entier -> "Z"
    | Var2 v -> v
    | Nat2 v -> v
    | Seq(v) -> "seq("^(code_type v)^")" 
    | SeqOne(v) -> "seq1("^(code_type v)^")" 
    | Pow(v) -> "POW("^(code_type v)^")"
    | PowOne(v) -> "POW1("^(code_type v)^")"
    | Bool -> "BOOL"
    | _ -> failwith "code_type()"
;;

(** codage des relations (fonctions) *)
let fonct u =
  match u with
      Relation -> "<->"
    | PartialFunc -> "+->"
    | PartialInj -> ">+>"
    | PartialSurj -> "+->>"
    | PartialBij -> ">+>>"
    | TotalFunc -> "-->"          
    | TotalInj -> ">->"
    | TotalSurj -> "-->>"
    | TotalBij -> ">->>"
;;
          
(** codage des expressions gauches *)
let exp_gauche u =
  match u with
      Ima(a,b)-> (devar a)^"("^(devar b)^")"
    | Val a->devar(a)
;;

(** codage des op�rateurs *)
let code_op = function
    Or -> " or "
  | And -> " & "
  | Implies -> " => "
  | Equiv -> "<=>"
  | Equal -> "="
  | NotEqual -> "/="
  | Less -> "<"
  | LessEqual -> "<="
  | Greater -> ">"
  | GreaterEqual -> ">="
  | SubSet -> "<:"
  | NotSubSet -> "/<:"
  | StrictSubSet -> "<<:"
  | NotStrictSubSet -> "/<<:"
  | In -> ":"
  | NotIn -> "/:"

  | Closure -> "closure"
  | Card -> "card"
  | InterSets -> "/\\"
  | UnionSets -> "\\/"
  | Difference -> "-"
  | Front -> "first"
  | Last -> "tail"
  | Vide -> "{}"
  | Mul -> "*"
  | Div -> "/"
  | Plus -> "+"
  | Minus -> "-"
  | Not -> "not"

  | Addition -> "\\/"
  | Soustraction -> "-"
  | DomRestrict -> "<|"
  | CartesianProd -> "*"
  | DomSubstract -> "<<|"
  | RanSubstract -> "|>>"
  | Produit_parallel -> "||"
  | Produit_direct -> "><"
  | Composition -> ";"
  | OverRideFwd -> "<+"
  | _ -> ""
;;

(** codage des expressions droites *)
let rec exp_droite u =
  match u with
      Exp_rel (a,b,c)->
	let v = (exp_droite a)^(code_op b)^(exp_droite c) in
	  begin
	    match b with
		Composition | DomRestrict -> "("^v^")"
	      | _ -> v
	  end
    | Par_list a -> lsep_str (List.map exp_droite a) ","
    | Ran a -> "ran("^(exp_droite a)^")"
    | Type_base a -> code_type a
    | Val_av a -> (exp_droite a)^"$0"
    | Dom a -> "dom("^(exp_droite a)^")"
    | Tilde a -> "("^(exp_droite a)^")~" 
    | Ens a ->"{"^(exp_droite a)^"}"
    | Rel(a,b,c) -> "("^(exp_droite a)^(fonct b)^(exp_droite c)^")"
    | Tuple(a,b) -> (exp_droite a)^"|->"^(exp_droite b)
    | Ima2(a,b) -> (exp_droite a)^"("^(exp_droite b)^")"
    | Ima3(a,b) -> (exp_droite a)^"["^(exp_droite b)^"]"
    | Op_bin(a,b,c) -> (exp_droite a)^(code_op b)^(exp_droite c)
    | Op_un(a,b) -> (code_op a)^"("^(exp_droite b)^")"
    | Empty_set -> "{}"
    | If_exp (a,b,c) ->
	"IF "^(exp_droite a)^" THEN\n"^(exp_droite b)^"\n"
	^"ELSE\n"^(exp_droite c)^"\n"
    | Def_set(a,b) ->
	"{"^(lsep_str (List.map devar a) ",")
	^" | "^(exp_droite b)^" }"
    | Union_q (a,b,c) ->
	"UNION("^(lsep_str (List.map devar a) ",")
	^").("^(exp_droite b)^" | "^(exp_droite c)^" )"
    | Inter_q (a,b,c) ->
	"INTER("^(lsep_str (List.map devar a) ",")
	^").("^(exp_droite b)^" | "^(exp_droite c)^" )"
    | Sum_g (a,b,c) ->
	"SIGMA("^(lsep_str (List.map devar a) ",")
	^").("^(exp_droite b)^" | "^(exp_droite c)^" )"
    | Prod_g (a,b,c)  ->
	"PI("^(lsep_str (List.map devar a) ",")
	^").("^(exp_droite b)^" | "^(exp_droite c)^" )"
    | Forall(a,b,c) ->
	"!("^(lsep_str (List.map devar a) ",")
	^").("^(exp_droite b)^"=>"^(exp_droite c)^")"
    | Exists(a,b,c) ->
	"#("^(lsep_str (List.map devar a) ",")
	^").("^(exp_droite b)^" & "^(exp_droite c)^")"
 (* | _ -> failwith "exp_droite()" *)
;;

(** codage des op�rateurs utilis�s dans la syntaxe des pr�dicats *)
let relation u =
  match u with
      Inclus -> "<<:"
    | Inclus_ou_egal -> "<:"
    | Appartient -> ":"
    | Different_rel -> "/="
    | Egal -> "="
    | Est_def -> "=="
    | Inferieur -> "<"
    | No_appart -> "/:"
    | _ -> ""
;;

(** codage des pr�dicats *)
let rec pred u =
  match u with
      Predicat v -> exp_droite v
    | Predicat1 (v,r,t) -> (exp_droite v)^(relation r)^(exp_droite t)
    | Predicat2 (v,u,t,x,p) ->
	(exp_droite v)^(relation u)^(exp_droite t)
	^(fonct x)^(exp_droite p)
    | Predicat3(a,b) -> (exp_droite a)^"("^(exp_droite b)^")"
    | Predicat4 s -> s
    | Qlq(a,b,c) ->
	"!("^(lsep_str (List.map devar a) ",")
	^").("^(pred b)^"=>"^(pred c)^")"
    | EXT(a,b,c) ->
	"#("^(lsep_str (List.map devar a) ",")
	^").("^(pred b)^" & "^(pred c)^")"
    | AND (a,b) -> "("^(pred a)^" & "^(pred b)^")"
    | OR (a,b) -> "("^(pred a)^" or "^(pred b)^")"
;;

(** codage des d�finitions *)
let rec code_def e =
  match e with
      [] ->[]
    | (Definition1 (a,c))::v ->
	((exp_gauche a)^"=="^(exp_droite c))::(code_def v)
    | (Definition2 a)::v -> (pred a)::(code_def v)
;;

(** codage des substitutions *)
let rec sub d =
  match d with
      Sub(a,b) -> "\t"^((exp_gauche a)^":="^(exp_droite b))
    | Subst(a,b) -> "\t"^((exp_droite a)^":="^(exp_droite b))
    | Dev_tq(a,b) -> 
	"\t"^(lsep_str (List.map devar a) ",")^
	" :( "^(exp_droite b)^" )"
    | If(a,b,c) ->
        "\tIF "^(pred a)^"\n"^"\tTHEN\n"^(sub b)^"\n"^
        "\tELSE\n"^(sub c)^"\tEND\n"
    | Any(a,b,c) ->
        "\tANY "^(lsep_str (List.map devar a) ",")^ 
        " WHERE\n"^(entree (separateur(List.map pred b) "&"))^ 
        "\n\tTHEN\n"^(sub c)^"\n\tEND\n"
    | SELECT((a,b),c) ->
	let rec trt_branche_select s =
	  begin
	    match s with
		[] -> ""
	      | (x,v)::w ->
		  if x=[] then "\tELSE\n\t"^(sub v)^"\tEND"
		  else "\tWHEN  "^(pred (List.hd x))^
                       "THEN"^(sub v)^(trt_branche_select w)^"\n\tEND\n"
	  end
	in "\tSELECT "^(pred a)^
           "\n\tTHEN\n"^(sub b)^"\n"^(trt_branche_select c)
    | SKIP -> "\tskip\n"
    | Appel1(a,b) ->
	"\t"^(devar a)^"("^(lsep_str (List.map devar b) ",")^")"
    | Parallel a ->
	begin
          match a with
              [] -> ""
            | x::y ->
		begin
		  match y with
		      [] -> (* "\t"^ *) sub x 
		    | _ -> (* "\t"^ *)
                           (sub x)^"  ||"^"\n"^(sub (Parallel y))
		end
	end
;;

(** codage d'une op�ration *)
let oper (Oper(r, e, a, Pre c, d)) =
  let u =
    match r with
	[] -> ""
      | _ -> (lsep_str (List.map devar r) ",")^"<--"
  and v =
    match a with
	[] -> "=\n"
      | _ -> "("^(lsep_str (List.map devar a) ",")^")="^"\n"
  and w =
    match c with
	[] -> "BEGIN"
      | _ -> "PRE\n"^(entree (separateur (List.map pred c) "&"))^"THEN"
  and x = "\n"^(sub d)^"\nEND"
  in u^(devar e)^v^w^x
;;

(** application du codage d'une op�ration � une liste d'op�rations *)
let rec liste_oper l =
  match l with
      [] -> ""
    | [x] -> oper x
    | u::v -> (oper u)^" ;"^"\n\n"^(liste_oper v)
;;

(** codage des ensembles *)
let code_sets e =
  match e with
    | Set1 a -> devar a
    | Set2 a -> exp_droite a
;;

(** codage des diff�rentes clauses *)
let clauses lst =
  let v1 =
    let b = cl_sets lst in
      match b with
	  [] -> ""
	| _ -> "SETS\n"^(entree(separateur (List.map code_sets b) " ;"))^"\n"
  and v2 =
    let c = cl_uses lst in
      match c with
	  [] -> ""
	| _ -> "\nUSES\n"^(entree(separateur (List.map devar c) ","))^"\n"
  and v3 =
    let c1 = cl_extends lst in
      match c1 with
	  [] -> ""
	| _ -> "EXTENDS\n"^(entree(separateur (List.map devar c1) ","))^"\n"
  and v4 =
    let d = cl_includes lst in
      match d with
	  [] -> ""
	| _ -> "INCLUDES\n"^(entree(separateur (List.map devar d) ","))^"\n"
  and v5 =
    let j = cl_promotes lst in
      match j with
	  [] -> ""
	| _ -> "PROMOTES\n"^(entree(separateur (List.map devar j) ","))^"\n"    
  and v6 =
    let e = cl_definitions lst in
      match e with
	  [] -> ""
	| _ -> "DEFINITIONS\n"^(entree(separateur (code_def e) " ^"))^"\n"
  and v7 =
    let f = cl_variables lst in
      match f with
	  [] -> ""
	| _ -> "VARIABLES\n"^(entree(separateur (List.map devar f) ","))^"\n"
  and v8 =
    let g = cl_invariant lst in
      match g with
	  [] -> ""
	| _ -> "INVARIANT\n"^(entree(separateur (List.map pred g) "&"))^"\n"
  and v9 =
    let h = cl_initialisation lst in
      match h with
	  Parallel [] -> ""
	| _ -> "INITIALISATION\n"^(sub h)
  and v10 =
    let i = cl_operations lst in
      match i with
	  [] -> ""
	| _ -> "\nOPERATIONS\n"^(liste_oper i)^"\n"
  in
    v1^v2^v3^v4^v5^v6^v7^v8^v9^v10^"\nEND\n"   

(** codage d'une machine *)
let rec machine u =
  match u with
      Machine ((a, _), lst) ->
	"\nMACHINE "^(devar a)^"\n"^(clauses lst)
    | Refinement ((a, _), a1, lst) ->
	"\nREFINEMENT "^(devar a)^"\nREFINES "^(devar a1)^"\n"^(clauses lst)
    | _ -> failwith "machine()"





(*** TRADUCTION DE L'ARBRE B EN UNE CHAINE DE CARACTERES ***)

(** ??? �criture des ensembles de base (-> incomplet ?) ??? *)	
let rec var_str b = 
  match b with
      Var2 u -> "Var2( "^u^" )"
    | Nat2 u -> "Nat2( "^u^" )"
    | _ -> failwith "var_str()"

(** ??? �criture des op�rateurs (-> incomplet ?) ??? *)	
and op_str = function
    Card -> "Card"
  | InterSets -> "InterSets"
  | UnionSets -> "UnionSets"
  | Difference -> "Difference"
  | Front -> "Front"
  | Last -> "Last"
  | Vide -> "Vide"
  | Equal -> "Equal"
  | NotEqual -> "NotEqual"
  | Less -> "Less"
  | LessEqual -> "LessEqual"
  | Greater -> "Greater"
  | GreaterEqual -> "GreaterEqual"
  | SubSet -> "SubSet"
  | NotSubSet -> "NotSubSet"
  | StrictSubSet -> "StrictSubSet"
  | NotStrictSubSet -> "NotStrictSubSet"
  | In -> "In"
  | NotIn -> "NotIn"
  | Mul -> "Mul"
  | Div -> "Div"
  | Plus -> "Plus"
  | Minus -> "Minus"
  | Not -> "Not"
  | Or -> "Or"
  | And -> "And"
  | Implies -> "Implies"
  | Equiv -> "Equiv"
  | _ -> failwith "op_str()"

(** �criture des expressions gauches *)
and exp_gauche_str b =
  match b with
      Ima (u,v) -> "Ima (Var("^(devar u)^") "^"Var("^(devar v)^") )"
    | Val u -> "Val ("^(devar u)^") "

(** �criture des expressions droites susceptibles de r�sulter de
la traduction d'une expression OCL en une expression B *)
and exp_str b =
  match b with
      Exp_rel(u,v,w) -> "Op_bin ("^(exp_str u)^(op_str v)^(exp_str w)^") "
    | Par_list a -> "Par_list ("
                    ^(lsep_str (List.map exp_droite a) ",")^" )"
    | Ima2(u,v) -> "Ima2 ("^(exp_str u)^(exp_str v)^") "
    | Ima3(u,v) -> "Ima3 ("^(exp_str u)^(exp_str v)^") "
    | Type_base(u) -> "Type_base ("^(var_str u)^") "
    | Op_bin(u,v,w) -> "Op_bin ("^(exp_str u)^(op_str v)^(exp_str w)^") "
    | Op_un(u,v) -> "Op_un ("^(op_str u)^"("^(exp_str v)^")"^") "
    | Empty_set -> "{}"
    | If_exp(u,v,w) -> "If_exp ("^(exp_str u)^(exp_str v)^(exp_str w)^") "
    | Def_set(u,v) ->
	"Def_set (("^(lsep_str (List.map devar u) " ")
	^")"^(exp_str v)^") "
    | Union_q (a,b,c) ->
	"Union_q ("^(lsep_str (List.map devar a) ",")^") "
	^" ) ( "^(exp_droite b)^" ) ( "^(exp_droite c)^" )"
    | Inter_q (a,b,c) ->
	"Inter_q ("^(lsep_str (List.map devar a) ",")^") "
	^" ) ( "^(exp_droite b)^" ) ( "^(exp_droite c)^" )"
    | Sum_g (a,b,c) ->
	"Sum_g( "^(lsep_str (List.map devar a) ",")^") "
	^" ) ( "^(exp_droite b)^" ) ( "^(exp_droite c)^" )"
    | Prod_g (a,b,c)  ->
	"Prod_g ("^(lsep_str (List.map devar a) ",")^") "
	^" ) ( "^(exp_droite b)^" ) ( "^(exp_droite c)^" )"
    | Forall(a,b,c) ->
	"Forall ("^(lsep_str (List.map devar a) ",")^") "
	^" ) ( "^(exp_droite b)^" ) ( "^(exp_droite c)^" )"
    | Exists(a,b,c) ->
	"Exists ("^(lsep_str (List.map devar a) ",")^") "
	^" ) ( "^(exp_droite b)^" ) ( "^(exp_droite c)^" )"    
    | _ -> "UNDEF_TYPE"

(** �criture des op�rateurs utilis�s dans la syntaxe des pr�dicats *)
and relation_str b =
  match b with
      Inclus -> "Inclus"
    | Inclus_ou_egal -> "Inclus_ou_egal"
    | Appartient -> "Appartient"
    | Different_rel -> "Different_rel"
    | Est_def -> "Est_def"
    | Egal -> "Egal"
    | Inferieur -> "Inferieur"
    | No_appart -> "No_appart"
    | Intersection_rel -> "Intersection_rel"

(** �criture des pr�dicats *)
and pred_str b =
  match b with
      Predicat u -> "Predicat ("^(exp_str u)^") "
    | Predicat1 (u,v,w) -> "Predicat1 ("^(exp_str u)
                           ^(relation_str v)^(exp_str w)^") "
    | Qlq (u,v,w) -> "Quelquesoit ("^(lsep_str (List.map devar u) ",")
                     ^(pred_str v)^(pred_str w)^") "
    | EXT (u,v,w) -> "Exists ("^(lsep_str (List.map devar u) ",")
                     ^(pred_str v)^(pred_str w)^") "
    | AND (u,v) -> "AND ("^(pred_str u)^(pred_str v)^") "
    | _ -> failwith "pred_str()"

(** �criture des alias *)
and def_str b =
  match b with
      Definition1 (u,v) -> (exp_gauche_str u)^(exp_str v)
    | Definition2 u -> pred_str u

(** �criture des op�rations *)
and oper_str (Oper (c,d,e,Pre f,g)) =
  let u = match c with 
      [] -> "\n\tListe()"
    | _ -> "\n\tListe ("^
	(lsep_str (List.map (fun x -> "(Var "^x^")") (List.map devar c)) " ")^" )"
  and v = match e with
      [] -> "\n\tListe()"
    | _ -> "\n\t	Liste ("^
	(lsep_str (List.map (fun x -> "(Var "^x^")") (List.map devar e)) " ")^" )"
  and w = match f with
      [] -> "\n\tPre()"
      | _ -> "\n\tPre ("^(lsep_str (List.map pred_str f) " ")^" )"
  and x = "Sub" (* sub g *)
  in 
    "\n\tOper ("^u^"Var("^(devar d)^")"^v^w^x^" )"

(** �criture d'une machine B *)
and const_str b =
  match b with
      Machine ((a, _), lst) ->
	"Machine (\n\t"^
	"Var ("^(devar a)^")\n"^
        "SETS\n"^(entree (separateur (List.map code_sets (cl_sets lst)) " ^"))^"\n"^
	"Uses ("^(lsep_str (List.map devar (cl_uses lst)) " ")^")\n\t"^
	"Extends ("^(lsep_str (List.map devar (cl_extends lst)) " ")^")\n\t"^
	"Includes ("^(lsep_str (List.map devar (cl_includes lst)) " ")^")\n\t"^
	"Promotes ("^(lsep_str (List.map devar (cl_promotes lst)) " ")^")\n\t"^
	"Definitions ("^(lsep_str (List.map def_str (cl_definitions lst)) " ")^")\n\t"^
	"Variables ("^(lsep_str (List.map devar (cl_variables lst)) " ")^")\n\t"^
	"Invariant ("^(lsep_str (List.map pred_str (cl_invariant lst)) " ")^")\n\t"^
	"Initialisation (" (* ??? cl_initialisation lst ??? *) ^")\n\t"^
	"Operations ("^(lsep_str (List.map oper_str (cl_operations lst)) " ")^")\n         )"
    | _ -> failwith "const_str()"

