(*** $Id$ ***)



(** {5 Syntaxe abstraite B }
@author
@version
@see
*)





(* DEFINITION DES TYPES *)

(** type g�n�rique d'un identificateur *)
type ident = Var of string ;;

(** d�finition des ensembles de base *)
type uml_type =
    SEQ of uml_type  (* suites d'�l�ments *)
  | SEQ1 of uml_type (* suites non vides *)
  | String           (* cha�nes de caract�res *)
  | Bool             (* bool�ens *)
  | Entier           (* entiers relatifs *)
  | Nat              (* entiers naturels impl�mentables *)
  | Nat1             (* entiers naturels non nuls impl�mentables *)
  | Nat2 of string   (* ??? *)
  | Var2 of string   (* ??? *)
  | POW of uml_type  (* sous-ensembles *)
  | POW1 of uml_type (* sous-ensembles non vides *)
;;



(*** D�finition des relations ***)

(** d�finition des op�rateurs utilis�s dans la syntaxe des pr�dicats *)
type elem_rel =
    Inclut
  | Inclut_ou_egale
  | Appartient
  | Different
  | Est_def
  | Egale
  | Inferieure
  | No_appart
  | Intersection
;;

(** caract�risation d'une relation *)
type type_relation =
    Relation    (* relation quelconque *)
  | Partial_fun (* fonction partielle *)
  | Partial_inj (* fct partielle injective *)
  | Partial_sur (* fct partielle surjective *)
  | Partial_bij (* fct partielle bijective *)
  | Function    (* application: fonction totale *)
  | Injection   (* application injective *)
  | Surjection  (* application surjective *)
  | Bijection   (* application bijective *)
;;



(*** D�finition des op�rateurs ***)

(** les diff�rents op�rateurs de la notation B *)
type operateur =
(* op�rateurs arithm�tiques *)
    Plus_b
  | Minus_b
  | Multiply_b
  | Divide_b

(* op�rateurs relationnels *)
  | Equal_b
  | Sup_b
  | Low_b
  | Supequal_b
  | Lowequal_b
  | Different_b

(* op�rateurs logiques *)
  | Not_b
  | And_b
  | Or_b
  | Implies_b

(* op�rateurs ensemblistes *)
  | Card_b
  | Is_empty_b
  | Belong_b
  | Inclusion_b
  | Intersection_b
  | Union_b
  | Difference_b
  | Symmdiff_b        (* diff�rence sym�trique: (A union B) - (A inter B) *)

(* op�rateurs sur les relations (fonctions) *)
  | Produit_cartesien (* produit cart�sien *)
  | Closure           (* fermeture transitive et r�flexive *)
  | Resdom            (* restriction de domaine *)
  | Antirestriction   (* anti-restriction de domaine: *)
                         (* on conserve les couples dont l'�l�ment de gauche
			    n'appartient pas � un certain sous-ensemble du domaine *)
  | CoAntirestriction (* anti-restriction de co-domaine: *)
                         (* on conserve les couples dont l'�l�ment de droite
			    n'appartient pas � un certain sous-ensemble du co-domaine *)
  | Surcharge         (* surcharge: *)
                         (* on conserve tous les couples de la relation �crasante,
			    plus les couples de la relation �cras�e dont l'�l�ment de
			    gauche n'appartient pas au domaine de la relation �crasante *)
  | Produit_direct    (* produit direct: *)
                         (* si R1:A<->B et R2:A<->C, alors R = R1><R2 est l'ensemble des
			    couples (a,(b,c)) tels que (a,b):R1 et (a,c):R2 *)
  | Produit_parallel  (* produit parall�le: *)
                         (* si R1:A<->B et R2:B<->C, alors R = R1||R2 est l'ensemble des
			    couples ((a,b1),(b2,c)) tels que (a,b2):R1 et (b1,c):R2 *)
  | Composition       (* composition en avant: f;g = g�f *)

(* op�rateurs sur les suites *)
  | Count_b           (* nb d'occurrences d'un �l�ment *)
  | First_b           (* premier �l�ment *)
  | Last_b            (* dernier �l�ment *)
  | At_b              (* i-�me �l�ment *)
  | Append_b          (* ajout � la fin *)
  | Prepend_b         (* ajout en t�te *)


(* ??? cat�gorie?  ??? *)
  | Addition          (* ??? diff�rence avec Union_b ? ??? *)
  | Soustraction      (* ??? cf. Addition ??? *)
  | Vide              (* ??? *)
;;


    
(*** D�finition d'expressions droite et gauche ***)

(** ??? expression situ�e � gauche d'un op�rateur ??? *)
type expression_gauche =
    Ima of ident * ident
  | Val of ident
;;

(** ??? expression situ�e � droite d'un op�rateur ??? *)
type expression_droite = 
(* expressions logiques *)
  | Forall_b of ident list * expression_droite * expression_droite        (* quantificateur universel *)
  | Exists_b of ident list * expression_droite * expression_droite        (* quantificateur existentiel *)
  | If_exp_b of expression_droite * expression_droite * expression_droite (* if then else *)
  | Rel of expression_droite * type_relation * expression_droite          (* ??? pr�dicat relationnel ??? *)

(* expressions ensemblistes *)
  | Empty_set (* l'ensemble vide *)
  | Tuple of expression_droite * expression_droite                (* maplet: couple d'�l�ments *)
  | Ens of expression_droite                                      (* ensemble d'�l�ments *)
  | Def_set of ident list * expression_droite                     (* ensemble d�fini en compr�hension *)
  | Union_q of ident list * expression_droite * expression_droite (* union quantifi�e *)
  | Inter_q of ident list * expression_droite * expression_droite (* intersection quantifi�e *)

(* expressions sur les relations (fonctions) *)
  | Dom of expression_droite                                     (* domaine d'une relation *)
  | Ran of expression_droite                                     (* range: co-domaine *)
  | Inv of expression_droite                                     (* relation r�ciproque *)
  | Ima2 of expression_droite * expression_droite                (* image directe (d'une fonction) *)
  | Ima3 of expression_droite * expression_droite                (* image relationnelle *)
  | Exp_rel of expression_droite * operateur * expression_droite (* ??? expression entre 2 relations ??? *)

(* expressions d'objets math�matiques *)
  | Sum_g of ident list * expression_droite * expression_droite  (* produit quantifi� *)
  | Prod_g of ident list * expression_droite * expression_droite (* produit quantifi� d'entiers *)

(* expressions li�es par un op�rateur *)
  | Op_un of operateur * expression_droite                      (* op�rateur unaire *)
  | Op_bin of expression_droite * operateur * expression_droite (* op�rateur binaire *)

  | Type_base of uml_type (* type de base *)
  | Val_av of expression_droite (* valeur pr�c�dente d'une donn�e *)
  | Par_list of expression_droite list (* ??? liste de param�tres ??? *)
;; 



(*** D�finition des pr�dicats ***)

(** syntaxe d'un pr�dicat *)
type predicat =
    Predicat of expression_droite           (* pr�dicat simple *)
  | Predicat1 of                            (* pr�dicat avec op�rateur *)
      expression_droite
      * elem_rel
      * expression_droite
  | Predicat2 of                            (* pr�dicat avec op�rateur et relation (fonction) *)
      expression_droite
      * elem_rel
      * expression_droite
      * type_relation
      * expression_droite
  | Predicat3 of                            (* ??? *)
      expression_droite
      * expression_droite
  | Predicat4 of string                     (* ??? *)
  | Qlq of ident list * predicat * predicat (* quantificateur universel *)
  | EXT of ident list * predicat * predicat (* quantificateur existentiel *)
  | AND of predicat * predicat              (* conjonction *)
  | OR of predicat * predicat               (* disjonction *)
;;





(*** D�finition des composants d'une machine B ***)

(** un INVARIANT est compos� d'une liste de pr�dicats *)              
type invariant = Invariant of predicat list ;;

(** caract�risation des substitutions en B *)
type substitution = 
    Sub of expression_gauche * expression_droite       (* substitution simple *)
  | Subst of expression_droite * expression_droite     (* ??? diff�rence avec Sub ??? *)
  | Dev_tq of ident list * expression_droite           (* devient tel que *)
  | If of predicat * substitution  * substitution      (* si pr�dicat vrai, alors subst1, sinon subst2 *)
  | SELECT of                                          (* substitution gard�e *)
      (predicat * substitution)
      * (predicat list * substitution) list
  | SKIP                                               (* substitution sans effet *)
  | Appel1 of ident * ident list                       (* ??? *)
  | Parallel  of substitution list                     (* substitutions effectu�es simultan�ment *)
  | Any_b of ident list * predicat list * substitution (* substitution de choix non born� *)
;;

(** d�finition d'un ensemble *)
type set =
    Set1 of ident
  | Set2 of expression_droite
;;

(** d�finition d'un alias *)
type def =
    Definition1 of expression_gauche * expression_droite
  | Definition2 of predicat
;;

(** la clause DEFINITIONS regroupe toutes les abr�viations *)
type definition = Definition of def list ;;

(** une pr�-condition d'une substitution porte sur une liste de pr�dicats *)
type precond = Pre of predicat list ;;

(** substitution d'initialisation *)
type initialisation = Initialisation of substitution ;;

(** la clause VARIABLES regroupe toutes les variables de la machine *)
type variables = Variables of ident list ;;

(** une op�ration est d�finie par :
- un identificateur
- une liste de param�tres
- une liste de valeurs de retour
- une pr�-condition
- une substitution *)
type operation = Oper of
  ident list     (* ??? valeurs de retour ??? *)
  * ident
  * ident list   (* ??? param�tres ??? *)
  * precond
  * substitution ;;

(** la clause OPERATIONS regroupe toutes les op�rations relatives � la machine *)
type operations = Operation of operation list ;;



(*** Clauses d'architecture ***)

(** la clause USES stipule le
partage des donn�es d'une machine incluse *)
type uses = Uses of ident list ;;

(** la clause SETS regroupe les
d�finitions des ensembles utilis�s dans la machine *)
type sets = Sets of set list ;;

(** la clause INCLUDES stipule le
partage des donn�es et des op�rations d'une autre machine *)
type includes = Includes of ident list ;;

(** la clause EXTENDS �nonce l'inclusion et la promotion
de toutes les op�rations de la machine incluse *)
type extendes = Extendes of ident list ;;

(** avec la clause PROMOTES, la machine incluante s'approprie les op�rations
de la machine incluse (pas de distinction avec ses propres op�rations) *)
type promote = Promote of ident list ;;



(** d�finition d'une machine B *)
type machine =
    Mache of           (* soit une machine abstraite *)
      ident
      * sets
      * uses
      * extendes
      * includes
      * promote
      * definition
      * variables
      * invariant
      * initialisation
      * operations
  | Refinement of      (* soit un raffinement *)
      ident
      * ident
      * sets
      * uses
      * extendes
      * includes
      * promote
      * definition
      * variables
      * invariant
      * initialisation
      * operations
;;





(* DEFINITION DES FONCTIONS *)

(** valeur d'un identificateur
@return la cha�ne de caract�res correspondant au nom de cet identificateur *)
let devar (Var a) = a ;;

(** union de 2 clauses INVARIANT
@return un invariant contenant la fusion des 2 listes de pr�dicats *)
let unionI (Invariant a) (Invariant b) = Invariant (a@b) ;;

(** union de 2 clauses VARIABLES
@return la fusion des 2 listes d'identificateurs *)
let unionV (Variables a) (Variables b) =  (a@b) ;;	      



(*** Fonctions d'acc�s aux composants d'une machine B ***)

(** nom d'une machine
@return l'identificateur de la machine *)
let acces_nom u =
  match u with
      Mache(x,_,_,_,_,_,_,_,_,_,_)-> x
    | Refinement (x,_,_,_,_,_,_,_,_,_,_,_)-> x
;;

(** invariant d'une machine
@return la liste des pr�dicats composant l'invariant de la machine *)
let acces_inv u =
  match u with
      Mache(_,_,_,_,_,_,_,_,Invariant x,_,_)-> x
    | Refinement(_,_,_,_,_,_,_,_,_,Invariant x,_,_)-> x
;;

(** op�rations d'une machine
@return la liste des op�rations de la  machine *)
let acces_opers u =
  match u with
      Mache(_,_,_,_,_,_,_,_,_,_,Operation x) -> x
    | Refinement(_,_,_,_,_,_,_,_,_,_,_,Operation x) -> x
;;

(** op�rations d'une machine
@return la clause OPERATIONS de la machine B *)
let acceso u =
  match u with
      Mache(_,_,_,_,_,_,_,_,_,_,x)-> x
    | Refinement (_,_,_,_,_,_,_,_,_,_,_,x)-> x
;;

(** nom d'une op�ration
@return la valeur de l'identificateur d'une op�ration *)
let acces_oper_id (Oper(_,Var ident,_,_,_)) = ident ;;




(*** Fonctions de cr�ation de composants d'une machine B ***)

(** cr�ation d'un identificateur
@param u une cha�ne de caract�res 
@return l'identificateur de nom u *)
let rec def_ident u = Var u

(** cr�ation de la clause USES
@param u une liste de noms (cha�nes de caract�res)
@return une clause USES compos�e d'une liste d'identificateurs
@return correspondant aux noms de u en majuscules *)
and def_uses u =
  let u_cp = (List.map String.capitalize u)
  in Uses (List.map def_ident u_cp)

(** cr�ation de la clause INCLUDES
@param u une liste de noms (cha�nes de caract�res)
@return une clause INCLUDES compos�e d'une liste d'identificateurs
@return correspondant aux noms de u en majuscules *)
and def_includes u =
  let u_cp = (List.map String.capitalize u)
  in Includes (List.map def_ident u_cp)

(** cr�ation de la clause EXTENDS
@param u une liste de noms (cha�nes de caract�res)
@return une clause EXTENDS compos�e d'une liste d'identificateurs
@return correspondant aux noms de u *)
and def_extendes u = Extendes (List.map def_ident u)

(** cr�ation de la clause PROMOTES
@param u une liste de noms (cha�nes de caract�res)
@return une clause PROMOTES compos�e d'une liste d'identificateurs
@return correspondant aux noms de u *)
and def_promote u = Promote (List.map def_ident u)

(** ??? cr�ation d'une clause DEFINITIONS vide (-> utilit� ?) ??? *)
and def_definition u = Definition []

(** cr�ation de la clause VARIABLES
@param u une liste de noms (cha�nes de caract�res)
@return une clause VARIABLES compos�e d'une liste d'identificateurs
@return correspondant aux noms de u en minuscules *)
and def_variables u =
  let u_lc = (List.map String.lowercase u)
  in Variables (List.map def_ident u_lc)

(** cr�ation de l'INVARIANT
@param u une liste de pr�dicats
@return une clause INVARIANT compos�e de u *)
and def_invariant u = Invariant u

(** cr�ation de la substitution d'initialisation
@param u une liste de noms (cha�nes de caract�res)
@return dans la clause INITIALISATION renvoy�e, on affecte en parall�le
@return � chaque variable la valeur ensemble vide, une variable �tant
@return d�termin�e par l'identificateur d'un nom de la liste u *)
and def_initialisation_empty x =
  let rec empty_sub u =
    match u with
	[] -> []
      | v::w -> Sub ((Val (Var v)),Empty_set)::empty_sub w
  in Initialisation (Parallel (empty_sub x))

(** cr�ation des OPERATIONS
@param u une liste de noms (cha�nes de caract�res)
@return dans la clause OPERATIONS renvoy�e, chaque op�ration est
@return d�sign�e par l'identificateur d'un nom de la liste u
@return et est d�finie par une substitution sans effet (SKIP),
@return sans pr�-condition, sans param�tre ni valeur de retour *)
and def_operations u = 
  let def_op v = Oper([],Var v ,[],Pre [],SKIP)
  in Operation(List.map def_op u)
;;

(* cr�ation d'une machine B vide *)
let new_machine = Mache(def_ident "Vide",
			Sets [],
			Uses [],
			Extendes [],
			Includes [],
			Promote [],
			Definition [],
			Variables [],
			Invariant [],
			Initialisation(Parallel []),
			Operation [])
;;

