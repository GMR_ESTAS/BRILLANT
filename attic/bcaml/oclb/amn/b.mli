(*** $Id$ ***)

(** {5 Syntaxe abstraite B }
@author 
@version 
*)



(** {1 DEFINITION DES TYPES } *)

(** type g�n�rique d'un identificateur *)
type ident = Var of string

(** !!! provisoire !!! *) (* blast.ml *)
and id = ident

(** !!! provisoire !!! *) (* blast.ml *)
and ids = id list

(** !!! provisoire !!! *) (* blast.ml *)
and head = id * ids

(** !!! provisoire !!! *) (* blast.ml *)
and instance = ident


(** d�finition des ensembles de base *)
and uml_type =
    Seq of uml_type
  | SeqOne of uml_type
  | ISeq of uml_type
  | ISeqOne of uml_type
  | Perm of uml_type
  | String
  | Bool
  | Entier
  | Nat
  | Nat1
  | Nat2 of string
  | Var2 of string
  | Pow of uml_type
  | PowOne of uml_type
  | Fin of uml_type
  | FinOne of uml_type


(** {3 D�finition des relations } *)

(** d�finition des op�rateurs utilis�s dans la syntaxe des pr�dicats *)
and elem_rel =
    Inclus
  | Inclus_ou_egal
  | Appartient
  | Different_rel
  | Est_def
  | Egal
  | Inferieur
  | No_appart
  | Intersection_rel

(** caract�risation d'une relation *)
and type_relation =
    Relation
  | PartialFunc
  | PartialInj
  | PartialSurj
  | PartialBij
  | TotalFunc
  | TotalInj
  | TotalSurj
  | TotalBij


(** {3 D�finition des op�rateurs } *)

(** les diff�rents op�rateurs de la notation B *)
and op =
    Plus
  | Minus
  | UMinus
  | Mul
  | Div
  | Mod
  | Puissance
  | Equal
  | NotEqual
  | Less
  | LessEqual
  | Greater
  | GreaterEqual
  | Not
  | Or
  | And
  | Implies
  | Equiv
  | SubSet
  | NotSubSet
  | StrictSubSet
  | NotStrictSubSet
  | In
  | NotIn
  | Card
  | Is_empty
  | InterSets
  | UnionSets
  | Difference
  | Symmdiff
  | Identity
  | CartesianProd
  | Closure
  | ClosureOne
  | DomRestrict
  | RanRestrict
  | DomSubstract
  | RanSubstract
  | OverRideFwd
  | OverRideBck
  | Produit_direct
  | Produit_parallel
  | Composition
  | RelFnc
  | FncRel
  | Prj1
  | Prj2
  | Size
  | Count
  | Front
  | Tail
  | Last
  | At
  | AppendSeq
  | PrependSeq
  | PrefixSeq
  | SuffixSeq
  | ConcatSeq
  | Rev
  | Tree
  | Btree
  | Left
  | Right
  | Top
  | Sons
  | Prefix
  | Infix
  | Postfix
  | SizeT
  | Rank
  | Father
  | SubTree
  | Arity
  | Addition
  | Soustraction
  | Vide


(** {3 D�finition d'expressions droite et gauche } *)

(** ??? expression situ�e � gauche d'un op�rateur ??? *)
and expr_left = Ima of ident * ident | Val of ident

(** ??? expression situ�e � droite d'un op�rateur ??? *)
and expr_right =
    Forall of ident list * expr_right * expr_right
  | Exists of ident list * expr_right * expr_right
  | If_exp of expr_right * expr_right * expr_right
  | Rel of expr_right * type_relation * expr_right
  | Empty_set
  | Tuple of expr_right * expr_right
  | Ens of expr_right
  | Def_set of ident list * expr_right
  | Union_q of ident list * expr_right * expr_right
  | Inter_q of ident list * expr_right * expr_right
  | Dom of expr_right
  | Ran of expr_right
  | Tilde of expr_right
  | Ima2 of expr_right * expr_right
  | Ima3 of expr_right * expr_right
  | Exp_rel of expr_right * op * expr_right
  | Sum_g of ident list * expr_right * expr_right
  | Prod_g of ident list * expr_right * expr_right
  | Op_un of op * expr_right
  | Op_bin of expr_right * op * expr_right
  | Type_base of uml_type
  | Val_av of expr_right
  | Par_list of expr_right list


(** {3 D�finition des pr�dicats } *)

(** syntaxe d'un pr�dicat *)
and predicate =
    Predicat of expr_right
  | Predicat1 of expr_right * elem_rel * expr_right
  | Predicat2 of expr_right * elem_rel * expr_right * type_relation *
      expr_right
  | Predicat3 of expr_right * expr_right
  | Predicat4 of string
  | Qlq of ident list * predicate * predicate
  | EXT of ident list * predicate * predicate
  | AND of predicate * predicate
  | OR of predicate * predicate



(** {3 D�finition des composants d'une machine B } *)

(*
(** un INVARIANT est compos� d'une liste de pr�dicats *)
and invariant = Invariant of predicate list
*)

(** caract�risation des substitutions en B *)
and substitution =
    Sub of expr_left * expr_right
  | Subst of expr_right * expr_right
  | Dev_tq of ident list * expr_right
  | If of predicate * substitution * substitution
  | SELECT of (predicate * substitution) *
      (predicate list * substitution) list
  | SKIP
  | Appel1 of ident * ident list
  | Parallel of substitution list
  | Any of ident list * predicate list * substitution

and event = id * ids * substitution (* blast.ml *)


(** d�finition d'un ensemble *)
and set = Set1 of ident | Set2 of expr_right

(** d�finition d'un alias *)
and def = Definition1 of expr_left * expr_right | Definition2 of predicate

(*
(** la clause DEFINITIONS regroupe toutes les abr�viations *)
and definitions = Definitions of def list
*)

(** une pr�-condition d'une substitution porte sur une liste de pr�dicats *)
and precond = Pre of predicate list

(*
(** substitution d'initialisation *)
and initialisation = Initialisation of substitution
*)

(*
(** la clause VARIABLES regroupe toutes les variables de la machine *)
and variables = Variables of ident list
*)

(** une op�ration est d�finie par :
- un identificateur
- une liste de param�tres
- une liste de valeurs de retour
- une pr�-condition
- une substitution *)
and operation =
    Oper of ident list * ident * ident list * precond * substitution

(*
(** la clause OPERATIONS regroupe toutes les op�rations relatives � la machine *)
and operations = Operations of operation list
*)


(** {3 Clauses d'architecture } *)

(*
(** la clause USES stipule le
partage des donn�es d'une machine incluse *)
and uses = Uses of ident list
*)

(*
(** la clause SETS regroupe les
d�finitions des ensembles utilis�s dans la machine *)
and sets = Sets of set list
*)

(*
(** la clause INCLUDES stipule le
partage des donn�es et des op�rations d'une autre machine *)
and includes = Includes of ident list
*)

(*
(** la clause EXTENDS �nonce l'inclusion et la promotion
de toutes les op�rations de la machine incluse *)
and extends = Extends of ident list
*)

(*
(** avec la clause PROMOTES, la machine incluante s'approprie les op�rations
de la machine incluse (pas de distinction avec ses propres op�rations) *)
and promotes = Promotes of ident list
*)


(** d�finition g�n�rale d'une clause (blast.ml) *)
type clause = 
  | Definitions of def list
  | Constraints of predicate
  | Invariant of predicate list      (* !!! diff�rent de blast.ml !!! *)
  | Variant of expr_right            (* !!! diff�rent de blast.ml !!! *)
  | Sets of set list
  | Initialisation of substitution
  | ConstantsConcrete of ids
  | ConstantsAbstract of ids
  | ConstantsHidden of ids
  | Properties of predicate
  | Values of (id * expr_right) list (* !!!diff�rent de blast.ml !!! *)
  | VariablesConcrete of ids
  | VariablesAbstract of ids         (* !!! remplace la clause VARIABLES !!! *)
  | VariablesHidden of ids
  | Promotes of ids
  | Assertions of predicate list
  | Operations of operation list
  | Events of event list
  | Sees of ids
  | Uses of ids
  | Extends of instance list
  | Includes of instance list
  | Imports of instance list
;;


(** d�finition d'une machine B *)
type amn =
(*
    Machine of
      ident
      * sets
      * uses
      * extends
      * includes
      * promotes
      * definitions
      * variables
      * invariant
      * initialisation
      * operations
  | Refinement of
      ident
      * ident
      * sets
      * uses
      * extends
      * includes
      * promotes
      * definitions
      * variables
      * invariant
      * initialisation
      * operations
*)
  | Machine of         (* soit une machine abstraite *)
      head
      * clause list
  | Refinement of      (* soit un raffinement *)
      head
      * id
      * clause list
  | Implementation of  (* derni�re �tape du raffinement *) (* blast.ml *)
      head
      * id
      * clause list
  | EmptyTree          (* blast.ml *)
;;





(** {1 DEFINITION DES FONCTIONS } *)

(** valeur d'un identificateur
@return la cha�ne de caract�res correspondant au nom de cet identificateur *)
val devar : ident -> string

(** union de 2 clauses INVARIANT
@return un invariant contenant la fusion des 2 listes de pr�dicats *)
val unionI : clause -> clause -> clause

(** union de 2 clauses VARIABLES
@return la fusion des 2 listes d'identificateurs *)
val unionV : clause -> clause -> ident list


(** {3 Fonctions d'acc�s aux composants d'une machine B } *)

(** nom d'une machine
@return l'identificateur de la machine *)
val acces_nom : amn -> ident


(** renvoie la liste des ensembles d'une  liste de clauses 
(on suppose que la clause SETS n'appara�t qu'une seule fois) *)
val cl_sets : clause list -> set list

(** renvoie la liste des identificateurs de la clause USES 
(on suppose que la clause USES n'appara�t qu'une seule fois) *)
val cl_uses : clause list -> ids

(** renvoie la liste des instances de la clause EXTENDS 
(on suppose que la clause EXTENDS n'appara�t qu'une seule fois) *)
val cl_extends : clause list -> instance list

(** renvoie la liste des instances de la clause INCLUDES 
(on suppose que la clause INCLUDES n'appara�t qu'une seule fois) *)
val cl_includes : clause list -> instance list

(** renvoie la liste des identificateurs de la clause PROMOTES 
(on suppose que la clause PROMOTES n'appara�t qu'une seule fois) *)
val cl_promotes : clause list -> ids

(** renvoie la liste des d�finitions d'une  liste de clauses 
(on suppose que la clause DEFINITIONS n'appara�t qu'une seule fois) *)
val cl_definitions : clause list -> def list

(** renvoie la liste des identificateurs des variables abstraites 
(on suppose que la clause VARIABLES n'appara�t qu'une seule fois) *)
val cl_variables : clause list -> ids

(** renvoie la liste des invariants d'une  liste de clauses 
(on suppose que la clause INVARIANT n'appara�t qu'une seule fois) *)
val cl_invariant : clause list -> predicate list

(** renvoie la substitution d'initialisation d'une  liste de clauses 
(on suppose que la clause INITIALISATION n'appara�t qu'une seule fois) *)
val cl_initialisation : clause list -> substitution

(** renvoie la liste des op�rations d'une  liste de clauses 
(on suppose que la clause OPERATIONS n'appara�t qu'une seule fois) *)
val cl_operations : clause list -> operation list

(** invariant d'une machine
@return la liste des pr�dicats composant l'invariant de la machine *)
val acces_inv : amn -> predicate list

(** op�rations d'une machine
@return la liste des op�rations de la  machine *)
val acces_opers : amn -> operation list

(** op�rations d'une machine
@return la clause OPERATIONS de la machine B *)
val acceso : amn -> clause

(** nom d'une op�ration
@return la valeur de l'identificateur d'une op�ration *)
val acces_oper_id : operation -> string


(** {3 Fonctions de cr�ation de composants d'une machine B } *)

(** cr�ation d'un identificateur
@param u une cha�ne de caract�res 
@return l'identificateur de nom u *)
val def_ident : string -> ident

(** cr�ation de la clause USES
@param u une liste de noms (cha�nes de caract�res)
@return une clause USES compos�e d'une liste d'identificateurs
        correspondant aux noms de u en majuscules *)
val def_uses : string list -> clause

(** cr�ation de la clause INCLUDES
@param u une liste de noms (cha�nes de caract�res)
@return une clause INCLUDES compos�e d'une liste d'identificateurs
        correspondant aux noms de u en majuscules *)
val def_includes : string list -> clause

(** cr�ation de la clause EXTENDS
@param u une liste de noms (cha�nes de caract�res)
@return une clause EXTENDS compos�e d'une liste d'identificateurs
        correspondant aux noms de u *)
val def_extends : string list -> clause

(** cr�ation de la clause PROMOTES
@param u une liste de noms (cha�nes de caract�res)
@return une clause PROMOTES compos�e d'une liste d'identificateurs
        correspondant aux noms de u *)
val def_promotes : string list -> clause

(** ??? cr�ation d'une clause DEFINITIONS vide (-> utilit� ?) ??? *)
val def_definitions : 'a -> clause

(** cr�ation de la clause VARIABLES
@param u une liste de noms (cha�nes de caract�res)
@return une clause VARIABLES compos�e d'une liste d'identificateurs
        correspondant aux noms de u en minuscules *)
val def_variables : string list -> clause

(** cr�ation de l'INVARIANT
@param u une liste de pr�dicats
@return une clause INVARIANT compos�e de u *)
val def_invariant : predicate list -> clause

(** cr�ation de la substitution d'initialisation
@param u une liste de noms (cha�nes de caract�res)
@return dans la clause INITIALISATION renvoy�e, on affecte en parall�le
        � chaque variable la valeur ensemble vide, une variable �tant
        d�termin�e par l'identificateur d'un nom de la liste u *)
val def_initialisation_empty : string list -> clause

(** cr�ation des OPERATIONS
@param u une liste de noms (cha�nes de caract�res)
@return dans la clause OPERATIONS renvoy�e, chaque op�ration est
        d�sign�e par l'identificateur d'un nom de la liste u
        et est d�finie par une substitution sans effet (SKIP),
        sans pr�-condition, sans param�tre ni valeur de retour *)
val def_operations : string list -> clause

(** cr�ation d'une machine B vide *)
val new_machine : amn

