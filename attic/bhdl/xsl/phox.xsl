<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text" omit-xml-declaration="yes"/>

<xsl:template match="var">
  <xsl:value-of select="@id"/>
</xsl:template>

<xsl:template match="fun">
  <xsl:text>(</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:for-each select="*">
    <xsl:text> </xsl:text>
    <xsl:apply-templates select="."/>
  </xsl:for-each>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="rel">
  <xsl:text>(</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:for-each select="*">
    <xsl:text> </xsl:text>
    <xsl:apply-templates select="."/>
  </xsl:for-each>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="equal">
  <xsl:text>(</xsl:text>
   <xsl:apply-templates select="*[1]"/>
   <xsl:text> = </xsl:text>
   <xsl:apply-templates select="*[2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="true">
 <xsl:text>True</xsl:text>
</xsl:template>

<xsl:template match="false">
 <xsl:text>False</xsl:text>
</xsl:template>

<xsl:template match="not">
 <xsl:text>~ </xsl:text>
 <xsl:apply-templates select="*[1]"/>
 <xsl:text></xsl:text>
</xsl:template>

<xsl:template match="and">
 <xsl:text>(</xsl:text>
 <xsl:apply-templates select="*[1]"/>
 <xsl:text> &amp; </xsl:text>
 <xsl:apply-templates select="*[2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="or">
 <xsl:text>(</xsl:text>
 <xsl:apply-templates select="*[1]"/>
 <xsl:text> or </xsl:text>
 <xsl:apply-templates select="*[2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="if">
 <xsl:text>(</xsl:text>
 <xsl:apply-templates select="*[1]"/>
 <xsl:text> -> </xsl:text>
 <xsl:apply-templates select="*[2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="iff">
 <xsl:text>(</xsl:text>
  <xsl:apply-templates select="*[1]"/>
  <xsl:text> &lt;-&gt; </xsl:text>
  <xsl:apply-templates select="*[2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="forall">
  <xsl:text>/\</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="*[1]"/>
  <xsl:text></xsl:text>
</xsl:template>

<xsl:template match="exists">
  <xsl:text>\/</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="*[1]"/>
  <xsl:text></xsl:text>
</xsl:template>

<xsl:template match="symb">
  <xsl:text>Cst </xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:text> : </xsl:text>
    <xsl:call-template name="for">
      <xsl:with-param name="min">0</xsl:with-param>
      <xsl:with-param name="max">
        <xsl:value-of select="@arity"/>
      </xsl:with-param>
      <xsl:with-param name="body">
        <xsl:text>'a -> </xsl:text>
      </xsl:with-param>
    </xsl:call-template> 
    <xsl:choose>
      <xsl:when test="@type = 'rel'">
        <xsl:text>prop</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>'a</xsl:text>
      </xsl:otherwise>
    </xsl:choose>
  <xsl:text>.</xsl:text>
</xsl:template>

<xsl:template name="for">
  <xsl:param name="min"/>
  <xsl:param name="max"/>
  <xsl:param name="body"/>
  <xsl:choose>
    <xsl:when test="$max > $min">
      <xsl:value-of select="$body"/>
      <xsl:call-template name="for">
        <xsl:with-param name="min">
          <xsl:value-of select="$min+1"/>
        </xsl:with-param>
        <xsl:with-param name="max">
          <xsl:value-of select="$max"/>
        </xsl:with-param>
        <xsl:with-param name="body">
          <xsl:value-of select="$body"/>
        </xsl:with-param>
      </xsl:call-template>
    </xsl:when>
    <xsl:otherwise>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="axiom">
  <xsl:text>claim </xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:text> </xsl:text>
  <xsl:apply-templates select="*[1]"/>
  <xsl:text>.</xsl:text>
</xsl:template>

<xsl:template match="theorem">
 <xsl:text>fact </xsl:text>
 <xsl:value-of select="@id"/>
 <xsl:text> </xsl:text>
 <xsl:apply-templates select="*[1]"/>
 <xsl:text>. </xsl:text>
 <xsl:apply-templates select="*[2]"/>
 <xsl:text>save.</xsl:text>
</xsl:template>

<xsl:template match="theory">
  <xsl:for-each select="*">
    <xsl:text>
</xsl:text>
    <xsl:apply-templates select="."/>
    <xsl:text>
</xsl:text>
  </xsl:for-each>
</xsl:template>

<xsl:template match="trivial">
 <xsl:text>trivial. </xsl:text>
</xsl:template>

</xsl:stylesheet>
