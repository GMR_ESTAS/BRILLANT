<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text" omit-xml-declaration="yes"/>

<xsl:template match="var">
  <xsl:value-of select="@id"/>
</xsl:template>

<xsl:template match="fun">
  <xsl:value-of select="@id"/>
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="position() = 1">
        <xsl:text>\ ( </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="position() != last ()">
        <xsl:text> , </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="position() = last ()">
        <xsl:text> , </xsl:text>
        <xsl:apply-templates select="."/>
        <xsl:text> )</xsl:text>
      </xsl:when>
      <xsl:otherwise>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<xsl:template match="rel">
  <xsl:value-of select="@id"/>
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="position() = 1">
        <xsl:text>\ ( </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="position() != last ()">
        <xsl:text> , </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="position() = last ()">
        <xsl:text> , </xsl:text>
        <xsl:apply-templates select="."/>
        <xsl:text> ) </xsl:text>
      </xsl:when>
      <xsl:otherwise>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<xsl:template match="equal">
  <xsl:text> ( </xsl:text>
   <xsl:apply-templates select="*[1]"/>
   <xsl:text> = </xsl:text>
   <xsl:apply-templates select="*[2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="true">
 <xsl:text>\top </xsl:text>
</xsl:template>

<xsl:template match="false">
 <xsl:text>\bot </xsl:text>
</xsl:template>

<xsl:template match="not">
 <xsl:text>\lnot </xsl:text>
 <xsl:apply-templates select="*[1]"/>
 <xsl:text></xsl:text>
</xsl:template>

<xsl:template match="and">
 <xsl:text>(</xsl:text>
 <xsl:apply-templates select="*[1]"/>
 <xsl:text> \land </xsl:text>
 <xsl:apply-templates select="*[2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="or">
 <xsl:text>(</xsl:text>
 <xsl:apply-templates select="*[1]"/>
 <xsl:text> \lor </xsl:text>
 <xsl:apply-templates select="*[2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="if">
 <xsl:text>(</xsl:text>
 <xsl:apply-templates select="*[1]"/>
 <xsl:text> \to </xsl:text>
 <xsl:apply-templates select="*[2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="iff">
 <xsl:text>(</xsl:text>
  <xsl:apply-templates select="*[1]"/>
  <xsl:text> \leftrightarrow  </xsl:text>
  <xsl:apply-templates select="*[2]"/>
 <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="forall">
  <xsl:text>\forall </xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:text> \ </xsl:text>
  <xsl:apply-templates select="*[1]"/>
  <xsl:text></xsl:text>
</xsl:template>

<xsl:template match="exists">
  <xsl:text>\exist </xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:text> \ </xsl:text>
  <xsl:apply-templates select="*[1]"/>
  <xsl:text></xsl:text>
</xsl:template>

<xsl:template match="axiom">
 <xsl:text>\begin{axiom}</xsl:text>
  <xsl:apply-templates select="*[1]"/>
  <xsl:text>\end{axiom}</xsl:text>
</xsl:template>

<xsl:template match="theorem">
 <xsl:text>\begin{theorem}</xsl:text>
  <xsl:apply-templates select="*[1]"/>
  <xsl:text>\end{theorem}</xsl:text>
  <xsl:text>\begin{proof}</xsl:text>
  <xsl:apply-templates select="*[2]"/>
 <xsl:text>\end{proof}</xsl:text>
</xsl:template>

<xsl:template match="theory">
  <xsl:for-each select="*">
    <xsl:text>
    </xsl:text>
    <xsl:apply-templates select="."/>
    <xsl:text>
    </xsl:text>
  </xsl:for-each>
</xsl:template>

<xsl:template match="trivial">
 <xsl:text></xsl:text>
</xsl:template>

</xsl:stylesheet>
