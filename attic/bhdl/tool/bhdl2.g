
// Antlr grammar for VHDL obtained from "The University of Cincinnati"
//  grammar (written in PCCTS)
// Some modifications have been done :
// - The rule "options" has been renamed => reserved word
// - May 13, 2003 : A part of the lexer (written in flex) has been put
//   in the parser part.
//   For instance, some of the lexical rules ("decimal_integer_literal",
//   "exp", "decimal_floating_point_literal", "based_floating_point_literal"
//   and "real_literal", ...) have been translated in the parser (see
//   BASED_LITERAL, BASED_INTEGER,EXP, DECIMAL_INTEGER_LITERAL and
//   BASED_INTEGER_LITERAL, abstract_literal and integer_literal.)
// - May 19, 2003 : The grammar is associated with a treewalker (treewalker.g).
//   It is the reason why some rules have been modified, especially in the
//   case of recursive structures based on COMMA, that is,by adding an
//   intermediate symbol.
//  May 19, 2003, Ammar Aljer, Philippe Devienne, Pierre Antoine Laloux
//   July 26, 2003 The parser has been modified to accept VHDL code generated
//                    by VGUI
//                  Empty units are removed, useful VHDL comments which are
//                    useful in B
//                  They are captured in order to form INVARIANT clause and
//                    DEFINITION clause in B
//  07,07,2003 Ammar Aljer, all the ?s are compared with the original version
//  04/18/2007 Modify Leyman KOMBATE NENE

// Copyright (c) 1993-2001 The University of Cincinnati.
// All rights reserved.

// UC MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
// THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE, OR NON-INFRINGEMENT.  UC SHALL NOT BE LIABLE FOR ANY DAMAGES
// SUFFERED BY LICENSEE AS A RESULT OF USING, RESULT OF USING, MODIFYING
// OR DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.

// By using or copying this Software, Licensee agrees to abide by the
// intellectual property laws, and all other applicable laws of the
// U.S., and the terms of this license.

// You may modify, distribute, and use the software contained in this
// package under the terms of the "GNU LIBRARY GENERAL PUBLIC LICENSE"
// version 2, June 1991. A copy of this license agreement can be found
// in the file "LGPL", distributed with this archive.

// Authors: Philip A. Wilsey    philip.wilsey@ieee.org
//          Dale E. Martin      dmartin@cliftonlabs.com
//          Timothy J. McBrayer
//          Malolan Chetlur     mal@ece.uc.edu

//---------------------------------------------------------------------------
//
// $Id$
//
//---------------------------------------------------------------------------

{
import java.io.*;
import antlr.collections.AST;

}

class bhdlParser extends Parser ;

options {
    exportVocab=vhdl;               // Call its vocabulary "vhdl"
    defaultErrorHandler = true ;
    buildAST=true;

    k=2;

         }


tokens
{
TOPLEVEL      ="top_level";
ABSTRACT      = "abstract";
CONCRETE      = "concrete";
ACTIVE        = "active" ;
ASCENDING     = "ascending" ;
BASE          = "base" ;
DELAYED       = "delayed" ;
DRIVING       = "driving" ;
DRIVING_VALUE = "driving_value" ;
EVENT         = "event" ;
HIGH          = "high" ;
//These tokens (IDENT...) are used to abduct the non determinism
// due to recursive sentences
IDENT         = "logical_library_name_list_root";
IDENT1        = "aggregate_root";
IDENT2        = "selected_waveforms_root";
IDENT3        = "sensitivity_list_root";
IDENT4        = "sequential_waveform_root";
IDENT5        = "entity_name_list_root";
IDENT6        = "instantiation_list_root";
IDENT7        = "signal_list_root";
IDENT8        = "enumeration_type_definition_root";
IDENT9        = "array_type_definition_head_root";
IDENT10       = "index_constraint_root";
IDENT11       = "identifier_list_root";
IDENT12       = "association_list_in_root";
IDENT13       = "entity_class_entry_list_root";
IDENT14       = "group_constituent_list_root";
IDENT15       = "association_list_out_root";
IDENT16       = "step_limit_specification_head_root";
IDENT17       = "array_nature_definition_head_root";
IDENT18       = "signal_assignment_st";
IDENT19       = "wave_element";
IDENT20       = "se_st";
IDENT21       = "label1";
IDENT22       = "colon1";
IDENT23       = "costat2b";
IDENT24       = "instantiate_stmt";
IDENT25       = "newcomp";
IDENT26       = "enum_list_root";

IMAGE         = "image" ;
INSTANCE_NAME = "instance_name" ;
LAST_ACTIVE   = "last_active" ;
LAST_EVENT    = "last_event" ;
LAST_VALUE    = "last_value" ;
LEFT          = "left" ;
LEFTOF        = "leftof" ;
LENGTH        = "length" ;
LOW           = "low" ;
PATH_NAME     = "path_name" ;
POS           = "pos" ;
PRED          = "pred" ;
QUIET         = "quiet " ;
REVERSE_RANGE = "reverse_range" ;
RIGHT         = "right" ;
RIGHTOF       = "rightof" ;
SIMPLE_NAME   = "simple_name" ;
STABLE        = "stable" ;
SUCC          = "succ" ;
TRANSACTION   = "transaction " ;
VAL           = "val" ;
VALUE         = "value" ;
ABS           = "abs" ;
ACCESS        = "access" ;
ACROSS        = "across" ;
AFTER         = "after" ;
ALIAS         = "alias" ;
ALL           = "all" ;
AND           = "and" ;
ARCHITECTURE  = "architecture" ;
ARRAY         = "array" ;
ASSERT        = "assert" ;
ATTRIBUTE     = "attribute" ;
BEGIN         = "begin" ;
BLOCK         = "block" ;
BODY          = "body" ;
BREAK         = "break" ;
BUFFER        = "buffer" ;
BUS           = "bus" ;
CASE          = "case" ;
COMPONENT     = "component" ;
CONFIGURATION = "configuration" ;
CONSTANT      = "constant" ;
DISCONNECT    = "disconnect" ;
DOWNTO        = "downto" ;
ELSE          = "else" ;
ELSIF         = "elsif" ;
END           = "end" ;
ENTITY        = "entity" ;
EXIT          = "exit" ;
FILE          = "file" ;
FOR           = "for" ;
FUNCTION      = "function" ;
GENERATE      = "generate" ;
GENERIC       = "generic" ;
GROUP         = "group" ;
GUARDED       = "guarded" ;
IF            = "if" ;
IMPURE        = "impure" ;
IN            = "in" ;
INERTIAL      = "inertial" ;
INOUT         = "inout" ;
IS            = "is" ;
LABEL         = "label" ;
LIBRARY       = "library" ;
LIMIT         = "limit " ;
LINKAGE       = "linkage" ;
LITERAL       = "literal" ;
LOOP          = "loop" ;
MAP           = "map" ;
MOD           = "mod" ;
NAND          = "nand" ;
NATURE        = "nature " ;
NEW           = "new" ;
NEXT          = "next" ;
NOISE         = "noise " ;
NOR           = "nor" ;
NOT           = "not" ;
NULL          = "null" ;
OF            = "of" ;
ON            = "on" ;
OPEN          = "open" ;
OR            = "or" ;
OTHERS        = "others" ;
OUT           = "out" ;
PACKAGE       = "package" ;
PORT          = "port" ;
POSTPONED     = "postponed" ;
PROCEDURAL    = "procedural" ;
PROCEDURE     = "procedure" ;
PROCESS       = "process" ;
PROTECTED     = "protected" ;
PURE          = "pure" ;
QUANTITY      = "quantity" ;
RANGE         = "range" ;
RECORD        = "record" ;
REFERENCE     = "reference" ;
REGISTER      = "register" ;
REJECT        = "reject" ;
REM           = "rem" ;
REPORT        = "report" ;
RETURN        = "return" ;
ROL           = "rol" ;
ROR           = "ror" ;
SELECT        = "select" ;
SEVERITY      = "severity" ;
SHARED        = "shared" ;
SIGNAL        = "signal" ;
SLA           = "sla" ;
SLL           = "sll" ;
SPECTRUM      = "spectrum" ;
SRA           = "sra" ;
SRL           = "srl" ;
SUBNATURE     = "subnature" ;
SUBTYPE       = "subtype" ;
TERMINAL      = "terminal" ;
THEN          = "then" ;
THROUGH       = "through" ;
TO            = "to" ;
TOLERANCE     = "tolerance" ;
TRANSPORT     = "transport" ;
TYPE          = "type" ;
UNAFFECTED    = "unaffected" ;
UNITS         = "units" ;
UNTIL         = "until" ;
USE           = "use" ;
VARIABLE      = "variable" ;
WAIT          = "wait" ;
WHEN          = "when" ;
WHILE         = "while" ;
WITH          = "with" ;
XNOR          = "xnor" ;
XOR           = "xor" ;

// CHOICE        = "choice" ;

}
{
// This variable is used to memorize the entity of an architecture in order
// to transfer the invariant comment and definition comment between both of
// them using a tree comment
String Entity_of_Arc="";

//A tree that containes all of B comments
AST comments = null;
AST enumerate = null;
AST enumerationThree = null;
AST ab = null;
AST a = null;
// This variable is used to concat many lines of comments in only one string
String commentcollect = "";
String enumcollect = "";
}

design_file! :
                {  // This branch create the root of VHDL comments
                   // which are useful in B.
		    #comments=#([IDENTIFIER,"comments"]);
		    #enumerate=#([IDENTIFIER,"enumerate"]);
                }
	(ben :benum_deff)? 
        df:design_file1
                {
		#enumerate.addChild(#ben);
		#enumerate.setNextSibling(#df);
		#comments.setNextSibling(#enumerate);
                #design_file = #comments;
                }
    ;
design_file1 :
      ( design_unit )+ EOF!
    ;
design_unit :
      context_clauses library_unit
    ;
library_unit :
      primary_unit
    | secondary_unit
    ;
primary_unit :
      entity_declaration
    | configuration_declaration
    | package_declaration
    ;
secondary_unit :
      architecture_body
    | package_body
    ;
library_clause :
      LIBRARY^ logical_library_name_list SEMI_COLON
    ;
logical_library_name_list !:
      ln1:logical_name ln2:logical_library_name_list_tail
                {#logical_library_name_list = #([IDENT
                                                , "logical_library_name_list_root"]
                                                , ln1
                                                , ln2);}
    ;
logical_library_name_list_tail :
    ( COMMA logical_name )*
    ;
logical_name :
      identifier
    ;
context_clauses :
      ( context_item )*
    ;
context_item :
      library_clause
    | use_clause
    ;
entity_declaration :
// to ignore empty entities which are generated by VGUI
     !(ENTITY (identifier) IS END)
        => ENTITY (identifier|TOPLEVEL) IS END (identifier|TOPLEVEL) SEMI_COLON
    | (ENTITY identifier IS entity_header)
        => ENTITY^ identifier IS e:entity_header entity_declarative_part
          ( entity_statement_part )? END  ( ENTITY )? ( simple_name )? SEMI_COLON
    ;
entity_header :
      ( generic_clause )? ( port_clause )?
    ;
entity_declarative_part :
      ( entity_declarative_item )*
    ;
entity_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | signal_declaration
    | shared_variable_declaration
    | file_declaration
    | alias_declaration
    | attribute_declarative_item
    | disconnection_specification
    | use_clause
    | ( LIMIT | NATURE | SUBNATURE | QUANTITY | TERMINAL )
          => ( step_limit_specification
               | nature_declaration
               | subnature_declaration
               | quantity_declaration
               | terminal_declaration
             )
    | ( GROUP IDENTIFIER COLON )
         => group_declaration
    | group_template_declaration
    ;
entity_statement_part :
      ( BEGIN )? ( entity_statement )*
    ;
entity_statement :
      ( stmt_label )? ( POSTPONED )? entity_stmt
    ;
entity_stmt :
      concurrent_assertion_statement
    | concurrent_procedure_call
    | process_statement
    ;
concurrent_procedure_call :
      complex_name SEMI_COLON
    ;
architecture_body :
// to ignore empty architectures which are generated by VGUI.
// and to ignore top_level architectures generated by VGUI.
      ! (ARCHITECTURE identifier OF TOPLEVEL)
            =>  ( ARCHITECTURE identifier OF TOPLEVEL IS
                  architecture_declarative_part
                  architecture_statement_part END ( ARCHITECTURE )?
                  ( simple_name )? SEMI_COLON
                )
    | ! ( ARCHITECTURE^ identifier OF selected_or_simple_name IS
           BEGIN END  simple_name SEMI_COLON
          )
            =>  ( ARCHITECTURE identifier OF selected_or_simple_name
                  IS BEGIN END   simple_name  SEMI_COLON
                )
    | (ARCHITECTURE identifier OF selected_or_simple_name)
         =>  ( ARCHITECTURE^ identifier OF son:selected_or_simple_name
               IS architecture_declarative_part
                 architecture_statement_part END ( ARCHITECTURE )?
                 ( simple_name )? SEMI_COLON
             )
    ;
architecture_declarative_part :
      ( block_declarative_item )*
    ;
architecture_statement_part :
      ( BEGIN )? ( architecture_statement_element )*
    ;
architecture_statement_element :
      ( CASE | NULL | PROCEDURAL | IF condition USE | simple_expression EQUAL_EQUAL )
          => simultaneous_statement
    | concurrent_statement
    ;
configuration_declaration :
       CONFIGURATION^ identifier OF selected_or_simple_name IS
       configuration_declarative_part  block_configuration END
       ( CONFIGURATION )? ( simple_name )? SEMI_COLON
    ;
configuration_declarative_part :
      ( configuration_declarative_item )*
    ;
configuration_declarative_item :
      use_clause
    | attribute_specification
    | group_declaration
    ;
block_configuration :
      FOR^ block_specification ( use_clause )* ( configuration_item )* END FOR SEMI_COLON
    ;
block_specification :
      selected_or_simple_name ( L_PAREN index_specifier R_PAREN )?
    ;
configuration_item :
      ( component_configuration )
          => ( component_configuration )
    | block_configuration
    ;
component_configuration :
      FOR^ component_specification ( binding_indication SEMI_COLON )?
      ( block_configuration )? END FOR SEMI_COLON
    ;

concurrent_statement! :
                     {#concurrent_statement=#([IDENT21,"label1"]);}
      ( st:stmt_label {#concurrent_statement.addChild(#st); })?

      (co: COLON {#concurrent_statement.addChild(#co);})?

       cs:concurrent_stmt {#concurrent_statement.setNextSibling(#cs);}
       // {#concurrent_statement.addChild(#cs);}
    ;

concurrent_stmt :
       ( BLOCK | FOR | IF | BREAK | CASE | NULL | PROCEDURAL |
         simple_expression EQUAL_EQUAL
        )
            => not_postponeable
    |!  ( po:POSTPONED! )? pos:postponeable
         {#concurrent_stmt=#([IDENT23,"costat2b"],pos);}
    ;

not_postponeable :
      block_statement
    | ( generate_scheme GENERATE )
          => generate_statement
    | ( IF | CASE | NULL | PROCEDURAL | simple_expression EQUAL_EQUAL )
          => simultaneous_stmt
    | concurrent_break_statement
    ;

postponeable :
      process_statement
    | concurrent_assertion_statement
    | selected_signal_assignment_statement
    | ( target LESS_EQUAL optionss conditional_waveforms SEMI_COLON)
        => conditional_signal_assignment
    //commentsr is a clause of B comments
    | ( COMPONENT | ENTITY | CONFIGURATION | cn:complex_name c:commentsr!
      ( generic_map_aspect | port_map_aspect ) )
          =>  instantiate_statement
    | concurrent_procedure_call
    ;
 commentsr :
      ( bcomment_defs bcomment_e bcomment_invs )? //benum_def ?
    ;
bcomment_defs :
    BCOMMENT_DEF! L_PAREN! identifier_list1 R_PAREN!
;

bcomment_invs :
     BCOMMENT_INV! L_PAREN! identifier_list1 R_PAREN!
;

bcomment_e! :
            {
                    commentcollect="";
            }
    ( bd:BCOMMENT_E {commentcollect=commentcollect.concat(bd.getText()+"\r\n");} )+
            {
                    #bcomment_e= #([BCOMMENT_E,commentcollect]);
            }
    ;

benum_deff! :

	{AST ab = null;
	AST a = null;}
	(TYPE! i:chiffre_nombre IS! l:enumeration_type_define {#ab=#(i,l);#benum_deff = #ab;})
	(TYPE! m:chiffre_nombre IS! n:enumeration_type_define {#a=#(m,n);#ab.setNextSibling(#a);#ab=#a;})*;

block_statement :
      BLOCK^ ( L_PAREN expression R_PAREN )? ( IS )? block_header block_declarative_part
      architecture_statement_part END BLOCK ( simple_name )? SEMI_COLON
    ;

block_header :
      ( generic_clause ( generic_map_aspect SEMI_COLON )? )?
      ( port_clause ( port_map_aspect SEMI_COLON )? )?

    ;

block_declarative_part :
      ( block_declarative_item )*
    ;

block_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | signal_declaration
    | shared_variable_declaration
    | file_declaration
    | alias_declaration
    | component_declaration
    | disconnection_specification
    | attribute_declarative_item
    | configuration_specification
    | use_clause
    | ( LIMIT | NATURE | SUBNATURE | QUANTITY | TERMINAL )
            => ( step_limit_specification
                   | nature_declaration
                   | subnature_declaration
                   | quantity_declaration
                   | terminal_declaration
               )
    | ( GROUP IDENTIFIER COLON )
          => group_declaration
    | group_template_declaration
    ;

process_statement :
      PROCESS^ ( L_PAREN sensitivity_list R_PAREN )? ( IS )? process_declarative_part
      sequence_of_statements END ( POSTPONED )? PROCESS ( simple_name )? SEMI_COLON
    ;

process_declarative_part :
      ( process_declarative_item )*
    ;

process_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | variable_declaration
    | file_declaration
    | alias_declaration
    | attribute_declarative_item
    | use_clause
    | ( GROUP IDENTIFIER COLON ) => group_declaration
    | group_template_declaration
    ;

concurrent_assertion_statement :
      assertion SEMI_COLON
    ;

concurrent_call_statement :
      complex_name SEMI_COLON
    ;

instantiate_statement! :
              {#instantiate_statement=#([IDENT24,"instantiate_stmt"]);}
      (    en:ENTITY
             {#instantiate_statement.addChild(#en);}
          |co:CONFIGURATION
             {#instantiate_statement.addChild(#co);}
          |( COMPONENT )?
       )

       cn:complex_name
         {#instantiate_statement.addChild(#cn);}
       c:commentsr
      {
          AST current_Comment_Tree=null;
          String current_archi=#cn.getText();
          if (#c!=null)
          {
                #current_Comment_Tree=#([IDENTIFIER,current_archi],c);
                #comments.addChild(#current_Comment_Tree);
          }
      }
      ( gma:generic_map_aspect
         {#instantiate_statement.addChild(#gma);}
       )?
       (
       pma:port_map_aspect
           {#instantiate_statement.addChild(#pma);}
       )?
       sc:SEMI_COLON
        {#instantiate_statement.addChild(#sc);}

//    | ENTITY^ complex_name ( generic_map_aspect )? ( port_map_aspect )? SEMI_COLON
//    | CONFIGURATION^ complex_name ( generic_map_aspect )? ( port_map_aspect )? SEMI_COLON
// The same treatement for  all branchs
    | ENTITY complex_name ( generic_map_aspect )? ( port_map_aspect )? SEMI_COLON
    | CONFIGURATION complex_name ( generic_map_aspect )? ( port_map_aspect )? SEMI_COLON
    ;

conditional_signal_assignment :
      target LESS_EQUAL optionss conditional_waveforms SEMI_COLON
    ;

target :
      ( complex_name ) => ( complex_name )
    | aggregate
    ;

aggregate !:
      lp:L_PAREN ae:aggregate_entry at:aggregate_tail rp:R_PAREN
      {#aggregate = #([IDENT1, "aggregate_root"], lp, ae, at, rp);}
    ;

aggregate_tail :
      ( COMMA aggregate_entry )*
    ;

aggregate_entry :
      choices ( EQUAL_GREATER expression )?
    ;

conditional_waveforms :
      concurrent_waveform ( WHEN condition ( ELSE conditional_waveforms )? )?
    ;

selected_signal_assignment_statement :
      WITH expression SELECT target LESS_EQUAL optionss selected_waveforms SEMI_COLON
    ;

//modif3
selected_waveforms !:
      swh:selected_waveforms_head swt:selected_waveforms_tail
      {#selected_waveforms = #([IDENT2, "selected_waveforms_root"], swh, swt);}
    ;

//modif3
selected_waveforms_head :
      concurrent_waveform WHEN choices
    ;

selected_waveforms_tail :
      ( COMMA selected_waveforms_head )*
    ;

optionss :
      ( GUARDED )? ( delay_mechanism )?
    ;

delay_mechanism :
      TRANSPORT
    | ( REJECT time_expression )? INERTIAL
    ;

generate_statement :
      generate_scheme GENERATE^ ( ( block_declarative_part BEGIN )
         => block_declarative_part BEGIN )? architecture_statement_part
            END GENERATE ( simple_name )? SEMI_COLON
    ;

generate_scheme :
      FOR^ identifier IN discrete_range
    | IF condition
    ;

sequence_of_statements :
      ( BEGIN )? ( sequential_statement )*
    ;

sequential_statement !:
        {AST sl=#([IDENTIFIER,"nolabel"]);}
        ( sl:stmt_label )? ss:sequential_stmt sc:SEMI_COLON
       { #sequential_statement=#([IDENT20,"se_st"],sl,ss,sc);}
    ;

sequential_stmt :
      wait_statement
    | assertion_statement
    | report_statement
    | ( NULL ) => null_statement
    | ( target LESS_EQUAL ) => sa:signal_assignment_statement
    | ( target COLON_EQUAL ) => variable_assignment_statement
    | ( complex_name SEMI_COLON ) => procedure_call_statement
    | if_statement
    | case_statement
    | loop_statement
    | next_statement
    | exit_statement
    | return_statement
    | break_statement
    ;

stmt_label :
      vhdl_label
    ;

wait_statement :
      WAIT ( sensitivity_clause )? ( condition_clause )? ( timeout_clause )?
    ;

sensitivity_clause :
      ON sensitivity_list
    ;

sensitivity_list !:
      cn:complex_name slt:sensitivity_list_tail
      {#sensitivity_list = #([IDENT3, "sensitivity_list_root"], cn, slt);}
    ;

sensitivity_list_tail :
      ( COMMA complex_name )*
    ;

condition_clause :
      UNTIL condition
    ;

condition :
      boolean_expression
    ;

timeout_clause :
      FOR time_expression
    ;

assertion_statement :
      assertion
    ;

assertion :
      ASSERT condition ( REPORT expression )? ( SEVERITY expression )?
    ;

report_statement :
      REPORT expression ( SEVERITY expression )?
    ;

null_statement :
      NULL
    ;

signal_assignment_statement ! :
      ta:target le:LESS_EQUAL ss:sequential_signal_assign_stmt
     {#signal_assignment_statement=#([IDENT18, "sequential_signal_assign_st"],ta,le,ss);}
    ;


variable_assignment_statement :
      target COLON_EQUAL^ variable_assign_stmt
    ;

procedure_call_statement :
      complex_name
    ;

sequential_signal_assign_stmt :
      ( delay_mechanism )? sequential_waveform
    ;

variable_assign_stmt :
      expression
    ;

sequential_waveform !:
      we:waveform_element swt:sequential_waveform_tail
            {
                #sequential_waveform = #([IDENT4, "sequential_waveform_root"], we, swt);
            }
    ;

sequential_waveform_tail :
      ( COMMA waveform_element )*
    ;

concurrent_waveform :
      sequential_waveform
    | UNAFFECTED
    ;

waveform_element !:
      exp1:expression ( af:AFTER exp2:expression )?
     {#waveform_element=#([IDENT19,"wave_element"],exp1,af,exp2);}
    ;

if_statement :
      IF condition THEN sequence_of_statements ( ( ELSIF ) => elsif_stmt )?
      ( ELSE sequence_of_statements )? END IF ( simple_name )?
    ;

elsif_stmt :
      ELSIF condition THEN sequence_of_statements ( ( ELSIF ) => elsif_stmt )?
    ;

case_statement :
      CASE expression IS ( case_statement_alternative )+ END CASE ( simple_name )?
    ;

case_statement_alternative :
      WHEN choices EQUAL_GREATER sequence_of_statements
    ;

choices :
      choice (CHOICE1 choice )*
    | OTHERS
    ;

choice :
      expression ( direction simple_expression )?
    ;

loop_statement :
      ( FOR ) => for_loop_statement
    | while_loop_statement
    ;

for_loop_statement :
      FOR identifier IN discrete_range LOOP sequence_of_statements END LOOP ( simple_name )?
    ;

while_loop_statement :
      ( WHILE condition )? LOOP sequence_of_statements END LOOP ( simple_name )?
    ;

next_statement :
      NEXT ( vhdl_label )? ( WHEN condition )?
    ;

exit_statement :
      EXIT ( vhdl_label )? ( WHEN condition )?
    ;

return_statement :
      RETURN ( expression )?
    ;

package_declaration :
      PACKAGE^ identifier IS package_declarative_part END
     ( PACKAGE )? ( simple_name )? SEMI_COLON
    ;

package_declarative_part :
      ( package_declarative_item )*
    ;

package_declarative_item :
      subprogram_declaration
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | signal_declaration
    | shared_variable_declaration
    | file_declaration
    | alias_declaration
    | component_declaration
    | attribute_declarative_item
    | disconnection_specification
    | use_clause
    | ( NATURE | SUBNATURE | TERMINAL )
          => ( nature_declaration | subnature_declaration | terminal_declaration )
    | ( GROUP IDENTIFIER COLON )
          => group_declaration
    | group_template_declaration
    ;

package_body :
      PACKAGE BODY^ identifier IS package_body_declarative_part
      END ( PACKAGE BODY )? ( simple_name )? SEMI_COLON
    ;

package_body_declarative_part :
      ( package_body_declarative_item )*
    ;

package_body_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | shared_variable_declaration
    | file_declaration
    | alias_declaration
    | use_clause
    | ( GROUP IDENTIFIER COLON )
          => group_declaration
    | group_template_declaration
    ;

subprogram :
      subprogram_header ( subprogram_body )? SEMI_COLON
    ;

subprogram_declaration :
      subprogram_header SEMI_COLON
    ;

subprogram_header :
      PROCEDURE^ designator ( L_PAREN formal_parameter_list R_PAREN )?
    | ( side_effects )? FUNCTION^ designator
      ( L_PAREN formal_parameter_list R_PAREN )? RETURN type_mark
    ;

side_effects :
      PURE
    | IMPURE
    ;

designator :
      operator_symbol
    | identifier
    ;

operator_symbol :
      STRING_LITERAL
    ;

formal_parameter_list :
      interface_list
    ;

subprogram_body :
      IS subprogram_declarative_part sequence_of_statements END
       ( FUNCTION | PROCEDURE )? ( designator )?
    ;

subprogram_declarative_part :
      ( subprogram_declarative_item )*
    ;

subprogram_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | variable_declaration
    | file_declaration
    | alias_declaration
    | attribute_declarative_item
    | use_clause
    ;

signature :
//      L_BRACKET ( type_mark ( COMMA type_mark )* )? ( RETURN type_mark )? R_BRACKET
      L_BRACKET ( type_mark ( COMMA type_mark )* )? ( RETURN type_mark )? R_BRACKET
    ;

attribute_declarative_item :
      ATTRIBUTE^ ( attribute_declaration_tail | attribute_specification_tail )
    ;

attribute_declaration_tail :
      identifier COLON^ type_mark SEMI_COLON
    ;

attribute_specification_tail :
      identifier OF entity_specification IS expression SEMI_COLON
    ;

attribute_specification :
      ATTRIBUTE^ attribute_specification_tail
    ;

entity_specification :
      entity_name_list COLON entity_class
    ;

entity_name_list :!
      enla:entity_name_list_head enlt:entity_name_list_tail
      {#entity_name_list = #([IDENT5, "entity_name_list_root"], enla, enlt);}
    | OTHERS
    | ALL
    ;

entity_name_list_head:
      entity_designator ( signature )?
    ;

entity_name_list_tail :
      ( COMMA entity_name_list_head)*
    ;

entity_designator :
      simple_name
    | operator_symbol
    ;

entity_class :
      ENTITY
    | ARCHITECTURE
    | CONFIGURATION
    | PROCEDURE
    | FUNCTION
    | PACKAGE
    | TYPE
    | SUBTYPE
    | CONSTANT
    | SIGNAL
    | VARIABLE
    | COMPONENT
    | LABEL
    | LITERAL
    | UNITS
    | GROUP
    | FILE
    | ( NATURE | SUBNATURE | QUANTITY | TERMINAL )
    ;

configuration_specification :
      FOR^ component_specification binding_indication SEMI_COLON
    ;

component_specification :
      instantiation_list COLON^ complex_name
    ;

instantiation_list :!
      sn:simple_name ilt:instantiation_list_tail
            {
                #instantiation_list = #([IDENT6, "instantiation_list_root"], sn, ilt);
            }
    | OTHERS
    | ALL
    ;

instantiation_list_tail :
      ( COMMA simple_name )*
    ;

binding_indication :
      ( USE entity_aspect )? ( generic_map_aspect )? ( port_map_aspect )?
    ;

entity_aspect :
      ENTITY^ selected_or_simple_name ( L_PAREN simple_name R_PAREN )?
    | CONFIGURATION^ complex_name
    | OPEN
    ;

generic_map_aspect :
      GENERIC^ MAP L_PAREN association_list_in R_PAREN
    ;

port_map_aspect :
      PORT^ MAP L_PAREN association_list_in R_PAREN
    ;

disconnection_specification :
      DISCONNECT guarded_signal_specification AFTER expression SEMI_COLON
    ;

guarded_signal_specification :
      signal_list COLON^ type_mark
    ;

signal_list :!
      cn:complex_name slt:signal_list_tail
            {
                #signal_list = #([IDENT7, "signal_list_root"], cn, slt);
            }
    | OTHERS
    | ALL
    ;

signal_list_tail :
      ( COMMA complex_name )*
    ;

use_clause :
      USE^ complex_name ( COMMA selected_name )* SEMI_COLON
    ;

scalar_type_definition :
      enumeration_type_definition
    | range_constraint ( physical_type_definition )?
    ;



enumeration_type_define :
	lp:L_PAREN! el:enumeration_literal etdt:enumeration_type_definition_tail rp:R_PAREN!
	    ;

enumeration_type_definition !:
      lp:L_PAREN el:enumeration_literal  etdt:enumeration_type_definition_tail rp:R_PAREN
            {
                #enumeration_type_definition =
                    #([IDENT8, "enumeration_type_definition_root"], lp, el, etdt, rp);
            };

enumeration_type_definition_tail :
      ( COMMA! enumeration_literal )*
    ;

enumeration_literal :
      identifier
    | CHARACTER_LITERAL
    | chiffre_nombre
    ;

range_constraint :
      RANGE^ range
    ;

range :
      ( ( selected_name QUOTE ( attribute_range | attribute_reverse_range ) ) )
            => ( ( selected_name QUOTE ( attribute_range | attribute_reverse_range ) ) )
    | simple_expression direction simple_expression
    ;

physical_type_definition :
      UNITS base_unit_declaration ( secondary_unit_declaration )* END UNITS ( simple_name )?
    ;

base_unit_declaration :
      identifier SEMI_COLON
    ;

secondary_unit_declaration :
      identifier EQUAL physical_literal SEMI_COLON
    ;

physical_literal :
      ( integer_literal )? simple_name
    ;

abstract_literal :
      integer_literal
    ;

composite_type_definition :
      array_type_definition
    | record_type_definition
    ;

//modif2
array_type_definition :
      ARRAY^ array_type_definition_head OF subtype_indication
    ;

//modif2
array_type_definition_head !:
      lp:L_PAREN ad:array_dimension atdat:array_type_definition_head_tail rp:R_PAREN
            {
                #array_type_definition_head =
                    #([IDENT9, "array_type_definition_head_root"], lp, ad, atdat, rp);
            }
    ;

array_type_definition_head_tail :
      ( COMMA array_dimension )*
    ;

array_dimension :
      ( index_subtype_definition )
        => ( index_subtype_definition )
    | ( discrete_range )
    ;

index_subtype_definition :
      type_mark RANGE LESS_GREATER
    ;

record_type_definition :
      RECORD^ ( element_declaration )+ END RECORD ( simple_name )?
    ;

element_declaration :
      identifier_list COLON^ subtype_indication SEMI_COLON
    ;

access_type_definition :
      ACCESS subtype_indication
    ;

file_type_definition :
      FILE OF type_mark
    ;

type_declaration :
      TYPE^ identifier ( IS type_definition )? SEMI_COLON
    ;

type_definition :
      scalar_type_definition
    | composite_type_definition
    | access_type_definition
    | file_type_definition
    | ( PROTECTED BODY )
        => protected_type_body
    | protected_type_declaration
    ;

subtype_declaration :
      SUBTYPE^ identifier IS subtype_indication SEMI_COLON
    ;

subtype_indication :
         ( ( complex_name type_mark )
               => complex_name type_mark ( constraint )?
         | type_mark ( constraint )? ) ( ( TOLERANCE )
               => TOLERANCE expression )?
   ;

selected_or_simple_name :
      simple_name ( DOT simple_name )*
    ;

type_mark :
      selected_or_simple_name
    ;

constraint :
      range_constraint
    | index_constraint
    ;

index_constraint !:
      lp:L_PAREN dr:discrete_range ict:index_constraint_tail rp:R_PAREN
      {#index_constraint = #([IDENT10, "index_constraint_root"], lp, dr, ict, rp);}
    ;

index_constraint_tail :
      ( COMMA discrete_range )*
    ;

constant_declaration :
      CONSTANT^ identifier_list COLON subtype_indication ( initialization )? SEMI_COLON
    ;

identifier_list !:
      i:identifier ilt:identifier_list_tail
      {#identifier_list = #([IDENT11, "identifier_list_root"], i, ilt);}
    ;

identifier_list1 !:
       i:identifier ilt:identifier_list_tail
       {#identifier_list1 = #([IDENT11, "identifier_list_root"], i, ilt);}
     ;

     identifier_liste !:
       i:IDENTIFIER {#identifier_liste = #([IDENT11, "identifier_list_root"], i);}
        (id:IDENTIFIER {System.out.println(id.getText()); #identifier_liste.addChild(#id); } )*
;


identifier_list2 !:
        i:IDENTIFIER {#identifier_list2 = #([IDENT26, "enum_list_root"], i);}
        (COMMA id:IDENTIFIER {System.out.println(id.getText());#identifier_list2.addChild(#id); } )*
;
identifier_list_tail :
      ( COMMA identifier )*
    ;

initialization :
      COLON_EQUAL expression
    ;

signal_declaration :
      SIGNAL^ identifier_list COLON subtype_indication ( signal_kind )?
        ( initialization )? SEMI_COLON
    ;

signal_kind :
      REGISTER
    | BUS
    ;

shared_variable_declaration :
      SHARED VARIABLE variable_declaration_body
    ;

variable_declaration :
      ( SHARED )? VARIABLE variable_declaration_body
    ;

variable_declaration_body :
      identifier_list COLON^ subtype_indication ( initialization )? SEMI_COLON
    ;

file_declaration :
      FILE^ identifier_list COLON subtype_indication ( file_open_information )? SEMI_COLON
    ;

file_open_information :
      ( OPEN expression )? IS file_logical_name
    ;

mode :
      IN
    | OUT
    | INOUT
    | BUFFER
    | LINKAGE
    ;

file_logical_name :
      expression
    ;

interface_declaration :
//      (   ( ( CONSTANT | SIGNAL | VARIABLE ) )?
//           identifier_list COLON^ ( mode )? subtype_indication
//                ( BUS )? ( initialization )?
      (   ( ( CONSTANT | SIGNAL | VARIABLE ) )? identifier_list
              COLON ( mode )? subtype_indication ( BUS )?
            (   initialization )?
        | FILE identifier_list COLON subtype_indication
        | ( TERMINAL identifier_list COLON subnature_indication
            | QUANTITY identifier_list COLON ( IN | OUT )?
              subtype_indication ( initialization )?
          )
      )
    ;

interface_list :
      interface_declaration ( SEMI_COLON interface_declaration )*
    ;

association_list_in !:
      a:association alit:association_list_in_tail
            {
                #association_list_in = #([IDENT12, "association_list_in_root"], a, alit);
            }
    ;

association_list_in_tail :
      ( COMMA association )*
    ;

association :
      association_element ( EQUAL_GREATER association_element )?
    ;

association_element :
      expression
    | OPEN
    ;

alias_declaration :
      ALIAS^ alias_designator ( COLON alias_indication )?
       IS complex_name ( signature )? SEMI_COLON
    ;

alias_designator :
      identifier
    | CHARACTER_LITERAL
    | operator_symbol
    ;

alias_indication :
      ( type_mark TOLERANCE )
        => subnature_indication
    | subtype_indication
    ;

component_declaration :
      COMPONENT^ identifier ( IS )? ( generic_clause )? ( port_clause )? END COMPONENT
      ( simple_name )? SEMI_COLON
    ;

generic_clause :
      GENERIC^ L_PAREN generic_list R_PAREN SEMI_COLON
    ;

port_clause :
      PORT^ L_PAREN port_list R_PAREN SEMI_COLON
    ;

generic_list :
      interface_list
    ;

port_list :
      interface_list
    ;

group_declaration :
      GROUP^ identifier COLON selected_or_simple_name
      L_PAREN group_constituent_list R_PAREN SEMI_COLON
    ;

group_template_declaration :
      GROUP identifier IS L_PAREN entity_class_entry_list R_PAREN
    ;

entity_class_entry_list !:
      ece:entity_class_entry ecelt:entity_class_entry_list_tail
      {#entity_class_entry_list = #([IDENT13, "entity_class_entry_list_root"], ece, ecelt);}
    ;

entity_class_entry_list_tail :
      ( COMMA entity_class_entry )*
    ;

entity_class_entry :
      entity_class ( LESS_GREATER )?
    ;

group_constituent_list !:
      gc:group_constituent gclt:group_constituent_list_tail
      {#group_constituent_list = #([IDENT14, "group_constituent_list_root"], gc, gclt);}
    ;

group_constituent_list_tail :
      ( COMMA group_constituent )*
    ;

group_constituent :
      ( CHARACTER_LITERAL )
        => CHARACTER_LITERAL
    | complex_name
    ;

boolean_expression :
      expression
    ;

time_expression :
      expression
    ;

expression :
      relation expression_tail
    ;

expression_tail :
      (
      //( and_or_xor_xnor ) => // warning syntactic predicate superflous
      and_or_xor_xnor relation )+
    | ( ( nand_nor )
        => nand_nor relation )?
    ;

relation :
      shift_expression ( ( relational_operator )
        => relational_operator shift_expression )?
    ;

shift_expression :
      simple_expression ( ( shift_operator )
        => shift_operator simple_expression )?
    ;

simple_expression :
     ( sign_operator )? term (
     // ( adding_operator ) =>    // warning syntactic predicate superflous
     adding_operator term )*
    ;

term :
      factor (
      //( multiplying_operator ) => // warning syntactic predicate superflous
      multiplying_operator factor )*
    ;

factor :
      primary ( (EXPONENT primary)
        => ( EXPONENT primary ) )?
      | abs_not primary
    ;

numeric_literal :
      abstract_literal ( ( IDENTIFIER ) => complex_name )?
    ;

literal :
      numeric_literal
    | CHARACTER_LITERAL
    | STRING_LITERAL
    | BIT_STRING_LITERAL
    | NULL
    ;

primary :
      ( STRING_LITERAL L_PAREN )
        => complex_name
    | ( literal )
        => ( literal )
    | complex_name
    | allocator
    | aggregate
    ;

association_list_out !:
      ea:element_association alot:association_list_out_tail
      {#association_list_out = #([IDENT15, "association_list_out_root"], ea, alot);}
    ;

association_list_out_tail :
      ( COMMA element_association )*
    ;

allocator :
      ( NEW subtype_indication )
        => ( NEW subtype_indication )
    | ( NEW complex_name )
    ;

element_association :
      ( choices EQUAL_GREATER expression )
        => ( choices EQUAL_GREATER expression )
    | association
    ;

complex_name :
      attribute_name
    ;

attribute_name :
      selected_name ( QUOTE attribute )*
    ;

attribute :
      ( selected_name )
        => attribute_user_defined
    | attribute_active
    | attribute_ascending
    | attribute_base
    | attribute_delayed
    | attribute_driving_value
    | attribute_driving
    | attribute_event
    | attribute_high
    | attribute_image
    | attribute_instance_name
    | attribute_last_active
    | attribute_last_event
    | attribute_last_value
    | attribute_leftof
    | attribute_left
    | attribute_length
    | attribute_low
    | attribute_path_name
    | attribute_pos
    | attribute_pred
    | attribute_range
    | attribute_rightof
    | attribute_right
    | attribute_reverse_range
    | attribute_simple_name
    | attribute_stable
    | attribute_succ
    | attribute_transaction
    | attribute_quiet
    | attribute_value
    | attribute_val
    | ( aggregate )
    ;

attribute_user_defined :
      selected_name
    ;

attribute_active :
      ACTIVE
    ;

attribute_ascending :
      ASCENDING ( ( L_PAREN )
        => L_PAREN expression R_PAREN )?
    ;

attribute_base :
      BASE
    ;

attribute_delayed :
      DELAYED ( ( L_PAREN )
        => L_PAREN expression R_PAREN )?
    ;

attribute_driving_value :
      DRIVING_VALUE
    ;

attribute_driving :
      DRIVING
    ;

attribute_event :
      EVENT
    ;

attribute_high :
      HIGH ( ( L_PAREN )
        => L_PAREN expression R_PAREN )?
    ;

attribute_image :
      IMAGE L_PAREN expression R_PAREN
    ;

attribute_instance_name :
      INSTANCE_NAME
    ;

attribute_last_active :
      LAST_ACTIVE
    ;

attribute_last_event :
      LAST_EVENT
    ;

attribute_last_value :
      LAST_VALUE
    ;

attribute_leftof :
      LEFTOF^ L_PAREN expression R_PAREN
    ;

attribute_left :
      LEFT^ ( ( L_PAREN )
        => L_PAREN expression R_PAREN )?
    ;

attribute_length :
      LENGTH^ ( ( L_PAREN )
        => L_PAREN expression R_PAREN )?
    ;

attribute_low :
      LOW^ ( ( L_PAREN )
        => L_PAREN expression R_PAREN )?
    ;

attribute_path_name :
      PATH_NAME
    ;

attribute_pos :
      POS^ L_PAREN expression R_PAREN
    ;

attribute_pred :
      PRED^ L_PAREN expression R_PAREN
    ;

attribute_range :
      RANGE ( ( L_PAREN )
          => L_PAREN expression R_PAREN )?
    ;

attribute_rightof :
      RIGHTOF^ L_PAREN expression R_PAREN
    ;

attribute_right :
      RIGHT^ ( ( L_PAREN )
            => L_PAREN expression R_PAREN )?
    ;

attribute_reverse_range :
      REVERSE_RANGE ( ( L_PAREN )
          => L_PAREN expression R_PAREN )?
    ;

attribute_simple_name :
      SIMPLE_NAME
    ;

attribute_stable :
      STABLE^ ( ( L_PAREN )
        => L_PAREN expression R_PAREN )?
    ;

attribute_succ :
      SUCC^ L_PAREN expression R_PAREN
    ;

attribute_transaction :
      TRANSACTION
    ;

attribute_quiet :
      QUIET^ ( ( L_PAREN )
        => L_PAREN expression R_PAREN )?
    ;

attribute_value :
      VALUE^ ( ( L_PAREN )
        => L_PAREN expression R_PAREN )?
    ;

attribute_val :
      VAL^ L_PAREN expression R_PAREN
    ;

selected_name :
//      indexed_name ( DOT^ ( indexed_name | ALL ( L_PAREN index_specifier R_PAREN )* ) )*
      indexed_name ( DOT ( indexed_name | ALL ( L_PAREN index_specifier R_PAREN )* ) )*
    ;

indexed_name :
      name ( L_PAREN index_specifier R_PAREN )*
    ;

index_specifier :
      ( expression direction )
        => discrete_range ( EQUAL_GREATER expression )?
    | association_list_out
    ;

slice_specifier :
      discrete_range
    ;

name :
      simple_name
    | STRING_LITERAL
    | CHARACTER_LITERAL
    ;

simple_name :
      identifier
    ;

alpha_literals :
      BIT_STRING_LITERAL
    | CHARACTER_LITERAL
    | STRING_LITERAL
    | NULL
    ;

physical_type_name :
      identifier
    ;

discrete_range :
      ( range )
        => ( range )
    | subtype_indication
    ;

direction :
      TO
    | DOWNTO
    ;

vhdl_label :
      identifier
    ;

integer_literal :
      DECIMAL_INTEGER_LITERAL
    | BASED_INTEGER_LITERAL
    ;

// real_literal :
//      decimal_floating_point_literal
//    | based_floating_point_literal
//     ;

identifier :
      IDENTIFIER
    ;

chiffre_nombre :
	identifier
	| integer_literal
    ;
nature_declaration :
      NATURE^ identifier IS nature_definition SEMI_COLON
    ;

nature_definition :
      scalar_nature_definition
    | array_nature_definition
    | record_nature_definition
    ;

terminal_declaration :
      TERMINAL^ identifier_list COLON subnature_indication SEMI_COLON
    ;

quantity_declaration :
      QUANTITY^ quantity_declaration_tail SEMI_COLON
    ;

quantity_declaration_tail :
//      ( identifier_list COLON )
//           => identifier_list COLON^ subtype_indication ( source_aspect | initialization )?
      ( identifier_list COLON )
          => identifier_list COLON subtype_indication ( source_aspect | initialization )?
    | ( identifier_list ( TOLERANCE | COLON_EQUAL | ACROSS | THROUGH ) )
          => across_through_aspect_body
            ( ACROSS ( across_through_aspect_body THROUGH )? | THROUGH ) terminal_aspect
    | terminal_aspect
    ;

across_through_aspect_body :
      identifier_list ( TOLERANCE expression )? ( initialization )?
    ;

terminal_aspect :
      complex_name ( TO complex_name )?
    ;

source_aspect :
      SPECTRUM^ simple_expression COMMA^ simple_expression
    | NOISE simple_expression
    ;

//modif1
step_limit_specification :
      LIMIT^ step_limit_specification_head COLON type_mark WITH expression SEMI_COLON
    ;

//modif1
step_limit_specification_head :!
      sosn:selected_or_simple_name slsat:step_limit_specification_head_tail
            {
                #step_limit_specification_head =
                    #([IDENT16, "step_limit_specification_head_root"], sosn, slsat);
            }
    | OTHERS
    | ALL
    ;

step_limit_specification_head_tail :
      ( COMMA selected_or_simple_name )*
    ;

scalar_nature_definition :
      type_mark ACROSS type_mark THROUGH identifier REFERENCE
    ;

//modif4
array_nature_definition :
      ARRAY^ array_nature_definition_head OF subnature_indication
    ;

//modif4
array_nature_definition_head !:
      lp:L_PAREN ad:array_dimension andat:array_nature_definition_head_tail rp:R_PAREN
            {
                #array_nature_definition_head =
                    #([IDENT17, "array_nature_definition_head_root"], lp, ad, andat, rp);
            }
    ;

array_nature_definition_head_tail :
      ( COMMA array_dimension )*
    ;

record_nature_definition :
      RECORD^ ( nature_element_declaration )+ END RECORD ( simple_name )?
    ;

nature_element_declaration :
      identifier_list COLON^ subnature_indication SEMI_COLON
    ;

subnature_declaration :
      SUBNATURE^ identifier IS subnature_indication SEMI_COLON
    ;

subnature_indication :
      type_mark ( index_constraint )? ( TOLERANCE expression ACROSS expression THROUGH )?
    ;

concurrent_break_statement :
//      BREAK^ ( break_element ( COMMA^ break_element )* )?
//     ( ON^ complex_name ( COMMA^ complex_name )* )?
//       ( WHEN expression )? SEMI_COLON
    BREAK ( break_element ( COMMA break_element )* )?
   ( ON complex_name ( COMMA complex_name )* )?
    ( WHEN expression )? SEMI_COLON
    ;

set_of_simultaneous_statements :
      ( concurrent_statement )*
    ;

simultaneous_statement :
      ( stmt_label! )? simultaneous_stmt
    ;

simultaneous_stmt :
      simple_simultaneous_statement
    | simultaneous_if_statement
    | simultaneous_case_statement
    | simultaneous_procedural_statement
    | simultaneous_null_statement
    ;

simple_simultaneous_statement :
      simple_expression EQUAL_EQUAL simple_expression ( TOLERANCE expression )? SEMI_COLON
    ;

simultaneous_if_statement :
      IF condition USE set_of_simultaneous_statements
      ( ELSIF condition USE set_of_simultaneous_statements )*
      ( ELSE set_of_simultaneous_statements )? END USE ( simple_name )? SEMI_COLON
    ;

simultaneous_case_statement :
      CASE expression USE ( WHEN choices EQUAL_GREATER set_of_simultaneous_statements )+
      END CASE ( simple_name )? SEMI_COLON
    ;

simultaneous_procedural_statement :
      PROCEDURAL^ ( IS )? ( procedural_declarative_item )* sequence_of_statements
      END PROCEDURAL ( simple_name )? SEMI_COLON
    ;

procedural_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | variable_declaration
    | alias_declaration
    | attribute_declarative_item
    | use_clause
    | ( GROUP IDENTIFIER COLON )
        => group_declaration
    | group_template_declaration
    ;

simultaneous_null_statement :
             NULL SEMI_COLON
    ;

break_statement :
//      BREAK ( break_element ( COMMA^ break_element )* )? ( WHEN expression )? SEMI_COLON
    BREAK ( break_element ( COMMA break_element )* )? ( WHEN expression )? SEMI_COLON
    ;

break_element :
      ( FOR complex_name USE )? complex_name EQUAL_GREATER expression
    ;

protected_type_declaration :
      PROTECTED^ ( protected_type_declarative_item )* END PROTECTED ( simple_name )?
    ;

protected_type_declarative_item :
      subprogram_declaration
    | attribute_specification
    | use_clause
    ;

protected_type_body :
      PROTECTED BODY^ ( protected_type_body_declarative_item )*
        END PROTECTED BODY ( simple_name )?
    ;

protected_type_body_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | variable_declaration
    | file_declaration
    | alias_declaration
    | attribute_declarative_item
    | use_clause
    | ( GROUP IDENTIFIER COLON )
        => group_declaration
    | group_template_declaration
    ;

and_or_xor_xnor :
       AND
     | OR
     | XOR
     | XNOR
     ;

nand_nor :
      NAND
    | NOR
    ;

relational_operator :  EQUAL | NOT_EQUAL | LESS | LESS_EQUAL | GREATER | GREATER_EQUAL ;

shift_operator  : SLL | SRL | SLA | SRA | ROL | ROR ;

sign_operator  : PLUS | MINUS ;

adding_operator : PLUS | MINUS | AMPERSAND ;

multiplying_operator : MULTIPLY | DIVIDE | MOD | REM ;

abs_not : ABS | NOT ;


class bhdlLexer extends Lexer ;
options {
    exportVocab     =   vhdl    ;       // Call its vocabulary "vhdl"
    charVocabulary = '\0'..'\377';
    k=9;
    testLiterals=true ;
    caseSensitive=false ;
    caseSensitiveLiterals=false ;
        }
// A partir du lexer de Savant


L_PAREN       : '(' ;
EQUAL_EQUAL   : "==" ;
AMPERSAND     : '&' ;
R_PAREN       : ')' ;
L_BRACKET     : '[' ;
R_BRACKET     : ']' ;
COMMA         : ',' ;
COLON_EQUAL   : ":=" ;
COLON         : ':' ;
SEMI_COLON    : ';' ;
LESS_GREATER  : "<>" ;
LESS_EQUAL    : "<=" ;
LESS          : "<" ;
EQUAL_GREATER : "=>" ;
EQUAL         : '=' ;
GREATER_EQUAL : ">=" ;
GREATER       : '>' ;
CHOICE1       : '|' ;
// CHOICE2       : '!' ;
NOT_EQUAL     : "/=" ;
DOT           : '.' ;
EXPONENT      : "**" ;
MULTIPLY      : '*'  ;
DIVIDE        : '/' ;
PLUS          : '+' ;
MINUS         : '-' ;
UNDERSCORE    : '_' ;


BIT_STRING_LITERAL
  : 'b' '"' ('0' | '1') ( ('_')? ('0' | '1') ) '"'
  | 'b' '%' ('0' | '1') ( ('_')? ('0' | '1') ) '%'
  | 'o' '"' ('0'..'7') ( ('_')? ('0'..'7') ) '"'
  | 'o' '%' ('0'..'7') ( ('_')? ('0'..'7') ) '%'
  | 'x' '"' ('0'..'9' | 'a'..'f') ( ('_')? ('0'..'9' | 'a'..'f') ) '"'
  | 'x' '%' ('0'..'9' | 'a'..'f') ( ('_')? ('0'..'9' | 'a'..'f') ) '%' ;

STRING_LITERAL :
           '"'
             ((~('\n'|'\r'|'"')))+
           '"'
           { $setType(STRING_LITERAL); };

protected
QUOTE : '\''  ;

protected
CHARACTER_LITERAL :
           '\'' (~('\n'|'\r')) '\''
           ;

CHARACTER_LITERAL_OR_QUOTE :
          ( '\'' (~('\n'|'\r')) '\'') => CHARACTER_LITERAL { $setType(CHARACTER_LITERAL) ;}
        | ('\'' ) => QUOTE { $setType(QUOTE) ;}
  ;

WS
  : ( ' ' |
      '\t'
    | '\f'
    | ( "\r\n"
      | '\n'
      )
      { newline(); }
    )
    { $setType(Token.SKIP); }

  ;



protected
DIGIT : '0'..'9' ;

protected
LETTER :   'a'..'z' ;

IDENTIFIER
options {testLiterals=true ; }
  : LETTER ( LETTER | DIGIT | '_' )*
  ;

BASED_INTEGER_LITERAL_OR_DECIMAL_INTEGER_LITERAL :
      (INTEGER '#') => BASED_INTEGER_LITERAL { $setType(BASED_INTEGER_LITERAL) ;}
    | (INTEGER) =>  DECIMAL_INTEGER_LITERAL { $setType(DECIMAL_INTEGER_LITERAL) ;}
;

protected
DECIMAL_INTEGER_LITERAL : INTEGER ( DOT INTEGER )? ( EXP )? ;

protected
BASED_INTEGER_LITERAL :
   INTEGER '#' BASED_INTEGER ( DOT BASED_INTEGER )? '#' ( EXP )?
 ;

protected
INTEGER : DIGIT (( UNDERSCORE )? DIGIT)*  ;

protected
BASED_INTEGER
:  (DIGIT | LETTER) ( ( UNDERSCORE )? (DIGIT | LETTER) )*
;

protected
EXP
: ('e') (PLUS|MINUS)? INTEGER
;

COMMENT
: "-- #i"    { $setType(BCOMMENT_INV); }
| "-- #d"    { $setType(BCOMMENT_DEF); }
| "-- #="!   (~('\n'|'\r'))*  { $setType(BCOMMENT_E); }
| "--" (~('\n'|'\r'))*   { $setType(Token.SKIP); };
