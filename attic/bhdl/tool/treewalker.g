// VHDL2B Tree parser
//
//        This ANTLR parser can walk the VHDL AST and transforms
//        a part of it to the associated B one
//
//        Authors :
//        Ammar Aljer, Philippe Devienne, Pierre Antoine Laloux
//              04/18/2007 Modify Leyman KOMBATE NENE
//
//

{
import java.io.*;
import antlr.collections.AST;
}

class treewalker extends TreeParser ;
options {
    importVocab=vhdl;               // Call its vocabulary "vhdl"
    defaultErrorHandler = true ;
    buildAST=true;
  //  codeGenMakeSwitchThreshold = 3;
  //  codeGenBitsetTestThreshold = 4;
    k=1;
         }
  {
  AST entity_comment;
  AST ab;
  PrintWriter amfile = null;
  String comment1;
  String comment2;
  }
design_file :
{
try{
 amfile = new PrintWriter(
 new BufferedWriter(new FileWriter("./B/source" + ".vhdl")));
  }catch(java.io.IOException e){}
}
  (c:comments!  {#entity_comment = #c.getFirstChild();})? (enum_file1)? design_file1
    {
    amfile.close();
    }
    ;

comments :
      #(IDENTIFIER (#(IDENTIFIER identifier_list1 BCOMMENT_E identifier_list1))*)
    ;
enum_file1  :
		#(IDENTIFIER (#(IDENTIFIER (chiffre_nombre identifier_list2)))*)
    ;
enum_list :
	(BENUM_DEF! #(IDENT11 EQUAL! L_PAREN! identifier_list R_PAREN!))
    ;

design_file1 :
	 (design_unit)*
    ;

//context_clauses are not used to generate B code
//context_clauses contain library clause and use clause
design_unit :
      context_clauses!  library_unit
    ;

library_unit :
      primary_unit
    | secondary_unit
    ;

//configuration_declaration and package_declaration
// are not used to generate B code
primary_unit :
      entity_declaration
    |! configuration_declaration
    |! package_declaration
    ;

secondary_unit :
       architecture_body
    |! package_body
    ;

library_clause! :
      #(LIBRARY logical_library_name_list SEMI_COLON)
    ;

logical_library_name_list :
      #(IDENT logical_name (COMMA logical_name)*)
    ;

logical_name :
      identifier
    ;

context_clauses :
      ( context_item )*
    ;

context_item :
      library_clause
    | use_clause
    ;

entity_declaration! :
      #(en:ENTITY {amfile.print(en.getText()+ " ");}
      ident:identifier {amfile.print(ident.getText()+ " ");}
      is:IS {amfile.println(is.getText());}
      entih:entity_header
      	entity_declarative_part
        ( entity_statement_part )?
        e:END {amfile.print(e.getText()+ " ");}
      ( ENTITY )?
      ( simple_name )?
        sc:SEMI_COLON {amfile.println(sc.getText()+ " ");} )
      {   AST tempA;
          AST MachineA;
          AST SeesA;
          AST VariablesA;
          AST DefinitionsA;
          AST InvariantA;
          AST InitialisationA;
          AST OperationsA;
          AST NextA;
          AST ModeA;
          AST TypeA;
          AST SeesSearch;

          AST DefVariablesA;
          AST DefTextA;
          AST InvVariablesA;
          AST TempDVariableA;
          AST TempIVariableA;
          AST OneVariableA;
          AST OneDVariableA;
          AST OneIVariableA;

          String OneVariableS;
          String OneDVariableS;
          String OneIVariableS;

          String DefTempVariable;
          String InvTempVariable;

          String Oper;
          String OperMod;
          String OperType;
          String SearchS;

          #tempA=#([ABSTRACT,"ABSTRACT"]);
          #MachineA=#([IDENTIFIER,"MACHINE"]);
          #SeesA=#([IDENTIFIER,"SEES"]);
          #VariablesA=#([IDENTIFIER,"VARIABLES"]);
          #DefinitionsA=#([IDENTIFIER,"DEFINITIONS"]);
          #InvariantA=#([IDENTIFIER,"INVARIANT"]);
          #InitialisationA=#([IDENTIFIER,"INITIALISATION"]);
          #OperationsA=#([IDENTIFIER,"OPERATIONS"]);
          #ident.setNextSibling(null);
          #MachineA.setFirstChild(#ident);
          if (#entih != null)
           {
             #NextA=#entih.getFirstChild();
             while (#NextA != null)
              {
                #TypeA=#NextA.getFirstChild();
                #ModeA=#TypeA.getNextSibling();

                #SeesSearch=#SeesA.getFirstChild();
                SearchS=TypeA.getText();
                while ( (#SeesSearch != null)  &&
                       !(SearchS.equals(#SeesSearch.getText() )  )
                       )
                 {
                   #SeesSearch = #SeesSearch.getNextSibling();
                 }
               if (#SeesSearch == null)
                {
                 #SeesA.addChild( #( [#TypeA.getType(), #TypeA.getText()] ) );
                }

               #VariablesA.addChild( #([#NextA.getType(),#NextA.getText()]) );

               #InitialisationA.addChild( #([#NextA.getType(),#NextA.getText()],
                                            [#TypeA.getType(),#TypeA.getText()])
                                         );
               #OperationsA.addChild( #([#NextA.getType(),#NextA.getText()],
                                        [#ModeA.getType(),#ModeA.getText()],
                                        [#TypeA.getType(),#TypeA.getText()])
                                     );


               #NextA=#NextA.getNextSibling();
             }
           }
          AST comment_search;
          String search2=#ident.getText();
          #comment_search=#entity_comment;
          while (       (#comment_search!=null)
                     && !(search2.equals(#comment_search.getText()))
                 )
           {
            #comment_search=#comment_search.getNextSibling();
           }
          if (#comment_search!=null)
           {
            #DefVariablesA=#comment_search.getFirstChild();
            #DefTextA=#DefVariablesA.getNextSibling();
            #InvVariablesA=#DefVariablesA.getNextSibling().getNextSibling();
            #DefVariablesA.setNextSibling(null);
            #DefTextA.setNextSibling(null);

            #OneVariableA=#VariablesA.getFirstChild();
            #OneDVariableA=#DefVariablesA.getFirstChild();
            #OneIVariableA=#InvVariablesA.getFirstChild();

            while (#OneVariableA!=null)
             {
               OneVariableS =#OneVariableA.getText();
               OneDVariableS=#OneDVariableA.getText();
               OneIVariableS=#OneIVariableA.getText();



               if ( !(OneVariableS.equals(OneIVariableS)) )
                {
                  #TempDVariableA=#OneDVariableA.getNextSibling();
                  #TempIVariableA=#OneIVariableA.getNextSibling();
                  while  (   (#TempIVariableA!=null)
                          && (!(OneVariableS.equals(#TempIVariableA.getText()))))
                   {

                     #TempDVariableA=#TempDVariableA.getNextSibling();
                     #TempIVariableA=#TempIVariableA.getNextSibling();
                   }

                  if   (#TempIVariableA!=null)

                    {
                      DefTempVariable=OneDVariableS;
                      InvTempVariable=OneIVariableS;
                      #OneDVariableA.setText(#TempDVariableA.getText());
                      #OneIVariableA.setText(#TempIVariableA.getText());
                      #TempDVariableA.setText(OneDVariableS);
                      #TempIVariableA.setText(OneIVariableS);
                    }

                }
               #OneVariableA= #OneVariableA.getNextSibling();
               #OneDVariableA=#OneDVariableA.getNextSibling();
               #OneIVariableA=#OneIVariableA.getNextSibling();
             }

            #DefinitionsA.addChild(#DefVariablesA);
            #DefinitionsA.addChild(#DefTextA);
            #InvariantA.addChild( #InvVariablesA);
           }
          #tempA.addChild(#MachineA);
          #tempA.addChild(#SeesA);
          #tempA.addChild(#VariablesA);
          #tempA.addChild(#DefinitionsA);
          #tempA.addChild(#InvariantA);
          #tempA.addChild(#InitialisationA);
          #tempA.addChild(#OperationsA);
          #entity_declaration=#tempA;
         }
    ;

entity_header :
      ( generic_clause! )? ( port_clause )?
    ;

entity_declarative_part :
      ( entity_declarative_item )*
    ;

entity_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | signal_declaration
    | shared_variable_declaration
    | file_declaration
    | alias_declaration
    | attribute_declarative_item
    | disconnection_specification
    | use_clause
    | ( LIMIT | NATURE | SUBNATURE | QUANTITY | TERMINAL )
        => ( step_limit_specification
             | nature_declaration
             | subnature_declaration
             | quantity_declaration
             | terminal_declaration
            )
    | ( GROUP IDENTIFIER COLON )
        => group_declaration
    | group_template_declaration
    ;

entity_statement_part :
      ( BEGIN )? ( entity_statement )*
    ;

entity_statement :
      ( stmt_label )? ( POSTPONED )? entity_stmt
    ;

entity_stmt :
      concurrent_assertion_statement
    | concurrent_procedure_call
    | process_statement
    ;

concurrent_procedure_call :
      complex_name SEMI_COLON
    ;

architecture_body !:
      #( ar:ARCHITECTURE  {amfile.print(ar.getText()+ " ");}
       i1:identifier {amfile.print(i1.getText()+ " ");}
       of:OF {amfile.print(of.getText()+ " ");}

        sosn:selected_or_simple_name {amfile.print(sosn.getText()+ " ");}
        is:IS {amfile.println(is.getText()+ " ");}

         adp:architecture_declarative_part

         asp:architecture_statement_part

         end:END {amfile.println(); amfile.print( end.getText()+ " ");}
         ( arch:ARCHITECTURE {amfile.print( arch.getText()+ " ");} )?
         ( sn:simple_name )?
         sc:SEMI_COLON {amfile.print( sc.getText()+ " ");}  )
           { if ( #asp.getFirstChild() != null)
              {
               #adp.setText("IMPORTS");
               #architecture_body=#([CONCRETE,"CONCRETE"],
                                     #([IDENTIFIER,"REFINEMENT"],i1,sosn),
                                     #([IDENTIFIER,"REFINES"],sosn),
                                     adp ,
                                     #([IDENTIFIER,"ARC_BODY"], asp) );
             }
            else
             {
               #adp.setText("IMPORTS");
               #architecture_body=#([CONCRETE,"CONCRETE"],
                                     #([IDENTIFIER,"REFINEMENT"],i1,sosn),
                                     #([IDENTIFIER,"REFINES"],sosn),
                                     adp
                                   );
             }
           }

    ;

architecture_declarative_part !:
      bdi:block_declarative_item adp:architecture_declarative_part
      {   AST  tempA;
                            #tempA=#bdi;
                            #adp.addChild(tempA);
                            #architecture_declarative_part=#adp;
      }
    | {
        #architecture_declarative_part=#([IDENTIFIER,"ArcDecPart"]);
      }
    ;

architecture_statement_part !:
      ( be:BEGIN    {amfile.print(be.getText()+ " ");}
              )?
              {
                #architecture_statement_part
                 = #([IDENTIFIER,"architecture_statement_part"]);
              }
      ( ase:architecture_statement_element
        {if (#ase != null)
                {AST tt;
                 #tt=null;
                 #tt=#([IDENT25,"newcomp"]);
                 #tt.addChild(#ase);
                 #architecture_statement_part.addChild(#tt);}
          }

        )*
    ;

architecture_statement_element! :
      ( CASE | NULL | PROCEDURAL | IF condition USE | simple_expression EQUAL_EQUAL )
        => simultaneous_statement!
    |  cs:concurrent_statement
          {if (#cs != null){#architecture_statement_element=#cs.getFirstChild();}}
    ;

configuration_declaration :
      #( CONFIGURATION identifier OF selected_or_simple_name IS
         configuration_declarative_part block_configuration
         END ( CONFIGURATION )? ( simple_name )? SEMI_COLON)
    ;

configuration_declarative_part :
      ( configuration_declarative_item )*
    ;

configuration_declarative_item :
      use_clause
    | attribute_specification
    | group_declaration
    ;

block_configuration! :
      #(FOR block_specification ( use_clause )*
         ( configuration_item )* END FOR SEMI_COLON
       )
    ;

block_specification :
      selected_or_simple_name ( L_PAREN index_specifier R_PAREN )?
    ;

configuration_item :
      ( component_configuration )
        => ( component_configuration )
    | block_configuration
    ;

component_configuration! :
      #( FOR component_specification ( binding_indication SEMI_COLON )?
      ( block_configuration )? END FOR SEMI_COLON)
    ;

concurrent_statement! :

     #(IDENT21    {amfile.println();}
     ( sl:stmt_label {amfile.print(#sl.getText());})?
     (co:COLON! {amfile.print(#co.getText());})?)

     cs:concurrent_stmt
     {if (sl != null) {#concurrent_statement=#(IDENT21,sl,cs);}}
    ;

concurrent_stmt :
      ( BLOCK | FOR | IF | BREAK | CASE | NULL | PROCEDURAL
         | simple_expression EQUAL_EQUAL
      )
        => not_postponeable
    |    #(IDENT23 postponeable)
    ;

not_postponeable :
      block_statement
    | ( generate_scheme GENERATE )
        => generate_statement
    | ( IF | CASE | NULL | PROCEDURAL | simple_expression EQUAL_EQUAL )
        => simultaneous_stmt
    | concurrent_break_statement
    ;

postponeable :
      process_statement
    | concurrent_assertion_statement
    | selected_signal_assignment_statement
    | ( target LESS_EQUAL )
        => conditional_signal_assignment
    |! ( COMPONENT | ENTITY | CONFIGURATION | complex_name
         ( generic_map_aspect | port_map_aspect )
       )
        => is:instantiate_statement   {#postponeable =#is.getFirstChild();}
    | concurrent_procedure_call
    ;


 commentsr :
       (BCOMMENT_DEF BCOMMENT_INV)?
       ;

block_statement :
      #( BLOCK ( L_PAREN expression R_PAREN )?
         ( IS )? block_header block_declarative_part
         architecture_statement_part END BLOCK ( simple_name )? SEMI_COLON
       )
    ;

block_header! :
      ( generic_clause ( generic_map_aspect SEMI_COLON )? )?
        ( port_clause ( port_map_aspect SEMI_COLON )? )?
    ;

block_declarative_part :
      ( block_declarative_item )*
    ;

block_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | signal_declaration
    | shared_variable_declaration
    | file_declaration
    | alias_declaration
    | component_declaration
    | disconnection_specification
    | attribute_declarative_item
    | configuration_specification
    | use_clause
    | ( LIMIT | NATURE | SUBNATURE | QUANTITY | TERMINAL )
        => ( step_limit_specification
             | nature_declaration
             | subnature_declaration
             | quantity_declaration
             | terminal_declaration
           )
    | ( GROUP IDENTIFIER COLON )
        => group_declaration
    | group_template_declaration
    ;

process_statement :
      #(PROCESS ( L_PAREN sensitivity_list R_PAREN )? ( IS )?
      process_declarative_part sequence_of_statements
      END ( POSTPONED )? PROCESS ( simple_name )? SEMI_COLON)
    ;

process_declarative_part :
      ( process_declarative_item )*
    ;

process_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | variable_declaration
    | file_declaration
    | alias_declaration
    | attribute_declarative_item
    | use_clause
    | ( GROUP IDENTIFIER COLON )
        => group_declaration
    | group_template_declaration
    ;

concurrent_assertion_statement :
      assertion SEMI_COLON
    ;

concurrent_call_statement :
      complex_name SEMI_COLON
    ;

instantiate_statement :

       #(IDENT24
            (ENTITY!|CONFIGURATION!|( COMPONENT )?)
            cn:complex_name {amfile.print(cn.getText());}
            ( generic_map_aspect! )?
            ( port_map_aspect )?
            SEMI_COLON!
       )    ;

conditional_signal_assignment :
      target LESS_EQUAL optionss conditional_waveforms SEMI_COLON
    ;

target :
      ( complex_name )
        => ( complex_name )
    | aggregate
    ;


aggregate :
      #(IDENT1 L_PAREN! aggregate_entry (COMMA aggregate_entry)* R_PAREN!)

    ;

aggregate_entry :
      choices ( EQUAL_GREATER expression )?
    ;

conditional_waveforms :
      concurrent_waveform ( WHEN condition ( ELSE conditional_waveforms )? )?
    ;

selected_signal_assignment_statement :

      WITH expression SELECT target LESS_EQUAL
      optionss selected_waveforms SEMI_COLON
    ;

selected_waveforms :
      #(IDENT2 selected_waveforms_head (COMMA selected_waveforms_head)*)
    ;

selected_waveforms_head :
      concurrent_waveform WHEN choices
    ;

optionss :
      ( GUARDED )? ( delay_mechanism )?
    ;

delay_mechanism :
      TRANSPORT
    | ( REJECT time_expression )? INERTIAL
    ;

generate_statement :
      generate_scheme GENERATE ( ( block_declarative_part BEGIN )
        => block_declarative_part BEGIN )? architecture_statement_part
           END GENERATE ( simple_name )? SEMI_COLON
    ;

generate_scheme :
      #(FOR identifier IN discrete_range)
    | IF condition
    ;

sequence_of_statements :
      ( BEGIN )? ( sequential_statement )*
    ;

sequential_statement :
     #(IDENT20 ( stmt_label )? sequential_stmt SEMI_COLON)
    ;

sequential_stmt :
      wait_statement
    | assertion_statement
    | report_statement
    | ( NULL )
        => null_statement
    | ( target LESS_EQUAL )
        => signal_assignment_statement
    | ( target COLON_EQUAL )
        => variable_assignment_statement
    | ( complex_name SEMI_COLON )
        => procedure_call_statement
    | if_statement
    | case_statement
    | loop_statement
    | next_statement
    | exit_statement
    | return_statement
    | break_statement
    ;

stmt_label :
      vhdl_label
    ;

wait_statement :
      WAIT ( sensitivity_clause )? ( condition_clause )? ( timeout_clause )?
    ;

sensitivity_clause :
      ON sensitivity_list
    ;

sensitivity_list :
      #(IDENT3 complex_name (COMMA complex_name)*)
    ;

condition_clause :
      UNTIL condition
    ;

condition :
      boolean_expression
    ;

timeout_clause! :
      FOR time_expression
    ;

assertion_statement :
      assertion
    ;

assertion :
      ASSERT condition ( REPORT expression )? ( SEVERITY expression )?
    ;

report_statement :
      REPORT expression ( SEVERITY expression )?
    ;

null_statement :
      NULL
    ;

signal_assignment_statement :
      #(IDENT18 target LESS_EQUAL sequential_signal_assign_stmt)
    ;


variable_assignment_statement :
      #(COLON_EQUAL target variable_assign_stmt)
    ;

procedure_call_statement :
      complex_name
    ;

sequential_signal_assign_stmt :
      ( delay_mechanism )? sequential_waveform
    ;

variable_assign_stmt :
      expression
    ;

sequential_waveform :
      #(IDENT4 waveform_element (COMMA waveform_element)*)
    ;

concurrent_waveform :
      sequential_waveform
    | UNAFFECTED
    ;

waveform_element :
      #(IDENT19 expression
      ( AFTER expression )?)
    ;

if_statement :
      IF condition THEN sequence_of_statements
       ( ( ELSIF ) => elsif_stmt )?
       ( ELSE sequence_of_statements )?
       END IF ( simple_name )?
    ;

elsif_stmt :
      ELSIF condition THEN sequence_of_statements
      ( ( ELSIF ) => elsif_stmt )?
    ;

case_statement :
      CASE expression IS ( case_statement_alternative )+ END CASE ( simple_name )?
    ;

case_statement_alternative :
      WHEN choices EQUAL_GREATER sequence_of_statements
    ;

choices :
      choice (CHOICE1  choice )*
    | OTHERS
    ;

choice :
      expression ( direction simple_expression )?
    ;

loop_statement :
      ( FOR )
        => for_loop_statement
    | while_loop_statement
    ;

for_loop_statement :
      FOR identifier IN discrete_range LOOP sequence_of_statements END
      LOOP ( simple_name )?
    ;

while_loop_statement :
      ( WHILE condition )? LOOP sequence_of_statements END LOOP ( simple_name )?
    ;

next_statement :
      NEXT ( vhdl_label )? ( WHEN condition )?
    ;

exit_statement :
      EXIT ( vhdl_label )? ( WHEN condition )?
    ;

return_statement :
      RETURN ( expression )?
    ;

package_declaration :
      #(PACKAGE identifier IS package_declarative_part END
         ( PACKAGE )? ( simple_name )? SEMI_COLON
        )
    ;

package_declarative_part :
      ( package_declarative_item )*
    ;

package_declarative_item :
      subprogram_declaration
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | signal_declaration
    | shared_variable_declaration
    | file_declaration
    | alias_declaration
    | component_declaration
    | attribute_declarative_item
    | disconnection_specification
    | use_clause
    | ( NATURE | SUBNATURE | TERMINAL )
        => ( nature_declaration | subnature_declaration | terminal_declaration )
    | ( GROUP IDENTIFIER COLON )
        => group_declaration
    | group_template_declaration
    ;

package_body :
      #( BODY PACKAGE  identifier IS package_body_declarative_part
         END ( PACKAGE BODY )? ( simple_name )? SEMI_COLON)
    ;

package_body_declarative_part :
      ( package_body_declarative_item )*
    ;

package_body_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | shared_variable_declaration
    | file_declaration
    | alias_declaration
    | use_clause
    | ( GROUP IDENTIFIER COLON )
        => group_declaration
    | group_template_declaration
    ;

subprogram :
      subprogram_header ( subprogram_body )? SEMI_COLON
    ;

subprogram_declaration :
      subprogram_header SEMI_COLON
    ;

subprogram_header :
      #(PROCEDURE designator ( L_PAREN formal_parameter_list R_PAREN )?)
    | #(FUNCTION ( side_effects )? designator
          ( L_PAREN formal_parameter_list R_PAREN )? RETURN type_mark
       )
    ;

side_effects :
      PURE
    | IMPURE
    ;

designator :
      operator_symbol
    | identifier
    ;

operator_symbol :
      STRING_LITERAL
    ;

formal_parameter_list :
      interface_list
    ;

subprogram_body :
      IS subprogram_declarative_part sequence_of_statements END
      ( FUNCTION | PROCEDURE )? ( designator )?
    ;

subprogram_declarative_part :
      ( subprogram_declarative_item )*
    ;

subprogram_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | variable_declaration
    | file_declaration
    | alias_declaration
    | attribute_declarative_item
    | use_clause
    ;

signature :
      L_BRACKET ( type_mark ( COMMA type_mark )* )? ( RETURN type_mark )? R_BRACKET
    ;

attribute_declarative_item :
      #(ATTRIBUTE ( attribute_declaration_tail | attribute_specification_tail ))
    ;

attribute_declaration_tail :
      #(COLON identifier type_mark SEMI_COLON)
    ;

attribute_specification_tail :
      identifier OF entity_specification IS expression SEMI_COLON
    ;

attribute_specification :
      #(ATTRIBUTE attribute_specification_tail)
    ;

entity_specification :
      entity_name_list COLON entity_class
    ;

entity_name_list :
      #(IDENT5 entity_designator ( signature )? (COMMA entity_designator ( signature )?)*)
    | OTHERS
    | ALL
    ;

entity_designator :
      simple_name
    | operator_symbol
    ;

entity_class :
      ENTITY
    | ARCHITECTURE
    | CONFIGURATION
    | PROCEDURE
    | FUNCTION
    | PACKAGE
    | TYPE
    | SUBTYPE
    | CONSTANT
    | SIGNAL
    | VARIABLE
    | COMPONENT
    | LABEL
    | LITERAL
    | UNITS
    | GROUP
    | FILE
    | ( NATURE | SUBNATURE | QUANTITY | TERMINAL )
    ;

configuration_specification! :
      #(FOR component_specification binding_indication SEMI_COLON)
    ;

component_specification :
      #(COLON instantiation_list complex_name)
    ;

instantiation_list :
      #(IDENT6 simple_name (COMMA simple_name)*)
    | OTHERS
    | ALL
    ;

binding_indication !:
      ( USE entity_aspect )? ( generic_map_aspect )? ( port_map_aspect )?
    ;

entity_aspect :
      #(ENTITY selected_or_simple_name ( L_PAREN simple_name R_PAREN )?)
    | #(CONFIGURATION complex_name)
    | OPEN
    ;

generic_map_aspect :
      #(GENERIC MAP L_PAREN! aslistin R_PAREN!)
    ;

port_map_aspect :
      #(po:PORT {amfile.println(); amfile.print(po.getText()+ " ");}
        ma:MAP  { amfile.print(ma.getText()+ " ");}
        lp:L_PAREN! {amfile.print(lp.getText()+ " ");}
        as:aslistin
        rp:R_PAREN!  {amfile.print(rp.getText()+ " ");}
        )
      //{#port_map_aspect=#asl; }
    ;

disconnection_specification :
      DISCONNECT guarded_signal_specification AFTER expression SEMI_COLON
    ;

guarded_signal_specification :
      #(COLON signal_list type_mark)
    ;

signal_list :
      #(IDENT7 complex_name (COMMA complex_name)*)
    | OTHERS
    | ALL
    ;

use_clause :
      #(USE complex_name ( COMMA selected_name )*
         SEMI_COLON
      )
    ;

scalar_type_definition :
      enumeration_type_definition
    | range_constraint ( physical_type_definition )?
    ;

enumeration_type_definition :
      #(IDENT8 L_PAREN! enumeration_literal (COMMA enumeration_literal)* R_PAREN!)
    ;

enumeration_literal :
      identifier
    | CHARACTER_LITERAL
    ;

range_constraint :
      #(RANGE range)
    ;

range :
      ( ( selected_name QUOTE ( attribute_range | attribute_reverse_range ) ) )
          => ( ( selected_name QUOTE ( attribute_range | attribute_reverse_range ) ) )
    | simple_expression direction simple_expression
    ;

physical_type_definition :
      UNITS base_unit_declaration ( secondary_unit_declaration )*
      END UNITS ( simple_name )?
    ;

base_unit_declaration :
      identifier SEMI_COLON
    ;

secondary_unit_declaration :
      identifier EQUAL physical_literal SEMI_COLON
    ;

physical_literal :
      ( integer_literal )? simple_name
    ;

abstract_literal :
      integer_literal
    ;

composite_type_definition :
      array_type_definition
    | record_type_definition
    ;

array_type_definition :
      #(ARRAY array_type_definition_head OF subtype_indication)
    ;

array_type_definition_head :
      #(IDENT9 L_PAREN! array_dimension (COMMA array_dimension)* R_PAREN!)
    ;

array_dimension :
      ( index_subtype_definition )
        => ( index_subtype_definition )
    | ( discrete_range )
    ;

index_subtype_definition :
      type_mark RANGE LESS_GREATER
    ;

record_type_definition :
      #(RECORD ( element_declaration )+ END RECORD ( simple_name )?)
    ;

element_declaration :
      #(COLON identifier_list subtype_indication SEMI_COLON)
    ;

access_type_definition :
      ACCESS subtype_indication
    ;

file_type_definition :
      FILE OF type_mark
    ;

type_declaration :
      #(TYPE identifier ( IS type_definition )? SEMI_COLON)
    ;

type_definition :
      scalar_type_definition
    | composite_type_definition
    | access_type_definition
    | file_type_definition
    | ( PROTECTED BODY )
        => protected_type_body
    | protected_type_declaration
    ;

subtype_declaration :
      #(SUBTYPE identifier IS subtype_indication SEMI_COLON)
    ;

subtype_indication :
             ( ( complex_name type_mark )
                 => complex_name type_mark ( constraint )?
         | type_mark ( constraint )? ) ( ( TOLERANCE )
                                           => TOLERANCE expression )?
   ;

selected_or_simple_name :
      simple_name ( DOT simple_name )*
    ;

type_mark :
      selected_or_simple_name
    ;

constraint :
      range_constraint
    | index_constraint
    ;

index_constraint :
      #(IDENT10 L_PAREN! discrete_range (COMMA discrete_range)* R_PAREN!)
    ;

constant_declaration :
      #(CONSTANT identifier_list COLON subtype_indication
        ( initialization )? SEMI_COLON
      )
    ;

identifier_list !:
     #(id:IDENT11
      i1:identifier  {amfile.print(i1.getText());}
        {#identifier_list=#(id, #i1);}
       ( co:COMMA {amfile.print(co.getText()+" ");}
        i2:identifier  {amfile.print(i2.getText());}
        {#identifier_list.addChild(#i2);}
       )*
      )
    ;

identifier_list1 !:
     #(id:IDENT11
      i1:identifier  {amfile.print(i1.getText());}
        {#identifier_list1=#(id, #i1);}
       ( co:COMMA {amfile.print(co.getText()+" ");}
        i2:identifier  {amfile.print(i2.getText());}
        {#identifier_list1.addChild(#i2);}
       )*
      )
    ;

 identifier_list2 :
		 (i:chiffre_nombre)*;
initialization :
      COLON_EQUAL expression
    ;

signal_declaration! :
      #(SIGNAL identifier_list COLON! subtype_indication
          ( signal_kind )? ( initialization )? SEMI_COLON
      )
    ;

signal_kind :
      REGISTER
    | BUS
    ;

shared_variable_declaration :
      SHARED VARIABLE variable_declaration_body
    ;

variable_declaration :
      ( SHARED )? VARIABLE variable_declaration_body
    ;

variable_declaration_body :
      #(COLON identifier_list subtype_indication ( initialization )? SEMI_COLON)
    ;

file_declaration :
      #(FILE identifier_list COLON subtype_indication
          ( file_open_information )? SEMI_COLON
      )
    ;

file_open_information :
      ( OPEN expression )? IS file_logical_name
    ;

mode :
      IN
    | OUT
    | INOUT
    | BUFFER
    | LINKAGE
    ;

file_logical_name :
      expression
    ;

interface_declaration !:
      ( ( ( CONSTANT!
            | si:SIGNAL {amfile.println(si.getText());}
            | VARIABLE! ) )?
      li:identifier_list
      co:COLON {amfile.print(co.getText()+ "  ");}
      ( mo:mode {amfile.print(mo.getText()+ "  ");} )?
        id:subtype_indication {amfile.print(id.getText()+ "  ");}
         ( BUS! )?
      ( initialization! )?
          {
            AST t;
            AST tempA;
            AST tNext;
            AST currentIST;
            #t=#li.getFirstChild();
            #tNext=#t.getNextSibling();
            #t.setNextSibling(null);
            if (mo!= null)
             {

               #tempA=#(t,[IDENTIFIER,id.getText()],[mo.getType(),mo.getText()]);

             }
            else
             {
              #tempA=#(t,[IDENTIFIER,id.getText()]);

             }
            #interface_declaration=#([IDENTIFIER ,"PortList"],tempA);
            while (#tNext !=  null)
             {
               #t=#tNext;

               #tNext=#t.getNextSibling();
               #t.setNextSibling(null);
               if (mo!= null)
                {
                  #tempA=#(t,[IDENTIFIER,id.getText()],[mo.getType(),mo.getText()]);

                }
               else
                {
                  #tempA=#(t,[IDENTIFIER,id.getText()]);

                }
                 #interface_declaration.addChild(#tempA);
              }

          }
     | FILE identifier_list COLON subtype_indication
     | ( TERMINAL identifier_list COLON subnature_indication
         | QUANTITY identifier_list COLON ( IN | OUT )?
             subtype_indication ( initialization )?
       )
   )
    ;

interface_list !:
      i2:interface_declaration {  #interface_list=#i2;}
      ( sc:SEMI_COLON { amfile.print(sc.getText());}
         i1:interface_declaration
                {
                    #interface_list.addChild(#i1.getFirstChild());
                }
       )*

    ;

aslistin !:
      #(IDENT12 a1:association {amfile.print(a1.getText()+ " ");}
                       {#aslistin=#([IDENTIFIER, "aslistin_root"],#a1);}
      (co:COMMA! {amfile.print(co.getText()+ " ");}
      a2:association {amfile.print(a2.getText()+ " ");}
                        {#aslistin.addChild(#a2);} )*)
    ;

association :
      association_element ( EQUAL_GREATER association_element )?
    ;

association_element :
      expression
    | OPEN
    ;

alias_declaration :
      #(ALIAS alias_designator ( COLON alias_indication )?
           IS complex_name ( signature )? SEMI_COLON)
    ;

alias_designator :
      identifier
    | CHARACTER_LITERAL
    | operator_symbol
    ;

alias_indication :
      ( type_mark TOLERANCE )
        => subnature_indication
    | subtype_indication
    ;
// generic_clause may be important but it is not sent to begenerater
component_declaration !:
      #( c:COMPONENT {amfile.print(c.getText()+ " ");}
       i:identifier {amfile.print(i.getText()+ " ");}
       (id:IS {amfile.print(id.getText()+ " ");} )?
       ( gc:generic_clause )?
       ( pl:port_clause )?
         end:END {amfile.print(end.getText()+ " ");}
         co:COMPONENT {amfile.print(co.getText()+ " ");}
         ( sn:simple_name {amfile.print(sn.getText()+ " ");})?
         sc:SEMI_COLON {amfile.print(sc.getText()+ " ");}
         )


        {#component_declaration=#(c,i,pl);}
    ;

generic_clause :
      #(GENERIC
       L_PAREN
        generic_list
        R_PAREN
        SEMI_COLON
        )
    ;

port_clause !:
      #(po:PORT {amfile.println(); amfile.println('\t'+po.getText());}
        lp:L_PAREN {amfile.print("\t   "+lp.getText());}
        pl:port_list
        rp:R_PAREN {amfile.println(); amfile.print( '\t'+ rp.getText());}
        sc:SEMI_COLON{amfile.println(sc.getText());}
        )
      { #port_clause=#pl; }
    ;

generic_list :
      interface_list
    ;

port_list :
      interface_list
    ;

group_declaration :
      #(GROUP identifier COLON selected_or_simple_name
        L_PAREN group_constituent_list R_PAREN SEMI_COLON
       )
    ;

group_template_declaration :
      GROUP identifier IS L_PAREN entity_class_entry_list R_PAREN
    ;

entity_class_entry_list :
      #(IDENT13 entity_class_entry (COMMA entity_class_entry)*)
    ;

entity_class_entry :
      entity_class ( LESS_GREATER )?
    ;

group_constituent_list :
      #(IDENT14 group_constituent ( COMMA group_constituent)*)
    ;

group_constituent :
      ( CHARACTER_LITERAL )
        => CHARACTER_LITERAL
    | complex_name
    ;

boolean_expression :
      expression
    ;

time_expression :
      expression
    ;


expression :
      relation

expression_tail

    ;

expression_tail :
      (
      and_or_xor_xnor relation )+
    | ( ( nand_nor )
          => nand_nor relation )?
    ;

relation :
      shift_expression ( ( relational_operator )
                           => relational_operator shift_expression )?
    ;

shift_expression :
      simple_expression ( ( shift_operator )
                            => shift_operator simple_expression )?
    ;

simple_expression :
     ( sign_operator )? term (
     adding_operator term )*
    ;

term :
      factor (
      multiplying_operator factor )*
    ;

factor :

    primary

    (  EXPONENT primary  )?

      | abs_not primary
    ;

numeric_literal :
      abstract_literal ( complex_name )?
    ;

literal :
      numeric_literal
    | CHARACTER_LITERAL
    | STRING_LITERAL
    | BIT_STRING_LITERAL
    | NULL
    ;

primary :
      ( STRING_LITERAL L_PAREN )
        => complex_name
    | ( literal )
        => ( literal )
    | complex_name
    | allocator
    | aggregate
    ;

association_list_out :
      #(IDENT15 element_association (COMMA element_association)*)
    ;

allocator :
      ( NEW subtype_indication )
        => ( NEW subtype_indication )
    | ( NEW complex_name )
    ;

element_association :
      ( choices EQUAL_GREATER expression )
        => ( choices EQUAL_GREATER expression )
    | association
    ;

complex_name :
      attribute_name
    ;

attribute_name :
      selected_name ( QUOTE attribute )*
    ;

attribute :
      ( selected_name )
        => attribute_user_defined
    | attribute_active
    | attribute_ascending
    | attribute_base
    | attribute_delayed
    | attribute_driving_value
    | attribute_driving
    | attribute_event
    | attribute_high
    | attribute_image
    | attribute_instance_name
    | attribute_last_active
    | attribute_last_event
    | attribute_last_value
    | attribute_leftof
    | attribute_left
    | attribute_length
    | attribute_low
    | attribute_path_name
    | attribute_pos
    | attribute_pred
    | attribute_range
    | attribute_rightof
    | attribute_right
    | attribute_reverse_range
    | attribute_simple_name
    | attribute_stable
    | attribute_succ
    | attribute_transaction
    | attribute_quiet
    | attribute_value
    | attribute_val
    | ( aggregate )
    ;

attribute_user_defined :
      selected_name
    ;

attribute_active :
      ACTIVE
    ;

attribute_ascending :
      ASCENDING ( ( L_PAREN )
                    => L_PAREN expression R_PAREN )?
    ;

attribute_base :
      BASE
    ;

attribute_delayed :
      DELAYED ( ( L_PAREN )
                  => L_PAREN expression R_PAREN )?
    ;

attribute_driving_value :
      DRIVING_VALUE
    ;

attribute_driving :
      DRIVING
    ;

attribute_event :
      EVENT
    ;

attribute_high :
      HIGH ( ( L_PAREN )
               => L_PAREN expression R_PAREN )?
    ;

attribute_image :
      IMAGE L_PAREN expression R_PAREN
    ;

attribute_instance_name :
      INSTANCE_NAME
    ;

attribute_last_active :
      LAST_ACTIVE
    ;

attribute_last_event :
      LAST_EVENT
    ;

attribute_last_value :
      LAST_VALUE
    ;

attribute_leftof :
      #(LEFTOF L_PAREN expression R_PAREN)
    ;

attribute_left :
      #(LEFT ( ( L_PAREN )
                 => L_PAREN expression R_PAREN )?)
    ;

attribute_length :
      #(LENGTH ( ( L_PAREN )
                   => L_PAREN expression R_PAREN )?)
    ;

attribute_low :
      #(LOW ( ( L_PAREN )
                => L_PAREN expression R_PAREN )?)
    ;

attribute_path_name :
      PATH_NAME
    ;

attribute_pos :
      #(POS L_PAREN expression R_PAREN)
    ;

attribute_pred :
      #(PRED L_PAREN expression R_PAREN)
    ;

attribute_range :
      RANGE ( ( L_PAREN )
                => L_PAREN expression R_PAREN )?
    ;

attribute_rightof :
      #(RIGHTOF L_PAREN expression R_PAREN)
    ;

attribute_right :
      #(RIGHT ( ( L_PAREN )
                  => L_PAREN expression R_PAREN )?)
    ;

attribute_reverse_range :
      REVERSE_RANGE ( ( L_PAREN )
                        => L_PAREN expression R_PAREN )?
    ;

attribute_simple_name :
      SIMPLE_NAME
    ;

attribute_stable :
      #(STABLE ( ( L_PAREN )
                   => L_PAREN expression R_PAREN )?)
    ;

attribute_succ :
      #(SUCC L_PAREN expression R_PAREN)
    ;

attribute_transaction :
      TRANSACTION
    ;

attribute_quiet :
      #(QUIET ( ( L_PAREN )
                  => L_PAREN expression R_PAREN )?)
    ;

attribute_value :
      #(VALUE ( ( L_PAREN )
                  => L_PAREN expression R_PAREN )?)
    ;

attribute_val :
      #(VAL L_PAREN expression R_PAREN)
    ;

selected_name :
      indexed_name ( DOT ( indexed_name | ALL ( L_PAREN index_specifier R_PAREN )* ) )*
    ;

indexed_name :
      name ( L_PAREN index_specifier R_PAREN )*
    ;

index_specifier :
      ( expression direction )
        => discrete_range ( EQUAL_GREATER expression )?
    | association_list_out
    ;

slice_specifier :
      discrete_range
    ;

name :
      simple_name
    | STRING_LITERAL
    | CHARACTER_LITERAL
    ;

simple_name :
      identifier
    ;

alpha_literals :
      BIT_STRING_LITERAL
    | CHARACTER_LITERAL
    | STRING_LITERAL
    | NULL
    ;

physical_type_name :
      identifier
    ;

discrete_range :
      ( range )
        => ( range )
    | subtype_indication
    ;

direction :
      TO
    | DOWNTO
    ;

vhdl_label :
      identifier
    ;

integer_literal :
      DECIMAL_INTEGER_LITERAL
    | BASED_INTEGER_LITERAL
    ;

identifier :
      id:IDENTIFIER
    ;

chiffre_nombre :
	identifier
	| integer_literal
    ;

nature_declaration :
      #(NATURE identifier IS nature_definition SEMI_COLON)
    ;

nature_definition :
      scalar_nature_definition
    | array_nature_definition
    | record_nature_definition
    ;

terminal_declaration! :
      #(TERMINAL identifier_list COLON subnature_indication SEMI_COLON)
    ;

quantity_declaration! :
      #(QUANTITY quantity_declaration_tail SEMI_COLON)
    ;

quantity_declaration_tail :
      ( identifier_list COLON )
        => identifier_list COLON subtype_indication ( source_aspect | initialization )?
    | ( identifier_list ( TOLERANCE | COLON_EQUAL | ACROSS | THROUGH ) )
        => across_through_aspect_body
            ( ACROSS ( across_through_aspect_body THROUGH )? | THROUGH )
           terminal_aspect
    | terminal_aspect
    ;

across_through_aspect_body :
      identifier_list ( TOLERANCE expression )? ( initialization )?
    ;

terminal_aspect :
      complex_name ( TO complex_name )?
    ;

source_aspect :
      #(SPECTRUM COMMA simple_expression simple_expression)
    | NOISE simple_expression
    ;

step_limit_specification :
      #(LIMIT step_limit_specification_head COLON type_mark WITH expression SEMI_COLON)
    ;

step_limit_specification_head :
      #(IDENT16 selected_or_simple_name (COMMA selected_or_simple_name)*)
    | OTHERS
    | ALL
    ;

scalar_nature_definition :
      type_mark ACROSS type_mark THROUGH identifier REFERENCE
    ;

array_nature_definition :
      #(ARRAY array_nature_definition_head OF subnature_indication)
    ;

array_nature_definition_head :
      #(IDENT17 L_PAREN! array_dimension (COMMA array_dimension)* R_PAREN!)
    ;

record_nature_definition :
      #(RECORD ( nature_element_declaration )+ END RECORD ( simple_name )?)
    ;

nature_element_declaration :
      #(COLON identifier_list subnature_indication SEMI_COLON)
    ;

subnature_declaration :
      #(SUBNATURE IDENTIFIER IS subnature_indication SEMI_COLON)
    ;

subnature_indication :
      type_mark ( index_constraint )? ( TOLERANCE expression ACROSS expression THROUGH )?
    ;

concurrent_break_statement :
      BREAK ( break_element ( COMMA break_element )* )?
      ( ON complex_name ( COMMA complex_name )* )? ( WHEN expression )? SEMI_COLON
    ;

set_of_simultaneous_statements :
      ( concurrent_statement )*
    ;

simultaneous_statement :
       simultaneous_stmt
    ;

simultaneous_stmt :
      ( simple_expression EQUAL_EQUAL)
        => simple_simultaneous_statement
    | simultaneous_if_statement
    | simultaneous_case_statement
    | simultaneous_procedural_statement
    | simultaneous_null_statement
    ;

simple_simultaneous_statement :
      simple_expression EQUAL_EQUAL simple_expression ( TOLERANCE expression )? SEMI_COLON
    ;

simultaneous_if_statement :
      IF condition USE set_of_simultaneous_statements
      ( ELSIF condition USE set_of_simultaneous_statements )*
      ( ELSE set_of_simultaneous_statements )? END USE ( simple_name )? SEMI_COLON
    ;

simultaneous_case_statement :
      CASE expression USE ( WHEN choices EQUAL_GREATER set_of_simultaneous_statements )+
      END CASE ( simple_name )? SEMI_COLON
    ;

simultaneous_procedural_statement :
      #(PROCEDURAL ( IS )? ( procedural_declarative_item )* sequence_of_statements
      END PROCEDURAL ( simple_name )? SEMI_COLON)
    ;

procedural_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | variable_declaration
    | alias_declaration
    | attribute_declarative_item
    | use_clause
    | ( GROUP IDENTIFIER COLON )
        => group_declaration
    | group_template_declaration
    ;

simultaneous_null_statement :
      NULL SEMI_COLON
    ;

break_statement :
      BREAK ( break_element ( COMMA break_element )* )? ( WHEN expression )? SEMI_COLON
    ;

break_element! :
      ( FOR complex_name USE )? complex_name EQUAL_GREATER expression
    ;

protected_type_declaration :
      #(PROTECTED ( protected_type_declarative_item )* END PROTECTED ( simple_name )?)
    ;

protected_type_declarative_item :
      subprogram_declaration
    | attribute_specification
    | use_clause
    ;

protected_type_body :
      #(BODY PROTECTED ( protected_type_body_declarative_item )* END
              PROTECTED BODY ( simple_name )?
       )
    ;

protected_type_body_declarative_item :
      subprogram
    | type_declaration
    | subtype_declaration
    | constant_declaration
    | variable_declaration
    | file_declaration
    | alias_declaration
    | attribute_declarative_item
    | use_clause
    | ( GROUP IDENTIFIER COLON )
        => group_declaration
    | group_template_declaration
    ;

and_or_xor_xnor! :
       AND
     | OR
     | XOR
     | XNOR
     ;

nand_nor! :
      NAND
    | NOR
    ;

relational_operator! :  EQUAL | NOT_EQUAL | LESS | LESS_EQUAL | GREATER | GREATER_EQUAL ;

shift_operator!  : SLL | SRL | SLA | SRA | ROL | ROR ;

sign_operator!  : PLUS | MINUS ;

adding_operator! : PLUS | MINUS | AMPERSAND ;

multiplying_operator! : MULTIPLY | DIVIDE | MOD | REM ;

abs_not! : ABS | NOT ;
