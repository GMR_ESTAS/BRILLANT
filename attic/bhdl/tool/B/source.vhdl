i, j, kintt, Divid, Moduloa, b, c, d, sAA, BB, CC_IN, CC_OUT, SSaLZeroz, v, y, x, w, r, d, q, c, p, b, o, aCC, S1, S4, S3, S2, B4, A4, B3, A3, B2, A2, B1, A1a, b, c, d, iBIT1_1, BIT1_2, BIT1_3, BIT1_4, Integs, a, b, c, d, eResult, BIT1_1, BIT1_2, BIT1_3, BIT1_4, CCi, m, rInteg, Modulo, Resuls, b, aSUM, BB, AAENTITY InitZero IS

	PORT
	   (LZero:  OUT  Integer  
	);
END ; 
ENTITY TestEnumerate IS

	PORT
	   (AColor:  OUT  Color  
	);
END ; 
ENTITY Integer2Bit IS

	PORT
	   (BIT1_3, BIT1_4, BIT1_2, BIT1_1:  OUT  COLOR  ;Integ:  IN  BIT4  
	);
END ; 
ARCHITECTURE structure OF Integer2Bit IS 
L2L3COMPONENT ModDiv2 
	PORT
	   (intt:  IN  Integer  ;Divid:  OUT  Integer  ;Modulo:  OUT  BIT1  
	);
END COMPONENT ; BEGIN 
ModDiv2a:ModDiv2
PORT MAP ( Integ , L2 , BIT1_1 ) 
ModDiv2b:ModDiv2
PORT MAP ( L2 , L3 , BIT1_2 ) 
ModDiv2c:ModDiv2
PORT MAP ( L3 , BIT1_4 , BIT1_3 ) 
END ; ENTITY Add_4Bits IS

	PORT
	   (CC, S1, S4, S3, S2:  OUT  BIT1  ;B4, A4, B3, A3, B2, A2, B1, A1:  IN  BIT1  
	);
END ; 
ARCHITECTURE structure OF Add_4Bits IS 
L12, L13, L14LZeroCOMPONENT Add1B 
	PORT
	   (CC_IN:  IN  BIT1  ;CC_OUT:  OUT  BIT1  ;BB, AA:  IN  BIT1  ;SS:  OUT  BIT1  
	);
END COMPONENT ; COMPONENT InitZero 
	PORT
	   (LZero:  OUT  Integer  
	);
END COMPONENT ; BEGIN 
Add1B_1:Add1B
PORT MAP ( LZero , L12 , B1 , A1 , S1 ) 
Add1B_2:Add1B
PORT MAP ( L12 , L13 , B2 , A2 , S2 ) 
Add1B_3:Add1B
PORT MAP ( L13 , L14 , B3 , A3 , S3 ) 
Add1B_4:Add1B
PORT MAP ( L14 , CC , B4 , A4 , S4 ) 
InitZero1:InitZero
PORT MAP ( LZero ) 
END ; ENTITY AddInteger IS

	PORT
	   (SUM:  OUT  BIT5  ;BB, AA:  IN  BIT4  
	);
END ; 
ARCHITECTURE structure OF AddInteger IS 
L0, L1, L2, L3, L4, L5, L6, L7, L8, L9, L10, L14, L15COMPONENT Add_4Bits 
	PORT
	   (CC, S1, S4, S3, S2:  OUT  BIT1  ;B4, A4, B3, A3, B2, A2, B1, A1:  IN  BIT1  
	);
END COMPONENT ; COMPONENT Integer2Bit 
	PORT
	   (BIT1_3, BIT1_4, BIT1_2, BIT1_1:  OUT  BIT1  ;Integ:  IN  BIT4  
	);
END COMPONENT ; COMPONENT Bit2Integer 
	PORT
	   (CC:  IN  BIT1  ;Result:  OUT  BIT5  ;BIT1_1, BIT1_2, BIT1_3, BIT1_4:  IN  BIT1  
	);
END COMPONENT ; BEGIN 
Add4B:Add_4Bits
PORT MAP ( L10 , L6 , L9 , L8 , L7 , L5 , L14 , L4 , L15 , L3 , L1 , L2 , L0 ) 
Integer2Bit_A:Integer2Bit
PORT MAP ( L15 , L14 , L1 , L0 , AA ) 
Integer2Bit_B:Integer2Bit
PORT MAP ( L4 , L5 , L3 , L2 , BB ) 
Bit2Integer1:Bit2Integer
PORT MAP ( L10 , SUM , L6 , L7 , L8 , L9 ) 
END ; ENTITY Mul2 IS

	PORT
	   (Modulo:  IN  BIT1  ;Integ:  IN  Integer  ;Resul:  OUT  Integer  
	);
END ; 
ENTITY Add1B IS

	PORT
	   (CC_IN:  IN  BIT1  ;CC_OUT:  OUT  BIT1  ;BB, AA:  IN  BIT1  ;SS:  OUT  BIT1  
	);
END ; 
ENTITY ModDiv2 IS

	PORT
	   (intt:  IN  Integer  ;Divid:  OUT  Integer  ;Modulo:  OUT  BIT1  
	);
END ; 
ENTITY Bit2Integer IS

	PORT
	   (CC:  IN  BIT1  ;Result:  OUT  BIT5  ;BIT1_1, BIT1_2, BIT1_3, BIT1_4:  IN  BIT1  
	);
END ; 
ARCHITECTURE structure OF Bit2Integer IS 
L7L9L11L13L16COMPONENT Mul2 
	PORT
	   (Modulo:  IN  BIT1  ;Integ:  IN  Integer  ;Resul:  OUT  Integer  
	);
END COMPONENT ; COMPONENT InitZero 
	PORT
	   (LZero:  OUT  Integer  
	);
END COMPONENT ; BEGIN 
Mul2a:Mul2
PORT MAP ( BIT1_4 , L16 , L9 ) 
Mul2b:Mul2
PORT MAP ( BIT1_3 , L9 , L11 ) 
Mul2c:Mul2
PORT MAP ( BIT1_2 , L11 , L13 ) 
Mul2d:Mul2
PORT MAP ( BIT1_1 , L13 , Result ) 
InitZero2:InitZero
PORT MAP ( L7 ) 
Mul2d1:Mul2
PORT MAP ( CC , L7 , L16 ) 
END ; 