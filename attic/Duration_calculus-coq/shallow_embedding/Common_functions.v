(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import IL_base.

(**
All functions in this file should be considered as "calculation" functions,
  because they are considered in corresponding proof systems as
  side-conditions. 
*)

(** 
Definition of the rigidity of a formula. Valid for both pure Interval Logic
   and Duration Calculus
*)

(** 
Note that a state can be not rigid. Indeed  S(1) = l, thus any state 
  other than 0 will be flexible. For 0 states, as S(0)=0, replacing 
  through equality allows finishing testing the rigidity of a term 
*)

Inductive rigid_term : R -> Prop :=
  | rig_r0 : rigid_term 0%R
  | rig_r1 : rigid_term 1%R
  | rig_rbin :
      forall (F : R -> R -> R) (x y : R),
      rigid_term x /\ rigid_term y -> rigid_term (F x y)
  | rig_opp : forall x : R, rigid_term x -> rigid_term (- x)%R
  | rig_inv : forall x : R, rigid_term x -> rigid_term (/ x)%R
  | rig_sqrt : forall x : R, rigid_term x -> rigid_term (sqrt x).

Inductive rigid : Prop -> Prop :=
  | rig_true : rigid True
  | rig_false : rigid False
  | rig_chop : forall p q : Prop, rigid p /\ rigid q -> rigid (chop p q)
  | rig_imp : forall p q : Prop, rigid p /\ rigid q -> rigid (p -> q)
  | rig_and : forall p q : Prop, rigid p /\ rigid q -> rigid (p /\ q)
  | rig_or : forall p q : Prop, rigid p /\ rigid q -> rigid (p \/ q)
  | rig_not : forall p : Prop, rigid p -> rigid (~ p)
  | rig_ex : forall p : R -> Prop, rigid (p 0%R) -> rigid (ex p)
  | rig_all : forall p : R -> Prop, rigid (p 0%R) -> rigid (all p)
  | rig_rel :
      forall (r : R -> R -> Prop) (x y : R),
      rigid_term x /\ rigid_term y -> rigid (r x y).

(**
What is important in this inductive definition of rigidity is what is not
  defined, i.e. all the *Var "constructions", and $\ell$. They don't appear in
  the definition, so when using the tactic Rigidity defined below, you won't
  be able to state that a formula containing $\ell$ or a variable is
  rigid, which correspond to the definition of rigidity.
  Note that we don't define flexibility, as such a side-condition never
  appears in the axioms and inference rules of the proof system of IL or DC.
*)

Ltac Rigidity := repeat first [ constructor | split ].

Inductive chop_free : Prop -> Prop :=
  | cf_true : chop_free True
  | cf_false : chop_free False
  | cf_imp :
      forall p q : Prop, chop_free p /\ chop_free q -> chop_free (p -> q)
  | cf_and :
      forall p q : Prop, chop_free p /\ chop_free q -> chop_free (p /\ q)
  | cf_or :
      forall p q : Prop, chop_free p /\ chop_free q -> chop_free (p \/ q)
  | cf_not : forall p : Prop, chop_free p -> chop_free (~ p)
  | cf_ex : forall p : R -> Prop, chop_free (p 0%R) -> chop_free (ex p)
  | cf_all : forall p : R -> Prop, chop_free (p 0%R) -> chop_free (all p)
  | cf_rel : forall (F : R -> R -> Prop) (x y : R), chop_free (F x y).

Ltac ChopFree := repeat first [ constructor | split ].
