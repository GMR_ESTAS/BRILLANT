(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import IL_base.
Require Export IL_axioms.
Require Export IL_length.
Require Export IL_theorems_basic.
Require Export Common_functions.
Require Export Classical_Type.
Require Export PL_utils.
Require Export Fourier.

Definition HC1 := IL2.
Definition HC2 := IL3.

(** printing l %\ensuremath{\ell}% *)
(** printing ^^ %\ensuremath{\uchop}% *)
(** printing [[]] %\ensuremath{\wdcboth{}}% *)
(** printing <> %\ensuremath{\eventually}% *)
(** printing [] %\ensuremath{\always}% *)

Theorem IL15a :
 forall (x : R) (A B C : Prop),
 chop (l = x /\ A) B -> ~ chop (l = x /\ ~ A) C.
intros x A B C lxA_B.

cut (~ (exists y : _, chop (l = x /\ ~ A) (l = y))).
 apply contraposition_not.
 intro lxnA_C; apply exists_chop_right.
 Monotony.
 intros; apply IL11lem1.

apply all_not_not_ex with (P := fun y : R => chop (l = x /\ ~ A) (l = y)).
intros.
cut (chop (l = x /\ A) (l = n) \/ chop (l = x /\ A) (l <> n)).
 intro lndisj.
 elim lndisj.
  intro lxA_ln.
  cut (~ chop (~ A) (l = n)).
   unfold not in |- *; intros nnAln lnAln; apply nnAln.
   clear lxA_ln; Monotony; tauto.
  apply lx_chop_not_left; Monotony; tauto.

  intro lxAnln. 
  cut (~ chop (l = x) (~ l <> n)).
   apply contraposition_not.
   intro lxnAln; cut (chop (l = x) (l = n));
    [ intro lxln; Monotony; tauto | Monotony; tauto ].
   apply lx_chop_not_right; Monotony; tauto.
 
cut (chop (l = x /\ A) (l = n \/ l <> n)).
 apply distr_chop_or_right.
 Monotony; intros; apply classic.
Qed.

Theorem IL15b :
 forall (x : R) (A B C : Prop),
 chop A (l = x /\ B) -> ~ chop C (l = x /\ ~ B).
intros x A B C A_lxB.

cut (~ (exists y : _, chop (l = y) (l = x /\ ~ B))).
 apply contraposition_not.
 intro C_lxnB; apply exists_chop_left.
 Monotony.
 intros; apply IL11lem1.

apply all_not_not_ex with (P := fun y : R => chop (l = y) (l = x /\ ~ B)).
intros.
cut (chop (l = n) (l = x /\ B) \/ chop (l <> n) (l = x /\ B)).
 intro lndisj.
 elim lndisj.
  intro ln_lxB.
  cut (~ chop (l = n) (~ B)).
   unfold not in |- *; intros nlnnA lnlxB; apply nlnnA.
   clear ln_lxB; Monotony; tauto.
  apply lx_chop_not_right; Monotony; tauto.

  intro nlnlxB. 
  cut (~ chop (~ l <> n) (l = x)).
   apply contraposition_not.
   intro lnlxnB; cut (chop (l = n) (l = x));
    [ intro lnlx; Monotony; tauto | Monotony; tauto ].
   apply lx_chop_not_left; Monotony; tauto.
 
cut (chop (l = n \/ l <> n) (l = x /\ B)).
 apply distr_chop_or_left.
 Monotony; intros; apply classic.
Qed.

Theorem IL16alem1 :
 forall (B1 B2 : Prop) (x : R),
 chop (l = x) B1 /\ chop (l = x) B2 -> chop (l = x) (B1 /\ B2).
intros B1 B2 x lxB1lxB2.
cut (chop (l = x) (B1 /\ ~ ~ B2)).
intro lxB1nnB2; Monotony; intro B1nnB2; elim B1nnB2; intros; split;
 [ assumption | apply NNPP; assumption ].
apply chop_and_right.
elim lxB1lxB2; intros.
split; [ assumption | apply lx_chop_not_right; assumption ].
Qed.

Theorem IL16blem1 :
 forall (A1 A2 : Prop) (x : R),
 chop A1 (l = x) /\ chop A2 (l = x) -> chop (A1 /\ A2) (l = x).
intros A1 A2 x lxA1lxA2.
cut (chop (A1 /\ ~ ~ A2) (l = x)).
intro lxA1nnA2; Monotony; intro A1nnA2; elim A1nnA2; intros; split;
 [ assumption | apply NNPP; assumption ].
apply chop_and_left.
elim lxA1lxA2; intros.
split; [ assumption | apply lx_chop_not_left; assumption ].
Qed.

Theorem IL16alem2 :
 forall (A1 A2 B : Prop) (x : R),
 chop A1 (B /\ l = x) /\ chop A2 (l = x) -> chop (A1 /\ A2) (B /\ l = x).
intros A1 A2 B x A1BlxA2lx.
cut (chop (A1 /\ A2) (l = x /\ ~ (~ B /\ l = x))).
 intro A1A2nnBlx; Monotony.
 intro lxnnBlx. 
 cut (l = x /\ B \/ l = x /\ l <> x).
  intro lxBlxnlx.
  cut (l = x /\ B \/ False).
   intro lxBF; apply or_false; elim lxBF; [ left; tauto | right; tauto ].
  elim lxBlxnlx; intros hypo; [ left; assumption | idtac ].
  right; elim hypo; intros; absurd (l = x); [ assumption | assumption ].
 apply and_distr_or; elim lxnnBlx; intros; split; [ assumption | idtac ].
 cut (~ ~ B \/ l <> x).
  intro nnBlx; elim nnBlx; intros;
   [ left; apply NNPP; assumption | right; assumption ].
 apply not_and_or; assumption.
apply chop_and_right.
split.
 elim A1BlxA2lx; intros; apply IL16blem1; split;
  [ clear H0; Monotony; intro Blx; elim Blx; intros; assumption
  | assumption ].
cut (~ chop (A1 /\ A2) (l = x /\ ~ B)).
 apply contraposition_not; intros; Monotony; tauto.
apply IL15b with A1; elim A1BlxA2lx; intros; Monotony; tauto.
Qed.

Theorem IL16blem2 :
 forall (A B1 B2 : Prop) (x : R),
 chop (A /\ l = x) B1 /\ chop (l = x) B2 -> chop (A /\ l = x) (B1 /\ B2).
intros A B1 B2 x AlxB1lxB2.
cut (chop (l = x /\ ~ (~ A /\ l = x)) (B1 /\ B2)).
 intro lxnnAlxB1B2; Monotony.
 intro lxnnAlx. 
 cut (l = x /\ A \/ l = x /\ l <> x).
  intro lxAlxnlx.
  cut (l = x /\ A \/ False).
   intro lxAF; apply or_false; elim lxAF; [ left; tauto | right; tauto ].
  elim lxAlxnlx; intros hypo; [ left; assumption | idtac ].
  right; elim hypo; intros; absurd (l = x); [ assumption | assumption ].
 apply and_distr_or; elim lxnnAlx; intros; split; [ assumption | idtac ].
 cut (~ ~ A \/ l <> x).
  intro nnAlx; elim nnAlx; intros;
   [ left; apply NNPP; assumption | right; assumption ].
 apply not_and_or; assumption.
apply chop_and_left.
split.
 elim AlxB1lxB2; intros; apply IL16alem1; split;
  [ clear H0; Monotony; intro Blx; elim Blx; intros; assumption
  | assumption ].
cut (~ chop (l = x /\ ~ A) (B1 /\ B2)).
 apply contraposition_not; intros; Monotony; tauto.
apply IL15a with B1; elim AlxB1lxB2; intros; Monotony; tauto.
Qed.

Theorem IL16alem3 :
 forall (A1 A2 B : Prop) (x : R),
 chop A1 (B /\ l = x) /\ chop A2 (B /\ l = x) -> chop (A1 /\ A2) (B /\ l = x).
intros A1 A2 B x A1BlxA2Blx.
apply IL16alem2; elim A1BlxA2Blx; intros.
split; [ assumption | Monotony; tauto ].
Qed.

Theorem IL16blem3 :
 forall (A B1 B2 : Prop) (x : R),
 chop (A /\ l = x) B1 /\ chop (A /\ l = x) B2 -> chop (A /\ l = x) (B1 /\ B2).
intros A B1 B2 x AlxB1AlxB2.
apply IL16blem2; elim AlxB1AlxB2; intros.
split; [ assumption | Monotony; tauto ].
Qed.

Theorem IL16lem4 :
 forall (A B : Prop) (x y : R),
 chop (l = x) A /\ chop B (l = y) /\ chop (l = x) (l = y) ->
 chop (B /\ l = x) (A /\ l = y).
intros A B x y lxABlylxly.
elim lxABlylxly; clear lxABlylxly; intros lxA Blylxly; elim Blylxly;
 clear Blylxly; intros Bly lxly.
cut (chop (B /\ l = x) (l = y /\ A /\ l = y)).
 intros; Monotony; intro lyAly; elim lyAly; intros ly1 Aly; elim Aly;
  intros HA ly2; split; [ assumption | assumption ].
apply IL16blem2.
split.
apply IL16blem1; split; [ assumption | assumption ].
apply IL16alem1; split; [ assumption | assumption ].
Qed.

Theorem IL16a :
 forall (A1 A2 B1 B2 : Prop) (x : R),
 chop (A1 /\ l = x) B1 /\ chop (A2 /\ l = x) B2 ->
 chop ((A1 /\ A2) /\ l = x) (B1 /\ B2).
intros A1 A2 B1 B2 x A1lxB1A2lxB2.
elim A1lxB1A2lxB2; clear A1lxB1A2lxB2; intros A1lxB1 A2lxB2.
cut (chop ((A1 /\ l = x) /\ A2 \/ False) (B1 /\ B2)).
 intros; Monotony; tauto.
cut (chop ((A1 /\ l = x) /\ A2 \/ (A1 /\ l = x) /\ l <> x) (B1 /\ B2)).
 intros; Monotony; tauto.
cut (chop ((A1 /\ l = x) /\ (A2 \/ l <> x)) (B1 /\ B2)).
 intros; Monotony; tauto.
cut (chop ((A1 /\ l = x) /\ ~ (~ A2 /\ l = x)) (B1 /\ B2)).
 intros; Monotony.
 intro A1lxnnA2lx; elim A1lxnnA2lx; clear A1lxnnA2lx; intros A1lx nnA2lx.
 split; [ assumption | idtac ].
 cut (~ ~ A2 \/ l <> x).  
  intro nnA2olx; elim nnA2olx; intros;
   [ left; apply NNPP; assumption | right; assumption ].
 apply not_and_or; assumption.
apply chop_and_left.
split.
 apply IL16blem2.
 split; [ assumption | Monotony; tauto ].
cut (~ chop (l = x /\ ~ A2) (B1 /\ B2)).
 apply contraposition_not; intros; Monotony; tauto.
apply IL15a with B2.
Monotony; tauto.
Qed.

Theorem IL16b :
 forall (A1 A2 B1 B2 : Prop) (x : R),
 chop A1 (l = x /\ B1) /\ chop A2 (l = x /\ B2) ->
 chop (A1 /\ A2) (l = x /\ B1 /\ B2).
intros A1 A2 B1 B2 x A1lxB1A2lxB2.
elim A1lxB1A2lxB2; clear A1lxB1A2lxB2; intros A1lxB1 A2lxB2.
cut (chop (A1 /\ A2) ((l = x /\ B1) /\ B2 \/ False)).
 intros; Monotony; tauto.
cut (chop (A1 /\ A2) ((l = x /\ B1) /\ B2 \/ (A1 /\ l = x) /\ l <> x)).
 intros; Monotony; tauto.
cut (chop (A1 /\ A2) ((l = x /\ B1) /\ (B2 \/ l <> x))).
 intros; Monotony; tauto.
cut (chop (A1 /\ A2) ((l = x /\ B1) /\ ~ (~ B2 /\ l = x))).
 intros; Monotony.
 intro lxB1nnB2lx; elim lxB1nnB2lx; clear lxB1nnB2lx; intros lxB1 nnB2lx.
 split; [ assumption | idtac ].
 cut (~ ~ B2 \/ l <> x).  
  intro nnB2olx; elim nnB2olx; intros;
   [ left; apply NNPP; assumption | right; assumption ].
 apply not_and_or; assumption.
apply chop_and_right.
split.
 cut (chop (A1 /\ A2) (B1 /\ l = x));
  [ intros; clear A1lxB1; Monotony; tauto | apply IL16alem2 ].
 split; [ Monotony; apply and_comm; tauto | Monotony; tauto ].
cut (~ chop (A1 /\ A2) (l = x /\ ~ B2)).
 apply contraposition_not; intros; Monotony; tauto.
apply IL15b with A2; assumption.
Qed.

Theorem IL18a : forall A B C : Prop, always A /\ chop B C -> chop (A /\ B) C.
intros A B C alwABC.
elim alwABC; intros alwA BC.
Monotony; split.
 apply always_imp_present; assumption.
 assumption.
Qed.

Theorem IL18b : forall A B C : Prop, always A /\ chop B C -> chop B (A /\ C).
intros A B C alwABC.
elim alwABC; intros alwA BC.
Monotony; split.
 apply always_imp_present; assumption.
 assumption.
Qed.

(*
Theorem IL19a:``l>0``^^``l>0`` -> ``l>0``.
Intros lpos_lpos.
Cut (EXT x | ( EXT y | ``l==x+y``/\``x>0``/\``y>0``)).
Intro exxy; Elim exxy.
Intros x exy; Elim exy; Intros y posxposylxy.
Elim posxposylxy; Intros lxy posxposy; Elim posxposy; Intros posx posy.
Unfold Rgt in posx posy.
Rewrite lxy.
Unfold Rgt.
Apply gt0_plus_gt0_is_gt0; [Assumption | Assumption].
Cut (EXT x:R | (EXT y:R | (``l == x``^^``l==y``) /\``x > 0``/\``y > 0``)).
Intro exxy; Elim exxy.
Intros x exy; Elim exy; Intros y lxlyposxposy.
Exists x; Exists y.
Elim lxlyposxposy; Intros lxly posxposy; Elim posxposy; Intros posx posy.
Split; [Idtac | Split; [ Assumption | Assumption]].
Cut (``l == x+y``<->``l == x``^^``l == y``).
Intro lxyeqlxly; Elim lxyeqlxly; Intros lxyilxly lxlyilxy; Apply lxlyilxy; Assumption.
Apply interval_chopping; Split; [Left ; Assumption | Left; Assumption].
Cut (EXT x:R | (EXT y:R | (``l == x``/\``x > 0``)^^(``l == y``/\``y > 0``))).
Intro exeylxxposlyypos; Elim exeylxxposlyypos; Intros x eylxxposlyypos; Elim eylxxposlyypos; Intros y lxxposlyypos.
Exists x; Exists y.
Split.
 Cut (``l == x``/\``x>0``)^^``l == y``;[Intros; Monotony; Tauto | Monotony; Tauto].

*)
