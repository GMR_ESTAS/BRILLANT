(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import IL_base. 
Require Import IL_axioms. 
Require Import DC_base. 
Require Import DC_axioms.
Require Export Reals.

Fixpoint repetition (p:Prop) (n:nat) {struct n} :Prop :=
match n with
 | O => (length_eq 0%R)
 | (S k) => chop (repetition p k) p
end.

Fixpoint repetition_closure (p:Prop) (n:nat) {struct n} :Prop :=
match n with
 | O => repetition p 0
 | (S k ) => (repetition p n) \/ (repetition_closure p k)
end.

Parameter iteration : Prop -> Prop.

Axiom always_star : forall p :Prop, []((iteration p) <-> (forall n:nat, (repetition_closure p n))).

Theorem star_def : forall p:Prop, (iteration p) <-> (forall n:nat, (repetition_closure p n)).
intros.
apply IL2; apply always_star.
Qed.

Axiom IRDCstar : forall P : Prop -> Prop, 
  (forall n : nat, forall S:DCState,
    (P (repetition ([[ S ]] \/ [[ ~S ]]) n))) -> (P True).
