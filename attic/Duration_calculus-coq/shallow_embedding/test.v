Parameter chop : Prop -> Prop -> Prop.
Definition sometimes (P : Prop) := chop True (chop P True).

Definition always (P : Prop) := ~ sometimes (~ P).
Notation "<> P":=(sometimes P) (at level 34, no associativity).
Notation "[] P":=(always P) (at level 35, no associativity).

Axiom IL1: forall p q:Prop, [](p->q) -> []p -> []q.
Axiom IL2: forall p:Prop, []p -> p.

Check and_ind.

Axiom always_S : forall p q : Prop, [](p->q->p).
Axiom always_K : forall p q r: Prop, []((p->q) -> (p->q->r) -> (p->r)).


Axiom always_and_ind: forall p q r : Prop, []( (p -> q -> r) -> p /\ q -> r).
Axiom always_and : forall p q : Prop, [](p -> q -> p /\ q).

Theorem always_id : forall p : Prop, [](p->p).
intros.
apply IL1 with (p->(p->p)->p).
2:apply always_S.
apply IL1 with (p->p->p).
apply always_K.
apply always_S.
Qed.

Theorem always_Sprime : forall p q : Prop, [](p -> q -> q).
intros.
apply IL1 with (q->q).
apply always_S.
apply always_id.
Qed.

Theorem always_proj1: forall p q :Prop, [](p /\ q -> p).
intros p q.
apply IL1 with (p->q->p).
apply always_and_ind.
apply always_S.
Qed.


Theorem always_proj2: forall p q:Prop, [](p /\ q ->q).
intros p q.
cut ([](q/\p -> q)).
intros.
apply IL1 with (p->q->q).
apply always_and_ind.
apply always_Sprime.
apply IL1 with (q->p->q).
apply always_and_ind.
apply always_S.
Qed.

Theorem always_false : False -> []False.
apply False_ind; assumption.
Qed.

Axiom always_True_ind : forall p : Prop, [](p -> True -> p).
Axiom always_True : []True.

Theorem simpler : forall p q : Prop, [](p -> (p -> q) -> q).
intros.






Theorem gni_S:  forall p q : Prop, [](p->q->p).
intros p q.
apply IL1 with (p -> (True -> p) -> (q ->p)).
apply IL1 with (p -> True -> p).
apply always_K.
apply always_True_ind.


Axiom always_or_ind : forall p q r : Prop, []((p -> r) -> (q->r) -> p\/q->r).
Axiom always_or_introl : forall p q : Prop, [](p -> p \/ q).
Axiom always_or_intror : forall p q : Prop, [](q -> p \/ q).




Check ex_ind.
Check ex2_ind.
Check eq_ind.

