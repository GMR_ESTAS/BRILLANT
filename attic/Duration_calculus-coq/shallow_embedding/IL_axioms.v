(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import IL_base.
Require Import Common_functions.
Require Reals.
(* Unset Printing Notations. *)

(** printing l %\ensuremath{\ell}% *)
(** printing [] %\ensuremath{\always}% *)

Axiom IL1: forall p q:Prop, [] (p->q) -> []p -> []q.
Axiom IL2: forall p:Prop, []p -> p.

(* Axioms for the special predicate involving interval length l *)
Open Scope R_scope.

Axiom always_length_lt_not_eq: [] ( forall x:R,l<x -> l<>x ) .
Theorem length_lt_not_eq:forall x:R,l<x -> l<>x .
apply IL2; apply always_length_lt_not_eq. Qed.

Axiom always_length_eq_Rgt: [] ( forall x y:R,l=x -> x>y -> l>y ) .
Theorem length_eq_Rgt:forall x y:R,l=x -> x>y -> l>y .
apply IL2; apply always_length_eq_Rgt. Qed.

Axiom always_length_eq_Rlt: [] ( forall x y:R,l=x -> x<y -> l<y ) .
Theorem length_eq_Rlt : forall x y:R,l=x -> x<y -> l<y .
apply IL2; apply always_length_eq_Rlt. Qed.
Axiom always_length_eq_length_lt: [] ( forall x y:R,l=x -> l<y -> x<y ) .
Theorem length_eq_length_lt : forall x y:R,l=x -> l<y -> x<y .
apply IL2; apply always_length_eq_length_lt. Qed.
Axiom always_length_eq_length_eq: [] ( forall x y:R,l=x -> l=y -> x=y ) .
Theorem length_eq_length_eq : forall x y:R,l=x -> l=y -> x=y .
apply IL2; apply always_length_eq_length_eq. Qed.
Axiom always_length_eq_length_gt: [] ( forall x y:R,l=x -> l>y -> x>y ) .
Theorem length_eq_length_gt : forall x y:R,l=x -> l>y -> x>y .
apply IL2; apply always_length_eq_length_gt. Qed.

Axiom always_length_gt_Rgt: [] ( forall x y:R,l>x -> x>y -> l>y ) .
Theorem length_gt_Rgt : forall x y:R,l>x -> x>y -> l>y .
apply IL2; apply always_length_gt_Rgt. Qed.
Axiom always_length_gt_length_lt: [] ( forall x y:R,l>x -> l<y -> x<y ) .
Theorem length_gt_length_lt : forall x y:R,l>x -> l<y -> x<y .
apply IL2; apply always_length_gt_length_lt. Qed.
Axiom always_length_gt_length_eq: [] ( forall x y:R,l>x -> l=y -> x<y ) .
Theorem length_gt_length_eq : forall x y:R,l>x -> l=y -> x<y .
apply IL2; apply always_length_gt_length_eq. Qed.

Axiom always_length_lt_Rlt: [] ( forall x y:R,l<x -> x<y -> l<y ) .
Theorem length_lt_Rlt : forall x y:R,l<x -> x<y -> l<y .
apply IL2; apply always_length_lt_Rlt. Qed.
Axiom always_length_lt_length_eq: [] ( forall x y:R,l<x -> l=y -> y<x ) .
Theorem length_lt_length_eq: forall x y:R,l<x -> l=y -> y<x .
apply IL2; apply always_length_lt_length_eq. Qed.
Axiom always_length_lt_length_gt: [] ( forall x y:R,l<x -> l>y -> y<x ) .
Theorem length_lt_length_gt : forall x y:R,l<x -> l>y -> y<x .
apply IL2; apply always_length_lt_length_gt. Qed.

Axiom always_length_exists: [] ( exists x:R, l=x) .
Theorem length_exists : exists x:R, l=x.
apply IL2; apply always_length_exists. Qed.


(** - Now for the original axioms *)

(** printing ^^ %\ensuremath{\uchop}% *)
(** printing [[]] %\ensuremath{\wdcboth{}}% *)
(** printing <> %\ensuremath{\eventually}% *)

Axiom always_pos_interval : [] (l>= 0).
Theorem pos_interval : l >= 0.
apply IL2; apply always_pos_interval. Qed.

Definition A0 := pos_interval.

Axiom
  always_chop_and_right :
    forall p q r : Prop, [] (chop p q /\ ~ chop p r -> chop p (q /\ ~ r)).
Axiom
  always_chop_and_left :
    forall p q r : Prop, [] (chop p q /\ ~ chop r q -> chop (p /\ ~ r) q).
Theorem chop_and_right: forall p q r : Prop, chop p q /\ ~ chop p r -> chop p (q /\ ~ r).
intros p q r; apply IL2; apply always_chop_and_right. Qed.
Theorem chop_and_left: forall p q r : Prop, chop p q /\ ~ chop r q -> chop (p /\ ~ r) q.
intros p q r; apply IL2; apply always_chop_and_left. Qed.

Definition A1_right := chop_and_right.
Definition A1_left := chop_and_left.

Axiom
  always_chop_assoc : forall p q r : Prop, [] (chop (chop p q) r <-> chop p (chop q r)).
Theorem chop_assoc: forall p q r : Prop, chop (chop p q) r <-> chop p (chop q r).
intros p q r; apply IL2; apply always_chop_assoc. Qed.
Definition A2 := chop_assoc.

Theorem chop_assoc_left_right :
 forall P Q R : Prop, chop (chop P Q) R -> chop P (chop Q R).
intros P Q R chopleft.
cut (chop (chop P Q) R <-> chop P (chop Q R)).
intros H.
elim H; intros CLR CRL.
apply CLR; assumption.
apply chop_assoc.
Qed.


Theorem chop_assoc_right_left :
 forall P Q R : Prop, chop P (chop Q R) -> chop (chop P Q) R.
intros P Q R chopright.
cut (chop (chop P Q) R <-> chop P (chop Q R)).
intros H.
elim H; intros CLR CRL.
apply CRL; assumption.
apply chop_assoc.
Qed.
 
Ltac ChopAssoc :=
  match goal with
  |  |- (chop ?X1 (chop ?X2 ?X3)) => apply chop_assoc_left_right
  |  |- (chop (chop ?X1 ?X2) ?X3) => apply chop_assoc_right_left
  end.

Axiom always_rigid_chop_left : forall p q : Prop, rigid p -> [] (chop p q -> p).
Axiom always_rigid_chop_right : forall p q : Prop, rigid q -> [] (chop p q -> q).
Theorem rigid_chop_left : forall p q : Prop, rigid p -> chop p q -> p.
intros p q H; apply IL2; apply always_rigid_chop_left; assumption. Qed.
Theorem rigid_chop_right : forall p q : Prop, rigid q -> chop p q -> q.
intros p q H; apply IL2; apply always_rigid_chop_right; assumption. Qed.
Definition R_left := rigid_chop_left.
Definition R_right := rigid_chop_right.

Axiom
  always_exists_chop_left :
    forall (p : R -> Prop) (q : Prop),
    [] (chop (exists x : _, p x) q -> exists x : _, chop (p x) q).
Axiom
  always_exists_chop_right :
    forall (p : Prop) (q : R -> Prop),
    [] (chop p (exists x : _, q x) -> exists x : _, chop p (q x)).
Theorem 
  exists_chop_left :
    forall (p : R -> Prop) (q : Prop),
    chop (exists x : _, p x) q -> exists x : _, chop (p x) q.
intros p q; apply IL2; apply always_exists_chop_left. Qed.
Theorem
  exists_chop_right :
    forall (p : Prop) (q : R -> Prop),
    chop p (exists x : _, q x) -> exists x : _, chop p (q x).
intros p q; apply IL2; apply always_exists_chop_right. Qed.
Definition B_left := exists_chop_left.
Definition B_right := exists_chop_right.

Axiom
  always_lx_chop_not_right :
    forall (x : R) (p : Prop), [] (chop (l = x) p -> ~ chop (l = x) (~ p)).
Axiom
  always_lx_chop_not_left :
    forall (x : R) (p : Prop), [] (chop p (l = x) -> ~ chop (~ p) (l = x)).
Theorem
  lx_chop_not_right :
    forall (x : R) (p : Prop), chop (l = x) p -> ~ chop (l = x) (~ p).
intros x p; apply IL2; apply always_lx_chop_not_right. Qed.
Theorem
  lx_chop_not_left :
    forall (x : R) (p : Prop), chop p (l = x) -> ~ chop (~ p) (l = x).
intros x p; apply IL2; apply always_lx_chop_not_left. Qed.
Definition L1_right := lx_chop_not_right.
Definition L1_left := lx_chop_not_left.

Axiom
  always_interval_chopping :
    forall x y : R,
    [] ((x >= 0)%R /\ (y >= 0)%R -> (l = (x + y)%R <-> chop (l = x) (l = y))).
Theorem
  interval_chopping :
    forall x y : R,
    (x >= 0)%R /\ (y >= 0)%R -> (l = (x + y)%R <-> chop (l = x) (l = y)).
intros x y; apply IL2; apply always_interval_chopping. Qed.
Definition L2 := interval_chopping.

Axiom always_add_point_left : forall p : Prop, [] (p -> chop p (l = 0%R)).
Axiom always_add_point_right : forall p : Prop, [] (p -> chop (l = 0%R) p).
Theorem add_point_left : forall p : Prop, p -> chop p (l = 0%R).
intros p; apply IL2; apply always_add_point_left. Qed.
Theorem add_point_right : forall p : Prop, p -> chop (l = 0%R) p.
intros p; apply IL2; apply always_add_point_right. Qed.
Definition L3_left := add_point_left.
Definition L3_right := add_point_right.

Axiom
  quantification :
    forall (p : R -> Prop) (x : R),
    chop_free (p 0%R) \/ rigid_term x -> all (fun y => p y) -> p x.

Ltac Quantification term := apply quantification with (x := term).

(* 
Wrong axioms, because p->[]p (different from |- p --> |- []p) 
would become provable

Axiom necessitation_left : forall p q : Prop, p -> ~ chop (~ p) q. 
Axiom necessitation_right : forall p q : Prop, p -> ~ chop q (~ p).

Ltac NecessitationLeft := apply necessitation_left.
Ltac NecessitationRight := apply necessitation_right.

We leave them here as a reminder for avoiding thinking too naively
*)

Axiom
  monotony_left :
    forall p q : Prop, [] (p -> q) -> forall r : Prop, chop p r -> chop q r.
Axiom
  monotony_right :
    forall p q : Prop, [] (p -> q) -> forall r : Prop, chop r p -> chop r q.
Definition monotone_chop := monotony_left.
Definition chop_monotone := monotony_right.

(*
  A handy tactic to use monotony with associativity rules. 
 The "clear hyp" is here to avoid the loop of the tactic. 
 Sometimes the behaviour might do something undesirable (there are
 two hypotheses with which the goal can be matched). In that case consider
 using the monotony axioms directly.
*)


Ltac Monotony :=
  match goal with
  | _:(chop ?X1 (chop ?X2 ?X3)) |- (chop (chop ?X1 ?X4) ?X5) =>
      ChopAssoc; Monotony
  | _:(chop (chop ?X1 ?X2) ?X3) |- (chop ?X4 (chop ?X5 ?X3)) =>
      ChopAssoc; Monotony
  | hyp:(chop (chop ?X1 ?X2) ?X3) |- (chop ?X1 (chop ?X4 ?X5)) =>
      cut (chop X1 (chop X2 X3));
       [ clear hyp; intros; Monotony | ChopAssoc; assumption ]
  | hyp:(chop ?X1 (chop ?X2 ?X3)) |- (chop (chop ?X4 ?X5) ?X3) =>
      cut (chop (chop X1 X2) X3);
       [ clear hyp; intros; Monotony | ChopAssoc; assumption ]
  | _:(chop ?X1 ?X3) |- (chop ?X2 ?X3) =>
      apply monotony_left with (p := X1); [ idtac | assumption ]
  | _:(chop ?X3 ?X1) |- (chop ?X3 ?X2) =>
      apply monotony_right with (p := X1); [ idtac | assumption ]
  end.

(** 
Finally we complete the axioms of classical logic with their necessitated 
counterpart 
*)

Axiom always_S : forall p q : Prop, [] (p->q->p).
Axiom always_K : forall p q r: Prop, [] ((p->q) -> (p->q->r) -> (p->r)).


Axiom always_and_ind: forall p q r : Prop, [] ( (p -> q -> r) -> p /\ q -> r).
Axiom always_and : forall p q : Prop, [] (p -> q -> p /\ q).

Theorem always_id : forall p : Prop, [] (p->p).
intros.
apply IL1 with (p->(p->p)->p).
2:apply always_S.
apply IL1 with (p->p->p).
apply always_K.
apply always_S.
Qed.

Theorem always_Sprime : forall p q : Prop, [] (p -> q -> q).
intros.
apply IL1 with (q->q).
apply always_S.
apply always_id.
Qed.

Theorem always_proj1: forall p q :Prop, [] (p /\ q -> p).
intros p q.
apply IL1 with (p->q->p).
apply always_and_ind.
apply always_S.
Qed.


Theorem always_proj2: forall p q:Prop, [] (p /\ q ->q).
intros p q.
cut ([] (q/\p -> q)).
intros.
apply IL1 with (p->q->q).
apply always_and_ind.
apply always_Sprime.
apply IL1 with (q->p->q).
apply always_and_ind.
apply always_S.
Qed.

Theorem always_false : False -> []False.
apply False_ind; assumption.
Qed.

Axiom always_True_ind : forall p : Prop, [] (p -> True -> p).
Axiom always_True : []True.

(*
Theorem simpler : forall p q : Prop, [] (p -> (p -> q) -> q).
intros.



Theorem gni_S:  forall p q : Prop, [] (p->q->p).
intros p q.
apply IL1 with (p -> (True -> p) -> (q ->p)).
apply IL1 with (p -> True -> p).
apply always_K.
apply always_True_ind.
*)


Axiom always_or_ind : forall p q r : Prop, [] ((p -> r) -> (q->r) -> p\/q->r).
Axiom always_or_introl : forall p q : Prop, [] (p -> p \/ q).
Axiom always_or_intror : forall p q : Prop, [] (q -> p \/ q).

(*
Check ex_ind.
Check ex2_ind.
Check eq_ind.
*)
