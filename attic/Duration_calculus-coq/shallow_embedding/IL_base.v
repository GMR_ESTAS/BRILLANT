(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Export Reals.

Parameter length_eq : R -> Prop.
Parameter length_lt : R -> Prop.

Definition length_le (x:R):=(length_eq x) \/ (length_lt x).
Definition length_gt (x:R):=~(length_le x).
Definition length_ge (x:R):=(length_eq x) \/ (length_gt x).

Parameter chop : Prop -> Prop -> Prop.

(** A few handy definitions *)

Definition point := (length_eq 0).
Definition sometimes (P : Prop) := chop True (chop P True).
Definition always (P : Prop) := ~ sometimes (~ P).

(** 
- "sometime" reads %\emph{for some subinterval}%
- "always" reads %\emph{for all subintervals}%
*) 

(** printing ^^ %\ensuremath{\uchop}% *)
Infix "^^" := chop (at level 79, right associativity).


(** printing l %\ensuremath{\ell}% 
printint 'l' %\ensuremath{\ell}% 
*)
Notation "'l' = x" := (length_eq x) (at level 65, no associativity).
Notation "'l' <> x" := (~ (length_eq x)) (at level 65, no associativity).
Notation "'l' <= x" := (length_le x) (at level 65, no associativity).
Notation "'l' < x" := (length_lt x) (at level 65, no associativity).
Notation "'l' >= x" := (length_ge x) (at level 65, no associativity).
Notation "'l' > x" := (length_gt x) (at level 65, no associativity).


(** printing [[]] %\ensuremath{\wdcboth{}}% *)
Notation "[[]]":=point (at level 33, no associativity).
(** printing <> %\ensuremath{\eventually}% *)
Notation "<> P":=(sometimes P) (at level 34, no associativity).
(** printing [] %\ensuremath{\always}% *)
Notation "[] P":=(always P) (at level 35, no associativity).
