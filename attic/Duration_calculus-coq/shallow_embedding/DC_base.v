(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import IL_base. 

Require Export Reals.

(* 
State expressions of DC 
*)

Inductive DCState : Set :=
  | SZero : DCState
  | SOne : DCState
  | SOr : DCState -> DCState -> DCState
  | SNot : DCState -> DCState
  | SAnd : DCState -> DCState -> DCState
  | SImply : DCState -> DCState -> DCState
  | SIff : DCState -> DCState -> DCState.

Axiom
  SAnd_def : forall S T : DCState, SAnd S T = SNot (SOr (SNot S) (SNot T)).
Axiom SImply_def : forall S T : DCState, SImply S T = SOr (SNot S) T.
Axiom
  SIff_def : forall S T : DCState, SIff S T = SAnd (SImply S T) (SImply T S).

Parameter SVar : DCState -> DCState.

Parameter duration_eq:DCState -> R -> Prop.
Parameter duration_lt:DCState -> R -> Prop.

Definition duration_le S x:=(duration_eq S x) \/ (duration_lt S x).
Definition duration_gt S x:=~(duration_le S x) .
Definition duration_ge S x:=(duration_eq S x) \/ (duration_gt S x).

(* 
Function used with the DCA6 axiom of DC, which states that
two durations are equal if their state expressions are logically equivalent 
*)

Fixpoint proplogic (S : DCState) : Prop :=
  match S with
  | SZero => False
  | SOne => True
  | SOr S1 S2 => proplogic S1 \/ proplogic S2
  | SNot S1 => ~ proplogic S1
  | SAnd S1 S2 => proplogic S1 /\ proplogic S2
  | SImply S1 S2 => proplogic S1 -> proplogic S2
  | SIff S1 S2 => proplogic S1 <-> proplogic S2
  end. 

Delimit Scope states_scope with S.
Bind Scope states_scope with DCState.
Open Scope states_scope.
Notation "1":=(SOne) (at level 70, no associativity) : states_scope.
Notation "0":=(SZero) (at level 70, no associativity) : states_scope.

Notation "~ A":=(SNot A) (at level 75, right associativity) : states_scope.
Notation "A /\ B":=(SAnd A B) (at level 80, right associativity) : states_scope.
Notation "A \/ B":=(SOr A B) (at level 85, right associativity) : states_scope.
Notation "A --> B":=(SImply A B) (at level 90, right associativity) : states_scope.
Notation "A <-> B":=(SIff A B) (at level 95, no associativity) : states_scope.

Notation "! s !":=(proplogic s) (at level 0, no associativity).

Notation "# s #= x":=(duration_eq s x) (at level 0, no associativity).
Notation "# s #< x":=(duration_lt s x) (at level 0, no associativity).
Notation "# s #<= x":=(duration_le s x) (at level 0, no associativity).
Notation "# s #> x":=(duration_gt s x) (at level 0, no associativity).
Notation "# s #>= x":=(duration_ge s x) (at level 0, no associativity).
Notation "# s #<> x":=(~(duration_eq s x) )(at level 0, no associativity).

Close Scope states_scope.

