(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import IL_base.
Require Import IL_axioms.
Require Import DC_base.
Require Import DC_axioms.
Require Import DCstar_base.
Require Import Common_functions.

Theorem DCstar1 : forall P:Prop, (length_eq 0) -> (iteration P).
intros.
generalize (star_def P).
intro.
cut (iteration P <-> (forall n : nat, repetition_closure P n)).
intros.
elim H1; intros.
apply H3.
intros.
induction n.
compute; assumption.
right; assumption.
assumption.
Qed.

Theorem DCstar2 : forall P:Prop, (chop (iteration P) P) -> (iteration P).
intros.
generalize (star_def P).
intros.
elim H0; intros.
apply H2.
cut ( (forall n : nat, repetition_closure P n) ^^ P).
intros.



unfold iteration in H.

elim (star P).
intros.
apply H1.
intros.


Qed.


Axiom DCstar3 : forall P Q:Prop, 
  ((iteration P) /\ (chop Q True))
  ->
    (P /\ (chop (length_eq 0)(True)) )
  \/
    (chop (((iteration P) /\ (chop (~Q)(P))) /\ Q) True).


Check DCstar1.