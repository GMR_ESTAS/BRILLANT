(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import Classical_Type.

Theorem contraposition_not : forall A B : Prop, (A -> B) -> ~ B -> ~ A.
unfold not in |- *; intros; tauto.
Qed.

Theorem not_contraposition : forall A B : Prop, (~ A -> ~ B) -> B -> A.
intros.
apply NNPP; unfold not in |- *; intros; apply H.
assumption.
assumption.
Qed.

Theorem or_false : forall A : Prop, A \/ False -> A.
intros A AF.
elim AF; [ trivial | tauto ].
Qed.

Theorem false_or : forall A : Prop, False \/ A -> A.
intros A AF.
elim AF; [ tauto | trivial ].
Qed.

Theorem or_false_r : forall A : Prop, A -> A \/ False.
intros A HA; left; assumption.
Qed.

Theorem false_or_r : forall A : Prop, A -> False \/ A.
intros A HA; right; assumption.
Qed.

Theorem and_true : forall A : Prop, A /\ True -> A.
intros A AT.
elim AT; intros; assumption.
Qed.

Theorem true_and : forall A : Prop, True /\ A -> A.
intros A AT.
elim AT; intros; assumption.
Qed.

Theorem and_true_r : forall A : Prop, A -> A /\ True.
intros A HA.
split; [ assumption | trivial ].
Qed.

Theorem true_and_r : forall A : Prop, A -> True /\ A.
intros A AT.
split; [ trivial | assumption ].
Qed.

Theorem and_comm : forall A B : Prop, A /\ B -> B /\ A.
intros A B AB; elim AB; intros; split; [ assumption | assumption ].
Qed.

Theorem or_comm : forall A B : Prop, A \/ B -> B \/ A.
intros A B AB; elim AB;
 [ intros; right; assumption | intros; left; assumption ].
Qed.

Theorem and_distr_or : forall A B C : Prop, A /\ (B \/ C) -> A /\ B \/ A /\ C.
intros A B C AaBoC.
elim AaBoC; intros HA HBoC.
elim HBoC.
 intros; left; split; [ assumption | assumption ].
 intros; right; split; [ assumption | assumption ].
Qed.

Theorem and_distr_or_r :
 forall A B C : Prop, A /\ B \/ A /\ C -> A /\ (B \/ C).
intros A B C AaBoAaC.
elim AaBoAaC; intros Aand.
 elim Aand; intros; split; [ assumption | left; assumption ].
 elim Aand; intros; split; [ assumption | right; assumption ].
Qed.

Theorem or_distr_and :
 forall A B C : Prop, A \/ B /\ C -> (A \/ B) /\ (A \/ C).
intros A B C AoBaC.
elim AoBaC.
 intro HA; split; [ left; assumption | left; assumption ].
 intro BaC; elim BaC; intros; split;
  [ right; assumption | right; assumption ]. 
Qed.

Theorem or_distr_and_r :
 forall A B C : Prop, (A \/ B) /\ (A \/ C) -> A \/ B /\ C.
intros A B C; apply not_contraposition.
intro nAoBC.
apply or_not_and.
cut (~ A /\ ~ B \/ ~ A /\ ~ C).
 intro nAnBonAnC; elim nAnBonAnC; intro hypo.
 left; apply and_not_or; assumption.
 right; apply and_not_or; assumption.
apply and_distr_or.
cut (~ A /\ ~ (B /\ C)).
 intro nAnBC; elim nAnBC; intros; split;
  [ assumption | apply not_and_or; assumption ].
apply not_or_and; assumption.
Qed.