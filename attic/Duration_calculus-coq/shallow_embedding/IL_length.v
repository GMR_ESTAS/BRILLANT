(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import IL_base.
Require Import IL_axioms.
Require Import Common_functions.
Require Import Classical_Type.
Require Import PL_utils.
Require Import Fourier.

Theorem length_gt_not_eq:forall x:R, l>x -> ~l=x.
compute.
intros; apply H.
left; assumption.
Qed.

Theorem length_eq_not_gt:forall x:R,l=x ->  ~(l>x).
compute; intros.
apply length_gt_not_eq with x.
assumption.
assumption.
Qed.

Theorem length_eq_not_lt:forall x:R,l=x ->  ~(l<x).
compute; intros.
apply length_lt_not_eq with x.
assumption.
assumption.
Qed.

Theorem  length_diff_lt_gt:forall x:R,l<>x <-> l<x \/ l>x.
intros; split.
intros.
cut (~(~l<x /\ ~l>x)).
intro.
apply NNPP.
intro.
elim H0.
apply not_or_and; assumption.
intro.
elim H0; intros.
compute in H2.
tauto.
intros; elim H; intros.
apply length_lt_not_eq; assumption.
apply length_gt_not_eq; assumption.
Qed.

Theorem length_lt_gt_false:forall x:R, l<x /\ l>x -> False.
compute; intros.
elim H; intros.
apply H1; right; assumption.
Qed.

Theorem total_length:forall x:R, l<x \/ l=x \/ l>x.
compute; intros.
elim (classic (l  < x \/ l  = x)).
tauto.
tauto.
Qed.

Theorem length_lt_not_ge:forall x:R, l<x <-> ~ l>=x.
intros; split.
intro.
unfold length_ge.
apply and_not_or; split.
apply length_lt_not_eq; assumption.
intro; apply H0; right; assumption.
intro.
compute in H.
cut (l<>x /\ ~(l  = x \/ l  < x -> False)).
intros.
elim H0; intros.
cut (l<=x).
intros.
elim H3; intros.
contradiction.
assumption.
apply NNPP; assumption.
apply not_or_and; assumption.
Qed.

Theorem length_le_not_gt:forall x:R, l<=x <-> ~ l>x.
intros; split.
intro.
unfold length_gt.
tauto.
intro.
apply NNPP.
intro.
apply H.
assumption.
Qed.

Theorem length_eq_Rge:forall x y:R,l=x -> x>=y -> l>=y.
intros.
elim H0; intros.
right; apply length_eq_Rgt with x; assumption.
left; rewrite <- H1; assumption.
Qed.

Theorem length_eq_Rle:forall x y:R,l=x -> x<=y -> l<=y.
intros.
elim H0; intros.
right; apply length_eq_Rlt with x; assumption.
left; rewrite <- H1; assumption.
Qed.

Theorem length_eq_length_le:forall x y:R,l=x -> l<=y -> x<=y.
intros.
elim H0; intros.
right; apply length_eq_length_eq; assumption.
left; apply length_eq_length_lt; assumption.
Qed.

Theorem length_eq_length_ge:forall x y:R,l=x -> l>=y -> x>=y.
intros.
elim H0; intros.
right; apply length_eq_length_eq; assumption.
left; apply length_eq_length_gt; assumption.
Qed.

Theorem length_gt_Rge:forall x y:R,l>x -> x>=y -> l>y.
intros.
elim H0; intros.
apply length_gt_Rgt with x; assumption.
rewrite <- H1; assumption.
Qed.

Theorem length_ge_Rgt:forall x y:R,l>=x -> x>y -> l>y.
intros.
elim H; intros.
apply length_eq_Rgt with x; assumption.
apply length_gt_Rgt with x; assumption.
Qed.

Theorem length_ge_Rge:forall x y:R,l>=x -> x>=y -> l>=y.
intros.
elim H; intros.
apply length_eq_Rge with x; assumption.
right; apply length_gt_Rge with x; assumption.
Qed.

Theorem length_ge_length_lt:forall x y:R,l>=x -> l<y -> x<y.
intros.
elim H; intros.
apply length_lt_length_eq; assumption.
apply length_gt_length_lt; assumption.
Qed.

Theorem length_gt_length_le:forall x y:R,l>x -> l<=y -> x<y.
intros.
elim H0; intros.
apply length_gt_length_eq; assumption.
apply length_gt_length_lt; assumption.
Qed.

Theorem length_ge_length_le:forall x y:R,l>=x -> l<=y -> x<=y.
intros.
elim H; intros.
apply length_eq_length_le; assumption.
left; apply length_gt_length_le; assumption.
Qed.

Theorem length_ge_length_eq:forall x y:R,l>=x -> l=y -> x<=y.
intros.
apply Rge_le.
apply length_eq_length_ge; assumption.
Qed.

Theorem length_le_Rlt:forall x y:R,l<=x -> x<y -> l<y.
intros.
elim H; intros.
apply length_eq_Rlt with x; assumption.
apply length_lt_Rlt with x; assumption.
Qed.

Theorem length_lt_Rle:forall x y:R,l<x -> x<=y -> l<y.
intros.
elim H0; intros.
apply length_lt_Rlt with x; assumption.
rewrite <- H1; assumption.
Qed.

Theorem length_le_Rle:forall x y:R,l<=x -> x<=y -> l<=y.
intros.
elim H; intros.
apply length_eq_Rle with x; assumption.
right; apply length_lt_Rle with x; assumption.
Qed.

Theorem length_le_length_eq:forall x y:R,l<=x -> l=y -> y<=x.
intros.
elim H; intros.
apply length_eq_length_le; assumption.
left; apply length_lt_length_eq; assumption.
Qed.

Theorem length_le_length_gt:forall x y:R,l<=x -> l>y -> y<x.
intros.
elim H; intros.
apply length_gt_length_eq; assumption.
apply length_lt_length_gt; assumption.
Qed.

Theorem length_lt_length_ge:forall x y:R,l<x -> l>=y -> y<x.
intros.
elim H0; intros.
apply length_lt_length_eq; assumption.
apply length_lt_length_gt; assumption.
Qed.

Theorem length_le_length_ge:forall x y:R,l<=x -> l>=y -> y<=x.
intros.
elim H; intros.
apply length_ge_length_eq; assumption.
left; apply length_lt_length_ge; assumption.
Qed.

Theorem length_between:forall x:R, l<=x -> l>=x -> l=x.
intros.
elim H0; intros.
assumption.
contradiction.
Qed.
