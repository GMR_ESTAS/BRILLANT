(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Import IL_base.
Require Export IL_axioms.
Require Export IL_length.
Require Export Common_functions.
Require Export Classical_Type.
Require Export PL_utils.
Require Export Fourier.


(** printing l %\ensuremath{\ell}% *)
(** printing ^^ %\ensuremath{\uchop}% *)
(** printing [[]] %\ensuremath{\wdcboth{}}% *)
(** printing <> %\ensuremath{\eventually}% *)
(** printing [] %\ensuremath{\always}% *)


Theorem always_simple: forall p:Prop, [] (p -> p).
intros.
apply IL1 with (p -> (p -> p) -> p).
apply IL1 with (p-> p -> p).
apply always_K.
apply always_S.
apply always_S.
Qed.


Theorem sub_left:forall P:Prop, P -> P ^^ True.
intros.
cut (P ^^ l=0).
intro.
Monotony.
apply IL1 with (True).
apply always_S.
apply always_True.
apply add_point_left.
assumption.
Qed.

Theorem sub_right:forall P:Prop, P -> True ^^ P.
intros.
cut ((l=0) ^^ P).
intro.
Monotony.
apply IL1 with (True).
apply always_S.
apply always_True.
apply add_point_right.
assumption.
Qed.

Theorem always_imp_true: forall p:Prop, [] (p -> True).
intros.
apply IL1 with (True).
apply always_S.
apply always_True.
Qed.

Theorem diaR: forall p:Prop, p -> <>p.
intros.
unfold sometimes.
apply sub_right; apply sub_left.
assumption.
Qed.

Theorem absorb_true_right: forall p:Prop, (p^^True)^^True -> (p ^^ True).
intros.
cut (p^^True^^True).
intros.
apply monotony_right with (True ^^ True).
apply always_imp_true.
assumption.
ChopAssoc.
assumption.
Qed.

Theorem absorb_true_left: forall p:Prop, True ^^ True ^^ p-> (True ^^ p).
intros.
cut ((True^^True)^^p).
intros.
apply monotony_left with (True ^^ True).
apply always_imp_true.
assumption.
ChopAssoc.
assumption.
Qed.


Theorem left_false : True ^^ False -> False.
apply not_contraposition.
intros.
cut (~ (True ^^ (True -> False))).
apply contraposition_not.
intro.
apply monotony_right with False.
apply always_S.
assumption.
generalize always_True.
unfold always; unfold sometimes.
apply contraposition_not.
intros.
ChopAssoc.
apply sub_left.
assumption.
Qed.

Theorem right_false : False ^^ True -> False.
apply not_contraposition.
intros.
cut (~ ((True -> False ) ^^ True )).
apply contraposition_not.
intro.
apply monotony_left with False.
apply always_S.
assumption.
generalize always_True.
unfold always; unfold sometimes.
apply contraposition_not.
intros.
apply sub_right.
assumption.
Qed.

(*
Theorem sometimes_false : <>False -> False.
apply not_contraposition.
intro.
cut ([] True).
unfold always.
apply contraposition_not.
intro.
apply 

unfold sometimes; intros.
apply right_false.
apply monotony_left with (True ^^ False).
intro.




Theorem always_nnpp : forall p:Prop, [] ( ~~p -> p).
intros.
unfold always.
unfold sometimes.
intro.
cut( True ^^ False ^^ True).
intro.
*)


Theorem nn_absorb_true_right: forall p:Prop, (~~(p^^True))^^True -> (p ^^ True).
intros.
cut ((p^^True)^^True).
intros.
cut (p^^True^^True).
intros.
apply monotony_right with (True ^^ True).
apply always_imp_true.
assumption.
ChopAssoc.
assumption.
Monotony.
Qed.


(*
Theorem IL3: forall p:Prop, []p -> [][]p.
intros.
intro.
apply H.
cut (<><>~p).
intros.
unfold sometimes in H1.
cut ((True ^^ (True ^^ ~ p ^^ True)) ^^ True).
clear H1; intros.
cut (((True ^^ True) ^^ ~ p ^^ True) ^^ True).
clear H1; intros.
cut ((True ^^ True) ^^ ((~ p ^^ True) ^^ True)).
clear H1; intros.
cut ((True ^^ True) ^^ (~ p ^^ True) ^^ True

intro.

Qed.
*)

(* 
Irrelevant/Impossible in our encoding
Theorem IL4: forall p:Prop, p |- []p.
*)


Theorem IL5: forall p q:Prop, []p -> ~( (~p) ^^ q).
intros.
intro.
apply H.
cut ((~p)^^True).
intro.
unfold sometimes.
apply chop_assoc_left_right. 
Monotony.
apply necessitation_weak with True.
intros.
apply sub_right; assumption.
exact I.
Monotony.
apply necessitation_weak with True.
tauto.
exact I.
Qed.

Theorem IL5_bis: forall p q:Prop, []p -> ~( q ^^ (~p)).
intros; intro; apply H.
cut (True ^^ (~p)).
intro.
unfold sometimes.
Monotony.
apply necessitation_weak with True.
intros.
apply sub_left; assumption.
exact I.
Monotony.
apply necessitation_weak with True.
tauto.
exact I.
Qed.

(*
Impossible in our encoding
Theorem IL6: forall p q:Prop, ([]p -> q) |- ([]p -> [] q).
*)

Theorem IL7: forall a b c:Prop, [] (a -> b) -> (a ^^ c -> b ^^ c).
intros.
Monotony.
assumption.
Qed.

Theorem IL8: forall p q:Prop, [] (p -> q) -> [] ([]p -> []q).
intros.
apply necessitation_weak with ([] (p -> q)).
intros.
apply IL1 with p.
assumption.
assumption.
assumption.
Qed.

Theorem IL9: forall p:Prop, rigid p -> p -> []p.
intros p H.
apply not_contraposition.
intro.
cut (<> ~p).
intros.
apply rigid_chop_left with True.
Rigidity.
assumption.
apply rigid_chop_right with True.
Rigidity.
assumption.
assumption.
apply NNPP.
intro; apply H0.
assumption.
Qed.

(*
Theorem IL10a: forall (p q r: R -> Prop), rigid (p x) -> (forall x : R, (p x) -> (q x)^^(r x)) |- (forall x:R, (p x) -> (q x)).
*)

(*
Theorem IL11: forall p:Prop, (forall x:R, l=x -> p) |- p.
*)

Theorem IL12: forall p q:R -> Prop, (exists x : R,(p x) ^^ (q x)) ->
 (exists x : _, p x) ^^ (exists x : _, q x).
intros.
elim H.
intros.
cut ((exists x0 : R, p x0) ^^ (q x)).
intros.
Monotony.
apply necessitation_weak with True.
intros.
exists x; assumption.
exact I.
Monotony.
apply necessitation_weak with True.
intros; exists x; assumption.
exact I.
Qed.

Theorem IL13a: forall p:Prop, p^^False <-> False.
intros.
split.
apply not_contraposition.
intro.
cut (~(p ^^ (~True))).
intros.
intro; apply H0.
Monotony.
apply necessitation_weak with True.
tauto.
exact I.
apply IL5_bis.
apply necessitation_weak with True.
tauto.
tauto.
tauto.
Qed.

Theorem IL13b: forall p:Prop, False ^^ p <-> False.
intros.
split.
apply not_contraposition.
intro.
cut (~((~True) ^^ p)).
intros.
intro; apply H0.
Monotony.
apply necessitation_weak with True.
tauto.
exact I.
apply IL5.
apply necessitation_weak with True.
tauto.
tauto.
tauto.
Qed.

Theorem IL14a: forall p q r: Prop, (p \/q)^^r <-> p^^r \/ q ^^r.
intros; split.
intros.
cut (~~(p ^^ r) \/ q ^^ r).
intro.
elim H0.
intro; left; apply NNPP; assumption.
tauto.
apply imply_to_or.
intro.
cut (((p\/q) /\ ~p) ^^ r).
intro.
Monotony.
apply necessitation_weak with True.
tauto.
exact I.
apply chop_and_left.
tauto.
intro; elim H.
intro; Monotony.
apply necessitation_weak with True; [tauto | exact I].
intro; Monotony.
apply necessitation_weak with True; [tauto | exact I].
Qed.

Theorem IL14b: forall p q r: Prop, r ^^ (p \/q) <-> r^^p \/ r^^q.
intros; split.
intros.
cut (~~(r ^^ p) \/ r ^^ q).
intro.
elim H0.
intro; left; apply NNPP; assumption.
tauto.
apply imply_to_or.
intro.
cut (r ^^ ((p\/q) /\ ~p)).
intro.
Monotony.
apply necessitation_weak with True.
tauto.
exact I.
apply chop_and_right.
tauto.
intro; elim H.
intro; Monotony.
apply necessitation_weak with True; [tauto | exact I].
intro; Monotony.
apply necessitation_weak with True; [tauto | exact I].
Qed.

(*
Theorem IL15a: forall x:R, rigid x |- forall p q:Prop, (l=x /\ p)^^q -> ~((l=x /\ ~p) ^^ q).
*)



Theorem IL17a: forall p:Prop, (p ^^ l=0) -> p.
intro p.
apply not_contraposition; intros.
cut (~((~~p)^^(l=0))).
intros.
intro; apply H0; Monotony.
apply necessitation_weak with True.
tauto.
exact I.
apply lx_chop_not_left.
apply add_point_left.
assumption.
Qed.

Theorem IL17b: forall p:Prop, ((l=0) ^^ p) -> p.
intro p.
apply not_contraposition; intros.
cut (~((l=0)^^(~~p))).
intros.
intro; apply H0; Monotony.
apply necessitation_weak with True.
tauto.
exact I.
apply lx_chop_not_right.
apply add_point_right.
assumption.
Qed.


Theorem IL18a: forall p q r:Prop, []p /\ q^^r -> (p /\ q)^^r.
intros.
elim H; intros.
Monotony.
apply necessitation_weak with True.
intros; split.
apply IL2; assumption.
assumption.
exact I.
Qed.

Theorem IL18b: forall p q r:Prop, []p /\ q^^r -> q^^(p /\ r).
intros.
elim H; intros.
Monotony.
apply necessitation_weak with True.
intros; split.
apply IL2; assumption.
assumption.
exact I.
Qed.

Theorem IL19a: (l>0) ^^ (l>0) -> (l>0).
apply not_contraposition.
intro.
cut (l=0).
intro.
unfold length_gt.
apply IL5.
apply necessitation_weak with True.
intros; left; assumption.
exact I.
apply length_between.
unfold length_le in |- *.
unfold length_gt in H.
unfold length_le in H.
apply NNPP; assumption.
apply pos_interval.
Qed.

Theorem IL19b: (l>=0) ^^ (l>0) -> (l>0).
unfold length_ge.
intros.
cut (((l=0) ^^ (l>0)) \/ ((l>0) ^^ (l>0))).
intros.
elim H0.
intros.
apply IL17b; assumption.
apply IL19a.
generalize (IL14a (l=0) (l>0) (l>0)).
intros.
elim H0; intros.
apply H1; assumption.
Qed.

Theorem IL19c: (l>0) ^^ (l>=0) -> (l>0).
unfold length_ge.
intros.
cut (((l>0) ^^ (l=0)) \/ ((l>0) ^^ (l>0))).
intros.
elim H0.
intros.
apply IL17a; assumption.
apply IL19a.
generalize (IL14b (l=0) (l>0) (l>0)).
intros.
elim H0; intros.
apply H1; assumption.
Qed.

Theorem IL20: forall p q:Prop, [] (p /\ q) <-> []p /\ []q.
intros; split.
apply not_contraposition.
intro.
cut (<>(~p \/ ~q)).
intros; intro; apply H1.
unfold sometimes.
unfold sometimes in H0.
Monotony.
apply necessitation_weak with True.
intros.
Monotony.
apply necessitation_weak with True.
tauto.
exact I.
exact I.
cut (<>(~p) \/ <>(~q)).
intros; elim H0.
intros.
unfold sometimes; unfold sometimes in H1.
Monotony.
apply necessitation_weak with True.
intros.
Monotony.
apply necessitation_weak with True.
intros.
tauto.
exact I.
exact I.
intros.
unfold sometimes; unfold sometimes in H1.
Monotony.
apply necessitation_weak with True.
intros.
Monotony.
apply necessitation_weak with True.
intros.
tauto.
exact I.
exact I.
generalize (not_and_or ([]p) ([]q)).
intros.
elim H0.
intros; left; apply NNPP; assumption.
intros; right; apply NNPP; assumption.
assumption.




Theorem exists_distr_chop :
 forall A B : R -> Prop,
 (exists x : _, chop (A x) (B x)) ->
 chop (exists x : _, A x) (exists x : _, B x).
intros A B extAB.
apply NNPP.
cut (~ ~ (exists x : R, chop (A x) (B x)));
 [ idtac | unfold not in |- *; intros extAB_F; apply extAB_F; assumption ].
apply contraposition_not.
intros nextAextB.
apply all_not_not_ex with (P := fun y : R => chop (A y) (B y)).
unfold not in |- *; intros.
apply nextAextB.
cut (chop (A n) (exists x : R, B x));
 [ intros AnextB; Monotony; intros An; exists n; assumption | idtac ].
cut (chop (A n) (B n));
 [ intros AnBn; Monotony; intros Bn; exists n; assumption | idtac ].
assumption.
Qed.



Theorem HC3: forall p:Prop, []p -> <>p.
intros; apply diaR; apply HC1; assumption.
Qed.



