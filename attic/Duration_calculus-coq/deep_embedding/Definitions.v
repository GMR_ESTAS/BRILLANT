(*
    Copyright (c) 2002-2009 Samuel Colin

    This file is part of a double implementation of duration calculus
    in Coq as a shallow embedding and a deep embedding, which we shall
    refer to collectively as DC-coq.

    DC-coq is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DC-coq is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DC-coq.  If not, see <http://www.gnu.org/licenses/>.
*)


Require Export Reals.
Definition Name := nat.

Inductive State_expr : Type :=
  | SE0 : State_expr
  | SE1 : State_expr
  | State_var : Name -> State_expr
  | SENot : State_expr -> State_expr
  | SEOr : State_expr -> State_expr -> State_expr.

Definition SEAnd (p q : State_expr) := SENot (SEOr (SENot p) (SENot q)).
Definition SEImplies (p q : State_expr) := SEOr (SENot p) q.
Definition SEIff (p q : State_expr) := SEAnd (SEImplies p q) (SEImplies q p).

Inductive DCTerm : Type :=
  | RVar : Name -> DCTerm
  | length : DCTerm
  | RVal : R -> DCTerm
  | RDur : State_expr -> DCTerm
  | DCAdd : DCTerm -> DCTerm -> DCTerm
  | DCOpp : DCTerm -> DCTerm
  | DCMult : DCTerm -> DCTerm -> DCTerm
  | DCInv : DCTerm -> DCTerm.

Inductive Formula : Type :=
  | FTrue : Formula
  | FLetter : Name -> Formula
  | FNot : Formula -> Formula
  | FOr : Formula -> Formula -> Formula
  | FExists : (DCTerm -> Formula) -> Formula
  | FChop : Formula -> Formula -> Formula
  | Flt : DCTerm -> DCTerm -> Formula
  | Feq : DCTerm -> DCTerm -> Formula.

Definition FImplies (f g : Formula) := FOr (FNot f) g.
Definition FFalse := FNot FTrue.
Definition FAnd (f g : Formula) := FNot (FOr (FNot f) (FNot g)).
Definition FIff (f g : Formula) := FAnd (FImplies f g) (FImplies g f).
Definition FAll (f : DCTerm -> Formula) :=
  FNot (FExists (fun x => FNot (f x))).

Definition sometime (f : Formula) := FChop FTrue (FChop f FTrue).
Definition always (f : Formula) := FNot (sometime (FNot f)).

Definition Fle (x y : DCTerm) := FOr (Flt x y) (Feq x y).
Definition Fgt (x y : DCTerm) := Flt y x.
Definition Fge (x y : DCTerm) := FOr (Fgt x y) (Feq x y).
Definition Fne (x y : DCTerm) := FNot (Feq y x).

Definition DCMinus (x y : DCTerm) := DCAdd x (DCOpp y).
Definition DCDiv (x y : DCTerm) := DCMult x (DCInv y).

Definition pr (S : State_expr) :=
  FAnd (Feq (RDur S) length) (Fgt length (RVal 0)).
Definition point := Feq length (RVal 0).

Parameter plvalid : Formula -> Prop.


Delimit Scope states_scope with State.
Bind Scope states_scope with State_expr.
Open Scope states_scope.
Notation "a --> b" := (SEImplies a b) (at level 90, right associativity) : states_scope.
Notation "a <-> b" := (SEIff a b) (at level 95, no associativity) : states_scope.
Notation "a \/ b" := (SEOr a b) (at level 85, right associativity) : states_scope.
Notation "a /\ b" := (SEAnd a b) (at level 80, right associativity) : states_scope.
Notation "~ a" := (SENot a) (at level 75, right associativity) : states_scope.
Notation "1" := SE1 : states_scope.
Notation "0" := SE0 : states_scope.
Notation "'svar' n" := (State_var n) (at level 10, no associativity) : states_scope.
Close Scope states_scope.

Delimit Scope terms_scope with Term.
Bind Scope terms_scope with DCTerm.
Open Scope terms_scope.
Notation "a + b" := (DCAdd a b) (at level 50, left associativity) : terms_scope.
Notation "a - b" := (DCMinus a b) (at level 50, left associativity) : terms_scope.
Notation "a * b" := (DCMult a b) (at level 40, left associativity) : terms_scope.
Notation "a / b" := (DCDiv a b) (at level 40, left associativity) : terms_scope.
Notation "- a" := (DCOpp a) (at level 35, right associativity) : terms_scope.
Notation "/ a" := (DCInv a) (at level 35, right associativity) : terms_scope.
Notation "'rvar' n" := (RVar n) (at level 10, no associativity) : terms_scope.
Notation "# n #" := (RDur n) (at level 10, no associativity) : terms_scope.
Notation "'l'" := (length) (at level 10, no associativity) : terms_scope.
Close Scope terms_scope.

(*
Grammar constr dcterm:ast:=
 | dcsimple[ dcterm1($c) ] -> [ $c ]
with dcterm4:=
 |dcnumber [ rnatural:rnumber($c) ] -> [ <<(RVal $c)>> ]
 |dcid [ constr:global($i) ] -> [ $i ]
.
*)

Delimit Scope formulas_scope with Form.
Bind Scope formulas_scope with Formula.
Open Scope formulas_scope.
Notation "a = b" := (Feq a%Term b%Term) (at level 70, no associativity) : formulas_scope.
Notation "a <= b" := (Fle a%Term b%Term) (at level 70, no associativity) : formulas_scope.
Notation "a < b" := (Flt a%Term b%Term) (at level 70, no associativity) : formulas_scope.
Notation "a >= b" := (Fge a%Term b%Term) (at level 70, no associativity) : formulas_scope.
Notation "a > b" := (Fgt a%Term b%Term) (at level 70, no associativity) : formulas_scope.
Notation "a <> b" := (Fne a%Term b) (at level 70, no associativity) : formulas_scope.
Notation "'All' i | c" := (FAll (fun i => c)) (at level 190, no associativity) : formulas_scope. 
Notation "'Ex' i | c" := (FExists (fun i => c)) (at level 190, no associativity) : formulas_scope. 
Notation "a --> b" := (FImplies a b) (at level 90, right associativity) : formulas_scope.
Notation "a <-> b" := (FIff a b) (at level 95, no associativity) : formulas_scope.
Notation "a \/ b" := (FOr a b) (at level 85, right associativity) : formulas_scope.
Notation "a /\ b" := (FAnd a b) (at level 80, right associativity) : formulas_scope.
Notation "a ^^ b" := (FChop a b) (at level 77, right associativity) : formulas_scope.
Notation "~ a" := (FNot a) (at level 75, right associativity) : formulas_scope.
(* Notation "'FTrue'" := (FTrue) : formulas_scope.*)
(* Notation "'FFalse'" := (FFalse) : formulas_scope.*)
Close Scope formulas_scope.
