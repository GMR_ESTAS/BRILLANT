#!/bin/sh

if [ $# -ne 1 ]
then exit 1
fi

for i in *.mch
do
  sed -n -f ../try.sed <"$i" | (truc=""; while read -r a; do truc="$truc""$a"; done; echo "$truc" >>"$1")
done
