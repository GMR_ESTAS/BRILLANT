
/^Theorem/! {
  s/<:/⊆/g
  s/:/∈/g
  s/{}/∅/g
  s/&/∧/g
  s/or/∨/g

  s/<->/↔/g
  s/+->>/⤀/g
  s/+->/⇸/g
  s/>+>>/⤗/g
  s/>+>/⤔/g
  s/-->>/↠/g
  s/-->/→/g
  s/>->>/⤖/g
  s/>->/↣/g

  s/\*/×/g
  s/<<|/⩤/g
  s/|>>/⩥/g
  s/<|/◁/g
  s/|>/▷/g
  s/<+/⥷/g

  s/dom/domain/g
  s/ran/range/g

  s/\\\//∪/g
  s/\/\\/∩/g

  s/||/∥/g
  s/></⊗/g

  s/ ->/ ⇒/g
  s/~/∼/g
  s/|->/↦/g
  s/-/∖/g
  s/;/;/g
}
