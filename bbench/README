The purpose of the Bbench module is to provide the community examples of B
projects for:
- Testing tools
- Show advantages or shortcomings of the B method
It contains several examples of B projects, split among the
following subdirectories:

classic/ contains examples expressed with the "classical" B method

prime/ contains examples expressed in a not-yet-fully-specified
transformed B method. Thus this subdirectory is the most likely to get a
lot of changes

system/ contains examples expressed with a pre-version of event
B. They are here mostly for historical purposes (note : subjective
interpretation of yours truly)

eventB/ contains (will contain) examples expressed with event B.

Each project should contain a README at the root, and a spec subdirectory
containing the sources. The README contents :
- Must state the goal of the project
- Must contain a quantified description of the generated proof obligations
and validated proofs with at least one tool. The "hand-written proof" tool
is accepted as long as it has been peer-reviewed (hence examples from
proof-read books are accepted)
- Should state where it comes from (paper, book, case study, teaching
material, etc)
- Should contain a documentation (as for any other software)
- Might contain a reference to a paper (update Bmethod.bib accordingly)

As for the tool, trustworthiness depends on the following scale (less
trustworthy first), with an example between brackets when relevant, valid
at the time of writing (2007-02-15) :
0 - Proprietary tool in an alpha stage
1 - Open tool in an alpha stage [bgop, rodin]
2 - Proprietary tool in a beta (bug-fixing) stage
3 - Open tool in a beta stage [bparser, bphox]
4 - Proprietary tool in a mature stage [b4free, atelierB]
5 - Open tool in a mature stage [brillant hopefully some day]

Of course, when the tool consists in a chain of tools, its trustworthiness
is that of the weakest link.
