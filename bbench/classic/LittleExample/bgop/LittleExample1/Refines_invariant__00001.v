 

Require Import Blib.

Theorem op:
~ (~ ((0 = (Bmax (Union (Empty_set _) {= 0 =}))))).
try intuition 
   || unfold BBOOL ; intuition ; apply Full_intro
   || red ;  red ; red ; omega.
Qed.
 