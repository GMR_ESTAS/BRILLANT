 

Require Import Blib.

Theorem op:
forall _nn _yy _zz, ( 
 (In (Power_finite BN1) _yy) -> 
(_zz = (Bmax (Union _yy {= 0 =}))) -> 
(In BN1 _nn)
  -> (
 ~ (~ (((Bmax {= _zz, _nn =}) = (Bmax (Union (Union _yy {= _nn =}) {= 0 =}))))) ) )
 .
try intuition 
   || unfold BBOOL ; intuition ; apply Full_intro
   || red ;  red ; red ; omega.
Qed.
 