 

Require Import Blib.

Theorem op:
forall _yy _zz, ( 
 (In (Power_finite BN1) _yy) -> 
(_zz = (Bmax (Union _yy {= 0 =}))) -> 
(_yy <> (Empty_set _))
  -> (
 (_zz <> 0) ) )
 .
try intuition 
   || unfold BBOOL ; intuition ; apply Full_intro
   || red ;  red ; red ; omega.
Qed.
 