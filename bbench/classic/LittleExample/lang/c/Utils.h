/******************************************************************************

 File Name               : Utils.h

 Date                    : 15/03/2011  15:24:15

 Author                  : mariano

 Atelier-B Version       : 4.0

 C Translator Version    : b2c V1.0 (08/08/2007)

 B Project Name          : LittleExample

******************************************************************************/

#ifndef _Utils_h
#define _Utils_h

/*--------------------------
   Added by the Translator
  --------------------------*/
#include <stdbool.h>
/*------------------------
   INITIALISATION Clause
  ------------------------*/
extern void Utils__INITIALISATION(void);

/*--------------------
   OPERATIONS Clause
  --------------------*/
extern void Utils__maxi(
   int Utils__xx,
   int Utils__yy,
   int Utils__zz,
   int *Utils__res);
extern void Utils__mini(
   int Utils__xx,
   int Utils__yy,
   int *Utils__res);

#endif


