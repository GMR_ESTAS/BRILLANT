#include <stdio.h>
#include <stdlib.h>

#include "Utils.h"

int main(int argc, char** argv) {

  int x,y,z;

  x = 0;   y = x+1;   z = y+1;

  int res;   int res2;

  Utils__maxi(x,y,z, &res);
  Utils__mini(x,y, &res2);

  printf("Liste pour le maxi = {%d, %d, %d}\n", x,y,z );
  printf("Maxi = %d\n", res);
  printf("Liste pour le mini = {%d, %d}\n", x,y );
  printf("Mini = %d\n", res2);
  return (EXIT_SUCCESS);
}

