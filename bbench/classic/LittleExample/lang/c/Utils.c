/******************************************************************************

 File Name               : Utils.c

 Date                    : 15/03/2011  15:24:15

 Author                  : mariano

 Atelier-B Version       : 4.0

 C Translator Version    : b2c V1.0 (08/08/2007)

 B Project Name          : LittleExample

******************************************************************************/

/*--------------------------
   Added by the Translator
  --------------------------*/
#include <stdbool.h>
#include "Utils.h"

/*------------------------
   INITIALISATION Clause
  ------------------------*/
void Utils__INITIALISATION(void) {
}

/*--------------------
   OPERATIONS Clause
  --------------------*/
void Utils__mini(
   int Utils__xx,
   int Utils__yy,
   int *Utils__res) {
   if (Utils__xx >=
      Utils__yy)
   {
      *Utils__res = Utils__yy;
   }
   else {
      *Utils__res = Utils__xx;
   }
}

void Utils__maxi(
   int Utils__xx,
   int Utils__yy,
   int Utils__zz,
   int *Utils__res) {
   if (Utils__xx >=
      Utils__yy)
   {
      *Utils__res = Utils__xx;
   }
   if (Utils__xx <=
      Utils__yy)
   {
      *Utils__res = Utils__yy;
   }
   if (*Utils__res <=
      Utils__zz)
   {
      *Utils__res = Utils__zz;
   }
}



