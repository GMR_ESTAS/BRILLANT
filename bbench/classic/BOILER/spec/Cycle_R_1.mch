/*? **************************** DIGILOG ************************************
 * Date : @(#) sccs get %G%
 *
 * File : %M%
 *
 * Version : %I%
 *
 * Directory : %P%
 *
 * Type	:	MACHINE
 *
 * Object :
 *
 * Copyright : (C) 1996 Jean-Raymond ABRIAL / DIGILOG - STERIA Mediterranee
 *
 *   Permission is granted to make and distribute verbatim copies of
 *   this file provided the copyright notice and this permission notice
 *   are preserved on all copies.
 *
 *   Permission is granted to copy and distribute modified versions of
 *   this file under the conditions for verbatim copying, provided also
 *   that the entire resulting derived work is distributed under the terms
 *   of a permission notice identical to this one.
 *
 *   Permission is granted to copy and distribute translations of this
 *   manual into another language, under the above conditions for modified
 *   versions
 *
 *	 This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *   This program can be redistributed or modified under the terms of
 *   the GNU General Public License. The General Public License can be
 *	 found in the file COPYING tha accompanies the source code.
 *
 *---------------------------------------------------------------------------
 *
 * Functions :
 *			main_R
 *			read_rtk
 *			read_rok
 *			
 *---------------------------------------------------------------------------
 *
 * Modifications :
 *
 ************************************************************************* ?*/

MACHINE
	Cycle_R_1(pump_number)

CONSTRAINTS
	pump_number : NAT

SEES
	Constants_L_1

VISIBLE_VARIABLES
	rtk, rok

INVARIANT
	rtk : BOOL &
	rok : BOOL 

INITIALISATION
	rtk :: BOOL ||
	rok :: BOOL 

OPERATIONS
	main_R = 
		PRE
			pump_number : 1..NB_PUMP
		THEN
			rtk :: BOOL ||
			rok :: BOOL 
		END;

	rtk_a <-- read_rtk = 
		BEGIN
			rtk_a := rtk
		END;

	rok_a <-- read_rok = 
		BEGIN
			rok_a := rok
		END
END

