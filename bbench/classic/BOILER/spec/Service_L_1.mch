/*? **************************** DIGILOG ************************************
 * Date : @(#) sccs get %G%
 *
 * File : %M%
 *
 * Version : %I%
 *
 * Directory : %P%
 *
 * Type	:	MACHINE
 *
 * Object : Service concerning the water level instrument
 *
 * Copyright : (C) 1996 Jean-Raymond ABRIAL / DIGILOG - STERIA Mediterranee
 *
 *   Permission is granted to make and distribute verbatim copies of
 *   this file provided the copyright notice and this permission notice
 *   are preserved on all copies.
 *
 *   Permission is granted to copy and distribute modified versions of
 *   this file under the conditions for verbatim copying, provided also
 *   that the entire resulting derived work is distributed under the terms
 *   of a permission notice identical to this one.
 *
 *   Permission is granted to copy and distribute translations of this
 *   manual into another language, under the above conditions for modified
 *   versions
 *
 *	 This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *   This program can be redistributed or modified under the terms of
 *   the GNU General Public License. The General Public License can be
 *	 found in the file COPYING tha accompanies the source code.
 *
 *---------------------------------------------------------------------------
 *
 * Functions :
 *		read_level
 *		calculate_current_level
 *		level_test
 *		equipment_test
 *		determine_mode
 *		adjust_level
 *---------------------------------------------------------------------------
 *
 * Modifications :
 *
 ************************************************************************* ?*/

MACHINE
	Service_L_1

SEES
	Cycle_S_1, Cycle_Y_1, Cycle_X_1,
	Constants_L_1, Acq_1

SETS
	MODE = { rescue, degraded, normal}

VISIBLE_VARIABLES
	lim, lrm,
	lfam, qr,
	qa1, qa2,
	qc1, qc2,
	lfm, lram,
	lok, ltk,
	eqs, mode

INVARIANT
	lim : BOOL & lrm : BOOL & lfam : BOOL &
	qr : NAT &
	qa1 : NAT & qa2 : NAT & qa1 <= qa2 & qa2 <= CAP &
	qc1 : NAT & qc2 : NAT & qc1 <= qc2 & qc2 <= CAP &
	lfm : BOOL & lram : BOOL & lok : BOOL &
	ltk : BOOL & eqs : BOOL &
	mode : MODE 
	

INITIALISATION
	lim :: BOOL ||
	lrm :: BOOL ||
	lfam :: BOOL ||
	qr :: NAT ||
	qa1 := N1 ||
	qa2 := N2 ||
	qc1 := N1 ||
	qc2 := N2 ||
	lfm :: BOOL ||
	lram :: BOOL ||
	lok :: BOOL ||
	ltk :: BOOL ||
	eqs :: BOOL ||
	mode :: MODE 
	

OPERATIONS
	read_level =
		BEGIN
			lim := acq_lim ||
			lrm := acq_lrm ||
			lfam := acq_lfam ||
			qr := acq_qr
		END;

	calculate_current_level =
		LET xx, yy BE
			xx = qa1 -va2*DT - (((U1*DT*DT) + 1)/2) + pa1_tot*DT &
			yy = qa2 -va1*DT + (((U2*DT*DT) + 1)/2) + pa2_tot*DT
		IN
			qc1 := min({CAP, max({0, xx})}) ||
			qc2 := min({CAP, max({0, yy})})	
		END;

	level_test =
		BEGIN
			ltk := bool (lim = TRUE & qr <= CAP &
						 (lok = FALSE or lrm = FALSE) &
						 (lfam = FALSE or lfm = TRUE)) ||

			lfm := bool (((qr /: qc1..qc2) &
						 (lok = TRUE or lrm = TRUE)) or
						 (lfm = TRUE &  lfam = FALSE)) ||

			lok := bool ((qr : qc1..qc2) &
						 (lok = TRUE or lrm = TRUE)) ||

			lram := lrm

		END;

	equipment_test =
		BEGIN
			eqs := bool (ltk = TRUE & stk = TRUE & wtk_tot = TRUE & 
						 rtk_tot = TRUE &
						 (lok = TRUE or sok = TRUE) &
						 (lok = TRUE or wok_tot = TRUE))
		END;

	determine_mode =
		BEGIN
			IF (lok = FALSE) THEN
				mode := rescue
			ELSIF (sok = FALSE or wok_tot = FALSE or rok_tot = FALSE) THEN
				mode := degraded
			ELSE
				mode := normal
			END
		END;

	adjust_level =
		BEGIN
			IF (lok = TRUE) THEN
				qa1 := min({CAP, qr}) ||
				qa2 := min({CAP, qr})
			ELSE
				qa1 := qc1 ||
				qa2 := qc2
			END
		END

END

