/*? **************************** DIGILOG  ************************************
 * Date : (#) sccs get %G%
 *
 * File : %M%
 *
 * Version : %I%
 *
 * Directory : %P%
 *
 * Type	:	MACHINE
 *
 * Object : Interface between B software and C software
 *
 * Copyright : (C) 1996 Jean-Raymond ABRIAL / DIGILOG - STERIA Mediterranee
 *
 *   Permission is granted to make and distribute verbatim copies of
 *   this file provided the copyright notice and this permission notice
 *   are preserved on all copies.
 *
 *   Permission is granted to copy and distribute modified versions of
 *   this file under the conditions for verbatim copying, provided also
 *   that the entire resulting derived work is distributed under the terms
 *   of a permission notice identical to this one.
 *
 *   Permission is granted to copy and distribute translations of this
 *   manual into another language, under the above conditions for modified
 *   versions
 *
 *	 This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *   This program can be redistributed or modified under the terms of
 *   the GNU General Public License. The General Public License can be
 *	 found in the file COPYING tha accompanies the source code.
 *
 *---------------------------------------------------------------------------
 *
 * functions :
 *		main_Acquisition
 *		read_lim
 *		read_lfam
 *		read_lrm
 *		read_qr
 *		read_sim
 *		read_sfam
 *		read_srm
 *		read_vr
 *		read_wim
 *		read_wfam
 *		read_wrm
 *		read_pr
 *		read_rim
 *		read_rfam
 *		read_rrm
 *		read_rr
 *		read_cmd_pompe
 *		read_cmd_moniteur
 *---------------------------------------------------------------------------
 *
 * Modifications :
 *
 ************************************************************************* ?*/

MACHINE
	Acq_1

SEES
	Constants_L_1

VARIABLES
	/* messages concerning the water level measuring unit */
	acq_lim,
	acq_lfam,
	acq_lrm,
	acq_qr,
	/* messages concerning the steam level measuring unit */
	acq_sim,
	acq_sfam,
	acq_srm,
	acq_vr,
	/* messages concerning the controllers */
	acq_wim,
	acq_wfam,
	acq_wrm,
	acq_pr,
	/* messages concerning the pumps */
	acq_rim,
	acq_rfam,
	acq_rrm,
	acq_rr,

	/* memorization of the command pump */
	acq_pop_0,
	acq_pcl_0,
	acq_wpop_1,
	acq_wpcl_0

INVARIANT
	acq_lim : BOOL &
	acq_lfam : BOOL &
	acq_lrm : BOOL &
	acq_qr : NAT &
	
	acq_sim : BOOL &
	acq_sfam : BOOL &
	acq_srm : BOOL &
	acq_vr : NAT &
		
	acq_wim : 1..NB_PUMP --> BOOL &
	acq_wfam : 1..NB_PUMP --> BOOL &
	acq_wrm : 1..NB_PUMP --> BOOL  &
	acq_pr : 1..NB_PUMP --> NAT &
	acq_pr(1) : {0, PNOMINAL} &
	acq_pr(2) : {0, PNOMINAL} &
	acq_pr(3) : {0, PNOMINAL} &
	acq_pr(4) : {0, PNOMINAL} &
			
	acq_rim : 1..NB_PUMP --> BOOL &
	acq_rfam : 1..NB_PUMP --> BOOL  &
	acq_rrm : 1..NB_PUMP --> BOOL  &
	acq_rr : 1..NB_PUMP --> BOOL &

	acq_pop_0 : BOOL &
	acq_pcl_0 : BOOL &
	acq_wpop_1 : BOOL &
	acq_wpcl_0 : BOOL


INITIALISATION
	acq_lim :: BOOL ||
	acq_lfam :: BOOL ||
	acq_lrm :: BOOL ||
	acq_qr :: NAT ||
	
	acq_sim :: BOOL ||
	acq_sfam :: BOOL ||
	acq_srm :: BOOL ||
	acq_vr :: NAT ||
		
	acq_wim := (1..NB_PUMP) *{FALSE} ||
	acq_wfam := (1..NB_PUMP) *{FALSE} ||
	acq_wrm := (1..NB_PUMP) * {FALSE} ||
	acq_pr := (1..NB_PUMP) * {0} ||
			
	acq_rim := (1..NB_PUMP) * {FALSE} ||
	acq_rfam := (1..NB_PUMP) * {FALSE} ||
	acq_rrm := (1..NB_PUMP) * {FALSE} ||
	acq_rr := (1..NB_PUMP) * {FALSE} ||

	acq_pop_0 := FALSE ||
	acq_pcl_0 := FALSE ||
	acq_wpop_1 := FALSE ||
	acq_wpcl_0 := FALSE


OPERATIONS
	main_Acquisition =
		ANY val_l
		WHERE
			val_l : {0, PNOMINAL}
		THEN
			acq_lim :: BOOL ||
			acq_lfam :: BOOL ||
			acq_lrm :: BOOL ||
			acq_qr :: NAT ||
			
			acq_sim :: BOOL ||
			acq_sfam :: BOOL ||
			acq_srm :: BOOL ||
			acq_vr :: NAT ||
		
			acq_wim :: 1..NB_PUMP --> BOOL ||
			acq_wfam :: 1..NB_PUMP --> BOOL ||
			acq_wrm :: 1..NB_PUMP --> BOOL ||
			acq_pr := (1..NB_PUMP)*{val_l} ||
					
			acq_rim :: 1..NB_PUMP --> BOOL ||
			acq_rfam :: 1..NB_PUMP --> BOOL ||
			acq_rrm :: 1..NB_PUMP --> BOOL ||
			acq_rr :: 1..NB_PUMP --> BOOL
		END;

	bb <--read_lim =
		BEGIN
			bb := acq_lim
		END;

	bb <--read_lfam =
		BEGIN
			bb := acq_lfam
		END;

	bb <--read_lrm =
		BEGIN
			bb := acq_lrm
		END;

	measure <--read_qr =
		BEGIN
			measure := acq_qr
		END;

	bb <--read_sim =
		BEGIN
			bb := acq_sim
		END;

	bb <--read_sfam =
		BEGIN
			bb := acq_sfam
		END;

	bb <--read_srm =
		BEGIN
			bb := acq_srm
		END;

	measure <--read_vr =
		BEGIN
			measure := acq_vr
		END;

	bb <--read_wim(number) =
		PRE
			number : 1..NB_PUMP
		THEN
			bb := acq_wim(number)
		END;

	bb <--read_wfam(number) =
		PRE
			number : 1..NB_PUMP
		THEN
			bb := acq_wfam(number)
		END;

	bb <--read_wrm(number) =
		PRE
			number : 1..NB_PUMP
		THEN
			bb := acq_wrm(number)
		END;

	measure <--read_pr(number) =
		PRE
			number : 1..NB_PUMP
		THEN
			measure := acq_pr(number)
		END;

	bb <--read_rim(number) =
		PRE
			number : 1..NB_PUMP
		THEN
			bb := acq_rim(number)
		END;

	bb <--read_rfam(number) =
		PRE
			number : 1..NB_PUMP
		THEN
			bb := acq_rfam(number)
		END;

	bb <--read_rrm(number) =
		PRE
			number : 1..NB_PUMP
		THEN
			bb := acq_rrm(number)
		END;

	bb <--read_rr(number) =
		PRE
			number : 1..NB_PUMP
		THEN
			bb := acq_rr(number)
		END;

	pop_0_a, pcl_0_a <--read_cmd_pompe =
		BEGIN
			pop_0_a := acq_pop_0 ||
			pcl_0_a := acq_pcl_0
		END;

	wpop_1_a, wpcl_0_a <--read_cmd_moniteur =
		BEGIN
			wpop_1_a := acq_wpop_1 ||
			wpcl_0_a := acq_wpcl_0
		END

END

