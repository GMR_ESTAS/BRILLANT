/*? **************************** DIGILOG ************************************
 * Date : @(#) sccs get %G%
 *
 * File : %M%
 *
 * Version : %I%
 *
 * Directory : %P%
 *
 * Type	:	IMPLEMENTATION
 *
 * Object : Service which realize the controllers's synthesis
 *
 * Copyright : (C) 1996 Jean-Raymond ABRIAL / DIGILOG - STERIA Mediterranee
 *
 *   Permission is granted to make and distribute verbatim copies of
 *   this file provided the copyright notice and this permission notice
 *   are preserved on all copies.
 *
 *   Permission is granted to copy and distribute modified versions of
 *   this file under the conditions for verbatim copying, provided also
 *   that the entire resulting derived work is distributed under the terms
 *   of a permission notice identical to this one.
 *
 *   Permission is granted to copy and distribute translations of this
 *   manual into another language, under the above conditions for modified
 *   versions
 *
 *	 This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *   This program can be redistributed or modified under the terms of
 *   the GNU General Public License. The General Public License can be
 *	 found in the file COPYING tha accompanies the source code.
 *
 *---------------------------------------------------------------------------
 *
 * Functions :
 *		synthesis_water
 *
 *---------------------------------------------------------------------------
 *
 * Modifications :
 *
 ************************************************************************* ?*/

IMPLEMENTATION
	Service_X_2

REFINES
	Service_X_1

SEES
	Constants_L_1,
	W1.Cycle_W_1,
	W2.Cycle_W_1,
	W3.Cycle_W_1,
	W4.Cycle_W_1

INITIALISATION
	pa1_tot := 0;
	pa2_tot := 0;
	wtk_tot := TRUE;
	wok_tot := TRUE

OPERATIONS

	synthesis_water =
		VAR pa1_l1, pa1_l2, pa1_l3, pa1_l4,
			pa2_l1, pa2_l2, pa2_l3, pa2_l4,
			wtk_l1, wtk_l2, wtk_l3, wtk_l4,
			wok_l1, wok_l2, wok_l3, wok_l4
		IN
			pa1_l1, pa2_l1 <-- W1.read_pa;	
			pa1_l2, pa2_l2 <-- W2.read_pa;	
			pa1_l3, pa2_l3 <-- W3.read_pa;	
			pa1_l4, pa2_l4 <-- W4.read_pa;	
			wtk_l1 <-- W1.read_wtk;	
			wtk_l2 <-- W2.read_wtk;	
			wtk_l3 <-- W3.read_wtk;	
			wtk_l4 <-- W4.read_wtk;	
			wok_l1 <-- W1.read_wok;	
			wok_l2 <-- W2.read_wok;	
			wok_l3 <-- W3.read_wok;	
			wok_l4 <-- W4.read_wok;	

			pa1_tot := (pa1_l1 + pa1_l2 + pa1_l3 + pa1_l4);
			pa2_tot := (pa2_l1 + pa2_l2 + pa2_l3 + pa2_l4);
			wtk_tot := bool(wtk_l1 = TRUE & wtk_l2 = TRUE & 
						wtk_l3 = TRUE & wtk_l4 = TRUE);
			wok_tot := bool(wok_l1 = TRUE or wok_l2 = TRUE or 
						wok_l3 = TRUE or wok_l4 = TRUE)
		END
END

