/*? **************************** DIGILOG ************************************
 * Date : @(#) sccs get %G%
 *
 * File : %M%
 *
 * Version : %I%
 *
 * Directory : %P%
 *
 * Type	:	MACHINE
 *
 * Object : Service which realize the pumps's synthesis
 *
 * Copyright : (C) 1996 Jean-Raymond ABRIAL / DIGILOG - STERIA Mediterranee
 *
 *   Permission is granted to make and distribute verbatim copies of
 *   this file provided the copyright notice and this permission notice
 *   are preserved on all copies.
 *
 *   Permission is granted to copy and distribute modified versions of
 *   this file under the conditions for verbatim copying, provided also
 *   that the entire resulting derived work is distributed under the terms
 *   of a permission notice identical to this one.
 *
 *   Permission is granted to copy and distribute translations of this
 *   manual into another language, under the above conditions for modified
 *   versions
 *
 *	 This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 *   This program can be redistributed or modified under the terms of
 *   the GNU General Public License. The General Public License can be
 *	 found in the file COPYING tha accompanies the source code.
 *
 *---------------------------------------------------------------------------
 *
 * Functions :
 *		synthesis_pump
 *---------------------------------------------------------------------------
 *
 * Modifications :
 *
 ************************************************************************* ?*/

MACHINE
	Service_Y_1

SEES
	P1.Cycle_R_1,
	P2.Cycle_R_1,
	P3.Cycle_R_1,
	P4.Cycle_R_1

VISIBLE_VARIABLES
	rtk_tot,
	rok_tot

INVARIANT
	rtk_tot : BOOL &
	rok_tot : BOOL 

INITIALISATION
	rtk_tot :: BOOL ||
	rok_tot :: BOOL

OPERATIONS
	synthesis_pump =
		BEGIN
			rtk_tot := bool(P1.rtk = TRUE & P2.rtk = TRUE & 
						P3.rtk = TRUE & P4.rtk = TRUE) ||
			rok_tot := bool(P1.rok = TRUE or P2.rok = TRUE or 
							P3.rok = TRUE or P4.rok = TRUE)
		END
END

