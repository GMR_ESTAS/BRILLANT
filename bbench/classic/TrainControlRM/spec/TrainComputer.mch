/*"
TrainComputer: Models a train computer that imposes speed restrictions
		and applys emergency braking 
 
Written By: Michael Huynh, Alan Au and Puneet Jaiswal \\
Last Modified: 27 October 1997 \\
\\

\textbf{Definitions Explanation} \\
\begin{description}

\item[deaccelerating\_dist] Distance travelled at the current deacceleration
		  for the full duration of the given time period.

\item[acclerating\_dist] Distance travelled at the current acceleration for
		  the full duration of the given time period. This
		  distance is used if the new speed does not exceed
		  the target speed.

\item[dist\_to\_target] Distance travelled at the current acceleration before
		reaching the target speed.

\item[dist\_to\_zero]  Distance travelled at the current deacceleration before
	       reaching the zero speed.

\item[const\_speed\_dist] Distance travelled at the (constant) target speed for
		  the portion of the given time period left over after
		  reaching the target speed.

\item[time\_to\_target] Time needed to reach the target speed starting from 
		 the current speed and with the current acceleration.

\item[time\_to\_zero] Time needed to reach the zero speed starting from 
	       the current speed and with the current deacceleration.

\item[const\_time] Portion of time remaining of the given time period after
	     the time needed to reach the target speed.

\end{description}
"*/

MACHINE 
	TrainComputer(max_train_acc, stand_train_acc)
SEES
	SystemData, Time, SectionSets, RailSets, ControlDefs, Bool_TYPE

EXTENDS
	RailNet

CONSTRAINTS
	max_train_acc > 0 & 
	stand_train_acc > 0 
	
DEFINITIONS
	NOT_BEGIN == CSTATE - {begin};
	TRAIN_MODE == CSTATE - {begin, init_pending}  
	
VARIABLES
	trn_id, control_mode, train_target_speed, cur_spd, cur_acc, 
	acc_sign, on_track, at_offset, train_state

INVARIANT
	trn_id : TRAIN_ID &
	on_track : TRACK &
	at_offset : NAT &
	(control_mode : NOT_BEGIN => on_track : track) &

	cur_spd : NAT &
	cur_acc : NAT &
	acc_sign : SIGN &

	control_mode : CSTATE & 
	train_state : TSTATE &
	train_target_speed : NAT &

	((control_mode = deacc_override & train_state = new_status) => 
		((acc_sign = negative & cur_acc = max_train_acc) or 
		 (cur_acc = 0 & cur_spd = 0))) & 

	(control_mode : NOT_BEGIN => at_offset <= track_length_on) &

        (control_mode : NOT_BEGIN => size(track_sections(on_track)) >= 1) &

	(acc_sign /= zero => cur_acc > 0) &

	(acc_sign = zero => cur_acc = 0)

	/*((trn_speed > train_target_speed) =>
		(control_mode = deacc_override))
	  */

INITIALISATION
	control_mode := begin ||
	train_state := new_status ||
	train_target_speed := 0 ||
	cur_spd := 0 ||
	cur_acc := 0 ||
	acc_sign := zero || 
	at_offset := 0 ||
	ANY idd WHERE idd : TRAIN_ID THEN
		trn_id := idd
	END ||
	ANY trk WHERE trk : TRACK THEN
		on_track := trk
	END
  
OPERATIONS
	send_trk, m_acc, s_acc <-- request_init(trk) =
	PRE
		trk : track &
		size(track_sections(trk)) >= 1 &
		control_mode = begin
	THEN
		send_trk := trk ||
		m_acc := max_train_acc ||
		s_acc := stand_train_acc ||
		at_offset := 0 ||
		on_track := trk ||
		control_mode := init_pending
	END;

	init_train(idd) =
	PRE
		idd : TRAIN_ID &
		control_mode = init_pending
	THEN
		trn_id := idd ||
		control_mode := manual
	END;		 		

	state_update(tme) =
	PRE
		tme : TIME &
		control_mode : TRAIN_MODE
	THEN
	    train_state := new_status ||
	    IF control_mode = manual THEN
	        IF acc_sign = positive THEN
		    IF cur_spd + cur_acc * tme >= 
			train_target_speed
		    THEN
			cur_spd := train_target_speed ||
			cur_acc := 0 ||
			acc_sign := zero ||

                        IF cur_spd <= train_target_speed & /* This guard (PO) */
                           time_to_target <= tme THEN
			  on_track, at_offset <--
			  new_train_position(on_track, at_offset, 
						dist_to_target + const_speed_dist)
			ELSE
			  /* This is not extremely physically accurate. */
			  on_track, at_offset <--
			  new_train_position(on_track, at_offset, 
						train_target_speed * tme)
			END
		    ELSE
			cur_spd := cur_spd + cur_acc * tme ||
			on_track, at_offset <--
			new_train_position(on_track, at_offset, accelerating_dist)
		    END
		ELSIF acc_sign = negative THEN
		    IF cur_spd >= cur_acc * tme THEN
			cur_spd := cur_spd - cur_acc * tme ||
			on_track, at_offset <--
			new_train_position(on_track, at_offset, 
						deaccelerating_dist(cur_acc))
		    ELSE
			cur_acc := 0 ||
			acc_sign := zero ||
			cur_spd := 0 ||
			on_track, at_offset <-- 
			new_train_position(on_track, at_offset, 
						dist_to_zero(cur_acc)) 
		    END
		ELSE /* no acceleration */
			on_track, at_offset <--  
			new_train_position(on_track, at_offset, cur_spd * tme)
		END
	    ELSIF control_mode = deacc_override THEN
		IF cur_spd >= max_train_acc * tme THEN
			cur_spd := cur_spd - max_train_acc * tme ||
			acc_sign := negative ||
			cur_acc := max_train_acc ||
			on_track, at_offset <--
			new_train_position(on_track, at_offset, 
						deaccelerating_dist(max_train_acc))
		ELSE
			cur_spd := 0 ||
			cur_acc := 0 ||
			acc_sign := zero ||
			on_track, at_offset <-- 
			new_train_position(on_track, at_offset, 
						dist_to_zero(max_train_acc))
		END
	    END
	END;

	receive_instruction(mode, spd) =
	PRE
		mode : TRAIN_MODE &
		control_mode : TRAIN_MODE &
		spd : NAT
	THEN
		train_state := received_instruction ||
		control_mode := mode ||
		train_target_speed := spd ||
		IF mode = deacc_override THEN
			cur_acc := max_train_acc ||
			acc_sign := negative
		END
	END;

	new_acc, new_sgn <-- computer_change_acc(acc, sgn) =
	PRE
		acc : NAT &
		acc <= max_train_acc &
		sgn : SIGN &
		(sgn = zero => acc = 0) &
		(sgn /= zero => acc > 0) 
	THEN
		IF control_mode = manual THEN
			new_acc := acc ||
			new_sgn := sgn ||
			cur_acc := acc ||
			acc_sign := sgn
		ELSIF control_mode = deacc_override THEN
			new_acc := max_train_acc ||
			new_sgn := negative ||
			cur_acc := max_train_acc ||
			acc_sign := negative
		ELSE
			new_acc := 0 ||
			new_sgn := zero
		END
	END;

	/* The following operations were originally implemented directly by Punneet 
	   in C. */

	spd <-- return_speed =
	  BEGIN 
		spd := cur_spd
	  END;

	off <-- return_offset =
	  BEGIN
		off := at_offset
	  END;

	trk <-- return_track =
	  BEGIN
		trk := on_track
	  END;

	acc <-- return_acc =
	  BEGIN
		acc := cur_acc
	  END;

	accsign <-- return_acc_sgn =
	  BEGIN
		accsign := acc_sign
	  END;

	cm <-- return_control_mode =	
	  BEGIN
		cm := control_mode
	  END;

	/*
	The following are operations added recently by Hong Chen, they are used 
	to check preconditions. 
	*/

	ok <-- valid_offset_query(trk, off) =
	    PRE
		trk : track &
		off : OFFSET
	    THEN
		ok := bool(off <= track_length)
	    END;

	ok <-- add_crossover_query(trk, off, end) =
	  PRE
	    trk : track &
	    off : OFFSET &
	    end : track &
	    size(track_sections(trk)) >= 1 &
	    off <= track_length
	  THEN
	    ok := bool(this_section : ran(track_sections(end)))
	  END;

	ok <-- control_mode_query(state) =
	  PRE
		state : CSTATE
	  THEN
		ok := bool(control_mode = state)
	  END

END

