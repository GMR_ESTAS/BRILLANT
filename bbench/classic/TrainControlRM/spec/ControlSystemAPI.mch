MACHINE           ControlSystemAPI(bubble_size, cross_speed)

SEES		  Responses, SectionSets, RailSets, 
		  SystemData, ControlDefs, Bool_TYPE

INCLUDES	  ControlSystem(bubble_size, cross_speed)

PROMOTES	  save_railnet, restore_railnet, save, restore

OPERATIONS

  	response, sect <-- start_sectionR(grad, leng, spd) =
	    PRE
		grad : GRADIENT &
		leng : LENGTH &
		spd : SPEED
	    THEN
		IF card(section) < maxsection THEN
			sect <-- start_section(grad, leng, spd) ||
			response := OK
		ELSE
			sect :: SECTION || response :: REFUSED_RESPONSES
		END
	    END;

	response, sect <-- next_sectionR(p_sect, grad, leng, spd) =
	    PRE
		p_sect : SECTION &
		grad : GRADIENT &
		leng : LENGTH &
		spd : SPEED
	    THEN
		IF p_sect : section & card(section) < maxsection THEN
			sect <-- next_section(p_sect, grad, leng, spd) ||
			response := OK
		ELSE
			sect :: SECTION || response :: REFUSED_RESPONSES
		END
	    END;

	response, sect <-- merge_sectionR(p_sect1, p_sect2,  grad, leng, spd) =
	    PRE
		p_sect1 : SECTION &
		p_sect2 : SECTION &
		grad : GRADIENT &
		leng : LENGTH &
		spd : SPEED
	    THEN
		IF p_sect1 : section & p_sect2 : section &
		  card(section) < maxsection THEN
		    	sect <-- merge_section(p_sect1, p_sect2,  grad, leng, spd) ||
			response := OK
		ELSE
			sect :: SECTION || response :: REFUSED_RESPONSES 
		END
	    END;

	response, grad <-- section_gradientR(sect) =
	    PRE
		sect : SECTION 
	    THEN
		IF sect : section THEN
			grad <-- section_gradient(sect) ||
			response := OK
		ELSE
			grad :: GRADIENT || response :: REFUSED_RESPONSES
		END
	    END;

	response, leng <-- section_lengthR(sect) =
	    PRE
		sect : SECTION 
	    THEN
		IF sect : section THEN
			leng <-- section_length(sect) ||
			response := OK
		ELSE
			leng :: LENGTH || response :: REFUSED_RESPONSES
		END
	    END;

	response, spd <-- section_speedlimitR(sect) =
	    PRE
		sect : SECTION 
	    THEN
		IF sect : section THEN
			spd <-- section_speedlimit(sect) ||
			response := OK
		ELSE
			spd :: SPEED || response :: REFUSED_RESPONSES
		END
	    END;

	response, trck <-- new_trackR(dir) =
	    PRE
		dir : DIRECTION
	    THEN
		IF card(track) < maxtrack THEN
		    trck <-- new_track(dir) ||
		    response := OK
		ELSE
		    trck :: TRACK || response :: REFUSED_RESPONSES
		END
	    END;

	response <-- extend_trackR(trk, sect) =
	    PRE
		trk : TRACK &
		sect : SECTION
	    THEN
		IF trk : track &
		   sect : section &
		   sect /: ran(track_sections(trk)) &
		   (track_sections(trk) /= <> & track_dir(trk) = right =>
		        sect : next(last(track_sections(trk)))) &
		   (track_sections(trk) /= <> & track_dir(trk) = left =>
			last(track_sections(trk)) : next(sect))
		THEN
		    extend_track(trk, sect) ||
		    response := OK
		ELSE
		    response :: REFUSED_RESPONSES
		END
	    END;

	response, stat <-- new_stationR(trk, off) = 
	    PRE
		trk : TRACK &
		off : OFFSET
	    THEN
		IF trk : track &
		   off <= track_length &  
		   card(station) < maxstation
		THEN
		   stat <-- new_station(trk, off) ||
		   response := OK
		ELSE
		   stat :: STATION || response :: REFUSED_RESPONSES
		END
	    END;

	response, cross <-- add_crossoverR(trk, off, end) =
	    PRE
		trk : TRACK &
		off : OFFSET &
		end : TRACK
	    THEN
		IF trk : track &
		   end : track &
		   off < track_length & 
		   card(crossover) < maxcross &
		   trk /= end &
		   track_dir(trk) = track_dir(end) &
		   this_section : ran(track_sections(end))
		THEN
		   cross <-- add_crossover(trk, off, end) ||
		   response := OK
		ELSE
		   cross :: CROSSOVER || response :: REFUSED_RESPONSES
		END
	    END;

	response <-- connect_crossR(cross) =
	    PRE
		cross : CROSSOVER
	    THEN
		IF cross : crossover &
		   cross_from(cross) /: ran(cross_on) THEN
		    connect_cross(cross) ||
		    response := OK
		ELSE
		    response :: REFUSED_RESPONSES
		END
	    END;

	response <-- disconnect_crossR(cross) =
	    PRE
		cross : CROSSOVER
	    THEN
		IF cross : crossover THEN
		    disconnect_cross(cross) ||
		    response := OK
		ELSE
		    response :: REFUSED_RESPONSES
		END
	    END;

	response, sect, off <-- track_queryR(trk, ii) =
	    PRE
		trk : TRACK &
		ii : NAT1
	    THEN
		IF trk : track &
		   size(track_sections(trk)) >= ii
		THEN
		   sect, off <-- track_query(trk, ii) ||
		   response := OK
		ELSE
		   sect :: SECTION || off :: OFFSET || response :: REFUSED_RESPONSES
		END
	    END;

	response, trk, off <-- cross_from_queryR(cross)	=
	    PRE
		cross : CROSSOVER
	    THEN
		IF cross: crossover THEN
		   trk, off <-- cross_from_query(cross) ||
		   response := OK
		ELSE
		   trk :: TRACK || off :: OFFSET || response :: REFUSED_RESPONSES
		END
	    END;

	response, trk <-- cross_to_queryR(cross) = 
	    PRE
		cross : CROSSOVER
	    THEN
		IF cross: crossover THEN
		    trk <-- cross_to_query(cross) ||
		    response := OK
		ELSE
		    trk :: TRACK || response :: REFUSED_RESPONSES
		END
	    END;

	response, cstat <-- cross_stat_queryR(cross) =
	    PRE
		cross : CROSSOVER
	    THEN
		IF cross: crossover THEN
		    cstat <-- cross_stat_query(cross) ||
		    response := OK
		ELSE
		    cstat :: CSTATUS || response :: REFUSED_RESPONSES
		END
	    END;

	response, len <-- track_length_queryR(trk) =
	    PRE
		trk : TRACK
	    THEN
		IF trk : track THEN
		    len <-- track_length_query(trk) ||
		    response := OK
		ELSE
		    len :: LENGTH || response :: REFUSED_RESPONSES
		END
	    END;

	response, thissect <-- this_section_queryR(trk,off) =
	    PRE
		trk: TRACK &
		off : OFFSET
	    THEN
		IF trk: track &
		   size(track_sections(trk)) >= 1 &
		   off <= track_length
		THEN
		   thissect <-- this_section_query(trk,off) ||
		   response := OK
		ELSE
		   thissect :: SECTION || response :: REFUSED_RESPONSES
		END
	    END;

	response, nextsect <-- next_section_queryR(trk,off) = 
	    PRE
		trk : TRACK &
		off : OFFSET
	    THEN
		IF trk : track & 
		   size(track_sections(trk)) >= 2 &
		   off < last(track_offsets(trk))
		THEN
		   nextsect <-- next_section_query(trk,off) ||
		   response := OK
		ELSE
		   nextsect :: SECTION || response :: REFUSED_RESPONSES
		END
	    END;

	response, exists <-- nextsection_exists_queryR(trk, off) =
	    PRE
		trk : TRACK &
		off : OFFSET
	    THEN
		IF trk : track THEN
		    exists <-- nextsection_exists_query(trk, off) ||
		    response := OK
		ELSE
		    exists := FALSE || response :: REFUSED_RESPONSES
		END
	    END;

	response, sectoff <-- section_offset_queryR(trk,off) =
	    PRE
		trk : TRACK &
		off : OFFSET
	    THEN
		IF trk : track &
		   size(track_sections(trk)) >= 1 &
		   off <= track_length 
		THEN
		   sectoff <-- section_offset_query(trk,off) ||
		   response := OK
		ELSE
		   sectoff :: NAT || response :: REFUSED_RESPONSES
		END
	    END;

	response, trkoff <-- convert_offsetR(trk, sect, off) =
	    PRE
		trk : TRACK &
		sect : SECTION &
		off : OFFSET
	    THEN
		IF trk : track & 
		   sect : section & 
	 	   sect : ran(track_sections(trk))
		THEN
		   trkoff <-- convert_offset(trk, sect, off) ||
		   response := OK
		ELSE
		   trkoff :: NAT || response :: REFUSED_RESPONSES
		END
	    END;

	response, schd <-- new_scheduleR =
	    BEGIN
		IF schedule /= SCHEDULE THEN
		    schd <-- new_schedule || response := OK
		ELSE
		    response :: REFUSED_RESPONSES
		END
	    END;

	response <-- extend_station_scheduleR(sch, stat)  =
	    PRE
		sch : SCHEDULE &
		stat : STATION
	    THEN
		IF sch : schedule &
		   stat : station &
 		   stat /: ran(timetable(sch))
		THEN
		   extend_station_schedule(sch, stat) ||
		   response := OK
		ELSE
		   response :: REFUSED_RESPONSES
		END
	    END;

	response <-- extend_cross_scheduleR(sch,cross) = 
	    PRE
		sch : SCHEDULE &
		cross : CROSSOVER 
	    THEN
		IF sch: schedule &
		   cross : crossover &
		   cross /: ran(cross_sched(sch)) &
		   (cross_sched(sch) /= <> => 
		     cross_to(last(cross_sched(sch))) = trk_elem(cross_from(cross)))
		THEN
		   extend_cross_schedule(sch,cross) ||
		   response := OK
		ELSE
		   response :: REFUSED_RESPONSES
		END
	    END;

	response, idd <-- register_trainR(trk, macc, sacc, schd) =
	    PRE
		trk : TRACK &
		macc : NAT &
		sacc : NAT &
		schd : SCHEDULE
	    THEN
		IF trk : track &
		   schd : schedule &	
		   card(train_id) < maxtrain &
		   size(track_sections(trk)) >= 1
		THEN
		   idd <-- register_train(trk, macc, sacc, schd) ||
		   response := OK
		ELSE
		   idd :: TRAIN_ID || response :: REFUSED_RESPONSES
		END
	    END;

	response <-- remove_trainR(idd) =
	    PRE
		idd : TRAIN_ID
	    THEN
		IF idd  : train_id THEN
		    remove_train(idd) ||
		    response := OK
		ELSE
		    response :: REFUSED_RESPONSES
		END
	    END;

	response, spd, mde <-- issue_instructionR(idd) =
	    PRE
		idd : TRAIN_ID
	    THEN
		IF idd : train_id &
		   idd /: update_pending
		THEN
		   spd, mde <-- issue_instruction(idd) ||
		   response := OK
		ELSE
		   spd :: NAT || mde :: CSTATE || response :: REFUSED_RESPONSES
		END
	    END;

	response <-- update_control_systemR(idd,trk,off,spd) = 
	    PRE
		idd : TRAIN_ID &
		trk : TRACK &
		off: NAT &
		spd: NAT
	    THEN
		IF idd: train_id &
		   trk : track &
		   off <= track_length &
		   idd : update_pending &
		   size(track_sections(trk)) >= 1
		THEN
		   update_control_system(idd,trk,off,spd) ||
		   response := OK
		ELSE
		   response :: REFUSED_RESPONSES
		END
	    END

END
