/*
Section:

Written By: Michael Huynh, Alan Au and Puneet Jaiswal
Last Modified: 25 September 1997
*/

MACHINE
	Section

SEES
	SectionSets, Bool_TYPE
	
/* DEFINITIONS

	GRADIENT == NAT1;
	LENGTH == NAT1;
	SPEED == NAT1
*/

VARIABLES
	
	section, next, gradient, length, speed_limit

INVARIANT

	section	<: SECTION &
	next 	: section --> POW(section) &
	gradient : section --> GRADIENT &
	length 	: section --> LENGTH &
	speed_limit : section --> SPEED

INITIALISATION
	
	section, next, gradient, length, speed_limit := {}, {}, {}, {}, {}

OPERATIONS

	sect <-- start_section(grad, leng, spd) =
	PRE
		grad : GRADIENT &
		leng : LENGTH &
		spd : SPEED &
		card(section) < maxsection
	THEN
		ANY oo WHERE oo : SECTION - section THEN
			section := section \/ {oo} ||
			next(oo) := {} || 
			gradient(oo) := grad ||
			length(oo) := leng || 
			speed_limit(oo) := spd ||
			sect := oo 
		END
	END;

	sect <-- next_section(p_sect, grad, leng, spd) =
	PRE
		p_sect : section &
		grad : GRADIENT &
		leng : LENGTH &
		spd : SPEED &
		card(section) < maxsection
	THEN
		ANY oo WHERE oo : SECTION - section THEN
			section := section \/ {oo} ||
			next := next <+ {p_sect |-> (next(p_sect) \/ {oo}), 
				oo |-> {}} ||  
			gradient(oo) := grad ||
			length(oo) := leng || 
			speed_limit(oo) := spd ||
			sect := oo 
		END
	END;

	sect <-- merge_section(p_sect1, p_sect2,  grad, leng, spd) =
	PRE
		p_sect1 : section &
		p_sect2 : section &
		grad : GRADIENT &
		leng : LENGTH &
		spd : SPEED &
		card(section) < maxsection
	THEN
		ANY oo WHERE oo : SECTION - section THEN
			section := section \/ {oo} || 
			gradient(oo) := grad ||
			length(oo) := leng || 
			speed_limit(oo) := spd ||
			sect := oo ||
			next := next <+ {p_sect1 |-> (next(p_sect1) \/ {oo}),
				p_sect2 |-> (next(p_sect2) \/ {oo}), 
				oo |-> {}} 
		END
	END;

	/*	
remove_section(sect) =
	PRE
		sect : section &
		next(sect) = {}
	THEN
		section := section - {sect} 
	||	next := %(sec).(sec : section & sec /= sect | 
				    next(sec) - {sect}) 
	|| 
		next := {sec, nextsecs | sec : section &
			sec /= sect & nextsecs : POW(section) & 
			nextsecs = next(sec) - {sect}} 
	|| 
		gradient := BEGIN {sect} <<| gradient 
	||	length := {sect} <<| length 
	||	speed_limit := {sect} <<| speed_limit
			END;
		END; 
	*/

	grad <-- section_gradient(sect) =
		PRE
			sect : section
		THEN
			grad := gradient(sect)
		END;

	leng <-- section_length(sect) =
		PRE
			sect : section
		THEN		
			leng := length(sect)
		END;

	spd <-- section_speedlimit(sect) = 
		PRE
			sect: section 
		THEN
			spd := speed_limit(sect)
		END;

	/*
	The following are operations added recently by Hong Chen, they are used 
	to check preconditions. 
	*/

	ok <-- more_sections_query =
		BEGIN
			ok := bool(card(section) < maxsection)
		END;

	ok <-- section_exists_query(sect) =
		PRE
			sect : SECTION
		THEN
			ok := bool(sect : section)
		END;

	sect <-- any_section =
		BEGIN
			sect :: SECTION
		END 
END


