/*? **************************************************************
 *
 * File : $Id: $
 *
 *
 * Object : Basic Machine
 * 
 * ---------------------------------------------------------------
 * Copyright (c) 1996 - GEC ALSTHOM Transport and STERIA - 
 * ---------------------------------------------------------------
 *
 * History : $Log: $
 *
 *****************************************************************?*/



MACHINE
        B_Operateur

SETS
        ORDRE_OPERATEUR = { Marche , Arret }

CONCRETE_VARIABLES
        teda                        /*? Time between 2 stops ?*/
,        avance                        /*? total amount of the DAB ?*/
,        ordre                        /*? functionning state of the DAB,
				 * equals Marche or Arret ?*/

INVARIANT
        teda   : NAT
&        avance : NAT
&        ordre  : ORDRE_OPERATEUR
&        ( ordre = Marche <=> ( 1 <= teda ) )
        /*? if the deadline is not reached the state of the DAB is Marche ?*/

INITIALISATION
        teda   :: NAT1
||        avance := 0
||        ordre  := Marche

OPERATIONS

 /*?
  * The banker turns on the dab and decides about the amount of the
  * cash register
  ?*/
avnc <-- montant_avance =
        ANY som WHERE
                som : NAT
        &        som > 900
        THEN
                teda   :: NAT1
        ||        ordre  := Marche
        ||        avance := som
        ||        avnc   := som
        END
;
 /*?
  * the DAB is enabled (state Marche).
  * Check that no stop order has been sent by the banker.
  * The time between 2 stops is finite but unknown, it is represented by teda,
  * a decreasing positive variable
  ?*/
ordr <-- ordre_operateur =
        PRE
                1 <= teda
        THEN
                ANY oo WHERE
                        oo : ORDRE_OPERATEUR
                &        ( oo = Marche <=> ( 1 < teda ) ) /* respect of the invariant */
                THEN
                        ordr  := oo
                ||        ordre := oo
                ||        teda  := teda - 1
                END
        END

END
