/*? **************************************************************
 *
 * File : $Id: $
 *
 *
 * Object : Implementation of the cash dispenser machine
 * 
 * ---------------------------------------------------------------
 * Copyright (c) 1996 - GEC ALSTHOM Transport and STERIA - 
 * ---------------------------------------------------------------
 *
 * History : $Log: $
 *
 *****************************************************************?*/


IMPLEMENTATION
        Dab_imp


REFINES
        Dab

SEES
        Date

IMPORTS
        Controleur , Fonctions , Etat

PROMOTES
        etat_distributeur



















INVARIANT
/*? linking invariants between imported machines ?*/
        etat_aut = courant
&       ( caisse_vde = TRUE => ( caisse < 900 ) )

/*? invariants using definitions above ?*/
&       ( courant = Traitement_code & etat_dab = En_service =>
                ( carte : clients - interdits & date_mesure <= date_validite & 100 <= comptes ( carte ) ) )
&       ( courant = Traitement_somme & etat_dab = En_service =>
                ( code_CB = code_saisi ) & ( carte : clients - interdits & date_mesure <= date_validite & 100 <= comptes ( carte ) ) )
&       ( courant = Distribution_billets & etat_dab = En_service =>
                ( carte : clients - interdits & somme <= comptes ( carte ) & somme <= caisse & somme : 100 .. 900 ) & ( code_CB = code_saisi ) & ( carte : clients - interdits & date_mesure <= date_validite & 100 <= comptes ( carte ) ) )

/*? invariants depending on the states of the automaton ?*/
&       ( etat_dab = En_service => code_demande = FALSE & somme_demandee = FALSE )
&       ( courant = Veille & etat_dab = En_service =>
                 carte = K0 & infos_lues = FALSE )
&       ( courant = Traitement_carte & etat_dab = En_service =>
                 carte = K0 & infos_lues = FALSE )
&       ( courant = Traitement_code & etat_dab = En_service =>
                 ( resultat : { Nouvel , Dernier } => essai_possible = TRUE )
         &       infos_lues = TRUE )
&       ( courant = Traitement_somme & etat_dab = En_service =>
                 infos_lues = TRUE )
&       ( courant = Restitution_carte => carte /= K0 )

&       ( etat_dab = En_service <=> ( caisse_vde = FALSE & panne_dab = FALSE ) )

&       ( courant : { Traitement_code , Traitement_somme , Distribution_billets } =>
                 carte /= K0 )
&       ( courant = Traitement_carte => carte = K0 )


OPERATIONS

initialiser_dab ( avnc ) =
        BEGIN
                initialiser_etat
        ;        initialiser_distributeur ( avnc )
        ;        initialiser_lecteur_dab
        ;        initialiser_clavier_code_dab
        ;        initialiser_clavier_somme_dab
        ;        initialiser_ecran
        END
;

tester_dispositifs =
        VAR tst , tst_dab , cse_vde
        IN
                tst_dab <-- tester_dab
        ;        cse_vde <-- tester_caisse_vide                                           /*? E10 ?*/
        ;        tst := bool ( tst_dab = TRUE or cse_vde = TRUE )
        ;        IF tst = TRUE THEN
                        mettre_hs ( tst_dab , cse_vde )
                END
        END
;
traiter_operation =
        VAR eta_cour , eta_prec , rsl_prec , rslt IN
                eta_cour , eta_prec , rsl_prec <-- etat_automate
        ;        CASE eta_cour OF
                        EITHER Veille THEN
                                autoriser_entree_carte
                        ;        rslt <-- surveiller

                        OR Traitement_carte THEN
                                autoriser_chargement_carte
                        ;        rslt <-- controler_carte

                        OR Traitement_code THEN
                                autoriser_saisie_code ( eta_prec , rsl_prec )
                        ;        rslt <-- controler_code

                        OR Traitement_somme THEN
                                autoriser_saisie_somme
                        ;        rslt <-- controler_somme

                        OR Distribution_billets THEN
                                autoriser_distribution_billets
                        ;        rslt <-- controler_distribution_billets

                        OR Restitution_carte THEN
                                autoriser_restitution_carte ( eta_prec , rsl_prec )
                        ;        rslt <-- controler_restitution_carte

                        ELSE /*OR Confiscation_carte THEN*/
                                autoriser_confiscation_carte ( eta_prec , rsl_prec )
                        ;        rslt <-- controler_confiscation_carte

                        END
                END

        ;        determiner_nouvel_etat ( rslt )

        END
;
fermer_dab =
        VAR eta_cour , eta_prec , rsl_prec IN
                eta_cour , eta_prec , rsl_prec <-- etat_automate
        ;        IF eta_cour /= Veille THEN
                        traiter_carte ( eta_cour )                                        /*? E48 ?*/
                END
        ;        autoriser_mise_hors_service                                        /*? E25 ?*/
        ;        mettre_en_veille
        END

END
