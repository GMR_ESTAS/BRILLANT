/*? **************************************************************
 *
 * File : $Id: $
 *
 *
 * Object : Modelizes the sensor used to read cards informations
 * 
 * ---------------------------------------------------------------
 * Copyright (c) 1996 - GEC ALSTHOM Transport and STERIA - 
 * ---------------------------------------------------------------
 *
 * History : $Log: $
 *
 *****************************************************************?*/


MACHINE
        Lecteur

SEES
        Etat , Carte , Date , Horloge , Site_central

ABSTRACT_VARIABLES
        lisible                           /*? equals TRUE if the card can be read by the reader ?*/
,        carte                           /*? equals K0 if there is no card,
				        *  or the card number if a card is inserted ?*/
,        cde_carte , date_validite  /*? code and expiration date read from the card
				        * when it is inserted ?*/

INVARIANT
        lisible              : BOOL
&        carte              : CARTE
&        cde_carte     : 0 .. 9999
&        date_validite : DATE

INITIALISATION
        lisible              := FALSE
||        carte              := K0
||        cde_carte     := 0
||        date_validite := D_min

OPERATIONS

 /*?
  * initialization of the variables linked to the card
  ?*/
initialiser_lecteur =
        BEGIN
                carte              := K0
        ||        cde_carte     := 0
        ||        date_validite := D_min
        END
;
  /*?
   * test of the sensor: tst_lct equals TRUE when an error occurs
   ?*/
tst_lct <-- tester_lecteur =
        BEGIN
                tst_lct :: BOOL
        END
;

  /*?
   * the dab is waiting for a card to be inserted
   ?*/
ouvrir_lecteur =
        PRE
                carte = K0
        THEN
                skip
        END
;
 /*?
  * the reader already contains a card, locks are set to avoid the insertion of
  * a second card
  ?*/
fermer_lecteur =
        PRE
                carte /= K0
        THEN
                skip
        END
;
  /*?
   * close the reader when the dab is disabled, to avoid a card to be inserted
   ?*/
condamner_lecteur =
        BEGIN
                carte := HS
        END
;
 /*?
  * waits until the customer enters his card, returns an event report:
  * Incident if a sensor error occurs,
  * Valide is a card is inserted,
  * Hors_delai otherwise
  * Delayed peration
  ?*/
rslt <-- scruter_lecteur =
        PRE
                carte = K0
        THEN
                rslt :: { Valide , Hors_delai , Incident }
        END
;
 /*?
  * check that the card is readable (update lisible), and get the card data
  * check that the card is still there, otherwise it is loaded.
  ?*/
charger_carte =
        PRE
                carte = K0
        THEN
                carte              :: CARTE
        ||        lisible              :: BOOL                                                   /*? E13 ?*/
        ||        cde_carte     :: 0 .. 9999
        ||        date_validite :: DATE
        END
;
 /*?
  * return the loading state:
  * Incident if the card was withdrawn when trying to load it,
  * Illisible if it is unreadable,
  * Valide otherwise (then the values of code_carte and date_validite are significant)
  ?*/
rslt <-- chargement_carte =
        ANY ctr
        WHERE
                ctr : { Valide , Incident , Illisible }
        &        ( ctr = Valide <=> ( carte /= K0 & lisible = TRUE ) )
        &        ( ctr = Incident <=> ( carte = K0 ) )
        THEN
                rslt := ctr
        END
;
 /*?
  * ask the central site to check permissions of the readable card
  * (rslt=Invalide if the card is not identified, rslt=Interdite if the card is not authorized)
  * check that the expiration date is a later date than the date given as a parameter
  * (rslt=Perimee if the date is exceedeed)
  * ask the central site the account balance of the card (solde)
  * and check that it is sufficient (rslt=Epuisee otherwise)
  ?*/
rslt , solde <-- validation_infos_carte ( dte ) =
        PRE
                dte : DATE
        &        carte /= K0
        &        lisible = TRUE
        THEN
                ANY ctr , sld WHERE
                        ctr : { Valide , Invalide , Interdite , Perimee , Epuisee }
                &        sld : NAT
                &        ( ( carte : clients - interdits
                         &        comptes ( carte ) >= 100
                         &        dte <= date_validite ) <=> ( ctr = Valide ) )       /*? E40 ?*/
                &        ( carte : clients - interdits => ( sld = comptes ( carte ) ) )
                THEN
                        rslt  := ctr
                ||        solde := sld
                END
        END
;
 /*?
  * read operation
  ?*/
crt <-- numero_carte =
        BEGIN
                crt := carte
        END
;
 /*?
  * read operation
  ?*/
cde <-- code_carte =
        BEGIN
                cde := cde_carte
        END
;
 /*?
  * get the card out of the reader, so that the customer may take it back
  ?*/
rendre_carte =
        PRE
                carte /= K0
        THEN
                carte := K0                                                           /*? E16 ?*/
        END
;
 /*?
  * wait for the customer to take back his card
  * return Incident if a sensor error occurs, Hors_delai if the card is not taken back,
  * Valide otherwise.
  * Delayed operation
  ?*/
rslt <-- restitution_carte =
        PRE
                carte = K0
        THEN
                rslt :: { Valide , Hors_delai , Incident }
        END
;

 /*?
  * confiscate the card if it is still in the reader (rslt=Valide)
  * else do nothing (rslt=Invalide)
  ?*/
rslt <-- confiscation_carte =
        BEGIN
                rslt  :: { Valide , Invalide }
        ||        carte := K0
        END


END
