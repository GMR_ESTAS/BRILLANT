/*? **************************************************************
 *
 * File : $Id: $
 *
 *
 * Object : Modelizes the keybord for custumer code
 * 
 * ---------------------------------------------------------------
 * Copyright (c) 1996 - GEC ALSTHOM Transport and STERIA - 
 * ---------------------------------------------------------------
 *
 * History : $Log: $
 *
 *****************************************************************?*/



MACHINE
        Clavier_code

SEES
        Etat

ABSTRACT_VARIABLES
        code_saisi        /*? code entered by the customer ?*/
,       etat_clc        /*? functionning state of the keyboard ?*/
,       essai_possible        /*? TRUE if the number of attemps < 3 ?*/


INVARIANT
        code_saisi     : 0 .. 9999
&        etat_clc       : BOOL
&        essai_possible : BOOL

INITIALISATION
        code_saisi     := 0
||        etat_clc       := FALSE
||        essai_possible := TRUE

OPERATIONS

 /*?
  * new customer: code_saisi is initialized but not significant
  * the number of attempts is 0, therfore essai_possible is set up to 0
  ?*/
initialiser_clavier_code =
        BEGIN
                code_saisi     := 0
        ||        essai_possible := TRUE
        END
;

 /*?
  * check that the keyboard is enabled (no failure)
  ?*/
tst_clc <-- tester_clavier_code =
        BEGIN
                tst_clc :: BOOL
        END
;
 /*?
  * the keyboard is enabled, the customer may enter his code
  ?*/
demander_code =
        PRE
                etat_clc = FALSE
        THEN
                etat_clc := TRUE
        END
;
 /*?
  * the keyboard is disabled
  ?*/
desactiver_clavier_code =
        BEGIN
                etat_clc := FALSE
        END
;
 /*?
  * wait for the customer to enter his code, then refresh the
  * variable code_saisi
  * return the reading state: Hors_delai if no code hes been entered,
  * Incident if a failure of the keyboard occurs, otherwise Valide
  * Delayed operation.
  ?*/
rslt <-- lecture_code =
        PRE
                etat_clc = TRUE
        THEN
                ANY ctr
                WHERE
                        ctr : { Valide , Hors_delai , Incident }
                THEN
                        rslt           := ctr
                ||        code_saisi :: 0 .. 9999
                END
        END
;

 /*?
  * compare the code entered with the code of the card
  * return the result considering the number of failed attempts
  * Valide if the code is correct, Nouvel or Dernier if the
  * number of attempts < 3
  * Invalide if the 3rd attempt failed.
  ?*/
rslt <-- validation_code ( cde_crt ) =
        PRE
                cde_crt : 0 .. 9999
        &        etat_clc = FALSE
        &        essai_possible = TRUE
        THEN
                ANY ctrl , essaip WHERE
                        ctrl : { Valide , Nouvel , Dernier , Invalide }
                 &        ( ctrl = Valide <=> ( cde_crt = code_saisi ) )               /*? E41 ?*/
                 &      essaip : BOOL
                 &      ( ctrl = Invalide => essaip = FALSE )
                 &      ( ctrl : { Nouvel , Dernier } => essaip = TRUE )            /*? E44 ?*/
                 THEN
                         rslt                := ctrl
                 ||         essai_possible := essaip
                 END
         END

END
