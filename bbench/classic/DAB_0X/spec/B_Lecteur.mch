/*? **************************************************************
 *
 * File : $Id: $
 *
 *
 * Object : Basic Machine
 * 
 * ---------------------------------------------------------------
 * Copyright (c) 1996 - GEC ALSTHOM Transport and STERIA - 
 * ---------------------------------------------------------------
 *
 * History : $Log: $
 *
 *****************************************************************?*/



MACHINE
        /* physical card reader */
        B_Lecteur

SEES
        Carte , Horloge

CONCRETE_VARIABLES
        b_verrous ,       /*? TRUE if the reader is locked ?*/
        b_carte                /*? TRUE if a card is inside the reader ?*/

INVARIANT
        b_verrous : BOOL
&        b_carte         : BOOL
&        ( b_carte = FALSE => b_verrous = FALSE )
   /*? if no card is present, then the reader is opened
    * except if the DAB is disabled. In this case, b_carte must be equal to TRUE
    * even if there is no card
    ?*/

INITIALISATION
        b_verrous , b_carte := FALSE , FALSE      /* deals with the GUI */

OPERATIONS

 /*?
  * unlock the card reader and enable it so that it can read the inserted card
  ?*/
b_ouvrir_lecteur =
        BEGIN
                b_verrous := FALSE
        END
;

b_initialiser_lecteur =
        BEGIN
                b_verrous , b_carte := FALSE , FALSE
        END
;

 /*?
  * lock the card reader, so that a second card cannot be inserted.
  ?*/
b_fermer_lecteur =
        PRE
                b_carte = TRUE
        THEN
                b_verrous := TRUE
        END
;

 /*?
  * lock the card reader, so that no card can be inserted in case of failure.
  * b_carte still equals TRUE to preserve the invariant.
  ?*/
b_condamner_lecteur =
        BEGIN
                b_carte          := TRUE
        ||        b_verrous := TRUE                                                   /*? E03 ?*/
        END
;

 /*?
  * test the card detector: pre = TRUE if there is a card. The detector is
  * stopped if the card reader is locked.
  ?*/
pre <-- b_test_carte_presente =
        PRE
                b_verrous = FALSE
        THEN
                pre :: BOOL                                                           /*? E27 ?*/
        END
;

 /*?
  * enable the card reader system: input of the card, reading of the tape and
  * of the chip.
  * chargee indicates whether the card is still here when it is about to be
  * read, otherwise  chargee = FALSE  and the other output parameters are not
  * significatn. val = FALSE if the card is unreadable, then crt
  * cdd and lmt are not significant. The card number (crt), code number (cdd)
  * and limit date (lmt) are returned in a format given in seen machines.
  ?*/
 chargee , val , crt , cdd , lmt <-- b_charger_carte =
        PRE     /*
		 * We do not want to cope with another card being loaded, etc ...
		 */
                b_verrous = FALSE
        &        b_carte = FALSE
        THEN    /*
		 * pre indicates whether the card is present when the card reader is
		 * enabled. The presence of the card should not be memorised when
		 * the card is inserted, because the card may have been removed after
		 * the last call to b_test_carte_presente.
		 */
                ANY pre , nro_carte
                WHERE
                        pre : BOOL
                &        nro_carte : CARTE
                &        ( ( pre = FALSE ) <=> ( nro_carte = K0 ) )
                THEN
                        b_carte := pre
                ||        crt        := nro_carte                                           /*? E28 ?*/
                ||        chargee := pre                                                   /*? E27 ?*/
                END     /*
			 * if no card is inserted, the operation is obviously a failure:
			 * val, crt, cdd and lmt are not significant.
			 */
        ||        val :: BOOL                                                           /*? E13 ?*/
        ||        cdd :: 0 .. 9999                                                         /*? E28 ?*/
        ||        lmt :: D_min .. D_max                                                 /*? E28 ?*/
        END
;

 /*?
  * enable the system that gives back a card. We are not interested in
  * what will happen if no card is present.
  ?*/
b_rendre_carte =
        PRE
                b_carte = TRUE
        THEN
                b_carte          := FALSE                                                   /*? E16 ?*/
        ||        b_verrous := FALSE
        END
;

 /*?
  * card reader test: tst_lct equals TRUE if an error occurs
  ?*/
tst_lct <-- b_tester_lecteur =
        BEGIN
                tst_lct :: BOOL                                                           /*? E24 ?*/
        END
;

 /*?
  * enable the card confiscation system.
  ?*/
b_confisquer_carte =
        BEGIN
                b_carte          := FALSE
        ||        b_verrous := FALSE
        END

END
