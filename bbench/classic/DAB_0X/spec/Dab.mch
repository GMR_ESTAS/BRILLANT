/*? **************************************************************
 *
 * File : $Id: $
 *
 *
 * Object : Modelizes the cash dispenser machine
 * 
 * ---------------------------------------------------------------
 * Copyright (c) 1996 - GEC ALSTHOM Transport and STERIA - 
 * ---------------------------------------------------------------
 *
 * History : $Log: $
 *
 *****************************************************************?*/


MACHINE
        Dab

INCLUDES
        Carte , Etat

ABSTRACT_VARIABLES
        clients , comptes , interdits ,     /*? central site data managed by Site_central ?*/
        caisse , corbeille , retraits ,     /*? bills dispatching managed by Distributeur ?*/
        etat_dab , etat_aut , panne_dab ,     /*? state of the dab, the automaton and the elements ?*/
        carte                                /*? card inserted in the reader ?*/

INVARIANT
        clients          : POW ( CARTE - { K0 , HS } )
&        comptes          : clients --> NAT
&        interdits : POW ( clients )
&        caisse          : NAT
&        corbeille : NAT
&        retraits  : NAT
&        etat_dab  : ETAT_DAB
&        etat_aut  : ETAT_AUTOMATE
&        panne_dab : BOOL
&        carte          : CARTE
&        caisse + corbeille + retraits : NAT

INITIALISATION
        ANY cl , co , in WHERE
                cl : POW ( CARTE - { K0 , HS } )
        &        co : cl --> NAT
        &         in : POW ( cl )
        THEN
                clients , comptes , interdits := cl , co , in
        END
||        caisse    := 0
||        corbeille := 0
||        retraits  := 0
||        etat_dab  := Hors_service
||        etat_aut  := Veille
||        panne_dab := FALSE
||        carte          := K0

OPERATIONS

 /*?
  * initialization of the cash register, the reader, the keyboards, the screen and the automaton
  ?*/
initialiser_dab ( avnc ) =
        PRE
                avnc : NAT
        &        900 <= avnc
        THEN
                ANY clts , cpt_crt , crt_int WHERE
                        clts        : POW ( CARTE - { K0 , HS } )
                &        cpt_crt        : clts --> NAT
                &        crt_int        : POW ( clts )
                THEN
                        clients   := clts
                ||        comptes   := cpt_crt
                ||        interdits := crt_int
                ||        caisse    := avnc
                ||        corbeille := 0
                ||        retraits  := 0
                ||        etat_aut  := Veille
                ||        etat_dab  := En_service
                ||        panne_dab := FALSE
                ||        carte          := K0
                END
        END
;
 /*?
  * read operation
  ?*/
etat <-- etat_distributeur =
        BEGIN
                etat := etat_dab
        END
;
 /*?
  * check whether no element is out of order and whether there is enough cash
  * in the cash register
  * else shut down the dab
  ?*/
tester_dispositifs =
        PRE
                  etat_dab = En_service
        THEN
                ANY etat , pan_dab WHERE
                        etat        : ETAT_DAB
                &        pan_dab        : BOOL
                &        ( etat = Hors_service <=>
                         ( pan_dab = TRUE or caisse < 900 ) )
                THEN
                        etat_dab  := etat
                ||        panne_dab := pan_dab
                END
        END
;
 /*?
  * perform operations depending on the dab state;
  * then turns the dab into the state determined by the automaton
  ?*/
traiter_operation =
        PRE
                etat_dab = En_service

        THEN
                CHOICE
                /* bills distribution case */
                        ANY crt , som , rjt WHERE
                                crt : clients
                        &        som : 100 .. 900
                        &        rjt : { 0 , som }
                        &        som <= caisse
                        &        som <= comptes ( crt )
                        THEN
                                caisse       := caisse - som
                        ||        corbeille    := corbeille + rjt
                        ||        retraits     := retraits + som - rjt
                        ||        comptes ( crt ) := comptes ( crt ) - som
                        ||        etat_aut     :: ETAT_AUTOMATE
                        END
                OR
                /* card inserted or removed case */
                        carte             :: CARTE
                ||        etat_aut     :: ETAT_AUTOMATE
                OR
                /* else case */
                        etat_aut     :: ETAT_AUTOMATE
                END
        END
;
 /*?
  * the dab is disabled (trouble or not enough cash in the cash register)
  * before shuting it down, if there is a card in the reader, then:
  * confiscate the card, if it was going to be confiscated,
  * otherwise give it back.
  ?*/
fermer_dab =
        PRE
                etat_dab = Hors_service
        THEN
                etat_aut := Veille
        ||        carte         := HS
        END

END
