/*? **************************************************************
 *
 * File : $Id: $
 *
 *
 * Object : Basic Machine
 * 
 * ---------------------------------------------------------------
 * Copyright (c) 1996 - GEC ALSTHOM Transport and STERIA - 
 * ---------------------------------------------------------------
 *
 * History : $Log: $
 *
 *****************************************************************?*/




MACHINE
        /* physical central site */
        B_Site_central

SEES
        Carte

ABSTRACT_VARIABLES
        b_no_clients /*? set of cards numbers known by the central site ?*/
,        b_interdits  /*? forbidden cards ?*/
,        b_solde             /*? balance of accounts ?*/

INVARIANT
   /* K0, HS are not customer's card number */
        b_no_clients <: CARTE - { K0 , HS }
&        b_interdits : b_no_clients --> BOOL
        /*? b_interdits(crt) equals TRUE if card number crt is forbidden ?*/
&        b_solde : b_no_clients --> NAT
        /*? b_solde(crt) is the balance of the account associated with card crt ?*/

INITIALISATION
        /*
	 * undeterministic initialization: the central site DB is unknown
	 * at the start up of the DAB
	 */
        ANY        nc , ni , ns WHERE
                nc <: CARTE - { K0 , HS }
        &        ni : nc --> BOOL
        &        ns : nc --> NAT

        THEN
                b_no_clients , b_interdits , b_solde := nc , ni , ns
        END

/****
WARNING: the following initialization is false
	b_no_clients : (b_no_clients <: CARTE)
||	b_interdits : (b_interdits: b_no_clients --> BOOL)
||	b_solde : (b_solde: b_no_clients --> NAT)
because it does not link the new b_no_clients and the 2 other variables.
*****/

OPERATIONS

 /*?
  * central site interrogation
  * the number crt read on the card is in a right format, therefore this is
  * an existant card; thought it must be checked by the central site.
  * val = TRUE indicates that the card is known.
  * itd = TRUE indicates that the card is forbidden.
  * solde is the balance of the account.
  ?*/
val , solde , itd <-- b_interroger_carte ( crt ) =
        PRE
                crt : CARTE
        THEN
                val := bool ( crt : b_no_clients )                                       /*? E18 ?*/
        ||        ANY ss , ii WHERE
                        ss : NAT                                                   /*? E19 ?*/
                &        ii : BOOL                                                   /*? E19 ?*/
                &        ( crt : b_no_clients => ss = b_solde ( crt ) & ii = b_interdits ( crt ) )
                        /* respect the definitions domaines of b_solde and b_interdits */
                THEN
                        solde , itd := ss , ii
                END
        END
;

 /*?
  * The account of the card with number crt is debited with the amount som.
  * This account must exist and must have enough money.
  * We consider that the DataBase of the central site does not change when
  * the DAB is enabled (otherwise an account could disappear between the
  * querry and the debit).
  * This hypothesis is actually false.
  ?*/
b_debiter_carte ( crt , som ) =
        PRE
                 crt : CARTE
        &        crt : b_no_clients
        &        som : NAT
        &        som <= b_solde ( crt )
        THEN
                b_solde ( crt ) := b_solde ( crt ) - som                             /*? E53 ?*/
        END

/* NOTA: the DAB cannot forbid a card */

END
