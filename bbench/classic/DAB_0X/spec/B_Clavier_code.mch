/*? **************************************************************
 *
 * File : $Id: $
 *
 *
 * Object : Basic Machine
 * 
 * ---------------------------------------------------------------
 * Copyright (c) 1996 - GEC ALSTHOM Transport and STERIA - 
 * ---------------------------------------------------------------
 *
 * History : $Log: $
 *
 *****************************************************************?*/



MACHINE
        B_Clavier_code

CONCRETE_VARIABLES
        /*?
	 * b_etat_clc : equals TRUE if the keyboard is opened
	 ?*/
        b_etat_clc

INVARIANT
        b_etat_clc : BOOL

INITIALISATION
        b_etat_clc := FALSE        /* deals with the GUI */

OPERATIONS

 /*?
  * read operation
  ?*/
ecc <-- b_lire_etat_clc =
        BEGIN
                ecc := b_etat_clc
        END
;

 /*?
  * close the code keyboard. Usefull to set up a maximum time to enter the code
  ?*/
b_desactiver_clavier_code =
        PRE
                b_etat_clc = TRUE
        THEN
                b_etat_clc := FALSE
        END
;

 /*?
  * keyboard test: tst_clc equals TRUE if an error occurs
  ?*/
tst_clc <-- b_tester_clavier_code =
        BEGIN
                tst_clc :: BOOL                                                           /*? E24 ?*/
        END
;

 /*? open the keyboard and reset it for the next code input ?*/
b_demander_code =
        PRE
                b_etat_clc = FALSE
        THEN
                 b_etat_clc := TRUE
        END
        ;

 /*?
  * val indicates whether a 4 digit code was entered
  * if val = TRUE, code equals this code.
  * Operation not delayed.
  * The keyboard is not disabled.
  ?*/
val , code <-- b_code_saisi =
        PRE
                b_etat_clc = TRUE
        THEN
                ANY rsl , cde
                WHERE
                        rsl : BOOL
                &        cde : NAT  /* significant if rsl = TRUE */
                &        ( rsl = TRUE => cde : 0 .. 9999 )
                THEN
                        code := cde                                                   /*? E30 ?*/
                ||        val  := rsl
                END
        END

END
