 

Require Import Blib.

Theorem op:
exists _c2, ((((In (interval 0 10) 0) /\ (In (interval 1 5) _c2)))).
try intuition 
   || unfold BBOOL ; intuition ; apply Full_intro
   || red ;  red ; red ; omega.
Qed.
 