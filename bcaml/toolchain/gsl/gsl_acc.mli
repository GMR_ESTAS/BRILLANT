(** Library for GSL constructors and accessors. This module defines functions to build or unassemble constructions of the type defined in Gsl_adt, the same way it is done in the blast directory. The only difference is that, due to the little size of the type, it is more convenient to put all accessors and constructors in the same module 
@author Samuel Colin*)

(** Create a GSL of affectation
@param iexprlist expr to be affected
@author Samuel Colin*)
val mkGsl_Affect : (Blast.id * Blast.expr) list -> Gsl_adt.gsl

(** Create a GSL of skip
@author Samuel Colin*)
val mkGsl_Skip : unit -> Gsl_adt.gsl

(** Create a GSL of pre-condition
@param condition the pre-condition
@param subst the subst
@author Samuel Colin*)
val mkGsl_Precondition : Blast.predicate -> Gsl_adt.gsl -> Gsl_adt.gsl

(** Create a GSL of choice
@param subst1 the first choice
@param subst2 the other choice
@author Samuel Colin*)
val mkGsl_bChoice : Gsl_adt.gsl -> Gsl_adt.gsl -> Gsl_adt.gsl

(** Create a GSL of Guard
@param condition the condition
@param subst the substitution
@author Samuel Colin*)
val mkGsl_Guard : Blast.predicate -> Gsl_adt.gsl -> Gsl_adt.gsl

(** Create a GSL of unbounded choice
@param idents the ID
@param subst the substitution
@author Samuel Colin*)
val mkGsl_uChoice : Blast.id list -> Gsl_adt.gsl -> Gsl_adt.gsl

(** Create a GSL of parallel substitution
@param subst1 the first substitution
@param subst2 the second substitution
@author Samuel Colin*)
val mkGsl_Parallel : Gsl_adt.gsl -> Gsl_adt.gsl -> Gsl_adt.gsl

(** Create a GSL of sequence substitution
@param subst1 the first substitution
@param subst2 the second substitution
@author Samuel Colin*)
val mkGsl_Sequence : Gsl_adt.gsl -> Gsl_adt.gsl -> Gsl_adt.gsl

(** Create a GSL of sequence substitution
@param condition a predicate that will be used as a condition
@param subst a GSL
@param variant an expr
@param invariant a predicate
@author Samuel Colin*)
val mkGsl_While :
  Blast.predicate ->
  Gsl_adt.gsl -> Blast.expr -> Blast.predicate -> Gsl_adt.gsl

(** Extracte the GSL inside the argument
@param substgen a GSL
@author Samuel Colin*)
val gsl_from : Gsl_adt.gsl -> Gsl_adt.gsl

(** Extracte the condition inside the given GSL
@param substgen a GSL
@author Samuel Colin*)
val cond_from : Gsl_adt.gsl -> Blast.predicate

(** Extracte the IDs inside the GSL
@param substgen a GSL
@author Samuel Colin*)
val allids_from : Gsl_adt.gsl -> Blast.id list

(** Extracte what is affected inside the GSL
@param substgen a GSL
@author Samuel Colin*)
val affects_from : Gsl_adt.gsl -> (Blast.id * Blast.expr) list

(** Extracte the two GSl that are inside the argument
@param substgen a GSL
@author Samuel Colin*)
val twosubsts_from : Gsl_adt.gsl -> Gsl_adt.gsl * Gsl_adt.gsl

(** Extracte the variant and invariant from a while GSL
@param substgen a GSL
@author Samuel Colin*)
val whilepreds_from : Gsl_adt.gsl -> Blast.expr * Blast.predicate
