(* $Id$ *)

(*i*)

open Gsl_adt
open Gsl_acc
open Pred_acc
open Subst_acc
open Blast
open Bbop

(*i*)

(*
Dans la fonction qui suit, les transformations de substitutions B vers les
substitutions g�n�ralis�es <<normalis�es>> se fait au plus proche de ce qui
est indiqu� dans \cite{BBook} (voir les tableaux ins�r�s dans le code). 
*)
    
let subst_to_gsl ids_to_avoid_set substB=
  let forbidden_set= ref 
      (Id_handling.Idset.union 
	 ids_to_avoid_set 
	 (Variables.all_vars_subst substB)
      )
  in
  
  let rec auxsubst_to_gsl subst =
    match subst with
      
(* \gsltab{BLOCK} *)
    | SubstBlock(s) -> 
      	auxsubst_to_gsl s
	  
(* \gsltab{SKIP} *)
    | SubstSkip -> 
      	mkGsl_Skip ()

(* \gsltab{PRE} *)
    | SubstPrecondition(c,s) -> 
      	mkGsl_Precondition c (auxsubst_to_gsl s)

(* \gsltab{IF} *)
    | SubstIf(_) -> 
      	let (i, t, e) = ite_from subst in
      	mkGsl_bChoice 
          (mkGsl_Guard i (auxsubst_to_gsl t) )
          (mkGsl_Guard (PredNegation(i)) (auxsubst_to_gsl e) )

(* \gsltab{CHOICE} *)
    | SubstChoice(substlist) ->
	begin
	  try
	    let (s1, s2)=Subst_acc.twosubsts_from subst in
	      mkGsl_bChoice (auxsubst_to_gsl s1) (auxsubst_to_gsl s2)
	  with
            Subst_acc.Empty_list
          | Subst_acc.Incomplete_list
          | Subst_acc.Unexpected_substitution ->
	     if substlist=[] then invalid_arg "subst_to_gsl"
	     else auxsubst_to_gsl (List.hd substlist)
	end
	
(* \gsltab{PARALLEL} *)
    | SubstParallel(s1,s2) -> 
      	mkGsl_Parallel (auxsubst_to_gsl s1) (auxsubst_to_gsl s2)

(* \gsltab{SEQUENCE} *)
    | SubstSequence(s1,s2) ->
      	mkGsl_Sequence (auxsubst_to_gsl s1) (auxsubst_to_gsl s2)

(* \gsltab{ANY} *)
    | SubstAny(_) -> 
      	let (v,c,s)=(allids_from subst, cond_from subst, subst_from subst) in
      	mkGsl_uChoice  v  (mkGsl_Guard c (auxsubst_to_gsl s))

(* \gsltab{WHILE} *)
    | SubstWhile(c,s,v,i) -> 
      	mkGsl_While c (auxsubst_to_gsl s) v i
	  
(* \gsltab{VAR} *)
    | SubstVar(_) -> 
      	let v=allids_from subst in
      	let s=subst_from subst in
      	mkGsl_uChoice v (auxsubst_to_gsl s)
	  
(* \gsltab{SELECT} *)
    | SubstSelect(_) -> 
      	if is_simple subst
      	then 
          let c=cond_from subst in
          let s=subst_from subst in
          mkGsl_Guard c (auxsubst_to_gsl s)
      	else
          let wtlist=normalize_select subst in
          let (conds, substs)=List.split wtlist in
          auxsubst_to_gsl ( 
	    SubstChoice (
	      List.map2 Blast_mk.mk_SubstSelectbasic conds substs) 
	  )
	    
(* \gsltab{LET} *)
    | SubstLet(_) -> 
      	auxsubst_to_gsl (let_to_any subst)
	  
(* \gsltab{CASE} *)
    | SubstCase(_) -> 
      	auxsubst_to_gsl (case_to_select subst)
	  
(* \gsltab{ASSERT} *)
    | SubstAssertion(p,s) -> 
      	auxsubst_to_gsl s
	  
(* \gsltab{SETEQUALS} *)
    | SubstSetEqualIds(v,e) ->
(* The infamous "actually it's syntactical bool()" transformation *)
	begin
	  match e with
	    | [ExprBool(predValue)] -> 
		let x=
		  match v with
		    | [y] -> y
		    | _ -> invalid_arg "subst_to_gsl: Bad number of left variables"
		in
      		mkGsl_bChoice 
		  (mkGsl_Guard
		     predValue 
		     (Gsl_acc.mkGsl_Affect [(x, ExprBoolConstant(TrueConstant))]) )
		  (mkGsl_Guard
		     (PredNegation(predValue))
		     (Gsl_acc.mkGsl_Affect [(x, ExprBoolConstant(FalseConstant))]) )
	    | _ ->
		Gsl_acc.mkGsl_Affect (List.combine v e)
	end
(* \gsltab{SETIN} *)
    | SubstSetIn(v,e) ->
	let (realids, newids)=
	  List.split (Id_handling.rename_ids v !forbidden_set) in
	let newexprs=(List.map (fun i -> ExprId(i)) newids) in
        let transformed_subst=
	  SubstAny(
	    newids,
            Blast_mk.mk_PredIn (ExprNuplet(newexprs)) e,
            SubstSetEqualIds(realids, newexprs))
	in
	forbidden_set:= List.fold_right Id_handling.Idset.add newids !forbidden_set
	    ;
	auxsubst_to_gsl transformed_subst
	  
(* \gsltab{SETEQUALFUN} *)
    | SubstSetEqualFun(_) ->
       	auxsubst_to_gsl (Subst_acc.to_SubstSetEqualIds subst)

(* \gsltab{BECOMESUCH} *)
    | SubstBecomeSuch(idents,pred) ->
	let beforeids=List.map Id_handling.id_before idents in
	forbidden_set := List.fold_right Id_handling.Idset.add beforeids !forbidden_set
            ;
	let assoc_oldnew=Id_handling.rename_ids idents !forbidden_set in
   	let newids=Id_handling.new_ids_from_renaming assoc_oldnew in
	forbidden_set := List.fold_right Id_handling.Idset.add newids !forbidden_set
            ;
        
(* Once here, the identifiers, their "before-loop" equivalent, and their *)
(* renamings must be at the same place in their respective lists. *)

	let hugerenaming=List.combine (beforeids@idents) (idents@newids) in
	let newpred=
	  List.fold_right
             ( fun (x,xnew) p -> Instanciation.id_subst_pred x xnew p)
	    hugerenaming
	    pred
	in
	let newexprs=List.map (fun i -> ExprId(i)) newids in
	let transformed_becomesuch=
	  SubstAny(
	    newids,
	    newpred,
	    SubstSetEqualIds( idents, newexprs))
	in
	auxsubst_to_gsl transformed_becomesuch
    | _ -> raise (Error.Not_implemented "subst_to_gsl")
	  
(*i*)
(*
   | SubstRenaming of id list * id list * substitution
   | SubstSetEqualRecords of exprRecords * expr
   | SubstOperCall of id list * id * expr list
 *)
(*i*)
	  
  in
  auxsubst_to_gsl substB

    
(*
   \section{Application de substitution}
   \gsltab{apply}
 *)
let apply substgsl predicate=
  let forbidden_set= ref 
      (Id_handling.Idset.union
	 (Variables.all_vars_pred predicate)
	 (Gsl_lib.all_vars substgsl)
      )
  in
  
(* Due to the peculiarity of the treatment of the While Substitution, the *)
(* resulting predicates are separated. This function makes the final *)
(* reunification *)
 
  let rebuild (tocomp, nottocomp)=
    match nottocomp with
    | None -> tocomp
    | Some(pred) -> Blast_mk.mk_Conjunction [tocomp; pred]
  in
  
  (*
     Calculus of \[S\]P
   *)
  let rec auxapply substgen pred =
    match substgen with
(* *)
    | Gsl_affect(_) ->
	let xelist=Gsl_acc.affects_from substgen in
	begin
	  match xelist with
	  | [(x, e)] -> ( Instanciation.replace_pred x e pred, None )
	  | [] -> raise (Invalid_argument "apply")
	  | _ -> 
	      let affectseq=
		Instanciation.multiple_affect_to_list forbidden_set xelist 
	      in
	      let applycpl=fun predic (xid, xexpr) -> 
		Instanciation.replace_pred xid xexpr predic 
	      in
(* fold_left, cause the applying must start from the first element of the list *)
	      let doneapplying=List.fold_left applycpl pred affectseq in
		(doneapplying, None)
	end
(*	*)  
(*
   \[Skip\]P -> P
 *)
    | Gsl_skip -> 
      	( pred, None )

    | Gsl_precondition(c,s) ->
	let p = (rebuild 
		   (auxapply s pred)
		) in	
        let sdone=PredParen( p ) in
	(PredBin(Bbop.And, c, sdone), None)

    | Gsl_bChoice(_) -> 
      	let (s1, s2)=Gsl_acc.twosubsts_from substgen in
	let s1done=rebuild (auxapply s1 pred) in
	let s2done=rebuild (auxapply s2 pred) in
      	(PredBin(Bbop.And, s1done, s2done), None )

    | Gsl_guard(_) ->
      	let c=Gsl_acc.cond_from substgen in
      	let s=Gsl_acc.gsl_from substgen in
	let sdone=PredParen(rebuild (auxapply s pred)) in	
	(PredBin(Bbop.Implies, c, sdone), None)

    | Gsl_uChoice(_) ->
      	let z=Gsl_acc.allids_from substgen in
      	let s=Gsl_acc.gsl_from substgen in
	let sdone=PredParen(rebuild (auxapply s pred)) in
      	(PredForAll(z, sdone), None )

    | Gsl_sequence(_) ->
      	let (s1, s2)=Gsl_acc.twosubsts_from substgen in
	let (s2comp, s2notcomp)=auxapply s2 pred in
	let s1comp=rebuild ((auxapply s1 s2comp)) in
	(s1comp, s2notcomp)

    | Gsl_parallel(_) ->  
   	let (s1, s2)=Gsl_acc.twosubsts_from substgen in
	let transformed_parallel=propagate_parallel s1 s2 in
	auxapply transformed_parallel pred

    | Gsl_while(c,s,v,i) ->
   	let c=Gsl_acc.cond_from substgen in
   	let s=Gsl_acc.gsl_from substgen in
   	let n=Id_handling.create_new_id !forbidden_set in 
        let si=rebuild (auxapply s i) in
	
        let vinnat=
	  PredAtom(In, v, ExprSet(SetPredefined(NAT)))
	in
	
        let vinfn=PredAtom(Less, v, ExprId(n)) in
   	let ngetv=Gsl_acc.mkGsl_Affect [(n,v)] in
   	let svn=rebuild (auxapply s vinfn) in
   	let donevinfn=rebuild (auxapply ngetv svn) in
	
   	let prop1 = i in
	(* i and c => si *)
   	let prop2 = PredBin(Implies, PredBin(And, i, c), si) in
	(* i => v : NAT *)
   	let prop3 = PredBin(Implies, i, vinnat) in
	(* i and c => [n:=v]([s] (v<n)) *)
   	let prop4 = PredBin(Implies, PredBin(And, i, c), donevinfn) in
	(* i and not(c) => pred *)
   	let prop5 = PredBin(Implies, PredBin(And, i, PredNegation(c)), pred) in
   	(prop1, Some(Blast_mk.mk_Conjunction [prop2; prop3; prop4; prop5]) )
	  
  and 

(* MG Mettre les r�gles de calcul *)
    propagate_parallel s1 s2=
    match s2 with
      | Gsl_affect(_) ->
	  begin
	    match s1 with
	      | Gsl_affect(_) -> 
		  let (xs1, es1)=List.split (Gsl_acc.affects_from s1) in
		  let (xs2, es2)=List.split (Gsl_acc.affects_from s2) in
		    Gsl_acc.mkGsl_Affect (List.combine (xs1@xs2) (es1@es2))
	      | _ -> propagate_parallel s2 s1
	  end

      | Gsl_skip -> 
	  s1

      | Gsl_precondition(prec,pres) ->
	  Gsl_acc.mkGsl_Precondition prec (propagate_parallel s1 pres)

      | Gsl_bChoice(bs1,bs2) ->
	  let propc1=propagate_parallel s1 bs1 in
	  let propc2=propagate_parallel s1 bs2 in
	    Gsl_acc.mkGsl_bChoice propc1 propc2	      

      | Gsl_guard(c,s) ->
	  Gsl_acc.mkGsl_Guard c (propagate_parallel s1 s)	      

      | Gsl_uChoice(_) ->
      	  let uz=Gsl_acc.allids_from s2 in
      	  let us=Gsl_acc.gsl_from s2 in
	  let uidset=Id_handling.set_from_ids uz in
	  let s1_idset=Gsl_lib.all_vars s1 in
	    if (Id_handling.Idset.inter uidset s1_idset) = Id_handling.Idset.empty
	    then Gsl_acc.mkGsl_uChoice uz (propagate_parallel s1 us)
	    else
	      begin
		let renaminguz=Id_handling.rename_ids uz !forbidden_set in
		let (olduz, newuz) = List.split renaminguz in
		  forbidden_set := List.fold_right 
		                    Id_handling.Idset.add  
				     newuz 
				     !forbidden_set
		  ;
		  let newus=List.fold_right2 Gsl_lib.id_subst olduz newuz us in
		    Gsl_acc.mkGsl_uChoice newuz (propagate_parallel s1 newus)
	      end

      | Gsl_parallel(s21,s22) ->  
	  propagate_parallel s1 (propagate_parallel s21 s22)

      | _ -> invalid_arg "propagate_parallel:substitution not allowed"

(* Indeed, parallel substitution is not of the implementation sort, whereas *)
(* sequence and while strictly are of that sort. As a consequence, these two *)
(* sorts of substitution can't coexist, so the sequence and the while need not *)
(* appear in the function that spreads the parallel *)
	  
  in 
  rebuild (
  auxapply substgsl predicate
 )

    
let rec calculate pred=
  match pred with
      Blast.PredParen(p) -> PredParen(calculate p)
    | Blast.PredNegation(p) -> PredNegation(calculate p)
    | Blast.PredExists(ids,p) -> PredExists(ids, calculate p)
    | Blast.PredForAll(ids,p) -> PredForAll(ids, calculate p)
    | Blast.PredBin(bin, p1, p2) -> 
	PredBin(bin, calculate p1, calculate p2)
    | Blast.PredAtom(_, _, _) -> pred 
    | Blast.SubstApply(s, p) -> 
	let donepred=calculate p in 
	let donesubst=Instanciation.apply_inner_instanciations s in
	let ids_to_avoid=Variables.all_vars_pred donepred in
	  apply (subst_to_gsl ids_to_avoid donesubst) donepred

