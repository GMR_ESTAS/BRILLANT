(**Library of GSL-handling functions
@author Samuel Colin*)

(** This function return all variable of predicate that are of the given kind (Free, Bound, Modified and All)
@param vartype indicate the kind of variable that should be affected
@param subst the substitution to be analyzed
@author Samuel Colin*)
val vars : Variables.variable_property -> Gsl_adt.gsl -> Id_handling.Idset.t

(** Retrieve all free variable from the substitution
@param subst the substitution to be analyzed
@author Samuel Colin*)
val free_vars : Gsl_adt.gsl -> Id_handling.Idset.t

(** Retrieve all bound variable from the substitution
@param subst the substitution to be analyzed
@author Samuel Colin*)
val bound_vars : Gsl_adt.gsl -> Id_handling.Idset.t

(** Retrieve all modified variable from the substitution
@param subst the substitution to be analyzed
@author Samuel Colin*)
val modified_vars : Gsl_adt.gsl -> Id_handling.Idset.t

(** Retrieve all variable from the substitution
@param subst the substitution to be analyzed
@author Samuel Colin*)
val all_vars : Gsl_adt.gsl -> Id_handling.Idset.t

(** Replace all occurence of var X by var Y in substitution substgen
@param x the variable to be replaced
@param y the variable that replace X
@param substgen a GSL where X should be replaced by Y
@author Samuel Colin*)
val id_subst : Blast.id -> Blast.id -> Gsl_adt.gsl -> Gsl_adt.gsl
