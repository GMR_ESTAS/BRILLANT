(**Here is the  Generalized Substitution Language  Abstract Data Type
@author Samuel Colin*)

type gsl =
    Gsl_affect of (Blast.id * Blast.expr) list
      (** Affectation  BBook p 236 (4.8) *) 
  | Gsl_skip
      (** Skip BBook p 247 (4.16)*)
  | Gsl_precondition of Blast.predicate * gsl
      (** PreCondition  BBook p 234 (4.7) *)
  | Gsl_bChoice of gsl * gsl
      (** Bounded Choice BBook p 244 (4.14) *)
  | Gsl_guard of Blast.predicate * gsl
      (** Guard  BBook p 246 *)
  | Gsl_uChoice of Blast.id list * gsl
      (** Unbounded Choice Book p 252 (4.18) *)
  | Gsl_parallel of gsl * gsl
      (** Parallel substitution Book p 243 (4.12) *)
  | Gsl_sequence of gsl * gsl
      (** Sequence substitution Book p 570 (12.1) *)
  | Gsl_while of Blast.predicate * gsl * Blast.expr * Blast.predicate
      (** While Loop Substitution  Book p 570 (12.1) *)
