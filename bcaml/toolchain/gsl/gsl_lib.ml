(* $Id$ *)

open Blast 
open Blast_mk
open Gsl_adt

open Id_handling
open Variables 
open Instanciation


let rec vars vartype subst=
  match subst with
    | Gsl_affect(_) ->
	let (xs, es)=List.split (Gsl_acc.affects_from subst) in
	  begin
	    match 
	      vartype 
	    with
	      | Free | All ->  
		  let expvars=
		    List.fold_right 
		      Idset.union 
		      (List.map (vars_expr vartype) es) 
		      Idset.empty 
		  in
		    List.fold_right Idset.add xs expvars
	      | Bound -> union_from_setlist (List.map (vars_expr vartype) es)
	      | Modified -> set_from_ids xs
	  end	  
    | Gsl_skip -> 
	Idset.empty
    | Gsl_precondition(_)
    | Gsl_guard(_) ->
	let c=Gsl_acc.cond_from subst in
	let s=Gsl_acc.gsl_from subst in
	  Idset.union (vars_pred vartype c) (vars vartype s)
    | Gsl_parallel(_)
    | Gsl_bChoice(_) 
    | Gsl_sequence(_)-> 
	let (s1, s2)=Gsl_acc.twosubsts_from subst in
	  Idset.union (vars vartype s1) (vars vartype s2)
    | Gsl_uChoice(_) ->
	let z=Gsl_acc.allids_from subst in
	let s=Gsl_acc.gsl_from subst in
	  begin
	    match vartype with
	      | Free -> List.fold_right Idset.remove z (vars vartype s)
	      | Bound | All -> List.fold_right Idset.add z (vars vartype s)
	      | Modified -> vars vartype s
	  end
    | Gsl_while(c,s,v,i) ->
	let set1=Idset.union (vars_pred vartype i) (vars_expr vartype v) in
	let set2=Idset.union (vars_pred vartype c) (vars vartype s) in
	  Idset.union set1 set2
	    
	    
	    
let free_vars      = vars Free
let bound_vars     = vars Bound
let modified_vars  = vars Modified
let all_vars       = vars All
  

  
let rec id_subst x y substgen=
  match substgen with
    | Gsl_affect(_) ->
	let (xs, es)=List.split (Gsl_acc.affects_from substgen) in
	let newxs=id_subst_ids x y xs in
	let newes=List.map (id_subst_expr x y) es in
	  Gsl_acc.mkGsl_Affect (List.combine newxs newes)
    | Gsl_skip -> 
	substgen
    | Gsl_precondition(_) -> 
	let c=Gsl_acc.cond_from substgen in
	let s=Gsl_acc.gsl_from substgen in
	  Gsl_acc.mkGsl_Precondition (id_subst_pred x y c) (id_subst x y s)
    | Gsl_bChoice(_) -> 
	let (s1, s2)=Gsl_acc.twosubsts_from substgen in
	  Gsl_acc.mkGsl_bChoice (id_subst x y s1) (id_subst x y s2)
    | Gsl_guard(c,s) ->
	Gsl_acc.mkGsl_Guard (id_subst_pred x y c) (id_subst x y s)
    | Gsl_uChoice(_) ->
	let z=Gsl_acc.allids_from substgen in
	let s=Gsl_acc.gsl_from substgen in
	  Gsl_acc.mkGsl_uChoice (id_subst_ids x y z) (id_subst x y s)
    | Gsl_sequence(_) ->
	let (s1, s2)=Gsl_acc.twosubsts_from substgen in
	  Gsl_acc.mkGsl_Sequence (id_subst x y s1) (id_subst x y s2)
    | Gsl_parallel(_) ->  
	let (s1, s2)=Gsl_acc.twosubsts_from substgen in
	  Gsl_acc.mkGsl_Parallel (id_subst x y s1) (id_subst x y s2)
    | Gsl_while(c,s,v,i) ->
	Gsl_acc.mkGsl_While
 	  (id_subst_pred x y c)
	  (id_subst x y s)
	  (id_subst_expr x y v)
	  (id_subst_pred x y i)

