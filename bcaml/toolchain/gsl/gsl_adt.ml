(* $Id$ *)

open Blast

(** Generalized Substitution Language  Abstract Data Type*)

type gsl =
      
  | Gsl_affect of (id * expr) list 
  | Gsl_skip 
  | Gsl_precondition of predicate * gsl 
  | Gsl_bChoice of gsl * gsl 
  | Gsl_guard of predicate * gsl 
  | Gsl_uChoice of id list * gsl 
  | Gsl_parallel of gsl * gsl 
  | Gsl_sequence of gsl * gsl 
  | Gsl_while of predicate * gsl * expr * predicate 

