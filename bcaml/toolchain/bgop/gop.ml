
(* $Id$ *)

(* 
      Copyright (C) 2007-2008 INRETS-ESTAS,  Samuel Colin 
*)

(*\section{Global process}*)


(*i*)
(* Variables that will change the nature of the generated proof obligations *)


(* Generating POs without splitting the goal (i.e. a conjunction) *)

let atelierB_generation = ref false 

(* Generating POs without applying the GSL calculus *)

let bbook_generation = ref false 

(* Like the '--AB' gop option *)
let set_atelierB_generation () = atelierB_generation:=true

(* Like the '--bbook' gop option *)
let set_bbook_generation () = bbook_generation:=true

(*i*)

(*
 Applique la fonction gen_po, qui cr�e des PO, au projet, applique les post-traitements adapt�s aux options et affiche la description du type de PO g�n�r�.

*)

let generate_po description gen_po project mchid output_directory=
  try
    begin
      let _ = Error.debug description in
      let _ = Error.debug ("(gop.generate_po) Treated machine:" ^ (Id_acc.name_of mchid)) in
      let auxpos=gen_po project mchid in
      let calculatedpos=
	
	(* POs post-processing *)
	if (not !bbook_generation) 
	then 
	  let expanded_pos=List.map (Gop_lib.expand_po project mchid) auxpos in
	  List.map Gop_lib.calculate expanded_pos
	else auxpos in
      let explodedpos=
	if !atelierB_generation 
	then List.flatten (List.map Gop_lib.explode calculatedpos)
	else calculatedpos in

        List.iter 
	  (Gop_lib.display_or_save output_directory (not !bbook_generation)) 
	  explodedpos
    end
  with
    | Po.No_po -> 
	Gop_lib.no_po description
    | Error.Not_implemented msg -> 
	Error.not_implemented (description ^ ":" ^ msg)
    | Invalid_argument e -> Gop_lib.po_cant_generate (description ^ ":" ^ e)
    | Error.Error msg -> Error.error ("generate_po:" ^ description ^ ":" ^ msg)
    | err -> (Error.warning description; raise err) 


let machine_pos=[
  (Gop_mch.po_initialization, 
   "[3.1] Checking Initialization" );
  (Gop_mch.po_assertions, 
   "[3.2] Checking Assertions" );
  (Gop_mch.po_operations, 
   "[3.3] Checking Operations" );
  (Gop_mch.po_includes_invariant, 
   "[?.?] Checking Initialisation for Included Components" );
  (Gop_mch.po_includes_params, 
   "[?.?] Checking Parameters for Included Components" );
  (Gop_mch.po_includes_assertions, 
   "[?.?] Checking Assertions of Included Components" );
  (Gop_mch.po_includes_operations, 
   "[?.?] Checking Operations of Included Components" );
  (Gop_mch.po_uses_invariant, 
   "[?.?] Checking Initialisations of Used Components" );
  (Gop_mch.po_uses_operations,
   "[?.?] Checking Operations of Used Components");
  (Gop_mch.po_uses_assertions, 
   "[?.?] Checking Assertions of Used Components")]

(*Idem avec les fonctions de cr�ations de PO li�es aux raffinements*)
let refinement_pos=[
  (Gop_ref.po_params, 
   "[4.1] Checking Parameters refinement");
  (Gop_ref.po_assertions, 
   "[4.2] Checking Assertion refinement");
  (Gop_ref.po_initialization, 
   "[4.3] Checking Initialization refinement");
  (Gop_ref.po_operations, 
   "[4.4] Checking Operations refinement")]

(*Idem avec les fonctions de cr�ations de PO li�es aux impl�mentations*)
let implementation_pos=[
  (Gop_imp.po_params, 
   "[] Checking Parameters of Implementation");
  (Gop_imp.po_values, 
   "[5.2] Checking Valuations of Implementation");
  (Gop_imp.po_assertions, 
   "[5.3] Checking Assertions of Implementation");
  (Gop_imp.po_initialization, 
   "[5.4] Checking Initialization of Implementation");
  (Gop_imp.po_operations, 
   "[??] Checking Operations of Implementation")]

(*
Cr�e les PO li�es � gop_mch en utilisant la liste machine_pos pour donner la bonne fonction de g�n�ration � generate_po
*)

let machine project mchid output_directory=
  let _ = Error.debug ("(gop.gop_machine) Entering") in
  let generate description genfunction=
    try
      generate_po description genfunction project mchid output_directory
    with
      | Error.Error msg -> Error.debug ("gop_machine:" ^ msg)
      | err -> (Error.warning "gop_machine : unexpected error"; raise err)
  in
  let (mchfuns, descriptions)=List.split machine_pos in
    List.iter2 generate descriptions mchfuns

(* PO for refinement component *)

let refinement project mchid output_directory=
  let _ = Error.debug ("gop.gop_refinement Entering") in
  let generate description genfunction=
    try
      generate_po description genfunction project mchid output_directory
    with
      | Error.Error msg -> Error.debug ("gop_refinement:" ^ msg)
      | err -> (Error.warning "gop_refinement : unexpected error"; raise err)
  in
  let (mchfuns, descriptions)=List.split refinement_pos in
    List.iter2 generate descriptions mchfuns

(*idem avec les PO d'implementation*)
let implementation project mchid output_directory=
  let _ = Error.debug ("(gop.gop_implementation) Entering ") in
  let generate description genfunction=
    try
      generate_po description genfunction project mchid output_directory
    with
      | Error.Error msg -> Error.debug ("gop_implementation:" ^ msg)
      | err -> (Error.warning "gop_implementation : unexpected error"; raise err)
  in
  let (mchfuns, descriptions)=List.split implementation_pos in
    List.iter2 generate descriptions mchfuns

