(* $Id$ *)

(*Cette section rassemble les fonctions destin�es � cr�er les POs li�es aux machines abstraites*)

(*i*)

(* 
   La d�pendance sur Po_adt devrait etre �limin�e 
*)

open Blast
open Po
open Gop_lib
open Project

(*Regarde si l'argument est un bon candidat � la cr�ation de PO de machine, sauf include et import*)
let candidate_simple_po ast=
  (Blast_acc.is_abstract_machine ast) 
  && (Blast_acc.is_basic_machine ast)

(*Regarde si l'argument est un bon candidat � la cr�ation de PO d'inclusion de machine*)
let candidate_includes_po ast=
  (Blast_acc.is_abstract_machine ast)
  && ((Clause_acc.get_Includes (Blast_acc.get_clause_list ast)) <> [])
  && ((Clause_acc.get_Uses (Blast_acc.get_clause_list ast)) = [])

(*Regarde si l'argument est un bon candidat � la cr�ation de PO d'inclusion de  machine*)
let candidate_uses_po ast=
  (Blast_acc.is_abstract_machine ast)
  && ((Clause_acc.get_Uses (Blast_acc.get_clause_list ast)) <> [])
  && ((Clause_acc.get_Includes (Blast_acc.get_clause_list ast)) = [])



(* \section{Initialization/Invariant} *)

(*Cr�e les PO de base d'une machine*)
let po_initialization project mchid =
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_simple_po machine) then raise No_po;
    let g=
      begin
	let inv=try 
	  Po_acc.get_component (Invariant(mchid)) machine 
	with  Not_found -> Blast_mk.mk_PredTrue ()
	in
	  try 
	    SubstApply(
	      Clause_acc.get_Initialisation 
		 (Blast_acc.get_clause_list machine),
	      inv)
	  with Not_found -> inv
      end
    in
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let hyprequest=(mchreq mchid)@seenhyps in
    let finalpo=
      { 
	hypos = Po_acc.mkHypothesis hyprequest project mchid;
	goal = g;      
	trace = "Initialization";
	filename = (Id_acc.name_of mchid) ^ ".mch";
      }
    in
      [finalpo]


(* \section{Assertions} *)
(* \gopdoc{gop-mch-assertions} *)

(*Cr�e les PO li�es aux assertions d'une machine*)
let po_assertions project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_simple_po machine) 
    then raise No_po;
    let finalgoal=
      try Po_acc.get_component (Assertions(mchid)) machine 
      with Not_found -> raise No_po
    in     
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id); Invariant(id)] 
    in

    let seenhyps=Gop_lib.seen_hypos project mchid in

    let hyprequest=(mchreq mchid)@seenhyps in
      

    let finalpo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid;
	goal = finalgoal;
	trace= "Assertions";
	filename = (Id_acc.name_of mchid) ^ ".mch";
      } 
    in
      [finalpo]
	

(* \section{Op�rations/Invariant} *)
(* \gopdoc{gop-mch-operations} *)

let po_operations project mchid =
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_simple_po machine) then raise No_po;
  
    let mchgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
    let mchreq=
      fun id -> [
	(* A *) Parameters(id); 
	(* B *) Sets(id); 
	(* C *) Constraints(id); 
	(* P *) Properties(id); 
	(* I *) Invariant(id); 
	(* J *) Assertions(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let hyprequest=(mchreq mchid)@seenhyps in
    let basepo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid;
	goal = mchgoal;
	trace= "Operations"; 
	filename = (Id_acc.name_of mchid) ^ ".mch";
      } 
    in
    let all_ops=
      Clause_acc.get_Operations 
	(Blast_acc.get_clause_list machine) in
    let all_opcomps=List.map Gop_lib.decompose_operation all_ops in
    let all_mypos=
      List.map (Gop_lib.complete_po_with_operation basepo) all_opcomps in
      if all_mypos=[]
      then raise No_po
      else all_mypos
	
	  
(* \section{Includes/included machines constraints} *)

(*Cr�e les PO li�es aux param�tres inclus par une machine*)
let po_includes_params project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_includes_po machine) then raise No_po;
 
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let hyprequest=(mchreq mchid)@seenhyps in
    let increq=fun id -> [Parameters(id);Constraints(id)] in
    let incedges=Bgraph.filter_sourceDep project.graph Bgraph.Includes mchid in
    let includedreqs=List.flatten (List.map increq (List.map Bgraph.target incedges)) in

    let incgoals=Po_acc.mkPoPredicate includedreqs project mchid in
(* As all POs have the same hypotheses, let's gather all that *)
    let finalgoal=
      match incgoals with
	| None -> raise No_po
	| Some(pred) -> pred 
    in
    let basepo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid ;
	goal = finalgoal;
	filename = (Id_acc.name_of mchid) ^ ".mch";
	trace= "Includes_parameters"; 
      } 
    in [basepo]
      
 
(*i*)

(* \section{Includes/Assertions} *)

(*Cr�e les PO li�es aux assertions inclus par une machine*)
let po_includes_assertions project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_includes_po machine) then raise No_po;
  
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id); Invariant(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let increq=
      fun id -> [Sets(id); Properties(id); Invariant(id); Assertions(id)] 
    in
    let incedges=Bgraph.filter_sourceDep project.graph Bgraph.Includes mchid in
    let includedreqs=List.flatten (List.map increq (List.map Bgraph.target incedges)) in

    let hyprequest=(mchreq mchid)@(seenhyps@includedreqs) in
    let finalgoal=
      try Po_acc.get_component (Assertions(mchid)) machine 
      with Not_found -> raise No_po
    in
    let finalpo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid;
	goal = finalgoal;
	filename=(Id_acc.name_of mchid) ^ ".mch";
	trace="Includes_assertions";
      } 
    in
      [finalpo]


(* \section{Includes/operations} *)

(*Cr�e les PO li�es aux op�rations inclus par une machine*)
let po_includes_operations project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_includes_po machine) then raise No_po;
  
    let mchgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id); Invariant(id); Assertions(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let increq=
      fun id -> [Sets(id); Properties(id); Invariant(id); Assertions(id)] 
    in
    let incedges=Bgraph.filter_sourceDep project.graph Bgraph.Includes mchid in
    let includedreqs=List.flatten (List.map increq (List.map Bgraph.target incedges)) in

    let hyprequest=(mchreq mchid)@(seenhyps@includedreqs) in
    let basepo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid ;
	goal = mchgoal;
	filename=(Id_acc.name_of mchid) ^ ".mch";
	trace="Includes_operations";
      } 
    in
    let all_ops=Clause_acc.get_Operations 
		  (Blast_acc.get_clause_list machine) in
    let all_opcomps=List.map Gop_lib.decompose_operation all_ops in
    let all_mypos=
      List.map (Gop_lib.complete_po_with_operation basepo) all_opcomps in
      if all_mypos=[]
      then raise No_po
      else all_mypos



(* \section{Includes/invariant} *)

(*Cr�e les PO li�es aux invariants inclus par une machine*)
let po_includes_invariant project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_includes_po machine) then raise No_po;
 
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let hyprequest=(mchreq mchid)@seenhyps in
    let invgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
    let mchinit=Gop_lib.get_initialization project mchid in
    let increq=fun id -> [Sets(id); Properties(id)] in
    let allinstanciations=
      Gop_lib.initializations_instanciations project (Blast.Includes([])) mchid
    in

    let (idInsts, substInsts)=List.split allinstanciations in

    let totalInstanciation=
      Blast_mk.mk_SubstSequence_from_list substInsts in
    let allincreqs=List.flatten (List.map increq idInsts) in

    let finalpo=
      { 
	hypos = Po_acc.mkHypothesis (hyprequest@allincreqs) project mchid;
	goal = SubstApply(
		 SubstSequence(
		   totalInstanciation,
		   mchinit),
		 invgoal);
	trace = "Includes_invariant";
	filename = (Id_acc.name_of mchid) ^ ".mch";
      }
    in
      [finalpo]


(* \section{Uses/Invariant} *)

(*Cr�e les PO li�es aux invariants import�s par une machine*)
let po_uses_invariant project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_uses_po machine) then raise No_po;

    let invar=try 
      Po_acc.get_component (Invariant(mchid)) machine 
    with  Not_found -> Blast_mk.mk_PredTrue ()
    in

    let problem_variables=Gop_lib.retrieve_used_variables project mchid in
    let clean_invar=
      Gop_lib.remove_variable_dependent_predicates 
	problem_variables
	invar
    in
    let goalpred=
      match clean_invar with
	| None -> raise No_po
	| Some(good_invariant) -> good_invariant
    in
    let g=
      try 
	SubstApply(
	  Clause_acc.get_Initialisation 
	    (Blast_acc.get_clause_list machine),
	  goalpred)
      with Not_found -> goalpred
    in
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let usedreqs=List.flatten 
		  (List.map mchreq (Blast_acc.get_used_mchids machine)) 
    in
    let hyprequest=((mchreq mchid)@seenhyps)@usedreqs in
    let finalpo=
      { 
	hypos = Po_acc.mkHypothesis hyprequest project mchid;
	goal = g;      
	trace = "Uses_initialisation";
	filename = (Id_acc.name_of mchid) ^ ".mch";
      }
    in
      [finalpo]


(* \section{Uses/Assertions} *)

(*Cr�e les PO li�es aux assertionss import�s par une machine*)
let po_uses_assertions project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_uses_po machine) then raise No_po;
    
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id); Invariant(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let usedreqs=List.flatten 
		  (List.map mchreq (Blast_acc.get_used_mchids machine)) 
    in
    let hyprequest=((mchreq mchid)@seenhyps)@usedreqs in
    let finalgoal=
      try Po_acc.get_component (Assertions(mchid)) machine 
      with Not_found -> raise No_po
    in
    let finalpo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid;
	goal = finalgoal;
	trace= "Uses_assertions";
	filename = (Id_acc.name_of mchid) ^ ".mch";
      } 
    in
      [finalpo]


(* \section{Uses/Operations} *)

(*Cr�e les PO li�es aux op�rations import�s par une machine*)
let po_uses_operations project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_uses_po machine) then raise No_po;
  
    let mchgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
    let mchreq=
      fun id -> [Parameters(id); Sets(id); Constraints(id); Properties(id); Invariant(id); Assertions(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let usedreqs=List.flatten 
		  (List.map mchreq (Blast_acc.get_used_mchids machine)) 
    in
    let hyprequest=((mchreq mchid)@seenhyps)@usedreqs in
    let basepo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid ;
	goal = mchgoal;
	trace= "Uses_operations"; 
	filename = (Id_acc.name_of mchid) ^ ".mch";
      } 
    in
    let all_ops=Clause_acc.get_Operations 
		  (Blast_acc.get_clause_list machine) in
    let all_opcomps=List.map Gop_lib.decompose_operation all_ops in
    let all_mypos=
      List.map (Gop_lib.complete_po_with_operation basepo) all_opcomps in
      if all_mypos=[]
      then raise No_po
      else all_mypos
	  
