 (* $Id$ *)

(* 
      Copyright (C) 2007-2008 INRETS-ESTAS,  Samuel Colin 
*)

  
(*Cette section regroupe les fonctions d'interfaces*)


let output_directory = ref "./"

let include_directories = ref [ "./" ]

let add_include_dir path = include_directories := !include_directories@[path]

let basic_path () = 
  try
    let basepath=Sys.getenv "BASICS_PATH" in
      add_include_dir basepath
  with
      _ ->  Error.env_variable "BASICS_PATH"

(*
let basic_name file_path =
  (basic_path () ^"/"^(Bfile.base_name file_path))
*)

(* let dest_set path = output_directory := Bfile.dest_check path *)
let dest_set path = output_directory := path
    
let ab_like () = Gop.set_atelierB_generation ()

let bbook_set () = Gop.set_bbook_generation ()

let filenames=ref []

let projectnames=ref []
  
let add_filename str=
  let _ = Error.debug ("Adding to filenames : " ^ str) in
  filenames:=str::(!filenames)

let add_projectname str=
  let _ = Error.debug ("Adding to projectnames : " ^ str) in
  projectnames:=str::(!projectnames)

   
let rec args = 
  [
    ("--help",
     Arg.Unit(fun () -> Arg.usage args (Bfile.info()) ),
     "\t\t display this help")
    ;
    ("-I", 
     Arg.String(add_include_dir),
     "dir, \t\t specifies another directory to add in search path")
    ;
    ("--dest",
     Arg.String(dest_set),
     "dir, \t\t place the generated files in the directory dir")
    ;
    ("--AB",
     Arg.Unit(ab_like),
     "\t\t\t compatibily mode <<� la Atelier B>> (default to false)")
    ;
    ("--bbook",
     Arg.Unit(bbook_set),
     "\t\t do not apply substitution calculus (default to false)")
    ;
    ("--project",
     Arg.String(add_projectname),
     "<filename>,  \t Parses and scopes <filename> and all machines it depends on (deactivates reading from stdin and output to stdout). \n\t\t\t\t Used for the debugging of the scoping phase.")
    ;
    ("--debug",
     Arg.Unit(Error.debug_set),
     "\t\t debug mode");
  ]
    
let treat_machine project mchid=
  begin
    Gop.machine project mchid !output_directory;
    Gop.refinement project mchid !output_directory;
    Gop.implementation project mchid !output_directory
  end
  

let process_file filename =
  begin
    let _ = Error.debug ("Processing file : "^filename) in
    let parsed_machines=Parser_lib.overParse !include_directories filename in
    let built_project=Project.buildProject parsed_machines in
    let (mchid, _)=Project.find_name built_project (Bfile.root_name filename) in
      treat_machine built_project mchid
  end
   
 
let process_project filename =
  begin
    Error.debug ("Processing project : "^filename) ;
    let parsed_machines=Parser_lib.overParse !include_directories filename in
    let built_project=Project.buildProject parsed_machines in
    let mchids=Project.machines_ids built_project in
      List.iter
	(fun i -> treat_machine built_project i)
	mchids
  end
    
    
let process_filenames ()=
  List.iter process_file !filenames
    
let process_projectnames ()=
  List.iter process_project !projectnames
    
let do_generate ()=
    let _ = Error.debug ("do_generate") in
  process_filenames ();
  process_projectnames ()
    
    
let _ = 
  Arg.parse args add_filename (Bfile.info());
  do_generate ()

    
    
    

