(**This file handle the command line and call others modules depending on
@author Samuel Colin*)

(**Contain the path where output should be written
@author Samuel Colin*)
val output_directory : string ref

(**Contain the list of the directories that should be included
@author Samuel Colin*)
val include_directories : string list ref

(**Add the argument to the included directory list
@author Samuel Colin*)
val add_include_dir : string -> unit

(**Add the path where the program is to the included directory list
@author Samuel Colin*)
val basic_path : unit -> unit

(**Set the output directory to the argument
@author Samuel Colin*)
val dest_set : string -> unit

(**Initialize the AtelierB-like PO generation option ; see gop.mli for details
@author Samuel Colin*)
val ab_like : unit -> unit

(**Initialize the B-Book PO generation option ; see gop.mli for details
@author Samuel Colin*)
val bbook_set : unit -> unit

(**List the file currently in use
@author Samuel Colin*)
val filenames : string list ref

(**List the project currently in use
@author Samuel Colin*)
val projectnames : string list ref

(**Add the file to the file list
@author Samuel Colin*)
val add_filename : string -> unit

(**Add the project to the project list
@author Samuel Colin*)
val add_projectname : string -> unit

(**handle arguments and options
@author Samuel Colin*)
val args : (Arg.key * Arg.spec * Arg.doc) list

(**Create all POs for the given project and machine
@author Samuel Colin*)
val treat_machine : Project.t -> Blast.id -> unit

(**Create all POs for the given project and machine
@author Samuel Colin*)
val process_file : string -> unit

(**Parse the file for the machines that are in, build accordingly a project, and generate the PO for machine in
@author Samuel Colin*)
val process_file : string -> unit

(**Parse the file and retrieve the machine for the project that is in, then generate the POs for the whole project
@author Samuel Colin*)
val process_project : string -> unit

(**Process the files that are in filenames
@author Samuel Colin*)
val process_filenames : unit -> unit

(**Process the projects that are in projectnames
@author Samuel Colin*)
val process_projectnames : unit -> unit

(**Process the content of both filenames and projectnames
@author Samuel Colin*)
val do_generate : unit -> unit
