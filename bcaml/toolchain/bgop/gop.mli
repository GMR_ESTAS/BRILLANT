(* $Id$ *)

(* 
      Copyright (C) 2007-2008 INRETS-ESTAS,  Samuel Colin 
*)


(**

Here are the interface function that call PO-generating function and post-proceeding function depending on options
@author Samuel Colin

*)



(** Variables that will change the nature of the generated proof obligations 
*)

(** Generate PO as close as possible as AtelierB would
*)
val atelierB_generation : bool ref

(** Do not expand PO
*)
val bbook_generation : bool ref

(**Initialize the atelierB_generation
*)
val set_atelierB_generation : unit -> unit

(**Initialize the BBook_generation
*)
val set_bbook_generation : unit -> unit

(**Apply the PO-generating function gen_po to the given machine, then post-proceed the POs depending on the options (see above) and finally write it

@param description  description of the PO-generating function
@param gen_po the function that generate the POs
@param project the active project
@param mchid the machine that is to be worked on
@param output_directory the directory where PO should be written*)
val generate_po :
  string ->
  (Project.t -> Blast.id -> Po.t list) ->
  Project.t -> Blast.id -> string -> unit

(**list of PO-generating function related to machine along with the description of the function
*)
val machine_pos :
  ((Project.t -> Blast.id -> Po.t list) * string) list

(**list of PO-generating function related to refinement along with the description of the function
*)
val refinement_pos :
  ((Project.t -> Blast.id -> Po.t list) * string) list

(**list of PO-generating function related to implementation along with the description of the function
*)
val implementation_pos :
  ((Project.t -> Blast.id -> Po.t list) * string) list

(**Create PO related with machine by using generate_po with the PO-generating function stored in machine_pos

@param project the active project
@param mchid the machine that is to be worked on
@param output_directory the directory where PO should be written*)
val machine : Project.t -> Blast.id -> string -> unit

(**Create PO related with refinement by using generate_po with the PO-generating function stored in machine_pos

@param project the active project
@param mchid the machine that is to be worked on
@param output_directory the directory where PO should be written*)
val refinement : Project.t -> Blast.id -> string -> unit

(**Create PO related with implementation by using generate_po with the PO-generating function stored in machine_pos

@param project the active project
@param mchid the machine that is to be worked on
@param output_directory the directory where PO should be written*)
val implementation : Project.t -> Blast.id -> string -> unit
