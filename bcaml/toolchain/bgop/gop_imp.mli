(** Proof Obligation generation for IMPLEMENTATION components
@author Samuel Colin*)

(**Create the PO required by parameter implements

@param project the project corresponding to the machine
@param mchid the machine that is to be parsed*)
val po_params : Project.t -> Blast.id -> Po.t list

(**Create the PO required by assertions implements, as in B Book [5.3]

@param project the project corresponding to the machine
@param mchid the machine that is to be parsed*)
val po_assertions : Project.t -> Blast.id -> Po.t list

(**Create the PO required by initialization implements, as in B Book [5.4]

@param project the project corresponding to the machine
@param mchid the machine that is to be parsed*)
val po_initialization : Project.t -> Blast.id -> Po.t list

(**Create the PO required by operation implements

@param project the project corresponding to the machine
@param mchid the machine that is to be parsed*)
val po_operations : Project.t -> Blast.id -> Po.t list

(**Create the PO required by value implements, as in B Book [5.2]

@param project the project corresponding to the machine
@param mchid the machine that is to be parsed*)
val po_values : Project.t -> Blast.id -> Po.t list
