(* $Id$ *)

(*Cette section regroupe les fonctions qui cr�ent les POs li�e � l'impl�mentation*)
 
(*i*)
open Blast
open Po
open Project  (* TODO: TO BE REMOVED... *)

  
(*i*)
      
(*Regarde si il y a des POs li� � l'impl�mentation de param�tre*)
let candidate_implements_params_po ast=
  (Blast_acc.is_implementation ast)
  && ((Clause_acc.get_Imports (Blast_acc.get_clause_list ast)) <> [])

(*Regarde si il y a des POs li� simplement � l'impl�mentation*)
let candidate_implements_po ast=
  (Blast_acc.is_implementation ast)
        

(* 
 \gopdoc{gop-imp-params} 
*)

(* Except previously included machines are imported here, this is the same 
code as for refined machines *)

(*Cr�e les POs li� � l'impl�mentation de param�tres*)
let po_params project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_implements_params_po machine) then raise No_po;


    let refid_sequence=Gop_lib.create_refid_sequence project mchid in
    let reverse_refids=List.rev refid_sequence in
    let firstmchid=List.hd reverse_refids in

    let hypACreq=(fun id -> [Parameters(id); Constraints(id)]) in
    let seqreq=(fun id -> [Sets(id); Properties(id)]) in
    let wholeseqreq=List.flatten (List.map seqreq reverse_refids) in

    let seenhyps=Gop_lib.seen_hypos project mchid in
    let corehyps= (hypACreq firstmchid)@wholeseqreq@seenhyps in

    let impreq=hypACreq in
    let impedges=Bgraph.filter_sourceDep project.graph Bgraph.Imports mchid in
    let importedreqs=List.flatten (List.map impreq (List.map Bgraph.target impedges)) in

    let impgoals=Po_acc.mkPoPredicate importedreqs project mchid in
(* As all POs have the same hypotheses, let's gather all that *)

    let finalgoal=
      match impgoals with
	| None -> raise No_po
	| Some(pred) -> pred 
    in
    let basepo=
      { hypos = Po_acc.mkHypothesis corehyps project mchid ;
	goal = finalgoal;
	filename = (Id_acc.name_of mchid) ^ ".imp";
	trace= "Implements_params"; 
      } 
    in [basepo]


(* \section{Assertions} *)
(* \gopdoc{gop-imp-assertions} *)

(*Cr�e des POs li� � l'impl�mentation des assertions*)
let po_assertions project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_implements_po machine) then raise No_po;
    
    let finalgoal=
      try Po_acc.get_component (Assertions(mchid)) machine 
      with Not_found -> raise No_po
    in
    let refid_sequence=Gop_lib.create_refid_sequence project mchid in
    let reverse_refids=List.rev refid_sequence in
    let firstmchid=List.hd reverse_refids in
    let prev_refids=List.rev (List.tl refid_sequence) in

    let hypACreq=(fun id -> [Parameters(id); Constraints(id)]) in
    let seqreq=(fun id -> [Sets(id); Properties(id)]) in
    let invreq=(fun id -> Invariant(id)) in
    let assreq=(fun id -> Assertions(id)) in

    let acfirstreq=(hypACreq firstmchid) in
    let wholeseqreq=List.flatten (List.map seqreq reverse_refids) in
    let allinvreq=List.map invreq reverse_refids in
    let allprevassreq=List.map assreq prev_refids in
    let impreq=
      (fun id -> 
	 [Sets(id); Properties(id); Invariant(id); Assertions(id)]) 
    in
    let impedges=Bgraph.filter_sourceDep project.graph Bgraph.Imports mchid in
    let importedreqs=List.flatten (List.map impreq (List.map Bgraph.target impedges)) in

    let seenhyps=Gop_lib.seen_hypos project mchid in

    let hyprequest=
      (acfirstreq 
       @ wholeseqreq 
       @ allinvreq 
       @ allprevassreq) 
      @ 
      (importedreqs @ seenhyps)
    in

    let basepo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid ;
	goal = finalgoal;
	filename = (Id_acc.name_of mchid) ^ ".imp";
	trace= "Implements_assertions"; 
      } 
    in [basepo]


(* \section{Initialisation} 
\gopdoc{gop-imp-initialisation}
*)

(*Cr�e des POs li� � l'impl�mentation*)
let po_initialization project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_implements_po machine) then raise No_po;

    let invgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
 
    let refid_sequence=Gop_lib.create_refid_sequence project mchid in
    let reverse_refids=List.rev refid_sequence in

    let hypACreq=(fun id -> [Parameters(id); Constraints(id)]) in
    let seqreq=(fun id -> [Sets(id); Properties(id)]) in

    let seenhyps=Gop_lib.seen_hypos project mchid in
    let wholeseqreq=List.flatten (List.map seqreq reverse_refids) in
    let hyprequest=(hypACreq mchid)@wholeseqreq@seenhyps in


    let not_invgoal = PredNegation invgoal in
    let previnit_notinv=
       try
	let beforelastid=List.hd (List.tl refid_sequence) in
	let prevmch=(snd (Project.find project beforelastid)).machine in
	let prevmchinit=
	  Clause_acc.get_Initialisation (Blast_acc.get_clause_list prevmch)
	in
	  SubstApply(prevmchinit, not_invgoal)
      with
	  Clause_acc.No_such_clause -> not_invgoal
    in
      
    let npini_goal = PredNegation(previnit_notinv) in

(* SC: Il peut ne pas y avoir d'init... *)
    let mchinit=Gop_lib.get_initialization project mchid in

    let impreq=fun id -> [Sets(id); Properties(id)] in
    let allinstanciations=
      Gop_lib.initializations_instanciations 
	project (Blast.Imports([])) mchid
    in

    let build_po id=
      { hypos = Po_acc.mkHypothesis (hyprequest@(impreq id)) project mchid ;
	goal = SubstApply(
		 SubstSequence(
		   List.assoc id allinstanciations,
		   mchinit),
		 npini_goal);
	filename = (Id_acc.name_of mchid) ^ ".imp";
	trace="Implements_initialization";
      } 
    in
    let allpos=List.map build_po (fst (List.split allinstanciations)) in
      if allpos=[] then (* there are no imported machines *)
	[{ hypos = Po_acc.mkHypothesis hyprequest project mchid ;
	  goal = SubstApply(mchinit, npini_goal) ;
	  filename = (Id_acc.name_of mchid) ^ ".imp";
	  trace="Implements_initialization";
	}] 
      else allpos


(* \section{Operations} *)


(* The only difference wrt the definition of the similar proof obligation for 
 refined machines (except imported machines instead of included ones) is the
 addition of all preconditions of all the refined operations in the hypotheses
*) 

(*Cr�e des POs li� � l'impl�mentation des op�rations*)
let po_operations project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_implements_po machine) then raise No_po;
  
    let invgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
    let refid_sequence=Gop_lib.create_refid_sequence project mchid in
    let reverse_refids=List.rev refid_sequence in
    let firstmchid=List.hd reverse_refids in

    let hypACreq=(fun id -> [Parameters(id); Constraints(id)]) in
    let allmchreq=
      fun id -> [Sets(id); Properties(id); Invariant(id); Assertions(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let impreq=
      fun id -> [Sets(id); Properties(id); Invariant(id); Assertions(id)] 
    in
    let impedges=Bgraph.filter_sourceDep project.graph Bgraph.Imports mchid in
    let importedreqs=List.flatten (List.map impreq (List.map Bgraph.target impedges)) in

    let hyprequest=(hypACreq firstmchid) @ 
		   (List.flatten (List.map allmchreq reverse_refids)) @ 
		   seenhyps @ importedreqs
    in
    
    let basepo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid ;
	goal = invgoal;
	filename=(Id_acc.name_of mchid) ^ ".imp";
	trace="Implements_operations";
      } 
    in
    let all_opids=List.map 
		    Clause_acc.get_operation_id
		    (Clause_acc.get_Operations 
		       ( Blast_acc.get_clause_list machine)) 
    in
    let local_operations =
      Clause_acc.get_LocalOperations ( Blast_acc.get_clause_list machine)
    in
    let (local_opids, real_opids) =
      List.partition
	(fun opid -> List.exists (fun (local_id,_,_,_) -> local_id = opid) local_operations)
	all_opids
    in
    let all_mypos=List.map
		    (fun id -> Gop_lib.complete_po_with_implemented_operation basepo id project mchid)
		    real_opids
    in
      if all_mypos=[]
      then raise No_po
      else all_mypos

(* \section{Values} *)

(*Cr�e des POs li� � l'impl�mentation de valeurs*)
let po_values project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_implements_po machine) then raise No_po;

    let allproperties=
      let retrievetmp=Gop_lib.retrieve_refined_properties project mchid in
	if retrievetmp=[] then raise No_po else retrievetmp
    in
    let basegoal=Blast_mk.mk_Conjunction allproperties in
      
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let impreq=(fun id -> [Sets(id); Properties(id)]) in
    let impedges=Bgraph.filter_sourceDep project.graph Bgraph.Imports mchid in
    let importedreqs=
      List.flatten 
	(List.map impreq (List.map Bgraph.target impedges)) in

    let hyprequest=importedreqs@seenhyps in 
    let impvalues=
      try Blast_acc.get_implementation_values machine 
      with Blast_acc.Empty_tree -> raise No_po
    in
      
    let abst_consts=Gop_lib.get_abstract_constants_from_refined project mchid
    in

    let (idents, exprs)=List.split impvalues in
    let appliedgoal=
      SubstApply(
	SubstSetEqualIds(idents, exprs),
	basegoal)
    in 
    let finalgoal=
      if abst_consts = []
      then appliedgoal
      else PredExists(abst_consts, appliedgoal)
    in
    let finalpo=
    { 
      hypos=Po_acc.mkHypothesis hyprequest project mchid ;
      goal=finalgoal;
      trace="Implements_values";
      filename=(Id_acc.name_of mchid) ^ ".imp"
    } 
    in
      [finalpo]

