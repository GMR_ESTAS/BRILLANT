(** Proof Obligation generation for MACHINE components
@author Samuel Colin *)


(** PO list basically required by any machine

@param project the current project
@param mchid the machine that is to be parsed*)
val po_initialization : Project.t -> Blast.id -> Po.t list

(** PO list basically required by assertions

@param project the current project
@param mchid the machine that is to be parsed*)
val po_assertions : Project.t -> Blast.id -> Po.t list

(** PO list required by operations

@param project the current project
@param mchid the machine that is to be parsed*)
val po_operations : Project.t -> Blast.id -> Po.t list

(** PO list required by included parameters

@param project the current project
@param mchid the machine that is to be parsed*)
val po_includes_params : Project.t -> Blast.id -> Po.t list

(** PO list required by included assertions

@param project the current project
@param mchid the machine that is to be parsed*)
val po_includes_assertions : Project.t -> Blast.id -> Po.t list

(** PO list required by included operations

@param project the current project
@param mchid the machine that is to be parsed*)
val po_includes_operations : Project.t -> Blast.id -> Po.t list

(** PO list required by included invariants

@param project the current project
@param mchid the machine that is to be parsed*)
val po_includes_invariant : Project.t -> Blast.id -> Po.t list

(** PO list required by included invariants

@param project the current project
@param mchid the machine that is to be parsed*)
val po_uses_invariant : Project.t -> Blast.id -> Po.t list

(** PO list required by used assertions

@param project the current project
@param mchid the machine that is to be parsed*)
val po_uses_assertions : Project.t -> Blast.id -> Po.t list

(** PO list required by used operations

@param project the current project
@param mchid the machine that is to be parsed*)
val po_uses_operations : Project.t -> Blast.id -> Po.t list
