(* $Id$ *)

(*Regroupe les types particuliers au gop
@author Samuel Colin*)

(* \subsection{Abstract type for proof obligations} *)

(*Exception renvoy� par une fonction quand elle n'a pas � cr�er de PO*)
exception No_po
  
(*Type composant, sert pour cr�er des hypoth�ses*)
type components=
  | Parameters of Blast.id
      (* Set parameters of the machine are int posets *)
  | Sets of Blast.id 
      (* Local sets of the machine are int posets and have distincts 
	 elements *)
  | Constraints of Blast.id
  | Properties of Blast.id
  | Invariant of Blast.id
  | Assertions of Blast.id

(*Type hypoth�ses, pour les PO*)
type hypos= components list

(*Type destin� � repr�senter les PO*)      
type t=
    { 
      hypos : Blast.predicate option;
      goal : Blast.predicate;
      mutable trace : string;
      filename : string;
    }

(*
SC : inutile?
let mk_goal po goal =   po.goal = goal


let mk_trace po trace =   po.trace = trace
*)

(* 
   The general shape of a proof obligation is:
   $ P \Rightarrow G $
   where:
   \begin{itemize}
   \item P represents the \emph{premisses}
   \item G represents the \emph{goal}
   \end{itemize}
   The typing indicates also that a PO can contain no hypotheses, and the goal
   may be either composed only with predicates, or applications of
   substitutions to predicates in addition to "normal" predicates.

   \emph{trace} shall contain informations allowing to find the origin of a
   PO. This field is not heavily used as for now.
   \emph{filename} is the field containing the filename under which the
   resulting PO will be saved.
*)
