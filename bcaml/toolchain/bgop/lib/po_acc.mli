(**Here are the accessor for project component used by the GOP
@author Samuel Colin*)

(**This function type a predicat
@author Samuel Colin
@param paramid the ID of the parameter*)
val mk_PowOne_predtyping : Blast.id -> Blast.predicate

(**Give type of machine parameter
@author Samuel Colin
@param mchhead the target machine, as returned by  amn_params_ids*)
val typings_of_mchparams : 'a * Blast.id list -> Blast.predicate list

(** Create the predicat that express the fact that the first ID is different from every ID in the idlist
@author Samuel Colin
@param id the ID who must be distinct from the idlist
@param idlist the reference ID list*)
val setEnumDec_to_pred : Blast.id -> Blast.id list -> Blast.predicate

(**Return predicates that express that every ID in the list are distinct
@author Samuel Colin
@param idlist liste of IDs to be check*)
val mkDistinctsIdsPred : Blast.id list -> Blast.predicate

(**type the setclause
@author Samuel Colin
@param mchclauses the setclause to be typed*)
val typings_of_setclause : Blast.clause list -> Blast.predicate list

(**check if the composant is in the project or not
@author Samuel Colin
@param comp the comp to test
@param ast the project*)
val correct_machine : Po.components -> Blast.amn -> bool

(**give the ID of the request
@author Samuel Colin
@param req the request*)
val get_id_from_request : Po.components -> Blast.id

(**give the name of the component
@author Samuel Colin
@param clause the clause*)
val name_of : Po.components -> string 

(**Rebuild the component from the project 
@author Samuel Colin
@param comp the component
@param ast the project*)
val get_component : Po.components -> Blast.amn -> Blast.predicate

(**Rebuild the component from the graph 
@author Samuel Colin
@param comp the component
@param project the project
@param the machine where comp should be*)
val get_component_from_graph :
  Po.components -> Project.t -> Blast.id -> Blast.predicate

(**List the component that are in the hypothesis
@author Samuel Colin
@param hypos hypothesis that should be listed
@param project the current project
@param mchid the current machine
t*)
val mkPoPredicate :
  Po.components list ->
  Project.t -> Blast.id -> Blast.predicate option

(**Same program as above, but different name for the ease of reading. It work on hypothesis that has same type but different role.
@author Samuel Colin
@param hypos hypothesis that should be listed
@param project the current project
@param mchid the current machine*)
val mkHypothesis :
  Po.components list ->
  Project.t -> Blast.id -> Blast.predicate option

(**Merge the two given hypotheses into one
@author Samuel Colin
@param comp1 the first hypothesis
@param comp2 the second hypothesis
t*)
val concat_po_hypos :
  Blast.predicate option -> Blast.predicate option -> Blast.predicate option
