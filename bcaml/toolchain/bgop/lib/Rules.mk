
FILES = po.mli \
 po.ml \
 po_acc.mli \
 po_acc.ml \
 gop_dc.mli \
 gop_dc.ml \
 gop_lib.mli \
 gop_lib.ml

DEPS = Xml Bcaml Bparser Gsl

NAME = Bgop
MAKELIB = yes
