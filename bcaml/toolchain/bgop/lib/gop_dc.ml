(* $Id$ *)
(*Entre autre, regroupe les fonctions de passage vers le XML et de fonctions utilitaires*)

(*i*)

open Blast
open Po

(*Fonction de passage vers du XML*)
let xml isProofTarget po=
  let all=
    match po.hypos with
      | None -> po.goal 
      | Some(preds) -> PredBin(
	  Bbop.Implies,
	  PredParen(preds),
	  po.goal)
  in
  let formulaTag=Bxml.pred_top all in
    if isProofTarget 
    then
      let freeVars=Variables.free_vars_pred all in
      let freeVarsList=Id_handling.Idset.elements freeVars in
      let freeVarsTag=Bxml.ids freeVarsList in
	Bxml.balise "ProofObligation" [] [freeVarsTag; formulaTag]
    else
      Bxml.balise "ProofObligation" [] [formulaTag]
      
(*Fonction de passage vers du XML*)
let po_doc r =
  let d = Xml.XMLDecl("1.0",Some(true),Some("ISO-8859-1")) in
  let dtd = Some(Xml.DTD("AMN",Some(Xml.DTDsys("AMN.dtd")))) in
  let p = Xml.Prolog(Some(d),dtd,[]) in 
     Xml.XML(p,r,[]) 


(* SC : tout ce qui suit sera � enlever/modifier. On sortira du xml dans *)
(* stdout, hmm? *)
(*Fonction de passage vers du XML*)
module OrderedString=
  struct
    type t=string
    let compare=compare
  end

module StringMap=Map.Make(OrderedString)



let unique_numbering= ref StringMap.empty

(*Numerote avec un numero non attribu� le fichier en argument et met dans unique_numbering le numero actuel de fichier*)
let get_number filename=
  try
    let numb=StringMap.find filename !unique_numbering in
    unique_numbering := StringMap.add filename (numb+1) !unique_numbering;
    (numb+1)
  with 
    Not_found ->
      unique_numbering := StringMap.add filename 1 !unique_numbering;
      1

(*exporte les PO en XML*)
let a_po destdir isProofTarget po = 
  let mchdir = destdir ^ "/" ^ (Bfile.root_name po.filename) in 
    Bfile.mkpath mchdir ;
    let ponumber=Printf.sprintf "%05i" (get_number po.filename) in
      
    let po_file_path = mchdir ^"/"^ po.trace ^ "__" ^ ponumber ^ ".xml" in
    let cout = open_out po_file_path in
    let linelen = 80
    in
      try
	let xmled_po=xml isProofTarget po in
	let full_podoc=Ppxml.ppDoc (po_doc xmled_po) in
	  Pp.ppToFile cout linelen full_podoc;
	  close_out cout;
      with
	  some_exception -> close_out cout; raise some_exception
		  



      
