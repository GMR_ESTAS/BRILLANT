
open Project
open Po

  
let mk_Idvar =
  Blast_mk.mk_ExprId ( (Blast_mk.mk_Id "t"))
 
let mk_Seq s =
  Blast.ExprUn ( Bbop.Seq , s )
    
let mk_In var expr =
  Blast.ExprPred(Blast.PredAtom (Bbop.In, var , expr))


let mk_PredIn var expr =
  Blast.PredAtom (Bbop.In, var , expr)
    
let mkTrue = 
  Blast.ExprBoolConstant(Blast.TrueConstant) 
    
    
let mkPredBin op x y = Blast.PredBin (op , x, y)
    
let mk_PredAnd x y =  mkPredBin Bbop.And x  y

let mk_PredImplies x y =  mkPredBin Bbop.Implies x  y

    

(* fonction qui renvoie deux predicats associ�s par l'op�rateur op *) 
let mk_PredBin (op,pred1,pred2) =
  Blast.PredBin(op,pred1,pred2)
    


(* fonction qui transforme un pr�dicat en expression *)
let mk_pred_to_expr pred =
  Blast.ExprPred(pred)
    

(* fonction affiche une EDS *)
(* A enrichir avec la sauvegarde fichier *)          
let expr_to_string expr = "A revoir ... MG"

(*MG
  let module Tg = Text_gen.TextGen(MacrosTXT) in
  let module Pg = Pretty_gen.PrettyGen(Tg) in
  let module Bg = Blast_gen.BlastGen(Pg) in
  let pretty_print = Tg.dump_box (Bg.bg_expr expr) in
  pretty_print
*)
  
 
(*module OrderedString=
  struct
    type t=string
    let compare=compare
  end

module StringMap=Map.Make(OrderedString)

let unique_numbering= ref StringMap.empty

let get_number filename=
  try
    let numb=StringMap.find filename !unique_numbering in
    unique_numbering := StringMap.add filename (numb+1) !unique_numbering;
    (numb+1)
  with 
    Not_found ->
      unique_numbering := StringMap.add filename 1 !unique_numbering;
      1
 
let basic_path () = 
  try
    Sys.getenv "BASICS_PATH"      
  with
      _ ->  Error.env_variable "BASICS_PATH"



let basic_name file_path =
  (basic_path () ^"/"^(Bfile.base_name file_path))
  

let retrieve_ast_from filename=
  (*  Error.debug ("Retrieving"^filename) ;*)
  try  
    let cin = 
      if Bfile.isBfile filename
      then open_in filename
      else 
	let basic_file = basic_name filename in	  
	  (*	  Error.debug ("trying to open "^basic_file); *)
	  if Bfile.isBfile basic_file
	  then open_in basic_file
	  else		    (* File not found ...*)
	    raise (Invalid_argument ("File not found : "^filename))     
    in 
      (* Parsing of the found file *)
      begin
	try
	  Error.debug ("Parsing of "^ (Bfile.base_name filename)) ;
	  let lexbuf = Lexing.from_channel cin in
	  let res = Bparser.amn Blexer.token lexbuf in
	    begin
	      close_in cin;res
	    end
	with 
	    Blast.Parse_error(nb,errmsg) -> 
	      close_in cin;
	      raise (Invalid_argument ("retrieve_ast_from : parse error :"^errmsg))
	  |	_ -> close_in cin;
		  raise Parsing.Parse_error
      end
  with 
    | Sys_error s ->  
	raise (Sys_error s)
    | Parsing.Parse_error -> 
	raise (Invalid_argument "retrieve_ast_from : Parse error")

*)
