(**Library of gop
@author Samuel Colin*)

(**Debug function
@author Samuel Colin*)
val debug_po : Po.t -> string

(**Debug function
@author Samuel Colin*)
val debug_subst : Blast.substitution -> string

(**Debug function
@author Samuel Colin*)
val debug_pred : Blast.predicate -> string

(**Debug function
@author Samuel Colin*)
val no_po : string -> unit

(**Debug function
@author Samuel Colin*)
val po_cant_generate : string -> unit

(**Debug function
@author Samuel Colin*)
val not_implemented_yet : string -> unit

(**Debug function
@author Samuel Colin*)
val po_not_calculated : string -> unit

(** Take an AST and return a couple with the AST name and the AST
@author Samuel Colin
@param ast the AST*)
val mkassoc_id_mch : Blast.amn -> Blast.id * Blast.amn

(** Take an AST and return a couple with the AST name and the parameter list of the head machine of the AST
@author Samuel Colin
@param ast the AST*)
val amn_params_ids : Blast.amn -> Blast.id * Blast.id list

(** Take two ID and create the predicate that express the equality between the IDs
@author Samuel Colin
@param id1 the first ID
@param id2 the last ID*)
val mkequalids : Blast.id -> Blast.id -> Blast.predicate

(** Seek for given operation throught the project and return a sub-project with all the machines that need that operation
@author Samuel Colin
@param opid the operation to seek
@param mchid the head machine of the project
@param project the current project*)
val find_called_operation_machine :
  Blast.id -> Blast.id -> Project.t -> Project.t

(**  Replace all operation calls in subst by their bodies, so that proof obligations can be calculated. Due to the recursive nature of dependencies between machines through operation calls, this function is here instead of the module Instanciation in the lib directory
@author Samuel Colin
@param project the current project
@param mchid the head machine of the project
@param subst the substitution*)
val expand_substitution :
  Project.t -> Blast.id -> Blast.substitution -> Blast.substitution

(**  This function looks for substitution applications in the predicate, and expands operation calls in it. It is not mutually recursive with expand_substitution, because we make the hypothesis that predicates inside substitutions come from B machines, and thus don't contain substitution applications
@author Samuel Colin
@param project the current project
@param mchid the head machine of the project
@param pred the predicate*)
val expand_predicate :
  Project.t -> Blast.id -> Blast.predicate -> Blast.predicate

(** This function looks for substitution applications in the predicate, and expands operation calls in it. It is not mutually recursive with expand_substitution, because we make the hypothesis that predicates inside substitutions come from B machines, and thus don't contain substitution applications
@author Samuel Colin
@param project the current project
@param mchid the head machine of the project
@param po the Proof Obligation *)
val expand_po : Project.t -> Blast.id -> Po.t -> Po.t

(** This function take a PO, calculate it hypotheses and goal, then return the PO with the calculated hypotheses and but
@author Samuel Colin
@param po the Proof Obligation *)
val calculate : Po.t -> Po.t

(** this function was meant to determine if the goal of the PO was included in the hypothesis, but since the GOP does not more look for obvious PO it has never been coded, and the result is alway "false".
@author Samuel Colin
@param po the Proof Obligation *)
val goal_in_hyp : 'a -> bool

(** this function was meant to determine if the PO was obvious or not, but since the GOP does not more look for obvious PO it has never been coded, and the result is alway "false".
@author Samuel Colin
@param po the Proof Obligation *)
val is_obvious : Po.t -> bool

(** this function call the a_po function to save the PO and display message according to final result.
@author Samuel Colin
@param outdir the output directory
@param isProofTarget true if the proof target is free
@param po the Proof Obligation *)
val display_or_save : string -> bool -> Po.t -> unit

(** explode the hypotheses and goal of the PO, that is to say to break them into simple predicat, then use the result to make multiples simpler POs
@author Samuel Colin
@param po the Proof Obligation *)
val explode : Po.t -> Po.t list

(** give a list of all components seen by the machine, according to the B Book visibility rules.
@author Samuel Colin
@param project the current project
@param mchid the target machine *)
val seen_hypos : Project.t -> Blast.id -> Po.components list

(** give the parameter of the given machine 
@author Samuel Colin
@param project the current project
@param mchid the machine *)
val get_id_params : Project.t -> Blast.id -> Blast.head

(** get the initialization of mchid
@author Samuel Colin
@param project the current project
@param mchid the machine *)
val get_initialization : Project.t -> Blast.id -> Blast.substitution

(** get the initialization of importeds and includeds components of mchid
@author Samuel Colin
@param project the current project
@param modclause the clauses of modules
@param mchid the machine *)
val initializations_instanciations :
  Project.t ->
  Blast.clause -> Blast.id -> (Blast.id * Blast.substitution) list

(** get all vars that are in the machine
@author Samuel Colin
@param mchid the machine *)
val get_Variables : Blast.amn -> Blast.id list

(** get all vars that are used in the machine
@author Samuel Colin
@param project the current project
@param mchid the machine *)
val retrieve_used_variables : Project.t -> Blast.id -> Blast.id list

(** Removes all the predicates in predic that depend of one or several (concrete, abstracts) variables
@author Samuel Colin
@param idlist the list from where vars are taken
@param predic the predicate *)
val remove_variable_dependent_predicates :
  Id_handling.Idset.elt list -> Blast.predicate -> Blast.predicate option

(** The created list is a refinements list starting from the given mchid to the more abstract machine 
@author Samuel Colin
@param project the current project
@param mchid the machine *)
val create_refid_sequence : Project.t -> Blast.id -> Blast.id list

(** From an operation, this function create a triplet with his name, his precondition and the operation itself
@author Samuel Colin
@param opname the operation name
@param parout Parin is not used in this function, it is here only to match the return format of some functions.
@param parin Parin is not used in this function, it is here only to match the return format of some functions.
@param subst the operation's subtitution*)
val decompose_operation :
  'a * 'b * 'c * Blast.substitution ->
  'a * Blast.predicate option * Blast.substitution

(** Complete the trace of the PO
@author Samuel Colin
@param op_id the ID of the PO*)
val complete_po_with_trace : Blast.id -> string

(** Complete the hypothesis hypos with the predicate comp
@author Samuel Colin
@param comp the predicat to add
@param hypos the hypothesis*)
val add_to_po_preds :
  Blast.predicate -> Blast.predicate option -> Blast.predicate option

(** Complete the hypothesis hypos with the predicate comp
@author Tisserant Pierre
@param comp the predicat to add
@param hypos the goal*)
val add_to_po_goals :
  Blast.predicate -> Blast.predicate -> Blast.predicate

(** Add the precondition of an operation to the PO.
@author Samuel Colin
@param po the PO to be completed
@param n one of the 3 part of the triplet that represent the operation
@param q one of the 3 part of the triplet that represent the operation
@param v one of the 3 part of the triplet that represent the operation*)
val complete_po_with_operation :
  Po.t ->
  Blast.id * Blast.predicate option * Blast.substitution -> Po.t

(** Add the precondition of an operation to the PO.
@author Tisserant Pierre
@param po the PO to be completed
@param n one of the 3 part of the triplet that represent the operation
@param q one of the 3 part of the triplet that represent the operation
@param v one of the 3 part of the triplet that represent the operation*)
val complete_goal_with_operation :
  Po.t ->
  Blast.id * Blast.predicate option * Blast.substitution -> Po.t

(** Returns the refined asts in the reverse order (abstract machine is at the 
   last position)
@author Samuel Colin
@param project the current project
@param mchid the machine to be raffined*)
val retrieve_all_refined_machines :
  Project.t -> Blast.id -> Blast.amn list

(**  Given a sequence of machines and an operation id, operations_sequence traverses the sequence and takes in each machine the first occurrence of the operation corresponding to the specified opid. This allows to transparently handle implicit inheritance. the returned operations list starts by the most refined operation, and ends by the abstract form of the operation, provided the machines are in that order.
@author Samuel Colin
@param opid the operation to be searched throught the machines
@param machines the machines sequence, beginning with the most refined*)
val operations_sequence : Blast.id -> Blast.amn list -> Blast.operation list

(** Add to the PO all hypothesis needed by the operation in the most refined machine
@author Samuel Colin
@param basepo the PO to be completed
@param opid the operation that add hypothesis
@param project the current project
@param mchid the machine related to the PO*)
val complete_po_with_refined_operation :
  Po.t -> Blast.id -> Project.t -> Blast.id -> Po.t

(** Idem but take the hypothesis needed by the whole refinement sequence
@author Samuel Colin
@param basepo the PO to be completed
@param opid the operation to be
@param project the current project
@param mchid the machine related to the PO*)
val complete_po_with_implemented_operation :
  Po.t -> Blast.id -> Project.t -> Blast.id -> Po.t

(** Give the propertie of the most refined machine of the refid sequence that included a given machine
@author Samuel Colin
@param project the current project
@param mchid the machine related to the refid sequence*)
val retrieve_refined_properties :
  Project.t -> Blast.id -> Blast.predicate list

(** Idem but instead of propertie give the abstracts constants
@author Samuel Colin
@param project the current project
@param mchid the machine related to the refid sequence*)
val get_abstract_constants_from_refined :
  Project.t -> Blast.id -> Blast.id list
