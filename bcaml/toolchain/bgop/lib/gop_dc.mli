(**In this file are both tools programs and programs that convert to XML*)

(**Function that convert caml into XML
@author Samuel Colin
@param is_proof_target if the PO is really a proof target
@param po the PO to convert*)
val xml : bool -> Po.t -> Xml.element

(**Function that document the XML
@author Samuel Colin
@param d the XML to  document*)
val po_doc : Xml.element -> Xml.document

(**Module that work on XML
@author Samuel Colin*)
module OrderedString : sig type t = string val compare : 'a -> 'a -> int end
module StringMap :
  sig
    type key = OrderedString.t
    type 'a t = 'a Map.Make(OrderedString).t
    val empty : 'a t
    val is_empty : 'a t -> bool
    val add : key -> 'a -> 'a t -> 'a t
    val find : key -> 'a t -> 'a
    val remove : key -> 'a t -> 'a t
    val mem : key -> 'a t -> bool
    val iter : (key -> 'a -> unit) -> 'a t -> unit
    val map : ('a -> 'b) -> 'a t -> 'b t
    val mapi : (key -> 'a -> 'b) -> 'a t -> 'b t
    val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
    val compare : ('a -> 'a -> int) -> 'a t -> 'a t -> int
    val equal : ('a -> 'a -> bool) -> 'a t -> 'a t -> bool
  end

(**the number given to the last file
@author Samuel Colin*)
val unique_numbering : int StringMap.t ref

(**Give an unused number to the given file and increase the unique_numbering by one
@author Samuel Colin
@param filename the name of the file*)
val get_number : StringMap.key -> int

(**Export the given PO into XML files
@author Samuel Colin
@param destdir the path where PO should be written
@param isProofTarget if the PO is really a proof target
@param po the PO to be exported*)
val a_po : string -> bool -> Po.t -> unit
