(**Here are all type specific to the GOP
@author Samuel Colin*)

(**Exception returned by a PO-generating function that does not have create any PO
@author Samuel Colin*)
exception No_po

(**Type that represent a piece of hypothese
@author Samuel Colin*)
type components =
    Parameters of Blast.id
  | Sets of Blast.id
  | Constraints of Blast.id
  | Properties of Blast.id
  | Invariant of Blast.id
  | Assertions of Blast.id

(**Type that represent the complete hypotheses list of the PO
@author Samuel Colin*)
type hypos = components list

(**Type that represent a PO
@author Samuel Colin*)
type t = {
  hypos : Blast.predicate option;
  goal : Blast.predicate;
  mutable trace : string;
  filename : string;
}
