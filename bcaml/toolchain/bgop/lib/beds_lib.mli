(**This section is the library linked to the EDS generation.
@author Samuel Colin
@author Tisserant Pierre*)

(**Create an ID variable
@author Samuel Colin*)
val mk_Idvar : Blast.expr

(**Create an expression composed of an empty sequence and the first argument
@author Samuel Colin
@param s the second part of the expression*)
val mk_Seq : Blast.expr -> Blast.expr

(**Create the expr of an atomic predicate
@author Samuel Colin
@param var the variable of the atomic expression
@param expr the expression of the atomic expression*)
val mk_In : Blast.expr -> Blast.expr -> Blast.expr

(**Create an atomic predicate
@author Samuel Colin
@param var the variable of the atomic expression
@param expr the expression of the atomic expression*)
val mk_PredIn : Blast.expr -> Blast.expr -> Blast.predicate

(**Create a 'true' constant
@author Samuel Colin
@param var the variable of the atomic expression
@param expr the expression of the atomic expression*)
val mkTrue : Blast.expr

(**Create a 'true' constant
@author Samuel Colin
@param var the variable of the atomic expression
@param expr the expression of the atomic expression*)
val mkPredBin :
  Bbop.bop2 -> Blast.predicate -> Blast.predicate -> Blast.predicate

(**Create a 'and' binary predicat between the arguments
@author Samuel Colin
@param x the first predicate
@param y the second predicate*)
val mk_PredAnd : Blast.predicate -> Blast.predicate -> Blast.predicate

(**Create a 'implie' binary predicat between the arguments
@author Samuel Colin
@param x the first predicate
@param y the second predicate*)
val mk_PredImplies : Blast.predicate -> Blast.predicate -> Blast.predicate


(**Create a binary predicate
@param op the operation between the predicats
@param pred1 the first predicat
@param pred1 the second predicat
@author Samuel Colin*)
val mk_PredBin :
  Bbop.bop2 * Blast.predicate * Blast.predicate -> Blast.predicate

(**turn an predicate into an expression
@param pred the predicate to be converted
@author Samuel Colin*)
val mk_pred_to_expr : Blast.predicate -> Blast.expr

(**An unfinished fonction that were made in order to display an EDS ; may be deleted or finished in next version
@author Samuel Colin*)
val expr_to_string : 'a -> string

