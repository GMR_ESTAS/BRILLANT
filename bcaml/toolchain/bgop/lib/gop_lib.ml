(* $Id$ *)
  
(*Librairie des fonctions utilitaires utilis� par le gop*)
  
open Blast
open Po
open Expr_acc
open Pred_acc
open Bbop 
open Project

(*Fonctions de d�buggage qui n'ont pas grand chose � faire l�*)
let debug_po po=
  let xmled_po=Gop_dc.xml false po in
  let full_podoc=Ppxml.ppDoc (Gop_dc.po_doc xmled_po) in
    Pp.ppToString 80 full_podoc

let debug_subst subst=
  let xmled_subst=Bxml.substitution subst in
  let full_substdoc=Ppxml.ppDoc (Bxml.doc xmled_subst) in
    Pp.ppToString 80 full_substdoc

let debug_pred pred=
  let xmled_pred=Bxml.predicat pred in
  let full_preddoc=Ppxml.ppDoc (Bxml.doc xmled_pred) in
    Pp.ppToString 80 full_preddoc


let no_po description =
  Error.debug ("No PO:" ^ description)
    
let po_cant_generate description =
  Error.debug ("Can't generate PO:" ^ description)
    
    
let not_implemented_yet description =
  Error.debug ("GSL not implemented:" ^ description)

let po_not_calculated description =
  Error.debug ("PO Not calculated" ^ description)

let mkassoc_id_mch ast=
  (Blast_acc.get_name ast, ast)
    
let amn_params_ids ast=
  let mchid = Blast_acc.get_name ast in
  let mchhead = Blast_acc.get_head ast in
  (mchid,Blast_acc.instance_params_of mchhead)

(*Renvoie le pr�dicat disant l'�galit� des deux arguments*)
let mkequalids id1 id2=
  PredAtom(Equal, ExprId(id1), ExprId(id2))
    


(* FIXME: cette fonction aurait plut�t sa place dans project.ml... *)
(* Recherche le sous-projet o� est opid *)
let find_called_operation_machine opid mchid project =
  let mchdeps=Bgraph.filter_source project.graph mchid in
  let incdeps=
    List.filter (fun edge -> ((Bgraph.dependency edge) = Bgraph.Includes)) mchdeps in
  let impdeps=
    List.filter (fun edge -> ((Bgraph.dependency edge) = Bgraph.Imports)) mchdeps in
  let seedeps=
    List.filter (fun edge -> ((Bgraph.dependency edge) = Bgraph.Sees)) mchdeps in
  let allids = List.map Bgraph.target (incdeps@impdeps@seedeps) in
    Error.debug ("Looking for operation " ^ (Id_acc.debug_id opid) ^ " in : " ^ 
		 (List.fold_left (fun s i -> ((Id_acc.debug_id i) ^ "," ^ s) ) "" allids));
  let searchedmchs=List.map (Project.find project) allids in

  let is_in_machine operid idmachcont=
    try
      ignore 
	(Clause_acc.retrieve_operation 
	   operid 
	   (Clause_acc.get_Operations 
	      (Blast_acc.get_clause_list (snd idmachcont).machine ))
	);
      true
    with Not_found -> false
  in
  let foundimc=List.find (is_in_machine opid) searchedmchs in
    Project.subproject project (fst foundimc)



(* Replace all operation calls in subst by their bodies, so that proof
   obligations can be calculated. Due to the recursive nature of
   dependencies between machines through operation calls, this function
   is here instead of the module Instanciation in the lib directory *)
let rec expand_substitution project mchid subst=
  match subst with
    | Blast.SubstOperCall(_) ->	
	let vars=Subst_acc.varlist_from subst in
	let exprs=Subst_acc.allexprs_from subst in
	let opid=Subst_acc.opid_from subst in
	let (is_local, containingproj) =
	  try
	    (false, find_called_operation_machine opid mchid project)
	  with
	      Not_found -> (* Not out, hence maybe in local operations ? *)
		let own_imc = Project.find project mchid in
		let own_localops =
		  Clause_acc.get_LocalOperations
		    (Blast_acc.get_clause_list (snd own_imc).machine)
		in
		  if List.exists (fun (locopid,_,_,_) -> locopid = opid) own_localops
		  then (true,
			Project.subproject project mchid)
		  else raise Not_found
	in
	let containingimc=Project.get_root_machine containingproj in

	let get_relevantOps =
	  if is_local then Clause_acc.get_LocalOperations
	  else Clause_acc.get_Operations
	in
	let (opname, parout, parin, opbody)=
	  Clause_acc.retrieve_operation
	    opid
	    (get_relevantOps
	       (Blast_acc.get_clause_list ((snd containingimc).machine))
	    )
	in
	let calcbody=expand_substitution containingproj (fst containingimc) opbody in
(* fold_left2 is tail-recursive, just a little optimization *)
	let changeid_subst sub xi xj=Instanciation.id_subst_subst xi xj sub in
	let replaced_outs=List.fold_left2 changeid_subst calcbody parout vars 
	in
	let replaced_ins=
	  let prepare_inst=SubstInstanciation(parin, exprs, replaced_outs)
	  in
(* yes, all instanciations must be done "at the same time", so we use the
   little trick of defining a to-be-done instanciations. Note this trick has
   not been applied to outparams, as these are stricts id (so they are not
   present as expressions in the substitution) and appear in the left member
   of affectations, thus there is no "instanciation order" problem *)
	    Instanciation.apply_inner_instanciations prepare_inst
	in
(* all instanciations have been made? So we return the substitution
   as a block (to ease reading in case POs are generated without having been
   calculated) *)
	  SubstBlock(replaced_ins)
	    
    | Blast.SubstSetEqualIds(_)
    | Blast.SubstSetEqualFun(_)
    | Blast.SubstBecomeSuch(_)
    | Blast.SubstSetEqualRecords(_)
    | Blast.SubstSetIn(_)
    | Blast.SubstSkip -> subst

    | Blast.SubstBlock(_) ->
	SubstBlock(expand_substitution project mchid (Subst_acc.subst_from subst))
    | Blast.SubstPrecondition(_) ->
	let c=Subst_acc.cond_from subst in
	let s=Subst_acc.subst_from subst in
	  SubstPrecondition(c, expand_substitution project mchid s)
    | Blast.SubstAssertion(_) ->
	let c=Subst_acc.cond_from subst in
	let s=Subst_acc.subst_from subst in
	  SubstAssertion(c, expand_substitution project mchid s)
    | Blast.SubstChoice(_) ->
	let sl=Subst_acc.allsubsts_from subst in
	  SubstChoice(List.map (expand_substitution project mchid) sl)
    | Blast.SubstSequence(_) ->
	let (s1, s2)=Subst_acc.twosubsts_from subst in
	  SubstSequence(
	    expand_substitution project mchid s1,
	    expand_substitution project mchid s2)
    | Blast.SubstParallel(_) ->
	let (s1, s2)=Subst_acc.twosubsts_from subst in
	 SubstParallel(
	    expand_substitution project mchid s1,
	    expand_substitution project mchid s2)
    | Blast.SubstIf(_) ->
	let conds=Subst_acc.allconds_from subst in
	let thens=Subst_acc.allsubsts_from subst in
	let newelse=
	  match (Subst_acc.elsepart_from subst) with
	    | None -> None
	    | Some(s) -> Some( expand_substitution project mchid s)
	in
	  SubstIf(
	    List.combine
	      conds
	      (List.map (expand_substitution project mchid) thens),
	    newelse)
    | Blast.SubstSelect(_) ->
	let whens=Subst_acc.allconds_from subst in
	let thens=Subst_acc.allsubsts_from subst in
	let newelse=
	  match (Subst_acc.elsepart_from subst) with
	    | None -> None
	    | Some(s) -> Some( expand_substitution project mchid s)
	in
	  SubstSelect(
	    List.combine
	      whens
	      (List.map (expand_substitution project mchid) thens),
	    newelse)
    | Blast.SubstCase(_) ->
	let caseexpr=Subst_acc.expr_from subst in
	let eithers=Subst_acc.allexprLists_from subst in
	let newthens=
	  List.map (expand_substitution project mchid) (Subst_acc.allsubsts_from subst) in
	let newelse=
	  match (Subst_acc.elsepart_from subst) with
	    | None -> None
	    | Some(s) -> Some( expand_substitution project mchid s)
	in
	  SubstCase(
	    caseexpr,
	    List.combine eithers newthens,
	    newelse)
    | Blast.SubstAny(_) ->
	let idents=Subst_acc.allids_from subst in
	let cond=Subst_acc.cond_from subst in
	let s=Subst_acc.subst_from subst in
	  SubstAny(idents, cond, expand_substitution project mchid s)
    | Blast.SubstLet(_) ->
	let idents=Subst_acc.allids_from subst in
	let c=Subst_acc.cond_from subst in
	let s=Subst_acc.subst_from subst in
	  SubstLet(idents, c, expand_substitution project mchid s)

    | Blast.SubstVar(_) ->
	let idents=Subst_acc.allids_from subst in
	let s=Subst_acc.subst_from subst in
	  SubstVar(idents, expand_substitution project mchid s)
    | Blast.SubstWhile(_) ->
	let c=Subst_acc.cond_from subst in
	let s=Subst_acc.subst_from subst in
	let (i,v)=Subst_acc.whilepreds_from subst in
	  SubstWhile(c, expand_substitution project mchid s, i, v)
    | Blast.SubstInstanciation(_) -> 
	let s=Subst_acc.subst_from subst in
	let instids=Subst_acc.allids_from subst in
	let instexprs=Subst_acc.allexprs_from subst in
	 SubstInstanciation(
	   instids,
	   instexprs,
	   expand_substitution project mchid s)



(* This function looks for substitution applications in the predicate, and
   expands operation calls in it. It is not mutually recursive with
   expand_substitution, because we make the hypothesis that predicates inside
   substitutions come from B machines, and thus don't contain substitution
   applications *)
let rec expand_predicate project mchid pred=
  match pred with
    | Blast.PredParen(_) ->
	let p=Pred_acc.pred_from pred in
	let newp=expand_predicate project mchid p in
	  PredParen(newp)
    | Blast.PredNegation(_) ->
	let p=Pred_acc.pred_from pred in
	let newp=expand_predicate project mchid p in
	  PredNegation(newp)
    | Blast.PredExists(_) ->
	let vars=Pred_acc.vars_from pred in
	let p=Pred_acc.pred_from pred in
	let newp=expand_predicate project mchid p in
	  PredExists(vars, newp)
    | Blast.PredForAll(_) ->
	let vars=Pred_acc.vars_from pred in
	let p=Pred_acc.pred_from pred in
	let newp=expand_predicate project mchid p in
	  PredForAll(vars, newp)
    | Blast.PredBin(_) ->
	let c=Pred_acc.connector_from pred in
	let (p1,p2)=Pred_acc.twopreds_from pred in
	let newp1=expand_predicate project mchid p1 in
	let newp2=expand_predicate project mchid p2 in
	  PredBin(c, newp1, newp2)
    | Blast.PredAtom(_) -> pred
    | Blast.SubstApply(_) ->
	let s=Pred_acc.subst_from pred in
	let news=expand_substitution project mchid s in
	let p=Pred_acc.pred_from pred in
	let newp=expand_predicate project mchid p in
	  SubstApply(news, newp)


(* This function looks for substitution applications in the predicate, and
   expands operation calls in it. It is not mutually recursive with
   expand_substitution, because we make the hypothesis that predicates inside
   substitutions come from B machines, and thus don't contain substitution
   applications *)
let expand_po project mchid po =
  try
    let thepo =
      { hypos = 
	  begin
	    match po.hypos with
	      | None -> None
	      | Some(h) -> Some(expand_predicate project mchid h)
	  end;
	goal = expand_predicate project mchid po.goal;
	trace = po.trace;
	filename = po.filename;
      } 
    in
      thepo
  with
    | Invalid_argument e -> 
	Error.error ("expand_po:" ^ e)
    | err -> (Error.warning "expand_po:unexpected error"; raise err)

(* 
   Calcul de l'OP : application de la substitution (si elle existe) au but 
 *)


(* SC : une bonne id�e � creuser : faire l'expansion des appels d'op�rations.
Voir si il faut inclure mchstock pour �viter des parsings inutiles.
Ou alors remplacer lors de la g�n�ration des ops d'op�rations...
(moins bien). 
 *)
(* Fonction qui calcule les Gsl des hypoth�ses et but *)
let calculate po =
  try
    let thepo =
      { hypos = 
	  begin
	    match po.hypos with
	      | None -> None
	      | Some(h) -> Some(Gsl.calculate h)
	  end;
	goal = Gsl.calculate po.goal;
	trace = po.trace;
	filename = po.filename;
      } 
    in thepo
  with
      Error.Not_implemented msg -> 
	let newmsg="calculate:" ^ msg in
	  Error.not_implemented newmsg;
	  raise (Error.Not_implemented newmsg)
    | Invalid_argument e -> 
	Error.error ("calculate:" ^ e)
    | err -> (Error.warning "calculate:unexpected error"; raise err)


let goal_in_hyp po = 
  false

    
(* This function is intended to detect if a proof obligation is 'obvious' or
   not.  For example, a proof is sayed to be 'obvious' if the goal is in the
   hypothesis list.  For now, every proof obligation is not obvious...  *)
(*Fonction destin� � permettre d'�liminer les PO �videntes ; cette fonction ne fait rien car la politique actuelle est de laisser cette tache au prouveur*)
let is_obvious po =
  match po.hypos with
    None -> false
  | _ ->   goal_in_hyp po
	


let display_or_save outdir isProofTarget po =
  Error.debug ("PO calculated : " ^ po.trace);
  Error.debug ("Saving in : " ^ outdir);
  try
    Gop_dc.a_po outdir isProofTarget po;
    Error.debug ("PO saved in : " ^ outdir)
  with
      err -> (Error.warning "display_or_save:couldn't save PO"; raise err)


let explode po=  
  let rec explode_pred blast_pred=
    match blast_pred with
    | Blast.PredBin(And,p1,p2) ->
	(explode_pred p1)@(explode_pred p2)			  
    | Blast.PredBin(Implies, p1, p2) ->
	List.map
	(fun p -> PredBin(Implies, p1, p))
	(explode_pred p2)
    | Blast.PredForAll(ids, p1) ->
	List.map
	(fun p -> PredForAll(ids, p))
	(explode_pred p1)
    | Blast.PredParen(p1) ->
	List.map
	(fun p -> PredParen(p))
	(explode_pred p1)
    | _ -> [blast_pred]
  in

  let replace_goal proofob g=
    { hypos = proofob.hypos;
      goal = g;
      trace = proofob.trace;
      filename = proofob.filename
    } 
  in
    Error.debug ("Exploding PO : " ^ po.trace);
    List.map (replace_goal po) (explode_pred po.goal)

(*Donne les hypoth�ses vu par la machine selon les r�gles de visibilit�es du BBook*)
let seen_hypos project mchid=
  let (_, machcont)=Project.find project mchid in
  let seen_ids=Blast_acc.get_seen_mchids machcont.machine in
  let seenreq=fun id -> [Sets(id); Properties(id); Invariant(id)] in
    List.flatten (List.map seenreq seen_ids)
   
(*Donne les param�tres de la machine en entr�e*)
let get_id_params project mchid=
  let (_, machcont)=Project.find project mchid in
    Blast_acc.get_head machcont.machine

(*obtient l'initialisation de mchid � partir de la liste des clauses de cette m�me machine*)
let get_initialization project mchid=
  let (_, machcont)=Project.find project mchid in
    try
      Clause_acc.get_Initialisation (Blast_acc.get_clause_list machcont.machine)
    with Clause_acc.No_such_clause -> 
      invalid_arg "get_initialization"
 
(*Cette fonction renvoie l'initialisation d� aux importations et aux inclusions *)
let initializations_instanciations project modclause mchid=
  let getmodclause=
    match modclause with
      | Blast.Includes(_) -> Blast_acc.get_included_mchinsts
      | Blast.Imports(_) -> Blast_acc.get_imported_mchinsts
      | _ -> invalid_arg "initialisations_instanciations"
  in
  let (_, machcont)=Project.find project mchid in
  let clause_insts=getmodclause machcont.machine in
  let incortedIds=
    let graphLink=
      match modclause with
	| Blast.Includes(_) -> Bgraph.Includes 
	| Blast.Imports(_) -> Bgraph.Imports 
	| _ -> invalid_arg "initialisations_instanciations"
    in
    let incortedEdges=Bgraph.filter_sourceDep project.graph graphLink mchid in
      List.map Bgraph.target incortedEdges
  in
  let incorted_params=List.map (get_id_params project) incortedIds in
  let idlist=fst (List.split clause_insts) in
  let get_initialization idmachcont=
    try
      Clause_acc.get_Initialisation (Blast_acc.get_clause_list (snd idmachcont).machine)
    with Clause_acc.No_such_clause -> 
      invalid_arg "initialisations_instanciations"
  in
    try
      List.map 
	(fun id -> (id, 
		    SubstInstanciation(
		      List.assoc id incorted_params,
		      List.assoc id clause_insts,
		      get_initialization (Project.find project id))
		   )
	)
	idlist
    with Not_found -> invalid_arg "initialisations_instanciations"

		
(*Recup�re toute les variables de la machine*)
let get_Variables mch=
  List.append 
    (Clause_acc.get_VariablesConcrete (Blast_acc.get_clause_list mch))
    ( List.append 
	(Clause_acc.get_VariablesAbstract (Blast_acc.get_clause_list mch))
	(Clause_acc.get_VariablesHidden (Blast_acc.get_clause_list mch)))
    
(*Recup�re toute les variables de la machine*)
let retrieve_used_variables project mchid=

  let get_used_machine id=
    let imc=Project.find project id in
      (snd imc).machine
  in

  let useddeps=Bgraph.filter_sourceDep project.graph Bgraph.Uses mchid in
  let usedids=List.map Bgraph.target useddeps in
  let allvars=List.map 
		get_Variables 
		(List.map get_used_machine usedids) 
  in
    List.flatten allvars


(* Removes all the predicates in predic that depend of one or several
(concrete, abstracts) variables *)

let remove_variable_dependent_predicates idlist predic=
  
  let concat_preds pred1 pred2=
    match (pred1,pred2) with
      | (None, None) 
	-> None
      | (None, Some(_) ) 
	-> pred2
      | (Some(_), None) 
	-> pred1
      | (Some(p1), Some(p2) ) 
	-> Some (Blast_mk.mk_Conjunction [p1;p2] )
  in
    
  let rec rm_vdp pbidset pred=
    match pred with 
      | Blast.PredBin(Bbop.And,p1,p2) -> 
	  concat_preds (rm_vdp pbidset p1) (rm_vdp pbidset p2)
      | _ -> 
	  let pbvars=Variables.free_vars_pred pred in
	    if (Id_handling.Idset.inter pbidset pbvars)=Id_handling.Idset.empty
	    then Some(pred)
	    else None
  in
    
    rm_vdp (Id_handling.set_from_ids idlist) predic



(* The created list is a refinements list starting from the given
   mchid to the more abstract machine *)
let create_refid_sequence project mchid=

  let rec aux_create_refidseq graph id=
    let refinededges=Bgraph.filter_targetDep graph Bgraph.RefinedBy id in
    let refinedids=List.map Bgraph.source refinededges in
      match refinedids with
	| [] -> [id]
	| [refid] -> id::(aux_create_refidseq graph refid)
	| _ -> invalid_arg "create_refid_sequence : one refinement for multiple machines"
  in
    
  let result=aux_create_refidseq project.graph mchid in
    result

(*Prend l'op�ration et en fait un triplet fait de son nom, ses pr�conditions, et de la substitution en elle m�me*)
let decompose_operation (opname, parout, parin, subst)=
  match subst with
    | Blast.SubstPrecondition(_)
      -> ( 
	opname,
	Some(Subst_acc.cond_from subst),
	(Subst_acc.subst_from subst)
      )
    |  _ 
      -> (
	opname,
	None, 
	subst
      )

(*Complete la trace d'une PO.*)
let complete_po_with_trace op_id=
  match op_id with 
      Blast.Id(_) -> ("Op__"^(Id_acc.name_of op_id))
    | _ -> "Operations"

(*Ajoute un pr�dicat aux hypotheses d'une PO*)	
let add_to_po_preds comp hypos=
  match hypos with
    | None -> Some(comp)
    | Some(oldhypos) ->
	Some(PredBin(And, oldhypos, comp))

(*Ajoute un pr�dicat aux buts d'une PO*)	
let add_to_po_goals comp hypos= PredBin(And, hypos, comp)

(*Ajoute les pr�conditions d'une op�ration aux hypotheses d'une PO*)	
let complete_po_with_operation po (n,q,v)=
  let newhypos=
    match q with
      | None 
	-> po.hypos
      | Some(comp) 
	-> add_to_po_preds comp po.hypos  
  in
  let newgoal= SubstApply(v, po.goal)  in
    { 
      hypos=newhypos;
      goal=newgoal;
      trace= complete_po_with_trace n;
      filename=po.filename
    } 

(*Ajoute les pr�conditions d'une op�ration aux buts d'une PO*)	
let complete_goal_with_operation po (n,q,v)=
  let newgoal=
    match q with
      | None 
	-> po.goal
      | Some(comp) 
	-> add_to_po_goals comp po.goal  
  in
    { 
      hypos=po.hypos;
      goal=newgoal;
      trace= complete_po_with_trace n;
      filename=po.filename
    } 

(* Returns the refined asts in the reverse order (abstract machine is at the 
   last position) *)
let retrieve_all_refined_machines project mchid=
  let refidseq=create_refid_sequence project mchid in
  let rev_mchlist=
    List.map (fun id -> (snd (Project.find project id)).machine) refidseq
  in
    rev_mchlist



(* Given a sequence of machines and an operation id, operations_list traverses the sequence and takes in each machine the first occurrence of the operation corresponding to the specified opid. This allows to transparently handle implicit inheritance. the returned operations list starts by the most refined operation, and ends by the abstract form of the operation, provided the machines are in that order. *)
let rec operations_sequence opid machines=
  match machines with
    | [] -> []
    | mch::othermchs -> 
	try let theop=Clause_acc.retrieve_operation opid 
			(Clause_acc.get_Operations 
			   (Blast_acc.get_clause_list mch))  
	in
	  theop::(operations_sequence opid othermchs)
	with 
	    Not_found -> (operations_sequence opid othermchs)



let complete_po_with_refined_operation basepo opid project mchid=
  Error.debug ("complete_po_with_refined_operation : " ^ (Id_acc.name_of opid));
  let mchsequence=retrieve_all_refined_machines project mchid in
(* The list of successively refined operations are in reverse order (abstract
   operation at last position *)
  let revoperlist=operations_sequence opid mchsequence in

  let oper1=List.hd (List.rev revoperlist) in
  let operi=List.hd (List.tl revoperlist) in
(* Note that oper1 and operi may happen to be the same operation *)
  let opern=List.hd revoperlist in

  let outparams= Clause_acc.get_operation_outparams opern 
  in

  (* The renaming set is empty because the new identifiers will never be 
     modified variables (see operations PO definition for refinement and 
     implementation *)

  let renaming_outparams=Id_handling.rename_ids outparams Id_handling.Idset.empty in
  let equalitylist=
    List.map
      ( fun (x,y) -> mkequalids x y)
      renaming_outparams
  in
  let (_, q1, _) = decompose_operation oper1 in
  let (_, _, vi) = decompose_operation operi in
  let (_ , qn, vn) = decompose_operation opern in
  let newgoal=
    if equalitylist=[]
    then basepo.goal
    else 
      PredBin(And, basepo.goal, Blast_mk.mk_Conjunction equalitylist)
  in  
  let (oldids,newids)=List.split renaming_outparams in
  let renamevn=SubstInstanciation(
		 oldids,
		 List.map (fun i -> ExprId(i)) newids,
		 vn)
  in
  let notgoal = PredNegation(newgoal) in
  let notrefinedgoal=
    PredNegation (SubstApply(vi, notgoal))
  in
  let fullsubstgoal = SubstApply(renamevn, notrefinedgoal) in
  let fullgoal=
    match qn with
      |	None -> fullsubstgoal
      |	Some(precond) -> PredBin(And, precond, fullsubstgoal)
  in
  let newhypos= Po_acc.concat_po_hypos basepo.hypos q1 in
    { 
      hypos = newhypos;
      goal = fullgoal;
      trace=basepo.trace;
      filename=basepo.filename;
    } 
    
(*On complete la PO en entr�e avec les op�rations impl�ment�e, selon les r�gles du BBook et de l'atelier B*)
let complete_po_with_implemented_operation basepo opid project mchid=
  Error.debug ("complete_po_with_implemented_operation : " ^ (Id_acc.debug_id opid));
  let mchsequence=retrieve_all_refined_machines project mchid in
(* The list of successively refined operations are in reverse order (abstract
operation at last position *)
  let revoperlist=operations_sequence opid mchsequence in

(* The renaming set is empty because the new identifiers will never be 
   modified variables (see operations PO definition for refinement and 
   implementation *)

  let outparams=Clause_acc.get_operation_outparams (List.hd revoperlist) in
  let renaming_outparams=Id_handling.rename_ids outparams Id_handling.Idset.empty in
  let equalitylist=
    List.map
      ( fun (x,y) -> mkequalids x y)
      renaming_outparams
  in

  let revdecomplist=List.map decompose_operation revoperlist in
  let (_, qn, vn) = List.hd revdecomplist in
  let (_, _, vi) = List.hd (List.tl revdecomplist) in

  let prevprecons=List.map 
		    (fun (_,q,_) -> q)
		    (List.rev (List.tl revdecomplist))
  in
  let newhypos=List.fold_left 
		 Po_acc.concat_po_hypos
		 basepo.hypos
		 prevprecons 
  in

  let newgoal=
    if equalitylist=[]
    then basepo.goal
    else 
      PredBin(And, basepo.goal, (Blast_mk.mk_Conjunction equalitylist))
  in  
  let (oldids,newids)=List.split renaming_outparams in
  let renamevn = SubstInstanciation(
                   oldids,
		   List.map (fun i -> ExprId(i)) newids,
		   vn)
  in
  let notgoal= PredNegation(newgoal) in
  let notrefinedgoal=
    PredNegation(SubstApply(vi, notgoal))
  in
  let fullsubstgoal = SubstApply(renamevn, notrefinedgoal) in
  let fullgoal=
    match qn with
      |	None -> fullsubstgoal
      |	Some(precond) -> PredBin(And, precond, fullsubstgoal)
  in
    { hypos = newhypos;
      goal = fullgoal;
      trace=basepo.trace;
      filename=basepo.filename;
    } 
    
(* Properties of refined machines*)

let retrieve_refined_properties project mchid=
  let machines=retrieve_all_refined_machines project mchid in
  let rec rrp machines=
    match machines with
      | [] -> []
      | mch::others -> 
	  try
	    let props=Clause_acc.get_Properties (Blast_acc.get_clause_list mch) in
	      props::(rrp others)
	  with Clause_acc.No_such_clause -> rrp others
  in
    rrp machines
      


(* You must pass an implementation to this function *)
let get_abstract_constants_from_refined project mchid=
      
  let rec gac_fr machines=
    match machines with
      | [] -> []
      | mch::others ->
	try
	  let abstconsts=Blast_acc.get_abstract_constants mch in
	    abstconsts@(gac_fr others)
	with Blast_acc.Empty_tree -> (gac_fr others)
  in
    
  let machines=retrieve_all_refined_machines project mchid in
    match machines with
      | [] -> invalid_arg "get_abstract_constants_from_refined"
      | [alone] -> invalid_arg "get_abstract_constants_from_refined"
      | _ -> gac_fr machines

