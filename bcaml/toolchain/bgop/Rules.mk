SUBDIRS = lib

FILES= gop_mch.mli \
 gop_mch.ml \
 gop_ref.mli \
 gop_ref.ml \
 gop_imp.mli \
 gop_imp.ml \
 gop.mli \
 gop.ml \
 gop_main.mli \
 gop_main.ml

EXTRADEPS = unix str
DEPS = Xml Bcaml Bparser Gsl Bgop

BIN = bgop
