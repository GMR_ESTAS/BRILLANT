
(** Proof Obligation generation for REFINEMENT components
@author Samuel Colin*)

(**Create the PO required by parameter refinement, as in B Book [4.1]

@param project the project corresponding to the machine
@param mchid the machine that is to be parsed*)
val po_params : Project.t -> Blast.id -> Po.t list

(**Create the PO required by assertions refinement, as in B Book [4.2]

@param project the project corresponding to the machine
@param mchid the machine that is to be parsed*)
val po_assertions : Project.t -> Blast.id -> Po.t list

(**Create the PO required by initialization refinement, as in B Book [4.3]

@param project the project corresponding to the machine
@param mchid the machine that is to be parsed*)
val po_initialization : Project.t -> Blast.id -> Po.t list

(**Create the PO required by operation refinement, as in B Book [4.3]

@param project the project corresponding to the machine
@param mchid the machine that is to be parsed*)
val po_operations : Project.t -> Blast.id -> Po.t list
