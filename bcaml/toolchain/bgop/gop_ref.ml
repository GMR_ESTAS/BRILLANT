(* $Id$ *)  

(*Cette section comprend toute les fonctions de g�n�rations des POs li�es aux raffinements
@author Samuel Colin*)

(*i*)
open Blast
open Po
open Gop_lib
open Project

(*i*)

(*Cette fonction a pour but de determiner si c'est un candidat � la cr�ation de POs de raffinement li�es aux param�tres*)
let candidate_refines_params_po ast=
  (Blast_acc.is_refinement ast)
  && ((Clause_acc.get_Includes (Blast_acc.get_clause_list ast)) <> [])

(*Cette fonction a pour but de determiner si c'est un candidat � la cr�ation de POs de raffinement*)
let candidate_refines_po ast=
  (Blast_acc.is_refinement ast)
        
(* \section{Included machines constraints}  \gopdoc{gop-ref-params} 
*)

(*Cr�e la PO requise pour le raffinement de param�tre, comme marqu� sur le BBook.*)
let po_params project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_refines_params_po machine) then raise No_po;


    let refid_sequence=Gop_lib.create_refid_sequence project mchid in
    let reverse_refids=List.rev refid_sequence in
    let firstmchid=List.hd reverse_refids in

    let hypACreq=(fun id -> [Parameters(id); Constraints(id)]) in
    let seqreq=(fun id -> [Sets(id); Properties(id)]) in
    let wholeseqreq=List.flatten (List.map seqreq reverse_refids) in

    let seenhyps=Gop_lib.seen_hypos project mchid in
    let corehyps= (hypACreq firstmchid)@wholeseqreq@seenhyps in

    let increq=hypACreq in
    let incedges=Bgraph.filter_sourceDep project.graph Bgraph.Includes mchid in
    let includedreqs=List.flatten (List.map increq (List.map Bgraph.target incedges)) in

    let incgoals=Po_acc.mkPoPredicate includedreqs project mchid in
(* As all POs have the same hypotheses, let's gather all that *)
    let finalgoal=
      match incgoals with
	| None -> raise No_po
	| Some(pred) -> pred 
    in
    let basepo=
      { hypos = Po_acc.mkHypothesis corehyps project mchid ;
	goal = finalgoal;
	filename = (Id_acc.name_of mchid) ^ ".ref";
	trace= "Refines_params"; 
      } 
    in [basepo]


(* \section{Assertions} *)
(* \gopdoc{gop-ref-assertions} *)
(*Cr�e la PO requise pour le raffinement d'assertion, comme marqu� sur le BBook.*)
let po_assertions project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_refines_po machine) then raise No_po;
    
    let finalgoal=
      try Po_acc.get_component (Assertions(mchid)) machine 
      with Not_found -> raise No_po
    in
    let refid_sequence=Gop_lib.create_refid_sequence project mchid in
    let reverse_refids=List.rev refid_sequence in
    let firstmchid=List.hd reverse_refids in
    let prev_refids=List.rev (List.tl refid_sequence) in

    let hypACreq=(fun id -> [Parameters(id); Constraints(id)]) in
    let seqreq=(fun id -> [Sets(id); Properties(id)]) in
    let invreq=(fun id -> Invariant(id)) in
    let assreq=(fun id -> Assertions(id)) in

    let acfirstreq=(hypACreq firstmchid) in
    let wholeseqreq=List.flatten (List.map seqreq reverse_refids) in
    let allinvreq=List.map invreq reverse_refids in
    let allprevassreq=List.map assreq prev_refids in
    let increq=
      (fun id -> 
	 [Sets(id); Properties(id); Invariant(id); Assertions(id)]) 
    in
    let incedges=Bgraph.filter_sourceDep project.graph Bgraph.Includes mchid in
    let includedreqs=List.flatten (List.map increq (List.map Bgraph.target incedges)) in

    let seenhyps=Gop_lib.seen_hypos project mchid in

    let hyprequest=
      (acfirstreq @ wholeseqreq @ allinvreq @ allprevassreq) @ 
      (includedreqs @ seenhyps)
    in

    let basepo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid ;
	goal = finalgoal;
	filename = (Id_acc.name_of mchid) ^ ".ref";
	trace= "Refines_assertions"; 
      } 
    in [basepo]


(* \section{Initialisation} 
\gopdoc{gop-ref-initialisation}
*)

let po_initialization project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_refines_po machine) then raise No_po;

    let invgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
 
    let refid_sequence=Gop_lib.create_refid_sequence project mchid in
    let reverse_refids=List.rev refid_sequence in

    let hypACreq=(fun id -> [Parameters(id); Constraints(id)]) in
    let seqreq=(fun id -> [Sets(id); Properties(id)]) in

    let seenhyps=Gop_lib.seen_hypos project mchid in
    let wholeseqreq=List.flatten (List.map seqreq reverse_refids) in
    let hyprequest=(hypACreq mchid)@wholeseqreq@seenhyps in


    let not_invgoal = PredNegation(invgoal) in
    let previnit_notinv=
      try
	let beforelastid=List.hd (List.tl refid_sequence) in
	let prevmch=(snd (Project.find project beforelastid)).machine in
	let prevmchinit=
	  Clause_acc.get_Initialisation (Blast_acc.get_clause_list prevmch)
	in
	  SubstApply(prevmchinit, not_invgoal)
      with
	  Clause_acc.No_such_clause -> not_invgoal
    in

    let npini_goal = PredNegation(previnit_notinv) in

(* SC: il peut ne pas y avoir d'init... *)
    let mchinit=Gop_lib.get_initialization project mchid in

    let increq=fun id -> [Sets(id); Properties(id)] in
    let allinstanciations=
      Gop_lib.initializations_instanciations project (Blast.Includes([])) mchid
    in

    let build_po id=
      { hypos = Po_acc.mkHypothesis (hyprequest@(increq id)) project mchid ;
	goal = SubstApply(
		 SubstSequence(
		    List.assoc id allinstanciations,
		    mchinit),
		 npini_goal);
	filename = (Id_acc.name_of mchid) ^ ".ref";
	trace="Refines_invariant";
      } 
    in
    let allpos=List.map build_po (fst (List.split allinstanciations)) in
      if allpos=[] then (* there are no included machines *)
	[{ hypos = Po_acc.mkHypothesis hyprequest project mchid ;
	  goal = SubstApply(mchinit, npini_goal) ;
	  filename = (Id_acc.name_of mchid) ^ ".ref";
	  trace="Refines_invariant";
	}] 
      else allpos


(* \section{Operations} *)

(*Cr�e la PO requise pour le raffinement d'operation, comme marqu� sur le BBook.*)
let po_operations project mchid=
  let machine=(snd (Project.find project mchid)).machine in
    if not (candidate_refines_po machine) then raise No_po;
 
    let invgoal=
      try Po_acc.get_component (Invariant(mchid)) machine
      with Not_found -> Blast_mk.mk_PredTrue ()
    in 
    let refid_sequence=Gop_lib.create_refid_sequence project mchid in
    let reverse_refids=List.rev refid_sequence in
    let firstmchid=List.hd reverse_refids in

    let hypACreq=(fun id -> [Parameters(id); Constraints(id)]) in
    let allmchreq=
      fun id -> [Sets(id); Properties(id); Invariant(id); Assertions(id)] 
    in
    let seenhyps=Gop_lib.seen_hypos project mchid in
    let increq=
      fun id -> [Sets(id); Properties(id); Invariant(id); Assertions(id)] 
    in
    let incedges=Bgraph.filter_sourceDep project.graph Bgraph.Includes mchid in
    let includedreqs=List.flatten (List.map increq (List.map Bgraph.target incedges)) in

    let hyprequest=(hypACreq firstmchid) @ 
		   (List.flatten (List.map allmchreq reverse_refids)) @ 
		   seenhyps @ includedreqs
    in
    
    let basepo=
      { hypos = Po_acc.mkHypothesis hyprequest project mchid ;
(*Pourquoi on ne r�utilise pas la variable base_hypos, juste au dessus ?*)
	goal = invgoal;
	filename=(Id_acc.name_of mchid) ^ ".ref";
	trace="Refines_operations";
      } 
    in
    let all_opids=List.map 
		    Clause_acc.get_operation_id
		    (Clause_acc.get_Operations 
		       (Blast_acc.get_clause_list machine)) 
    in
    let all_mypos=List.map
		    (fun id -> Gop_lib.complete_po_with_refined_operation basepo id project mchid)
		    all_opids
    in
      if all_mypos=[]
      then raise No_po
      else all_mypos

