(* $Id$ *)

(*
  Petite librairie de fonctions utilitaires pour la manipulations
  de fichiers

  Dorian Petit
*)

(* Bfile is too low-level to stay in lib, so I've put it in lparser. The other
   reason is that in the long term the other tools won't use it, instead
   they'll parse xml *)


let path = "../BEST"

let path_tree = path^"/Tree"

let path_error = path^"/Error"

let path_crypt = path^"/Crypt"

let copyright =
	Error.debug ("-- Copyright INRETS-ESTAS 2000,2001"
		     ^" -- Contact mailto:georges.mariano@inrets.fr\n") 

let info () = "bcaml project (version 0.31)" 

type amn_type = MCH | REF | IMP 

let kind filename = ()

let xml_extension = "xml"


let isBfile file = 
  try
    let _ = Unix.stat file 
    in	
      ( Filename.check_suffix file ".mch" ||
	Filename.check_suffix file ".imp" || 
	Filename.check_suffix file ".ref"
      ) 
  with
      Unix.Unix_error(Unix.ENOENT,_,_) -> raise Not_found

(*
  les deux fonctions suivantes construisent une liste de nom 
  de fichier a partir d'un repertoire
*)

(*TD*)
let rec listOfDirhandle dirhandle =
  let rec dir dirhandle = 
    try
      let h = Unix.readdir dirhandle
      in
      if (h<>"." && h<>"..")
      then (h :: dir dirhandle)
      else (dir dirhandle)
    with End_of_file -> []
  in
  dir dirhandle


let listOfDirectory dir =
  let dirhandle = Unix.opendir dir
  in
  let fileList=listOfDirhandle dirhandle in
    Unix.closedir dirhandle;
    fileList


(*
  droits necessaires a l'appel de certaines fonctions du 
  module System
*)

let filePerm = 0o660

(*
let fileXPerm = 0o770

*)
let directoriePerm = 0o770


let simpleMkdir dirpath =
  try
    Unix.mkdir dirpath directoriePerm  
  with Unix.Unix_error(Unix.EEXIST,_,_) -> ()


let mkpath dirpath =
  let dirlist1 = Str.split (Str.regexp "/") dirpath
  in
  let double_point str strlist =
    match str with
	".." -> 
	  (match strlist with
	       [] -> []
	     | h::t -> (str^"/"^h)::t)
      | _ -> str::strlist
  in
  let dirlist = List.fold_right double_point dirlist1 []
  in
  let makedir current_path  str = 
    let new_path = current_path^str^"/"
    in
      simpleMkdir new_path;
      new_path
  in
  let startPath=if Filename.is_relative dirpath then "" else "/" in
    ignore (List.fold_left makedir startPath dirlist)



let delete file =
   (*Sys.command ("rm "^file)*)
  try

    Sys.remove file
  with
      _ -> Error.debug ("Can't remove "^file)



let base_name file_name = 
  Filename.basename file_name


let amn_name file_name =
  Filename.chop_extension file_name


let root_name file_name =
  let base = Filename.basename file_name in
     Filename.chop_extension base


let dir_name = Filename.dirname

  
type component_type = 
    Machine
  | Refinement
  | Implementation


let btype file_path =
  if Filename.check_suffix file_path ".mch"
  then Machine
  else 
    if Filename.check_suffix file_path ".imp"
    then Implementation
    else
      if Filename.check_suffix file_path ".ref"
      then Refinement
      else raise (Invalid_argument ("Bfile.btype "^file_path^"not a B file"))


(* Extraction du nom de projet a partir du chemin d'acces*)
let project path =
  let p = Filename.dirname path    in
  let nbleft = String.rindex p '/'    in
  let pp = String.sub p 0 nbleft    in
  let nbright = String.rindex pp '/'    in
    String.sub pp (nbright+1) (String.length pp - nbright -1)

(* SC: Gard� uniquement pour bunfold! Apr�s modification du main.ml de
   bunfold, dest_check devrait ne plus �tre n�cessaire *)
let dest_check path = 
  (if Filename.check_suffix path "/"
   then path 
   else (path^"/")
  )


(* We suppose a refinement has a name different from the one of the machine *)
(* it refines *)
let find_machine directory  machine_name=
  let mch_name=directory ^ "/" ^ machine_name ^ ".mch" 
  and ref_name=directory ^ "/" ^ machine_name ^ ".ref" 
  and imp_name=directory ^ "/" ^ machine_name ^ ".imp" 
  in 
  if Sys.file_exists mch_name then mch_name
  else 
    if Sys.file_exists ref_name then ref_name
    else 
      if Sys.file_exists imp_name then imp_name
      else raise Not_found


let rec find_filename directories machine_name=
  match directories with
    | [] -> raise Not_found
    | dir::otherdirs -> 
	try
	  find_machine dir machine_name
	with
	    Not_found -> find_filename otherdirs machine_name
 

let is_abstract_machine_file file=
  Filename.check_suffix file ".mch"


let is_refinement_file file=
  Filename.check_suffix file ".ref"


let is_implementation_file file=
  Filename.check_suffix file ".imp"

