open Bparser
open Blexemes
open Blexer

(* This file contains all machinery for handling definitions *)

exception EmptyDefinition
exception MalformedDefinition of string * token list
exception MalformedDefinitionCall of string * token list

type definition_shape =
  | Definition_declaration
  | Definition_local_inclusion
  | Definition_standard_inclusion
  | Definition_incomplete
  | Definition_empty

type definition_full = {
  name : string;
  parameters : string list;
  body : Bparser.token list;
}

type definition_entry =
  | Declaration of definition_full
  | Local_inclusion of string
  | Standard_inclusion of string

(* Tells if it is a forbidden keyword to use in a definition *)

let is_forbidden token =
  match token with
 | B_MACHINE(_)
 | B_REFINEMENT(_)
 | B_IMPLEMENTATION(_)
 | B_REFINES(_)
 | B_DEFINITIONS(_)
 | B_IMPORTS(_)
 | B_SEES(_)
 | B_INCLUDES(_)
 | B_USES(_)
 | B_EXTENDS(_)
 | B_PROMOTES(_)
 | B_SETS(_)
 | B_ABSTRACT_CONSTANTS(_)
 | B_CONCRETE_CONSTANTS(_)
 | B_VISIBLE_CONSTANTS(_)  (* Addition wrt the B reference manual (BRM) *)
 | B_HIDDEN_CONSTANTS(_) (* Addition wrt the BRM *)
 | B_CONSTANTS(_)
 | B_PROPERTIES(_)
 | B_VALUES(_)
 | B_ABSTRACT_VARIABLES(_)
 | B_VARIABLES(_)
 | B_CONCRETE_VARIABLES(_)
 | B_VISIBLE_VARIABLES(_) (* Addition wrt the BRM *)
 | B_HIDDEN_VARIABLES(_) (* Addition wrt the BRM *)
 | B_INVARIANT(_)
 | B_ASSERTIONS(_)
 | B_INITIALISATION(_)
 | B_OPERATIONS(_)
 | B_LOCAL_OPERATIONS(_) (* Addition wrt the BRM *)
     -> true
 | _ -> false


let rec is_end_of_component lexemes =
  match lexemes with
    | [] -> true
    | B_formatting(_)::others
    | B_comment(_)::others -> is_end_of_component others
    | _ -> false


(* Separates the given list of lexemes into (no_defs, only_defs) where
   [no_defs] is the given list without all lexemes belonging to
   definitions and [only_defs] is the list of all lexemes belonging to
   definitions. This functions takes into account several definitions
   clauses, provided they are not following one another (forbidden by
   the BRM anyway. The order on the returned lists of lexemes is
   preserved *)
let extract_definitions lexemes =
  let rec aux_extract no_defs only_defs inside lexlist =
    match lexlist with
      | [] -> (List.rev no_defs, List.rev only_defs)
      | head_lexeme::other_lexemes ->
	if inside
	then
	  if is_forbidden head_lexeme
	  then aux_extract (head_lexeme::no_defs) only_defs false other_lexemes
	  else
	    (* END can appear in definitions, except when it is the END of the
	       machine: it should be considered a clause then *)
	    match head_lexeme with
	      | B_END(_) -> (* look if it's the end of the machine *)
		if is_end_of_component other_lexemes
		then aux_extract (head_lexeme::no_defs) only_defs false other_lexemes
		else aux_extract no_defs (head_lexeme::only_defs) true other_lexemes
	      | _ -> aux_extract no_defs (head_lexeme::only_defs) true other_lexemes
	else
	  match head_lexeme with
	    | B_DEFINITIONS(_) ->
	      aux_extract no_defs only_defs true other_lexemes
	    | _ ->
	      aux_extract (head_lexeme::no_defs) only_defs false other_lexemes
  in
  aux_extract [] [] false lexemes


(* This function analyses the given list of lexemes to see what
   definition_shape it looks like *)
let shape_of_definition deflexemes =
  let is_double_equal = function
    | B_double_equal(_) -> true
    | _ -> false
  in
  match (remove_meaningless deflexemes) with
    | [] -> Definition_empty
    | [B_STRING(_)] -> Definition_local_inclusion
    | [B_less(_); B_IDENTIFIER(_); B_greater(_)] ->
(* This limits filenames to B identifiers here, but can be quite
   easily extended *)
      Definition_standard_inclusion
    | _ ->
      if List.exists is_double_equal deflexemes
      then Definition_declaration
      else Definition_incomplete


let filename_from_definition lexemes =
  match lexemes with
    | [B_less(_); B_IDENTIFIER(li); B_greater(_)] -> li.lex_value
    | [B_STRING(li)] -> li.lex_value
    | _ -> raise (MalformedDefinition("filename_from_definition", lexemes))


let rec finalize_declaration lexemes =
  match lexemes with
    | B_IDENTIFIER(id)::params_or_dbequal -> parse_after_defid params_or_dbequal id.lex_value
    | B_formatting(_)::good_lexemes
    | B_comment(_)::good_lexemes -> finalize_declaration good_lexemes
    | _ -> raise (MalformedDefinition("finalize_declaration", lexemes))
and parse_after_defid lexemes id =
  match lexemes with
    | B_PARENOPEN(_)::params -> parse_def_parameters params id []
    | B_double_equal(_)::body -> parse_def_body lexemes id []
    | B_formatting(_)::good_lexemes
    | B_comment(_)::good_lexemes -> parse_after_defid good_lexemes id
    | _ -> raise (MalformedDefinition("parse_after_defid", lexemes))
(* Careful : the acc_params must be reversed at the end *)
and parse_def_parameters lexemes id acc_params =
  match lexemes with
    | B_IDENTIFIER(x)::restparams -> bypass_comma restparams id ((x.lex_value)::acc_params)
    | B_formatting(_)::good_lexemes
    | B_comment(_)::good_lexemes -> parse_def_parameters good_lexemes id acc_params
    | _ -> raise (MalformedDefinition("parse_def_parameters", lexemes))
and bypass_comma lexemes id acc_params =
  match lexemes with
    | B_comma(_)::restparams -> parse_def_parameters restparams id acc_params
    | B_PARENCLOSE(_)::dbeq_body -> parse_def_body dbeq_body id (List.rev acc_params)
    | B_formatting(_)::good_lexemes
    | B_comment(_)::good_lexemes -> bypass_comma good_lexemes id acc_params
    | _ -> raise (MalformedDefinition("bypass_comma", lexemes))
and parse_def_body lexemes id params =
  match lexemes with
    | B_double_equal(_)::body -> { name = id; parameters = params; body = body }
    | B_formatting(_)::good_lexemes
    | B_comment(_)::good_lexemes -> parse_def_body good_lexemes id params
    | _ -> raise (MalformedDefinition("parse_def_body", lexemes))

(* This function returns several list of lexemes : each list of
   lexemes corresponds to a single definition declaration, be it a
   definition or a definition file inclusion. This function works in
   reverse so as to avoid the "dangling ;" problem *)
let rec aux_split accdefs current_def lexlist =
  match lexlist with
    | [] ->
      begin
	match (attempt_shaping current_def) with
	  | None -> raise (MalformedDefinition("aux_split", lexlist))
	  | Some(new_def) -> new_def::accdefs
      end
    | h::t ->
      match h with
	| B_semicolon(_) ->
	  begin
	    match (attempt_shaping current_def) with
	      | None -> aux_split accdefs (h::current_def) t
	      | Some(new_def) -> aux_split (new_def::accdefs) [] t
	  end
	| _ -> aux_split accdefs (h::current_def) t

and attempt_shaping lexemes =
  match (shape_of_definition lexemes) with
    | Definition_declaration ->
      Some(Declaration(finalize_declaration lexemes))
    | Definition_local_inclusion ->
      Some(Local_inclusion(filename_from_definition lexemes))
    | Definition_standard_inclusion ->
      Some(Standard_inclusion(filename_from_definition lexemes))
    | Definition_incomplete -> None
    | Definition_empty -> raise EmptyDefinition


let split_definitions deflexemes = aux_split [] [] (List.rev deflexemes)

(* TODO: move this function into a bfile.ml, for instance *)

let rec find_filename directories filename=
  match directories with
    | [] -> raise Not_found
    | dir::otherdirs ->
      if Sys.file_exists (dir ^ "/" ^ filename)
      then (dir ^ "/" ^ filename)
      else find_filename otherdirs filename

(* TODO: some cycle checking in definition files should be done here *)
let flatten_definitions local_dir included_dirs definitions =
  let obtain_file filename =
    let deffile_lexemes = Blexer.lexemize_file filename in
    let (no_defs, def_clauses) = extract_definitions deffile_lexemes in
    let () = assert (no_defs = []) in (* normally there are only defs *)
    split_definitions def_clauses
  in
  let rec aux_flatten acc defs =
    match defs with
      | [] -> List.rev acc
      | h::t ->
	match h with
	  | Declaration(def) -> aux_flatten (def::acc) t
	  | Local_inclusion(filename) -> begin
	    try
	      let found_file = find_filename [local_dir] filename in
	      let new_definitions = obtain_file found_file in
	      aux_flatten acc (new_definitions@t)
            with
		Not_found -> raise (MalformedDefinition("flatten_definitions: " ^ filename ^ " does not exists", []))
	  end
	  | Standard_inclusion(filename) -> begin
	    try
	      let found_file = find_filename included_dirs filename in
	      let new_definitions = obtain_file found_file in
	      aux_flatten acc (new_definitions@t)
	    with
		Not_found -> raise (MalformedDefinition("flatten_definitions: " ^ filename ^ " can not be found", []))
	  end

  in
  aux_flatten [] definitions


let obtain_definition definition_list name =
  List.find (fun def -> def.name = name) definition_list

(* This function removes the remainder of a definition call and builds
   an association list telling by what each parameter is instanciated. It
   returns the association list and the lexemes without the definition
   call. Say we have:
   mydef(x,y) == something
   and somewhere in the code : mydef((1+1), f) > 0
   This function is called upon ((1+1), f) > 0, as mydef has been
   found to be a definition. Then the function builds (x -> (1+1), y
   -> f) and returns this association list as well as "> 0" as we have
   consumed the parameter instanciation of the definition call.

   Note: this function is aware of parentheses levels. This is similar
   to what the atelierB seems to do
*)
let parse_definition_call definition lexemes =
  let rec skip_first_paren lexes =
    match lexes with
      | (B_PARENOPEN(_))::t -> t
      | B_formatting(_)::good_lexes
      | B_comment(_)::good_lexes -> skip_first_paren good_lexes
      | _ -> raise (MalformedDefinitionCall("parse_definition_call: no opening parenthese", lexes))
  in
  let rec aux_parse done_assocs rem_params current_inst enclosing_stack lexes =
    match lexes with
      | [] -> raise (MalformedDefinitionCall("parse_definition_call: list of lexemes ended", lexes))
      | (B_CURLYOPEN(_) as h)::t
      | (B_BRACKOPEN(_) as h)::t
      | (B_PARENOPEN(_) as h)::t ->
	aux_parse done_assocs rem_params (h::current_inst) (h::enclosing_stack) t
      | (B_CURLYCLOSE(_) as h)::t -> begin
 	match enclosing_stack with
	  | (B_CURLYOPEN(_)::t_enclosing) ->
	    aux_parse done_assocs rem_params (h::current_inst) t_enclosing t
	  | [] -> raise (MalformedDefinitionCall("parse_definition_call: spurious } found", lexes))
	  | _ -> raise (MalformedDefinitionCall("parse_definition_call: mismatched enclosing , found }", lexes))
     end
      | (B_BRACKCLOSE(_) as h)::t -> begin
 	match enclosing_stack with
	  | (B_BRACKOPEN(_)::t_enclosing) ->
	    aux_parse done_assocs rem_params (h::current_inst) t_enclosing t
	  | [] -> raise (MalformedDefinitionCall("parse_definition_call: spurious ] found", lexes))
	  | _ -> raise (MalformedDefinitionCall("parse_definition_call: mismatched enclosing , found ]", lexes))
      end
      | (B_PARENCLOSE(_) as h)::t -> begin
	match enclosing_stack with
	  | [] -> begin (* handled as the end of the definition call *)
	    match rem_params with
	    | [] -> raise (MalformedDefinitionCall("parse_definition_call: definition called with too many parameters", lexes))
	    | [x] -> ( (x,List.rev current_inst)::done_assocs, t )
	    | _ -> raise (MalformedDefinitionCall("parse_definition_call: definition called with not enough parameters", lexes))
	  end
	  | (B_PARENOPEN(_)::t_enclosing) ->
	    aux_parse done_assocs rem_params (h::current_inst) t_enclosing t
	  | _ -> raise (MalformedDefinitionCall("parse_definition_call: mismatched enclosing , found )", lexes))
      end
      | (B_comma(_) as h)::t -> begin
	match enclosing_stack with
	  | [] -> begin
	    match rem_params with
	      | [] -> raise (MalformedDefinitionCall("parse_definition_call: definition called with too many parameters", lexes))
	      | x::other_params ->
		aux_parse
		  ((x,List.rev current_inst)::done_assocs)
		  other_params [] enclosing_stack t
	  end
	  | _ -> aux_parse done_assocs rem_params (h::current_inst) enclosing_stack t
      end
      | h::t -> aux_parse done_assocs rem_params (h::current_inst) enclosing_stack t
  in
  aux_parse [] definition.parameters [] [] (skip_first_paren lexemes)


(* This function replaces the identifiers of [lexemes] by what they
   are associated to in the [instanciations] list. For instance we have:
   mydef(x,y) == x + y(0)
   "x + y(0)" will be our list of [lexemes].
   And the association is (x -> (1+1), y -> f). Then this function will return:
   "(1+1) + f(0)".
*)
let instanciate_body instanciations lexemes =
  let rec aux_instanciate acc_instanciated lexes =
    match lexes with
      | [] -> List.rev acc_instanciated
      | (B_IDENTIFIER(x) as h)::t -> begin
	try
	  let inst = List.assoc x.lex_value instanciations in
	  aux_instanciate ((List.rev inst)@acc_instanciated) t
	with Not_found -> aux_instanciate (h::acc_instanciated) t
      end
      | h::t -> aux_instanciate (h::acc_instanciated) t
  in
  aux_instanciate [] lexemes

(* We found a definition call to expand : we parse the parameters of
   the definition in the lexemes, if any, and we return the lexemes
   prepended with body of the definition and the parameters
   instanciated *)
let do_expansion definition lexemes =
  if definition.parameters = []
  then definition.body@lexemes
  else (* We have some parameters parsing to do *)
    let (params_instanciations, removed_call_lexemes) =
      parse_definition_call definition lexemes
    in
    let instanciated_body =
      instanciate_body params_instanciations definition.body
    in
    instanciated_body@removed_call_lexemes


(* The list of definitions in parameters is supposed flattened, i.e.
   any file inclusion has been replaced with the definitions the
   referenced file contained *)
let expand_definitions definition_list lexemes =
  let rec aux_expand expanded to_expand =
    match to_expand with
      | [] -> List.rev expanded
      | (B_IDENTIFIER(x) as lex)::other_lexemes -> begin
	try
	  let definition = obtain_definition definition_list x.lex_value in
	  let done_expansion = do_expansion definition other_lexemes in
	  aux_expand expanded done_expansion
	with Not_found -> aux_expand (lex::expanded) other_lexemes
      end
      | h::t -> aux_expand (h::expanded) t
  in
  aux_expand [] lexemes

let expand local_dir included_dirs lexemes =
  let (no_defs, def_clauses) = extract_definitions lexemes in
  if def_clauses = [] then lexemes else
    let definition_entries = split_definitions def_clauses in
    let flattened_definitions =
      flatten_definitions local_dir included_dirs definition_entries
    in
    let done_expansion = expand_definitions flattened_definitions no_defs in
    done_expansion
