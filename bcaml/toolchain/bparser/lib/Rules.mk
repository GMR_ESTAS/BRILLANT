FILES = \
 blexemes.ml \
 bparser.mly \
 blexer.mll \
 bfile.ml \
 amn_cache.ml \
 definitions.ml \
 parser_lib.ml

DEPS = Bcaml

NAME = Bparser
MAKELIB = yes
