{
  open Bparser
  open Lexing
  open Blexemes
  (* little helpers *)

  (* except for strings and comments, the lexeme should start and end on
     the same line (newline characters as well) *)
  let get t   =
    let s = Lexing.lexeme t in
    let size = String.length s in
    let l_beg = (t.lex_curr_p.pos_lnum, t.lex_curr_p.pos_bol) in
    let l_end = (t.lex_curr_p.pos_lnum, t.lex_curr_p.pos_bol + size - 1) in
    begin
      t.lex_curr_p <- { t.lex_curr_p with pos_bol = t.lex_curr_p.pos_bol + size }
    end;
    { lex_value = s;
      lex_beginning = l_beg;
      lex_end = l_end
    }


  let count_new_line t =
    begin
      Lexing.new_line t;
      t.lex_curr_p <- { t.lex_curr_p with pos_bol = 0 }
    end


  let contextual_lexeme = ref (default_info "")

  (* skip_size will be 1 for strings and 2 for comments because of the
     leading characters *)
  let reset_contextual_lexeme t skip_size =
    begin
      t.lex_curr_p <- { t.lex_curr_p with pos_bol = t.lex_curr_p.pos_bol + skip_size };
      contextual_lexeme := full_lexeme_info
	""
	(t.lex_start_p.pos_lnum, t.lex_start_p.pos_bol)
	(t.lex_curr_p.pos_lnum, t.lex_curr_p.pos_bol)
    end

  let advance_contextual_lexeme t =
    let s = Lexing.lexeme t in
    let size = String.length s in
    begin
      t.lex_curr_p <- { t.lex_curr_p with pos_bol = t.lex_curr_p.pos_bol + size };
      contextual_lexeme := append !contextual_lexeme
	s
	(t.lex_curr_p.pos_lnum, t.lex_curr_p.pos_bol)
    end

  (* Same remark as reset_contextual_lexeme, but for closing
     characters *)
  let get_contextual_lexeme t skip_size =
    begin
      t.lex_curr_p <- { t.lex_curr_p with pos_bol = t.lex_curr_p.pos_bol + skip_size - 1};
      !contextual_lexeme
    end


  let init = [
    "MACHINE",           B_MACHINE(default_info "MACHINE");
    "REFINEMENT",        B_REFINEMENT(default_info "REFINEMENT");
    "IMPLEMENTATION",    B_IMPLEMENTATION(default_info "IMPLEMENTATION");
    "REFINES",           B_REFINES(default_info "REFINES");
    "CONSTRAINTS",       B_CONSTRAINTS(default_info "CONSTRAINTS");
    "SEES",              B_SEES(default_info "SEES");
    "SETS",              B_SETS(default_info "SETS");
    "CONSTANTS",         B_CONSTANTS(default_info "CONSTANTS");
    "PROPERTIES",        B_PROPERTIES(default_info "PROPERTIES");
    "INCLUDES",          B_INCLUDES(default_info "INCLUDES");
    "PROMOTES",          B_PROMOTES(default_info "PROMOTES");
    "EXTENDS",           B_EXTENDS(default_info "EXTENDS");
    "USES",              B_USES(default_info "USES");
    "VARIABLES",         B_VARIABLES(default_info "VARIABLES");
    "HIDDEN_VARIABLES",  B_HIDDEN_VARIABLES(default_info "HIDDEN_VARIABLES");
    "VISIBLE_VARIABLES", B_VISIBLE_VARIABLES(default_info "VISIBLE_VARIABLES");
    "CONCRETE_VARIABLES",B_CONCRETE_VARIABLES(default_info "CONCRETE_VARIABLES");
    "ABSTRACT_VARIABLES",B_ABSTRACT_VARIABLES(default_info "ABSTRACT_VARIABLES");
    "VISIBLE_CONSTANTS", B_VISIBLE_CONSTANTS(default_info "VISIBLE_CONSTANTS");
    "CONCRETE_CONSTANTS",B_CONCRETE_CONSTANTS(default_info "CONCRETE_CONSTANTS");
    "ABSTRACT_CONSTANTS",B_ABSTRACT_CONSTANTS(default_info "ABSTRACT_CONSTANTS");
    "HIDDEN_CONSTANTS",  B_HIDDEN_CONSTANTS(default_info "HIDDEN_CONSTANTS");
    "INVARIANT",         B_INVARIANT(default_info "INVARIANT");
    "ASSERTIONS",        B_ASSERTIONS(default_info "ASSERTIONS");
    "INITIALISATION",    B_INITIALISATION(default_info "INITIALISATION");
    "LOCAL_OPERATIONS",  B_LOCAL_OPERATIONS(default_info "LOCAL_OPERATIONS");
    "OPERATIONS",        B_OPERATIONS(default_info "OPERATIONS");
    "IMPORTS",           B_IMPORTS(default_info "IMPORTS");
    "DEFINITIONS",       B_DEFINITIONS(default_info "DEFINITIONS");
    "VALUES",            B_VALUES(default_info "VALUES");
    "ANY",               B_ANY(default_info "ANY");
    "or",                B_or(default_info "or");
    "not",               B_not(default_info "not");
    "skip",              B_SKIP(default_info "SKIP");
    "BEGIN",             B_BEGIN(default_info "BEGIN");
    "PRE",               B_PRE(default_info "PRE");
    "IF",                B_IF(default_info "IF");
    "THEN",              B_THEN(default_info "THEN");
    "ELSIF",             B_ELSIF(default_info "ELSIF");
    "ELSE",              B_ELSE(default_info "ELSE");
    "ASSERT",            B_ASSERT(default_info "ASSERT");
    "WHERE",             B_WHERE(default_info "WHERE");
    "LET",               B_LET(default_info "LET");
    "BE",                B_BE(default_info "BE");
    "IN",                B_IN(default_info "IN");
    "CHOICE",            B_CHOICE(default_info "CHOICE");
    "OR",                B_OR(default_info "OR");
    "SELECT",            B_SELECT(default_info "SELECT");
    "WHEN",              B_WHEN(default_info "WHEN");
    "CASE",              B_CASE(default_info "CASE");
    "OF",                B_OF(default_info "OF");
    "EITHER",            B_EITHER(default_info "EITHER");
    "VAR",               B_VAR(default_info "VAR");
    "WHILE",             B_WHILE(default_info "WHILE");
    "DO",                B_DO(default_info "DO");
    "VARIANT",           B_VARIANT(default_info "VARIANT");
    "BOOL",              B_BOOL(default_info "BOOL");
    "STRING",            B_STRINGS(default_info "STRING");
    "NAT",               B_NAT(default_info "NAT");
    "NAT1",              B_NATONE(default_info "NAT1");
    "NATURAL",           B_NATURAL(default_info "NATURAL");
    "NATURAL1",          B_NATURALONE(default_info "NATURAL1");
    "INT",               B_INT(default_info "INT");
    "INTEGER",           B_INTEGER(default_info "INTEGER");
    "TRUE",              B_true(default_info "TRUE");
    "FALSE",             B_false(default_info "FALSE");
    "POW",               B_POW(default_info "POW");
    "FIN",               B_FIN(default_info "FIN");
    "POW1",              B_POWONE(default_info "POW1");
    "FIN1",              B_FINONE(default_info "FIN1");
    "inter",             B_inter(default_info "inter");
    "union",             B_union(default_info "union");
    "UNION",             B_UNION(default_info "UNION");
    "INTER",             B_INTER(default_info "INTER");
    "id",                B_id(default_info "id");
    "dom",               B_dom(default_info "dom");
    "ran",               B_ran(default_info "ran");
    "prj1",              B_prjone(default_info "prj1");
    "prj2",              B_prjtwo(default_info "prj1");
    "iterate",           B_iterate(default_info "iterate");
    "closure",           B_closure(default_info "closure");
    "closure1",          B_closureone(default_info "closure1");
    "card",              B_card(default_info "card");
    "max",               B_max(default_info "max");
    "min",               B_min(default_info "min");
    "pred",              B_pred(default_info "pred");
    "succ",              B_succ(default_info "succ");
    "mod",               B_mod(default_info "mod");
    "SIGMA",             B_SIGMA(default_info "SIGMA");
    "PI",                B_PI(default_info "PI");
    "seq",               B_seq(default_info "seq");
    "iseq",              B_iseq(default_info "iseq");
    "seq1",              B_seqone(default_info "seq1");
    "iseq1",             B_iseqone(default_info "iseq1");
    "perm",              B_perm(default_info "perm");
    "conc",              B_conc(default_info "conc");
    "front",             B_front(default_info "front");
    "tail",              B_tail(default_info "tail");
    "first",             B_first(default_info "first");
    "last",              B_last(default_info "last");
    "size",              B_size(default_info "size");
    "rev",               B_rev(default_info "rev");
    "fnc",               B_fnc(default_info "fnc");
    "rel",               B_rel(default_info "rel");
    "bool",              B_bool(default_info "bool");
    "tree",              B_tree(default_info "tree");
    "binbtree",          B_btree(default_info "binbtree");
    "consconst",         B_consconst(default_info "consconst");
    "top",               B_top(default_info "top");
    "sons",              B_sons(default_info "sons");
    "preprefix",         B_prefix(default_info "preprefix");
    "postpostfix",       B_postfix(default_info "postpostfix");
    "sizet",             B_sizet(default_info "sizet");
    "mirror",            B_mirror(default_info "mirror");
    "rank",              B_rank(default_info "rank");
    "father",            B_father(default_info "father");
    "son",               B_son(default_info "son");
    "subtree",           B_subtree(default_info "subtree");
    "arity",             B_arity(default_info "arity");
    "bin",               B_bin(default_info "bin");
    "left",              B_left(default_info "left");
    "right",             B_right(default_info "right");
    "infix",             B_infix(default_info "infix");
    "END",               B_END(default_info "END");
    "MAXINT",            B_MAXINT(default_info "MAXINT");
    "MININT",            B_MININT(default_info "MININT");
    "struct",            B_struct(default_info "struct");
    "rec",               B_rec(default_info "rec");
  ]


  let keyword_table =
    let tbl = Hashtbl.create (List.length init) in
    (
      List.iter (fun (key, data) -> Hashtbl.add tbl key data) init;
      tbl
    )

  let update_keyword_lexeme_pos k linfo =
    match k with
      | B_MACHINE(li) -> B_MACHINE(super_impose_position li linfo)
      | B_REFINEMENT(li) -> B_REFINEMENT(super_impose_position li linfo)
      | B_IMPLEMENTATION(li) -> B_IMPLEMENTATION(super_impose_position li linfo)
      | B_REFINES(li) -> B_REFINES(super_impose_position li linfo)
      | B_CONSTRAINTS(li) -> B_CONSTRAINTS(super_impose_position li linfo)
      | B_SEES(li) -> B_SEES(super_impose_position li linfo)
      | B_SETS(li) -> B_SETS(super_impose_position li linfo)
      | B_CONSTANTS(li) -> B_CONSTANTS(super_impose_position li linfo)
      | B_PROPERTIES(li) -> B_PROPERTIES(super_impose_position li linfo)
      | B_INCLUDES(li) -> B_INCLUDES(super_impose_position li linfo)
      | B_PROMOTES(li) -> B_PROMOTES(super_impose_position li linfo)
      | B_EXTENDS(li) -> B_EXTENDS(super_impose_position li linfo)
      | B_USES(li) -> B_USES(super_impose_position li linfo)
      | B_VARIABLES(li) -> B_VARIABLES(super_impose_position li linfo)
      | B_HIDDEN_VARIABLES(li) -> B_HIDDEN_VARIABLES(super_impose_position li linfo)
      | B_VISIBLE_VARIABLES(li) -> B_VISIBLE_VARIABLES(super_impose_position li linfo)
      | B_CONCRETE_VARIABLES(li) -> B_CONCRETE_VARIABLES(super_impose_position li linfo)
      | B_ABSTRACT_VARIABLES(li) -> B_ABSTRACT_VARIABLES(super_impose_position li linfo)
      | B_VISIBLE_CONSTANTS(li) -> B_VISIBLE_CONSTANTS(super_impose_position li linfo)
      | B_CONCRETE_CONSTANTS(li) -> B_CONCRETE_CONSTANTS(super_impose_position li linfo)
      | B_ABSTRACT_CONSTANTS(li) -> B_ABSTRACT_CONSTANTS(super_impose_position li linfo)
      | B_HIDDEN_CONSTANTS(li) -> B_HIDDEN_CONSTANTS(super_impose_position li linfo)
      | B_INVARIANT(li) -> B_INVARIANT(super_impose_position li linfo)
      | B_ASSERTIONS(li) -> B_ASSERTIONS(super_impose_position li linfo)
      | B_INITIALISATION(li) -> B_INITIALISATION(super_impose_position li linfo)
      | B_LOCAL_OPERATIONS(li) -> B_LOCAL_OPERATIONS(super_impose_position li linfo)
      | B_OPERATIONS(li) -> B_OPERATIONS(super_impose_position li linfo)
      | B_IMPORTS(li) -> B_IMPORTS(super_impose_position li linfo)
      | B_DEFINITIONS(li) -> B_DEFINITIONS(super_impose_position li linfo)
      | B_VALUES(li) -> B_VALUES(super_impose_position li linfo)
      | B_ANY(li) -> B_ANY(super_impose_position li linfo)
      | B_or(li) -> B_or(super_impose_position li linfo)
      | B_not(li) -> B_not(super_impose_position li linfo)
      | B_SKIP(li) -> B_SKIP(super_impose_position li linfo)
      | B_BEGIN(li) -> B_BEGIN(super_impose_position li linfo)
      | B_PRE(li) -> B_PRE(super_impose_position li linfo)
      | B_IF(li) -> B_IF(super_impose_position li linfo)
      | B_THEN(li) -> B_THEN(super_impose_position li linfo)
      | B_ELSIF(li) -> B_ELSIF(super_impose_position li linfo)
      | B_ELSE(li) -> B_ELSE(super_impose_position li linfo)
      | B_ASSERT(li) -> B_ASSERT(super_impose_position li linfo)
      | B_WHERE(li) -> B_WHERE(super_impose_position li linfo)
      | B_LET(li) -> B_LET(super_impose_position li linfo)
      | B_BE(li) -> B_BE(super_impose_position li linfo)
      | B_IN(li) -> B_IN(super_impose_position li linfo)
      | B_CHOICE(li) -> B_CHOICE(super_impose_position li linfo)
      | B_OR(li) -> B_OR(super_impose_position li linfo)
      | B_SELECT(li) -> B_SELECT(super_impose_position li linfo)
      | B_WHEN(li) -> B_WHEN(super_impose_position li linfo)
      | B_CASE(li) -> B_CASE(super_impose_position li linfo)
      | B_OF(li) -> B_OF(super_impose_position li linfo)
      | B_EITHER(li) -> B_EITHER(super_impose_position li linfo)
      | B_VAR(li) -> B_VAR(super_impose_position li linfo)
      | B_WHILE(li) -> B_WHILE(super_impose_position li linfo)
      | B_DO(li) -> B_DO(super_impose_position li linfo)
      | B_VARIANT(li) -> B_VARIANT(super_impose_position li linfo)
      | B_BOOL(li) -> B_BOOL(super_impose_position li linfo)
      | B_STRINGS(li) -> B_STRINGS(super_impose_position li linfo)
      | B_NAT(li) -> B_NAT(super_impose_position li linfo)
      | B_NATONE(li) -> B_NATONE(super_impose_position li linfo)
      | B_NATURAL(li) -> B_NATURAL(super_impose_position li linfo)
      | B_NATURALONE(li) -> B_NATURALONE(super_impose_position li linfo)
      | B_INT(li) -> B_INT(super_impose_position li linfo)
      | B_INTEGER(li) -> B_INTEGER(super_impose_position li linfo)
      | B_true(li) -> B_true(super_impose_position li linfo)
      | B_false(li) -> B_false(super_impose_position li linfo)
      | B_POW(li) -> B_POW(super_impose_position li linfo)
      | B_FIN(li) -> B_FIN(super_impose_position li linfo)
      | B_POWONE(li) -> B_POWONE(super_impose_position li linfo)
      | B_FINONE(li) -> B_FINONE(super_impose_position li linfo)
      | B_inter(li) -> B_inter(super_impose_position li linfo)
      | B_union(li) -> B_union(super_impose_position li linfo)
      | B_UNION(li) -> B_UNION(super_impose_position li linfo)
      | B_INTER(li) -> B_INTER(super_impose_position li linfo)
      | B_id(li) -> B_id(super_impose_position li linfo)
      | B_dom(li) -> B_dom(super_impose_position li linfo)
      | B_ran(li) -> B_ran(super_impose_position li linfo)
      | B_prjone(li) -> B_prjone(super_impose_position li linfo)
      | B_prjtwo(li) -> B_prjtwo(super_impose_position li linfo)
      | B_iterate(li) -> B_iterate(super_impose_position li linfo)
      | B_closure(li) -> B_closure(super_impose_position li linfo)
      | B_closureone(li) -> B_closureone(super_impose_position li linfo)
      | B_card(li) -> B_card(super_impose_position li linfo)
      | B_max(li) -> B_max(super_impose_position li linfo)
      | B_min(li) -> B_min(super_impose_position li linfo)
      | B_pred(li) -> B_pred(super_impose_position li linfo)
      | B_succ(li) -> B_succ(super_impose_position li linfo)
      | B_mod(li) -> B_mod(super_impose_position li linfo)
      | B_SIGMA(li) -> B_SIGMA(super_impose_position li linfo)
      | B_PI(li) -> B_PI(super_impose_position li linfo)
      | B_seq(li) -> B_seq(super_impose_position li linfo)
      | B_iseq(li) -> B_iseq(super_impose_position li linfo)
      | B_seqone(li) -> B_seqone(super_impose_position li linfo)
      | B_iseqone(li) -> B_iseqone(super_impose_position li linfo)
      | B_perm(li) -> B_perm(super_impose_position li linfo)
      | B_conc(li) -> B_conc(super_impose_position li linfo)
      | B_front(li) -> B_front(super_impose_position li linfo)
      | B_tail(li) -> B_tail(super_impose_position li linfo)
      | B_first(li) -> B_first(super_impose_position li linfo)
      | B_last(li) -> B_last(super_impose_position li linfo)
      | B_size(li) -> B_size(super_impose_position li linfo)
      | B_rev(li) -> B_rev(super_impose_position li linfo)
      | B_fnc(li) -> B_fnc(super_impose_position li linfo)
      | B_rel(li) -> B_rel(super_impose_position li linfo)
      | B_bool(li) -> B_bool(super_impose_position li linfo)
      | B_tree(li) -> B_tree(super_impose_position li linfo)
      | B_btree(li) -> B_btree(super_impose_position li linfo)
      | B_consconst(li) -> B_consconst(super_impose_position li linfo)
      | B_top(li) -> B_top(super_impose_position li linfo)
      | B_sons(li) -> B_sons(super_impose_position li linfo)
      | B_prefix(li) -> B_prefix(super_impose_position li linfo)
      | B_postfix(li) -> B_postfix(super_impose_position li linfo)
      | B_sizet(li) -> B_sizet(super_impose_position li linfo)
      | B_mirror(li) -> B_mirror(super_impose_position li linfo)
      | B_rank(li) -> B_rank(super_impose_position li linfo)
      | B_father(li) -> B_father(super_impose_position li linfo)
      | B_son(li) -> B_son(super_impose_position li linfo)
      | B_subtree(li) -> B_subtree(super_impose_position li linfo)
      | B_arity(li) -> B_arity(super_impose_position li linfo)
      | B_bin(li) -> B_bin(super_impose_position li linfo)
      | B_left(li) -> B_left(super_impose_position li linfo)
      | B_right(li) -> B_right(super_impose_position li linfo)
      | B_infix(li) -> B_infix(super_impose_position li linfo)
      | B_END(li) -> B_END(super_impose_position li linfo)
      | B_MAXINT(li) -> B_MAXINT(super_impose_position li linfo)
      | B_MININT(li) -> B_MININT(super_impose_position li linfo)
      | B_struct(li) -> B_struct(super_impose_position li linfo)
      | B_rec(li) -> B_rec(super_impose_position li linfo)
      | _ -> invalid_arg "update_keyword_lexeme_pos: not a keyword lexeme"


}

rule token = parse
  | [' ' '\t']+	{B_formatting(get lexbuf)}
  | '\n'        { let r = B_formatting(get lexbuf) in
		  let () = count_new_line lexbuf in
		  r
		}
  | "\r\n"      { let r = B_formatting(get lexbuf) in
		  let () = count_new_line lexbuf in
		  r
		}
  | "/*"        {reset_contextual_lexeme lexbuf 2; comment lexbuf}
  | "//"        {reset_contextual_lexeme lexbuf 2; one_line_comment lexbuf}
  | "\""        {reset_contextual_lexeme lexbuf 1; string lexbuf}
  | "'"         {B_quote(get lexbuf)}
  | "=="        {B_double_equal(get lexbuf)}
  | "-"         {B_minus(get lexbuf)}
  | "\\"        {B_setminus(get lexbuf)} (* Non-standard notation *)
  | "+"         {B_plus(get lexbuf)}
  | "**"        {B_power(get lexbuf)}
  | "*"         {B_mul(get lexbuf)}
  | "/"         {B_div(get lexbuf)}
  | "="         {B_equal(get lexbuf)}
  | "&"         {B_conjunction(get lexbuf)}
  | "<"         {B_less(get lexbuf)}
  | ">"         {B_greater(get lexbuf)}
  | ","         {B_comma(get lexbuf)}
  | ";"         {B_semicolon(get lexbuf)}
  | ":"         {B_in(get lexbuf)}
  | "::"        {B_setin(get lexbuf)}
  | "<=>"       {B_equivalence(get lexbuf)}
  | "<:"        {B_subset(get lexbuf)}
  | "/<:"       {B_notsubset(get lexbuf)}
  | "=>"        {B_implies(get lexbuf)}
  | "<<:"       {B_strictsubset(get lexbuf)}
  | "/<<:"      {B_notstrictsubset(get lexbuf)}
  | "/:"        {B_notin(get lexbuf)}
  | "/="        {B_notequal(get lexbuf)}
  | "<="        {B_lessorequal(get lexbuf)}
  | ">="        {B_greaterorequal(get lexbuf)}
  | "("         {B_PARENOPEN(get lexbuf)}
  | ")"         {B_PARENCLOSE(get lexbuf)}
  | ">*<"       {B_cartesianprod(get lexbuf)} (* Non-standard notation *)
  | "><"        {B_directprod(get lexbuf)}
  | "(><)"      {B_directprod(get lexbuf)} (* Non-standard notation *)
  | "+->"       {B_partialfunc(get lexbuf)}
  | "<->"       {B_relation(get lexbuf)}
  | "-->"       {B_totalfunc(get lexbuf)}
  | ">+>"       {B_partialinject(get lexbuf)}
  | ">->"       {B_totalinject(get lexbuf)}
  | "+->>"      {B_partialsurject(get lexbuf)}
  | "-->>"      {B_totalsurject(get lexbuf)}
  | ">->>"      {B_bijection(get lexbuf)}
  | ">+>>"      {B_partialbij(get lexbuf)}
  | "<--"       {B_operationsig(get lexbuf)}
  | ":="        {B_simplesubst(get lexbuf)}
  | "["         {B_BRACKOPEN(get lexbuf)}
  | "]"         {B_BRACKCLOSE(get lexbuf)}
  | "||"        {B_parallelcomp(get lexbuf)}
  | "|->"       {B_mapletorpair(get lexbuf)}
  | "<|"        {B_domainrestrict(get lexbuf)}
  | "|>"        {B_rangerestrict(get lexbuf)}
  | "<<|"       {B_domainsubstract(get lexbuf)}
  | "|>>"       {B_rangesubstract(get lexbuf)}
  | "<+"        {B_override(get lexbuf)}
  | "^"         {B_concatseq(get lexbuf)}
  | "->"        {B_prependseq(get lexbuf)}
  | "<-"        {B_appendseq(get lexbuf)}
  | "/|\\"      {B_prefixseq(get lexbuf)}
  | "\\|/"      {B_suffixseq(get lexbuf)}
  | ".."        {B_natsetrange(get lexbuf)}
  | "\\/"       {B_unionsets(get lexbuf)}
  | "/\\"       {B_intersectionsets(get lexbuf)}
  | "%"         {B_lambda(get lexbuf)}
  | "{}"        {B_emptyset(get lexbuf)}
  | "<>"        {B_seqempty(get lexbuf)}
  | "[]"        {B_seqempty(get lexbuf)}
  | "."         {B_point(get lexbuf)}
  | '|'         {B_such(get lexbuf)}
  | "!"         {B_forall(get lexbuf)}
  | "#"         {B_exists(get lexbuf)}
  | "~"         {B_tilde(get lexbuf)}
  | "{"         {B_CURLYOPEN(get lexbuf)}
  | "}"         {B_CURLYCLOSE(get lexbuf)}
  | "$0"        {B_before(get lexbuf)}
  | ['a'-'z' 'A'-'Z' '_'] (['a'-'z' 'A'-'Z' '_' '0'-'9'] | '.'['a'-'z' 'A'-'Z' '_' '0'-'9']) *
      {   let s = get lexbuf in
	  try
	    let found_lexeme = Hashtbl.find keyword_table s.lex_value in
	    update_keyword_lexeme_pos found_lexeme s
	  with Not_found -> B_IDENTIFIER(s)
      }
  |['0'-'9']+	{B_NUMBER(get lexbuf)}
  | eof         {EOF}
  | _           { let c = get lexbuf in
		  Error.error (
		    "Illegal character line "
		    ^ (string_of_int (fst c.lex_beginning)) ^ " column "
		    ^ (string_of_int (snd c.lex_beginning)) ^ ": "
		    ^ c.lex_value
		  )
		}

and comment = parse
  | [^ '\n']  {advance_contextual_lexeme lexbuf; comment lexbuf}
  | '\n'      {advance_contextual_lexeme lexbuf; count_new_line lexbuf; comment lexbuf}
  | "*/"      {B_comment(get_contextual_lexeme lexbuf 2)}
  | eof       {Error.error "unterminated comment"}

and one_line_comment = parse
  | [^ '\r' '\n']  {advance_contextual_lexeme lexbuf; one_line_comment lexbuf}
  | "\r\n"         {B_comment(get_contextual_lexeme lexbuf 2)}
  | '\r'           {advance_contextual_lexeme lexbuf; one_line_comment lexbuf}
  | '\n'           {B_comment(get_contextual_lexeme lexbuf 1)}
  | eof            {B_comment(get_contextual_lexeme lexbuf 0)}

and string = parse
  | [^ '"' '\n']+ {advance_contextual_lexeme lexbuf; string lexbuf}
  | '\n'          {advance_contextual_lexeme lexbuf; count_new_line lexbuf; string lexbuf}
  | '"'           {B_STRING(get_contextual_lexeme lexbuf 1)}
  | eof	          {Error.error "unterminated string"}
  | _	          {Error.error "illegal char in string"}

{
(* Here follow some handy functions, needed e.g. for definitions *)

(* Useful for debugging lexing-related problems in general,
   definitions in particular. Obsoletes debug.ml *)
let string_of_lexeme lexeme =
  match lexeme with
    | EOF -> "EOF"
    | B_formatting(s) -> "B_formatting:" ^ (string_of_lex_info s)
    | B_comment(s) -> "B_comment:" ^ (string_of_lex_info s)
    | B_NUMBER(s) -> "B_NUMBER:" ^ (string_of_lex_info s)
    | B_STRING(s) -> "B_STRING:" ^ (string_of_lex_info s)
    | B_IDENTIFIER(s) -> "B_IDENTIFIER: " ^ (string_of_lex_info s)
    | B_MACHINE(s) -> "B_MACHINE: " ^ (string_of_lex_info s)
    | B_CONSTRAINTS(s) -> "B_CONSTRAINTS: " ^ (string_of_lex_info s)
    | B_SEES(s) -> "B_SEES: " ^ (string_of_lex_info s)
    | B_SETS(s) -> "B_SETS: " ^ (string_of_lex_info s)
    | B_CONSTANTS(s) -> "B_CONSTANTS: " ^ (string_of_lex_info s)
    | B_CONCRETE_CONSTANTS(s) -> "B_CONCRETE_CONSTANTS: " ^ (string_of_lex_info s)
    | B_ABSTRACT_CONSTANTS(s) -> "B_ABSTRACT_CONSTANTS: " ^ (string_of_lex_info s)
    | B_VISIBLE_CONSTANTS(s) -> "B_VISIBLE_CONSTANTS: " ^ (string_of_lex_info s)
    | B_HIDDEN_CONSTANTS(s) -> "B_HIDDEN_CONSTANTS: " ^ (string_of_lex_info s)
    | B_PROPERTIES(s) -> "B_PROPERTIES: " ^ (string_of_lex_info s)
    | B_INCLUDES(s) -> "B_INCLUDES: " ^ (string_of_lex_info s)
    | B_PROMOTES(s) -> "B_PROMOTES: " ^ (string_of_lex_info s)
    | B_EXTENDS(s) -> "B_EXTENDS: " ^ (string_of_lex_info s)
    | B_USES(s) -> "B_USES: " ^ (string_of_lex_info s)
    | B_VARIABLES(s) -> "B_VARIABLES: " ^ (string_of_lex_info s)
    | B_CONCRETE_VARIABLES(s) -> "B_CONCRETE_VARIABLES: " ^ (string_of_lex_info s)
    | B_ABSTRACT_VARIABLES(s) -> "B_ABSTRACT_VARIABLES: " ^ (string_of_lex_info s)
    | B_VISIBLE_VARIABLES(s) -> "B_VISIBLE_VARIABLES: " ^ (string_of_lex_info s)
    | B_HIDDEN_VARIABLES(s) -> "B_HIDDEN_VARIABLES: " ^ (string_of_lex_info s)
    | B_INVARIANT(s) -> "B_INVARIANT: " ^ (string_of_lex_info s)
    | B_INITIALISATION(s) -> "B_INITIALISATION: " ^ (string_of_lex_info s)
    | B_LOCAL_OPERATIONS(s) -> "B_LOCAL_OPERATIONS: " ^ (string_of_lex_info s)
    | B_OPERATIONS(s) -> "B_OPERATIONS: " ^ (string_of_lex_info s)
    | B_REFINEMENT(s) -> "B_REFINEMENT: " ^ (string_of_lex_info s)
    | B_REFINES(s) -> "B_REFINES: " ^ (string_of_lex_info s)
    | B_IMPLEMENTATION(s) -> "B_IMPLEMENTATION: " ^ (string_of_lex_info s)
    | B_IMPORTS(s) -> "B_IMPORTS: " ^ (string_of_lex_info s)
    | B_DEFINITIONS(s) -> "B_DEFINITIONS: " ^ (string_of_lex_info s)
    | B_VALUES(s) -> "B_VALUES: " ^ (string_of_lex_info s)
    | B_ASSERTIONS(s) -> "B_ASSERTIONS: " ^ (string_of_lex_info s)
    | B_or(s) -> "B_or: " ^ (string_of_lex_info s)
    | B_setminus(s) -> "B_setminus: " ^ (string_of_lex_info s)
    | B_not(s) -> "B_not: " ^ (string_of_lex_info s)
    | B_SKIP(s) -> "B_SKIP: " ^ (string_of_lex_info s)
    | B_BEGIN(s) -> "B_BEGIN: " ^ (string_of_lex_info s)
    | B_PRE(s) -> "B_PRE: " ^ (string_of_lex_info s)
    | B_IF(s) -> "B_IF: " ^ (string_of_lex_info s)
    | B_THEN(s) -> "B_THEN: " ^ (string_of_lex_info s)
    | B_ELSIF(s) -> "B_ELSIF: " ^ (string_of_lex_info s)
    | B_ELSE(s) -> "B_ELSE: " ^ (string_of_lex_info s)
    | B_ASSERT(s) -> "B_ASSERT: " ^ (string_of_lex_info s)
    | B_ANY(s) -> "B_ANY: " ^ (string_of_lex_info s)
    | B_WHERE(s) -> "B_WHERE: " ^ (string_of_lex_info s)
    | B_LET(s) -> "B_LET: " ^ (string_of_lex_info s)
    | B_BE(s) -> "B_BE: " ^ (string_of_lex_info s)
    | B_IN(s) -> "B_IN: " ^ (string_of_lex_info s)
    | B_CHOICE(s) -> "B_CHOICE: " ^ (string_of_lex_info s)
    | B_OR(s) -> "B_OR: " ^ (string_of_lex_info s)
    | B_SELECT(s) -> "B_SELECT: " ^ (string_of_lex_info s)
    | B_WHEN(s) -> "B_WHEN: " ^ (string_of_lex_info s)
    | B_CASE(s) -> "B_CASE: " ^ (string_of_lex_info s)
    | B_OF(s) -> "B_OF: " ^ (string_of_lex_info s)
    | B_EITHER(s) -> "B_EITHER: " ^ (string_of_lex_info s)
    | B_VAR(s) -> "B_VAR: " ^ (string_of_lex_info s)
    | B_WHILE(s) -> "B_WHILE: " ^ (string_of_lex_info s)
    | B_DO(s) -> "B_DO: " ^ (string_of_lex_info s)
    | B_VARIANT(s) -> "B_VARIANT: " ^ (string_of_lex_info s)
    | B_NAT(s) -> "B_NAT: " ^ (string_of_lex_info s)
    | B_NATONE(s) -> "B_NATONE: " ^ (string_of_lex_info s)
    | B_NATURAL(s) -> "B_NATURAL: " ^ (string_of_lex_info s)
    | B_NATURALONE(s) -> "B_NATURALONE: " ^ (string_of_lex_info s)
    | B_INT(s) -> "B_INT: " ^ (string_of_lex_info s)
    | B_INTEGER(s) -> "B_INTEGER: " ^ (string_of_lex_info s)
    | B_false(s) -> "B_false: " ^ (string_of_lex_info s)
    | B_true(s) -> "B_true: " ^ (string_of_lex_info s)
    | B_POW(s) -> "B_POW: " ^ (string_of_lex_info s)
    | B_FIN(s) -> "B_FIN: " ^ (string_of_lex_info s)
    | B_POWONE(s) -> "B_POWONE: " ^ (string_of_lex_info s)
    | B_FINONE(s) -> "B_FINONE: " ^ (string_of_lex_info s)
    | B_union(s) -> "B_union: " ^ (string_of_lex_info s)
    | B_inter(s) -> "B_inter: " ^ (string_of_lex_info s)
    | B_UNION(s) -> "B_UNION: " ^ (string_of_lex_info s)
    | B_INTER(s) -> "B_INTER: " ^ (string_of_lex_info s)
    | B_id(s) -> "B_id: " ^ (string_of_lex_info s)
    | B_dom(s) -> "B_dom: " ^ (string_of_lex_info s)
    | B_ran(s) -> "B_ran: " ^ (string_of_lex_info s)
    | B_iterate(s) -> "B_iterate: " ^ (string_of_lex_info s)
    | B_closure(s) -> "B_closure: " ^ (string_of_lex_info s)
    | B_closureone(s) -> "B_closureone: " ^ (string_of_lex_info s)
    | B_card(s) -> "B_card: " ^ (string_of_lex_info s)
    | B_max(s) -> "B_max: " ^ (string_of_lex_info s)
    | B_min(s) -> "B_min: " ^ (string_of_lex_info s)
    | B_mod(s) -> "B_mod: " ^ (string_of_lex_info s)
    | B_SIGMA(s) -> "B_SIGMA: " ^ (string_of_lex_info s)
    | B_PI(s) -> "B_PI: " ^ (string_of_lex_info s)
    | B_seq(s) -> "B_seq: " ^ (string_of_lex_info s)
    | B_iseq(s) -> "B_iseq: " ^ (string_of_lex_info s)
    | B_seqone(s) -> "B_seqone: " ^ (string_of_lex_info s)
    | B_iseqone(s) -> "B_iseqone: " ^ (string_of_lex_info s)
    | B_perm(s) -> "B_perm: " ^ (string_of_lex_info s)
    | B_conc(s) -> "B_conc: " ^ (string_of_lex_info s)
    | B_front(s) -> "B_front: " ^ (string_of_lex_info s)
    | B_tail(s) -> "B_tail: " ^ (string_of_lex_info s)
    | B_first(s) -> "B_first: " ^ (string_of_lex_info s)
    | B_last(s) -> "B_last: " ^ (string_of_lex_info s)
    | B_size(s) -> "B_size: " ^ (string_of_lex_info s)
    | B_rev(s) -> "B_rev: " ^ (string_of_lex_info s)
    | B_bool(s) -> "B_bool: " ^ (string_of_lex_info s)
    | B_END(s) -> "B_END: " ^ (string_of_lex_info s)
    | B_struct(s) -> "B_struct: " ^ (string_of_lex_info s)
    | B_rec(s) -> "B_rec: " ^ (string_of_lex_info s)
    | B_quote(s) -> "B_quote: " ^ (string_of_lex_info s)
    | B_minus(s) -> "B_minus: " ^ (string_of_lex_info s)
    | B_tilde(s) -> "B_tilde: " ^ (string_of_lex_info s)
    | B_before(s) -> "B_before: " ^ (string_of_lex_info s)
    | B_plus(s) -> "B_plus: " ^ (string_of_lex_info s)
    | B_power(s) -> "B_power: " ^ (string_of_lex_info s)
    | B_concatseq(s) -> "B_concatseq: " ^ (string_of_lex_info s)
    | B_mul(s) -> "B_mul: " ^ (string_of_lex_info s)
    | B_div(s) -> "B_div: " ^ (string_of_lex_info s)
    | B_equal(s) -> "B_equal: " ^ (string_of_lex_info s)
    | B_conjunction(s) -> "B_conjunction: " ^ (string_of_lex_info s)
    | B_less(s) -> "B_less: " ^ (string_of_lex_info s)
    | B_greater(s) -> "B_greater: " ^ (string_of_lex_info s)
    | B_comma(s) -> "B_comma: " ^ (string_of_lex_info s)
    | B_semicolon(s) -> "B_semicolon: " ^ (string_of_lex_info s)
    | B_in(s) -> "B_in: " ^ (string_of_lex_info s)
    | B_setin(s) -> "B_setin: " ^ (string_of_lex_info s)
    | B_equivalence(s) -> "B_equivalence: " ^ (string_of_lex_info s)
    | B_subset(s) -> "B_subset: " ^ (string_of_lex_info s)
    | B_notsubset(s) -> "B_notsubset: " ^ (string_of_lex_info s)
    | B_implies(s) -> "B_implies: " ^ (string_of_lex_info s)
    | B_strictsubset(s) -> "B_strictsubset: " ^ (string_of_lex_info s)
    | B_notstrictsubset(s) -> "B_notstrictsubset: " ^ (string_of_lex_info s)
    | B_notin(s) -> "B_notin: " ^ (string_of_lex_info s)
    | B_notequal(s) -> "B_notequal: " ^ (string_of_lex_info s)
    | B_lessorequal(s) -> "B_lessorequal: " ^ (string_of_lex_info s)
    | B_greaterorequal(s) -> "B_greaterorequal: " ^ (string_of_lex_info s)
    | B_relation(s) -> "B_relation: " ^ (string_of_lex_info s)
    | B_rel(s) -> "B_rel: " ^ (string_of_lex_info s)
    | B_fnc(s) -> "B_fnc: " ^ (string_of_lex_info s)
    | B_cartesianprod(s) -> "B_cartesianprod: " ^ (string_of_lex_info s)
    | B_totalfunc(s) -> "B_totalfunc: " ^ (string_of_lex_info s)
    | B_partialfunc(s) -> "B_partialfunc: " ^ (string_of_lex_info s)
    | B_totalsurject(s) -> "B_totalsurject: " ^ (string_of_lex_info s)
    | B_partialsurject(s) -> "B_partialsurject: " ^ (string_of_lex_info s)
    | B_totalinject(s) -> "B_totalinject: " ^ (string_of_lex_info s)
    | B_partialinject(s) -> "B_partialinject: " ^ (string_of_lex_info s)
    | B_partialbij(s) -> "B_partialbij: " ^ (string_of_lex_info s)
    | B_bijection(s) -> "B_bijection: " ^ (string_of_lex_info s)
    | B_operationsig(s) -> "B_operationsig: " ^ (string_of_lex_info s)
    | B_simplesubst(s) -> "B_simplesubst: " ^ (string_of_lex_info s)
    | B_CURLYOPEN(s) -> "B_CURLYOPEN: " ^ (string_of_lex_info s)
    | B_CURLYCLOSE(s) -> "B_CURLYCLOSE: " ^ (string_of_lex_info s)
    | B_PARENOPEN(s) -> "B_PARENOPEN: " ^ (string_of_lex_info s)
    | B_PARENCLOSE(s) -> "B_PARENCLOSE: " ^ (string_of_lex_info s)
    | B_BRACKOPEN(s) -> "B_BRACKOPEN: " ^ (string_of_lex_info s)
    | B_BRACKCLOSE(s) -> "B_BRACKCLOSE: " ^ (string_of_lex_info s)
    | B_parallelcomp(s) -> "B_parallelcomp: " ^ (string_of_lex_info s)
    | B_mapletorpair(s) -> "B_mapletorpair: " ^ (string_of_lex_info s)
    | B_domainrestrict(s) -> "B_domainrestrict: " ^ (string_of_lex_info s)
    | B_rangerestrict(s) -> "B_rangerestrict: " ^ (string_of_lex_info s)
    | B_domainsubstract(s) -> "B_domainsubstract: " ^ (string_of_lex_info s)
    | B_rangesubstract(s) -> "B_rangesubstract: " ^ (string_of_lex_info s)
    | B_override(s) -> "B_override: " ^ (string_of_lex_info s)
    | B_directprod(s) -> "B_directprod: " ^ (string_of_lex_info s)
    | B_prependseq(s) -> "B_prependseq: " ^ (string_of_lex_info s)
    | B_appendseq(s) -> "B_appendseq: " ^ (string_of_lex_info s)
    | B_prefixseq(s) -> "B_prefixseq: " ^ (string_of_lex_info s)
    | B_suffixseq(s) -> "B_suffixseq: " ^ (string_of_lex_info s)
    | B_natsetrange(s) -> "B_natsetrange: " ^ (string_of_lex_info s)
    | B_unionsets(s) -> "B_unionsets: " ^ (string_of_lex_info s)
    | B_intersectionsets(s) -> "B_intersectionsets: " ^ (string_of_lex_info s)
    | B_lambda(s) -> "B_lambda: " ^ (string_of_lex_info s)
    | B_emptyset(s) -> "B_emptyset: " ^ (string_of_lex_info s)
    | B_seqempty(s) -> "B_seqempty: " ^ (string_of_lex_info s)
    | B_point(s) -> "B_point: " ^ (string_of_lex_info s)
    | B_such(s) -> "B_such: " ^ (string_of_lex_info s)
    | B_forall(s) -> "B_forall: " ^ (string_of_lex_info s)
    | B_exists(s) -> "B_exists: " ^ (string_of_lex_info s)
    | B_double_equal(s) -> "B_double_equal: " ^ (string_of_lex_info s)
    | B_MAXINT(s) -> "B_MAXINT: " ^ (string_of_lex_info s)
    | B_MININT(s) -> "B_MININT: " ^ (string_of_lex_info s)
    | B_prjone(s) -> "B_prjone: " ^ (string_of_lex_info s)
    | B_prjtwo(s) -> "B_prjtwo: " ^ (string_of_lex_info s)
    | B_succ(s) -> "B_succ: " ^ (string_of_lex_info s)
    | B_pred(s) -> "B_pred: " ^ (string_of_lex_info s)
    | B_tree(s) -> "B_tree: " ^ (string_of_lex_info s)
    | B_btree(s) -> "B_btree: " ^ (string_of_lex_info s)
    | B_consconst(s) -> "B_consconst" ^ (string_of_lex_info s)
    | B_top(s) -> "B_top: " ^ (string_of_lex_info s)
    | B_sons(s) -> "B_sons: " ^ (string_of_lex_info s)
    | B_prefix(s) -> "B_prefix: " ^ (string_of_lex_info s)
    | B_postfix(s) -> "B_postfix: " ^ (string_of_lex_info s)
    | B_sizet(s) -> "B_sizet: " ^ (string_of_lex_info s)
    | B_mirror(s) -> "B_mirror: " ^ (string_of_lex_info s)
    | B_rank(s) -> "B_rank: " ^ (string_of_lex_info s)
    | B_father(s) -> "B_father: " ^ (string_of_lex_info s)
    | B_son(s) -> "B_son: " ^ (string_of_lex_info s)
    | B_subtree(s) -> "B_subtree: " ^ (string_of_lex_info s)
    | B_arity(s) -> "B_arity: " ^ (string_of_lex_info s)
    | B_bin(s) -> "B_bin" ^ (string_of_lex_info s)
    | B_left(s) -> "B_left: " ^ (string_of_lex_info s)
    | B_right(s) -> "B_right: " ^ (string_of_lex_info s)
    | B_infix(s) -> "B_infix: " ^ (string_of_lex_info s)
    | B_BOOL(s) -> "B_BOOL: " ^ (string_of_lex_info s)
    | B_STRINGS(s) -> "B_STRINGS: " ^ (string_of_lex_info s)


let string_of_lexemes lexemes =
  String.concat "\n"
    (List.map string_of_lexeme lexemes)


let lexeme_to_string lexeme =
  match lexeme with
    | B_formatting(s)
    | B_NUMBER(s)
    | B_IDENTIFIER(s)
    | B_MACHINE(s)
    | B_CONSTRAINTS(s)
    | B_SEES(s)
    | B_SETS(s)
    | B_CONSTANTS(s)
    | B_CONCRETE_CONSTANTS(s)
    | B_ABSTRACT_CONSTANTS(s)
    | B_VISIBLE_CONSTANTS(s)
    | B_HIDDEN_CONSTANTS(s)
    | B_PROPERTIES(s)
    | B_INCLUDES(s)
    | B_PROMOTES(s)
    | B_EXTENDS(s)
    | B_USES(s)
    | B_VARIABLES(s)
    | B_CONCRETE_VARIABLES(s)
    | B_ABSTRACT_VARIABLES(s)
    | B_VISIBLE_VARIABLES(s)
    | B_HIDDEN_VARIABLES(s)
    | B_INVARIANT(s)
    | B_INITIALISATION(s)
    | B_LOCAL_OPERATIONS(s)
    | B_OPERATIONS(s)
    | B_REFINEMENT(s)
    | B_REFINES(s)
    | B_IMPLEMENTATION(s)
    | B_IMPORTS(s)
    | B_DEFINITIONS(s)
    | B_VALUES(s)
    | B_ASSERTIONS(s)
    | B_or(s)
    | B_setminus(s)
    | B_not(s)
    | B_SKIP(s)
    | B_BEGIN(s)
    | B_PRE(s)
    | B_IF(s)
    | B_THEN(s)
    | B_ELSIF(s)
    | B_ELSE(s)
    | B_ASSERT(s)
    | B_ANY(s)
    | B_WHERE(s)
    | B_LET(s)
    | B_BE(s)
    | B_IN(s)
    | B_CHOICE(s)
    | B_OR(s)
    | B_SELECT(s)
    | B_WHEN(s)
    | B_CASE(s)
    | B_OF(s)
    | B_EITHER(s)
    | B_VAR(s)
    | B_WHILE(s)
    | B_DO(s)
    | B_VARIANT(s)
    | B_NAT(s)
    | B_NATONE(s)
    | B_NATURAL(s)
    | B_NATURALONE(s)
    | B_INT(s)
    | B_INTEGER(s)
    | B_false(s)
    | B_true(s)
    | B_POW(s)
    | B_FIN(s)
    | B_POWONE(s)
    | B_FINONE(s)
    | B_union(s)
    | B_inter(s)
    | B_UNION(s)
    | B_INTER(s)
    | B_id(s)
    | B_dom(s)
    | B_ran(s)
    | B_iterate(s)
    | B_closure(s)
    | B_closureone(s)
    | B_card(s)
    | B_max(s)
    | B_min(s)
    | B_mod(s)
    | B_SIGMA(s)
    | B_PI(s)
    | B_seq(s)
    | B_iseq(s)
    | B_seqone(s)
    | B_iseqone(s)
    | B_perm(s)
    | B_conc(s)
    | B_front(s)
    | B_tail(s)
    | B_first(s)
    | B_last(s)
    | B_size(s)
    | B_rev(s)
    | B_bool(s)
    | B_END(s)
    | B_struct(s)
    | B_rec(s)
    | B_quote(s)
    | B_minus(s)
    | B_tilde(s)
    | B_before(s)
    | B_plus(s)
    | B_power(s)
    | B_concatseq(s)
    | B_mul(s)
    | B_div(s)
    | B_equal(s)
    | B_conjunction(s)
    | B_less(s)
    | B_greater(s)
    | B_comma(s)
    | B_semicolon(s)
    | B_in(s)
    | B_setin(s)
    | B_equivalence(s)
    | B_subset(s)
    | B_notsubset(s)
    | B_implies(s)
    | B_strictsubset(s)
    | B_notstrictsubset(s)
    | B_notin(s)
    | B_notequal(s)
    | B_lessorequal(s)
    | B_greaterorequal(s)
    | B_relation(s)
    | B_rel(s)
    | B_fnc(s)
    | B_cartesianprod(s)
    | B_totalfunc(s)
    | B_partialfunc(s)
    | B_totalsurject(s)
    | B_partialsurject(s)
    | B_totalinject(s)
    | B_partialinject(s)
    | B_partialbij(s)
    | B_bijection(s)
    | B_operationsig(s)
    | B_simplesubst(s)
    | B_CURLYOPEN(s)
    | B_CURLYCLOSE(s)
    | B_PARENOPEN(s)
    | B_PARENCLOSE(s)
    | B_BRACKOPEN(s)
    | B_BRACKCLOSE(s)
    | B_parallelcomp(s)
    | B_mapletorpair(s)
    | B_domainrestrict(s)
    | B_rangerestrict(s)
    | B_domainsubstract(s)
    | B_rangesubstract(s)
    | B_override(s)
    | B_directprod(s)
    | B_prependseq(s)
    | B_appendseq(s)
    | B_prefixseq(s)
    | B_suffixseq(s)
    | B_natsetrange(s)
    | B_unionsets(s)
    | B_intersectionsets(s)
    | B_lambda(s)
    | B_emptyset(s)
    | B_seqempty(s)
    | B_point(s)
    | B_such(s)
    | B_forall(s)
    | B_exists(s)
    | B_double_equal(s)
    | B_MAXINT(s)
    | B_MININT(s)
    | B_prjone(s)
    | B_prjtwo(s)
    | B_succ(s)
    | B_pred(s)
    | B_tree(s)
    | B_btree(s)
    | B_consconst(s)
    | B_top(s)
    | B_sons(s)
    | B_prefix(s)
    | B_postfix(s)
    | B_sizet(s)
    | B_mirror(s)
    | B_rank(s)
    | B_father(s)
    | B_son(s)
    | B_subtree(s)
    | B_arity(s)
    | B_bin(s)
    | B_left(s)
    | B_right(s)
    | B_infix(s)
    | B_BOOL(s)
    | B_STRINGS(s) -> Blexemes.to_string s
    | EOF -> ""
    | B_comment(s) -> "/*" ^ (Blexemes.to_string s) ^ "*/"
    | B_STRING(s) -> "\"" ^ (Blexemes.to_string s) ^ "\""


let lexemes_to_string lexemes =
  String.concat ""
    (List.map lexeme_to_string lexemes)


(* This function consumes everything in the lexing buffer so as to
   produce a list of lexemes whose definitions can then be expanded *)
let lexemize lexbuf =
  let rec aux_parse acc =
    let new_token = token lexbuf in
    if new_token = EOF
    then List.rev acc
    else aux_parse (new_token::acc)
  in
  aux_parse []

let lexemize_chan chan = lexemize (Lexing.from_channel chan)

let lexemize_file file =
  let fh = open_in file in
  let lexemes = lexemize_chan fh in
  let () = close_in fh in
  lexemes

let lexemize_string str = lexemize (Lexing.from_string str)


let remove_comments lexemes =
  let rec aux_remove lexes acc =
    match lexes with
      | B_comment(_)::t -> aux_remove t acc
      | h::t -> aux_remove t (h::acc)
      | [] -> List.rev acc
  in
  aux_remove lexemes []


let remove_formatting lexemes =
  let rec aux_remove lexes acc =
    match lexes with
      | B_formatting(_)::t -> aux_remove t acc
      | h::t -> aux_remove t (h::acc)
      | [] -> List.rev acc
  in
  aux_remove lexemes []


(* Same as both above, but made into a single pass *)
let remove_meaningless lexemes =
  let rec aux_remove lexes acc =
    match lexes with
      | B_comment(_)::t -> aux_remove t acc
      | B_formatting(_)::t -> aux_remove t acc
      | h::t -> aux_remove t (h::acc)
      | [] -> List.rev acc
  in
  aux_remove lexemes []


(* This function builds a "false" Lexer from a list of lexemes so that
   the parser can be called upon it. This is needed by the somewhat
   old interface of ocamlyacc. It also returns an additional
   access_lexemes function that gives access to the array of lexemes :
   useful in case of a parser error so as to go a bit back
*)
let build_dummy_lexer_array lexemes =
  let dummy_lexbuf = Lexing.from_string "" in
  let my_lexemes = Array.of_list lexemes in
  let length = Array.length my_lexemes in
  let position = ref 0 in
  let next_token (dummy_lexbuf: Lexing.lexbuf) =
    if !position >= length then EOF
    else
      let l = my_lexemes.(!position) in
      let () = incr position in
      l
  in
  let access_lexemes () =
    (my_lexemes, !position)
  in
  (next_token, dummy_lexbuf, access_lexemes)


}
