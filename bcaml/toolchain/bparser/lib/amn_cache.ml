
(* $Id$ *)

(* AMN Cache management *)

(* type amn_cache = (string , int) Hashtbl.t *)

(* let repository = 
      ((Hashtbl.create 100) : ((string , int)  Hashtbl.t) *)


let repository = ((Hashtbl.create 50) : ((string, Blast.amn) Hashtbl.t));;



let already_in amn_file = Hashtbl.mem repository amn_file 

let add amn_file res =  Hashtbl.add repository  amn_file res

let find amn_file =  Hashtbl.find repository  amn_file 
