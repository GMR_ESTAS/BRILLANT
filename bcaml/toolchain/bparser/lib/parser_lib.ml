(* $Id$ *)

open Definitions

(* Miscellaneous functions related to parsing *)

(* open Amn_cache *)


let call_parser lexemes =
  let (next_token, dummy_lexbuf, access_lexemes) =
    Blexer.build_dummy_lexer_array lexemes
  in
  try
    Bparser.amn next_token dummy_lexbuf
  with
      err ->
	let (alexemes, pos) = access_lexemes () in
	let doable_length = min 5 ((Array.length alexemes) - (pos - 1)) in
	let got_lexemes = Array.sub alexemes (pos - 1) doable_length in
	let str = Blexer.string_of_lexemes (Array.to_list got_lexemes) in
	let () = print_endline str in
	raise err

let parse_channel chan=
  let lexemes = Blexer.remove_meaningless (Blexer.lexemize_chan chan) in
  call_parser lexemes

let parse_string str=
  let lexemes = Blexer.remove_meaningless (Blexer.lexemize_string str) in
  call_parser lexemes

(* TODO: add the machinery for standard (definition) inclusion paths *)
let parse_file filename =
  let local_dir = Filename.dirname filename in
  let lexemes = Blexer.remove_meaningless (Blexer.lexemize_file filename) in
  let expanded_lexemes = Definitions.expand local_dir [] lexemes in
  call_parser expanded_lexemes


let expand_file remove_comments filename =
  let local_dir = Filename.dirname filename in
  let lexemes =
    if remove_comments then Blexer.remove_comments (Blexer.lexemize_file filename)
    else Blexer.lexemize_file filename
  in
  let expanded_lexemes = Definitions.expand local_dir [] lexemes in
  Blexer.lexemes_to_string expanded_lexemes


let retrieve_ast_from filename =
  (* Error.debug ("Retrieving : " ^ filename);
     if Hashtbl.mem repository filename *)
  if Amn_cache.already_in filename
  then
    let _= Error.debug ("Found already parsed " ^ filename) in
      Amn_cache.find filename
  else
    try
      if Bfile.isBfile filename
      then
	let res = parse_file filename in
	let () = Amn_cache.add filename res in
	res
      else invalid_arg ("retrieve_ast_from: Not a B file : " ^ filename)
    with
	Not_found -> invalid_arg ("retrieve_ast_from: file not found : " ^ filename)
      | Blast.Parse_error(nb,errmsg) ->
	let (string_error,line,col) = Linenum.for_position filename nb in
	let () = Error.parsing_failure filename line col errmsg in
	invalid_arg ("retrieve_ast_from: parse error :" ^ errmsg)
      | Sys_error s ->
	( Error.warning ("retrieve_ast_from : "^s);
	  raise (Sys_error s) )
      | err ->
	( Error.warning "retrieve_ast_from : error";
	  raise err )


(*
  let machineStock = Hashtbl.create 50
  
  
  let retrieve_machines dirs filename=
  
  let stock=ref [] in
  let newdirs=(Filename.dirname filename)::dirs in
  let newmchname=Filename.chop_extension (Filename.basename filename) in
  
  let rec aux_retrieve_machines dirs mchname=
  if List.mem_assoc mchname !stock
  then ()
  else
  let topmachine=
  if Hashtbl.mem machineStock mchname 
  then Hashtbl.find machineStock mchname 
  else 
  begin
  let retrmch=retrieve_ast_from (Bfile.find_filename dirs mchname) in
  Hashtbl.add machineStock mchname retrmch;
  retrmch
  end
  in
  
  let includedids=Blast_acc.get_included_mchids topmachine in
  let usedids=Blast_acc.get_used_mchids topmachine in
  let seenids=Blast_acc.get_seen_mchids topmachine in
  let importedids=Blast_acc.get_imported_mchids topmachine in
  let extendedids=Blast_acc.get_extended_mchids topmachine in
      let refinedid=
  if Blast_acc.is_abstract_machine topmachine then None 
  else Some(Blast_acc.get_refined_name topmachine)
  in
  
  let modsnames=List.map
  Id_acc.name_without_prefix
  (includedids@usedids@seenids@importedids@extendedids)
  in
  let allnames=
  match refinedid with
  | None -> modsnames
  | Some(refid) -> (Id_acc.name_without_prefix refid)::modsnames
  in
  begin
  stock:=(mchname, topmachine)::!stock;
  List.iter (aux_retrieve_machines dirs) allnames
  end
  in
  begin
  aux_retrieve_machines newdirs newmchname;
  snd (List.split !stock)
  end
*)
	  
	  
let retrieve_all_machines dir=
  let allFiles=Bfile.listOfDirectory dir in
  let fullPathFiles=
    List.map (fun s -> dir ^ "/" ^ s) 
      allFiles in
  let goodFiles=
    List.filter 
      Bfile.isBfile 
      fullPathFiles in
    List.map retrieve_ast_from goodFiles
      
      
let getNeededNames machine=
  let includedids=Blast_acc.get_included_mchids machine in
  let usedids=Blast_acc.get_used_mchids machine in
  let seenids=Blast_acc.get_seen_mchids machine in
  let importedids=Blast_acc.get_imported_mchids machine in
  let extendedids=Blast_acc.get_extended_mchids machine in
  let refinedid=
    if Blast_acc.is_abstract_machine machine then None 
    else Some(Blast_acc.get_refined_name machine)
  in
    
  let modsnames=
    List.map
      Id_acc.name_without_prefix
      (includedids@usedids@seenids@importedids@extendedids)
  in
  let allnames=
    match refinedid with
      | None -> modsnames
      | Some(refid) -> (Id_acc.name_without_prefix refid)::modsnames
  in
    allnames
      
      
let overParse dirs filename=
  let newdirname=Filename.dirname filename in
  let newdirs=
    if 
      List.mem newdirname dirs 
    then 
      dirs
    else 
      (Filename.dirname filename)::dirs
  in
  let mchname=Filename.chop_extension (Filename.basename filename) in
    
  let overMachines=
    List.concat (List.map retrieve_all_machines newdirs) in
  let assocMachines=
    List.map 
      (fun m -> ((Id_acc.name_of (Blast_acc.get_name m)), m)) 
      overMachines 
  in 
  let startMachine=List.assoc mchname assocMachines in
    
  let rec getConnexPart connexMachines assocMachines=
    let neededMachines=
      List.fold_left 
	(fun list nameMch -> 
	   let thisNeeded=getNeededNames (snd nameMch) in
	   let notPresent=
	     List.filter 
	       (fun n -> not (List.mem_assoc n connexMachines)) 
	       thisNeeded 
	   in
	     notPresent@list
	)
	[]
	connexMachines
    in
    let gotNeededMachines=
      List.map 
	(fun x -> (x, (List.assoc x assocMachines)) ) 
	neededMachines in
    let refinements=
      List.filter 
	(fun (_, mch) -> not (Blast_acc.is_abstract_machine mch) ) 
	assocMachines 
    in
    let relevantRefinements=
      List.filter
	(fun (x, mch) -> 
	   let refinedName=Id_acc.name_of (Blast_acc.get_refined_name mch) in
	     List.mem_assoc 
	       refinedName connexMachines
	)
	refinements
    in
    let toRemove=neededMachines@(fst (List.split relevantRefinements)) in
      if toRemove=[]
	(* We have found all the refinements and the needed machines *)
      then connexMachines
      else 
	let shrinkedAssoc=
	  List.fold_left 
	    (fun l x -> List.remove_assoc x l) 
	    assocMachines 
	    toRemove 
	in
	let grownConnex=gotNeededMachines@relevantRefinements@connexMachines in
	  getConnexPart grownConnex shrinkedAssoc
  in
    
  let gotAllMachines=getConnexPart [(mchname, startMachine)] assocMachines in
    snd (List.split gotAllMachines)
      
