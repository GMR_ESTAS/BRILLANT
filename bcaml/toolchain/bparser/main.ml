
open Project

(*
   Variables <<d'environnements>> et fonctions utiles pour les options
*)

let output_directory =
  try
    ref (Sys.getenv "BCAML_DEST")
  with
    Not_found -> ref("./")


let to_stdout = ref true
let from_stdin = ref true
let verbose = ref false

let dest_set path =
  output_directory := path;
  to_stdout:=false

let bparser_version = "0.35"
let usage_message =
  "Bparser " ^ bparser_version
  ^ "\n If no file is given in the command line, assumes reading from stdin (except for some specific options, see --help)\n"


let filenames=ref []
let projectnames=ref []
let lexemization = ref false
let expansion = ref false
let remove_comments = ref false
let xml2 = ref false
let dotnames = ref []

let add_filename str=
  Error.debug ("Adding to filenames : " ^ str);
  from_stdin:=false;
  filenames:=str::(!filenames)

let add_projectname str=
  Error.debug ("Adding to projectnames : " ^ str);
  from_stdin:=false;
  projectnames:=str::(!projectnames)

let add_dotname str=
  Error.debug ("Adding to dotnames : " ^ str);
  from_stdin:=false;
  dotnames:=str::(!dotnames)


let rec args =
  [
   ("--help",
    Arg.Unit(fun () ->
      Arg.usage args usage_message; raise Exit),
    "\t\t\t display this help")
     ;
   ("-help",
    Arg.Unit(fun () ->
      Arg.usage args usage_message; raise Exit),
    "\t\t\t See --help")
     ;
   ("--dest",
    Arg.String(dest_set),
    "<dir>,  \t\t output the generated files in directory <dir> (deactivates output to stdout), \n\t\t\t\t and defaults to the current directory if other options activate it")
     ;
   ("--debug",
    Arg.Unit(Error.debug_set),
    ", \t\t\t debug mode (default to false)")
     ;
   ("--lexemize",
    Arg.Set(lexemization),
    ", \t\t\t debug mode (default to false)")
     ;
   ("--expand",
    Arg.Set(expansion),
    ", \t\t\t only expand the (definitions of) the component (default to false)")
     ;
   ("--remove_comments",
    Arg.Set(remove_comments),
    ", \t\t\t when expanding with --expand, remove comments (default to false)")
     ;
   ("--xml2",
    Arg.Set(xml2),
    ", \t\t\t use the xmlm-based library with the new xml format instead of the old one (default to false)")
     ;
   ("--verbose",
    Arg.Set(verbose),
    ", \t\t\t display statistical information")
     ;
    ("--project",
    Arg.String(add_projectname),
    "<filename>,  \t Parses and scopes <filename> and all machines it depends on (deactivates reading from stdin and output to stdout). \n\t\t\t\t Used for the debugging of the scoping phase")
     ;
    ("--dot",
    Arg.String(add_dotname),
    "<filename>,  \t Does the same as --project, but produces a dot file instead.")
     ;
]

let xml_output out ast =
  if !xml2 then Bxml2.output out ast
  else Bxml.to_chan out ast


let save_ast dir ast=
  let mchname=Id_acc.name_of (Blast_acc.get_name ast) in
  let filename=dir ^ "/" ^ mchname ^ "." ^ Bfile.xml_extension in
    Bfile.mkpath dir;
    let out=open_out filename in
      try
	xml_output out ast;
	close_out out
      with
	  err -> close_out out; raise err


let out_ast ast=
  if !to_stdout
  then xml_output stdout ast
  else
    save_ast !output_directory ast


let out_project proj=
  let projid=Project.get_project_id proj in
  let projname=Id_acc.name_of projid in
  let imchconts=Project.MachineSet.elements proj.machines in
  let machines=List.map (fun imc -> (snd imc).machine) imchconts in
  let save_path=(!output_directory) ^ "/" ^ projname in
    List.iter (save_ast save_path) machines

let dot_compatible id =
  let name = Id_acc.name_of id in
  String.map (fun c -> if c = '.' then '_' else c) name


let out_dot proj =
  let projid=Project.get_project_id proj in
  let projname=Id_acc.name_of projid in
  let dot_string = Bgraph.dot dot_compatible proj.graph in
  let filename = projname ^ ".dot" in
  let out = open_out filename in
    try
      output_string out dot_string
    with
	err -> close_out out; raise err

let parse_files ()=
  let (ast_list, project_list, dot_list)=
    if !from_stdin
    then ([(Parser_lib.parse_channel stdin)], [], [])
    else
      let allasts = List.map Parser_lib.retrieve_ast_from !filenames in

      let allprojgroups = List.map (Parser_lib.overParse []) !projectnames in
      let allprojects = List.map Project.buildProject allprojgroups in

      let alldotgroups = List.map (Parser_lib.overParse []) !dotnames in
      let alldots = List.map Project.buildProject alldotgroups in

	(allasts, allprojects, alldots)
  in
    List.iter out_ast ast_list;
    List.iter out_project project_list;
    List.iter out_dot dot_list

let lexemize filename = Blexer.lexemize_file filename

let lexemize_files () =
  let print_lexemes file =
    let lexemes = lexemize file in
    print_endline (Blexer.string_of_lexemes lexemes)
  in
  List.iter print_lexemes !filenames

let expand_files () =
  let expansions = List.map (Parser_lib.expand_file !remove_comments) !filenames in
  List.iter print_endline expansions

let do_parsing ()=
  if !expansion
  then
    expand_files ()
  else
    if !lexemization
    then lexemize_files ()
    else parse_files ()

let _ =
  try
    Arg.parse args add_filename usage_message;
    do_parsing ();
    print_string "\n";
    flush stdout
(* Gui.process  files_list *)
  with Exit -> ()
