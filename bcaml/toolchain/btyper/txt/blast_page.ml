(* $Id$ *)

open Page;;
open Blast;;
open Bbop;;

let str_and = " "^(bop2_ascii And)^" "

let rec flatten_and = function
    HBox[x1;Word a;x2] 
      when a=str_and -> 
	(flatten_and x1)@(flatten_and x2)
  | b -> [b]
      
module Blast_page = struct
  type amn_ty = page
  type head_ty = page
  type clause_ty = page list
  type def_ty = page
  type defBody_ty = page
  type set_ty = page
  type instance_ty = page
  type type_B_ty = page
  type field_ty = page
  type predicate_ty = page
  type operation_ty = page
  type expr_ty = page
  type exprRecords_ty = page
  type recordsItem_ty = page
  type exprSeq_ty = page
  type exprSet_ty = page
  type setName_ty = page
  type boolConstant_ty = page
  type number_ty = page
  type substitution_ty = page
  type ids_ty = bool -> page
  type origine_ty = string list
  type id_ty = bool -> page

  let mk_ids (_x1:id_ty list) =
    fun pr ->mk_VBox (List.map (fun x -> x pr) _x1)
      
  let mk_expr (_x1:expr_ty)=
    mkClause "Expr" _x1
      
  let mk_idList (_x1:expr_ty)=
    mkClause "IdList" _x1
      
  let mk_exprList (_x1:expr_ty list) = 
    mkClause "ExprList" 
      (mk_VBox 
	 (List.map (fun x -> mk_expr (mk_VBox [x])) _x1))
      
  (*YP  MRB  4 p 27 *)
  let mk_Predicate (_x1:expr_ty) = 
    mkClause "Predicate" _x1 
      
      
  (*YP MRB 7 p 109 *)
      
  let mkAMN kind (_x1:head_ty) (_x2:clause_ty list) = 
    mkClause "AMN" (mkClause kind  
		      (mk_VBox 
			 [_x1 ;mkClause 
			    "ClausesList" (mk_VBox (List.concat _x2))]))
      
      
  (* YP MRB 7.1 p 109*) 
  let mk_Machine  = mkAMN "MACHINE"
		      
  (* YP MRB 7.3 p 112 *) 
  let mk_Refinement  = mkAMN "REFINEMENT"
			 
  (*YP MRB 7.4 p 114*) 
  let mk_Implementation  = mkAMN "IMPLEMENTATION"
			     
  (*MG homog�n�iser ??*)  
  let mk_EmptyTree = mk_Word "EMPTY"

  (*YP MRB 7.2 p 111 *)		       
  let mk_head (_x1:id_ty) (_x2:ids_ty) = 
    let x2 = _x2 true in
       if ((x2 <> HBox [])&&(x2 <> VBox []))
       then mkClause "Head" (mk_VBox [_x1 false;  mkClause "HeadPar" x2 ])
       else mkClause "Head" (mk_HBox [_x1 false;x2])
  
  (*YP MRB 7.6 p 116*)
  let mk_Refines (_x1:id_ty) = 
    [mkClause "REFINES" (mk_VBox [_x1 false])]
      
  (*YP MRB 2.3 p 8 *)
  let mk_Definitions (_x1:def_ty list) = 
    [mkClause "DEFINITIONS" (vbox_list "" "" ";" _x1)]
      
  (*YP MRB 7.5 p 116 *)  
  let mk_Constraints (_x1:expr_ty) = 
    [mkClause "CONSTRAINTS" 
       ( mk_Predicate (mk_VBox (flatten_and _x1)))]
      
  (*YP MRB 7.20 p 156 *)
  let mk_Invariant (_x1:expr_ty) = 
    [mkClause "INVARIANT" 
       (mk_Predicate (mk_VBox(flatten_and _x1)))]
  
  (*YP MRB 7.13 p 135 *)
  let mk_Sets (_x1:set_ty list) = 
    [mkClause "SETS" (mk_HBox (_x1))]
      
  (*YP MRB 7.22 p 161 *)
  let mk_Initialisation (_x1:substitution_ty) = 
    [mkClause "INITIALISATION" ( mk_VBox [_x1])]
      
  (*YP MRB 7.14 p 138 *)
  let mk_ConstantsConcrete (_x1:ids_ty) = 
    [mkClause "CCONSTANTS" (mk_idList (mk_VBox [_x1 true]))]

  (*YP MRB 7.15 p 140 *)
  let mk_ConstantsAbstract (_x1:ids_ty) = 
    [mkClause "ACONSTANTS" (mk_idList (mk_VBox [_x1 true]))] 
      
  let mk_ConstantsHidden (_x1:ids_ty) = 
    [mkClause "HCONSTANTS" (mk_idList (mk_VBox [_x1 true]))]
      
  (*YP MRB 7.16 p 142 *)
  let mk_Properties (_x1:expr_ty) = 
    [mkClause "PROPERTIES" (mk_Predicate (mk_VBox (flatten_and _x1)))]
      
  (*YP MRB 7.17 p 145 *)
  let mk_Values (_x1:(id_ty * expr_ty) list) = 
    [ mkClause "VALUES" 
	 (
	  mkClause "Valuation" (mk_VBox 
	    (List.map (fun (i,e) -> mk_HBox [i false;e]) _x1)))]
    
  (*YP MRB 7.18 p 152 *)
  let mk_VariablesConcrete (_x1:ids_ty) = 
    [mkClause "CVARIABLES"  (mk_idList (mk_VBox [_x1 true]))]
    
  (*YP MRB 7.19 p 154 *)
  let mk_VariablesAbstract (_x1:ids_ty) = 
    [mkClause "AVARIABLES"  (mk_idList(mk_VBox [_x1 true]))] 
    
  (*YP MRB ??? *)  
  let mk_VariablesHidden (_x1:ids_ty) =  
    [mkClause "HVARIABLES"  (mk_idList(mk_VBox [_x1 true]))]
    
  (*YP MRB 7.10 p 130 *)
  let mk_Promotes (_x1:ids_ty) =  
    [mkClause "PROMOTES" (mk_VBox [_x1 true])]
    
    
  (*YP MRB 7.21 p 160 *)
  let mk_Assertions (_x1:expr_ty list) = 
    [mkClause "ASSERTIONS" (mk_Predicate (mk_VBox _x1))]
    
  (*YP MRB 7.23 p 164 *)
  let mk_Operations (_x1:operation_ty list) = 
    [mkClause "OPERATIONS" (mk_VBox _x1)]
    
  (*YP MRB 7.8 p 122 *) 
  let mk_Sees (_x1:ids_ty) =  
   [mkClause "SEES" (mk_VBox [_x1 true])]
   
 (*YP MRB 7.12 p 133 *)
  let mk_Uses (_x1:ids_ty) =  
    [mkClause "USE" (mk_VBox [_x1 true])]
    
    
  (*YP MRB 7.11 p 132 *)
  let mk_Extends (_x1:instance_ty list) = 
    [mkClause "EXTENDS" (mk_VBox [mk_VBox _x1])]
    
  (*YP MRB 7.9 p 126 *)
  let mk_Includes (_x1:instance_ty list) = 
     [mkClause "INCLUDES" (mk_VBox [mk_VBox _x1])]
    
  (*YP MRB 7.7 p 118 *)
  let mk_Imports (_x1:instance_ty list) = 
      [mkClause "IMPORTS" (mk_VBox [mk_VBox _x1])]

  (*YP MRB 2.3 p 8 *)
  let mk_Def (_x1:id_ty) (_x2:ids_ty) (_x3:defBody_ty) = 
    mkClause "DEFINITION" 
      (mk_HBox[_x1 false;hbox_paren "(" ")" (_x2 true); mk_Word " == ";_x3])
      
      
  let mk_FileDef (_x1:string) = 
    mk_HBox [ mk_Word "FILEDEF" ; mk_Word _x1] 
   
  let mk_ExprDefBody (_x1:expr_ty) = _x1
				       
  let mk_SubstDefBody (_x1:substitution_ty) = _x1
						
  (*YP MRB 7.13 p135  les ensembles (manipulation) *)
  let mk_SetAbstDec (_x1:id_ty) = _x1 false

  let mk_SetEnumDec (_x1:id_ty) (_x2:ids_ty) = 
     mkClause "SetEnumDec" (mk_VBox [_x1 false;mk_idList (_x2 false)])
    
  let mk_SetRecordsDec (_x1:id_ty) (_x2:recordsItem_ty list) = 
    mk_HBox [_x1 false; mk_Word " = "; mk_Word "struct" ; vbox_list "(" ")" ", " _x2]

  let mk_instance (_x1:id_ty) (_x2:expr_ty list) = 
     mkClause "instance" (mk_VBox [_x1 false;mk_exprList _x2])




  (*YP MRB 5.6 p 45 *)
  let mk_SetEmpty =
     mk_Word "<SetEmpty/>" 
      
  (*YP MRB 5.7 p 46 *)
  let mk_TypeNatSetRange (_x1:number_ty) (_x2:number_ty) = 
     mkClause "NatSetRange" (mk_VBox [mk_expr _x1;mk_expr _x2])

      
  (*YP MRB 5.8 p 48 Union quantifi�e*)
  let mk_SetUnionQuantify (_x1:id_ty list) (_x2:expr_ty) (_x3:expr_ty) = 
     mkClause "UnionQ"
     ( mk_HBox [
	mk_ids _x1 true; 
	_x2;
	_x3; 
      ])

  (*YP MRB 5.8 p 48 Intersection quantifi�e*)
  let mk_SetInterQuantify (_x1:id_ty list) (_x2:expr_ty) (_x3:expr_ty) = 
      mkClause "InterQ"
     ( mk_HBox [
	mk_ids _x1 true; 
	_x2; 
	_x3; 
      ])
    let mk_MinNumber = mk_Word "MININT"

  let mk_Number (_x1:int32) = mk_Word (Int32.to_string _x1)

  let mk_MaxNumber = mk_Word "MAXINT"

  (* Le langage de types *)

  let mk_PredType (_x1:expr_ty) = _x1 
				    
  (*YP MRB 5.9 p 51 *)
  (*VS
    let mk_RecordsType (_x1:field_ty list) = 
    mk_HBox [mk_Word "struct" ; vbox_list "(" ")" ", " _x1]
  *)
      
  let mk_TypeIdentifier (_x1:id_ty) = _x1 false
					
  let mk_PFunType (_x1:type_B_ty) (_x2:type_B_ty) = 
      mkClause "PFunType" (mk_HBox[_x1;_x2])
      
  let mk_FunType (_x1:type_B_ty) (_x2:type_B_ty) =
    mkClause "FunType" (mk_VBox[_x1; _x2])
      
  let mk_SubType (_x1:type_B_ty) (_x2:expr_ty) = 
    (*VS
      mk_HBox[ mk_Word "{"; _x1; mk_Word " | "; _x2;  mk_Word "}"]
    *)
    mkClause "SubType" (mk_VBox[_x1; _x2])
      
  let mk_ProdType (_x1:type_B_ty) (_x2:type_B_ty) = 
    (*VS
      mk_HBox [_x1; mk_Word " * "; _x2]
    *)
    mkClause "ProdType" (mk_VBox[_x1; _x2])
      
  (*YP MRB 5.17 p 67*)
  let mk_Sequence (_x1:type_B_ty)  = 
    mkClause "Seq" (mk_HBox [mk_expr _x1 ])
      
  (*YP MRB 5.7 p 46*)   
  let mk_Fin (_x1:type_B_ty) = 
    mkClause "Fin"  (mk_expr _x1)
      
  let mk_Untyped = mk_Word "UNTYPED"
		     
  let mk_field (_x1:id_ty) (_x2:type_B_ty) = 
    mk_HBox [_x1 false; mk_Word "'"; _x2]
      
  (* Les pr�dicats *)
  (*YP  MRB  4.1 p 28 *)
  let mk_PredParen (_x1:expr_ty) = 
    mkClause "PredParen" _x1
      
      
      
  (*YP  MRB  4.1 p 28 *)
  let mk_PredNegation (_x1:expr_ty) = 
    mkClause "Neg" _x1 
      
  (*YP  MRB  4.2 p 29 *)
  let mk_PredExists (_x1:id_ty list) (_x2:expr_ty) = 
    mkClause "Exists" 
      (mk_HBox [mk_idList (mk_ids _x1 true);mk_Predicate _x2])
      
  (*YP  MRB  4.2 p 29 *)
  let mk_PredForAll (_x1:id_ty list) (_x2:expr_ty) = 
    mkClause "ForAll" 
      (mk_HBox [mk_idList (mk_ids _x1 true);mk_Predicate _x2])
      
      
  (*YP  MRB  4.1 --> 4.6 op�rateurs d�finis dans blast/Bbop.ml *)  
  let mk_PredBin (_x1:Bbop.bop2) (_x2:predicate_ty) (_x3:predicate_ty) = 
      mkClause (bop2_name _x1) (mk_VBox [_x2;_x3])
      

  let mk_PredAtom (_x1:Bbop.bop2) (_x2:expr_ty) (_x3:expr_ty) = 
    mkClause (bop2_name _x1) (mk_VBox [_x2;_x3])
      
  (*YP MRB 7.23 p 164 *)
  let mk_operation (_x1:id_ty) (_x2:ids_ty) (_x3:ids_ty) (_x4:substitution_ty) 
    = 
    let x2 = _x2 true in
    let x3 = _x3 true in  
      mkClause "Operation" 
	(mk_VBox [
	   (mkClause "OperHead" 
	      (mk_VBox[(mkClause "OperName" (_x1 false));
		       (if 
			  ((x2<>HBox [])&&(x2<>VBox [])) 
			then					
			  (mkClause "OperOut" (mk_idList x2)) 
			else 
			  VBox []);
		       (if ((x3<>HBox [])&&(x3<>VBox [])) 
			then
			  (mkClause "OperIn" (mk_idList x3)) 
			else 
			  VBox [])]));
	   mkClause"OperBody" 
	     (mk_VBox [_x4])])
	
  let mk_ExprSequence (_x1:expr_ty) (_x2:expr_ty) = 
     mkClause "ExprSequence"  (mk_VBox [mk_expr _x1; mk_expr _x2])
      
  let mk_ExprParen (_x1:expr_ty) = 
    mkClause "ExprParen" _x1 
 	  
  let mk_ExprId (_x1:id_ty) = _x1 false


  (*YP MRB 5.16 p 64*)
  let mk_ExprFunCall (_x1:expr_ty) (_x2:expr_ty list) = 
       mkClause "ExprFunCall" ( mk_VBox [mk_expr _x1;mk_exprList _x2])

  (* YP MRB 5  expressions artihm�tiques  
     (op�rateurs d�finis dans blast/Bbop.ml) p 39 
  *)

  (* Op�rateurs binaires *)
  let mk_ExprBin (_x1:Bbop.op2) (_x2:expr_ty) (_x3:expr_ty) = 
      match _x1 with
      | Bbop.Image -> 
	  mkClause "Image" (mk_VBox[mk_expr _x2;mk_expr _x3])
      | Bbop.Prj1 
      | Bbop.Prj2 -> 
	  mkClause (op2_name _x1) (
	    mk_VBox[mk_expr _x2;mk_expr _x3])
      | _ -> 
	  mkClause (op2_name _x1) (
	    mk_VBox[mk_expr _x2;mk_expr _x3]) 
	    
  (* Op�rateurs Unaires *)
  let mk_ExprUn (_x1:Bbop.op1) (_x2:expr_ty) = 
     if 
      _x1 = Bbop.Tilde 
      || _x1 = Bbop.Closure 
      || _x1 = Bbop.ClosureOne 
    then 
     mkClause (op1_name _x1) (mk_HBox[mk_expr _x2 ])
    else 
      if _x1 = Bbop.UMinus 
      then
	mkClause (op1_name _x1) (mk_VBox[mk_expr _x2])
      else 
	mkClause (op1_name _x1) (mk_VBox[mk_expr _x2])
   
    
  (* MRB 5.16*)
  let mk_ExprLambda (_x1:id_ty list) (_x2:expr_ty) (_x3:expr_ty) = 
      mkClause "ExprLambda" (
      mk_HBox [
	mk_ids _x1 true; 
	mk_Predicate (_x2); mk_expr _x3
      ])
      
  let mk_ExprSeq (_x1:exprSeq_ty) = mkClause "Seq" _x1
				      
				      
  (*YP MRB  5.7 p 53  sous r�serves*)
  let mk_ExprSet (_x1:exprSet_ty) =  _x1
				       
  (*YP MRB  5.10 p 53 *)
  let mk_RelSet (_x1:expr_ty) (_x2:expr_ty) = 
     mkClause "RelSet" (mk_HBox[mk_expr _x1; mk_expr _x2])
      
  let mk_RelSeqComp (_x1:expr_ty list) =  
(*VS
    hbox_list "(" ")" ";" _x1
*)
    mkClause "RelSeqComp" (mk_VBox[ mk_exprList _x1 ])
 
  let mk_ExprNumber (_x1:number_ty) = 
    mkBalise "Number" _x1
      
  let mk_ExprBool (_x1:bool ) = 
    mkBalise "ExprBool" (mk_Word (if _x1 then "true" else "false"))
      
  let mk_ExprString (_x1:string) = 
    mk_Word _x1 
      
      
  let mk_ExprRecords (_x1:exprRecords_ty) = _x1
					      
  let mk_ExprPred (_x1:predicate_ty) = 
    mkClause "BoolEvaluation" (mk_Predicate (mk_HBox [_x1]))
      
  let mk_ExprNuplet (_x1:expr_ty list) = 
    mkClause "Maplet" (mk_VBox ( List.map (fun x -> mk_expr (mk_VBox [x])) _x1 ))
      
  (* YP MRB 5.4 p 41 *)
  let mk_ExprSIGMA (_x1:id_ty list) (_x2:expr_ty) (_x3:expr_ty) = 
    mkClause "SIGMA"
      (mk_HBox [
	 mk_ids _x1 true;
	 mk_Predicate _x2;
	 mk_expr _x3;
      ])

  (* YP MRB 5.4 p 41 *)
  let mk_ExprPI (_x1:id_ty list) (_x2:expr_ty) (_x3:expr_ty) =
    mkClause "PI"
      (mk_HBox [
	 mk_ids _x1 true;
	 mk_Predicate _x2;mk_expr _x3;
       ])
      
  (*MG 
  let mk_RecordsWithField (_x1:recordsItem_ty list) = 
    mk_HBox[mk_Word "rec" ; hbox_list "(" ")" ", " _x1]
      
  let mk_RecordsSet (_x1:recordsItem_ty list) = 
    mk_HBox[mk_Word "struct" ; hbox_list "(" ")" ", " _x1]
      
  let mk_Records (_x1:expr_ty list) = 
    mk_HBox[mk_Word "rec" ; mk_Word "(";mk_exprList _x1;mk_Word ")"]
      
  let mk_RecordsAccess (_x1:expr_ty) (_x2:id_ty) = 
    mk_HBox [_x1; mk_Word "'"; _x2 false]
      
  let mk_recordsItem (_x1:id_ty) (_x2:expr_ty) = 
    mk_HBox [_x1 false; mk_Word ":"; _x2]
*)
  (* *)
  (* YP MRB 5.17 p 67*)
  let mk_SeqEnum (_x1:expr_ty list) = 
    mkClause "SeqEnum"( mk_HBox [mk_exprList _x1])
      
      
  let mk_SeqEmpty = mk_Word "SeqEmpty"
		      
  let mk_SetRange (_x1:expr_ty) (_x2:expr_ty) = 
    mkClause "SetRange" (mk_HBox [_x1; _x2])
      
  let mk_SetEnum (_x1:expr_ty list) = mk_exprList _x1
					
  let mk_SetCompr (_x1:expr_ty list) = 
    (*YP  mk_HBox [mk_Word "{"; mk_exprList _x1; mk_Word "}"] YP*)
    mkClause "SetCompr" (mk_VBox (List.map (fun x -> mk_expr x) _x1))
      
  let mk_SetComprPred (_x1:id_ty list) (_x2:expr_ty) = 
    mk_HBox[mk_Word "{"; mk_ids _x1 true;  mk_Word "|" ; _x2 ; mk_Word "}"]
      
  (*YP MRB 6.16 p 102*)
  let mk_SubstOperCall (_x1:ids_ty) (_x2:id_ty) (_x3:expr_ty list) = 
    (*YP 
      la Balise  OutParam correspond au(x) param�tre(s) de sorties de la fonction, la balise OperCallName correspond au nom de fonction appel�e et OperCallIn correspond au(x) param�tres d'entr�es *)
    mkClause "OperCall" 
      (mk_HBox[
	 mk_idList (_x1 false); 
	 mk_expr (_x2 false); 
	 mk_exprList _x3
       ])
      (*MG mort/redondant
	let mk_Substitution (_x1:substitution_ty) =
      (*YP   mkClause "Substitution" _x1 *)
	mkClause "Substitution"_x1	
      *)

  (*YP MRB 6.1 p 84 *)
  let mk_SubstBlock (_x1:substitution_ty) =
    mkClause "SubstBlock" _x1
      
  (*YP MRB 6.4 p 88 *)
  let mk_SubstPrecondition (_x1:expr_ty) (_x2:substitution_ty) =
    mkClause "SubstPre" 
      (mk_VBox [mkClause "PRE" (
		  mk_Predicate _x1);
		mkClause "THEN" ( _x2); 
	       ])
      (*MG 
	SubsPrecondition == Precondition(Predicate,Substitution)
      *)
      
  (* YP  MRB 6.5 p 89 *)
  let mk_SubstAssertion (_x1:expr_ty) (_x2:substitution_ty) = 

    mkClause "SubstAssert" 
      (mk_VBox [ 
	 mk_Predicate _x1;
	 mk_VBox [_x2];
       ])
      
  (* YP  MRB 6.6 p 90 *)   
  let mk_SubstChoice (_x1:substitution_ty list) = 
    mkClause "SubstChoice" 
      (mk_VBox (_x1))
      
      
      
  (* YP  MRB 6.7 p 91 *)
  let mk_SubstIf (_x1:(expr_ty * substitution_ty) list) (_x2:substitution_ty option) = 
    let box_if = 
      List.concat 
	(List.map 
	   (fun (c,a) ->  
	      [mkClause "IfThen" 
		 (mk_VBox (
		    [mk_Predicate (mk_VBox [c]) ; 
		     mk_VBox[a]]))]) _x1) in
      match _x2 with
	  Some s -> 
	    mkClause "SubstIf" (mk_VBox (box_if@[mk_VBox [s]]))
	| None -> 
	    mkClause "SubstIf" (mk_VBox (box_if))
	      
	      
  (* YP MRB 6.8 p 93 *)
  let mk_SubstSelect 
    (_x1:(expr_ty * substitution_ty) list) 
    (_x2:substitution_ty option) = 
     let box_WhenPart = 
      List.map 
	(fun (c,a) -> 
	   mkClause "WhenPart" 
	   (mk_VBox [mk_VBox [mk_Predicate c] ; 
		     mk_VBox[a]])) _x1 in
      match _x2 with
	  Some s -> 
	    mkClause "SubstSelect" 
	    (mk_VBox (box_WhenPart@[mk_VBox [s]]))
	| None -> 
	    mkClause "SubstSelect" (mk_VBox box_WhenPart)
	      
	      
	      
  let mk_SubstCase (_x1:expr_ty) 
    (_x2:(expr_ty list * substitution_ty) list) 
    (_x3:substitution_ty option) =
    
    let box_elsifs =
      [mkClause "OrPartList" 
	 (mk_VBox (
	    List.concat 
		     (List.map 
			(fun (c,a) ->
			   [mkClause "OrPart" 
			      (mk_VBox 
				 [mk_VBox 
				    (List.map (fun e ->mk_expr e) c) ;mk_VBox[a]])]) _x2)))] 
    in
    let box_cases = 
      match _x3 with
	  Some s -> mk_VBox (box_elsifs@[mk_VBox [s]])
	| None -> mk_VBox (box_elsifs) in
      mkClause "SubstCase" 
	(mk_VBox [ mk_HBox [mkClause"Expr" _x1];
		   mk_VBox [box_cases]])
	
	
  (* YP MRB 6.10 p 96 *)  
  let mk_SubstAny 
    (_x1:id_ty list) (_x2:expr_ty) (_x3:substitution_ty) = 
    
    mkClause "SubstAny" 
      (mk_VBox 
	 [mk_idList 
	    (mk_VBox [mk_ids _x1 true]); 
	  mk_Predicate
	    ( mk_VBox [_x2]);
	  mk_VBox [_x3];
	 ] )
      
  let mk_SubstLet (_x1:id_ty list) (_x2:predicate_ty) (_x3:substitution_ty) = 
    mkClause "SubstLet" 
      (mk_VBox 
	 [mk_idList 
	    (mk_VBox [mk_ids _x1 true]); 
	  mk_Predicate
	    ( mk_VBox [_x2]);
	  mk_VBox [_x3];
	 ] )
      
  (* YP MRB 6.14 p 100 *)
  let mk_SubstVar (_x1:id_ty list) (_x2:substitution_ty) =  
     mkClause "SubstVar"
      (mk_VBox [ mk_HBox[mk_idList (mk_ids _x1 true)];
		 mk_VBox [_x2]
	       ])
      
  (*YP MRB 6.17 p 104 *)
  let mk_SubstWhile (_x1:expr_ty) (_x2:substitution_ty) (_x3:expr_ty) (_x4:predicate_ty) = 
      mkClause "SubstWhile" 
      (mk_VBox 
	 [mk_HBox 
	    [ mkClause "WhileTest" (mk_Predicate _x1)];
	  mkClause "WhileBody"  (mk_VBox [_x2]);
	  mkClause "WhileInvariant" (mk_Predicate _x4) ;
	  mkClause "WhileVariant" (mk_expr _x3) 
	 ])
	  

  (* YP MRB 6.2 p 85 *) 
  let mk_SubstSkip = 
    mk_Word "<SubstSkip/>" 
      
  (* YP MRB 6.15 p 101 *) 
  let mk_SubstSequence (_x1:substitution_ty) (_x2:substitution_ty) =
    mkClause "SubstSeq" (mk_VBox[_x1;_x2])
      
  (* YP MRB 6.18 p 106 *)  
  let mk_SubstParallel (_x1:substitution_ty) (_x2:substitution_ty) =
    mkClause "SubstPar" (mk_VBox[_x1;_x2])
      
  (*YP MRB 6.3 p 87 *)
  let mk_SubstSetEqualIds (_x1:ids_ty) (_x2:expr_ty list) = 
    mkClause "SetEqualIds"   
      (mk_VBox [mk_idList (_x1 false); mk_exprList _x2 ])
      
      
  let mk_SubstSetEqualFun (_x1:expr_ty) (_x2:expr_ty list) (_x3:expr_ty) = 
    mkClause "SetEqualFun" 
      (mk_VBox [mk_ExprFunCall _x1 _x2; mk_expr _x3])
      
  let mk_SubstSetEqualRecords (_x1:exprRecords_ty) (_x2:expr_ty) = 
    mkClause "SetEqualRecords" 
      (mk_HBox[_x1;mk_expr _x2])
      
  (*YP MRB 6.13 p 99*)
  let mk_SubstBecomeSuch (_x1:ids_ty) (_x2:expr_ty) = 
    mkClause "SubstBecomeSuch" 
      (mk_HBox[mk_idList (_x1 false); mk_Predicate _x2])
      
      
  (*YP MRB 6.12 p 98 *)
  let mk_SubstSetIn (_x1:ids_ty) (_x2:expr_ty) =
    mkClause "SubstSetIn" (mk_HBox[mk_idList(_x1 false); mk_expr _x2])
      
  let mk_Local = []
		   
  let mk_Remote (_x1:(string * string) list) =
    List.filter (fun x -> x <> "") (List.map fst _x1)
      
  let mk_OrgRefine (_x1:string) (_x2:(string * string) list) = 
    List.filter (fun x -> x <> "") (List.map fst _x2)
      
  let mk_Prefix (_x1:string list) = 
    List.filter (fun x -> x <> "") _x1
      
  let mkTypeB (ty:type_B_ty)  =
    mkClause "BType" ty
      
  let mk_Id (prefixe: origine_ty) (n:string) (t:type_B_ty option) k = 
    fun pr -> 
      let fullname =  (mk_Word(String.concat "." (prefixe@[n]))) in
      match t with
	  Some ty ->
	    (if  pr || !Options.prall 
	     then 
	       (mkClause "IdTyped" 
		  (mk_VBox 
		     [(mkBalise "Id" fullname
		      )
		     ;
		      (mkTypeB ty)
		     ]
		  )
	       )
	     else 
	       (mk_VBox 
		  ((mkBalise "Id" fullname ::[]))))
	    
	| None -> 
	    mkBalise "Id" fullname
end
  
module TXT = Blast_it.Blast_it(Blast_page)
  
let blast_page m =
  TXT.it_amn m




