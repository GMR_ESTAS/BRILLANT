(* $Id *)

open Blast   ;;        
open Unix    ;;
open Bparser ;;

let date = 
 let tm = Unix.localtime (time ()) in
 let hh = Printf.sprintf "%02d" tm.tm_hour in
 let mm = Printf.sprintf "%02d" tm.tm_min in
 let ss = Printf.sprintf "%02d" tm.tm_sec in
 let j = Printf.sprintf "%02d" tm.tm_mday in
 let m = Printf.sprintf "%02d" ( 1 + tm.tm_mon) in
 let a = string_of_int (1900 + tm.tm_year) in
 j^"-"^m^"-"^a^"  "^hh^":"^mm^":"^ss
;;


let entete_ml = 
"(* BCaml/btype IRIT                                          *)\n"^
"(* source genere le "     ^date^     "                     *)\n"


let entete_tex = Str.global_replace (Str.regexp "^.") "%0" entete_ml

let sources = ref ["xml.ml"] 

let print n suffixe s =
  let f = if suffixe <> "" then n^"."^suffixe else n in
  let entete = 
    if suffixe = "ml" 
    then entete_ml 
    else if suffixe = "tex" 
    then entete_tex 
    else if suffixe = "sty" 
    then entete_tex 
    else "" in
  try
    (try Unix.rename f (f^".back") with _ -> ());
    let fd = open_out f in
      output_string fd (entete^s);
      close_out fd;
      chmod f 0o444;
      if f <> "Makefile" 
      then sources := (!sources @ [f]);
  with Sys_error s -> prerr_string ("error: " ^ s ^ "\n")
    
    
    
exception Not_B_file of string
exception Stop
  
let mylexer = ref  Blexer.token;;

let parsing_success file =
  Error.message ("\n --Bhappy "^"<"^file^">"^ " parsed");
;;

let parsing_failure file line col =
  Error.message ("\n --Bwarn   "
                 ^"<"^ file ^">"
                 ^"Parse error at line "
                 ^(string_of_int line)
                 ^" at column "
                 ^(string_of_int col)) ;
  EmptyTree
;;

let parse_in_channel str cin =
  try
    let lexbuf = Lexing.from_channel cin      in
    let res = Bparser.amn !mylexer lexbuf     in
      (close_in cin ; (parsing_success str) ;  res )
  with Error.Parse_error(nb,_) -> 
    (* Echec parsing *)
    let (string_error,line,col) = Linenum.for_position str nb  in
      close_in cin; (parsing_failure str line col) 
    |     _ -> close_in cin
            ; raise Parsing.Parse_error
;;

let parse_string str =
  try  
    let lexbuf = Lexing.from_string str in
      Bparser.amn !mylexer lexbuf
  with _ -> raise (Invalid_argument "analyse")
;;

let analyse str = 
  try  
    let cin = open_in str
    in
    let res = parse_in_channel str cin in 
      (str,res)
  with Sys_error s -> ("",parse_string str)
    | Parsing.Parse_error -> 
	Error.message ("\n --Bwarn "^"<"^str^">"^" not parsed");
	(str,Blast.EmptyTree)
    | Not_B_file str -> (str,Blast.EmptyTree)
;;

(*
let pretty_txt f = 
  let (m,ast) = analyse f in
  let bf = Filename.chop_extension f in
    (try 
       Unix.rename (bf ^ ".txt") (bf^".txt.back") 
     with _ -> ()
    );
    let fd = open_out (bf ^ ".txt") in
      Format.set_formatter_out_channel fd;
      Blast_txt.page_txt Format.std_formatter (Blast_page.TXT.it_amn ast);
      Format.print_newline ();
      Format.print_flush ();
      close_out fd
;;
*)
let ast2file ast file = 
  let fd = open_out file in
    Format.set_formatter_out_channel (fd);
    Blast_txt.page_txt Format.std_formatter (Blast_page.TXT.it_amn ast);
    Format.print_newline ();
    Format.print_flush ();
    close_out fd
;;

let expr2file ast file = 
  let fd = open_out file in
    Format.set_formatter_out_channel (fd);
    Blast_txt.page_txt Format.std_formatter (Blast_page.TXT.it_expr ast);
    Format.print_newline ();
    Format.print_flush ();
    close_out fd
;;
