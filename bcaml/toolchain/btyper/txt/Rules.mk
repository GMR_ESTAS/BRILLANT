
FILES = page.ml \
 blast_page.ml \
 blast_txt.ml \
 pretty_txt.ml

DEPS = Btyperlib Btyperblast Btyperparser

NAME = Btypertxt
MAKELIB = yes
