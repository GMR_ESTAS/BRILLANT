(*************************************************************)
(*                PROJET BCAML                               *)
(*                                                           *)
(* http://www3.inrets.fr/Public/ESTAS/Mariano.Georges/Bcaml/ *)
(*                                                           *)
(*                   IRIT                                    *)
(*                                                           *)
(*   source genere le 14-05-2001  09:04:43                   *)
(*************************************************************)

module type BlastSig = sig
  type amn_ty
  type head_ty
  type clause_ty
  type def_ty
  type defBody_ty
  type set_ty
  type instance_ty
  type type_B_ty
  type field_ty
  type predicate_ty
  type operation_ty
  type expr_ty
  type exprSeq_ty
  type exprSet_ty
  type number_ty
  type substitution_ty
(*  type ids_ty *)
  type origin_ty
  type id_ty
  val mk_Machine: Blast.amn -> head_ty ->  clause_ty list -> amn_ty
  val mk_Refinement: Blast.amn -> head_ty ->  clause_ty list -> amn_ty
  val mk_Implementation: Blast.amn -> head_ty ->  clause_ty list -> amn_ty
  val mk_EmptyTree: Blast.amn -> amn_ty
  val mk_head: Blast.head -> id_ty ->  id_ty list -> head_ty
  val mk_Definitions: Blast.clause -> def_ty list -> clause_ty
  val mk_Constraints: Blast.clause -> predicate_ty -> clause_ty
  val mk_Invariant: Blast.clause -> predicate_ty -> clause_ty
  val mk_Sets: Blast.clause -> set_ty list -> clause_ty
  val mk_Initialisation: Blast.clause -> substitution_ty -> clause_ty
  val mk_ConstantsConcrete: Blast.clause -> id_ty list -> clause_ty
  val mk_ConstantsAbstract: Blast.clause -> id_ty list -> clause_ty
  val mk_ConstantsHidden: Blast.clause -> id_ty list -> clause_ty
  val mk_Properties: Blast.clause -> predicate_ty -> clause_ty
  val mk_Values: Blast.clause -> (id_ty * expr_ty) list -> clause_ty
  val mk_VariablesConcrete: Blast.clause -> id_ty list -> clause_ty
  val mk_VariablesAbstract: Blast.clause -> id_ty list -> clause_ty
  val mk_VariablesHidden: Blast.clause -> id_ty list -> clause_ty
  val mk_Promotes: Blast.clause -> id_ty list -> clause_ty
  val mk_Assertions: Blast.clause -> predicate_ty list -> clause_ty
  val mk_Operations: Blast.clause -> operation_ty list -> clause_ty
  val mk_Sees: Blast.clause -> id_ty list -> clause_ty
  val mk_Uses: Blast.clause -> id_ty list -> clause_ty
  val mk_Extends: Blast.clause -> instance_ty list -> clause_ty
  val mk_Includes: Blast.clause -> instance_ty list -> clause_ty
  val mk_Imports: Blast.clause -> instance_ty list -> clause_ty
  val mk_Refines: Blast.clause -> id_ty -> clause_ty
  val mk_Def: Blast.def -> id_ty ->  id_ty list ->  defBody_ty -> def_ty
  val mk_FileDef: Blast.def -> string -> def_ty
  val mk_ExprDefBody: Blast.defBody -> expr_ty -> defBody_ty
  val mk_SubstDefBody: Blast.defBody -> substitution_ty -> defBody_ty
  val mk_SetAbstDec: Blast.set -> id_ty -> set_ty
  val mk_SetEnumDec: Blast.set -> id_ty ->  id_ty list -> set_ty
  val mk_instance: Blast.instance -> id_ty ->  expr_ty list -> instance_ty
  val mk_TypeNatSetRange: Blast.type_B -> expr_ty ->  expr_ty -> type_B_ty
  val mk_PredType: Blast.type_B -> expr_ty -> type_B_ty
  val mk_TypeIdentifier: Blast.type_B -> id_ty -> type_B_ty
  val mk_PFunType: Blast.type_B -> type_B_ty ->  type_B_ty -> type_B_ty
  val mk_FunType: Blast.type_B -> type_B_ty ->  type_B_ty -> type_B_ty
  val mk_ProdType: Blast.type_B -> type_B_ty ->  type_B_ty -> type_B_ty
  val mk_Sequence: Blast.type_B -> type_B_ty -> type_B_ty
  val mk_SubType: Blast.type_B -> type_B_ty ->  expr_ty -> type_B_ty
  val mk_Fin: Blast.type_B -> type_B_ty -> type_B_ty
  val mk_Untyped: Blast.type_B -> type_B_ty
  val mk_field: Blast.field -> id_ty ->  type_B_ty -> field_ty
  val mk_PredParen: Blast.predicate -> predicate_ty -> predicate_ty
  val mk_PredNegation: Blast.predicate -> predicate_ty -> predicate_ty
  val mk_PredExists: Blast.predicate -> id_ty list ->  predicate_ty -> predicate_ty
  val mk_PredForAll: Blast.predicate -> id_ty list ->  predicate_ty -> predicate_ty
  val mk_PredBin: Blast.predicate -> Bbop.bop2 ->  predicate_ty ->  predicate_ty -> predicate_ty
  val mk_PredAtom: Blast.predicate -> Bbop.bop2 ->  expr_ty ->  expr_ty -> predicate_ty
  val mk_operation: Blast.operation -> id_ty ->  id_ty list ->  id_ty list ->  substitution_ty -> operation_ty
  val mk_ExprSequence: Blast.expr -> expr_ty ->  expr_ty -> expr_ty
  val mk_ExprParen: Blast.expr -> expr_ty -> expr_ty
  val mk_ExprId: Blast.expr -> id_ty -> expr_ty
  val mk_ExprFunCall: Blast.expr -> expr_ty ->  expr_ty list -> expr_ty
  val mk_ExprBin: Blast.expr -> Bbop.op2 ->  expr_ty ->  expr_ty -> expr_ty
  val mk_ExprUn: Blast.expr -> Bbop.op1 ->  expr_ty -> expr_ty
  val mk_ExprSeq: Blast.expr -> exprSeq_ty -> expr_ty
  val mk_ExprSet: Blast.expr -> exprSet_ty -> expr_ty
  val mk_RelSet: Blast.expr -> expr_ty ->  expr_ty -> expr_ty
  val mk_RelSeqComp: Blast.expr -> expr_ty list -> expr_ty
  val mk_ExprNumber: Blast.expr -> number_ty -> expr_ty
  val mk_ExprBool: Blast.expr -> bool -> expr_ty
  val mk_ExprString: Blast.expr -> string -> expr_ty
  val mk_ExprPred: Blast.expr -> predicate_ty -> expr_ty
  val mk_ExprNuplet: Blast.expr -> expr_ty list -> expr_ty
  val mk_ExprSIGMA: Blast.expr -> id_ty list ->  predicate_ty ->  expr_ty -> expr_ty
  val mk_ExprPI: Blast.expr -> id_ty list ->  predicate_ty ->  expr_ty -> expr_ty
  val mk_ExprLambda: Blast.expr -> id_ty list ->  predicate_ty ->  expr_ty -> expr_ty
  val mk_SeqEnum: Blast.exprSeq -> expr_ty list -> exprSeq_ty
  val mk_SeqEmpty: Blast.exprSeq -> exprSeq_ty
  val mk_SetRange: Blast.exprSet -> expr_ty ->  expr_ty -> exprSet_ty
  val mk_SetEnum: Blast.exprSet -> expr_ty list -> exprSet_ty
  val mk_SetCompr: Blast.exprSet -> expr_ty list -> exprSet_ty
  val mk_SetComprPred: Blast.exprSet -> id_ty list ->  predicate_ty -> exprSet_ty
  val mk_SetEmpty: Blast.exprSet -> exprSet_ty
  val mk_SetUnionQuantify: Blast.exprSet -> id_ty list ->  predicate_ty ->  expr_ty -> exprSet_ty
  val mk_SetInterQuantify: Blast.exprSet -> id_ty list ->  predicate_ty ->  expr_ty -> exprSet_ty
  val mk_MinNumber: Blast.number -> number_ty
  val mk_Number: Blast.number -> int32 -> number_ty
  val mk_MaxNumber: Blast.number -> number_ty
  val mk_SubstOperCall: Blast.substitution -> id_ty list ->  id_ty ->  expr_ty list -> substitution_ty
  val mk_SubstBlock: Blast.substitution -> substitution_ty -> substitution_ty
  val mk_SubstPrecondition: Blast.substitution -> predicate_ty ->  substitution_ty -> substitution_ty
  val mk_SubstAssertion: Blast.substitution -> predicate_ty ->  substitution_ty -> substitution_ty
  val mk_SubstChoice: Blast.substitution -> substitution_ty list -> substitution_ty
  val mk_SubstIf: Blast.substitution -> (predicate_ty * substitution_ty) list ->  substitution_ty option -> substitution_ty
  val mk_SubstSelect: Blast.substitution -> (predicate_ty * substitution_ty) list ->  substitution_ty option -> substitution_ty
  val mk_SubstCase: Blast.substitution -> expr_ty ->  (expr_ty list * substitution_ty) list ->  substitution_ty option -> substitution_ty
  val mk_SubstAny: Blast.substitution -> id_ty list ->  predicate_ty ->  substitution_ty -> substitution_ty
  val mk_SubstLet: Blast.substitution -> id_ty list ->  predicate_ty ->  substitution_ty -> substitution_ty
  val mk_SubstVar: Blast.substitution -> id_ty list ->  substitution_ty -> substitution_ty
  val mk_SubstWhile: Blast.substitution -> predicate_ty ->  substitution_ty ->  expr_ty ->  predicate_ty -> substitution_ty
  val mk_SubstSkip: Blast.substitution -> substitution_ty
  val mk_SubstSequence: Blast.substitution -> substitution_ty ->  substitution_ty -> substitution_ty
  val mk_SubstParallel: Blast.substitution -> substitution_ty ->  substitution_ty -> substitution_ty
  val mk_SubstSetEqualIds: Blast.substitution -> id_ty list ->  expr_ty list -> substitution_ty
  val mk_SubstSetEqualFun: Blast.substitution -> expr_ty ->  expr_ty list ->  expr_ty -> substitution_ty
  val mk_SubstBecomeSuch: Blast.substitution -> id_ty list ->  predicate_ty -> substitution_ty
  val mk_SubstSetIn: Blast.substitution -> id_ty list ->  expr_ty -> substitution_ty
  val mk_ids: Blast.id list -> id_ty list -> id_ty list
  val mk_Local: Blast.origin -> origin_ty
  val mk_Remote: Blast.origin -> (string * string) list -> origin_ty
  val mk_OrgRefine: Blast.origin -> string ->  (string * string) list -> origin_ty
  val mk_Prefix: Blast.origin -> string list -> origin_ty
  val mk_Id: Blast.id -> origin_ty ->  string ->  type_B_ty option ->  Bbop.kind -> id_ty
end

open Blast
let it_option f = function Some o -> Some (f o) | None -> None
module Blast_it =
  functor (MS_:BlastSig) ->
  struct
    let rec it_amn self = match self with
        Machine(_x1, _x2) ->
        let _y1 = (it_head _x1) in
        let _y2 = ((List.map it_clause) _x2) in
        MS_.mk_Machine self _y1 _y2
      | Refinement(_x1, _x2) ->
        let _y1 = (it_head _x1) in
        let _y2 = ((List.map it_clause) _x2) in
        MS_.mk_Refinement self _y1 _y2
      | Implementation(_x1, _x2) ->
        let _y1 = (it_head _x1) in
        let _y2 = ((List.map it_clause) _x2) in
        MS_.mk_Implementation self _y1 _y2
      | EmptyTree ->
        
        MS_.mk_EmptyTree self
    and it_head ((_x1, _x2) as self) = let _y1 = (it_id _x1) in
        let _y2 = (it_ids _x2) in
        MS_.mk_head self _y1 _y2
    and it_clause self = match self with
        Definitions(_x1) ->
        let _y1 = ((List.map it_def) _x1) in
        MS_.mk_Definitions self _y1
      | Constraints(_x1) ->
        let _y1 = (it_predicate _x1) in
        MS_.mk_Constraints self _y1
      | Invariant(_x1) ->
        let _y1 = (it_predicate _x1) in
        MS_.mk_Invariant self _y1
      | Sets(_x1) ->
        let _y1 = ((List.map it_set) _x1) in
        MS_.mk_Sets self _y1
      | Initialisation(_x1) ->
        let _y1 = (it_substitution _x1) in
        MS_.mk_Initialisation self _y1
      | ConstantsConcrete(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_ConstantsConcrete self _y1
      | ConstantsAbstract(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_ConstantsAbstract self _y1
      | ConstantsHidden(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_ConstantsHidden self _y1
      | Properties(_x1) ->
        let _y1 = (it_predicate _x1) in
        MS_.mk_Properties self _y1
      | Values(_x1) ->
        let _y1 = ((List.map (fun (_x1, _x2) -> let _y1 = (it_id _x1) in
        let _y2 = (it_expr _x2) in
        (_y1, _y2))) _x1) in
        MS_.mk_Values self _y1
      | VariablesConcrete(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_VariablesConcrete self _y1
      | VariablesAbstract(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_VariablesAbstract self _y1
      | VariablesHidden(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_VariablesHidden self _y1
      | Promotes(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_Promotes self _y1
      | Assertions(_x1) ->
        let _y1 = ((List.map it_predicate) _x1) in
        MS_.mk_Assertions self _y1
      | Operations(_x1) ->
        let _y1 = ((List.map it_operation) _x1) in
        MS_.mk_Operations self _y1
      | Sees(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_Sees self _y1
      | Uses(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_Uses self _y1
      | Extends(_x1) ->
        let _y1 = ((List.map it_instance) _x1) in
        MS_.mk_Extends self _y1
      | Includes(_x1) ->
        let _y1 = ((List.map it_instance) _x1) in
        MS_.mk_Includes self _y1
      | Imports(_x1) ->
        let _y1 = ((List.map it_instance) _x1) in
        MS_.mk_Imports self _y1
      | Refines(_x1) ->
        let _y1 = (it_id _x1) in
        MS_.mk_Refines self _y1
    and it_def self = match self with
        Def(_x1, _x2, _x3) ->
        let _y1 = (it_id _x1) in
        let _y2 = (it_ids _x2) in
        let _y3 = (it_defBody _x3) in
        MS_.mk_Def self _y1 _y2 _y3
      | FileDef(_x1) ->
        let _y1 = _x1 in
        MS_.mk_FileDef self _y1
    and it_defBody self = match self with
        ExprDefBody(_x1) ->
        let _y1 = (it_expr _x1) in
        MS_.mk_ExprDefBody self _y1
      | SubstDefBody(_x1) ->
        let _y1 = (it_substitution _x1) in
        MS_.mk_SubstDefBody self _y1
    and it_set self = match self with
        SetAbstDec(_x1) ->
        let _y1 = (it_id _x1) in
        MS_.mk_SetAbstDec self _y1
      | SetEnumDec(_x1, _x2) ->
        let _y1 = (it_id _x1) in
        let _y2 = (it_ids _x2) in
        MS_.mk_SetEnumDec self _y1 _y2
    and it_instance ((_x1, _x2) as self) = let _y1 = (it_id _x1) in
        let _y2 = ((List.map it_expr) _x2) in
        MS_.mk_instance self _y1 _y2
    and it_type_B self = match self with
        TypeNatSetRange(_x1, _x2) ->
        let _y1 = (it_expr _x1) in
        let _y2 = (it_expr _x2) in
        MS_.mk_TypeNatSetRange self _y1 _y2
      | PredType(_x1) ->
        let _y1 = (it_expr _x1) in
        MS_.mk_PredType self _y1
      | TypeIdentifier(_x1) ->
        let _y1 = (it_id _x1) in
        MS_.mk_TypeIdentifier self _y1
      | PFunType(_x1, _x2) ->
        let _y1 = (it_type_B _x1) in
        let _y2 = (it_type_B _x2) in
        MS_.mk_PFunType self _y1 _y2
      | FunType(_x1, _x2) ->
        let _y1 = (it_type_B _x1) in
        let _y2 = (it_type_B _x2) in
        MS_.mk_FunType self _y1 _y2
      | ProdType(_x1, _x2) ->
        let _y1 = (it_type_B _x1) in
        let _y2 = (it_type_B _x2) in
        MS_.mk_ProdType self _y1 _y2
      | Sequence(_x1) ->
        let _y1 = (it_type_B _x1) in
        MS_.mk_Sequence self _y1
      | SubType(_x1, _x2) ->
        let _y1 = (it_type_B _x1) in
        let _y2 = (it_expr _x2) in
        MS_.mk_SubType self _y1 _y2
      | Fin_B(_x1) ->
        let _y1 = (it_type_B _x1) in
        MS_.mk_Fin self _y1
      | Untyped ->
        MS_.mk_Untyped self
    and it_field ((_x1, _x2) as self) = let _y1 = (it_id _x1) in
        let _y2 = (it_type_B _x2) in
        MS_.mk_field self _y1 _y2
    and it_predicate self = match self with
        PredParen(_x1) ->
        let _y1 = (it_predicate _x1) in
        MS_.mk_PredParen self _y1
      | PredNegation(_x1) ->
        let _y1 = (it_predicate _x1) in
        MS_.mk_PredNegation self _y1
      | PredExists(_x1, _x2) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        MS_.mk_PredExists self _y1 _y2
      | PredForAll(_x1, _x2) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        MS_.mk_PredForAll self _y1 _y2
      | PredBin(_x1, _x2, _x3) ->
        let _y1 = _x1 in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_predicate _x3) in
        MS_.mk_PredBin self _y1 _y2 _y3
      | PredAtom(_x1, _x2, _x3) ->
        let _y1 = _x1 in
        let _y2 = (it_expr _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_PredAtom self _y1 _y2 _y3
    and it_operation ((_x1, _x2, _x3, _x4) as self) = let _y1 = (it_id _x1) in
        let _y2 = (it_ids _x2) in
        let _y3 = (it_ids _x3) in
        let _y4 = (it_substitution _x4) in
        MS_.mk_operation self _y1 _y2 _y3 _y4
    and it_expr self = match self with
        ExprSequence(_x1, _x2) ->
        let _y1 = (it_expr _x1) in
        let _y2 = (it_expr _x2) in
        MS_.mk_ExprSequence self _y1 _y2
      | ExprParen(_x1) ->
        let _y1 = (it_expr _x1) in
        MS_.mk_ExprParen self _y1
      | ExprId(_x1) ->
        let _y1 = (it_id _x1) in
        MS_.mk_ExprId self _y1
      | ExprFunCall(_x1, _x2) ->
        let _y1 = (it_expr _x1) in
        let _y2 = ((List.map it_expr) _x2) in
        MS_.mk_ExprFunCall self _y1 _y2
      | ExprBin(_x1, _x2, _x3) ->
        let _y1 = _x1 in
        let _y2 = (it_expr _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_ExprBin self _y1 _y2 _y3
      | ExprUn(_x1, _x2) ->
        let _y1 = _x1 in
        let _y2 = (it_expr _x2) in
        MS_.mk_ExprUn self _y1 _y2
      | ExprSeq(_x1) ->
        let _y1 = (it_exprSeq _x1) in
        MS_.mk_ExprSeq self _y1
      | ExprSet(_x1) ->
        let _y1 = (it_exprSet _x1) in
        MS_.mk_ExprSet self _y1
      | RelSet(_x1, _x2) ->
        let _y1 = (it_expr _x1) in
        let _y2 = (it_expr _x2) in
        MS_.mk_RelSet self _y1 _y2
      | RelSeqComp(_x1) ->
        let _y1 = ((List.map it_expr) _x1) in
        MS_.mk_RelSeqComp self _y1
      | ExprNumber(_x1) ->
        let _y1 = (it_number _x1) in
        MS_.mk_ExprNumber self _y1
      | ExprBool(_x1) ->
        let _y1 = _x1 in
        MS_.mk_ExprBool self _y1
      | ExprString(_x1) ->
        let _y1 = _x1 in
        MS_.mk_ExprString self _y1
      | ExprPred(_x1) ->
        let _y1 = (it_predicate _x1) in
        MS_.mk_ExprPred self _y1
      | ExprNuplet(_x1) ->
        let _y1 = ((List.map it_expr) _x1) in
        MS_.mk_ExprNuplet self _y1
      | ExprSIGMA(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_ExprSIGMA self _y1 _y2 _y3
      | ExprPI(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_ExprPI self _y1 _y2 _y3
      | ExprLambda(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_ExprLambda self _y1 _y2 _y3
    and it_exprSeq self = match self with
        SeqEnum(_x1) ->
        let _y1 = ((List.map it_expr) _x1) in
        MS_.mk_SeqEnum self _y1
      | SeqEmpty ->
        
        MS_.mk_SeqEmpty self
    and it_exprSet self = match self with
        SetRange(_x1, _x2) ->
        let _y1 = (it_expr _x1) in
        let _y2 = (it_expr _x2) in
        MS_.mk_SetRange self _y1 _y2
      | SetEnum(_x1) ->
        let _y1 = ((List.map it_expr) _x1) in
        MS_.mk_SetEnum self _y1
      | SetCompr(_x1) ->
        let _y1 = ((List.map it_expr) _x1) in
        MS_.mk_SetCompr self _y1
      | SetComprPred(_x1, _x2) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        MS_.mk_SetComprPred self _y1 _y2
      | SetEmpty ->
        
        MS_.mk_SetEmpty self
      | SetUnionQuantify(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_SetUnionQuantify self _y1 _y2 _y3
      | SetInterQuantify(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_SetInterQuantify self _y1 _y2 _y3
    and it_number self = match self with
        MinNumber ->
        
        MS_.mk_MinNumber self
      | Number(_x1) ->
        let _y1 = _x1 in
        MS_.mk_Number self _y1
      | MaxNumber ->
        
        MS_.mk_MaxNumber self
    and it_substitution self = match self with
        SubstOperCall(_x1, _x2, _x3) ->
        let _y1 = (it_ids _x1) in
        let _y2 = (it_id _x2) in
        let _y3 = ((List.map it_expr) _x3) in
        MS_.mk_SubstOperCall self _y1 _y2 _y3
      | SubstBlock(_x1) ->
        let _y1 = (it_substitution _x1) in
        MS_.mk_SubstBlock self _y1
      | SubstPrecondition(_x1, _x2) ->
        let _y1 = (it_predicate _x1) in
        let _y2 = (it_substitution _x2) in
        MS_.mk_SubstPrecondition self _y1 _y2
      | SubstAssertion(_x1, _x2) ->
        let _y1 = (it_predicate _x1) in
        let _y2 = (it_substitution _x2) in
        MS_.mk_SubstAssertion self _y1 _y2
      | SubstChoice(_x1) ->
        let _y1 = ((List.map it_substitution) _x1) in
        MS_.mk_SubstChoice self _y1
      | SubstIf(_x1, _x2) ->
        let _y1 = ((List.map (fun (_x1, _x2) -> let _y1 = (it_predicate _x1) in
        let _y2 = (it_substitution _x2) in
        (_y1, _y2))) _x1) in
        let _y2 = ((it_option it_substitution) _x2) in
        MS_.mk_SubstIf self _y1 _y2
      | SubstSelect(_x1, _x2) ->
        let _y1 = ((List.map (fun (_x1, _x2) -> let _y1 = (it_predicate _x1) in
        let _y2 = (it_substitution _x2) in
        (_y1, _y2))) _x1) in
        let _y2 = ((it_option it_substitution) _x2) in
        MS_.mk_SubstSelect self _y1 _y2
      | SubstCase(_x1, _x2, _x3) ->
        let _y1 = (it_expr _x1) in
        let _y2 = ((List.map (fun (_x1, _x2) -> let _y1 = ((List.map it_expr) _x1) in
        let _y2 = (it_substitution _x2) in
        (_y1, _y2))) _x2) in
        let _y3 = ((it_option it_substitution) _x3) in
        MS_.mk_SubstCase self _y1 _y2 _y3
      | SubstAny(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_substitution _x3) in
        MS_.mk_SubstAny self _y1 _y2 _y3
      | SubstLet(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_substitution _x3) in
        MS_.mk_SubstLet self _y1 _y2 _y3
      | SubstVar(_x1, _x2) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_substitution _x2) in
        MS_.mk_SubstVar self _y1 _y2
      | SubstWhile(_x1, _x2, _x3, _x4) ->
        let _y1 = (it_predicate _x1) in
        let _y2 = (it_substitution _x2) in
        let _y3 = (it_expr _x3) in
        let _y4 = (it_predicate _x4) in
        MS_.mk_SubstWhile self _y1 _y2 _y3 _y4
      | SubstSkip ->
        
        MS_.mk_SubstSkip self
      | SubstSequence(_x1, _x2) ->
        let _y1 = (it_substitution _x1) in
        let _y2 = (it_substitution _x2) in
        MS_.mk_SubstSequence self _y1 _y2
      | SubstParallel(_x1, _x2) ->
        let _y1 = (it_substitution _x1) in
        let _y2 = (it_substitution _x2) in
        MS_.mk_SubstParallel self _y1 _y2
      | SubstSetEqualIds(_x1, _x2) ->
        let _y1 = (it_ids _x1) in
        let _y2 = ((List.map it_expr) _x2) in
        MS_.mk_SubstSetEqualIds self _y1 _y2
      | SubstSetEqualFun(_x1, _x2, _x3) ->
        let _y1 = (it_expr _x1) in
        let _y2 = ((List.map it_expr) _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_SubstSetEqualFun self _y1 _y2 _y3
      | SubstBecomeSuch(_x1, _x2) ->
        let _y1 = (it_ids _x1) in
        let _y2 = (it_predicate _x2) in
        MS_.mk_SubstBecomeSuch self _y1 _y2
      | SubstSetIn(_x1, _x2) ->
        let _y1 = (it_ids _x1) in
        let _y2 = (it_expr _x2) in
        MS_.mk_SubstSetIn self _y1 _y2
    and it_ids ((_x1) as self) = let _y1 = ((List.map it_id) _x1) in
        MS_.mk_ids self _y1
    and it_origin self = match self with
        Local ->        
        MS_.mk_Local self
      | Remote(_x1) ->
        let _y1 = ((List.map (fun (_x1, _x2) -> let _y1 = _x1 in
        let _y2 = _x2 in
        (_y1, _y2))) _x1) in
        MS_.mk_Remote self _y1
      | OrgRefine(_x1, _x2) ->
        let _y1 = _x1 in
        let _y2 = ((List.map (fun (_x1, _x2) -> let _y1 = _x1 in
        let _y2 = _x2 in
        (_y1, _y2))) _x2) in
        MS_.mk_OrgRefine self _y1 _y2
      | Prefix(_x1) ->
        let _y1 = _x1 in
        MS_.mk_Prefix self _y1
    and it_id self = match self with
        Id(_x1, _x2, _x3, _x4) ->
        let _y1 = (it_origin _x1) in
        let _y2 = _x2 in
        let _y3 = ((it_option it_type_B) _x3) in
        let _y4 = _x4 in
        MS_.mk_Id self _y1 _y2 _y3 _y4
  end
