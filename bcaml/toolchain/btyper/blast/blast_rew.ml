(* $Id$ *)

open Blast
open Blast_lib
let i = fun x -> x
module Set_string = Set.Make (
struct 
  type t = string
  let compare = compare
end)
let l_union_id ls = List.fold_left Set_id.union Set_id.empty ls
type rew_id_blast_driver = {
  rew_id_in_amn: amn -> amn;
  rew_id_in_head: head -> head;
  rew_id_in_clause: clause -> clause;
  rew_id_in_def: def -> def;
  rew_id_in_defBody: defBody -> defBody;
  rew_id_in_set: set -> set;
  rew_id_in_instance: instance -> instance;
  rew_id_in_type_B: type_B -> type_B;
  rew_id_in_field: field -> field;
  rew_id_in_predicate: predicate -> predicate;
  rew_id_in_operation: operation -> operation;
  rew_id_in_expr: expr -> expr;
  rew_id_in_exprRecords: exprRecords -> exprRecords;
  rew_id_in_recordsItem: recordsItem -> recordsItem;
  rew_id_in_exprSeq: exprSeq -> exprSeq;
  rew_id_in_exprSet: exprSet -> exprSet;
  rew_id_in_number: number -> number;
  rew_id_in_substitution: substitution -> substitution;
  rew_id_in_ids: ids -> ids;
  rew_id_in_origine: origine -> origine;
  rew_id_in_id: id -> id
}

let rew_id_in_blast ?(r_amn = i) ?(r_head = i) ?(r_clause = i) ?(r_def = i) ?(r_defBody = i) ?(r_set = i) ?(r_instance = i) ?(r_type_B = i) ?(r_field = i) ?(r_predicate = i) ?(r_operation = i) ?(r_expr = i) ?(r_exprRecords = i) ?(r_recordsItem = i) ?(r_exprSeq = i) ?(r_exprSet = i) ?(r_number = i) ?(r_substitution = i) ?(r_ids = i) ?(r_origine = i) ?(r_id = i) ?(_support = []) ?(_rename = i) () =
  let _support_ren = List.map (fun id -> (id,_rename id)) _support in 
  let rew_free_id rew _bvars x = 
    if Set_id.mem x _bvars then
      try List.assoc x _support_ren with Not_found -> x
    else rew _bvars x in
  let rew_id_in_bool _ (b:bool) = b in
  let rew_id_in_string _ (s:string) = s in
  let rew_id_in_int _ (i:int) = i in
  let rew_option f bv = function Some x -> Some(f bv x) | None -> None in
  let rew_seq f bv l = List.map (f bv) l in 
    
  let rec rew_id_in_amn  _bvars = 
    fun _x -> r_amn((function
			 Machine(_x1, _x2) 
			 -> Machine((rew_id_in_head  _bvars _x1), 
				    ((rew_seq (rew_id_in_clause )) 
				       _bvars _x2))
		       | Refinement(_x1, _x2) 
			 -> Refinement((rew_id_in_head  _bvars _x1), 
				       ((rew_seq (rew_id_in_clause )) 
					  _bvars _x2))
		       | Implementation(_x1, _x2) 
			 -> Implementation((rew_id_in_head  _bvars _x1), 
					   ((rew_seq (rew_id_in_clause )) _bvars _x2))
		       | EmptyTree -> EmptyTree )_x)
      
  and rew_id_in_head  _bvars = 
    fun _x -> 
      r_head((fun (_x1, _x2) -> 
		(rew_free_id rew_id_in_id  _bvars _x1), (rew_id_in_ids  _bvars _x2))_x)
      
  and rew_id_in_clause  _bvars = 
    fun _x -> r_clause((function
			    Definitions(_x1) 
			    -> Definitions(((rew_seq (rew_id_in_def )) _bvars _x1))
			  | Constraints(_x1) 
			    -> Constraints((rew_id_in_predicate  _bvars _x1))
			  | Invariant(_x1) 
			    -> Invariant((rew_id_in_predicate  _bvars _x1))
			  | Sets(_x1) 
			    -> Sets(((rew_seq (rew_id_in_set )) _bvars _x1))
			  | Initialisation(_x1) 
			    -> Initialisation((rew_id_in_substitution  _bvars _x1))
			  | ConstantsConcrete(_x1) 
			    -> ConstantsConcrete((rew_id_in_ids  _bvars _x1))
			  | ConstantsAbstract(_x1) 
			    -> ConstantsAbstract((rew_id_in_ids  _bvars _x1))
			  | ConstantsHidden(_x1) 
			    -> ConstantsHidden((rew_id_in_ids  _bvars _x1))
			  | Properties(_x1) 
			    -> Properties((rew_id_in_predicate  _bvars _x1))
			  | Values(_x1) -> Values(((rew_seq (fun _bvars (_x1, _x2) -> ((rew_free_id rew_id_in_id  _bvars _x1), (rew_id_in_expr  _bvars _x2)))) _bvars _x1))
			  | VariablesConcrete(_x1) -> VariablesConcrete((rew_id_in_ids  _bvars _x1))
			  | VariablesAbstract(_x1) -> VariablesAbstract((rew_id_in_ids  _bvars _x1))
			  | VariablesHidden(_x1) -> VariablesHidden((rew_id_in_ids  _bvars _x1))
			  | Promotes(_x1) -> Promotes((rew_id_in_ids  _bvars _x1))
			  | Assertions(_x1) -> Assertions(((rew_seq (rew_id_in_predicate )) _bvars _x1))
			  | Operations(_x1) -> Operations(((rew_seq (rew_id_in_operation )) _bvars _x1))
			  | Sees(_x1) -> Sees((rew_id_in_ids  _bvars _x1))
			  | Uses(_x1) -> Uses((rew_id_in_ids  _bvars _x1))
			  | Extends(_x1) -> Extends(((rew_seq (rew_id_in_instance )) _bvars _x1))
			  | Includes(_x1) -> Includes(((rew_seq (rew_id_in_instance )) _bvars _x1))
			  | Imports(_x1) -> Imports(((rew_seq (rew_id_in_instance )) _bvars _x1))
			  | Refines(_x1) -> Refines((rew_free_id rew_id_in_id  _bvars _x1)))_x)

  and rew_id_in_def  _bvars = 
    fun _x -> r_def((function
			 Def(_x1, _x2, _x3) 
			 -> Def((rew_free_id rew_id_in_id  _bvars _x1), 
				(rew_id_in_ids  _bvars _x2), 
				(rew_id_in_defBody  _bvars _x3))
		       | FileDef(_x1) 
			 -> FileDef((rew_id_in_string  _bvars _x1)))_x)

  and rew_id_in_defBody  _bvars = 
    fun _x -> r_defBody((function
			     ExprDefBody(_x1) 
			     -> ExprDefBody((rew_id_in_expr  _bvars _x1))
			   | SubstDefBody(_x1) 
			     -> SubstDefBody((rew_id_in_substitution  _bvars _x1)))_x)

  and rew_id_in_set  _bvars = 
    fun _x -> r_set((function
			 SetAbstDec(_x1) -> SetAbstDec((rew_free_id rew_id_in_id  _bvars _x1))
		       | SetEnumDec(_x1, _x2) -> SetEnumDec((rew_free_id rew_id_in_id  _bvars _x1), (rew_id_in_ids  _bvars _x2))
		       | SetRecordsDec(_x1, _x2) -> SetRecordsDec((rew_free_id rew_id_in_id  _bvars _x1), ((rew_seq (rew_id_in_recordsItem )) _bvars _x2)))_x)
      
  and rew_id_in_instance  _bvars = 
    fun _x -> r_instance((fun (_x1, _x2) -> (rew_free_id rew_id_in_id  _bvars _x1), ((rew_seq (rew_id_in_expr )) _bvars _x2))_x)
      
  and rew_id_in_type_B  _bvars = 
    fun _x -> r_type_B((function
			    TypeNatSetRange(_x1, _x2) -> TypeNatSetRange((rew_id_in_expr  _bvars _x1), (rew_id_in_expr  _bvars _x2))
			  | PredType(_x1) -> PredType((rew_id_in_expr  _bvars _x1))
			  | RecordsType(_x1) -> RecordsType(((rew_seq (rew_id_in_field )) _bvars _x1))
			  | TypeIdentifier(_x1) -> TypeIdentifier((rew_free_id rew_id_in_id  _bvars _x1))
			  | PFunType(_x1, _x2) -> PFunType((rew_id_in_type_B  _bvars _x1), (rew_id_in_type_B  _bvars _x2))
			  | FunType(_x1, _x2) -> FunType((rew_id_in_type_B  _bvars _x1), (rew_id_in_type_B  _bvars _x2))
			  | ProdType(_x1, _x2) -> ProdType((rew_id_in_type_B  _bvars _x1), (rew_id_in_type_B  _bvars _x2))
			  | Sequence(_x1) -> Sequence((rew_id_in_type_B  _bvars _x1))
			  | SubType(_x1, _x2) -> SubType((rew_id_in_type_B  _bvars _x1), (rew_id_in_expr  _bvars _x2))
			  | Fin(_x1) -> Fin((rew_id_in_type_B  _bvars _x1))
			  | Untyped -> Untyped )_x)

  and rew_id_in_field  _bvars = 
    fun _x -> r_field((fun (_x1, _x2) -> (rew_free_id rew_id_in_id  _bvars _x1), (rew_id_in_type_B  _bvars _x2))_x)
      
  and rew_id_in_predicate  _bvars = 
    fun _x -> r_predicate((function
			       PredParen(_x1) -> PredParen((rew_id_in_predicate  _bvars _x1))
			     | PredNegation(_x1) -> PredNegation((rew_id_in_predicate  _bvars _x1))
			     | PredExists(_x1, _x2) -> PredExists(((rew_seq (rew_free_id rew_id_in_id )) (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x1), (rew_id_in_predicate  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x2))
			     | PredForAll(_x1, _x2) -> PredForAll(((rew_seq (rew_free_id rew_id_in_id )) (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x1), (rew_id_in_predicate  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x2))
			     | PredBin(_x1, _x2, _x3) -> PredBin(_x1, (rew_id_in_predicate  _bvars _x2), (rew_id_in_predicate  _bvars _x3))
			     | PredAtom(_x1, _x2, _x3) -> PredAtom(_x1, (rew_id_in_expr  _bvars _x2), (rew_id_in_expr  _bvars _x3)))_x)
      
  and rew_id_in_operation  _bvars = 
    fun _x -> r_operation((fun (_x1, _x2, _x3, _x4) -> (rew_free_id rew_id_in_id  _bvars _x1), (rew_id_in_ids  _bvars _x2), (rew_id_in_ids  _bvars _x3), (rew_id_in_substitution  _bvars _x4))_x)
      
  and rew_id_in_expr  _bvars = 
    fun _x -> r_expr((function
			  ExprSequence(_x1, _x2) -> ExprSequence((rew_id_in_expr  _bvars _x1), (rew_id_in_expr  _bvars _x2))
			| ExprParen(_x1) -> ExprParen((rew_id_in_expr  _bvars _x1))
			| ExprId(_x1) -> ExprId((rew_free_id rew_id_in_id  _bvars _x1))
			| ExprFunCall(_x1, _x2) -> ExprFunCall((rew_id_in_expr  _bvars _x1), ((rew_seq (rew_id_in_expr )) _bvars _x2))
			| ExprBin(_x1, _x2, _x3) -> ExprBin(_x1, (rew_id_in_expr  _bvars _x2), (rew_id_in_expr  _bvars _x3))
			| ExprUn(_x1, _x2) -> ExprUn(_x1, (rew_id_in_expr  _bvars _x2))
			| ExprSeq(_x1) -> ExprSeq((rew_id_in_exprSeq  _bvars _x1))
			| ExprSet(_x1) -> ExprSet((rew_id_in_exprSet  _bvars _x1))
			| RelSet(_x1, _x2) -> RelSet((rew_id_in_expr  _bvars _x1), (rew_id_in_expr  _bvars _x2))
			| RelSeqComp(_x1) -> RelSeqComp(((rew_seq (rew_id_in_expr )) _bvars _x1))
			| ExprNumber(_x1) -> ExprNumber((rew_id_in_number  _bvars _x1))
			| ExprBool(_x1) -> ExprBool((rew_id_in_bool  _bvars _x1))
			| ExprString(_x1) -> ExprString((rew_id_in_string  _bvars _x1))
			| ExprRecords(_x1) -> ExprRecords((rew_id_in_exprRecords  _bvars _x1))
			| ExprPred(_x1) -> ExprPred((rew_id_in_predicate  _bvars _x1))
			| ExprNuplet(_x1) -> ExprNuplet(((rew_seq (rew_id_in_expr )) _bvars _x1))
			| ExprSIGMA(_x1, _x2, _x3) 
			  -> ExprSIGMA(((rew_seq (rew_free_id rew_id_in_id )) 
					  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x1), (rew_id_in_predicate  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x2), (rew_id_in_expr  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x3))
			| ExprPI(_x1, _x2, _x3) -> ExprPI(((rew_seq (rew_free_id rew_id_in_id )) (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x1), (rew_id_in_predicate  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x2), (rew_id_in_expr  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x3))
			| ExprLambda(_x1, _x2, _x3) -> ExprLambda(((rew_seq (rew_free_id rew_id_in_id )) (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x1), (rew_id_in_predicate  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x2), (rew_id_in_expr  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x3)))_x)
      
  and rew_id_in_exprRecords  _bvars = 
    fun _x -> r_exprRecords((function
				 RecordsWithField(_x1) -> RecordsWithField(((rew_seq (rew_id_in_recordsItem )) _bvars _x1))
			       | Records(_x1) -> Records(((rew_seq (rew_id_in_expr )) _bvars _x1))
			       | RecordsAccess(_x1, _x2) -> RecordsAccess((rew_id_in_expr  _bvars _x1), (rew_free_id rew_id_in_id  _bvars _x2))
			       | RecordsSet(_x1) -> RecordsSet(((rew_seq (rew_id_in_recordsItem )) _bvars _x1)))_x)

  and rew_id_in_recordsItem  _bvars = 
    fun _x -> r_recordsItem((fun (_x1, _x2) -> (rew_free_id rew_id_in_id  _bvars _x1), (rew_id_in_expr  _bvars _x2))_x)
      
  and rew_id_in_exprSeq  _bvars = 
  fun _x -> r_exprSeq((function
    SeqEnum(_x1) -> SeqEnum(((rew_seq (rew_id_in_expr )) _bvars _x1))
  | SeqEmpty -> SeqEmpty )_x)

  and rew_id_in_exprSet  _bvars = 
  fun _x -> r_exprSet((function
    SetRange(_x1, _x2) -> SetRange((rew_id_in_expr  _bvars _x1), (rew_id_in_expr  _bvars _x2))
  | SetEnum(_x1) -> SetEnum(((rew_seq (rew_id_in_expr )) _bvars _x1))
  | SetCompr(_x1) -> SetCompr(((rew_seq (rew_id_in_expr )) _bvars _x1))
  | SetComprPred(_x1, _x2) -> SetComprPred(((rew_seq (rew_free_id rew_id_in_id )) (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x1), (rew_id_in_predicate  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x2))
  | SetEmpty -> SetEmpty 
  | SetUnionQuantify(_x1, _x2, _x3) -> SetUnionQuantify(((rew_seq (rew_free_id rew_id_in_id )) (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x1), (rew_id_in_predicate  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x2), (rew_id_in_expr  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x3))
  | SetInterQuantify(_x1, _x2, _x3) -> SetInterQuantify(((rew_seq (rew_free_id rew_id_in_id )) (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x1), (rew_id_in_predicate  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x2), (rew_id_in_expr  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x3)))_x)

  and rew_id_in_number  _bvars = 
  fun _x -> r_number((function
    MinNumber -> MinNumber 
  | Number(_x1) -> Number((rew_id_in_int  _bvars _x1))
  | MaxNumber -> MaxNumber )_x)

  and rew_id_in_substitution  _bvars = 
  fun _x -> r_substitution((function
    SubstOperCall(_x1, _x2, _x3) -> SubstOperCall((rew_id_in_ids  _bvars _x1), (rew_free_id rew_id_in_id  _bvars _x2), ((rew_seq (rew_id_in_expr )) _bvars _x3))
  | SubstBlock(_x1) -> SubstBlock((rew_id_in_substitution  _bvars _x1))
  | SubstPrecondition(_x1, _x2) -> SubstPrecondition((rew_id_in_predicate  _bvars _x1), (rew_id_in_substitution  _bvars _x2))
  | SubstAssertion(_x1, _x2) -> SubstAssertion((rew_id_in_predicate  _bvars _x1), (rew_id_in_substitution  _bvars _x2))
  | SubstChoice(_x1) -> SubstChoice(((rew_seq (rew_id_in_substitution )) _bvars _x1))
  | SubstIf(_x1, _x2) -> SubstIf(((rew_seq (fun _bvars (_x1, _x2) -> ((rew_id_in_predicate  _bvars _x1), (rew_id_in_substitution  _bvars _x2)))) _bvars _x1), ((rew_option (rew_id_in_substitution )) _bvars _x2))
  | SubstSelect(_x1, _x2) -> SubstSelect(((rew_seq (fun _bvars (_x1, _x2) -> ((rew_id_in_predicate  _bvars _x1), (rew_id_in_substitution  _bvars _x2)))) _bvars _x1), ((rew_option (rew_id_in_substitution )) _bvars _x2))
  | SubstCase(_x1, _x2, _x3) -> SubstCase((rew_id_in_expr  _bvars _x1), ((rew_seq (fun _bvars (_x1, _x2) -> (((rew_seq (rew_id_in_expr )) _bvars _x1), (rew_id_in_substitution  _bvars _x2)))) _bvars _x2), ((rew_option (rew_id_in_substitution )) _bvars _x3))
  | SubstAny(_x1, _x2, _x3) -> SubstAny(((rew_seq (rew_free_id rew_id_in_id )) (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x1), (rew_id_in_predicate  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x2), (rew_id_in_substitution  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x3))
  | SubstLet(_x1, _x2, _x3) -> SubstLet(((rew_seq (rew_free_id rew_id_in_id )) (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x1), (rew_id_in_predicate  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x2), (rew_id_in_substitution  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x3))
  | SubstVar(_x1, _x2) -> SubstVar(((rew_seq (rew_free_id rew_id_in_id )) (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x1), (rew_id_in_substitution  (Set_id.union _bvars (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))) _x2))
  | SubstWhile(_x1, _x2, _x3, _x4) -> SubstWhile((rew_id_in_predicate  _bvars _x1), (rew_id_in_substitution  _bvars _x2), (rew_id_in_expr  _bvars _x3), (rew_id_in_predicate  _bvars _x4))
  | SubstSkip -> SubstSkip 
  | SubstSequence(_x1, _x2) -> SubstSequence((rew_id_in_substitution  _bvars _x1), (rew_id_in_substitution  _bvars _x2))
  | SubstParallel(_x1, _x2) -> SubstParallel((rew_id_in_substitution  _bvars _x1), (rew_id_in_substitution  _bvars _x2))
  | SubstSetEqualIds(_x1, _x2) -> SubstSetEqualIds((rew_id_in_ids  _bvars _x1), ((rew_seq (rew_id_in_expr )) _bvars _x2))
  | SubstSetEqualFun(_x1, _x2, _x3) -> SubstSetEqualFun((rew_id_in_expr  _bvars _x1), ((rew_seq (rew_id_in_expr )) _bvars _x2), (rew_id_in_expr  _bvars _x3))
  | SubstSetEqualRecords(_x1, _x2) -> SubstSetEqualRecords((rew_id_in_exprRecords  _bvars _x1), (rew_id_in_expr  _bvars _x2))
  | SubstBecomeSuch(_x1, _x2) -> SubstBecomeSuch((rew_id_in_ids  _bvars _x1), (rew_id_in_predicate  _bvars _x2))
  | SubstSetIn(_x1, _x2) -> SubstSetIn((rew_id_in_ids  _bvars _x1), (rew_id_in_expr  _bvars _x2)))_x)

  and rew_id_in_ids  _bvars = 
  fun _x -> r_ids((fun (_x1) -> ((rew_seq (rew_free_id rew_id_in_id )) _bvars _x1))_x)

  and rew_id_in_origine  _bvars = 
  fun _x -> r_origine((function
    Local -> Local 
  | Remote(_x1) -> Remote(((rew_seq (fun _bvars (_x1, _x2) -> ((rew_id_in_string  _bvars _x1), (rew_id_in_string  _bvars _x2)))) _bvars _x1))
  | OrgRefine(_x1, _x2) -> OrgRefine((rew_id_in_string  _bvars _x1), ((rew_seq (fun _bvars (_x1, _x2) -> ((rew_id_in_string  _bvars _x1), (rew_id_in_string  _bvars _x2)))) _bvars _x2))
  | Prefix(_x1) -> Prefix(((rew_seq (rew_id_in_string )) _bvars _x1)))_x)

  and rew_id_in_id  _bvars = 
  fun _x -> r_id((function
    Id(_x1, _x2, _x3, _x4) -> Id((rew_id_in_origine  _bvars _x1), (rew_id_in_string  _bvars _x2), ((rew_option (rew_id_in_type_B )) _bvars _x3), _x4))_x)
  in
{  rew_id_in_amn = rew_id_in_amn Set_id.empty;
  rew_id_in_head = rew_id_in_head Set_id.empty;
  rew_id_in_clause = rew_id_in_clause Set_id.empty;
  rew_id_in_def = rew_id_in_def Set_id.empty;
  rew_id_in_defBody = rew_id_in_defBody Set_id.empty;
  rew_id_in_set = rew_id_in_set Set_id.empty;
  rew_id_in_instance = rew_id_in_instance Set_id.empty;
  rew_id_in_type_B = rew_id_in_type_B Set_id.empty;
  rew_id_in_field = rew_id_in_field Set_id.empty;
  rew_id_in_predicate = rew_id_in_predicate Set_id.empty;
  rew_id_in_operation = rew_id_in_operation Set_id.empty;
  rew_id_in_expr = rew_id_in_expr Set_id.empty;
  rew_id_in_exprRecords = rew_id_in_exprRecords Set_id.empty;
  rew_id_in_recordsItem = rew_id_in_recordsItem Set_id.empty;
  rew_id_in_exprSeq = rew_id_in_exprSeq Set_id.empty;
  rew_id_in_exprSet = rew_id_in_exprSet Set_id.empty;
  rew_id_in_number = rew_id_in_number Set_id.empty;
  rew_id_in_substitution = rew_id_in_substitution Set_id.empty;
  rew_id_in_ids = rew_id_in_ids Set_id.empty;
  rew_id_in_origine = rew_id_in_origine Set_id.empty;
  rew_id_in_id = rew_id_in_id Set_id.empty
}
