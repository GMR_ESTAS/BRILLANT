(* $Id$ *)

(* Warning btyper/blast <> bcaml/blast !! *)

(**  Abstract Syntax Tree for the B Language (BLAST) *)

(* ************************************************** *)
(* NE PAS DOCUMENTER CE FICHIER MAIS PLUTOT blast.mli *)
(* ************************************************** *)

type amn =
    Machine of head * clause list
  | Refinement of head * clause list
  | Implementation of head * clause list
  | EmptyTree
and head =
    id * id list
and clause =
  | Definitions of def list
  | Constraints of predicate
  | Invariant of predicate
  | Sets of set list
  | Initialisation of substitution
  | ConstantsConcrete of id list
  | ConstantsAbstract of id list
  | ConstantsHidden of id list
  | Properties of predicate
  | Values of (id * expr) list
  | VariablesConcrete of id list
  | VariablesAbstract of id list
  | VariablesHidden of id list
  | Promotes of id list
  | Assertions of predicate list
  | Operations of operation list
  | Sees of id list
  | Uses of id list
  | Extends of instance list
  | Includes of instance list
  | Imports of instance list
  | Refines of id

and def =
  | Def of id * id list * defBody
  | FileDef of string

and defBody =
(*  | PredDefBody of predicate *)
  | ExprDefBody of expr
  | SubstDefBody of substitution

and set =
  | SetAbstDec of id
  | SetEnumDec of id * id list

and instance = id * expr list

and type_B =
  | TypeNatSetRange of expr * expr
  | PredType of expr
  | TypeIdentifier of id
  | PFunType of type_B * type_B
  | FunType of type_B * type_B
  | ProdType of type_B * type_B
  | Sequence of type_B
  | SubType of type_B * expr
  | Fin_B of type_B
  | Untyped

and field =
    id * type_B

and predicate =
  | PredParen of predicate
  | PredNegation of predicate
  | PredExists of id list * predicate
  | PredForAll of id list * predicate
  | PredBin of Bbop.bop2 * predicate * predicate
  | PredAtom of Bbop.bop2 * expr * expr

 and operation = id * id list * id list * substitution

and expr =
  | ExprPred of predicate
  | ExprParen of expr
  | ExprId of id
  | ExprFunCall of expr * expr list
  | ExprBin of Bbop.op2 * expr * expr
  | ExprUn of Bbop.op1 * expr
  | ExprSeq of exprSeq
  | ExprSet of exprSet
  | RelSet of expr * expr
  | RelSeqComp of expr list
  | ExprNumber of number
  | ExprString of string
  | ExprNuplet of expr list
  | ExprSIGMA of id list * predicate * expr
  | ExprPI of id list * predicate * expr
  | ExprLambda of id list * predicate * expr
  | ExprSequence of expr * expr
  | ExprBool of bool
and exprSeq =
  | SeqEnum of expr list
  | SeqEmpty
and exprSet =
  | SetEmpty
  | SetEnum of expr list
  | SetCompr of expr list
  | SetComprPred of id list * predicate
  | SetUnionQuantify of id list * predicate * expr
  | SetInterQuantify of id list * predicate * expr
  | SetRange of expr * expr
and number =
  | MinNumber
  | Number of int32
  | MaxNumber
 and substitution =
  | SubstOperCall of id list * id * expr list
  | SubstBlock of substitution
  | SubstPrecondition of predicate * substitution
  | SubstAssertion of predicate * substitution
  | SubstChoice of substitution list
  | SubstIf of (predicate * substitution) list * substitution option
  | SubstSelect of (predicate * substitution) list * substitution option
  | SubstCase of expr * (expr list * substitution) list * substitution option
  | SubstAny of id list * predicate * substitution
  | SubstLet of id list * predicate * substitution
  | SubstVar of id list * substitution
  | SubstWhile of predicate * substitution * expr * predicate
  | SubstSkip
  | SubstSequence of substitution * substitution
  | SubstParallel of substitution * substitution
  | SubstSetEqualIds of id list * expr list
  | SubstSetEqualFun of expr * expr list * expr
  | SubstBecomeSuch of id list * predicate
  | SubstSetIn of id list * expr

and origin =
  | Local
  | Remote of (string * string) list
  | OrgRefine of string * (string * string) list
  | Prefix of string list
and id =
    Id of origin * string * type_B option * Bbop.kind
