(*************************************************************)
(*                PROJET BCAML                               *)
(*                                                           *)
(* http://www3.inrets.fr/Public/ESTAS/Mariano.Georges/Bcaml/ *)
(*                                                           *)
(*                   IRIT                                    *)
(*                                                           *)
(*   source genere le 14-05-2001  09:04:43                   *)
(*************************************************************)

module type BlastSig = sig
  type amn_ty
  type head_ty
  type clause_ty
  type def_ty
  type defBody_ty
  type set_ty
  type instance_ty
  type type_B_ty
  type field_ty
  type predicate_ty
  type operation_ty
  type expr_ty
  type exprSeq_ty
  type exprSet_ty
  type number_ty
  type substitution_ty
  type ids_ty
  type origine_ty
  type id_ty
  val mk_Machine: head_ty ->  clause_ty list -> amn_ty
  val mk_Refinement: head_ty ->  clause_ty list -> amn_ty
  val mk_Implementation: head_ty ->  clause_ty list -> amn_ty
  val mk_EmptyTree: amn_ty
  val mk_head: id_ty ->  ids_ty -> head_ty
  val mk_Definitions: def_ty list -> clause_ty
  val mk_Constraints: predicate_ty -> clause_ty
  val mk_Invariant: predicate_ty -> clause_ty
  val mk_Sets: set_ty list -> clause_ty
  val mk_Initialisation: substitution_ty -> clause_ty
  val mk_ConstantsConcrete: ids_ty -> clause_ty
  val mk_ConstantsAbstract: ids_ty -> clause_ty
  val mk_ConstantsHidden: ids_ty -> clause_ty
  val mk_Properties: predicate_ty -> clause_ty
  val mk_Values: (id_ty * expr_ty) list -> clause_ty
  val mk_VariablesConcrete: ids_ty -> clause_ty
  val mk_VariablesAbstract: ids_ty -> clause_ty
  val mk_VariablesHidden: ids_ty -> clause_ty
  val mk_Promotes: ids_ty -> clause_ty
  val mk_Assertions: predicate_ty list -> clause_ty
  val mk_Operations: operation_ty list -> clause_ty
  val mk_Sees: ids_ty -> clause_ty
  val mk_Uses: ids_ty -> clause_ty
  val mk_Extends: instance_ty list -> clause_ty
  val mk_Includes: instance_ty list -> clause_ty
  val mk_Imports: instance_ty list -> clause_ty
  val mk_Refines: id_ty -> clause_ty
  val mk_Def: id_ty ->  ids_ty ->  defBody_ty -> def_ty
  val mk_FileDef: string -> def_ty
  val mk_ExprDefBody: expr_ty -> defBody_ty
  val mk_SubstDefBody: substitution_ty -> defBody_ty
  val mk_SetAbstDec: id_ty -> set_ty
  val mk_SetEnumDec: id_ty ->  ids_ty -> set_ty
  val mk_instance: id_ty ->  expr_ty list -> instance_ty
  val mk_TypeNatSetRange: expr_ty ->  expr_ty -> type_B_ty
  val mk_PredType: expr_ty -> type_B_ty
  val mk_TypeIdentifier: id_ty -> type_B_ty
  val mk_PFunType: type_B_ty ->  type_B_ty -> type_B_ty
  val mk_FunType: type_B_ty ->  type_B_ty -> type_B_ty
  val mk_ProdType: type_B_ty ->  type_B_ty -> type_B_ty
  val mk_Sequence: type_B_ty -> type_B_ty
  val mk_SubType: type_B_ty ->  expr_ty -> type_B_ty
  val mk_Fin: type_B_ty -> type_B_ty 
  val mk_Untyped: type_B_ty
  val mk_field: id_ty ->  type_B_ty -> field_ty
  val mk_PredParen: predicate_ty -> predicate_ty
  val mk_PredNegation: predicate_ty -> predicate_ty
  val mk_PredExists: id_ty list ->  predicate_ty -> predicate_ty
  val mk_PredForAll: id_ty list ->  predicate_ty -> predicate_ty
  val mk_PredBin: Bbop.bop2 ->  predicate_ty ->  predicate_ty -> predicate_ty
  val mk_PredAtom: Bbop.bop2 ->  expr_ty ->  expr_ty -> predicate_ty
  val mk_operation: id_ty ->  ids_ty ->  ids_ty ->  substitution_ty -> operation_ty
  val mk_ExprSequence: expr_ty ->  expr_ty -> expr_ty
  val mk_ExprParen: expr_ty -> expr_ty
  val mk_ExprId: id_ty -> expr_ty
  val mk_ExprFunCall: expr_ty ->  expr_ty list -> expr_ty
  val mk_ExprBin: Bbop.op2 ->  expr_ty ->  expr_ty -> expr_ty
  val mk_ExprUn: Bbop.op1 ->  expr_ty -> expr_ty
  val mk_ExprSeq: exprSeq_ty -> expr_ty
  val mk_ExprSet: exprSet_ty -> expr_ty
  val mk_RelSet: expr_ty ->  expr_ty -> expr_ty
  val mk_RelSeqComp: expr_ty list -> expr_ty
  val mk_ExprNumber: number_ty -> expr_ty
  val mk_ExprBool: bool -> expr_ty
  val mk_ExprString: string -> expr_ty
  val mk_ExprPred: predicate_ty -> expr_ty
  val mk_ExprNuplet: expr_ty list -> expr_ty
  val mk_ExprSIGMA: id_ty list ->  predicate_ty ->  expr_ty -> expr_ty
  val mk_ExprPI: id_ty list ->  predicate_ty ->  expr_ty -> expr_ty
  val mk_ExprLambda: id_ty list ->  predicate_ty ->  expr_ty -> expr_ty
  val mk_SeqEnum: expr_ty list -> exprSeq_ty
  val mk_SeqEmpty: exprSeq_ty
  val mk_SetRange: expr_ty ->  expr_ty -> exprSet_ty
  val mk_SetEnum: expr_ty list -> exprSet_ty
  val mk_SetCompr: expr_ty list -> exprSet_ty
  val mk_SetComprPred: id_ty list ->  predicate_ty -> exprSet_ty
  val mk_SetEmpty: exprSet_ty
  val mk_SetUnionQuantify: id_ty list ->  predicate_ty ->  expr_ty -> exprSet_ty
  val mk_SetInterQuantify: id_ty list ->  predicate_ty ->  expr_ty -> exprSet_ty
  val mk_MinNumber: number_ty
  val mk_Number: int32 -> number_ty
  val mk_MaxNumber: number_ty
  val mk_SubstOperCall: ids_ty ->  id_ty ->  expr_ty list -> substitution_ty
  val mk_SubstBlock: substitution_ty -> substitution_ty
  val mk_SubstPrecondition: predicate_ty ->  substitution_ty -> substitution_ty
  val mk_SubstAssertion: predicate_ty ->  substitution_ty -> substitution_ty
  val mk_SubstChoice: substitution_ty list -> substitution_ty
  val mk_SubstIf: (predicate_ty * substitution_ty) list ->  substitution_ty option -> substitution_ty
  val mk_SubstSelect: (predicate_ty * substitution_ty) list ->  substitution_ty option -> substitution_ty
  val mk_SubstCase: expr_ty ->  (expr_ty list * substitution_ty) list ->  substitution_ty option -> substitution_ty
  val mk_SubstAny: id_ty list ->  predicate_ty ->  substitution_ty -> substitution_ty
  val mk_SubstLet: id_ty list ->  predicate_ty ->  substitution_ty -> substitution_ty
  val mk_SubstVar: id_ty list ->  substitution_ty -> substitution_ty
  val mk_SubstWhile: predicate_ty ->  substitution_ty ->  expr_ty ->  predicate_ty -> substitution_ty
  val mk_SubstSkip: substitution_ty
  val mk_SubstSequence: substitution_ty ->  substitution_ty -> substitution_ty
  val mk_SubstParallel: substitution_ty ->  substitution_ty -> substitution_ty
  val mk_SubstSetEqualIds: ids_ty ->  expr_ty list -> substitution_ty
  val mk_SubstSetEqualFun: expr_ty ->  expr_ty list ->  expr_ty -> substitution_ty

  val mk_SubstBecomeSuch: ids_ty ->  predicate_ty -> substitution_ty
  val mk_SubstSetIn: ids_ty ->  expr_ty -> substitution_ty
  val mk_ids: id_ty list -> ids_ty
  val mk_Local: origine_ty
  val mk_Remote: (string * string) list -> origine_ty
  val mk_OrgRefine: string ->  (string * string) list -> origine_ty
  val mk_Prefix: string list -> origine_ty
  val mk_Id: origine_ty ->  string ->  type_B_ty option ->  Bbop.kind -> id_ty
end

open Blast
let it_option f = function Some o -> Some (f o) | None -> None
module Blast_it =
  functor (MS_:BlastSig) ->
  struct
    let rec it_amn = function
        Machine(_x1, _x2) ->
        let _y1 = (it_head _x1) in
        let _y2 = ((List.map it_clause) _x2) in
        MS_.mk_Machine _y1 _y2
      | Refinement(_x1, _x2) ->
        let _y1 = (it_head _x1) in
        let _y2 = ((List.map it_clause) _x2) in
        MS_.mk_Refinement _y1 _y2
      | Implementation(_x1, _x2) ->
        let _y1 = (it_head _x1) in
        let _y2 = ((List.map it_clause) _x2) in
        MS_.mk_Implementation _y1 _y2
      | EmptyTree ->
        
        MS_.mk_EmptyTree
    and it_head (_x1, _x2) = let _y1 = (it_id _x1) in
        let _y2 = (it_ids _x2) in
        MS_.mk_head _y1 _y2
    and it_clause = function
        Definitions(_x1) ->
        let _y1 = ((List.map it_def) _x1) in
        MS_.mk_Definitions _y1
      | Constraints(_x1) ->
        let _y1 = (it_predicate _x1) in
        MS_.mk_Constraints _y1
      | Invariant(_x1) ->
        let _y1 = (it_predicate _x1) in
        MS_.mk_Invariant _y1
      | Sets(_x1) ->
        let _y1 = ((List.map it_set) _x1) in
        MS_.mk_Sets _y1
      | Initialisation(_x1) ->
        let _y1 = (it_substitution _x1) in
        MS_.mk_Initialisation _y1
      | ConstantsConcrete(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_ConstantsConcrete _y1
      | ConstantsAbstract(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_ConstantsAbstract _y1
      | ConstantsHidden(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_ConstantsHidden _y1
      | Properties(_x1) ->
        let _y1 = (it_predicate _x1) in
        MS_.mk_Properties _y1
      | Values(_x1) ->
        let _y1 = ((List.map (fun (_x1, _x2) -> let _y1 = (it_id _x1) in
        let _y2 = (it_expr _x2) in
        (_y1, _y2))) _x1) in
        MS_.mk_Values _y1
      | VariablesConcrete(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_VariablesConcrete _y1
      | VariablesAbstract(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_VariablesAbstract _y1
      | VariablesHidden(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_VariablesHidden _y1
      | Promotes(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_Promotes _y1
      | Assertions(_x1) ->
        let _y1 = ((List.map it_predicate) _x1) in
        MS_.mk_Assertions _y1
      | Operations(_x1) ->
        let _y1 = ((List.map it_operation) _x1) in
        MS_.mk_Operations _y1
      | Sees(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_Sees _y1
      | Uses(_x1) ->
        let _y1 = (it_ids _x1) in
        MS_.mk_Uses _y1
      | Extends(_x1) ->
        let _y1 = ((List.map it_instance) _x1) in
        MS_.mk_Extends _y1
      | Includes(_x1) ->
        let _y1 = ((List.map it_instance) _x1) in
        MS_.mk_Includes _y1
      | Imports(_x1) ->
        let _y1 = ((List.map it_instance) _x1) in
        MS_.mk_Imports _y1
      | Refines(_x1) ->
        let _y1 = (it_id _x1) in
        MS_.mk_Refines _y1
    and it_def = function
        Def(_x1, _x2, _x3) ->
        let _y1 = (it_id _x1) in
        let _y2 = (it_ids _x2) in
        let _y3 = (it_defBody _x3) in
        MS_.mk_Def _y1 _y2 _y3
      | FileDef(_x1) ->
        let _y1 = _x1 in
        MS_.mk_FileDef _y1
    and it_defBody = function
        ExprDefBody(_x1) ->
        let _y1 = (it_expr _x1) in
        MS_.mk_ExprDefBody _y1
      | SubstDefBody(_x1) ->
        let _y1 = (it_substitution _x1) in
        MS_.mk_SubstDefBody _y1
    and it_set = function
        SetAbstDec(_x1) ->
        let _y1 = (it_id _x1) in
        MS_.mk_SetAbstDec _y1
      | SetEnumDec(_x1, _x2) ->
        let _y1 = (it_id _x1) in
        let _y2 = (it_ids _x2) in
        MS_.mk_SetEnumDec _y1 _y2
    and it_instance (_x1, _x2) = let _y1 = (it_id _x1) in
        let _y2 = ((List.map it_expr) _x2) in
        MS_.mk_instance _y1 _y2
    and it_type_B = function
        TypeNatSetRange(_x1, _x2) ->
        let _y1 = (it_expr _x1) in
        let _y2 = (it_expr _x2) in
        MS_.mk_TypeNatSetRange _y1 _y2
      | PredType(_x1) ->
        let _y1 = (it_expr _x1) in
        MS_.mk_PredType _y1
      | TypeIdentifier(_x1) ->
        let _y1 = (it_id _x1) in
        MS_.mk_TypeIdentifier _y1
      | PFunType(_x1, _x2) ->
        let _y1 = (it_type_B _x1) in
        let _y2 = (it_type_B _x2) in
        MS_.mk_PFunType _y1 _y2
      | FunType(_x1, _x2) ->
        let _y1 = (it_type_B _x1) in
        let _y2 = (it_type_B _x2) in
        MS_.mk_FunType _y1 _y2
      | ProdType(_x1, _x2) ->
        let _y1 = (it_type_B _x1) in
        let _y2 = (it_type_B _x2) in
        MS_.mk_ProdType _y1 _y2
      | Sequence(_x1) ->
        let _y1 = (it_type_B _x1) in
        MS_.mk_Sequence _y1
      | SubType(_x1, _x2) ->
        let _y1 = (it_type_B _x1) in
        let _y2 = (it_expr _x2) in
        MS_.mk_SubType _y1 _y2
      | Fin_B(_x1) ->
          let _y1 = (it_type_B _x1) in MS_.mk_Fin _y1 
      | Untyped ->
        
        MS_.mk_Untyped
    and it_field (_x1, _x2) = let _y1 = (it_id _x1) in
        let _y2 = (it_type_B _x2) in
        MS_.mk_field _y1 _y2
    and it_predicate = function
        PredParen(_x1) ->
        let _y1 = (it_predicate _x1) in
        MS_.mk_PredParen _y1
      | PredNegation(_x1) ->
        let _y1 = (it_predicate _x1) in
        MS_.mk_PredNegation _y1
      | PredExists(_x1, _x2) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        MS_.mk_PredExists _y1 _y2
      | PredForAll(_x1, _x2) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        MS_.mk_PredForAll _y1 _y2
      | PredBin(_x1, _x2, _x3) ->
        let _y1 = _x1 in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_predicate _x3) in
        MS_.mk_PredBin _y1 _y2 _y3
      | PredAtom(_x1, _x2, _x3) ->
        let _y1 = _x1 in
        let _y2 = (it_expr _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_PredAtom _y1 _y2 _y3
    and it_operation (_x1, _x2, _x3, _x4) = let _y1 = (it_id _x1) in
        let _y2 = (it_ids _x2) in
        let _y3 = (it_ids _x3) in
        let _y4 = (it_substitution _x4) in
        MS_.mk_operation _y1 _y2 _y3 _y4
    and it_expr = function
        ExprSequence(_x1, _x2) ->
        let _y1 = (it_expr _x1) in
        let _y2 = (it_expr _x2) in
        MS_.mk_ExprSequence _y1 _y2
      | ExprParen(_x1) ->
        let _y1 = (it_expr _x1) in
        MS_.mk_ExprParen _y1
      | ExprId(_x1) ->
        let _y1 = (it_id _x1) in
        MS_.mk_ExprId _y1
      | ExprFunCall(_x1, _x2) ->
        let _y1 = (it_expr _x1) in
        let _y2 = ((List.map it_expr) _x2) in
        MS_.mk_ExprFunCall _y1 _y2
      | ExprBin(_x1, _x2, _x3) ->
        let _y1 = _x1 in
        let _y2 = (it_expr _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_ExprBin _y1 _y2 _y3
      | ExprUn(_x1, _x2) ->
        let _y1 = _x1 in
        let _y2 = (it_expr _x2) in
        MS_.mk_ExprUn _y1 _y2
      | ExprSeq(_x1) ->
        let _y1 = (it_exprSeq _x1) in
        MS_.mk_ExprSeq _y1
      | ExprSet(_x1) ->
        let _y1 = (it_exprSet _x1) in
        MS_.mk_ExprSet _y1
      | RelSet(_x1, _x2) ->
        let _y1 = (it_expr _x1) in
        let _y2 = (it_expr _x2) in
        MS_.mk_RelSet _y1 _y2
      | RelSeqComp(_x1) ->
        let _y1 = ((List.map it_expr) _x1) in
        MS_.mk_RelSeqComp _y1
      | ExprNumber(_x1) ->
        let _y1 = (it_number _x1) in
        MS_.mk_ExprNumber _y1
      | ExprBool(_x1) ->
        let _y1 = _x1 in
        MS_.mk_ExprBool _y1
      | ExprString(_x1) ->
        let _y1 = _x1 in
        MS_.mk_ExprString _y1
      | ExprPred(_x1) ->
        let _y1 = (it_predicate _x1) in
        MS_.mk_ExprPred _y1
      | ExprNuplet(_x1) ->
        let _y1 = ((List.map it_expr) _x1) in
        MS_.mk_ExprNuplet _y1
      | ExprSIGMA(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_ExprSIGMA _y1 _y2 _y3
      | ExprPI(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_ExprPI _y1 _y2 _y3
      | ExprLambda(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_ExprLambda _y1 _y2 _y3
    and it_exprSeq = function
        SeqEnum(_x1) ->
        let _y1 = ((List.map it_expr) _x1) in
        MS_.mk_SeqEnum _y1
      | SeqEmpty ->
        
        MS_.mk_SeqEmpty
    and it_exprSet = function
        SetRange(_x1, _x2) ->
        let _y1 = (it_expr _x1) in
        let _y2 = (it_expr _x2) in
        MS_.mk_SetRange _y1 _y2
      | SetEnum(_x1) ->
        let _y1 = ((List.map it_expr) _x1) in
        MS_.mk_SetEnum _y1
      | SetCompr(_x1) ->
        let _y1 = ((List.map it_expr) _x1) in
        MS_.mk_SetCompr _y1
      | SetComprPred(_x1, _x2) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        MS_.mk_SetComprPred _y1 _y2
      | SetEmpty ->
        MS_.mk_SetEmpty
      | SetUnionQuantify(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_SetUnionQuantify _y1 _y2 _y3
      | SetInterQuantify(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_SetInterQuantify _y1 _y2 _y3
    and it_number = function
        MinNumber ->
        MS_.mk_MinNumber
      | Number(_x1) ->
        let _y1 = _x1 in
        MS_.mk_Number _y1
      | MaxNumber ->        
        MS_.mk_MaxNumber
    and it_substitution = function
        SubstOperCall(_x1, _x2, _x3) ->
        let _y1 = (it_ids _x1) in
        let _y2 = (it_id _x2) in
        let _y3 = ((List.map it_expr) _x3) in
        MS_.mk_SubstOperCall _y1 _y2 _y3
      | SubstBlock(_x1) ->
        let _y1 = (it_substitution _x1) in
        MS_.mk_SubstBlock _y1
      | SubstPrecondition(_x1, _x2) ->
        let _y1 = (it_predicate _x1) in
        let _y2 = (it_substitution _x2) in
        MS_.mk_SubstPrecondition _y1 _y2
      | SubstAssertion(_x1, _x2) ->
        let _y1 = (it_predicate _x1) in
        let _y2 = (it_substitution _x2) in
        MS_.mk_SubstAssertion _y1 _y2
      | SubstChoice(_x1) ->
        let _y1 = ((List.map it_substitution) _x1) in
        MS_.mk_SubstChoice _y1
      | SubstIf(_x1, _x2) ->
        let _y1 = ((List.map (fun (_x1, _x2) -> let _y1 = (it_predicate _x1) in
        let _y2 = (it_substitution _x2) in
        (_y1, _y2))) _x1) in
        let _y2 = ((it_option it_substitution) _x2) in
        MS_.mk_SubstIf _y1 _y2
      | SubstSelect(_x1, _x2) ->
        let _y1 = ((List.map (fun (_x1, _x2) -> let _y1 = (it_predicate _x1) in
        let _y2 = (it_substitution _x2) in
        (_y1, _y2))) _x1) in
        let _y2 = ((it_option it_substitution) _x2) in
        MS_.mk_SubstSelect _y1 _y2
      | SubstCase(_x1, _x2, _x3) ->
        let _y1 = (it_expr _x1) in
        let _y2 = ((List.map (fun (_x1, _x2) -> let _y1 = ((List.map it_expr) _x1) in
        let _y2 = (it_substitution _x2) in
        (_y1, _y2))) _x2) in
        let _y3 = ((it_option it_substitution) _x3) in
        MS_.mk_SubstCase _y1 _y2 _y3
      | SubstAny(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_substitution _x3) in
        MS_.mk_SubstAny _y1 _y2 _y3
      | SubstLet(_x1, _x2, _x3) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_predicate _x2) in
        let _y3 = (it_substitution _x3) in
        MS_.mk_SubstLet _y1 _y2 _y3
      | SubstVar(_x1, _x2) ->
        let _y1 = ((List.map it_id) _x1) in
        let _y2 = (it_substitution _x2) in
        MS_.mk_SubstVar _y1 _y2
      | SubstWhile(_x1, _x2, _x3, _x4) ->
        let _y1 = (it_predicate _x1) in
        let _y2 = (it_substitution _x2) in
        let _y3 = (it_expr _x3) in
        let _y4 = (it_predicate _x4) in
        MS_.mk_SubstWhile _y1 _y2 _y3 _y4
      | SubstSkip ->        
        MS_.mk_SubstSkip
      | SubstSequence(_x1, _x2) ->
        let _y1 = (it_substitution _x1) in
        let _y2 = (it_substitution _x2) in
        MS_.mk_SubstSequence _y1 _y2
      | SubstParallel(_x1, _x2) ->
        let _y1 = (it_substitution _x1) in
        let _y2 = (it_substitution _x2) in
        MS_.mk_SubstParallel _y1 _y2
      | SubstSetEqualIds(_x1, _x2) ->
        let _y1 = (it_ids _x1) in
        let _y2 = ((List.map it_expr) _x2) in
        MS_.mk_SubstSetEqualIds _y1 _y2
      | SubstSetEqualFun(_x1, _x2, _x3) ->
        let _y1 = (it_expr _x1) in
        let _y2 = ((List.map it_expr) _x2) in
        let _y3 = (it_expr _x3) in
        MS_.mk_SubstSetEqualFun _y1 _y2 _y3
      | SubstBecomeSuch(_x1, _x2) ->
        let _y1 = (it_ids _x1) in
        let _y2 = (it_predicate _x2) in
        MS_.mk_SubstBecomeSuch _y1 _y2
      | SubstSetIn(_x1, _x2) ->
        let _y1 = (it_ids _x1) in
        let _y2 = (it_expr _x2) in
        MS_.mk_SubstSetIn _y1 _y2
    and it_ids (_x1) = let _y1 = ((List.map it_id) _x1) in
        MS_.mk_ids _y1
    and it_origine = function
        Local ->
        
        MS_.mk_Local
      | Remote(_x1) ->
        let _y1 = ((List.map (fun (_x1, _x2) -> let _y1 = _x1 in
        let _y2 = _x2 in
        (_y1, _y2))) _x1) in
        MS_.mk_Remote _y1
      | OrgRefine(_x1, _x2) ->
        let _y1 = _x1 in
        let _y2 = ((List.map (fun (_x1, _x2) -> let _y1 = _x1 in
        let _y2 = _x2 in
        (_y1, _y2))) _x2) in
        MS_.mk_OrgRefine _y1 _y2
      | Prefix(_x1) ->
        let _y1 = _x1 in
        MS_.mk_Prefix _y1
    and it_id = function
        Id(_x1, _x2, _x3, _x4) ->
        let _y1 = (it_origine _x1) in
        let _y2 = _x2 in
        let _y3 = ((it_option it_type_B) _x3) in
        let _y4 = _x4 in
        MS_.mk_Id _y1 _y2 _y3 _y4
  end
