(*************************************************************)
(*                PROJET BCAML                               *)
(*                                                           *)
(* http://www3.inrets.fr/Public/ESTAS/Mariano.Georges/Bcaml/ *)
(*                                                           *)
(*                   IRIT                                    *)
(*                                                           *)
(*   source genere le 14-05-2001  09:04:43                   *)
(*************************************************************)

open Blast
open Blast_lib
module Set_string = Set.Make (
struct 
  type t = string
  let compare = compare
end)
let free_id_in_bool _ = Set_id.empty
let free_id_in_int _ = Set_id.empty
let l_union_id ls = List.fold_left Set_id.union Set_id.empty ls
let free_seq_id f l = l_union_id (List.map f l)
let free_id_in_string _ = Set_id.empty
let free_option_id f = function Some x -> f x | _ -> Set_id.empty
let free_id_in_bool _ = Set_id.empty
let rec free_id_in_amn  = 
  fun _x -> ((function
    Machine(_x1, _x2) -> (l_union_id [(free_id_in_head  _x1); ((free_seq_id (free_id_in_clause )) _x2)])
  | Refinement(_x1, _x2) -> (l_union_id [(free_id_in_head  _x1); ((free_seq_id (free_id_in_clause )) _x2)])
  | Implementation(_x1, _x2) -> (l_union_id [(free_id_in_head  _x1); ((free_seq_id (free_id_in_clause )) _x2)])
  | EmptyTree -> Set_id.empty)_x)

  and free_id_in_head  = 
  fun _x -> ((fun (_x1, _x2) -> (l_union_id [(Set_id.singleton _x1); (free_id_in_ids  _x2)]))_x)

  and free_id_in_clause  = 
  fun _x -> ((function
    Definitions(_x1) -> ((free_seq_id (free_id_in_def )) _x1)
  | Constraints(_x1) -> (free_id_in_predicate  _x1)
  | Invariant(_x1) -> (free_id_in_predicate  _x1)
  | Sets(_x1) -> ((free_seq_id (free_id_in_set )) _x1)
  | Initialisation(_x1) -> (free_id_in_substitution  _x1)
  | ConstantsConcrete(_x1) -> (free_id_in_ids  _x1)
  | ConstantsAbstract(_x1) -> (free_id_in_ids  _x1)
  | ConstantsHidden(_x1) -> (free_id_in_ids  _x1)
  | Properties(_x1) -> (free_id_in_predicate  _x1)
  | Values(_x1) -> ((free_seq_id (fun (_x1, _x2) -> (l_union_id [(Set_id.singleton _x1); (free_id_in_expr  _x2)]))) _x1)
  | VariablesConcrete(_x1) -> (free_id_in_ids  _x1)
  | VariablesAbstract(_x1) -> (free_id_in_ids  _x1)
  | VariablesHidden(_x1) -> (free_id_in_ids  _x1)
  | Promotes(_x1) -> (free_id_in_ids  _x1)
  | Assertions(_x1) -> ((free_seq_id (free_id_in_predicate )) _x1)
  | Operations(_x1) -> ((free_seq_id (free_id_in_operation )) _x1)
  | Sees(_x1) -> (free_id_in_ids  _x1)
  | Uses(_x1) -> (free_id_in_ids  _x1)
  | Extends(_x1) -> ((free_seq_id (free_id_in_instance )) _x1)
  | Includes(_x1) -> ((free_seq_id (free_id_in_instance )) _x1)
  | Imports(_x1) -> ((free_seq_id (free_id_in_instance )) _x1)
  | Refines(_x1) -> (Set_id.singleton _x1))_x)

  and free_id_in_def  = 
  fun _x -> ((function
    Def(_x1, _x2, _x3) -> (l_union_id [(Set_id.singleton _x1); (free_id_in_ids  _x2); (free_id_in_defBody  _x3)])
  | FileDef(_x1) -> (free_id_in_string  _x1))_x)

  and free_id_in_defBody  = 
  fun _x -> ((function
    ExprDefBody(_x1) -> (free_id_in_expr  _x1)
  | SubstDefBody(_x1) -> (free_id_in_substitution  _x1))_x)

  and free_id_in_set  = 
  fun _x -> ((function
    SetAbstDec(_x1) -> (Set_id.singleton _x1)
  | SetEnumDec(_x1, _x2) -> (l_union_id [(Set_id.singleton _x1); (free_id_in_ids  _x2)])
  | SetRecordsDec(_x1, _x2) -> (l_union_id [(Set_id.singleton _x1); ((free_seq_id (free_id_in_recordsItem )) _x2)]))_x)

  and free_id_in_instance  = 
  fun _x -> ((fun (_x1, _x2) -> (l_union_id [(Set_id.singleton _x1); ((free_seq_id (free_id_in_expr )) _x2)]))_x)

  and free_id_in_type_B  = 
  fun _x -> ((function
    TypeNatSetRange(_x1, _x2) -> (l_union_id [(free_id_in_expr  _x1); (free_id_in_expr  _x2)])
  | PredType(_x1) -> (free_id_in_expr  _x1)
  | RecordsType(_x1) -> ((free_seq_id (free_id_in_field )) _x1)
  | TypeIdentifier(_x1) -> (Set_id.singleton _x1)
  | PFunType(_x1, _x2) -> (l_union_id [(free_id_in_type_B  _x1); (free_id_in_type_B  _x2)])
  | FunType(_x1, _x2) -> (l_union_id [(free_id_in_type_B  _x1); (free_id_in_type_B  _x2)])
  | ProdType(_x1, _x2) -> (l_union_id [(free_id_in_type_B  _x1); (free_id_in_type_B  _x2)])
  | Sequence(_x1) -> (free_id_in_type_B  _x1)
  | SubType(_x1, _x2) -> (l_union_id [(free_id_in_type_B  _x1); (free_id_in_expr  _x2)])
  | Fin(_x1) -> (free_id_in_type_B  _x1)
  | Untyped -> Set_id.empty)_x)

  and free_id_in_field  = 
  fun _x -> ((fun (_x1, _x2) -> (l_union_id [(Set_id.singleton _x1); (free_id_in_type_B  _x2)]))_x)

  and free_id_in_predicate  = 
  fun _x -> ((function
    PredParen(_x1) -> (free_id_in_predicate  _x1)
  | PredNegation(_x1) -> (free_id_in_predicate  _x1)
  | PredExists(_x1, _x2) -> (Set_id.diff (l_union_id [((free_seq_id (Set_id.singleton)) _x1); (free_id_in_predicate  _x2)]) (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1)))
  | PredForAll(_x1, _x2) -> (Set_id.diff (l_union_id [((free_seq_id (Set_id.singleton)) _x1); (free_id_in_predicate  _x2)]) (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1)))
  | PredBin(_x1, _x2, _x3) -> (l_union_id [(Bbop_free.free_id_in_bop2  _x1); (free_id_in_predicate  _x2); (free_id_in_predicate  _x3)])
  | PredAtom(_x1, _x2, _x3) -> (l_union_id [(Bbop_free.free_id_in_bop2  _x1); (free_id_in_expr  _x2); (free_id_in_expr  _x3)]))_x)

  and free_id_in_operation  = 
  fun _x -> ((fun (_x1, _x2, _x3, _x4) -> (l_union_id [(Set_id.singleton _x1); (free_id_in_ids  _x2); (free_id_in_ids  _x3); (free_id_in_substitution  _x4)]))_x)

  and free_id_in_expr  = 
  fun _x -> ((function
    ExprSequence(_x1, _x2) -> (l_union_id [(free_id_in_expr  _x1); (free_id_in_expr  _x2)])
  | ExprParen(_x1) -> (free_id_in_expr  _x1)
  | ExprId(_x1) -> (Set_id.singleton _x1)
  | ExprFunCall(_x1, _x2) -> (l_union_id [(free_id_in_expr  _x1); ((free_seq_id (free_id_in_expr )) _x2)])
  | ExprBin(_x1, _x2, _x3) -> (l_union_id [(Bbop_free.free_id_in_op2  _x1); (free_id_in_expr  _x2); (free_id_in_expr  _x3)])
  | ExprUn(_x1, _x2) -> (l_union_id [(Bbop_free.free_id_in_op1  _x1); (free_id_in_expr  _x2)])
  | ExprSeq(_x1) -> (free_id_in_exprSeq  _x1)
  | ExprSet(_x1) -> (free_id_in_exprSet  _x1)
  | RelSet(_x1, _x2) -> (l_union_id [(free_id_in_expr  _x1); (free_id_in_expr  _x2)])
  | RelSeqComp(_x1) -> ((free_seq_id (free_id_in_expr )) _x1)
  | ExprNumber(_x1) -> (free_id_in_number  _x1)
  | ExprBool(_x1) -> (free_id_in_bool  _x1)
  | ExprString(_x1) -> (free_id_in_string  _x1)
  | ExprRecords(_x1) -> (free_id_in_exprRecords  _x1)
  | ExprPred(_x1) -> (free_id_in_predicate  _x1)
  | ExprNuplet(_x1) -> ((free_seq_id (free_id_in_expr )) _x1)
  | ExprSIGMA(_x1, _x2, _x3) -> (Set_id.diff (l_union_id [((free_seq_id (Set_id.singleton)) _x1); (free_id_in_predicate  _x2); (free_id_in_expr  _x3)]) (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1)))
  | ExprPI(_x1, _x2, _x3) -> (Set_id.diff (l_union_id [((free_seq_id (Set_id.singleton)) _x1); (free_id_in_predicate  _x2); (free_id_in_expr  _x3)]) (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1)))
  | ExprLambda(_x1, _x2, _x3) -> (Set_id.diff (l_union_id [((free_seq_id (Set_id.singleton)) _x1); (free_id_in_predicate  _x2); (free_id_in_expr  _x3)]) (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))))_x)

  and free_id_in_exprRecords  = 
  fun _x -> ((function
    RecordsWithField(_x1) -> ((free_seq_id (free_id_in_recordsItem )) _x1)
  | Records(_x1) -> ((free_seq_id (free_id_in_expr )) _x1)
  | RecordsAccess(_x1, _x2) -> (l_union_id [(free_id_in_expr  _x1); (Set_id.singleton _x2)])
  | RecordsSet(_x1) -> ((free_seq_id (free_id_in_recordsItem )) _x1))_x)

  and free_id_in_recordsItem  = 
  fun _x -> ((fun (_x1, _x2) -> (l_union_id [(Set_id.singleton _x1); (free_id_in_expr  _x2)]))_x)

  and free_id_in_exprSeq  = 
  fun _x -> ((function
    SeqEnum(_x1) -> ((free_seq_id (free_id_in_expr )) _x1)
  | SeqEmpty -> Set_id.empty)_x)

  and free_id_in_exprSet  = 
  fun _x -> ((function
    SetRange(_x1, _x2) -> (l_union_id [(free_id_in_expr  _x1); (free_id_in_expr  _x2)])
  | SetEnum(_x1) -> ((free_seq_id (free_id_in_expr )) _x1)
  | SetCompr(_x1) -> ((free_seq_id (free_id_in_expr )) _x1)
  | SetComprPred(_x1, _x2) -> (Set_id.diff (l_union_id [((free_seq_id (Set_id.singleton)) _x1); (free_id_in_predicate  _x2)]) (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1)))
  | SetEmpty -> Set_id.empty
  | SetUnionQuantify(_x1, _x2, _x3) -> (Set_id.diff (l_union_id [((free_seq_id (Set_id.singleton)) _x1); (free_id_in_predicate  _x2); (free_id_in_expr  _x3)]) (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1)))
  | SetInterQuantify(_x1, _x2, _x3) -> (Set_id.diff (l_union_id [((free_seq_id (Set_id.singleton)) _x1); (free_id_in_predicate  _x2); (free_id_in_expr  _x3)]) (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1))))_x)

  and free_id_in_number  = 
  fun _x -> ((function
    MinNumber -> Set_id.empty
  | Number(_x1) -> (free_id_in_int  _x1)
  | MaxNumber -> Set_id.empty)_x)

  and free_id_in_substitution  = 
  fun _x -> ((function
    SubstOperCall(_x1, _x2, _x3) -> (l_union_id [(free_id_in_ids  _x1); (Set_id.singleton _x2); ((free_seq_id (free_id_in_expr )) _x3)])
  | SubstBlock(_x1) -> (free_id_in_substitution  _x1)
  | SubstPrecondition(_x1, _x2) -> (l_union_id [(free_id_in_predicate  _x1); (free_id_in_substitution  _x2)])
  | SubstAssertion(_x1, _x2) -> (l_union_id [(free_id_in_predicate  _x1); (free_id_in_substitution  _x2)])
  | SubstChoice(_x1) -> ((free_seq_id (free_id_in_substitution )) _x1)
  | SubstIf(_x1, _x2) -> (l_union_id [((free_seq_id (fun (_x1, _x2) -> (l_union_id [(free_id_in_predicate  _x1); (free_id_in_substitution  _x2)]))) _x1); ((free_option_id (free_id_in_substitution )) _x2)])
  | SubstSelect(_x1, _x2) -> (l_union_id [((free_seq_id (fun (_x1, _x2) -> (l_union_id [(free_id_in_predicate  _x1); (free_id_in_substitution  _x2)]))) _x1); ((free_option_id (free_id_in_substitution )) _x2)])
  | SubstCase(_x1, _x2, _x3) -> (l_union_id [(free_id_in_expr  _x1); ((free_seq_id (fun (_x1, _x2) -> (l_union_id [((free_seq_id (free_id_in_expr )) _x1); (free_id_in_substitution  _x2)]))) _x2); ((free_option_id (free_id_in_substitution )) _x3)])
  | SubstAny(_x1, _x2, _x3) -> (Set_id.diff (l_union_id [((free_seq_id (Set_id.singleton)) _x1); (free_id_in_predicate  _x2); (free_id_in_substitution  _x3)]) (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1)))
  | SubstLet(_x1, _x2, _x3) -> (Set_id.diff (l_union_id [((free_seq_id (Set_id.singleton)) _x1); (free_id_in_predicate  _x2); (free_id_in_substitution  _x3)]) (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1)))
  | SubstVar(_x1, _x2) -> (Set_id.diff (l_union_id [((free_seq_id (Set_id.singleton)) _x1); (free_id_in_substitution  _x2)]) (l_union_id (List.map (fun _x ->(Set_id.singleton _x))_x1)))
  | SubstWhile(_x1, _x2, _x3, _x4) -> (l_union_id [(free_id_in_predicate  _x1); (free_id_in_substitution  _x2); (free_id_in_expr  _x3); (free_id_in_predicate  _x4)])
  | SubstSkip -> Set_id.empty
  | SubstSequence(_x1, _x2) -> (l_union_id [(free_id_in_substitution  _x1); (free_id_in_substitution  _x2)])
  | SubstParallel(_x1, _x2) -> (l_union_id [(free_id_in_substitution  _x1); (free_id_in_substitution  _x2)])
  | SubstSetEqualIds(_x1, _x2) -> (l_union_id [(free_id_in_ids  _x1); ((free_seq_id (free_id_in_expr )) _x2)])
  | SubstSetEqualFun(_x1, _x2, _x3) -> (l_union_id [(free_id_in_expr  _x1); ((free_seq_id (free_id_in_expr )) _x2); (free_id_in_expr  _x3)])
  | SubstSetEqualRecords(_x1, _x2) -> (l_union_id [(free_id_in_exprRecords  _x1); (free_id_in_expr  _x2)])
  | SubstBecomeSuch(_x1, _x2) -> (l_union_id [(free_id_in_ids  _x1); (free_id_in_predicate  _x2)])
  | SubstSetIn(_x1, _x2) -> (l_union_id [(free_id_in_ids  _x1); (free_id_in_expr  _x2)]))_x)

  and free_id_in_ids  = 
  fun _x -> ((fun (_x1) -> ((free_seq_id (Set_id.singleton)) _x1))_x)

  and free_id_in_origine  = 
  fun _x -> ((function
    Local -> Set_id.empty
  | Remote(_x1) -> ((free_seq_id (fun (_x1, _x2) -> (l_union_id [(free_id_in_string  _x1); (free_id_in_string  _x2)]))) _x1)
  | OrgRefine(_x1, _x2) -> (l_union_id [(free_id_in_string  _x1); ((free_seq_id (fun (_x1, _x2) -> (l_union_id [(free_id_in_string  _x1); (free_id_in_string  _x2)]))) _x2)])
  | Prefix(_x1) -> ((free_seq_id (free_id_in_string )) _x1))_x)

  and free_id_in_id  = 
  fun _x -> ((function
    Id(_x1, _x2, _x3, _x4) -> (l_union_id [(free_id_in_origine  _x1); (free_id_in_string  _x2); ((free_option_id (free_id_in_type_B )) _x3); (Bbop_free.free_id_in_kind  _x4)]))_x)
let is_free_id_in_amn v t = Set_id.mem v (free_id_in_amn t)
let is_free_id_in_head v t = Set_id.mem v (free_id_in_head t)
let is_free_id_in_clause v t = Set_id.mem v (free_id_in_clause t)
let is_free_id_in_def v t = Set_id.mem v (free_id_in_def t)
let is_free_id_in_defBody v t = Set_id.mem v (free_id_in_defBody t)
let is_free_id_in_set v t = Set_id.mem v (free_id_in_set t)
let is_free_id_in_instance v t = Set_id.mem v (free_id_in_instance t)
let is_free_id_in_type_B v t = Set_id.mem v (free_id_in_type_B t)
let is_free_id_in_field v t = Set_id.mem v (free_id_in_field t)
let is_free_id_in_predicate v t = Set_id.mem v (free_id_in_predicate t)
let is_free_id_in_operation v t = Set_id.mem v (free_id_in_operation t)
let is_free_id_in_expr v t = Set_id.mem v (free_id_in_expr t)
let is_free_id_in_exprRecords v t = Set_id.mem v (free_id_in_exprRecords t)
let is_free_id_in_recordsItem v t = Set_id.mem v (free_id_in_recordsItem t)
let is_free_id_in_exprSeq v t = Set_id.mem v (free_id_in_exprSeq t)
let is_free_id_in_exprSet v t = Set_id.mem v (free_id_in_exprSet t)
let is_free_id_in_number v t = Set_id.mem v (free_id_in_number t)
let is_free_id_in_substitution v t = Set_id.mem v (free_id_in_substitution t)
let is_free_id_in_ids v t = Set_id.mem v (free_id_in_ids t)
let is_free_id_in_origine v t = Set_id.mem v (free_id_in_origine t)
let is_free_id_in_id v t = Set_id.mem v (free_id_in_id t)
