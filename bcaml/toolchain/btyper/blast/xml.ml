(************************************************************)
(*                                                          *)
(*               Generateur d'iterateur sur des types       *)
(*                          inductifs                       *)
(*                         IRIT   2000                      *)
(*                                                          *)
(************************************************************)
type xml =
    XString of string
  | XInt of int
  | XBool of bool
  | XBalise of string * xml list

let rec xml2str i = function
    XString s -> (String.make i ' ') ^ s ^ "\n"
  | XBool true -> (String.make i ' ') ^ "true" ^ "\n"
  | XBool false -> (String.make i ' ') ^ "false" ^ "\n"
  | XInt n -> (String.make i ' ') ^ (string_of_int n) ^ "\n"
  | XBalise (s,l) ->
      (String.make i ' ') ^ "<" ^ s ^ ">\n" ^
      (String.concat "" (List.map (xml2str (i+2)) l)) ^
      (String.make i ' ') ^ "</" ^ s ^ ">\n"

exception InvalidXml of string

let xmlparse_empty c l = if l <> [] then raise (InvalidXml c)

let xmlparse_string = function
    (XString s::l) -> s,l
  | _ -> raise (InvalidXml "string")

let xmlparse_bool = function
    (XBool s::l) -> s,l
  | _ -> raise (InvalidXml "bool")

let xmlparse_int = function
    (XInt s::l) -> s,l
  | _ -> raise (InvalidXml "int")

let xmlparse_option f = function
    (x::l) -> begin try Some (fst (f [x])),l with _ -> None,(x::l) end
  | [] -> None,[]

let rec xmlparse_seq f = function
  [] -> [],[]
  | x::l -> try 
      let y = fst (f [x]) in
      let r,nl = xmlparse_seq f l in
      y::r,nl
  with _ -> [],(x::l)

let p_xml_int s = [XInt s]

