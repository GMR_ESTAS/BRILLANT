(*************************************************************)
(*                PROJET BCAML                               *)
(*                                                           *)
(* http://www3.inrets.fr/Public/ESTAS/Mariano.Georges/Bcaml/ *)
(*                                                           *)
(*                   IRIT                                    *)
(*                                                           *)
(*   source genere le 14-05-2001  09:04:43                   *)
(*************************************************************)

open Blast
open Location

(* Constructeurs *)
let mk_Machine _x1 _x2 = Machine(_x1, _x2)
let mk_Refinement _x1 _x2 = Refinement(_x1, _x2)
let mk_Implementation _x1 _x2 = Implementation(_x1, _x2)
let mk_EmptyTree = EmptyTree
let mk_amn _x = _x
let mk_head _x1 _x2 = (_x1, _x2)
let mk_Definitions _x1 = Definitions(_x1)
let mk_Constraints _x1 =  Constraints(_x1)
let mk_Invariant _x1 = Invariant(_x1)
let mk_Sets _x1 = Sets(_x1)
let mk_Initialisation _x1 = Initialisation(_x1)
let mk_ConstantsConcrete _x1 = ConstantsConcrete(_x1)
let mk_ConstantsAbstract _x1 = ConstantsAbstract(_x1)
let mk_ConstantsHidden _x1 = ConstantsHidden(_x1)
let mk_Properties _x1 = Properties(_x1)
let mk_Values _x1 = Values(_x1)
let mk_VariablesConcrete _x1 = VariablesConcrete(_x1)
let mk_VariablesAbstract _x1 = VariablesAbstract(_x1)
let mk_VariablesHidden _x1 = VariablesHidden(_x1)
let mk_Promotes _x1 = Promotes(_x1)
let mk_Assertions _x1 = Assertions(_x1)
let mk_Operations _x1 = Operations(_x1)
let mk_Sees _x1 = Sees(_x1)
let mk_Uses _x1 = Uses(_x1)
let mk_Extends _x1 = Extends(_x1)
let mk_Includes _x1 = Includes(_x1)
let mk_Imports _x1 = Imports(_x1)
let mk_Refines _x1 = Refines(_x1)
let mk_clause _x = _x
let mk_Def _x1 _x2 _x3 = Def(_x1, _x2, _x3)
let mk_FileDef _x1 = FileDef(_x1)
let mk_def _x = _x
let mk_ExprDefBody _x1 = ExprDefBody(_x1)
let mk_SubstDefBody _x1 = SubstDefBody(_x1)
let mk_defBody _x = _x
let mk_SetAbstDec _x1 = SetAbstDec(_x1)
let mk_SetEnumDec _x1 _x2 = SetEnumDec(_x1, _x2)
let mk_set _x = _x
let mk_instance _x1 _x2 = (_x1, _x2)
let mk_TypeNatSetRange _x1 _x2 = TypeNatSetRange(_x1, _x2)
let mk_PredType _x1 = PredType(_x1)
let mk_TypeIdentifier _x1 = TypeIdentifier(_x1)
let mk_PFunType _x1 _x2 = PFunType(_x1, _x2)
let mk_FunType _x1 _x2 = FunType(_x1, _x2)
let mk_ProdType _x1 _x2 = ProdType(_x1, _x2)
let mk_Sequence _x1 = Sequence(_x1)
let mk_SubType _x1 _x2 = SubType(_x1, _x2)
let mk_Fin _x1 = Fin_B(_x1)
let mk_Untyped = Untyped
let mk_type_B _x = _x
let mk_field _x1 _x2 = (_x1, _x2)
let mk_PredParen _x1 = PredParen(_x1)
let mk_PredNegation _x1 = PredNegation(_x1)
let mk_PredExists _x1 _x2 = PredExists(_x1, _x2)
let mk_PredForAll _x1 _x2 = PredForAll(_x1, _x2)
let mk_PredBin _x1 _x2 _x3 = PredBin(_x1, _x2, _x3)
let mk_PredAtom _x1 _x2 _x3 = PredAtom(_x1, _x2, _x3)
let mk_predicate _x = _x
let mk_operation _x1 _x2 _x3 _x4 = (_x1, _x2, _x3, _x4)
let mk_ExprSequence _x1 _x2 = ExprSequence(_x1, _x2)
let mk_ExprParen _x1 = ExprParen(_x1)
let mk_ExprId _x1 = ExprId(_x1)
let mk_ExprFunCall _x1 _x2 = ExprFunCall(_x1, _x2)
let mk_ExprBin _x1 _x2 _x3 = ExprBin(_x1, _x2, _x3)
let mk_ExprUn _x1 _x2 = ExprUn(_x1, _x2)
let mk_ExprSeq _x1 = ExprSeq(_x1)
let mk_ExprSet _x1 = ExprSet(_x1)
let mk_RelSet _x1 _x2 = RelSet(_x1, _x2)
let mk_RelSeqComp _x1 = RelSeqComp(_x1)
let mk_ExprNumber _x1 = ExprNumber(_x1)
let mk_ExprBool _x1 = ExprBool(_x1)
let mk_ExprString _x1 = ExprString(_x1)
let mk_ExprPred _x1 = ExprPred(_x1)
let mk_ExprNuplet _x1 = ExprNuplet(_x1)
let mk_ExprSIGMA _x1 _x2 _x3 = ExprSIGMA(_x1, _x2, _x3)
let mk_ExprPI _x1 _x2 _x3 = ExprPI(_x1, _x2, _x3)
let mk_ExprLambda _x1 _x2 _x3 = ExprLambda(_x1, _x2, _x3)
let mk_expr _x = _x
let mk_SeqEnum _x1 = SeqEnum(_x1)
let mk_SeqEmpty = SeqEmpty
let mk_exprSeq _x = _x
let mk_SetRange _x1 _x2 = SetRange(_x1, _x2)
let mk_SetEnum _x1 = SetEnum(_x1)
let mk_SetCompr _x1 = SetCompr(_x1)
let mk_SetComprPred _x1 _x2 = SetComprPred(_x1, _x2)
let mk_SetEmpty = SetEmpty
let mk_SetUnionQuantify _x1 _x2 _x3 = SetUnionQuantify(_x1, _x2, _x3)
let mk_SetInterQuantify _x1 _x2 _x3 = SetInterQuantify(_x1, _x2, _x3)
let mk_exprSet _x = _x
let mk_MinNumber = MinNumber
let mk_Number _x1 = Number(_x1)
let mk_MaxNumber = MaxNumber
let mk_number _x = _x
let mk_SubstOperCall _x1 _x2 _x3 = SubstOperCall(_x1, _x2, _x3)
let mk_SubstBlock _x1 = SubstBlock(_x1)
let mk_SubstPrecondition _x1 _x2 = SubstPrecondition(_x1, _x2)
let mk_SubstAssertion _x1 _x2 = SubstAssertion(_x1, _x2)
let mk_SubstChoice _x1 = SubstChoice(_x1)
let mk_SubstIf _x1 _x2 = SubstIf(_x1, _x2)
let mk_SubstSelect _x1 _x2 = SubstSelect(_x1, _x2)
let mk_SubstCase _x1 _x2 _x3 = SubstCase(_x1, _x2, _x3)
let mk_SubstAny _x1 _x2 _x3 = SubstAny(_x1, _x2, _x3)
let mk_SubstLet _x1 _x2 _x3 = SubstLet(_x1, _x2, _x3)
let mk_SubstVar _x1 _x2 = SubstVar(_x1, _x2)
let mk_SubstWhile _x1 _x2 _x3 _x4 = SubstWhile(_x1, _x2, _x3, _x4)
let mk_SubstSkip = SubstSkip
let mk_SubstSequence _x1 _x2 = SubstSequence(_x1, _x2)
let mk_SubstParallel _x1 _x2 = SubstParallel(_x1, _x2)
let mk_SubstSetEqualIds _x1 _x2 = SubstSetEqualIds(_x1, _x2)
let mk_SubstSetEqualFun _x1 _x2 _x3 = SubstSetEqualFun(_x1, _x2, _x3)

let mk_SubstBecomeSuch _x1 _x2 = SubstBecomeSuch(_x1, _x2)
let mk_SubstSetIn _x1 _x2 = SubstSetIn(_x1, _x2)
let mk_substitution _x = _x
let mk_ids _x1 = (_x1)
let mk_Local = Local
let mk_Remote _x1 = Remote(_x1)
let mk_OrgRefine _x1 _x2 = OrgRefine(_x1, _x2)
let mk_Prefix _x1 = Prefix(_x1)
let mk_origine _x = _x
let mk_Id _x1 _x2 _x3 _x4 =  Id(_x1, _x2, _x3, _x4) 
let mk_id _x = _x

(* Tests *)
(* tests de amn *)

(*MG
let is_Machine = function
|  Machine _ -> true
|  _ -> false

let is_Refinement = function
|  Refinement _ -> true
|  _ -> false

let is_Implementation = function
|  Implementation _ -> true
|  _ -> false

let is_EmptyTree = function
|  EmptyTree   -> true
|  _ -> false

(* tests de head *)

(* tests de clause *)
let is_Definitions = function
|  Definitions _ -> true
|  _ -> false

let is_Constraints = function
|  Constraints _ -> true
|  _ -> false

let is_Invariant = function
|  Invariant _ -> true
|  _ -> false

let is_Sets = function
|  Sets _ -> true
|  _ -> false

let is_Initialisation = function
|  Initialisation _ -> true
|  _ -> false

let is_ConstantsConcrete = function
|  ConstantsConcrete _ -> true
|  _ -> false

let is_ConstantsAbstract = function
|  ConstantsAbstract _ -> true
|  _ -> false

let is_ConstantsHidden = function
|  ConstantsHidden _ -> true
|  _ -> false

let is_Properties = function
|  Properties _ -> true
|  _ -> false

let is_Values = function
|  Values _ -> true
|  _ -> false

let is_VariablesConcrete = function
|  VariablesConcrete _ -> true
|  _ -> false

let is_VariablesAbstract = function
|  VariablesAbstract _ -> true
|  _ -> false

let is_VariablesHidden = function
|  VariablesHidden _ -> true
|  _ -> false

let is_Promotes = function
|  Promotes _ -> true
|  _ -> false

let is_Assertions = function
|  Assertions _ -> true
|  _ -> false

let is_Operations = function
|  Operations _ -> true
|  _ -> false

let is_Sees = function
|  Sees _ -> true
|  _ -> false

let is_Uses = function
|  Uses _ -> true
|  _ -> false

let is_Extends = function
|  Extends _ -> true
|  _ -> false

let is_Includes = function
|  Includes _ -> true
|  _ -> false

let is_Imports = function
|  Imports _ -> true
|  _ -> false

let is_Refines = function
|  Refines _ -> true
|  _ -> false

(* tests de def *)
let is_Def = function
|  Def _ -> true
|  _ -> false

let is_FileDef = function
|  FileDef _ -> true
|  _ -> false

(* tests de defBody *)
let is_ExprDefBody = function
|  ExprDefBody _ -> true
|  _ -> false

let is_SubstDefBody = function
|  SubstDefBody _ -> true
|  _ -> false

(* tests de set *)
let is_SetAbstDec = function
|  SetAbstDec _ -> true
|  _ -> false

let is_SetEnumDec = function
|  SetEnumDec _ -> true
|  _ -> false


(* tests de instance *)

(* tests de type_B *)
let is_TypeNatSetRange = function
|  TypeNatSetRange _ -> true
|  _ -> false

let is_PredType = function
|  PredType _ -> true
|  _ -> false


let is_TypeIdentifier = function
|  TypeIdentifier _ -> true
|  _ -> false

let is_PFunType = function
|  PFunType _ -> true
|  _ -> false

let is_FunType = function
|  FunType _ -> true
|  _ -> false

let is_ProdType = function
|  ProdType _ -> true
|  _ -> false

let is_Sequence = function
|  Sequence _ -> true
|  _ -> false

let is_SubType = function
|  SubType _ -> true
|  _ -> false

let is_Fin = function
|  Fin _ -> true
|  _ -> false

let is_Untyped = function
|  Untyped   -> true
|  _ -> false

(* tests de field *)

(* tests de predicate *)
let is_PredParen = function
|  PredParen _ -> true
|  _ -> false

let is_PredNegation = function
|  PredNegation _ -> true
|  _ -> false

let is_PredExists = function
|  PredExists _ -> true
|  _ -> false

let is_PredForAll = function
|  PredForAll _ -> true
|  _ -> false

let is_PredBin = function
|  PredBin _ -> true
|  _ -> false

let is_PredAtom = function
|  PredAtom _ -> true
|  _ -> false

(* tests de operation *)

(* tests de expr *)
let is_ExprSequence = function
|  ExprSequence _ -> true
|  _ -> false

let is_ExprParen = function
|  ExprParen _ -> true
|  _ -> false

let is_ExprId = function
|  ExprId _ -> true
|  _ -> false

let is_ExprFunCall = function
|  ExprFunCall _ -> true
|  _ -> false

let is_ExprBin = function
|  ExprBin _ -> true
|  _ -> false

let is_ExprUn = function
|  ExprUn _ -> true
|  _ -> false

let is_ExprSeq = function
|  ExprSeq _ -> true
|  _ -> false

let is_ExprSet = function
|  ExprSet _ -> true
|  _ -> false

let is_RelSet = function
|  RelSet _ -> true
|  _ -> false

let is_RelSeqComp = function
|  RelSeqComp _ -> true
|  _ -> false

let is_ExprNumber = function
|  ExprNumber _ -> true
|  _ -> false

let is_ExprBool = function
|  ExprBool _ -> true
|  _ -> false

let is_ExprString = function
|  ExprString _ -> true
|  _ -> false


let is_ExprPred = function
|  ExprPred _ -> true
|  _ -> false

let is_ExprNuplet = function
|  ExprNuplet _ -> true
|  _ -> false

let is_ExprSIGMA = function
|  ExprSIGMA _ -> true
|  _ -> false

let is_ExprPI = function
|  ExprPI _ -> true
|  _ -> false

let is_ExprLambda = function
|  ExprLambda _ -> true
|  _ -> false


(* tests de recordsItem *)

(* tests de exprSeq *)
let is_SeqEnum = function
|  SeqEnum _ -> true
|  _ -> false

let is_SeqEmpty = function
|  SeqEmpty   -> true
|  _ -> false

(* tests de exprSet *)
let is_SetRange = function
|  SetRange _ -> true
|  _ -> false

let is_SetEnum = function
|  SetEnum _ -> true
|  _ -> false

let is_SetCompr = function
|  SetCompr _ -> true
|  _ -> false

let is_SetComprPred = function
|  SetComprPred _ -> true
|  _ -> false

let is_SetEmpty = function
|  SetEmpty   -> true
|  _ -> false

let is_SetUnionQuantify = function
|  SetUnionQuantify _ -> true
|  _ -> false

let is_SetInterQuantify = function
|  SetInterQuantify _ -> true
|  _ -> false

(* tests de number *)
let is_MinNumber = function
|  MinNumber   -> true
|  _ -> false

let is_Number = function
|  Number _ -> true
|  _ -> false

let is_MaxNumber = function
|  MaxNumber   -> true
|  _ -> false

(* tests de substitution *)
let is_SubstOperCall = function
|  SubstOperCall _ -> true
|  _ -> false

let is_SubstBlock = function
|  SubstBlock _ -> true
|  _ -> false

let is_SubstPrecondition = function
|  SubstPrecondition _ -> true
|  _ -> false

let is_SubstAssertion = function
|  SubstAssertion _ -> true
|  _ -> false

let is_SubstChoice = function
|  SubstChoice _ -> true
|  _ -> false

let is_SubstIf = function
|  SubstIf _ -> true
|  _ -> false

let is_SubstSelect = function
|  SubstSelect _ -> true
|  _ -> false

let is_SubstCase = function
|  SubstCase _ -> true
|  _ -> false

let is_SubstAny = function
|  SubstAny _ -> true
|  _ -> false

let is_SubstLet = function
|  SubstLet _ -> true
|  _ -> false

let is_SubstVar = function
|  SubstVar _ -> true
|  _ -> false

let is_SubstWhile = function
|  SubstWhile _ -> true
|  _ -> false

let is_SubstSkip = function
|  SubstSkip   -> true
|  _ -> false

let is_SubstSequence = function
|  SubstSequence _ -> true
|  _ -> false

let is_SubstParallel = function
|  SubstParallel _ -> true
|  _ -> false

let is_SubstSetEqualIds = function
|  SubstSetEqualIds _ -> true
|  _ -> false

let is_SubstSetEqualFun = function
|  SubstSetEqualFun _ -> true
|  _ -> false


let is_SubstBecomeSuch = function
|  SubstBecomeSuch _ -> true
|  _ -> false

let is_SubstSetIn = function
|  SubstSetIn _ -> true
|  _ -> false

(* tests de ids *)

(* tests de origine *)
let is_Local = function
|  Local   -> true
|  _ -> false

let is_Remote = function
|  Remote _ -> true
|  _ -> false

let is_OrgRefine = function
|  OrgRefine _ -> true
|  _ -> false

let is_Prefix = function
|  Prefix _ -> true
|  _ -> false

(* tests de id *)
let is_Id (t:id) = true

*)

(* Destructeurs *)
(* destructeurs de amn *)

(*MG le seul utilis� !!*)
let destr_Machine = function
|  Machine (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_Machine"

(*
let destr_Refinement = function
|  Refinement (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_Refinement"

let destr_Implementation = function
|  Implementation (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_Implementation"

let destr_EmptyTree = function
|  EmptyTree -> ()
|  _ -> failwith "destr_EmptyTree"

(* destructeurs de head *)

(* destructeurs de clause *)
let destr_Definitions = function
|  Definitions (_x1) -> _x1
|  _ -> failwith "destr_Definitions"

let destr_Constraints = function
|  Constraints (_x1) -> _x1
|  _ -> failwith "destr_Constraints"

let destr_Invariant = function
|  Invariant (_x1) -> _x1
|  _ -> failwith "destr_Invariant"

let destr_Sets = function
|  Sets (_x1) -> _x1
|  _ -> failwith "destr_Sets"

let destr_Initialisation = function
|  Initialisation (_x1) -> _x1
|  _ -> failwith "destr_Initialisation"

let destr_ConstantsConcrete = function
|  ConstantsConcrete (_x1) -> _x1
|  _ -> failwith "destr_ConstantsConcrete"

let destr_ConstantsAbstract = function
|  ConstantsAbstract (_x1) -> _x1
|  _ -> failwith "destr_ConstantsAbstract"

let destr_ConstantsHidden = function
|  ConstantsHidden (_x1) -> _x1
|  _ -> failwith "destr_ConstantsHidden"

let destr_Properties = function
|  Properties (_x1) -> _x1
|  _ -> failwith "destr_Properties"

let destr_Values = function
|  Values (_x1) -> _x1
|  _ -> failwith "destr_Values"

let destr_VariablesConcrete = function
|  VariablesConcrete (_x1) -> _x1
|  _ -> failwith "destr_VariablesConcrete"

let destr_VariablesAbstract = function
|  VariablesAbstract (_x1) -> _x1
|  _ -> failwith "destr_VariablesAbstract"

let destr_VariablesHidden = function
|  VariablesHidden (_x1) -> _x1
|  _ -> failwith "destr_VariablesHidden"

let destr_Promotes = function
|  Promotes (_x1) -> _x1
|  _ -> failwith "destr_Promotes"

let destr_Assertions = function
|  Assertions (_x1) -> _x1
|  _ -> failwith "destr_Assertions"

let destr_Operations = function
|  Operations (_x1) -> _x1
|  _ -> failwith "destr_Operations"

let destr_Sees = function
|  Sees (_x1) -> _x1
|  _ -> failwith "destr_Sees"

let destr_Uses = function
|  Uses (_x1) -> _x1
|  _ -> failwith "destr_Uses"

let destr_Extends = function
|  Extends (_x1) -> _x1
|  _ -> failwith "destr_Extends"

let destr_Includes = function
|  Includes (_x1) -> _x1
|  _ -> failwith "destr_Includes"

let destr_Imports = function
|  Imports (_x1) -> _x1
|  _ -> failwith "destr_Imports"

let destr_Refines = function
|  Refines (_x1) -> _x1
|  _ -> failwith "destr_Refines"

(* destructeurs de def *)
let destr_Def = function
|  Def (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_Def"

let destr_FileDef = function
|  FileDef (_x1) -> _x1
|  _ -> failwith "destr_FileDef"

(* destructeurs de defBody *)
let destr_ExprDefBody = function
|  ExprDefBody (_x1) -> _x1
|  _ -> failwith "destr_ExprDefBody"

let destr_SubstDefBody = function
|  SubstDefBody (_x1) -> _x1
|  _ -> failwith "destr_SubstDefBody"

(* destructeurs de set *)
let destr_SetAbstDec = function
|  SetAbstDec (_x1) -> _x1
|  _ -> failwith "destr_SetAbstDec"

let destr_SetEnumDec = function
|  SetEnumDec (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SetEnumDec"


(* destructeurs de instance *)

(* destructeurs de type_B *)
let destr_TypeNatSetRange = function
|  TypeNatSetRange (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_TypeNatSetRange"

let destr_PredType = function
|  PredType (_x1) -> _x1
|  _ -> failwith "destr_PredType"


let destr_TypeIdentifier = function
|  TypeIdentifier (_x1) -> _x1
|  _ -> failwith "destr_TypeIdentifier"

let destr_PFunType = function
|  PFunType (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_PFunType"

let destr_FunType = function
|  FunType (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_FunType"

let destr_ProdType = function
|  ProdType (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_ProdType"

let destr_Sequence = function
|  Sequence (_x1) -> _x1
|  _ -> failwith "destr_Sequence"

let destr_SubType = function
|  SubType (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SubType"

let destr_Fin = function
|  Fin (_x1) -> _x1
|  _ -> failwith "destr_Fin"

let destr_Untyped = function
|  Untyped -> ()
|  _ -> failwith "destr_Untyped"

(* destructeurs de field *)

(* destructeurs de predicate *)
let destr_PredParen = function
|  PredParen (_x1) -> _x1
|  _ -> failwith "destr_PredParen"

let destr_PredNegation = function
|  PredNegation (_x1) -> _x1
|  _ -> failwith "destr_PredNegation"

let destr_PredExists = function
|  PredExists (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_PredExists"

let destr_PredForAll = function
|  PredForAll (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_PredForAll"

let destr_PredBin = function
|  PredBin (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_PredBin"

let destr_PredAtom = function
|  PredAtom (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_PredAtom"

(* destructeurs de operation *)

(* destructeurs de expr *)
let destr_ExprSequence = function
|  ExprSequence (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_ExprSequence"

let destr_ExprParen = function
|  ExprParen (_x1) -> _x1
|  _ -> failwith "destr_ExprParen"

let destr_ExprId = function
|  ExprId (_x1) -> _x1
|  _ -> failwith "destr_ExprId"

let destr_ExprFunCall = function
|  ExprFunCall (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_ExprFunCall"

let destr_ExprBin = function
|  ExprBin (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_ExprBin"

let destr_ExprUn = function
|  ExprUn (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_ExprUn"

let destr_ExprSeq = function
|  ExprSeq (_x1) -> _x1
|  _ -> failwith "destr_ExprSeq"

let destr_ExprSet = function
|  ExprSet (_x1) -> _x1
|  _ -> failwith "destr_ExprSet"

let destr_RelSet = function
|  RelSet (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_RelSet"

let destr_RelSeqComp = function
|  RelSeqComp (_x1) -> _x1
|  _ -> failwith "destr_RelSeqComp"

let destr_ExprNumber = function
|  ExprNumber (_x1) -> _x1
|  _ -> failwith "destr_ExprNumber"

let destr_ExprBool = function
|  ExprBool (_x1) -> _x1
|  _ -> failwith "destr_ExprBool"

let destr_ExprString = function
|  ExprString (_x1) -> _x1
|  _ -> failwith "destr_ExprString"


let destr_ExprPred = function
|  ExprPred (_x1) -> _x1
|  _ -> failwith "destr_ExprPred"

let destr_ExprNuplet = function
|  ExprNuplet (_x1) -> _x1
|  _ -> failwith "destr_ExprNuplet"

let destr_ExprSIGMA = function
|  ExprSIGMA (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_ExprSIGMA"

let destr_ExprPI = function
|  ExprPI (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_ExprPI"

let destr_ExprLambda = function
|  ExprLambda (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_ExprLambda"


(* destructeurs de exprSeq *)
let destr_SeqEnum = function
|  SeqEnum (_x1) -> _x1
|  _ -> failwith "destr_SeqEnum"

let destr_SeqEmpty = function
|  SeqEmpty -> ()
|  _ -> failwith "destr_SeqEmpty"

(* destructeurs de exprSet *)
let destr_SetRange = function
|  SetRange (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SetRange"

let destr_SetEnum = function
|  SetEnum (_x1) -> _x1
|  _ -> failwith "destr_SetEnum"

let destr_SetCompr = function
|  SetCompr (_x1) -> _x1
|  _ -> failwith "destr_SetCompr"

let destr_SetComprPred = function
|  SetComprPred (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SetComprPred"

let destr_SetEmpty = function
|  SetEmpty -> ()
|  _ -> failwith "destr_SetEmpty"

let destr_SetUnionQuantify = function
|  SetUnionQuantify (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_SetUnionQuantify"

let destr_SetInterQuantify = function
|  SetInterQuantify (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_SetInterQuantify"

(* destructeurs de number *)
let destr_MinNumber = function
|  MinNumber -> ()
|  _ -> failwith "destr_MinNumber"

let destr_Number = function
|  Number (_x1) -> _x1
|  _ -> failwith "destr_Number"

let destr_MaxNumber = function
|  MaxNumber -> ()
|  _ -> failwith "destr_MaxNumber"

(* destructeurs de substitution *)
let destr_SubstOperCall = function
|  SubstOperCall (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_SubstOperCall"

let destr_SubstBlock = function
|  SubstBlock (_x1) -> _x1
|  _ -> failwith "destr_SubstBlock"

let destr_SubstPrecondition = function
|  SubstPrecondition (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SubstPrecondition"

let destr_SubstAssertion = function
|  SubstAssertion (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SubstAssertion"

let destr_SubstChoice = function
|  SubstChoice (_x1) -> _x1
|  _ -> failwith "destr_SubstChoice"

let destr_SubstIf = function
|  SubstIf (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SubstIf"

let destr_SubstSelect = function
|  SubstSelect (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SubstSelect"

let destr_SubstCase = function
|  SubstCase (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_SubstCase"

let destr_SubstAny = function
|  SubstAny (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_SubstAny"

let destr_SubstLet = function
|  SubstLet (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_SubstLet"

let destr_SubstVar = function
|  SubstVar (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SubstVar"

let destr_SubstWhile = function
|  SubstWhile (_x1,_x2,_x3,_x4) -> _x1,_x2,_x3,_x4
|  _ -> failwith "destr_SubstWhile"

let destr_SubstSkip = function
|  SubstSkip -> ()
|  _ -> failwith "destr_SubstSkip"

let destr_SubstSequence = function
|  SubstSequence (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SubstSequence"

let destr_SubstParallel = function
|  SubstParallel (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SubstParallel"

let destr_SubstSetEqualIds = function
|  SubstSetEqualIds (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SubstSetEqualIds"

let destr_SubstSetEqualFun = function
|  SubstSetEqualFun (_x1,_x2,_x3) -> _x1,_x2,_x3
|  _ -> failwith "destr_SubstSetEqualFun"


let destr_SubstBecomeSuch = function
|  SubstBecomeSuch (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SubstBecomeSuch"

let destr_SubstSetIn = function
|  SubstSetIn (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_SubstSetIn"

(* destructeurs de ids *)

(* destructeurs de origine *)
let destr_Local = function
|  Local -> ()
|  _ -> failwith "destr_Local"

let destr_Remote = function
|  Remote (_x1) -> _x1
|  _ -> failwith "destr_Remote"

let destr_OrgRefine = function
|  OrgRefine (_x1,_x2) -> _x1,_x2
|  _ -> failwith "destr_OrgRefine"

let destr_Prefix = function
|  Prefix (_x1) -> _x1
|  _ -> failwith "destr_Prefix"

(* destructeurs de id *)
let destr_Id (Id (_x1,_x2,_x3,_x4)) = _x1,_x2,_x3,_x4

*)

(* Ensembles de binders *)
module Set_id = Set.Make (
struct 
  type t = id
  let compare = compare
end)

