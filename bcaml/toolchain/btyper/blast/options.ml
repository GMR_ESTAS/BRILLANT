

let expansion = ref false;;

let prall = ref false;;

let header = ref true;;

let rec split_path_from_env p =
  try
    let ix_dxpt = String.index p ':' in
    try
      (String.sub p 0 ix_dxpt)::
      (split_path_from_env (String.sub  p (ix_dxpt + 1) 
		      (String.length p - ix_dxpt - 1)))
    with _ -> []
  with _ -> [p]

let standard_library = try Sys.getenv "B_BASICS" with Not_found -> "."

let load_path = split_path_from_env standard_library

let find_in_path path name =
(*MG 
  let _ = Error.debug ("[Options] find_in_path"^name) in 
 *)
  let rec try_dir = function
      [] -> failwith (name^" not found")
    | dir::rem ->
        let fullname = Filename.concat dir name in
        if Sys.file_exists fullname then fullname else try_dir rem
  in try_dir path


(* 
setenv B_BASICS ".:/home/filali/JAVALOG/BPARSE/IRIT/TYPEUR3/studies/BASICLIB35_0/spec:/home/filali/JAVALOG/BPARSE/IRIT/TYPEUR3/studies/BASICLIB35_0X/spec:/home/filali/JAVALOG/BPARSE/IRIT/TYPEUR3/studies/BASICLIB36_0/spec:/home/filali/JAVALOG/BPARSE/IRIT/TYPEUR3/studies/BASICLIB36_0X/spec"
*)





