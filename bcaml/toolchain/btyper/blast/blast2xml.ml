(*************************************************************)
(*                PROJET BCAML                               *)
(*                                                           *)
(* http://www3.inrets.fr/Public/ESTAS/Mariano.Georges/Bcaml/ *)
(*                                                           *)
(*                   IRIT                                    *)
(*                                                           *)
(*   source genere le 14-05-2001  09:04:43                   *)
(*************************************************************)

open Xml
open Blast

let p_xml_string s = [XString s]
let p_xml_bool b = [XBool b]
let p_xml_option f = function Some x -> f x | None -> []
let p_xml_seq f l = List.concat (List.map f l)

let rec p_xml_amn _x1 = [XBalise ("amn", (match _x1 with
  Machine(_x1, _x2) -> [XBalise ("Machine", ((p_xml_head _x1) @ ((p_xml_seq p_xml_clause) _x2)))]
| Refinement(_x1, _x2) -> [XBalise ("Refinement", ((p_xml_head _x1) @ ((p_xml_seq p_xml_clause) _x2)))]
| Implementation(_x1, _x2) -> [XBalise ("Implementation", ((p_xml_head _x1) @ ((p_xml_seq p_xml_clause) _x2)))]
| EmptyTree -> [XBalise ("EmptyTree",[])]))]
and p_xml_head (_x1, _x2) =
  [XBalise ("head", ((p_xml_id _x1) @ (p_xml_ids _x2)))]
and p_xml_clause _x1 = [XBalise ("clause", (match _x1 with
  Definitions(_x1) -> [XBalise ("Definitions", (((p_xml_seq p_xml_def) _x1)))]
| Constraints(_x1) -> [XBalise ("Constraints", ((p_xml_predicate _x1)))]
| Invariant(_x1) -> [XBalise ("Invariant", ((p_xml_predicate _x1)))]
| Sets(_x1) -> [XBalise ("Sets", (((p_xml_seq p_xml_set) _x1)))]
| Initialisation(_x1) -> [XBalise ("Initialisation", ((p_xml_substitution _x1)))]
| ConstantsConcrete(_x1) -> [XBalise ("ConstantsConcrete", ((p_xml_ids _x1)))]
| ConstantsAbstract(_x1) -> [XBalise ("ConstantsAbstract", ((p_xml_ids _x1)))]
| ConstantsHidden(_x1) -> [XBalise ("ConstantsHidden", ((p_xml_ids _x1)))]
| Properties(_x1) -> [XBalise ("Properties", ((p_xml_predicate _x1)))]
| Values(_x1) -> [XBalise ("Values", (((p_xml_seq (fun (_x1, _x2) -> (p_xml_id _x1) @ (p_xml_expr _x2))) _x1)))]
| VariablesConcrete(_x1) -> [XBalise ("VariablesConcrete", ((p_xml_ids _x1)))]
| VariablesAbstract(_x1) -> [XBalise ("VariablesAbstract", ((p_xml_ids _x1)))]
| VariablesHidden(_x1) -> [XBalise ("VariablesHidden", ((p_xml_ids _x1)))]
| Promotes(_x1) -> [XBalise ("Promotes", ((p_xml_ids _x1)))]
| Assertions(_x1) -> [XBalise ("Assertions", (((p_xml_seq p_xml_predicate) _x1)))]
| Operations(_x1) -> [XBalise ("Operations", (((p_xml_seq p_xml_operation) _x1)))]
| Sees(_x1) -> [XBalise ("Sees", ((p_xml_ids _x1)))]
| Uses(_x1) -> [XBalise ("Uses", ((p_xml_ids _x1)))]
| Extends(_x1) -> [XBalise ("Extends", (((p_xml_seq p_xml_instance) _x1)))]
| Includes(_x1) -> [XBalise ("Includes", (((p_xml_seq p_xml_instance) _x1)))]
| Imports(_x1) -> [XBalise ("Imports", (((p_xml_seq p_xml_instance) _x1)))]
| Refines(_x1) -> [XBalise ("Refines", ((p_xml_id _x1)))]))]
and p_xml_def _x1 = [XBalise ("def", (match _x1 with
  Def(_x1, _x2, _x3) -> [XBalise ("Def", ((p_xml_id _x1) @ (p_xml_ids _x2) @ (p_xml_defBody _x3)))]
| FileDef(_x1) -> [XBalise ("FileDef", ((p_xml_string _x1)))]))]
and p_xml_defBody _x1 = [XBalise ("defBody", (match _x1 with
  ExprDefBody(_x1) -> [XBalise ("ExprDefBody", ((p_xml_expr _x1)))]
| SubstDefBody(_x1) -> [XBalise ("SubstDefBody", ((p_xml_substitution _x1)))]))]
and p_xml_set _x1 = [XBalise ("set", (match _x1 with
  SetAbstDec(_x1) -> [XBalise ("SetAbstDec", ((p_xml_id _x1)))]
| SetEnumDec(_x1, _x2) -> [XBalise ("SetEnumDec", ((p_xml_id _x1) @ (p_xml_ids _x2)))]
| SetRecordsDec(_x1, _x2) -> [XBalise ("SetRecordsDec", ((p_xml_id _x1) @ ((p_xml_seq p_xml_recordsItem) _x2)))]))]
and p_xml_instance (_x1, _x2) =
  [XBalise ("instance", ((p_xml_id _x1) @ ((p_xml_seq p_xml_expr) _x2)))]
and p_xml_type_B _x1 = [XBalise ("type_B", (match _x1 with
  TypeNatSetRange(_x1, _x2) -> [XBalise ("TypeNatSetRange", ((p_xml_expr _x1) @ (p_xml_expr _x2)))]
| PredType(_x1) -> [XBalise ("PredType", ((p_xml_expr _x1)))]
| RecordsType(_x1) -> [XBalise ("RecordsType", (((p_xml_seq p_xml_field) _x1)))]
| TypeIdentifier(_x1) -> [XBalise ("TypeIdentifier", ((p_xml_id _x1)))]
| PFunType(_x1, _x2) -> [XBalise ("PFunType", ((p_xml_type_B _x1) @ (p_xml_type_B _x2)))]
| FunType(_x1, _x2) -> [XBalise ("FunType", ((p_xml_type_B _x1) @ (p_xml_type_B _x2)))]
| ProdType(_x1, _x2) -> [XBalise ("ProdType", ((p_xml_type_B _x1) @ (p_xml_type_B _x2)))]
| Sequence(_x1) -> [XBalise ("Sequence", ((p_xml_type_B _x1)))]
| SubType(_x1, _x2) -> [XBalise ("SubType", ((p_xml_type_B _x1) @ (p_xml_expr _x2)))]
| Fin(_x1) -> [XBalise ("Fin", ((p_xml_type_B _x1)))]
| Untyped -> [XBalise ("Untyped",[])]))]
and p_xml_field (_x1, _x2) =
  [XBalise ("field", ((p_xml_id _x1) @ (p_xml_type_B _x2)))]
and p_xml_predicate _x1 = [XBalise ("predicate", (match _x1 with
  PredParen(_x1) -> [XBalise ("PredParen", ((p_xml_predicate _x1)))]
| PredNegation(_x1) -> [XBalise ("PredNegation", ((p_xml_predicate _x1)))]
| PredExists(_x1, _x2) -> [XBalise ("PredExists", (((p_xml_seq p_xml_id) _x1) @ (p_xml_predicate _x2)))]
| PredForAll(_x1, _x2) -> [XBalise ("PredForAll", (((p_xml_seq p_xml_id) _x1) @ (p_xml_predicate _x2)))]
| PredBin(_x1, _x2, _x3) -> [XBalise ("PredBin", ((Bbop.p_xml_bop2 _x1) @ (p_xml_predicate _x2) @ (p_xml_predicate _x3)))]
| PredAtom(_x1, _x2, _x3) -> [XBalise ("PredAtom", ((Bbop.p_xml_bop2 _x1) @ (p_xml_expr _x2) @ (p_xml_expr _x3)))]))]
and p_xml_operation (_x1, _x2, _x3, _x4) =
  [XBalise ("operation", ((p_xml_id _x1) @ (p_xml_ids _x2) @ (p_xml_ids _x3) @ (p_xml_substitution _x4)))]
and p_xml_expr _x1 = [XBalise ("expr", (match _x1 with
  ExprSequence(_x1, _x2) -> [XBalise ("ExprSequence", ((p_xml_expr _x1) @ (p_xml_expr _x2)))]
| ExprParen(_x1) -> [XBalise ("ExprParen", ((p_xml_expr _x1)))]
| ExprId(_x1) -> [XBalise ("ExprId", ((p_xml_id _x1)))]
| ExprFunCall(_x1, _x2) -> [XBalise ("ExprFunCall", ((p_xml_expr _x1) @ ((p_xml_seq p_xml_expr) _x2)))]
| ExprBin(_x1, _x2, _x3) -> [XBalise ("ExprBin", ((Bbop.p_xml_op2 _x1) @ (p_xml_expr _x2) @ (p_xml_expr _x3)))]
| ExprUn(_x1, _x2) -> [XBalise ("ExprUn", ((Bbop.p_xml_op1 _x1) @ (p_xml_expr _x2)))]
| ExprSeq(_x1) -> [XBalise ("ExprSeq", ((p_xml_exprSeq _x1)))]
| ExprSet(_x1) -> [XBalise ("ExprSet", ((p_xml_exprSet _x1)))]
| RelSet(_x1, _x2) -> [XBalise ("RelSet", ((p_xml_expr _x1) @ (p_xml_expr _x2)))]
| RelSeqComp(_x1) -> [XBalise ("RelSeqComp", (((p_xml_seq p_xml_expr) _x1)))]
| ExprNumber(_x1) -> [XBalise ("ExprNumber", ((p_xml_number _x1)))]
| ExprBool(_x1) -> [XBalise ("ExprBool", ((p_xml_bool _x1)))]
| ExprString(_x1) -> [XBalise ("ExprString", ((p_xml_string _x1)))]
| ExprRecords(_x1) -> [XBalise ("ExprRecords", ((p_xml_exprRecords _x1)))]
| ExprPred(_x1) -> [XBalise ("ExprPred", ((p_xml_predicate _x1)))]
| ExprNuplet(_x1) -> [XBalise ("ExprNuplet", (((p_xml_seq p_xml_expr) _x1)))]
| ExprSIGMA(_x1, _x2, _x3) -> [XBalise ("ExprSIGMA", (((p_xml_seq p_xml_id) _x1) @ (p_xml_predicate _x2) @ (p_xml_expr _x3)))]
| ExprPI(_x1, _x2, _x3) -> [XBalise ("ExprPI", (((p_xml_seq p_xml_id) _x1) @ (p_xml_predicate _x2) @ (p_xml_expr _x3)))]
| ExprLambda(_x1, _x2, _x3) -> [XBalise ("ExprLambda", (((p_xml_seq p_xml_id) _x1) @ (p_xml_predicate _x2) @ (p_xml_expr _x3)))]))]
and p_xml_exprRecords _x1 = [XBalise ("exprRecords", (match _x1 with
  RecordsWithField(_x1) -> [XBalise ("RecordsWithField", (((p_xml_seq p_xml_recordsItem) _x1)))]
| Records(_x1) -> [XBalise ("Records", (((p_xml_seq p_xml_expr) _x1)))]
| RecordsAccess(_x1, _x2) -> [XBalise ("RecordsAccess", ((p_xml_expr _x1) @ (p_xml_id _x2)))]
| RecordsSet(_x1) -> [XBalise ("RecordsSet", (((p_xml_seq p_xml_recordsItem) _x1)))]))]
and p_xml_recordsItem (_x1, _x2) =
  [XBalise ("recordsItem", ((p_xml_id _x1) @ (p_xml_expr _x2)))]
and p_xml_exprSeq _x1 = [XBalise ("exprSeq", (match _x1 with
  SeqEnum(_x1) -> [XBalise ("SeqEnum", (((p_xml_seq p_xml_expr) _x1)))]
| SeqEmpty -> [XBalise ("SeqEmpty",[])]))]
and p_xml_exprSet _x1 = [XBalise ("exprSet", (match _x1 with
  SetRange(_x1, _x2) -> [XBalise ("SetRange", ((p_xml_expr _x1) @ (p_xml_expr _x2)))]
| SetEnum(_x1) -> [XBalise ("SetEnum", (((p_xml_seq p_xml_expr) _x1)))]
| SetCompr(_x1) -> [XBalise ("SetCompr", (((p_xml_seq p_xml_expr) _x1)))]
| SetComprPred(_x1, _x2) -> [XBalise ("SetComprPred", (((p_xml_seq p_xml_id) _x1) @ (p_xml_predicate _x2)))]
| SetEmpty -> [XBalise ("SetEmpty",[])]
| SetUnionQuantify(_x1, _x2, _x3) -> [XBalise ("SetUnionQuantify", (((p_xml_seq p_xml_id) _x1) @ (p_xml_predicate _x2) @ (p_xml_expr _x3)))]
| SetInterQuantify(_x1, _x2, _x3) -> [XBalise ("SetInterQuantify", (((p_xml_seq p_xml_id) _x1) @ (p_xml_predicate _x2) @ (p_xml_expr _x3)))]))]
and p_xml_number _x1 = [XBalise ("number", (match _x1 with
  MinNumber -> [XBalise ("MinNumber",[])]
| Number(_x1) -> [XBalise ("Number", ((p_xml_int _x1)))]
| MaxNumber -> [XBalise ("MaxNumber",[])]))]
and p_xml_substitution _x1 = [XBalise ("substitution", (match _x1 with
  SubstOperCall(_x1, _x2, _x3) -> [XBalise ("SubstOperCall", ((p_xml_ids _x1) @ (p_xml_id _x2) @ ((p_xml_seq p_xml_expr) _x3)))]
| SubstBlock(_x1) -> [XBalise ("SubstBlock", ((p_xml_substitution _x1)))]
| SubstPrecondition(_x1, _x2) -> [XBalise ("SubstPrecondition", ((p_xml_predicate _x1) @ (p_xml_substitution _x2)))]
| SubstAssertion(_x1, _x2) -> [XBalise ("SubstAssertion", ((p_xml_predicate _x1) @ (p_xml_substitution _x2)))]
| SubstChoice(_x1) -> [XBalise ("SubstChoice", (((p_xml_seq p_xml_substitution) _x1)))]
| SubstIf(_x1, _x2) -> [XBalise ("SubstIf", (((p_xml_seq (fun (_x1, _x2) -> (p_xml_predicate _x1) @ (p_xml_substitution _x2))) _x1) @ ((p_xml_option p_xml_substitution) _x2)))]
| SubstSelect(_x1, _x2) -> [XBalise ("SubstSelect", (((p_xml_seq (fun (_x1, _x2) -> (p_xml_predicate _x1) @ (p_xml_substitution _x2))) _x1) @ ((p_xml_option p_xml_substitution) _x2)))]
| SubstCase(_x1, _x2, _x3) -> [XBalise ("SubstCase", ((p_xml_expr _x1) @ ((p_xml_seq (fun (_x1, _x2) -> ((p_xml_seq p_xml_expr) _x1) @ (p_xml_substitution _x2))) _x2) @ ((p_xml_option p_xml_substitution) _x3)))]
| SubstAny(_x1, _x2, _x3) -> [XBalise ("SubstAny", (((p_xml_seq p_xml_id) _x1) @ (p_xml_predicate _x2) @ (p_xml_substitution _x3)))]
| SubstLet(_x1, _x2, _x3) -> [XBalise ("SubstLet", (((p_xml_seq p_xml_id) _x1) @ (p_xml_predicate _x2) @ (p_xml_substitution _x3)))]
| SubstVar(_x1, _x2) -> [XBalise ("SubstVar", (((p_xml_seq p_xml_id) _x1) @ (p_xml_substitution _x2)))]
| SubstWhile(_x1, _x2, _x3, _x4) -> [XBalise ("SubstWhile", ((p_xml_predicate _x1) @ (p_xml_substitution _x2) @ (p_xml_expr _x3) @ (p_xml_predicate _x4)))]
| SubstSkip -> [XBalise ("SubstSkip",[])]
| SubstSequence(_x1, _x2) -> [XBalise ("SubstSequence", ((p_xml_substitution _x1) @ (p_xml_substitution _x2)))]
| SubstParallel(_x1, _x2) -> [XBalise ("SubstParallel", ((p_xml_substitution _x1) @ (p_xml_substitution _x2)))]
| SubstSetEqualIds(_x1, _x2) -> [XBalise ("SubstSetEqualIds", ((p_xml_ids _x1) @ ((p_xml_seq p_xml_expr) _x2)))]
| SubstSetEqualFun(_x1, _x2, _x3) -> [XBalise ("SubstSetEqualFun", ((p_xml_expr _x1) @ ((p_xml_seq p_xml_expr) _x2) @ (p_xml_expr _x3)))]
| SubstSetEqualRecords(_x1, _x2) -> [XBalise ("SubstSetEqualRecords", ((p_xml_exprRecords _x1) @ (p_xml_expr _x2)))]
| SubstBecomeSuch(_x1, _x2) -> [XBalise ("SubstBecomeSuch", ((p_xml_ids _x1) @ (p_xml_predicate _x2)))]
| SubstSetIn(_x1, _x2) -> [XBalise ("SubstSetIn", ((p_xml_ids _x1) @ (p_xml_expr _x2)))]))]
and p_xml_ids _x1 = [XBalise ("ids", (((p_xml_seq p_xml_id) _x1)))]
and p_xml_origine _x1 = [XBalise ("origine", (match _x1 with
  Local -> [XBalise ("Local",[])]
| Remote(_x1) -> [XBalise ("Remote", (((p_xml_seq (fun (_x1, _x2) -> (p_xml_string _x1) @ (p_xml_string _x2))) _x1)))]
| OrgRefine(_x1, _x2) -> [XBalise ("OrgRefine", ((p_xml_string _x1) @ ((p_xml_seq (fun (_x1, _x2) -> (p_xml_string _x1) @ (p_xml_string _x2))) _x2)))]
| Prefix(_x1) -> [XBalise ("Prefix", (((p_xml_seq p_xml_string) _x1)))]))]
and p_xml_id _x1 = [XBalise ("id", (match _x1 with
  Id(_x1, _x2, _x3, _x4) -> [XBalise ("Id", ((p_xml_origine _x1) @ (p_xml_string _x2) @ ((p_xml_option p_xml_type_B) _x3) @ (Bbop.p_xml_kind _x4)))]))]
