(*************************************************************)
(*                PROJET BCAML                               *)
(*                                                           *)
(* http://www3.inrets.fr/Public/ESTAS/Mariano.Georges/Bcaml/ *)
(*                                                           *)
(*                   IRIT                                    *)
(*                                                           *)
(*   source genere le 14-05-2001  09:04:43                   *)
(*************************************************************)

open Xml
open Blast_lib

let rec xmlparse_amn = function
  XBalise("amn",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("Machine",_r)::_l ->
    let _x1,_r = xmlparse_head _r in
    let _x2,_r = (xmlparse_seq xmlparse_clause) _r in
    xmlparse_empty "Machine" _r;
    (mk_Machine _x1 _x2),_l
  | XBalise ("Refinement",_r)::_l ->
    let _x1,_r = xmlparse_head _r in
    let _x2,_r = (xmlparse_seq xmlparse_clause) _r in
    xmlparse_empty "Refinement" _r;
    (mk_Refinement _x1 _x2),_l
  | XBalise ("Implementation",_r)::_l ->
    let _x1,_r = xmlparse_head _r in
    let _x2,_r = (xmlparse_seq xmlparse_clause) _r in
    xmlparse_empty "Implementation" _r;
    (mk_Implementation _x1 _x2),_l
  | XBalise ("EmptyTree",_r)::_l ->
    mk_EmptyTree,_l
  | _ -> raise (InvalidXml "Machine|Refinement|Implementation|EmptyTree")
  end in
    xmlparse_empty "amn" _r;
    mk_amn _x1, _l
| _ -> raise (InvalidXml "amn")

and xmlparse_head = function
  XBalise("head",_r)::_l ->
    let _x1,_r = xmlparse_id _r in
    let _x2,_r = xmlparse_ids _r in
    xmlparse_empty "head" _r;
    mk_head _x1 _x2, _l
| _ -> raise (InvalidXml "head")

and xmlparse_clause = function
  XBalise("clause",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("Definitions",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_def) _r in
    xmlparse_empty "Definitions" _r;
    (mk_Definitions _x1),_l
  | XBalise ("Constraints",_r)::_l ->
    let _x1,_r = xmlparse_predicate _r in
    xmlparse_empty "Constraints" _r;
    (mk_Constraints _x1),_l
  | XBalise ("Invariant",_r)::_l ->
    let _x1,_r = xmlparse_predicate _r in
    xmlparse_empty "Invariant" _r;
    (mk_Invariant _x1),_l
  | XBalise ("Sets",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_set) _r in
    xmlparse_empty "Sets" _r;
    (mk_Sets _x1),_l
  | XBalise ("Initialisation",_r)::_l ->
    let _x1,_r = xmlparse_substitution _r in
    xmlparse_empty "Initialisation" _r;
    (mk_Initialisation _x1),_l
  | XBalise ("ConstantsConcrete",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    xmlparse_empty "ConstantsConcrete" _r;
    (mk_ConstantsConcrete _x1),_l
  | XBalise ("ConstantsAbstract",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    xmlparse_empty "ConstantsAbstract" _r;
    (mk_ConstantsAbstract _x1),_l
  | XBalise ("ConstantsHidden",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    xmlparse_empty "ConstantsHidden" _r;
    (mk_ConstantsHidden _x1),_l
  | XBalise ("Properties",_r)::_l ->
    let _x1,_r = xmlparse_predicate _r in
    xmlparse_empty "Properties" _r;
    (mk_Properties _x1),_l
  | XBalise ("Values",_r)::_l ->
    let _x1,_r = (xmlparse_seq     (fun _r -> 
    let _x1,_r = xmlparse_id _r in
    let _x2,_r = xmlparse_expr _r in
    (_x1, _x2),_r)) _r in
    xmlparse_empty "Values" _r;
    (mk_Values _x1),_l
  | XBalise ("VariablesConcrete",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    xmlparse_empty "VariablesConcrete" _r;
    (mk_VariablesConcrete _x1),_l
  | XBalise ("VariablesAbstract",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    xmlparse_empty "VariablesAbstract" _r;
    (mk_VariablesAbstract _x1),_l
  | XBalise ("VariablesHidden",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    xmlparse_empty "VariablesHidden" _r;
    (mk_VariablesHidden _x1),_l
  | XBalise ("Promotes",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    xmlparse_empty "Promotes" _r;
    (mk_Promotes _x1),_l
  | XBalise ("Assertions",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_predicate) _r in
    xmlparse_empty "Assertions" _r;
    (mk_Assertions _x1),_l
  | XBalise ("Operations",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_operation) _r in
    xmlparse_empty "Operations" _r;
    (mk_Operations _x1),_l
  | XBalise ("Sees",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    xmlparse_empty "Sees" _r;
    (mk_Sees _x1),_l
  | XBalise ("Uses",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    xmlparse_empty "Uses" _r;
    (mk_Uses _x1),_l
  | XBalise ("Extends",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_instance) _r in
    xmlparse_empty "Extends" _r;
    (mk_Extends _x1),_l
  | XBalise ("Includes",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_instance) _r in
    xmlparse_empty "Includes" _r;
    (mk_Includes _x1),_l
  | XBalise ("Imports",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_instance) _r in
    xmlparse_empty "Imports" _r;
    (mk_Imports _x1),_l
  | XBalise ("Refines",_r)::_l ->
    let _x1,_r = xmlparse_id _r in
    xmlparse_empty "Refines" _r;
    (mk_Refines _x1),_l
  | _ -> raise (InvalidXml "Definitions|Constraints|Invariant|Sets|Initialisation|ConstantsConcrete|ConstantsAbstract|ConstantsHidden|Properties|Values|VariablesConcrete|VariablesAbstract|VariablesHidden|Promotes|Assertions|Operations|Sees|Uses|Extends|Includes|Imports|Refines")
  end in
    xmlparse_empty "clause" _r;
    mk_clause _x1, _l
| _ -> raise (InvalidXml "clause")

and xmlparse_def = function
  XBalise("def",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("Def",_r)::_l ->
    let _x1,_r = xmlparse_id _r in
    let _x2,_r = xmlparse_ids _r in
    let _x3,_r = xmlparse_defBody _r in
    xmlparse_empty "Def" _r;
    (mk_Def _x1 _x2 _x3),_l
  | XBalise ("FileDef",_r)::_l ->
    let _x1,_r = xmlparse_string _r in
    xmlparse_empty "FileDef" _r;
    (mk_FileDef _x1),_l
  | _ -> raise (InvalidXml "Def|FileDef")
  end in
    xmlparse_empty "def" _r;
    mk_def _x1, _l
| _ -> raise (InvalidXml "def")

and xmlparse_defBody = function
  XBalise("defBody",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("ExprDefBody",_r)::_l ->
    let _x1,_r = xmlparse_expr _r in
    xmlparse_empty "ExprDefBody" _r;
    (mk_ExprDefBody _x1),_l
  | XBalise ("SubstDefBody",_r)::_l ->
    let _x1,_r = xmlparse_substitution _r in
    xmlparse_empty "SubstDefBody" _r;
    (mk_SubstDefBody _x1),_l
  | _ -> raise (InvalidXml "ExprDefBody|SubstDefBody")
  end in
    xmlparse_empty "defBody" _r;
    mk_defBody _x1, _l
| _ -> raise (InvalidXml "defBody")

and xmlparse_set = function
  XBalise("set",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("SetAbstDec",_r)::_l ->
    let _x1,_r = xmlparse_id _r in
    xmlparse_empty "SetAbstDec" _r;
    (mk_SetAbstDec _x1),_l
  | XBalise ("SetEnumDec",_r)::_l ->
    let _x1,_r = xmlparse_id _r in
    let _x2,_r = xmlparse_ids _r in
    xmlparse_empty "SetEnumDec" _r;
    (mk_SetEnumDec _x1 _x2),_l
  | XBalise ("SetRecordsDec",_r)::_l ->
    let _x1,_r = xmlparse_id _r in
    let _x2,_r = (xmlparse_seq xmlparse_recordsItem) _r in
    xmlparse_empty "SetRecordsDec" _r;
    (mk_SetRecordsDec _x1 _x2),_l
  | _ -> raise (InvalidXml "SetAbstDec|SetEnumDec|SetRecordsDec")
  end in
    xmlparse_empty "set" _r;
    mk_set _x1, _l
| _ -> raise (InvalidXml "set")

and xmlparse_instance = function
  XBalise("instance",_r)::_l ->
    let _x1,_r = xmlparse_id _r in
    let _x2,_r = (xmlparse_seq xmlparse_expr) _r in
    xmlparse_empty "instance" _r;
    mk_instance _x1 _x2, _l
| _ -> raise (InvalidXml "instance")

and xmlparse_type_B = function
  XBalise("type_B",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("TypeNatSetRange",_r)::_l ->
    let _x1,_r = xmlparse_expr _r in
    let _x2,_r = xmlparse_expr _r in
    xmlparse_empty "TypeNatSetRange" _r;
    (mk_TypeNatSetRange _x1 _x2),_l
  | XBalise ("PredType",_r)::_l ->
    let _x1,_r = xmlparse_expr _r in
    xmlparse_empty "PredType" _r;
    (mk_PredType _x1),_l
  | XBalise ("RecordsType",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_field) _r in
    xmlparse_empty "RecordsType" _r;
    (mk_RecordsType _x1),_l
  | XBalise ("TypeIdentifier",_r)::_l ->
    let _x1,_r = xmlparse_id _r in
    xmlparse_empty "TypeIdentifier" _r;
    (mk_TypeIdentifier _x1),_l
  | XBalise ("PFunType",_r)::_l ->
    let _x1,_r = xmlparse_type_B _r in
    let _x2,_r = xmlparse_type_B _r in
    xmlparse_empty "PFunType" _r;
    (mk_PFunType _x1 _x2),_l
  | XBalise ("FunType",_r)::_l ->
    let _x1,_r = xmlparse_type_B _r in
    let _x2,_r = xmlparse_type_B _r in
    xmlparse_empty "FunType" _r;
    (mk_FunType _x1 _x2),_l
  | XBalise ("ProdType",_r)::_l ->
    let _x1,_r = xmlparse_type_B _r in
    let _x2,_r = xmlparse_type_B _r in
    xmlparse_empty "ProdType" _r;
    (mk_ProdType _x1 _x2),_l
  | XBalise ("Sequence",_r)::_l ->
    let _x1,_r = xmlparse_type_B _r in
    xmlparse_empty "Sequence" _r;
    (mk_Sequence _x1),_l
  | XBalise ("SubType",_r)::_l ->
    let _x1,_r = xmlparse_type_B _r in
    let _x2,_r = xmlparse_expr _r in
    xmlparse_empty "SubType" _r;
    (mk_SubType _x1 _x2),_l
  | XBalise ("Fin",_r)::_l ->
    let _x1,_r = xmlparse_type_B _r in
    xmlparse_empty "Fin" _r;
    (mk_Fin _x1),_l
  | XBalise ("Untyped",_r)::_l ->
    mk_Untyped,_l
  | _ -> raise (InvalidXml "TypeNatSetRange|PredType|RecordsType|TypeIdentifier|PFunType|FunType|ProdType|Sequence|SubType|Fin|Untyped")
  end in
    xmlparse_empty "type_B" _r;
    mk_type_B _x1, _l
| _ -> raise (InvalidXml "type_B")

and xmlparse_field = function
  XBalise("field",_r)::_l ->
    let _x1,_r = xmlparse_id _r in
    let _x2,_r = xmlparse_type_B _r in
    xmlparse_empty "field" _r;
    mk_field _x1 _x2, _l
| _ -> raise (InvalidXml "field")

and xmlparse_predicate = function
  XBalise("predicate",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("PredParen",_r)::_l ->
    let _x1,_r = xmlparse_predicate _r in
    xmlparse_empty "PredParen" _r;
    (mk_PredParen _x1),_l
  | XBalise ("PredNegation",_r)::_l ->
    let _x1,_r = xmlparse_predicate _r in
    xmlparse_empty "PredNegation" _r;
    (mk_PredNegation _x1),_l
  | XBalise ("PredExists",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_id) _r in
    let _x2,_r = xmlparse_predicate _r in
    xmlparse_empty "PredExists" _r;
    (mk_PredExists _x1 _x2),_l
  | XBalise ("PredForAll",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_id) _r in
    let _x2,_r = xmlparse_predicate _r in
    xmlparse_empty "PredForAll" _r;
    (mk_PredForAll _x1 _x2),_l
  | XBalise ("PredBin",_r)::_l ->
    let _x1,_r = Bbop.xmlparse_bop2 _r in
    let _x2,_r = xmlparse_predicate _r in
    let _x3,_r = xmlparse_predicate _r in
    xmlparse_empty "PredBin" _r;
    (mk_PredBin _x1 _x2 _x3),_l
  | XBalise ("PredAtom",_r)::_l ->
    let _x1,_r = Bbop.xmlparse_bop2 _r in
    let _x2,_r = xmlparse_expr _r in
    let _x3,_r = xmlparse_expr _r in
    xmlparse_empty "PredAtom" _r;
    (mk_PredAtom _x1 _x2 _x3),_l
  | _ -> raise (InvalidXml "PredParen|PredNegation|PredExists|PredForAll|PredBin|PredAtom")
  end in
    xmlparse_empty "predicate" _r;
    mk_predicate _x1, _l
| _ -> raise (InvalidXml "predicate")

and xmlparse_operation = function
  XBalise("operation",_r)::_l ->
    let _x1,_r = xmlparse_id _r in
    let _x2,_r = xmlparse_ids _r in
    let _x3,_r = xmlparse_ids _r in
    let _x4,_r = xmlparse_substitution _r in
    xmlparse_empty "operation" _r;
    mk_operation _x1 _x2 _x3 _x4, _l
| _ -> raise (InvalidXml "operation")

and xmlparse_expr = function
  XBalise("expr",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("ExprSequence",_r)::_l ->
    let _x1,_r = xmlparse_expr _r in
    let _x2,_r = xmlparse_expr _r in
    xmlparse_empty "ExprSequence" _r;
    (mk_ExprSequence _x1 _x2),_l
  | XBalise ("ExprParen",_r)::_l ->
    let _x1,_r = xmlparse_expr _r in
    xmlparse_empty "ExprParen" _r;
    (mk_ExprParen _x1),_l
  | XBalise ("ExprId",_r)::_l ->
    let _x1,_r = xmlparse_id _r in
    xmlparse_empty "ExprId" _r;
    (mk_ExprId _x1),_l
  | XBalise ("ExprFunCall",_r)::_l ->
    let _x1,_r = xmlparse_expr _r in
    let _x2,_r = (xmlparse_seq xmlparse_expr) _r in
    xmlparse_empty "ExprFunCall" _r;
    (mk_ExprFunCall _x1 _x2),_l
  | XBalise ("ExprBin",_r)::_l ->
    let _x1,_r = Bbop.xmlparse_op2 _r in
    let _x2,_r = xmlparse_expr _r in
    let _x3,_r = xmlparse_expr _r in
    xmlparse_empty "ExprBin" _r;
    (mk_ExprBin _x1 _x2 _x3),_l
  | XBalise ("ExprUn",_r)::_l ->
    let _x1,_r = Bbop.xmlparse_op1 _r in
    let _x2,_r = xmlparse_expr _r in
    xmlparse_empty "ExprUn" _r;
    (mk_ExprUn _x1 _x2),_l
  | XBalise ("ExprSeq",_r)::_l ->
    let _x1,_r = xmlparse_exprSeq _r in
    xmlparse_empty "ExprSeq" _r;
    (mk_ExprSeq _x1),_l
  | XBalise ("ExprSet",_r)::_l ->
    let _x1,_r = xmlparse_exprSet _r in
    xmlparse_empty "ExprSet" _r;
    (mk_ExprSet _x1),_l
  | XBalise ("RelSet",_r)::_l ->
    let _x1,_r = xmlparse_expr _r in
    let _x2,_r = xmlparse_expr _r in
    xmlparse_empty "RelSet" _r;
    (mk_RelSet _x1 _x2),_l
  | XBalise ("RelSeqComp",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_expr) _r in
    xmlparse_empty "RelSeqComp" _r;
    (mk_RelSeqComp _x1),_l
  | XBalise ("ExprNumber",_r)::_l ->
    let _x1,_r = xmlparse_number _r in
    xmlparse_empty "ExprNumber" _r;
    (mk_ExprNumber _x1),_l
  | XBalise ("ExprBool",_r)::_l ->
    let _x1,_r = xmlparse_bool _r in
    xmlparse_empty "ExprBool" _r;
    (mk_ExprBool _x1),_l
  | XBalise ("ExprString",_r)::_l ->
    let _x1,_r = xmlparse_string _r in
    xmlparse_empty "ExprString" _r;
    (mk_ExprString _x1),_l
  | XBalise ("ExprRecords",_r)::_l ->
    let _x1,_r = xmlparse_exprRecords _r in
    xmlparse_empty "ExprRecords" _r;
    (mk_ExprRecords _x1),_l
  | XBalise ("ExprPred",_r)::_l ->
    let _x1,_r = xmlparse_predicate _r in
    xmlparse_empty "ExprPred" _r;
    (mk_ExprPred _x1),_l
  | XBalise ("ExprNuplet",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_expr) _r in
    xmlparse_empty "ExprNuplet" _r;
    (mk_ExprNuplet _x1),_l
  | XBalise ("ExprSIGMA",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_id) _r in
    let _x2,_r = xmlparse_predicate _r in
    let _x3,_r = xmlparse_expr _r in
    xmlparse_empty "ExprSIGMA" _r;
    (mk_ExprSIGMA _x1 _x2 _x3),_l
  | XBalise ("ExprPI",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_id) _r in
    let _x2,_r = xmlparse_predicate _r in
    let _x3,_r = xmlparse_expr _r in
    xmlparse_empty "ExprPI" _r;
    (mk_ExprPI _x1 _x2 _x3),_l
  | XBalise ("ExprLambda",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_id) _r in
    let _x2,_r = xmlparse_predicate _r in
    let _x3,_r = xmlparse_expr _r in
    xmlparse_empty "ExprLambda" _r;
    (mk_ExprLambda _x1 _x2 _x3),_l
  | _ -> raise (InvalidXml "ExprSequence|ExprParen|ExprId|ExprFunCall|ExprBin|ExprUn|ExprSeq|ExprSet|RelSet|RelSeqComp|ExprNumber|ExprBool|ExprString|ExprRecords|ExprPred|ExprNuplet|ExprSIGMA|ExprPI|ExprLambda")
  end in
    xmlparse_empty "expr" _r;
    mk_expr _x1, _l
| _ -> raise (InvalidXml "expr")

and xmlparse_exprRecords = function
  XBalise("exprRecords",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("RecordsWithField",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_recordsItem) _r in
    xmlparse_empty "RecordsWithField" _r;
    (mk_RecordsWithField _x1),_l
  | XBalise ("Records",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_expr) _r in
    xmlparse_empty "Records" _r;
    (mk_Records _x1),_l
  | XBalise ("RecordsAccess",_r)::_l ->
    let _x1,_r = xmlparse_expr _r in
    let _x2,_r = xmlparse_id _r in
    xmlparse_empty "RecordsAccess" _r;
    (mk_RecordsAccess _x1 _x2),_l
  | XBalise ("RecordsSet",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_recordsItem) _r in
    xmlparse_empty "RecordsSet" _r;
    (mk_RecordsSet _x1),_l
  | _ -> raise (InvalidXml "RecordsWithField|Records|RecordsAccess|RecordsSet")
  end in
    xmlparse_empty "exprRecords" _r;
    mk_exprRecords _x1, _l
| _ -> raise (InvalidXml "exprRecords")

and xmlparse_recordsItem = function
  XBalise("recordsItem",_r)::_l ->
    let _x1,_r = xmlparse_id _r in
    let _x2,_r = xmlparse_expr _r in
    xmlparse_empty "recordsItem" _r;
    mk_recordsItem _x1 _x2, _l
| _ -> raise (InvalidXml "recordsItem")

and xmlparse_exprSeq = function
  XBalise("exprSeq",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("SeqEnum",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_expr) _r in
    xmlparse_empty "SeqEnum" _r;
    (mk_SeqEnum _x1),_l
  | XBalise ("SeqEmpty",_r)::_l ->
    mk_SeqEmpty,_l
  | _ -> raise (InvalidXml "SeqEnum|SeqEmpty")
  end in
    xmlparse_empty "exprSeq" _r;
    mk_exprSeq _x1, _l
| _ -> raise (InvalidXml "exprSeq")

and xmlparse_exprSet = function
  XBalise("exprSet",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("SetRange",_r)::_l ->
    let _x1,_r = xmlparse_expr _r in
    let _x2,_r = xmlparse_expr _r in
    xmlparse_empty "SetRange" _r;
    (mk_SetRange _x1 _x2),_l
  | XBalise ("SetEnum",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_expr) _r in
    xmlparse_empty "SetEnum" _r;
    (mk_SetEnum _x1),_l
  | XBalise ("SetCompr",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_expr) _r in
    xmlparse_empty "SetCompr" _r;
    (mk_SetCompr _x1),_l
  | XBalise ("SetComprPred",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_id) _r in
    let _x2,_r = xmlparse_predicate _r in
    xmlparse_empty "SetComprPred" _r;
    (mk_SetComprPred _x1 _x2),_l
  | XBalise ("SetEmpty",_r)::_l ->
    mk_SetEmpty,_l
  | XBalise ("SetUnionQuantify",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_id) _r in
    let _x2,_r = xmlparse_predicate _r in
    let _x3,_r = xmlparse_expr _r in
    xmlparse_empty "SetUnionQuantify" _r;
    (mk_SetUnionQuantify _x1 _x2 _x3),_l
  | XBalise ("SetInterQuantify",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_id) _r in
    let _x2,_r = xmlparse_predicate _r in
    let _x3,_r = xmlparse_expr _r in
    xmlparse_empty "SetInterQuantify" _r;
    (mk_SetInterQuantify _x1 _x2 _x3),_l
  | _ -> raise (InvalidXml "SetRange|SetEnum|SetCompr|SetComprPred|SetEmpty|SetUnionQuantify|SetInterQuantify")
  end in
    xmlparse_empty "exprSet" _r;
    mk_exprSet _x1, _l
| _ -> raise (InvalidXml "exprSet")

and xmlparse_number = function
  XBalise("number",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("MinNumber",_r)::_l ->
    mk_MinNumber,_l
  | XBalise ("Number",_r)::_l ->
    let _x1,_r = xmlparse_int _r in
    xmlparse_empty "Number" _r;
    (mk_Number _x1),_l
  | XBalise ("MaxNumber",_r)::_l ->
    mk_MaxNumber,_l
  | _ -> raise (InvalidXml "MinNumber|Number|MaxNumber")
  end in
    xmlparse_empty "number" _r;
    mk_number _x1, _l
| _ -> raise (InvalidXml "number")

and xmlparse_substitution = function
  XBalise("substitution",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("SubstOperCall",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    let _x2,_r = xmlparse_id _r in
    let _x3,_r = (xmlparse_seq xmlparse_expr) _r in
    xmlparse_empty "SubstOperCall" _r;
    (mk_SubstOperCall _x1 _x2 _x3),_l
  | XBalise ("SubstBlock",_r)::_l ->
    let _x1,_r = xmlparse_substitution _r in
    xmlparse_empty "SubstBlock" _r;
    (mk_SubstBlock _x1),_l
  | XBalise ("SubstPrecondition",_r)::_l ->
    let _x1,_r = xmlparse_predicate _r in
    let _x2,_r = xmlparse_substitution _r in
    xmlparse_empty "SubstPrecondition" _r;
    (mk_SubstPrecondition _x1 _x2),_l
  | XBalise ("SubstAssertion",_r)::_l ->
    let _x1,_r = xmlparse_predicate _r in
    let _x2,_r = xmlparse_substitution _r in
    xmlparse_empty "SubstAssertion" _r;
    (mk_SubstAssertion _x1 _x2),_l
  | XBalise ("SubstChoice",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_substitution) _r in
    xmlparse_empty "SubstChoice" _r;
    (mk_SubstChoice _x1),_l
  | XBalise ("SubstIf",_r)::_l ->
    let _x1,_r = (xmlparse_seq     (fun _r -> 
    let _x1,_r = xmlparse_predicate _r in
    let _x2,_r = xmlparse_substitution _r in
    (_x1, _x2),_r)) _r in
    let _x2,_r = (xmlparse_option xmlparse_substitution) _r in
    xmlparse_empty "SubstIf" _r;
    (mk_SubstIf _x1 _x2),_l
  | XBalise ("SubstSelect",_r)::_l ->
    let _x1,_r = (xmlparse_seq     (fun _r -> 
    let _x1,_r = xmlparse_predicate _r in
    let _x2,_r = xmlparse_substitution _r in
    (_x1, _x2),_r)) _r in
    let _x2,_r = (xmlparse_option xmlparse_substitution) _r in
    xmlparse_empty "SubstSelect" _r;
    (mk_SubstSelect _x1 _x2),_l
  | XBalise ("SubstCase",_r)::_l ->
    let _x1,_r = xmlparse_expr _r in
    let _x2,_r = (xmlparse_seq     (fun _r -> 
    let _x1,_r = (xmlparse_seq xmlparse_expr) _r in
    let _x2,_r = xmlparse_substitution _r in
    (_x1, _x2),_r)) _r in
    let _x3,_r = (xmlparse_option xmlparse_substitution) _r in
    xmlparse_empty "SubstCase" _r;
    (mk_SubstCase _x1 _x2 _x3),_l
  | XBalise ("SubstAny",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_id) _r in
    let _x2,_r = xmlparse_predicate _r in
    let _x3,_r = xmlparse_substitution _r in
    xmlparse_empty "SubstAny" _r;
    (mk_SubstAny _x1 _x2 _x3),_l
  | XBalise ("SubstLet",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_id) _r in
    let _x2,_r = xmlparse_predicate _r in
    let _x3,_r = xmlparse_substitution _r in
    xmlparse_empty "SubstLet" _r;
    (mk_SubstLet _x1 _x2 _x3),_l
  | XBalise ("SubstVar",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_id) _r in
    let _x2,_r = xmlparse_substitution _r in
    xmlparse_empty "SubstVar" _r;
    (mk_SubstVar _x1 _x2),_l
  | XBalise ("SubstWhile",_r)::_l ->
    let _x1,_r = (function
    XBalise("cond",_r)::_l ->
   let _x1,_r = xmlparse_predicate _r in
    xmlparse_empty "cond" _r;
    _x1,_l
  |  _ -> raise (InvalidXml "cond")) _r in
    let _x2,_r = xmlparse_substitution _r in
    let _x3,_r = (function
    XBalise("variant",_r)::_l ->
   let _x1,_r = xmlparse_expr _r in
    xmlparse_empty "variant" _r;
    _x1,_l
  |  _ -> raise (InvalidXml "variant")) _r in
    let _x4,_r = (function
    XBalise("inv",_r)::_l ->
   let _x1,_r = xmlparse_predicate _r in
    xmlparse_empty "inv" _r;
    _x1,_l
  |  _ -> raise (InvalidXml "inv")) _r in
    xmlparse_empty "SubstWhile" _r;
    (mk_SubstWhile _x1 _x2 _x3 _x4),_l
  | XBalise ("SubstSkip",_r)::_l ->
    mk_SubstSkip,_l
  | XBalise ("SubstSequence",_r)::_l ->
    let _x1,_r = xmlparse_substitution _r in
    let _x2,_r = xmlparse_substitution _r in
    xmlparse_empty "SubstSequence" _r;
    (mk_SubstSequence _x1 _x2),_l
  | XBalise ("SubstParallel",_r)::_l ->
    let _x1,_r = xmlparse_substitution _r in
    let _x2,_r = xmlparse_substitution _r in
    xmlparse_empty "SubstParallel" _r;
    (mk_SubstParallel _x1 _x2),_l
  | XBalise ("SubstSetEqualIds",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    let _x2,_r = (xmlparse_seq xmlparse_expr) _r in
    xmlparse_empty "SubstSetEqualIds" _r;
    (mk_SubstSetEqualIds _x1 _x2),_l
  | XBalise ("SubstSetEqualFun",_r)::_l ->
    let _x1,_r = xmlparse_expr _r in
    let _x2,_r = (xmlparse_seq xmlparse_expr) _r in
    let _x3,_r = xmlparse_expr _r in
    xmlparse_empty "SubstSetEqualFun" _r;
    (mk_SubstSetEqualFun _x1 _x2 _x3),_l
  | XBalise ("SubstSetEqualRecords",_r)::_l ->
    let _x1,_r = xmlparse_exprRecords _r in
    let _x2,_r = xmlparse_expr _r in
    xmlparse_empty "SubstSetEqualRecords" _r;
    (mk_SubstSetEqualRecords _x1 _x2),_l
  | XBalise ("SubstBecomeSuch",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    let _x2,_r = xmlparse_predicate _r in
    xmlparse_empty "SubstBecomeSuch" _r;
    (mk_SubstBecomeSuch _x1 _x2),_l
  | XBalise ("SubstSetIn",_r)::_l ->
    let _x1,_r = xmlparse_ids _r in
    let _x2,_r = xmlparse_expr _r in
    xmlparse_empty "SubstSetIn" _r;
    (mk_SubstSetIn _x1 _x2),_l
  | _ -> raise (InvalidXml "SubstOperCall|SubstBlock|SubstPrecondition|SubstAssertion|SubstChoice|SubstIf|SubstSelect|SubstCase|SubstAny|SubstLet|SubstVar|SubstWhile|SubstSkip|SubstSequence|SubstParallel|SubstSetEqualIds|SubstSetEqualFun|SubstSetEqualRecords|SubstBecomeSuch|SubstSetIn")
  end in
    xmlparse_empty "substitution" _r;
    mk_substitution _x1, _l
| _ -> raise (InvalidXml "substitution")

and xmlparse_ids = function
  XBalise("ids",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_id) _r in
    xmlparse_empty "ids" _r;
    mk_ids _x1, _l
| _ -> raise (InvalidXml "ids")

and xmlparse_origine = function
  XBalise("origine",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("Local",_r)::_l ->
    mk_Local,_l
  | XBalise ("Remote",_r)::_l ->
    let _x1,_r = (xmlparse_seq     (fun _r -> 
    let _x1,_r = xmlparse_string _r in
    let _x2,_r = xmlparse_string _r in
    (_x1, _x2),_r)) _r in
    xmlparse_empty "Remote" _r;
    (mk_Remote _x1),_l
  | XBalise ("OrgRefine",_r)::_l ->
    let _x1,_r = xmlparse_string _r in
    let _x2,_r = (xmlparse_seq     (fun _r -> 
    let _x1,_r = xmlparse_string _r in
    let _x2,_r = xmlparse_string _r in
    (_x1, _x2),_r)) _r in
    xmlparse_empty "OrgRefine" _r;
    (mk_OrgRefine _x1 _x2),_l
  | XBalise ("Prefix",_r)::_l ->
    let _x1,_r = (xmlparse_seq xmlparse_string) _r in
    xmlparse_empty "Prefix" _r;
    (mk_Prefix _x1),_l
  | _ -> raise (InvalidXml "Local|Remote|OrgRefine|Prefix")
  end in
    xmlparse_empty "origine" _r;
    mk_origine _x1, _l
| _ -> raise (InvalidXml "origine")

and xmlparse_id = function
  XBalise("id",_r)::_l ->
  let _x1,_r = begin   match _r with
    XBalise ("Id",_r)::_l ->
    let _x1,_r = xmlparse_origine _r in
    let _x2,_r = xmlparse_string _r in
    let _x3,_r = (xmlparse_option xmlparse_type_B) _r in
    let _x4,_r = Bbop.xmlparse_kind _r in
    xmlparse_empty "Id" _r;
    (mk_Id _x1 _x2 _x3 _x4),_l
  | _ -> raise (InvalidXml "Id")
  end in
    xmlparse_empty "id" _r;
    mk_id _x1, _l
| _ -> raise (InvalidXml "id")

