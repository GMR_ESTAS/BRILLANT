/*  $Id$ */

%{
open Blast
open Blast_lib 
open Bbop 
open Options
open Int32


exception End_expected of string

exception Parse_error of int * string

let parse_error msg  =
  raise (Parse_error (Parsing.symbol_start (),msg))
    ;;
    
let expr2id = function 
    ExprId id -> id
  | ExprFunCall (ExprId id,[]) -> id
  | _ -> parse_error ("identifier expected")
    
let rec exprSequence_to_exprList es =
  match es with 
    ExprSequence(e1,e2) -> (exprSequence_to_exprList e1)@[e2]
  | _ -> [es]
        ;;

let exprSequence_to_Nuplet es =
  match (exprSequence_to_exprList es) with
  | [x] -> x
  | [] -> failwith "exprSequence_to_Nuplet internal error\n"
  | l -> ExprNuplet l

let unclosed expected = parse_error  (expected^" expected")

let expr_macros = ref [];;
let subst_macros = ref [];;
%}
	
	
%token EOF
	
%token <string> B_operator
	
%token <string>  B_AT
%token <string>  B_NUMBER
%token <string>  B_STRING
%token <string>  B_IDENTIFIER
%token <string>  B_UPPERIDENT
%token <string>  B_CURLYOPEN
%token <string>  B_CURLYCLOSE
%token <string>  B_MACHINE      
%token <string>  B_CONSTRAINTS  
%token <string>  B_SEES 
%token <string>  B_SETS  
%token <string>  B_CONSTANTS    
%token <string>  B_CONCRETE_CONSTANTS
%token <string>  B_ABSTRACT_CONSTANTS
%token <string>  B_VISIBLE_CONSTANTS
%token <string>  B_HIDDEN_CONSTANTS
%token <string>  B_PROPERTIES   
%token <string>  B_INCLUDES     
%token <string>  B_PROMOTES     
%token <string>  B_EXTENDS      
%token <string>  B_USES 
%token <string>  B_VARIABLES    
%token <string>  B_CONCRETE_VARIABLES
%token <string>  B_ABSTRACT_VARIABLES
%token <string>  B_VISIBLE_VARIABLES
%token <string>  B_HIDDEN_VARIABLES
%token <string>  B_INVARIANT    
%token <string>  B_INITIALISATION       
%token <string>  B_OPERATIONS
%token <string>  B_REFINEMENT
%token <string>  B_REFINES      
%token <string>  B_IMPLEMENTATION       
%token <string>  B_IMPORTS      
%token <string>  B_DEFINITIONS
%token <string>  B_DefIdent
%token <string>  B_VALUES       
%token <string>  B_ASSERTIONS
%token <string>  B_or   
%token <string>  B_setminus
%token <string>  B_circ
%token <string>  B_not  
%token <string>  B_SKIP 
%token <string>  B_BEGIN        
%token <string>  B_PRE  
%token <string>  B_IF   
%token <string>  B_THEN 
%token <string>  B_ELSIF        
%token <string>  B_ELSE
%token <string>  B_ASSERT       
%token <string>  B_ANY  
%token <string>  B_WHERE        
%token <string>  B_LET  
%token <string>  B_BE   
%token <string>  B_IN   
%token <string>  B_CHOICE       
%token <string>  B_OR   
%token <string>  B_SELECT       
%token <string>  B_WHEN 
%token <string>  B_CASE 
%token <string>  B_OF   
%token <string>  B_EITHER       
%token <string>  B_VAR  
%token <string>  B_WHILE        
%token <string>  B_DO   
%token <string>  B_VARIANT
%token <string>  B_NAT  
%token <string>  B_NATONE               
%token <string>  B_NATURAL              
%token <string>  B_NATURALONE
%token <string>  B_INT
%token <string>  B_INTEGER      
%token <string>  B_false
%token <string>  B_true
%token <string>  B_POW  
%token <string>  B_FIN  
%token <string>  B_POWONE       
%token <string>  B_FINONE
%token <string>  B_union        
%token <string>  B_inter        
%token <string>  B_UNION        
%token <string>  B_INTER        
%token <string>  B_id   
%token <string>  B_dom  
%token <string>  B_cod  
%token <string>  B_ran  
%token <string>  B_closure
%token <string>  B_closureone
%token <string>  B_card 
%token <string>  B_max  
%token <string>  B_min  
%token <string>  B_mod  
%token <string>  B_SIGMA        
%token <string>  B_PI   
%token <string>  B_seq  
%token <string>  B_iseq 
%token <string>  B_seqone
%token <string>  B_iseqone      
%token <string>  B_perm 
%token <string>  B_conc 
%token <string>  B_front        
%token <string>  B_tail 
%token <string>  B_first        
%token <string>  B_last 
%token <string>  B_size 
%token <string>  B_rev  
%token <string>  B_bool
%token <string>  B_END
%token <string>  B_minus
%token <string>  B_tilde
%token <string>  B_plus
%token <string>  B_power
%token <string>  B_concatseq
%token <string>  B_mul
%token <string>  B_div
%token <string>  B_egal
%token <string>  B_opdef
%token <string>  B_conjonction
%token <string>  B_less
%token <string>  B_greater
%token <string>  B_comma
%token <string>  B_semicolon
%token <string>  B_in
%token <string>  B_dollars
%token <string>  B_setin
%token <string>  B_equivalence
%token <string>  B_subset
%token <string>  B_notsubset
%token <string>  B_implies
%token <string>  B_strictsubset
%token <string>  B_notstrictsubset
%token <string>  B_notin
%token <string>  B_notequal
%token <string>  B_lessorequal
%token <string>  B_greaterorequal
%token <string>  B_partialfunc
%token <string>  B_relation
%token <string>  B_rel
%token <string>  B_fnc
%token <string>  B_relcomp
%token <string>  B_totalfunc
%token <string>  B_partialfunc
%token <string>  B_totalsurject
%token <string>  B_partialsurject
%token <string>  B_totalinject
%token <string>  B_partialinject
%token <string>  B_partialbij
%token <string>  B_bijection
%token <string>  B_operationsig
%token <string>  B_simplesubst
%token <string>  B_CURLYOPEN
%token <string>  B_CURLYCLOSE
%token <string>  B_PARENOPEN
%token <string>  B_PARENCLOSE
%token <string>  B_BRACKOPEN
%token <string>  B_BRACKCLOSE
%token <string>  B_quoteident
%token <string>  B_parallelcomp
%token <string>  B_mapletorpair
%token <string>  B_domainrestrict
%token <string>  B_rangerestrict
%token <string>  B_domainsubstract
%token <string>  B_rangesubstract
%token <string>  B_overrideforward
%token <string>  B_overridebackward
%token <string>  B_relprod
%token <string>  B_preppendseq
%token <string>  B_appendseq
%token <string>  B_prefixseq
%token <string>  B_suffixseq
%token <string>  B_natsetrange
%token <string>  B_unionsets
%token <string>  B_intersectionsets
%token <string>  B_lambda
%token <string>  B_emptyset
%token <string>  B_seqempty
%token <string>  B_point
%token <string>  B_such
%token <string>  B_forall
%token <string>  B_exists
%token <string>  B_double_egal
%token <string>  B_MAXINT
%token <string>  B_MININT
%token <string>  B_prjone
%token <string>  B_prjtwo
%token <string>  B_garde 
%token <string>  B_succ 
%token <string>  B_pred  
%token <string>  B_become
%token <string>  B_becomesuch
%token <string>  B_quote
%token <string>  B_struct
%token <string>  B_rec
%token <string>  B_tree

%token <string>  B_BOOL

%start amn substitution expression
%type <Blast.amn> amn
%type <Blast.substitution> substitution
%type <Blast.expr> expression ExprLevel0 ExprLevel1 ExprLevel2 ExprParen
%type <Blast.predicate> Pred PredLevel0 PredLevel1 PredLevel2 PredParen



            /* Priorités */
            /* point (?) */
%left B_PARENOPEN
%left B_PARENCLOSE


%right B_point
%left B_simplesubst
            /* 30 */
%left B_double_egal
%left B_such
%left B_implies 
%left B_equivalence

%left B_in

            /* 40 */
%left B_conjonction 
%left B_or
            /* 50 */
%left B_notequal 
%left B_less 
%left B_lessorequal
%left B_greater
%left B_greaterorequal
%left B_equal
            /* 60 */

%left B_notin
%left B_subset
%left B_notsubset
%left B_notstrictsubset
%left B_strictsubset

            /* 80 */
%left B_semicolon
            /* 100 */
%left B_relprod 
%left B_parallelcomp
            /* 110 */
%left B_concatseq 
            /* 115 */
%left B_comma
            /* 125 */
%left B_partialfunc 
%left B_relation 
%left B_totalfunc 
%left B_partialinject 
%left B_totalinject
%left B_partialsurject 
%left B_totalsurject 
%left B_bijection
            /* 130 */
%left B_preppendseq  
%left B_appendseq 
%left B_preffixseq  
%left B_suffixseq 
            /* 140 */
%left B_unionsets
%left B_intersectionsets
            /* 160 */
%left B_mapletorpair
%left B_circ B_domainrestrict B_rangerestrict  
%left B_domainsubstract B_rangesubstract
%left B_overrideforward B_overridebackward 
            /*%left B_relprodparallel*/
 

%left B_closure 
%left B_closure1
            /* 170 */
%left B_natsetrange 
            /* 180 */
%left B_plus 
%left B_minus 
            /* 190 */
%left B_mul
%left B_mod 
%left B_div 
            /* 200 */ 
%left B_power
            /* 220 */

            /* 250 */   
%left B_exists
%left B_forall    
%left B_lambda
%left B_quote
            

  
%% 
	
amn : 
  Entity EOF
  {
   expr_macros := [];
   subst_macros := [];
   $1
 }
  ;
 
 
Entity : 
|   Machine             {$1}
|   Refinement      	{$1}
|   Implementation    	{$1}
    ;

Machine : 
| B_MACHINE MchHead MchClauseList End       {Machine($2,$3)}
| B_MACHINE MchHead End      {Machine($2,[])}
    ;

Refinement : 
    B_REFINEMENT MchHead  Refines RefClauseList End    
	    {Refinement($2,(Refines $3)::$4)}
|  B_REFINEMENT MchHead  Refines End     {Refinement($2,[Refines $3])}
    ;


Implementation : 
    B_IMPLEMENTATION MchHead Refines ImpClauseList End  
    {Implementation($2,(Refines $3)::$4)}
|  B_IMPLEMENTATION MchHead Refines End  
    {Implementation($2,[Refines $3])}
    ;

Refines : 
    B_REFINES Refined     {$2}
    ;

Refined : 
    MchName    {Id(Local,$1,None,MachineName)}
    ;



MchHead : 
    MachineName               {$1}
    ;

MachineName : 
    MchName B_PARENOPEN MchParams  B_PARENCLOSE    {(Id(Local,$1,None,MachineName),$3)}
| MchName                   {(Id(Local,$1,None,MachineName),[])}
    ;
  
MchName : 
    Identifier     {$1}
    ;

MchParams : 
    InstanceDefList    {$1}  
    ;


MchClauseList : 
    MchClauseList MchClause      {$1@[$2]}
| MchClause         {[$1]}
    ;


RefClauseList : 
    RefClauseList RefClause  {$1@[$2]}
| RefClause     {[$1]}
    ;

ImpClauseList : 
    ImpClauseList ImpClause  
    {$1@[$2]}
| ImpClause     
    {[$1]}
    ;

/*
\section{Abstract Machine Clauses}
*/

MchClause : 
          | Constraints         {$1}
          | Sees                {$1}
          | Invariant           {$1}
          | Initialisation      {$1}
          | Operations          {$1}
          | Definitions         {$1}
          | Sets                {$1}
          | Constants           {$1}
          | ConstantsVisible    {$1}
          | ConstantsHidden     {$1}
          | ConstantsConcrete   {$1}
          | ConstantsAbstract   {$1}
          | Properties          {$1}
          | Variables           {$1}
          | VariablesConcrete   {$1}
          | VariablesHidden     {$1}
          | VariablesAbstract   {$1}
          | VariablesVisible    {$1}
          | Promotes            {$1}
          | Extends             {$1}
          | Assertions          {$1}
          | Includes            {$1}
          | Uses                {$1}
              ;

/*
\section{Refinement Machine Clauses}
*/

RefClause : 
	  | Constraints       {$1} 
          | Includes            {$1}
          | Promotes            {$1}
          | Definitions         {$1}
          | Sees                {$1}
          | Sets                {$1}
          | Properties          {$1}
          | Constants           {$1}
          | ConstantsVisible    {$1}
          | ConstantsHidden     {$1}
          | ConstantsConcrete   {$1}
          | Variables           {$1}
          | VariablesHidden     {$1}
          | VariablesConcrete   {$1}
          | VariablesAbstract   {$1}
          | VariablesVisible    {$1}
          | Invariant           {$1}
          | Extends             {$1}
          | Assertions          {$1}
          | Initialisation      {$1}
          | Operations          {$1}
          | ConstantsAbstract   {$1}
              ;

/*
\section{Implementation Machine Clauses}
*/

ImpClause :
          | Imports             {$1}
          | Sees                {$1}
          | Variables           {$1}
          | VariablesVisible    {$1}
          | VariablesConcrete   {$1}
          | Definitions         {$1}
          | Promotes            {$1}
          | Extends             {$1}
          | Sets                {$1}
          | Constants           {$1}
          | ConstantsVisible    {$1}
          | ConstantsConcrete   {$1}
          | Properties          {$1}
          | Invariant           {$1}
          | Assertions          {$1}
          | Initialisation      {$1}
          | Operations          {$1}
          | Values              {$1}
              ;

/* \subsection{Clause CONSTRAINTS} */

Constraints : 
	      B_CONSTRAINTS Pred
/*              {Location.put (Constraints $2)} */
	    {mk_Constraints $2}
	      ;
	    
/* \subsection{Clause INCLUDES} */
Includes : 
	      B_INCLUDES ImportedList
              {Includes($2)}
	      ;

/* \subsection{Clause EXTENDS} */
Extends : 
	      B_EXTENDS ImportedList
              {Extends($2)}
	      ;

/* \subsection{Clause IMPORTS} */
Imports : 
	      B_IMPORTS ImportedList
              {Imports($2)}
	      ;


ImportedList : 
	  | ImportedList B_comma Imported
              {$1@[$3]}
          | Imported
              {[$1]}
              ;

Imported : 
	      ImplementationName  
              {$1}
	      ; 


ImplementationName : 
	  | ImpName ImpParams  
              {($1,$2)}
          | ImpName             
              {($1,[])}
              ;

ImpName : 
	  | PrefixMachineName           
              {$1}
          | B_PARENOPEN ImpName B_PARENCLOSE
              {$2}
              ; 

ImpParams : 
	      B_PARENOPEN ExprLevel0bis B_PARENCLOSE
	      {exprSequence_to_exprList $2}  
	      ;


PrefixMachineNameList : 
	  | PrefixMachineNameList B_comma PrefixMachineName
              {$1@[$3]}
          | PrefixMachineName
              {[$1]}
              ;
	    
PrefixMachineName : 
	      Identifier B_point MchName
              {Id(Prefix [$1],$3,None,MachineName)} 
          | MchName
              {Id(Local,$1,None,MachineName)}
              ;

/* \subsection{Clause USES} */
Uses : 
	      B_USES  PrefixMachineNameList
              {Uses($2)}
	      ;

/* \subsection{Clause PROMOTES} */
Promotes : 
	      B_PROMOTES PromotedList
              {Promotes($2)}
	      ;

PromotedList : 
	      PromotedList B_comma Promoted
              {$1@[$3]}
          | Promoted    
              {[$1]}
              ;

Promoted : 
	      Identifier           
              {Id(Local,$1,None,OpName)}
          | Identifier B_point Identifier 
              {Id(Prefix [$1],$3,None,OpName)}
              ;

/* \subsection{Clause SEES} */
Sees : 
	      B_SEES SeenList
              {Sees($2)}
	      ;

SeenList : 
	      SeenList B_comma Seen
              {$1@[$3]}
          | Seen                
              {[$1]}
              ;

Seen : 
	      PrefixMachineName                
              {$1}
          | B_PARENOPEN Seen B_PARENCLOSE
              {$2}
              ;

/* \subsection{Clause SETS} */
Sets : 
	      B_SETS SetList
              {Sets($2)}
	      ;

/* \subsection{Clause CONSTANTS} */
Constants : 
	      B_CONSTANTS ConstantList
              {ConstantsConcrete($2)}
	      ;

ConstantsVisible : 
	      B_VISIBLE_CONSTANTS ConstantList
              {ConstantsConcrete($2)}
	      ;

ConstantsHidden : 
	      B_HIDDEN_CONSTANTS ConstantList
              {ConstantsHidden($2)}
	      ;

ConstantsAbstract :  
	      B_ABSTRACT_CONSTANTS VarSimpleList
              {ConstantsAbstract($2)}
	      ; 

ConstantsConcrete :  
	      B_CONCRETE_CONSTANTS VarSimpleList
              {ConstantsConcrete($2)}
	      ; 
	    
/* \subsection{Clause PROPERTIES} */
Properties : 
	      B_PROPERTIES PropertyPredicate
              {Properties $2}
	      ;

PropertyPredicate : 
	      Pred        
              {$1}
	      ;

/* \subsection{Clause VARIABLES} */
Variables :  
	      B_VARIABLES VarSimpleList
              {VariablesAbstract($2)}
	      ;

VariablesHidden   :  
	      B_HIDDEN_VARIABLES VarSimpleList
              {VariablesHidden($2)}
	      ;

VariablesVisible   :  
	      B_VISIBLE_VARIABLES VarSimpleList
              {VariablesConcrete($2)}
	      ;

VariablesConcrete  :  
	      B_CONCRETE_VARIABLES VarSimpleList
              {VariablesConcrete($2)}
	      ;

VariablesAbstract :  
	      B_ABSTRACT_VARIABLES VarSimpleList
              {VariablesAbstract($2)}
	      ; 

/* \subsection{Clause INVARIANT} */
Invariant : B_INVARIANT InvariantPredicate
              {Invariant $2}
  ;

InvariantPredicate : Pred       
              {$1}
  ;
/* \subsection{Clause ASSERTIONS} */
Assertions : B_ASSERTIONS AssertionList
              {Assertions($2)}
  ;

AssertionList : AssertionList B_semicolon Pred
              {$1@[$3]}
          | Pred
              {[$1]}
;

/* \subsection{Clause INITIALISATION} */
Initialisation : B_INITIALISATION Substitution
              {Initialisation($2)}
  ;

/* \subsection{Clause OPERATIONS} */
Operations : B_OPERATIONS OperList
              {Operations($2)}
  ;
OperList : OperList B_semicolon Operation
              {$1@[$3]}
          | Operation           
              {[$1]}
      ;

Definitions : B_DEFINITIONS DefList
              {Definitions($2)}
  ;

DefList : DefList  B_semicolon Definition
              {$1@[$3]}
          | Definition    
              {[$1]}
      ;

Definition : DefHead B_double_egal DefBody
              {let (Id(_,n,_,_) as id,params) = $1 in
	       let _ = match $3 with
		 ExprDefBody bdy -> 
		   expr_macros := (n,(params,bdy))::!expr_macros
	       | SubstDefBody bdy ->
		   subst_macros := (n,(params,bdy))::!subst_macros in
	       Def(id,params,$3)} 
          | B_STRING 
              {FileDef($1)}  
      ;

DefBody :  ExprLevel0
              {ExprDefBody($1)}
          | SubstDelimited	
              {SubstDefBody($1)}
      ;

SubstDelimited :
    SubstBlock       {$1}
  | SubstPre         {$1}
  | SubstIf          {$1}
  | SubstChoice      {$1}
  | SubstSelect      {$1}
  | SubstCase        {$1}
  | SubstVar         {$1}
  | SubstAny         {$1}
  | SubstLet         {$1}
  | SubstWhile       {$1}
  | SubstAssert      {$1}
  | SubstSkip        {$1}
	;

DefHead : Identifier   {(Id(Local,$1,None,OpName),[])}
| Identifier  B_PARENOPEN DefParams ParenClose  {(Id(Local,$1,None,OpName),$3)}

      ;

DefParams : InstanceDefList     
              {$1}
  ;

InstanceDefList : InstanceDefList B_comma InstanceDef
              {$1@[$3]}
          | InstanceDef
              {[$1]}
;

InstanceDef : Identifier
              {Id(Local,$1,None,MachineName)}
;

Values : B_VALUES ValuationList 
              {Values($2)}
  ;

ValuationList : ValuationList B_semicolon Valuation     
              {$1@[$3]}
          | Valuation   
              {[$1]}
      ;

Valuation : InstanceDef B_egal ExprLevel0 
              {($1,$3)}
              ;
VarSimpleList : VarSimpleList B_comma VarSimple
              {$1@[$3]}
          | VarSimple   
              {[$1]}
      ;

Identifier :  B_IDENTIFIER      
              {$1}
  ;

SetList : SetList SetSep Set       
              {$1@[$3]}
          | Set                         
              {[$1]}
      ;

SetSep : B_semicolon {$1} | B_comma {$1}
	;

Set : SetName SetDef B_CURLYOPEN ElementList B_CURLYCLOSE       
              {SetEnumDec($1,$4)}
          | SetName                     
              {SetAbstDec($1)}
      ;



SetDef : B_egal         
              {$1}
  ;


SetName : Identifier            
              {Id(Local,$1,None,Set)}

          |           B_BOOL
              {Id(Local,"BOOL",None,Set)}
  ;

ElementList : VarSimpleList
              {$1}
;
                 
ConstantList : ConstantList B_comma Constant    
              {$1@[$3]}
          | Constant            
              {[$1]}
      ;

Constant : VarSimpleOrPrefix           
              {let Id(p,i,t,_) = $1 in Id(p,i,t,Cst)}
        ;


VarSimpleOrPrefixList : VarSimpleOrPrefixList B_comma VarSimpleOrPrefix
                {$1@[$3]}
            | VarSimpleOrPrefix
                {[$1]}
                ;
VarSimpleOrPrefix : Identifier
                {Location.put(Id(Local,$1,None,UnknownId))}
            |  VarSimpleOrPrefix B_point Identifier
                { match $1 with
		  Id (Prefix pr,i,t,k) -> Location.put(Id(Prefix (pr@[i]),$3,t,k))
		| Id (Local,i,t,k) -> Location.put(Id(Prefix [i],$3,t,k))
		| _ -> failwith "Bparser.VarSimpleOrPrefix"
		}
                ;

VarSimple : Identifier
      {Location.put(Id(Local,$1,None,UnknownId))}
	    |  Identifier B_in ExprLevel3
      {Location.put(Id(Local,$1,None,UnknownId))}
  ;


Operation : OperSig OperDef OperBody    
                {let (name,params_out,params_in) = $1
                in
                (name,params_out,params_in,$3)
                }
      ;

OperBody : SubstLevel1          
        {$1}
  ;

OperDef : B_egal        
      {$1}
  ;

OperSig : VarSimpleList B_operationsig DefHead
  {let (name,paramsOut) = $3 in (name,$1,paramsOut)}
	    | DefHead
		{let (name,paramsOut) = $1 in (name,[],paramsOut)}
		;

/*
\section{Substitutions}
*/

substitution : Substitution {$1}
  ;
Substitution : SubstSequence    
  {$1}
	    | SubstParallel       
		{$1}
	    | SubstLevel1
		{$1}
		;

SubstSequence : Substitution B_semicolon Substitution   
  {SubstSequence($1,$3)}
  ;

SubstParallel : Substitution B_parallelcomp Substitution
  {SubstParallel($1,$3)}
  ;


SubstLevel1 : 
	    | SubstSetIn       {$1}
	    | AffectMultiple   {$1}
	    | SubstBlock       {$1}
	    | SubstPre         {$1}
	    | SubstIf          {$1}
	    | SubstChoice      {$1}
	    | SubstSelect      {$1}
	    | SubstCase        {$1}
	    | SubstVar         {$1}
	    | SubstAny         {$1}
	    | SubstLet         {$1}
	    | SubstWhile       {$1}
	    | SubstAssert      {$1}
	    | SubstSkip        {$1}

	    | SimpleFunCall    {
	      let (Id(pr,s,_,_) as f,l) = $1 in
	      if !expansion && pr = Local then
	      	try
		  let (args,bdy) = List.assoc s !subst_macros in
		  let substs = List.map2 (fun i e -> ExprId i,e) args l in
		  let subst exp = try List.assoc exp substs with _ -> exp in
		  Blast_maps.map_expr_substitution subst bdy
	      	with _ -> SubstOperCall([],f,l)
	      else
		SubstOperCall([],f,l)
	      }

		;

SubstSetIn : VarSimpleOrPrefixList B_setin ExprLevel0
  {SubstSetIn($1,$3)}
  ;


SimpleFunCall : 
	    | VarSimpleOrPrefix
		{($1,[])}
	    | VarSimpleOrPrefix ExprListParen
		{($1,$2)}
		;

FunCall : 
	    | VarSimpleOrPrefix
		{
		 let (Id(pr,s,_,_) as f) = $1 in
		 if !expansion && pr = Local 
		 then
	      	   try
		     let (args,bdy) = List.assoc s !expr_macros in
		     if args <> [] 
		     then parse_error "Bad macro call";
		     (bdy,[])
		   with Not_found -> (ExprId(f),[])
		 else (ExprId(f),[])
	       }
	    | VarSimpleOrPrefix ExprListParen
		{
		 let (Id(pr,s,_,_) as f) = $1 and l = $2 in
		 if !expansion && pr = Local 
		 then
	      	   try
		     let (args,bdy) = List.assoc s !expr_macros in
		     let substs = List.map2 (fun i e -> ExprId i,e) args l in
		     let subst exp = try List.assoc exp substs with _ -> exp in
		     (Blast_maps.map_expr_expr subst bdy,[])
	      	   with Invalid_argument _ -> parse_error "Bad macro call"
		   | _ -> (ExprId(f),l)
		 else
		   (ExprId(f),l)
	       }
	    | FunCall ExprListParen
		{let (f,l) = $1 in (ExprFunCall(f,l),$2)}
		;

Var : 
	    | FunCall
		{
		 match $1 with
		   ((ExprId _) as id,[]) -> id
		 | f,[] -> f
		 | f,l -> ExprFunCall(f,l)}
		;

VarFunCall : 
	    | VarSimpleOrPrefix ExprListParen
		{(ExprId $1,$2)}
	    | FunCall ExprListParen
		{(let (f,l) = $1 in ExprFunCall(f,l),$2)}
		;       

/*  regroupement des formes debutant par VarList */
AffectMultiple : 
            | VarSimpleOrPrefixList B_in PredParen
		{SubstBecomeSuch($1,$3)}
            | VarSimpleOrPrefixList B_operationsig SimpleFunCall
                {let (f,l) = $3 in SubstOperCall($1,f,l)}
            | VarSimpleOrPrefixList  B_simplesubst ExprList
		{SubstSetEqualIds($1,$3)}
            | VarFunCall  B_simplesubst ExprLevel0
		{let (f,l) = $1 in SubstSetEqualFun(f,l,$3)}
              ;


SubstAssert : B_ASSERT Pred Then Substitution End   
		{SubstAssertion($2,$4)}
		;

SubstBlock : Begin Substitution End         
		{SubstBlock($2)}
		;

SubstPre : B_PRE Pred Then Substitution End      
		{SubstPrecondition($2,$4)}
		;

SubstSkip : B_SKIP
		{SubstSkip}
		;

SubstIf : B_IF ThenPart B_ELSE Substitution End       
		{SubstIf($2,Some($4))}
	    | B_IF ThenPart End         
		{SubstIf($2,None)}
		;

ThenPart : ThenPart B_ELSIF Pred Then Substitution    
		{$1@[($3,$5)]}    
	    | Pred Then Substitution    
		{[($1,$3)]}
		;

SubstChoice : B_CHOICE  OrPart  End   
		{SubstChoice($2)}
		;

OrPart : OrPart B_OR Substitution       
		{$1@[$3]}
	    | Substitution                
		{[$1]}
		;

SubstSelect : B_SELECT SelectPart B_ELSE Substitution End     
		{SubstSelect($2,Some($4))}
	    | B_SELECT SelectPart End           
		{SubstSelect($2,None)}
		;

SelectPart : SelectPart B_WHEN Pred Then Substitution         
		{$1@[($3,$5)]}
	    | Pred Then Substitution    
		{[($1,$3)]}
		;

SubstCase : B_CASE ExprLevel0 B_OF B_EITHER CasePart End End
		{SubstCase($2,$5,None)}      
	    | B_CASE ExprLevel0 B_OF B_EITHER CasePart B_ELSE Substitution End End
		{SubstCase($2,$5,Some($7))}     
		;

CasePart : CasePart B_OR ExprList Then Substitution   
		{$1@[($3,$5)]}
	    | ExprList Then Substitution        
		{[($1,$3)]}
		;


SubstVar : B_VAR VarSimpleList B_IN Substitution End          
		{SubstVar($2,$4)}
		; 

SubstAny : B_ANY VarSimpleList B_WHERE Pred Then Substitution End   
		{SubstAny($2,$4,$6)}
		;

SubstLet : B_LET VarSimpleList B_BE PredLetList B_IN Substitution End         
		{SubstLet($2,$4,$6)}
		;

PredLetList : PredLetList B_conjonction PredLet
		{mk_PredBin And $1 $3}
	    | PredLet
		{$1}
		;

PredLet : Identifier B_egal ExprLevel0
		{mk_PredAtom Equal (ExprId(Id(Local,$1,None,LocVar))) $3}
		;

SubstWhile : 
	    | B_WHILE Pred 
		B_DO Substitution 
		B_INVARIANT Pred
		B_VARIANT ExprLevel0 
		End
		{SubstWhile($2,$4,$8,$6)}
	    | B_WHILE Pred 
		B_DO Substitution 
		B_VARIANT ExprLevel0 
		B_INVARIANT Pred
		End  
		{SubstWhile($2,$4,$6,$8)}
		;

/*
\section{Expressions}\label{gc:expressions}
*/
expression : ExprLevel0 
		{$1} 
		;
	      
Pred: PredLevel0 {$1}
		;
	      
PredLevel0 : PredImplies         
		{$1}
	    | PredLevel1        
		{$1}
		;

PredImplies : PredLevel0 B_implies PredLevel1
		{mk_PredBin Implies $1 $3}
		;

PredLevel1 : PredAnd    {$1}
	    | PredOr           {$1}
	    | PredLevel2       {$1}
		;

PredAnd : PredLevel1 B_conjonction PredLevel2
		{mk_PredBin And $1 $3}
		;

PredOr : PredLevel1 B_or PredLevel2
		{mk_PredBin Ou $1 $3}
		;

PredLevel2 : 
     | SetMembership    {$1}
     | PredEqual        {$1}
     | PredEquiv        {$1}
     | PredLevel3       {$1}
	 ;

SetMembership : ExprLevel0bis B_in ExprLevel0
	 {mk_PredAtom In (exprSequence_to_Nuplet $1) $3}
	 ;
       
PredEqual : ExprLevel0 B_egal ExprLevel0
	 {mk_PredAtom Equal $1 $3}
	 ;

PredEquiv : PredLevel2 B_equivalence PredLevel3
	 {mk_PredBin Equiv $1 $3}
	 ;


PredLevel3 :
     | Subset                    {$1}
     | NotSubset                 {$1}
     | StrictSubset              {$1}
     | NotStrictSubset           {$1}
     | NotSetMembership          {$1}
     | PredNotEqual              {$1}
     | LessThan                  {$1}
     | LessThanEqual             {$1}
     | GreaterThan               {$1}
     | GreaterThanEqual          {$1}
     | PredNot                   {$1}
     | PredForAll                {$1}
     | PredExists                {$1}
     | PredParen                 {PredParen($1)}
         ;

Subset : ExprLevel0 B_subset ExprLevel0
	 {mk_PredAtom SubSet $1 $3}
	 ;
       
NotSubset : ExprLevel0 B_notsubset ExprLevel0
	{mk_PredAtom NotSubSet $1 $3}
	 ;

StrictSubset : ExprLevel0 B_strictsubset ExprLevel0
	{mk_PredAtom StrictSubSet $1 $3}
	 ;

NotStrictSubset : ExprLevel0 B_notstrictsubset ExprLevel0
	{mk_PredAtom NotStrictSubSet $1 $3}
	 ;

NotSetMembership : ExprLevel0bis B_notin ExprLevel0
	{mk_PredAtom NotIn (exprSequence_to_Nuplet $1) $3 }
	 ;

PredNotEqual : ExprLevel0 B_notequal ExprLevel0
	 {mk_PredAtom NotEqual $1 $3}
	 ;

LessThan : ExprLevel1 B_less ExprLevel0
	 {mk_PredAtom Less $1 $3}
	 ;

LessThanEqual : ExprLevel0 B_lessorequal ExprLevel0
	 {mk_PredAtom LessEqual $1 $3}
	 ;

GreaterThan : ExprLevel0 B_greater ExprLevel0   
	 {mk_PredAtom Greater $1 $3}
	 ;

GreaterThanEqual : ExprLevel0 B_greaterorequal ExprLevel0
	 {mk_PredAtom GreaterEqual $1 $3}
	 ;

PredNot : B_not PredLevel3
         {mk_PredNegation $2}
	 ;

PredForAll : B_forall VarSimpleList B_point PredParen
         {mk_PredForAll $2 $4}
     | B_forall BrackVarSimpleList B_point PredParen
         {mk_PredForAll $2 $4}
         ;

PredExists : B_exists VarSimpleList B_point PredParen
         {mk_PredExists $2 $4}
     | B_exists BrackVarSimpleList B_point PredParen   
         {mk_PredExists $2 $4}
         ;

ExprLevel0bis : 
	  ExprLevel0bis B_comma ExprLevel0  {ExprSequence($1,$3)}
	| ExprLevel0      {$1}
	    ;

ExprLevel0 : ExprLevel1 {$1}
	 ;


ExprLevel1 : ExprLevel2 	        {$1}
	 ;


ExprLevel2 : Relation                    {$1}
     | PartialFunc               {$1}
     | TotalFunc                 {$1}
     | PartialInject             {$1}
     | TotalInject               {$1}
     | PartialSurject            {$1}
     | TotalSurject              {$1}
     | Bijection                 {$1}
     | BoolEvaluation            {$1}
     | ExprLevel2bis             {$1}
         ;

BoolEvaluation : B_bool PredParen       
         {Location.put (ExprPred($2))}
	 ;

PartialFunc : ExprLevel2 B_partialfunc ExprLevel0
	 {mk_ExprBin PartialFunc $1 $3} 
	 ;

Relation : ExprLevel2 B_relation ExprLevel0
         {Location.put (RelSet($1,$3))}
	 ;

TotalFunc : ExprLevel2 B_totalfunc ExprLevel0
	 {mk_ExprBin TotalFunc $1 $3} 
	 ;

PartialInject : ExprLevel2 B_partialinject ExprLevel0
	 {mk_ExprBin PartialInj $1 $3} 
	 ;

TotalInject : ExprLevel2 B_totalinject ExprLevel0
	 {mk_ExprBin TotalInj $1 $3} 
	 ;

PartialSurject : ExprLevel2 B_partialsurject ExprLevel0
	 {mk_ExprBin PartialSurj $1 $3} 
	 ;

TotalSurject : ExprLevel2 B_totalsurject ExprLevel0
	 {mk_ExprBin TotalSurj $1 $3} 
	 ;

Bijection : ExprLevel0 B_bijection ExprLevel0           
	 {mk_ExprBin TotalBij $1 $3} 
	 ;

ExprLevel2bis : ExprParen {$1}
     | ExprLevel3 {$1}
	 ;

ExprParen : B_PARENOPEN ExprLevel0 B_PARENCLOSE
	 {Location.put (ExprParen($2))}
	 ;

PredParen : B_PARENOPEN Pred B_PARENCLOSE        {$2}
	 ;


ExprLevel3 : MapletOrPair        {$1}
     | DomainRestrict            {$1}
     | RangeRestrict             {$1}
     | DomainSubstract           {$1}
     | RangeSubstract            {$1}
     | OverRideForward           {$1}
     | OverRideBackward          {$1}
     | RelProd                   {$1}
     | RelProdParallel           {$1}
     | SeqPrepend                {$1}
     | SeqAppend                 {$1}
     | SeqPreffix                {$1}
     | SeqSuffix                 {$1}
     | SeqConcat                 {$1}
     | RelSeqComp                {$1}

       ;

RelSeqComp : B_PARENOPEN RelSeqCompList B_PARENCLOSE
	 {Location.put (RelSeqComp($2))}
	 ;


RelSeqCompList : ExprLevel0 B_semicolon RelSeqCompList          {$1::$3}
     | ExprLevel0         {[$1]}
         ;



MapletOrPair : 
       B_PARENOPEN ExprLevel2bis B_mapletorpair ExprLevel2bis B_PARENCLOSE 
         {Location.put (ExprNuplet($2::[$4]))}
     |  ExprLevel2bis B_mapletorpair ExprLevel2bis 
         {Location.put (ExprNuplet($1::[$3]))}
     | B_PARENOPEN ExprList B_PARENCLOSE
         {Location.put (ExprNuplet($2))}
	 ;
         
    
DomainRestrict : ExprLevel2bis B_domainrestrict ExprLevel0      
         {Location.put (ExprBin(DomRestrict,$1,$3))}
	 ;

RangeRestrict : ExprLevel2bis B_rangerestrict ExprLevel0        
         {Location.put (ExprBin(RanRestrict,$1,$3))}
	 ;

DomainSubstract : ExprLevel2bis B_domainsubstract ExprLevel0            
         {Location.put (ExprBin(DomSubstract,$1,$3))}
	 ;

RangeSubstract : ExprLevel2bis B_rangesubstract ExprLevel0      
         {Location.put (ExprBin(RanSubstract,$1,$3))}
	 ;

OverRideForward : ExprLevel2bis B_overrideforward ExprLevel0            
	 {Location.put (ExprBin(OverRideFwd,$1,$3))}  
	 ;

OverRideBackward : ExprLevel2bis B_overridebackward ExprLevel0          
         {Location.put (ExprBin(OverRideBck,$1,$3))} 
	 ;


RelProd : ExprLevel2bis B_relprod ExprLevel0            
         {Location.put (ExprBin(RelProd,$1,$3))}
	 ;
       
RelProdParallel : B_PARENOPEN ExprLevel2bis B_parallelcomp ExprLevel0 B_PARENCLOSE
         {Location.put (ExprBin(ParallelComp,$2,$4))}
	 ;

/*
\section{Expressions sur les  séquences}
*/
SeqPrepend : ExprLevel2bis B_preppendseq ExprLevel0                     
         {Location.put (ExprBin(PrependSeq,$1,$3))}
	 ;

SeqAppend : ExprLevel2bis B_appendseq ExprLevel0        
         {Location.put (ExprBin(AppendSeq,$1,$3))}
	 ;

SeqPreffix : ExprLevel2bis B_preffixseq ExprLevel2bis /* IRIT */
       {Location.put (ExprBin(PrefixSeq,$1,$3))}
	 ;

SeqSuffix : ExprLevel2bis B_suffixseq ExprLevel2bis  /* IRIT */      
       {Location.put (ExprBin(SuffixSeq,$1,$3))}
	 ;
       
SeqConcat : ExprLevel2bis B_concatseq ExprLevel0        
         {Location.put (ExprBin(ConcatSeq,$1,$3))}
	 ;

/*
\section{Expressions arithmétiques}\label{sec:GC-expr-arith}
*/
ExprLevel3 : 
     | AdditionNats              {$1}
     | DiffNats                  {$1}
     | NatSetRange               {$1}
	 ;

AdditionNats : ExprLevel2bis B_plus ExprLevel2bis /* IRIT */
         {Location.put (ExprBin(Plus,$1,$3))}
	 ;

DiffNats : ExprLevel3 B_minus ExprLevel2bis /* IRIT */
  {Location.put (ExprBin(Minus,$1,$3))}
     | ExprParen B_minus ExprLevel2bis /* IRIT */
	 {Location.put (ExprBin(Minus,$1,$3))}
	 ;

NatSetRange : ExprLevel2bis B_natsetrange ExprLevel2bis /* IRIT */
       {Location.put (ExprBin(NatSetRange,$1,$3))}
     | B_PARENOPEN ExprLevel2bis B_natsetrange ExprLevel2bis B_PARENCLOSE /* IRIT */
       {Location.put (ExprBin(NatSetRange,$2,$4))}
	 ;

ExprLevel3 : 
     | ProdSetaOrNats            {$1}
     | Puissance                 {$1}
     | DivisionNats              {$1}
     | RemainderNats             {$1}
     | Successor                 {$1}
     | Predecessor               {$1}
	 ;

Puissance : ExprLevel2bis B_power ExprLevel0
         {Location.put (ExprBin(Puissance,$1,$3))}
	 ;

ProdSetaOrNats : ExprLevel2bis B_mul ExprLevel0
         {Location.put (ExprBin(Mul,$1,$3))}
	 ;

DivisionNats : ExprLevel2bis B_div ExprLevel0         
         {Location.put (ExprBin(Div,$1,$3))}
	 ;

RemainderNats : ExprLevel2bis B_mod ExprLevel0
         {Location.put (ExprBin(Mod,$1,$3))}
	 ;

Predecessor : B_pred ExprParen  
         {Location.put (ExprUn(Pred,$2))}
	 ; 

Successor : B_succ ExprParen
         {Location.put (ExprUn(Succ,$2))}
	 ; 

ExprLevel3 :UnionSets			{$1}
	| IntersectionSets		{$1}
	    ;

UnionSets : ExprLevel2bis B_unionsets ExprLevel0           
       {Location.put (ExprBin(UnionSets,$1,$3))}
	 ;

IntersectionSets : ExprLevel2bis B_intersectionsets ExprLevel0  
       {Location.put (ExprBin(InterSets,$1,$3))}
         ;

ExprLevel3 :  RelImage      {$1}
  | FunctionInverse           {$1}
  | FunctionApplic            {$1} 
      ;

FunctionApplic : ExprLevel2bis ExprListParen    
         {Location.put (ExprFunCall($1,$2))}
      ;

RelImage : ExprLevel2bis SequenceEnum
         { match $2 with
	   [x] -> Location.put (ExprBin(Image,$1,x))
	 | _ -> parse_error "RelImage"
	 }
      ;

FunctionInverse : ExprLevel2bis B_tilde         
      {Location.put (ExprUn(Tilde,$1))}
      ;

ExprLevel3 : 
  | SequenceEnum			{ExprSeq(SeqEnum($1))}
  | SetComprEnum			{ExprSet($1)}
  | Naturals				{ExprId(Id(Local,$1,None,Set))}
  | Var					{$1}
  | SetEmpty				{ExprSet($1)}
  | Number				{ExprNumber($1)}	
  | String				{$1}
  | RelExpr				{$1}
  | SeqExpr				{$1}
  | GeneralExpr				{$1}
  | Lambda				{$1}      
         ;


RelExpr :
       RelFnc                    {$1}
     | FncRel                    {$1}
         ;

SeqExpr :
     | SeqEmpty                  {ExprSeq($1)}
         ;

GeneralExpr :
     | GeneralUnion     {$1}
     | GeneralInter     {$1}
     | GeneralSum       {$1}
     | GeneralProd      {$1}
         ;

ExprLevel3 : FunName1 ExprLevel2bis
         {ExprUn($1,$2)} 
	 ;

ExprLevel3 : FunName2Unaire B_PARENOPEN ExprLevel0 B_PARENCLOSE
         {ExprUn($1,$3)}
	 ;

ExprLevel3 : FunName2Binaire B_PARENOPEN ExprLevel0 B_comma ExprLevel0 B_PARENCLOSE
         {ExprBin($1,$3,$5)}
	 ;

ExprLevel3 : B_conc B_PARENOPEN ExprLevel0 B_PARENCLOSE
         {ExprUn(Conc,$3)}
     | B_conc RelSeqComp
         {ExprUn(Conc,$2)}
	 ;

FunName1 : B_minus {UMinus}
     | B_closure{Closure}
     | B_closureone{ClosureOne}
     | B_cod {Cod}
     | B_perm{Perm}
	 ;

FunName2Unaire :
       B_min{Min}
     | B_max {Max}
     | B_card {Card}
     | B_POW {Pow}
     | B_POWONE {PowOne}
     | B_FIN {Fin}
     | B_FINONE {FinOne}
     | B_id{Identity}
     | B_dom {Dom}
     | B_ran {Ran}
     | B_seq {Seq}
     | B_seqone {SeqOne}
     | B_iseq {ISeq}
     | B_iseqone{ISeqOne}
     | B_size{Size}
     | B_first {First}
     | B_last{Last}
     | B_front{Front}
     | B_tail {Tail}
     | B_rev {Rev}
     | B_union {Union}
     | B_inter {Inter}
	 ;

FunName2Binaire :
       B_power{Puissance}
     | B_plus{Plus}
     | B_mul{Mul}
     | B_div{Div}
     | B_mod{Mod}
     | B_natsetrange{NatSetRange}
     | B_prjone{Prj1}     
     | B_prjtwo{Prj2}     
	 ;

ExprListParen :  B_PARENOPEN ExprList ParenClose     {$2}
	 ;



RelFnc : B_fnc B_PARENOPEN ExprLevel0 B_PARENCLOSE    
	 {ExprUn(RelFnc,$3)}
	 ;

FncRel : B_rel B_PARENOPEN ExprLevel0 B_PARENCLOSE    
	 {ExprUn(FncRel,$3)}
	 ;



GeneralUnion : 
     | B_UNION BrackVarSimpleList 
	B_point B_PARENOPEN Pred B_such ExprLevel0 B_PARENCLOSE 
         {ExprSet(SetUnionQuantify($2,$5,$7))}
     | B_UNION Identifier 
	B_point B_PARENOPEN Pred B_such ExprLevel0 B_PARENCLOSE 
         {ExprSet(SetUnionQuantify([Id(Local,$2,None,LocVar)],$5,$7))}
         ;

GeneralInter :
     | B_INTER BrackVarSimpleList 
	B_point B_PARENOPEN Pred B_such ExprLevel0 B_PARENCLOSE
         {ExprSet(SetUnionQuantify($2,$5,$7))}
         ;

GeneralSum : B_SIGMA BrackVarSimpleList 
  B_point B_PARENOPEN Pred B_such ExprLevel0 B_PARENCLOSE
  {ExprSIGMA($2,$5,$7)}
     | B_SIGMA VarSimple 
	 B_point B_PARENOPEN Pred B_such ExprLevel0 B_PARENCLOSE   
         {ExprSIGMA([$2],$5,$7)}
	 ;

GeneralProd : B_PI BrackVarSimpleList 
	 B_point B_PARENOPEN Pred B_such ExprLevel0 B_PARENCLOSE 
	 {ExprPI($2,$5,$7)}
	 ;

SequenceEnum : B_BRACKOPEN ExprList B_BRACKCLOSE        
	 {$2}
	 ;

SetComprEnum : B_CURLYOPEN SetComprBody B_CURLYCLOSE            
	 {$2}
	 ;

Lambda : 
	 B_lambda VarSimple 
         B_point B_PARENOPEN Pred B_such ExprLevel0 B_PARENCLOSE
         {ExprLambda([$2],$5,$7)}
     | B_lambda VarSimpleList 
	 B_point B_PARENOPEN Pred B_such ExprLevel0 B_PARENCLOSE
         {ExprLambda($2,$5,$7)}
     | B_lambda BrackVarSimpleList 
	 B_point B_PARENOPEN Pred B_such ExprLevel0 B_PARENCLOSE
         {ExprLambda($2,$5,$7)}
         ;

Naturals : B_NAT        {"NAT"}
     | B_NATONE         {"NAT1"}
     | B_INT            {"INT"}
     | B_NATURAL        {"NATURAL"}
     | B_NATURALONE     {"NATURAL1"}
     | B_INTEGER        {"INTEGER"}
     | B_BOOL           {"BOOL"}
         ;

SetEmpty : B_emptyset           
         {SetEmpty}
	 ;

SeqEmpty : B_seqempty           
         {SeqEmpty}
	 ;


ExprList : ExprLevel0bis
         {exprSequence_to_exprList $1}
	 ;

SetComprBody : ExprList B_such Pred     
         {SetComprPred(List.map expr2id $1,$3)}
     | ExprList         
         {SetCompr($1)}
         ;

BrackVarSimpleList : B_PARENOPEN VarSimpleList B_PARENCLOSE             
         {$2} 
     | VarSimpleList
         {$1} 
	 ;



Number : B_NUMBER                        {Number(Int32.of_string($1))}
     | B_MAXINT                          {MaxNumber}
     | B_MININT                          {MinNumber}
         ;

String : B_STRING                {ExprString($1)}
	 ;


End : B_END {$1}
     |  {parse_error  "END expected"} 
	 ;
       
Begin : B_BEGIN {$1}
	 ;

Then : B_THEN {$1}
	 ;

ParenClose : B_PARENCLOSE {$1}
	 ;
%%
