(* $Id$ *)

{
 open Bparser
 (* little helpers *)
   
let get t   = 
  let s = Lexing.lexeme t in
(* MG  print_string (s^" ") ; flush stdout ;  *)
s
    
    
let strconst      = ref ""
let storeStr s    = strconst := s
  let getStr ()     = !strconst

  let init = [
    "MACHINE" ,        B_MACHINE("MACHINE") ;
    "CONSTRAINTS",     B_CONSTRAINTS("CONSTRAINTS") ;
    "SEES",            B_SEES("SEES")      ;
    "SETS",    B_SETS("SETS")      ;
    "CONSTANTS"	, B_CONSTANTS("CONSTANTS")      ;
    "PROPERTIES"	, B_PROPERTIES("PROPERTIES")      ;
    "INCLUDES"	, B_INCLUDES("INCLUDES")      ;
    "PROMOTES"	, B_PROMOTES("PROMOTES")      ;
    "EXTENDS"	        , B_EXTENDS("EXTENDS")      ;
    "USES"	        , B_USES("USES")      ;
    "VARIABLES"	, B_VARIABLES("VARIABLES")      ;
    "HIDDEN_VARIABLES"   , B_HIDDEN_VARIABLES("HIDDEN_VARIABLES")      ;
    "VISIBLE_VARIABLES"  , B_VISIBLE_VARIABLES("VISIBLE_VARIABLES")      ;
    "CONCRETE_VARIABLES" , B_CONCRETE_VARIABLES("CONCRETE_VARIABLES")      ;
    "ABSTRACT_VARIABLES" , B_ABSTRACT_VARIABLES("ABSTRACT_VARIABLES")      ;
    "VISIBLE_CONSTANTS"  , B_VISIBLE_CONSTANTS("VISIBLE_CONSTANTS")      ;
    "CONCRETE_CONSTANTS" , B_CONCRETE_CONSTANTS("CONCRETE_CONSTANTS")      ;
    "ABSTRACT_CONSTANTS" , B_ABSTRACT_CONSTANTS("ABSTRACT_CONSTANTS")      ;
    "HIDDEN_CONSTANTS"   ,  B_HIDDEN_CONSTANTS("HIDDEN_CONSTANTS")      ;
    "INVARIANT"	, B_INVARIANT("INVARIANT")      ;
    "VARIANT"	, B_VARIANT("VARIANT")      ;
    "ASSERTIONS"	, B_ASSERTIONS("ASSERTIONS")      ;
    "INITIALISATION"     , B_INITIALISATION("INITIALISATION")      ;
    "OPERATIONS"	, B_OPERATIONS("OPERATIONS")      ;
    "REFINEMENT"	, B_REFINEMENT("REFINEMENT")      ;
    "REFINES"	      , B_REFINES("REFINES")      ;
    "IMPLEMENTATION"     , B_IMPLEMENTATION("IMPLEMENTATION")      ;
    "IMPORTS"	      , B_IMPORTS("IMPORTS")      ;
    "DEFINITIONS"	, B_DEFINITIONS("DEFINITIONS")      ;
    "VALUES"  	, B_VALUES("VALUES")      ;
    "ANY"	        , B_ANY("ANY")      ;
    "or"	        , B_or("or")      ;
    "not"	        , B_not("not")      ;
    "skip"	        , B_SKIP("SKIP")      ;
    "BEGIN"	, B_BEGIN("BEGIN")      ;
    "PRE"	        , B_PRE("PRE")      ;
    "IF"	        , B_IF("IF")      ;
    "THEN"	        , B_THEN("THEN")      ;
    "ELSIF"        , B_ELSIF("ELSIF")    	;
    "ELSE"	        , B_ELSE("ELSE")	  ;
    "ASSERT"       , B_ASSERT("ASSERT")	    ;
    "WHERE"	, B_WHERE("WHERE")	      ;
    "LET"	        , B_LET("LET")		;
    "BE"	        , B_BE("BE")		  ;
    "IN"	        , B_IN("IN")		    ;
    "CHOICE"	, B_CHOICE("CHOICE")     ;
    "OR"	        , B_OR("OR")	;
    "SELECT"	, B_SELECT("SELECT")  ;
    "WHEN"	        , B_WHEN("WHEN")   ;
    "CASE"	        , B_CASE("CASE")      ;
    "OF"	        , B_OF("OF") ;
    "EITHER"	, B_EITHER("EITHER")      ;
    "VAR"	        , B_VAR("VAR")      ;
    "WHILE"	, B_WHILE("WHILE")      ;
    "DO"	        , B_DO("DO")      ;
    "VARIANT"	, B_VARIANT("VARIANT")
(*MG
      ;"BOOL"	        , B_BOOL("BOOL")
*)
      ;"NAT"	        , B_NAT("NAT")
      ;"NAT1"	        , B_NATONE("NAT1")
      ;"NATURAL"      , B_NATURAL("NATURAL")
      ;"NATURAL1"     , B_NATURALONE("NATURAL1")
      ;"INT"          , B_INT("INT")
      ;"INTEGER"      , B_INTEGER("INTEGER")
(* ;"TRUE"	        , B_true("TRUE") *)
(*      ;"FALSE"	, B_false("FALSE") *)
    ;"POW"	        , B_POW("POW")
    ;"FIN"	        , B_FIN("FIN")
    ;"POW1"	        , B_POWONE("POW1")
    ;"FIN1"	        , B_FINONE("FIN1")
    ;"inter"	, B_inter("inter")
    ;"union"	, B_union("union")
    ;"UNION"	, B_UNION("UNION")
    ;"INTER"	, B_INTER("INTER")
    ;    "id"	        , B_id("id")
    ;    "dom"	        , B_dom("dom")
    ;    "cod"	        , B_cod("cod")
    ;    "ran"	        , B_ran("ran")
    ;
    (*MG 
      "iterate"	, B_iterate("iterate")      ;
    *)
    
    "closure"	, B_closure("closure")
    ;    "closure1"     , B_closureone("closure1")
    ;    "card"	        , B_card("card")
    ;    "max"	        , B_max("max")
    ;    "min"	        , B_min("min")
    ;    "pred"	        , B_pred("pred")
    ;    "succ"	        , B_succ("succ")
    ;    "mod"	        , B_mod("mod")
    ;    "SIGMA"        , B_SIGMA("SIGMA")
    ;    "PI"	        , B_PI("PI")
    ;    "seq"	        , B_seq("seq")
    ;    "iseq"	        , B_iseq("iseq")
    ;    "seq1"	        , B_seqone("seq1")
    ;    "iseq1"        , B_iseqone("iseq1")
    ;    "perm"	        , B_perm("perm")
    ;    "conc"	        , B_conc("conc")
    ;    "front"        , B_front("front")
    ;    "tail"	        , B_tail("tail")
    ;    "first"        , B_first("first")
    ;    "last"	        , B_last("last")
    ;    "size"	        , B_size("size")
    ;    "rev"	        , B_rev("rev")
    ;    "fnc"	        , B_fnc("fnc")
    ;    "rel"	        , B_rel("rel")
    ;    "bool"	        , B_bool("bool")
    ;    "END"	        , B_END("END")
    ;    "MAXINT"       , B_MAXINT("MAXINT" )
    ;    "MININT"       , B_MININT("MININT")
    ;    "struct"       , B_struct("struct")
    ;    "structure"    , B_struct("structure")
    ;    "rec"          , B_rec("rec")
    ;  "tree"             , B_tree("tree")
(*    "'"       , B_quote("'") *)
]


let keyword_table = 
  let tbl = Hashtbl.create (List.length init) in
  (
   List.iter (fun (key, data) -> Hashtbl.add tbl key data) init;
   tbl
  )

} 

rule token = parse
  | [' ' '\t']	{   token lexbuf } 
  | ['\n']      {   token lexbuf } 
  |"/*"         {   comment lexbuf }
  |"\""         {   storeStr "" ; string lexbuf }
  |"@"          {   B_AT (get lexbuf)}
  |"=="	        {   B_double_egal(get lexbuf)}
  |"==>"        {   B_garde(get lexbuf)}
  |"-"	        {   B_minus(get lexbuf)}
  |"\\"	        {   B_setminus(get lexbuf)}
  |"+"	        {   B_plus(get lexbuf)}
  |"**"	        {   B_power(get lexbuf)}
  |"*"	        {   B_mul(get lexbuf)}
  |"/"	        {   B_div(get lexbuf)}
  |"="	        {   B_egal(get lexbuf)}
  |"&"	        {   B_conjonction(get lexbuf)}
  |"<"	        {   B_less(get lexbuf)}
  |">"	        {   B_greater(get lexbuf)}
  |","	        {   B_comma(get lexbuf)}
  |";"	        {   B_semicolon(get lexbuf)}
  |";;"	        {   B_relcomp(get lexbuf)} 
  |":"	        {   B_in(get lexbuf)}
  |"$"	        {   B_dollars(get lexbuf)}
  |"::"	        {   B_setin(get lexbuf)}
  |"<=>"        {   B_equivalence(get lexbuf)}
  |"<:"	        {   B_subset(get lexbuf)}
  |"/<:"	{   B_notsubset(get lexbuf)}
  |"=>"	        {   B_implies(get lexbuf)}
  |"<<:"	{   B_strictsubset(get lexbuf)}
  |"/<<:"	{   B_notstrictsubset(get lexbuf)}
  |"/:"	        {   B_notin(get lexbuf)}
  |"/="	        {   B_notequal(get lexbuf)}
  |"<="	        {   B_lessorequal(get lexbuf)}
  |">="	        {   B_greaterorequal(get lexbuf)}
  |"("	        {   B_PARENOPEN(get lexbuf) }
  |")"	        {   B_PARENCLOSE(get lexbuf) }
  |"+->"	{   B_partialfunc(get lexbuf)}
  |"<->"	{   B_relation(get lexbuf)}
  |"-->"	{   B_totalfunc(get lexbuf)}
  |">+>"	{   B_partialinject(get lexbuf)}
  |">->"	{   B_totalinject(get lexbuf)}
  |"+->>"	{   B_partialsurject(get lexbuf)}
  |"-->>"	{   B_totalsurject(get lexbuf)}
  |">->>"	{   B_bijection(get lexbuf)}
  |">+>>"	{   B_partialbij(get lexbuf)}
  |"<--"	{   B_operationsig(get lexbuf)}
  |":="	        {   B_simplesubst(get lexbuf)}
  |"::="	{   B_opdef(get lexbuf)}
  |"["	        {   B_BRACKOPEN(get lexbuf)}
  |"]"	        {   B_BRACKCLOSE(get lexbuf)}
  |"'"	        {   B_quote(get lexbuf)}
  |"||"	        {   B_parallelcomp(get lexbuf)}
  |"|->"	{   B_mapletorpair(get lexbuf)}
  |"<|"	        {   B_domainrestrict(get lexbuf)}
  |"|>"	        {   B_rangerestrict(get lexbuf)}
  |"<<|"	{   B_domainsubstract(get lexbuf)}
  |"|>>"	{   B_rangesubstract(get lexbuf)}
  |"<+"	        {   B_overrideforward(get lexbuf)}
  |"+>"	        {   B_overridebackward(get lexbuf)}
  |"><"	        {   B_relprod(get lexbuf)}
  |"^"	        {   B_concatseq(get lexbuf)}
  |"->"	        {   B_preppendseq(get lexbuf)}
  |"<-"	        {   B_appendseq(get lexbuf)}
  |"/|\\"	{   B_prefixseq(get lexbuf)}
  |"\\|/"	{   B_suffixseq(get lexbuf)}
  |".."	        {   B_natsetrange(get lexbuf)}
  |"\\/"	{   B_unionsets(get lexbuf)}
  |"/\\"	{   B_intersectionsets(get lexbuf)}
  |"%"	        {   B_lambda(get lexbuf)}
  |"{}"	        {   B_emptyset(get lexbuf)}
  |"<>"	        {   B_seqempty(get lexbuf)}
  |"[]"	        {   B_seqempty(get lexbuf)}
  |"."	        {   B_point(get lexbuf)}
  |":|"         {   B_become(get lexbuf)}
  |"|"	        {   B_such(get lexbuf)}
  |"!"	        {   B_forall(get lexbuf)}
  |"#"	        {   B_exists(get lexbuf)}
  |"~"	        {   B_tilde(get lexbuf)}
  |"{"	        {   B_CURLYOPEN(get lexbuf) }
  |"}"	        {   B_CURLYCLOSE(get lexbuf) }
  |"prj1"	{   B_prjone(get lexbuf) }
  |"prj2"	{   B_prjtwo(get lexbuf) }
  | ['a'-'z' 'A'-'Z' '_'] ['a'-'z' 'A'-'Z' '_' '0'-'9' '$'] *
    {   
      let s = get lexbuf 
      in
	try 
	  Hashtbl.find keyword_table s
	with Not_found -> B_IDENTIFIER s
    }
  |['0'-'9']+	{   B_NUMBER(get lexbuf)  }
  | eof           {   EOF  }
      
and comment = parse
    "*/"  { token lexbuf   }
  | eof   { Error.error "unterminated comment"}
  | _     { comment lexbuf }
      

and string = parse
  | [^ '"']+   { storeStr (get lexbuf); string lexbuf       }
  | '"'		{ B_STRING(getStr())             }
  | eof		{ Error.error "unterminated string"    }
  | '\n'        { Error.error "new_line not accepted in string" }
  | _		{ Error.error "illegal char in string" }

    

