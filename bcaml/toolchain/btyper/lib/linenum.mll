{
let filename = ref ""
let linenum = ref 0
let linebeg = ref 0
let linebeg_tmp = ref 0
} 

rule skip_line = parse
  '\n' { incr linenum ; 
	 linebeg_tmp :=  Lexing.lexeme_end lexbuf;
	 !linebeg_tmp
       } 
	 
| [^'\n'] {skip_line lexbuf}


{
let for_position file loc =
  let ic = open_in_bin file in
  let lb = Lexing.from_channel ic in
  filename := file;
  linenum := 1;
  linebeg := 1;
  begin try
    while skip_line lb <= loc do (linebeg := !linebeg_tmp) done
  with End_of_file -> ()
  end;
  close_in ic;
  (!filename, !linenum - 1, (loc - !linebeg))

}
