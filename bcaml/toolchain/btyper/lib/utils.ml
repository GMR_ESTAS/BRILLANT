let upto n =
  let rec upto1 n r = if n = 0 then r else upto1 (n-1) (n-1::r) in
  upto1 n []

let rec last = function
  | [] -> failwith "last"
  | [x] -> x
  | x::l -> last l


