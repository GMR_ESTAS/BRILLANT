
let current_file = ref "";;

let file_set f = 
  current_file := f

let file_get () = !current_file 

type t =
  { file: string; loc_start: int; loc_end: int }

let none = 
  { file = ""; loc_start = -1; loc_end = -1 }

let loc_symbol () = 
  try
    { file = !current_file; 
      loc_start = Parsing.symbol_start(); 
      loc_end = Parsing.symbol_end() 
    }
  with _ -> 
    let _ = Error.debug "[Location] catching expression" in 
    none
     

let loc_merge l1 l2 =
  { file = l1.file; loc_start = l1.loc_start; loc_end = l2.loc_end}

let merge (_,l1) (_,l2) = loc_merge l1 l2

let loc_print l =
  let (_,l1,c1) = Linenum.for_position l.file l.loc_start in
  let (_,l2,c2) = Linenum.for_position l.file l.loc_end in
  (*Format.print_string "file: " ; 
    Format.print_string l.file;
    Format.print_string ", start: "; 
    Format.print_int l1; 
    Format.print_string":"; 
    Format.print_int c1;
    Format.print_string " , end: "; 
    Format.print_int l2; 
    Format.print_string":"; 
    Format.print_int c2;
    Format.print_newline ()
    *)
  Error.debug ("file: "^l.file^", start: "^(string_of_int(l1))^":"^(string_of_int(c1)));
      Error.debug ("end: "^(string_of_int(l2))^":"^(string_of_int(c2))^"\n"); 
    
;;

let loctable = Hashtbl.create 1001;;

let put o = Hashtbl.add loctable (Obj.repr o) (loc_symbol ()); o

let get o = 
  try 
    Hashtbl.find loctable (Obj.repr o) 
  with _ -> none

let putl o1 o = Hashtbl.add loctable (Obj.repr o) (get o1); o

let oloc_print o = 
  let l = get o in
  if l <> none 
  then loc_print l
