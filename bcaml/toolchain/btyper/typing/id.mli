(* $Id *)

(**  Id management module *)


val org2prefix : Blast.origin -> string list

val add_org : string -> string -> Blast.origin -> Blast.origin

val name_of : Blast.id -> string
val prefix_of : Blast.id -> string list
val typeof : Blast.id -> Blast.type_B

val debug_id : Blast.id -> string
val debug_ids : Blast.id list -> string
val debug_bienv : Blast.id list * Blast.id list -> string

val remote_amn_org : Blast.id -> Blast.origin
val refine_amn_org : Blast.id -> Blast.origin


val set2id : Blast.set -> Blast.id


val as_Cst : Blast.id -> Blast.id
val as_StateVar : Blast.id -> Blast.id
val as_LocVar : Blast.id -> Blast.id
val as_Set : Blast.id -> Blast.id
val as_OpName : Blast.id -> Blast.id
val as_MachineName : Blast.id -> Blast.id

