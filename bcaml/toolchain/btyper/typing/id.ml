(* $Id$ *)

open Blast


open Except


let org2prefix = function
    Local -> []
  | Remote l -> List.filter (fun x -> x <> "") (List.map fst l)
  | OrgRefine (r,l) -> List.filter (fun x -> x <> "") (List.map fst l)
  | Prefix l -> l

let add_org p m = function
    Local -> Remote [p,m]
  | Remote l -> Remote ((p,m)::l)
  | OrgRefine _ -> raise (NotYetImplemented "Id.add_org (OrgRefine)")
  | Prefix l -> raise (NotYetImplemented "Id.add_org (Prefix)")

let name_of = function Id(_,s,_,_) -> let _ = Error.debug ("name_of "^s) in s

let prefix_of = function Id(p,_,_,_) -> org2prefix p

let typeof = function Id (_,_,Some t,_) -> t | _ -> Untyped


let debug_origin = function
  | Local -> "local"
  | Remote(ssl) ->
      "remote("
      ^	(String.concat ";" (List.map (fun (s,t) -> s ^ "" ^ t) ssl))
      ^ ")"
  | OrgRefine(x,ssl) ->
      "orgrefine[" ^ x ^ "]("
      ^	(String.concat ";" (List.map (fun (s,t) -> s ^ "" ^ t) ssl))
      ^ ")"
  | Prefix(sl) ->
      "prefix("
      ^	(String.concat ";" sl)
      ^ ")"

let rec debug_type t =
  match t with
  | TypeNatSetRange(a,b) -> "interval"
  | PredType(_) -> "predtype"
  | TypeIdentifier(id) -> name_of id
  | PFunType(a,b) ->
      (debug_type a) ^ "+->" ^ (debug_type b)
  | FunType(a,b) ->
      (debug_type a) ^ "-->" ^ (debug_type b)
  | ProdType(a,b) ->
      (debug_type a) ^ "><" ^ (debug_type b)
  | Sequence(a) ->
      "seq(" ^ (debug_type a) ^ ")"
  | SubType(a,b) -> "subtype"
  | Fin_B(a) -> "Pow(" ^ (debug_type a) ^ ")"
  | Untyped -> "?"

let debug_typeopt topt =
  match topt with
    | None -> ""
    | Some t -> debug_type t

let debug_kind kind =
  Bbop.kind_name kind

let debug_id = function
  | Id(pref,str,topt,kind) ->
      "(" ^ (debug_origin pref)
      ^ "|" ^ str
      ^ "|" ^ (debug_typeopt topt)
      ^ "|" ^ (debug_kind kind)
      ^ ")"

let debug_ids ids =
  String.concat "," (List.map debug_id ids)

let debug_bienv (ep, en) =
  "++"
  ^ (debug_ids ep)
  ^ "--"
  ^ (debug_ids en)

let remote_amn_org mid =
  let p = prefix_of mid and m = name_of mid in
  if p = [] then Remote ["",m] else Remote [List.hd p,m]

let refine_amn_org mid = OrgRefine (name_of mid,[])

let set2id = function
    SetAbstDec i -> i
  | SetEnumDec (i,_) -> i




let as_Cst (Id(p,n,t,_)) = Id(p,n,t,Bbop.Cst)

let as_StateVar (Id(p,n,t,_)) = Id(p,n,t,Bbop.StateVar)

let as_LocVar (Id(p,n,t,_)) = Id(p,n,t,Bbop.LocVar)

let as_Set (Id(p,n,t,_)) = 
  let _ = Error.debug ("as_Set "^n) in 
    Id(p,n,t,Bbop.Set)

let as_OpName (Id(p,n,t,_)) = Id(p,n,t,Bbop.OpName)

let as_MachineName (Id(p,n,t,_)) = Id(p,n,t,Bbop.MachineName)
