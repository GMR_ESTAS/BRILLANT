(* $Id *)

(** Catching and displaying type check errors *)

open Blast
open Utils

let print_ctx () = 
  let fd =stderr in
    Format.set_formatter_out_channel (fd);
    match !Typing.exc_expr with
      | Some e -> 
	  Format.print_string "**** expr_ctx: "; 
	  Location.oloc_print e;
	  Utils.print_expr e;
	  Format.print_newline ()
      | _ -> ();
	  match !Typing.exc_subst with
	      Some e -> 
		Format.print_string "**** subst_ctx: "; 
		Location.oloc_print e;
		Utils.print_substitution e;
		Format.print_newline ()
	    | _ -> ()
		
let typed_print_err ast  = 
  let fd =stderr in
    Format.set_formatter_out_channel (fd);flush fd;
    try
      Options.expansion := true;
      Typing.of_ast ast
    with
	Except.TypeError(ty1,ty2) ->
	  Format.print_newline();
	  Format.print_string "**** type error:\n";
	  Location.oloc_print ty1;
	  Utils.print_type_B ty1;
	  Format.print_newline ();
	  Location.oloc_print ty2;
	  Utils.print_type_B ty2;
	  print_ctx ();
	  Format.print_newline();
	  EmptyTree 
      | Except.NotProduct(ty) ->
	  Format.print_newline();
	  Format.print_string "**** type error (not a product):\n";
	  Utils.print_type_B ty;	
	  Location.oloc_print ty;
	  print_ctx ();
	  Format.print_newline ();
	  EmptyTree
      | Except.NotRelation(ty) ->
	  Format.print_newline();
	  Format.print_string "**** type error (not a relation):\n";
	  Utils.print_type_B ty;
	  Location.oloc_print ty;
	  print_ctx ();
	  Format.print_newline ();
	  EmptyTree
      | Except.NotPowerSet(ty) ->
	  Format.print_newline();
	  Format.print_string "**** type error (not a powerset):\n";
	  print_ctx ();
	  Location.oloc_print ty;
	  Utils.print_type_B ty;
	  Format.print_newline ();
	  EmptyTree
      | Except.BadApply(ty1,ty2) ->
	  Format.print_newline();
	  Format.print_string "**** BadApply :\n";
	  Location.oloc_print ty1;
	  Utils.print_type_B ty1;
	  Format.print_newline ();
	  Location.oloc_print ty2;
	  Utils.print_type_B ty2;
	  print_ctx ();
	  Format.print_newline();
	  EmptyTree
      | Except.BadAssign (ty) ->
	  Format.print_newline();
	  Format.print_string "**** BadAssign :\n";
	  Utils.print_type_B ty;
	  Location.oloc_print ty;
	  print_ctx ();
	  Format.print_newline ();
	  EmptyTree
      | Except.InvalidApplication(s,tys) ->
	  Format.print_newline();
	  Format.print_string "**** Invalid Application :\n";
	  print_ctx ();
	  Format.print_string s;
	  Format.print_newline ();
	  List.iter (fun ty -> Location.oloc_print ty; Utils.print_type_B ty)  tys ;
	  EmptyTree
      | Except.InternalError(e) ->
	  Format.print_newline();
	  Format.print_string "**** Internal Error :\n";
	  Utils.print_expr e;	
	  Location.oloc_print e;
	  print_ctx ();	
	  Format.print_newline ();
	  EmptyTree
      | Except.NotYetImplemented(s) ->
	  Format.print_newline();
	  Format.print_string ("**** Not yet implemented : " ^s ^ "\n");
	  print_ctx ();
	  Format.print_newline ();
	  EmptyTree
      | Except.UntypedId(id) ->
	  Format.print_newline();
	  Format.print_string "**** UntypedId :\n";
	  Utils.print_expr (ExprId id);
	  Format.print_newline ();	
	  Location.oloc_print id;
	  print_ctx ();	
	  Format.print_newline ();
	  EmptyTree
      | Except.Undeclared(s,id) ->
	  Format.print_newline();
	  Format.print_string s ;
	  Format.print_newline();
	  Format.print_string "**** Undeclared :\n";
	  Utils.print_expr (ExprId id);
	  Format.print_newline ();
	  Location.oloc_print id;
	  print_ctx ();
	  Format.print_newline ();
	  EmptyTree
      | Except.InvalidRedefinition(id) ->
	  Format.print_newline();
	  Format.print_string "****  Invalid redefinition :\n";
	  Utils.print_expr (ExprId id);	
	  print_ctx ();
	  Format.print_newline ();
	  EmptyTree
      | Except.InvalidResult(s) ->
	  Format.print_newline();
	  Format.print_string "**** Invalid Result :\n";
	  Utils.print_substitution s;	
	  print_ctx ();
	  Location.oloc_print s;
	  Format.print_newline ();
	  ignore (EmptyTree)
	  ; 
	  (*MG insertion d'un code de sortie non nul *)
	  exit 1
;;

let typed_print f =
  begin
    Options.expansion := true;
    let (_,ast) = Lecteur.analyse f in
    let tyast = typed_print_err ast in
      let fd = stdout  in
	Format.set_formatter_out_channel (fd);
	(*VS
	Format.print_string ("<!DOCTYPE AMN SYSTEM \"./AMN.dtd\">");
	Format.print_newline();
	*)
	Utils.print_amn (tyast) ;
	(* YP (ast); *)
	Format.print_flush (); 
	close_out fd
  end
;;
