(* $Id$ *)

(** Typing environment management functions *)

val env :
  (string list * string, Blast.origin * int * Blast.type_B * Bbop.kind)
  Hashtbl.t

val dump : 'a -> unit

val add_const : Blast.id * int -> unit

val add_const1 : Blast.id -> unit

val add_tyvar : Blast.id * int -> Blast.type_B -> Blast.origin -> unit

val set_origin : Blast.origin -> Blast.id -> Blast.id

val define_id : string -> Blast.type_B -> Bbop.kind -> unit

val change_tyvar : Blast.id * int -> Blast.type_B -> Blast.id

(** Define a builtin type *)
val mk_builtintype : string -> Blast.type_B

val list_mk_prod : Blast.type_B list -> Blast.type_B

val dest_prod_ty : Blast.type_B -> Blast.type_B list

(** Type constructors *)

val mk_ftype : Blast.type_B list -> Blast.type_B -> Blast.type_B
val mk_bool : Blast.type_B
val mk_integer : Blast.type_B
val mk_natural : Blast.type_B
val mk_string : Blast.type_B
val mk_seq : Blast.type_B -> Blast.type_B
val mk_pred : Blast.type_B
val mk_subst : Blast.type_B
val mk_mch : Blast.type_B 


val init : unit -> unit
val cmpty1 : Blast.type_B -> Blast.type_B -> unit
val is_typed : Blast.type_B -> bool
val type_cmp : Blast.type_B -> Blast.type_B -> unit
val mk_empty : 'a list * 'b list
val valueof : 'a * 'b -> 'a
val typeof : 'a * 'b -> 'b
val typeof_id : Blast.id -> Blast.type_B
val origin_of_id : Blast.id -> Blast.origin
val env_id : Blast.id -> Blast.id
val is_SET : Blast.id -> bool
