(* $Id$ *)

open Blast

let print_amn ast = 
  Blast_txt.page_txt Format.std_formatter (Blast_page.TXT.it_amn ast)

let print_substitution ast = 
  Blast_txt.page_txt Format.std_formatter (Blast_page.TXT.it_substitution ast)

let print_expr ast =
  Blast_txt.page_txt Format.std_formatter (Blast_page.TXT.it_expr ast)

let print_type_B ast =
  Blast_txt.page_txt Format.std_formatter (Blast_page.TXT.it_type_B ast)


let expr_cstr = function
     ExprSequence(_,_) -> "ExprSequence"
   | ExprParen(_) -> "ExprParen"
   | ExprId(_) -> "ExprId"
   | ExprFunCall(_,_ ) -> "ExprFunCall"
   | ExprBin(_,_,_) -> "ExprBin"
   | ExprUn(_,_) -> "ExprUn"
   | ExprSeq(_) -> "ExprSeq"
   | ExprSet(_) -> "ExprSet"
   | RelSet(_,_) -> "RelSet"
   | RelSeqComp(_) -> "RelSeqComp"
   | ExprNumber(_) -> "ExprNumber"
   | ExprBool(_) -> "ExprBool"
   | ExprString(_) -> "ExprString"

   | ExprPred(_) -> "ExprPred"
   | ExprNuplet(_) -> "ExprNuplet"
   | ExprSIGMA(_ ,_,_) -> "ExprSIGMA"
   | ExprPI(_ ,_,_) -> "ExprPI"
   | ExprLambda(_ ,_,_) -> "ExprLambda"


let first(x,_,_) = x
let second (_,y,_) = y
let third (_,_,z) = z


