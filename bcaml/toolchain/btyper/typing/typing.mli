(* $Id$ *)


(** Typing module *)


val exc_expr : Blast.expr option ref

val exc_subst : Blast.substitution option ref

(** Typing in environment *)

val in_env :
  unit ->
  (Blast.amn -> Blast.amn) *
  (Blast.expr -> Blast.id list * Blast.id list -> Blast.expr * Blast.type_B) *
  (Blast.type_B -> Blast.type_B)

val amn_in_env : Blast.amn -> Blast.amn

val expr_in_env :
  Blast.expr -> Blast.id list * Blast.id list -> Blast.expr * Blast.type_B


val of_ast : Blast.amn -> Blast.amn

