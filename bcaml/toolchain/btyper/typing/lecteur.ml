(* $Id$ *)

(** Lecture et analyse syntaxique des composants *)

let mylexer = ref  Blexer.token

let parsing_success file =
  Error.message ("\n --Bhappy "^"<"^file^">"^ " parsed")
 
let parsing_failure file line col =
  Error.message ("\n --Bwarn   "
		 ^"<"^ file ^">"
		 ^"Parse error at line "
		 ^(string_of_int line)
		 ^" at column "
		 ^(string_of_int col));
  (Blast.EmptyTree)

let parse_in_channel str cin =
  let _ = Error.debug ("[Lecteur] Parsing "^str) in
    try
      let lexbuf = Lexing.from_channel cin      in
      let res = Bparser.amn !mylexer lexbuf     in
	(
	  close_in cin 
	; (parsing_success str) 
	;  res 
	)
    with Error.Parse_error(nb,s) ->       (* Echec parsing *)
      let (string_error,line,col) = Linenum.for_position str nb  in
	(
	  close_in cin
	  ; (parsing_failure s line col) 
	)
      |	_ -> close_in cin
          ; raise Parsing.Parse_error
            
let parse_string str =
  let _ = Error.debug ("[Lecteur] parse_string "^str^"\n") in
    try  
      let lexbuf = Lexing.from_string str in
	Bparser.amn !mylexer lexbuf
    with _ -> raise (Invalid_argument "parse_string")


      
let analyse str = 
  let _ = Error.debug ("[Lecteur] "^str) in
  let old_file = Location.file_get () in
  let p_str = str in
    try  
      Location.file_set  p_str
      ;

      let cin = 	open_in p_str      in

      let res = parse_in_channel p_str cin in 
	Location.file_set old_file;
	(str,res)
    with 
	Sys_error s -> 
	  Error.message ("\n --Sys error: " ^ s ^ "\n");
	  Location.file_set old_file;
	  (str,(Blast.EmptyTree))
      | Parsing.Parse_error -> 
	  Error.message ("\n --Bwarn "^"<"^str^">"^" not parsed");
	  Location.file_set old_file;
	  (str,(Blast.EmptyTree))
      | _ ->
	  Error.message ("\n --Bwarn "^"<"^str^">"^" Erreur inconnue");
	  (str,(Blast.EmptyTree))
	  

