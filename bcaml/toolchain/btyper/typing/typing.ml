(* $Id$ *)
(* TYPAGE DE INIT *)

open Blast
open Blast_lib
open Except
(* open Module_id *)


(* open Id *)

(* *) open Tyenv  (* *)


open Utils


let exc_expr = ref None
let exc_subst = ref None



(*i
let get_amn id =
  let name = id2name id in
  Hashtbl.find amn_cache name 
i*)

(* *)
let amn_cache = Hashtbl.create 53
(* *)
let rec in_env ()=
  let typeof_expr e = Tyenv.typeof (second (in_env ()) e mk_empty) in
  let module Blast_typing =
    struct
      type env_ty = (id list) * (id list)  (* positifs - negatifs *)

      type amn_ty = amn
      type head_ty = head
      type clause_ty = clause
      type def_ty = def
      type defBody_ty = env_ty -> (defBody * type_B)
      type set_ty = set
      type instance_ty = instance
      type type_B_ty = type_B
      type field_ty = field
      type predicate_ty = env_ty -> predicate
      type operation_ty = operation
      type expr_ty = env_ty -> (expr * type_B)
      type exprSeq_ty = env_ty -> (exprSeq * type_B)
      type exprSet_ty = env_ty -> (exprSet * type_B)
      type number_ty = number
      type substitution_ty = env_ty -> substitution
      type origin_ty = origin
(*      type ids_ty = ids *)
      type id_ty = id

      let mk_exprList (l:expr_ty list) ev =
	List.map (fun e -> e ev) l


	  
      let lookup id e =
	  List.mem id e


      (* \section{Type checking} *)
			  
      let arity1 s l =
	if List.length l <> 1 then raise (BadArity s)
	  
      let arity2 s l =
	if List.length l <> 2 then raise (BadArity s)
	  
      (* \subsection{Super type} *)
      let rec super ty =
	match ty with
	  | TypeNatSetRange _ -> mk_integer
	  | TypeIdentifier i -> super_id i
	  | PFunType(ty1,ty2) -> 
	      Fin_B (Blast_lib.mk_ProdType (super ty1) (super ty2))
	  | FunType(ty1,ty2) -> 
	      Fin_B (Blast_lib.mk_ProdType (super ty1) (super ty2))
	  | Sequence ty -> 
	      Fin_B (Blast_lib.mk_ProdType mk_integer (super ty))
	  | ProdType(ty1,ty2) -> 
	      Blast_lib.mk_ProdType (super ty1) (super ty2)
	  | SubType(ty1,_) -> super ty1
	  | Fin_B t -> Fin_B (super t)
	  | Untyped -> ty
	  | PredType e -> ty_elems (super (typeof_expr e))
      and super_id i =
	match i with
	  | Id (Local,"NAT",None,_) -> mk_integer 
  	  | Id (Local,"NAT1",None,_) -> mk_integer
  	  | Id (Local,"NATURAL",None,_) -> mk_integer
  	  | Id (Local,"NATURAL1",None,_) -> mk_integer
  	  | Id (Local,"INTEGER",None,_) -> mk_integer
	      (*MG ?*)
  	  | Id (Local,"INT",None,_) -> mk_integer
	  | Id (p,s,_,k) -> 
	    (* let _ = Error.debug ("typing Id "^s) 
			    in *)
	      TypeIdentifier (Id(p,s,None,k))
      and 
	ty_apply ty ta =
	let sta = super ta and sty = super ty in
	  match sty with
	      Fin_B (ProdType (t1,t2)) -> check_type t1 sta; t2
	    | PFunType (t1,t2) -> check_type t1 sta; t2
	    | FunType (t1,t2) -> check_type t1 sta; t2
	    | Untyped -> raise (UnknownTyping)
	    |	_ -> raise (BadApply (sty,sta))
      and 
	tyFuncall self nf nargs =
	if nargs = [] then (typeof nf) 
	else begin
	  exc_expr := Some self;
	  ty_apply (typeof nf) (list_mk_prod (List.map typeof nargs))
	end
      and check_type t1 t2 = 
      type_cmp (super t1) (super t2)
      and 
	  echeck_type e t1 t2 = 
	try 
	  check_type t1 t2 
	with exc -> exc_expr := Some e; raise exc
      and asType (e,t) = try ty_elems t with _ -> ty_elems (super t)
      and check_is_set ty = 
	match (super ty) with
	| Fin_B _ -> true
	| Untyped -> raise (NotYetImplemented "check_is_set:Untyped")
	| _ -> false
      and ty_elems = function
	  Fin_B t -> t
	| ty -> raise (NotPowerSet ty)
      and dom_of t = 
	match (super t) with
	  Fin_B (ProdType(t1,t2)) -> t1
	| _ -> raise (NotRelation t)
      and ran_of t = match (super t) with
	  Fin_B (ProdType(t1,t2)) -> t2
	| _ -> raise (NotRelation t)

     let get_prj1 t = 
       match t with
         ProdType(t1,t2) -> t1
       | _ ->
           begin
             match (super t) with
               ProdType(t1,t2) -> t1
             | _ -> raise (NotProduct t)
           end

     let get_prj2 t =  
       match t with
         ProdType(t1,t2) -> t2
       | _ ->
           begin 
             match (super t) with
               ProdType(t1,t2) -> t2
             | _ -> raise (NotProduct t)
           end

	      
      let rec split_prod exp n =
	if n = 1 
	then [exp]
	else (get_prj1 exp)::(split_prod (get_prj2 exp) (n - 1))

      let get_set exp =
	let _ = ty_elems (super (typeof exp)) in
	valueof exp

      let untype ty exp =
	echeck_type (valueof exp) ty (typeof exp);
	valueof exp
	  
      let list_unique_ty = function
	  [] -> failwith "list_unique_ty"
	| t1::tl -> List.iter (check_type t1) tl; t1

      let mk_comp_ty t1 t2 = 
	check_type (ran_of t1) (dom_of t2);
	Blast_lib.mk_ProdType (dom_of t1) (ran_of t2)
	  
      let extract_id_type (Id(o,n,ty,k) as id) = 
	let p = Id.org2prefix o in
	let t = typeof_id id and org = origin_of_id id in
	let _ = Hashtbl.remove env (p,n) in
	  if is_typed (super t) then
	    match ty with
	      | Some tt -> check_type t tt; Id(org,n,ty,k)
	      | None -> Id(org,n,Some t,k)
	  else raise (UntypedId id)
	    
      let extract_set_type = function 
	  SetAbstDec id -> SetAbstDec (extract_id_type id)
  	| SetEnumDec (id,ids) -> 
	    SetEnumDec (extract_id_type id, List.map extract_id_type ids)
	      
      let update_id_type (Id(p,n,ty,k) as id) = 
	let t = typeof_id id in
	if is_typed (super t) then
	  match ty with
	  | Some tt -> check_type t tt; Id(origin_of_id id,n,ty,k)
	  | None -> Id(origin_of_id id,n,Some t,k)
	else raise (UntypedId id)
	 
	  
      let params = ref []
      let variables = ref []
      let constantes = ref []


      let load_amn id =
	let name = Id.name_of id in
	let _ = Error.debug ("[Typing] "^ "Loading " ^ name ^ " ...") in
	try 
	  let typed_amn = Hashtbl.find amn_cache name in
	  let _ = Error.debug ("[Typing] "^ name ^ " already in cache") in
	  typed_amn
	with
	  _ -> 
            let (_,amn) = 
	      try 
		Lecteur.analyse (name ^ ".mch") 
	      with 
		_ -> 
		  Lecteur.analyse (name ^ ".ref") 
	    in
            let _ = Error.debug ("[Typing] " ^ name ^ " loaded") in 
            let typed_amn = first (in_env ()) amn in
	    let _ = Error.debug ("[Typing] " ^ name ^ " typed") in

	    Hashtbl.add amn_cache name typed_amn;
	    typed_amn

      let call_amn amn args =
	let ((name,params),_) = Blast_lib.destr_Machine amn in
	  try
	    let substs = List.map2 (fun i e -> ExprId i,e) params args in
	    let subst exp = try List.assoc exp substs with _ -> exp in
	      Blast_maps.map_expr_amn subst amn
	  with 
	      Invalid_argument _ 
	      -> raise (InvalidApplication (Id.name_of name,[]))
	    
      let prefix_amn mid amn =
	let p = Id.prefix_of mid 
	and m = Id.name_of mid in
	  (* le prefixe d'importation contient au plus un ident *)
	let mp = 
	  if p = [] 
	  then "" 
	  else List.hd p in 
	let prefix_id (Id(pr,s,t,k) as i) = 
	  if 
	    k = Bbop.StateVar || k = Bbop.OpName 
	  then 
	    Id(Id.add_org mp m pr,s,t,k)
	  else i 
	in
	  Blast_maps.map_id_amn prefix_id amn
	    
      let env_of_cst_id i = add_tyvar (Id.as_Cst i,0) (Id.typeof i) Local 
	(*BUG loc*)
	
      let env_of_var_id org i = add_tyvar (Id.as_StateVar i,0) (Id.typeof i) org 
	(* BUG loc *)
	
      let env_of_op org (op,res,args,_) =
	let res_ty = 
	  if res = [] 
	  then mk_subst
	  else mk_ProdType mk_subst (list_mk_prod(List.map Id.typeof res)) in
	let op_ty = mk_ftype (List.map Id.typeof args) res_ty in
	add_tyvar (Id.as_OpName op,0) op_ty org (* BUG loc *)

      let env_of_set = function 
	  SetAbstDec id -> 
	    add_tyvar (id,0) (Fin_B (TypeIdentifier id)) Local
  	| SetEnumDec (id,ids) -> 
	(* ?    let nt = add_tyvar (id,0) (Fin_B (TypeIdentifier id)) Local 
	    in *)
	      List.iter 
		(fun e -> 
		   match e with 
		     | Id(Local,s,_,_) 
		       -> add_tyvar (e,0) (TypeIdentifier id) Local
		     | _ -> raise (NotYetImplemented "Typing.env_of_set \n") ) ids
      let rec env_of_cl org = function
	| ConstantsConcrete ids -> List.iter env_of_cst_id ids
	| ConstantsAbstract ids -> List.iter env_of_cst_id ids
	| VariablesConcrete ids -> List.iter (env_of_var_id org) ids
	| VariablesAbstract ids -> List.iter (env_of_var_id org) ids
	| Includes instance_ty_list ->
	    let includes (i,args) = 
	      env_of_amn (Id.remote_amn_org i)
		(call_amn (prefix_amn i (load_amn i)) args)
	    in
	    List.iter includes instance_ty_list;
	| Imports instance_ty_list ->
	    let import (i,args) = 
	      env_of_amn (Id.remote_amn_org i)
		(call_amn (prefix_amn i (load_amn i)) args)
	    in
	    List.iter import instance_ty_list;
	| Sets ls -> List.iter env_of_set ls
	| Operations ops -> List.iter (env_of_op org) ops
	| _ -> ()
      and env_of_amn org = function
	  Machine(_,cl) -> List.iter (env_of_cl org) cl
	| Refinement(_,cl) -> List.iter (env_of_cl org) cl
	| _ -> ()

      let type_vars cl =
	List.map
	    (function
		ConstantsConcrete ids ->
		  ConstantsConcrete (List.map extract_id_type ids)
	      |	ConstantsAbstract ids ->
		  ConstantsAbstract (List.map extract_id_type ids)
	      |	ConstantsHidden ids ->
		  ConstantsHidden (List.map extract_id_type ids)
	      |	VariablesConcrete ids ->
		  VariablesConcrete (List.map extract_id_type ids)
	      |	VariablesAbstract ids ->
		  VariablesAbstract (List.map extract_id_type ids)
	      |	VariablesHidden ids ->
		  VariablesHidden (List.map extract_id_type ids)
	      |	Sets ls ->
		  Sets (List.map extract_set_type ls)
	      |	c -> c)
	  cl

(*MG Le d�but de la manip *)


      let mk_Machine self head_ty clause_ty_list =
	let _ = Error.debug  "Machine MRB 7.1 p110" 	in
	let (id,ids) = head_ty in
	let nh = (id, List.map extract_id_type ids) in
	Machine (nh, type_vars clause_ty_list)
	  

      let mk_Refinement self head_ty clause_ty_list =
	let _ = Error.debug   "Refinement MRB 7.3 p112"	in
	let (id,ids) = head_ty in
	let nh = (id, List.map extract_id_type ids) in
	Refinement (nh, type_vars clause_ty_list)
	  

      let mk_Implementation self head_ty clause_ty_list =
	let _ = Error.debug "Implementation MRB 7.4 p114" in
	let (id,ids) = head_ty in
	let nh = (id, List.map extract_id_type ids) in
	Implementation (nh, type_vars clause_ty_list)
	  
      
      let mk_Refines self id_ty =
	let _ = Error.debug "Refines MRB 7.6 p117" in
        env_of_amn (Id.refine_amn_org id_ty) (load_amn id_ty);
	Refines id_ty

      let mk_EmptyTree self =
	EmptyTree


      let mk_head self id_ty ids =
	Error.debug "Head MRB 7.1 - 7.2 p109 - 111" ;
	let ids_sets,ids_others = List.partition is_SET ids in
	List.iter add_const1 ids_others;
	List.iter (fun i -> 
	  let ki = Id.as_Set i in
	  add_tyvar (ki,0) (Fin_B (TypeIdentifier ki)) Local) ids_sets;
	params := !params@ids_others;
	(Id.as_MachineName id_ty, ids)

      (*YP MRB 2.3 p 8*)
      let mk_Definitions self def_ty_list =
	Definitions (def_ty_list)
	  
      
      let mk_Constraints self pred_ty =
	Error.debug "Constraints MRB 7.14 p 138";
	Constraints (pred_ty (!params,[]))

      let mk_Invariant self pred_ty =
	Error.debug "YP Invariant MRB 7.20 p 156";
	Invariant (pred_ty (!variables,[]))
	
      let mk_Sets self set_ty_list =
	Error.debug "Sets MRB 7.13 p 135";
	Sets set_ty_list
	  
      
      let mk_Initialisation self substitution_ty =
	Error.debug "(* YP Initialisation MRB 7.22 p 161*)" ;
	Initialisation (substitution_ty mk_empty)

(* YP 
   fonction am�liorant le code 
   et remplacant constantes := !constantes@nids;
   et permettant aussi un d�buggage 
*)
 
      let constantes_Add (nids) =
	let _ = Error.debug ("ajout de constantes")
	in
	  constantes := !constantes@nids 

      (* YP MRB 7.14 p 138*) 
      let mk_ConstantsConcrete self ids =
	let nids = List.map Id.as_Cst ids in
	  List.iter add_const1 nids;
	  (* YP constantes := !constantes@nids;*)
	  constantes_Add (nids);
	  ConstantsConcrete (nids)

      (* YP MRB 7.15 p 140*)
      let mk_ConstantsAbstract self ids =
	let nids = List.map Id.as_Cst ids in
	  List.iter add_const1 nids;
	  (* YP constantes := !constantes@nids;*)
	  constantes_Add (nids);
	  ConstantsAbstract (nids)

      (*YP MRB ??? *)  
      let mk_ConstantsHidden self ids =
	let nids = List.map Id.as_Cst ids in
	  List.iter add_const1 nids;
	  (* YP constantes := !constantes@nids;*)
	  constantes_Add (nids); 
	  ConstantsHidden (nids)
	  

      let mk_Properties self pred_ty =
	Error.debug "(*YP Properties MRB 7.16 p 142 *)" ;
	Properties (pred_ty (!constantes,[]))
	  
      
      let mk_Values self id_expr_ty_list =
	Error.debug "(* YP MRB 7.17 p 145*)" ;
	Values (List.map (fun (id,exp) -> (id, get_set (exp mk_empty)))
		  id_expr_ty_list)

      (*YP fonction remplacant variables := !variables@nids; *)
      let variables_add nids =
	let _ =  Error.debug ("ajout de variables") in 
	  variables := !variables@nids

      (* YP MRB 7.18 p 152*)	  
      let mk_VariablesConcrete self ids =
	let nids = List.map Id.as_StateVar ids in
	  List.iter add_const1 nids;
	  variables_add nids; 
	  VariablesConcrete (nids)

      (*YP MRB 7.19 p 154*)
      let mk_VariablesAbstract self ids =
	let nids = List.map Id.as_StateVar ids in
	  List.iter add_const1 nids;
	  variables_add nids;
	  VariablesAbstract (nids)
	  
      (*YP MRB ???*)
      let mk_VariablesHidden self ids =
	let nids = List.map Id.as_StateVar ids in
	List.iter add_const1 nids;
	(* YP variables := !variables@nids;*)
	variables_add nids;
	VariablesHidden (nids)
	  
      (*YP MRB 7.10 p 130 *) 
      let mk_Promotes self ids =
	Promotes (ids)
	  
      (*YP MRB 7.21 p 160 *)
      let mk_Assertions self pred_ty_list =
	Assertions 
	  (List.map (fun e -> e mk_empty) pred_ty_list)
	  
      (*YP MRB 7.23 p 164*) 
      let mk_Operations self operation_ty_list =
	Operations 
	  (operation_ty_list)
      
      (* YP n'est plus utile 
	 let rec is_suffix l1 l2 =
	 if List.length l1 <= List.length l2 then l1 = l2 
	 else is_suffix (List.tl l1) l2*)
	  
      (* SEES a revoir *)

      (*YP MRB 7.8 p 122*) 
      let mk_Sees self ids =
	
	List.iter (fun (Id(p,n,_,_) as i) ->
		     (*i
		       let del_ids = ref [] in
		       Hashtbl.iter (fun cle ->
		       function  (OrgSee (pp,nn),_,ty,_) 
		       when n = nn && is_suffix p pp ->
		       del_ids := cle::!del_ids
		       | _ -> ()) env;
		       List.iter (Hashtbl.remove env) !del_ids;
		       i*)
		     env_of_amn (Id.remote_amn_org i) (load_amn i)) ids;
	Sees (ids)
	

      (*YP MRB 7.12 p 133*)  
      let mk_Uses self ids =
	Uses (ids)
	  
      (*YP MRB 7.11 p 132*)
      let mk_Extends self instance_ty_list =
	Extends (instance_ty_list)
	
      (*YP MRB 7.9 p 126 *)
      let mk_Includes self instance_ty_list =
	let incl (i,args) = 
	  env_of_amn (Id.remote_amn_org i) (prefix_amn i (load_amn i))
	in
	List.iter incl instance_ty_list;
	Includes (instance_ty_list)

      (*YP MRB 7.7 p 118*)  
      let mk_Imports self instance_ty_list =
	let import (i,args) = 
	  env_of_amn (Id.remote_amn_org i)
	    (call_amn (prefix_amn i (load_amn i)) args)
	in
	List.iter import instance_ty_list;
	Imports (instance_ty_list)
	  
      (*YP MRB 2.3 p 8-11 (?)*)
      let mk_Def self id_ty ids defBody_ty =
	Def (id_ty, ids, valueof (defBody_ty mk_empty))

      (*YP MRB 2.3 p 8-11 (?)*)
      let mk_FileDef self string =
	FileDef (string)

      (*YP MRB 2.3 p 8-11 (?)*)
      let mk_ExprDefBody self expr_ty =
	fun e -> self, Untyped
	
      (*YP MRB 2.3 p 8-11 (?)*)	  
      let mk_SubstDefBody self substitution_ty =
	fun e -> self, Untyped
	 

      (* MRB p17 (?) *)
      let mk_SetAbstDec self id_ty =
	let k_id = Id.as_Set id_ty in
	add_tyvar (k_id,0) (Fin_B (TypeIdentifier k_id)) Local;
	SetAbstDec k_id
	  
      (* MRB p17 (?) *)  
      let mk_SetEnumDec self id_ty ids =
	let k_id = Id.as_Set id_ty in
	(* ? let nt = add_tyvar (k_id,0) (Fin_B (TypeIdentifier k_id)) Local in *)
	List.iter (fun e -> 
	  match e with 
	  | Id(Local,s,_,_) -> 
	      add_tyvar (Id.as_Cst e,0) (TypeIdentifier k_id) Local
	  | _ -> raise (NotYetImplemented "Typing.mk_SetEnumDec") ) ids;
	SetEnumDec (k_id, ids)
	  


      (* YP MRB ??? peut etre 7.17 p 145 valuation*)
      let mk_instance self id_ty exprList_ty =
	let exps = mk_exprList exprList_ty mk_empty in
	let org = Id.remote_amn_org id_ty in
	(set_origin org (Id.as_MachineName id_ty), List.map valueof exps)
	
      (*YP MRB 3.4 p 17 (?)*)
      let mk_TypeNatSetRange self e1 e2 =
	TypeNatSetRange 
	  (untype mk_integer (e1 mk_empty),untype mk_integer (e2 mk_empty))

      (* YP MRB 5.3 p 39*)  
      let mk_PredType self expr_ty =
	PredType (untype mk_pred (expr_ty mk_empty))
	  
     (*YP MRB ??? *)
      let mk_TypeIdentifier self id_ty =
	TypeIdentifier (id_ty)
	
      (* YP MRB 5.15 p 62 (fonctions partielles)*)
      let mk_PFunType self t1 t2 =
	PFunType (t1, t2)
	  
      (*YP MRB 5.15 p 62 (fonctions totales)*)
      let mk_FunType self t1 t2 =
	FunType (t1, t2)
	  
      (* YP MRB 5.7 p 46 (produit cart�sien)*)  
      let mk_ProdType self t1 t2 =
	ProdType (t1, t2)
	  
      (*YP MRB ??? *)
      let mk_Sequence self t =
	Sequence t
	
      (*YP MRB ???*)  
      let mk_SubType self t1 e2 =
	SubType (t1,  (untype mk_pred (e2 mk_empty)))

      (*YP MRB 5.7 p 46*)
      let mk_Fin self t =
	Fin_B t

      (*YP MRB ???*)
      let mk_Untyped self = 
	Untyped
	  
      (*YP MRB 3.4 p 18 ou 5.9 p 51 *)
      let mk_field self id_ty type_B_ty =
	(id_ty, type_B_ty)
	  
      (*YP MRB 4.1 p 28 *)
      let mk_PredParen self pred_ty =
	fun e -> PredParen (pred_ty e)
	    
      (*YP MRB 4.1 p 28 *)  
      let mk_PredNegation self pred_ty =
	fun e ->
	  let _ =
	    Error.debug
	      ((Id.debug_bienv e)
	       ^ " |- not "
	      )
	  in
	  PredNegation (pred_ty mk_empty)
	    
      (*YP MRB 4.2 p 29 *)
      let mk_PredExists self ids pred_ty =
	fun (ep,en) -> 
	  let _ =
	    Error.debug
	      ((Id.debug_bienv (ep,en))
	       ^ " |- #"
	       ^ (Id.debug_ids ids)
	      )
	  in
	  let kids = List.map Id.as_LocVar ids in
	  let _ = List.iter add_const1 kids in
	  let ne = pred_ty (ids@ep,en) in
	  let nids = List.map extract_id_type kids in
	  PredExists (nids, ne)

      (*YP MRB 4.2 p 29 *)    
      let mk_PredForAll self ids pred_ty =
	fun (ep,en) ->
	  let _ =
	    Error.debug
	      ((Id.debug_bienv (ep,en))
	       ^ " |- !"
	       ^ (Id.debug_ids ids)
	      )
	  in
	  let kids = List.map Id.as_LocVar ids in
	  let _ = List.iter add_const1 kids in
	  let ne = pred_ty (ep,ids@en) in
	  let nids = List.map extract_id_type kids in
	  PredForAll (nids, ne)
	    
      (* MRB 4.1 *)
      let mk_PredBin self bop2 pred_ty1 pred_ty2 =
	fun (ep,en) ->
	  let _ =
	    Error.debug
	      ((Id.debug_bienv (ep,en))
	       ^ " |- " ^ (Bbop.bop2_name bop2)
	      )
	  in
	  let _ = Error.debug ("Typing a predbin...") in
	  let env1,env2 = match bop2 with
	    Bbop.And -> (ep,[]),(ep,[])
	  | Bbop.Implies -> (en,[]),(ep,[])
	  | _ -> mk_empty,mk_empty
	  in
	  let e1 = pred_ty1 env1 in
	  let e2 = pred_ty2 env2 in
	  match bop2 with
	    Bbop.Ou | Bbop.And | Bbop.Implies | Bbop.Equiv ->
	      PredBin (bop2, e1, e2)
	  | _ -> raise (NotYetImplemented (Bbop.bop2_name bop2))

      let mk_PredAtom self bop2 expr_ty1 expr_ty2 =
	fun e -> 
	  let _ =
	    Error.debug
	      ((Id.debug_bienv e)
	       ^ " |- " ^ (Bbop.bop2_name bop2)
	      )
	  in
	  let e1 = expr_ty1 e in
	  let e2 = expr_ty2 e in
	  match bop2 with
	  | Bbop.In ->
	      let els = 
	    	match e1 with
		  ExprNuplet l,t ->  
		    List.map2 (fun e t -> (e,t)) l (dest_prod_ty t)
	    	| x -> [x] in
	      let n = List.length els in
	      let nels = List.map2 
		(fun e1 e2 ->
	    	   match e1 with
		       (ExprId (Id(p,n,t,_) as id), Untyped) ->  (* BUG loc *)
			 ExprId (change_tyvar (id,0) e2)
	    	     | (ExprFunCall (ExprId (Id(p,n,t,_) as id),[]),Untyped) ->
			 ExprFunCall (ExprId (change_tyvar (id,0) e2),[])
	    	     | (ee,Untyped) -> raise (InternalError ee)
	    	     | _ -> check_type (typeof e1) e2; valueof e1
		)
	    	els (split_prod (asType e2) n) in
	      let ne1 = 
	    	match e1 with
		    ExprNuplet l,t -> ExprNuplet nels
		  | x -> List.hd nels in
		PredAtom (bop2, ne1, valueof e2)
		  (*YP MRB 4.4 p 31*)
	  | Bbop.NotIn -> 
	      check_type (typeof e1) (asType e2);
	      PredAtom (bop2,valueof e1, valueof e2)
		(*YP MRB 4.6 p 33 *)
	  | Bbop.Less 
	  | Bbop.LessEqual 
	  | Bbop.Greater 
	  | Bbop.GreaterEqual ->
	      PredAtom (bop2, untype mk_integer e1, untype mk_integer e2)
		(* YP MRB 4.5 p 32*)
	  | Bbop.SubSet 
	  | Bbop.StrictSubSet -> 
	      if check_is_set (typeof e2) then 
	    	let ne1 = match e1 with (* BUG loc *)
		    ((ExprId (Id(p,n,t,_) as id)), Untyped) -> 
		      ExprId(change_tyvar (id,0) (typeof e2))
	    	  | ((ExprFunCall ((ExprId (Id(p,n,t,_) as id)),[])), Untyped) ->
		      ExprFunCall (ExprId(change_tyvar (id,0) (typeof e2)),[])
	    	  | (ee,Untyped) -> raise (InternalError ee)
	    	  | _ -> check_type (typeof e1) (typeof e2); valueof e1 in
		  PredAtom (bop2, ne1, valueof e2)
	      else raise (NotPowerSet (typeof e2))
		
	  | Bbop.NotSubSet 
	  | Bbop.NotStrictSubSet ->
	      check_type (asType e1) (asType e2);
	      PredAtom (bop2, valueof e1, valueof e2)
		(*YP MRB 4.3 p 30 *)
		(* \typage{T3 + T7} *)
	  | Bbop.NotEqual -> 
	      check_type (typeof e1) (typeof e2);
	      PredAtom (bop2, valueof e1, valueof e2)
		
	  (* MRB 4.3 p30 \typage{T7 p. 68} *)
	  | Bbop.Equal -> 
	      let ne1 = match e1 with (* BUG loc *)
		  ((ExprId (Id(p,n,t,_) as id)), Untyped) -> 
		    ExprId(change_tyvar (id,0) (typeof e2))
		| ((ExprFunCall ((ExprId (Id(p,n,t,_) as id)),[])), Untyped) ->
		    ExprFunCall (ExprId(change_tyvar (id,0) (typeof e2)),[])
		| (ee,Untyped) -> raise (InternalError ee)
		| _ -> check_type (typeof e1) (typeof e2); valueof e1 in
		PredAtom (bop2, ne1, valueof e2)
	  | _ -> raise (NotYetImplemented (Bbop.bop2_name bop2))
	      
      (*YP MRB 7.23 p 164*)
      let mk_operation self id_ty ids1 ids2 substitution_ty =
	try
	  let ty = typeof_id id_ty in
	  let (ty1,ty2) =
	    match ty with
	      | PFunType (d,ProdType (s,r)) when s = mk_subst -> 
		  (dest_prod_ty r,dest_prod_ty d)
	      | FunType (d,ProdType (s,r)) when s = mk_subst -> 
		  (dest_prod_ty r,dest_prod_ty d)
	      | PFunType (d,s) when s = mk_subst -> ([],dest_prod_ty d)
	      | FunType (d,s) when s = mk_subst -> ([],dest_prod_ty d)
	      | s when s = mk_subst -> ([],[])
	      | ProdType (s,r) when s = mk_subst -> 
		  (dest_prod_ty r,[])
	      |	_ -> raise (InvalidRedefinition id_ty) in
	    List.iter2
	      (fun i t -> 
		 match i with
		   | Id(Local,s,_,_) -> add_tyvar (Id.as_LocVar i,0) t Local 
		       (* BUG loc *)
		   | _ -> raise (UnknownTyping)
	      ) ids1 ty1;
	    List.iter2
	      (fun i t -> 
		 match i with
		   | Id(Local,s,_,_) -> add_tyvar (Id.as_LocVar i,0) t Local 
		       (* BUG loc *)
		   | _ -> raise (UnknownTyping)
	      ) ids2 ty2;
	    let bdy = substitution_ty (ids1,ids2) in
	      (id_ty, List.map extract_id_type ids1, 
	       List.map extract_id_type ids2, bdy)
	with
	    InvalidRedefinition id_ty -> raise (InvalidRedefinition id_ty)
	  | _ -> begin
	      List.iter 
	    	(fun i -> 
		   match i with
		     | Id(Local,s,_,_) 
		       -> add_tyvar (Id.as_LocVar i,0) Untyped Local 
			 (* BUG loc *)
		     | _ -> raise (UnknownTyping)
		) ids1;
	      List.iter 
	    	(fun i -> 
		   match i with
		     | Id(Local,s,_,_) 
		       -> add_tyvar (Id.as_LocVar i,0) Untyped Local 
			 (* BUG loc *)
		     | _ -> raise (UnknownTyping)
		) ids2;
	      let bdy = substitution_ty (ids1,ids2) in
		(Id.as_OpName id_ty, List.map extract_id_type ids1, 
		 List.map extract_id_type ids2, bdy)
	    end
	      
      (*YP MRB 6.15 p 101 (?)*)
      let mk_ExprSequence self expr_ty1 expr_ty2 =
	fun e -> 
	  exc_expr := Some self;
	  let e1 = expr_ty1 mk_empty and e2 = expr_ty2 mk_empty in
	  let res = ProdType (typeof e1,typeof e2) in
	    ((ExprSequence (valueof e1, valueof e2)), res)
	      
      (*YP MRB 5.1 p 36*)
      let mk_ExprParen self expr_ty =
	fun e -> 
	  exc_expr := Some self; 
	  let ne = expr_ty e in
	    ((ExprParen (valueof ne)), typeof ne)
	      
      (*YP MRB 5.11 p 54*)	    
      let mk_ExprId self (Id(p,n,t,k) as id_ty) =
	fun (ep,_) -> 
	  try 
	    let ty_id = typeof_id id_ty in
	      (match t with
		   Some t_ty -> type_cmp t_ty ty_id 
		 | _ -> ());
	      ExprId (env_id id_ty), ty_id 
	  with exc -> 
	    if lookup id_ty ep then 
	      (ExprId id_ty), (Untyped)
	    else raise exc
	      
      (*YP MRB 5.15 p 63*)
      let mk_ExprFunCall self f args =
	fun e ->  
	  exc_expr := Some self;
	  let nf = f e 
	  and nargs = mk_exprList args e in
	    (ExprFunCall (valueof nf, List.map valueof nargs)),
	    (tyFuncall self nf nargs)
	      
      let mk_ExprBin self op2 expr_ty1 expr_ty2 =
	fun e -> 
	  exc_expr := Some self;	
	  (* 
	     let _ =
	     print_expr self ; flush
	     in
	  *)
	  let e1 = expr_ty1 e 
	  and e2 = expr_ty2 e in
	  let ty1 = typeof e1 
	  and ty2 = typeof e2 in
	  let nop2 = 
	    let _ = Error.debug "Sets Mult." in
	      if op2 = Bbop.Mul && check_is_set ty1 
	      then Bbop.SetMul
	      else 
		let _ = Error.debug "Sets Sub." in
		  if op2 = Bbop.Minus && check_is_set ty1 
		  then Bbop.SetMinus
		  else op2 
	  in
	    (Location.putl e 
	       (ExprBin (nop2, valueof e1, valueof e2))),
	  (match op2 with
	       
	     (* \typage{MRB 5.7 Expressions ensemblistes} *)
	     | Bbop.NatSetRange ->
		 (* ? let t1 = valueof e1 in *)
		 (* ? let t2 = valueof e2 in *)
		 echeck_type self 
		   (list_mk_prod [mk_integer;mk_integer]) 
		   (list_mk_prod [typeof e1;typeof e2]);
		 Fin_B (TypeNatSetRange(valueof e1, valueof e2))
		   
	     (* \typage{MRB 5.8  Expressions ensemblistes} *)
	     (* SC: 
		redondance avec la 
		v�rification du d�but de fonction? *)
	     | Bbop.Mul 
		 when check_is_set ty1 && check_is_set ty2 -> 
		 Blast.Fin_B (Blast.ProdType (asType e1, asType e2))
	     | Bbop.Minus 
		 when check_is_set ty1 && check_is_set ty2 ->
		 let t1 = asType e1 and t2 = asType e2 in
		   echeck_type self t1 t2;
		   Blast.Fin_B t1
	     | Bbop.InterSets | Bbop.UnionSets ->
		 let t1 = asType e1 
		 and t2 = asType e2 in
		   echeck_type self t1 t2;
		   Blast.Fin_B t1
		     (*YP MRB 5.14 Expressions de relations p 60 *)
	     | Bbop.OverRideFwd ->
		 begin
	    	   match (super ty1, super ty2) with 
		       (Fin_B ((ProdType (t1,t2)) as ty1), Fin_B ty2) ->
			 echeck_type self ty1 ty2; Blast.Fin_B ty1
	    	     | (t1,t2) -> raise (InvalidApplication ("OverRidFwd",[t1;t2]))
		 end
	     | Bbop.DomRestrict | Bbop.DomSubstract ->
		 begin
	    	   match (super ty1, super ty2) with 
		       (ty1, Blast.Fin_B (ProdType (t1,t2))) ->
			 echeck_type self ty1 (Blast.Fin_B t1); typeof e2
	    	     | (t1,t2) -> raise (InvalidApplication ("<|",[t1;t2]))
		 end
		   
		   
	     (* \typage{MRB 5.3\\  Expressions arithm�tiques} *)
	     | Bbop.Mul | Bbop.Div | Bbop.Plus | Bbop.Minus | Bbop.Mod 
	     | Bbop.Puissance ->
		 echeck_type self (list_mk_prod [mk_integer;mk_integer]) 
		   (list_mk_prod [typeof e1;typeof e2]);
		 mk_integer
		   
	     | Bbop.PartialFunc 
	     | Bbop.PartialInj 
	     | Bbop.PartialSurj 
	     | Bbop.PartialBij ->
		 begin
		   let _ = 
		     Error.debug ( "MRB 5.15 Ensemble de fonctions p 62")  in
		     match 
		       (super ty1, super ty2) 
		     with
			 (Fin_B t1, Fin_B t2) -> 
			   Fin_B (PFunType(asType e1,asType e2))
		       | (t1,t2) -> 
			   raise (InvalidApplication ("-->",[t1;t2]))
		 end
		   
	     | Bbop.TotalFunc 
	     | Bbop.TotalInj 
	     | Bbop.TotalSurj 
	     | Bbop.TotalBij ->
		 begin
		   match 
		     (super ty1, super ty2) 
		   with
		       (Fin_B t1, Fin_B t2) -> 
			 Fin_B (FunType(asType e1,asType e2))
		     | (t1,t2) -> 
			 raise (InvalidApplication ("-->",[t1;t2]))
		 end
	     | Bbop.RelProd ->
		 let _ =
		   Error.debug ("MRB 5.11 Expressions de relations p.54 ") in
		 let dom1 = dom_of ty1 and
		     ran1 = ran_of ty1 and
		     dom2 = dom_of ty2 and
		     ran2 = ran_of ty2 in
		   echeck_type self dom1 dom2;
		   Fin_B(ProdType (dom1,ProdType(ran1,ran2)))
	     | Bbop.Image ->
		 echeck_type self (dom_of ty1) (asType e2);
		 Fin_B (ran_of ty1)
	     | Bbop.Prj1 -> 
		 let t1 = asType e1 and t2 = asType e2 in
		   FunType(Blast.ProdType (t1,t2), t1)
	     | Bbop.Prj2 ->  
		 let t1 = asType e1 and t2 = asType e2 in
		   FunType(Blast.ProdType (t1,t2), t2)
		     (*YP MBR 5.19 Expressions de suites p 70 *)	
	     | Bbop.ConcatSeq ->
		 (* *)
		 echeck_type self (ran_of ty1) (ran_of ty2);
		 echeck_type self (dom_of ty1) mk_natural;
		 echeck_type self (dom_of ty2) mk_natural;
		 typeof e1
	     | Bbop.AppendSeq ->
		 echeck_type self (ran_of ty1) ty2;
		 echeck_type self (dom_of ty1) mk_natural;
		 typeof e1
	     | Bbop.PrependSeq ->
		 echeck_type self (ran_of ty2) ty1;
		 echeck_type self (dom_of ty2) mk_natural;
		 typeof e2
	     | Bbop.SuffixSeq | Bbop.PrefixSeq ->
		 echeck_type self (dom_of ty1) mk_natural;
		 echeck_type self ty2 mk_natural;
		 typeof e1
	     | _ -> raise (NotYetImplemented (Bbop.op2_name op2)))
	      
      let mk_ExprUn self op1 expr_ty =
	fun e -> 
	  exc_expr := Some self;
	  let e1 = expr_ty e in
	    (ExprUn (op1, valueof e1)),
	  (match op1 with
	       (* a revoir verifier que e1 est un type *)
	       (*YP MRB 5.7 p 47*)
	     | Bbop.Fin -> Fin_B (typeof e1)
		 (* a revoir verifier que e1 est un type *)
	     | Bbop.Pow -> Fin_B (typeof e1) 
		 (*YP MRB 5.17 p 66*)		   
	     | Bbop.Seq 
	     | Bbop.ISeq -> Fin_B (mk_seq (asType e1))
	     | Bbop.Perm 
		 
	     (*MRB 5.18 
	       In expressions size (E), first (E), last (E), front (E), 
	       tail (E) and rev (E), 
	       E must designate a P(Z x T) type sequence. 
	       The type of size (E) is an integer type. 
	       The type of expressions first (E) and last (E) is t. 
	       The type of sequences front(E), tail(E) and rev(E) is P(Z x T). 
	     *)
	     | Bbop.Size -> 
		   let ty = dom_of (typeof e1) in check_type ty mk_integer; ty
	       | Bbop.First 
	       | Bbop.Last -> 
		   echeck_type self (dom_of (typeof e1)) mk_natural; 
		   ran_of (typeof e1)
		     
		     
	       | Bbop.Conc -> 
		   echeck_type self (dom_of (typeof e1)) mk_natural; 
		   let ty = ran_of (typeof e1) in
		     check_type (dom_of ty) mk_natural;
		     ty
	       | Bbop.Rev 
	       | Bbop.Front 
	       | Bbop.Tail -> 
		   echeck_type self (dom_of (typeof e1)) mk_natural; typeof e1
		     
	       (*YP MRB 5.4 p 41 *)
	       | Bbop.Card -> 
		   if 
		     check_is_set (typeof e1) 
		   then 
		     mk_natural
		   else 
		     raise (InvalidApplication ("card",[typeof e1]))
	       | Bbop.Min 
	       | Bbop.Max -> 
		   echeck_type self (Fin_B mk_natural) (typeof e1); mk_natural
		     
	       (*MRB 5.13 
		 In the expressions dom (R) and ran (R), 
		 R must be a P(T x V) type relation. 
		 The type of dom will be P(T) and the type of ran is P(V).
	       *)
	       | Bbop.Dom -> Fin_B (dom_of (typeof e1))
	       | Bbop.Ran -> Fin_B (ran_of (typeof e1))

	       (*MRB 5.14  
	       | Bbop.DomSubstract -> Fin_B (dom_of (typeof e1))
	       | Bbop.RanSubstract -> Fin_B (ran_of (typeof e1))
	       *)
		   (*YP MRB 5.2 p 38*)
	       | Bbop.Bool -> echeck_type self mk_pred (typeof e1); mk_bool
		   (*YP MRB 5.3 Moins Unaire p 39*)
	       | Bbop.UMinus -> echeck_type self mk_integer (typeof e1); mk_integer 
		   (*YP MRB 5.11 p 54 Expressions de relations Inverse *)
	       | Bbop.Tilde -> 
		   Fin_B (Blast_lib.mk_ProdType 
			  (ran_of (typeof e1)) (dom_of (typeof e1)))
	       |  _ -> raise (NotYetImplemented (Bbop.op1_name op1)))
	      
      (*YP MRB 5.16 p 64 Lambda expression*)
      let mk_ExprLambda self ids pred_ty1 expr_ty2 =
	fun ((ep,en) as e) ->
	let _ =
	  Error.debug ("MRB 5.16 p 64 Lambda expression ") in
	  exc_expr := Some self;
	  let kids = List.map Id.as_LocVar ids in
	  let _ = List.iter add_const1 kids in
	  let ne1 = pred_ty1 (ids@ep,en) in
	  let ne2 = expr_ty2 e in
	  let nids = List.map extract_id_type kids in
	    ExprLambda (nids, ne1, valueof ne2), 
	    mk_ftype (List.map Id.typeof nids) (typeof ne2)
	      
      (*YP MRB 5.17 p 66 *)
      let mk_ExprSeq self exprSeq_ty =
	fun e -> 
	  exc_expr := Some self;
	  let ns = exprSeq_ty e in
	    (ExprSeq (valueof ns)), typeof ns
	      
      (*YP MRB ???*)
      let mk_ExprSet self exprSet_ty =
	fun e -> 
	  exc_expr := Some self;
	  let ns = exprSet_ty e in
	    (ExprSet (valueof ns)), typeof ns
	      
      (*YP MRB ???*)
      let mk_RelSet self expr_ty1 expr_ty2 =
	fun e -> 
	  exc_expr := Some self;
	  let e1 = expr_ty1 e and e2 = expr_ty2 e in
	    (RelSet (valueof e1, valueof e2)),
	    (Fin_B (Fin_B (Blast_lib.mk_ProdType (asType e1) (asType e2))))
	      
      (*YP MRB ???*)
      let mk_RelSeqComp self expr_ty_list = 
	fun ev -> 
	  exc_expr := Some self;
	  let el = mk_exprList expr_ty_list ev in
	    (RelSeqComp (List.map valueof el), 
	     list_unique_ty (List.map typeof el))
	      
      
      let mk_ExprNumber self number_ty =
	let _ = Error.debug "ExprNumber MRB 5.6 p.44" in
	fun e -> 
	  exc_expr := Some self ; 
	  (ExprNumber (number_ty)), mk_integer
	    
      
      let mk_ExprBool self bool =
	let _ = Error.debug "ExprBool MRB 5.6 p.44" in
	fun e -> exc_expr := Some self;
	  (ExprBool (bool)), mk_bool
	  
      
      let mk_ExprString self string =
	let _ = Error.debug "(*YP ExprString MRB 5.6 p 44*)" in
	  fun e ->  exc_expr := Some self;(ExprString (string)), mk_string

      (*YP MRB ???*)
      let mk_ExprPred self predicate_ty =
	fun e -> (ExprPred (predicate_ty e)), mk_bool
	  
      (*YP MRB ???*)
      let mk_ExprNuplet self expr_ty_list =
	fun e -> 
	  let l_ns = mk_exprList expr_ty_list e in
	    (ExprNuplet (List.map valueof l_ns)),
	    list_mk_prod (List.map typeof l_ns)
	      
      
      let mk_ExprSIGMA self ids pred_ty1 expr_ty2 =
	let _ = Error.debug "(*YP MRB 5.4 p 41*)" in
	fun (ep,en) ->
	  let kids = List.map Id.as_LocVar ids in
	  let _ = List.iter add_const1 kids in
	  let e1 = pred_ty1 (ids@ep,en) in
	  let e2 = expr_ty2 (ep,en) in
	  let nids = List.map extract_id_type kids in
	    (ExprSIGMA (nids, e1, untype mk_integer e2)), mk_integer
	      
      (*YP MRB 5.4 p 41*)    
      let mk_ExprPI self ids pred_ty1 expr_ty2 =
	fun (ep,en) -> 
	  let kids = List.map Id.as_LocVar ids in
	  let _ = List.iter add_const1 kids in
	  let e1 = pred_ty1 (ids@ep,en) in
	  let e2 = expr_ty2 (ep,en) in
	  let nids = List.map extract_id_type kids in
	  (ExprPI (nids, e1, untype mk_integer e2)), mk_integer
	    
      (*YP MRB 5.17 p 66 *)
      let mk_SeqEnum self expr_ty_list =
	fun e ->
	  let nl = mk_exprList expr_ty_list e in
	  SeqEnum (List.map valueof nl),
	  mk_seq (list_unique_ty (List.map typeof nl))
	    
      (*YP MRB 5.17 p 66 suite vide *)
      let mk_SeqEmpty self =
	fun _ -> SeqEmpty, mk_seq Untyped

      (*YP MRB 5.7 p 46 Expressions Ensemblistes intervalle*)
      let mk_SetRange self expr_ty1 expr_ty2 =
	fun e ->
	  let ne1 = expr_ty1 e and ne2 = expr_ty2 e in
	  SetRange (untype mk_integer ne1, untype mk_integer ne2), 
	  (Fin_B (TypeNatSetRange (valueof ne1, valueof ne2)))
	   
      (*YP MRB 5.7 p 46 (?)*)
      let mk_SetEnum self expr_ty_list =
	fun e ->
	  let el = mk_exprList expr_ty_list e in
	  SetEnum (List.map valueof el), 
	  Fin_B (list_unique_ty (List.map typeof el))
	    
      (*YP MRB 5.7 p 46 Ensemble d�fini en comprehension *)
      let mk_SetCompr self expr_ty_list =
	fun e ->
	  let el = mk_exprList expr_ty_list e in
	    SetCompr (List.map valueof el), 
	    Fin_B (list_unique_ty (List.map typeof el))
	      
      (*YP MRB 5.7 p 46  Ensemble d�fini en comprehension (?)*)
      let mk_SetComprPred self ids pred_ty =
	fun _ -> 
	  let kids = List.map Id.as_LocVar ids in
	  let _ = List.iter add_const1 kids in
	  let e = pred_ty ([],kids) in
	  let nids = List.map extract_id_type kids in
	    (SetComprPred (nids, e), 
	     Fin_B (list_mk_prod (List.map Id.typeof nids)))
	    
      (*YP MRB 5.6 p 44 Ensemble vide*)
      let mk_SetEmpty self =
	fun _ -> SetEmpty, Fin_B Untyped
	    
      (*YP MRB 5.8 p 48 Union quantifi�e*)
      let mk_SetUnionQuantify self ids pred_ty1 expr_ty2 =
	fun e ->
	  let ne1 = pred_ty1 (ids,[]) and ne2 = expr_ty2 e in
	  SetUnionQuantify (ids, ne1, valueof ne2), 
	  (typeof ne2)
	    
	   
      let mk_SetInterQuantify self ids pred_ty1 expr_ty2 =
	fun e ->
	  let _ = Error.debug "(*YP MRB 5.8 p 48 Intersection quantifi�e*)" in
	  let ne1 = pred_ty1 (ids,[]) 
	  and ne2 = expr_ty2 e 
	  in
	    SetInterQuantify (ids, ne1, valueof ne2), (typeof ne2)
	      
      (*YP MRB 5.3 p 39*)
      let mk_MinNumber self = MinNumber
      let mk_Number self int = Number (int)
      let mk_MaxNumber self = MaxNumber


      (*YP MRB ???*)
      let check_tyassigns (ep,en) (Id(p,n,t,k) as id_ty) t2 =
	let t1 = typeof_id id_ty in
	  begin
	    match t1 with
	      |	Untyped when (lookup id_ty ep) ->
		  if is_typed (super t2) then 
	      	    change_tyvar (id_ty,0) t2 (* BUG loc *)
		  else raise (BadAssign t2)
	      | Untyped -> raise (UntypedId id_ty)
	      | _ -> check_type t1 t2; Id(p,n,Some t1,k)
	  end
	    
      (*YP MRB ???*)
      let check_assigns e id_ty e2 = 
	check_tyassigns e id_ty (typeof e2)
	  
      (*YP MRB 6.16 p 102 *)
      let mk_SubstOperCall self ids f args1 =
	fun e ->
	  exc_subst := Some self;
	  let args = mk_exprList args1 e in
	  let sres = if args = [] then (typeof_id f) 
	  else ty_apply (typeof_id f) (list_mk_prod (List.map typeof args)) in
	  let res =
	    match sres with
		i when i = mk_subst -> []
	      | ProdType(i,r) when i = mk_subst -> dest_prod_ty r
	      | _ -> raise (InvalidResult self) in
	  let nids =
	    try
	      List.map2 (check_tyassigns e) ids res;
	    with _ -> raise (InvalidResult self) in
	    Blast_lib.mk_SubstOperCall nids (env_id f) (List.map valueof args)
	      
      
      let mk_SubstBlock self substitution_ty =
	fun e -> 
	  let _ = Error.debug "(*YP substBlock MRB 6.1 p 84 *)" in
	  SubstBlock (substitution_ty e)
	    
      
      let mk_SubstPrecondition self pred_ty substitution_ty =
	fun (ep,en) ->
	  let _ = Error.debug "(*YP Precondition MRB 6.4 p 88 *)" in
	  let pre = pred_ty (en,[]) in (* ordre *)
	  SubstPrecondition (pre, substitution_ty (ep,[]))
	    

      let mk_SubstAssertion self pred_ty substitution_ty =
	fun ev ->
	  let _ = Error.debug "(*YP SubstAssert MRB 6.5 p 89 *)" in 
	  SubstAssertion (pred_ty ev, substitution_ty ev)
	    
      (*YP MRB 6.6 p 90 *)
      let mk_SubstChoice self substitution_ty_list =
	fun ev ->
	  let _ = Error.debug "(*YP SubstChoice MRB 6.6 p 90 *)" in 
	  SubstChoice (List.map (fun s -> (s ev)) substitution_ty_list)
	  
      (*YP MRB 6.7 p 91 *)
      let mk_SubstIf self pred_substitution_list substitution_option =
	fun ev -> begin
	  match substitution_option with
	    Some sub ->
      	      SubstIf (List.map (fun (p,s) -> ((p ev),(s ev))) 
			 pred_substitution_list,
		       Some (sub ev))
	  | None -> SubstIf (List.map (fun(p,s)-> ((p ev),(s ev)))
			       pred_substitution_list,
			     None)
	end
	    
      (*YP MRB 6.8 p 93 *)
      let mk_SubstSelect self pred_substitution_list substitution_option =
	fun ev -> begin
	  match substitution_option with
	    Some sub -> SubstSelect 
		(List.map (fun (p,s) -> ((p ev),s ev)) 
		   pred_substitution_list,
		 Some (sub ev))
	  | None -> SubstSelect 
		(List.map (fun (p,s) -> ((p ev),s ev)) 
		   pred_substitution_list,
		 None)
	end
	    
      (*YP MRB 6.9 p 94 *)
      let mk_SubstCase self expr_ty expr_list_substitution_list 
	  substitution_option =
	fun ev ->
	  let ne = expr_ty ev in
	  let esl = List.map (fun (el,s) -> mk_exprList el ev,s ev) 
	      expr_list_substitution_list in
	  List.iter (fun (el,s) ->
	    List.iter 
	      (fun e -> echeck_type (valueof ne) (typeof ne) (typeof e)) el) esl;
	  SubstCase (valueof ne, 
		     List.map (fun (el,s) -> (List.map valueof el,s)) esl,
		     match substitution_option with
		       Some sub -> Some (sub ev)
		     | None -> None)
	    
      (*YP MRB 6.10 p 96 *)
      let mk_SubstAny self ids pred_ty substitution_ty =
	fun ev ->
	  let kids = List.map Id.as_LocVar ids in
	  let _ = List.iter add_const1 kids in
	  let p = pred_ty (kids,[]) in
	  let bdy = substitution_ty ev in
	  let nids = List.map extract_id_type kids in
	  SubstAny (nids, p, bdy)
	    
      (*YP MRB 6.11 p 97*)
      let mk_SubstLet self ids pred_ty substitution_ty =
	fun ((ep,en) as ev) ->
	  let kids = List.map Id.as_LocVar ids in
	  let _ = List.iter add_const1 kids in
	  let p = pred_ty (kids,[]) in
	  let sub = substitution_ty ev in
	  let nids = List.map extract_id_type kids in
	  SubstLet (nids, p, sub)
	    
      (*YP MRB 6.14 p 100 *)
      let mk_SubstVar self ids substitution_ty =
	fun (ep,en) ->
	  let kids = List.map Id.as_LocVar ids in
	  let _ = List.iter add_const1 kids in
	  let bdy = substitution_ty (ids@ep,en) in
	  let nids = List.map extract_id_type kids in
	  SubstVar (nids, bdy)
	    
      (*YP MRB 6.17 p 104 *)
      let mk_SubstWhile self pred_cnd substitution_ty  expr_var pred_inv =
	fun ev ->
	  SubstWhile (pred_cnd ev, 
		      substitution_ty ev,
		      untype mk_integer (expr_var ev), 
		      pred_inv ev)
	     
      (*YP MRB 6.2 p 85 *)
      let mk_SubstSkip self =
	fun _ -> SubstSkip
	    
      (*YP MRB 6.15 p 101 *)
      let mk_SubstSequence self substitution_ty1 substitution_ty2 =
	fun ev ->
	  let s1 = substitution_ty1 ev in
	  let s2 =  substitution_ty2 ev in
	  SubstSequence (s1, s2)
	    
      (*YP MRB 6.18 p 106 *)
      let mk_SubstParallel self substitution_ty1 substitution_ty2 =
	fun ev ->
	  SubstParallel (substitution_ty1 ev, substitution_ty2 ev)
	    
      (*YP MRB 6.3 p 86 (?) *)
      let mk_SubstSetEqualIds self ids expr_ty_list =
	fun ev ->
	  let el = mk_exprList expr_ty_list ev in
	  let nids = List.map2 (check_assigns ev) ids el in
	  SubstSetEqualIds (nids, List.map valueof el)
	    
      (*YP MRB 6.16 p 102 *)
      let mk_SubstSetEqualFun self f args1 expr_ty =
	fun ev ->
	  exc_subst := Some self;
	  let ff = f ev in
	  let args = mk_exprList args1 ev in
	  let exp = expr_ty ev in
	  check_type 
	    (ty_apply (typeof ff) (list_mk_prod (List.map typeof args)))
	    (typeof exp);
	  SubstSetEqualFun(valueof ff, List.map valueof args, valueof exp)
	    
      (*YP MRB 6.13 p 99 *)    
      let mk_SubstBecomeSuch self ids pred_ty =
	fun ev ->
	  let cnd = pred_ty (ids,[]) in
	  SubstBecomeSuch (ids, cnd)
	    
      (* \typage{T8} *)
	    (*YP MRB 6.12 p 98*)
      let mk_SubstSetIn self ids expr_ty = 
	fun ev ->
	  let n = List.length ids in
	  let set = expr_ty ev in
	  let nids = 
	    List.map2 (check_tyassigns ev) ids (split_prod (asType set) n) in
	  SubstSetIn (nids, valueof set)

      let mk_Local self = Blast_lib.mk_Local

      let mk_Remote self (_x1:(string * string) list) = Blast_lib.mk_Remote _x1

      let mk_OrgRefine self (_x1:string) (_x2:(string * string) list) = 
	Blast_lib.mk_OrgRefine _x1 _x2

      let mk_Prefix self (_x1:string list) = Blast_lib.mk_Prefix _x1

      let mk_ids self l = l
	  
      let mk_Id self p n t k = self
	  
    end 
      in let module TY = Blast_itself.Blast_it(Blast_typing) in
      (TY.it_amn,TY.it_expr,Blast_typing.super)

(* SC: We need a dummy value (here a unit ()) to make ocaml 3.07,3.08 accept a
recursive definition of a non-functional value (by making it a fonctional
value)
*)
let (amn_in_env, expr_in_env, super) = in_env ()

let of_ast ast =
  let _ = Tyenv.init () in  
  amn_in_env ast
