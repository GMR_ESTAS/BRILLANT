(* $Id$ *)

open Blast

open Blast_lib

open Id


let env = Hashtbl.create 100
	    

let dump f = 
  let vb = ref [] in
    Hashtbl.iter (fun (p,n) (org,_,ty,k) -> 
		    vb := (Page.HBox [
			     Page.Word ((Bbop.kind_name k)^"\t")
			     ;	
			     Blast_page.TXT.it_id (Id(org,n,None,k)) false
			     ;    
			     Page.Word ("\t")
			     ;
			     Blast_page.TXT.it_type_B ty
			   ]) 
		  :: !vb)	   
      env

(*MG
    ;
    (try 
      Unix.unlink f 
    with _ -> 
      prerr_endline ("Ne peut effacer :"^f) 
    )

    let fd = open_out f in
      Format.set_formatter_out_channel (fd);
      Blast_txt.page_txt Format.std_formatter (Page.VBox (!vb));
      Format.print_flush ();
      close_out fd
	
	*)
	
let add_const (Id(o,n,_,k),(loc:int)) = (* BUG loc???? *)
  Hashtbl.add env (org2prefix o,n) (Local,loc,Untyped,k)
    
let add_const1 x = add_const (x,0) (* BUG loc *)
		     
let add_tyvar (Id(o,n,_,k) as id,loc) ty org = 
  let p = org2prefix org in
    try
      match (Hashtbl.find env (p,n)) 
      with
	  (org1,_,_,_) -> 
	    if org <> org1 
	    then raise (Except.InvalidRedefinition id)
      with _ -> Hashtbl.add env (p,n) (org,loc,ty,k)
	
let set_origin org = function
    Id(_,n,ty,k) -> Id(org,n,ty,k)
      
let define_id s ty k = 
  add_tyvar (Id(Local,s,None,k),0) ty Local (* PB loc -> 0 *)
    
let change_tyvar ((Id(o,n,_,_) as id),loc) ty = 
  let p = org2prefix o in
    try
      let (org,_,_,k) = Hashtbl.find env (p,n) in
      let nid = Id(org,n,Some ty,k) in
	Hashtbl.remove env (p,n);
	add_tyvar (nid,loc) ty Local;
	nid
    with Not_found -> 
      raise (Except.InternalError (ExprId id))
      
      
let mk_builtintype s = TypeIdentifier (Id(Local,s,None,Bbop.Set))
			 
let list_mk_prod = function
    [] -> Untyped (* ou erreur *)
  | t::l -> List.fold_left mk_ProdType t l
      
let rec dest_prod_ty = function
    ProdType(t1,t2) -> (dest_prod_ty t1)@[t2]
  | t -> [t]
      
let mk_ftype lt1 t2 =  
  if lt1 = [] then t2 else PFunType (list_mk_prod lt1, t2)
    
let mk_bool = mk_builtintype "BOOL"
let mk_integer = mk_builtintype "INTEGER"
let mk_natural = mk_builtintype "NATURAL"
let mk_string = mk_builtintype "STRING"
let mk_seq t = Sequence t
let mk_pred = mk_builtintype "!PRED"
let mk_subst = mk_builtintype "!SUBST"
let mk_mch = mk_builtintype "!MCH"
	       
let init () =
  define_id "BOOL" (Fin_B mk_bool) Bbop.Set ;
  define_id "STRING" (Fin_B mk_string) Bbop.Set ;
  define_id "TRUE" mk_bool Bbop.Cst;
  define_id "FALSE" mk_bool Bbop.Cst;
  define_id "NAT" (Fin_B (mk_builtintype "NAT")) Bbop.Set ;
  define_id "NAT1" (Fin_B (mk_builtintype "NAT1")) Bbop.Set ; 
  define_id "NATURAL" (Fin_B mk_natural) Bbop.Set ; 
  define_id "NATURAL1" (Fin_B (mk_builtintype "NATURAL1")) Bbop.Set ; 
  define_id "INTEGER" (Fin_B mk_integer) Bbop.Set ;;
define_id "INT" (Fin_B mk_integer) Bbop.Set ;;

let rec cmpty1 ty1 ty2 = 
  match ty1,ty2 with
    | TypeIdentifier i1, TypeIdentifier i2 when i1 = i2 -> ()
    | PFunType (d1,r1) , PFunType (d2,r2) ->
	cmpty1 d1 d2; cmpty1 r1 r2
    | ProdType (d1,r1) , ProdType (d2,r2) ->
	cmpty1 d1 d2; cmpty1 r1 r2
    | Fin_B t1, Fin_B t2 -> cmpty1 t1 t2
    | Untyped,_ -> ()
    | _ , Untyped -> ()
    | _ -> raise (Except.TypeError(ty1,ty2))
	
let rec is_typed = function
  | TypeIdentifier i1 -> true
  | PFunType (d,r) -> is_typed d && is_typed r
  | ProdType (d,r) -> is_typed d && is_typed r
  | Fin_B t -> is_typed t
  | Untyped -> false
  | _ -> failwith "is_typed"
      
let type_cmp ty1 ty2 =
  try cmpty1 ty1 ty2
  with _ -> raise (Except.TypeError(ty1,ty2))
    
let mk_empty = ([],[])
		 
let valueof exp = fst exp
		    
let typeof exp = snd exp
		   
let typeof_id (Id(o,n,_,_) as id) = 
  try 
    begin
      match (Hashtbl.find env (org2prefix o,n)) with
	  (_,_,ty,_) -> ty
    end
  with Not_found -> 
    raise (Except.Undeclared("identificateur non d�clar�", id))

let origin_of_id (Id(o,n,_,_) as id) = 
  try 
    begin
      match (Hashtbl.find env (org2prefix o,n)) with
	(org,_,_,_) -> org
    end
  with Not_found -> 
    raise (Except.Undeclared ("identificateur non d�clar�", id))
    
let env_id (Id(o,n,_,_) as id) = 
  try 
    begin
      match (Hashtbl.find env (org2prefix o,n)) with
	  (org,l,ty,k) -> Id(org,n,Some ty,k)
    end
  with Not_found -> 
    raise (Except.Undeclared ("identificateur non d�clar�", id))
    
let is_SET (Id(p,n,_,_)) = String.uppercase_ascii n = n












