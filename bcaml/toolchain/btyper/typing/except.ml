(* $Id$ *)


(** Exceptions related with type checking *)

open Blast

exception TypeError of type_B * type_B

exception BadApply of type_B * type_B

exception BadAssign of type_B

exception Undeclared of string * id

exception UntypedId of id

exception UnknownTyping

exception NotYetImplemented of string

exception InternalError of expr

exception NotProduct of type_B

exception NotRelation of type_B

exception NotPowerSet of type_B

exception BadArity of string

exception InvalidApplication of string * type_B list

exception InvalidResult of substitution

exception InvalidRedefinition of id

exception SuperError

