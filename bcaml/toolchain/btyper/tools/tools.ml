

let _ =
  let cmd = Filename.basename (Sys.argv.(0)) in
  Arg.parse [] Typer.typed_print ("usage: " ^ cmd ^ " <file>")
(*
  match cmd with
    "btyper" -> 
      Arg.parse [] Pretty_txt.pretty_txt "usage: btyper <file>" 
      Arg.parse [] Typer.typed_print "usage: btyper <file>"

  | "type_tool" -> 
      Arg.parse [] Typer.typed_tex_print "usage: type_tool <file>"
  | "tex_tool" -> 
      Arg.parse [("-h", Arg.Clear Options.header,
		  "suppress latex header")] 
	(Pretty_tex.pretty_tex true) "usage: tex_tool <file>"
  | "norm_tool" -> 
      Arg.parse [] Norm.norm_tex_print "usage: norm_tool <file>"
  | "pvs_tool" -> 
      Arg.parse [] B2pvs.b2pvs_print "usage: pvs_tool <file>"
  | _ -> print_string ("unknown command: "^cmd^"\n")
*)

