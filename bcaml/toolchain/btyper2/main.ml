(* $Id$ *)

(* This file is a copy/paste from bparser/main.ml and bunfold/main2.ml *)

open Project

(*
   Variables <<d'environnements>> et fonctions utiles pour les options
*)

let output_directory =
  try
    ref (Sys.getenv "BCAML_DEST")
  with
    Not_found -> ref("./")


let to_stdout = ref true
let verbose = ref false

let dest_set path =
  output_directory := path;
  to_stdout:=false

let btyper_version = "0.35"
let usage_message =
  "btyper " ^ btyper_version ^ " <filenames>"
  ^ "\n For each filename given on the command line, generates an unfolding of this filename\n"


let filenames=ref []

let add_filename str=
  Error.debug ("Adding to filenames : " ^ str);
  filenames:=str::(!filenames)


let rec args =
  [
   ("--help",
    Arg.Unit(fun () ->
      Arg.usage args usage_message; raise Exit),
    "\t\t\t display this help")
     ;
   ("-help",
    Arg.Unit(fun () ->
      Arg.usage args usage_message; raise Exit),
    "\t\t\t See --help")
     ;
   ("--dest",
    Arg.String(dest_set),
    "<dir>,  \t\t output the generated files in directory <dir> (deactivates output to stdout), \n\t\t\t\t and defaults to the current directory if other options activate it")
     ;
   ("--debug",
    Arg.Unit(Error.debug_set),
    ", \t\t\t debug mode (default to false)")
     ;
   ("--verbose",
    Arg.Set(verbose),
    ", \t\t\t display statistical information")
     ;
]


let save_ast dir ast=
  let mchname=Id_acc.name_of (Blast_acc.get_name ast) in
  let filename=dir ^ "/" ^ mchname ^ "." ^ Bfile.xml_extension in
    Bfile.mkpath dir;
    let out=open_out filename in
      try
        Bxml.to_chan out ast;
        close_out out
      with
          err -> close_out out; raise err


let out_ast ast=
  if !to_stdout
  then Bxml.to_chan stdout ast
  else
    save_ast !output_directory ast


let do_parsing ()=
  let allprojgroups=
    List.map
      (fun fname -> (fname, Parser_lib.overParse [] fname))
      !filenames in
  let project_list=
    List.map
      (fun (fname, projgroup) -> (Bfile.root_name fname, Project.buildProject projgroup))
      allprojgroups in
  let typed_projects=List.map Typing_top.type_check project_list in
    List.iter out_ast typed_projects

let _ =
  try
    Arg.parse args add_filename usage_message;
    do_parsing ();
    print_string "\n";
    flush stdout
(* Gui.process  files_list *)
  with Exit -> ()
