open Blast
open Tytools

type typing_env = {
  typed_machine: Blast.amn;
  type_env: Typing_env.t
}


let value_of clause=
  match clause with
  | Constraints(_) -> 1
  | Sees(_) -> 2
  | Includes(_) -> 3
  | Imports(_) -> 3
  | Promotes(_) -> 3
  | Extends(_) -> 3
  | Uses(_) -> 4
  | Sets(_) -> 5
  | ConstantsConcrete(_) -> 6
  | ConstantsAbstract(_) -> 6
  | ConstantsHidden(_) -> 6
  | Properties(_) -> 7
  | Values(_) -> 8
  | VariablesConcrete(_) -> 9
  | VariablesAbstract(_) -> 9
  | VariablesHidden(_) -> 9
  | Invariant(_) -> 10
  | Assertions(_) -> 10
  | Initialisation(_) -> 11
  | LocalOperations(_) -> 11
  | Operations(_) -> 11


(*FIXME: put the 3 following functions in Tytools *)

(* This confirms that cartesian product is left-associative *)
let rec types_of_prod = function
  | TypeProd(t1, t2) -> (types_of_prod t1)@[t2]
  | t -> [t]

let rec rev_prod_of_types = function
  | [] -> invalid_arg "prod_of_types"
  | [ty] -> ty
  | ty::othertys -> TypeProd(rev_prod_of_types othertys, ty)

(* Building the cartesian product from a list is a bit weird *)
let prod_of_types types_list = rev_prod_of_types (List.rev types_list)


(* "Type synthesis in B and the translation of B to PVS" Bodeveix,
   Filali section 4.2. The subdirectory doc/ contains the listing of
   explicit rules, to the best it could be deduced from the paper.
 *)

let negate_atomic_predicate = function
  | Bbop.Equal -> Bbop.NotEqual
  | Bbop.SubSet -> Bbop.NotSubSet
  | Bbop.StrictSubSet -> Bbop.NotStrictSubSet
  | Bbop.In -> Bbop.NotIn
  | Bbop.NotEqual -> Bbop.Equal
  | Bbop.NotSubSet -> Bbop.SubSet
  | Bbop.NotStrictSubSet -> Bbop.StrictSubSet
  | Bbop.NotIn -> Bbop.In
  | Bbop.Less -> Bbop.GreaterEqual
  | Bbop.LessEqual -> Bbop.Greater
  | Bbop.Greater -> Bbop.LessEqual
  | Bbop.GreaterEqual -> Bbop.Less
  | _ -> invalid_arg "negate_atomic_predicate"



let type_id_in tyenv id set_type =
  let e2_elt_type =
    try inner_type set_type
    (* FIXME: No way to give more information for the error at the moment *)
    with (NotPowerSet(_) as exc) -> raise exc
  in
  let id_path = path_of_id id in
  if Typing_env.mem tyenv id_path then
    try
      match (Typing_env.assoc tyenv id_path) with
      | Untyped -> Typing_env.replace tyenv (id_path, e2_elt_type)
      | someType ->
	 (* Looks like it was already typed. Does it match ? *)
	 if types_correspond someType e2_elt_type
	 then tyenv
	 else raise (TypeMismatch(id_path, someType, e2_elt_type))
    with Not_found -> assert false
  else invalid_arg ("type_id_in:" ^ (Id_acc.name_of id) ^ ": untyped not in typing environment")


let type_id_included tyenv id set_type =
  type_id_in tyenv id (TypePowerset(set_type))


let type_id_equal = type_id_included


(* The (pos,neg) parameter corresponds to the env_ty pairing in the
   original btyper algorithm. In a nutshell, we try to emulate ForAll
   and Implies elimination as required by Bodeveix and Filali's
   typechecking algorithm. To that end, one separates positively
   expressed untyped ids and negatively expressed.  Only positively
   expressed ids should be typeable. This is what was done in the
   "original" btyper. The code below should catch more cases than the
   original btyper, such as #(x,y).(not(x/:N or x/=y)).
 *)
let rec type_pred tyenv (pos,neg) = function
  | PredParen(pred) ->
     let (pred', tyenv') = type_pred tyenv (pos,neg) pred in
     (PredParen(pred'), tyenv')

  | PredNegation(pred) ->
     (* We do not remove the untyped ids from tyenv as in the pen&paper
	 rule, because these are supposed to have been transfered into the
	 (pos,neg) argument when calling type_pred initially *)
     let (pred', tyenv') = type_pred tyenv (neg,pos) pred in
     (PredNegation(pred'), tyenv')

  | PredExists(ids, pred) ->
     let path_ids = path_of_ids ids in
     let (pred', tyenv') = type_pred tyenv (path_ids@pos, neg) pred in
     let locally_typed_ids = locally_type_ids tyenv' ids in
     (* Now we remove the bindings that will be unseen at upper
	   levels *)
     let tyenv_nobinding =
       List.fold_left
	 (fun te pi -> Typing_env.remove te pi)
	 tyenv'
	 path_ids
     in
     (PredExists(locally_typed_ids, pred'), tyenv_nobinding)

  | PredForAll(ids, pred) ->
     let path_ids = path_of_ids ids in
     let (pred', tyenv') = type_pred tyenv (pos,path_ids@neg) pred in
     let locally_typed_ids = locally_type_ids tyenv' ids in
     let tyenv_nobinding =
       List.fold_left
	 (fun te pi -> Typing_env.remove te pi)
	 tyenv'
	 path_ids
     in
     (PredForAll(locally_typed_ids, pred'), tyenv_nobinding)

  | PredBin(logic_op, pred1, pred2) ->
     let (pred1', tyenv') =
       match logic_op with
       | Bbop.And -> type_pred tyenv (pos,[]) pred1
       | Bbop.Implies -> type_pred tyenv (neg, [])  pred1
       | Bbop.Ou -> type_pred tyenv ([], neg) pred1
       | Bbop.Equiv -> type_pred tyenv ([],[]) pred1
       | _ -> invalid_arg ("type_pred: malformed predicate")
     in
     let pos' = remove_typed_from_untyped_list tyenv' pos in
     let neg' = remove_typed_from_untyped_list tyenv' neg in
     let (pred2', tyenv'') =
       match logic_op with
       | Bbop.And -> type_pred tyenv' (pos',[]) pred2
       | Bbop.Implies -> type_pred tyenv' (pos',[]) pred2
       | Bbop.Ou -> type_pred tyenv' ([],neg') pred2
       | Bbop.Equiv -> type_pred tyenv' ([],[]) pred2
       | _ -> invalid_arg ("type_pred:PredBin: malformed predicate")
     in
     (PredBin(logic_op, pred1', pred2'),
      tyenv'')

  | PredAtom(atom_op, expr1, expr2) ->
     begin
       match atom_op with
       | Bbop.Less
       | Bbop.LessEqual
       | Bbop.Greater
       | Bbop.GreaterEqual ->
	  (* With these, both expressions should already be typeable *)
	  let (expr1', e1type) = type_expr tyenv expr1
	  and (expr2', e2type) = type_expr tyenv expr2 in
	  begin
	    match super e1type, super e2type with
	    | TypeBaseSet(Blast.INTEGER), TypeBaseSet(Blast.INTEGER) ->
	       (PredAtom(atom_op, expr1', expr2'), tyenv)
	    | TypeBaseSet(Blast.INTEGER), _ ->
	       raise (TypeMismatchExpr(expr2, e2type, TypeBaseSet(Blast.INTEGER)))
	    | _, _ ->
	       raise (TypeMismatchExpr(expr1, e1type, TypeBaseSet(Blast.INTEGER)))
	  end
       | Bbop.Equal
       | Bbop.SubSet
       | Bbop.StrictSubSet
       | Bbop.In ->
	  let annotation_function =
	    match atom_op with
	    | Bbop.Equal -> type_annotation_eq
	    | Bbop.SubSet
	    | Bbop.StrictSubSet -> type_annotation_subset
	    | Bbop.In -> type_annotation_in
	    | _ -> assert false
	  in
	  let full_tyenv =
	    List.fold_left
	      (fun tenv p -> Typing_env.add tenv (p, Untyped))
	      tyenv
	      pos
	  in
	  let (tyenv', expr1', expr2') =
	    annotation_function full_tyenv expr1 expr2
	  in
	  (PredAtom(atom_op, expr1', expr2'),
	   tyenv')

       (* For the negations, we use the equivalent negated formulation and rebuild the original NotSomething once it has been typed *)
       | Bbop.NotEqual
       | Bbop.NotSubSet
       | Bbop.NotStrictSubSet
       | Bbop.NotIn ->
	  let annotation_function =
	    match atom_op with
	    | Bbop.NotEqual -> type_annotation_eq
	    | Bbop.NotSubSet
	    | Bbop.NotStrictSubSet -> type_annotation_subset
	    | Bbop.NotIn -> type_annotation_in
	    | _ -> assert false
	  in
	  (* Here we use the negatively yet untyped variables *)
	  let full_tyenv =
	    List.fold_left
	      (fun tenv p -> Typing_env.add tenv (p, Untyped))
	      tyenv
	      neg
	  in
	  let (tyenv', expr1', expr2') =
	    annotation_function full_tyenv expr1 expr2
	  in
	  (PredAtom(atom_op, expr1', expr2'),
	   tyenv')
       | _ -> invalid_arg "type_pred: malformed predicate (PredAtom)"
     end
  | SubstApply(_) ->
     invalid_arg "type_pred:SubstApply: irrelevant"

and type_annotation_in tyenv expr1 expr2 =
  let (expr2', e2type) = type_expr (Typing_env.no_untyped tyenv) expr2 in
  match expr1 with
  | ExprId(id) ->
     (type_id_in tyenv id e2type,
      expr1,
      expr2')
  | ExprNuplet(exprs) ->
     (* expr2 thus has a cartesian product type *)
     let e2_separate_types = types_of_prod e2type in
     let rec match_in_types sub_exprs set_types updated_env rev_updated_nuplet =
       match (sub_exprs, set_types) with
       | ([], []) -> (updated_env, List.rev rev_updated_nuplet)
       | ([], _)
       | (_, []) -> raise (ArityError(expr1, expr2))
       | (sub_expr::other_exprs, set_type::other_types) ->
	  begin
	    (* Now sub_expr is supposed to be "in" sub_type *)
	    match sub_expr with
	    | ExprId(id) ->
	       (* annotating case, but in a n-uple (does
			     not appear in the refered paper,
			     though *)
	       let new_tyenv = type_id_in updated_env id set_type in
	       match_in_types other_exprs other_types new_tyenv (sub_expr::rev_updated_nuplet)
	    (* FIXME: ExprParen interferes here,
				 invalidating a legitimate type
				 inference *)
	    | not_an_exprid -> (* typechecking case, in a n-uple *)
	       let (not_exprid', not_exprid_type) = type_expr tyenv not_an_exprid in
	       let inner_set_type =
		 try inner_type set_type
		 (* FIXME: No way to give more
				 information for the error at the
				 moment *)
		 with (NotPowerSet(_) as exc) -> raise exc
	       in
	       if types_correspond not_exprid_type inner_set_type
	       then match_in_types other_exprs other_types updated_env (not_exprid'::rev_updated_nuplet)
	       else raise (TypeMismatchExpr(not_an_exprid, not_exprid_type, inner_set_type))
	  end
     in
     let (tyenv', nuples) = match_in_types exprs e2_separate_types tyenv [] in
     (tyenv',
      ExprNuplet(nuples),
      expr2')
  (* FIXME: ExprParen interferes here, invalidating a legitimate type inference *)
  | _ ->
     let (expr1', e1type) = type_expr tyenv expr1 in
     let e2_elt_type =
       try inner_type e2type
       (* FIXME: No way to give more information for the error at the moment *)
       with (NotPowerSet(_) as exc) -> raise exc
     in
     if types_correspond e1type e2_elt_type
     then (tyenv, expr1', expr2')
     else raise (TypeMismatchExpr(expr1, e1type, e2_elt_type))

and type_annotation_subset tyenv expr1 expr2 =
  let (expr2', e2type) = type_expr tyenv expr2 in
  let () = if not (check_is_set e2type) then invalid_arg "type_annotation_subset: right-hand side not a set" in
  match expr1 with
  (* FIXME: ExprParen interferes here, invalidating a legitimate type inference *)
  | ExprId(id) ->
     (type_id_included tyenv id e2type,
      expr1,
      expr2')
  | _ ->
     let (expr1', e1type) = type_expr tyenv expr1 in
     if types_correspond e1type e2type
     then (tyenv, expr1', expr2')
     else raise (TypeMismatchExpr(expr1, e1type, e2type))

and type_annotation_eq tyenv expr1 expr2 =
  let (expr1', e1type) = type_expr tyenv expr1
  and (expr2', e2type) = type_expr tyenv expr2 in
  match (expr1', expr2') with
  | ExprId(id1), ExprId(id2) ->
     if e1type = Untyped then
       if e2type = Untyped then raise (UntypedId(id2))
       else (type_id_equal tyenv id1 e2type,
	     expr1,
	     expr2)
     else
       if e2type = Untyped then
	 (type_id_equal tyenv id2 e1type,
	  expr1,
	  expr2)
       else
	 if types_correspond e1type e2type
	 then (tyenv, expr1, expr2)
	 else raise (TypeMismatch(path_of_id id1, e1type, e2type))
  (* FIXME: ExprParen interferes here, invalidating a legitimate type inference *)
  | ExprId(id1), _ ->
     if e1type = Untyped then
       if e2type = Untyped then raise (UntypedExpr(expr2))
       else (type_id_equal tyenv id1 e2type,
	     expr1,
	     expr2)
     else
       if types_correspond e1type e2type
       then (tyenv, expr1, expr2')
       else raise (TypeMismatch(path_of_id id1, e1type, e2type))
  | _, ExprId(id2) ->
     if e2type = Untyped then
       if e1type = Untyped then raise (UntypedExpr(expr1))
       else (type_id_equal tyenv id2 e1type,
	     expr1',
	     expr2)
     else
       if types_correspond e1type e2type
       then (tyenv, expr1', expr2)
       else raise (TypeMismatch(path_of_id id2, e2type, e1type))
  | _, _ ->
     if types_correspond e1type e2type
     then (tyenv, expr1', expr2')
     else raise (TypeMismatchExpr(expr1, e1type, e2type))

(* Note: [type_id] removes all local type information (but still
   checks it). Moreover, type_id does not try to be smart: whether a
   predicate is an annotation is only visible at the level of the
   predicate, not here. *)
and type_id tyenv x =
  match x with
  | Id(path) ->
     begin
       try (x, Typing_env.assoc tyenv path)
       with Not_found ->
	 invalid_arg ("type_id:"
		      ^ (Id_acc.name_of x)
		      ^ " not in typing environment")
     end
  |IdTy(i,ty) ->
    let (j, ty_found) = type_id tyenv i in
    if types_correspond ty ty_found then (j, ty_found)
    else invalid_arg ("type_id:"
		      ^ (Id_acc.name_of x)
		      ^ "locally typed in an incompatible way")

and locally_type_id tyenv x =
  let (x', x_type) = type_id tyenv x in
  IdTy(x', x_type)

and type_ids tyenv ids = List.map (type_id tyenv) ids

and locally_type_ids tyenv ids = List.map (locally_type_id tyenv) ids

and type_expr tyenv = function
  | ExprParen(e) ->
     let (e', etype) = type_expr tyenv e in
     (ExprParen(e'), etype)

  (* Note: the following code should be changed, would we want to
     annotate all ids, not only those at binding locations *)

  | ExprId(id) ->
     let (x, ty_found) = type_id tyenv id in
     (ExprId(x), ty_found)
  | ExprFunCall(efun, expr) ->
     let (efun', efuntype) = type_expr tyenv efun
     and (expr', exprtype) = type_expr tyenv expr in
     begin
       match (super efuntype) with
       | TypePowerset(TypeProd(ty1, ty2)) ->
	  if types_correspond ty1 exprtype
	  then (ExprFunCall(efun',expr'), ty2)
	  else raise (TypeMismatchExpr(expr', exprtype, ty1))
       | _ -> raise (NotFunType(efun, efuntype))
     end

  | ExprBin(binop, e1, e2) ->
     type_ExprBin tyenv binop e1 e2
  | ExprUn(unop, e1) ->
     type_ExprUn tyenv unop e1
  | ExprTrin(_,_,_,_)
  | ExprRecords(_)
  | ExprBefore(_)
  | ExprSeq(_)
  | ExprSet(_)
  | ExprNumber(_)
  | ExprString(_)
  | ExprNuplet(_)
  | ExprSIGMA(_, _, _)
  | ExprPI(_, _, _)
  | ExprLambda(_, _, _)
  | ExprBool(_)
  | ExprBoolConstant(_)-> invalid_arg "In being implemented"


(*
  FIXME: do for these expressions
  | ExprBefore of id
  | ExprTrin of Bbop.op3 * expr * expr * expr
  | ExprSeq of exprSeq
  | ExprSet of exprSet
  | ExprNumber of number
  | ExprString of string
  | ExprRecords of exprRecords
  | ExprNuplet of expr list
  | ExprSIGMA of id list * predicate * expr
  | ExprPI of id list * predicate * expr
  | ExprLambda of id list * predicate * expr
  | ExprBool of predicate (* bool() *)
  | ExprBoolConstant of boolConstant
 *)

and type_ExprBin tyenv binop e1 e2 =
  let (e1', e1type) = type_expr tyenv e1
  and (e2', e2type) = type_expr tyenv e2 in
  match binop with
  (* This is the only place where syntax disambiguation can be done for these operators *)
  | Bbop.Minus ->
     if types_correspond e1type (TypeBaseSet(INTEGER))
     then
       if types_correspond e1type e2type
       then (ExprBin(binop, e1', e2'), TypeBaseSet(INTEGER))
       else raise (TypeMismatchExpr(e2, e2type, e1type))
     else
       if check_is_set e1type then (* It might actually be set difference *)
	 if types_correspond e1type e2type
	 then (ExprBin(Bbop.SetMinus, e1', e2'), e1type)
	 else raise (TypeMismatchExpr(e2, e2type, e1type))
       else raise (TypeMismatchExpr(e1, e1type, TypeBaseSet(INTEGER)))
  | Bbop.Mul ->
     if types_correspond e1type (TypeBaseSet(INTEGER))
     then
       if types_correspond e1type e2type
       then (ExprBin(binop, e1', e2'), TypeBaseSet(INTEGER))
       else raise (TypeMismatchExpr(e2, e2type, e1type))
     else
       if check_is_set e1type then (* It might actually be set product *)
	 if check_is_set e2type
	 then (ExprBin(Bbop.CartesianProd, e1', e2'),
	       TypePowerset(TypeProd(inner_type e1type, inner_type e2type)))
	 else raise (TypeMismatchExpr(e2, e2type, e1type))
       else raise (TypeMismatchExpr(e1, e1type, TypeBaseSet(INTEGER)))
  | Bbop.Puissance
  | Bbop.Div
  | Bbop.Mod
  | Bbop.Plus ->
     if types_correspond e1type (TypeBaseSet(INTEGER))
     then
       if types_correspond e2type (TypeBaseSet(INTEGER))
       then (ExprBin(binop, e1', e2'), TypeBaseSet(INTEGER))
       else raise (TypeMismatchExpr(e2, e2type, TypeBaseSet(INTEGER)))
     else raise (TypeMismatchExpr(e1, e1type, TypeBaseSet(INTEGER)))

  (* FIXME: better rename NatSetRange IntSetRange ? *)
  | Bbop.NatSetRange ->
     if types_correspond e1type (TypeBaseSet(INTEGER))
     then
       if types_correspond e2type (TypeBaseSet(INTEGER))
       then (ExprBin(binop, e1', e2'), TypeNatSetRange(e1',e2'))
       else raise (TypeMismatchExpr(e2, e2type, TypeBaseSet(INTEGER)))
     else raise (TypeMismatchExpr(e1, e1type, TypeBaseSet(INTEGER)))


  | Bbop.CartesianProd ->
     if check_is_set e1type then
       if check_is_set e2type
       then (ExprBin(binop, e1', e2'),
	     TypePowerset(TypeProd(inner_type e1type, inner_type e2type)))
       else raise (TypeMismatchExpr(e2, e2type, TypePowerset(e2type)))
     else
       if types_correspond e1type (TypeBaseSet(INTEGER))
       then
	 if types_correspond e1type e2type
	 (* This was actually integer multiplication *)
	 then (ExprBin(Bbop.Mul, e1', e2'), TypeBaseSet(INTEGER))
	 else raise (TypeMismatchExpr(e1, e1type, TypePowerset(e1type)))
       else raise (TypeMismatchExpr(e1, e1type, TypePowerset(e1type)))

  | Bbop.Relation ->
     begin
       match super e1type, super e2type with
       | TypePowerset(ty1), TypePowerset(ty2) ->
	  (ExprBin(binop, e1', e2'),
	   TypePowerset(TypePowerset(TypeProd(ty1, ty2))))
       | TypePowerset(_), _ ->
	  raise (TypeMismatchExpr(e2, e2type, TypePowerset(e2type)))
       | _, _ ->
	  raise (TypeMismatchExpr(e1, e1type, TypePowerset(e1type)))
     end

  | Bbop.PartialFunc
  | Bbop.PartialInj
  | Bbop.PartialSurj
  | Bbop.PartialBij ->
     begin
       match super e1type, super e2type with
       | TypePowerset(ty1), TypePowerset(ty2) ->
	  (ExprBin(binop, e1', e2'),
	   TypePowerset(TypePFun(ty1, ty2)))
       | TypePowerset(_), _ ->
	  raise (TypeMismatchExpr(e2, e2type, TypePowerset(e2type)))
       | _, _ ->
	  raise (TypeMismatchExpr(e1, e1type, TypePowerset(e1type)))
     end
  | Bbop.TotalFunc
  | Bbop.TotalInj
  | Bbop.TotalSurj
  | Bbop.TotalBij ->
     begin
       match super e1type, super e2type with
       | TypePowerset(ty1), TypePowerset(ty2) ->
	  (ExprBin(binop, e1', e2'),
	   TypePowerset(TypeFun(ty1, ty2)))
       | TypePowerset(_), _ ->
	  raise (TypeMismatchExpr(e2, e2type, TypePowerset(e2type)))
       | _, _ ->
	  raise (TypeMismatchExpr(e1, e1type, TypePowerset(e1type)))
     end

  | Bbop.OverRide ->
     begin
       match super e1type, super e2type with
       | TypePowerset(TypeProd(_,_)), TypePowerset(_) ->
	  if types_correspond e1type e2type
	  then (ExprBin(binop, e1', e2'),
		e1type)
	  else raise (TypeMismatchExpr(e2, e2type, e1type))
       | _, _ ->
	  raise (TypeMismatchExpr(e2, e2type, e1type))
     end

  | Bbop.AppendSeq ->
     begin
       match super e1type, super e2type with
       | TypePowerset(TypeProd(TypeBaseSet(INTEGER),elty1)),
	 TypePowerset(elty2) ->
	  if types_correspond elty1 elty2
	  then (ExprBin(binop, e1', e2'),
		e1type)
	  else raise (TypeMismatchExpr(e2, e2type, elty1))
       | TypePowerset(TypeProd(TypeBaseSet(INTEGER),elty1)), _ ->
	  raise (TypeMismatchExpr(e2, e2type, elty1))
       | _, _ ->
	  raise (TypeMismatchExpr(e1, e1type, TypeSequence(e1type)))
     end


  | Bbop.PrependSeq ->
     begin
       match super e1type, super e2type with
       | TypePowerset(elty1),
	 TypePowerset(TypeProd(TypeBaseSet(INTEGER),elty2)) ->
	  if types_correspond elty1 elty2
	  then (ExprBin(binop, e1', e2'),
		e2type)
	  else raise (TypeMismatchExpr(e1, e1type, elty2))
       | _, TypePowerset(TypeProd(TypeBaseSet(INTEGER),elty2)) ->
	  raise (TypeMismatchExpr(e1, e1type, elty2))
       | _, _ ->
	  raise (TypeMismatchExpr(e2, e2type, TypeSequence(e2type)))
     end

  | Bbop.PrefixSeq
  | Bbop.SuffixSeq ->
     begin
       match super e1type, super e2type with
       | TypePowerset(TypeProd(TypeBaseSet(INTEGER),_)),
	 TypePowerset(TypeBaseSet(INTEGER)) ->
	  (ExprBin(binop, e1', e2'),
	   e1type)
       | TypePowerset(TypeProd(TypeBaseSet(INTEGER),_)), _ ->
	  raise (TypeMismatchExpr(e2, e2type,TypePowerset(TypeBaseSet(INTEGER))))
       | _, _ ->
	  raise (TypeMismatchExpr(e1, e1type, TypeSequence(e1type)))
     end

  | Bbop.ConcatSeq ->
     begin
       match super e1type, super e2type with
       | TypePowerset(TypeProd(TypeBaseSet(INTEGER),elty1)),
	 TypePowerset(TypeProd(TypeBaseSet(INTEGER),elty2)) ->
	  if types_correspond elty1 elty2
	  then (ExprBin(binop, e1', e2'),
		e1type)
	  else raise (TypeMismatchExpr(e2, e2type, TypeSequence(elty1)))
       | TypePowerset(TypeProd(TypeBaseSet(INTEGER),_)), _ ->
	  raise (TypeMismatchExpr(e2, e2type, e1type))
       | _, _ ->
	  raise (TypeMismatchExpr(e1, e1type, TypeSequence(e1type)))
     end

  | Bbop.DomRestrict
  | Bbop.DomSubstract ->
     begin
       match super e1type, super e2type with
       | TypePowerset(ty1), TypePowerset(TypeProd(ty2dom,_)) ->
	  if types_correspond ty1 ty2dom
	  then (ExprBin(binop, e1', e2'),
		e2type)
	  else raise (TypeMismatchExpr(e1, e1type, TypePowerset(ty2dom)))
       | _, TypePowerset(TypeProd(ty2dom,_)) ->
	  raise (TypeMismatchExpr(e1, e1type, TypePowerset(ty2dom)))
       | _, _ ->
	  raise (NotProductExpr(e2,e2type))
     end


  | Bbop.RanRestrict
  | Bbop.RanSubstract ->
     begin
       match super e1type, super e2type with
       | TypePowerset(TypeProd(_,ty1ran)), TypePowerset(ty2) ->
	  if types_correspond ty1ran ty2
	  then (ExprBin(binop, e1', e2'),
		e1type)
	  else raise (TypeMismatchExpr(e2, e2type, TypePowerset(ty1ran)))
       | TypePowerset(TypeProd(_,ty1ran)), _ ->
	  raise (TypeMismatchExpr(e2, e2type, TypePowerset(ty1ran)))
       | _, _ ->
	  raise (NotProductExpr(e1,e1type))
     end

  | Bbop.InterSets
  | Bbop.UnionSets ->
     if check_is_set e1type then
       if types_correspond e1type e2type
       (* FIXME: the choice of e1 is purely arbitrary here *)
       then (ExprBin(binop, e1', e2'), e1type)
       else raise (TypeMismatchExpr(e2, e2type, e1type))
     else raise (TypeMismatchExpr(e1, e1type, TypePowerset(e1type)))

  | Bbop.SetMinus ->
     if check_is_set e1type then
       if types_correspond e1type e2type
       then (ExprBin(binop, e1', e2'), e1type)
       else raise (TypeMismatchExpr(e2, e2type, e1type))
     else
       if types_correspond e1type (TypeBaseSet(INTEGER))
       then
	 if types_correspond e1type e2type
	 (* This was actually integer subtraction *)
	 then (ExprBin(Bbop.Minus, e1', e2'), TypeBaseSet(INTEGER))
	 else raise (TypeMismatchExpr(e1, e1type, TypePowerset(e1type)))
       else raise (TypeMismatchExpr(e1, e1type, TypePowerset(e1type)))

  | Bbop.DirectProd ->
     begin
       match super e1type, super e2type with
       | TypePowerset(TypeProd(domty1,ranty1)),
	 TypePowerset(TypeProd(domty2,ranty2)) ->
	  if types_correspond domty1 domty2
	  then (ExprBin(binop, e1', e2'),
		TypePowerset(TypeProd(domty1,(TypeProd(ranty1, ranty2)))))
	  else raise (TypeMismatchExpr(e2, e2type, TypePowerset(TypeProd(domty1,ranty2))))
       | TypePowerset(TypeProd(domty1,_)), _ ->
	  raise (TypeMismatchExpr(e2, e2type, TypePowerset(TypeProd(domty1,e2type))))
       | _, _ ->
	  raise (TypeMismatchExpr(e1, e1type, TypePowerset(TypeProd(e1type, e1type))))
     end

  | Bbop.Composition ->
     begin
       match super e1type, super e2type with
       | TypePowerset(TypeProd(domty1,ranty1)),
	 TypePowerset(TypeProd(domty2,ranty2)) ->
	  if types_correspond ranty1 domty2
	  then (ExprBin(binop, e1', e2'),
		TypePowerset(TypeProd(domty1, ranty2)))
	  else raise (TypeMismatchExpr(e2, e2type, TypePowerset(TypeProd(ranty1,ranty2))))
       | TypePowerset(TypeProd(_,ranty1)), _ ->
	  raise (TypeMismatchExpr(e2, e2type, TypePowerset(TypeProd(ranty1, e2type))))
       | _, _ ->
	  raise (TypeMismatchExpr(e1, e1type, TypePowerset(TypeProd(e1type, e1type))))
     end

  | Bbop.ParallelComp ->
     begin
       match super e1type, super e2type with
       | TypePowerset(TypeProd(domty1,ranty1)),
	 TypePowerset(TypeProd(domty2,ranty2)) ->
	  (ExprBin(binop, e1', e2'),
	   TypePowerset(TypeProd((TypeProd(domty1, domty2),
				  TypeProd(ranty1, ranty2)))) )
       | TypePowerset(TypeProd(_,_)), _ ->
	  raise (TypeMismatchExpr(e2, e2type, TypePowerset(TypeProd(e2type, e2type))))
       | _, _ ->
	  raise (TypeMismatchExpr(e1, e1type, TypePowerset(TypeProd(e1type, e1type))))
     end

  | Bbop.Image ->
     begin
       match super e1type with
       | TypePowerset(TypeProd(domty1,ranty1)) ->
	  if types_correspond domty1 e2type then
	    (ExprBin(binop, e1', e2'), TypePowerset(ranty1))
	  else raise (TypeMismatchExpr(e2, e2type, TypePowerset(domty1)))
       | TypePowerset(_) -> raise (NotProduct(e1type))
       | _ -> raise (NotPowerSet(e1type))
     end

  | Bbop.Prj1 ->
     begin
       if check_is_set e1type
       then if check_is_set e2type
	    then (ExprBin(binop, e1', e2'),
		  TypeFun(TypeProd(e1type, e2type), e1type))
	    else raise(NotSetExpr(e2,e2type))
       else raise(NotSetExpr(e1,e1type))
     end
  | Bbop.Prj2 ->
     begin
       if check_is_set e1type
       then if check_is_set e2type
	    then (ExprBin(binop, e1', e2'),
		  TypeFun(TypeProd(e1type, e2type), e2type))
	    else raise (NotSetExpr(e2,e2type))
       else raise (NotSetExpr(e1,e1type))
     end
  (* FIXME: const(elem, seq of trees) seems to be missing *)

  | Bbop.Rank ->
     if check_is_tree e1type
     then if check_is_seq e2type
	  then (ExprBin(binop, e1', e2'),
		TypeBaseSet(INTEGER))
	  else raise (NotSequenceExpr(e2, e2type))
     else raise (NotTreeExpr(e1, e1type))

  | Bbop.Father ->
     if check_is_tree e1type
     then if check_is_seq e2type
	  then (ExprBin(binop, e1', e2'),
		TypePowerset(TypeProd(TypeBaseSet(INTEGER),TypeBaseSet(INTEGER))))
	  else raise (NotSequenceExpr(e2, e2type))
     else raise (NotTreeExpr(e1, e1type))

  | Bbop.SubTree ->
     if check_is_tree e1type
     then if check_is_seq e2type
	  then (ExprBin(binop, e1', e2'),
		build_tree_type (tree_inner_type e1type))
	  else raise (NotSequenceExpr(e2, e2type))
     else raise (NotTreeExpr(e1, e1type))

  | Bbop.Arity ->
     if check_is_tree e1type
     then if check_is_seq e2type
	  then (ExprBin(binop, e1', e2'),
		TypeBaseSet(INTEGER))
	  else raise (NotSequenceExpr(e2, e2type))
     else raise (NotTreeExpr(e1, e1type))
  | Bbop.Consconst
  | Bbop.Iterate -> invalid_arg "In being implemented"

and type_ExprUn tyenv unop e1 =
  let (e1', e1type) = type_expr tyenv e1 in
  match unop with
  | Bbop.Closure
  | Bbop.ClosureOne ->
     begin
       match super e1type with
       | TypePowerset(TypeProd(ty1dom, ty1ran)) ->
	  if types_correspond ty1dom ty1ran
	  then (ExprUn(unop, e1'),
		e1type)
	  else raise (TypeMismatchExpr(e1, e1type, TypePowerset(TypeProd(ty1dom, ty1dom))))
       | TypePowerset(ty1) -> raise (TypeMismatchExpr(e1, e1type, TypePowerset(TypeProd(e1type, e1type))))
       | _ -> raise (NotPowerSetExpr(e1, e1type))
     end
  | Bbop.Seq
  | Bbop.SeqOne
  | Bbop.ISeq
  | Bbop.ISeqOne
  | Bbop.Perm ->
     begin
       match super e1type with
       | TypePowerset(TypePowerset(TypeProd(ty1dom, ty1ran))) ->
	  if types_correspond ty1dom (TypeBaseSet(INTEGER))
	  then (ExprUn(unop, e1'),
		e1type)
	  else raise (TypeMismatchExpr(e1, e1type, TypePowerset(TypePowerset(TypeProd(TypeBaseSet(INTEGER), ty1ran)))))
       | TypePowerset(TypePowerset(ty1)) ->
	  raise (TypeMismatchExpr(e1, e1type, TypePowerset(TypePowerset(TypeProd(TypeBaseSet(INTEGER), ty1)))))
       | TypePowerset(ty1) ->
	  raise (TypeMismatchExpr(e1, e1type, TypePowerset(TypePowerset(TypeProd(TypeBaseSet(INTEGER), ty1)))))
       | _ -> raise (NotPowerSetExpr(e1, e1type))
     end
  | Bbop.Min
  | Bbop.Max
  | Bbop.Card
  | Bbop.Pow
  | Bbop.PowOne
  | Bbop.Fin
  | Bbop.FinOne
  | Bbop.Identity
  | Bbop.Dom
  | Bbop.Ran
  | Bbop.UnionGen
  | Bbop.InterGen
  | Bbop.Size
  | Bbop.First
  | Bbop.Last
  | Bbop.Front
  | Bbop.Tail
  | Bbop.Rev
  | Bbop.Conc
  | Bbop.RelFnc
  | Bbop.FncRel
  | Bbop.Tree
  | Bbop.Btree
  | Bbop.Bin1 (* the unary shape of the bin() operator *)
  | Bbop.Left
  | Bbop.Right
  | Bbop.Infix
  | Bbop.Top
  | Bbop.Sons
  | Bbop.Prefix
  | Bbop.Postfix
  | Bbop.SizeT
  | Bbop.Mirror
  | Bbop.Bool
  | Bbop.Pred
  | Bbop.Succ
  | Bbop.Tilde
  | Bbop.UMinus -> invalid_arg "In being implemented"
  | Bbop.Cod -> invalid_arg "Cod is deprecated and should be removed soon"
