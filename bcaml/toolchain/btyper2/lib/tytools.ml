open Blast

(* The various exceptions for warning of a typechecking error *)

exception TypeMismatch of Modules.path * type_B * type_B
exception TypeEnvMismatch of Modules.path * type_B * type_B
exception TypeMismatchExpr of Blast.expr * type_B * type_B
exception NotSet of type_B
exception NotSetExpr of Blast.expr * type_B
exception NotPowerSet of type_B
exception NotPowerSetExpr of Blast.expr * type_B
exception NotFunType of Blast.expr * type_B
exception NotRelation of type_B
exception NotProduct of type_B
exception NotProductExpr of Blast.expr * type_B
exception NotSequence of type_B
exception NotSequenceExpr of Blast.expr * type_B
exception NotTree of type_B
exception NotTreeExpr of Blast.expr * type_B
exception PartiallyUntyped
exception UntypedId of Blast.id
exception UntypedExpr of Blast.expr
exception ArityError of Blast.expr * Blast.expr
exception IncompatibleTypes


  (* [super ty] calculates the maximal type of the passed type
     construct. It corresponds to the \uparrow operator of the
     Bodeveix-Filali paper *)
let rec super ty =
  match ty with
    | TypeNatSetRange _ -> TypeBaseSet(INTEGER)
    | TypeIdentifier i -> ty
    | TypeBaseSet s -> TypeBaseSet(super_setName s)
    | TypePFun(ty1,ty2) ->
	TypePowerset(TypeProd(super ty1, super ty2))
    | TypeFun(ty1,ty2) ->
	TypePowerset(TypeProd(super ty1, super ty2))
    | TypeSequence(ty) ->
	TypePowerset(TypeProd(TypeBaseSet(INTEGER), super ty))
    | TypeProd(ty1,ty2) ->
	TypeProd(super ty1, super ty2)
    | TypePowerset(t) -> TypePowerset(super t)
    | TypeRecords(fields) ->
       TypeRecords(List.map super_field fields)
    | Untyped -> ty
and super_setName s =
  match s with
    | NAT
    | NATURAL
    | NAT1
    | NATURAL1
    | INT
    | INTEGER -> INTEGER
    | STRING -> STRING
    | BOOL -> BOOL
and super_field (field, ty) =
  (field, super ty)


(* [compare_types ty1 ty2] tells if [ty1] and [ty2] are the same
   type. In the context of a typechecking algorithm, it should be
   combined with [super] *)
let rec compare_types ty1 ty2 =
  match ty1, ty2 with
    | TypeNatSetRange(x1,y1), TypeNatSetRange(x2,y2) -> x1 = x2 && y1 = y2
    | TypeIdentifier i1, TypeIdentifier i2 -> i1 = i2
    | TypeBaseSet(s1), TypeBaseSet(s2) -> s1 = s2
    | TypePFun(d1,r1), TypePFun(d2,r2) ->
	(compare_types d1 d2) && (compare_types r1 r2)
    | TypeFun(d1,r1), TypeFun(d2,r2) ->
	(compare_types d1 d2) && (compare_types r1 r2)
    | TypeSequence(r1), TypeSequence(r2)  ->
	(compare_types r1 r2)
    | TypeProd(s1,t1), TypeProd(s2,t2) ->
	(compare_types s1 s2) && (compare_types t1 t2)
    | TypePowerset(t1), TypePowerset(t2) ->
	(compare_types t1 t2)
	  (* This exception is then used for performing type unification *)
    | TypeRecords(fields1), TypeRecords(fields2) ->
       (* Beware : as we don't know if fields can be reordered, we
       must assume that. Hence we have to check that the same fields
       appear for both types ty1 and ty2 and that they have the same types *)
       begin
	 let is_among_fields fields (f, t) =
	   try
	     let found_t = List.assoc f fields in
	     compare_types t found_t
	   with
	     Not_found -> false
	 in
	 let included_1_2 = List.for_all (is_among_fields fields2) fields1 in
	 let included_2_1 = List.for_all (is_among_fields fields1) fields2 in
	 included_1_2 && included_2_1
       end
    | Untyped ,_
    | _, Untyped -> raise PartiallyUntyped
    | _ -> false

(* See the commentary of [compare_types] above *)
let types_correspond ty1 ty2 =
  compare_types (super ty1) (super ty2)

(* [smart_unify t1 t2] tries to merge type information from t1 and t2
   to obtain a more precise type. For instance, if x is already typed
   as an integer, but one encounter a "x : NATURAL" predicate, one
   would want to improve the type information of x. This is what this
   function does, also with functions and other type structures. Note
   that this function "absorbs" the Untyped type (a better name would
   be UnknownType). We make the assumption that [t1] might be the one
   for which to infer the type. *)

(*FIXME: rename Untyped UnknownType *)

let rec smart_unify t1 t2 =
  match t1 with
(* FIXME: TypeNatSetRange is a shifting type: make it parameterized by constants *)
    | TypeNatSetRange _ ->
	begin
	  match t2 with
(* FIXME: compare bounds, once they are fixed *)
	    | TypeNatSetRange _ -> t1
	    | TypeBaseSet s2 ->
		begin
		  match s2 with
		    | NAT
		    | NATURAL
		    | NAT1
		    | NATURAL1
		    | INT
		    | INTEGER -> t1
		    | STRING
		    | BOOL -> raise IncompatibleTypes
		end
	    | Untyped -> t1
	    | _ -> raise IncompatibleTypes
	end
    | TypeIdentifier i ->
	begin
	  match t2 with
	    | TypeIdentifier j ->
		if i=j
		then TypeIdentifier(i)
		else raise IncompatibleTypes
	    | _ -> raise IncompatibleTypes
	end
    | TypeBaseSet s1 ->
	begin
	  match t2 with
	    | TypeNatSetRange _ ->
		begin
		  match s1 with
		    | NAT
		    | NATURAL
		    | NAT1
		    | NATURAL1
		    | INT
		    | INTEGER -> t2
		    | STRING
		    | BOOL -> raise IncompatibleTypes
		end
	    | TypeBaseSet s2 ->
		TypeBaseSet(smart_unify_base_sets s1 s2)
	    | Untyped -> t1
	    | _ -> raise IncompatibleTypes
	end
    | TypePFun(dom1,ran1) ->
	begin
	  match t2 with
	    | TypePFun(dom2,ran2) ->
		TypePFun(smart_unify dom1 dom2,
			 smart_unify ran1 ran2)
	    | TypeFun(dom2,ran2) ->
		TypeFun(smart_unify dom1 dom2,
			smart_unify ran1 ran2)
	    | TypeSequence(ty) ->
(* TODO: is sequence really more precise ? *)
		if types_correspond dom1 (TypeBaseSet(INTEGER))
		then TypeSequence(smart_unify ran1 ty)
		else raise IncompatibleTypes
	    | TypePowerset(ty2) ->
		begin
		  match ty2 with
		    | TypeProd(dom2,ran2) ->
			TypePFun(smart_unify dom1 dom2,
				 smart_unify ran1 ran2)
		    | _ -> raise IncompatibleTypes
		end
	    | Untyped -> t1
	    | _ -> raise IncompatibleTypes
	end
    | TypeFun(dom1,ran1) ->
	begin
	  match t2 with
	    | TypePFun(dom2,ran2) ->
		TypeFun(smart_unify dom1 dom2,
			smart_unify ran1 ran2)
	    | TypeFun(dom2,ran2) ->
		TypeFun(smart_unify dom1 dom2,
			smart_unify ran1 ran2)
	    | TypeSequence(ran2) ->
(* TODO: is sequence really more precise ? *)
		if types_correspond dom1 (TypeBaseSet(INTEGER))
		then TypeSequence(smart_unify ran1 ran2)
		else raise IncompatibleTypes
	    | TypePowerset(t) ->
		begin
		  match t with
		    | TypeProd(dom2,ran2) ->
			TypeFun(smart_unify dom1 dom2,
				smart_unify ran1 ran2)
		    | _ -> raise IncompatibleTypes
		end
	    | Untyped -> t1
	    | _ -> raise IncompatibleTypes
	end
    | TypeSequence(ran1) ->
	begin
	  match t2 with
	    | TypePFun(dom2,ran2)
	    | TypeFun(dom2,ran2) ->
		if types_correspond dom2 (TypeBaseSet(INTEGER))
		then TypeSequence(smart_unify ran1 ran2)
		else raise IncompatibleTypes
	    | TypeSequence(ran2) ->
		TypeSequence(smart_unify ran1 ran2)
	    | TypePowerset(ty2) ->
		begin
		  match ty2 with
		    | TypeProd(dom2,ran2) ->
			if types_correspond dom2 (TypeBaseSet(INTEGER))
			then TypeSequence(smart_unify ran1 ran2)
			else raise IncompatibleTypes
		    | _ -> raise IncompatibleTypes
		end
	    | Untyped -> t1
	    | _ -> raise IncompatibleTypes
	end
   | TypeProd(dom1,ran1) ->
	begin
	  match t2 with
	    | TypeProd(dom2,ran2) ->
		TypeProd(smart_unify dom1 dom2,
			 smart_unify ran1 ran2)
	    | Untyped -> t1
	    | _ -> raise IncompatibleTypes
	end
    | TypePowerset(ty1) ->
	begin
	  match t2 with
	    | TypePFun(dom2,ran2) ->
		begin
		  match ty1 with
		    | TypeProd(dom1,ran1) ->
			TypePFun(smart_unify dom1 dom2,
				 smart_unify dom1 ran2)
		    | _ -> raise IncompatibleTypes
		end
	    | TypeFun(dom2,ran2) ->
		begin
		  match ty1 with
		    | TypeProd(dom1,ran1) ->
			TypeFun(smart_unify dom1 dom2,
				smart_unify dom1 ran2)
		    | _ -> raise IncompatibleTypes
		end
	    | TypeSequence(ran2) ->
		begin
		  match ty1 with
		    | TypeProd(dom1,ran1) ->
			if types_correspond dom1 (TypeBaseSet(INTEGER))
			then TypeSequence(smart_unify ran1 ran2)
			else raise IncompatibleTypes
		    | _ -> raise IncompatibleTypes
		end
	    | TypePowerset(ty2) ->
		TypePowerset(smart_unify ty1 ty2)
	    | Untyped -> t1
	    | _ -> raise IncompatibleTypes
	end
    | TypeRecords(fields1) ->
       begin
	 match t2 with
	 | TypeRecords(fields2) ->
	    let same_fields =
	      begin
		let is_among_fields fields (f, _) = List.mem_assoc f fields in
		let included_1_2 = List.for_all (is_among_fields fields2) fields1 in
		let included_2_1 = List.for_all (is_among_fields fields1) fields2 in
		included_1_2 && included_2_1
	      end
	    in
	    if same_fields then
	      let unify_field fields (f, t) = (f, smart_unify t (List.assoc f fields)) in
	      TypeRecords(List.map (unify_field fields2) fields1)
	    else raise IncompatibleTypes
	 | Untyped -> t1
	 | _ -> raise IncompatibleTypes
       end
    | Untyped -> t2
and smart_unify_base_sets s1 s2 =
  match s1 with
    | NAT ->
	begin
	  match s2 with
	    | NAT -> s1
	    | NATURAL -> s1
	    | NAT1 -> s2
	    | NATURAL1 -> NAT1 (* the intersection of NAT and NATURAL1 *)
	    | INT -> s1
	    | INTEGER -> s1
	    | STRING
	    | BOOL -> raise IncompatibleTypes
	end
    | NATURAL ->
	begin
	  match s2 with
	    | NAT -> s2
	    | NATURAL -> s1
	    | NAT1 -> s2
	    | NATURAL1 -> s2
	    | INT -> NAT
	    | INTEGER -> s1
	    | STRING
	    | BOOL -> raise IncompatibleTypes
	end
    | NAT1 ->
	begin
	  match s2 with
	    | NAT
	    | NATURAL
	    | NAT1
	    | NATURAL1
	    | INT
	    | INTEGER -> s1
	    | STRING
	    | BOOL -> raise IncompatibleTypes
	end
    | NATURAL1 ->
	begin
	  match s2 with
	    | NAT -> NAT1
	    | NATURAL -> s1
	    | NAT1 -> s2
	    | NATURAL1 -> s1
	    | INT -> NAT1
	    | INTEGER -> s1
	    | STRING
	    | BOOL -> raise IncompatibleTypes
	end
    | INT ->
	begin
	  match s2 with
	    | NAT -> s2
	    | NATURAL -> NAT
	    | NAT1 -> s2
	    | NATURAL1 -> NAT1
	    | INT -> s1
	    | INTEGER -> s1
	    | STRING
	    | BOOL -> raise IncompatibleTypes
	end
    | INTEGER ->
	begin
	  match s2 with
	    | NAT
	    | NATURAL
	    | NAT1
	    | NATURAL1
	    | INT
	    | INTEGER -> s2
	    | STRING
	    | BOOL -> raise IncompatibleTypes
	end
    | STRING ->
	begin
	  match s2 with
	    | STRING -> s1
	    | NAT
	    | NATURAL
	    | NAT1
	    | NATURAL1
	    | INT
	    | INTEGER
	    | BOOL -> raise IncompatibleTypes
	end
    | BOOL ->
	begin
	  match s2 with
	    | BOOL -> s1
	    | NAT
	    | NATURAL
	    | NAT1
	    | NATURAL1
	    | INT
	    | INTEGER
	    | STRING -> raise IncompatibleTypes
	end




(* Here follows many small but useful functions, for making the
   algorithms more readable *)

let check_is_set ty =
  match ty with
    | TypePFun(_)
    | TypeFun(_)
    | TypeSequence(_)
    | TypePowerset(_) -> true
    | Untyped -> raise PartiallyUntyped
    | _ -> false

let inner_type ty =
  match ty with
    | TypePFun(t1,t2) -> TypeProd(t1,t2)
    | TypeFun(t1,t2) -> TypeProd(t1,t2)
    | TypeSequence(t) -> TypeProd(TypeBaseSet(Blast.INTEGER), t)
    | TypePowerset(t) -> t
    | Untyped -> raise PartiallyUntyped
    | _ -> raise (NotPowerSet(ty))

let get_dom ty =
  match ty with
    | TypePFun(t1,t2) -> t1
    | TypeFun(t1,t2) -> t1
    | TypeSequence(t) -> TypeBaseSet(Blast.INTEGER)
    | TypePowerset(TypeProd(t1,_)) -> t1
    | Untyped -> raise PartiallyUntyped
    | _ -> raise (NotRelation(ty))

let get_ran ty =
  match ty with
    | TypePFun(t1,t2) -> t2
    | TypeFun(t1,t2) -> t2
    | TypeSequence(t) -> t
    | TypePowerset(TypeProd(_,t2)) -> t2
    | Untyped -> raise PartiallyUntyped
    | _ -> raise (NotRelation(ty))

let is_integer_subset ty =
  match ty with
    | TypeNatSetRange(_)
    | TypeBaseSet(NAT)
    | TypeBaseSet(NATURAL)
    | TypeBaseSet(NAT1)
    | TypeBaseSet(NATURAL1)
    | TypeBaseSet(INT)
    | TypeBaseSet(INTEGER) -> true
    | _ -> false

(* All the expressions corresponding to the following subtypes might
   be used as sequences. This distinction is a bit useless for
   checking ("super ty" could be used) but for the extraction function
   it avoids losing too much precise type information. *)
let check_is_seq ty =
  match ty with
    | TypeSequence(t) -> true
    | TypePFun(t,_) when is_integer_subset t -> true
    | TypeFun(t,_) when is_integer_subset t -> true
    | TypePowerset(TypeProd(t,_)) when is_integer_subset t -> true
    | Untyped -> raise PartiallyUntyped
    | _ -> false

let seq_inner_type ty =
  match ty with
    | TypeSequence(t) -> t
    | TypePFun(t1,t2) when is_integer_subset t1 -> t2
    | TypeFun(t1,t2) when is_integer_subset t1 -> t2
    | TypePowerset(TypeProd(t1,t2)) when is_integer_subset t1 -> t2
    | Untyped -> raise PartiallyUntyped
    | _ -> raise (NotSequence(ty))

let build_super_seq_type ty =
  TypePowerset(TypeProd(TypeBaseSet(INTEGER),ty))

let build_seq_type ty=
  TypeSequence(ty)

let check_is_tree ty =
  match (super ty) with
    | TypePowerset(TypeProd(
		     TypePowerset(TypeProd(
				    TypeBaseSet(INTEGER),
				    TypeBaseSet(INTEGER)
				  )),
		     _))
      -> true
    | Untyped -> raise PartiallyUntyped
    | _ -> false

let has_tree_path_type ty =
  match (super ty) with
    | TypePowerset(TypeProd(TypeBaseSet(INTEGER), TypeBaseSet(INTEGER) )) ->
	true
    | TypePowerset(TypeProd(TypeBaseSet(INTEGER), Untyped ))
    | TypePowerset(TypeProd(Untyped, TypeBaseSet(INTEGER) ))
    | TypePowerset(TypeProd(Untyped, Untyped))
    | TypePowerset(Untyped)
    | Untyped -> raise PartiallyUntyped
    | _ -> false


let tree_inner_type ty =
  match ty with
    | TypePFun(t1,t2) when has_tree_path_type t1 -> t2
    | TypeFun(t1,t2) when has_tree_path_type t1 -> t2
    | TypePowerset(TypeProd(t1,t2)) when has_tree_path_type t1 -> t2
    | Untyped -> raise PartiallyUntyped
    | _ -> raise (NotTree(ty))

let build_tree_type ty =
  TypePowerset(TypeProd(
		 TypePowerset(TypeProd(
				TypeBaseSet(INTEGER),
				TypeBaseSet(INTEGER)
			      )),
		 ty))


(* FIXME: should be in Id_acc *)
let rec path_of_id = function
  | Id(i) -> i
  | IdTy(i, ty) -> path_of_id i

let path_of_ids = List.map path_of_id

(* Gives a way of comparing two unique identifiers *)
(* FIXME: should be in Id_acc *)
let rec compare_paths p1 p2 =
  match p1, p2 with
    | Modules.Pident(i1), Modules.Pident(i2) ->
	Pervasives.compare (Modules.Ident.number i1) (Modules.Ident.number i2)
    | Modules.Pdot(sp1, s1), Modules.Pdot(sp2, s2) ->
	let subpath_comp = compare_paths sp1 sp2 in
	if  subpath_comp = 0 then Pervasives.compare s1 s2
	else subpath_comp
    | Modules.Pident(_), Modules.Pdot(_) -> -1
    | Modules.Pdot(_), Modules.Pident(_) -> 1


module Typing_env =
struct
  (* FIXME: this structure could be optimized later on by some form of
     association set, for instance. This should speed up type
     associations lookahead. The use of a hash table is discouraged
     because of in-place modifications. At the moment we use a sorted
     list, for simplicity, no double entry and some speed. *)

  type t = (Modules.path * Blast.type_B) list

  let empty = []

  let unsafe_add tyenv elt = elt::tyenv

  let rec add tyenv (key, ty) =
    match tyenv with
      | [] -> []
      | (x,t)::tyenv' ->
	  if compare_paths key x < 0 then (x,t)::(add tyenv' (key, ty))
	  else if compare_paths key x > 0 then (key,ty)::tyenv
	  else if types_correspond ty t then tyenv
	  else raise(TypeEnvMismatch(key, ty, t))

  (* Removes the entry with the provided key. As the list is sorted,
     the function stops as soon as possible *)
  let rec remove tyenv key =
    match tyenv with
      | [] -> []
      | (x,t)::tyenv' ->
	  if compare_paths key x < 0 then (x,t)::(remove tyenv' key)
	  else if compare_paths key x > 0 then tyenv
	  else tyenv

  let rec replace tyenv (key, ty) =
    match tyenv with
      | [] -> raise Not_found
      | (x,t)::tyenv' ->
	  if compare_paths key x < 0 then (x,t)::(add tyenv' (key, ty))
	  else if compare_paths key x > 0 then tyenv
	  else (key, ty)::tyenv'

  let assoc tyenv key = List.assoc key tyenv

  let mem tyenv key = List.mem_assoc key tyenv

  let mem_typed tyenv key =
    try
      let its_type =
	assoc tyenv key
      in
	match its_type with
	  | Untyped -> false
	  | _ -> true
    with
	Not_found -> false

  let no_untyped tyenv =
    List.filter (fun (_,ty) -> ty <> Untyped) tyenv

end

let remove_typed_from_untyped_list tyenv pid_list =
  let rec aux_remove_typed_untyped acc left_paths =
    match left_paths with
      | [] -> List.rev acc
      | pid::other_paths ->
	  if Typing_env.mem_typed tyenv pid
	  then aux_remove_typed_untyped acc other_paths
	  else aux_remove_typed_untyped (pid::acc) other_paths
  in
    aux_remove_typed_untyped [] pid_list

(* Tells if the type structure is not totally specified. It might be
   useful because of the polymorphism of the empty set, that makes one
   build a structure with an untyped part *)
let rec has_untyped_part ty =
  match ty with
    | TypeNatSetRange _ -> false
    | TypeIdentifier i -> false
    | TypeBaseSet s -> false
    | TypePFun(ty1,ty2) ->
	(has_untyped_part ty1) || (has_untyped_part ty2)
    | TypeFun(ty1,ty2) ->
	(has_untyped_part ty1) || (has_untyped_part ty2)
    | TypeSequence(ty1) -> has_untyped_part ty1
    | TypeProd(ty1,ty2) ->
	(has_untyped_part ty1) || (has_untyped_part ty2)
    | TypePowerset(ty1) -> has_untyped_part ty1
    | TypeRecords(fields) ->
       List.exists (fun (f,t) -> has_untyped_part t) fields
    | Untyped -> true


