open Project
open Tytools
open Typing_new

(* Hash table of already typed machines *)
(* TODO: remove the explicit typing *)
let typecheckedRepository = Hashtbl.create 53


let real_type_check typing_context ast =
  { typed_machine = ast;
    type_env = Typing_env.empty
  }

(* Assumption : mchid is in the project *)
let rec auxtype_check project mchid =
  let _ = Error.debug ("State of the project : " ^ (Project.project_to_string project)) in
    if Hashtbl.mem typecheckedRepository mchid
    then
      let _ = Error.debug ("Found " ^ (Id_acc.name_of mchid) ^ " in the cache") in
	Hashtbl.find typecheckedRepository mchid
    else
      let dependedIds =	Bgraph.dependedMachines project.graph mchid in
      let typedDependedMchs = List.map (auxtype_check project) dependedIds in
      let mergedTypingContext = (* FIXME: not accurate (propose a function for merging typing contexts) *)
	List.flatten (List.map (fun te -> te.type_env) typedDependedMchs)
      in
      let mchcont = Project.find project mchid in
      let doneTyping = real_type_check mergedTypingContext (snd mchcont).machine in
      let () = Hashtbl.add typecheckedRepository mchid doneTyping in
	doneTyping


(* We must pass the machine name because sometimes it can be a refinement,
  which is not at the root of the project graph *)
let type_check (mch_name, project) =
  let (mchid, mchcontext) = Project.find_name project mch_name in
  let te = auxtype_check project mchid in
    te.typed_machine
