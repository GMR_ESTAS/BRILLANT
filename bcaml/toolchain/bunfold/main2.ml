(* $Id$ *)
 
(* SC: un odieux copier/coller du bparser :-) *)
  

open Project
  
(* 
   Variables <<d'environnements>> et fonctions utiles pour les options 
*)
    
let output_directory = 
  try
    ref (Sys.getenv "BCAML_DEST")
  with
    Not_found -> ref("./")
        
        
let to_stdout = ref true

let dest_set path = 
  output_directory := path; 
  to_stdout:=false
        
let bunfold_version = "0.35"
let usage_message = 
  "bunfold " ^ bunfold_version ^ " <filenames>"
  ^ "\n For each filename given on the command line, generates an unfolding of this filename"


let projectnames=ref []

let add_projectname str=
  Error.debug ("Adding to projectnames : " ^ str);
  projectnames:=str::(!projectnames)


let rec args = 
  [
   ("--help",
    Arg.Unit(fun () -> 
      Arg.usage args usage_message; raise Exit),
    "\t\t\t display this help")
     ;
   ("-help",
    Arg.Unit(fun () -> 
      Arg.usage args usage_message; raise Exit),
    "\t\t\t See --help")
     ;
   ("--dest",
    Arg.String(dest_set),
    "<dir>,  \t\t output the generated files in directory <dir> (deactivates output to stdout)")
     ;
   ("--debug",
    Arg.Unit(Error.debug_set),
    ", \t\t\t debug mode (default to false)")
     ;
]
    

let save_ast dir ast=
  let mchname=Id_acc.name_of (Blast_acc.get_name ast) in
  let filename=dir ^ "/" ^ mchname ^ "." ^ Bfile.xml_extension in
    Bfile.mkpath dir;
    let out=open_out filename in
      try
	Bxml.to_chan out ast;
	close_out out
      with
	  err -> close_out out; raise err


let out_ast ast=
  if !to_stdout
  then Bxml.to_chan stdout ast
  else
    save_ast !output_directory ast


let do_parsing ()=
  let allprojgroups=List.map (Parser_lib.overParse []) !projectnames in
  let project_list=List.map Project.buildProject allprojgroups in
  let unfolded_projects=List.map Unfold2.unfold project_list in
    List.iter out_ast unfolded_projects

let _ = 
  try
    Arg.parse args add_projectname usage_message; 
    do_parsing ();
    print_string "\n"; 
    flush stdout 
(* Gui.process  files_list *)
  with Exit -> ()
