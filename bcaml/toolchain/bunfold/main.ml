(* $Id$ *)
  

open Unfold_main
open Modules
open Blast 
open Benv_basic
exception Not_B_file of string
    
exception Stop
    
    
let mylexer_no_debug =   
  Blexer.token
    
let mylexer = ref mylexer_no_debug
    
let mylexer_debug lexbuf = 
  let t = Blexer.token lexbuf   in
    Error.debug "\n*** SC: debug.ml desactive ***\n";
    flush stdout; 
    t
        
(* 
   Variables <<d'environnements>> et fonctions utiles pour les options
*)
    
let output_directory = 
  try
    ref (Bfile.dest_check (Sys.getenv "BCAML_DEST"))
  with
    Not_found -> ref("./")
        
        
let dest_bool = ref false
    
let unfold = ref false
    
let unfold_mch_source = ref ""
    
let unfold_mch_target = ref ""
    
let ascii_dump = ref false
    
    
    
(* LaTeX controls *)
let latex_dump = ref false
    
let latex_set () = latex_dump := true
    
        
let verbose = ref false
    
let xml_dump = ref false
    
let depli_dump = ref false    
    
let tree_dump = ref false

let tree_set () = 
  tree_dump:=true
    
let dest_set path = 
  (output_directory := Bfile.dest_check path)
    ; dest_bool := true
        
        
let deps_build = ref false
    
let bdeps_output = ref "BDEPS.output"
    
let bdeps_set file = 
  deps_build := true
      ; bdeps_output := file
          
let unfold_name1 = ref ""
let unfold_name2 = ref ""

let unfoldall_bool = ref false
let unfoldall_node = ref ""
let unfoldall_set string = 
  unfoldall_node := string;
  unfoldall_bool := true;
  deps_build := true

let unfold_set stringe = 
  let arg1 = 
    String.sub stringe 0 (String.index stringe '@')
  in
  Error.debug ("argument 1 :"^arg1^"\n");
  let arg2 =
    String.sub stringe ((String.index stringe '@')+1) (String.length stringe - String.length arg1 - 1)
  in
  Error.debug ("argument 2 :"^arg2^"\n");
  unfold := true;
  deps_build := true;
  unfold_name1 := arg1;
  unfold_name2 := arg2

let unfold_set_source mch_name = 
  ( unfold_mch_source := mch_name )
    ; unfold := true
	; deps_build := true
            

let unfold_set_target mch_name = 
  unfold_mch_target := mch_name 
      
let ascii_set () = ascii_dump := true
    
    
let verbose_set () = verbose := true
    
let quiet_set () = verbose := false 
    
let xml_set () = xml_dump := true
    
let depli_set () = depli_dump := true    
    
let bparser_version = "0.33"
    
    
let rec args = 
  [
   ("--help",
    Arg.Unit(fun () -> 
      Arg.usage args ("Bparser "^bparser_version); raise Stop ),
    "\t\t\t display this help")
     ;
   ("--dest",
    Arg.String(dest_set),
    "<dir>,  \t\t output the generated files in directory <dir>")
     ;
   ("--debug",
    Arg.Unit(Error.debug_set),
    ", \t\t\t debug mode (default to false)")
     ;
   ("--verbose",
    Arg.Unit(verbose_set),
    ", \t\t\t display statistical information")
     ;
   ("--quiet",
    Arg.Unit(quiet_set),
    ", \t\t\t quiet version")
     ;
   ("--xml",
    Arg.Unit(xml_set),
    ", \t\t\t dumps  xml files")
     ;
   ("--depli",
    Arg.Unit(depli_set),
    ", \t\t\t v�rification des conditions d�pliage")
     ;
   ("--tree",
    Arg.Unit(tree_set),
    ", \t\t\t g�n�ration sch�ma PST")
     ;
   ("--unfold",
    Arg.String(unfold_set),
    "<name1@name2>, \t\t\t d�pliage de deux composants")
     ;
   ("--unfoldall",
    Arg.String(unfoldall_set),
    "nom du noeud, \t\t\t d�pliage de tout un projet")
     ;
 ]
    
    
(*MG pour le debugging *)
let met_de_cote file = 
  if Error.debug_mode () then 
    let l = Bfile.base_name file in
    let d = !output_directory^"XXX/spec/"^l in
    let cmd = "cp --force --backup "^file^" "^d in
    let _ = Error.debug cmd in
    ignore (Sys.command (cmd)) 
      ; 
    flush stdout
      ; 
    ()  
      
      
let parse_in_channel file cin =
  try
    let lexbuf = Lexing.from_channel cin      in
    let res = Bparser.amn !mylexer lexbuf     in
    (
     close_in cin 
       ; (Error.parsing_success file)  
       ; res 
    )
  with Blast.Parse_error(nb,msg) -> 
    let (string_error,line,col) = Linenum.for_position file nb  in
    let _ = met_de_cote file  in
    close_in cin
      ; (Error.parsing_failure file line col msg) 
      ; Blast.EmptyTree 
      (*    |	_ -> close_in cin ; raise Parsing.Parse_error*)
      
      
let parse_string str =
  try  
    let lexbuf = Lexing.from_string str in
    (Bparser.amn !mylexer lexbuf)
  with _ -> raise (Invalid_argument "analyse")
      
      
let analyse str = 
  try  
    let cin = 
      if Bfile.isBfile str
      then open_in str
      else raise (Not_B_file str)
    in
    let res = parse_in_channel str cin    in 
    (str,res)
  with Sys_error s ->  
    ("",parse_string str)
  | Parsing.Parse_error -> 
      Error.message ("\n --Bwarn "^"[?not parsed?]  "^"<"^str^">");
      (str,Blast.EmptyTree)
  | Not_B_file str -> 
      (str,Blast.EmptyTree)
        
let do_mkdir_all = ref true

let mkdir_all (filename,ast) =
  if !do_mkdir_all then
    let dir =  !output_directory^(Bfile.project filename)  in
    let filename_out = dir^"/spec/"^(Filename.basename filename)   in
    (
     Bfile.simpleMkdir dir
       ; Bfile.simpleMkdir (dir^"/spec") 
       ; Bfile.simpleMkdir (dir^"/tex") 
       ; Bfile.simpleMkdir (dir^"/lang") 
       ; Bfile.simpleMkdir (dir^"/bdp") 
       ; Bfile.simpleMkdir (dir^"/xml")
       ; Bfile.simpleMkdir (dir^"/unfold")
       ; do_mkdir_all := false
  )
       
let files_list = ref []

let xml_processing (filename,ast) =
  Error.debug "xml_processing starting ..."
    ;
  if !xml_dump
  then   
    begin
      mkdir_all  (filename,ast)
	;
      let r = (Bxml.amn ast) in
      let r1 = (Bxml.doc r) in
      let dir = !output_directory
(*DP   (Bfile.project filename)^  *)
      in
      let output_file = dir ^(Filename.basename filename)^".xml" in
      let chan = open_out output_file in
      let linelen = 80 in 
      (Pp.ppToFile chan linelen (Ppxml.ppDoc r1)
	 ;
       close_out chan ) 
    end

let unfold_xml_processing (filename,ast) dir =
  begin
    (*    
       mkdir_all  (filename,ast);
     *)
    let r = (Bxml.amn ast) in
    let r1 = (Bxml.doc r) in
    (*
       let dir = !output_directory^(Bfile.project filename)^"/unfold/xml/" in
       let output_file = dir^(Filename.basename filename)^".xml" in
     *)
      (* YP let output_file = (dir^"xml/"^(Filename.basename filename)^".xml") in*)
    let output_file = (dir^(Filename.basename filename)^".xml") in
    let chan = open_out output_file in
    let linelen = 80 in
    (Pp.ppToFile chan linelen (Ppxml.ppDoc r1)
       ;
     close_out chan )
  end

(* Processing of graph dependencies *)

let bgraph = ref Bgraph_old.emptyGraph
    
let deps_processing ast =
  if (!deps_build or !depli_dump)
  then bgraph := Bgraph_old.append !bgraph (Bgraph_old.build ast)
      
let process_bdeps () =
  (
   match (!deps_build or !depli_dump) with
       true -> 
	 let graph_out = open_out ((!output_directory)^(!bdeps_output))    in
(* SC: 
   (Bgraph_old.dc_graph graph_out !bgraph); 
*)
	   Error.debug "\n*** SC: decompilation desactivee ***\n";
	   flush graph_out; 
	   close_out graph_out
   | false -> ()
  )
    
let process_file str = 
  let (filename,ast) as parsing_result = analyse str  in
  (
   (files_list := parsing_result::!files_list )
     ;
   (Error.debug (filename^"\n"))
     ;
   xml_processing parsing_result
     ;
   deps_processing ast;
  )
    
let process_verbose time_debut =
  (
   match !verbose with 
     true -> 
       let (echec,reussite) = 
         List.fold_left 
           (fun (e,r) (_,ast) -> 
	     match ast with
	       Blast.EmptyTree -> (e+1,r)
	     | _ -> (e,r+1))
           (0,0)
           !files_list
       in
       let time = flush stdout;Unix.times() in
       Error.message ("--Stat Nombre de fichiers analyses : "
		      ^(string_of_int (List.length !files_list))
		     )
	 ;
       Error.message ("--Stat Nombre d'echecs : "
		      ^(string_of_int echec)
		     )
	 ;
       Error.message ("--Stat Nombre de reussite : "
		      ^(string_of_int reussite)
		     )
	 ;
       Error.message ("\n--Stat Temps d'execution (system): "
		      ^(string_of_float (time.Unix.tms_stime ))
		     )
	 ;
       Error.message ("--Stat Temps d'execution (users): "
		      ^(string_of_float (time.Unix.tms_utime))
		     )
	 ;
       Error.message ("--Stat Temps d'execution (total): "
		      ^(string_of_float ((Unix.gettimeofday()) -. time_debut)))
	 ;
       Error.message ("\n");
   | false -> ()
  )
    
    
let bgraph_tt_fichier = ref Bgraph_old.emptyGraph
;;

let bdeps_output_tt_fichier = ref "./BDEPS_All_Project.output"
;;

let deps_processing_tt_fichier ast =
  if (!deps_build or !depli_dump)
  then bgraph_tt_fichier := Bgraph_old.append !bgraph_tt_fichier (Bgraph_old.build ast)
;;

let genere_liste_tous_fichier nom_fichier =
  let path = Filename.dirname nom_fichier in
  let liste_totale = Bfile.listOfDirectorie(Bfile.dest_check path) in
  let rec aux liste resultat =
    match liste with
    | [] -> resultat
    | l::reste ->
	begin
	  let (filename,ast) as parsing_result = analyse ((Bfile.dest_check path)^l) in
	  deps_processing_tt_fichier ast;
	  aux reste (parsing_result::resultat)
	end
  in
  aux liste_totale []
;;


let scope_info = ref []

let initialise_scope_env () =
    List.fold_left (fun env id -> Scope.enter_value (Ident.create id) env) Scope.empty Benv_basic.basic


let scope_env_basic = 
  initialise_scope_env ()

let scope_processing files_list graphe =
(*YP Debugage *)
    let _ = Error.debug ("\n\n****************** Scoping *****************\n\n") in
  let scope_component_type = ref "mch"
  in
  let scope_env = 
(*       ref (initialise_scope_env ()) *)
    ref []
  in
  let designe_source ast =
    match ast with
    | Machine(_,_) ->
	Error.debug ("Je suis le composant : "^(Id_acc.name_of (Blast_acc.get_name ast))^" et je pars de  : "^(Id_acc.name_of (Blast_acc.get_name ast)));
	Id_acc.name_of (Blast_acc.get_name ast)
    | EmptyTree ->
	Error.error "Erreur dans le scoping... designe_source avec EmptyTree"
    | _ ->
	Error.debug ("Je suis le composant : "^(Id_acc.name_of (Blast_acc.get_name ast))^" et je pars de  : "^(Id_acc.name_of (Blast_acc.get_refined_name ast)));
	Id_acc.name_of (Blast_acc.get_refined_name ast)
	
  in
  let rec scope_rec fifo result=
    match fifo with
      [] -> result
    | (filename,ast)::t -> 
        try
          let (new_amn,new_env) = 
            Bscoping.scope_amn 
              (select_scope 
                 (designe_source ast) (*DP d�clenche une erreur pour les fichiers ...~ *)
                 (!scope_env) 
                 graphe 
                 scope_env_basic) 
              ast
          in
          (
	   scope_info := (filename::(!scope_info));
	   Error.debug "\n"; 
	   Error.scopping_success filename;
	   Error.debug "\n";
           scope_env := ((Id_acc.name_of (Blast_acc.get_name ast)),new_env)::(!scope_env);
           scope_rec t ((filename,new_amn)::result)
	  )
        with Modules.Error s -> 
          Error.debug ("\n"^filename^" � cause de : "^s^"\n") ;
	  scope_rec (t@[(filename,ast)]) result
  in
  scope_rec files_list []
;;
      
let process_bdeps_tt_fichier () =
  match !depli_dump with
    true -> 
      let graph_out = open_out ((!output_directory)^(!bdeps_output_tt_fichier)) in
(* SC: decompilation desactivee
      Bgraph_old.dc_graph graph_out !bgraph_tt_fichier;
*)
	Error.debug "\n*** SC: decompilation desactivee***\n";
      flush graph_out;
      close_out graph_out
  | false -> ()
;;    

let depli_processing files_list bgraph =
  if !depli_dump
  then   
    begin
      let liste_tt_fichier = (genere_liste_tous_fichier (fst (List.hd files_list))) in
      process_bdeps_tt_fichier ();
      Depliage.resultat (snd (List.split files_list)) bgraph (snd (List.split liste_tt_fichier)) !bgraph_tt_fichier
    end
  else
    ()

let tree_processing files_list bgraph =
  if !tree_dump then
    Tree.main (snd (List.split files_list)) bgraph "STRUCTURE.pst"

(*DP A revoir car je comprends pas trop les diff�rences avec unfoldall, il me semble que d�plier deux composants
   peut se ramener � une partie du d�pliage de toutes une serie de composant, il suffit de construire autrement la liste
   des evenements.
    
let unfold_processing files_list bgraph =
  if !unfold 
  then  
    let ma_liste =
      List.find_all (fun (a,b) -> (a = (!unfold_name1)) or (a = (!unfold_name2))) files_list
    in
    let ma_liste_splitee =
      List.split ma_liste
    in
    match (snd ma_liste_splitee) with
    | source::target::[] ->
(*	let (unfold_amn,new_graph) = Unfold.unfold_IMPORTS source target bgraph in*)
(*	let (unfold_amn,new_graph) = Unfold.unfold_INCLUDES source target bgraph in*)
	let (unfold_amn,new_graph) = Unfold.unfold_REFINES target source bgraph in
	Error.message ("\n\nD�pliage de <"^(fst (List.hd ma_liste))^"> et de <"^(fst (List.hd (List.tl ma_liste)))^"> r�ussit.\n");
	Error.message ("R�sultat mis dans le fichier : result_unfold.ref.xml");
	unfold_xml_processing
	  ("result_unfold.ref",unfold_amn)
	  !output_directory (*DP (Bfile.dest_check (Filename.dirname (List.hd (fst ma_liste_splitee))))*)
    | _ -> 
	Error.error "Unfold : uniquement 2 composants"
*)          
let unfoldall_processing files_list bgraph =
  (*YP debugage *)
    Error.debug ("\n\n **************** unfoldall_processing ******************\n\n");
  Error.message ("\nunfold_processing debut  \n");  
  if !unfoldall_bool then
(*DP    let racine = 
      Bfile.dest_check (Filename.dirname (fst (List.hd files_list)))
    in
    let projet = (!output_directory)^(Bfile.project racine)
    in
    let projet_out =
      Bfile.dest_check (projet^"_2")
    in
*)
    let (unfold_amn,liste_de_fichier) = 
      let _ = 	Error.debug ("\n\n ************* Lancement de Unfold_main.realise ********\n\n") in
      Unfold_main.realise files_list bgraph !unfoldall_node
    in
(*DP    let _ = Unix.system ("mkdir -p "^projet_out) in
    let _ = Unix.system ("mkdir -p "^projet_out^"spec") in
    let _ = Unix.system ("mkdir -p "^projet_out^"bdp") in
    let _ = Unix.system ("mkdir -p "^projet_out^"lang") in
    let _ = Unix.system ("mkdir -p "^projet_out^"xsl") in
    let _ = Unix.system ("mkdir -p "^projet_out^"txt") in
    let _ = Unix.system ("mkdir -p "^projet_out^"xml") in
    let _ = Unix.system ("cp -rf "^racine^"../bdp/* "^projet_out^"bdp") in
    let _ = Unix.system ("cp -rf "^racine^"../lang/* "^projet_out^"lang") in
    let _ = Unix.system ("cp -rf "^"/estas/methodeb/Parsers/BCaml/bcaml-0.35/bxml/xsl/amn2txt.xsl"^" "^projet_out^"xsl") in
    let _ = Unix.system ("cp -rf "^"/estas/methodeb/Parsers/BCaml/bcaml-0.35/bxml/xsl/Makefile"^" "^projet_out^"xml") in
    let _ = Unix.system ("cp -rf "^"/estas/methodeb/Parsers/BCaml/bcaml-0.35/blast/AMN.dtd"^" "^projet_out^"xml") in

    let rec termine_aux liste =
      match liste with
      |	[] ->
	  ()
      |	(a,b)::reste ->
	  (
	   let name = Id_acc.name_of (Blast_acc.get_name b) in
	   if not (List.exists (fun a -> a=name) liste_de_fichier)
	   then
	     begin
	       let result =
		 Unix.system ("cp "^a^" "^projet_out^"spec/")
	       in
	       (match result with
	       | Unix.WEXITED(_) ->
		   Error.message (a^" a bien �t� copi�.");
	       | _ ->
		   Error.message (a^" n'a pas correctement �t� copi�."););
	       termine_aux reste
	     end
	   else
	     termine_aux reste
	  )
    in
*)
    Error.message("\nUnfold All --- Bhappy :-)");
    (*    
       Error.message racine;
       Error.message projet;
     *)
    Error.debug ("\n\n***************************** unfold_xml_processing ********* \n\n");
    unfold_xml_processing 
      (((!unfoldall_node)^".ref"),unfold_amn)
      
!output_directory (*DP      projet_out *)  ; 

(*DP  
  (*
     (List.iter
     (fun (x,y) -> output_string stdout ((x)^" ;\n ");flush stdout)
     (files_list));*)
   termine_aux files_list;
   Sys.chdir (projet_out^"xml");
   let _ = Unix.system ("make -f "^projet_out^"xml/Makefile") in
   let _ = Unix.system ("cp "^projet_out^"txt/"^(!unfoldall_node)^".ref.xml.txt "^projet_out^"spec/"^(!unfoldall_node)^".ref") in
*)
    ()
      
let _ = 

try
      let time_debut = Unix.gettimeofday() in
    (	
      Arg.parse args process_file ("Bparser "^bparser_version);
      (*YP Debugage *)
      let _ = Error.debug ("\n\n****************** MAIN *****************\n\n") in
      if (!unfold || !unfoldall_bool)
      then 
	begin
	  let new_list_scoping = scope_processing !files_list !bgraph in
	    (*DP cf definition de la fonction	  unfold_processing new_list_scoping !bgraph;*)
	  unfoldall_processing new_list_scoping !bgraph
	end;
	(* YP debugage *)
	Error.debug ("\n\n ***** depli_processing ******* \n\n");
      depli_processing !files_list !bgraph;
      (* YP debugage *)
      Error.debug ("\n\n ***** Tree_processing ******* \n\n");
      tree_processing !files_list !bgraph;
      (* YP debugage *)
      Error.debug ("\n\n ***** process_bdeps ******* \n\n");
      process_bdeps ();
      (* YP debugage *)
      Error.debug ("\n\n ***** process_verbose ******* \n\n");
      process_verbose time_debut;
      print_string "\n"; 
      flush stdout 
	(* 
	    ;
            Gui.process  files_list
          *)
     )
with Stop -> ()

