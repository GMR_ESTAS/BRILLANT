(* $Id$ *)

open Blast

let debugOpers = 
  fun msg oper -> 
    Error.debug (msg ^ (Id_acc.name_of (Clause_acc.get_operation_id oper))) 


let debugMachines msg machines=
  let mchids=List.map Blast_acc.get_name machines in
  let mchstrs=List.map Id_acc.name_of mchids in
  let strconcat=String.concat "," mchstrs in
    Error.debug (msg ^ strconcat)

let instanciate_predicate pred mchparams paramvalues=
  if mchparams=[] && paramvalues=[] then pred
  else 
    if mchparams <> [] && paramvalues<>[]
    then     
      let created=
	SubstApply(
	  SubstSetEqualIds(mchparams, paramvalues),
	  pred)
      in
	Gsl.calculate created
    else invalid_arg "instanciate_predicate : parameters number"


let instanciate_substitution subst mchparams paramvalues=
  if mchparams=[] && paramvalues=[] then subst
  else 
    if mchparams <> [] && paramvalues<>[]
    then     
      let created = SubstInstanciation(mchparams, paramvalues, subst) in
	Instanciation.apply_inner_instanciations created
    else invalid_arg "instanciate_substitution : parameters number"


let predicate_of_values values=
  if values=[] then invalid_arg "predicate_of_values"
  else
    let (ids, exprs)=List.split values in
    let exprids=List.map (fun i -> ExprId(i)) ids in
    let equalities=List.map2 (fun e1 e2 -> PredAtom(Bbop.Equal, e1, e2)) exprids exprs in
      Blast_mk.mk_Conjunction equalities



(* All these merge* functions don't do any renaming or instanciation, as it
   can be done later. Furthermore, the second machine in the parameter is
   meant to be the refined/imported one.  Now, for the clauses that aren't
   mentioned here: 

   - Values : handled in the refinement merge of Properties of an
   Implementation. Nothing is said about it in the case of an importation
   merge

   - Promotes : not handled yet (but adding the missing operations is easy)
   
   - Extends : not handled yet (it is probably similar to an importation merge)

   - Assertions : not handled yet

   - Events : it would depend of the chosen approach for Event B : translation
   towards B, or specific handling

   - Sees : things aren't described clearly, whether we must keep the sees
   clause of the importing machine or not

   - Uses : due to the Includes proviso for this clause, it isn't handled

   - Includes/Imports : they vanish when the merge is done.

 *)


let mergeConstraints machine1 machine2=
  Error.debug ("Merging Constraints for " ^ (Id_acc.name_of (Blast_acc.get_name machine1)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name machine2)));
  let cllist1=Blast_acc.get_clause_list machine1 in
  let cllist2=Blast_acc.get_clause_list machine2 in
    try
      let p1=Clause_acc.get_Constraints cllist1 in
	try
	  let p2=Clause_acc.get_Constraints cllist2 in
	    Constraints(PredBin(Bbop.And, p1, p2))
	with 
	    Clause_acc.No_such_clause -> 
	      Constraints(p1)
	with
	    Clause_acc.No_such_clause ->
	      try
		let p2=Clause_acc.get_Constraints cllist2 in
		 Constraints(p2)
	      with 
		  Clause_acc.No_such_clause -> 
		    invalid_arg "mergeConstraints"
		    

let mergeInvariant_inc machine impMachine=
  Error.debug ("Merging Invariant_inc for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name impMachine)));
  let cllist=Blast_acc.get_clause_list machine in
  let impCllist=Blast_acc.get_clause_list impMachine in
  let impMchname=Blast_acc.get_name impMachine in
  let mchparams=Blast_acc.get_params impMachine in
  let calledMachines=
    (Clause_acc.get_Includes cllist)@(Clause_acc.get_Imports cllist) 
  in
  let paramvalues= List.assoc impMchname calledMachines in
    try
      let pImp=Clause_acc.get_Invariant impCllist in
	try 
	  let p=Clause_acc.get_Invariant cllist in
	  let doneCalledInvariant=instanciate_predicate pImp mchparams paramvalues in
	    Invariant(PredBin(Bbop.And, p, doneCalledInvariant))
	with (* p doesn't exist *)
	    Clause_acc.No_such_clause -> 
	      let doneCalledInvariant=instanciate_predicate pImp mchparams paramvalues in
		Invariant(doneCalledInvariant)
    with (*pImp doesn't exist *)
	Clause_acc.No_such_clause -> 
	  try
	    let p=Clause_acc.get_Invariant cllist in
	      Invariant(p)
	  with
	    Clause_acc.No_such_clause ->   invalid_arg "merge_Invariant : no invariants"

	      
(* Hypothesis : the variables replacement (if there were identical variables
   in the machine and the refinement) has already been done *)
let mergeInvariant_ref machine refinement=
  Error.debug ("Merging Invariant_ref for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name refinement)));
  let cllist=Blast_acc.get_clause_list machine in
  let refCllist=Blast_acc.get_clause_list refinement in
  let refinedVars=
    (Clause_acc.get_VariablesConcrete cllist)@
    (Clause_acc.get_VariablesAbstract cllist)@
    (Clause_acc.get_VariablesHidden cllist)
  in
    try
      let p=Clause_acc.get_Invariant cllist in
	try
	  let pRef=Clause_acc.get_Invariant refCllist in
	  let almostInvariant = PredBin(Bbop.And, p, pRef) in
	    if refinedVars=[] then Invariant(almostInvariant)
	    else 
	      Invariant(PredExists(refinedVars, almostInvariant))
	with 
	    Clause_acc.No_such_clause -> 
	      if refinedVars=[] then Invariant(p)
	      else Invariant(PredExists(refinedVars, p))
	with
	    Clause_acc.No_such_clause ->
	      try
		let pRef=Clause_acc.get_Invariant refCllist in
		  if refinedVars=[] then Invariant(pRef)
		  else Invariant(PredExists(refinedVars, pRef))
     with 
	 Clause_acc.No_such_clause -> 
	   invalid_arg "mergeInvariant_ref" 
		    

let mergeProperties_inc machine impMachine=
  Error.debug ("Merging Properties_inc for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name impMachine)));
  let cllist=Blast_acc.get_clause_list machine in
  let impCllist=Blast_acc.get_clause_list impMachine in
  let values=Clause_acc.get_Values cllist in
    try
      let p=Clause_acc.get_Properties cllist in
	try
	  let pImp=Clause_acc.get_Properties impCllist in
	  let allProps = PredBin(Bbop.And, p, pImp) in
	    if values=[]
	    then Properties(allProps)
	    else 
	      let newProps = PredBin(Bbop.And, allProps, predicate_of_values values) in
		Properties(newProps)
	with  (* No properties for the imported machine *)
	    Clause_acc.No_such_clause -> 
	      if values=[]
	      then Properties(p)
	      else 
		Properties(PredBin(Bbop.And, p, predicate_of_values values))
	with (* No properties for the machines*)
	    Clause_acc.No_such_clause ->
	      try
		let pImp=Clause_acc.get_Properties impCllist in
		  if values=[]
		  then Properties(pImp)
		  else 
		    Properties(PredBin(Bbop.And, pImp, predicate_of_values values))
	      with 
		  Clause_acc.No_such_clause -> 
		    invalid_arg "mergeProperties_inc"
		    

let mergeProperties_ref machine refinement=
  Error.debug ("Merging Properties_ref for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name refinement)));
  let cllist=Blast_acc.get_clause_list machine in
  let refCllist=Blast_acc.get_clause_list refinement in
    try
      let p=Clause_acc.get_Properties cllist in
	try
	  let pRef=Clause_acc.get_Properties refCllist in
	    Properties (PredBin(Bbop.And, p, pRef))
	with 
	    Clause_acc.No_such_clause -> 
	      Properties(p)
	with
	    Clause_acc.No_such_clause ->
	      try
		let pRef=Clause_acc.get_Properties refCllist in
		  Properties(pRef)
	      with 
		  Clause_acc.No_such_clause -> 
		    invalid_arg "mergeProperties_ref"
		    

let mergeSets machine1 machine2=
  Error.debug ("Merging Sets for " ^ (Id_acc.name_of (Blast_acc.get_name machine1)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name machine2)));
  let cllist1=Blast_acc.get_clause_list machine1 in
  let cllist2=Blast_acc.get_clause_list machine2 in
  let result=
    (Clause_acc.get_Sets cllist1)@(Clause_acc.get_Sets cllist2) 
  in
    if result=[] then invalid_arg "merge_Sets"
    else Sets(result)



let mergeInitialisation_inc machine impMachine=
  Error.debug ("Merging Initialisation_inc for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name impMachine)));
  let cllist=Blast_acc.get_clause_list machine in
  let impCllist=Blast_acc.get_clause_list impMachine in
  let impMchname=Blast_acc.get_name impMachine in
  let mchparams=Blast_acc.get_params impMachine in
  let calledMachines=
    (Clause_acc.get_Includes cllist)@(Clause_acc.get_Imports cllist) 
  in
  let paramvalues= List.assoc impMchname calledMachines in
    try
      let s=Clause_acc.get_Initialisation cllist in
	try
	  let sImp=Clause_acc.get_Initialisation impCllist in
	  let sImpDone=instanciate_substitution sImp mchparams paramvalues in
	    Initialisation(SubstSequence(sImpDone, s))
	with 
	    Clause_acc.No_such_clause -> 
	      Initialisation(s)
    with
	Clause_acc.No_such_clause ->
	  try
	    let sImp=Clause_acc.get_Initialisation impCllist in
	    let sImpDone=instanciate_substitution sImp mchparams paramvalues in
	      Initialisation(sImpDone)
	  with 
	      Clause_acc.No_such_clause -> 
		invalid_arg "mergeInitialisation_inc"


let mergeInitialisation_ref machine refinement=
  Error.debug ("Merging Initialisation_ref for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name refinement)));
  let refCllist=Blast_acc.get_clause_list refinement in
    try
      let s=Clause_acc.get_Initialisation refCllist in
	Initialisation(s)
    with
	Clause_acc.No_such_clause ->
	  invalid_arg "mergeInitialisation_ref"


let mergeConstantsConcrete machine1 machine2=
  Error.debug ("Merging ConstantsConcrete for " ^ (Id_acc.name_of (Blast_acc.get_name machine1)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name machine2)));
  let cllist1=Blast_acc.get_clause_list machine1 in
  let cllist2=Blast_acc.get_clause_list machine2 in
  let result=
    (Clause_acc.get_ConstantsConcrete cllist2)@
    (Clause_acc.get_ConstantsConcrete cllist1)
  in
    if result=[]
    then invalid_arg "mergeConstantsConcrete"
    else ConstantsConcrete(result)


let mergeConstantsAbstract machine1 machine2=
  Error.debug ("Merging ConstantsAbstract for " ^ (Id_acc.name_of (Blast_acc.get_name machine1)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name machine2)));
  let cllist1=Blast_acc.get_clause_list machine1 in
  let cllist2=Blast_acc.get_clause_list machine2 in
  let result=
    (Clause_acc.get_ConstantsAbstract cllist2)@
    (Clause_acc.get_ConstantsAbstract cllist1)
  in
    if result=[]
    then invalid_arg "mergeConstantsAbstract"
    else ConstantsAbstract(result)


let mergeConstantsHidden machine1 machine2=
  Error.debug ("Merging ConstantsHidden for " ^ (Id_acc.name_of (Blast_acc.get_name machine1)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name machine2)));
  let cllist1=Blast_acc.get_clause_list machine1 in
  let cllist2=Blast_acc.get_clause_list machine2 in
  let result=
    (Clause_acc.get_ConstantsHidden cllist2)@
    (Clause_acc.get_ConstantsHidden cllist1)
  in
    if result=[]
    then invalid_arg "mergeConstantsHidden"
    else ConstantsHidden(result)


let mergeVariablesConcrete_inc machine impMachine=
  Error.debug ("Merging VariablesConcrete_inc for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name impMachine)));
  let cllist=Blast_acc.get_clause_list machine in
  let impCllist=Blast_acc.get_clause_list impMachine in
  let result=
    (Clause_acc.get_VariablesConcrete cllist)@
    (Clause_acc.get_VariablesConcrete impCllist)
  in
    if result=[]
    then invalid_arg "mergeVariablesConcrete_inc"
    else VariablesConcrete(result)


(*Hypothesis : the variable renaming has already been done *)
let mergeVariablesConcrete_ref machine refinement=
  Error.debug ("Merging VariablesConcrete_ref for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name refinement)));
  let refCllist=Blast_acc.get_clause_list refinement in
  let result=Clause_acc.get_VariablesConcrete refCllist in
    if result=[]
    then invalid_arg "mergeVariablesConcrete_ref"
    else VariablesConcrete(result)


let mergeVariablesAbstract_inc machine impMachine=
  Error.debug ("Merging VariablesAbstract_inc for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name impMachine)));
  let cllist=Blast_acc.get_clause_list machine in
  let impCllist=Blast_acc.get_clause_list impMachine in
  let result=
    (Clause_acc.get_VariablesAbstract cllist)@
    (Clause_acc.get_VariablesAbstract impCllist)
  in
    if result=[]
    then invalid_arg "mergeVariablesAbstract_inc"
    else VariablesAbstract(result)


(*Hypothesis : the variable renaming has already been done *)
let mergeVariablesAbstract_ref machine refinement=
  Error.debug ("Merging VariablesAbstract_ref for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name refinement)));
  let refCllist=Blast_acc.get_clause_list refinement in
  let result=Clause_acc.get_VariablesAbstract refCllist in
    if result=[]
    then invalid_arg "mergeVariablesAbstract_ref"
    else VariablesAbstract(result)


let mergeVariablesHidden_inc machine impMachine=
  Error.debug ("Merging VariablesHidden_inc for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name impMachine)));
  let cllist=Blast_acc.get_clause_list machine in
  let impCllist=Blast_acc.get_clause_list impMachine in
  let result=
    (Clause_acc.get_VariablesHidden cllist)@
    (Clause_acc.get_VariablesHidden impCllist)
  in
    if result=[]
    then invalid_arg "mergeVariablesHidden_inc"
    else VariablesHidden(result)


(*Hypothesis : the variable renaming has already been done *)
let mergeVariablesHidden_ref machine refinement=
  Error.debug ("Merging VariablesHidden_ref for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name refinement)));
  let refCllist=Blast_acc.get_clause_list refinement in
  let result=Clause_acc.get_VariablesHidden refCllist in
    if result=[]
    then invalid_arg "mergeVariablesHidden_ref"
    else VariablesHidden(result)



let findOperation opid machines=
  let inMachine opid machine=
    Error.debug ("Looking for operation " ^ (Id_acc.name_of opid) ^ 
		 " in " ^ (Id_acc.name_of (Blast_acc.get_name machine)));
    try
      let _=
	Clause_acc.retrieve_operation opid 
	  (Clause_acc.get_Operations (Blast_acc.get_clause_list machine))
      in
	Error.debug ("Found operation " ^ (Id_acc.name_of opid) ^ 
		     " in " ^ (Id_acc.name_of (Blast_acc.get_name machine)));	
	true
    with
	Not_found -> false
  in
    try
      List.hd (List.filter (inMachine opid) machines)
    with
	Failure _ ->raise Not_found


(* The first parameter is the machine for which operation calls must
   be instanciated, the second parameter is a list of \emph{unfolded}
   machines which should contain the needed operation bodies. We have
   to instanciate the parameters of the called operation and the
   parameters of the machine holding the called operation *)

let expandOperationCalls machine neededMachines visibleMachines=
  let _=Error.debug ("Merging Operations_inc for " ^ (Id_acc.name_of (Blast_acc.get_name machine))) in
  let _=debugMachines "Needed machines " neededMachines in
  let _=debugMachines "Visible machines " visibleMachines in
  let machineClauses=Blast_acc.get_clause_list machine in
  let operations=Clause_acc.get_Operations machineClauses in
    List.iter (debugOpers "Expansion : ") operations;		   


  let rec expandOperation operation=
    let opid=Clause_acc.get_operation_id operation in
      Error.debug ("Expanding operation " ^ (Id_acc.name_of opid));
    let outparams=Clause_acc.get_operation_outparams operation in
    let inparams=Clause_acc.get_operation_inparams operation in
    let newbody=
      expandSubstitution (Clause_acc.get_operation_body operation)
    in
      (opid, outparams, inparams, newbody)

  and expandSubstitution subst=
    match subst with
      | Blast.SubstOperCall(_) ->	
	  let vars=Subst_acc.varlist_from subst in
	  let exprs=Subst_acc.allexprs_from subst in
	  let calledId=Subst_acc.opid_from subst in
	    Error.debug ("Found operation call to " ^ (Id_acc.name_of calledId));
	  let calledMachine=findOperation calledId (neededMachines@visibleMachines) in
	  let calledClauses=Blast_acc.get_clause_list calledMachine in
	  let operations=Clause_acc.get_Operations calledClauses in

	  let calledOperation=Clause_acc.retrieve_operation calledId operations in
	  let calledOuts=Clause_acc.get_operation_outparams calledOperation in
	  let calledIns=Clause_acc.get_operation_inparams calledOperation in
	  let calledBody=Clause_acc.get_operation_body calledOperation in

(* fold_left2 is tail-recursive, just a little optimization *)
	let changeid_subst sub xi xj=Instanciation.id_subst_subst xi xj sub in
	let replaced_outs=List.fold_left2 changeid_subst calledBody calledOuts vars 
	in
	let replaced_ins=
	  let prepare_inst=SubstInstanciation(calledIns, exprs, replaced_outs)
	  in
(* yes, all instanciations must be done "at the same time", so we use the
   little trick of defining a to-be-done instanciations. Note this trick has
   not been applied to outparams, as these are stricts id (so they are not
   present as expressions in the substitution) and appear in the left member
   of affectations, thus there is no "instanciation order" problem *)
	    Instanciation.apply_inner_instanciations prepare_inst
	in
	let finalBlock=
	  try
	    let machineParams=Blast_acc.get_params calledMachine in
	    let calledMchId=Blast_acc.get_name calledMachine in
	    let machineInsts=
	      List.assoc calledMchId 
		((Clause_acc.get_Imports machineClauses)@(Clause_acc.get_Includes machineClauses))
	    in
	    let withParams=SubstInstanciation(machineParams, machineInsts, replaced_ins) in
	      Instanciation.apply_inner_instanciations withParams
	  with
	      Not_found -> (* List.assoc raised an exception *)
		Instanciation.apply_inner_instanciations replaced_ins
	in
(* all instanciations have been made? So we return the substitution
   as a block (to ease reading in case POs are generated without having been
   calculated) *)
	SubstBlock(finalBlock)

	    
    | Blast.SubstSetEqualIds(_)
    | Blast.SubstSetEqualFun(_)
    | Blast.SubstSetEqualRecords(_)
    | Blast.SubstBecomeSuch(_)
    | Blast.SubstSetIn(_)
    | Blast.SubstSkip -> subst

    | Blast.SubstBlock(_) ->
	SubstBlock(expandSubstitution (Subst_acc.subst_from subst))
    | Blast.SubstPrecondition(_) ->
	let c=Subst_acc.cond_from subst in
	let s=Subst_acc.subst_from subst in
	  SubstPrecondition(c, (expandSubstitution s))
    | Blast.SubstAssertion(_) ->
	let c=Subst_acc.cond_from subst in
	let s=Subst_acc.subst_from subst in
	  SubstAssertion(c, (expandSubstitution s))
    | Blast.SubstChoice(_) ->
	let sl=Subst_acc.allsubsts_from subst in
	  SubstChoice(List.map expandSubstitution sl)
    | Blast.SubstSequence(_) ->
	let (s1, s2)=Subst_acc.twosubsts_from subst in
	  SubstSequence(
	    expandSubstitution s1,
	    expandSubstitution s2)
    | Blast.SubstParallel(_) ->
	let (s1, s2)=Subst_acc.twosubsts_from subst in
	  SubstParallel(
	    expandSubstitution s1,
	    expandSubstitution s2)
    | Blast.SubstIf(_) ->
	let conds=Subst_acc.allconds_from subst in
	let thens=Subst_acc.allsubsts_from subst in
	let newelse=
	  match (Subst_acc.elsepart_from subst) with
	    | None -> None
	    | Some(s) -> Some( expandSubstitution s)
	in
	  SubstIf(
	    List.combine
	      conds
	      (List.map expandSubstitution thens),
	    newelse)
    | Blast.SubstSelect(_) ->
	let whens=Subst_acc.allconds_from subst in
	let thens=Subst_acc.allsubsts_from subst in
	let newelse=
	  match (Subst_acc.elsepart_from subst) with
	    | None -> None
	    | Some(s) -> Some( expandSubstitution s)
	in
	  SubstSelect(
	    List.combine
	      whens
	      (List.map expandSubstitution thens),
	    newelse)
    | Blast.SubstCase(_) ->
	let caseexpr=Subst_acc.expr_from subst in
	let eithers=Subst_acc.allexprLists_from subst in
	let newthens=
	  List.map (expandSubstitution) (Subst_acc.allsubsts_from subst) in
	let newelse=
	  match (Subst_acc.elsepart_from subst) with
	    | None -> None
	    | Some(s) -> Some( expandSubstitution s)
	in
	  SubstCase(
	    caseexpr,
	    List.combine eithers newthens,
	    newelse)
    | Blast.SubstAny(_) ->
	let idents=Subst_acc.allids_from subst in
	let cond=Subst_acc.cond_from subst in
	let s=Subst_acc.subst_from subst in
	 SubstAny(idents, cond, expandSubstitution s)
    | Blast.SubstLet(_) ->
	let idents=Subst_acc.allids_from subst in
	let c=Subst_acc.cond_from subst in
	let s=Subst_acc.subst_from subst in
	  SubstLet(idents, c, expandSubstitution s)

    | Blast.SubstVar(_) ->
	let idents=Subst_acc.allids_from subst in
	let s=Subst_acc.subst_from subst in
	  SubstVar(idents, expandSubstitution s)
    | Blast.SubstWhile(_) ->
	let c=Subst_acc.cond_from subst in
	let s=Subst_acc.subst_from subst in
	let (i,v)=Subst_acc.whilepreds_from subst in
	  SubstWhile(c, expandSubstitution s, i, v)
    | Blast.SubstInstanciation(_) -> 
	let s=Subst_acc.subst_from subst in
	let instids=Subst_acc.allids_from subst in
	let instexprs=Subst_acc.allexprs_from subst in
	  SubstInstanciation(
	    instids,
	    instexprs,
	    expandSubstitution s)
  in

  let expandedOperations=
    List.map expandOperation operations
  in
    List.iter (debugOpers "Expanded : ") expandedOperations;
    Operations(expandedOperations)



let renameDuplicateVariables variableClause machine refinement=
  let (getVars, setVars)=
    match variableClause with
      | Blast.VariablesConcrete(_) -> 
	  (Clause_acc.get_VariablesConcrete, fun c -> VariablesConcrete(c))
      | Blast.VariablesAbstract(_) -> 
	  (Clause_acc.get_VariablesAbstract, fun c -> VariablesAbstract(c))
      | Blast.VariablesHidden(_) -> 
	  (Clause_acc.get_VariablesHidden,  fun c -> VariablesHidden(c))
      | _ -> invalid_arg "renameDuplicateVariables : not a variable clause"
  in
  let mchVars=getVars (Blast_acc.get_clause_list machine) in
  let refVars=getVars (Blast_acc.get_clause_list refinement) in
  let mchVarSet=Id_handling.set_from_ids mchVars in
  let refVarSet=Id_handling.set_from_ids refVars in
  let sameVarSet=Id_handling.Idset.inter mchVarSet refVarSet in
  let sameVars=Id_handling.Idset.elements sameVarSet in
  let _=Error.debug ("Variables to rename : " ^ (String.concat "," (List.map Id_acc.name_of (sameVars)))) in
  let renaming=Id_handling.rename_ids sameVars (Id_handling.Idset.union mchVarSet refVarSet) in
    if renaming=[] then machine
    else
      let (renamed, newnames)=List.split renaming in
      let exprRenamed=List.map (fun i -> ExprId(i)) renamed in
      let exprNewnames=List.map (fun i -> ExprId(i)) newnames in
      let renamings =
	List.map2 (fun e1 e2 -> PredAtom(Bbop.Equal, e1, e2)) exprRenamed exprNewnames
      in
      let predRenaming=Blast_mk.mk_Conjunction renamings in
      let changeid_machine ast id1 id2=Instanciation.id_subst_machine id1 id2 ast in
      let renamedMachine=List.fold_left2 changeid_machine refinement renamed newnames in
      let oldInvariant=
	try 
	  Clause_acc.get_Invariant (Blast_acc.get_clause_list refinement) 
	with 
	    Clause_acc.No_such_clause -> Blast_mk.mk_PredTrue ()
      in
      let newInvariant = PredBin(Bbop.And, oldInvariant, predRenaming) in
	Blast_acc.replace_clause 
	  renamedMachine 
	  (Invariant(oldInvariant))
	  (Invariant(newInvariant))


let renameAllDuplicateVariables machine refinement=
  let mch1=renameDuplicateVariables (Blast.VariablesConcrete([])) machine refinement in
  let mch2=renameDuplicateVariables (Blast.VariablesAbstract([])) mch1 refinement in
    renameDuplicateVariables (Blast.VariablesHidden([])) mch2 refinement

 
let extendPrecondition operation predicate=
  let opid=Clause_acc.get_operation_id operation in
  let outparams=Clause_acc.get_operation_outparams operation in
  let inparams=Clause_acc.get_operation_inparams operation in
  let opbody=Clause_acc.get_operation_body operation in
  let newbody=
    match opbody with
      | Blast.SubstPrecondition(_) ->
	  let cond=Subst_acc.cond_from opbody in
	  let subst=Subst_acc.subst_from opbody in
	  let newcond = PredBin(Bbop.And, cond, predicate) in
	    SubstPrecondition(newcond, subst)
      | _ -> 
	  SubstPrecondition(predicate, opbody)
  in
    (opid, outparams, inparams, newbody)
      

(* Hypothesis : the machine variables have already been renamed, and the
invariant has been updated *)
let mergeOperations_ref machine refinement=
  Error.debug ("Merging Operations_ref for " ^ (Id_acc.name_of (Blast_acc.get_name machine)) 
	       ^ " and " ^ (Id_acc.name_of (Blast_acc.get_name refinement)));
  let refOperations=
    Clause_acc.get_Operations (Blast_acc.get_clause_list refinement)
  in
    List.iter (debugOpers "In refinement : ") refOperations;
  let operations=   
    Clause_acc.get_Operations (Blast_acc.get_clause_list machine) 
  in
    List.iter (debugOpers "Maybe refined : ") operations;
  let opids=List.map Clause_acc.get_operation_id operations in
  let refopids=List.map Clause_acc.get_operation_id refOperations in
  let notrefopids=
    List.filter
      (fun id -> not (List.mem id refopids))
      opids
  in
  let missingOperations=
    List.find_all 
      (fun oper -> List.mem (Clause_acc.get_operation_id oper) notrefopids) 
      operations
  in
    List.iter (debugOpers "Missing : ") missingOperations;
  let unfoldOperation operation refOperation refVariables newInvariant=
    let opBody=Clause_acc.get_operation_body operation in
    match opBody with
      | Blast.SubstPrecondition(_) ->
	  let abstractPreCond=Subst_acc.cond_from opBody in
	  let addedPred=
	    if refVariables=[]
	    then 
	      PredBin(Bbop.And, newInvariant, abstractPreCond)
	    else
	      PredExists(refVariables, PredBin(Bbop.And, newInvariant, abstractPreCond))
	  in
	    extendPrecondition refOperation addedPred
      | _ -> 
	  let addedPred=
	    if refVariables=[] then newInvariant 
	    else PredExists(refVariables, newInvariant)
	  in
	    extendPrecondition refOperation addedPred
  in

  let refCllist=Blast_acc.get_clause_list refinement in
  let refVariables=
    (Clause_acc.get_VariablesConcrete refCllist)@
    (Clause_acc.get_VariablesAbstract refCllist)@
    (Clause_acc.get_VariablesHidden refCllist)
  in
  let newInvariant=
    try
      Clause_acc.get_Invariant (Blast_acc.get_clause_list machine)
    with
	Clause_acc.No_such_clause -> Blast_mk.mk_PredTrue ()
  in
  let refinementsList=
    List.map 
      (fun id -> 
	 (Clause_acc.retrieve_operation id operations,
	  Clause_acc.retrieve_operation id refOperations)
      )
      refopids
(* SC: ^ Pas une bonne id�e, voir le TODO dans bunfold/ *)
  in
  let unfoldedOperations=
    List.map
      (fun (op, refop) -> unfoldOperation op refop refVariables newInvariant)
      refinementsList
  in
  let allOperations=unfoldedOperations@missingOperations in
    List.iter (debugOpers "All : ") allOperations;
    Operations(allOperations)


let mergeRefFunctions=
  [ mergeConstraints
  ; mergeInvariant_ref
  ; mergeSets
  ; mergeInitialisation_ref
  ; mergeConstantsConcrete
  ; mergeConstantsAbstract
  ; mergeConstantsHidden
  ; mergeProperties_ref
  ; mergeVariablesConcrete_ref
  ; mergeVariablesAbstract_ref
  ; mergeVariablesHidden_ref
  ]


(* Note there is no Operations in this list : the function requires
   \emph{several} imported machines to be able to do the instanciation *)
let mergeIncFunctions=
  [ mergeConstraints
  ; mergeInvariant_inc
  ; mergeSets
  ; mergeInitialisation_inc
  ; mergeConstantsConcrete
  ; mergeConstantsAbstract
  ; mergeConstantsHidden
  ; mergeProperties_inc
  ; mergeVariablesConcrete_inc
  ; mergeVariablesAbstract_inc
  ; mergeVariablesHidden_inc
  ]
