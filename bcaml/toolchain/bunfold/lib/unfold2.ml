(* $Id$ *)

(* This function do the unfolding accordingly to the following algorithm
   (technical report INRETS/RE-01-710-FR) :
   Lets define the following notation:
   \begin{itemize}
   \item $N_2\/N_1$ stands for "the machine $N_2$ refines the $N_1$ machine"
   \item $N.[T_1,\ldots,T_n]$ means that "the machine $N$ imports $T_1,\ldots,T_n$"
   \item $\{N\}$ represents the AST of the machine $N$
   \end{itemize}

   Lets now define the functions the unfolding is based on (actual names in this
   library might differ, though): 
   \begin{itemize}
   \item enrich_inc($\{N\}$) is built by making the transitive closure of INCLUDES links
   \item enrich_imp($\{N\},\{N_1\},\ldots,\{N_m\}$) is built with $N$ importing the $N_i$ machines. 
    The $N_i$ machines might as well be the results of previous unfoldings
   \item enrich_ref($\{N\},\{N_1\}$) is built with $N$ and his refinement $N_1$. 
    $N$ and $N_1$ might be the results of previous unfoldings
   \end{itemize}

   Now for the unfolding algorithm $\Phi$: 
   let $T$ be a subtree structure representing the project that is to be unfolded, 
   and $N, N_i, T_i$ be machines
   \begin{itemize}
   \item $T=N$ : $\Phi(T) = \mbox{enrich_inc}($\{N\}$)$
   \item $T=U\/\ldots\/N_1$ with $U=N_m$ or $U=N_m.[T_1,\ldots,T_n]$ : 
   $T'=N_{m-1}\/\ldots\/N_1$, $T''=U$, $\Phi(T) = \mbox{enrich_ref}(\Phi(T'), \Phi(T''))$
   \item $T=N.[T_1,\ldots,T_n]$ : $\Phi(T) = \mbox{enrich_imp}(\Phi(\{N\}),\Phi(T_1),\ldots,\Phi(T_n))$
   \end{itemize}
 *)

open Blast
open Project

let rec applyMergeFunctions mergefunlist mch1 mch2=
  match mergefunlist with
    | [] -> []
    | mergefun::others ->
	try
	  let newclause=mergefun mch1 mch2 in
	    newclause::(applyMergeFunctions others mch1 mch2)
	with
	    Invalid_argument e ->
	      Error.debug ("Got an error : " ^ e ^ " . Going on anyway\n");
	      applyMergeFunctions others mch1 mch2


(* In the parameters, abstract machine comes first, then his refinement *)
let enrich_ref machine refinement=
  let _=Error.debug ("Enriching ref " ^ (Id_acc.name_of (Blast_acc.get_name machine)) ^ " with " ^ (Id_acc.name_of (Blast_acc.get_name refinement)) ) in
  let newRefinement=Unfold_lib2.renameAllDuplicateVariables machine refinement in
  let newClauses=applyMergeFunctions Unfold_lib2.mergeRefFunctions machine newRefinement in
  let mchHead=Blast_acc.get_head machine in
  let oldOperations =
    Operations(Clause_acc.get_Operations (Blast_acc.get_clause_list machine))
  in
  let almostDone = Machine(mchHead, oldOperations::newClauses) in
  let newOperations=Unfold_lib2.mergeOperations_ref almostDone refinement in
  let doneMachine = Machine(mchHead, newOperations::newClauses) in
  let niceMachine=Blast_acc.sortClauses doneMachine in
  let _=Error.debug ("Just built Refinement enrichment for:" ^ (Id_acc.name_of (Blast_acc.get_name niceMachine)) ) in
    niceMachine


let enrich_inc machine modedMachines visibleMachines=
  let _=Error.debug ("Enriching imp " ^ (Id_acc.name_of (Blast_acc.get_name machine))) in
  let mchHead=Blast_acc.get_head machine in
  let clauseList=Blast_acc.get_clause_list machine in
  (* we have to keep the Includes or Imports clause as long as the
     merging is not complete : some merging functions need the
     instanciation parameters of the included machines*)
  let oldIncludes = Includes(Clause_acc.get_Includes clauseList) in
  let oldImports = Imports(Clause_acc.get_Imports clauseList) in
  let oldOperations = Operations(Clause_acc.get_Operations clauseList) in
    
  let buildMachine=
    match machine with
      | Blast.EmptyTree -> invalid_arg "enrich_inc"
      | Blast.Machine(_) -> fun clauses -> Machine(mchHead, clauses)
      | Blast.Refinement(_)
      | Blast.Implementation(_) ->
	  let refName=Blast_acc.get_refined_name machine in
	    fun clauses -> Refinement(mchHead, refName, clauses)
  in

  let enrichSingle mchBase mchInc=
    let newClauses=applyMergeFunctions Unfold_lib2.mergeIncFunctions mchBase mchInc in
      buildMachine (oldIncludes::(oldImports::newClauses))
  in

  let allEnriched=List.fold_left enrichSingle machine modedMachines in
  let allClauses=Blast_acc.get_clause_list allEnriched in
  let cleanClauses=
    Blast_acc.removeClause
      oldOperations
      (Blast_acc.removeClause 
	 oldIncludes
	 ( Blast_acc.removeClause oldImports allClauses)
      )
  in
  let newOperations=Unfold_lib2.expandOperationCalls machine modedMachines visibleMachines in
  let doneMachine=buildMachine (newOperations::cleanClauses) in
  let niceMachine=Blast_acc.sortClauses doneMachine in
  let _=Error.debug ("Just built Importation enrichment for:" ^ (Id_acc.name_of (Blast_acc.get_name machine))) in
    niceMachine


let enrich_imp machine importedMachines=
  enrich_inc machine importedMachines


(* We add a hash table to cache already unfolded machines *)

let unfoldedRepository=Hashtbl.create 53

(* Hypothesis : \emph{project} is a well-built project (see the Project module in lib\/) *)
let rec auxUnfold unfoldedRefined project=
  let _=Error.debug ("State of the project : " ^ (Project.project_to_string project)) in
  let rootid=Project.get_project_id project in
    if Hashtbl.mem unfoldedRepository rootid
    then 
      let _=Error.debug ("Found " ^ (Id_acc.name_of rootid) ^ " in the cache") in
	Hashtbl.find unfoldedRepository rootid
    else
      let rootimchcont=Project.find project rootid in
      let rootmch=(snd rootimchcont).machine in

      let rootDeps=Bgraph.filter_source project.graph rootid in
      let (refDeps, notRefDeps)=
	List.partition (fun e -> (Bgraph.dependency e)=Bgraph.RefinedBy) rootDeps
      in

      let includesDeps=
	List.filter (fun edge -> ((Bgraph.dependency edge)=Bgraph.Includes) ) notRefDeps in
      let importsDeps=
	List.filter (fun edge -> ((Bgraph.dependency edge)=Bgraph.Imports) ) notRefDeps in
      let otherDeps=
	List.filter 
	  (fun edge -> ((Bgraph.dependency edge)<>Bgraph.Imports) &&
	     ((Bgraph.dependency edge)<>Bgraph.Includes) &&
	     ((Bgraph.dependency edge)<>Bgraph.Renames)
	  )
	  notRefDeps
      in
      let modsDeps=
	match rootmch with
	  | Blast.Machine(_)
	  | Blast.Refinement(_) -> 
	      let _=Error.debug ("Found abstract : " ^ (Id_acc.name_of rootid)) in
		includesDeps 
	  | Blast.Implementation(_) -> 
	      let _=Error.debug ("Found implementation : " ^ (Id_acc.name_of rootid)) in
		importsDeps
	  | Blast.EmptyTree -> invalid_arg "auxUnfold : empty tree found"
      in
      let _=Error.debug ("Source file : " ^ (Bxml.to_string rootmch)) in
      let modsIds=List.map Bgraph.target modsDeps in
      let otherIds=List.map Bgraph.target otherDeps in
      let modsProjects=List.map (Project.subproject project) modsIds in
      let otherProjects=List.map (Project.subproject project) otherIds in
      let unfoldedProjects=List.map (auxUnfold Blast.EmptyTree) modsProjects in
      let unfoldedOthers=List.map (auxUnfold Blast.EmptyTree) otherProjects in

      let unfoldedSelf=enrich_inc rootmch unfoldedProjects unfoldedOthers in
      let enrichedTree=
	if unfoldedRefined=Blast.EmptyTree 
	then unfoldedSelf
	else enrich_ref unfoldedRefined unfoldedSelf
      in
	match refDeps with
	  | [] -> 
	      let unfoldedId=Blast_acc.get_name enrichedTree in
	      let _=Error.debug ("Adding " ^ (Id_acc.name_of unfoldedId) ^ " in the cache") in
		Hashtbl.add unfoldedRepository unfoldedId enrichedTree;
		enrichedTree
	  | [alone] ->
	      let refId=Bgraph.target alone in
	      let refProject=Project.subproject project refId in
		auxUnfold enrichedTree refProject
	  | _ -> invalid_arg "auxUnfold : more than one refinement"


let unfold project= auxUnfold Blast.EmptyTree project
