
define include_subdir_rules
dir_stack := $(d) $(dir_stack)
d := $(d)/$(1)
include $(MK)/header.mk
include $$(addsuffix /Rules.mk,$$(call unescolon,$$(d)))
include $(MK)/calc_rules.mk
d := $$(firstword $$(dir_stack))
dir_stack := $$(wordlist 2,$$(words $$(dir_stack)),$$(dir_stack))
endef

define subtree_tgts
$(TARGETS_$(1)) $(foreach sd,$(FULLSUBDIRS_$(1)),$(call subtree_tgts,$(sd)))
endef

define subdirs_var
$(foreach sd,$(FULLSUBDIRS_$(2)),$(1)_$(sd))
endef

define self_and_subdirs_var
$($(1)_$(2)) $(foreach sd,$(FULLSUBDIRS_$(2)),$(1)_$(sd))
endef
