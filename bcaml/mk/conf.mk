
# VERBOSE := true

DESTDIR=
prefix=$(DESTDIR)/usr
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin
#sbindir=$(exec_prefix)/sbin
#libexecdir=$(exec_prefix)/libexec
datarootdir=$(prefix)/share
texdir=$(datarootdir)/texmf-texlive/tex/latex/brillant
#datadir=$(datarootdir)/bcaml
datadir=$(datarootdir)/brillant
xsldir=$(datadir)/bxml/xsl
#sysconfdir=$(prefix)/etc
#sharedstatedir=$(prefix)/com
#localstatedir=$(prefix)/var
includedir=$(prefix)/include
oldincludedir=/usr/include
docdir=$(datarootdir)/doc/bcaml
infodir=$(datarootdir)/info
htmldir=$(docdir)/html
dvidir=$(docdir)
pdfdir=$(docdir)
psdir=$(docdir)
libdir=$(exec_prefix)/lib/bcaml
lispdir=$(datarootdir)/emacs/site-lisp
localedir=$(datarootdir)/locale
mandir=$(datarootdir)/man
manext=.1


INSTALL=install
INSTALL_PROGRAM=$(INSTALL) -D
INSTALL_DATA=$(INSTALL) -D -m 644


black   := $(shell tput setaf 0)
red     := $(shell tput setaf 1)
green   := $(shell tput setaf 2)
yellow  := $(shell tput setaf 3)
blue    := $(shell tput setaf 4)
magenta := $(shell tput setaf 5)
cyan    := $(shell tput setaf 6)
white   := $(shell tput setaf 7)
bold    := $(shell tput bold)
uline   := $(shell tput smul)
reset   := $(shell tput sgr0)

ifndef COLOR_TTY
COLOR_TTY := $(shell [ `tput colors` -gt 2 ] && echo true)
endif

ifneq ($(VERBOSE),true)
ifeq ($(COLOR_TTY),true)
echo_prog := $(shell if echo -e | grep -q -- -e; then echo echo; else echo echo -e; fi)
echo_cmd = @$(echo_prog) "$(1)$(2)$(reset)";
else
echo_cmd = @echo "$(2)";
endif
else # Verbose output
echo_cmd =
endif
