
%.ml %.mli: %.mly
	$(call echo_cmd,$(uline),OCAMLYACC $(SHORTNAME_$(@D)): $(@F))ocamlyacc $(YACCFLAGS) $<

%.ml: %.mll
	$(call echo_cmd,$(uline),OCAMLLEX $(SHORTNAME_$(@D)): $(@F))ocamllex $(LEXFLAGS) $<

%.cmi: %.mli
	$(call echo_cmd,$(yellow),CMI $(SHORTNAME_$(@D)): $(@F))ocamlc -I $(@D) $(INCLUDES) -c $<

%.cmo: %.ml
	$(call echo_cmd,$(yellow),CMO $(SHORTNAME_$(@D)): $(@F))ocamlc -I $(@D) $(INCLUDES) -c $<

%.cmx %.o: %.ml
	$(call echo_cmd,$(yellow),CMX $(SHORTNAME_$(@D)): $(@F))ocamlopt -I $(@D) $(INCLUDES) -c $<

%.cma:
	$(call echo_cmd,$(cyan),CMA $(SHORTNAME_$(@D)): $(@F))ocamlc -a -o $@ -I $(@D) $(INCLUDES) $(FLAGS) $^

%.cmxa %.a:
	$(call echo_cmd,$(cyan),CMXA $(SHORTNAME_$(@D)): $(@F))ocamlopt -a -o $*.cmxa -I $(@D) $(INCLUDES) $(FLAGS) $^

%.byte:
	$(call echo_cmd,$(green),BYTE $(SHORTNAME_$(@D)): $(@F))ocamlc -o $@ -I $(@D) $(INCLUDES) $(foreach dep,$(EXTRADEPS_$(call escolon,$(@D))),$(dep).cma)  $(filter %.cma %.cmo, $^)

%.native:
	$(call echo_cmd,$(green),NATIVE $(SHORTNAME_$(@D)): $(@F))ocamlopt -o $@ -I $(@D) $(INCLUDES) $(foreach dep,$(EXTRADEPS_$(call escolon,$(@D))),$(dep).cmxa)  $(filter %.cmxa %.cmx, $^)

# We have to cd elsewhere because even with absolute paths, ocamldep
# insists on removing the path if some files are in the same directory
# where ocamldep is run, which is not good if we generate another part
# of the project with a depend.mk where some absolute directories are
# missing
%depend.mk:
	$(call echo_cmd,$(bold),DEP $(@D))cd $(TOP) && ocamldep -slash -I $(@D) $(INCLUDES) $(DMLI_$(call escolon,$(@D))) $(DML_$(call escolon,$(@D))) >$@

%TAGS:
	$(call echo_cmd,$(bold),TAG $(@D))cd $(TOP) && otags -o $@ $(DMLI_$(call escolon,$(@D))) $(DSTRICT_ML_$(call escolon,$(@D)))
