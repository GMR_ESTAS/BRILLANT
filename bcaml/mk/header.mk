ifndef d
d := $(or $(shell echo $(TOP) | sed -e 's/\([A-Z]\):\//\1\\\:\//g'),$(shell cygpath -m $$PWD | sed -e 's/\([A-Z]\):\//\1\\\:\//g'))
dir_stack :=
# Automatic inclusion of the skel.mk at the top level - that way
# Rules.top has exactly the same structure as other Rules.mk
endif

SUBDIRS :=
FILES :=
NAME :=
MAKELIB :=
EXTRADEPS :=
DEPS :=
BIN :=

