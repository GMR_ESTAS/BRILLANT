
SUBDIRS_$(d) := $(SUBDIRS)
FILES_$(d) := $(FILES)
NAME_$(d) := $(NAME)
MAKELIB_$(d) := $(MAKELIB)
EXTRADEPS_$(d) := $(EXTRADEPS)
DEPS_$(d) := $(DEPS)
BIN_$(d) := $(BIN)

$(foreach sd,$(SUBDIRS),$(eval $(call include_subdir_rules,$(sd))))

FULLSUBDIRS_$(d) := $(patsubst %/,%,$(addprefix $(d)/,$(SUBDIRS_$(d))))

INIT_ML_$(d):=$(filter-out %.mli, $(FILES_$(d)))
# Needed for otags who does not like *.ml files from *.mll or *.mly because of line directives in them
STRICT_ML_$(d):=$(filter-out %.mll %.mly,$(INIT_ML_$(d)))
INIT_ML_$(d):=$(patsubst %.mly, %.ml, $(INIT_ML_$(d)))
INIT_ML_$(d):=$(patsubst %.mll, %.ml, $(INIT_ML_$(d)))

INIT_MLI_$(d):=$(filter %.mli %.mly, $(FILES_$(d)))
INIT_MLI_$(d):=$(patsubst %.mly, %.mli, $(INIT_MLI_$(d)))

INIT_MLY_$(d):=$(filter %.mly, $(FILES_$(d)))
INIT_MLL_$(d):=$(filter %.mll, $(FILES_$(d)))
# The 3 following rules are useful only for knowing what to clean
MLMLY_$(d):=$(patsubst %.mly, %.ml, $(INIT_MLY_$(d)))
MLIMLY_$(d):=$(patsubst %.mly, %.mli, $(INIT_MLY_$(d)))
MLMLL_$(d):=$(patsubst %.mll, %.ml, $(INIT_MLL_$(d)))


MLIML_$(d):=$(patsubst %.mli, %.ml, $(INIT_MLI_$(d)))
MLNOMLI_$(d):=$(foreach f,$(INIT_ML_$(d)),$(if $(findstring $(f),$(MLIML_$(d))),,$(f)))
DMLNOMLI_$(d):=$(foreach file,$(MLNOMLI_$(d)),$(d)/$(file))

define cmicmo
$(patsubst %.ml,%.cmi,$(1)): $(patsubst %.ml,%.cmo,$(1))
	touch $$@
endef
$(foreach f,$(DMLNOMLI_$(d)),$(eval $(call cmicmo,$(f))))

ML_$(d):=$(INIT_ML_$(d))
MLI_$(d):=$(INIT_MLI_$(d))

CMO_$(d):=$(patsubst %.ml,%.cmo,$(ML_$(d)))
CMI_$(d):=$(sort $(patsubst %.ml,%.cmi,($ML_$(d)) $(patsubst %.mli,%.cmi,$(MLI_$(d))))) # To remove duplicates
CMX_$(d):=$(patsubst %.ml,%.cmx,$(ML_$(d)))
O_$(d):=$(patsubst %.ml,%.o,$(ML_$(d)))

DML_$(d):=$(foreach file,$(ML_$(d)),$(d)/$(file))
DSTRICT_ML_$(d):=$(foreach file,$(STRICT_ML_$(d)),$(d)/$(file))
DMLI_$(d):=$(foreach file,$(MLI_$(d)),$(d)/$(file))

DCMO_$(d):=$(patsubst %.ml,%.cmo,$(DML_$(d)))
DCMI_$(d):=$(sort $(patsubst %.ml,%.cmi,$(DML_$(d))) $(patsubst %.mli,%.cmi,$(DMLI_$(d)))) # To remove duplicates
DCMX_$(d):=$(patsubst %.ml,%.cmx,$(DML_$(d)))
DO_$(d):=$(patsubst %.ml,%.o,$(DML_$(d)))

CLEAN_$(d):=$(DCMO_$(d)) $(DCMI_$(d)) $(DCMX_$(d)) $(DO_$(d)) \
 $(foreach file,$(MLMLY_$(d)) $(MLIMLY_$(d)) $(MLMLL_$(d)),$(d)/$(file)) \
 $(d)/depend.mk $(d)/TAGS

byte_deps_$(d):=$(foreach dep,$(DEPS_$(d)),$(if $(MAKELIB_$(PATH_TO_$(dep))),$(PATH_TO_$(dep))/lib$(dep).cma,$(DCMO_$(PATH_TO_$(dep)))))
nati_deps_$(d):=$(foreach dep,$(DEPS_$(d)),$(if $(MAKELIB_$(PATH_TO_$(dep))),$(PATH_TO_$(dep))/lib$(dep).cmxa $(PATH_TO_$(dep))/lib$(dep).a,$(DCMX_$(PATH_TO_$(dep)))))


$(d)/depend.mk: $(DMLI_$(d)) $(DML_$(d)) $(foreach dep,$(DEPS_$(d)),$(PATH_TO_$(dep))/depend.mk)

$(DCMI_$(d)): $(d)/depend.mk
$(DCMO_$(d)): $(d)/depend.mk
$(DCMX_$(d)): $(d)/depend.mk

$(d)/depend.mk: INCLUDES:=$(foreach dep,$(DEPS_$(d)),-I $(PATH_TO_$(dep)))
$(DCMO_$(d)): INCLUDES:=$(foreach dep,$(DEPS_$(d)),-I $(PATH_TO_$(dep)))
$(DCMX_$(d)): INCLUDES:=$(foreach dep,$(DEPS_$(d)),-I $(PATH_TO_$(dep)))
$(d)/TAGS: INCLUDES:=$(foreach dep,$(DEPS_$(d)),-I $(PATH_TO_$(dep)))

ifneq ($(strip $(NAME_$(d))),)

$(d)/lib$(NAME).cma: $(filter %.cmo,$(byte_deps_$(d))) $(DCMO_$(d))

$(d)/lib$(NAME).cmxa $(d)/lib$(NAME).a: $(filter %.cmx,$(nati_deps_$(d))) $(DCMX_$(d))

BYTE_LIBRARIES:=$(BYTE_LIBRARIES) $(d)/lib$(NAME).cma
NATI_LIBRARIES:=$(NATI_LIBRARIES) $(d)/lib$(NAME).cmxa $(d)/lib$(NAME).a

BYTE_TARGETS_$(d):=$(if $(MAKELIB_$(d)),$(d)/lib$(NAME_$(d)).cma,)
NATI_TARGETS_$(d):=$(if $(MAKELIB_$(d)),$(d)/lib$(NAME_$(d)).cmxa $(d)/lib$(NAME_$(d)).a,)

TARGETS_$(d):=$(if $(MAKELIB_$(d)),$(d)/lib$(NAME_$(d)).cma $(d)/lib$(NAME_$(d)).cmxa,)

PATH_TO_$(NAME_$(d)):=$(d)

CLEAN_$(d):=$(CLEAN_$(d)) $(d)/lib$(NAME_$(d)).cma $(d)/lib$(NAME_$(d)).cmxa $(d)/lib$(NAME_$(d)).a

endif

ifneq ($(and $(strip $(MAKELIB_$(d))),$(if $(strip $(NAME_$(d))),,noname)),)
$(error Inconsistency: $(d) has a MAKELIB but no NAME)
endif

ifneq ($(strip $(MAKELIB_$(d))),)
local_install_lib_byte_$(d): $(d)/lib$(NAME_$(d)).cma
	$(call echo_cmd,$(bold)$(cyan),INSTALL $<)$(INSTALL_DATA) $< $(libdir)/$(<F)
local_install_lib_nati_$(d): $(d)/lib$(NAME_$(d)).cmxa $(d)/lib$(NAME_$(d)).a
	$(call echo_cmd,$(bold)$(cyan),INSTALL $<)$(INSTALL_DATA) $< $(libdir)/$(<F) && \
	$(INSTALL_DATA) $(patsubst %.cmxa,%.a,$<) $(libdir)/$(patsubst %.cmxa,%.a,$(<F))
local_uninstall_lib_byte_$(d):
	$(call echo_cmd,$(bold)$(cyan),UNINSTALL lib$(NAME_$(subst local_uninstall_lib_byte_,,$@)).cma)rm -f $(libdir)/lib$(NAME_$(subst local_uninstall_lib_byte_,,$@)).cma
local_uninstall_lib_nati_$(d):
	$(call echo_cmd,$(bold)$(cyan),UNINSTALL lib$(NAME_$(subst local_uninstall_lib_nati_,,$@)).cmxa)rm -f $(libdir)/lib$(NAME_$(subst local_uninstall_lib_nati_,,$@)).cmxa $(libdir)/lib$(NAME_$(subst local_uninstall_lib_nati_,,$@)).a
else # No library ? The install targets do nothing
local_install_lib_byte_$(d):
local_install_lib_nati_$(d):
local_uninstall_lib_byte_$(d):
local_uninstall_lib_nati_$(d):
endif

.PHONY: all_install_lib_byte_$(d) local_install_lib_byte_$(d) \
        all_install_lib_nati_$(d) local_install_lib_nati_$(d)

all_install_lib_byte_$(d): local_install_lib_byte_$(d) $(call subdirs_var,all_install_lib_byte,$(d))
all_install_lib_nati_$(d): local_install_lib_nati_$(d) $(call subdirs_var,all_install_lib_nati,$(d))

.PHONY: all_uninstall_lib_byte_$(d) local_uninstall_lib_byte_$(d) \
        all_uninstall_lib_nati_$(d) local_uninstall_lib_nati_$(d)

all_uninstall_lib_byte_$(d): local_uninstall_lib_byte_$(d) $(call subdirs_var,all_uninstall_lib_byte,$(d))
all_uninstall_lib_nati_$(d): local_uninstall_lib_nati_$(d) $(call subdirs_var,all_uninstall_lib_nati,$(d))


ifneq ($(strip $(BIN_$(d))),)

# Include CMA/CMXA for deps whose MAKELIB is yes, include the CMO/CMX
# for the others plus the local CMO/CMX, keeping the order is mandatory

$(d)/$(BIN_$(d)).byte: $(byte_deps_$(d))  $(DCMO_$(d))
$(d)/$(BIN_$(d)).byte: INCLUDES:=$(foreach dep,$(DEPS_$(d)),-I $(PATH_TO_$(dep)))

$(d)/$(BIN_$(d)).native: $(nati_deps_$(d))  $(DCMX_$(d))
$(d)/$(BIN_$(d)).native: INCLUDES:=$(foreach dep,$(DEPS_$(d)),-I $(PATH_TO_$(dep)))

BYTE_BINS:=$(BYTE_BINS) $(d)/$(BIN_$(d)).byte
NATI_BINS:=$(NATI_BINS) $(d)/$(BIN_$(d)).native

TARGETS_$(d):= $(TARGETS_$(d)) $(d)/$(BIN_$(d)).byte $(d)/$(BIN_$(d)).native
CLEAN_$(d):=$(CLEAN_$(d)) $(d)/$(BIN_$(d)).byte $(d)/$(BIN_$(d)).native

local_install_bin_byte_$(d): $(d)/$(BIN_$(d)).byte
	$(call echo_cmd,$(bold)$(green),INSTALL $<)$(INSTALL_PROGRAM) $< $(bindir)/$(<F)
local_install_bin_nati_$(d): $(d)/$(BIN_$(d)).native
	$(call echo_cmd,$(bold)$(green),INSTALL $<)$(INSTALL_PROGRAM) $< $(bindir)/$(<F)
local_uninstall_bin_byte_$(d):
	$(call echo_cmd,$(bold)$(green),UNINSTALL $(BIN_$(subst local_uninstall_bin_byte_,,$@)).byte)rm -f $(bindir)/$(BIN_$(subst local_uninstall_bin_byte_,,$@)).byte
local_uninstall_bin_nati_$(d):
	$(call echo_cmd,$(bold)$(green),UNINSTALL $(BIN_$(subst local_uninstall_bin_nati_,,$@)).native)rm -f $(bindir)/$(BIN_$(subst local_uninstall_bin_nati_,,$@)).native

else #No binary ? The install targets do nothing
local_install_bin_byte_$(d):
local_install_bin_nati_$(d):
local_uninstall_bin_byte_$(d):
local_uninstall_bin_nati_$(d):
endif


.PHONY: all_install_bin_byte_$(d) local_install_bin_byte_$(d) \
        all_install_bin_nati_$(d) local_install_bin_nati_$(d)

all_install_bin_byte_$(d): local_install_bin_byte_$(d) $(call subdirs_var,all_install_bin_byte,$(d))
all_install_bin_nati_$(d): local_install_bin_nati_$(d) $(call subdirs_var,all_install_bin_nati,$(d))

.PHONY: all_uninstall_bin_byte_$(d) local_uninstall_bin_byte_$(d) \
        all_uninstall_bin_nati_$(d) local_uninstall_bin_nati_$(d)

all_uninstall_bin_byte_$(d): local_uninstall_bin_byte_$(d) $(call subdirs_var,all_uninstall_bin_byte,$(d))
all_uninstall_bin_nati_$(d): local_uninstall_bin_nati_$(d) $(call subdirs_var,all_uninstall_bin_nati,$(d))

.PHONY: all_install_byte_$(d) local_install_byte_$(d) \
        all_install_nati_$(d) local_install_nati_$(d) \
        all_uninstall_byte_$(d) local_uninstall_byte_$(d) \
        all_uninstall_nati_$(d) local_uninstall_nati_$(d)

all_install_byte_$(d): all_install_bin_byte_$(d) all_install_lib_byte_$(d)
all_install_nati_$(d): all_install_bin_nati_$(d) all_install_lib_nati_$(d)
all_uninstall_byte_$(d): all_uninstall_bin_byte_$(d) all_uninstall_lib_byte_$(d)
all_uninstall_nati_$(d): all_uninstall_bin_nati_$(d) all_uninstall_lib_nati_$(d)
local_install_byte_$(d): local_install_bin_byte_$(d) local_install_lib_byte_$(d)
local_install_nati_$(d): local_install_bin_nati_$(d) local_install_lib_nati_$(d)
local_uninstall_byte_$(d): local_uninstall_bin_byte_$(d) local_uninstall_lib_byte_$(d)
local_uninstall_nati_$(d): local_uninstall_bin_nati_$(d) local_uninstall_lib_nati_$(d)


ifeq ($(strip $(TARGETS_$(d))),)
TARGETS_$(d):=$(foreach sd,$(FULLSUBDIRS_$(d)),$(TARGETS_$(sd)))
endif


# Proposing a short name for nice displays
ifneq ($(strip $(NAME_$(d))),)
SHORTNAME_$(d):=$(NAME_$(d))
DISPLAYNAME_$(d):=$(NAME_$(d))
else
SHORTNAME_$(d):=$(if $(subst $(TOP),,$(call unescolon,$(d))),$(subst /,_,$(subst $(TOP)/,,$(call unescolon,$(d)))),$(notdir $(TOP)))
DISPLAYNAME_$(d):=$(if $(subst $(TOP),,$(call unescolon,$(d))),$(subst $(TOP)/,,$(call unescolon,$(d))),$(notdir $(TOP)))
endif

.PHONY: all_dir_$(d) local_dir_$(d)

all_dir_$(d): local_dir_$(d) $(call subdirs_var,all_dir,$(d))
local_dir_$(d): $(TARGETS_$(d))

# tex_$(d) and html_$(d) can not be put in generic_rules because they are
# not files and pattern-rules do not seem to accept phony targets.
# Plus, we do not use $(d) in the commands because $(d) has changed by
# the time the command is executed. It is better to rely on the target
# name

.PHONY: local_tex_$(d) \
        local_dvi_$(d) \
        local_ps_$(d) \
        local_pdf_$(d) \
        local_html_$(d) \
        local_install_dvi_$(d) \
        local_uninstall_dvi_$(d) \
        local_install_ps_$(d) \
        local_uninstall_ps_$(d) \
        local_install_pdf_$(d) \
        local_uninstall_pdf_$(d) \
        local_install_html_$(d) \
        local_uninstall_html_$(d)

# We compile the documentation only if there is a name we can use for it
ifneq ($(strip $(NAME_$(d))),)

local_tex_$(d): INCLUDES:=$(foreach dep,$(DEPS_$(d)),-I $(PATH_TO_$(dep)))
# *.cmo are needed because we include *.ml files, hence the *.cmi they
# depend on must be compiled. We get the automated compilation through
# the dependencies
local_tex_$(d): $(DCMI_$(d)) $(DCMO_$(d))
local_tex_$(d):
	$(call echo_cmd,$(bold),TEX $(NAME_$(call escolon,$(subst local_tex_,,$@))))cd $(subst local_tex_,,$@) && \
	mkdir -p doc_tex && \
	ocamldoc -latex -d doc_tex -o doc_tex/$(NAME_$(call escolon,$(subst local_tex_,,$@))).tex -I $(subst local_tex_,,$@) $(INCLUDES) $(DMLI_$(call escolon,$(subst local_tex_,,$@))) $(DML_$(call escolon,$(subst local_tex_,,$@)))

local_dvi_$(d): local_tex_$(d)
	$(call echo_cmd,$(bold),DVI $(NAME_$(call escolon,$(subst local_dvi_,,$@))))cd $(subst local_dvi_,,$@)/doc_tex && \
	latex $(NAME_$(call escolon,$(subst local_dvi_,,$@))) && \
	latex $(NAME_$(call escolon,$(subst local_dvi_,,$@)))

local_ps_$(d): local_dvi_$(d)
	$(call echo_cmd,$(bold),PS $(NAME_$(call escolon,$(subst local_ps_,,$@))))cd $(subst local_ps_,,$@)/doc_tex && \
	dvips -o $(NAME_$(call escolon,$(subst local_ps_,,$@))).ps $(NAME_$(call escolon,$(subst local_ps_,,$@)).dvi

local_pdf_$(d): local_tex_$(d)
	$(call echo_cmd,$(bold),PDF $(NAME_$(call escolon,$(subst local_pdf_,,$@))))cd $(subst local_pdf_,,$@)/doc_tex && \
	pdflatex $(NAME_$(call escolon,$(subst local_pdf_,,$@))) && \
	pdflatex $(NAME_$(call escolon,$(subst local_pdf_,,$@)))

local_html_$(d): INCLUDES:=$(foreach dep,$(DEPS_$(d)),-I $(PATH_TO_$(dep)))
# Same remark about the *.cmo as for local_tex_$(d)
local_html_$(d): $(DCMI_$(d)) $(DCMO_$(d))
local_html_$(d):
	$(call echo_cmd,$(bold),HTML $(NAME_$(call escolon,$(subst local_html_,,$@))))cd $(subst local_html_,,$@) && \
	mkdir -p doc_html && \
	ocamldoc -html -d doc_html -I $(subst local_html_,,$@) $(INCLUDES) $(DMLI_$(call escolon,$(subst local_html_,,$@))) $(DML_$(call escolon,$(subst local_html_,,$@)))


local_install_dvi_$(d): local_dvi_$(d)
	$(call echo_cmd,$(uline)$(bold),INSTALL $(NAME_$(subst local_install_dvi_,,$@)))$(INSTALL_DATA) $(subst local_install_dvi_,,$@)/doc_tex/$(NAME_$(subst local_install_dvi_,,$@)).dvi $(dvidir)/$(NAME_$(subst local_install_dvi_,,$@)).dvi

local_uninstall_dvi_$(d):
	$(call echo_cmd,$(uline)$(bold),UNINSTALL $(NAME_$(subst local_uninstall_dvi_,,$@)))rm -f $(dvidir)/$(NAME_$(subst local_uninstall_dvi_,,$@)).dvi

local_install_ps_$(d): local_ps_$(d)
	$(call echo_cmd,$(uline)$(bold),INSTALL $(NAME_$(subst local_install_ps_,,$@)))$(INSTALL_DATA) $(subst local_install_ps_,,$@)/doc_tex/$(NAME_$(subst local_install_ps_,,$@)).ps $(psdir)/$(NAME_$(subst local_install_ps_,,$@)).ps

local_uninstall_ps_$(d):
	$(call echo_cmd,$(uline)$(bold),UNINSTALL $(NAME_$(subst local_uninstall_ps_,,$@)))rm -f $(psdir)/$(NAME_$(subst local_uninstall_ps_,,$@)).ps

local_install_pdf_$(d): local_pdf_$(d)
	$(call echo_cmd,$(uline)$(bold),INSTALL $(NAME_$(subst local_install_pdf_,,$@)))$(INSTALL_DATA) $(subst local_install_pdf_,,$@)/doc_tex/$(NAME_$(subst local_install_pdf_,,$@)).pdf $(pdfdir)/$(NAME_$(subst local_install_pdf_,,$@)).pdf

local_uninstall_pdf_$(d):
	$(call echo_cmd,$(uline)$(bold),UNINSTALL $(NAME_$(subst local_uninstall_pdf_,,$@)))rm -f $(pdfdir)/$(NAME_$(subst local_uninstall_pdf_,,$@)).pdf

local_install_html_$(d): local_html_$(d)
	$(call echo_cmd,$(uline)$(bold),INSTALL $(NAME_$(subst local_install_html_,,$@)))cp -a $(subst local_install_html_,,$@)/doc_html $(htmldir)/$(NAME_$(subst local_install_html_,,$@))

local_uninstall_html_$(d):
	$(call echo_cmd,$(uline)$(bold),UNINSTALL $(NAME_$(subst local_uninstall_html_,,$@)))rm -rf $(htmldir)/$(NAME_$(subst local_uninstall_html_,,$@))


else
local_tex_$(d):
local_dvi_$(d):
local_ps_$(d):
local_pdf_$(d):
local_install_dvi_$(d):
local_uninstall_dvi_$(d):
local_install_ps_$(d):
local_uninstall_ps_$(d):
local_install_pdf_$(d):
local_uninstall_pdf_$(d):
local_install_html_$(d):
local_uninstall_html_$(d):
endif


.PHONY: all_tex_$(d) \
        all_dvi_$(d) \
        all_ps_$(d) \
        all_pdf_$(d) \
        all_html_$(d)

all_tex_$(d): local_tex_$(d) $(call subdirs_var,all_tex,$(d))
all_dvi_$(d): local_dvi_$(d) $(call subdirs_var,all_dvi,$(d))
all_ps_$(d): local_ps_$(d) $(call subdirs_var,all_ps,$(d))
all_pdf_$(d): local_pdf_$(d) $(call subdirs_var,all_pdf,$(d))
all_html_$(d): local_html_$(d) $(call subdirs_var,all_html,$(d))


.PHONY: all_install_dvi_$(d) all_uninstall_dvi_$(d)
all_install_dvi_$(d): local_install_dvi_$(d) $(call subdirs_var,all_install_dvi,$(d))
all_uninstall_dvi_$(d): local_uninstall_dvi_$(d) $(call subdirs_var,all_uninstall_dvi,$(d))

.PHONY: all_install_ps_$(d) all_uninstall_ps_$(d)
all_install_ps_$(d): local_install_ps_$(d) $(call subdirs_var,all_install_ps,$(d))
all_uninstall_ps_$(d): local_uninstall_ps_$(d) $(call subdirs_var,all_uninstall_ps,$(d))

.PHONY: all_install_pdf_$(d) all_uninstall_pdf_$(d)
all_install_pdf_$(d): local_install_pdf_$(d) $(call subdirs_var,all_install_pdf,$(d))
all_uninstall_pdf_$(d): local_uninstall_pdf_$(d) $(call subdirs_var,all_uninstall_pdf,$(d))

.PHONY: all_install_html_$(d) all_uninstall_html_$(d)
all_install_html_$(d): local_install_html_$(d) $(call subdirs_var,all_install_html,$(d))
all_uninstall_html_$(d): local_uninstall_html_$(d) $(call subdirs_var,all_uninstall_html,$(d))


.PHONY: all_TAGS_$(d) local_TAGS_$(d)

all_TAGS_$(d): local_TAGS_$(d) $(call subdirs_var,all_TAGS,$(d))
local_TAGS_$(d): $(d)/TAGS
$(d)/TAGS: $(foreach dep,$(DEPS_$(d)),$(PATH_TO_$(dep))/TAGS)
$(d)/TAGS: $(DMLI_$(d)) $(DML_$(d))

.PHONY: all_clean_$(d) local_clean_$(d)

all_clean_$(d): local_clean_$(d) $(call subdirs_var,all_clean,$(d))
local_clean_$(d):
	$(call echo_cmd,$(bold),CLEAN $(SHORTNAME_$(call escolon,$(subst local_clean_,,$@))))rm -f $(CLEAN_$(call escolon,$(subst local_clean_,,$@))) && \
	rm -rf $(subst local_clean_,,$@)/doc_tex && \
	rm -rf $(subst local_clean_,,$@)/doc_html


# Now on to calculate the depend.mk files $(d) might actually need
# included by the top Makefile

ifneq ($(strip $(FILES_$(d))),)
DEPENDS_ON_$(d):=$(d)/depend.mk
else
DEPENDS_ON_$(d):=$(foreach fsd,$(FULLSUBDIRS_$(d)),$(DEPENDS_ON_$(fsd)))
endif

PATH_DEPS_$(d):=$(foreach dep,$(DEPS_$(d)),$(PATH_TO_$(dep)))
DEPENDS_ON_$(d):=$(sort $(DEPENDS_ON_$(d)) $(foreach pdep,$(PATH_DEPS_$(d)),$(DEPENDS_ON_$(pdep))))

