open Modules

(*
let scope_info = ref []
*)

let initialise_scope_env () =
  let ident_list_add = 
    ["ADD";"SUB";"BOOL";"TRUE";"FALSE";"MUL";"DIV";"STRING_WRITE";"STRING";"INT_WRITE"]
  in 
  let new_env = 
    List.fold_left (fun env id -> Scope.enter_value (Ident.create id) env) Scope.empty ident_list_add
  in 
  new_env

let scope_env_basic = 
  initialise_scope_env ()
