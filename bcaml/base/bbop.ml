(* $Id$ *)

(*
   defines the following types
   bop2  : for binary predicates
   op1   : for unary expressions
   op2   : for binary expressions
*)

(* bop2 o b i  *)
(*  op1 o u p  *)

(* \section{Op�rateurs binaires infixes} *)

type bop2 = 
  | Ou 
  | And 
  | Implies
  | Equiv
  | Equal
  | NotEqual
      
  | Less 
  | LessEqual 
  | Greater 
  | GreaterEqual 
      
  | SubSet
  | NotSubSet
  | StrictSubSet
  | NotStrictSubSet
  | In
  | NotIn

let bop2_name = function
    Ou -> "Or"
  | And -> "And"
  | Implies -> "Implies"
  | Equiv -> "Equiv"
  | Equal -> "Equal"
  | NotEqual -> "NotEqual"
  | Less -> "Less"
  | LessEqual -> "LessEqual"
  | Greater -> "Greater"
  | GreaterEqual -> "GreaterEqual"
  | SubSet -> "SubSet"
  | NotSubSet -> "NotSubSet"
  | StrictSubSet -> "StrictSubSet"
  | NotStrictSubSet -> "NotStrictSubSet"
  | In -> "In"
  | NotIn -> "NotIn"
        
let bop2_ascii = function
    Ou -> "or"
  | And -> "&"
  | Implies -> "=>"
  | Equiv -> "<=>"
  | Equal -> "="
  | NotEqual -> "/="
  | Less -> "<"
  | LessEqual -> "<="
  | Greater -> ">"
  | GreaterEqual -> ">="
  | SubSet -> "<:"
  | NotSubSet ->  "/<:"
  | StrictSubSet -> "<<:"
  | NotStrictSubSet -> "/<<:"
  | In -> ":"
  | NotIn -> "/:"

(* \section{op�rateurs unaire pr�fixes} *)
type op1 =
  | Closure 
  | ClosureOne 
  | Cod 
  | Perm
  | Min
  | Max
  | Card
  | Pow
  | PowOne
  | Fin
  | FinOne
  | Identity
  | Dom
  | Ran
  | UnionGen
  | InterGen
  | Seq
  | SeqOne
  | ISeq
  | ISeqOne
  | Size
  | First
  | Last
  | Front
  | Tail
  | Rev
  | Conc
  | RelFnc
  | FncRel
  | Tree
  | Btree
  | Bin1 (* the unary shape of the bin() operator *)
  | Left
  | Right
  | Infix
  | Top
  | Sons
  | Prefix
  | Postfix
  | SizeT
  | Mirror
  | Bool
  | Pred
  | Succ
  | Tilde
  | UMinus

type op2 = 
    Minus 
  | Puissance
  | Mul
  | Div
  | Mod
  | Plus 

  | NatSetRange
  
  | CartesianProd
  | Relation
  | PartialFunc
  | TotalFunc
  | PartialInj
  | TotalInj
  | PartialSurj
  | TotalSurj
  | PartialBij
  | TotalBij
      
  | OverRide
      
  | AppendSeq
  | PrependSeq
  | PrefixSeq
  | SuffixSeq
  | ConcatSeq
      
  | DomRestrict
  | RanRestrict
  | DomSubstract
  | RanSubstract

  | InterSets
  | UnionSets
  | SetMinus

  | DirectProd
  | Composition
  | ParallelComp
  | Image

  | Iterate
  | Prj1
  | Prj2


  | Consconst
  | Rank
  | Father
  | SubTree
  | Arity


type op3 =
  | Son
  | Bin3 (* the ternary shape of the bin() operator *)


(* *)
let affixe op =
  match op with
    Card | Front | Tail | Rev | RelFnc | FncRel |
    Dom | Ran | Pow | PowOne | Fin | FinOne | Min | Max
    -> Affix.Prefix
  | _ -> Affix.Infix
(* *)

let op1_ascii = function
  | Closure -> "closure"
  | ClosureOne -> "closure1"
  | Cod -> "cod"
  | Perm -> "perm"
  | Min -> "min"
  | Max -> "max"
  | Card -> "card"
  | Pow -> "POW"
  | PowOne -> "POW1"
  | Fin -> "FIN"
  | FinOne -> "FIN1"
  | Identity -> "id"
  | Dom -> "dom"
  | Ran -> "ran"
  | UnionGen -> "union"
  | InterGen -> "inter"
  | Seq -> "seq"
  | SeqOne -> "seq1"
  | ISeq -> "iseq"
  | ISeqOne -> "iseq1"
  | Size -> "size"
  | First -> "first"
  | Last -> "last"
  | Front -> "front"
  | Tail -> "tail"
  | Rev -> "rev"
  | Conc -> "conc"

  | Tree -> "tree"
  | Btree -> "btree"
  | Bin1 -> "bin"
  | Left -> "left"
  | Right -> "right"
  | Infix -> "infix"

  | Top -> "top"
  | Sons -> "sons"
  | Prefix -> "prefix"
  | Postfix -> "postfix"
  | SizeT -> "sizet"
  | Mirror -> "mirror"
        
  | RelFnc -> "rel"
  | FncRel -> "fnc"
  | Bool -> "bool"
  | Pred -> "pred"
  | Succ -> "succ"
  | Tilde -> "~"
  | UMinus -> "-"

let op2_ascii = function
    Minus -> "-"
  | SetMinus -> "\\"
  | Plus -> "+"
  | Puissance -> "**"
  | Mul -> "*"
  | Div -> "/"
  | Mod -> "mod"
  | NatSetRange -> ".."
(* FunName2 *)
  | CartesianProd -> "*"
  | Relation -> "<->"
  | PartialFunc -> "+->"
  | TotalFunc -> "-->"
  | PartialInj -> ">+>"
  | TotalInj -> ">->"
  | TotalSurj -> "-->>"
  | PartialSurj -> "+->>"
  | PartialBij -> ">+>>"
  | TotalBij -> ">->>"
        
  | OverRide -> "<+"

  | AppendSeq -> "<-"
  | PrependSeq -> "->"
  | PrefixSeq -> "/|\\"
  | SuffixSeq -> "\\|/"
  | ConcatSeq -> "^"
        
        
  | DomRestrict -> "<|"
  | RanRestrict -> "|>"
  | DomSubstract -> "<<|"
  | RanSubstract -> "|>>"
        
  | InterSets -> "/\\"
  | UnionSets -> "\\/"

  | DirectProd -> "><"
  | Composition -> ";"
  | ParallelComp -> "||"
  | Image -> "Im"

  | Iterate -> "iterate"

  | Prj1 -> "prj1"
  | Prj2 -> "prj2"

  | Consconst -> "consconst"
  | Rank -> "rank"
  | Father -> "father"
  | SubTree -> "subtree"
  | Arity -> "arity"

let op3_ascii = function
  | Son -> "son"
  | Bin3 -> "bin"

        
let op1_name = function
  | Closure -> "Closure"
  | ClosureOne -> "ClosureOne"
  | Cod -> "Cod"
  | Perm -> "Perm"
  | Min -> "Min"
  | Max -> "Max"
  | Card -> "Card"
  | Pow -> "Pow"
  | PowOne -> "PowOne"
  | Fin -> "Fin"
  | FinOne -> "FinOne"
  | Identity -> "Identity"
  | Dom -> "Dom"
  | Ran -> "Ran"
  | UnionGen -> "UnionGen"
  | InterGen -> "InterGen"
  | Seq -> "Seq"
  | SeqOne -> "SeqOne"
  | ISeq -> "ISeq"
  | ISeqOne -> "ISeqOne"
  | Size -> "Size"
  | First -> "First"
  | Last -> "Last"
  | Front -> "Front"
  | Tail -> "Tail"
  | Rev -> "Rev"
  | Conc -> "Conc"
        
  | Tree -> "Tree"
  | Btree -> "Btree"
  | Bin1 -> "Bin1"
  | Left -> "Left"
  | Right -> "Right"
  | Infix -> "Infix"
  | Prefix -> "Prefix"
  | Postfix -> "Postfix"

  | Top -> "Top"
  | Sons -> "Sons"
  | SizeT -> "SizeT"
  | Mirror -> "Mirror"

  | RelFnc -> "RelFnc"
  | FncRel -> "FncRel"
  | Bool -> "Bool"
  | Pred -> "Pred"
  | Succ -> "Succ"
  | Tilde -> "Tilde"
  | UMinus -> "UMinus"

let op2_name = function
    Minus -> "Minus"
  | SetMinus -> "SetMinus"
  | NatSetRange -> "NatSetRange"
  | Plus -> "Plus"
  | Puissance -> "Puissance"
  | Mul -> "Mul"
  | Div -> "Div"
  | Mod -> "Modulo"
(* FunName2 *)
  | CartesianProd -> "CartesianProd"
  | Relation -> "Relation"
  | PartialFunc -> "PartialFunc"
  | TotalFunc -> "TotalFunc"
  | PartialInj -> "PartialInj"
  | TotalInj -> "TotalInj"
  | PartialSurj -> "PartialSurj"
  | TotalSurj -> "TotalSurj"
  | PartialBij -> "PartialBij"
  | TotalBij -> "TotalBij"
        
  | OverRide -> "OverRide"
        
  | AppendSeq -> "AppendSeq"
  | PrependSeq -> "PrependSeq"
  | PrefixSeq -> "PrefixSeq"
  | SuffixSeq -> "SuffixSeq"
  | ConcatSeq -> "ConcatSeq"
        
  | DomRestrict -> "DomRestrict"
  | RanRestrict -> "RanRestrict"
  | DomSubstract -> "DomSubstract"
  | RanSubstract -> "RanSubstract"

  | InterSets ->  "InterSets"
  | UnionSets ->  "UnionSets"

  | DirectProd -> "DirectProd"
  | Composition -> "Composition"
  | ParallelComp -> "ParallelComp"
  | Image -> "Image"

  | Iterate -> "Iterate"

  | Prj1 -> "PrjOne"
  | Prj2 -> "PrjTwo"

  | Consconst -> "Consconst"
  | Rank -> "Rank"
  | Father -> "Father"
  | SubTree -> "SubTree"
  | Arity -> "Arity"


let op3_name = function
    Son -> "Son"
  | Bin3 -> "Bin3"


(*i
let string_to_bbop str =
  match str with
    "or" -> Ou
  | "&" -> And  
  | "=>" -> Implies  
  | "<=>" -> Equiv  
  | "=" -> Equal  
  | "/=" -> NotEqual  
  | "<" -> Less  
  | "<=" -> LessEqual  
  | ">" -> Greater  
  | ">=" -> GreaterEqual  
  | "<:" -> SubSet  
  | "/<:" -> NotSubSet   
  | "<<:" -> StrictSubSet  
  | "/<<:" -> NotStrictSubSet  
  | _ -> raise (Invalid_argument "string_to_bbop")
i*)

