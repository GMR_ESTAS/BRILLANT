(* $Id$ *)

(** B Project management *)

type machine_context = { 
  machine : Blast.amn; 
  context : Modules.Scope.t; 
}

type machine_assoc = Blast.id * machine_context

module OrderedMachineAssoc :
  sig type t = machine_assoc val compare : 'a * 'b -> 'a * 'c -> int end

module MachineSet :
  sig
    type elt = OrderedMachineAssoc.t
    type t = Set.Make(OrderedMachineAssoc).t
    val empty : t
    val is_empty : t -> bool
    val mem : elt -> t -> bool
    val add : elt -> t -> t
    val singleton : elt -> t
    val remove : elt -> t -> t
    val union : t -> t -> t
    val inter : t -> t -> t
    val diff : t -> t -> t
    val compare : t -> t -> int
    val equal : t -> t -> bool
    val subset : t -> t -> bool
    val iter : (elt -> unit) -> t -> unit
    val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
    val for_all : (elt -> bool) -> t -> bool
    val exists : (elt -> bool) -> t -> bool
    val filter : (elt -> bool) -> t -> t
    val partition : (elt -> bool) -> t -> t * t
    val cardinal : t -> int
    val elements : t -> elt list
    val min_elt : t -> elt
    val max_elt : t -> elt
    val choose : t -> elt
    val split : elt -> t -> t * bool * t
  end


type t = { 
  graph : Blast.id Bgraph.t; 
  machines : MachineSet.t; 
} (** A B project is a set of machines and a graph of dependencies *)


val emptyProject : t
  (** Special value to denote an empty project *)

val project_to_string : t -> string

val mem_name : t -> string -> bool

val mem : t -> Blast.id -> bool

val find_name : t -> string -> MachineSet.elt

val find : t -> Blast.id -> MachineSet.elt

val get_project_id : t -> Blast.id

val get_root_machine : t -> MachineSet.elt

val add :
  t -> Blast.id Bgraph.edge list -> MachineSet.elt list -> t

val merge : t -> t -> t

val replace : t -> Blast.id -> t -> t

val subproject : t -> Blast.id -> t

val all_subprojects : t -> Bgraph.linkType -> Blast.id -> t list

val get_context : t -> Modules.Scope.t
val getMoreAbstractId : t -> Blast.id -> Blast.id
val getMoreConcreteId : t -> Blast.id -> Blast.id
val get_ops_names : Blast.amn -> string list

val find_machine_with_opname : Blast.amn list -> string -> string list
val find_machine : string -> Blast.amn list -> Blast.amn

val addRenamesLinks :
  (string * Bgraph.linkType * string) list ->
  (string * Bgraph.linkType * string) list

val build_dependencies :
  Blast.amn -> Blast.amn list -> (string * Bgraph.linkType * string) list
val prefix_source :
  string -> Modules.Scope.t -> Blast.id * 'a * 'b -> Blast.id * 'a * 'b
val prefix_edge :
  string ->
  Modules.Scope.t -> Blast.id * 'a * Blast.id -> Blast.id * 'a * Blast.id
val getRenamed : t -> Blast.id -> MachineSet.elt
val prefixProject :
  (string, t) Hashtbl.t ->
  t ->
  t -> Modules.Scope.t -> string -> Id_handling.Idset.elt -> t
val findMachine : Blast.amn list -> string -> Blast.amn
val transform_edge :
  Blast.id list -> string * 'a * string -> Blast.id * 'a * Blast.id
val buildProject_aux :
  (string, t) Hashtbl.t ->
  t ->
  Blast.amn list ->
  (string * Bgraph.linkType * string) list -> string -> t


val projectRepository : (string, t) Hashtbl.t

val machines_ids : t -> Blast.id list

val buildProject : Blast.amn list -> t
