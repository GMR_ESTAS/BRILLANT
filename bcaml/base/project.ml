(* $Id$ *)

(* open Parser_lib *)

(* *)
open Bgraph 
(* *)

(* This module provides functions that allows to deal with B \emph{projects},
   that is gives us ways to treat dependencies between B machines. It is still
   strongly related to parsing files, but in the long term parsing functions
   will only appear in the parser, and other tools will call the bxml
   parser. *)


type machine_context = { machine : Blast.amn; context : Modules.Scope.t}
type machine_assoc = Blast.id * machine_context

module OrderedMachineAssoc=
struct
  type t=machine_assoc
(* As the identifiers are meant to be unique in a B project, let's use an
   efficient comparison function *)
  let compare mc1 mc2=
    Pervasives.compare (fst mc1) (fst mc2)
end 

module MachineSet=Set.Make(OrderedMachineAssoc)

type t = { graph : Blast.id Bgraph.t; machines : MachineSet.t }
let emptyProject = {graph=Bgraph.emptyGraph; machines=MachineSet.empty}


let project_to_string proj=
  let machines=MachineSet.elements proj.machines in
  let strgraph=Bgraph.graph_to_string Id_acc.debug_id proj.graph in
  let strmachines=
    List.fold_left
      (fun str imc -> str ^ " ; " ^ (Id_acc.debug_id (fst imc)))
      ""
      machines
  in
    "Project graph :\n" ^ strgraph ^ "\n Machines in the project :\n" ^ strmachines


let mem_name proj name=
  MachineSet.exists
    (fun idmch -> (Id_acc.name_of (fst idmch)) = name)
    proj.machines


(* The id should correspond to the scoped one *)
let mem proj id=
  MachineSet.exists
    (fun idmch -> (fst idmch) = id)
    proj.machines


let find_name proj name=
  let all_found=
    MachineSet.filter
      (fun idmch -> (Id_acc.name_of (fst idmch)) = name)
      proj.machines
  in
    try
      List.hd (MachineSet.elements all_found)
    with Failure _ -> raise Not_found


(* The id should correspond to the scoped one *)
let find proj id=
  let all_found=
    MachineSet.filter
      (fun idmch -> (fst idmch) = id)
      proj.machines
  in
    try
      List.hd (MachineSet.elements all_found)
    with Failure _ -> raise Not_found


let get_project_id proj=
  try
    if proj.graph=Bgraph.emptyGraph
    then 
      fst (List.hd (MachineSet.elements proj.machines))
    else 
	Bgraph.get_root_node proj.graph
  with
  | Bgraph.No_root_node
  | Bgraph.Several_root_nodes
  | Failure _ -> invalid_arg "get_project_id"


let get_root_machine proj=
  if proj.graph=Bgraph.emptyGraph
  then 
    try List.hd (MachineSet.elements proj.machines)
    with Failure _ -> invalid_arg "get_root_machine"
  else 
    let rootid=
      try 
	  Bgraph.get_root_node proj.graph
      with
      | Bgraph.No_root_node
      | Bgraph.Several_root_nodes -> invalid_arg "get_root_machine"
    in
      find proj rootid



(* \emph{machconts} is a machine_assoc list to be added to the project
   \emph{proj}. \emph{deps} is a graph (possibly empty).  *)

let add proj deps machassocs=
  let newmachines=
    List.fold_left
      (fun s x -> MachineSet.add x s)
      proj.machines
      machassocs
  in
  let newdeps=Bgraph.merge proj.graph deps in
    {graph=newdeps; machines=newmachines}


let merge proj1 proj2=
  {graph=Bgraph.merge proj1.graph proj2.graph;
   machines=MachineSet.union proj1.machines proj2.machines
  }


(* The only legal operations in a B project are adding machines
   including, using,etc the ones already present, and replacing one
   machine and its dependencies by something equivalent (this function is
   written with unfolding in mind). Removing would cause the presence of
   multiples root nodes (suppose you remove the main machine from a
   project, for example). *)


(* This function replaces \emph{x} in \emph{proj} and all the machines
   it depends on (except if these machines are needed by other machines
   not appearing in \emph{x} dependencies), by \emph{newproj}, which is a
   project that should provide the same functionalities as \emph{x}. Note
   that \emph{newproj} may be constituted by only one machine *)

let replace proj x newproj=
  let oldsubgraph=Bgraph.subgraph proj.graph x in
  let newgraph=List.fold_left Bgraph.remove proj.graph (Bgraph.to_list oldsubgraph) in  

  let deps_to_change=Bgraph.filter_target newgraph x in
  let newlocalroot=fst (get_root_machine newproj) in
    
  let change_target y edge=
    Bgraph.mk_edge (source edge) (dependency edge) y
  in

  let newdeps=List.map (change_target newlocalroot) deps_to_change in

  let withoutolddeps=List.fold_left Bgraph.remove newgraph deps_to_change in
  let withnewdeps=List.fold_left Bgraph.add withoutolddeps newdeps in

(* Now we have the final graph. We should remove the unused machines
   from the machines set, and finally merge the two graphs *)

  let present_nodes=Bgraph.nodes withnewdeps in
  let purged_mchset=
    MachineSet.filter
      (fun machcont -> List.mem (fst machcont) present_nodes)
      proj.machines
  in
    { graph=Bgraph.merge withnewdeps newproj.graph;
      machines=MachineSet.union purged_mchset newproj.machines
    }
    

let subproject proj id=
  Error.debug ("Building subproject for " ^ (Id_acc.name_of id));
  let newgraph=Bgraph.subgraph proj.graph id in
  let remainingmchs=
    if newgraph=[] 
    then [id]
    else Bgraph.nodes newgraph 
  in
  let purged_mchset=
    MachineSet.filter
      (fun machcont -> List.mem (fst machcont) remainingmchs)
      proj.machines
  in
  let subproj={graph=newgraph; machines=purged_mchset} 
  in
    subproj


let all_subprojects proj dependency id=
  let linktargets=Bgraph.filter 
		    proj.graph
		    (fun edge -> 
		       ((Bgraph.dependency edge)=dependency)
		       && ((Bgraph.source edge)=id)
		    )
  in
  let targetids=List.map Bgraph.target linktargets in
    List.map (subproject proj) targetids 

 
let get_context proj=
  let rootmch=get_root_machine proj in
    (snd rootmch).context
    

let getMoreAbstractId project mchid=
  let rec auxGet proj id=
    let refEdges=Bgraph.filter_targetDep proj.graph Bgraph.RefinedBy id in
      match refEdges with
	| [] -> id
	| [alone] -> auxGet proj (Bgraph.source alone)
	| _ -> invalid_arg "getMoreAbstractId : more than one machine refined"
  in
    auxGet project mchid


let getMoreConcreteId project mchid=
  let rec auxGet proj id=
    let refEdges=Bgraph.filter_sourceDep proj.graph Bgraph.RefinedBy id in
      match refEdges with
	| [] -> id
	| [alone] -> auxGet proj (Bgraph.target alone)
	| _ -> invalid_arg "getMoreConcreteId : more than one refinement"
  in
    auxGet project mchid



(* The construction of a project needs 2 phases : first the construction of a
   dependencies graph so to make the scoping phase in correct order (the built
   graph has strings as nodes), then with the help of that graph we can build the
   definitive graph with correctly scoped machines, ids, and scoping contexts in
   the repository. The following functions handle these tasks *) 


(* We make the assumption that all the machines in the different functions
   parameters aren't scoped, because building scoped project is the purpose of
   this module *)


let get_ops_names machine=
  List.map
    Id_acc.name_of
    ( List.map 
	Clause_acc.get_operation_id
	(Clause_acc.get_Operations (Blast_acc.get_clause_list machine))
    )

(* This function retrieves all the machines in the list defining an operation
whose name is \emph{opname} *)
let rec find_machine_with_opname machine_list opname=
  match machine_list with
    | [] -> []
    | mch::others -> 
	let mchname=Blast_acc.get_amn_head_name (Blast_acc.get_head mch) in
	let opsnames=get_ops_names mch in
	  if List.mem opname opsnames 
	  then mchname::(find_machine_with_opname others opname)
	  else find_machine_with_opname others opname


let rec find_machine name mchlist=
  match mchlist with
    | [] -> raise Not_found
    | mch::others -> 
	if (Blast_acc.get_amn_head_name (Blast_acc.get_head mch))=name
	then mch
	else find_machine name others


let addRenamesLinks namesGraph=
  let rec auxAdd nodes=
    if nodes <> [] then
      let prefixedNodes=List.filter Id_handling.isPrefixedName nodes in
      let newNodes=List.map Id_handling.removePrefixName prefixedNodes in
      let newEdges=
	List.map2 
	  (fun n1 n2 -> Bgraph.mk_edge n1 Bgraph.Renames n2) 
	  prefixedNodes newNodes 
      in
	newEdges@(auxAdd newNodes)
    else []
  in

  let allNewEdges=auxAdd (Bgraph.nodes namesGraph) in
    List.fold_left Bgraph.add namesGraph allNewEdges



(* This function takes a non-scoped machine, a list of non-scoped machines,
and returns the dependencies of the machine. The machines list is here in case
there is an PROMOTES dependency, and in that case we need to know from which
machine the promoted operations come. Hypothesis : the machines are correctly
built (promoted operations come from machines appearing in includes or imports
clause, etc) and the machines from which some operations are promoted appear
in the \emph{machine_list}.*)

let build_dependencies machine machines_list=
  let machine_name=Blast_acc.get_amn_head_name (Blast_acc.get_head machine) in
  Error.debug ("Building dependency graph for : " ^ machine_name);
  let seenids=Blast_acc.get_seen_mchids machine in
  let seennames=List.map Id_acc.name_of seenids in
  let usedids=Blast_acc.get_used_mchids machine in
  let usednames=List.map Id_acc.name_of usedids in
  let includedids=Blast_acc.get_included_mchids machine in
  let includednames=List.map Id_acc.name_of includedids in
  let importedids=Blast_acc.get_imported_mchids machine in
  let importednames=List.map Id_acc.name_of importedids in
  let extendedids=Blast_acc.get_extended_mchids machine in 
  let extendednames=List.map Id_acc.name_of extendedids in
  let promoted_opids=Clause_acc.get_Promotes (Blast_acc.get_clause_list machine) in
  let promoted_opnames=List.map Id_acc.name_without_prefix promoted_opids in
(* We must remove prefixes, because the \emph{machines_list} contains
   non-scoped, non-prefixed machines *)

  let refined_id=
    try Some(Blast_acc.get_refined_name machine)
    with
      Blast_acc.Not_a_refinement
    | Blast_acc.Empty_tree -> None
  in

  let build_edges mchname link linked_names=
    List.map (Bgraph.mk_edge mchname link) linked_names
  in

  let modules_list=
    List.flatten
      ( List.map2 
	  (build_edges machine_name)    
	  [Sees; Uses; Includes; Imports; Extends]
	  [seennames; usednames; includednames; importednames; extendednames]
      )
  in

  let promotables=
    List.map 
      (fun id -> find_machine (Id_acc.name_without_prefix id) machines_list)
      (includedids@importedids)
  in
  let promoted_mchnames=
    List.flatten
      ( List.map
	  (find_machine_with_opname promotables)
	  promoted_opnames
      )
  in
  let promoted_list=build_edges machine_name Promotes promoted_mchnames in
  let almost_done=List.fold_left Bgraph.add Bgraph.emptyGraph (modules_list@promoted_list) in
  let fulldone=
    match refined_id with
      | None -> almost_done
      | Some(refid) -> 
	    Bgraph.add almost_done (mk_edge (Id_acc.name_of refid) RefinedBy machine_name)
  in
  let addedRenamings=addRenamesLinks fulldone in
  (* let _=Error.debug ("Obtained graph:\n" ^ (Bgraph.graph_to_string (fun s -> s) addedRenamings)) in *)
    addedRenamings



(*
let rec prefix_project proj_rep scope_context prefix proj=

  let prefix_source prefix scope_env edge= 
    let nameSource=Id_acc.name_of (Bgraph.source edge) in
    let prefNameSource=prefix ^ "." ^ nameSource in
    let (newSource,_)=Bscoping.scope_id_module scope_env (Blast_mk.mk_Id prefNameSource) in
      Bgraph.mk_edge newSource (Bgraph.dependency edge) (Bgraph.target edge)
  in
    
  let prefix_edge prefix scope_env edge= 
    let nameSource=Id_acc.name_of (Bgraph.source edge) in
    let prefNameSource=prefix ^ "." ^ nameSource in
    let (newSource,_)=Bscoping.scope_id_module scope_env (Blast_mk.mk_Id prefNameSource) in
    let nameTarget=Id_acc.name_of (Bgraph.target edge) in
    let prefNameTarget=prefix ^ "." ^ nameTarget in
    let (newTarget,_)=Bscoping.scope_id_module scope_env (Blast_mk.mk_Id prefNameTarget) in
      Bgraph.mk_edge newSource (Bgraph.dependency edge) newTarget
  in

  let rec auxPrefix proj_rep scope_context prefix alreadyDone proj=
    if proj.graph=Bgraph.emptyGraph
    then 

      let machassoc=get_root_machine proj in
      let mchname=Id_acc.name_of (Blast_acc.get_name (snd machassoc).machine)  in
	Error.debug ("Prefixing the project :" ^ (prefix ^ "." ^ mchname) ^ "\n");
	if Hashtbl.mem proj_rep (prefix ^ "." ^ mchname)
	then Hashtbl.find proj_rep (prefix ^ "." ^ mchname)
	else
	  let prefixables=(fun () -> Instanciation.prefixable_identifiers_from (snd machassoc).machine) in
	  let prefixed=Instanciation.prefix_machine prefixables prefix (snd machassoc).machine in

	  let (scoped_mch, scope_env)=Bscoping.scope_amn scope_context prefixed  in
	  let _=Error.debug ("Just leaf-scoped the prefixed machine : " ^ (prefix ^ "." ^ mchname)) in
	  let machcont={machine=scoped_mch; context=scope_env} in
	  let prefixedproject=
	    add emptyProject Bgraph.emptyGraph [((Blast_acc.get_name scoped_mch), machcont)]
	  in
	  let renamesLink=Bgraph.mk_edge (Blast_acc.get_name scoped_mch) Renames (fst machassoc) in
	  let newProject=
	    add prefixedproject 
	      (Bgraph.add Bgraph.emptyGraph renamesLink)
	      [machassoc]
	  in
	  let finallyDone=merge alreadyDone newProject in
	    begin 
	      Hashtbl.add proj_rep (prefix ^ "." ^ mchname) finallyDone;
	      Error.debug ("Done prefixing :" ^ (prefix ^ "." ^ mchname) ^ "\n");
	      finallyDone
	    end
    else

      let mchid=get_project_id proj in
      let mchname=Id_acc.name_of mchid in
	Error.debug ("Prefixing the project :" ^ (prefix ^ "." ^ mchname) ^ "\n");
	if Hashtbl.mem proj_rep (prefix ^ "." ^ mchname)
	then Hashtbl.find proj_rep (prefix ^ "." ^ mchname)
	else
	  let alldeps=Bgraph.filter_source proj.graph mchid in
	  let (refDeps, notrefDeps)=
	    List.partition (fun e -> Bgraph.dependency e=Bgraph.RefinedBy) alldeps 
	  in
	  let dependencies=Bgraph.sort_edges notrefDeps in
	  let (prefixDep, notprefixDep)=List.partition Bgraph.prefixable_dependency dependencies in

	  let to_prefix=List.map (subproject proj) (List.map Bgraph.target prefixDep) in
	  let as_is=List.map  (subproject proj) (List.map Bgraph.target notprefixDep) in

	  let prefixed=List.map (auxPrefix proj_rep scope_context prefix alreadyDone) to_prefix in
	  let prefixed_contexts=List.map (fun p -> (snd (get_root_machine p)).context ) prefixed in
	  let normal_contexts=List.map (fun p -> (snd (get_root_machine p)).context ) as_is in
	  let unified_contexts=
	    List.fold_left Modules.Scope.merge Modules.Scope.empty (prefixed_contexts@normal_contexts)
	  in

	  let machassoc=get_root_machine proj in
	  let prefixables=(fun () -> Instanciation.prefixable_identifiers_from (snd machassoc).machine) in
	  let prefmch=Instanciation.prefix_machine prefixables prefix (snd machassoc).machine in
	  let (scoped_mch, scope_env)=Bscoping.scope_amn unified_contexts prefmch  in
	  let _=Error.debug ("Just node-scoped the prefixed machine : " ^ (prefix ^ "." ^ mchname)) in
	  let machcont={machine=scoped_mch; context=scope_env} in
	  let newPrefDep=List.map (prefix_edge prefix scope_env) prefixDep in
	  let newNotPrefDep=List.map (prefix_source prefix scope_env) notprefixDep in
	  let mergedDep=List.fold_left merge emptyProject (as_is@prefixed) in
	  let newGraph=List.fold_left Bgraph.add Bgraph.emptyGraph (newNotPrefDep@newPrefDep) in
	  let prefixedProject=add mergedDep newGraph [((Blast_acc.get_name scoped_mch), machcont)] in
	  let almostProject=merge prefixedProject alreadyDone in
	  let newproject=
	    {graph=Bgraph.add 
		     almostProject.graph 
		     (Bgraph.mk_edge (Blast_acc.get_name scoped_mch) Renames (fst machassoc));
	     machines=almostProject.machines
	    }
	  in
	    Hashtbl.add proj_rep (Id_acc.name_of (Blast_acc.get_name scoped_mch)) newproject;
	    Error.debug ("Done prefixing :" ^ (prefix ^ "." ^ mchname) ^ "\n");
	    match refDeps with
	      | [] -> newproject
	      | [alone] -> 
		  let refProject=subproject proj (Bgraph.target alone) in
		    auxPrefix proj_rep machcont.context prefix newproject refProject
	      | _ -> invalid_arg "prefix_project : more than one refinement for a machine"

  in
    auxPrefix proj_rep scope_context prefix proj proj
 *)


(* This function takes a list of not-yet scoped B machines list, and builds a
   project structure. Hypothesis : the machines are all part of a project, with
   no one missing, and they are correctly built (i.e. all operation names are
   different, etc). The interesting part is that, as the prefixed machines are
   already dealt with through a graph of machines names (i.e. strings), we don't
   have to worry about the "problem" of unicity of identifiers when they are
   created. *)

(*
let build_project machines_list=
  if machines_list=[] then 
    begin
      Error.debug (project_to_string emptyProject);
      emptyProject
    end
  else
    let machines_graphs=
      List.map
	(fun mch -> build_dependencies mch machines_list)
	machines_list
    in

    let project_namegraph=List.fold_left Bgraph.merge Bgraph.emptyGraph machines_graphs in
    let root_name=
      if project_namegraph=[]
      then 
	if (List.length machines_list) = 1
	then Id_acc.name_of (Blast_acc.get_name (List.hd machines_list))
	else invalid_arg "build_project : no dependencies between machines"
      else
	begin
	  Error.debug ("Getting root node for :\n" ^ (Bgraph.graph_to_string (fun s -> s) project_namegraph ));
	  Bgraph.get_root_node project_namegraph
	end
    in
      

    let repository=Hashtbl.create 50 in
    let basic_context=Benv.scope_env_basic in

    let transform_edge ids nameedge=
      let sourceid=Id_handling.find_by_name (Bgraph.source nameedge) ids in
      let targetid=Id_handling.find_by_name (Bgraph.target nameedge) ids in
	Bgraph.mk_edge sourceid (Bgraph.dependency nameedge) targetid 
    in
      
    let rec aux_build_project proj_rep alreadyDone mchname=
      if Hashtbl.mem proj_rep mchname
      then Hashtbl.find proj_rep mchname
      else

	let unprefixed=Id_handling.removePrefixName mchname in
	  if unprefixed <> mchname 
	  then 
	    let bare_project=aux_build_project proj_rep alreadyDone unprefixed in
	    let prefix=Id_handling.prefixFromName mchname in

	    let prefixed_project=prefix_project proj_rep basic_context prefix bare_project in
	      prefixed_project
	  else 

	    let mch=find_machine mchname machines_list in
	    let alldepends=filter_source project_namegraph mchname in
	    let (refDeps, notrefDeps)=
	      List.partition (fun e -> (Bgraph.dependency e)=Bgraph.RefinedBy) alldepends in
	    let depends=Bgraph.sort_edges notrefDeps in
	    let dependnodes=List.map Bgraph.target depends in
	      match (depends@refDeps) with
		| [] -> 
		    begin
		      Error.debug ("Building project for the leaf : " ^ mchname);

		      let (scoped_mch, scope_env)=Bscoping.scope_amn basic_context mch in
		      let machcont={machine=scoped_mch; context=scope_env} in
		      let newproject=
			add emptyProject Bgraph.emptyGraph [((Blast_acc.get_name scoped_mch), machcont)]
		      in
			Hashtbl.add proj_rep mchname newproject;
			Error.debug ("Just leaf-scoped,etc... " ^ mchname);
			newproject
		    end
		| _ -> 
		    begin
		      Error.debug ("Building project for the node : " ^ mchname);
		      let depended_projects=
			List.map (aux_build_project proj_rep alreadyDone) dependnodes 
		      in
			Error.debug ("Built depended projects for the node : " ^ mchname);
		      let depended_graphs=List.map (fun proj -> proj.graph) depended_projects in
		      let depended_ids=List.map 
					 (fun p -> fst (get_root_machine p))
					 depended_projects
		      in
		      let depended_contexts=List.map get_context depended_projects in

		      let unified_contexts=
			List.fold_left Modules.Scope.merge Modules.Scope.empty depended_contexts 
		      in
		      let almost_project=List.fold_left merge emptyProject depended_projects in
		      let (scoped_mch, scope_env)=Bscoping.scope_amn unified_contexts mch in
		      let machcont={machine=scoped_mch; context=scope_env} in
		      let newdeps=
			List.map (transform_edge ((Blast_acc.get_name scoped_mch)::depended_ids)) depends 
		      in
		      let newgraph=List.fold_left Bgraph.add Bgraph.emptyGraph newdeps in
		      let newproject=
			add almost_project newgraph [((Blast_acc.get_name scoped_mch), machcont)]
		      in
			Hashtbl.add proj_rep mchname newproject;
			Error.debug ("Just node-scoped,etc... " ^ mchname);
			match refDeps with
			  | [] -> newproject
			  | [alone] ->
			      let refName=Bgraph.target alone in
				aux_build_project proj_rep newproject refName
			  | _ -> invalid_arg "build_project : more than one refinement"
		    end
    in
      
    let built_project=aux_build_project repository emptyProject root_name in
      Error.debug ("\n" ^ (project_to_string built_project) ^ "\n");
      built_project
 *)

		
let prefix_source prefix scope_env edge= 
  let nameSource=Id_acc.name_of (Bgraph.source edge) in
  let prefNameSource=Id_handling.prefixName prefix nameSource in
  let (newSource,_)=Bscoping.scope_id_module scope_env (Blast_mk.mk_Id prefNameSource) in
    Bgraph.mk_edge newSource (Bgraph.dependency edge) (Bgraph.target edge)
    

let prefix_edge prefix scope_env edge= 
  let nameSource=Id_acc.name_of (Bgraph.source edge) in
  let prefNameSource=Id_handling.prefixName prefix nameSource in
  let (newSource,_)=Bscoping.scope_id_module scope_env (Blast_mk.mk_Id prefNameSource) in
  let nameTarget=Id_acc.name_of (Bgraph.target edge) in
  let prefNameTarget=Id_handling.prefixName prefix nameTarget in
  let (newTarget,_)=Bscoping.scope_id_module scope_env (Blast_mk.mk_Id prefNameTarget) in
    Bgraph.mk_edge newSource (Bgraph.dependency edge) newTarget


let getRenamed proj mchid=
  if Id_handling.isPrefixed mchid then
    let renDeps=Bgraph.filter_sourceDep proj.graph Renames mchid in
      try
	let renamed=Bgraph.target (List.hd renDeps) in
	  find proj renamed
      with
	  Failure _ -> invalid_arg "getRenamed"
  else invalid_arg "getRenamed"


(* Hypothesis : All the machines that needs prefixing can be found in \emph{proj} *)
let rec prefixProject projRep proj prefixedPart context prefix mchId=
  let prefixedName=Id_handling.prefixName prefix (Id_acc.name_of mchId) in
    Error.debug ("Prefixing project:" ^ prefixedName);
    if Hashtbl.mem projRep prefixedName
    then 
      let foundProject=Hashtbl.find projRep prefixedName in
      let _=Error.debug ("Found prefixed project:" ^ prefixedName) in
	foundProject
    else
      let deps=Bgraph.filter_source proj.graph mchId in
      let (refDeps, notRefDeps)=
	List.partition (fun e -> (Bgraph.dependency e)=Bgraph.RefinedBy) deps in
      let sortedDeps=Bgraph.sort_edges notRefDeps in
      let (prefixDeps, notprefixDeps)=List.partition Bgraph.prefixable_dependency sortedDeps in
	
      let toPrefixId=List.map Bgraph.target prefixDeps in
(*      let toPrefix=List.map (subproject proj) toPrefixId in *)
      let asIs=List.map  (subproject proj) (List.map Bgraph.target notprefixDeps) in
      let baseMachAssoc=find proj mchId in
      let baseMachine=(snd baseMachAssoc).machine in
	
      let _=Error.debug ("Prefixing the depended machines for:" ^ prefixedName) in
      let prefixed=List.map (prefixProject projRep proj emptyProject context prefix) toPrefixId in
      let prefixedContexts=List.map (fun p -> (snd (get_root_machine p)).context ) prefixed in
      let normalContexts=List.map (fun p -> (snd (get_root_machine p)).context ) asIs in
      let contextUnified=
	List.fold_left 
	  Modules.Scope.merge 
	  Modules.Scope.empty 
	  (context::(prefixedContexts@normalContexts))
      in
      let _=Error.debug ("Prefixing the machine:" ^ prefixedName) in
(*      let _=Error.debug (Bxml.to_string baseMachine) in *)
(* This is the abstract machine from which we'll get the variables to
   prefix, therefore it is not prefixed *)
      let abstractInterface=
	if Blast_acc.is_abstract_machine baseMachine
	then baseMachine
	else 
	  let abstractId=get_project_id prefixedPart in
	  let renamedOne=getRenamed prefixedPart abstractId in
	    (snd renamedOne).machine
      in

      let includedPrefixables=
	List.map 
	  (fun id -> let (_, {machine=mch;context=_})=find proj id in
	     Instanciation.prefixable_identifiers_from mch)
	  toPrefixId
      in
      let prefixables=(fun () -> 
			 mchId::
			 ((Instanciation.prefixable_identifiers_from abstractInterface)@
			  (List.flatten includedPrefixables)))  
      in
      let prefixedMch=Instanciation.prefix_machine prefixables prefix baseMachine in
      let _=Error.debug ("Scoping the newly prefixed machine:" ^ prefixedName) in
(*      let _=Error.debug (Bxml.to_string prefixedMch) in *)
      let (scopedMch, scopeEnv)=Bscoping.scope_amn contextUnified prefixedMch  in
      let _=Error.debug ("Just scoped the machine:" ^ prefixedName) in
(*      let _=Error.debug (Bxml.to_string scopedMch) in *)
      let scopedMchId=Blast_acc.get_name scopedMch in
      let machCont={machine=scopedMch; context=scopeEnv} in

      let newPrefDeps=List.map (prefix_edge prefix scopeEnv) prefixDeps in
      let newNotPrefDeps=List.map (prefix_source prefix scopeEnv) notprefixDeps in
      let renamingDep=Bgraph.mk_edge scopedMchId Renames mchId in
      let allDeps=
	if Blast_acc.is_abstract_machine baseMachine 
	then (renamingDep::(newNotPrefDeps@newPrefDeps@notRefDeps))
	else
	  let refPrefDep=Bgraph.mk_edge (Blast_acc.get_refined_name scopedMch) RefinedBy scopedMchId in
	  let upperRefDep=Bgraph.mk_edge (Blast_acc.get_refined_name baseMachine) RefinedBy mchId in
	    refPrefDep::(upperRefDep::(renamingDep::(newNotPrefDeps@newPrefDeps@notRefDeps)))
      in

      let merged1=List.fold_left merge emptyProject asIs in
      let mergedDeps=List.fold_left merge merged1 prefixed in
	
      let newGraph=
	List.fold_left Bgraph.add Bgraph.emptyGraph allDeps
      in

      let newProject=add mergedDeps newGraph [(scopedMchId, machCont); baseMachAssoc]  in
      let doneProject=merge prefixedPart newProject in
(*      let _=Error.debug ("State of prefixing before loop:" ^ (project_to_string doneProject)) in *)
	match refDeps with
	  | [] ->
	      let projectName=Id_acc.name_of (get_project_id doneProject) in
	      Hashtbl.add projRep projectName doneProject;
	      doneProject
	  | [alone] -> 
	      let refId=Bgraph.target alone in
(*	      let refProject=subproject proj refId in *)
		prefixProject projRep proj doneProject scopeEnv prefix refId
	  | _ -> invalid_arg "prefixProject : multiple refinements for one machine" 



let rec findMachine mchlist name =
  match mchlist with
    | [] -> raise Not_found
    | mch::others -> 
	if (Blast_acc.get_amn_head_name (Blast_acc.get_head mch))=name
	then mch
	else findMachine others name
	  

let transform_edge ids nameedge=
  let sourceid=Id_handling.find_by_name (Bgraph.source nameedge) ids in
  let targetid=Id_handling.find_by_name (Bgraph.target nameedge) ids in
    Bgraph.mk_edge sourceid (Bgraph.dependency nameedge) targetid 
      

let rec buildProject_aux projRep previousProj machinesList nameGraph mchname=
  Error.debug ("Building project:" ^ mchname);
  let context=
    if previousProj=emptyProject 
    then Benv.scope_env_basic
    else get_context previousProj 
  in
  if Hashtbl.mem projRep mchname 
  then 
    let foundProject=Hashtbl.find projRep mchname in
(*    let _=Error.debug ("Got:" ^ (project_to_string foundProject)) in *)
      foundProject
  else
    if Id_handling.isPrefixedName mchname
    then
      let _=Error.debug ("Must build prefixed machine:" ^ mchname) in
      let realName=Id_handling.removePrefixName mchname in
      let prefix=Id_handling.prefixFromName mchname in
      let proj=buildProject_aux projRep previousProj machinesList nameGraph realName in
      let projId=get_project_id proj in
      let prefixedProj=prefixProject projRep proj emptyProject Benv.scope_env_basic prefix projId in
	merge previousProj prefixedProj
    else
      let _=Error.debug ("Building machines depended on for:" ^ mchname) in
      let deps=Bgraph.filter_source nameGraph mchname in
      let (refDeps, notRefDeps)=
	List.partition (fun e -> (Bgraph.dependency e)=Bgraph.RefinedBy) deps in

      let projDeps=
	List.map (buildProject_aux projRep emptyProject machinesList nameGraph) 
	  (List.map Bgraph.target notRefDeps)
      in
      let idDeps=List.map (fun p -> fst (get_root_machine p)) projDeps in

      let _=Error.debug ("Preparing scoping context of:" ^ mchname) in
      let actualMachine=findMachine machinesList mchname in
      let contextDeps=List.map get_context projDeps in
(*      let _=Error.debug ("More abstract part:" ^ (project_to_string previousProj)) in *)
      let contextUnified=
	List.fold_left 
	  Modules.Scope.merge 
	  Modules.Scope.empty 
	  (contextDeps@[context])
      in
      let (scopedMch, scopeEnv)=Bscoping.scope_amn contextUnified actualMachine in
      let machCont={machine=scopedMch; context=scopeEnv} in
      let scopedMchId=Blast_acc.get_name scopedMch in
      let newDeps=
	List.map (transform_edge (scopedMchId::idDeps)) notRefDeps
      in
      let refinedEdges=Bgraph.filter_targetDep nameGraph RefinedBy mchname in
      let allDeps=
	match refinedEdges with
	  | [] -> newDeps
	  | [alone] -> 
	      (transform_edge (scopedMchId::[(Blast_acc.get_refined_name scopedMch)]) alone)::newDeps 
	  | _ -> invalid_arg "buildProject_aux : one refinement for several machines" 
      in
      let partialProject=List.fold_left merge emptyProject projDeps in
      let newGraph=List.fold_left Bgraph.add Bgraph.emptyGraph allDeps in
      let newProject=add partialProject newGraph [(scopedMchId, machCont)] in
      let doneProject=merge previousProj newProject in
  (*    let _=Error.debug ("Before recursive:" ^ (project_to_string doneProject)) in *)
	match refDeps with
	  | [] -> 
	      let projectName=Id_acc.name_of (get_project_id doneProject) in
	      Hashtbl.add projRep projectName doneProject;
	      doneProject
	  | [alone] ->
	      let _=Error.debug ("Found refinement:" ^ (Bgraph.target alone)) in
	      buildProject_aux projRep doneProject machinesList nameGraph (Bgraph.target alone)
	  | _ -> invalid_arg "buildProject_aux : multiple refinements for one machine" 


(* Using an execution-persistant database of already built projects, is safe
as long as during one execution, there are no machines having the same
name. If you want to execute your program with two different machines having
the same name, the following declaration is no more valid *)
let projectRepository=Hashtbl.create 50

let buildProject machinesList=
  if machinesList=[] then emptyProject
  else 
    let machines_graphs=
      List.map
	(fun mch -> build_dependencies mch machinesList)
	machinesList
    in
    let projectNamegraph=List.fold_left Bgraph.merge Bgraph.emptyGraph machines_graphs in
    let withRenamings=addRenamesLinks projectNamegraph in
    let rootName=
      if withRenamings=[]
      then 
	if (List.length machinesList)=1
	then Id_acc.name_of (Blast_acc.get_name (List.hd machinesList))
	else invalid_arg "buildProject : no dependencies between machines"
      else Bgraph.get_root_node withRenamings
    in
      buildProject_aux projectRepository emptyProject machinesList withRenamings rootName

		  
let machines_ids project=
  let allids=Bgraph.nodes project.graph in
    if allids=[] 
    then [ (get_project_id project) ]
    else
      let (prefids, bareids)=List.partition Id_handling.isPrefixed allids in
	bareids

