(* $Id$ *)

open Blast
open Blast_mk
open Id_handling
open Subst_acc


(* The purpose of this module is to provide functions for everything that has
   relation with instanciation or replacement *)


(*
  \subsection{Expressions : renaming}
 *)


(* Takes an identifiers set and and a list of (id, expr) couples, and returns *)
(* a list of couples (id, expr) which is equivalent at the instanciation *)
(* level: the effect of applying maffect is the same than applying the *)
(* returned list starting from the first element. Currently this function does *)
(* "brute-force" renaming, i.e. it doesn't analyse if there are variables that *)
(* don't need renaming. E.g. $x,y:=y,x$ will become *)
(* $x_0:=y;y_0:=x;y:=y_0;x:=x_0$ *)

let rec multiple_affect_to_list refidset idexplist=
  match idexplist with
    | (x,e)::others -> 
	begin
	  let alreadyseq=multiple_affect_to_list refidset others in
	  let xfresh=create_id_from_id x !refidset in
	  let renaming=(x, ExprId(xfresh)) in
	  let renamed=(xfresh, e) in
	    refidset:= Idset.add xfresh !refidset;
	    renaming::( alreadyseq @ [renamed])   
	end
    | [] -> []
	


let rec replace_expr x e expression=
  match expression with
  | ExprUn(op,e1) ->
      ExprUn(op, replace_expr x e e1)
  | ExprBin(op,e1,e2) ->
      ExprBin(op, replace_expr x e e1, replace_expr x e e2 )
  | ExprTrin(op,e1,e2, e3) ->
      ExprTrin(op, replace_expr x e e1, replace_expr x e e2, replace_expr x e e3)
  | ExprParen(exp) ->
      ExprParen(replace_expr x e exp)   
  | ExprId(i) ->
      if x=i then e else expression
  | ExprBefore(i) -> (* A "before" expression is likely never to be replaced *)
      expression
  | ExprFunCall(expr1,expr2) ->
      let e1 = replace_expr x e expr1 in
      let e2 = replace_expr x e expr2 in
      ExprFunCall(e1,e2)
  | ExprSeq(expseq) ->
      ExprSeq( replace_exprSeq x e expseq )
  | ExprSet(expset) ->
      ExprSet( replace_exprSet x e expset )
  | ExprNuplet(exprs) ->
      ExprNuplet( List.map (replace_expr x e) exprs )	

  | ExprSIGMA(idents, e1, e2) ->
      ExprSIGMA(idents, replace_pred x e e1, replace_expr x e e2)
  | ExprPI(idents, e1, e2) ->
      ExprPI(idents, replace_pred x e e1, replace_expr x e e2)
  | ExprLambda(idents, e1, e2) ->
      ExprLambda(idents, replace_pred x e e1, replace_expr x e e2)
  | ExprRecords(records) ->
      ExprRecords(replace_exprRecords x e records)
  | ExprNumber(_) 
  | ExprBoolConstant(_) 
  | ExprBool(_) (* TODO: SC: Warning : should be corrected *)
  | ExprString (_) -> expression 
        
and replace_exprRecords x e records =
  match records with
  | RecordsFields(fields) ->
      let (ids, exprs) = List.split fields in
      let new_exprs = List.map (replace_expr x e) exprs in
      RecordsFields(List.combine ids new_exprs)
  | RecordsAccess(expr,id) ->
        RecordsAccess(replace_expr x e expr, id)
and replace_funCall x e (expr,exprlist) =
  let e1 = replace_expr x e expr in
  let e2 = List.map (replace_expr x e ) exprlist in
  (e1,e2)

and replace_exprSeq x e expseq=
  match expseq with
  | SeqEnum(exps) -> 
      SeqEnum( List.map (replace_expr x e) exps )
  | SeqEmpty -> 
      expseq
        
and 
    replace_exprSet x e expset=
  match expset with
  | SetEmpty -> expset
  | SetPredefined(_) -> 
      expset
  | SetEnum(exps) ->
      SetEnum( List.map (replace_expr x e) exps )
  | SetUnionQ(idents, e1, e2) ->
      let p = replace_pred x e e1 in
      let e = replace_expr x e e2 in
      SetUnionQ(idents, p,e)
  | SetComprPred(ids, p1) ->
      let p = replace_pred x e p1 in
     SetComprPred(ids , p)
  | SetInterQ(idents, p1, e2) ->
      let p = replace_pred x e p1 in
      let e = replace_expr x e e2 in
      SetInterQ(idents, p ,e)
  | SetRecords(fields) ->
      let (ids, exprs) = List.split fields in
      let new_exprs = List.map (replace_expr x e) exprs in
      SetRecords(List.combine ids new_exprs)
        
and

    (*
       \subsection{Pr�dicats : renommage}
     *)
  replace_pred x e pred=
  match pred with
  | PredParen(p) ->
     PredParen (replace_pred x e p)
  | PredNegation(p) ->
      PredNegation (replace_pred x e p)
  | PredExists(v,p) ->
      PredExists(v, (replace_pred x e p))
  | PredForAll(v,p) ->
      PredForAll( v, (replace_pred x e p))
  | PredBin(op,p1,p2) ->
      PredBin(op, (replace_pred x e p1), (replace_pred x e p2))
  | PredAtom(op,e1,e2) ->
      PredAtom(op, (replace_expr x e e1), (replace_expr x e e2))
(* TODO: SC : � investiguer *)
  | SubstApply(_) -> invalid_arg "replace_pred : SubstApply construction"
and 

    (*
       \subsection{Substitutions : renommage}
     *)
    replace_subst x e subst=
  match subst with
  | SubstOperCall(ids,id,exprlist) ->
      SubstOperCall(ids, id, (List.map (replace_expr x e) exprlist))
  | SubstBlock(_) ->
      SubstBlock(replace_subst x e (subst_from subst))
  | SubstSetEqualIds(_) ->
      let idents=allids_from subst in
      let exprs=allexprs_from subst in
      SubstSetEqualIds((List.map (replace_id x e ) idents), 
		       (List.map (replace_expr x e) exprs))
  | SubstSetEqualFun(funchain,expr) ->
      SubstSetEqualFun((replace_funchain x e funchain),
		       (replace_expr x e expr))
  | SubstSetEqualRecords(fieldchain,expr) ->
      SubstSetEqualRecords((replace_fieldchain x e fieldchain),
			   (replace_expr x e expr))
  | SubstBecomeSuch(ids,p) ->
      let vars=allids_from subst in
      let p=cond_from subst in
      SubstBecomeSuch(vars, (replace_pred x e p))
  | SubstSetIn(_) ->
      let vars=allids_from subst in
      let exp=expr_from subst in 
      SubstSetIn(vars, (replace_expr x e exp))
  | SubstPrecondition(_) ->
      let c=cond_from subst in
      let s=subst_from subst in
	SubstPrecondition((replace_pred x e c), (replace_subst x e s))
  | SubstAssertion(c,s) ->
      let c=cond_from subst in
      let s=subst_from subst in
	SubstAssertion((replace_pred x e c), (replace_subst x e s))
  | SubstChoice(_) ->
      let sl=allsubsts_from subst in
      SubstChoice(List.map (replace_subst x e) sl)
  | SubstSkip -> SubstSkip
  | SubstSequence(_) ->
      let (s1, s2)=twosubsts_from subst in
      SubstSequence((replace_subst x e s1), (replace_subst x e s2))
  | SubstParallel(_) ->
      let (s1, s2)=twosubsts_from subst in
      SubstParallel((replace_subst x e s1), (replace_subst x e s2))
  | SubstIf(_) ->
      let exprs=allconds_from subst in
      let thens=allsubsts_from subst in
      let newelse=
	match (elsepart_from subst) with
	| None -> None
	| Some(s) -> Some( replace_subst x e s)
      in
      SubstIf(
	(List.combine
	   (List.map (replace_pred x e) exprs)
	   (List.map (replace_subst x e) thens)),
	newelse)
  | SubstSelect(_) ->
      let whens=allconds_from subst in
      let thens=allsubsts_from subst in
      let newelse=
	match (elsepart_from subst) with
	| None -> None
	| Some(s) -> Some( replace_subst x e s)
      in
      SubstSelect(
	(List.combine
	   (List.map (replace_pred x e) whens)
	   (List.map (replace_subst x e) thens)),
	newelse)
  | SubstCase(_) ->
      let newcaseexpr=
	replace_expr x e (expr_from subst) in
      let neweithers=
	List.map 
	  (List.map (replace_expr x e) )
	  (allexprLists_from subst) in
      let newthens=
	List.map (replace_subst x e) (allsubsts_from subst) in
      let newelse=
	match (elsepart_from subst) with
	| None -> None
	| Some(s) -> Some( replace_subst x e s)
      in
      SubstCase(newcaseexpr,
		(List.combine neweithers newthens),
		newelse)
  | SubstAny(_) ->
      let idents=allids_from subst in
      let cond=cond_from subst in
      let s=subst_from subst in
      SubstAny(idents,
	       (replace_pred x e cond),
	       (replace_subst x e s))
  | SubstLet(_) ->
      let idents=allids_from subst in
      let c=cond_from subst in
      let s=subst_from subst in
      SubstLet(idents,
	       (replace_pred x e c),
	       (replace_subst x e s))
  | SubstVar(_) ->
      let idents=allids_from subst in
      let s=subst_from subst in
      SubstVar(idents,
	       (replace_subst x e s))
  | SubstWhile(_) ->
      let c=cond_from subst in
      let s=subst_from subst in
      let (i,v)=whilepreds_from subst in
      SubstWhile((replace_pred x e c),
		 (replace_subst x e s),
		 (replace_expr x e i),
		 (replace_pred x e v))

(* We can do replacement in such a construction, but we choose not to do it,
   because it makes rules unclear : it already denotes a replacement, and
   defining replacement of replacement of replacement... is complicated *)

  | SubstInstanciation(_) -> 
      invalid_arg "replace_subst : SubstInstanciation construction" 
and replace_funchain x e funchain=
  match funchain with
    | SetFunVar(id) ->
      SetFunVar(replace_id x e id)
    | SetFunExpr(fc, expr) ->
      SetFunExpr(replace_funchain x e fc, replace_expr x e expr)
and replace_fieldchain x e fieldchain=
  match fieldchain with
    | SetRecVar(id) ->
      SetRecVar(replace_id x e id)
    | SetRecField(fc, i) -> (* normally we should never have to rename fields *)
      SetRecField(replace_fieldchain x e fc, i)
and replace_id x e i=
  match e with
    | ExprId(j) -> 
	let result= if x=i then j else i
	in
	  Error.debug ("Replaced " ^ (Id_acc.debug_id i) ^ " by " ^ (Id_acc.debug_id result));
	  result
    | _ -> i



(* This function replaces identifier x by identifier y in the third
parameter. Very useful indeed when including or using a prefixed machine *)

let rec id_subst_expr x y expression=
  match expression with
    | ExprUn(op,e1) ->
	ExprUn(op, id_subst_expr x y e1)
    | ExprBin(op,e1,e2) ->
	ExprBin(op, id_subst_expr x y e1, id_subst_expr x y e2 )
    | ExprTrin(op,e1,e2,e3) ->
	ExprTrin(op, id_subst_expr x y e1, id_subst_expr x y e2, id_subst_expr x y e3)
    | ExprParen(exp) ->
	ExprParen(id_subst_expr x y exp)   
    | ExprId(i) ->
	ExprId( id_subst_id x y i)
    | ExprBefore(i) ->
	ExprBefore( id_subst_id x y i)
    | ExprFunCall(expr1,expr2) ->
	let e1 = id_subst_expr x y expr1 in
	let e2 = id_subst_expr x y expr2 in
	  ExprFunCall(e1,e2)
    | ExprSeq(expseq) ->
	ExprSeq( id_subst_exprSeq x y expseq )
    | ExprSet(expset) ->
	ExprSet( id_subst_exprSet x y expset )
    | ExprNuplet(exprs) ->
	ExprNuplet( List.map (id_subst_expr x y) exprs )	
    | ExprSIGMA(idents, e1, e2) ->
	ExprSIGMA( id_subst_ids x y idents, 
		   id_subst_pred x y e1, 
		   id_subst_expr x y e2)
    | ExprPI(idents, e1, e2) ->
	ExprPI( id_subst_ids x y idents, 
		id_subst_pred x y e1, 
		id_subst_expr x y e2)
    | ExprLambda(idents, e1, e2) ->
	ExprLambda( id_subst_ids x y idents, 
		    id_subst_pred x y e1, 
		    id_subst_expr x y e2)
    | ExprBool(p) -> 
	ExprBool( id_subst_pred x y p)
    | ExprRecords(r) ->
        ExprRecords(id_subst_exprRecords x y r)
    | ExprBoolConstant(_) | ExprNumber(_) | ExprString (_) -> expression 
and 
  id_subst_exprRecords x y records=
  match records with
    | RecordsFields(fields) ->
      (* FIXME: replacing the names of the field is debatable, but it
	 is on par with the purpose of the function. Same remark
         for SetRecords *)
        let (opt_ids, exprs) = List.split fields in
	let id_subst_id_opt =
	  function None -> None | Some(id) -> Some(id_subst_id x y id)
	in
	let new_ids = List.map id_subst_id_opt opt_ids in
	let new_exprs = List.map (id_subst_expr x y) exprs in
	RecordsFields(List.combine new_ids new_exprs)
    | RecordsAccess(expr, id) ->
        RecordsAccess(id_subst_expr x y expr, id_subst_id x y id)
and 
  id_subst_exprSeq x y expseq=
  match expseq with
    | SeqEnum(exps) -> 
	SeqEnum( List.map (id_subst_expr x y) exps )
    | SeqEmpty -> 
	expseq
and 
  id_subst_exprSet x y expset=
  match expset with

    | SetPredefined(_) -> 
	expset
    | SetEnum(exps) ->
	SetEnum( List.map (id_subst_expr x y) exps )
    | SetComprPred(ids, e1) ->
	SetComprPred( id_subst_ids x y ids , id_subst_pred x y e1)
    | SetEmpty ->
	expset
    | SetUnionQ(idents, e1, e2) ->
	SetUnionQ(id_subst_ids x y idents, 
		  id_subst_pred x y e1, 
		  id_subst_expr x y e2)
    | SetInterQ(idents, e1, e2) ->
	SetInterQ(id_subst_ids x y idents, 
		  id_subst_pred x y e1, 
		  id_subst_expr x y e2)
    | SetRecords(fields) ->
      let (ids, exprs) = List.split fields in
      let new_ids = id_subst_ids x y ids in
      let new_exprs = List.map (id_subst_expr x y) exprs in
      SetRecords(List.combine new_ids new_exprs)
        
and id_subst_pred x y pred=
  match pred with
    | PredParen(p) ->
	PredParen(id_subst_pred x y p)
    | PredNegation(_) ->
	let p=Pred_acc.pred_from pred in
	  PredNegation(id_subst_pred x y p)
    | PredExists(_) ->
	let v=Pred_acc.vars_from pred in
	let p=Pred_acc.pred_from pred in
	  PredExists((id_subst_ids x y v ), (id_subst_pred x y p))
    | PredForAll(_) ->
	let v=Pred_acc.vars_from pred in
	let p=Pred_acc.pred_from pred in
	  PredForAll((id_subst_ids x y v ), (id_subst_pred x y p))
    | PredBin(op,p1,p2) ->
	PredBin(op, (id_subst_pred x y p1), (id_subst_pred x y p2))
    | PredAtom(op,e1,e2) ->
        PredAtom(op, (id_subst_expr x y e1), (id_subst_expr x y e2))
(* TODO: SC : � investiguer *)
    | SubstApply(_) -> invalid_arg "id_subst_pred : SubstApply construction"

and 
  id_subst_subst x y subst=
  match subst with
    | SubstOperCall(ids,id,exprlist) ->
	SubstOperCall((id_subst_ids x y ids),
		      (id_subst_id x y id),
		      (List.map (id_subst_expr x y) exprlist))
    | SubstBlock(_) ->
	SubstBlock(id_subst_subst x y (subst_from subst))
    | SubstSetEqualIds(_) ->
	let idents=allids_from subst in
	let exprs=allexprs_from subst in
	  SubstSetEqualIds((id_subst_ids x y idents),
			   (List.map (id_subst_expr x y) exprs))
    | SubstSetEqualFun(funchain,expr) ->
	SubstSetEqualFun(
          (id_subst_funchain x y funchain),
	  (id_subst_expr x y expr))
    | SubstSetEqualRecords(fieldchain,expr) ->
	SubstSetEqualRecords(
          (id_subst_fieldchain x y fieldchain),
	  (id_subst_expr x y expr))
    | SubstBecomeSuch(_) ->
	let vars=allids_from subst in
	let p=cond_from subst in
	  SubstBecomeSuch((id_subst_ids x y vars), (id_subst_pred x y p))
    | SubstSetIn(_) ->
	let vars=allids_from subst in
	let exp=expr_from subst in 
	  SubstSetIn((id_subst_ids x y vars), (id_subst_expr x y exp))
    | SubstPrecondition(_) ->
	let c=cond_from subst in
	let s=subst_from subst in
	  SubstPrecondition((id_subst_pred x y c), (id_subst_subst x y s))
    | SubstAssertion(_) ->
	let c=cond_from subst in
	let s=subst_from subst in
	  SubstAssertion((id_subst_pred x y c), (id_subst_subst x y s))
    | SubstChoice(_) ->
	let sl=allsubsts_from subst in
	  SubstChoice(List.map (id_subst_subst x y) sl)
    | SubstSkip -> SubstSkip
    | SubstSequence(_) ->
	let (s1, s2)=twosubsts_from subst in
	  SubstSequence((id_subst_subst x y s1), (id_subst_subst x y s2))
    | SubstParallel(_) ->
	let (s1, s2)=twosubsts_from subst in
	  SubstParallel((id_subst_subst x y s1), (id_subst_subst x y s2))
    | SubstIf(_) ->
	let conds=allconds_from subst in
	let thens=allsubsts_from subst in
	let newelse=
	  match (elsepart_from subst) with
	    | None -> None
	    | Some(s) -> Some( id_subst_subst x y s)
	in
	  SubstIf(
	    (List.combine
	       (List.map (id_subst_pred x y) conds)
	       (List.map (id_subst_subst x y) thens)),
	    newelse)
    | SubstSelect(_) ->
	let whens=allconds_from subst in
	let thens=allsubsts_from subst in
	let newelse=
	  match (elsepart_from subst) with
	    | None -> None
	    | Some(s) -> Some( id_subst_subst x y s)
	in
	  SubstSelect(
	    (List.combine
	       (List.map (id_subst_pred x y) whens)
	       (List.map (id_subst_subst x y) thens)),
	    newelse)
    | SubstCase(_) ->
	let newcaseexpr=
	  id_subst_expr x y (expr_from subst) in
	let neweithers=
	  List.map 
	    (List.map (id_subst_expr x y) )
	    (allexprLists_from subst) in
	let newthens=
	  List.map (id_subst_subst x y) (allsubsts_from subst) in
	let newelse=
	  match (elsepart_from subst) with
	    | None -> None
	    | Some(s) -> Some( id_subst_subst x y s)
	in
	  SubstCase(
	    newcaseexpr,
	    (List.combine neweithers newthens),
	    newelse)
    | SubstAny(_) ->
	let idents=allids_from subst in
	let cond=cond_from subst in
	let s=subst_from subst in
	  SubstAny(
	    (id_subst_ids x y idents),
	    (id_subst_pred x y cond),
	    (id_subst_subst x y s))
    | SubstLet(idents,conds,s) ->
	SubstLet(
	  (id_subst_ids x y idents),
          (id_subst_pred x y conds),
	  (id_subst_subst x y s))
    | SubstVar(_) ->
	let idents=allids_from subst in
	let s=subst_from subst in
	  SubstVar((id_subst_ids x y idents), (id_subst_subst x y s))
    | SubstWhile(_) ->
	let c=cond_from subst in
	let s=subst_from subst in
	let (i,v)=whilepreds_from subst in
	  SubstWhile(
	    (id_subst_pred x y c),
	    (id_subst_subst x y s),
	    (id_subst_expr x y i),
	    (id_subst_pred x y v))

(* We can do replacement in such a construction, but we choose not to do it,
   because it makes rules unclear : it already denotes a replacement, and
   defining replacement of replacement of replacement... is complicated *)

    | SubstInstanciation(_) -> invalid_arg "id_subst_subst : SubstInstanciation construction"

and 
  id_subst_funchain x y funchain=
  match funchain with
    | SetFunVar(id) -> SetFunVar(id_subst_id x y id)
    | SetFunExpr(fc, e) -> SetFunExpr(id_subst_funchain x y fc, id_subst_expr x y e)

and 
  id_subst_fieldchain x y fieldchain=
  match fieldchain with
    | SetRecVar(id) -> SetRecVar(id_subst_id x y id)
    | SetRecField(fc, id) -> SetRecField(id_subst_fieldchain x y fc, id_subst_id x y id)

and id_subst_ids x y idents=
  List.map (id_subst_id x y) idents

and id_subst_id x y i=
  if x=i then y else i

let id_subst_operation x y (name, params_out, params_ini, body)=
    ((id_subst_id x y name),
     (id_subst_ids x y params_out),
     (id_subst_ids x y params_ini),
      (id_subst_subst x y body))

let id_subst_instance x y (mchid, exprs)=
    ((id_subst_id x y mchid),
     (List.map (id_subst_expr x y) exprs))

let id_subst_set x y set=
  match set with
  | SetAbstDec(id) -> SetAbstDec(id_subst_id x y id)
  | SetEnumDec(id, idlist) ->
      SetEnumDec((id_subst_id x y id), (id_subst_ids x y idlist))

let id_subst_clause x y clause=
  match clause with
    | Constraints(pred) -> Constraints(id_subst_pred x y pred)
    | Invariant(pred) -> Invariant(id_subst_pred x y pred)
    | Sets(sets)-> Sets(List.map (id_subst_set x y) sets)
    | Initialisation(subst) -> 
	Initialisation(id_subst_subst x y subst)
    | ConstantsConcrete(ids) -> 
	ConstantsConcrete(id_subst_ids x y ids) 
    | ConstantsAbstract(ids) -> 
	ConstantsAbstract(id_subst_ids x y ids) 
    | ConstantsHidden(ids) -> 
	ConstantsHidden(id_subst_ids x y ids) 
    | Properties(pred) -> Properties(id_subst_pred x y pred)
    | Values(idexprs) ->
	let (ids, exprs)=List.split idexprs in
	  Values(
	    List.combine
	      (id_subst_ids x y ids)
	      (List.map (id_subst_expr x y) exprs))
    | VariablesConcrete(ids) -> 
	VariablesConcrete(id_subst_ids x y ids)
    | VariablesAbstract(ids) -> 
	VariablesAbstract(id_subst_ids x y ids)
    | VariablesHidden(ids) -> 
	VariablesHidden(id_subst_ids x y ids)
    | Assertions(preds) -> 
	Assertions(List.map (id_subst_pred x y) preds)
    | Sees(ids) -> Sees(id_subst_ids x y ids)
    | Uses(ids) -> Uses(id_subst_ids x y ids)
    | Includes(insts) -> 
	Includes(List.map (id_subst_instance x y) insts)
    | Imports(insts) ->
	Imports(List.map (id_subst_instance x y) insts)
    | LocalOperations(ops) -> 
	LocalOperations(List.map (id_subst_operation x y) ops)
    | Operations(ops) -> 
	Operations(List.map (id_subst_operation x y) ops)
(*MG
    | Events(_) -> invalid_arg "id_subst_clause : got events. Is it formally defined then, or are you only giving BCaml a try?" 
*)
    | Promotes(ids) -> Promotes(id_subst_ids x y ids)
    | Extends(insts) ->
	Extends(List.map (id_subst_instance x y) insts)

let id_subst_head x y (id_head, ids)=
  ((id_subst_id x y id_head), (id_subst_ids x y ids))

let id_subst_machine x y ast=
  match ast with
    | Machine(head, cl) -> 
	let newhead=id_subst_head x y head in
	let newclauses=List.map (id_subst_clause x y) cl in
	  Machine(newhead, newclauses)
    | Refinement(head, refining, cl) ->
	let newhead=id_subst_head x y head in
	let newrefining=id_subst_id x y refining in
	let newclauses=List.map (id_subst_clause x y) cl in
	  Refinement(newhead, newrefining, newclauses)
    | Implementation(head, implementing, cl) ->
	let newhead=id_subst_head x y head in
	let newimplementing=id_subst_id x y implementing in
	let newclauses=List.map (id_subst_clause x y) cl in
	  Implementation(newhead, newimplementing, newclauses)
    | EmptyTree -> EmptyTree


(* This function creates the set of potentially accessed by an including,
using,etc machine identifiers.
It will be used to determine the identifiers one has to rename when 
manipulating a prefixed machine *)

let prefixable_identifiers_from ast=

(* Note the parameters won't be prefixed, because they will be instanciated 
by an including machine, or referenced by a using machine, hence 
to-be-instanciated by another including machine *)

  let prefid_clause clause=
    match clause with
      | VariablesConcrete(ids)
      | VariablesAbstract(ids)
      | VariablesHidden(ids) -> set_from_ids ids
      | Operations(oplist) ->
	  set_from_ids (List.map Clause_acc.get_operation_id oplist)

      | Constraints(_)
      | Invariant(_)
      | Sets(_)
      | Initialisation(_)
      | ConstantsConcrete(_)
      | ConstantsAbstract(_)
      | ConstantsHidden(_)
      | Properties(_)
      | Values(_)
      | Promotes(_)
      | Assertions(_)
      | LocalOperations(_)
      | Sees(_)
      | Uses(_)
      | Extends(_)
      | Includes(_)
      | Imports(_) -> Idset.empty
  in

  let prefid_clauses cl=
    let idsclauses=List.map prefid_clause cl in 
      List.fold_left Idset.union Idset.empty idsclauses
  in
    
  let prefid_machine mch=
    let mchid=Blast_acc.get_name mch in
    let insideids=prefid_clauses (Blast_acc.get_clause_list mch) in
      Idset.add mchid insideids

  in Idset.elements (prefid_machine ast)


(* This function prefixes all identifiers in the list returned by
   \emph{getPrefixables}, with the prefix \emph{prefix} in the machines
   \emph{ast}. The obtained machine is in a non-scoped state. Given the nature
   of the refinements, \emph{getPrefixables} may return e.g. the visible
   variables of the machine that have been defined in the most abstract
   machine *)
let prefix_machine getPrefixables prefix ast=
  let _=Error.debug ("Prefixing the machine " ^ (Id_acc.name_of (Blast_acc.get_name ast)) ^ " with " ^ prefix ) in
  let prefixables=getPrefixables () in
  let prefnames=List.map Id_acc.debug_id prefixables in
  let prefStr=String.concat "," prefnames in
  let _=Error.debug ("Found prefixables:" ^ prefStr) in

  let rec prefix_id someprefix id=
    match id with
      | Id(name) -> 
(* FIXME: SC : pas forcement la meilleure fa�on... *)
	  Blast_mk.mk_Id (someprefix ^ "." ^ (Id_acc.name_of id))
      | IdTy(someid, typeb) ->
	  IdTy((prefix_id someprefix someid), typeb)
  in

  let update_machine aprefix someast id=
    let newid=prefix_id aprefix id in
      id_subst_machine id newid someast
  in
    
    List.fold_left (update_machine prefix) ast prefixables



let rec apply_inner_instanciations subst=
  match subst with
    | SubstOperCall(_)
    | SubstSetEqualIds(_)
    | SubstSetEqualFun(_)
    | SubstSetEqualRecords(_)
    | SubstBecomeSuch(_)
    | SubstSetIn(_) 
    | SubstSkip -> subst

    | SubstBlock(_) ->
	SubstBlock(apply_inner_instanciations (subst_from subst))
    | SubstPrecondition(_) ->
	let c=cond_from subst in
	let s=subst_from subst in
	  SubstPrecondition(c, apply_inner_instanciations s)
    | SubstAssertion(_) ->
	let c=cond_from subst in
	let s=subst_from subst in
	  SubstAssertion(c, apply_inner_instanciations s)
    | SubstChoice(_) ->
	let sl=allsubsts_from subst in
	  SubstChoice(List.map apply_inner_instanciations sl)
    | SubstSequence(_) ->
	let (s1, s2)=twosubsts_from subst in
	  SubstSequence(
	    apply_inner_instanciations s1,
	    apply_inner_instanciations s2)
    | SubstParallel(_) ->
	let (s1, s2)=twosubsts_from subst in
	  SubstParallel(
	    apply_inner_instanciations s1,
	    apply_inner_instanciations s2)
    | SubstIf(_) ->
	let conds=allconds_from subst in
	let thens=allsubsts_from subst in
	let newelse=
	  match (elsepart_from subst) with
	    | None -> None
	    | Some(s) -> Some( apply_inner_instanciations s)
	in
	  SubstIf(
	    List.combine
	      conds
	      (List.map apply_inner_instanciations thens),
	    newelse)
    | SubstSelect(_) ->
	let whens=allconds_from subst in
	let thens=allsubsts_from subst in
	let newelse=
	  match (elsepart_from subst) with
	    | None -> None
	    | Some(s) -> Some( apply_inner_instanciations s)
	in
	  SubstSelect(
	    List.combine
	      whens
	      (List.map apply_inner_instanciations thens),
	    newelse)
    | SubstCase(_) ->
	let caseexpr=expr_from subst in
	let eithers=allexprLists_from subst in
	let newthens=
	  List.map apply_inner_instanciations (allsubsts_from subst) in
	let newelse=
	  match (elsepart_from subst) with
	    | None -> None
	    | Some(s) -> Some( apply_inner_instanciations s)
	in
	  SubstCase(
	    caseexpr,
	    List.combine eithers newthens,
	    newelse)
    | SubstAny(_) ->
	let idents=allids_from subst in
	let cond=cond_from subst in
	let s=subst_from subst in
	  SubstAny(idents, cond, apply_inner_instanciations s)
    | SubstLet(_) ->
	let idents=allids_from subst in
	let c=cond_from subst in
	let s=subst_from subst in
	  SubstLet(idents, c, apply_inner_instanciations s)

    | SubstVar(_) ->
	let idents=allids_from subst in
	let s=subst_from subst in
	  SubstVar(idents, apply_inner_instanciations s)
    | SubstWhile(_) ->
	let c=cond_from subst in
	let s=subst_from subst in
	let (i,v)=whilepreds_from subst in
	  SubstWhile(c, apply_inner_instanciations s, i, v)
    | SubstInstanciation(_) -> 
	let s=subst_from subst in
	let instids=allids_from subst in
	let instexprs=allexprs_from subst in
	let treated=apply_inner_instanciations s in
	let forbiddenids=ref (Variables.all_vars_subst treated) in
	let seqaffects=
	  try multiple_affect_to_list forbiddenids 
	    (List.combine instids instexprs)
	  with Invalid_argument e -> 
	    invalid_arg ("apply_inner_instanciations:" ^ e)
	in
	let applycpl sub (xid, xexpr)= replace_subst xid xexpr sub 
	in
	  List.fold_left applycpl treated seqaffects


(* SC: V�rifier si �a fait doublon avec les fonctions d�finies plus haut *)
(* \section{Parcours g\'en\'erique de l'arbre abstrait} 
   Pour l'instant, ce n'est qu'une ebauche... *)

(************************************************************)
(************************************************************)

(*    map_head (id,headPar) =
  
and
    map_headPar = 
  | IdentTypedParList(identTypedParlist) -> 
  | IdentParList(ids) -> 
and
    map_identTypedPar = id,type_B
and
*)
let rec map_clause_list map_id st_table rename_list clauselist =
  match clauselist with
    [] -> []
  | cl::t ->
      (match cl with 
      | Constraints(pred) -> 
	  Constraints(map_predicate map_id st_table rename_list pred)
	  ::map_clause_list map_id st_table rename_list t
      |  Invariant(pred) -> 
	  Invariant(map_predicate map_id st_table rename_list pred)
	  ::map_clause_list map_id st_table rename_list t
      |  Sets (setlist) -> 
	  Sets (map_setlist map_id st_table rename_list setlist)
	  ::map_clause_list map_id st_table rename_list t
      |  Initialisation(subst) -> 
	  Initialisation(map_substitution map_id st_table rename_list subst)
	  ::map_clause_list map_id st_table rename_list t
      |  ConstantsConcrete(ids) -> 
	  ConstantsConcrete(map_ids map_id st_table rename_list ids)
	  ::map_clause_list map_id st_table rename_list t
      |  ConstantsAbstract(ids) -> 
	  ConstantsAbstract(map_ids map_id st_table rename_list ids)
	  ::map_clause_list map_id st_table rename_list t
      |  ConstantsHidden(ids) -> 
	  ConstantsHidden(map_ids map_id st_table rename_list ids)
	  ::map_clause_list map_id st_table rename_list t
      |  Properties(pred) -> 
	  Properties(map_predicate map_id st_table rename_list pred)
	  ::map_clause_list map_id st_table rename_list t
      |  Values(valuelist) -> 
	  Values(map_valuelist map_id st_table rename_list valuelist)
	  ::map_clause_list map_id st_table rename_list t
      |  VariablesConcrete(ids) -> 
	  VariablesConcrete(map_ids map_id st_table rename_list ids)
	  ::map_clause_list map_id st_table rename_list t
      |  VariablesAbstract(ids) -> 
	  VariablesAbstract(map_ids map_id st_table rename_list ids)
	  ::map_clause_list map_id st_table rename_list t
      |  VariablesHidden(ids) -> 
	  VariablesHidden(map_ids map_id st_table rename_list ids)
	  ::map_clause_list map_id st_table rename_list t
      |  Promotes(ids) -> 
	  Promotes(map_ids map_id st_table rename_list ids)
	  ::map_clause_list map_id st_table rename_list t
      |  Assertions(predList) -> 
	  Assertions(map_predlist map_id st_table rename_list predList)
	  ::map_clause_list map_id st_table rename_list t
      |  LocalOperations(funDeflist) -> 
	  LocalOperations(map_funDeflist map_id st_table rename_list funDeflist)
	  ::map_clause_list map_id st_table rename_list t
      |  Operations(funDeflist) -> 
	  Operations(map_funDeflist map_id st_table rename_list funDeflist)
	  ::map_clause_list map_id st_table rename_list t
      |  Sees(ids) -> 
	  Sees(ids)
	  ::map_clause_list map_id st_table rename_list t
      |  Uses(ids) -> 
	  Uses(ids)
	  ::map_clause_list map_id st_table rename_list t
      |  Extends(instancelist) -> 
	  Extends(map_instancelist map_id st_table rename_list instancelist)
	  ::map_clause_list map_id st_table rename_list t
      |  Includes(instancelist) -> 
	  Includes(map_instancelist map_id st_table rename_list instancelist)
	  ::map_clause_list map_id st_table rename_list t
      |  Imports(instancelist) -> 
	  Imports(map_instancelist map_id st_table rename_list instancelist)
	  ::map_clause_list map_id st_table rename_list t
	      )
and 
    map_valuelist map_id st_table rename_list valuelist =
  List.map (map_value map_id st_table rename_list) valuelist
and
    map_value map_id st_table rename_list (id,expr) =
  (map_id st_table rename_list id, map_expr map_id st_table rename_list expr)
and
    map_setlist map_id st_table rename_list setlist = 
  match setlist with
    [] -> []
  | s::t -> (map_set map_id st_table rename_list s)::map_setlist map_id st_table rename_list t
and
(* Pour l'instant on ne fait rien ici, il faudra voir si il est necessaire
   de renommer les ensembles. *)
    map_set map_id st_table rename_list s =
  match s with
  | SetAbstDec(id) -> SetAbstDec(id)
  | SetEnumDec(id,ids) -> SetEnumDec(id,ids)

and
    map_instancelist map_id st_table rename_list inslist = 
  match inslist with
    [] -> []
  | (id,exprList)::t -> 
      (id,map_exprlist map_id st_table rename_list exprList)
      ::map_instancelist map_id st_table rename_list t
and 
    map_type_B map_id st_table rename_list type_B =  
  match type_B with
  | TypeNatSetRange(_,_) as t -> t
  | TypeIdentifier(id) as t -> t
  | TypeBaseSet(bSet) as t -> t
  | _ -> raise (Invalid_argument "(MG) map_type_B  " )
and
    map_predicate map_id st_table rename_list pred =
  match pred with 
  | PredParen(pred) -> 
      PredParen(map_predicate map_id st_table rename_list pred)
  | PredNegation(pred) -> 
      PredNegation(map_predicate map_id st_table rename_list pred)
  | PredBin(op,pred1,pred2) -> 
      PredBin(op,map_predicate map_id st_table rename_list pred1,
	     map_predicate map_id st_table rename_list pred2)
  | PredAtom(op,expr1,expr2) -> 
      PredAtom(op,map_expr map_id st_table rename_list expr1,
	     map_expr map_id st_table rename_list expr2)
  | PredExists(ids,pred) -> 
      PredExists(ids,map_predicate map_id (add_ids st_table ids) rename_list pred)
  | PredForAll(ids,pred) -> 
      PredForAll(ids,map_predicate map_id (add_ids st_table ids) rename_list pred)
  | SubstApply(_) -> invalid_arg "instanciation:map_predicate:SubstApply"
and
    map_funDeflist map_id st_table rename_list fdeflist = 
  match fdeflist with 
    [] -> []
  | (id,ids_out,ids_in,subst)::t ->
      (id,ids_out,ids_in,map_substitution map_id st_table rename_list subst)
      ::(map_funDeflist map_id st_table rename_list t)
and
    map_expr map_id st_table rename_list expr =
  match expr with
  | ExprTrin(op,expr1,expr2,expr3) -> 
      ExprTrin(op,
              map_expr map_id st_table rename_list expr1,
	      map_expr map_id st_table rename_list expr2,
	      map_expr map_id st_table rename_list expr3)
  | ExprBin(op,expr1,expr2) -> 
      ExprBin(op,
              map_expr map_id st_table rename_list expr1,
	      map_expr map_id st_table rename_list expr2)
  | ExprUn(op,expr) -> 
      ExprUn(op,  map_expr map_id st_table rename_list expr)
  | ExprParen(expr) -> 
      ExprParen(map_expr map_id st_table rename_list expr)
  | ExprId(id) -> 
      ExprId(map_id st_table rename_list id)
  | ExprBefore(id) -> 
      ExprBefore(map_id st_table rename_list id)
  | ExprFunCall(expr1, expr2) -> 
      ExprFunCall(
	map_expr map_id st_table rename_list expr1,
	map_expr map_id st_table rename_list expr2)
  | ExprSeq(exprSeq) -> 
      ExprSeq(map_exprSeq map_id st_table rename_list exprSeq)
  | ExprSet(exprSet) -> 
      ExprSet(map_exprSet map_id st_table rename_list exprSet)
  | ExprNumber(number) as e -> e
  | ExprBoolConstant(boolcst) as e -> e
  | ExprBool(boolConstant) as e -> e (* SC: Warning : should be corrected *)
  | ExprString(string) as e -> e
  | ExprNuplet(exprList) -> 
      ExprNuplet(map_exprlist map_id st_table rename_list exprList)
  | ExprSIGMA(ids,pred1,expr2) -> 
      let new_st = add_ids st_table ids
      in
      ExprSIGMA(ids,
		map_predicate map_id new_st rename_list pred1,
		map_expr map_id new_st rename_list expr2)
  | ExprPI(ids,pred1,expr2) -> 
      let new_st = add_ids st_table ids
      in
      ExprPI(ids,
	     map_predicate map_id new_st rename_list pred1,
	     map_expr map_id new_st rename_list expr2)
  | ExprLambda(ids,pred1,expr2) -> 
      let new_st = add_ids st_table ids
      in
      ExprLambda(ids,
	     map_predicate map_id new_st rename_list pred1,
	     map_expr map_id new_st rename_list expr2)
  | ExprRecords(records) ->
      ExprRecords(map_exprRecords map_id st_table rename_list records)
and
(* FIXME: not sure if map_id should apply or not here : I think not *)
    map_exprRecords map_id st_table rename_list records =
  match records with
  | RecordsFields(fields) ->
    let (oids, exprs) = List.split fields in
      RecordsFields(List.combine oids (map_exprlist map_id st_table rename_list exprs))
  | RecordsAccess(expr, id) ->
      RecordsAccess(map_expr map_id st_table rename_list expr, id)
and
    map_exprSeq map_id st_table rename_list exprseq = 
  match exprseq with 
  | SeqEnum(exprList) -> 
      SeqEnum(map_exprlist map_id st_table rename_list exprList)
  | SeqEmpty -> 
      SeqEmpty
and
    map_predTyping map_id st_table rename_list (ids,expr) =
  (ids,map_expr map_id st_table rename_list expr)
and 
    map_exprSet map_id st_table rename_list exprset = 
  match exprset with
  | SetPredefined(setName) -> 
      SetPredefined(setName)
  | SetEnum(exprList) -> 
      SetEnum(map_exprlist map_id st_table rename_list exprList)
  | SetComprPred(ids,pred) -> 
      SetComprPred(
      map_ids map_id st_table rename_list ids,
      map_predicate map_id st_table rename_list pred
     )
  | SetEmpty -> 
      SetEmpty
  | SetUnionQ(ids,pred1,expr2) -> 
      let new_st = add_ids st_table ids
      in
      SetUnionQ(ids,
		map_predicate map_id new_st rename_list pred1,
		map_expr map_id new_st rename_list expr2)
  | SetInterQ(ids,pred1,expr2) -> 
      let new_st = add_ids st_table ids
      in
      SetInterQ(ids,
		map_predicate map_id new_st rename_list pred1,
		map_expr map_id new_st rename_list expr2)
  | SetRecords(fields) ->
    let (ids, exprs) = List.split fields in
      SetRecords(List.combine ids (map_exprlist map_id st_table rename_list exprs))
and
    map_exprlist map_id st_table rename_list exprlist = 
  List.map (fun expr -> map_expr map_id st_table rename_list expr) exprlist
and
    map_predlist map_id st_table rename_list predlist = 
  List.map (fun pred -> map_predicate map_id st_table rename_list pred) predlist
and
    map_substitution map_id st_table rename_list subst = 	
  match subst with
  | SubstOperCall(ids, id, exprlist) ->   
      SubstOperCall(
      map_ids map_id st_table rename_list ids,
      map_id st_table rename_list id,
      List.map (map_expr map_id st_table rename_list) exprlist)
  | SubstBlock(subst) -> 
      SubstBlock(map_substitution map_id st_table rename_list subst)
  | SubstSetEqualIds(ids,exprList) -> 
      SubstSetEqualIds(ids,
		       map_exprlist map_id st_table rename_list exprList)
  | SubstSetEqualFun(funchain,expr) ->
      SubstSetEqualFun(map_funchain map_id st_table rename_list funchain,
		       map_expr map_id st_table rename_list expr)
  | SubstSetEqualRecords(fieldchain,expr) ->
      SubstSetEqualRecords(fieldchain, map_expr map_id st_table rename_list expr)
  | SubstBecomeSuch(ids,pred) -> 
      SubstBecomeSuch(ids,
		      map_predicate map_id st_table rename_list pred)
  | SubstSetIn(ids,expr) -> 
      SubstSetIn(ids,
		 map_expr map_id st_table rename_list expr)
  | SubstPrecondition(pred,subst) -> 
      SubstPrecondition(map_predicate map_id st_table rename_list pred,
			map_substitution map_id st_table rename_list subst)
  | SubstAssertion(pred,subst) -> 
      SubstAssertion(map_predicate map_id st_table rename_list pred,
		     map_substitution map_id st_table rename_list subst)
  | SubstChoice(substlist) -> 
      SubstChoice(List.map 
                    (map_substitution map_id st_table rename_list)
                    substlist)
  | SubstIf(iflist,subst) -> 
      SubstIf(map_iflist map_id st_table rename_list iflist,
	map_substitution_option map_id st_table rename_list subst)
  | SubstSelect(selectlist,substOption) -> 
      SubstSelect(map_iflist map_id st_table rename_list selectlist,
		  map_substitution_option map_id st_table rename_list substOption)
  | SubstCase(expr,caselist,substOption) -> 
      SubstCase(map_expr map_id st_table rename_list expr,
		map_caselist map_id st_table rename_list caselist,
		map_substitution_option map_id st_table rename_list substOption)
  | SubstAny(ids,pred,subst) -> 
      let new_st = add_ids st_table ids 
      in
      SubstAny(ids,
	       map_predicate map_id st_table rename_list pred,
	       map_substitution map_id new_st rename_list subst)
  | SubstLet(ids,pred,subst) -> 
      let new_st = add_ids st_table ids 
      in
      SubstLet(ids,
	       map_predicate map_id st_table rename_list pred,
	       map_substitution map_id new_st rename_list subst)
  | SubstVar(ids,subst) -> 
      let new_st = add_ids st_table ids 
      in      
      SubstVar(ids,
	       map_substitution map_id new_st rename_list subst)
  | SubstSequence(subst1,subst2) -> 
      SubstSequence(map_substitution map_id st_table rename_list subst1,
		    map_substitution map_id st_table rename_list subst2)
  | SubstWhile(pred1,subst,expr2,pred3) -> 
      SubstWhile(map_predicate map_id st_table rename_list pred1,
		 map_substitution map_id st_table rename_list subst,
		 map_expr map_id st_table rename_list expr2,
		 map_predicate map_id st_table rename_list pred3)
  | SubstParallel(subst1,subst2) -> 
      SubstParallel(map_substitution map_id st_table rename_list subst1,
		    map_substitution map_id st_table rename_list subst2)
  | SubstSkip -> 
      SubstSkip
  | SubstInstanciation(_) -> invalid_arg "instanciation:map_substitution:SubstInstanciation"
and
    map_funchain map_id st_table rename_list funchain =
  match funchain with
  | SetFunVar(id) -> funchain (* We do as in SubstSetEqualIds *)
  | SetFunExpr(fc, expr) ->
    SetFunExpr(map_funchain map_id st_table rename_list fc,
	       map_expr map_id st_table rename_list expr)
and 
    map_substitution_option map_id st_table rename_list substOption =
  match substOption with
    Some(s) -> Some(map_substitution map_id st_table rename_list s)
  | None -> None
and
    map_caselist map_id st_table rename_list caselist =
  List.map
    (fun (exprlist, subst) -> 
      (map_exprlist map_id st_table rename_list exprlist,
      map_substitution map_id st_table rename_list subst))
    caselist
and
(*    map_selectlist = map_iflist
and
  *)
    map_iflist map_id st_table rename_list iflist =
      List.map 
        (fun (pred,subst) -> 
          (map_predicate map_id st_table rename_list pred,
          map_substitution map_id st_table rename_list subst))
        iflist
and
    map_ids map_id st_table rename_list idlist =
  List.map (map_id st_table rename_list) idlist

let map_amn map_id st_table rename_list ast =
  match ast with
  | Machine(head,clauselist) -> 
      Machine(head,
              map_clause_list map_id st_table rename_list clauselist
             )
  | Refinement(head,id,clauselist) -> 
      Refinement(head,
                 id,
                 map_clause_list map_id st_table rename_list clauselist)
  | Implementation(head,id,clauselist) -> 
      Implementation(head,
                     id,
                     map_clause_list map_id st_table rename_list clauselist)
  | EmptyTree -> EmptyTree



(*SC: Fonctions pour le remplacement des appels d'op�rations. Devrait
  remplacer les fonctions d�finies plus haut *)
let rec replace_operCall opl operName exprList params_out =
  match operName with
    | ExprId(funName) ->
	(try		
	   let (nameCall,ids_out,ids_in,body) = 
	     List.find 
               (fun (id,_,_,_) -> 
		  Id_handling.large_equality funName id 
               ) 
               opl  (*DP car pas encore une bonne gestion du scoping et d'un environnement...*)
	in
        let ids = ids_in @ ids_out 
        in
	(match ids_in with
	  [] -> 
	    (match ids_out with
	      [] -> 
		body
	    | _ -> 
		SubstVar(ids,SubstSequence(body,SubstSetEqualIds(ids_out,exprList))  )
	    )
	| _ -> 
	    (match ids_out with
	      [] -> 
		SubstVar(ids,SubstSequence(SubstSetEqualIds(ids_in,exprList),body))
	    | _ -> 
		SubstVar(ids,SubstSequence(SubstSetEqualIds(ids_in,exprList),
			      SubstSequence(body,SubstSetEqualIds(ids_out,exprList))))
	    )
	)
      with Not_found -> (	SubstOperCall(params_out,funName,replace_operCall_exprList opl exprList)
      )
      )
  | _ -> Error.message "raise (Invalid_argument replace_operCall)"; raise (Invalid_argument "replace_operCall")
and

(*i*)
(* TD --A regler
   replace\_funCall opl expr exprList =
   try
   let replace = 
   replace\_operCall opl expr exprList []
   in
   match replace with
   SubstOperCall(\_,id,exprList2) -> ExprFunCall(ExprId(id),exprList2)
   | SubstSequence(body,SubstSetEqualIds(ids\_out,exprList))
   with Invalid\_argument("replace\_operCall") -> 
   ExprFunCall(replace\_operCall\_expr
   opl expr,replace\_operCall\_exprList opl  exprList)
   
   and
*)
(*i*)

    replace_operCall_substitution opl s =
  match s with
  | SubstOperCall(variablelist,funname,exprlist) -> 
      replace_operCall opl (ExprId(funname)) exprlist variablelist
  | SubstBlock(subst) -> 
      SubstBlock(replace_operCall_substitution opl subst)
  | SubstSetEqualIds(ids,exprList) -> 
      SubstSetEqualIds(ids,replace_operCall_exprList opl exprList)
  | SubstSetEqualFun(funchain,expr) -> 
      SubstSetEqualFun(replace_operCall_funchain opl funchain,
		       replace_operCall_expr opl expr)
  | SubstSetEqualRecords(fieldchain,expr) -> 
      SubstSetEqualRecords(fieldchain,replace_operCall_expr opl expr)
  | SubstBecomeSuch(ids,pred) -> 
      SubstBecomeSuch(ids,replace_operCall_predicate opl pred)
  | SubstSetIn(ids,expr) -> 
      SubstSetIn(ids,replace_operCall_expr opl expr)
  | SubstPrecondition(pred,subst) -> 
      SubstPrecondition(replace_operCall_predicate opl pred,
                        replace_operCall_substitution opl subst)
  | SubstAssertion(pred,subst) -> 
      SubstAssertion(replace_operCall_predicate opl pred,
                     replace_operCall_substitution opl subst)
  | SubstChoice(substlist) -> 
      SubstChoice(List.map (replace_operCall_substitution opl) substlist)
  | SubstIf(iflist,substOption) -> 
      SubstIf(replace_operCall_iflist opl iflist,
        replace_operCall_substitution_option opl substOption)
  | SubstSelect(iflist,substOption) -> 
      SubstSelect(replace_operCall_iflist opl iflist,
        replace_operCall_substitution_option opl substOption)
  | SubstCase(expr,iflist,substOption) -> 
      SubstCase(replace_operCall_expr opl expr,
                (replace_operCall_caselist opl iflist),
                (replace_operCall_substitution_option opl substOption))
  | SubstAny(ids,pred,subst) -> 
      SubstAny(ids,
               replace_operCall_predicate opl pred,
               replace_operCall_substitution opl subst)
  | SubstLet(ids,pred,subst) -> 
      SubstLet(ids,
               replace_operCall_predicate opl pred,
               replace_operCall_substitution opl subst)
  | SubstVar(ids,subst) -> 
      SubstVar(ids,replace_operCall_substitution opl subst)
  | SubstSequence(subst1,subst2) -> 
      SubstSequence(replace_operCall_substitution opl subst1,
                    replace_operCall_substitution opl subst2)
  | SubstWhile(pred1,subst,expr2,pred3) -> 
      SubstWhile(replace_operCall_predicate opl pred1,
		 replace_operCall_substitution opl subst,
		 replace_operCall_expr opl expr2,
		 replace_operCall_predicate opl pred3)
  | SubstParallel(subst1,subst2) -> 
      SubstParallel(replace_operCall_substitution opl subst1,
                    replace_operCall_substitution opl subst2)
  | SubstSkip -> SubstSkip
  | SubstInstanciation(_) -> invalid_arg "instanciation:replace_operCall_substitution:SubstInstanciation"
and
    replace_operCall_funchain opl funchain =
  match funchain with
    SetFunVar(id) -> funchain
  | SetFunExpr(fc, expr) ->
      SetFunExpr(replace_operCall_funchain opl fc,
		 replace_operCall_expr opl expr)
and
    replace_operCall_substitution_option opl substOption =
  match substOption with
    Some(s) -> Some(replace_operCall_substitution opl s)
  | None -> None
and
    replace_operCall_exprList opl elist =
  List.map (replace_operCall_expr opl) elist
and
    replace_operCall_substList opl slist =
  List.map (replace_operCall_substitution opl) slist
and
    replace_operCall_iflist opl iflist =
      List.map (replace_operCall_if opl) iflist
and
    replace_operCall_if opl (pred,subst) =
  (replace_operCall_predicate opl pred,replace_operCall_substitution opl subst)
and
    replace_operCall_caselist opl caselist =
  List.map (replace_operCall_case opl) caselist
and
    replace_operCall_case opl (exprlist,subst) =
  (replace_operCall_exprList opl exprlist,
   replace_operCall_substitution opl subst)
and
    replace_operCall_expr opl e =
  match e with
  | ExprTrin(op,expr1,expr2,expr3) -> 
      ExprTrin(op,
              replace_operCall_expr opl expr1,
              replace_operCall_expr opl expr2,
              replace_operCall_expr opl expr3)
  | ExprBin(op,expr1,expr2) -> 
      ExprBin(op,
              replace_operCall_expr opl expr1,
              replace_operCall_expr opl expr2)
  | ExprUn(op,expr) -> 
      ExprUn(op,replace_operCall_expr opl expr)
  | ExprParen(expr) -> 
      ExprParen(replace_operCall_expr opl expr)
  | ExprId(id) -> 
      ExprId(id)
  | ExprBefore(id) -> 
      ExprBefore(id)
(*DP Essai: *)
  | ExprFunCall(expr1,expr2) -> 
      ExprFunCall(replace_operCall_expr opl expr1, replace_operCall_expr opl expr2)
(*       (match expr with 
         ExprId(id) ->
           (try
             (* on cherche l'operation qui va remplacer l'appel *)
             let (op_name,op_params_out,op_params_in,op_body) = List.find (fun (idop,_,_,_) -> Ident.equal idop id) opl
             in
             
           with Not_found -> ExprFunCall(replace_operCall_expr opl expr, replace_operCall_exprList opl exprList )
           )
       | _ -> ExprFunCall(replace_operCall_expr opl expr, replace_operCall_exprList opl exprList )
       )
*)

(**)
  | ExprSeq(exprSeq) -> 
      ExprSeq(replace_operCall_exprSeq opl exprSeq)
  | ExprSet(exprSet) -> 
      ExprSet(replace_operCall_exprSet opl exprSet)
  | ExprNumber(number) -> 
      ExprNumber(number)
  | ExprBoolConstant(boolcst) -> 
      ExprBoolConstant(boolcst)
(* SC: Normally, no operation call might appear in a predicate inside a bool() function *)
  | ExprBool(predValue) -> 
      ExprBool(predValue)
  | ExprString(string) -> 
      ExprString(string)
  | ExprRecords(records) ->
      ExprRecords(replace_operCall_exprRecords opl records)
  | ExprNuplet(exprList) -> 
      ExprNuplet(replace_operCall_exprList opl exprList)
  | ExprSIGMA(ids,pred1,expr2) -> 
      ExprSIGMA(ids,
                replace_operCall_predicate opl pred1,
                replace_operCall_expr opl expr2)
  | ExprPI(ids,pred1,expr2) -> 
      ExprPI(ids,
             replace_operCall_predicate opl pred1,
             replace_operCall_expr opl expr2)
  | ExprLambda(ids,pred1,expr2) -> 
      ExprLambda(ids,
                 replace_operCall_predicate opl pred1,
                 replace_operCall_expr opl expr2)
and
    replace_operCall_predicate opl p =
  match p with
    PredParen(pred) -> 
      PredParen(replace_operCall_predicate opl pred)
  | PredNegation(pred) -> 
      PredNegation(replace_operCall_predicate opl pred)
  | PredExists(ids,pred) -> 
      PredExists(ids,replace_operCall_predicate opl pred)
  | PredForAll(ids,pred) -> 
      PredForAll(ids,replace_operCall_predicate opl pred)
  | PredBin(bbop,pred1,pred2) -> 
      PredBin(bbop,
              replace_operCall_predicate opl pred1,
              replace_operCall_predicate opl pred2)
  | PredAtom(bbop,expr1,expr2) -> 
      PredAtom(bbop,
               replace_operCall_expr opl expr1,
               replace_operCall_expr opl expr2)
  | SubstApply(_) -> invalid_arg "instanciation:replace_operCall_predicate:SubstApply"
and
    replace_operCall_exprRecords opl records =
  match records with
  | RecordsFields(fields) ->
      let (oids, exprs) = List.split fields in
      RecordsFields(List.combine oids (replace_operCall_exprList opl exprs))
  | RecordsAccess(expr, id) ->
      RecordsAccess(replace_operCall_expr opl expr, id)
and
    replace_operCall_exprSeq opl exprSeq = 
  match exprSeq with
    SeqEnum(exprList) ->
      SeqEnum(replace_operCall_exprList opl exprList)
  | SeqEmpty -> SeqEmpty
and
    replace_operCall_exprSet opl exprSet = 
  match exprSet with
      SetPredefined(_) as s -> 
	s
    | SetEnum(exprList) -> 
	SetEnum(replace_operCall_exprList opl exprList)
    | SetComprPred(ids,pred) -> 
	SetComprPred(ids,replace_operCall_predicate opl pred)
    | SetEmpty -> 
	SetEmpty
    | SetUnionQ(ids,pred1,expr2) -> 
	SetUnionQ(ids,
		  replace_operCall_predicate opl pred1,
		  replace_operCall_expr opl expr2)
    | SetInterQ(ids,pred1,expr2) -> 
	SetInterQ(ids,
		  replace_operCall_predicate opl pred1,
		  replace_operCall_expr opl expr2)	
    | SetRecords(fields) ->
      let (ids, exprs) = List.split fields in
      SetRecords(List.combine ids (replace_operCall_exprList opl exprs))
        
let instanciate_operCall (name,ids_out,ids_in,body) opl =
  (name,ids_out,ids_in,replace_operCall_substitution opl body)
