exception Empty_list
val mk_path : string -> Modules.path
val mk_Id : string -> Blast.id
val mk_PredTrue : unit -> Blast.predicate
val mk_PredIn : Blast.expr -> Blast.expr -> Blast.predicate
val mk_PredNotIn : Blast.expr -> Blast.expr -> Blast.predicate
val mk_Disjunction : Blast.predicate list -> Blast.predicate
val mk_Conjunction : Blast.predicate list -> Blast.predicate
val mkSet : Blast.expr list -> Blast.expr
val mk_SubstSequence_from_list :
  Blast.substitution list -> Blast.substitution
val mk_SubstParallel_from_list :
  Blast.substitution list -> Blast.substitution
val mk_SubstSelectbasic :
  Blast.predicate -> Blast.substitution -> Blast.substitution
