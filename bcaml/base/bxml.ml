(* $Id$ *)

open Blast


(* To be moved to an XML generic module *)
let comment stuff  = "<!--\n"^stuff^"\n-->"

let encoding = "UTF-8"

let standalone = "standalone = no"

let version = "<?xml version = \"1.0\"?"
	      ^standalone
	      ^encoding
	      ^"?>\n" 

let declaration = version 

let doctype = "<!DOCTYPE AMN PUBLIC \"-//INRETS// AMN Markup Language 0.1//EN\"  >"

let copyright = "copyright INRETS-ESTAS 2000-2003" 

let bhead = comment "B/XML"^copyright

let balise name att stuff = 
(*MG debug
  let _ = Error.message name in
*)
	Xml.Eelement(name,att,stuff) 

let empty name = Xml.Eempty(name,[])

let rec flatten_list string_separator f l =
  match l with
    [] -> ""
  | [h] -> f h
  | h::t -> (f h)
      ^ string_separator 
      ^ (flatten_list string_separator f t)
	
let flatten f l = flatten_list "\n" f l

let rec id ident = 
  match ident with
      Id(path) -> balise "Id" [] [Xml.Echunk(Id_acc.name_of ident)]
(* SC : garder ces commentaires, ils servent au d�buggage du scoping! *)
(*      Id(path) -> balise "id" [] [Xml.Echunk(Id_acc.debug_id ident)] *)    
(*      Id(path) -> balise "id" [] [Xml.Echunk(Id_acc.name_of_path path)] *)   
    | IdTy(id2,_) -> id id2 


let  ids idlist =  balise "IdList" [] (List.map (id) idlist)

(* XML output of a string *)
let string s =   balise "STRING" [] [Xml.Echunk(s)]

(* XML output for numbers *)	
let number num =
  match num with
    MinNumber -> empty "MININT" 
  | MaxNumber -> empty "MAXINT"
  | Number(n) -> balise "Number" [] [Xml.Echunk(Int32.to_string n)]
	
(* XML output for boolean constants *)
let booleanConstant boolcst =
  match boolcst with
    | TrueConstant -> empty "TRUE"
    | FalseConstant -> empty "FALSE"
	
let rec expr expression =
  match expression with
  | ExprUn(op,expr1) ->
      balise (Bbop.op1_name op) [] [(expr_top expr1)]
  | ExprBin(op,expr1,expr2) ->
      balise (Bbop.op2_name op) [] [(expr_top expr1);(expr_top expr2)]
  | ExprTrin(op,expr1,expr2,expr3) ->
      balise (Bbop.op3_name op) [] [(expr_top expr1);(expr_top expr2);(expr_top expr3)]
(*MG
  | ExprSequence(expr1,expr2) ->
      balise "ExprSequence" [] [(expr_top expr1);(expr_top expr2)]
*)
  | ExprParen(expr1) ->
      balise "ExprParen" [] [expr expr1]
  | ExprId(id1) ->
      id id1 
  | ExprBefore(id1) ->
      balise "ExprBefore" [] [(id id1)]
  | ExprFunCall(expr1,expr2) ->
      balise "ExprFunCall" [] [(expr_top expr1);(expr_top expr2)]
  | ExprSeq(seq) ->
      exprSeq seq
  | ExprSet(set) ->
	exprSet set
  | ExprNumber(num) ->
      number num
  | ExprBoolConstant(bcst) ->
      booleanConstant bcst 
  | ExprBool(pred1) ->
      balise "BoolEvaluation" []  [pred_top pred1]
  | ExprString(str) ->
      string str
  | ExprRecords(record) ->
      exprRecords record
  | ExprNuplet(exprlist) ->
      balise "Maplet" [] (List.map (expr_top) exprlist)
  | ExprSIGMA(ids1,pred,expr) ->
      balise "SIGMA" [] [(ids ids1);(pred_top pred);(expr_top expr)]
  | ExprPI(ids1,pred,expr) ->
      balise "PI" [] [(ids ids1);(pred_top pred);(expr_top expr)]
  | ExprLambda(ids1,pred,expr) ->
      balise "Lambda" [] [(ids ids1);(pred_top pred);(expr_top expr)]
and 
    exprlist exprlist1 = 
  balise "ExprList" [] (List.map (expr_top) exprlist1)
and
    expr_top expr1 =
  balise "Expr" [] [expr expr1] 
and 
  exprSeq seq =
  match seq with
    SeqEnum(exprlist) ->
      balise "SeqEnum" [] (List.map expr_top exprlist)
  | SeqEmpty ->
      empty "SeqEmpty"
and
    substitution subs =
  match subs with
    | SubstBlock(subst) ->
	balise "SubstBlock" [] [substitution subst]

    | SubstSetEqualIds(idlist,exprlist1) ->
	balise "SetEqualIds" [] 
	[(ids idlist);(exprlist exprlist1)]

    | SubstSetEqualFun(fc,expr) ->      
	balise "SetEqualFun" [] 
        [(funchain fc);(expr_top expr)]
    | SubstSetEqualRecords(fc,expr) ->      
	balise "SetEqualRecords" []
	[(fieldchain fc); (expr_top expr)]
    | SubstBecomeSuch(idlist,pred1) ->
	balise "SubstBecome" [] [(ids idlist); pred_top pred1]

    | SubstSetIn(ids1,expression) ->
	balise "SubstSetIn" [] [(ids ids1) ; expr_top expression]

    | SubstPrecondition(pred,subst) ->
	(balise "SubstPre" [] 
	   [(balise "PRE" [] [pred_top pred ])
	   ; (balise "THEN" [] [substitution subst])])

    | SubstAssertion(pred,subst) -> 
	balise "SubstAssert" [] [pred_top pred;substitution subst]

    | SubstChoice(substlist) ->
	balise "SubstChoice" [] (List.map substitution substlist)

    | SubstIf(predsubstList,substOption) ->      
	balise "SubstIf" [] 
	((List.map elseIf predsubstList)@(substitution_option substOption))

    | SubstSelect(whenpartlist,substOption) -> 
	let l = (List.map whenpart whenpartlist) in 
	  (balise "SubstSelect" [] (l @ (substitution_option substOption) ))

    | SubstCase(expression,orpartlist1,substOption) ->      
	balise "SubstCase" [] 
	([expr_top expression;
          (orPartList orpartlist1)]
	 @substitution_option substOption)

    | SubstAny(idlist,pred,subst) ->      
	balise "SubstAny" [] 
	[(ids idlist);(pred_top pred);(substitution subst)]

    | SubstLet(idlist,pred,subst) ->      
	balise "SubstLet" [] 
        [(ids idlist);(pred_top pred);(substitution subst)]

    | SubstVar(idlist,subst) ->
	balise "SubstVar" [] 
	([ids idlist]
	 @[substitution subst])

    | SubstSequence(subst1,subst2) ->      
	balise "SubstSequence" [] 
	([(substitution subst1)
	 ; (substitution subst2)])

    | SubstWhile(pred1,subst,expression,pred2) ->      
	balise "SubstWhile" [] 
	([(balise "WhileTest" [] [(pred_top pred1)])
	 ; (balise "WhileBody" [] [(substitution subst)])
	 ; (balise "WhileInvariant" [] [(pred_top pred2)])
	 ; (balise "WhileVariant" [] [(expr_top expression)])
	 ])

    | SubstOperCall(ids1,id,exprlist1) ->    
	balise "SubstOperCall" [] 
        ([ids ids1]@[(expr_top (ExprId(id)));(exprlist exprlist1)]) 	

    | SubstParallel(subst1,subst2) ->      
	balise "SubstParallel" [] [(substitution subst1);(substitution subst2)]

    | SubstSkip ->
      	balise "SubstSkip" [] []

    | SubstInstanciation(idslist, exprslist, subst) -> 
	balise "SubstInstanciation" [] [
	  (ids idslist); 
	  (exprlist exprslist); 
	  (substitution subst)
	]
and
    fieldchain fc =
  match fc with
    SetRecVar(i) -> id i
  | SetRecField(f,i) -> balise "RecField" [] [(fieldchain f);(id i)]
and
    funchain fc =
  match fc with
    SetFunVar(i) -> id i
  | SetFunExpr(f,e) -> balise "FunExpr" [] [(funchain f);(expr_top e)]
and
    substitution_option substOption =
  match substOption with
    Some(s) -> [substitution s]
  | None -> []
and
    orPart (exprlist,subst) =
  balise "OrPart" [] ((List.map expr_top exprlist)@[(substitution subst)])
and    
    orPartList listorpart =
  balise "OrPartList" [] (List.map orPart listorpart)
and
  elseIf (pred,subst) =
  balise "IfThen" [] [(pred_top pred);(substitution subst)]
and
  funParams params = (
    if params != [] 
    then [(balise "fparams" [] (List.map expr_top params))]
    else []
  )
and
    predicat p = 
  match p with
    PredParen(p) ->
      balise "PredParen" []  [(predicat p)]
  | PredBin(op,pred1,pred2) ->
      balise (Bbop.bop2_name op) [] [(predicat pred1);(predicat pred2)]   
  | PredAtom(op,expr1,expr2) ->
      balise (Bbop.bop2_name op) [] [(expr expr1);(expr expr2)]   
  | PredNegation(pred) ->
      balise "Neg" []   [(predicat pred)]    
  | PredExists(ids1,pred) ->
      balise "Exists" [] [(ids ids1);(pred_top pred)]
  | PredForAll(ids1,pred) ->    
      balise "ForAll" [] [(ids ids1);(pred_top pred)]
(* SC : � discuter �galement *)
  | SubstApply(subst, pred) -> 
      balise "SubstApply" [] [(substitution subst); (pred_top pred)]
and
    pred_top pred1 = (balise "Predicate" [] [(predicat pred1)])

and    
    type_B t =
  match t with 
    TypeNatSetRange(e1,e2) ->
      balise "Range" [] [(expr e1);(expr e2)]
  | TypeIdentifier(id1) ->
      id id1
  | TypeBaseSet(bSet) ->
      setName bSet
  | _ -> raise (Error.Not_implemented "bxml:Btypes not implemented, sorry")
and
  head (ident,param) =
  let p = balise "HeadPar" [] (List.map id param) in
  balise "Head" [] [(id ident);p]
and    
 amn ast =
  match ast with
    Machine (entete,clauses1) ->
      balise "AMN" [] [
      balise "MACHINE" [] [(head entete);(clauses clauses1 None)]
    ]
  | Refinement(entete,refinesName,clauses1) ->
      balise "AMN" [] [
      balise "REFINEMENT" [] [
      (head entete);(clauses clauses1 (Some (balise "REFINES" [] [id
refinesName])))]
    ]
  | Implementation(entete,implementationName,clauses1) ->
         balise "AMN" [] [
      balise "IMPLEMENTATION" [] [(head entete);(clauses clauses1 (Some
(balise "REFINES" [] [id implementationName])))]
    ]
  | EmptyTree -> balise "AMN" [] [empty "EmptyTree"]
and
    clauses clausess option =
match option with
| None ->
    balise "ClausesList" []  (List.map clause clausess)
| Some(a) ->
    balise "ClausesList" []  (a::(List.map clause clausess))
and
    clause clauseAMN =
  match clauseAMN with
  |  Constraints(pred) ->
      balise  "CONSTRAINTS" [] [(pred_top pred)]
  |  Invariant(pred) ->
      balise  "INVARIANT" [] [(pred_top pred)]
  |  Sets (setlist) ->
      balise  "SETS"  [] (List.map set setlist)
  |  Initialisation(subst) ->
      balise  "INITIALISATION" []  [substitution subst]
  |  ConstantsConcrete(idlist) ->
      balise  "CCONSTANTS" []  [ids idlist]
  |  ConstantsAbstract(idlist) ->
      balise  "ACONSTANTS" []  [ids idlist]
  |  ConstantsHidden(idlist) ->
      balise  "HCONSTANTS" []  [ids idlist]
  |  VariablesHidden(idlist) ->
      balise  "HVARIABLES" []  [ids idlist]
  |  Properties(pred) ->
      balise  "PROPERTIES"  [] [pred_top pred]
  |  Values(valuationlist) ->
	 balise  "VALUES"  [] (List.map valuation valuationlist)
  |  VariablesConcrete(ids1) ->
      balise  "CVARIABLES"  [] [ids ids1]
  |  VariablesAbstract(ids1) ->
      balise  "AVARIABLES"  [] [ids ids1]
  |  Promotes(idlist) ->
      balise  "PROMOTES"  [] (List.map id idlist)
  |  Extends(instancelist) ->
      balise  "EXTENDS"  [] (List.map instance instancelist)
  |  Assertions(predlist) ->
      balise  "ASSERTIONS"  [] (List.map pred_top predlist)
  |  LocalOperations(operationlist) ->
      balise  "LOCAL_OPERATIONS"  [] (List.map operation operationlist)
  |  Operations(operationlist) ->
      balise  "OPERATIONS"  [] (List.map operation operationlist)
  |  Sees(idlist) ->
      balise "SEES"  [] (List.map id idlist)
  |  Uses(idlist) ->
      balise  "USES"  [] (List.map id idlist)
  |  Includes(instancelist) ->
      balise  "INCLUDES"  [] (List.map instance instancelist)
  |  Imports(instancelist) ->
      balise  "IMPORTS"  [] (List.map instance instancelist)
and 
    valuation (ident,expression) =
  balise "Valuation" [] [(id ident);(expr_top expression)]
and 
    set ens = match ens with
      SetAbstDec(ident) ->
	balise "SetAbstract" [] [(id ident)]
    | SetEnumDec(ident,idlist) ->
	balise "SetEnumDec" [] ([id ident]@[ids idlist])
and 
    instance (ident,exprlist1) =
  balise "instance" []  ([id ident]@[exprlist exprlist1])
and
    field (id1,typ) =  balise "field" [] [id id1;type_B typ]
and
    ids_params ids1  =  match ids1 
    with 
      [] -> [Xml.Echunk("")]
    | _ ->
	[ids ids1]	  
and
    operation (name,ids_out,ids_in,body) =
  let oout = match ids_out with
    [] -> []
  | _ -> [balise "OperOut" [] (ids_params ids_out)]
  in
  let oin  = match ids_in with
    [] -> []
  | _ -> [balise "OperIn" [] (ids_params ids_in)]
  in
  balise "Operation" [] 
    ([(balise "OperHead" [] 
	 (
	  [balise "OperName" [] [(id name)]]
	  @ oout
	  @ oin
	 )
      )]
     @
       [(balise "OperBody" [] [(substitution body)])])
    (*MG
and 
  event (name,ids_in,body) =  operation(name,[],ids_in,body)   
   *)
and
    exprRecords record =
  match record with
    RecordsFields(fields) ->
      balise "RecordsFields" [] (List.map recordsItem_opt fields)
  | RecordsAccess(expr,id1) ->
      balise "RecordsAccess" [] [(expr_top expr); (id id1)]
and
    recordsItem_opt (id_opt,expr) =
  match id_opt with
    None -> balise "Field" [] [(expr_top expr)]
  | Some(id1) -> balise "Field" [] [(id id1);(expr_top expr)] 
and
    predTyping (ids1,expr1)=
  balise "PredType" [] ([ids ids1]@[(expr expr1)])
and
    exprSet set =
  match set with
  | SetPredefined(setName1) ->
      balise "SetPredefined" [] [setName setName1]
  | SetEnum(exprlist) ->
      balise "SetEnum" [] (List.map expr_top exprlist)
  | SetComprPred(ids1,pred) ->
      balise "SetComprPred" [] ([ids ids1]@[(pred_top pred)])
  | SetEmpty -> 
      empty "SetEmpty"
  | SetUnionQ(ids1,pred,expr) ->
      balise "SetUnionQ" [] ([ids ids1]@[(pred_top pred);(expr_top expr)])
  | SetInterQ(ids1,pred,expr) ->
      (balise "SetInterQ") [] ([ids ids1]@[(pred_top pred);(expr_top expr)])
  | SetRecords(recordsItemlist) ->
       balise "SetRecords" [] (List.map recordsItem recordsItemlist)
and recordsItem (id1,expr) = balise "Field" [] [(id id1);(expr_top expr)]
and
    setName n =
  match n with
    NAT -> 
      empty "NAT"
  | NATURAL ->
      empty "NATURAL"
  | NAT1 ->
      empty "NAT1"
  | NATURAL1 ->
      empty "NATURAL1"
  | INTEGER ->
      empty "INTEGER"
  | INT ->
      empty "INT"
  | STRING ->
      empty "STRING"
  | BOOL ->
      empty "BOOL"
and
    boolConstant b =
  match b with 
    TrueConstant ->
      empty  "TRUE"
  | FalseConstant ->
      empty "FALSE"
and 
    whenpart (pred,subst) =
  balise "WhenPart" [] [(pred_top pred);(substitution subst)]
    
let doc r =
  let d = Xml.XMLDecl("1.0",Some(false),Some("ISO-8859-1")) in
(* *) 
let dtd = Some(Xml.DTD("AMN",Some(Xml.DTDsys("AMN.dtd")))) in 
(* *)
(* MG
    let dtd = None in 
*)
  let p = Xml.Prolog(Some(d),dtd,[]) in 
  Xml.XML(p,r,[]) 

let to_chan chan ast=  
  let xmlamn = amn ast in  
  let xmldoc = doc xmlamn in  
  let linelen = 80 in  
    begin  
      Pp.ppToFile chan linelen (Ppxml.ppDoc xmldoc);  
      flush chan  
    end  
    

let to_string ast=
  let xmlamn = amn ast in  
  let xmldoc = doc xmlamn in  
  let linelen = 80 in  
    Pp.ppToString linelen (Ppxml.ppDoc xmldoc);  
