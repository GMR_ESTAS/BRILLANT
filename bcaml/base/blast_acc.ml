(* $Id$  *)

(*
   Manipulations d'un arbre abstrait B
   Dorian PETIT -- Georges MARIANO
   INRETS-ESTAS copyright 2000,2001,2002,2003
*)


open Blast
open Blast_mk
(* open Bbop *)
open Modules


exception Empty_tree
exception Empty_list
exception Not_a_refinement

(*
  \section{Fonctions d'acc�s aux �lements d'un arbre abstrait}
*)
let get_head ast =
  match ast with
  | Machine(h,_) -> h
  | Refinement(h,_,_) -> h
  | Implementation(h,_,_) -> h
  | EmptyTree -> raise Empty_tree
	
let get_name_from_head (id,_) = id

let get_name ast =
  get_name_from_head (get_head ast)

let get_name_from_instance (id,_) = id
let instance_params_of (_,insparams) = insparams

let get_amn_head_name (id,_) = 
  Id_acc.name_of id

let get_refined_name ast =
  match ast with
  | Machine(_,_) -> raise Not_a_refinement
  | Refinement(_,r,_) -> r
  | Implementation(_,r,_) -> r
  | EmptyTree -> raise Empty_tree
	
	
let get_clause_list ast =
  match ast with
  | Machine(_,l) -> l
  | Refinement(_,_,l) -> l
  | Implementation(_,_,l) -> l
  | EmptyTree -> raise Empty_tree


(* SC:let parList_to_idlist parlist= *)
let untype_idlist parlist=
  let rec untype id=
    match id with 
    | IdTy(ident,typ) -> untype ident
    | i -> i
  in
  List.map untype parlist


let get_head_var_list (name,params) = untype_idlist params

let get_params ast = get_head_var_list (get_head ast)


(* 
   \section{Concatenation de clauses}
 *)

(* TODO: SC : accesseurs ! *)
let append_list l1 l2 = 
  l1@l2


let append_predicate p1 p2 =
  PredBin(Bbop.And, p1, p2)


let append_substitution subst1 subst2 =
  SubstSequence(subst1, subst2)


let append_SEES = append_list
let append_SETS = append_list
let append_CONSTANT = append_list
let append_CONSTANT_ABSTRACT = append_list
let append_VARIABLES = append_list
let append_ABSTRACT_VARIABLES = append_list
let append_CONCRETE_VARIABLES = append_list

let append_PROPERTIES = append_predicate
let append_INVARIANT = append_predicate

let append_INITIALISATION = append_substitution



(* \section{Accesseurs plus complexes} *)


let get_seen_mchids ast=
  try 
    Clause_acc.get_Sees (get_clause_list ast) 
  with 
    Empty_tree -> 
      raise (Invalid_argument "get_seen_id_list")
        
        
let get_included_mchinsts ast=
  try 
    Clause_acc.get_Includes (get_clause_list ast) 
  with Empty_tree -> 
    raise Empty_list
      
let get_included_mchids ast=
  try 
    fst (List.split (get_included_mchinsts ast))
  with Empty_list -> 
    raise (Invalid_argument "get_included_mchids")
  
let get_used_mchids ast=
  try 
    Clause_acc.get_Uses (get_clause_list ast) 
  with Empty_tree -> 
    raise (Invalid_argument "get_used_mchids")
      
      
let get_imported_mchinsts ast=
  try 
    Clause_acc.get_Imports (get_clause_list ast) 
  with Empty_tree -> 
    raise Empty_list
      
let get_imported_mchids ast=
  try 
    fst (List.split (get_imported_mchinsts ast))
  with Empty_list -> 
    raise (Invalid_argument "get_imported_mchids")
      
let get_extended_mchinsts ast=
  try 
    Clause_acc.get_Extends (get_clause_list ast) 
  with Empty_tree -> 
    raise Empty_list

let get_extended_mchids ast=
  try 
    fst (List.split (get_extended_mchinsts ast))
  with Empty_list -> 
    raise (Invalid_argument "get_extended_mchids")
      
let get_implementation_values ast=
  try 
    Clause_acc.get_Values (get_clause_list ast) 
  with Empty_tree -> 
    raise Empty_tree
      
      
let get_abstract_constants ast=
  try 
    Clause_acc.get_ConstantsAbstract (get_clause_list ast) 
  with Empty_tree -> 
    raise Empty_tree

      
let is_abstract_machine mch=
  match mch with
  | Machine(_) -> true
  | _ -> false


let is_refinement mch=
  match mch with
  | Refinement(_) -> true
  | _ -> false


let is_implementation mch=
  match mch with
  | Implementation(_) -> true
  | _ -> false


let is_modularity_clause c=
  match c with
(*    | Blast.Promotes(_) *)
(*    | Blast.Sees(_) *)
    | Uses(_)
(*    | Blast.Extends(_) *)
    | Includes(_)
    | Imports(_) -> true
    | _ -> false



let is_basic_machine mch=
  let clauses = get_clause_list mch in
  
  let rec test_clauses cls=
    match cls with
    | cl::othercls ->
	if is_modularity_clause cl
	    then false
	    else test_clauses othercls
    | [] -> true
  in

  test_clauses clauses


(* On a side note, the numbers can also be used to put clauses in a
   correct order *)
let clause_number clause=
  match clause with
    | Sets(_) -> 1
    | ConstantsConcrete(_) -> 2
    | ConstantsAbstract(_) -> 3
    | ConstantsHidden(_) -> 4
    | VariablesConcrete(_) -> 5
    | VariablesAbstract(_) -> 6
    | VariablesHidden(_) -> 7
    | Values(_) -> 8
    | Constraints(_) -> 10
    | Properties(_) -> 11
    | Assertions(_) -> 12
    | Sees(_) -> 13
    | Uses(_) -> 14
    | Includes(_) -> 15
    | Imports(_) -> 16
    | Promotes(_) -> 17
    | Extends(_) -> 18
    | Invariant(_) -> 20
    | Initialisation(_) -> 21
    | LocalOperations(_) -> 22
    | Operations(_) -> 23


let rec removeClause clause clauseList=
  match clauseList with
    | [] -> [] 
    | oneClause::others->
	if (clause_number oneClause)=(clause_number clause) 
	then removeClause clause others
	else oneClause::(removeClause clause others)


(* Replaces in the \emph{machine} all clauses of the type of \emph{clause1} by
   \emph{clause2}. Note that there is no need for the clauses to be replaced, to
   be actually equal to \emph{clause1} *)
let replace_clause machine clause1 clause2=
  if machine=EmptyTree
  then invalid_arg "replace_clause : EmptyTree"
  else
    let clauses=get_clause_list machine in
    let newclauses=(removeClause clause1 clauses)@[clause2] in
      match machine with
	| Machine(i,_) -> Machine(i, newclauses)
	| Refinement(i,r,_) -> Refinement(i, r, newclauses)
	| Implementation(i,r,_) -> Implementation(i, r, newclauses)
	| EmptyTree ->invalid_arg "replace_clause : EmptyTree"


let sortClauses machine=
  let clauses=get_clause_list machine in
  let newClauses = List.sort (fun c1 c2 -> (clause_number c1) - (clause_number c2)) clauses in
    match machine with
      | Machine(i,_) -> Machine(i, newClauses)
      | Refinement(i,r,_) -> Refinement(i, r, newClauses)
      | Implementation(i,r,_) -> Implementation(i, r, newClauses)
      | EmptyTree -> machine
     
