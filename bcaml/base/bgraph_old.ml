(* $Id$ *)

open Blast
open Modules
open Id_acc

(*
   Ce module Bgraph permet de r�colter des informations sur les liens de d�pendance entre les diff�rents composants d'un projet $B$. Bgraph permet donc de g�n�rer un graphe de d�pendance du projet $B$.
*)

(* \unfoldsec{D�finition des types}
Chaque d�pendances entre composants est d�finie par 
 \begin{itemize}
 \item un "type" (raffinement, sees, imports \ldots)
 \item une origine/source  (abstract machine) 
 \item une cible/target (abstract machine).   
 \end{itemize}
*)

type 
      graph = dependency list
and
      dependency = (linkType * amnName * amnName)
and
      amnName = Blast.id
and 

(* \unfoldsec{Les types de liens} *) 
(*
   Ici le type des liens est directement li� et d�pendant du module Blast.
*)
      linkType =     
    PromotesLink of string
  | ExtendsLink of string * Blast.expr list
  | SeesLink of string
  | UsesLink of string
  | IncludesLink of string * Blast.expr list
  | ImportsLink of string * Blast.expr list
  | RefinesLink
  | NoLink


(* \unfoldsec{Les constructeurs et les accesseurs} *)

(*i*)
(* TD-- Jamais utilis�
let emptyNode = 
  (NoLink,"","")
*)
(*i*)

(* \unfoldsubsec{emptyGraph}
   [emptyGraph] : ['a list] \\
   Un graphe vide est une liste vide. 
 *)

let emptyGraph = []

(* \unfoldsubsec{is\_empty}
   [is_empty] : [graph] -> [bool] \\
   Teste si un graphe est vide.
 *)

let is_empty graph = ( graph = emptyGraph )

(* \unfoldsubsec{nodeMake}
   [nodeMake] : [linkType] -> [amnName] -> [amnName] -> [(linkType * amnName * amnName)] \\
   Cr�ation d'un lien � partir des trois information : la source, la cible et le lien qui les unient. 
 *)

let nodeMake link source target =
  (link, source, target)

(* \unfoldsubsec{source\_amn}
   [source_amn] : [(linkType * amnName * amnName)] -> [string] \\
   Retourne le nom de la source d'un lien. 
 *)

let source_amn (_,source,_) = 
  name_of source

(* \unfoldsubsec{source\_amn\_id}
   [source_amn] : [(linkType * amnName * amnName)] -> [amnName] \\
   Retourne l'ID de la source d'un lien. 
 *)

let source_amn_id (_,source,_) = 
  source

(* \unfoldsubsec{link\_type}
   [link_type] : [(linkType * amnName * amnName)] -> [linkType] \\
   Retourne le type d'un lien. 
 *)

let link_type (link,_,_) = 
  link

(* \unfoldsubsec{link\_type\_info}
   [link_type_info] : [linkType] -> [(string * Blast.instance list)] \\
   Retourne les infos d'un lien donn�. 
 *)

let link_type_info link =
  match link with  
    PromotesLink(str) -> 
      (str,[])
  | ExtendsLink(str,exprList) -> 
      (str,exprList)
  | SeesLink(str) -> 
      (str,[])
  | UsesLink(str) -> 
      (str,[])
  | IncludesLink(str,exprList) -> 
      (str,exprList)
  | ImportsLink(str,exprList) -> 
      (str,exprList)
  | RefinesLink -> 
      raise (Invalid_argument "link_type_info : no info on this link type")
  | NoLink -> 
      raise (Invalid_argument "link_type_info : no link")

(* \unfoldsubsec{target\_amn}
   [target_amn] : [(linkType * amnName * amnName)] -> [string] \\
   Retourne le nom de la cible d'un lien. 
 *)

let target_amn (_,_,target) =
  name_without_prefix target

(* \unfoldsubsec{target\_amn\_id}
   [target_amn] : [(linkType * amnName * amnName)] -> [amnName] \\
 Retourne l'ID de la cible d'un lien. *)

let target_amn_id (_,_,target) =
  target

(* \unfoldsubsec{addNode}
   [addNode] : [graph] -> [dependency] -> [graph] \\
   Ajoute un lien � un graphe. 
 *)

let addNode graphe node =
   let (ltype,_,_) = node
   in
     match ltype with
	 NoLink -> graphe
       | _ -> node::graphe
		      
(* \unfoldsubsec{append}
   [append] : [graph] -> [graph] -> [graph] \\
   Concat�ne deux graphes. 
 *)

let append graphe1 graphe2 =
  List.append graphe1 graphe2

(* \unfoldsubsec{librairie Caml}
   Utilisation des fonctions pr�sentes dans CAML. 
 *)

let mem = List.mem
let iter = List.iter
let partition = List.partition
let map = List.map
let rec walker order graph start = 
  let next_list = order start graph
  in
  List.iter (walker order graph) next_list
let fold = List.fold_left 
let card g = List.fold_left (fun cpt noeud -> cpt+1) 0 g
let exists = List.exists
let find = List.find
let find_all = List.find_all
let filter = List.filter

(* \unfoldsubsec{delete}
   [delete] : [dependency] -> [graph] -> [graph] \\
   D�truire une d�pendance dans un graphe. *)

let delete node graph =
  List.rev (fold (fun g dep -> if dep = node then g else (addNode g dep)) emptyGraph graph )


let enleve_doublon graph =
  fold 
    (fun g dep -> if (mem dep g) then g else addNode g dep)
    emptyGraph
    graph

(* \unfoldsec{Les accesseurs sur les d�pendances} *)

(* \unfoldsubsec{match\_\textit{Type\_de\_lien}}
   [match_Type_de_lien] : [dependency] -> [bool] \\
   Fonctions permettant de filtrer un graphe en fonction d'un lien.
 *)

let match_Promotes dependency =
  match dependency with
    (PromotesLink(_),_,_) -> true
  | _ -> false

let match_Extends dependency =
  match dependency with
    (ExtendsLink(_,_),_,_) -> true
  | _ -> false

let match_Sees dependency =
  match dependency with
    (SeesLink(_),_,_) -> true
  | _ -> false

let match_Uses dependency =
  match dependency with
    (UsesLink(_),_,_) -> true
  | _ -> false

let match_Includes dependency =
  match dependency with
    (IncludesLink(_,_),_,_) -> true
  | _ -> false

let match_Imports dependency =
  match dependency with
    (ImportsLink(_,_),_,_) -> true
  | _ -> false

let match_Refines dependency =
  match dependency with
    (RefinesLink,_,_) -> true
  | _ -> false

(* \unfoldsubsec{match\_source}
   [match_source] : [dependency] -> [string] -> [bool] \\
   Permet de s�lectionner une d�pendance d'un graphe � partir du nom de la source. 
 *)

let match_source dep amn_source =
  (source_amn dep) = amn_source

(* \unfoldsubsec{match\_source\_id}
   [match_source_id] : [dependency] -> [amnName] -> [bool] \\
   Permet de s�lectionner une d�pendance d'un graphe � partir du nom de la source (Id). 
 *)

let match_source_id dep amn_source =
  (source_amn_id dep) = amn_source

(*i*)
(*TD*)
let rec est_ss_chaine sc1 sc2 =
  if (String.length sc1)>(String.length sc2)
  then false
  else
    if (sc1=(String.sub sc2 0 (String.length sc1)))
    then true
    else est_ss_chaine sc1 (String.sub sc2 1 (String.length sc2-1))
	
let match_target_sub dep amn_target =
  est_ss_chaine (target_amn dep) amn_target

let dependencies_with_target_sub graphe amn_name =
  find_all (fun dep -> match_target_sub dep amn_name) graphe
(*TD*)
(*i*)

(* \unfoldsubsec{match\_target}
   [match_target] : [dependency] -> [string] -> [bool] \\
   Permet de s�lectionner une d�pendance d'un graphe � partir du nom de la cible. 
 *)

let match_target dep amn_target =
  (target_amn dep) = amn_target

(* \unfoldsubsec{dependencies\_with\_link}
   [dependencies_with_link] : [graph] -> ([dependency] -> [bool]) -> [graph] \\
   Permet de s�lectionner une/des d�pendance(s) d'un graphe � partir d'un type de lien. 
 *)

let dependencies_with_link graphe match_link =
  find_all match_link graphe

(* \unfoldsubsec{dependencies\_with\_source}
   [dependencies_with_source] : [graph] -> [string] -> [graph] \\
   Permet de s�lectionner une/des d�pendance(s) d'un graphe � partir d'un nom de source. 
 *)
    
let dependencies_with_source graphe amn_name =
  find_all (fun dep -> match_source dep amn_name) graphe

(* \unfoldsubsec{dependencies\_with\_source\_id}
   [dependencies_with_source_id] : [graph] -> [amnName] -> [graph] \\
   Permet de s�lectionner une/des d�pendance(s) d'un graphe � partir d'un nom de source (Id). 
 *)

let dependencies_with_source_id graphe amn_name_id =
  find_all (fun dep -> match_source_id dep amn_name_id) graphe

(* \unfoldsubsec{dependencies\_with\_target}
   [dependencies_with_target] : [graph] -> [string] -> [graph] \\
   Permet de s�lectionner une/des d�pendance(s) d'un graphe � partir d'un nom de cible. 
 *)

let dependencies_with_target graphe amn_name =
  find_all (fun dep -> match_target dep amn_name) graphe


let get_root_node graph =
  let get_sources gr = fold (fun lnodes dep -> (source_amn_id dep)::lnodes) [] gr
  in
  let enleve_doublon liste = 
    List.fold_left (fun l el -> if List.mem el l then l else el::l) [] liste
  in
  let id_all_nodes = get_sources graph
  in
  let _ = 
    print_string ("OUTPUT DES SOURCES NODES :");
    List.iter (fun el -> print_string (" "^(name_of el))) (enleve_doublon id_all_nodes) ;
    print_string (" FIN OUTPUT DES SOURCES NODES \n");
    flush stdout
  in
  let root_nodes_graph = 
    List.fold_left
      (fun id_res_l id_node -> 
        if (exists (fun dep -> match_target dep (name_of id_node)) graph)
        then id_res_l 
        else id_node::id_res_l
      )
      []
      (enleve_doublon id_all_nodes)
  in
  let id_root_nodes = enleve_doublon root_nodes_graph
  in
  let _ = 
    print_string ("OUTPUT DES ROOT NODES :");
    List.iter (fun el -> print_string (" "^(name_of el))) id_root_nodes ;
    print_string (" FIN OUTPUT DES ROOT NODES \n");
    flush stdout
  in
  match id_root_nodes with 
    [h] -> name_of h
  |  [] -> raise (Invalid_argument "get_root_node empty list of root nodes ")
  |  _ -> raise (Invalid_argument "get_root_node more than one root node")

(* \unfoldsec{Cr�ation de liens} *)
(* \unfoldsubsec{build\_link}
   [build_link] : [amnName] -> [Blast.clause] -> [graph] \\
   On poss�de une clause et le nom du composant qui poss�de cette clause et on produit un lien contenant toutes les informations n�cessaires. 
 *)

let build_link source_name clause =
  match clause with
  | Promotes(ids) -> 
      List.fold_left 
	(fun g id -> 
          addNode  
	    g 
	    (nodeMake 
	       (PromotesLink(prefix_of id)) source_name id)
	)
        emptyGraph  
        ids 
  | Sees(ids) -> 
      List.fold_left 
        (fun g id -> 
          addNode  
	    g 
	    (nodeMake (SeesLink(prefix_of id)) source_name id)
	)
        emptyGraph  
        ids 
  | Uses(ids) -> 
      List.fold_left 
        (fun g id -> 
          addNode  
	    g 
	    (nodeMake (UsesLink(prefix_of id)) source_name id)
	)
        emptyGraph  
        ids 
  | Extends(instanceList) -> 
      List.fold_left 
        (fun g instance -> 
          addNode 
	    g
	    (nodeMake 
	       (ExtendsLink
		  (prefix_of (Blast_acc.get_name_from_instance instance), 
		   Blast_acc.instance_params_of instance)
	       )	       
	       source_name 
	       (Blast_acc.get_name_from_instance instance))
	)
        emptyGraph
        instanceList
  | Includes(instanceList) -> 
      List.fold_left 
          (fun g instance -> 
            addNode 
	      g
	      (nodeMake 
		 (IncludesLink(
		  prefix_of (Blast_acc.get_name_from_instance instance), 
		  Blast_acc.instance_params_of instance)
		 )
		 source_name 
		 (Blast_acc.get_name_from_instance instance))
	  )
        emptyGraph
        instanceList
  | Imports(instanceList) -> 
      List.fold_left 
        (fun g instance -> 
          addNode 
	    g
	    (nodeMake 
	       (ImportsLink(prefix_of (Blast_acc.get_name_from_instance instance), 
			    Blast_acc.instance_params_of instance)
	       )
	       source_name 
	       (Blast_acc.get_name_from_instance instance))
	)
        emptyGraph
        instanceList
  | _ -> emptyGraph

(* \unfoldsubsec{build}
   [build] : [Blast.ast] -> [graph] \\
   Permet de construire un graphe de d�pendance � partir de l'arbre abstrait, ceci en appelant de mani�re it�rative 
   la fonction [build_link]. *)

let build ast =
  match ast with
      EmptyTree -> emptyGraph
    | Machine(head,clauseList) ->  
	List.fold_left 
          (fun g clause -> 
            append 
	      g
	      (build_link (Blast_acc.get_name_from_head head) clause))
          emptyGraph
          clauseList
     | Refinement(head,idrefined,clauseList) -> 
	 List.fold_left 
           (fun g clause -> 
              append 
		g
		(build_link (Blast_acc.get_name_from_head head) clause))
           (addNode emptyGraph 
	      (nodeMake RefinesLink 
		 idrefined
		 (Blast_acc.get_name_from_head head)))
(*c 
   Pour revenir � une forme Refines au lieu de \verb+is_refined_by+, il suffit 
   d'inverser les deux param�tres de l'appel pr�c�dent. 
*)
           clauseList
     | Implementation(head, idrefined, clauseList) -> 
	 List.fold_left 
           (fun g clause -> 
              append 
		g
		(build_link (Blast_acc.get_name_from_head head) clause))
           (addNode emptyGraph 
	      (nodeMake RefinesLink 
		 idrefined
		 (Blast_acc.get_name_from_head head)))
           clauseList

 
(* \unfoldsec{D�compilation du graphe}
   Cela permet de sauver dans un fichier, ou encore de visualiser � l'�cran le graphe g�n�r� (informations, D�bogage, ...).
 *)

(* SC : on met la decompilation en veilleuse en attendant 
 let dc_link link source target =
   let param str =
     if String.length str >0 then "\t params=\""^str^"\" " else ""
   in
   let rename str =
     if String.length str >0 then "\t rename=\""^str^"\" " else ""
   in
   let (dc_link,dc_rename,dc_param) = 
     match link with
	 PromotesLink(str) 
	 -> ("PROMOTES",(rename str),"")
       | ExtendsLink(str,exprList) 
	 -> ("EXTENDS",(rename str),(Blast_dc.dc_exprlist exprList))
       | SeesLink(str) 
	 -> ("SEES",(rename str),"")
       | UsesLink(str)
	 -> ("USES",(rename str),"")
       | IncludesLink(str,exprList)
	 -> ("INCLUDES",(rename str),(Blast_dc.dc_exprlist exprList))
       | ImportsLink(str,exprList)
	 -> ("IMPORTS",(rename str),(Blast_dc.dc_exprlist exprList))
       | RefinesLink
	 -> ("REFINES","","")
       | NoLink 
	 -> raise (Invalid_argument "dc_link")
   in
   "<"
   ^dc_link
   ^" source=\""^source^"\""
   ^"\t target=\""^target^"\""
   ^(dc_rename)
   ^(param dc_param)
   ^"\\>\n"    
 
 let dc_dependency stdoutput dependency =
   output_string 
     stdoutput 
     (dc_link 
	(link_type dependency) 
	(source_amn dependency) 
	(target_amn dependency)
     )
      
 let dc_graph stdoutput graphe =
   iter (dc_dependency stdoutput) graphe;
   flush stdoutput
*)

