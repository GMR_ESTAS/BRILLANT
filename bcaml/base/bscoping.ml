(*
  $Id$ 

  Module implementant la phase de scoping decrite dans a modular module system
  (annexe web) de Xavier Leroy. Il s'agit en fait de coder les dependances
  entre les entites pour la passe necessaire apres l'analyse syntaxique.

  Le scoping ne concerne ici que l'arbre syntaxique, la phase se rapportant
  aux modules se fera dans le module Bmodscoping.

  Dorian Petit : dorian.petit@inrets.fr
 *)

(*i*)
(*
   RUBRIQUE A FAIRE :

  - regarder ce qu'il est utile de stocker quand on d�clare un id (location,
  type de donn�e qu'il designe,...)

  - regarder comment on gere les ensembles enumeres : est ce que chaque
  element de cet ensemble est lui meme � considerer comme un id ?

  - regarder si dans type_B on doit faire du scope ou du scope_dec

  - idem que precedemment pour les records

  - 
 *)
(*i*)

(* \section{Module scoping} *)

open Blast
open Modules

(* \subsection{Machine clauses sorting} *)


(* This function assigns values to clauses, so to determine the order we will
   scope the clauses of the machine. Thus, once sorted, the scoping of the
   clauses will be done in that order. This is useful to address problems of
   visibility of some variables in the machine (for example : the invariant must
   see the parameters, the sets, the constants and the variables *)
let value_of clause=
  match clause with
    | Constraints(_) -> 1
    | Sees(_) -> 2
    | Includes(_) -> 3
    | Imports(_) -> 3
    | Promotes(_) -> 3
    | Extends(_) -> 3
    | Uses(_) -> 4
    | Sets(_) -> 5
    | ConstantsConcrete(_) -> 6
    | ConstantsAbstract(_) -> 6
    | ConstantsHidden(_) -> 6
    | Properties(_) -> 7
    | Values(_) -> 8
    | VariablesConcrete(_) -> 9
    | VariablesAbstract(_) -> 9
    | VariablesHidden(_) -> 9
    | Invariant(_) -> 10
    | Assertions(_) -> 10
    | Initialisation(_) -> 11
    | LocalOperations(_) -> 11
    | Operations(_) -> 11


let compare_clauses clause1 clause2=
  let value1=value_of clause1 in
  let value2=value_of clause2 in
    compare value1 value2


let sort_clauses cllist=
  List.fast_sort compare_clauses cllist


(* \subsection{Fonctions g�n�riques} *)

let funid a b = (b,a)

let funmap scope_alpha scope_env l = 
  List.map (fun el -> fst(scope_alpha scope_env el)) l


(* scope_alpha_list make the scoping of the alpha_list in the order: scope
   first, get the scoping environment, scope the second with that environment,
   etc... Now due to the importance of the order of the scoping of clauses, this
   behaviour is mandatory for this function *)
let scope_alpha_list scope_alpha scope_env alpha_list =
  let (new_alpha_list,scope_env_new) =
    List.fold_left 
      (fun (alpha_l,sc_env) alpha -> 
         let (new_alpha,new_env) = scope_alpha sc_env alpha
         in
           (new_alpha::alpha_l,new_env)
      )
      ([],scope_env)
      alpha_list
  in
    (List.rev new_alpha_list, scope_env_new)

(* \subsection{Scoping des identificateurs} *)

let rec scope_id scope_function scope_env = function
  | Id(path) -> 
      let result=(
	Id(scope_function path scope_env),
	scope_env
      )
      in
	result	
  | IdTy(id,type_B) -> 
      (
       IdTy(fst(scope_id scope_function scope_env id),type_B),
       scope_env
      )

let scope_id_value = 
  scope_id Scope.value_path

let scope_id_type = 
  scope_id Scope.type_path

let scope_id_module = 
  scope_id Scope.module_path

let scope_ids_value = 
  scope_alpha_list scope_id_value

let scope_ids_module = 
  scope_alpha_list scope_id_module

let rec scope_dec_id scope_function scope_env id = 
  match id with
  | Id(Pident(ident)) -> 
      (id,scope_function ident scope_env)
  | IdTy(id1,type_B) -> 
      (id,snd(scope_dec_id scope_function scope_env id1))

(* 
   Cas utile pour la red�finition de module par prefixage
   Il faudrat peut etre prevoir certaines restrictions (un seul prefixe � la fois, ...)
 *)

  | Id(Pdot(id2,str)) -> 
      match id2 with
        Pident(ident) -> 
          let scope_env_new = 
	    snd (scope_dec_id scope_function scope_env (Id(id2)))
          in 
	  (id,scope_env_new)
	| Pdot(_) -> invalid_arg "bscoping:scope_dec_id:Id(Pdot(Pdot...))" 

(* This modification is a patch to treat the problem of prefixed variables of
   a machine : these must be entered in the scope environment, except if they
   have been prefixed (in which case the prefix is the same for all the
   variables) *)
let scope_dec_id_value scope_env id=
  try scope_id_value scope_env id
  with Modules.Error s -> 
    scope_dec_id Scope.enter_value scope_env id
(*
  match id with
    | Id(Pdot(_)) -> 
	begin
	  try scope_id_value scope_env id
	  with Modules.Error s -> 
	    scope_dec_id Scope.enter_value scope_env id
	end
    | _ -> scope_dec_id Scope.enter_value scope_env id
 *)


let scope_dec_id_type = 
  scope_dec_id Scope.enter_type

let scope_dec_ids_value = 
  scope_alpha_list scope_dec_id_value

let scope_dec_ids_value scope_env ids = 
  scope_alpha_list scope_dec_id_value scope_env ids
    
let scope_dec_id_module scope_env id = 
  try
    scope_id_module scope_env id
  with Modules.Error s -> 
    scope_dec_id Scope.enter_module scope_env id
    
let scope_dec_ids_module = 
  scope_alpha_list scope_dec_id_module

let scope_setName = 
  funid 

let scope_boolConstant = 
  funid

let scope_number =  
  funid
    
(* \subsection{Scoping de l'arbre abstrait} *)

let refinement = 
  ref false

let rec  scope_amn scope_env = function
  | Machine(head,clauselist) -> 
      refinement := false ;
      let (h,scope_env_head) = 
	scope_head scope_env head
      in
      let (cllist, scope_env_new) = 
	    scope_clauselist scope_env_head clauselist
      in 
(*
      let n = 
        match h with 
          | (Id(Pident(id)),_) -> 
	      id
          | _ -> 
	      raise (Invalid_argument "Nom de machine invalide")
      in
	(Machine(h,cllist),(Scope.enter_module n scope_env_new))
 *)
	(Machine(h,cllist), scope_env_new)
  | Refinement(head,id,clauselist ) -> 
      refinement := true ;
      let (h,scope_env_head) = 
	scope_head scope_env head
      in
      let (cllist, scope_env_new) = 
	scope_clauselist scope_env_head clauselist
      in
(* 
      let n = 
        match h with 
          | (Id(Pident(id)),_) -> 
	      id
          | _ -> 
	      raise (Invalid_argument "Nom de machine invalide")
      in
	(
	  Refinement
	   (h,
	    fst(scope_id_module scope_env_head id),
	    cllist),
	   (Scope.enter_module n scope_env_new)
	)
 *)
	(
	  Refinement
	   (h,
	    fst(scope_id_module scope_env_head id),
	    cllist),
	   scope_env_new
	)
  | Implementation(head,id,clauselist) -> 
      refinement := true ;
      let (h,scope_env_head) = 
	scope_head scope_env head
      in
      let (cllist, scope_env_new) = 
	scope_clauselist scope_env_head clauselist
      in 
(*
      let n = 
        match h with 
          | (Id(Pident(id)),_) -> 
	      id
          | _ -> 
	      raise (Invalid_argument "Nom de machine invalide")
      in
	(
	  Implementation
	   (h,
	    fst(scope_id_module scope_env_head id),
	    cllist),
	   (Scope.enter_module n scope_env_new)
	)
 *)
	(
	  Implementation
	   (h,
	    fst(scope_id_module scope_env_head id),
	    cllist),
	   scope_env_new
	)
  | EmptyTree -> 
      (EmptyTree, scope_env)
and
  scope_clauselist scope_env cllist = 
  let sorted_clauses=sort_clauses cllist in
    scope_alpha_list scope_clause scope_env sorted_clauses
and
  scope_head scope_env = 
  function (id,ids) ->
(* if the machine is prefixed, we add the prefix to the known modules, so that
   prefixed variables/operations are correctly scoped. When dealing with
   included prefixed machines, as the scope_dec_id_module first attempt to
   find the name in the scoping environment, scoping through a project should
   be OK.
    let rec get_newenv ident=
      match ident with
	| Id(Pdot(prefix, _)) -> snd (scope_dec_id_module scope_env (Id(prefix)) )
	| IdTy(id1, _) -> get_newenv ident
	| _ -> scope_env
    in
 *)
    let (newhead,new_env)=scope_dec_id_module scope_env id in
      ((newhead, ids), snd(scope_dec_ids_value new_env ids))
and
  scope_idexpr scope_env = 
  function (id,expr) -> 
    (
      (fst(scope_id_value scope_env id),
       fst(scope_expr scope_env expr)),
      scope_env)
and
  scope_clause scope_env = function
    | Constraints(pred) -> 
	(Constraints(fst(scope_predicate scope_env pred)),
	 scope_env)
    | Invariant(pred) -> 
	(Invariant(fst(scope_predicate scope_env pred)),
	 scope_env)
    | Initialisation(substitution) -> 
	(Initialisation(fst(scope_substitution scope_env substitution)),
	 scope_env)
    | Properties(pred) -> 
	(Properties(fst(scope_predicate scope_env pred)),
	 scope_env)
    | Values(idexprlist) -> 
	(Values(fst(scope_alpha_list scope_idexpr scope_env  idexprlist)),
	 scope_env)
    | Assertions(predlist) -> 
	(Assertions(fst(scope_alpha_list scope_predicate scope_env predlist)), 
	 scope_env)
    | Sees(ids) -> 
        let (ids_new,scope_env_new) = 
	  scope_dec_ids_module scope_env ids
        in 
	  (Sees(ids_new),scope_env_new)
    | Uses(ids) -> 
        let (ids_new,scope_env_new) = 
	  scope_dec_ids_module scope_env ids
        in 
	  (Uses(ids_new),scope_env_new)
    | Extends(instancelist) -> 
	(Extends(fst(scope_alpha_list scope_instance scope_env instancelist)),
	 scope_env)
    | Includes(instancelist) -> 
        let (instancelist_new,scope_env_new) = 
	  scope_alpha_list scope_instance scope_env instancelist
        in 
	  (Includes(instancelist_new),scope_env_new)
    | Imports(instancelist) -> 
        let (instancelist_new,scope_env_new) = 
	  scope_alpha_list scope_instance scope_env instancelist
        in 
	  (Imports(instancelist_new),scope_env_new)            
    | Promotes(ids) -> 
	(Promotes(fst(scope_ids_value scope_env ids)),scope_env)            
    | LocalOperations(operationlist) -> 
        let (oplist,scope_env_new) = 
	  scope_alpha_list scope_local_operation scope_env operationlist
        in 
	  (LocalOperations(oplist),scope_env_new)
    | Operations(operationlist) -> 
        let (oplist,scope_env_new) = 
	  scope_alpha_list scope_operation scope_env operationlist
        in 
	  (Operations(oplist),scope_env_new)
    | Sets (setlist) -> 
        let (slist,scope_env_new) = 
	  scope_alpha_list scope_set scope_env setlist
        in 
	  (Sets(slist),scope_env_new)
    | ConstantsConcrete(ids) -> 
	let (newids, newscope)=scope_dec_ids_value scope_env ids in
	  (ConstantsConcrete(newids), newscope)
    | ConstantsAbstract(ids) -> 
	let (newids, newscope)=scope_dec_ids_value scope_env ids in
	  (ConstantsAbstract(newids), newscope)
    | ConstantsHidden(ids) -> 
	let (newids, newscope)=scope_dec_ids_value scope_env ids in
	  (ConstantsHidden(newids), newscope)
    | VariablesConcrete(ids) -> 
	let (newids, newscope)=scope_dec_ids_value scope_env ids in
	  (VariablesConcrete(newids), newscope)
    | VariablesAbstract(ids) -> 
	let (newids, newscope)=scope_dec_ids_value scope_env ids in
	  (VariablesAbstract(newids), newscope)
    | VariablesHidden(ids) -> 
	let (newids, newscope)=scope_dec_ids_value scope_env ids in
	  (VariablesHidden(newids), newscope)
and
  scope_set scope_env = 
  function
    | SetAbstDec(id) -> 
	(SetAbstDec(id),snd(scope_dec_id_value scope_env id))
    | SetEnumDec(id,ids) -> 
        let scope_env_new = 
	  snd(scope_dec_ids_value scope_env ids)
        in 
	  (
	    SetEnumDec(id,ids),snd(scope_dec_id_value scope_env_new id)
	  )
and
  scope_instance scope_env = 
  function
      (id,exprlist) ->  
        let (id_new,scope_env_new) = 
	  scope_dec_id_module scope_env id
        in
          ((id_new,fst(scope_alpha_list scope_expr scope_env exprlist)),
	   scope_env_new)
and 
  scope_type_B scope_env = 
  function
    | TypeNatSetRange(number1,number2) as t ->  
        (t,
	 scope_env)
    | TypeIdentifier(id) ->  
        (TypeIdentifier(fst(scope_id_value scope_env id)),
	 scope_env)
    | TypeBaseSet(bSet) ->
        (TypeBaseSet(bSet),
         scope_env)
    | TypePFun(type_B1,type_B3) ->  
        (TypePFun(fst(scope_type_B scope_env type_B1),
		  fst(scope_type_B scope_env type_B3)),
	 scope_env)
    | TypeFun(type_B1,type_B2) -> 
        (TypeFun(fst(scope_type_B scope_env type_B1),
		 fst(scope_type_B scope_env type_B2)),
	 scope_env)
    | TypeProd(type_B1,type_B2) -> 
        (TypeProd(fst(scope_type_B scope_env type_B1),
		  fst(scope_type_B scope_env type_B2)),
	 scope_env)
    | TypeSequence(type_B) -> 
        (TypeSequence(fst(scope_type_B scope_env type_B)),
	 scope_env)
    | TypePowerset(type_B) -> 
        (TypePowerset(fst(scope_type_B scope_env type_B)),
	 scope_env)
    | TypeRecords(fields) ->
        (TypeRecords(fst (List.split (List.map (scope_field scope_env) fields))),
	 scope_env)
    | Untyped -> 
	(Untyped,
	 scope_env)
and
  scope_field scope_env = 
  function
      (id,type_B) -> 
	((fst(scope_id_value scope_env id),fst(scope_type_B scope_env type_B)),
	 scope_env)
and
  scope_predicate scope_env = 
  function
    | PredParen(pred) -> 
        (PredParen(fst(scope_predicate scope_env pred)),
	 scope_env)
    | PredNegation(pred) -> 
        (PredNegation(fst(scope_predicate scope_env pred)),
	 scope_env)
    | PredExists(ids,pred) -> 
        (PredExists(ids,fst(scope_predicate 
			      (snd(scope_dec_ids_value scope_env ids)) pred)),
	 scope_env)
    | PredForAll(ids,pred) -> 
        (PredForAll(ids,fst(scope_predicate
			      (snd(scope_dec_ids_value scope_env ids)) pred)),
	 scope_env)
    | PredBin(bop2,pred1,pred2) -> 
        (PredBin(bop2,
		 fst(scope_predicate scope_env pred1),
		 fst(scope_predicate scope_env pred2)),
	 scope_env)
    | PredAtom(bop2,expr1,expr2) ->
	(PredAtom(bop2,
		  fst(scope_expr scope_env expr1),
		  fst(scope_expr scope_env expr2)),
	 scope_env)
    | SubstApply(_) -> invalid_arg "bscoping:scope_predicate:SubstApply"
and
  scope_local_operation scope_env =
  function
      (id,ids1,ids2,substitution) ->
        let scope_env_new = snd(scope_dec_id_value scope_env id) in
	let (newids1, scopeids1)=scope_dec_ids_value scope_env_new ids1 in
	let (newids2, scopeids2)=scope_dec_ids_value scopeids1 ids2 in
	  ((fst(scope_id_value scope_env_new id),
	    newids1,
	    newids2,
	    fst(scope_substitution scopeids2 substitution)),
           scopeids2)
and
  scope_operation scope_env = 
  function
      (id,ids1,ids2,substitution) -> 
        let scope_env_new = 
          if !refinement 
          then scope_env 
          else snd(scope_dec_id_value scope_env id)
        in 
	let (newids1, scopeids1)=scope_dec_ids_value scope_env_new ids1 in
	let (newids2, scopeids2)=scope_dec_ids_value scopeids1 ids2 in
	  ((fst(scope_id_value scope_env_new id),
	    newids1,
	    newids2,
	    fst(scope_substitution scopeids2 substitution)),
           scopeids2)
(*
 Changed due to the new behaviour of the scope_dec_id function
	  if !refinement 
          then 
	    ((fst(scope_id_value scope_env id),
	      ids1,
	      ids2,
	      fst(scope_substitution scope_env_operation substitution)),
             scope_env_new)
	  else
	    ((id,
	      ids1,
	      ids2,
	      fst(scope_substitution scope_env_operation substitution)),
	     scope_env_new)
 *)
and
  scope_expr_sets scope_env = 
  function
    | ExprId(id) ->   
        let id_new =  
          try 
	    fst(scope_id_value scope_env id)
          with Modules.Error(_) -> 
	    fst(scope_id_type scope_env id)
        in
          (ExprId(id_new),scope_env)
    | expr -> 
	scope_expr scope_env expr
and
  scope_expr scope_env = 
  function
    | ExprParen(expr) ->            
        (ExprParen(fst(scope_expr scope_env expr)),
	 scope_env)
    | ExprId(id) ->                 
      (ExprId(fst(scope_id_value scope_env id)),
	 scope_env)
    | ExprBefore(id) ->                 
      (ExprBefore(fst(scope_id_value scope_env id)),
	 scope_env)
    | ExprFunCall(expr1,expr2) -> 
        (ExprFunCall(fst(scope_expr scope_env expr1),
		     fst(scope_expr scope_env expr2)),
	 scope_env)
    | ExprTrin(op3,expr1,expr2,expr3) ->
        (ExprTrin(op3,fst(scope_expr scope_env expr1),
		 fst(scope_expr scope_env expr2),
		 fst(scope_expr scope_env expr3)),
	 scope_env)
    | ExprBin(op2,expr1,expr2) ->
        (ExprBin(op2,fst(scope_expr scope_env expr1),
		 fst(scope_expr scope_env expr2)),
	 scope_env)
    | ExprUn (op1,expr) ->     
        (ExprUn(op1,fst(scope_expr scope_env expr)),
	 scope_env)
    | ExprSeq(exprSeq) ->           
        (ExprSeq(fst(scope_exprSeq scope_env exprSeq)),
	 scope_env)
    | ExprSet(exprSet) ->           
        (ExprSet(fst(scope_exprSet scope_env exprSet)),
	 scope_env)
    | ExprNumber(number) as e ->    
	(e,
	 scope_env)
    | ExprBoolConstant(bcst) as e ->    
	(e,
	 scope_env)
    | ExprBool(pred) -> 
	let (newpred, newscope)=scope_predicate scope_env pred in
	(ExprBool(newpred), newscope)
    | ExprString(string) as e  ->   
	(e,
	 scope_env)
    | ExprRecords(expr) ->
        (ExprRecords(fst(scope_exprRecords scope_env expr)),
         scope_env)
    | ExprNuplet(exprlist) ->       
        (ExprNuplet(fst(scope_alpha_list scope_expr scope_env exprlist)),
	 scope_env)	  
    | ExprSIGMA(ids,pred1,expr2) -> 
        let scope_env_sigma = 
	  snd(scope_dec_ids_value scope_env ids)
        in 
	  (ExprSIGMA(ids,
		     fst(scope_predicate scope_env_sigma pred1),
		     fst(scope_expr scope_env_sigma expr2)),
	   scope_env)
    | ExprPI(ids,pred1,expr2) ->    
        let scope_env_pi = 
	  snd(scope_dec_ids_value scope_env ids)
        in 
	  (ExprPI(ids,
		  fst(scope_predicate scope_env_pi pred1),
		  fst(scope_expr scope_env_pi expr2)),
	   scope_env)
    | ExprLambda(ids,pred1,expr2) ->    
        let scope_env_pi = 
	  snd(scope_dec_ids_value scope_env ids)
        in 
	  (ExprLambda(ids,
		      fst(scope_predicate scope_env_pi pred1),
		      fst(scope_expr scope_env_pi expr2)),
	   scope_env)
and
  scope_exprRecords scope_env =
  function
    | RecordsFields(fields) ->
      let scope_id_option =
	function
	  | None -> None
	  | Some(id) -> Some(fst(scope_id_value scope_env id))
      in
      let scope_field (i,e) = (scope_id_option i, fst(scope_expr scope_env e)) in
        (RecordsFields(List.map scope_field fields),
	 scope_env)
    | RecordsAccess(expr, id) ->
        (RecordsAccess(fst(scope_expr scope_env expr),
                      fst(scope_id_value scope_env id)),
         scope_env)
and
  scope_exprSeq scope_env = 
  function
    | SeqEnum(exprlist) -> 
	(SeqEnum(funmap scope_expr scope_env exprlist),scope_env)
    | SeqEmpty -> 
	(SeqEmpty,scope_env)
and
  scope_exprSet scope_env = 
  function
    | SetPredefined(setName) -> 
        (SetPredefined(fst(scope_setName scope_env setName)),
	 scope_env)
    | SetEnum(exprlist) -> 
        (SetEnum(funmap scope_expr scope_env exprlist),
	 scope_env)
    | SetComprPred(ids,pred) -> 
        (SetComprPred(ids,
		      fst(scope_predicate
			    (snd(scope_dec_ids_value scope_env ids)) 
			    pred)),
	 scope_env)
    | SetEmpty -> 
	(SetEmpty,scope_env)
    | SetUnionQ(ids,pred1,expr2) -> 
        let scope_env_temp = 
	  snd(scope_dec_ids_value scope_env ids)
        in 
	  (SetUnionQ(ids,
		     fst(scope_predicate scope_env_temp pred1),
		     fst(scope_expr scope_env_temp expr2)),
	   scope_env)
    | SetInterQ(ids,pred1,expr2) -> 
        let scope_env_temp = 
	  snd(scope_dec_ids_value scope_env ids)
        in 
	  (SetInterQ(ids,
		     fst(scope_predicate scope_env_temp pred1),
		     fst(scope_expr scope_env_temp expr2)),
	   scope_env)
    | SetRecords(fields) ->
(* TODO: the new_scope is probably not taken into account, because records declarations do not happen in a declaring clause (such as SETS or VARIABLES) but in PROPERTIES, for instance *)
        let (ids, exprs) = List.split fields in
        let (new_ids, new_scope) = scope_dec_ids_value scope_env ids in
        let new_exprs = fst (List.split (List.map (scope_expr scope_env) exprs)) in
	  (SetRecords(List.combine new_ids new_exprs),
	   new_scope)
	  
and
  scope_exprsubst scope_env = 
  function 
      (expr,substitution) -> 
	((fst(scope_expr scope_env expr),
	  fst(scope_substitution scope_env substitution)),
	 scope_env)   
and    
  scope_predsubst scope_env = 
  function 
      (pred,substitution) -> 
	((fst(scope_predicate scope_env pred),
	  fst(scope_substitution scope_env substitution)),
	 scope_env)   
and
  scope_substoption scope_env = 
  function
    | None -> 
	(None,scope_env)
    | Some(subst) -> 
        let (s,scope_env_new) = 
	  scope_substitution scope_env subst 
        in 
	  (Some(s),
	   scope_env_new)
and
  scope_exprlistsubst scope_env = 
  function
      (exprlist,substitution) -> 
	((fst(scope_alpha_list scope_expr scope_env exprlist),
          fst(scope_substitution scope_env substitution)),
	 scope_env)

and
  scope_substitution scope_env = 
  function
    | SubstOperCall(ids,id,exprlist) -> 
        (SubstOperCall(fst(scope_ids_value scope_env ids),
		       fst(scope_id_value scope_env id),
		       funmap scope_expr scope_env exprlist),
	 scope_env)
    | SubstBlock(substitution) -> 
        (SubstBlock(fst(scope_substitution scope_env substitution)),
	 scope_env)
    | SubstPrecondition(pred,substitution) -> 
        (SubstPrecondition(fst(scope_predicate scope_env pred),
			   fst(scope_substitution scope_env substitution)),
	 scope_env)
    | SubstAssertion(pred,substitution) -> 
        (SubstAssertion(fst(scope_predicate scope_env pred),
			fst(scope_substitution scope_env substitution)),
	 scope_env)
    | SubstChoice(substitutionlist) -> 
        (SubstChoice(funmap scope_substitution scope_env substitutionlist),
	 scope_env)
    | SubstIf(predsubstlist,substoption) -> 
        (SubstIf(funmap scope_predsubst scope_env predsubstlist,
		 fst(scope_substoption scope_env substoption)),
	 scope_env)
    | SubstSelect(predsubstlist,substoption) -> 
        (SubstSelect(funmap scope_predsubst scope_env predsubstlist,
		     fst(scope_substoption scope_env substoption)),
	 scope_env)
    | SubstCase(expr,exprlistsubstlist,substoption) -> 
        (SubstCase(fst(scope_expr scope_env expr),
                   (funmap scope_exprlistsubst scope_env exprlistsubstlist),
                   (fst(scope_substoption scope_env substoption))),
	 scope_env)
    | SubstAny(ids,pred,substitution) -> 
        let scope_env_temp = 
	  snd(scope_dec_ids_value scope_env ids)
        in 
	  (SubstAny(ids,fst(scope_predicate scope_env_temp pred),
		    fst(scope_substitution scope_env_temp substitution)),
	   scope_env)
    | SubstLet(ids,pred,substitution) -> 
        let scope_env_temp = 
	  snd(scope_dec_ids_value scope_env ids)
        in 
	  (SubstLet(ids,fst(scope_predicate scope_env_temp pred),
		    fst(scope_substitution scope_env_temp substitution)),
	   scope_env)
    | SubstVar(ids,substitution) -> 
        let scope_env_temp = 
	  snd(scope_dec_ids_value scope_env ids)
        in 
	  (SubstVar(ids,fst(scope_substitution scope_env_temp substitution)),
	   scope_env)
    | SubstWhile(pred1,substitution,expr2,pred3) -> 
        (SubstWhile(fst(scope_predicate scope_env pred1),
                    fst(scope_substitution scope_env substitution),
                    fst(scope_expr scope_env expr2),
                    fst(scope_predicate scope_env pred3)),
	 scope_env)
    | SubstSkip -> 
	(SubstSkip,
	 scope_env)
    | SubstSequence(substitution1,substitution2) -> 
        (SubstSequence(fst(scope_substitution scope_env substitution1),
		       fst(scope_substitution scope_env substitution2)),
	 scope_env)
    | SubstParallel(substitution1,substitution2) -> 
        (SubstParallel(fst(scope_substitution scope_env substitution1),
		       fst(scope_substitution scope_env substitution2)),
	 scope_env)
    | SubstSetEqualIds(ids,exprlist) -> 
        (SubstSetEqualIds(fst(scope_ids_value scope_env ids),
			  funmap scope_expr scope_env exprlist),
	 scope_env)
    | SubstSetEqualFun(funchain, expr) -> 
        (SubstSetEqualFun(fst(scope_funchain scope_env funchain),
			  fst(scope_expr scope_env expr)),
	 scope_env)
    | SubstSetEqualRecords(fieldchain,expr) -> 
        (SubstSetEqualRecords(fst(scope_fieldchain scope_env fieldchain),
			      fst(scope_expr scope_env expr)),
	 scope_env)
    | SubstBecomeSuch(ids,pred) -> 
        (SubstBecomeSuch(fst(scope_ids_value scope_env ids),
			 fst(scope_predicate scope_env pred)),
	 scope_env)
    | SubstSetIn(ids,expr) -> 
        (SubstSetIn(fst(scope_ids_value scope_env ids),
		    fst(scope_expr scope_env expr)),
	 scope_env)
    | SubstInstanciation(_) -> invalid_arg "bscoping:scope_substitution:SubstInstanciation"
and scope_fieldchain scope_env =
  function
    | SetRecVar(id) ->
        (SetRecVar(fst(scope_id_value scope_env id)),
	 scope_env)
    | SetRecField(fc, id) ->
        (SetRecField(fst(scope_fieldchain scope_env fc),
		     fst(scope_id_value scope_env id)),
	 scope_env)
and scope_funchain scope_env =
  function
    | SetFunVar(id) ->
        (SetFunVar(fst(scope_id_value scope_env id)),
	 scope_env)
    | SetFunExpr(fc, expr) ->
        (SetFunExpr(fst(scope_funchain scope_env fc),
		    fst(scope_expr scope_env expr)),
	 scope_env)
