open Blast_mk
open Blast

exception No_such_clause

let get_Constraints cl =
  try
    let l =
      List.fold_left 
      	(fun predlist clause -> 
      	  match clause with
	    Constraints(c) -> c::predlist
      	  |	_ -> predlist)
      	[]
      	cl
    in
    mk_Conjunction (List.rev l)
  with Blast_mk.Empty_list ->
    raise No_such_clause


let get_Invariant cl =
  try
    let l =
      List.fold_left 
      	(fun predlist clause -> 
      	  match clause with
	    Invariant(p) -> p::predlist
      	  |	_ -> predlist)
      	[]
      	cl
    in
    mk_Conjunction (List.rev l)
  with Blast_mk.Empty_list ->
    raise No_such_clause



let get_Sets cl =
  let l =
    List.fold_left 
      (fun setslist clause -> 
      	match clause with
	  Sets(setlist) -> setlist::setslist
      	|	_ -> setslist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_Initialisation cl =
  try 
    let l =
      List.fold_left 
      	(fun initlist clause -> 
      	  match clause with
	    Initialisation(subst) -> subst::initlist
      	  |	_ -> initlist)
      	[]
      	cl
    in
    mk_SubstSequence_from_list (List.rev l)
  with 
      Blast_mk.Empty_list -> raise No_such_clause

let get_ConstantsConcrete cl =
  let l =
    List.fold_left 
      (fun conslist clause -> 
      	match clause with
	  ConstantsConcrete(ids) -> ids::conslist
      	|	_ -> conslist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_ConstantsAbstract cl =
  let l =
    List.fold_left 
      (fun conslist clause -> 
      	match clause with
	  ConstantsAbstract(ids) -> ids::conslist
      	|	_ -> conslist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_ConstantsHidden cl =
  let l =
    List.fold_left 
      (fun conslist clause -> 
      	match clause with
	  ConstantsHidden(ids) -> ids::conslist
      	|	_ -> conslist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_Properties cl =
  try
    let l =
      List.fold_left 
      	(fun predlist clause -> 
      	  match clause with
	    Properties(pred) -> pred::predlist
      	  |	_ -> predlist)
      	[]
      	cl
    in
    mk_Conjunction (List.rev l)
  with Blast_mk.Empty_list ->
    raise No_such_clause


let get_Values cl =
  let l =
    List.fold_left 
      (fun valueslist clause -> 
      	match clause with
	  Values(values) -> values::valueslist
      	|	_ -> valueslist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_VariablesConcrete cl =
  let l =
    List.fold_left 
      (fun varlist clause -> 
      	match clause with
	  VariablesConcrete(var) -> var::varlist
      	|	_ -> varlist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_VariablesAbstract cl =
  let l =
    List.fold_left 
      (fun varlist clause -> 
      	match clause with
	  VariablesAbstract(var) -> var::varlist
      	|	_ -> varlist)
      []
      cl
  in
  List.flatten (List.rev l)

let get_VariablesHidden cl =
  let l =
    List.fold_left 
      (fun varlist clause -> 
      	match clause with
	  VariablesHidden(var) -> var::varlist
      	|	_ -> varlist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_Promotes cl =
  let l =
    List.fold_left 
      (fun promoteslist clause -> 
      	match clause with
	  Promotes(ids) -> ids::promoteslist
      	|	_ -> promoteslist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_Assertions cllist =
  let l =
    List.fold_left 
      (fun predlistlist clause -> 
      	match clause with
	  Assertions(predlist) -> predlist::predlistlist
      	|	_ -> predlistlist)
      []
      cllist
  in
  let l2 = List.flatten (List.rev l)
  in
  match l2 with
    [] -> raise (Invalid_argument "get_Assertions")
  | _ -> l2


let get_LocalOperations cl =
  let l =
    List.fold_left 
      (fun oplistlist clause -> 
      	match clause with
	  LocalOperations(oplist) -> oplist::oplistlist
      	|	_ -> oplistlist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_Operations cl =
  let l =
    List.fold_left 
      (fun oplistlist clause -> 
      	match clause with
	  Operations(oplist) -> oplist::oplistlist
      	|	_ -> oplistlist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_Sees cl =
  let l =
    List.fold_left 
      (fun idlist clause -> 
      	match clause with
	  Sees(ids) -> ids::idlist
      	|	_ -> idlist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_Uses cl =
  let l =
    List.fold_left 
      (fun idlist clause -> 
      	match clause with
	  Uses(ids) -> ids::idlist
      	|	_ -> idlist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_Extends cl =
  let l =
    List.fold_left 
      (fun instlistlist clause -> 
      	match clause with
	  Extends(instlist) -> instlist::instlistlist
      	|	_ -> instlistlist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_Imports cl =
  let l =
    List.fold_left 
      (fun instlistlist clause -> 
      	match clause with
	  Imports(instlist) -> instlist::instlistlist
      	|	_ -> instlistlist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_Includes cl =
  let l =
    List.fold_left 
      (fun instlistlist clause -> 
      	match clause with
	  Includes(instlist) -> instlist::instlistlist
      	|	_ -> instlistlist)
      []
      cl
  in
  List.flatten (List.rev l)


let get_operation_id (opid,_,_,_)=
  opid
    
let get_operation_outparams (_,outparams,_,_)=
  outparams
    
let get_operation_inparams (_,_,inparams,_)=
  inparams
 
let get_operation_body (_,_,_,body)=
  body
   

(* This function returns the \emph{first} operation corresponding to the give
  operation id \emph{opid} in the given \emph{operations} list *)
let rec retrieve_operation opid operations=
  match operations with
    | oper::others -> 
	let currentopid=get_operation_id oper in
	  Error.debug ("Comparing " ^ (Id_acc.debug_id opid) ^ " with " ^ (Id_acc.debug_id currentopid) );
	  if opid = get_operation_id oper
	  then 
	    oper 
	  else retrieve_operation opid others
    | [] -> 
	Error.debug ((Id_acc.debug_id opid) ^ " not found in the given list of operations");
	raise Not_found



(* SC: blast_acc doit d�pendre de clause_acc, pas le contraire. Effacez si �a
   compile *)
(*
let collect_Initialisation ast=
  try
    let mchclauses=get_clause_list ast in
      get_Initialisation mchclauses
  with
      Blast_acc.Empty_tree  
    | Invalid_argument "get_Initialisation" -> raise Not_found


let collect_Operations ast=
  try 
    get_Operations (get_clause_list ast) 
  with
      Blast_acc.Empty_tree -> []
 *)
