(* $Id$ *)

open Blast

(* SC: enlever les fonctions commentées *)
(*
let mkSetEnum explist=
  ExprSet(SetEnum(explist))

let mkExprId i=
  ExprId(i)
*)

let is_set paramid=
  let idname=Id_acc.name_without_prefix paramid in 
    (idname=String.uppercase_ascii idname)

