
open Blast
open Xmlm

type tree = E of Xmlm.tag * tree list | D of string

(* We do not put namespace information in tags *)
let na_leaf_tag name =
  E((("", name), []), [])

let leaf_tag name attributes =
  let no_ns_attributes = List.map (fun (att,value) -> (("", att), value)) attributes in
  E((("", name), no_ns_attributes), [])

let data s = D(s)

(* We do not put namespace information in attributes at the moment *)
let tag name attributes children_and_data =
  let no_ns_attributes = List.map (fun (att,value) -> (("", att), value)) attributes in
  E((("", name), no_ns_attributes),
    children_and_data)

(* Like tag, but with no attributes (saves adding an often empty argument) *)
let na_tag name children_and_data =
  E((("",name), []),
    children_and_data)

let rec id ident =
  match ident with
    | Id(path) ->
      leaf_tag
	"BoundIdent"
	[("name", Id_acc.name_of ident);
	 ("idref", string_of_int (Id_acc.number_of_path path));
	 ("prefix", Id_acc.prefix_of ident)]
    | IdTy(id2, _) -> id id2

let ids idents = List.map id idents

let rec amn ast =
  match ast with
    | Machine (entete,clauses1) ->
      na_tag "Machine" ((head entete) :: (List.map clause clauses1))
    | Refinement(entete,refinesName,clauses1) ->
      na_tag "Refinement" (
	(head entete)
	:: (refines refinesName)
	:: (List.map clause clauses1))
    | Implementation(entete,refinesName,clauses1) ->
      na_tag "Implementation" (
	(head entete)
	:: (refines refinesName)
	:: (List.map clause clauses1))
    | EmptyTree -> invalid_arg "amn: empty tree"
and head (ident, params) =
  na_tag "MachineHeader" ((id ident) :: (ids params))
and refines ident =
  na_tag "Refines" [ id ident ]
and clause clauseAMN =
  match clauseAMN with
    | Constraints(pred) ->
      na_tag "Constraints" [top_predicate pred]
    | Invariant(pred) ->
      na_tag "Invariant" [top_predicate pred]
    | Sets (setlist) ->
      na_tag "Sets" (List.map set setlist)
    | Initialisation(subst) ->
      na_tag "Initialisation" [top_substitution subst]
    | ConstantsConcrete(idlist) ->
      na_tag "ConcreteConstants" (ids idlist)
    | ConstantsAbstract(idlist) ->
      na_tag "AbstractConstants" (ids idlist)
    | ConstantsHidden(idlist) ->
      na_tag "HiddenConstants" (ids idlist)
    | VariablesConcrete(idlist) ->
      na_tag "ConcreteVariables" (ids idlist)
    | VariablesAbstract(idlist) ->
      na_tag "AbstractVariables" (ids idlist)
    | VariablesHidden(idlist) ->
      na_tag "HiddenVariables" (ids idlist)
    | Properties(pred) ->
      na_tag "Properties" [top_predicate pred]
    | Values(valuationlist) ->
      na_tag "Values" (List.fold_right (fun v l -> (valuation v) @ l) valuationlist [])
    | Promotes(idlist) ->
      na_tag "Promotes" (ids idlist)
    | Extends(instancelist) ->
      na_tag "Extends" (List.fold_right (fun i l -> (instance i) @ l) instancelist [])
    | Assertions(predlist) ->
      na_tag "Assertions" (List.map top_predicate predlist)
    | LocalOperations(operationlist) ->
      na_tag "LocalOperations" (List.map operation operationlist)
    | Operations(operationlist) ->
      na_tag "Operations" (List.map operation operationlist)
    | Sees(idlist) ->
      na_tag "Sees" (ids idlist)
    | Uses(idlist) ->
      na_tag "Uses" (ids idlist)
    | Includes(instancelist) ->
      na_tag "Includes" (List.fold_right (fun i l -> (instance i) @ l) instancelist [])
    | Imports(instancelist) ->
      na_tag "Imports" (List.fold_right (fun i l -> (instance i) @ l) instancelist [])
and set s =
  match s with
    | SetAbstDec(ident) -> na_tag "AbstractSet" [id ident]
    | SetEnumDec(ident, elts) -> na_tag "EnumeratedSet" ((id ident) :: (ids elts))
and valuation (ident,expr) = [(id ident); (top_expression expr)]
and instance (ident,exprlist) = (id ident) :: (List.map top_expression exprlist)
and operation (name,ids_out,ids_in,body) =
  na_tag "Operation"
    [ na_tag "OutParameters" (ids ids_out);
      na_tag "OperationIdent" [id name];
      na_tag "Inparameters" (ids ids_in);
      na_tag "OperationBody" [top_substitution body] ]
and top_expression expr =
  na_tag "Expression" [expression expr]
and expression expr =
  match expr with
    | ExprUn(op,expr1) ->
      na_tag (Bbop.op1_name op) [top_expression expr1]
    | ExprBin(op,expr1,expr2) ->
      na_tag (Bbop.op2_name op) [top_expression expr1; top_expression expr2]
    | ExprTrin(op,expr1,expr2,expr3) ->
      na_tag (Bbop.op3_name op) [top_expression expr1; top_expression expr2; top_expression expr3]
    | ExprParen(expr1) -> (* TODO: make ExprParen an attribute *)
      na_tag "Parentheses" [top_expression expr1]
    | ExprId(id1) ->
      id id1
    | ExprBefore(id1) ->
      na_tag "Before" [(id id1)]
    | ExprFunCall(expr1,expr2) ->
      na_tag "FunctionCall" [top_expression expr1; top_expression expr2]
    | ExprSeq(seq) ->
      exprSeq seq
    | ExprSet(set) ->
      exprSet set
    | ExprNumber(num) ->
      na_tag "Number" [number num]
    | ExprBoolConstant(bcst) ->
      na_tag "Boolean" [boolean bcst]
    | ExprBool(pred1) ->
      na_tag "Bool" [top_predicate pred1]
    | ExprString(str) ->
      na_tag "String" [string str]
    | ExprRecords(record) ->
      exprRecords(record)
    | ExprNuplet(exprlist) ->
      na_tag "Tuple" (List.map top_expression exprlist)
    | ExprSIGMA(ids1,pred,expr) ->
      na_tag "Sigma" ((ids ids1) @ [top_predicate pred; top_expression expr])
    | ExprPI(ids1,pred,expr) ->
      na_tag "Pi" ((ids ids1) @ [top_predicate pred; top_expression expr])
    | ExprLambda(ids1,pred,expr) ->
      na_tag "Lambda" ((ids ids1) @ [top_predicate pred; top_expression expr])
and exprSeq seq =
  match seq with
    | SeqEnum(exprlist) -> na_tag "Sequence" (List.map top_expression exprlist)
    | SeqEmpty -> na_leaf_tag "Sequence"
and exprSet set =
  match set with
    | SetPredefined(setName1) ->
      setName setName1
    | SetEnum(exprlist) ->
      na_tag "SetExtension" (List.map top_expression exprlist)
    | SetEmpty ->
      na_tag "SetExtension" []
    | SetComprPred(ids1,pred) ->
      na_tag "SetComprehension" ((ids ids1) @ [top_predicate pred])
    | SetUnionQ(ids1,pred,expr) ->
      na_tag "QuantifiedUnion" ((ids ids1) @ [top_predicate pred; top_expression expr])
    | SetInterQ(ids1,pred,expr) ->
      na_tag "QuantifiedIntersection" ((ids ids1) @ [top_predicate pred; top_expression expr])
    | SetRecords(recordsItemlist) ->
      na_tag "Struct"
	(List.fold_right (fun (i,e) l -> (id i)::(top_expression e)::l) recordsItemlist [])
and exprRecords record =
  match record with
    | RecordsFields(fields) ->
      na_tag "Rec" (List.fold_right (fun f l -> (recordsItem_opt f) @ l) fields [])
    | RecordsAccess(expr,id1) ->
      tag "Field" [("name", (Id_acc.name_of id1))] [top_expression expr]
and recordsItem_opt (id_opt,expr) =
  match id_opt with
    None -> [(top_expression expr)]
  | Some(id1) -> [(id id1);(top_expression expr)]
and setName n =
  match n with
    | NAT -> leaf_tag "PredefinedSet" [("name", "NAT")]
    | NATURAL -> leaf_tag "PredefinedSet" [("name", "NATURAL")]
    | NAT1 -> leaf_tag "PredefinedSet" [("name", "NAT1")]
    | NATURAL1 -> leaf_tag "PredefinedSet" [("name", "NATURAL1")]
    | INTEGER -> leaf_tag "PredefinedSet" [("name", "INTEGER")]
    | INT -> leaf_tag "PredefinedSet" [("name", "INT")]
    | STRING -> leaf_tag "PredefinedSet" [("name", "STRING")]
    | BOOL -> leaf_tag "PredefinedSet" [("name", "BOOL")]
and boolean b =
  match b with
    | TrueConstant -> data "TRUE"
    | FalseConstant -> data "FALSE"
and number num =
  match num with
    | MinNumber -> data "MININT"
    | MaxNumber -> data "MAXINT"
    | Number(n) -> data (Int32.to_string n)
and string s = data s
and top_predicate p =
  na_tag "Predicate" [predicate p]
and predicate p =
  match p with
    | PredParen(p) -> (* TODO: make it an attribute *)
      na_tag "Parentheses" [top_predicate p]
    | PredBin(op,pred1,pred2) ->
      na_tag (Bbop.bop2_name op) [top_predicate pred1; top_predicate pred2]
    | PredAtom(op,expr1,expr2) ->
      na_tag (Bbop.bop2_name op) [top_expression expr1; top_expression expr2]
    | PredNegation(pred) ->
      na_tag "Not" [top_predicate pred]
    | PredExists(ids1,pred) ->
      na_tag "Exists" ((ids ids1) @ [top_predicate pred])
    | PredForAll(ids1,pred) ->
      na_tag "Forall" ((ids ids1) @ [top_predicate pred])
    | SubstApply(subst, pred) ->
      na_tag "SubstApply" [top_substitution subst; top_predicate pred]
and top_substitution subs =
  na_tag "Substitution" [substitution subs]
and substitution subs =
  match subs with
    | SubstBlock(subst) ->
      na_tag "Block" [top_substitution subst]
    | SubstSetEqualIds(idlist,exprlist1) ->
      na_tag "BecomesEqual" ((ids idlist) @ (List.map top_expression exprlist1))
    | SubstSetEqualFun(fc,expr) ->
      na_tag "ImageBecomesEqual" [funchain fc; top_expression expr]
    | SubstSetEqualRecords(fc,expr) ->
      na_tag "FieldBecomesEqual" [fieldchain fc; top_expression expr]
    | SubstBecomeSuch(idlist,pred1) ->
      na_tag "BecomesSuchThat" ((ids idlist) @ [top_predicate pred1])
    | SubstSetIn(ids1,expr) ->
      na_tag "BecomesElement" ((ids ids1) @ [top_expression expr])
    | SubstPrecondition(pred,subst) ->
      na_tag "Precondition" [top_predicate pred; top_substitution subst]
    | SubstAssertion(pred,subst) ->
      na_tag "Assertion" [top_predicate pred; top_substitution subst]
    | SubstChoice(substlist) ->
      na_tag "Choice" (List.map top_substitution substlist)
    | SubstIf(predsubstList,substOption) ->
      let ifthens = List.fold_right
	(fun (p,s) l -> (top_predicate p) :: (top_substitution s) :: l) predsubstList []
      in
      na_tag "If" (ifthens @ (substitution_option substOption))
    | SubstSelect(whenpartlist,substOption) ->
      let whenthens = List.fold_right
	(fun (p,s) l -> (top_predicate p) :: (top_substitution s) :: l) whenpartlist []
      in
      na_tag "Select" (whenthens @ (substitution_option substOption))
    | SubstCase(expr,orpartlist,substOption) ->
      na_tag "Case"
	( [top_expression expr]
	  @ (List.fold_right
	       (fun (el,s) l -> (List.map top_expression el) @ [top_substitution s] @ l)
	       orpartlist
	       [])
	  @ (substitution_option substOption))
    | SubstAny(idlist,pred,subst) ->
      na_tag "Any" ( (ids idlist) @ [top_predicate pred; top_substitution subst] )
    | SubstLet(idlist,pred,subst) ->
      na_tag "Let" ( (ids idlist) @ [top_predicate pred; top_substitution subst] )
    | SubstVar(idlist,subst) ->
      na_tag "Var" ( (ids idlist) @ [top_substitution subst] )
    | SubstSequence(subst1,subst2) ->
      na_tag "Sequence" [top_substitution subst1; top_substitution subst2]
    | SubstWhile(pred1,subst,expr,pred2) ->
      na_tag "While" [
	na_tag "Condition" [top_predicate pred1];
	na_tag "Do" [top_substitution subst];
	na_tag "Variant" [top_expression expr];
	na_tag "Invariant" [top_predicate pred2]]
    | SubstOperCall(ids1,opid,exprlist1) ->
      na_tag "OperationCall" [
	na_tag "Out" (ids ids1);
	na_tag "Operation" [id opid];
	na_tag "In" (List.map top_expression exprlist1)]
    | SubstParallel(subst1,subst2) ->
      na_tag "Parallel" [top_substitution subst1; top_substitution subst2]
    | SubstSkip ->
      na_leaf_tag "Skip"
    | SubstInstanciation(idslist, exprslist, subst) ->
      na_tag "Instanciation"
	((ids idslist)
	 @ (List.map top_expression exprslist)
	 @ [top_substitution subst])
and fieldchain fc =
  match fc with
    | SetRecVar(i) -> id i
    | SetRecField(f,i) -> tag "Field" [("name", (Id_acc.name_of i))] [fieldchain f]
and funchain fc =
  match fc with
    | SetFunVar(i) -> id i
    | SetFunExpr(f,e) -> na_tag "Image" [funchain f; top_expression e]
and substitution_option substOption =
  match substOption with
    | Some(s) -> [top_substitution s]
    | None -> []


let frag_of_tree = function
  | E(tag, children) -> `El(tag, children)
  | D(d) -> `Data(d)


let output out_chan ast =
  let out_options = make_output ~indent:(Some(2)) (`Channel(out_chan)) in
  let tree_ast = amn ast in
  output_doc_tree frag_of_tree out_options (None, tree_ast)

