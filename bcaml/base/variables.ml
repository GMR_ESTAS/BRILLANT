(* $Id$ *)

open Blast
open Bbop
open Blast_mk
open Subst_acc
open Id_handling


type variable_property=
  | Free
  | Bound
  | Modified
  | All


(*
\section{Expressions : Manipulations des variables}
*)


let rec vars_expr vartype expr=
  match expr with
  | ExprUn(op, e1) ->
      begin
	match vartype with
	| Free | Bound | All -> (vars_expr vartype e1) 
	| Modified -> Idset.empty
      end
  | ExprBin(_,e1, e2) 
    ->
      begin
	match vartype with
	| Free | Bound | All -> 
	    Idset.union (vars_expr vartype e1) (vars_expr vartype e2) 
	| Modified -> Idset.empty
      end
  | ExprTrin(_,e1, e2, e3) ->
      begin
	match vartype with
	| Free | Bound | All -> 
	    Idset.union
	      (Idset.union (vars_expr vartype e1) (vars_expr vartype e2))
	      (vars_expr vartype e3)
	| Modified -> Idset.empty
      end
  | ExprParen(e) ->
      begin
	match vartype with
	| Free | Bound | All -> vars_expr vartype e
	| Modified -> Idset.empty 
      end
  | ExprId(i) ->
      begin
	match vartype with
	| Free | All -> Idset.singleton i
	| Bound | Modified -> Idset.empty
      end
  | ExprBefore(i) ->
      begin
	match vartype with
	| Free | All -> Idset.singleton i
	| Bound | Modified -> Idset.empty
      end
  | ExprFunCall(expr1,expr2) ->
      begin
	match vartype with
	| Free 
        | Bound 
        | All -> vars_funCall vartype (expr1,expr2)
	| Modified -> Idset.empty
      end
  | ExprSeq(aseq) ->
      begin
	match vartype with
	| Free | Bound | All -> vars_exprSeq vartype aseq
	| Modified -> Idset.empty
      end
  | ExprSet(expset) ->
      begin
	match vartype with
	| Free | Bound | All -> vars_exprSet vartype expset
	| Modified -> Idset.empty
      end
  | ExprNuplet(exprs) ->
      begin
	match vartype with
	| Free | Bound | All -> 
	    union_from_setlist (List.map (vars_expr vartype) exprs)
	| Modified -> Idset.empty
      end
  | ExprSIGMA(idents,e1,e2) ->
      let auxset=Idset.union (vars_pred vartype e1) (vars_expr vartype e2) in
      begin
	match vartype with
	| Free -> List.fold_right Idset.remove idents auxset
	| Bound | All -> List.fold_right Idset.add idents auxset
	| Modified -> Idset.empty
      end
  | ExprPI(idents,e1,e2) ->
      let auxset=Idset.union (vars_pred vartype e1) (vars_expr vartype e2) in
      begin
	match vartype with
	| Free -> List.fold_right Idset.remove idents auxset
	| Bound | All -> List.fold_right Idset.add idents auxset
	| Modified -> Idset.empty
      end
  | ExprLambda(idents,e1,e2) ->
      let auxset=Idset.union (vars_pred vartype e1) (vars_expr vartype e2) in
      begin
	match vartype with
	| Free -> List.fold_right Idset.remove idents auxset
	| Bound | All -> List.fold_right Idset.add idents auxset
	| Modified -> Idset.empty
      end
  | ExprRecords(records) ->
      begin
        match vartype with
	  | Free | Bound | All -> vars_exprRecords vartype records
          | Modified -> Idset.empty
      end
  | ExprNumber(_) 
  | ExprBoolConstant(_) 
  | ExprBool(_)  (* SC: Warning : no variables are gathered here. Should be corrected *)
  | ExprString (_) -> Idset.empty

and 
    vars_exprRecords vartype records =
  match records with
  | RecordsFields(fields) ->
(* identifiers are useless, they are not variables per se *)
      let exps = snd (List.split fields) in
      List.fold_right Idset.union (List.map (vars_expr vartype) exps) Idset.empty
  | RecordsAccess(expr,id) ->
      begin
	match vartype with
	  | Free | Bound | All -> vars_expr vartype expr
	  | Modified -> Idset.empty
      end
and 
    vars_funCall vartype (expr1, expr2)=
  Idset.union (vars_expr vartype expr1) (vars_expr vartype expr2)

and vars_funName vartype fname=
  match fname with 
  | _ -> invalid_arg "gsl_lib:vars_funName"


and vars_exprSeq vartype expseq=
  match expseq with
  | SeqEnum(exps) -> 
      List.fold_right Idset.union (List.map (vars_expr vartype) exps) Idset.empty
  | SeqEmpty -> 
      Idset.empty

and vars_exprSet vartype expset=
  match expset with
  | SetPredefined(_) -> 
      Idset.empty

  | SetEnum(exps) ->
      List.fold_right Idset.union (List.map (vars_expr vartype) exps) Idset.empty

  | SetComprPred(ids, e1) ->
      let exps = List.map (fun s -> ExprId(s)) ids in
      let auxset=List.fold_right Idset.union (List.map (vars_expr vartype) exps) Idset.empty in
      begin 
	match vartype with
	| Free -> Idset.diff (vars_pred vartype e1) auxset
	| Bound | All -> Idset.union (vars_pred vartype e1) auxset
	| Modified -> auxset
      end

  | SetEmpty ->
      Idset.empty

  | SetUnionQ(idents, e1, e2) ->
      let auxset=
	Idset.union (vars_pred vartype e1) (vars_expr vartype e2) in
      begin
	match vartype with
	| Free -> List.fold_right Idset.remove idents auxset
	| Bound | All -> List.fold_right Idset.add idents auxset
	| Modified -> auxset
      end

  | SetInterQ(idents, e1, e2) ->
      let auxset=Idset.union (vars_pred vartype e1) (vars_expr vartype e2) in
      begin
	match vartype with
	| Free -> List.fold_right Idset.remove idents auxset
	| Bound | All -> List.fold_right Idset.add idents auxset
	| Modified -> auxset
      end

  | SetRecords(fields) ->
    (* identifiers of the fields are not variables per se *)
      let exps = snd (List.split fields) in
      List.fold_right Idset.union (List.map (vars_expr vartype) exps) Idset.empty

and vars_var vartype var=
  match var with
    _ -> invalid_arg "gsl_lib:vars_var"
and 
    
(*
   \section{Pr�dicats : Manipulations des variables}
 *)
    vars_pred vartype pred=
  match pred with
  | PredParen(p) 
  | PredNegation(p) ->
      vars_pred vartype p
  | PredExists(_)   
  | PredForAll(_) ->
      let p=Pred_acc.pred_from pred in
      let vars=Pred_acc.vars_from pred in
      begin
	match vartype with
	| Free -> List.fold_right Idset.remove vars (vars_pred vartype p)
	| Bound | All -> List.fold_right Idset.add vars (vars_pred vartype p)
	| Modified -> vars_pred vartype p
      end      
  | PredBin(_)  ->
      let (p1, p2)=Pred_acc.twopreds_from pred in
      Idset.union (vars_pred vartype p1) (vars_pred vartype p2)
  | PredAtom(_) -> 
      let (e1, e2)=Pred_acc.twoexprs_from pred in
      Idset.union (vars_expr vartype e1) (vars_expr vartype e2)
(* SC : � "investiguer" *)
  | SubstApply(_) -> invalid_arg "vars_pred : SubstApply construction"
and 
    vars_subst vartype subst=
  match subst with
  | SubstSkip ->
      Idset.empty
  | SubstAny(_) ->
      let (v,c,s)=(allids_from subst, 
		   cond_from subst, 
		   subst_from subst) in
      let auxset=Idset.union (vars_pred vartype c) (vars_subst vartype s) in
      begin
	match vartype with
	| Free -> List.fold_right Idset.remove v auxset 
	| Bound | All -> List.fold_right Idset.add v auxset
	| Modified -> auxset
      end
  | SubstLet(_) ->
      vars_subst vartype (let_to_any subst)
  | SubstVar(_) ->
      let v=allids_from subst in
      let s=subst_from subst in
      begin
	match vartype with
	| Free -> List.fold_right Idset.remove v (vars_subst vartype s)
	| Bound | All -> List.fold_right Idset.add v (vars_subst vartype s)
	| Modified -> vars_subst vartype s
      end
  | SubstBlock(_) ->
      let s=subst_from subst in
      vars_subst vartype s
  | SubstSetEqualIds(_) ->
      let (v, e, s)=affect_from subst in
      let maybeset=
	if is_simple subst 
	then Idset.empty 
	else vars_subst vartype s 
      in
      begin
	match vartype with
	| Free | All -> Idset.union ( Idset.add v (vars_expr vartype e) ) maybeset
	| Bound -> Idset.union (vars_expr vartype e) maybeset
	| Modified -> Idset.add v maybeset
      end
  | SubstPrecondition(_) 
  | SubstAssertion(_) ->
      let c=cond_from subst in
      let s=subst_from subst in
      Idset.union (vars_pred vartype c) (vars_subst vartype s)
  | SubstSequence(_) 
  | SubstParallel(_) 
  | SubstChoice(_) ->
      let (s1, s2) = twosubsts_from subst in
      Idset.union (vars_subst vartype s1) (vars_subst vartype s2) 
  | SubstIf(_) ->
      let (i, t, e) = ite_from subst in
      let auxset=Idset.union (vars_pred vartype i) (vars_subst vartype t) in
      Idset.union (vars_subst vartype e) auxset
  | SubstSelect(_) ->
      if is_simple subst
      then 
        let c=cond_from subst in
        let s=subst_from subst in
	Idset.union (vars_pred vartype c) (vars_subst vartype s) 
      else
        let wtlist=normalize_select subst in
        let (conds, substs)=List.split wtlist in
          vars_subst vartype 
	    (SubstChoice (List.map2 mk_SubstSelectbasic conds substs) )
  | SubstCase(_) ->
      vars_subst vartype (Subst_acc.case_to_select subst)
  | SubstWhile(_) ->
      let (i, v) = Subst_acc.whilepreds_from subst in
      let c=cond_from subst in
      let s=subst_from subst in
      Idset.union
	(Idset.union (vars_expr vartype i) (vars_pred vartype v))
	(Idset.union (vars_pred vartype c) (vars_subst vartype s))	
  | SubstSetIn(_) ->
      let ids=allids_from subst in
      let e=expr_from subst in
      begin
	match vartype with
	| Free | Modified | All -> List.fold_right Idset.add ids (vars_expr vartype e)
	| Bound -> vars_expr vartype e
      end
  | SubstSetEqualFun(_) ->
      let fc=funCall_from subst in
      let e=expr_from subst in
      Idset.union (vars_funCall vartype fc) (vars_expr vartype e)
  | SubstSetEqualRecords(fc, e) ->
      Idset.union (vars_fieldchain vartype fc) (vars_expr vartype e)
  | SubstBecomeSuch(_) ->
      let idents=allids_from subst in
      let pred=cond_from subst in
      Idset.union (set_from_ids idents) (vars_pred vartype pred)
  | SubstInstanciation (_) -> 
      Error.warning "Substitution instanciation detected";
      raise (Error.error "Variables extraction in substitutions")
(*  | SubstPost (_) ->
      Error.debug "Post-condition detected";
      raise (Error.Not_implemented "Variables extraction in substitutions")*)
  | SubstOperCall (_,opid,_) -> 
      Error.debug ("Operation call detected:" ^ (Id_acc.name_of opid));
      raise (Error.Not_implemented "Variables extraction in substitutions")
 and 
    vars_fieldchain vartype fieldchain=
  match fieldchain with
  | SetRecVar(id) ->
      begin
	match vartype with
	| Free | All | Modified -> Idset.add id Idset.empty
	| Bound ->  Idset.empty
      end
  | SetRecField(fc, i) -> vars_fieldchain vartype fc

        
        
let free_vars_expr = vars_expr Free
let bound_vars_expr = vars_expr Bound
let modified_vars_expr = vars_expr Modified
let all_vars_expr = vars_expr All

let free_vars_pred = vars_pred Free
let bound_vars_pred = vars_pred Bound
let modified_vars_pred = vars_pred Modified
let all_vars_pred = vars_pred All
    
let free_vars_subst = vars_subst Free
let bound_vars_subst = vars_subst Bound
let modified_vars_subst = vars_subst Modified
let all_vars_subst = vars_subst All
