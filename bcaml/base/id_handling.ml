(* $Id$ *)

open Blast
open Modules

(* \section{Comparisons} *)
(* SC: il y a des trucs pas clairs : la red�finition de <> n'influe-t-elle pas *)
(* sur cette d�finition de =? (je pense que non, cependant). La red�finition *)
(* de <> plus bas ne devrait-elle pas �tre r�cursive? *)
let (=) =
  fun a b -> not(a <> b)

let rec path_equal p1 p2 = 
  match p1 with 
    Pident(id1) -> 
      (match p2 with
        Pident(id2) -> Ident.equal id1 id2
      |  _ -> false
      )
  | Pdot(path1,str1) -> 
      (match p2 with
        Pdot(path2,str2) -> (path_equal path1 path2) && str1 = str2
      | _ -> false
      )


let (<>) = fun id1 id2 ->
  match id1 with
    | Id(path1) -> 
	(match id2 with
	     Id(path2) -> path_equal path1 path2
	   | _ -> false
	)
    | IdTy(id1,_) -> 
        (match id2 with
(* SC: hey! �a devrait pas etre la diff�rence qu'on est en train de d�finir
  qu'il faudrait appeler r�cursivement, plut�t? *)
             IdTy(s,_) -> (id1 <> s)
           | Id(s1) -> 
               (match id1 with
		    Id(s2) -> s1 <> s2
		  | _ -> false)
        )
 

let rec path_equal_large p1 p2 =
  match p1 with 
      Pident(id1) -> 
	(match p2 with
           | Pident(id2) -> 
               (Ident.name id1) = (Ident.name id2)
	   |  _ -> false
	)
    | Pdot(path1,str1) -> 
	(match p2 with
           | Pdot(path2,str2) -> ((path_equal_large path1 path2) && str1 = str2)
	   | _ -> false
	)
	

(* let egalitelarge = fun id1 id2 -> *)
let large_equality = fun id1 id2 ->
    match id1 with
      | Id(path1) -> 
	(match id2 with
	   | Id(path2) -> 
               (path_equal_large path1 path2)
	   | _ -> false
	)
      | IdTy(id1,_) -> 
        (match id2 with
           | IdTy(s,_) -> (id1 = s)
           | Id(s1) -> 
               (match id1 with
		  | Id(s2) -> s1 = s2
		  | _ -> false)
        )


(*
  Type of a symbol table (simplified) to avoid variables (name?) capture
*)
type simple_st = id list

let st_empty = []

let add_ids st_table ids =
  ids@st_table

let rec lookup_in_st id st_table =
  match st_table with
    [] -> false
  | h::t -> 
(* Beware! This "<>" is specific to identifiers. See above. *) 
      if id <> h
      then true
      else lookup_in_st id t

(*
  Lookup of an id in an instanciation= (identifier, expression) list 
*)
let rec lookup_in_instanciation id ins =
  match ins with
      [] -> false
(* SC : comparer ident=id et (name_of ident)=(name_of id) *)
    | (ident,_)::t when ident = id -> 
	begin
	  Error.debug ("***Comparaison de "^(Id_acc.name_of id) ^
		       " et de "^(Id_acc.name_of ident)^"***\n"); 
	  true
	end
    | _::t -> lookup_in_instanciation id t
      

module OrderedId=
struct
  type t=Blast.id

  let rec cat ident=
    match ident with 
    | Id(s) -> s
    | IdTy(s,_) -> cat s
  let compare id1 id2=
    compare (cat id1) (cat id2)
end;;

module Idset=Set.Make(OrderedId);;

let union_from_setlist sl=
  List.fold_right Idset.union sl Idset.empty

let set_from_ids ids=
  List.fold_right Idset.add ids Idset.empty



let create_id_from_name basename idset=
  let cpt=ref 0 in
  let newbase=(basename ^ "_fresh_") in
  let created=ref ( Printf.sprintf "%s%04d" newbase !cpt ) in
  let idlist=Idset.elements idset in
  let namelist=List.map Id_acc.name_of idlist in
    while List.mem !created namelist do
      cpt:=!cpt+1;
      created:=Printf.sprintf "%s%04d" newbase !cpt
    done;
    let newid=Blast_mk.mk_Id(!created) in
      if Idset.mem newid idset 
      then raise (Invalid_argument ("create_id:" ^ basename))
      else newid

(*SC: la cr�ation d'identificateurs est d�sormais unique, alors on utilise des "trucs" pour que les nouveaux identificateurs soient un tant soit peu diff�rents
    while Idset.mem (Blast_mk.mk_Id !created) idset do
      cpt:=!cpt+1;
      created:=Printf.sprintf "%s%04d" newbase !cpt
    done;
    let newid=Blast_mk.mk_Id(!created) in
      if Idset.mem newid idset 
      then raise (Invalid_argument ("create_id:" ^ basename))
      else newid
 *)

let create_new_id idset=
  create_id_from_name "newvar" idset


let rec create_id_from_id baseid idset=
    create_id_from_name (Id_acc.name_of baseid) idset


(* This function creates an association list from an identifiers list. The
   list is made of couples, the first element being the identifier to rename, the
   second being the proposed new identifier *)
let rec rename_ids vars idset=
  match vars with
    | hid::tids -> 
	let sure_creation_set=Idset.add hid idset in
	let assoc_id=create_id_from_id hid sure_creation_set in
	let new_idset=Idset.add assoc_id sure_creation_set in
	  (hid, assoc_id)::(rename_ids tids new_idset)
    | [] -> []


let new_ids_from_renaming renlist=
  snd (List.split renlist)


let rec id_before ident=
  match ident with
    | Blast.Id(i) -> Blast_mk.mk_Id ((Id_acc.name_of ident) ^ "$0")
    | Blast.IdTy(i,j) -> IdTy((id_before i), j)


let find_by_name name ids=
  try
    List.hd (List.filter (fun id -> (Id_acc.name_of id) = name) ids)
  with Failure _ -> raise Not_found


let isPrefixed id=
  Pervasives.(<>) (Id_acc.prefix_of id) ""

let isPrefixedName name=
  try
    (String.contains name '.')
    && (String.index name '.') > 0 
    && (String.rindex name '.') < (String.length name)-1
  with Not_found -> false
    

let removePrefixName name=
  if isPrefixedName name then
    let mostLeftDot=String.index name '.' in
      String.sub name (mostLeftDot+1) ((String.length name) - mostLeftDot - 1) 
  else name


let prefixFromName name=
  if isPrefixedName name then
      let mostLeftDot=String.index name '.' in
	String.sub name 0 mostLeftDot
  else invalid_arg "prefixFromName"


let prefixName prefix name=
  if (Pervasives.(<>) prefix "") && (Pervasives.(<>) name "")
  then prefix ^ "." ^ name
  else invalid_arg "prefixName : empty prefix or name"
