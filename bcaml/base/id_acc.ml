(* $Id$ *)


open Modules
open Blast

(* As an exception and due to the peculiar nature et critical use of *)
(* identifiers in a B project, here should be defined functions *)
(* dealing with the id type from blast and ident type from Modules *)

let rec name_of_path p =
  match p with 
    Pident(ident) -> Ident.name ident
  | Pdot(path,string) -> (name_of_path path)^"."^string

let rec number_of_path p =
  match p with 
    Pident(ident) -> Ident.number ident
  | Pdot(path,string) -> number_of_path path

let rec number_of id =
  match id with
      Id(path) -> number_of_path path
    | IdTy(x,_) -> number_of x


let rec name_of id =
  match id with
    | Id(path) -> name_of_path path
    | IdTy(x,_) -> name_of x

let rec name_of_path_without_prefix p =
  match p with 
    Pident(ident) -> Ident.name ident
  | Pdot(path,string) -> string
  
let rec name_without_prefix id =
  match id with
      Id(path) -> name_of_path_without_prefix path
    | IdTy(x,_) -> name_of x

let rec prefix_of id =
  match id with
    | Id(path) -> 
        (match path with
             Pident(_) -> ""
           | Pdot(p,_) -> name_of_path p
        )
    | IdTy(id2,_) -> prefix_of id2


let rec debug_path p=
  match p with
    | Pident(i) -> (Ident.name i) ^ "|" ^ (string_of_int (Ident.number i)) ^ "|"
    | Pdot(i,s) -> (debug_path i) ^ "." ^ s

let rec debug_id id=
  match id with
    | Id(path) -> debug_path path
    | IdTy(id2, _) -> debug_id id2
