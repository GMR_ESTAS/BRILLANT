FILES = error.ml \
 linenum.mll \
 affix.ml \
 bbop.ml \
 modules.ml \
 blast.ml \
 blast_mk.mli \
 blast_mk.ml \
 id_acc.ml \
 subst_acc.ml \
 expr_acc.ml \
 pred_acc.ml \
 clause_acc.ml \
 blast_acc.ml \
 bscoping.ml \
 bxml.ml \
 bxml2.ml \
 id_handling.ml \
 variables.ml \
 instanciation.ml \
 benv_basic.ml \
 benv.ml \
 bgraph.mli \
 bgraph.ml \
 project.mli \
 project.ml

NAME = Bcaml
MAKELIB = yes

DEPS = Xml Xmlm

