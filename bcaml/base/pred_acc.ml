(* $Id$ *)

open Blast
  

let pred_from pred=
  match pred with
  | PredParen(p) -> p
  | PredNegation(p) -> p
  | PredExists(_,p) -> p
  | PredForAll(_,p) -> p
  | SubstApply(_,p) -> p
  | _ -> raise (Invalid_argument "pred_from")

let subst_from pred=
  match pred with
    | SubstApply(s, _) -> s
    | _ -> raise (Invalid_argument "subst_from")

let twopreds_from pred=
  match pred with    
  | PredBin(_,p1, p2) -> (p1, p2)
  | _ -> raise (Invalid_argument "twopreds_from")
	
	
let connector_from pred=
  match pred with
    | PredBin(c, _, _) -> c
    | PredAtom(c, _, _) -> c
    | _ -> raise (Invalid_argument "connector_from")
	
let twoexprs_from pred=
  match pred with 
    | PredAtom(_,e1,e2) -> (e1,e2)
    | _ -> raise (Invalid_argument "twoexprs_from")
        
        
let vars_from pred=
  match pred with
  | PredExists(v,_) -> v
  | PredForAll(v,_) -> v
  | _ -> raise (Invalid_argument "vars_from")
        
        
let cvars_from pred=
  match pred with
  | _ -> raise (Invalid_argument "cvars_from")
        
        
	
	





