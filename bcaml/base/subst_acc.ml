(* $Id$ *)

open Blast
open Blast_mk

(* 
"ite" stands for "if ... then ... else"
"w_t_" stands for "when ... then ..."
*)

exception Empty_list
exception Incomplete_list
exception Unexpected_substitution

let subst_from subst=
  match subst with
  | SubstBlock(s) -> s
  | SubstPrecondition(_,s) -> s
  | SubstAny(_, _, s) -> s 
  | SubstWhile(_, s, _, _) -> s
  | SubstVar(_, s) -> s
  | SubstAssertion(_, s) -> s
  | SubstLet(_,_,s) -> s
  | SubstSelect([(_, s)], elsepart) -> 
      begin match elsepart with
      | None -> s
      | Some(_) -> raise (Invalid_argument "subst_from")
      end
  | SubstInstanciation(_,_,s) -> s
  | _ -> raise (Invalid_argument "subst_from")


let cond_from subst=
  match subst with
  | SubstPrecondition(c,_) -> c
  | SubstAny(_, c, _) -> c 
  | SubstLet(_, c, _) -> c
  | SubstWhile(c, _, _, _) -> c
  | SubstAssertion(c, _) -> c
  | SubstSelect(w_t_list, _) -> 
      begin match w_t_list with
      | [(c,_)] -> c
      | _ -> raise (Invalid_argument "cond_from")
      end
  | SubstBecomeSuch(_, c) -> c
  | _ -> raise (Invalid_argument "cond_from")


let expr_from subst=
  match subst with
  | SubstSetEqualFun(_, e) -> e
  | SubstSetEqualRecords(_, e) -> e
(*MG
  | SubstBecomeSuch(_, e) -> e
*)
  | SubstSetEqualIds([i], [e]) -> e
  | SubstSetIn(_,e) -> e
  | SubstCase(e, _, _) -> e
  | _ -> raise (Invalid_argument "expr_from")

let opid_from subst=
  match subst with
    | SubstOperCall(_, i, _) -> i
    | _ -> raise (Invalid_argument "opid_from")

let allids_from subst=
  match subst with
  | SubstAny(v, _, _) -> v
  | SubstLet(v, _, _) -> v
  | SubstVar(v, _) -> v
  | SubstSetEqualIds(v, _) -> v
  | SubstSetIn(v,_) -> v
  | SubstBecomeSuch(v, _) -> v
  | SubstInstanciation(v, _, _) -> v 
  | _ -> raise (Invalid_argument "allids_from")


let allconds_from subst=
  match subst with
  | SubstIf(itlist, _) ->
      fst ( List.split itlist )
  | SubstSelect(w_t_list, _) ->
      fst ( List.split w_t_list )
  | _ -> raise (Invalid_argument "allconds_from")


let allexprs_from subst=
  match subst with
  | SubstSetEqualIds(_, es) -> es
  | SubstInstanciation(_, es,_) -> es
  | SubstOperCall(_, _, es) -> es
  | _ -> raise (Invalid_argument "allexprs_from")


let allsubsts_from subst=
  match subst with
  | SubstChoice(sl) -> sl
  | SubstIf(itlist, _) -> snd ( List.split itlist )
  | SubstSelect(w_t_list, _) -> snd ( List.split w_t_list )
  | SubstCase(_, eitherlist, _) -> snd ( List.split eitherlist )
  | _ -> raise (Invalid_argument "allsubsts_from")


let elsepart_from subst=
  match subst with
  | SubstIf(_, elsepart) -> elsepart
  | SubstSelect(_, elsepart) -> elsepart
  | SubstCase(_, _, elsepart) -> elsepart
  | _ -> raise (Invalid_argument "elsepart_from")


let allexprLists_from subst=
  match subst with
  | SubstCase(_, eitherlist, _) -> fst ( List.split eitherlist)
  | _ -> raise (Invalid_argument "allexprlists_from")
 

let varlist_from subst=
  match subst with
  | SubstOperCall(vl,_ ,_) -> vl
  | _ -> raise (Invalid_argument "varlist_from")


let funCall_from subst=
  match subst with
  | SubstOperCall(_, id, elist) ->
      let called_nuple = ExprNuplet(elist) in
      (ExprId(id),called_nuple)
  | SubstSetEqualFun(fc, _) ->
    begin
      match fc with
	| SetFunExpr(SetFunVar(id),e) -> (ExprId(id),e)
	| _ -> invalid_arg "funCall_from"
    end
  | _ -> raise (Invalid_argument "funCall_from")

(*MG
let exprRecords_from subst=
  match subst with
  | SubstSetEqualRecords(er, _) -> er
  | _ -> raise (Invalid_argument "exprRecords_from")

*)
let ite_from subst=
  match subst with
  | SubstIf( ifthenlist, None) -> 
      begin
        match ifthenlist with
        | (cond,instr)::ifthentail -> 
            if ifthentail = [] 
            then (cond, instr, SubstSkip)
            else (cond, instr, SubstIf(ifthentail, None) )
        | [] -> raise ( Invalid_argument "ite_from : no <<if then>> part")
      end
  | SubstIf( ifthenlist, Some(elsepart)) -> 
      begin
        match ifthenlist with
        | (cond,instr)::ifthentail -> 
            if ifthentail = [] 
            then (cond, instr, elsepart)
            else (cond, instr, SubstIf(ifthentail, (Some(elsepart))) )
        | [] -> raise (Invalid_argument "ite_from : no <<if then>> part")
      end
  |  _ -> raise (Invalid_argument "ite_from")


let twosubsts_from subst=
  match subst with
  | SubstChoice( substlist ) -> 
      begin
        match substlist with
        | shead::stail -> 
            begin
              match stail with
              | [] -> raise Incomplete_list
              | [s] -> (shead, s)
              | _ -> (shead, (SubstChoice(stail)) )
            end
        | [] -> raise Empty_list
      end
  | SubstParallel(s1,s2) -> (s1, s2)
  | SubstSequence(s1,s2) -> (s1, s2)
  |  _ -> raise Unexpected_substitution


let whilepreds_from subst=
  match subst with
  | SubstWhile(_, _, invariant, variant) -> (invariant, variant)
  | _ -> raise (Invalid_argument "whilepreds_from")


let affect_from subst=
  match subst with
    | SubstSetEqualIds(vars, exprs) -> 
	begin
          match (vars, exprs) with
            | (v::vtail, e::etail) -> (v, e, (SubstSetEqualIds(vtail, etail)))
            | _ -> raise (Invalid_argument "affect_from")
	end
    | _ -> raise (Invalid_argument "affect_from ")


(* is_simple : check that a substitution is under its minimal form *)


let is_simple subst=
  match subst with
    | SubstSelect( [(_,_)], None ) -> true
    | SubstSelect( _, _) -> false
    | SubstSetEqualIds( [_], [_] ) -> true
    | SubstSetEqualIds(_) -> false
    | _ -> raise (Invalid_argument "is_simple")

(* Transforms the else part into a "when ...then" predicate *)
let normalize_select subst=
  match subst with
    | SubstSelect(w_t_list,None) -> w_t_list
    | SubstSelect(w_t_list,Some(elsesubst)) -> 
	let elsecond = PredNegation(mk_Disjunction (List.map fst w_t_list)) in
	  w_t_list@[(elsecond, elsesubst)]
    | _ -> raise (Invalid_argument "normalize_select")
	

(*
Conversion LET into ANY
*)

let let_to_any subst=
  match subst with
    | SubstLet(vars, cond, insubst) -> 
	SubstAny(vars, cond, insubst)
(*MG !!*)
(* SC: et si!! BBook p.269 *)
    | _ -> raise (Invalid_argument "let_to_any")

(*
Conversion CASE into SELECT
*)

let case_to_select subst=
  match subst with
    | SubstCase(caseexpr, eitherthenlist, elsepart) -> 
	let (eithers,thens)=List.split eitherthenlist in
	let sets=List.map 
		   (fun e ->  ExprSet(SetEnum(e)))
		      eithers 
		    in
	let whens=List.map (mk_PredIn caseexpr) sets in
	  SubstSelect( List.combine whens thens, elsepart )
    | _ -> raise (Invalid_argument "case_to_select")



let rec to_SubstSetEqualIds subst=
  match subst with
    | SubstSetEqualIds(_) -> subst
    | SubstSetEqualFun(fc, e) -> normalize_funchain fc e
    | _ -> raise (Invalid_argument "to_SubstSetEqualIds") 
(* Function for doing the following transformation:
      f(x)(y) := E
      f(x) := f(x) <+ { y |-> E}
      f := f <+ {x |-> (f(x) <+ { y |-> E})}
      *)
and normalize_funchain funchain expression =
  match funchain with
    | SetFunVar(id) -> SubstSetEqualIds([id], [expression])
    | SetFunExpr(fc, expr) ->
      let relation = to_funcall fc in
      let newassoc = ExprSet(SetEnum([ExprNuplet([expr; expression])])) in
      let right_member = ExprBin(Bbop.OverRide, relation, newassoc) in
      normalize_funchain fc right_member
and to_funcall funchain =
  match funchain with
    | SetFunVar(id) -> ExprId(id)
    | SetFunExpr(fc, expr) -> ExprFunCall(to_funcall fc, expr)
