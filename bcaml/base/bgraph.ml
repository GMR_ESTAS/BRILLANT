
(* The purpose of this module is to give the ability to represent dependencies
between B machines. While the edges of the graph will be labeled with
classical B dependency types, the nodes can be anything the user wants
(identifiers, strings, B machines,etc *)

(* TODO: consider using ocamlgraph *)

type linkType =
  | Promotes
  | Extends
  | Sees
  | Uses
  | Includes
  | Imports
  | RefinedBy
(* this link will help us avoiding multiple root nodes, for example if
   a machine is always included with a prefix, but never with its "bare"
   form *)
  | Renames


type 'a edge =  'a * linkType * 'a
type 'a t = ('a edge) list

(* By construction, all the edges in the graph will be different. We don't use
   sorted lists, because B projects usually aren't huge *)

let emptyGraph = []

let compare_graphs graph1 graph2 =
  compare (List.sort compare graph1) (List.sort compare graph2)

let equal graph1 graph2 =
  (compare_graphs graph1 graph2) = 0

let empty graph = equal graph emptyGraph

let mk_edge node1 link node2=(node1, link, node2)

let source (node, _, _) = node
let target (_, _, node) = node
let dependency (_, link, _) = link

(* The edge is added to the graph only if it isn't already present in it *)
let rec add graph edge=
  match graph with
    | [] -> [edge]
    | one_edge::others ->
	if one_edge=edge then graph
	else one_edge::(add others edge)


(* Merges [graph1] and [graph2] *)
let rec merge graph1 graph2 =
  List.fold_left (fun g e -> add g e) graph1 graph2

(* As by construction, all edges are different, we stop as soon as we find the
   edge that is to be removed *)
let rec remove graph edge=
  match graph with
    | [] -> []
    | one_edge::others ->
	if one_edge = edge then others
	else one_edge::(remove others edge)

(* Removes a subgraph from a graph. Does not fail if [subgraph] contains
   edges not in [graph]. *)
let rec sub graph subgraph =
  match subgraph with
    | [] -> graph
    | one_edge::others ->
	remove (sub graph others) one_edge


let to_list graph = graph

(* Creates the graph from a list of edges, removing redundancy in the
   process *)
let from_list edgeList = merge emptyGraph edgeList


let prefixable_dependency edge=
  match (dependency edge) with
    | Extends | Includes | Imports -> true
    | _ -> false


(* For some very particular cases, we need to have a scoping order among the
   machines we depend on : indeed, when defining aliases (visible variables)
   in an imported machine and the refined machine, we have to ensure that
   variables in the refined machine are the same (scoping) that the ones from
   the imported machine. To this end we must scope first the imported machine,
   then the refined machine so that in the scoping environment the variables
   from the refined machine come first, thus ensuring the variables used in
   the machine are the same are the ones from the refined one. We can make a
   similar remark for the promoted operations. *)
let value_of_dep dep=
  match dep with
    | Promotes -> 1
    | Extends -> 2
    | Sees -> 3
    | Uses -> 3
    | Includes -> 3
    | Imports -> 3
    | RefinedBy -> 4
    | Renames -> 5


let compare_edges edge1 edge2=
  let val1=value_of_dep (dependency edge1) in
  let val2=value_of_dep (dependency edge2) in
    compare val1 val2

let sort_edges edgeList=
  List.fast_sort compare_edges edgeList


let rec mem_edge graph edge=List.mem edge graph

let rec mem_node graph node=
  match graph with
    | [] -> false
    | (n1, _, n2)::others ->
	if n1=node || n2=node then true else mem_node others node

(* By hypothesis, graph1 and graph2 are correctly built, that is all their
   edges respectively are different *)
let rec merge graph1 graph2=
  match graph1 with
    | [] -> graph2
    | one_edge::others ->
	add (merge others graph2) one_edge

let filter graph condition = List.filter condition graph

let filter_dependency graph dep =
  filter graph (fun edge -> (dependency edge)=dep)

let filter_source graph node =
  filter graph (fun edge -> (source edge)=node)

let filter_target graph node =
  filter graph (fun edge -> (target edge)=node)

let filter_node graph node =
  filter graph (fun edge -> (source edge) = node || (target edge) = node)


let filter_sourceDep graph dep node=
  filter graph (fun e -> (dependency e)=dep && (source e)=node)

let filter_targetDep graph dep node=
  filter graph (fun e -> (dependency e)=dep && (target e)=node)

(* The output is a couple (g1, g2) where g1 satisfies the \emph{condition},
   and not g2 *)
let partition graph condition = List.partition condition graph


(* The ouput is a graph where all edges have been changed by the
   function \emph{change}. Note that depending on this function, the
   edges of the returned graph might not be unique *)
let map change graph=
  List.map change graph


(* Retrieves a list of all nodes in the graph *)
let nodes graph=
  let rec aux_nodes gr accu=
  match gr with
    | [] -> accu
    | (n1, _, n2)::others ->
	let accu1=if List.mem n1 accu then accu else n1::accu in
	let accu2=if List.mem n2 accu1 then accu1 else n2::accu1 in
	  aux_nodes others accu2
  in
    aux_nodes graph []


(* This function returns the graph of all the edges belonging to a
   chained dependency starting from \emph{node} *)
let rec subgraph graph node=
  let first_edges=filter_source graph node in
  let depnodes=List.map target first_edges in
  let subgraphs=List.map (subgraph graph) depnodes in
  let mergedsubs=List.fold_left merge emptyGraph subgraphs in
  let added_edges=List.fold_left add mergedsubs first_edges in
    added_edges


exception No_root_node
exception Several_root_nodes

(* This is specific to B projects : the B project is an \emph{extended tree},
   that is leafs can be linked to several times by differents machines, but there
   is anyway a main machine that is never linked to. This function raises an
   error if it finds multiple root nodes, or no one *)
let get_root_node graph=
  let all_nodes=nodes graph in
  let is_never_target gr node=
    (filter_target gr node)=emptyGraph
  in
  let found_roots=
    List.filter (is_never_target graph) all_nodes
  in
    match found_roots with
      | [] -> raise No_root_node
      | [root] -> root
      | _ -> raise Several_root_nodes


let dependedMachines graph node=
  let strictDepended=filter_source graph node in
  let withoutRefinement=List.filter (fun e -> (dependency e) <> RefinedBy) strictDepended in
  let noRefNodes=List.map target withoutRefinement in
  let containsRefined=filter_target graph node in
    try
      let edgeRefined=List.find (fun e -> (dependency e) = RefinedBy) containsRefined in
	(source edgeRefined)::noRefNodes
    with
	Not_found -> noRefNodes


let dependency_to_string dep=
  match dep with
  | Promotes -> "Promotes"
  | Extends -> "Extends"
  | Sees -> "Sees"
  | Uses -> "Uses"
  | Includes -> "Includes"
  | Imports -> "Imports"
  | RefinedBy -> "RefinedBy"
  | Renames -> "Renames"


let edge_to_string node_to_string edge=
  (node_to_string (source edge))
  ^ "--" ^ (dependency_to_string (dependency edge)) ^ "-->"
  ^ (node_to_string (target edge))

let graph_to_string node_to_string g=
  let strlist=List.map (edge_to_string node_to_string) g in
  let fullstring=List.fold_left (fun s1 s2 -> s1 ^ "\n"^ s2) "" strlist in
    fullstring

(* Needed for "connex", but a set module built upon an ordered
   type of nodes would be better. [l] is supposed to be sorted *)
let rec add_no_redundant l x =
  match l with
    | [] -> [x]
    | y::tail ->
	if x > y then y::(add_no_redundant tail x)
	else if x = y then tail
	else x::l


(* Chooses a node in [graph] *)
let choose graph =
  try
    Some(source (List.hd graph))
  with
      Failure _ -> None

(* Chooses an edge in [graph] *)
let choose_edge graph =
  try
    Some(List.hd graph)
  with
      Failure _ -> None


(* Computes the (undirected) connected part of [graph] to which [node]
   belong *)
let connected graph node =
  let rec aux_connected in_connection lesser_graph =
    let connected_nodes = nodes in_connection in
    let (conn_graph, graph_left) =
      partition
	lesser_graph
	(fun edge ->
	   (List.mem (source edge) connected_nodes)
	   || (List.mem (target edge) connected_nodes))
    in
    let more_connection = merge in_connection conn_graph in
      if equal in_connection more_connection then (in_connection, graph_left)
      else aux_connected more_connection graph_left

  in
  let relevant_edges = filter_node graph node in
  let chosen_edge = choose_edge relevant_edges in
    match chosen_edge with
      | None -> (emptyGraph, graph)
      | Some(edge) -> aux_connected [edge] (remove graph edge)


(* Sends a list of strongly connected subgraphs of [graph] *)
let connected_parts graph =
  let rec aux_connected_parts done_parts subgraph =
    let node_opt = choose subgraph in
      match node_opt with
	| None -> done_parts
	| Some(node) ->
	    let (node_connected, left_graph) = connected subgraph node in
	      aux_connected_parts (node_connected::done_parts) left_graph

  in
    aux_connected_parts [] graph

let edge_attributes = [
  (Promotes, "[arrowhead=invdot,color=blue]");
  (Extends, "[arrowhead=inv,color=blue]");
  (Sees,"[color=lightgray]");
  (Uses,"[color=blueviolet]");
  (Includes,"[color=blue]");
  (Imports,"[color=midnightblue]");
  (RefinedBy,"[style=bold,dir=back]");
  (Renames,"[color=red,style=dotted]");
]


let dot_of_edge string_of_node (source, link, target) =
  (string_of_node source)
  ^ " -> "
  ^ (string_of_node target)
  ^ " "
  ^ (List.assoc link edge_attributes)

let dot_of_edges indent string_of_node graph =
  if empty graph then "" else
    indent ^
      (String.concat
	 (";\n" ^ indent)
	 (List.map (dot_of_edge string_of_node) graph))
    ^ ";"

let cluster_of_graph indent moreindent string_of_node graph =
  let root = get_root_node graph in
  let rootname = string_of_node root in
  let cluster_name = "cluster" ^ rootname in
    indent ^ "subgraph " ^ cluster_name ^ "{\n"
    ^ indent ^ moreindent ^ "label = \"" ^ rootname ^ "\";\n"
    ^ (dot_of_edges (indent ^ moreindent) string_of_node) graph
    ^ indent ^ "}"


(* TODO: it would also make sense to interface it through ODot (part
   of cameleon or, better, the dot interface of ocamlgraph. *)

(* The idea is to build a cluster for each refinement sequence in the
   graph and to generate remaining links once it is done. [string_of_node]
   must produce a string compatible with dot ids, i.e. alphanumeric strings
   possibly containing underscores and not starting with a digit, or
   quoted strings possibly containing escaped strings.
 *)
let dot string_of_node graph =
  let indent = "  " in
  let (ref_seqs, not_refs) =
    partition graph (fun edge -> (dependency edge) = RefinedBy)
  in
  let connected_refseqs = connected_parts ref_seqs in
  let clusters =
    List.map
      (cluster_of_graph indent indent string_of_node)
      connected_refseqs
  in
  let left_links = dot_of_edges indent string_of_node not_refs in
  let root = get_root_node graph in
    "digraph " ^ (string_of_node root) ^ " {\n"
    ^ (String.concat "\n\n" clusters) ^ "\n\n"
    ^ left_links ^ "\n"
    ^ "}\n"
