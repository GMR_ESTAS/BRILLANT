open Blast

(*type amn = 			
  | Machine of head * clause list
  | Refinement of head * id * clause list  
  | Implementation of head * id * clause list 
  | EmptyTree

and
   head = id * ids

and
   clause = 
  | Definitions of def list
  | Constraints of predicate
  | Invariant of predicate
  | Variant of expr
  | Sets  of set list
  | Initialisation of substitution
  | ConstantsConcrete of ids
  | ConstantsAbstract of ids 
  | ConstantsHidden of ids
  | Properties of predicate
  | Values of (id * expr) list
  | VariablesConcrete of ids
  | VariablesAbstract of ids
  | VariablesHidden of ids
  | Promotes of ids 
  | Assertions of predicate list
  | Operations of operation list 
  | Sees of ids 
  | Uses of ids 
  | Extends of instance list
  | Includes of instance list
  | Imports of instance list

and
   def = 
  | Def of id * ids * defBody 
  | FileDef of string

and 
   defBody = 
  | PredDefBody of predicate
  | ExprDefBody of expr
  | SubstDefBody of substitution

and
   set = 
  | SetAbstDec of id
  | SetEnumDec of id * ids  
  | SetRecordsDec of id * recordsItem list

and
   instance = id * expr list 

and
   type_B =  
  | TypeNatSetRange of expr * expr
  | PredType of expr
  | RecordsType of field list
  | TypeIdentifier of id
  | PFunType of type_B * type_B 
  | FunType of type_B * type_B 
  | ProdType of type_B * type_B 
  | Sequence of type_B 
  | SubType of type_B * expr
  | Fin_B of type_B 
  | Untyped 

and
  field = id * type_B

and
    predicate =
     PredParen of predicate
   | PredNegation of predicate
   | PredExists of ids * predicate
   | PredForAll of ids * predicate
   | PredBin of Bbop.bop2 * predicate * predicate
   | PredAtom of Bbop.bop2 * expr * expr 
   | PredCall of id * expr list	 
   | SubstApply of substitution * predicate

and
    operation = id * ids * ids * substitution

and
    substitution =
   | SubstOperCall of ids * id * expr list
   | SubstPost of substitution * predicate
   | SubstBlock of substitution 
   | SubstPrecondition of predicate * substitution
   | SubstAssertion of predicate * substitution 
   | SubstChoice of substitution list
   | SubstIf of (predicate * substitution) list * substitution option
   | SubstSelect of (predicate * substitution) list * substitution option
   | SubstCase of expr * (expr list * substitution) list * substitution option
   | SubstAny of ids * predicate * substitution 
   | SubstLet of ids * predicate * substitution 
   | SubstVar of ids * substitution 
   | SubstWhile of predicate * substitution * expr * predicate
   | SubstSkip
   | SubstSequence of substitution * substitution 
   | SubstParallel of substitution * substitution 
   | SubstBecomeSuch of ids * predicate
   | SubstSetEqualIds of ids * expr list
   | SubstSetEqualFun of expr * expr list * expr
   | SubstSetEqualRecords of exprRecords * expr
   | SubstSetIn of ids * expr  
   | SubstInstanciation of ids * expr list * substitution

and
    expr =	
   | ExprParen of expr
   | ExprId of id
   | ExprFunCall of expr * expr list
   | ExprBin of Bbop.op2 * expr * expr 
   | ExprUn  of Bbop.op1 * expr
   | ExprSeq of exprSeq 
   | ExprSet of exprSet 
   | RelSet of expr * expr 
   | RelSeqComp of expr * expr
   | ExprNumber of number 
   | ExprString of string 
   | ExprRecords of exprRecords 
   | ExprNuplet of expr list 
   | ExprSIGMA of ids * predicate * expr
   | ExprPI of ids * predicate * expr
   | ExprLambda of ids * predicate * expr
   | ExprBool of predicate
   | ExprBoolConstant of boolConstant

and 
    exprRecords = 	
   | RecordsWithField of recordsItem list
   | Records of expr list 
   | RecordsAccess of expr * id

and 
    recordsItem = 
    id * expr 

and
    exprSeq = 
   | SeqEnum of expr list 
   | SeqEmpty

and 
    exprSet = 
   | SetEmpty
   | SetPredefined of setName
   | SetEnum of expr list 
   | SetCompr of expr list
   | SetComprPred of ids * predicate 
   | SetUnionQ of ids * predicate * expr 
   | SetInterQ of ids * predicate * expr 
 
and
    setName =     
   | NAT
   | NATURAL
   | NAT1
   | NATURAL1
   | INTEGER
   | INT
   | STRING
   | BOOL

and
    boolConstant = 
     TrueConstant
   | FalseConstant

and
    number =  
   | MinNumber
   | Number of int
   | MaxNumber 

and
   ids = id list

and
   id = 
   | Id of path
   | IdTy of id * type_B

and path =
    Pident of t             
  | Pdot of path * string 

and t = {nom: string; indice: int}

and
    bop2 = 
  | Ou 
  | And 
  | Implies
  | Equiv
  | Equal
  | NotEqual
  | Less 
  | LessEqual 
  | Greater 
  | GreaterEqual 
  | SubSet
  | NotSubSet
  | StrictSubSet
  | NotStrictSubSet
  | In
  | NotIn

and
   op1 =
  | Closure 
  | ClosureOne 
  | Cod 
  | Perm
  | Min
  | Max
  | Card
  | Pow
  | PowOne
  | Fin
  | FinOne
  | Identity
  | Dom
  | Ran
  | UnionGen
  | InterGen
  | Seq
  | SeqOne
  | ISeq
  | ISeqOne
  | Size
  | First
  | Last
  | Front
  | Tail
  | Rev
  | Conc
  | RelFnc
  | FncRel
  | Tree
  | Btree
  | Left
  | Right
  | Infix
  | Top
  | Sons
  | Prefix
  | Postfix
  | SizeT
  | Mirror
  | Bool
  | Pred
  | Succ
  | Tilde
  | UMinus

and
   op2 = 
    Minus 
  | Puissance
  | Mul
  | Div
  | Mod
  | Plus 
  | NatSetRange  
  | CartesianProd
  | PartialFunc
  | TotalFunc
  | PartialInj
  | TotalInj
  | PartialSurj
  | TotalSurj
  | PartialBij
  | TotalBij      
  | OverRide
  | AppendSeq
  | PrependSeq
  | PrefixSeq
  | SuffixSeq
  | ConcatSeq      
  | DomRestrict
  | RanRestrict
  | DomSubstract
  | RanSubstract
  | InterSets
  | UnionSets
  | SetMinus
  | DirectProd
  | ParallelComp
  | Image
  | Prj1
  | Prj2
  | Rank
  | Father
  | SubTree
  | Arity*)

val bxml2blast: string -> amn
