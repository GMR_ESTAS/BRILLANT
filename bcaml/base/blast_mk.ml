(* $Id$ *)

open Blast
open Modules

exception Empty_list

let mk_path _x1 = 
  let strlist = List.rev (Str.split (Str.regexp "\\.") _x1)
  in
  let rec list_to_path stringlist =
    match stringlist with
      [] -> raise (Invalid_argument "mk_path s : s should be a non empty string")
    | [s] -> Pident(Ident.create s)
    | s::t -> Pdot((list_to_path t),s)
  in
  list_to_path strlist

(* SC: Ca jure un peu, quand m�me... *)
(* VS: CDuce integration *)
let mk_Id _x1 = Id(mk_path _x1)


(* Now some more complex constructors *)

(* Predicates *)

let mk_PredTrue () = 
(* let alone_exprid=mk_ExprId(mk_Id "x") in  *)
  let alone_exprid=ExprNumber(Number(1l)) in 
  PredAtom(Bbop.Equal, alone_exprid, alone_exprid)

let mk_PredIn vars set= PredAtom(Bbop.In,vars,set)

let mk_PredNotIn vars set= PredAtom(Bbop.NotIn,vars,set)

(* TODO: mk_Disjunction and mk_Conjunction do not seem to follow left associativity *)
let rec mk_Disjunction predlist=
  match predlist with
  | phead::ptail -> 
      begin
        try
          PredBin(Bbop.Ou, phead, (mk_Disjunction ptail))
        with
        | Empty_list -> phead
      end
  | [] -> raise Empty_list
	
	
let rec mk_Conjunction predlist=
  match predlist with
  | phead::ptail -> 
      begin
        try
          PredBin(Bbop.And, phead, (mk_Conjunction ptail))
        with
        | Empty_list -> phead
      end
  | [] -> raise Empty_list


(* Sets *)
(* TODO: SC: changer le nom *)
let mkSet constlist=
  ExprSet(SetEnum(constlist))


(* Substitutions *)

let mk_SubstSequence_from_list substlist=
  
  let rec mkSSfl sl=
    match sl with
    | [] -> raise Empty_list
    | subst::sltail ->
	try SubstSequence(subst, (mkSSfl sltail)) with
	  Empty_list -> subst
  in
  mkSSfl substlist



let mk_SubstParallel_from_list substlist=
  
  let rec mkSPfl sl=
    match sl with
    | [] -> raise Empty_list
    | subst::sltail ->
	try SubstParallel(subst, (mkSPfl sltail)) with
	  Empty_list -> subst
  in
  mkSPfl substlist


let mk_SubstSelectbasic cond subst=
  SubstSelect([(cond, subst)], None)
