
(**  Abstract Syntax Tree for the B Language (BLAST) *)

(* ************************************************** *)
(* NE PAS DOCUMENTER CE FICHIER MAIS PLUTOT blast.mli *)
(* ************************************************** *)

exception Parse_error of int * string


type amn =
  | Machine of head * clause list
  | Refinement of head * id * clause list
  | Implementation of head * id * clause list
  | EmptyTree

and head = id * id list

and clause =
  | Constraints of predicate
  | Invariant of predicate
  | Sets of set list
  | Initialisation of substitution
  | ConstantsConcrete of id list
  | ConstantsAbstract of id list
  | ConstantsHidden of id list
  | Properties of predicate
  | Values of (id * expr) list
  | VariablesConcrete of id list
  | VariablesAbstract of id list
  | VariablesHidden of id list
  | Promotes of id list
  | Assertions of predicate list
  | LocalOperations of operation list
  | Operations of operation list
  | Sees of id list
  | Uses of id list
  | Extends of instance list
  | Includes of instance list
  | Imports of instance list

and set =
  | SetAbstDec of id
  | SetEnumDec of id * id list

and instance = id * expr list

and type_B =
  | TypeNatSetRange of expr * expr
  | TypeBaseSet of setName
  | TypeIdentifier of id
  | TypePFun of type_B * type_B
  | TypeFun of type_B * type_B
  | TypeProd of type_B * type_B
  | TypeSequence of type_B
  | TypePowerset of type_B
  | TypeRecords of field list
  | Untyped

and field = id * type_B

and predicate =
  | PredParen of predicate
  | PredNegation of predicate
  | PredExists of id list * predicate
  | PredForAll of id list * predicate
  | PredBin of Bbop.bop2 * predicate * predicate
  | PredAtom of Bbop.bop2 * expr * expr
  | SubstApply of substitution * predicate

and operation = id * id list * id list * substitution

and expr =
  | ExprParen of expr
  | ExprId of id
  | ExprBefore of id
  | ExprFunCall of expr * expr
  | ExprTrin of Bbop.op3 * expr * expr * expr
  | ExprBin of Bbop.op2 * expr * expr
  | ExprUn of Bbop.op1 * expr
  | ExprSeq of exprSeq
  | ExprSet of exprSet
  | ExprNumber of number
  | ExprString of string
  | ExprRecords of exprRecords
  | ExprNuplet of expr list
  | ExprSIGMA of id list * predicate * expr
  | ExprPI of id list * predicate * expr
  | ExprLambda of id list * predicate * expr
  | ExprBool of predicate (* bool() *)
  | ExprBoolConstant of boolConstant

and exprRecords =
  | RecordsFields of (id option * expr) list
  | RecordsAccess of expr * id

and exprSeq =
  | SeqEnum of expr list
  | SeqEmpty

and exprSet =
  | SetEmpty
  | SetPredefined of setName
  | SetEnum of expr list
  | SetComprPred of id list * predicate
  | SetUnionQ of id list * predicate * expr
  | SetInterQ of id list * predicate * expr
  | SetRecords of (id * expr) list

and setName =
  | NAT
  | NATURAL
  | NAT1
  | NATURAL1
  | INTEGER
  | INT
  | STRING
  | BOOL

and boolConstant =
  | TrueConstant
  | FalseConstant

and number =
  | MinNumber
  | Number of int32
  | MaxNumber

and substitution =
  | SubstOperCall of id list * id * expr list
  | SubstBlock of substitution
  | SubstPrecondition of predicate * substitution
  | SubstAssertion of predicate * substitution
  | SubstChoice of substitution list
  | SubstIf of (predicate * substitution) list * substitution option
  | SubstSelect of (predicate * substitution) list * substitution option
  | SubstCase of expr * (expr list * substitution) list * substitution option
  | SubstAny of id list * predicate * substitution
  | SubstLet of id list * predicate * substitution
  | SubstVar of id list * substitution
  | SubstWhile of predicate * substitution * expr * predicate
  | SubstSkip
  | SubstSequence of substitution * substitution
  | SubstParallel of substitution * substitution
  | SubstSetEqualIds of id list * expr list
  | SubstSetEqualFun of funchain * expr
  | SubstSetEqualRecords of fieldchain * expr
  | SubstBecomeSuch of id list * predicate
  | SubstSetIn of id list * expr
  | SubstInstanciation of id list * expr list * substitution

and fieldchain =
  | SetRecVar of id
  | SetRecField of fieldchain * id

and funchain =
  | SetFunVar of id
  | SetFunExpr of funchain * expr

and ids = id list

and id =
  | Id of Modules.path
  | IdTy of id * type_B

