 (*  
    ------------------------------------------------------------------ 
    $Id$
    
    Copyright (c) 1999 Christian Lindig <lindig@ips.cs.tu-bs.de>. All
    rights reserved. See COPYING for details.
    ------------------------------------------------------------------ 
    Slightly modified by Georges Mariano for the Bcaml project
  *)
  
(*  xml abstract syntax  *) 
  
 (* 
    http://www.w3.org/TR/REC-xml#NT-Name
    (Letter | '_' | ':') (NameChar)*
  *)
  
type name	
      = string
      
type attvalue 
      = string
      
(* 
   http://www.w3.org/TR/REC-xml#NT-Attribute    
   Attribute ::= Name Eq AttValue
 *)
      
type attribute	
      = name * attvalue           
		    (* name="value" *)
      
(* 
   processing instruction 
   http://www.w3.org/TR/REC-xml#sec-pi
 *)
      
type pi         
      = name * string list    
      
type misc  = 
    Pi of pi
  | Comment of string
	
(* (start-)tag <termdef id="dt-dog" term="dog"> *)
	
type tag = 
    name * attribute list 
      
(* http://www.w3.org/TR/REC-xml#NT-element *)
      
      
type element	= 
    Eelement  of name * attribute list * element list
  | Eempty    of tag 
  | Echunk    of string
  | Epi       of pi 
	
	
type dtd_id     = 
    DTDsys    of string
  | DTDpub    of string * string
	
	
(* dtd is incomplete - no markup type provided  *)
	
	
type dtd        = 
    DTD      of name * dtd_id option 
	
(* BUT 'dtd' should be  'doctypedecl' *)
	
type doctypedecl = 
    DOCTYPE of name *  dtd_id option 
    
type stringType = 
    CDATA
      
type tokenizedType = 
    ID 
  | IDREF 
  | IDREFS 
  | ENTITY 
  | ENTITIES 
  | NMTOKEN 
  | NMTOKENS
      
type enumeration = string list
      
type notationType = string list
      
type enumeratedType = 
    NotationType of notationType 
  | Enumeration of enumeration
	
type defaultDecl = 
    REQUIRED 
  | IMPLIED 
  | FIXED of string
	
 (* http://www.w3.org/TR/REC-xml#NT-AttType *)
	
type attType = 
    StringType of stringType
  | TokenizedType of tokenizedType
  | EnumeratedType of enumeratedType
	
	
type attDef = string 
      * attType 
      * defaultDecl
      
type version_info = string 
      
type encName = string 
      
 (* http://www.w3.org/TR/REC-xml#NT-XMLDecl  *)
      
type xmldecl    = 
    XMLDecl   of version_info     
        * bool option       (* standalone *)
	* encName option     (* encoding *) 
	
	
 (* http://www.w3.org/TR/REC-xml#NT-prolog *)
	
type prolog     = 
    Prolog    of xmldecl option 
	* dtd option 
	* pi list 
       
(* pi should be misc *)
	
  (* http://www.w3.org/TR/REC-xml#NT-document *)
	
type document   = 
    XML of prolog 
	* element  
	* pi list                
   
