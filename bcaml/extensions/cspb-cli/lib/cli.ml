(*
    Copyright (c) 2009 Huu Nghia Nguyen

    This file is part of the cspb-cli tool integrated in BRILLANT.

    cspb-cli is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cspb-cli is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cspb-cli.  If not, see <http://www.gnu.org/licenses/>.
*)

 
(**
 * nhnghia
 * Chercher un CLI qui est sous forme : ((cb=1)=>P1) & ((cb=2)=>P2) & ... & ((cb=x)=>Px)
 * Px est un predicat concernat des parametres de processus x-iem
 *)

  (**
   * Retourne une list des 
   *  - name : string (nom de processus)
   *  - index: int
   *  - (variable : string) list
   * CLI :  ((cb = index) => P(variable_list)) &  ((cb = index) => P(variable_list)) & ...
   *)
let get_cli_in_process_list cproc_lst = 
  let lst = List.map (fun (name,param,_) -> (name,param) ) cproc_lst in
  let arr = Array.of_list lst in
  let len = (Array.length arr) - 1 in
  let lst = ref ([] : (string*int*(string list)) list) in
  
  for i=0 to len do
    let (name,param) = arr.(i) in
    if param <> [] then
      lst := (name, i, param) :: !lst
  done;
  !lst
  
and
  cli2string  cli = 
    if cli = [] then ""
    else        
      let trait_cli (name,i,param) =
        let id = (string_of_int i) in
        let str = ("((cb = " ^ id ^ ") => P_" ^ id ^ ")") in
          str
      in    
      let lst = List.map trait_cli cli in
      let cli_str = B2string.list2string " & " (fun x->x) lst in
      
      let trait_form (name,i,param) = 
        let p = B2string.list2string ", " (fun x->x) param in
        let id = (string_of_int i) in
        let str = ("   P_" ^ id ^ " = predicat(" ^ p ^ ")") in
          str  
      in
      let lst1 = List.map trait_form cli in
      let cli_form = B2string.list2string "\n" (fun x->x) lst1 in
      (cli_str ^ "\n avec : \n" ^ cli_form)
      