(*
    Copyright (c) 2009 Huu Nghia Nguyen

    This file is part of the cspb-cli tool integrated in BRILLANT.

    cspb-cli is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cspb-cli is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cspb-cli.  If not, see <http://www.gnu.org/licenses/>.
*)

  (**
   * nhnghia
   * tous les types de donnes de CSP
   *)
type csp = 
      Include of string
    | Definition of definition
    | Process of process
and definition = 
      Datatype of datatype
    | Nametype of nametype
    | Constant of constant
    | Channel  of channel
    | GroupeChannel of string * (string list)
and datatype = string * (string list)
and nametype = string * expr
and constant = string * expr
and channel = string * expr list
and groupe_channel = string * (string list)
and process = string * (string list) * (process_body list)

and process_body = 
      BodyParen of (process_body list)
    | ChannelCall of string * (channel_param list)
    | ProcessCall of string * (expr list)
    | Choice of ((process_body list) list)
    | If of expr * (process_body list) * (process_body  list)
    | Lambda of string * expr * (process_body list)
    | Funx of string
    
and channel_param = 
      channel_param_type * expr    
      
and channel_param_type = IN | OUT | ID
and expr =
      ExprParen of expr 
    | String of string
    | Int of int
    | Bool of bool
    | Set of set
    | ExprBin1 of string * expr * expr
    | ExprBin2 of expr * string * expr
    | ExprUn of string * expr
and set = 
      SetName of string
    | SetEnum of expr list
    | SetBound of expr * expr

 ;;
