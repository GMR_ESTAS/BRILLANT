(*
    Copyright (c) 2009 Huu Nghia Nguyen

    This file is part of the cspb-cli tool integrated in BRILLANT.

    cspb-cli is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cspb-cli is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cspb-cli.  If not, see <http://www.gnu.org/licenses/>.
*)

exception Error of string;;         

(*-----------------------------------------------------------------------*)
(*-----------------------VARIABLES IS IN---------------------------------*)
(*-----------------------------------------------------------------------*)
let rec
  is_in_expr var_name expr = match expr with
      Csptypes.String s -> (Csp2string.string2string s) = (Csp2string.string2string var_name)
    | Csptypes.ExprParen e  ->  (is_in_expr var_name e)
    | Csptypes.ExprBin1 (s, expr1, expr2) -> is_in_expr2 var_name expr1 expr2      
    | Csptypes.ExprBin2 (expr1, s, expr2) -> is_in_expr2 var_name expr1 expr2
    | Csptypes.Set set -> false
    | _  -> false
      
and
  is_in_expr2 var_name expr1 expr2 = 
    if (is_in_expr var_name expr1) then
      true
    else (is_in_expr var_name expr2)
;;

(*-----------------------------------------------------------------------*)
(*-----------------------GET TYPE OF VARIABLE----------------------------*)
(*-----------------------------------------------------------------------*)

let cchan_lst = ref ([] : Csptypes.channel list);;
let cdef_lst = ref ([] : Csptypes.definition list);;

let rec  
  get_variable_global cproc_lst = 
  let lst = List.map (get_variable) cproc_lst in
    let ls1 = List.map (fun (a,b,c)-> if b=[] then [] else [a,b,c]) lst in
  let lst = List.fold_left List.append [] ls1 in
    
  let combine (proc_name, param_lst, type_lst) = 
    let lst1 = List.map2 (fun x->fun y-> if y="x" then [] else [(((*proc_name ^ "_" ^ *)x),y)]) param_lst type_lst in
    let lst2 = List.fold_left List.append [] lst1 in
    
    let var_lst = List.map (fun (x,y) -> x) lst2 in
    let var = B2string.list2string ", " (fun x->x) var_lst in
    
    let inv_lst = List.map (fun (x,y) -> if (y="SET") then (x ^ " <: NAT") else(x ^ " : " ^ y)) lst2 in
    let inv = B2string.list2string " & " (fun x->x) inv_lst in
    
    let ini_lst = List.map (fun (x,y) -> if (y="SET") then (x ^ " := {}") else (x ^ " :: " ^ y)) lst2 in
    let ini = B2string.list2string " || " (fun x->x) ini_lst in
    
    (var, inv, ini)
  in
    let lst1 = List.map combine lst in
    
    let rec remove_identity l  = match l with
    [] -> []
    | [h] -> [h]
    | (a,b,c)::t ->   if (Outil.isExiste (fun (x,y,z) ->
      (Str.string_match (Str.regexp (".*" ^ a)) x 0)) t) then
      remove_identity t
    else
      
      (a,b,c) :: (remove_identity t)
    in    
    let lst1 = remove_identity lst1 in
    
    let var = Outil.list2string ",\n   " (fun (x,y,z)->x) lst1 in    
    let inv = Outil.list2string " &\n   " (fun (x,y,z)->y) lst1 in    
    let ini = Outil.list2string " ||\n   " (fun (x,y,z)->z) lst1 in
    (var, inv, ini)
    
and
  get_variable (proc_name, param_lst, body) = 
    let type_lst = List.map (fun x -> (get_type_from_body x body)) param_lst in
    (proc_name, param_lst, type_lst)
    
and
  get_type_from_body var_name body = match body with
      []   -> ""
    | h::t -> let s = (get_type_from_ope var_name h) in
      if s <> "" then
        s
      else
        get_type_from_body var_name (List.tl body)
        
and  
  get_type_from_body_list var_name body_lst = match body_lst with
      []   -> ""
    | h::t -> let s = (get_type_from_body var_name h) in
      if s <> "" then
        s
      else
        get_type_from_body_list var_name (List.tl body_lst)
        
and
  get_type_from_ope var_name ope = match ope with
      Csptypes.BodyParen b_lst              -> (get_type_from_body var_name b_lst)  
    | Csptypes.ChannelCall (name, param_lst)-> get_type_from_channel var_name name param_lst 
    | Csptypes.ProcessCall (name, expr_lst) -> get_type_from_expr_list var_name expr_lst
    | Csptypes.Choice b_lst_lst             -> get_type_from_body_list var_name b_lst_lst
    | Csptypes.If (cond, b1_lst, b2_lst)    -> get_type_from_if var_name cond b1_lst b2_lst      
    | Csptypes.Lambda (name, expr, b_lst)   -> get_type_from_lambda var_name expr b_lst
    | _ -> ""
    
and
  get_type_from_channel var_name name param_lst = 
    let lst = List.map (fun (_,x) -> 
      if ((Csp2string.expr2string x) = (Csp2string.string2string var_name)) then "x" else "") param_lst
    in
    try
      let (_,prototype) = List.find (fun (x,_)-> x=name) !cchan_lst in
      let prototype = List.map (fun x -> match x with 
        Csptypes.String x -> x
    | _ -> "SET") prototype in

      let lst1 = List.map2 (fun x->fun y-> if x = "" then "" else y) lst prototype in
      let s1 = List.fold_left (^) "" lst1 in
      if (s1 <> "") then
        s1
      else
        let lst3 = List.map (fun (_,x)-> get_type_from_expr var_name x) param_lst in
        List.fold_left (^) "" lst3
    with _ ->
      raise (Error ("[trait_param] : il n'existe pas le canal [" ^ name ^ "]"))
      

and
  get_type_from_expr_list var_name expr_lst = 
    
    let lst = List.map (get_type_from_expr var_name) expr_lst in
    List.fold_left (^) "" lst
    
and
  get_type_from_expr var_name expr =
      if (is_in_expr var_name expr) then
        get_type_of_expr var_name expr 
      else 
        ""
      
and
  get_type_of_expr var_name expr = match expr with
      Csptypes.String s  ->  (get_type_from_definition s)
    | Csptypes.Set s -> print_string (var_name) ; "SET"
    | Csptypes.Bool b -> "BOOL" 
    | Csptypes.Int i  -> "NAT"
      
    | Csptypes.ExprParen e  ->  (get_type_of_expr var_name e)
    
    | Csptypes.ExprBin1 (s,e1,e2) 
    | Csptypes.ExprBin2 (e1,s,e2) ->
      begin
        match s with 
        "union" | "inter" | "diff" | "Union" | "Inter" | "member" | "card" | "empty" | "set" | "Set" | "Seq"
                  -> "SET"
        | "+" | "-" | "*" | "/" | "%" | ">" | "<" | ">=" | "<=" -> "NAT"                        
        | _ -> 
                                                           
      let v_n = (Csp2string.string2string var_name) and
        ss1 = (Csp2string.expr2string e1) and
          ss2 = (Csp2string.expr2string e2) in
      if v_n = ss1 then 
        begin 
        get_type_from_definition ss2 end
      else
        get_type_from_definition ss1
      end
          
    | Csptypes.ExprUn (s,e) -> 
          begin
          match s with 
          | "-" -> "NAT"
          |"not" -> "BOOL"                                 
          | _ -> "NAT"
            end
            
        
      
and
  get_type_from_definition s = 
    let lst = List.map (fun x -> match x with Csptypes.Datatype p -> [p] | _ -> []) (!cdef_lst) in
    let data_type_lst = List.fold_left List.append [] lst in 

    let get_type_from_data_type s (name,param) =
      if (Outil.isExiste (fun x -> (Csp2string.string2string x) = s) param) then
        name
      else
          ""
    in
    let lst1 = List.map (get_type_from_data_type s) data_type_lst in
    List.fold_left (^) "" lst1
    
and
  get_type_from_if var_name cond b1_lst b2_lst = 
    let s = (get_type_from_expr var_name cond) in
      if s <> "" then
        s
      else
        let s1 = get_type_from_body var_name b1_lst in
        if s1 <> "" then
          s1
        else
          get_type_from_body var_name b2_lst
          
and
  get_type_from_lambda var_name expr b_lst = 
    let s = (get_type_from_expr var_name expr) in
      if s <> "" then
        s
      else
        get_type_from_body var_name b_lst
        
        
;;
(*-----------------------------------------------------------------------*)
(*-----------------------VARIABLES MODIFIED------------------------------*)
(*-----------------------------------------------------------------------*)

let  rec
  is_modify_in_body var_name body = match body with
      []   -> false
    | h::t -> let s = (is_modify_in_ope var_name h) in
      if s then
        s
      else
        is_modify_in_body var_name (List.tl body)
        
and  
  is_modify_in_body_list var_name body_lst = match body_lst with
      []   -> false
    | h::t -> let s = (is_modify_in_body var_name h) in
      if s then
        s
      else
        is_modify_in_body_list var_name (List.tl body_lst)
        
and
  is_modify_in_ope var_name ope = match ope with
      Csptypes.BodyParen b_lst              -> (is_modify_in_body var_name b_lst)  
    | Csptypes.ChannelCall (name, param_lst)-> is_modify_in_channel var_name name param_lst
    | Csptypes.ProcessCall (name, expr_lst) -> is_modify_in_expr_list var_name expr_lst
    | Csptypes.Choice b_lst_lst             -> is_modify_in_body_list var_name b_lst_lst
    | Csptypes.If (cond, b1_lst, b2_lst)    -> is_modify_in_if var_name cond b1_lst b2_lst      
    | Csptypes.Lambda (name, expr, b_lst)   -> is_modify_in_lambda var_name expr b_lst
    | _ -> false
and
  is_modify_in_channel var_name name param_lst = 
    false
    
and
  is_modify_in_expr_list var_name expr_lst =
    let lst = List.map (is_modify_in_expr var_name) expr_lst in
    List.fold_left (fun x->fun y-> x || y) false lst
    
and
  is_modify_in_expr var_name expr = match expr with
      Csptypes.ExprParen e  ->  (is_modify_in_expr var_name e)
    | Csptypes.ExprBin1 (s, expr1, expr2) -> is_in_expr var_name (Csptypes.ExprBin1 (s, expr1, expr2))      
    | Csptypes.ExprBin2 (expr1, s, expr2) -> is_in_expr var_name (Csptypes.ExprBin2 (expr1, s, expr2))
    | Csptypes.Set set -> false
    | _  -> false
   
    
    
and
  is_modify_in_if var_name cond b1_lst b2_lst =
    if (is_modify_in_expr var_name cond) then
      true
    else if (is_modify_in_body var_name b1_lst) then
      true
    else
      (is_modify_in_body var_name b2_lst)
      
    
and
  is_modify_in_lambda var_name expr b_lst = 
    if (is_modify_in_expr var_name expr) then
      true
    else
      (is_modify_in_body var_name b_lst)
    
;;

  
