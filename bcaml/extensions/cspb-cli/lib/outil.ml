(*
    Copyright (c) 2009 Huu Nghia Nguyen

    This file is part of the cspb-cli tool integrated in BRILLANT.

    cspb-cli is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cspb-cli is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cspb-cli.  If not, see <http://www.gnu.org/licenses/>.
*)

  (**
   * nhnghia
   * Les outils 
   *)
let isExiste f lst = List.fold_right (fun x -> fun b -> (f x) || b) lst false
;;

let list2string spc funx lst = 
  let lst1 = List.map (fun x -> spc ^ (funx x)) lst in
  List.fold_left (^) "" lst1
;;
