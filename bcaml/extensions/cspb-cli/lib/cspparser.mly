/*
    Copyright (c) 2009 Huu Nghia Nguyen

    This file is part of the cspb-cli tool integrated in BRILLANT.

    cspb-cli is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cspb-cli is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cspb-cli.  If not, see <http://www.gnu.org/licenses/>.
*/

 %{

 open Csptypes;;

 %}

 %token <int> INT
 %token <string> IDENT
 %token <string> DIVER
 %token <string> STR
 %token STR COLON COMMA
 %token EQUAL POINT LPARE RPARE QUEST EXCLA
 %token FOLLO CHOIC PARAL
 %token LBRAC RBRAC VERTI
 
 %token <string>RELAT_1
 %token <string>RELAT_2
 %token <bool>BOOL
 %token LOWER LOWEQ SUPER SUPEQ EQUAQ AND OR
 
 %token IF THEN ELSE END
 %token INCLUDE CHANNEL DATATYPE NAMETYPE
 %token ASSERT SLASH DIVERGENCE_FREE DEADLOCK_FREE
 
 %token LAMDA INTERLEAVING
   
 %token LCHAN RCHAN GROUPE_CHANNEL
   
 %token EOF
 %token STOP SKIP


 %left STR INT
 %left IDENT
 
   
 %right EQUAL VERTI POINT QUEST EXCLA COMMA
 %right FOLLO PARAL 

 %right INCLUDE CHANNEL DATATYPE NAMETYPE
 %right RESULT
 
 %right RELAT_1 RELAT_2 CHOIC
 
 %left LCHAN SLASH
 %right RCHAN
   
 %start csp
 %type <Csptypes.csp list> csp

 %%


csp : 
     includes             csp  { (Include $1) :: $2 }
   | definition           csp  { (Definition $1) :: $2 }
   | process              csp  { (Process $1) :: $2 }
   | process_groupes      csp  { $2 }
   | assertion            csp  { $2 }
   | EOF { [] }
;

includes :
    INCLUDE STR                                { $2 }
;


definition :
    DATATYPE IDENT EQUAL datatype_params  { Datatype ($2,$4) }
  | NAMETYPE IDENT EQUAL expr             { Nametype ($2,$4) }
  | IDENT EQUAL expr                      { Constant ($1,$3) }
  | channels        { Channel  $1 }
  | groupe_channels { $1}

;
datatype_params :
    IDENT                          { [$1] }
  | IDENT VERTI datatype_params    { $1::$3 }
;

channels :
    CHANNEL IDENT                       { ($2,[]) }
  | CHANNEL IDENT COLON channel_params  { ($2,$4) }
;
channel_params :
    expr                                { [$1] }
  | expr POINT channel_params_detail    { ($1)::$3 }
;
channel_params_detail :
    expr                                { [$1] }
  | expr POINT channel_params_detail    { ($1) :: $3 }
;

groupe_channels :
   IDENT EQUAL LBRAC VERTI process_params VERTI RBRAC  { GroupeChannel($1, $5) }
;

assertion :
      ASSERT IDENT         DIVERGENCE_FREE       { [] }
    | ASSERT process_calls DIVERGENCE_FREE       { [] }
    | ASSERT IDENT         DEADLOCK_FREE         { [] }
    | ASSERT process_calls DEADLOCK_FREE         { [] }
    | ASSERT process_calls SLASH IDENT DIVERGENCE_FREE       { [] }
    | ASSERT process_calls SLASH IDENT DEADLOCK_FREE         { [] }
;

process :
    IDENT EQUAL process_body { ($1, [],$3) }
  | IDENT LPARE process_params RPARE EQUAL process_body { ($1,$3,$6) }
;
process_params : 
    IDENT                       { [$1] }
  | IDENT COMMA process_params  { $1 :: $3 }
;

process_body :
    choice_expr   { [ (Choice $1) ] }
  | body_expr     { $1 }
;
body_expr :
    LPARE process_body RPARE           { [ (BodyParen $2) ] }
  | STOP {[Funx "skip"]}
  | SKIP {[Funx "skip"]}
  | channel_calls FOLLO process_body   { $1 :: $3 }
  | process_calls                      {  $1  }
  | IF expr THEN process_body ELSE process_body  { [ (If ($2,$4,$6)) ] }
  | expr AND process_body                        { [ (If ($1,$3,[])) ] }  
  | lambdas        { $1 }
;
choice_expr :
  | body_expr CHOIC body_expr   { $1::[$3] }
  | body_expr CHOIC choice_expr { $1::$3 }
;
channel_calls :
    IDENT                     { (ChannelCall($1, [])) }
  | IDENT channel_call_params { (ChannelCall($1, $2)) }
;
channel_call_params :
      QUEST IDENT                    { [(OUT, String $2)] }   
  | QUEST IDENT COLON expr         { [(OUT, ExprBin2((String $2),"and", ExprBin1("member", (String $2),$4)))] }
    | EXCLA expr                     { [(IN,  $2)] }
    | POINT expr                     { [(ID,  $2)] }
    | QUEST IDENT channel_call_params  { (OUT, String $2) :: $3 }
    | EXCLA expr   channel_call_params { (IN, $2) :: $3 }
    | POINT expr   channel_call_params { (ID, $2) :: $3 }
;

process_calls :
    IDENT                      { [ (ProcessCall($1,[])) ] }
  | IDENT LPARE process_call_params RPARE  { [ (ProcessCall($1, $3)) ] }
;
process_call_params :
     expr                           { [$1] }
   | expr COMMA process_call_params { $1 :: $3 }
;


expr :
   IDENT                        { (String $1) }
 | INT                          { (Int $1 ) }
 | BOOL                         { (Bool $1) }
 | set                          { Set $1 }
 | LPARE expr RPARE              { ExprParen  $2 }
 | EXCLA expr                    { ExprUn ("!", $2) }
 | RELAT_1 expr                  { ExprUn ($1, $2) }
 | expr RELAT_2 expr             { ExprBin2 ($1, $2, $3) } 
 | RELAT_2 LPARE expr COMMA expr RPARE  { ExprBin1 ($1, $3 , $5) } 
;
set :
    IDENT                             { SetName $1 }
  | LBRAC RBRAC                       { (SetEnum []) }
  | LBRAC expr POINT POINT expr RBRAC { SetBound ($2,$5) }
  | LBRAC set_params RBRAC            { SetEnum ($2) }
;
set_params :
    expr            { [$1] }
  | expr COMMA set_params { $1 :: $3 }
;

lambdas :
  INTERLEAVING IDENT COLON expr LAMDA process_body    { [ (Lambda($2, $4, $6)) ] }
;


process_groupes :
    IDENT EQUAL IDENT LCHAN IDENT RCHAN IDENT { [] }
  | process_header EQUAL process_header LCHAN IDENT RCHAN process_header { [] }
                | IDENT EQUAL IDENT GROUPE_CHANNEL IDENT { [] }
                | IDENT EQUAL IDENT SLASH set {[]}
;
;
process_header : 
    IDENT {[]}
  | IDENT LPARE process_params RPARE {[]}

  

