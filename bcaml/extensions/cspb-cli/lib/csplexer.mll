(*
    Copyright (c) 2009 Huu Nghia Nguyen

    This file is part of the cspb-cli tool integrated in BRILLANT.

    cspb-cli is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cspb-cli is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cspb-cli.  If not, see <http://www.gnu.org/licenses/>.
*)

{
  open Cspparser;;
  let string_chars s = String.sub s 1 ((String.length s)-2) ;;
 }
 
 rule token = parse
     [' ' '\t' '\n']       { token lexbuf }
   | "--"   { line_comment lexbuf }
   | "{-"   { block_comment lexbuf}
   | ['0'-'9']+ as lxm { INT (int_of_string lxm) }
   | ['+' '-' '*' '/' '%' ]  { RELAT_2 (Lexing.lexeme lexbuf) }
   | ['<' '>' ]  { RELAT_2 (Lexing.lexeme lexbuf) }
   | "!="
   | "=="        { RELAT_2 (Lexing.lexeme lexbuf) }
   | "<="        { RELAT_2 (Lexing.lexeme lexbuf) }
   | ">="        { RELAT_2 (Lexing.lexeme lexbuf) }
   | '('  { LPARE }
   | ')'  { RPARE }
   | '.'  { POINT }
   | '?'  { QUEST }
   | '!'  { EXCLA }
   | '='  { EQUAL }   
   | '&'  { AND }
   | "->" { FOLLO }
   | "[]" { CHOIC }
   | '{'  { LBRAC }
   | '}'  { RBRAC }
   | '|'  { VERTI }
   | ','  { COMMA }
   | ':'  { COLON }
   | "[|" { LCHAN }
   | "|]" { RCHAN }
   | "\\" { SLASH }
   | "STOP" {STOP}
   | "SKIP" {SKIP}
   | '"' [^ '"']* '"'  { STR (string_chars (Lexing.lexeme lexbuf)) }
   | "include"         { INCLUDE }
   | "datatype"        { DATATYPE }
   | "nametype"        { NAMETYPE }
   | "channel"		     { CHANNEL }
   | "assert"	         { ASSERT }
   
   | "union"           { RELAT_2 (Lexing.lexeme lexbuf) }
   | "inter"           { RELAT_2 (Lexing.lexeme lexbuf) }
   | "diff"            { RELAT_2 (Lexing.lexeme lexbuf) }
   | "member"          { RELAT_2 (Lexing.lexeme lexbuf) }
   | "Union"         { RELAT_1 (Lexing.lexeme lexbuf) }
   | "Inter"         { RELAT_1 (Lexing.lexeme lexbuf) }
   | "card"          { RELAT_1 (Lexing.lexeme lexbuf) }
   | "empty"         { RELAT_1 (Lexing.lexeme lexbuf) }
   | "set"           { RELAT_1 (Lexing.lexeme lexbuf) }
   | "Set"           { RELAT_1 (Lexing.lexeme lexbuf) }
   | "Seq"           { RELAT_1 (Lexing.lexeme lexbuf) }
   
   | "not"           { RELAT_1 (Lexing.lexeme lexbuf) }
   | "OR" | "or" { RELAT_2 (Lexing.lexeme lexbuf) }
   | "AND"| "and"{ RELAT_2 (Lexing.lexeme lexbuf) }
   
   | ":[ deadlock free [F] ]"     { DEADLOCK_FREE }
   | ":[ divergence free ]"       { DIVERGENCE_FREE }
   | "[|" ['A'-'z' '0'-'9' ' ' ',']+ "|]" { GROUPE_CHANNEL }
   | "|||" | "|~|"                    { INTERLEAVING }
   | "@"               {LAMDA}
   | "TRUE" | "true"   { BOOL true }
   | "FALSE" | "false" { BOOL false }
   | "IF" | "if"       { IF }
   | "THEN" | "then"   { THEN }
   | "ELSE" | "else"   { ELSE }
   | "END"  | "end"    { END }
   | ['A'-'z''0'-'9']+ { IDENT (Lexing.lexeme lexbuf)}
   
   
   | _    {DIVER (Lexing.lexeme lexbuf)}
   | eof { EOF }
   
and block_comment = parse
    "-}"  { token lexbuf   }
  | eof   {Error.error "unterminated comment"}
  | _     { block_comment lexbuf }
  
and line_comment = parse
    '\n'  { token lexbuf   }
  | eof   { EOF}
  | _     { line_comment lexbuf }

