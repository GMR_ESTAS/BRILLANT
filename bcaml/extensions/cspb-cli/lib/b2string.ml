(*
    Copyright (c) 2009 Huu Nghia Nguyen

    This file is part of the cspb-cli tool integrated in BRILLANT.

    cspb-cli is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cspb-cli is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cspb-cli.  If not, see <http://www.gnu.org/licenses/>.
*)

open Blast;;

let rec
  list2string spc func lst = match lst with
    | [t]  -> (func t)
    | h::t -> ((func h) ^ spc ^ (list2string spc  func t))
    | []   -> ""
;;

let rec predicat2string pred =  match pred with
    PredParen(p) -> ("(" ^ (predicat2string p) ^ ")" )

  | PredBin(op,pred1,pred2) ->
      (predicat2string pred1) ^ " " ^  (Bbop.bop2_ascii op) ^ " " ^   (predicat2string pred2)

  | PredAtom(op,expr1,expr2) ->
      (expr2string expr1)  ^ " " ^  (Bbop.bop2_ascii op) ^ " " ^   (expr2string expr2)

  | PredNegation(pred) -> ("not(" ^ (predicat2string pred) ^ ")")

  | PredExists(ids1,pred) -> "PredExists"

  | PredForAll(ids1,pred) -> "!(" ^ (list2string ", " id2string ids1) ^ ").(" ^ (predicat2string pred) ^ ")\n"

  
  | SubstApply(subst, pred) -> "PredApply"


(* FIXME: code duplication with some parts of b2pred.ml *)
and expr2string expression = match expression with
      ExprUn(op,expr1) ->
	 (Bbop.op1_ascii op) ^ " " ^ (expr2string expr1)

    | ExprBin(op,expr1,expr2) ->
	 (expr2string expr1) ^ " " ^ (Bbop.op2_ascii op) ^ " " ^ (expr2string expr2)

    | ExprParen(expr1) ->
	("(" ^ (expr2string expr1) ^ ")")

    | ExprId(id1) ->
	(id2string id1)

    | ExprSeq(seq) ->
	(exprSeq2string seq)

    | ExprSet(set) ->
	(exprSet2string set)

    | ExprNumber(num) ->
	(number2string num)

    | ExprBoolConstant(bcst) ->
	(boolConstant2string bcst )

    | ExprBool(pred1) ->
	(predicat2string pred1)

    | ExprString(str) ->
	str


   | ExprNuplet (expr_lst) -> "ExprNuplet"
   | ExprSIGMA (id_lst, pred, expr) -> "ExprSIGMA"
   | ExprPI (id_lst,pred, expr) -> "ExprPI"
   | ExprLambda (id_lst, pred, expr) -> "ExprLambda"

   | ExprFunCall (expr1, expr2) -> ((expr2string expr1) ^ "(" ^ (expr2string expr2) ^ ")")

   | ExprRecords(_) -> "ExprRecords"
   | ExprTrin(_,_,_,_) -> "ExprTrin"
   | ExprBefore(id) -> (id2string id) ^ "$0"

and  
  id2string ident =   match ident with
      Id(path) -> (Id_acc.name_of ident)
    | IdTy(id2,_) -> id2string id2 

and
  exprSeq2string seq =  match seq with
      SeqEnum(exprlist) ->
	"{" ^ (list2string ", "  expr2string exprlist) ^"}"
    | SeqEmpty ->
	"{}"

and  
  number2string num =  match num with
      MinNumber ->  "MININT" 
    | MaxNumber ->  "MAXINT"
    | Number(n) -> (Int32.to_string n)

and  
  exprSet2string set =  match set with
      SetPredefined(setName1) ->
	(setName2string setName1)

    | SetEnum(expr_lst) -> ("{" ^ (list2string "," expr2string expr_lst) ^ "}")

    | SetComprPred(ids1,pred) -> "setComprRed"

    | SetEmpty ->  "{}"

    | SetUnionQ(ids1,pred,expr) -> "setUnionQ"

    | SetInterQ(ids1,pred,expr) -> "setInterQ"

    | SetRecords(_) -> "SetRecords"
and
  setName2string n =  match n with
	NAT ->  "NAT"
      | NATURAL -> "NATURAL"
      | NAT1 -> "NAT1"
      | NATURAL1 -> "NATURAL1"
      | INTEGER -> "INTEGER"
      | INT -> "INT"
      | STRING -> "STRING"
      | BOOL -> "BOOL"

and
  boolConstant2string b =  match b with 
      TrueConstant ->  "TRUE"
    | FalseConstant -> "FALSE"


