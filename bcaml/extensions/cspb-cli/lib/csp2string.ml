(*
    Copyright (c) 2009 Huu Nghia Nguyen

    This file is part of the cspb-cli tool integrated in BRILLANT.

    cspb-cli is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cspb-cli is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cspb-cli.  If not, see <http://www.gnu.org/licenses/>.
*)


exception Error of string 
let csp_process_prototype = ref ([] : Csptypes.process list);;
let channel_communication_prototype = ref ([] : Csptypes.channel list);;

let rec
  process_operation_list b_lst =
    if (b_lst = []) then
      ["SELECT TRUE=FALSE THEN skip END\n"]
    else
      let lst = List.map process_operation b_lst in
      let lst1 = List.fold_left List.append [] lst in
      
      let spc = ref "" and
        end_lst = ref ([] : string list) in
      
      let trait_any str = 
        if (Str.string_match (Str.regexp ".*ANY_THEN") str 0) then
          begin
            end_lst := (!spc ^ "   END\n") :: (!end_lst);            
            let str = (!spc ^ (Str.global_replace (Str.regexp "ANY_THEN") "THEN" str)) in
              spc := !spc ^ "   ";
                str
          end
        else
          if (Str.string_match (Str.regexp ".*VAR_IN") str 0) then
          begin
            end_lst := (!spc ^ "   END\n") :: (!end_lst);
            let str = (!spc ^ (Str.global_replace (Str.regexp "VAR_IN") "VAR" str)) in
            spc := !spc ^ "   ";
              str
          end
        else
          (!spc ^ str)
      in      
      let lst2 = List.map trait_any lst1 in
      (List.append lst2 !end_lst)
      
and  
  process_operation ope =  
  let body2string = match (ope) with
        Csptypes.BodyParen b_lst              -> body_paren b_lst  
      | Csptypes.ChannelCall (name, param_lst)-> channel_call name param_lst
      | Csptypes.ProcessCall (name, expr_lst) -> process_call name expr_lst
      | Csptypes.Choice b_lst                 -> choice b_lst
      | Csptypes.If (cond, b1_lst, b2_lst)    -> if2string cond b1_lst b2_lst
      | Csptypes.Lambda (name, expr, b_lst)   -> lambda name expr b_lst 
      | Csptypes.Funx s                       -> [(string2string s) ^ "\n"]
  in  
  let spc = "   " in
  (List.map (fun x-> (spc ^ x)) body2string)
    
and
  body_paren b_lst =
    let lst = ("BEGIN\n" :: (process_operation_list b_lst)) in
    (List.append lst ["END\n"])

and
  channel_call name param_lst = 
    try
      let (_, prototype) = List.find (fun (x,_) -> x = name) ! channel_communication_prototype in
      (channel_communication_call param_lst prototype)
    with _ -> (channel_machine_call name param_lst)
      
and
  channel_communication_call param_lst prototype = 
    let prototype = List.map expr2string prototype in
    let lst = List.map (fun x -> match x with (Csptypes.OUT, expr) -> [expr] | _ -> []) param_lst in 
    let lst1 = List.fold_left List.append [] lst in
    if lst1 = [] then []
    else        
      let lst11 = List.map (fun x-> match x with 
        Csptypes.ExprBin2((Csptypes.String s),"and",_) -> string2string s
    | _ -> (expr2string x)) lst1 in
      
      let any = "ANY " ^ (B2string.list2string ", " (fun x->x) lst11) ^ "\n" in
      
      let lst2 = List.map2 (fun x -> fun y -> 
        (match x with (Csptypes.OUT, expr) -> 
          begin
             match expr with 
               Csptypes.ExprBin2((Csptypes.String s),"and",ex) -> ((string2string s) ^ " : " ^ y) :: [expr2string ex]
              | _ ->  [((expr2string expr) ^ " : " ^ y)]
            end
          | _ -> [])
                          ) param_lst prototype in
      let lst3 = List.fold_left List.append [] lst2 in
      let where = "WHERE " ^ (B2string.list2string " & " (fun x -> x) lst3) ^ "\n" in      
      any :: where :: ["ANY_THEN\n"]
    
and
  channel_machine_call name param_lst =
    let lst = List.map (fun x -> match x with (Csptypes.OUT, expr) -> [expr] | _ -> []) param_lst in
    let out = List.fold_left List.append [] lst in
    let lst = List.map (fun x -> match x with (Csptypes.IN, expr) -> [expr] | _ -> []) param_lst in
    let expr_in = List.fold_left List.append [] lst in
    
    if (out <> []) && (expr_in <> []) then
      let str_out = B2string.list2string ", " expr2string out and
      str_in = B2string.list2string ", " expr2string expr_in in
      ("VAR_IN " ^ str_out ^ " IN\n") ::
      [str_out ^ " <-- " ^ name ^ "(" ^ str_in ^ ");\n"]
    else if (expr_in <> []) then
      let str = B2string.list2string ", " expr2string expr_in in
      [name ^ "(" ^ str ^ ");\n"]
    else if (out <> []) then
       let str = B2string.list2string ", " expr2string out in
      ("VAR_IN " ^ str ^ " IN\n") ::
      [str ^ " <-- " ^ name ^ ";\n"]
    else
      [name ^ ";\n"]
    
    
and
  process_call name expr_lst =
    try
      let index = ref (-1) in
      let find_prototype (n,a,b) = 
        index := !index + 1;
          if (n = name) then true else false
      in      
      let (_,prototype, _) = List.find find_prototype !csp_process_prototype in
      
      let trait_substitution x y =
        let s1 = (string2string x) and s2 = (expr2string y) in
        if (s1 <> s2) then
          [( s1 ^ " := " ^ s2 ^ ";\n")]
        else
          [("/*" ^ s1 ^ " := " ^ s2 ^ ";*/\n")]
      in
      let lst  = List.map2 trait_substitution prototype expr_lst in
      let lst2 = List.fold_left List.append [] lst in
      (*print_string name;
      List.iter (fun x->print_string (expr2string x)) expr_lst;
      List.iter (fun x->print_string (", " ^ x)) prototype;
      print_string "\n";*)
      List.append lst2 ["cb := " ^ (string_of_int !index) ^ "\n"]
      
    with not_found ->
      raise (Error ("il n'existe pas le processus csp [" ^ name ^ "]"))
    
    
and
  choice b_lst_lst  = 
    let lst1 = List.map process_operation_list b_lst_lst in
    let lst2 = List.map (fun x-> "OR\n" :: x) lst1 in
    let lst3 = List.fold_left List.append [] lst2 in
    let lst4 = "CHOICE\n" :: (List.tl lst3) in
    (List.append lst4 ["END\n"])
    
and
  if2string cond b1_lst b2_lst  =
    let str = ("IF " ^ (expr2string cond) ^ "\n") :: ["THEN\n"] in
    let str1 = List.append str (process_operation_list b1_lst) in
    if (b2_lst = []) then
      (List.append str1 ["END\n"])
    else      
      let str2 = "ELSE\n" :: (process_operation_list b2_lst) in
      let str3 = List.append str1 str2 in
      (List.append str3 ["END\n"])    
      
and
  lambda str expr b_lst  =
    let id = (string2string str) in
    let str = ("ANY " ^ id ^ "\n") :: 
      ("WHERE   " ^ id ^ " : " ^ (expr2string expr) ^ "\n") :: ["THEN\n"] in
    let str1 = List.append str (process_operation_list b_lst) in
    (List.append str1 ["END\n"])
    
and
  expr2string expr = match expr with
      Csptypes.ExprParen expr -> ("(" ^ (expr2string expr) ^ ")") 
    | Csptypes.String  str    -> (string2string str)
    | Csptypes.Int int        -> (string_of_int int)
    | Csptypes.Bool bool      -> if bool then "TRUE" else "FALSE"
    | Csptypes.Set set        -> set2string set
    | Csptypes.ExprBin1  (str, expr1, expr2) -> 
      let e1 = (expr2string expr1) and e2 = (expr2string expr2) in
      begin
        match str with
                  "member" -> "(" ^ e1 ^ " : " ^ e2 ^ ")"
       |  "union" -> "(" ^ e1 ^ " \\/ " ^ e2 ^ ")"
       |  "diff" -> "(" ^ e1 ^ " - " ^ e2 ^ ")"
       | _ ->
      ((string2string str) ^ "(" ^ e1 ^"," ^ e2 ^")")
      end
    | Csptypes.ExprBin2 (expr1, str, expr2) ->
      ( (expr2string expr1) ^ " " ^ (string2string str) ^ " " ^ (expr2string expr2))
    | Csptypes.ExprUn (str, expr) -> 
      ( (string2string str) ^ "(" ^ (expr2string expr) ^ ")" )

and
  set2string set = match set with
      Csptypes.SetName str             -> (string2string str)
    | Csptypes.SetEnum  (expr_lst)     -> 
      ("{" ^  B2string.list2string ", " expr2string expr_lst ^ "}")
    | Csptypes.SetBound (expr1, expr2) -> 
      ("(" ^ (expr2string expr1) ^ " .. " ^ (expr2string expr2) ^ ")")

and
  string2string str = match str with
      "id"   -> "Id"
    | "bool" -> "BOOL"
    | "=="   -> "="
    | "!="   -> "/="
    | "and"  -> "&"
    | "%"    -> "mod"
    | _      -> if (String.length str) = 1 && str >= "A" && str <= "z" then (str ^ str) else str
