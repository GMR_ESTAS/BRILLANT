(*
    Copyright (c) 2009 Huu Nghia Nguyen

    This file is part of the cspb-cli tool integrated in BRILLANT.

    cspb-cli is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cspb-cli is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cspb-cli.  If not, see <http://www.gnu.org/licenses/>.
*)



open Blast

exception Error of string;;

let rec
  list2string spc func lst = match lst with
    | [t]  -> (func t)
    | h::t -> ((func h) ^ spc ^ (list2string spc  func t))
    | []   -> ""
;;

let global_operations_list = ref ([])
;;

let rec get_operation name  = 
  try

    List.find (fun (x,_,_,_) -> (Id_acc.name_of x) = (Id_acc.name_of name)) !global_operations_list

  with _ ->
    let str = ("Not found operator [" ^ (Id_acc.name_of name) ^ "]") in
(*    let print_ope_name (name, o, i, b) =
      print_string ("[" ^ (Id_acc.name_of name) ^ "] ")
    in
      List.iter print_ope_name !global_operations_list;
*)      raise (Error str)
;;      

(*******************************************************************************************************************)
(************************************ traduire une subtitution a` un predicate *************************************)
(*******************************************************************************************************************)

let rec  operation2predicat (name, ids_out, ids_in, body) =
  let lst = List.append ids_out ids_in in
  let pred = substitution2predicat body in
    if lst = [] then
      pred
    else
      PredForAll (lst, pred)

and 
  substitution2predicat sub  =  match sub with
 
   | SubstOperCall (id_lst, id, expr_lst) ->
      PredParen (operCall2predicat (id_lst, id, expr_lst))

   | SubstBlock substitution -> (substitution2predicat substitution)

   |  SubstPrecondition (pred, subst) ->
      PredParen (PredBin (Bbop.Implies, PredParen pred, PredParen (substitution2predicat subst)))

(*   | SubstAssertion of predicate * substitution *)

   | SubstChoice subst_lst -> choice2predicat subst_lst

   | SubstSelect (pred_sub_lst, sub_opt) 
   | SubstIf (pred_sub_lst, sub_opt) ->
       begin
       match sub_opt with
	 | None -> (if2predicat pred_sub_lst)
	 | Some(s) -> PredBin (Bbop.Ou, (if2predicat pred_sub_lst), (else2predicat pred_sub_lst s))
       end



(*   | SubstCase of expr * (expr list * substitution) list * substitution option
*)  
   | SubstAny(id_lst,pred,subst)
   | SubstLet (id_lst, pred, subst) ->
       PredParen (PredForAll(id_lst, PredBin(Bbop.Implies, PredParen(pred), PredParen((substitution2predicat subst)))))


   | SubstVar (id_lst, sub) -> PredParen (PredExists(id_lst, substitution2predicat sub))

(*   | SubstWhile of predicate * substitution * expr * predicate
*)

(*   | SubstSkip *)

   | SubstSequence (sub1, sub2) 
   | SubstParallel (sub1, sub2) -> PredBin(Bbop.And, (substitution2predicat sub1), (substitution2predicat sub2))

   | SubstSetEqualIds (id_lst, expr_lst) -> (setEqualIds2predicat id_lst expr_lst)

   | SubstSetEqualFun (_, _) -> 
       substitution2predicat (Subst_acc.to_SubstSetEqualIds sub)

(*   | SubstBecomeSuch (id_list, pred)
*)
   | SubstSetIn (id_lst, expr ) -> setIn2predicat id_lst expr

(*   | SubstInstanciation of id list * expr list * substitution
*)
  | _ -> PredAtom(Bbop.Equal, ExprBoolConstant TrueConstant, ExprBoolConstant TrueConstant)


and
  choice2predicat lst = match lst with
      h::[t] -> PredBin (Bbop.Ou, (substitution2predicat h), (substitution2predicat t))
    | h::t   -> PredBin (Bbop.Ou, (substitution2predicat h), (choice2predicat t))
    | [] -> invalid_arg "choice2predicat: choice with only one option"

and
  if2predicat pred_sub_lst = match pred_sub_lst with
      [pred, sub] -> PredParen (PredBin (Bbop.Implies, pred, substitution2predicat sub))
    | h::t        -> PredBin (Bbop.Ou, (if2predicat [h]), (if2predicat t))
    | [] -> invalid_arg "if2predicat: empty if_then list"

and
  else2predicat pred_sub_lst sub =
  begin    
  let rec get_pred lst = match lst with
      [] -> []
    | (pred, sub)::t  -> pred :: (get_pred t)
  in
  let rec lst_pred2predicat lst = match lst with
      [h]  -> h
    | h::t -> PredBin (Bbop.And, h, (lst_pred2predicat t))    
    | [] -> invalid_arg "lst_pred2predicat: empty list of predicates"
  in
  let pred_lst = (get_pred pred_sub_lst) in
  let pred = (lst_pred2predicat pred_lst) in
  let pred1 = PredNegation (pred) in
  let pred2 = PredBin(Bbop.Implies, pred1, substitution2predicat sub) in
    PredParen pred2
  end
    

and
  setEqualIds2predicat id_lst expr_lst =
  let lst = (List.combine id_lst expr_lst) in
  let rec get_pred lst =
    match lst with
	[(i,e)] -> PredAtom(Bbop.Equal, ExprId i, e)
      | h::t    -> PredBin (Bbop.And, get_pred [h], get_pred t)
      | [] -> invalid_arg "setEqualIds2predicat: empty list of (id, expr) couples"
  in
    get_pred lst

and 
  setIn2predicat id_lst expr = match id_lst with
      [h]  ->  PredAtom(Bbop.In, ExprId h, expr)
    | h::t ->  PredBin (Bbop.And, (setIn2predicat [h] expr), (setIn2predicat t expr))
    | [] -> invalid_arg "setIn2predicat: empty list of identifier"

and 
  operCall2predicat (id_lst, id, expr_lst)  = 
  let (n,o,i,b) = get_operation id in

  let rec get_pred lst =
    match lst with
	[(i,e)] -> PredAtom(Bbop.Equal, ExprId i, ExprId e)
      | h::t    -> PredBin (Bbop.And, get_pred [h], get_pred t)
      | [] -> invalid_arg "operCall2predicat: empty list of assignments"
  in

  let pred_body = substitution2predicat b in
  if (o <> []) && (id_lst <> []) && (i <> []) then
    begin
      let lst = (List.combine id_lst o) in
      let ids_out = (get_pred lst) and
	  ids_in  = (setEqualIds2predicat i expr_lst) in
      let pred    = PredBin(Bbop.And, ids_out, ids_in) in
	PredBin(Bbop.Implies, pred, pred_body)
    end
  else if (o <> []) && (id_lst <> []) then
    begin
      let lst = (List.combine id_lst o) in
      let ids_out = (get_pred lst) in
	PredBin (Bbop.Implies, ids_out, pred_body)
    end
  else if (i <> []) then
    begin
      let ids_in = (setEqualIds2predicat i expr_lst) in
	PredBin(Bbop.Implies, ids_in, pred_body)
    end
  else
  pred_body
;;


(*******************************************************************************************************************)
(************************************ representer un predicate sous un string  *************************************)
(*******************************************************************************************************************)


let rec predicat2string pred =  match pred with
    PredParen(p) -> ("(" ^ (predicat2string p) ^ ")" )

  | PredBin(op,pred1,pred2) ->
      (predicat2string pred1) ^ " " ^  (Bbop.bop2_ascii op) ^ " " ^   (predicat2string pred2)

  | PredAtom(op,expr1,expr2) ->
      (expr2string expr1)  ^ " " ^  (Bbop.bop2_ascii op) ^ " " ^   (expr2string expr2)

  | PredNegation(pred) -> ("not(" ^ (predicat2string pred) ^ ")")

  | PredExists(ids1,pred) -> "PredExists"

  | PredForAll(ids1,pred) -> "!(" ^ (list2string ", " id2string ids1) ^ ").(" ^ (predicat2string pred) ^ ")\n"

  
  | SubstApply(subst, pred) -> "PredApply"



and expr2string expression = match expression with
      ExprUn(op,expr1) ->
	 (Bbop.op1_ascii op) ^ " " ^ (expr2string expr1)

    | ExprBin(op,expr1,expr2) ->
	 (expr2string expr1) ^ " " ^ (Bbop.op2_ascii op) ^ " " ^ (expr2string expr2)

    | ExprParen(expr1) ->
	("(" ^ (expr2string expr1) ^ ")")

    | ExprId(id1) ->
	(id2string id1)

    | ExprSeq(seq) ->
	(exprSeq2string seq)

    | ExprSet(set) ->
	(exprSet2string set)

    | ExprNumber(num) ->
	(number2string num)

    | ExprBoolConstant(bcst) ->
	(boolConstant2string bcst )

    | ExprBool(pred1) ->
	(predicat2string pred1)

    | ExprString(str) ->
	str


   | ExprNuplet (expr_lst) -> "ExprNuplet"
   | ExprSIGMA (id_lst, pred, expr) -> "ExprSIGMA"
   | ExprPI (id_lst,pred, expr) -> "ExprPI"
   | ExprLambda (id_lst, pred, expr) -> "ExprLambda"

   | ExprFunCall (expr1, expr2) -> ((expr2string expr1) ^ "(" ^ (expr2string expr2) ^ ")")
   | ExprRecords(_) -> "ExprRecords"
   | ExprTrin(_,_,_,_) -> "ExprTrin"
   | ExprBefore(id) -> (id2string id) ^ "$0"


and  
  id2string ident =   match ident with
      Id(path) -> (Id_acc.name_of ident)
    | IdTy(id2,_) -> id2string id2 

and
  exprSeq2string seq =  match seq with
      SeqEnum(exprlist) ->
	"{" ^ (list2string ", "  expr2string exprlist) ^"}"
    | SeqEmpty ->
	"{}"

and  
  number2string num =  match num with
      MinNumber ->  "MININT" 
    | MaxNumber ->  "MAXINT"
    | Number(n) -> (Int32.to_string n)

and  
  exprSet2string set =  match set with
      SetPredefined(setName1) ->
	(setName2string setName1)

    | SetEnum(expr_lst) -> ("{" ^ (list2string "," expr2string expr_lst) ^ "}")

    | SetComprPred(ids1,pred) -> "setComprRed"

    | SetEmpty ->  "{}"

    | SetUnionQ(ids1,pred,expr) -> "setUnionQ"

    | SetInterQ(ids1,pred,expr) -> "setInterQ"

    | SetRecords(_) -> "SetRecords"

and
  setName2string n =  match n with
	NAT ->  "NAT"
      | NATURAL -> "NATURAL"
      | NAT1 -> "NAT1"
      | NATURAL1 -> "NATURAL1"
      | INTEGER -> "INTEGER"
      | INT -> "INT"
      | STRING -> "STRING"
      | BOOL -> "BOOL"

and
  boolConstant2string b =  match b with 
      TrueConstant ->  "TRUE"
    | FalseConstant -> "FALSE"







(*******************************************************************************************************************)
(************************************ get un predicate d'une operation*******  *************************************)
(*******************************************************************************************************************)


let rec get_list_operations_from_file file_name = 
  let b = (Bparser.amn Blexer.token (Lexing.from_channel (open_in (file_name)))) in
     match b with
         Machine ((ident, param),clause) ->  get_list_operations_from_clause clause
       | Refinement ((ident, param), id ,clause) -> get_list_operations_from_clause clause
       | _             -> []

and
  get_list_operations_from_clause clause = match clause with
      [] -> []
    | h::t -> begin
	match h with
	    Operations (lst) -> lst
	  | Sees (lst)
	  | Uses (lst) ->
	    begin
	      let lst_file = List.map ((fun x-> (id2string x) ^ ".mch") ) lst in
	      let lst_opes = List.map get_list_operations_from_file lst_file in
	      let opes = List.fold_left List.append [] lst_opes in
		(List.append (get_list_operations_from_clause t) opes)
	    end

	  | Includes (lst) ->
	      begin
		let get_ope (name, expr_lst) =
		  let file_name = ((id2string name) ^ ".mch") in
		    get_list_operations_from_file file_name
		in
		let lst_opes = List.map get_ope lst in
		let opes = List.fold_left List.append [] lst_opes in
		  (List.append (get_list_operations_from_clause t) opes)
	      end
		
	  | _ -> get_list_operations_from_clause t
	  end
	    

(* return un predicat (string) apres avoir execute l'operateur "name" dans la liste des operateurs "ope_lst"*)
let visit_operation (name,ids_out,ids_in, body) =
  let vars = operation2predicat (name, ids_out, ids_in, body) in
  let str = predicat2string vars in
(*  print_string ("\nOperation : " ^ (id2string name) ^ "\n" ^ str)
*)
   str
;;

let get_operation_from_name name =
  try

    List.find (fun (x,_,_,_) -> (Id_acc.name_of x) = name) !global_operations_list

  with _ ->
    let str = ("Not found operator [" ^ name ^ "]") in
      raise (Error str)
;;      
  

let get_predicat_from_operation_name name =
  let (n, o,i,b) = (get_operation_from_name name) in
    visit_operation (n,o,i,b)
;;

let traitment file_name = 
  let lst_opes = (get_list_operations_from_file  file_name) in
    global_operations_list := lst_opes;
(*    print_string "\n\n\n\n";
    let lst = List.map visit_operation lst_opes in ()
*)
;;

(*
let _ = 
  if Array.length (Sys.argv) <> 2 then
    print_string ("usage : " ^ Sys.argv.(0) ^ " filename\n")
  else 
  (
    traitment Sys.argv.(1) 
  )
;;


*)
