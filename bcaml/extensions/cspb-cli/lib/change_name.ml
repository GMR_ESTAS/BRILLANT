(*
    Copyright (c) 2009 Huu Nghia Nguyen

    This file is part of the cspb-cli tool integrated in BRILLANT.

    cspb-cli is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cspb-cli is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cspb-cli.  If not, see <http://www.gnu.org/licenses/>.
*)

(**
   * Changer le nom de variable dans un processus csp "body"
   * Retourne un processus avec le novel nom de variable
     *)
exception Error of string;;         

let  rec
  change_name_variable prefix cproc_lst = 
    let change_name (name, param_lst, body) = 
      
      let body = change_name_parametre_channel_in_body body in
      
      if param_lst = [] then 
        (name, param_lst, body)
      else 
        let p_lst = List.map (fun x -> (prefix ^ "_" ^x)) param_lst in
        let pa = List.map2 (fun x-> fun y-> (x,y)) param_lst p_lst in
        
        let rec trait_body (new_param_lst, body) = match new_param_lst with
          []   -> body
        | (var_name, new_name)::t -> 
          let b =  change_name_variable_in_body var_name new_name body in
          trait_body (t, b)
        in        
        let b = trait_body (pa,body) in
        (name, p_lst, b)
        
    in
    let proc = List.map change_name cproc_lst in
    proc
    
and  
  change_name_variable_in_body var_name new_name body = 
    List.map (change_name_variable_in_ope var_name new_name) body
        
and  
  change_name_variable_in_body_list var_name new_name body_lst = 
    let lst = List.map (change_name_variable_in_body var_name new_name)  body_lst in
    lst
        
and
  change_name_variable_in_ope  var_name new_name ope = match ope with
      Csptypes.BodyParen b_lst              
    -> let lst = (change_name_variable_in_body var_name new_name b_lst) in
     Csptypes.BodyParen lst
    
    | Csptypes.ProcessCall (name,param_lst) ->
      let p = List.map (change_name_variable_in_expr var_name new_name) param_lst in
      
      Csptypes.ProcessCall (name, p)
      
    | Csptypes.ChannelCall (name, param_lst)-> 
      change_name_variable_in_channel var_name new_name (name, param_lst)
      
    | Csptypes.Choice b_lst_lst 
      -> let lst = change_name_variable_in_body_list var_name new_name b_lst_lst in
      Csptypes.Choice lst
      
    | Csptypes.If (cond, b1_lst, b2_lst)  
      -> let lst1 = (change_name_variable_in_body var_name new_name b1_lst) and
        lst2 = (change_name_variable_in_body var_name new_name b2_lst) and
          ex = change_name_variable_in_expr var_name new_name cond in
        Csptypes.If (ex, lst1, lst2)
      
    | Csptypes.Lambda (name, expr, b_lst)   -> 
      let lst = (change_name_variable_in_body var_name new_name b_lst)  and
          ex = change_name_variable_in_expr var_name new_name expr in   
      Csptypes.Lambda (name, ex, lst)
    | _ -> ope
      
and
  change_name_variable_in_expr var_name new_name expr = match expr with
      Csptypes.String s -> 
       let ss = (if ((Csp2string.string2string s) = (Csp2string.string2string var_name)) then 
         new_name 
       else s) 
       in 
       Csptypes.String ss
    
    | Csptypes.ExprBin1 (s,e1,e2) -> 
      let ee1 = change_name_variable_in_expr var_name new_name e1 and
        ee2 = change_name_variable_in_expr var_name new_name e2 in
      Csptypes.ExprBin1 (s,ee1,ee2)
      
    | Csptypes.ExprBin2 (e1,s,e2) -> 
      let ee1 = change_name_variable_in_expr var_name new_name e1 and
        ee2 = change_name_variable_in_expr var_name new_name e2 in
      Csptypes.ExprBin2 (ee1, s,ee2)
      
    | Csptypes.ExprParen e -> let e1 = change_name_variable_in_expr var_name new_name e in
      Csptypes.ExprParen e1
    
    | Csptypes.ExprUn (s,e) -> let e1 = change_name_variable_in_expr var_name new_name e in
      Csptypes.ExprUn (s,e1)
      
    | Csptypes.Set s -> Csptypes.Set (change_name_variable_in_set var_name new_name s)
    | _ -> expr
    
and
  change_name_variable_in_channel var_name new_name (name, param_lst) = 
    let change_param var_name new_name (t,e) = match t with 
       Csptypes.OUT -> begin
         match e with
           Csptypes.ExprBin2((Csptypes.String s),"and",_) -> let e1 = change_name_variable_in_expr var_name new_name e in (t,e1)
    | _ -> (t,e)
      end
      | _ -> let e1 = change_name_variable_in_expr var_name new_name e in (t,e1)
    in
      let p = List.map (change_param var_name new_name) param_lst in
      Csptypes.ChannelCall (name, p)

and 
   change_name_variable_in_set var_name new_name s = match s with
      Csptypes.SetName s -> let ss = (if ((Csp2string.string2string s) = (Csp2string.string2string var_name)) then 
         new_name 
       else s) 
       in 
       Csptypes.SetName ss
     | Csptypes.SetBound (e1, e2) ->
       let ee1 = change_name_variable_in_expr var_name new_name e1 and
        ee2 = change_name_variable_in_expr var_name new_name e2 in
      Csptypes.SetBound (ee1, ee2)
     | Csptypes.SetEnum e_lst ->
       let ee_lst = List.map (change_name_variable_in_expr var_name new_name) e_lst in
       Csptypes.SetEnum ee_lst
        
and 
  change_name_parametre_channel_in_body body = match body with
    [] -> []
    | ope::sub_body ->
      begin
        match ope with
          Csptypes.ChannelCall (name,p_lst) -> (*print_string (name ^ ":  ");*)
          begin
            let rec trait_param param_lst body = match param_lst with
              [] -> ([],body)
              | (typ,pa) :: sub_param_lst -> 
                begin
                  match typ with
                    Csptypes.OUT -> 
                    let pa_name = 
                      match pa with
                        Csptypes.ExprBin2((Csptypes.String s),"and",_) -> Csp2string.string2string s
                        | _ -> (Csp2string.expr2string pa)
                    in
                      (*
let pa_name = Csp2string.expr2string pa in*)
                          
                      let n_pa_name = (name ^ "_" ^ pa_name) in (*print_string (n_pa_name ^ "\n");*)
                      let n_pa = change_name_variable_in_expr pa_name n_pa_name pa in
                      let n_pa = (typ, n_pa) in
                      let n_body = change_name_variable_in_body pa_name n_pa_name body in
                      let (n_sub_param_lst,nn_body) = trait_param sub_param_lst n_body in                    
                      (n_pa :: n_sub_param_lst ,nn_body)
                    
                    | _ -> let (n_sub_param_lst,n_body) = trait_param sub_param_lst body in
                           ((typ,pa)::n_sub_param_lst,n_body)
                end     
            in
            let (n_param_lst,n_body) = trait_param p_lst sub_body in
            let nn_body = change_name_parametre_channel_in_body n_body in
            (Csptypes.ChannelCall (name,n_param_lst)) :: nn_body
          end
          
          | Csptypes.BodyParen b_lst              
          -> let lst = (change_name_parametre_channel_in_body b_lst) in
           (Csptypes.BodyParen lst):: (change_name_parametre_channel_in_body sub_body)
          
          | Csptypes.Choice b_lst_lst 
            -> let lst = List.map change_name_parametre_channel_in_body b_lst_lst in
            (Csptypes.Choice lst):: (change_name_parametre_channel_in_body sub_body)
            
          | Csptypes.If (cond, b1_lst, b2_lst)  
            -> let lst1 = (change_name_parametre_channel_in_body b1_lst) and
              lst2 = (change_name_parametre_channel_in_body b2_lst)  in
             ( Csptypes.If (cond, lst1, lst2)):: (change_name_parametre_channel_in_body sub_body)
            
          | Csptypes.Lambda (name, expr, b_lst)   -> 
            let lst = (change_name_parametre_channel_in_body b_lst) in   
            (Csptypes.Lambda (name, expr, lst)) :: (change_name_parametre_channel_in_body sub_body)
          
          |_ -> ope::(change_name_parametre_channel_in_body sub_body)   
      end
      
