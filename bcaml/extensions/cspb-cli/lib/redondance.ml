(*
    Copyright (c) 2009 Huu Nghia Nguyen

    This file is part of the cspb-cli tool integrated in BRILLANT.

    cspb-cli is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cspb-cli is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cspb-cli.  If not, see <http://www.gnu.org/licenses/>.
*)

(**
 * nhnghia
 * eliminer les redondances dans un processus csp :
 * 
 *)

exception Error of string;;                                                                           
(*-----------------------------------------------------------------------*)
(*-----------------------VARIABLES MODIFIED------------------------------*)
(*-----------------------------------------------------------------------*)
  (**
                                                                
Une variable est necessaire quand elle apparait in expression de
- cannal de machine
- if
- process call
    *)  
let channel_communication_prototype = ref ([] : Csptypes.channel list);;
  
let  rec
  is_necessary_in_body var_name body = match body with
      []   -> false
    | h::t -> let s = (is_necessary_in_ope var_name h) in
      
      if s then
        s
      else
        is_necessary_in_body var_name (List.tl body)
        
and  
  is_necessary_in_body_list var_name body_lst = match body_lst with
      []   -> false
    | h::t -> let s = (is_necessary_in_body var_name h) in
      
      if s then
        s
      else
        is_necessary_in_body_list var_name (List.tl body_lst)
        
and
  is_necessary_in_ope var_name ope = match ope with
      Csptypes.BodyParen b_lst              -> (is_necessary_in_body var_name b_lst)  
    | Csptypes.ChannelCall (name, param_lst)-> is_necessary_in_channel var_name name param_lst
    | Csptypes.ProcessCall (name, expr_lst) -> 
      let trait_expr expr = 
        if (Csp2string.string2string var_name) = (Csp2string.expr2string expr) then
          begin 
            (*print_string (var_name ^ "-" ^ (Csp2string.expr2string expr) ^ ", ");*)
            true
          end
        else
          Variable.is_in_expr var_name expr
      in      
      let lst = List.map (trait_expr) expr_lst in
      let b = List.fold_left (fun x->fun y->x || y) false lst in
      b
    | Csptypes.Choice b_lst_lst             -> is_necessary_in_body_list var_name b_lst_lst
    | Csptypes.If (cond, b1_lst, b2_lst)    -> is_necessary_in_if var_name cond b1_lst b2_lst      
    | Csptypes.Lambda (name, expr, b_lst)   -> is_necessary_in_lambda var_name expr b_lst
    | _ -> false
      
and
  is_necessary_in_channel var_name name param_lst = 
    if Outil.isExiste (fun (x,_) -> x = name) ! channel_communication_prototype then
      false 
    else
      let lst = List.map (fun (_,x) -> x) param_lst in
      let lst1 = List.map (Variable.is_in_expr var_name) lst in
      let b = List.fold_left (fun x->fun y-> x || y) false lst1 in
       (*
      if b then begin
        print_string ("\nIs necessary in " ^ name ^ " : "); 
        List.iter (fun x->print_string (", " ^ (Csp2string.expr2string x))) lst;        
      end;
       *)
        b
    
and
  is_necessary_in_expr_list var_name expr_lst =
    let lst = List.map (is_necessary_in_expr var_name) expr_lst in
    List.fold_left (fun x->fun y-> x || y) false lst
    
and
  is_necessary_in_expr var_name expr = match expr with
    Csptypes.String s -> false
    | _ -> (Variable.is_in_expr var_name expr)
   
and
  is_necessary_in_if var_name cond b1_lst b2_lst =
    if (is_necessary_in_expr var_name cond) then
      true
    else if (is_necessary_in_body var_name b1_lst) then
      true
    else
      (is_necessary_in_body var_name b2_lst)
        
and
  is_necessary_in_lambda var_name expr b_lst = 
    
    if (is_necessary_in_expr var_name expr) then
      true
    else
    begin
      (is_necessary_in_body var_name b_lst)
    end
      
    
;;

(*-----------------------------------------------------------------------*)
(*-----------------------ELIMINER REDONDANCE-----------------------------*)
(*-----------------------------------------------------------------------*)

(*
Un cannal de communication est elimine quand ses variables son necessaires
*)
  
let  rec
  remove_redondance_in_body body = match body with
    [] -> []
    | h :: t -> let h1 = remove_redondance_in_ope h t in
      if h1 = [] then
        (remove_redondance_in_body t)
      else
        List.append h1 (remove_redondance_in_body t)
        
and  
  remove_redondance_in_body_list body_lst = 
    let lst = List.map remove_redondance_in_body  body_lst in
    lst
        
and
  remove_redondance_in_ope ope body = match ope with
      Csptypes.BodyParen b_lst              -> let lst = (remove_redondance_in_body b_lst) in
       if lst = [] then [] else [Csptypes.BodyParen lst]
      
    | Csptypes.ChannelCall (name, param_lst)-> remove_redondance_in_channel name param_lst body
      
    | Csptypes.Choice b_lst_lst             -> let lst = remove_redondance_in_body_list b_lst_lst in
      [Csptypes.Choice lst]
      
    | Csptypes.If (cond, b1_lst, b2_lst)  
      -> let lst1 = (remove_redondance_in_body b1_lst) and
        lst2 = (remove_redondance_in_body b2_lst) in
      if lst1 = lst2 then [Csptypes.BodyParen lst1]
        else [Csptypes.If (cond, lst1, lst2)]
      
    | Csptypes.Lambda (name, expr, b_lst)   -> let lst = (remove_redondance_in_body b_lst) in
      [Csptypes.Lambda (name, expr, lst)]
    
    | _ -> [ope]
      
and
  remove_redondance_in_channel name param_lst body = 
(*  
    let param_lst = List.map (fun (p,x)-> match x with
      Csptypes.ExprBin2((Csptypes.String s),"and",_) -> (p,(Csptypes.String s))
    | _ -> (p,x)) param_lst in
*)
    if Outil.isExiste (fun (x,_) -> x = name) ! channel_communication_prototype then      
      let lst1 = List.map (fun (_,x) -> match x with 
        Csptypes.ExprBin2((Csptypes.String s),"and",_) -> Csp2string.string2string s
    | _ -> (Csp2string.expr2string x)) param_lst in      
      let lst2 = List.map (fun x -> (is_necessary_in_body x body)) lst1 in
      (*print_string ("\n" ^ name ^ ": ");
      List.iter2 (fun x->fun y->print_string (y ^ " " ^ (string_of_bool x) ^ ", ")) lst2 lst1;
                                                *)                                         
      let b = List.fold_left (fun p q -> p || q) false lst2 in
      if (b = false) then
        (*let t = print_string ("\nBỏ " ^ name) in t;*) []
      else
        
        let lst3 = List.map2 (fun x->fun y-> (if y then x else (Csptypes.IN, (Csptypes.String "")))) param_lst lst2 in        
        [Csptypes.ChannelCall (name, lst3)]
    else
      [Csptypes.ChannelCall (name, param_lst)]
      
and
  remove_redondance_in_process (name, param_lst, body) = 
    let body1 = remove_redondance_in_body body in
    let lst = List.map (fun x -> (is_necessary_in_body x body1)) param_lst in
    (*print_string ("\nprocess : " ^ name);
                                                                                                                                     List.iter (fun x->print_string (", " ^ (string_of_bool x))) lst;*)
    (name,lst, body1)
    
and
  remove_redondance cproc_lst = 
    let prototype_lst = List.map remove_redondance_in_process cproc_lst in
    (*
    List.iter (fun (x,_,_) -> print_string (x ^ "\n")) prototype_lst;
    *)
    let rec trait_process_call_in_body body = 
      List.map trait_process_call_in_ope body
      
    
    and
      trait_process_call_in_ope ope =  match ope with
        Csptypes.BodyParen b_lst -> let lst = (trait_process_call_in_body b_lst) in
        Csptypes.BodyParen lst
        
      | Csptypes.Choice b_lst_lst -> let lst = List.map trait_process_call_in_body b_lst_lst in
        Csptypes.Choice lst
        
      | Csptypes.If (cond, b1_lst, b2_lst)  
        -> let lst1 = (trait_process_call_in_body b1_lst) and
          lst2 = (trait_process_call_in_body b2_lst) in
        Csptypes.If (cond, lst1, lst2)
        
      | Csptypes.Lambda (name, expr, b_lst)   -> let lst = (trait_process_call_in_body b_lst) in
        Csptypes.Lambda (name, expr, lst)
      
      | Csptypes.ProcessCall (n,p) -> Csptypes.ProcessCall (n,(trait_param n p )) 
      | _ -> ope
        
   and
      trait_param n p = 
        try
          let (_,pro,b) = List.find (fun (x,_,_)-> x=n) prototype_lst in
          let lst = List.map2 (fun x->fun y->if x then [y] else []) pro p in
          let lst1 = List.fold_left List.append [] lst in
          (lst1)
        with _ -> 
          print_string "Liste des processus : " ;
          List.iter (fun (x,_,_) -> print_string (x ^ "; ")) prototype_lst;
          print_string "\n";
          raise (Error ("[trait_param] : il n'existe pas le processus csp [" ^ n ^ "]"))
   and
      trait_proc (n,p,b) =
        try
          let (_,_,b) = List.find (fun (x,_,_)-> x=n) prototype_lst in
          
          let p = List.map (fun x-> Csptypes.String x) p in

          let p1 = trait_param n p  and
            b1 = (trait_process_call_in_body b) in
          let p2 = List.map Csp2string.expr2string p1 in
          (n,p2,b1)
            
        with Error e ->
          raise (Error e)
      
    in 
      List.map trait_proc cproc_lst
       
