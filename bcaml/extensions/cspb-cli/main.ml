(*
    Copyright (c) 2009 Huu Nghia Nguyen

    This file is part of the cspb-cli tool integrated in BRILLANT.

    cspb-cli is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    cspb-cli is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with cspb-cli.  If not, see <http://www.gnu.org/licenses/>.
*)


exception Error of string;;

let rec get_space level = 
  if level = 1 then "   "
  else
    ("   " ^ (get_space (level - 1)))
;;
        
(*-----------------------------------------------------------------------*)
(*--------------------------MACHINE ABSTRAIT-----------------------------*)
(*-----------------------------------------------------------------------*)
let write_machine_abs file_name ((name,params), clause) cproc_lst =
  
  let f = open_out (file_name ^ "_abs.mch") in  
  
  let out = (Printf.fprintf) in
  let len = (List.length cproc_lst)-1 in
  let spc = (get_space 1) in
    out f "MACHINE %s_abs" file_name;
    if params <> [] then
      let str = (B2string.list2string ", " B2string.id2string params) in
	out f "(%s)\n" str;
    else
      out f "\n";

    let rec print_clauses cl =
      if cl <> [] then
	begin
	  begin
	  match List.hd(cl) with
	      Blast.Constraints c -> out f "CONSTRAINTS\n%s %s\n" spc (B2string.predicat2string c)
	    | _  -> ()
	  end;
	  print_clauses (List.tl cl)
	end	  
    in
      print_clauses clause;
	      
    out f "VARIABLES\n%s cb\n" spc;
    out f "INVARIANT\n%s cb : 0..%d\n" spc len;
    out f "INITIALISATION\n%s cb := 0\n" spc;
    out f "OPERATIONS\n";
    out f "%sControls =\n" spc;
    out f "%sBEGIN  cb :: 0..%d  END\n" spc len;
    out f "END\n";
    flush f
;;

(*-----------------------------------------------------------------------*)
(*-----------------------REFFINEMENT-------------------------------------*)
(*-----------------------------------------------------------------------*)



let rec
  write_reffinement file_name ((name,params), clause) cproc_lst cdef_lst =
  let f = open_out (file_name ^ "_ref.ref") in
  let out = (Printf.fprintf) in
print_string "\n      eliminer des redondances\n"; flush stdout;
    let channel_communication_lst = get_channel_communication clause cdef_lst in
       (*Redondance*)
      Redondance.channel_communication_prototype := channel_communication_lst;
      let cproc_lst = Redondance.remove_redondance cproc_lst in
                                        
print_string "\n      changer le nom des variables\n"; flush stdout;
      (*changer les noms des variables*)
      let cproc_lst = Change_name.change_name_variable (file_name ^ "_ref") cproc_lst in
        
print_string "\n      chercher les types des variables\n"; flush stdout;
       (*Variable*) 
      let chan_lst = List.map (fun x -> match x with Csptypes.Channel (n,p) -> [(n,p)] | _ -> []) cdef_lst 
      in          
      Variable.cdef_lst := cdef_lst;
      Variable.cchan_lst := List.fold_left List.append [] chan_lst;
        
      let (var, inv, ini) = Variable.get_variable_global cproc_lst in 
        
print_string "\n      chercher le CLI : "; flush stdout;
      (*CLI*)
      let cli = Cli.get_cli_in_process_list cproc_lst in
      let cli = Cli.cli2string cli in
        if cli <> "" then
        begin
          print_string "Oui, il doit ajouter une invariance\n";
          out f "/*\n";
          out f "Il faut ajouter une invariance\n CLI = %s\n"  cli ;
          out f "*/\n"
        end
          else  print_string " Non, il n'a pas besoin d'ajouter une invariance\n";
          
  let bope = ref ([] : string list) in
  let spc = (get_space 1) in
    let params_str = 
      if params <> [] then
        let str = (B2string.list2string ", " B2string.id2string params) in
        ("(" ^ str ^ ")");
      else
        ""
    in
    
print_string "\n      ecriture du fichier de raffinement\n"; flush stdout;
          
    out f "REFINEMENT %s_ref%s\n" file_name params_str;    

    out f "REFINES %s\n" (file_name ^ "_abs");

    out f "INCLUDES %s%s\n" file_name params_str;

    let rec print_clauses cl =
      if cl <> [] then
        begin
          begin
            match List.hd(cl) with
              Blast.Sees s        ->
                out f "SEES\n%s %s\n" spc (B2string.list2string ", " B2string.id2string s)
              | Blast.Operations ope_lst ->
                bope := List.map (fun (name, i, o, b) -> (B2string.id2string name) ) ope_lst
              | _  -> ()
          end;
          print_clauses (List.tl cl)
        end
    in
      print_clauses clause;

           
        
      let clen = (List.length cproc_lst) -1 in
      out f "VARIABLES\n%scb%s\n" spc var;
      out f "INVARIANT\n%scb : 0..%d%s\n" spc clen inv;
      out f "INITIALISATION\n%scb := 0%s\n" spc ini;
      out f "OPERATIONS\n";
      out f "%sControls =\n" spc;
      out f "%sBEGIN\n" spc;
      let spc2 = (get_space 2) in
            
      
      let cproc_arr = Array.of_list cproc_lst in
      
      Csp2string.csp_process_prototype := cproc_lst;
      Csp2string.channel_communication_prototype := channel_communication_lst;
      
      for i=0 to clen do
        let (name, params, body) = cproc_arr.(i) in
        if i=0 then
          out f "%sSELECT cb = 0 THEN  /*%s*/ \n" spc2 name
        else
          out f "\n%sWHEN cb = %d THEN   /*%s*/ \n" spc2 i name
        ;
          
          
          let lst = Csp2string.process_operation_list body in
          List.iter (fun x -> (out f "%s%s" spc2 x)) lst;
            
      done;
      out f "%sEND\n" spc2;
      out f "%sEND\n" spc;
      out f "END\n";
      flush f
        
and
  get_channel_communication clause cdef_lst = 
    let lst = List.map (fun x -> match x with Blast.Operations op -> [op] | _ -> []) clause in
    let lst1 = List.fold_left List.append [] lst in
    let bope_lst = List.fold_left List.append [] lst1 in
    
    let lst2 = List.map (fun x -> match x with Csptypes.Channel c -> [c] | _ -> []) cdef_lst in
    let cchan_lst = List.fold_left List.append [] lst2 in
    
    let is_channel_communication (name,param) =  
      let va = Outil.isExiste (fun (n,_,_,_) -> (B2string.id2string n) = name) bope_lst in
      if (va) then
        false  (* il est un cannal de machine *)
      else true
    in
    List.find_all is_channel_communication cchan_lst
    
;;

(*-----------------------------------------------------------------------*)
(*-----------------------MAIN--------------------------------------------*)
(*-----------------------------------------------------------------------*)
let trait_file_csp file_name = 
  
  let trait_one_file file_name = 
    print_string ("\n      le fichier : [" ^ file_name ^ "]\n");
    let c = Cspparser.csp Csplexer.token (Lexing.from_channel (open_in file_name)) in    
    c
  in
  let csp = trait_one_file file_name in
  let rec trait_include c_lst = 
    if c_lst = [] then []
    else begin
      begin
        match List.hd (c_lst) with
            Csptypes.Include str -> List.append (trait_one_file str) (trait_include (List.tl c_lst))
          | _  -> trait_include (List.tl c_lst)          
      end
    end
  in
    List.append csp (trait_include csp)
;;

let traitment file_name = 
  
  
print_string ("\nLecture du fichier B : [" ^ file_name ^ ".mch]\n");
  let amn = Bparser.amn Blexer.token (Lexing.from_channel (open_in (file_name ^ ".mch"))) in
  let get_machine =
    match amn with
	Blast.Machine (h,b) -> (h,b)
      | _         -> raise (Error ("Not found machine [" ^ file_name ^ "]\n"))
  in
  let b = get_machine in
print_string "   ==>  OK\n"; flush stdout;

print_string ("\nLecture des fichiers CSP : \n");
  let csp = trait_file_csp (file_name ^ ".csp") in
  
  let lst = List.map (fun x -> match x with Csptypes.Process p -> [p] | _ -> []) csp in
  let cproc_lst = List.fold_left List.append [] lst in
  
  let lst1 = List.map (fun x -> match x with Csptypes.Definition p -> [p] | _ -> []) csp in
  let cdef_lst = List.fold_left List.append [] lst1 in
print_string "   ==>  OK\n"; flush stdout;
  
 
print_string ("\nConstruction de la machine abstraite : [" ^ file_name ^ "_abs.mch]\n");
  write_machine_abs file_name  b cproc_lst ;
print_string "   ==>  OK\n"; flush stdout;

  
print_string ("\nConstruction de le raffinement : [" ^ file_name ^ "_ref.ref]\n");
  write_reffinement file_name  b cproc_lst cdef_lst ;
print_string "   ==>  OK\n"; flush stdout;
  
print_string ("\nFini\n");
;;
  
  
  
let _ = 
  if Array.length (Sys.argv) <> 2 then
    print_string (Sys.argv.(0) ^ " filename\n")
  else 
  (
    traitment Sys.argv.(1) 
  )
;;


