exception NotImplemented of string
exception UnknownTyping
val ident_to_path : Modules.Ident.t -> Modules.path
val path_to_ident : Modules.path -> Modules.Ident.t
val path_to_string : Modules.path -> string
val name_of_path : Modules.path -> string
type paths = Modules.path list
module B :
  sig
    type ids = Modules.Ident.t list
    type id = Modules.Ident.t
    type predicate =
        PredExists of ids * predicate
      | PredForAll of ids * predicate
      | PredBin of Bbop.bop2 * predicate * predicate
      | PredNegation of predicate
      | PredAtom of Bbop.bop2 * expr * expr
      | PredParen of predicate
    and expr =
        ExprSequence of expr * expr
      | ExprParen of expr
      | ExprId of Modules.path
      | ExprFunCall of expr * expr list
      | ExprSeq of exprSeq
      | ExprSet of exprSet
      | RelSet of expr * expr
      | RelSeqComp of expr list
      | ExprNumber of number
      | ExprBool of bool
      | ExprString of string
      | ExprPred of predicate
      | ExprNuplet of expr list
      | ExprLambda of ids * predicate * expr
      | ExprSIGMA of ids * predicate * expr
      | ExprPI of ids * predicate * expr
      | ExprBin of Bbop.op2 * expr * expr
      | ExprUn of Bbop.op1 * expr
      | ExprRecords of exprRecords
    and exprSeq = SeqEnum of expr list | SeqEmpty
    and exprSet =
        SetRange of expr * expr
      | SetEnum of expr list
      | SetCompr of expr list
      | SetComprPred of ids * predicate
      | SetEmpty
      | SetPredefined of setName
      | SetUnionQuantify of ids * predicate * expr
      | SetInterQuantify of ids * predicate * expr
    and setName =
        NAT
      | NATURAL
      | NAT1
      | NATURAL1
      | INTEGER
      | INT
      | STRING
      | BOOL
    and substitution =
        SubstOperCall of paths * Modules.path * expr list
      | SubstBlock of substitution
      | SubstPrecondition of predicate * substitution
      | SubstAssertion of predicate * substitution
      | SubstChoice of substitution list
      | SubstIf of (predicate * substitution) list * substitution option
      | SubstSelect of (predicate * substitution) list * substitution option
      | SubstCase of expr * (expr list * substitution) list *
          substitution option
      | SubstAny of ids * predicate * substitution
      | SubstLet of ids * predicate * substitution
      | SubstVar of ids * substitution
      | SubstWhile of predicate * substitution * expr * predicate
      | SubstSkip
      | SubstSequence of substitution * substitution
      | SubstParallel of substitution * substitution
      | SubstSetEqualIds of paths * expr list
      | SubstSetEqualFun of expr * expr list * expr
      | SubstBecomeSuch of paths * predicate
      | SubstSetIn of paths * expr
    and variable = VarFun of expr * expr list | VarId of Modules.path
    and btype =
        PredType of expr
      | PFunType of btype * btype
      | Fin of btype
      | Untyped
      | Z
      | Bool
      | String
      | Power of btype
      | ProdType of btype * btype
      | TypeNatSetRange of expr * expr
      | TypeIdentifier of Modules.path * ids
      | RecordsType of field list
      | FunType of btype * btype
      | Sequence of btype
      | SubType of btype * expr
      | Unit
    and number = MinNumber | Number of int | MaxNumber
    and exprRecords =
        RecordsWithField of recordsItem list
      | Records of expr list
      | RecordsAccess of expr * Modules.path
      | RecordsSet of recordsItem list
    and recordsItem = Modules.path * expr
    and field = Modules.path * btype
    type term =
        Invariant of predicate
      | Constraints of predicate
      | Assertions of predicate
      | ConstantConcrete of val_type * expr
      | ConstantAbstract of val_type
      | ConstantHidden of val_type
      | Properties of predicate
      | VariablesConcrete of val_type * substitution
      | VariablesAbstract of val_type * substitution
      | VariablesHidden of val_type * substitution
      | Operations of (Modules.path * val_type) list *
          (Modules.path * val_type) list * substitution
    and val_type =
        Var_Abstract of btype
      | Var_Concrete of btype
      | Cons_Concrete of btype
      | Cons_Abstract of btype
      | Predicate
      | Operation of btype list * btype list
      | Enumerate of Modules.path
    val val_type_of_enumerate_type : Modules.Ident.t -> val_type
    type def_type = SetAbstract of expr option | SetEnumerate of ids
    val get_values_of_def_type : def_type -> ids
    type kind = unit
    exception BadApply of btype * btype
    exception NotPowerSet of btype
    exception NotProduct of btype
    exception NotRelation of btype
    exception InvalidApplication of string * btype list
    exception UntypedId of Modules.Ident.t
    val number_of_expr : expr -> number
    val expr_to_number : expr -> number
    val subst_btype : Modules.Subst.t -> btype -> btype
    val subst_valtype : val_type -> Modules.Subst.t -> val_type
    val subst_deftype : 'a -> 'b -> 'a
    val subst_kind : 'a -> 'b -> 'a
  end
module BMod :
  sig
    module Core :
      sig
        type term = B.term
        type val_type = B.val_type
        type def_type = B.def_type
        type kind = B.kind
        val subst_valtype : val_type -> Modules.Subst.t -> val_type
        val subst_deftype : def_type -> Modules.Subst.t -> def_type
        val subst_kind : kind -> Modules.Subst.t -> kind
        val get_values_of_def_type : def_type -> Modules.Ident.t list
        val val_type_of_enumerate_type : Modules.Ident.t -> val_type
      end
    type type_decl =
      Modules.Mod_syntax(B).type_decl = {
      kind : Core.kind;
      manifest : Core.def_type option;
    }
    type mod_type =
      Modules.Mod_syntax(B).mod_type =
        Signature of signature
      | Functor_type of Modules.Ident.t * mod_type * mod_type
    and signature = specification list
    and specification =
      Modules.Mod_syntax(B).specification =
        Value_sig of Modules.Ident.t * Core.val_type
      | Type_sig of Modules.Ident.t * type_decl
      | Module_sig of Modules.Ident.t * mod_type
    type mod_term =
      Modules.Mod_syntax(B).mod_term =
        Longident of Modules.path
      | Structure of structure
      | Functor of Modules.Ident.t * mod_type * mod_term
      | Apply of mod_term * mod_term
      | Constraint of mod_term * mod_type
    and structure = definition list
    and definition =
      Modules.Mod_syntax(B).definition =
        Value_str of Modules.Ident.t * Core.term
      | Type_str of Modules.Ident.t * Core.kind * Core.def_type
      | Module_str of Modules.Ident.t * mod_term
      | Open_str of Modules.path
    val subst_typedecl : type_decl -> Modules.Subst.t -> type_decl
    val subst_modtype : mod_type -> Modules.Subst.t -> mod_type
    val subst_sig_item : Modules.Subst.t -> specification -> specification
  end
module BEnv :
  sig
    module Mod :
      sig
        module Core :
          sig
            type term = BMod.Core.term
            type val_type = BMod.Core.val_type
            type def_type = BMod.Core.def_type
            type kind = BMod.Core.kind
            val subst_valtype : val_type -> Modules.Subst.t -> val_type
            val subst_deftype : def_type -> Modules.Subst.t -> def_type
            val subst_kind : kind -> Modules.Subst.t -> kind
            val get_values_of_def_type : def_type -> Modules.Ident.t list
            val val_type_of_enumerate_type : Modules.Ident.t -> val_type
          end
        type type_decl =
          BMod.type_decl = {
          kind : Core.kind;
          manifest : Core.def_type option;
        }
        type mod_type =
          BMod.mod_type =
            Signature of signature
          | Functor_type of Modules.Ident.t * mod_type * mod_type
        and signature = specification list
        and specification =
          BMod.specification =
            Value_sig of Modules.Ident.t * Core.val_type
          | Type_sig of Modules.Ident.t * type_decl
          | Module_sig of Modules.Ident.t * mod_type
        type mod_term =
          BMod.mod_term =
            Longident of Modules.path
          | Structure of structure
          | Functor of Modules.Ident.t * mod_type * mod_term
          | Apply of mod_term * mod_term
          | Constraint of mod_term * mod_type
        and structure = definition list
        and definition =
          BMod.definition =
            Value_str of Modules.Ident.t * Core.term
          | Type_str of Modules.Ident.t * Core.kind * Core.def_type
          | Module_str of Modules.Ident.t * mod_term
          | Open_str of Modules.path
        val subst_typedecl : type_decl -> Modules.Subst.t -> type_decl
        val subst_modtype : mod_type -> Modules.Subst.t -> mod_type
      end
    type binding =
      Modules.Env(BMod).binding =
        Value of Mod.Core.val_type
      | Type of Mod.type_decl
      | Module of Mod.mod_type
    type t = binding Modules.Ident.tbl
    val empty : 'a Modules.Ident.tbl
    val add_value :
      Modules.Ident.t ->
      Mod.Core.val_type ->
      binding Modules.Ident.tbl -> binding Modules.Ident.tbl
    val add_module :
      Modules.Ident.t ->
      Mod.mod_type -> binding Modules.Ident.tbl -> binding Modules.Ident.tbl
    val find : Modules.path -> binding Modules.Ident.tbl -> binding
    val find_field :
      Modules.path -> string -> Modules.Subst.t -> Mod.signature -> binding
    val find_value :
      Modules.path -> binding Modules.Ident.tbl -> Mod.Core.val_type
    val find_type :
      Modules.path -> binding Modules.Ident.tbl -> Mod.type_decl
    val find_module :
      Modules.path -> binding Modules.Ident.tbl -> Mod.mod_type
    val add_type :
      Modules.Ident.t ->
      Mod.type_decl -> binding Modules.Ident.tbl -> binding Modules.Ident.tbl
    val add_spec :
      Mod.specification ->
      binding Modules.Ident.tbl -> binding Modules.Ident.tbl
    val add_signature :
      Mod.specification list ->
      binding Modules.Ident.tbl -> binding Modules.Ident.tbl
  end
module BTyping :
  sig
    module Core :
      sig
        type ids = Modules.Ident.t list
        type id = Modules.Ident.t
        type predicate =
          B.predicate =
            PredExists of ids * predicate
          | PredForAll of ids * predicate
          | PredBin of Bbop.bop2 * predicate * predicate
          | PredNegation of predicate
          | PredAtom of Bbop.bop2 * expr * expr
          | PredParen of predicate
        and expr =
          B.expr =
            ExprSequence of expr * expr
          | ExprParen of expr
          | ExprId of Modules.path
          | ExprFunCall of expr * expr list
          | ExprSeq of exprSeq
          | ExprSet of exprSet
          | RelSet of expr * expr
          | RelSeqComp of expr list
          | ExprNumber of number
          | ExprBool of bool
          | ExprString of string
          | ExprPred of predicate
          | ExprNuplet of expr list
          | ExprLambda of ids * predicate * expr
          | ExprSIGMA of ids * predicate * expr
          | ExprPI of ids * predicate * expr
          | ExprBin of Bbop.op2 * expr * expr
          | ExprUn of Bbop.op1 * expr
          | ExprRecords of exprRecords
        and exprSeq = B.exprSeq = SeqEnum of expr list | SeqEmpty
        and exprSet =
          B.exprSet =
            SetRange of expr * expr
          | SetEnum of expr list
          | SetCompr of expr list
          | SetComprPred of ids * predicate
          | SetEmpty
          | SetPredefined of setName
          | SetUnionQuantify of ids * predicate * expr
          | SetInterQuantify of ids * predicate * expr
        and setName =
          B.setName =
            NAT
          | NATURAL
          | NAT1
          | NATURAL1
          | INTEGER
          | INT
          | STRING
          | BOOL
        and substitution =
          B.substitution =
            SubstOperCall of paths * Modules.path * expr list
          | SubstBlock of substitution
          | SubstPrecondition of predicate * substitution
          | SubstAssertion of predicate * substitution
          | SubstChoice of substitution list
          | SubstIf of (predicate * substitution) list * substitution option
          | SubstSelect of (predicate * substitution) list *
              substitution option
          | SubstCase of expr * (expr list * substitution) list *
              substitution option
          | SubstAny of ids * predicate * substitution
          | SubstLet of ids * predicate * substitution
          | SubstVar of ids * substitution
          | SubstWhile of predicate * substitution * expr * predicate
          | SubstSkip
          | SubstSequence of substitution * substitution
          | SubstParallel of substitution * substitution
          | SubstSetEqualIds of paths * expr list
          | SubstSetEqualFun of expr * expr list * expr
          | SubstBecomeSuch of paths * predicate
          | SubstSetIn of paths * expr
        and variable =
          B.variable =
            VarFun of expr * expr list
          | VarId of Modules.path
        and btype =
          B.btype =
            PredType of expr
          | PFunType of btype * btype
          | Fin of btype
          | Untyped
          | Z
          | Bool
          | String
          | Power of btype
          | ProdType of btype * btype
          | TypeNatSetRange of expr * expr
          | TypeIdentifier of Modules.path * ids
          | RecordsType of field list
          | FunType of btype * btype
          | Sequence of btype
          | SubType of btype * expr
          | Unit
        and number = B.number = MinNumber | Number of int | MaxNumber
        and exprRecords =
          B.exprRecords =
            RecordsWithField of recordsItem list
          | Records of expr list
          | RecordsAccess of expr * Modules.path
          | RecordsSet of recordsItem list
        and recordsItem = Modules.path * expr
        and field = Modules.path * btype
        type term =
          B.term =
            Invariant of predicate
          | Constraints of predicate
          | Assertions of predicate
          | ConstantConcrete of val_type * expr
          | ConstantAbstract of val_type
          | ConstantHidden of val_type
          | Properties of predicate
          | VariablesConcrete of val_type * substitution
          | VariablesAbstract of val_type * substitution
          | VariablesHidden of val_type * substitution
          | Operations of (Modules.path * val_type) list *
              (Modules.path * val_type) list * substitution
        and val_type =
          B.val_type =
            Var_Abstract of btype
          | Var_Concrete of btype
          | Cons_Concrete of btype
          | Cons_Abstract of btype
          | Predicate
          | Operation of btype list * btype list
          | Enumerate of Modules.path
        val val_type_of_enumerate_type : Modules.Ident.t -> val_type
        type def_type =
          B.def_type =
            SetAbstract of expr option
          | SetEnumerate of ids
        val get_values_of_def_type : def_type -> ids
        type kind = unit
        exception BadApply of btype * btype
        exception NotPowerSet of btype
        exception NotProduct of btype
        exception NotRelation of btype
        exception InvalidApplication of string * btype list
        exception UntypedId of Modules.Ident.t
        val number_of_expr : expr -> number
        val expr_to_number : expr -> number
        val subst_btype : Modules.Subst.t -> btype -> btype
        val subst_valtype : val_type -> Modules.Subst.t -> val_type
        val subst_deftype : 'a -> 'b -> 'a
        val subst_kind : 'a -> 'b -> 'a
      end
    module Env :
      sig
        module Mod :
          sig
            module Core :
              sig
                type term = BMod.Core.term
                type val_type = BMod.Core.val_type
                type def_type = BMod.Core.def_type
                type kind = BMod.Core.kind
                val subst_valtype : val_type -> Modules.Subst.t -> val_type
                val subst_deftype : def_type -> Modules.Subst.t -> def_type
                val subst_kind : kind -> Modules.Subst.t -> kind
                val get_values_of_def_type : def_type -> Modules.Ident.t list
                val val_type_of_enumerate_type : Modules.Ident.t -> val_type
              end
            type type_decl =
              BMod.type_decl = {
              kind : Core.kind;
              manifest : Core.def_type option;
            }
            type mod_type =
              BMod.mod_type =
                Signature of signature
              | Functor_type of Modules.Ident.t * mod_type * mod_type
            and signature = specification list
            and specification =
              BMod.specification =
                Value_sig of Modules.Ident.t * Core.val_type
              | Type_sig of Modules.Ident.t * type_decl
              | Module_sig of Modules.Ident.t * mod_type
            type mod_term =
              BMod.mod_term =
                Longident of Modules.path
              | Structure of structure
              | Functor of Modules.Ident.t * mod_type * mod_term
              | Apply of mod_term * mod_term
              | Constraint of mod_term * mod_type
            and structure = definition list
            and definition =
              BMod.definition =
                Value_str of Modules.Ident.t * Core.term
              | Type_str of Modules.Ident.t * Core.kind * Core.def_type
              | Module_str of Modules.Ident.t * mod_term
              | Open_str of Modules.path
            val subst_typedecl : type_decl -> Modules.Subst.t -> type_decl
            val subst_modtype : mod_type -> Modules.Subst.t -> mod_type
          end
        type binding =
          Modules.Env(BMod).binding =
            Value of Mod.Core.val_type
          | Type of Mod.type_decl
          | Module of Mod.mod_type
        type t = binding Modules.Ident.tbl
        val empty : 'a Modules.Ident.tbl
        val add_value :
          Modules.Ident.t ->
          Mod.Core.val_type ->
          binding Modules.Ident.tbl -> binding Modules.Ident.tbl
        val add_module :
          Modules.Ident.t ->
          Mod.mod_type ->
          binding Modules.Ident.tbl -> binding Modules.Ident.tbl
        val find : Modules.path -> binding Modules.Ident.tbl -> binding
        val find_field :
          Modules.path ->
          string -> Modules.Subst.t -> Mod.signature -> binding
        val find_value :
          Modules.path -> binding Modules.Ident.tbl -> Mod.Core.val_type
        val find_type :
          Modules.path -> binding Modules.Ident.tbl -> Mod.type_decl
        val find_module :
          Modules.path -> binding Modules.Ident.tbl -> Mod.mod_type
        val add_type :
          Modules.Ident.t ->
          Mod.type_decl ->
          binding Modules.Ident.tbl -> binding Modules.Ident.tbl
        val add_spec :
          Mod.specification ->
          binding Modules.Ident.tbl -> binding Modules.Ident.tbl
        val add_signature :
          Mod.specification list ->
          binding Modules.Ident.tbl -> binding Modules.Ident.tbl
      end
    val btype_of_valtype : B.val_type -> B.btype
    val type_of_expr : Env.binding Modules.Ident.tbl -> B.expr -> B.btype
    val type_of_exprSet :
      Env.binding Modules.Ident.tbl -> B.exprSet -> B.btype
    val type_of_predicate :
      Env.binding Modules.Ident.tbl -> B.predicate -> B.btype
    val type_of_exprRecords :
      Env.binding Modules.Ident.tbl -> B.exprRecords -> B.btype
    val check_btype : BEnv.binding Modules.Ident.tbl -> B.btype -> unit
    val check_valtype : BEnv.binding Modules.Ident.tbl -> B.val_type -> unit
    val kind_deftype : 'a -> 'b -> unit
    val check_kind : 'a -> 'b -> unit
    val deftype_of_path : 'a -> 'b -> B.def_type
    val super : B.btype -> B.btype
    val btype_match :
      Env.binding Modules.Ident.tbl -> B.btype -> B.btype -> bool
    val valtype_match :
      Env.binding Modules.Ident.tbl -> B.val_type -> B.val_type -> bool
    val deftype_equiv : 'a -> 'b -> B.def_type -> B.def_type -> bool
    val kind_match : 'a -> 'b -> 'c -> bool
    val ty_apply :
      Env.binding Modules.Ident.tbl -> B.btype -> B.btype -> B.btype
    val ty_elems : B.btype -> B.btype
    val asType : 'a * B.btype -> B.btype
    val list_mk_prod : B.btype list -> B.btype
    val dest_prod_ty : B.btype -> B.btype list
    val change_tyvar : 'a -> 'b -> 'c
    val get_prj1 : B.btype -> B.btype
    val get_prj2 : B.btype -> B.btype
    val split_prod : B.btype -> int -> B.btype list
    val untype : 'a -> 'b -> 'c
    val check_is_set : B.btype -> bool
    val get_dom : B.btype -> B.btype
    val get_ran : B.btype -> B.btype
    val ty_access : B.btype -> Modules.path -> B.btype
    val is_typed : B.btype -> bool
    val check_tyassigns :
      Env.binding Modules.Ident.tbl -> Modules.path -> B.btype -> B.btype
    val list_unique_ty :
      Env.binding Modules.Ident.tbl -> B.btype list -> B.btype
    val type_expr :
      Env.binding Modules.Ident.tbl -> B.expr -> B.expr * B.btype
    val type_opUn :
      Env.binding Modules.Ident.tbl ->
      Bbop.op1 -> B.expr * B.btype -> B.btype
    val type_opBin :
      Env.binding Modules.Ident.tbl ->
      Bbop.op2 -> B.expr * B.btype -> B.expr * B.btype -> B.btype
    val type_exprSet :
      Env.binding Modules.Ident.tbl -> B.exprSet -> B.exprSet * B.btype
    val type_exprSeq :
      Env.binding Modules.Ident.tbl -> B.exprSeq -> B.exprSeq * B.btype
    val type_predicate :
      Env.binding Modules.Ident.tbl -> B.predicate -> B.predicate
    val type_exprRecord :
      Env.binding Modules.Ident.tbl ->
      B.exprRecords -> B.exprRecords * B.btype
    val type_recordItem :
      Env.binding Modules.Ident.tbl ->
      B.recordsItem -> Modules.path * (B.expr * B.btype)
    val ident_of_path : Modules.path -> Modules.Ident.t
    val check_tyassignlist :
      Env.binding Modules.Ident.tbl ->
      Modules.path -> B.btype -> Env.binding Modules.Ident.tbl
    val type_substitution :
      Env.binding Modules.Ident.tbl -> B.substitution -> B.substitution
    val type_term : Env.binding Modules.Ident.tbl -> B.term -> B.val_type
  end
module BModTyping :
  sig
    module Mod :
      sig
        module Core :
          sig
            type term = BMod.Core.term
            type val_type = BMod.Core.val_type
            type def_type = BMod.Core.def_type
            type kind = BMod.Core.kind
            val subst_valtype : val_type -> Modules.Subst.t -> val_type
            val subst_deftype : def_type -> Modules.Subst.t -> def_type
            val subst_kind : kind -> Modules.Subst.t -> kind
            val get_values_of_def_type : def_type -> Modules.Ident.t list
            val val_type_of_enumerate_type : Modules.Ident.t -> val_type
          end
        type type_decl =
          BMod.type_decl = {
          kind : Core.kind;
          manifest : Core.def_type option;
        }
        type mod_type =
          BMod.mod_type =
            Signature of signature
          | Functor_type of Modules.Ident.t * mod_type * mod_type
        and signature = specification list
        and specification =
          BMod.specification =
            Value_sig of Modules.Ident.t * Core.val_type
          | Type_sig of Modules.Ident.t * type_decl
          | Module_sig of Modules.Ident.t * mod_type
        type mod_term =
          BMod.mod_term =
            Longident of Modules.path
          | Structure of structure
          | Functor of Modules.Ident.t * mod_type * mod_term
          | Apply of mod_term * mod_term
          | Constraint of mod_term * mod_type
        and structure = definition list
        and definition =
          BMod.definition =
            Value_str of Modules.Ident.t * Core.term
          | Type_str of Modules.Ident.t * Core.kind * Core.def_type
          | Module_str of Modules.Ident.t * mod_term
          | Open_str of Modules.path
        val subst_typedecl : type_decl -> Modules.Subst.t -> type_decl
        val subst_modtype : mod_type -> Modules.Subst.t -> mod_type
      end
    module Env :
      sig
        module Mod :
          sig
            module Core :
              sig
                type term = BMod.Core.term
                type val_type = BMod.Core.val_type
                type def_type = BMod.Core.def_type
                type kind = BMod.Core.kind
                val subst_valtype : val_type -> Modules.Subst.t -> val_type
                val subst_deftype : def_type -> Modules.Subst.t -> def_type
                val subst_kind : kind -> Modules.Subst.t -> kind
                val get_values_of_def_type : def_type -> Modules.Ident.t list
                val val_type_of_enumerate_type : Modules.Ident.t -> val_type
              end
            type type_decl =
              BMod.type_decl = {
              kind : Core.kind;
              manifest : Core.def_type option;
            }
            type mod_type =
              BMod.mod_type =
                Signature of signature
              | Functor_type of Modules.Ident.t * mod_type * mod_type
            and signature = specification list
            and specification =
              BMod.specification =
                Value_sig of Modules.Ident.t * Core.val_type
              | Type_sig of Modules.Ident.t * type_decl
              | Module_sig of Modules.Ident.t * mod_type
            type mod_term =
              BMod.mod_term =
                Longident of Modules.path
              | Structure of structure
              | Functor of Modules.Ident.t * mod_type * mod_term
              | Apply of mod_term * mod_term
              | Constraint of mod_term * mod_type
            and structure = definition list
            and definition =
              BMod.definition =
                Value_str of Modules.Ident.t * Core.term
              | Type_str of Modules.Ident.t * Core.kind * Core.def_type
              | Module_str of Modules.Ident.t * mod_term
              | Open_str of Modules.path
            val subst_typedecl : type_decl -> Modules.Subst.t -> type_decl
            val subst_modtype : mod_type -> Modules.Subst.t -> mod_type
          end
        type t = BEnv.t
        val empty : t
        val add_value : Modules.Ident.t -> Mod.Core.val_type -> t -> t
        val add_type : Modules.Ident.t -> Mod.type_decl -> t -> t
        val add_module : Modules.Ident.t -> Mod.mod_type -> t -> t
        val add_spec : Mod.specification -> t -> t
        val add_signature : Mod.signature -> t -> t
        val find_value : Modules.path -> t -> Mod.Core.val_type
        val find_type : Modules.path -> t -> Mod.type_decl
        val find_module : Modules.path -> t -> Mod.mod_type
      end
    val modtype_match : Env.t -> Mod.mod_type -> Mod.mod_type -> unit
    val pair_signature_components :
      Mod.signature ->
      Mod.signature ->
      (Mod.specification * Mod.specification) list * Modules.Subst.t
    val specification_match :
      Env.t ->
      Modules.Subst.t -> Mod.specification * Mod.specification -> unit
    val typedecl_match :
      Env.t -> Modules.Ident.t -> Mod.type_decl -> Mod.type_decl -> bool
    val strengthen_modtype : Modules.path -> Mod.mod_type -> Mod.mod_type
    val strengthen_spec :
      Modules.path -> Mod.specification -> Mod.specification
    val check_modtype : Env.t -> Mod.mod_type -> unit
    val check_signature : Env.t -> string list -> Mod.signature -> unit
    val type_module : Env.t -> Mod.mod_term -> Mod.mod_type
    val type_structure :
      Env.t -> string list -> Mod.structure -> Mod.signature
    val type_definition :
      Env.t ->
      string list -> Mod.definition -> Mod.specification * string list
  end
