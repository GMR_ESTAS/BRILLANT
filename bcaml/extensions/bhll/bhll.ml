(*18/8/2003 DP 
   Evolution pour la prise en compte des cat�gories syntaxique et mise en conformit� avec la th�se
*)


(*i*)
open Modules
exception NotImplemented of string
exception UnknownTyping
(*i*)

let ident_to_path id = Pident(id)



let path_to_ident p = 
  match p with 
    Pident(id) -> id
  | _ -> raise (Invalid_argument "path_to_ident: could not get the ident of a Pdot constructor")

let rec path_to_string p = 
  match p with 
    Pident(id) -> Ident.name id
  | Pdot(p,str) -> (path_to_string p)^str

let rec name_of_path path =
  match path with
    Pident id -> Ident.name id
  | Pdot (p,s) -> (name_of_path p)^"."^s

type paths = path list


(* \section{module B} *)

(*i*)
module B =
  struct
(*i*)

(* \subsection{type ids} *)
    type ids = Ident.t list
    type id = Ident.t 

(* \subsection{type predicate} *)
    type predicate =
	PredExists of ids * predicate	
      | PredForAll of ids * predicate
      | PredBin of Bbop.bop2 * predicate * predicate
      | PredNegation of predicate
      |	PredAtom of Bbop.bop2 * expr * expr
      |	PredParen of predicate 

(* \subsection{type expr} *)
    and expr =
        ExprSequence of expr * expr
      | ExprParen of expr
      | ExprId of path
      | ExprFunCall of expr * expr list
      | ExprSeq of exprSeq 
      | ExprSet of exprSet 
      | RelSet of expr * expr 
      | RelSeqComp of expr list
      | ExprNumber of number
      | ExprBool of bool
      | ExprString of string 
      | ExprPred of predicate
      | ExprNuplet of expr list 
      | ExprLambda  of ids * predicate * expr
      | ExprSIGMA of ids * predicate * expr
      | ExprPI of ids * predicate * expr
      | ExprBin of Bbop.op2 * expr * expr
      | ExprUn of Bbop.op1 * expr
      | ExprRecords of exprRecords

(* \subsection{type exprSeq} *) 
    and exprSeq = 
        SeqEnum of expr list 
      | SeqEmpty

(* \subsection{type exprSet} *)
    and exprSet = 
        SetRange of expr * expr
      | SetEnum of expr list
      | SetCompr of expr list
      | SetComprPred of ids * predicate
      | SetEmpty
      | SetPredefined of setName
      | SetUnionQuantify of ids * predicate * expr (*v�rifier le ids*)
      | SetInterQuantify of ids * predicate * expr (*v�rifier le ids*)

(* \subsection{type setName} *)

    and setName =     
        NAT
      | NATURAL
      | NAT1
      | NATURAL1
      | INTEGER
      | INT
      | STRING
      | BOOL

(* \subsection{type substitution} *)
    and substitution =
        SubstOperCall of paths * path * expr list
      | SubstBlock of substitution 
      | SubstPrecondition of predicate * substitution 
      | SubstAssertion of predicate * substitution 
      | SubstChoice of substitution list
      | SubstIf of (predicate * substitution) list * substitution option
      | SubstSelect of (predicate * substitution) list * substitution option
      | SubstCase of expr * (expr list * substitution) list * substitution option
      | SubstAny of ids * predicate * substitution
      | SubstLet of ids * predicate * substitution 
      | SubstVar of ids * substitution
      | SubstWhile of predicate * substitution * expr * predicate
      | SubstSkip
      | SubstSequence of substitution * substitution 
      | SubstParallel of substitution * substitution 
      | SubstSetEqualIds of paths * expr list 
      | SubstSetEqualFun of expr * expr list * expr
      | SubstBecomeSuch of paths * predicate
      | SubstSetIn of paths * expr 

(* \subsection{type variable} *)
    and variable = 
        VarFun of expr * expr list
      | VarId of path

(* \subsection{type btype} *)
    and btype =
	PredType of expr
      | PFunType of btype * btype
      | Fin of btype
      | Untyped
      | Z
      | Bool
      | String
      | Power of btype
      |	ProdType of btype * btype
      | TypeNatSetRange of expr * expr
      |	TypeIdentifier of path * ids (*v�rifier le ids*)
      | RecordsType of field list
(*DP      | Operation of btype list * btype list*)
      | FunType of btype * btype
      |	Sequence of btype
      |	SubType of btype * expr
      | Unit

(* \subsection{type number} *)
    and number =
	MinNumber
      |	Number of int
      |	MaxNumber

(* \subsection{type exprRecords} *)
    and exprRecords=
	RecordsWithField of recordsItem list
      | Records of expr list
      |	RecordsAccess of expr * path
      |	RecordsSet of recordsItem list

(* \subsection{type recordsItem} *)
    and recordsItem = path * expr

(* \subsection{type field} *)
    and field = path * btype

(* \subsection{type term} *)
    type term =
        Invariant of predicate
      | Constraints of predicate
      | Assertions of predicate (*DP expr *)
      | ConstantConcrete of val_type * expr
      | ConstantAbstract of val_type
      | ConstantHidden of val_type
      | Properties of predicate
      | VariablesConcrete of val_type * substitution
      | VariablesAbstract of val_type * substitution
      | VariablesHidden of val_type * substitution
      | Operations of (path*val_type) list * (path*val_type) list * substitution

(* \subsection{type val\_type} *)
    and val_type = 
        Var_Abstract of btype
      | Var_Concrete of btype
      | Cons_Concrete of btype
      | Cons_Abstract of btype
      | Predicate
      | Operation of btype list * btype list
(* DP Pour la prise en compte de la d�finition de valeur dans la d�finition d'un type *)
      | Enumerate of path

let val_type_of_enumerate_type id = 
  Enumerate(ident_to_path id)


(* \subsection{type def\_type} *)
    type def_type = 
        SetAbstract of expr option
      | SetEnumerate of ids

    let get_values_of_def_type t =
      match t with 
        SetEnumerate(idlist) -> idlist
      | _ -> []

(* \subsection{type kind} *)
    type kind = unit

    exception BadApply of btype * btype
    exception NotPowerSet of btype
    exception NotProduct of btype
    exception NotRelation of btype
    exception InvalidApplication of string * btype list
    exception UntypedId of Ident.t

let number_of_expr expr =
  match expr with 
      ExprNumber(n) -> n
    | _ -> raise (Invalid_argument "number_of_expr : could get number only from ExprNumber")
let expr_to_number = number_of_expr 
  
(* \subsection{fonction subst\_btype} *)		  
let rec subst_btype subst = function
    PFunType (bt1,bt2) -> PFunType (subst_btype subst bt1,subst_btype subst bt2)
  | Fin  bt -> Fin (subst_btype subst bt)
  | Power bt -> Power(subst_btype subst bt) 
  |	ProdType (bt1,bt2) -> ProdType (subst_btype subst bt1,subst_btype subst bt2)
  |	TypeIdentifier (path,idlist)  -> TypeIdentifier(Subst.path path subst,idlist)
	  (*DP      | Operation (btlist1,btlist2)  -> Operation(List.map (subst_btype subst) btlist1,List.map (subst_btype subst) btlist2)*)
  | FunType (bt1,bt2) -> FunType (subst_btype subst bt1,subst_btype subst bt2)
  |	Sequence bt -> Sequence (subst_btype subst bt)
  |	SubType (bt,exp) -> SubType (subst_btype subst bt,exp)
  | bt -> bt
      
(* \subsection{fonction subst\_valtype} *)            
let subst_valtype vty subst = 
  match vty with 
      Var_Abstract(btype) -> Var_Abstract(subst_btype subst btype)
    | Var_Concrete(btype) -> Var_Concrete(subst_btype subst btype)
    | Cons_Concrete(btype) -> Cons_Concrete(subst_btype subst btype)
    | Cons_Abstract(btype) -> Cons_Abstract(subst_btype subst btype)
    | Predicate -> Predicate 
    | Operation(btypelist1,btypelist2) -> Operation(List.map (subst_btype subst) btypelist1,List.map (subst_btype subst) btypelist2)
    | Enumerate(_) -> raise (NotImplemented("subst_valtype: Enumerate"))
	
	
(* \subsection{fonction subst\_deftype} *)
let subst_deftype dty subst = dty
  (*voir pour le expr      match dty with 
    SetAbstract(Some(expr)) -> SetAbstract(Some(subst_expr subst expr))
    | SetAbstract(None) -> dty
    | SetEnumerate(ids) 
  *)
  
  
(* \subsection{fonction subst\_kind} *)
let subst_kind kind subst = kind
  
  end
    
(* \section{module BMod} *)
module BMod = Mod_syntax(B)
  
(* \section{module BEnv} *)
module BEnv = Env(BMod)
  
(* \section{module BTyping} *)
  
(*i*)
module BTyping =
struct
  module Core = B
  module Env = BEnv
  open BMod
  open B
  (*i*)
  
  (* \subsection{Les fonctions type\_of...} 
     Avant de commencer � coder les fonctions d�finies dans la signature \texttt{CORE\_TYPING}, nous devons nous
     d�finir des fonctions permettant de retourner le type d'une expression \texttt{expr}, etc...
     Ces fonctions vont notamment faire appel � \texttt{Env.find\_value} (cf ??) qui va retourner une valeur de
     type \texttt{btype} ( car \texttt{val\_type} et \texttt{btype} sont �gaux).
     DP Faux maintenant val_type et btype sont diff�rents.
  *)
  let btype_of_valtype = function
      Var_Abstract(btype) -> btype
    | Var_Concrete(btype) -> btype
    | Cons_Concrete(btype) -> btype
    | Cons_Abstract(btype) -> btype
    | Predicate -> Bool
    | Operation(_,_) -> 
	raise (Invalid_argument "btype_of_valtype : can't get the btype of an operation")
    | Enumerate(_) -> raise (NotImplemented("btype_of_valtype: Enumerate"))
	  
	  
  let rec type_of_expr env expr =
    match expr with
	ExprParen expr' -> type_of_expr env expr'
      | ExprId path -> btype_of_valtype (Env.find_value path env)
      | ExprFunCall (expr',_) -> type_of_expr env expr'
      | ExprSet exprset -> type_of_exprSet env exprset
      | ExprNumber n -> Z
      | ExprBool b -> Bool
      | ExprString s -> String
      | ExprPred pred -> type_of_predicate env pred
      | ExprNuplet nuplet -> type_of_expr env (List.hd nuplet)
      | ExprRecords record -> type_of_exprRecords env record
      | _ -> raise (Invalid_argument "type_of_expr")
	  
  and type_of_exprSet env exprset =
    match exprset with
	SetRange (exp1,exp2) ->
	  (match (exp1,exp2) with
	       (ExprNumber _,ExprNumber _) -> TypeNatSetRange (exp1,exp2)
	     | _ -> raise (Invalid_argument "type_of_exprSet"))
      | SetEnum l -> type_of_expr env (List.hd l)
      | SetCompr l -> type_of_expr env (List.hd l)
      | SetComprPred (_,pred) -> type_of_predicate env pred
      | SetEmpty -> Untyped
      | SetUnionQuantify (_,exp1,exp2) -> type_of_expr env exp2
      | SetInterQuantify (_,exp1,exp2) -> type_of_expr env exp2
      | _ -> raise (NotImplemented "type_of_exprSet")(*Invalid_argument "type_of_exprSet") i*)
	  
  and type_of_predicate env pred =
    match pred with
      | _ -> Bool
	  
  and type_of_exprRecords env record =
    match record with
      | _ -> raise (Invalid_argument "type_of_exprRecords")
          
  (* \subsection{fonction check\_valtype} *)    
  let rec check_btype env = function
      PFunType (btyp1,btyp2) -> check_btype env btyp1;check_btype env btyp2
    | Fin btyp -> check_btype env btyp
    |	ProdType (btyp1,btyp2) -> check_btype env btyp1;check_btype env btyp2
    |	TypeIdentifier (path,idlist) -> ignore(BEnv.find_value path env)
    | FunType (btyp1,btyp2) -> check_btype env btyp1;check_btype env btyp2
    |	Sequence btyp -> check_btype env btyp
    |	SubType (btyp,exp) -> check_btype env btyp
    | _ -> ()
	
	
  let check_valtype env = function
      Var_Abstract(btype) -> check_btype env btype
    | Var_Concrete(btype) -> check_btype env btype
    | Cons_Concrete(btype) -> check_btype env btype
    | Cons_Abstract(btype) -> check_btype env btype
    | Predicate -> ()
    | Operation(btypelistout,btypelistin) -> 
        (List.iter (check_btype env) btypelistout) ; 
        (List.iter (check_btype env) btypelistin)
    | Enumerate(_) -> raise (NotImplemented("check_valtype: Enumerate"))
          
          
          
  (* \subsection{fonction kind\_deftype} *)
  let kind_deftype env dt = ()
    
  (* \subsection{fonction check\_find} *)
  let check_kind env k = ()
    
  (* \subsection{fonction deftype\_of\_path} *)
  let deftype_of_path path kind = (*DP a priori cette fonction ne nous sert pas (pas de variables de type). A revoir
                                    TypeIdentifier (path,[])*)
    SetAbstract(None)  
      
  (* \subsection{fonction valtype\_match}
     Cette fonction va permettre de v�rifier la concordance de types de deux variables. Dans le typeur de l'IRIT, la fonction
     �quivalente est \texttt{Tyenv.cmpty1}. Certains constructeurs sont transform�s avant l'appel de cette fonction. 
     Par exemple, \texttt{TypeNatSetRange} devient un \texttt{TypeIdentifier}
  *)
      
  let rec super t =
    match t with
	TypeNatSetRange _ -> TypeIdentifier ((Pident (Ident.create "INTEGER")),[])
	  (*i	| PredType exp -> super (type_of_expr exp)i*)
      | t' -> t'
	  
  let rec btype_match env bty1 bty2 =
    match (super bty1,super bty2) with
	(Z,Z) -> true
      | (Bool,Bool) -> true
      | (String,String) -> true
      | (RecordsType l1,RecordsType l2) ->
	  (List.for_all2 
	     (fun x y -> 
	        (path_equal (fst x) (fst y)) && 
		  (btype_match env (snd x) (snd y))) l1 l2)
	    (*      | (TypeIdentifier (path1,_),TypeIdentifier (path2,_)) -> 
		    path_equal path1 path2 ||
		    begin match (Env.find_type path1 env,
                    Env.find_type path2 env) with
		    ({manifest = Some def}, _) -> valtype_match env def bty2
		    | (_, {manifest = Some def}) -> valtype_match env bty1 def
		    | ({manifest = None}, {manifest = None}) -> false
		    end
		    | (TypeIdentifier (path,_),_) ->
		    begin 
		    match Env.find_type path env with
		    {manifest = Some def} -> valtype_match env def bty2
		    | {manifest = None} -> false
		    end
		    | (_,TypeIdentifier (path,_)) -> 
		    begin 
		    match Env.find_type path env  with
		    {manifest = Some def} -> valtype_match env def bty2
		    | {manifest = None} -> false
		    end
	    *)
      | (PFunType (btyp1,btyp2),PFunType (btyp3,btyp4)) -> 
	  (btype_match env btyp1 btyp3) &&
	    (btype_match env btyp2 btyp4)
      | (FunType (btyp1,btyp2),FunType (btyp3,btyp4)) -> 
	  (btype_match env btyp1 btyp3) &&
	    (btype_match env btyp2 btyp4)
      | (ProdType (btyp1,btyp2),ProdType (btyp3,btyp4)) -> 
	  (btype_match env btyp1 btyp3) &&
	    (btype_match env btyp2 btyp4)
      | (Sequence btyp1,Sequence btyp2) -> btype_match env btyp1 btyp2
      | (SubType (btyp1,exp1),SubType (btyp2,exp2)) ->
	  (btype_match env btyp1 btyp2) &&
	    ((type_of_expr env exp1)=(type_of_expr env exp2))
      | (Fin btyp1,Fin btyp2) -> btype_match env btyp1 btyp2
	  (*	| (Power btyp1,Power btyp2) -> valtype_match env btyp1 btyp2 *)
	  (*	| (Operation (l1,l2),Operation (l3,l4)) ->  
		(List.length l1 = List.length l3) &&
		(List.length l2 = List.length l4) &&
		(List.for_all2 (valtype_match env) l1 l3) &&
		(List.for_all2 (valtype_match env) l2 l4) *)
      | (_,_) -> false
          
  and valtype_match env ty1 ty2 =
    match (ty1,ty2) with
        (Var_Abstract(btype1),Var_Abstract(btype2)) -> btype_match env btype1 btype2
      | (Var_Concrete(btype1),Var_Concrete(btype2)) -> btype_match env btype1 btype2
      | (Cons_Concrete(btype1),Cons_Concrete(btype2)) -> btype_match env btype1 btype2
      | (Cons_Abstract(btype1),Cons_Abstract(btype2)) -> btype_match env btype1 btype2
      | (Predicate,Predicate) -> true
      | (Operation(btypelistin1,btypelistout1),Operation(btypelistin2,btypelistout2)) ->
	  (List.length btypelistin1 = List.length btypelistin2) &&
            (List.length btypelistout1 = List.length btypelistout2) &&
            (List.for_all2 (btype_match env) btypelistin1 btypelistin2) &&
            (List.for_all2 (btype_match env) btypelistout1  btypelistout2)
      | _ -> raise(NotImplemented " valtype_match ")
  let deftype_equiv env kind t1 t2 =   (* a revoir *)
    match (t1,t2) with
        (SetAbstract(_),SetAbstract(_)) -> true
      | (SetEnumerate(ids1),SetEnumerate(ids2)) -> true
      | _ -> raise(NotImplemented "deftype_equiv")
	  
  let kind_match env k1 k2 = true
    
    
  (* \subsection{type\_term}
     Dans le typeur de l'IRIT, c'est la fonction \texttt{type\_vars}
  *)
    
    
  (* � commenter !!!! *)
  let ty_apply env ty ta =
    let sta = super ta and sty = super ty in
      match sty with
	  Fin (ProdType (t1,t2)) -> ignore(btype_match env t1 sta) ; t2
	| PFunType (t1,t2) -> ignore(btype_match env t1 sta); t2
	| FunType (t1,t2) -> ignore(btype_match env t1 sta); t2
	| Untyped -> raise (UnknownTyping)
	| _ -> raise (BadApply (sty,sta))
	    
  (* � commenter !!!! *)
  let ty_elems t =
    match t with
	Fin t' -> t'
      | ty -> raise (NotPowerSet ty)
	  
  (* � commenter !!!! *)
  let asType (_,t) =
    try ty_elems t with _ -> ty_elems (super t)
      
      
  (* � commenter !!!! *)
  let list_mk_prod l =
    match l with
      | [] -> Untyped
      | t::l -> List.fold_left (fun x y -> ProdType (x,y)) t l
	  
  (* ... ??????????????????? *)
  let rec dest_prod_ty = function
      ProdType(t1,t2) -> (dest_prod_ty t1)@[t2]
    | t -> [t]
	
  (* ... ??????????????????? *)
  let change_tyvar id e = raise (NotImplemented "change_tyvar")
    
  let get_prj1 t = 
    match t with
        ProdType(t1,t2) -> t1
      | _ ->
          begin
            match (super t) with
		ProdType(t1,t2) -> t1
	      | _ -> raise (NotProduct t)
          end
	    
    let get_prj2 t =  
      match t with
          ProdType(t1,t2) -> t2
	| _ ->
            begin
              match (super t) with
		  ProdType(t1,t2) -> t2
		| _ -> raise (NotProduct t)
            end
	      
	      
    let rec split_prod exp n =
      if n = 1 
      then [exp]
      else (get_prj1 exp)::(split_prod (get_prj2 exp) (n - 1))

    let untype ty exp = raise (NotImplemented "untype")
      
    let check_is_set ty = 
      match (super ty) with
	| Fin _ -> true
	| Untyped -> raise (NotImplemented "check_is_set")
	| _ -> false
	    
    let get_dom t = 
      match (super t) with
	  Fin (ProdType(t1,t2)) -> t1
	| _ -> raise (NotRelation t)
	    
    let get_ran t = match (super t) with
	Fin (ProdType(t1,t2)) -> t2
      | _ -> raise (NotRelation t)
	  
	  
    let ty_access ty path = 
      match super ty with
	  RecordsType fs -> begin
	    try 
	      List.assoc path fs 
	    with
		Not_found -> raise (InvalidApplication (name_of_path path,[ty]))
	  end
	| _ -> raise (InvalidApplication (name_of_path path,[ty]))
	    
    let rec is_typed = function
      | RecordsType fl -> List.for_all (fun (_,t) -> is_typed t) fl
      | TypeIdentifier (i1,_) -> true
      | PFunType (d,r) -> is_typed d && is_typed r
      | ProdType (d,r) -> is_typed d && is_typed r
      | Fin t -> is_typed t
      | Untyped -> false
      | _ -> failwith "is_typed"
	  
    let check_tyassigns env p t2 =
      let t1 = btype_of_valtype(Env.find_value (p) env) in
	ignore (btype_match env t1 t2); t1
	  (*	begin
		match t1 with
	  (*i	      Untyped  ->
		if is_typed (super t2) then 
	      	change_tyvar id t2
		else raise (BadAssign t2) i*)
		| Untyped -> raise (UntypedId id)
		| _ -> 
		end
	  *)
	  (* v�rifie que tous les �l�ments de la liste sont coh�rents entre eux, id est qui ils sont tous du m�me type... *)
	  
    (* equivaut � list_unique_ty chez IRIT *)
    let list_unique_ty env l =
      match l with
	| [] -> raise (Invalid_argument "list_unique_ty")
	| t::tl -> ignore (List.for_all (btype_match env t) tl); t
	    (* typeof equivaut � snd et valueof � fst ... *)
	    
	    
    let rec type_expr env expr =
      match expr with
          ExprSequence (expr1,expr2) ->
	    let t1 = type_expr Env.empty expr1 in
	    let t2 = type_expr Env.empty expr2 in
	    let res = ProdType (snd t1,snd t2) in
	      (ExprSequence (fst t1,fst t2),res)
	| ExprParen expr ->
	    let t = type_expr env expr in
	      (ExprParen (fst t),snd t)
	| ExprId path ->
	    (* � v�rifier ... !!! *)
	    let t = Env.find_value path env in
	      (ExprId path,btype_of_valtype t)
	| ExprFunCall (expr,exprlist) ->
	    let t = type_expr env expr in
	    let lt = List.map (type_expr env) exprlist in
	    let lt' = List.map (snd) lt in
	      (ExprFunCall (fst t,List.map fst lt),
	       if lt = [] then (snd t) else ty_apply env (snd t) (list_mk_prod lt'))
	| ExprSeq exprSeq ->
	    let t = type_exprSeq env exprSeq in
	      (ExprSeq (fst t),snd t)
	| ExprSet exprSet ->
	    let t = type_exprSet env exprSet in
	      (ExprSet (fst t),snd t)
	| RelSet (expr1,expr2) ->
	    let t1 = type_expr env expr1 in
	    let t2 = type_expr env expr2 in
	      (RelSet (fst t1,fst t2),
	      (Fin (Fin (ProdType (asType t1,asType t2)))))
	| RelSeqComp exprlist ->
	    let lt = List.map (type_expr env) exprlist in
	      (RelSeqComp (List.map fst lt),
               list_unique_ty env (List.map snd lt))
	| ExprNumber number -> (ExprNumber number,Z)
	| ExprBool bool -> (ExprBool bool,Bool)
	| ExprString string -> (ExprString string,String)
	| ExprPred pred ->
	    let t = type_predicate env pred in
	      (ExprPred t,Bool)
	| ExprNuplet exprlist ->
	    let lt = List.map (type_expr env) exprlist in
	      (ExprNuplet (List.map fst lt),
	       list_mk_prod (List.map snd lt))
		(* mise � LocVar des ids inutile car pas de champ Bbop.kind dans id ... ??? *)
	| ExprLambda  (ids,pred,expr) ->  
	    (* regarder si les variables sont typ�es dans le pr�dicat pred. Si oui aller chercher le type
               avant d'ins�rer les variables dans l'environnement. *) 
	    let rec add i e =
	      match i with
		  [] -> e
		| h::t -> 
                    let new_e = Env.add_value h (Var_Concrete(Untyped)) env in
		      add t new_e
	    in
	    let new_env = add ids env in
	    let t1 = type_predicate new_env pred in
	    let t2 = type_expr new_env expr in
	    let l = List.map (fun x -> btype_of_valtype (Env.find_value (Pident x) new_env)) ids in 
	      (* en attendant quelques explications de l'IRIT. *)
	      (ExprLambda (ids,t1,fst(t2)),
               if ids = [] then snd t2 else PFunType (list_mk_prod l , snd t2))
	| ExprSIGMA (ids,pred,expr) -> (* idem que pour ExprLambda *)
	    let rec add i e =
	      match i with
		  [] -> e
		| h::t -> let new_e = Env.add_value h (Var_Concrete(Untyped)) env in
		    add t new_e
	    in
	    let new_env = add ids env in
	    let t1 = type_predicate new_env pred in
	    let t2 = type_expr new_env expr in
(* IRIT: cmpty .... ??? *)
	      (ExprSIGMA (ids,t1,fst t2),Z)
	| ExprPI (ids,pred,expr) -> (* idem que pour ExprLambda *)
	    let rec add i e =
	      match i with
		  [] -> e
		| h::t -> let new_e = Env.add_value h (Var_Concrete(Untyped)) env in
		    add t new_e
	    in
	    let new_env = add ids env in
	    let t1 = type_predicate new_env pred in
	    let t2 = type_expr new_env expr in
(* IRIT: cmpty .... ??? *)
	      (ExprPI (ids,t1,fst t2),Z)
	| ExprBin (op,expr1,expr2) ->
	    let t1 = type_expr env expr1 in
	    let t2 = type_expr env expr2 in
(*i � v�rifier le Location.putl ... i*)
	      (ExprBin (op,fst t1,fst t2),
	       type_opBin env op t1 t2)
	| ExprUn (op,expr) ->
	    let t = type_expr env expr in
	      (ExprUn (op,fst t),type_opUn env op t)
	| ExprRecords exprRecords ->
	    let t = type_exprRecord env exprRecords in
	      (ExprRecords (fst t),snd t)

    and type_opUn env op expr =
      match op with
	| Bbop.Fin -> Fin (snd expr)
	| Bbop.Pow -> Fin (snd expr) 
	| Bbop.Seq 
	| Bbop.ISeq -> Fin (Sequence (asType expr))
	| Bbop.Size -> 
	    let ty = get_dom (snd expr) in ignore (btype_match env ty (TypeIdentifier ((Pident (Ident.create "INTEGER")),[]))); ty
	  | Bbop.First 
	  | Bbop.Last -> 
	      ignore (btype_match env (get_dom (snd expr)) (TypeIdentifier ((Pident (Ident.create "NATURAL")),[]))); 
	      get_ran (snd expr)
	  | Bbop.Perm 
	  | Bbop.Rev -> 
	      ignore (btype_match env (get_dom (snd expr)) (TypeIdentifier ((Pident (Ident.create "NATURAL")),[]))); 
	      snd expr
	  | Bbop.Conc -> 
	      ignore (btype_match env (get_dom (snd expr)) (TypeIdentifier ((Pident (Ident.create "NATURAL")),[]))); 
	      let ty = get_ran (snd expr) in
	      ignore (btype_match env (get_dom ty) (TypeIdentifier ((Pident (Ident.create "NATURAL")),[])));
	      ty
	  | Bbop.Front 
	  | Bbop.Tail -> 
	      ignore (btype_match env (get_dom (snd expr)) (TypeIdentifier ((Pident (Ident.create "NATURAL"),[])))); snd expr
	  | Bbop.Card -> 
	      if check_is_set (snd expr) then (TypeIdentifier ((Pident (Ident.create "NATURAL"),[])))
	      else raise (InvalidApplication ("card",[snd expr]))
	  | Bbop.Min 
	  | Bbop.Max ->
	      let nat = (TypeIdentifier ((Pident (Ident.create "NATURAL")),[])) in
		ignore (btype_match env (Fin nat) (snd expr)); nat
	  | Bbop.Dom -> Fin (get_dom (snd expr))
	  | Bbop.Ran -> Fin (get_ran (snd expr))
	  | Bbop.Bool -> ignore (btype_match env (TypeIdentifier ((Pident (Ident.create "PREDICATE"),[]))) (snd expr)); (TypeIdentifier ((Pident (Ident.create "BOOL"),[])))
	  | Bbop.UMinus -> 
	      let integer = (TypeIdentifier ((Pident (Ident.create "INTEGER"),[]))) in
		ignore (btype_match env integer (snd expr)); integer
	  | Bbop.Tilde -> 
	      Fin (ProdType (get_ran (snd expr),get_dom (snd expr)))
(*DP deplacement de type_opBin vers ici, a completer
         | Bbop.Prj1 -> 
              let t1 = asType expr1 and t2 = asType expr2 in
              FunType(ProdType (t1,t2), t1)
	  | Bbop.Prj2 ->  
	      let t1 = asType expr1 and t2 = asType expr2 in
		FunType(ProdType (t1,t2), t2)
*)
	  |  _ -> raise (NotImplemented "type_opUn ")
       
    and type_opBin env op expr1 expr2 =
      match op with
	| Bbop.NatSetRange ->
	    let integer = (TypeIdentifier ((Pident (Ident.create "INTEGER"),[]))) in
	      ignore (btype_match env (list_mk_prod [integer;integer]) 
	      (list_mk_prod [snd expr1;snd expr2]));
	      Fin (TypeNatSetRange(fst expr1, fst expr2))
	| Bbop.Mul 
	      when check_is_set (snd expr1) && check_is_set (snd expr2) -> 
	      Fin (ProdType (asType expr1, asType expr2))
	  | Bbop.Minus 
	      when check_is_set (snd expr1) && check_is_set (snd expr2) ->
	      let t1 = asType expr1 and t2 = asType expr2 in
		ignore (btype_match env t1 t2);
		Fin t1
	  | Bbop.InterSets 
	  | Bbop.UnionSets ->
	      let t1 = asType expr1 
	      and t2 = asType expr2 in
		ignore (btype_match env t1 t2);
		Fin t1
	  | Bbop.OverRide ->
	      begin
	    	match (super (snd expr1), super (snd expr2)) with 
		    (Fin (ProdType (t1,t2)), Fin ty2) ->
		    ignore (btype_match env (snd expr1) (snd expr2)); Fin (snd expr1)
	    	  | (t1,t2) -> raise (InvalidApplication ("<+",[t1;t2]))
	      end
	  | Bbop.DomRestrict 
	  | Bbop.DomSubstract ->
	      begin
	    	match (super (snd expr1), super (snd expr2)) with 
		    (ty1, Fin (ProdType (t1,t2))) ->
		      ignore (btype_match env ty1 (Fin t1)); snd expr2
	    	  | (t1,t2) -> raise (InvalidApplication ("<|",[t1;t2]))
	      end
	  | Bbop.Mul 
	  | Bbop.Div 
	  | Bbop.Plus 
	  | Bbop.Minus 
	  | Bbop.Mod 
	  | Bbop.Puissance ->
	      let integer = (TypeIdentifier ((Pident (Ident.create "INTEGER"),[]))) in
		ignore (btype_match env (list_mk_prod [integer;integer]) 
		(list_mk_prod [snd expr1;snd expr2]));
		integer
	  | Bbop.PartialFunc 
	  | Bbop.PartialInj 
	  | Bbop.PartialSurj 
	  | Bbop.PartialBij ->
	      begin
		match (super (snd expr1), super (snd expr2)) with
		  (Fin t1, Fin t2) -> Fin (PFunType(asType expr1,asType expr2))
		| (t1,t2) -> raise (InvalidApplication ("-->",[t1;t2]))
	      end
	  | Bbop.TotalFunc 
	  | Bbop.TotalInj 
	  | Bbop.TotalSurj 
	  | Bbop.TotalBij ->
	      begin
		match (super (snd expr1), super (snd expr2)) with
		  (Fin t1, Fin t2) -> Fin (FunType(asType expr1,asType expr2))
		| (t1,t2) -> raise (InvalidApplication ("-->",[t1;t2]))
	      end
	  | Bbop.DirectProd ->
	      let dom1 = get_dom (snd expr1) and
		ran1 = get_ran (snd expr1) and
		dom2 = get_dom (snd expr2) and
		ran2 = get_ran (snd expr2) in
		ignore (btype_match env dom1 dom2);
		Fin(ProdType (dom1,ProdType(ran1,ran2)))
(*	  | Bbop.Image ->
	      valtype_match env (get_dom (snd expr1)) (asType expr2);
	      Fin (get_ran (snd expr1)) *)
	  | Bbop.ConcatSeq ->
	      let nat = (TypeIdentifier ((Pident (Ident.create "NATURAL"),[]))) in
	      ignore (btype_match env (get_ran (snd expr1)) (get_ran (snd expr2)));
	      ignore (btype_match env (get_dom (snd expr1)) nat);
	      ignore (btype_match env (get_dom (snd expr2)) nat);
	      snd expr1
	  | Bbop.AppendSeq ->
	      let nat = (TypeIdentifier ((Pident (Ident.create "NATURAL"),[]))) in
		ignore (btype_match env (get_ran (snd expr1)) (snd expr2));
		ignore (btype_match env (get_dom (snd expr1)) nat);
		snd expr1
	  | Bbop.PrependSeq ->
	      let nat = (TypeIdentifier ((Pident (Ident.create "NATURAL"),[]))) in
		ignore (btype_match env (get_ran (snd expr2)) (snd expr1));
		ignore (btype_match env (get_dom (snd expr2)) nat);
		snd expr2
	  | Bbop.SuffixSeq 
	  | Bbop.PrefixSeq ->
	      let nat = (TypeIdentifier ((Pident (Ident.create "NATURAL"),[]))) in
		ignore (btype_match env (get_dom (snd expr1)) nat);
		ignore (btype_match env (snd expr2) nat);
		snd expr1
	  | _ -> raise (NotImplemented "type_opBin")
		

    and type_exprSet env exprset =
      match exprset with
        SetRange (expr1,expr2) ->
	  let t1 = type_expr env expr1 in
	  let t2 = type_expr env expr2 in
	    (SetRange ((untype (TypeIdentifier ((Pident (Ident.create "INTEGER"),[]))) t1),(untype (TypeIdentifier ((Pident (Ident.create "INTEGER"),[]))) t2)),Fin (TypeNatSetRange (fst t1,fst t2)))
      | SetEnum exprlist ->
	  let lt = List.map (type_expr env) exprlist in
	  let lt' = List.map snd lt in
	    (SetEnum (List.map fst lt),
	    Fin (list_unique_ty env lt'))
      | SetCompr exprlist ->
	  let lt = List.map (type_expr env) exprlist in
	  let lt' = List.map snd lt in
	    (SetCompr (List.map fst lt), 
	    Fin (list_unique_ty env lt'))
(*DP      | SetComprPred (ids,pred) ->
	  let rec add i e =
	    match i with
		[] -> e
	      | h::t -> let new_e = Env.add_value h Untyped env in
		  add t new_e
	  in
	  let new_env = add ids env in
	  let t = type_predicate new_env pred in
	  let l = List.map (fun x -> Env.find_value (Pident x) new_env) ids in
	    (SetComprPred (ids,t),
	       Fin (list_mk_prod l))
*)
      | SetEmpty -> (SetEmpty,Fin Untyped)
      | SetUnionQuantify (ids,pred,expr) ->
	  (* ici il faut ajouter les ids... ??? *)
	  let t1 = type_predicate env pred in
	  let t2 = type_expr env expr in
	    (SetUnionQuantify (ids,t1,fst t2), 
	    snd t2)
      | SetInterQuantify (ids,pred,expr) ->
	  (* ici il faut ajouter les ids... ??? *)
	  let t1 = type_predicate env pred in
	  let t2 = type_expr env expr in
	    (SetInterQuantify (ids,t1,fst t2),snd t2)
      | _ -> raise(NotImplemented "type_exprSet")

    and type_exprSeq env exprseq =
      match exprseq with
	  SeqEnum exprlist ->
	    let lt = List.map (type_expr env) exprlist in
	    let lt' = List.map snd lt in
	      (SeqEnum (List.map fst lt),
	      Sequence (list_unique_ty env lt'))
	| SeqEmpty -> (SeqEmpty,Sequence Untyped)

    and type_predicate env pred =
      match pred with
	  PredExists (ids,pred) ->
	    let new_env = List.fold_left (fun new_e x -> Env.add_value x (Var_Concrete(Untyped)) new_e) env ids in
	    let t = type_predicate new_env pred in
	      PredExists (ids,t)
	| PredForAll (ids,pred) ->
	    let new_env = List.fold_left (fun new_e x -> Env.add_value x (Var_Concrete(Untyped)) new_e) env ids in
	    let t = type_predicate new_env pred in
	      PredForAll (ids,t)
	| PredBin (op,pred1,pred2) ->
	    (* � compl�ter *)
	    raise (NotImplemented "PredBin in type_predicate")
	| PredNegation pred ->
	    let t = type_predicate Env.empty pred in
	      t
	| PredAtom (op,expr1,expr2) ->
	    begin
	      let e1 = type_expr env expr1 in
	      let e2 = type_expr env expr2 in
		match op with
		  | Bbop.In ->
		      let els =
			match e1 with
			    ExprNuplet l,t ->  List.map2 (fun e t -> (e,t)) l (dest_prod_ty t)
			  | x -> [x] in
		      let n = List.length els in
		      let nels = List.map2 (fun e1 e2 ->
					      match e1 with
						  (ExprId path, Untyped) ->
						    ExprId (change_tyvar path e2)
	    					| (ExprFunCall (ExprId path,[]),Untyped) ->
						    ExprFunCall (ExprId (change_tyvar path e2),[])
	    					| (ee,Untyped) -> raise (Invalid_argument "PredAtom")
	    					| _ -> ignore (btype_match env (snd e1) e2);fst e1
					   )
				   els (split_prod (asType e2) n) in
		      let ne1 = 
	    		match e1 with
			    ExprNuplet l,t -> ExprNuplet nels
			  | x -> List.hd nels in
			PredAtom (op, ne1, fst e2)
		  | Bbop.NotIn -> 
		      ignore (btype_match env (snd e1) (snd e2));
		      PredAtom (op,fst e1,fst e2)
		  | Bbop.Less 
		  | Bbop.LessEqual 
		  | Bbop.Greater 
		  | Bbop.GreaterEqual ->
		      PredAtom (op, untype (TypeIdentifier ((Pident (Ident.create "INTEGER"),[]))) e1, untype (TypeIdentifier ((Pident (Ident.create "INTEGER"),[]))) e2)
		  | Bbop.SubSet 
		  | Bbop.StrictSubSet -> 
		      if check_is_set (snd e2) then 
	    		let ne1 = match e1 with
			    ((ExprId path), Untyped) -> 
			      ExprId(change_tyvar path (snd e2))
	    		  | ((ExprFunCall ((ExprId path),[])), Untyped) ->
			      ExprFunCall (ExprId (change_tyvar path (snd e2)),[])
	    		  | (ee,Untyped) -> raise (Invalid_argument "PredAtom")
	    		  | _ -> ignore (btype_match env (snd e1) (snd e2)); fst e1 in
			  PredAtom (op, ne1,fst e2)
		      else raise (NotPowerSet (snd e2)) 
		  | Bbop.NotSubSet 
		  | Bbop.NotStrictSubSet ->
                      ignore (btype_match env (asType e1) (asType e2));
		      PredAtom (op, fst e1, fst e2)
		  | Bbop.NotEqual -> 
		      ignore (btype_match env (snd e1) (snd e2));
		    PredAtom (op, fst e1, fst e2)
		  | Bbop.Equal -> 
		      let ne1 = match e1 with
			  ((ExprId path), Untyped) -> 
			    ExprId(change_tyvar path (snd e2))
			| ((ExprFunCall (ExprId path ,[])), Untyped) ->
			    ExprFunCall (ExprId(change_tyvar path (snd e2)),[])
			| (ee,Untyped) -> raise (Invalid_argument "PredAtom")
			| _ -> ignore (btype_match env (snd e1) (snd e2)); fst e1 in
		      PredAtom (op, ne1, fst e2)
		  | _ -> raise (NotImplemented "type_predicate")
	    end
	| PredParen pred ->
	    let t = type_predicate env pred in
	      t

    and type_exprRecord env record =
      match record with
	  RecordsWithField l ->
	    let l' = List.map (type_recordItem env) l in
	      (RecordsWithField (List.map (fun (i,e) -> (i,fst e)) l'),
	       RecordsType (List.map (fun (i,e) -> (i,snd e)) l'))
	| Records l ->
	    let l' = List.map (type_expr env) l in
	      (Records (List.map fst l'),list_mk_prod (List.map snd l'))
	| RecordsAccess (expr,path) ->
	    let t1 = type_expr env expr in
	      (RecordsAccess (fst t1,path),
	       ty_access (snd t1) path)
	| RecordsSet l ->
	    let l' = List.map (type_recordItem env) l in
	      List.iter (fun (_,e) -> if not (check_is_set (snd e)) 
			 then  raise (NotPowerSet (snd e))) l';
	      (RecordsSet (List.map (fun (i,e) -> (i,fst e)) l'),
	      Fin (RecordsType (List.map (fun (i,e) -> (i,asType e)) l')))

    and type_recordItem env (path,expr) =
      let t = type_expr env expr in
	(path,t)

    let ident_of_path p =
      match p with 
        Pident(i) -> i
      | Pdot(_) -> raise (Invalid_argument "could not send the ident.t value of a Pdot construction")

    let rec check_tyassignlist env path btype = 
      try
        ignore(check_tyassigns env path btype);
        env
      with Not_found -> Env.add_value (ident_of_path path) (Var_Concrete(btype)) env (*Normalement Var_Local*)

    let rec type_substitution env subst =
      match subst with
          SubstOperCall (paths,f,args) ->
	    let args' = List.map (type_expr env) args in
(* chez l'irit, le f est env_id f ... ??? *)
	      SubstOperCall (paths,f,(List.map fst args'))
	| SubstBlock subst ->
	    let t = type_substitution env subst in
	      t
	| SubstPrecondition (pred,subst) ->
	    let t1 = type_predicate env pred in
	    let t2 = type_substitution env subst in
	      SubstPrecondition (t1,t2)
	| SubstAssertion (pred,subst) ->
	    let t1 = type_predicate env pred in
	    let t2 = type_substitution env subst in
	      SubstAssertion (t1,t2)
	| SubstChoice l ->
	    let l' = List.map (type_substitution env) l in
	      SubstChoice l'
	| SubstIf (l,subs_op) ->
	    let l' = List.map (fun (x,y) -> (type_predicate env x,type_substitution env y)) l in
	      (match subs_op with
		  Some sub ->
      		    SubstIf (l',(Some sub))
		| None -> SubstIf (l',None))
	| SubstSelect (l,subs_op) ->
	    let l' = List.map (fun (x,y) -> (type_predicate env x,type_substitution env y)) l in
	      (match subs_op with
		  Some sub ->
      		    SubstIf (l',(Some sub))
		| None -> SubstIf (l',None))
	| SubstCase (expr1,l,subs_op) ->
	    let t = type_expr env expr1 in
	    let l' = List.map (fun (x,y) -> ((List.map (type_expr env) x),type_substitution env y)) l in
	      
	      (*List.iter (fun e -> ignore(valtype_match env snd(e) snd(t))) l';*)
	      SubstCase (fst t, 
		     List.map (fun (el,s) -> (List.map fst el,s)) l',
		     match subs_op with
			 Some sub -> Some sub 
		       | None -> None)
	| SubstAny (ids,pred,subst) -> (* idem que pour EcprLambda *)
	    let rec add i e =
	    match i with
		[] -> e
	      | h::t -> let new_e = Env.add_value h (Var_Concrete(Untyped)) env in
		  add t new_e
	    in
	    let new_env = add ids env in
	    let t1 = type_predicate new_env pred in
	    let t2 = type_substitution new_env subst in
	      SubstAny (ids,t1,t2)
	| SubstLet (ids,pred,subst) -> (* idem que pour ExprLambda *)
	    let rec add i e =
	      match i with
		  [] -> e
		| h::t -> let new_e = Env.add_value h (Var_Concrete(Untyped)) env in
		    add t new_e
	    in
	    let new_env = add ids env in
	    let t1 = type_predicate new_env pred in
	    let t2 = type_substitution new_env subst in
	      SubstLet (ids,t1,t2)
	| SubstVar (ids,subst) ->
	    let rec add i e =
	      match i with
		  [] -> e
		| h::t -> let new_e = Env.add_value h (Var_Concrete(Untyped)) env in
		    add t new_e
	    in
	    let new_env = add ids env in
	    let t = type_substitution new_env subst in
	      SubstVar (ids,t)
	| SubstWhile (pred1,subst,expr,pred2) ->
	    let t1 = type_predicate env pred1 in
	    let t2 = type_substitution env subst in
	    let t3 = type_expr env expr in
	    let t4 = type_predicate env pred2 in
	      SubstWhile(t1,t2,untype (TypeIdentifier ((Pident (Ident.create "INTEGER"),[]))) t3,t4)
	| SubstSkip -> SubstSkip
	| SubstSequence (subst1,subst2) ->
	    let t1 = type_substitution env subst1 in
	    let t2 = type_substitution env subst2 in
	      SubstSequence (t1,t2)
	| SubstParallel  (subst1,subst2) ->
	    let t1 = type_substitution env subst1 in
	    let t2 = type_substitution env subst2 in
	      SubstParallel (t1,t2)
        | SubstSetEqualIds (paths,exprlist) ->
            let lt = List.map (type_expr env ) exprlist in
	       SubstSetEqualIds (paths, List.map fst lt)
	| SubstSetEqualFun (expr1,exprlist,expr2) ->
	    let t1 = type_expr env expr1 in
	    let lt = List.map (type_expr env) exprlist in
	    let t2 = type_expr env expr2 in
	      ignore (btype_match env
		(ty_apply env (snd t1) (list_mk_prod (List.map snd lt)))
		(snd t2));
	      SubstSetEqualFun(fst t1, List.map fst lt, fst t2)
	| SubstBecomeSuch (paths,pred) ->
	    let t = type_predicate env  pred in
	      SubstBecomeSuch (paths,t)
	| SubstSetIn (paths,expr) ->
	    let t = type_expr env expr in
(*	    let nids = 
	      List.map2 (check_tyassigns env) ids (split_prod (asType t) n) in*)
	      SubstSetIn (paths,fst t)

    let type_term env term =
      match term with
	  Invariant expr -> raise (NotImplemented "type_term")
	| ConstantConcrete (valtyp,exp) ->
	    check_valtype env valtyp;
	    let t = type_expr env exp in
	      check_btype env (snd t);
            ignore(btype_match env (btype_of_valtype(valtyp)) (snd t));
	      valtyp
	| ConstantAbstract valtyp -> check_valtype env valtyp ; valtyp
	| ConstantHidden valtyp -> check_valtype env valtyp ; valtyp
	| Properties p -> 
            Predicate
(*	| VariablesConcrete (valtyp,subst) ->
	    let t = type_substitution env subst in
	      check_valtype env t;
	      ignore(valtype_match env valtyp t);
	      valtyp
	| VariablesAbstract (valtyp,subst) ->
	    let t = type_substitution env subst in
	      check_valtype env t;
	      ignore(valtype_match env valtyp t);
	      valtyp
	| VariablesHidden (valtyp,subst) ->
	    let t = type_substitution env exp in
	      check_valtype env t;
	      ignore(valtype_match env valtyp t);
	      valtyp *)
(*	| Assertions pred ->
	    let t = type_expr env exp in
	      check_btype env (snd t);
	      ignore(btype_match env Bool (snd t));
	      Predicate
*)	| Operations (l1,l2,sub) ->
            let l1' = List.map (fun (_,t) -> check_valtype env t ; btype_of_valtype(t)) l1 in
            let l2' = List.map (fun (_,t) -> check_valtype env t ; btype_of_valtype(t)) l2 in
(*            let t = type_substitution env sub in
	      check_valtype env (snd t); *)
	    Operation (l1',l2')
      | _ -> raise(NotImplemented "type_term")

  end

module BModTyping = Mod_typing(BMod)(BEnv)(BTyping)
;;






































