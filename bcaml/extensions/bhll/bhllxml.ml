
open Modules
open Bhll.B
open Bhll
open Bhll.BTyping

open Bhll.BMod

(* To be moved to an XML generic module *)
let comment stuff  = "<!--\n"^stuff^"\n-->"

let encoding = "UTF-8"

let standalone = "standalone = no"

let version = "<?xml version = \"1.0\"?"
	      ^standalone
	      ^encoding
	      ^"?>\n" 

let declaration = version 

let doctype = "<!DOCTYPE AMN PUBLIC \"-//INRETS// AMN Markup Language 0.1//EN\"  >"

let copyright = "copyright INRETS-ESTAS 2000/2001" 

let bhead = comment "B/XML"^copyright

let balise name att stuff = 
  let _ = Error.debug name in
  Xml.Eelement(name,att,stuff) 

let empty name = Xml.Eempty(name,[])

(* voir � peut �tre rajouter le stamp *)
let id ident = balise "id" [] [Xml.Echunk(Modules.Ident.name ident)]

let rec path ident = 
    match ident with 
      Pident(id) -> balise "path" [] [Xml.Echunk(Modules.Ident.name id)]
    | Pdot(p,str) -> balise "path" [] [(path p) ; Xml.Echunk(str)]



let idgen idmapfun idlist =  balise "idList" [] (List.map idmapfun idlist)
let ids  = idgen id
let paths = idgen path 

let path p = balise "id" [] [Xml.Echunk(Blast_lib.name_of_path p)]

(* XML output of a string *)
let string s =   balise "STRING" [] [Xml.Echunk(s)]

(* XML output for numbers *)	
let number num =
  match num with
    MinNumber -> empty "MININT" 
  | MaxNumber -> empty "MAXINT"
  | Number(n) -> balise "Number" [] [Xml.Echunk(string_of_int n)]
	
	
let rec expr expression =
  match expression with
  | ExprUn(op,expr1) ->
      balise (Bbop.op1_name op) [] [(expr_top expr1)]
  | ExprBin(op,expr1,expr2) ->
      balise (Bbop.op2_name op) [] [(expr_top expr1);(expr_top expr2)]
  | ExprSequence(expr1,expr2) ->
      balise "ExprSequence" [] [(expr_top expr1);(expr_top expr2)]
  | RelSeqComp(expr_list) ->
      balise "RelSeqComp" [] [exprlist expr_list]
  | ExprParen(expr1) ->
      balise "ExprParen" [] [expr expr1]
  | ExprId(id1) -> path id1
  | ExprFunCall(expr1,exprlist1) ->
      balise "ExprFunCall" [] [(expr_top expr1);(exprlist exprlist1)]
  | ExprSeq(seq) ->
      exprSeq seq
  | ExprSet(set) ->
	exprSet set
  | RelSet(expr1,expr2) ->
      balise "RelSet" [] [(expr_top expr1);(expr_top expr2)]
  | ExprNumber(num) ->
      number num
  | ExprBool(c) ->
      boolConstant c
  | ExprString(str) ->
      string str
  | ExprRecords(record) ->
      exprRecords record
  | ExprPred(pred1) ->
      balise "BoolEvaluation" [] [pred_top pred1]
  | ExprNuplet(exprlist) ->
      balise "Maplet" [] (List.map (expr_top) exprlist)
  | ExprSIGMA(ids1,pred,expr) ->
      balise "SIGMA" [] [(ids ids1);(pred_top pred);(expr_top expr)]
  | ExprPI(ids1,pred,expr) ->
      balise "PI" [] [(ids ids1);(pred_top pred);(expr_top expr)]
  | ExprLambda(ids1,pred,expr) ->
      balise "Lambda" [] [(ids ids1);(pred_top pred);(expr_top expr)]
and 
    exprlist exprlist1 = 
  balise "ExprList" [] (List.map (expr_top) exprlist1)
and
    expr_top expr1 =
  balise "Expr" [] [expr expr1] 
and 
  exprSeq seq =
  match seq with
    SeqEnum(exprlist) ->
      balise "SeqEnum" [] (List.map expr_top exprlist)
  | SeqEmpty ->
      empty "SeqEmpty"
and
    substitution subs =
  match subs with
  | SubstBlock(subst) ->
      balise "Block" [] [substitution subst]
  | SubstSetEqualIds(ps,exprlist1) ->
      balise "SetEqualIds" [] 
	[(paths ps);(exprlist exprlist1)]
  | SubstSetEqualFun(expr1,exprlist,expr2) ->      
      balise "SetEqualFun" [] 
        [(expr (ExprFunCall(expr1,exprlist)));(expr_top expr2)]
  | SubstBecomeSuch(ps,pred1) ->
      balise "BecomeSuch" [] [(paths ps); pred_top pred1]
  | SubstSetIn(ps1,expression) ->
      balise "SetIn" [] [(paths ps1) ; expr_top expression]
  | SubstPrecondition(pred,subst) ->
      (balise "Precondition" [] 
	 [(balise "PRE" [] [pred_top pred ])
	    ; (balise "THEN" [] [substitution subst])])
  | SubstAssertion(pred,subst) -> 
      balise "ASSERT" [] [pred_top pred;substitution subst]
  | SubstChoice(substlist) ->
      balise "CHOICE" [] (List.map substitution substlist)
  | SubstIf(predsubstList,substOption) ->      
      balise "IF" [] 
	((List.map elseIf predsubstList)@(substitution_option substOption))
  | SubstSelect(whenpartlist,substOption) -> 
      let l = (List.map whenpart whenpartlist) in 
      (balise "SELECT" [] (l @ (substitution_option substOption) ))
  | SubstCase(expression,orpartlist1,substOption) ->      
      balise "CASE" [] 
	([expr_top expression;
          (orPartList orpartlist1)]
	 @substitution_option substOption)
  | SubstAny(idlist,pred,subst) ->      
      balise "ANY" [] 
	[(ids idlist);(pred_top pred);(substitution subst)]
  | SubstLet(idlist,pred,subst) ->      
      balise "LET" [] 
        [(ids idlist);(pred_top pred);(substitution subst)]
  | SubstVar(idlist,subst) ->
      balise "VAR" [] 
	([ids idlist]
	 @[substitution subst])
  | SubstSequence(subst1,subst2) ->      
      balise "Sequence" [] 
	([(substitution subst1)
	    ; (substitution subst2)])
  | SubstWhile(pred1,subst,expression,pred2) ->      
      balise "WHILE" [] 
	([(balise "WhileTest" [] [(pred_top pred1)])
	    ; (balise "WhileBody" [] [(substitution subst)])
	    ; (balise "WhileInvariant" [] [(pred_top pred2)])
	    ; (balise "WhileVariant" [] [(expr_top expression)])
	])
  | SubstOperCall(ps1,id,exprlist1) ->    
      balise "OperCall" [] 
        ([paths ps1]@[(expr_top (ExprId(id)));(exprlist exprlist1)]) 	
  | SubstParallel(subst1,subst2) ->      
      balise "Parallel" [] [(substitution subst1);(substitution subst2)]
  | SubstSkip ->
      	balise "Skip" [] []
and
    substitution_option substOption =
  match substOption with
    Some(s) -> [substitution s]
  | None -> []
and
    orPart (exprlist,subst) =
  balise "OrPart" [] ((List.map expr_top exprlist)@[(substitution subst)])
and    
    orPartList listorpart =
  balise "OrPartList" [] (List.map orPart listorpart)
and
  elseIf (pred,subst) =
  balise "IfThen" [] [(pred_top pred);(substitution subst)]
and
  funParams params = (
    if params != [] 
    then [(balise "fparams" [] (List.map expr_top params))]
    else []
  )
and
    predicat p = 
  match p with
    PredParen(p) ->
      balise "PredParen" []  [(predicat p)]
  | PredBin(op,pred1,pred2) ->
      balise (Bbop.bop2_name op) [] [(predicat pred1);(predicat pred2)]   
  | PredAtom(op,expr1,expr2) ->
      balise (Bbop.bop2_name op) [] [(expr expr1);(expr expr2)]   
  | PredNegation(pred) ->
      balise "Neg" []   [(predicat pred)]    
  | PredExists(ids1,pred) ->
      balise "Exists" [] [(ids ids1);(pred_top pred)]
  | PredForAll(ids1,pred) ->    
      balise "ForAll" [] [(ids ids1);(pred_top pred)]
and
    pred_top pred1 = (balise "Predicate" [] [(predicat pred1)])

and    
    def_type dt =
  match dt with
    SetAbstract(Some(exp)) -> 
      balise "AbstractSet" [] [expr exp]
  | SetAbstract(None) -> 
      balise "AbstractSet" [] []
  | SetEnumerate(ids1) ->
      balise "AbstractSet" [] [ids ids1]
and
    type_B t =
  match t with 
    TypeNatSetRange(exp1,exp2) ->
      balise "Range" [] [(expr exp1);(expr exp2)]
  | PredType(pred) ->
      balise "PredType" [] [expr pred]
  | RecordsType(fieldlist) ->
      balise  "RecordsType" [] (List.map  field fieldlist)
  | PFunType (btype1,btype2) ->  balise "PFunType" [] [(type_B btype1);(type_B btype2)]
  | Fin btype -> balise "Fin" [] [type_B btype]
  | Untyped -> empty "Untyped"
  | Z -> empty "Z"
  | Bool -> empty "Bool"
  | String -> empty "String"
(*DP  | Power btype -> balise "Power" [] [type_B btype] *)
  | ProdType (btype1,btype2) -> balise "ProdType" [] [(type_B btype1);(type_B btype2)]
(* � revoir *)
  | TypeIdentifier (p,id_list) -> balise "TypeIdentifier" [] [(path p);(ids id_list)]
(*DP  | Operation (btype_list1,btype_list2) -> balise "Operation" [] ((List.map type_B btype_list1)@(List.map type_B btype_list2)) *)
  | FunType (btype1,btype2) -> balise "FunType" [] [(type_B btype1);(type_B btype2)]
  | Sequence btype -> balise "Sequence" [] [type_B btype]
  | SubType (btype,exp) -> balise "SubType" [] [(type_B btype);(expr exp)]
  | _ -> raise(NotImplemented "type_B")
and
    head (ident,param) =
  let p = balise "HeadPar" [] (List.map id param) in
  balise "Head" [] [(id ident);p]
and    
    term t =
  match t with
  |  Invariant(pred) ->
      balise  "INVARIANT" [] [(pred_top pred)]
(*DP  |  Assertions(pred) ->
      balise  "ASSERTIONS" [] [(pred_top pred)]
*)
  |  ConstantConcrete(valtype,exp) ->
      balise  "CCONSTANTS" []  [(type_B (btype_of_valtype(valtype)));(expr exp)]
  |  ConstantAbstract(valtype) ->
      balise  "ACONSTANTS" []  [type_B (btype_of_valtype(valtype))]
  |  ConstantHidden(valtype) ->
      balise  "HCONSTANTS" []  [type_B (btype_of_valtype(valtype))]
  |  VariablesHidden(valtype,sub) ->
      balise  "HVARIABLES" []  [(type_B (btype_of_valtype(valtype)));(substitution sub)]
  |  Properties(exp) ->
      balise  "PROPERTIES"  [] [predicat exp]
  |  VariablesConcrete(valtype,sub) ->
      balise  "CVARIABLES"  [] [(type_B (btype_of_valtype(valtype)));(substitution sub)]
  |  VariablesAbstract(valtype,sub) ->
      balise  "AVARIABLES"  [] [(type_B (btype_of_valtype(valtype)));(substitution sub)]
  |  Operations(out,arg,body) ->
      balise  "OPERATIONS"  [] [operation (out,arg,body)]
  | _ -> raise(NotImplemented "term")
and 
    valuation (ident,expression) =
  balise "Valuation" [] [(id ident);(expr_top expression)]
and 
    instance (ident,exprlist1) =
  balise "instance" []  ([id ident]@[exprlist exprlist1])
and
    field (p,typ) =  balise "field" [] [(path p);(type_B typ)]
and
    operation (out,arg,body) =
  let oout = match out with
    [] -> []
  | _ -> [balise "OperOut" [] (List.map (fun (p,valtype) -> balise "IdType" [] [path p;type_B (btype_of_valtype(valtype))]) out)]
  in
  let oin  = match arg with
    [] -> []
  | _ ->  [balise "OperIn" [] (List.map (fun (p,valtype) -> balise "IdType" [] [path p;type_B (btype_of_valtype(valtype))]) out)]
  in
  balise "Operation" [] 
    ([balise "OperHead" [] 
	 ([balise "OperName" [] (oout@oin)]@[balise "OperBody" [] [(substitution body)]])])

and
    exprRecords record =
  match record with 
    RecordsWithField(recordsItemlist) ->
      balise "RecWithFields" [] (List.map  recordsItem recordsItemlist)
  | Records(exprlist) ->
      balise "Records" [] (List.map expr_top exprlist)
  | RecordsAccess(expr,id1) ->
      balise "RecordAccess" [] [(expr_top expr); (path id1)]
and
    recordsItem (id1,expr) =
  balise "RecordItem" [] [(path id1);(expr_top expr)] 
and
    predTyping (ids1,expr1)=
  balise "PredType" [] ([ids ids1]@[(expr expr1)])
and
    exprSet set =
  match set with
    SetRange(expr1,expr2) ->
      balise "SetRange"  [] [(expr_top expr1);(expr_top expr2)]
  | SetPredefined(setName1) ->
      balise "SetPredefined" [] [setName setName1]
  | SetEnum(exprlist) ->
      balise "SetEnum" [] (List.map expr_top exprlist)
  | SetCompr(exprlist) ->
      balise "SetCompr" [] (List.map expr_top exprlist)
  | SetComprPred(ids1,pred) ->
      balise "SetComprPred" [] ([ids ids1]@[(pred_top pred)])
  | SetEmpty -> 
      empty "SetEmpty"
  | SetUnionQuantify(ids1,pred,expr) ->
      balise "SetUnionQ" [] ([ids ids1]@[(pred_top pred);(expr_top expr)])
  | SetInterQuantify(ids1,pred,expr) ->
      (balise "INTER") [] ([ids ids1]@[(pred_top pred);(expr_top expr)])
and
    setName n =
  match n with
    NAT -> 
      empty "NAT"
  | NATURAL ->
      empty "NATURAL"
  | NAT1 ->
      empty "NAT1"
  | NATURAL1 ->
      empty "NATURAL1"
  | INTEGER ->
      empty "INTEGER"
  | INT ->
      empty "INT"
  | STRING ->
      empty "STRING"
  | BOOL ->
      empty "BOOL"
and
    boolConstant b =
  if b then
    empty  "TRUE" 
  else
    empty "FALSE"
and 
    whenpart (pred,subst) =
  balise "WhenPart" [] [(pred_top pred);(substitution subst)]
and
  mod_definition d =
  match d with
    | Value_str (i,t) -> balise "Value_str" [] [id i;term t]
    | Type_str (i,kind,def_type) -> balise "Type_str" [] [id i](*DP ;type_B def_type]*)
    | Module_str (i,mt) -> balise "Module_str" [] [id i;mod_term mt]
and
  mod_term t =
  match t with
    | Longident p -> balise "Longident" [] [path p]
    | Structure s -> balise "Structure" [] (mod_structure s)
    | Functor (i,mtype,mterm) -> balise "Functor" [] [id i;mod_type mtype;mod_term mterm]
    | Apply (mterm1,mterm2) -> balise "Apply" [] [mod_term mterm1;mod_term mterm2]
    | Constraint (mterm,mtype) -> balise "Constraint" [] [mod_term mterm;mod_type mtype]
and
  mod_type t =
  match t with
    | Signature s -> balise "Signature" [] (mod_signature s)
    | Functor_type (i,mtype1,mtype2) -> balise "Functor_type" [] [id i;mod_type mtype1;mod_type mtype2]
and
  mod_signature l = List.map mod_specification l
and
  mod_specification s =
  match s with
    | Value_sig (i,valtype) -> balise "Value_sig" [] [id i;type_B (btype_of_valtype(valtype))]
    | Type_sig (i,t) -> balise "Type_sig" [] ([id i]@(mod_type_decl t))
    | Module_sig (i,mtype) -> balise "Module_sig" [] [id i;mod_type mtype]
and
  mod_structure s = List.map mod_definition s
and 
  mod_type_decl t =
  match t.manifest with
    | Some dtype -> [balise "Type_decl" [] [def_type dtype]]
    | None -> []
    
let doc r =
  let d = Xml.XMLDecl("1.0",Some(false),Some("ISO-8859-1")) in
  let dtd = Some(Xml.DTD("AMN",Some(Xml.DTDsys("./AMN.dtd")))) in
  let p = Xml.Prolog(Some(d),dtd,[]) in 
  Xml.XML(p,r,[]) 

