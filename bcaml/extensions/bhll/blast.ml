(* $Id$ *)
(*
  Abstract Syntax Tree for the B Language (BLAST)
  
  Georges Mariano <georges.mariano@inrets.fr>
  Dorian Petit <dorian.petit@inrets.fr>
  Copyright 2000,2001 -- INRETS-ESTAS

  Mamoun Filali <mamoun.filali@irit.fr>
  Copyright 2000,2001 -- IRIT
*)


exception Parse_error of int * string
open Modules 
open Bhll
    
(* \section{Abstract Machine Components}  *)
(* \MRB{7.1} *)
type amn = 			
  | Machine of head * clause list
  | Refinement of head * id * clause list  
  | Implementation of head * id * clause list 
  | EmptyTree
and 
      head = id * ids
and
      
(* 
\section{Clauses}  
   
   \MRB{7.1} 
   \MRB{7.3} 
   \MRB{7.4} 
*)
      clause = 
  | Definitions of def list
  | Constraints of predicate
  | Invariant of predicate
  | Sets  of set list
  | Initialisation of substitution
  | ConstantsConcrete of ids
  | ConstantsAbstract of ids 
  | ConstantsHidden of ids
  | Properties of predicate
  | Values of (id * expr) list
  | VariablesConcrete of ids
  | VariablesAbstract of ids
  | VariablesHidden of ids
  | Assertions of predicate list
  | Operations of operation list 
  | Promotes of paths 
  | Sees of paths 
  | Uses of paths 
  | Extends of instance list
  | Includes of instance list
  | Imports of instance list
        
and 

(* \section{Definitions} *)
      def = 
  | Def of id * ids * defBody 
  | FileDef of string
and 
      
(* 
   \textbf{In the current grammar, definitions bodies are restricted to
   well defined syntactic entities.}
*)
      defBody = 
  | ExprDefBody of expr
  | SubstDefBody of substitution
and

(* \subsection{Sets} *)
      set = 
  | SetAbstDec of id
  | SetEnumDec of id * ids  
  | SetRecordsDec of id * B.recordsItem list
and
      instance = path * expr list 
and 
      
      
(* \section{Predicates}\label{sec:GA-predicate} \MRB{4} *)
   predicate = B.predicate
 and

(* \section{Operations}\label{sec:GA-Oper} \MRB{5} *)
   operation = id * ids * ids * substitution
 and

(* \section{Expressions}\label{sec:GA-expr} \MRB{5} *)
   expr =  B.expr
 and

(*\section{Substitutions}\label{sec:GA-substitution} *)
   substitution = B.substitution
 and
   ids = B.ids
 and

(* \subsection{Identifiers} *)
   id = B.id
;;
