(*
  $Id$
*)
 
(* MG *)

open Amn2bhll

exception Not_B_file of string

exception Stop

    
let mylexer_no_debug =   
  Blexer.token
            
let mylexer = ref mylexer_no_debug
    
let mylexer_debug lexbuf = 
  let t = Blexer.token lexbuf   in
  (* Debug.affiche_debug t 
    ; flush stdout
    ; *) 
     t
    
(* 
   Variables <<d'environnements>> et fonctions utiles pour les options 
*)
    
let output_directory = 
  try
    ref (Bfile.dest_check (Sys.getenv "BCAML_DEST"))
  with
    Not_found -> ref("/tmp")
        
        
let dest_bool = ref false

let verbose = ref false

(* decompilation XML - inactive *)    
let xml_dump = ref false
    
let bhll = ref false

let bhll_set () =
  bhll := true
    
let dest_set path = 
  (output_directory := Bfile.dest_check path)
    ; dest_bool := true
        
        
let deps_build = ref false
    
let bdeps_output = ref "./BDEPS.output"
    
let bdeps_set file = 
  deps_build := true
      ; bdeps_output := file

let verbose_set () = verbose := true
    
let quiet_set () = verbose := false 
    
let xml_set () = xml_dump := true
    
    
let bparser_version = "0.35"
    
    
let rec args = 
  [
   ("--help",
    Arg.Unit(fun () -> 
      Arg.usage args ("Bparser "^bparser_version); raise Stop ),
    "\t\t\t display this help")
     ;
   ("--dest",
    Arg.String(dest_set),
    "<dir>,  \t\t output the generated files in directory <dir>")
     ;
   ("--debug",
    Arg.Unit(Error.debug_set),
    ", \t\t\t debug mode (default to false)")
     ;
   ("--verbose",
    Arg.Unit(verbose_set),
    ", \t\t\t display statistical information")
     ;
   ("--quiet",
    Arg.Unit(quiet_set),
    ", \t\t\t quiet version")
     ;
   ("--xml",
    Arg.Unit(xml_set),
    ", \t\t\t dumps  xml files")
     ;
   ("--bhll",
    Arg.Unit(bhll_set),
    ", \t\t\t convert into B/HLL syntax")

]
    
    
(*MG pour le debugging 
let met_de_cote file = 
  if Error.debug_mode () then 
  let l = Bfile.base_name file in
  let d = !output_directory^"XXX/spec/"^l in
  let cmd = "cp --force --backup "^file^" "^d in
  let _ = Error.debug cmd in
  (Sys.command (cmd)) 
    ; 
  flush stdout
    ; 
  ()  
    
    *)

let parse_in_channel file cin =
  try
    let lexbuf = Lexing.from_channel cin      in
    let res = Bparser.amn !mylexer lexbuf     in
    (
     close_in cin 
       ; (Error.parsing_success file)  
       ; res 
    )
  with Blast.Parse_error(nb,msg) -> 
    let (string_error,line,col) = Linenum.for_position file nb  in
 (*   let _ = met_de_cote file  in *)
    close_in cin
      ; (Error.parsing_failure file line col msg) 
      ; Blast.EmptyTree 
      (*    |	_ -> close_in cin ; raise Parsing.Parse_error*)
      
      
let parse_string str =
  try  
    let lexbuf = Lexing.from_string str in
    (Bparser.amn !mylexer lexbuf)
  with _ -> raise (Invalid_argument "analyse")
      
      
let analyse str = 
  try  
    let cin = 
      if Bfile.isBfile str
      then open_in str
      else raise (Not_B_file str)
    in
    let res = parse_in_channel str cin    in 
    (str,res)
  with Sys_error s ->  
    ("",parse_string str)
  | Parsing.Parse_error -> 
      Error.message ("\n --Bwarn "^"[?not parsed?]  "^"<"^str^">");
      (str,Blast.EmptyTree)
  | Not_B_file str -> 
      (str,Blast.EmptyTree)
        
        
let do_all_mkdir = ref false
    
    
let mkdir_all (filename,ast) = 
  let dir =  !output_directory^(Bfile.project filename)  in
  let filename_out = dir^"/spec/"^(Filename.basename filename)   in
  (
   Bfile.simpleMkdir dir
     ; Bfile.simpleMkdir (dir^"/spec") 
     ; Bfile.simpleMkdir (dir^"/tex") 
     ; Bfile.simpleMkdir (dir^"/lang") 
     ; Bfile.simpleMkdir (dir^"/bdp") 
     ; Bfile.simpleMkdir (dir^"/xml") 
     ; do_all_mkdir := true
  )
          
let files_list = ref []
    

let xml_processing (filename,ast)  =
(*MG
  Error.debug "xml_processing starting ..."
    ;
*)
  if !xml_dump
  then   
    begin
      mkdir_all  (filename,ast)
        ; 
      let r = (Bhllxml.mod_definition ast) in
      let r1 = (Bhllxml.doc r) in
      let dir = !output_directory^(Bfile.project filename)^"/xml/" in
      let output_file = dir ^(Filename.basename filename)^".xml" in
      let chan = open_out output_file in
      let linelen = 80 in 
      (Pp.ppToFile chan linelen (Ppxml.ppDoc r1)
	 ;
       close_out chan ) 
    end
      
      
let process_file str = 
  let (filename,ast) as parsing_result = analyse str  in
  (
   (files_list := parsing_result::!files_list )     
     ;
(*MG debug
   (Error.debug (filename^"\n"))
   ;
*)
   
   let conversion_result = amn2bhll_processing ast in
   xml_processing (filename,conversion_result);   
   ()
     
  )
        
let process_verbose time_debut =
  (
   match !verbose with 
     true -> 
       let (echec,reussite) = 
         List.fold_left 
           (fun (e,r) (_,ast) -> 
	     match ast with
	       Blast.EmptyTree -> (e+1,r)
	     | _ -> (e,r+1))
           (0,0)
           !files_list
       in
       let time = flush stdout;Unix.times() in
       Error.message ("--Stat Nombre de fichiers analyses : "
		      ^(string_of_int (List.length !files_list))
		     )
	 ;
       Error.message ("--Stat Nombre d'echecs : "
		      ^(string_of_int echec)
		     )
	 ;
       Error.message ("--Stat Nombre de reussite : "
		      ^(string_of_int reussite)
		     )
	 ;
       Error.message ("\n--Stat Temps d'execution (system): "
		      ^(string_of_float (time.Unix.tms_stime ))
		      )
					;
       Error.message ("--Stat Temps d'execution (users): "
		      ^(string_of_float (time.Unix.tms_utime))
		      )
					;
       Error.message ("--Stat Temps d'execution (total): "
		      ^(string_of_float ((Unix.gettimeofday()) -. time_debut)))
					;
       Error.message ("\n");
       | false -> ()
	 )


let _ = 
try
  let time_debut = Unix.gettimeofday() in
  (	
    Arg.parse args process_file ("Bparser "^bparser_version)
      ; process_verbose time_debut 
      ; print_string "\n" 
      ; flush stdout 
       (*
	  ;
          Gui.process  files_list
        *)
   )
with Stop -> ()
    
    
(*
   $Log$
   Revision 1.1  2004/03/06 23:12:41  gmariano
   (MG) Savannah => Gna!

   Revision 1.3  2004/02/06 15:33:45  dorian_petit
   r�int�gration de bhll

   Revision 1.1  2002/08/29 16:39:22  adoniec
   *** empty log message ***

   Revision 1.13  2002/02/20 10:13:56  gmariano
   Mise � jour CVS - R.A.S.

   Revision 1.12  2001/11/07 17:46:26  gmariano
   Synchro 0.35 cvs/Savannah (reset de main.ml)

   Revision 1.6  2001/05/29 15:36:51  methodeb
   Mise � jour g�n�rale (MG)

   Revision 1.5  2001/04/05 13:43:24  methodeb
   Nettoyage de source

   Revision 1.4  2000/12/11 17:07:07  mariano
   modification du mode debug

   Revision 1.3  2000/10/18 11:29:20  methodeb
   .

   Revision 1.2  2000/09/22 14:00:38  methodeb
   * le profil de PredIn est maintenant expr * expr (MG)
   
   Revision 1.1.1.1  2000/08/21 14:08:24  methodeb
   Imported using tkCVS
   
*)
    

    



