(* $Id$*)

exception NotYetImplemented of (string * string)


exception NoInitialisation of string

exception NoInvariant of string

open Bhll

open Modules



(*
let rec id_to_path id=
  match id with  
    Blast.Id(i) ->  i
  | Blast.IdTy(i,_) -> id_to_path i
  
let rec id_to_ident id =
  match id with  
    Blast.Id(i) ->  
      (match i with 
        Pident(ident) -> ident
      | _ -> Error.error "Could not get an Ident.t value from a dot identifier"
      )
  | Blast.IdTy(i,_) -> id_to_ident i
  
*)
let rec amn t =
  match t with
    | Blast.Machine (h,clause_list) -> 
        let h' = head h in
        let def_list = List.map (fun x -> clause x clause_list) clause_list 
        in
	BMod.Module_str (fst h',BMod.Structure (List.concat def_list))
(*DP raise (NotYetImplemented ("amn","Machine"))*)
    | Blast.Refinement (h,i,clause_list) -> 
	let h' = head h in
(*GM	let i' = id i in *)
	let def_list = List.map (fun x -> clause x clause_list) clause_list in
	  BMod.Module_str (fst h',BMod.Structure (List.concat def_list))
(*DP raise (NotYetImplemented ("amn","Refinement"))*)
    | Blast.Implementation (h,i,clause_list) -> 
	let h' = head h in
(*GM	let i' = id i in *)
	let def_list = List.map (fun x -> clause x clause_list) clause_list in
	  BMod.Module_str (fst h',BMod.Structure (List.concat def_list))
    | Blast.EmptyTree -> raise (Invalid_argument "EmptyTree")
	   
and head (i,id_list) =
  let name = id i in
  let args = List.map id id_list in
    (name,args)
    
and clause c clause_list =
  match c with
    | Blast.Definitions def_list -> 
	[]
    | Blast.Constraints pred -> 
	(* BMod.Value_str (Ident.create "constraints",predicate pred) *) 
	[BMod.Value_str (Ident.create "constraints",B.Constraints (predicate pred))]
    | Blast.Invariant pred -> 
	[BMod.Value_str (Ident.create "invariant",B.Invariant (predicate pred))]
    | Blast.Sets set_list -> List.map set set_list
    | Blast.Initialisation subst -> 
        []
    | Blast.ConstantsConcrete id_list -> 
	[]
    | Blast.ConstantsAbstract id_list -> 
	List.map 
          (fun x -> let identt = (
            match x with
	    | Pident p -> p
	    | _ -> Error.error "Need a correct identifiers")
          in BMod.Value_str (identt,B.ConstantAbstract (B.Cons_Abstract(B.Untyped)))
          )
          (List.map ident_to_path id_list)
(*DP existe encore comme constante ?
    | Blast.ConstantsHidden id_list -> 
	List.map (fun x -> let identt = (
          match x with
          | Blast.Id (Pident p) -> p
          | _ -> raise (Error "Need a correct identifiers")) in 
	BMod.Value_str (identt,B.ConstantHidden (B.Untyped))) id_list
*)
    | Blast.Properties pred -> 
	[BMod.Value_str (Ident.create "properties",B.Properties (predicate pred))]
    | Blast.Values l -> 
        List.map (fun (x,y) -> 
          let identt = x (*id_to_ident x *)
(*DP 30/9/03
   (
   match x with
   | Blast.Id (Pident p) -> p
   | _ -> raise (Error "Need a correct identifiers"))
*)
          in 
          BMod.Value_str (identt,B.ConstantConcrete (B.Cons_Concrete(B.Untyped),expr y))) l

    | Blast.VariablesConcrete id_list -> 
	(* recuperation d une eventuelle clause initialisation *)
	let ini = List.find (fun x -> match x with
			     | Blast.Initialisation _ -> true
			     | _ -> false) clause_list in
	let s = (match ini with
		   | Blast.Initialisation sub -> sub
		   | _ -> Error.error "Error in initialisation of concrete variable") in
	(* recuperation de la clause invariant *)
	let inv = List.find (fun x -> match x with
			       | Blast.Invariant _ -> true
			       | _ -> false) clause_list in
	let p = 
          (match inv with
	  | Blast.Invariant pred -> pred
	  | _ -> Error.error "Error in invariant of concrete variable") in
	List.map 
          (
           fun x -> 
             let identt = x (*id_to_ident x*)
             in 
	     BMod.Value_str (identt,B.VariablesConcrete (B.Var_Concrete(search_btype p x),search_sub s x))
          )
          id_list
    | Blast.VariablesAbstract id_list -> 
	(* recuperation d une eventuelle clause initialisation *)
	let ini = List.find (fun x -> match x with
			       | Blast.Initialisation _ -> true
			       | _ -> false) clause_list in
	let s = (match ini with
		   | Blast.Initialisation sub -> sub
		   | _ -> Error.error "No initialisation for abstract variable") in
	  (* recuperation de la clause invariant *)
	let inv = List.find (fun x -> match x with
			       | Blast.Invariant _ -> true
			       | _ -> false) clause_list in
	let p = (match inv with
		   | Blast.Invariant pred -> pred
		   | _ -> Error.error "No invariant for abstract variable") in
	  List.map 
          (
           fun x -> let identt = x (* id_to_ident x*)
           in 
	   BMod.Value_str (identt,B.VariablesAbstract(B.Var_Abstract(search_btype p x),search_sub s x))
          )
	  id_list
(*    | Blast.VariablesHidden id_list -> 
	(* recuperation d une eventuelle clause initialisation *)
	let ini = List.find (fun x -> match x with
			       | Blast.Initialisation _ -> true
			       | _ -> false) clause_list in
	let s = (match ini with
		   | Blast.Initialisation sub -> sub
		   | _ -> Error.error "Error in initialisation of concrete variable") in
	  (* recuperation de la clause invariant *)
	let inv = List.find (fun x -> match x with
			       | Blast.Invariant _ -> true
			       | _ -> false) clause_list in
	let p = (match inv with
		   | Blast.Invariant pred -> pred
		   | _ -> Error.error "Error in invariant of concrete variable") in
	  List.map (fun x -> let identt = x
(*DP 30/9/03
(match x with
                                     	     | Blast.Id (Pident pa) -> pa
                                   	     | _ -> raise (Error "Need a correct identifiers"))
*)
 in 
		      BMod.Value_str (identt,B.VariablesHidden (search_valtype p x,search_sub s x)))
	    id_list
*)
    | Blast.Promotes id_list -> 
	raise (NotYetImplemented ("clause","Promotes"))
(*    | Blast.Assertions pred_list -> 
        [BMod.Value_str (Ident.create "assertions",B.Assertions (List.map expr pred_list))]
*)
(*	raise (NotYetImplemented ("clause","Assertions"))*)
    | Blast.Operations operation_list -> List.map operation operation_list
(*DP G�rer le open dans BMod *)
(*    | Blast.Sees id_list -> 
	List.map (BMod.Open) id_list 
*)
    | Blast.Uses id_list ->
	List.map (fun x -> BMod.Open_str x) id_list
    | Blast.Extends instance_list -> 
	List.map (fun x -> let id = instance x in BMod.Open_str (id)) instance_list
    | Blast.Includes instance_list -> 
	List.map (fun x -> let id = instance x in BMod.Open_str (id)) instance_list
    | Blast.Imports instance_list -> 
	List.map (fun x -> let id = instance x in BMod.Open_str (id)) instance_list
(**)
    | _ -> 	raise (NotYetImplemented ("clause","unknow"))
and setname_to_btype s = 
  match s with 
    | B.NAT -> B.Z
    | B.NATURAL -> B.Z
    | B.NAT1 -> B.Z
    | B.NATURAL1 -> B.Z
    | B.INTEGER -> B.Z
    | B.INT -> B.Z
    | B.STRING -> B.String
    | B.BOOL -> B.Bool
and btype_of_setName x = setname_to_btype x 
and search_btype_in_exprSet exprset =
  match exprset with
  | B.SetEmpty -> B.Untyped
  | B.SetRange(a,b) -> B.TypeNatSetRange(a,b)
  | B.SetPredefined(setName) -> btype_of_setName setName
  | B.SetEnum(_) -> raise (NotYetImplemented("search_btype_in_exprSet", "SetEnum"))
  | B.SetCompr(_) -> raise (NotYetImplemented("search_btype_in_exprSet", "SetCompr"))
  | B.SetComprPred(_) -> raise (NotYetImplemented("search_btype_in_exprSet", "SetComprPred"))
  | B.SetUnionQuantify(_) -> raise (NotYetImplemented("search_btype_in_exprSet", "SetUnionQuantify"))
  | B.SetInterQuantify(_) -> raise (NotYetImplemented("search_btype_in_exprSet", "SetInterQuantify"))

and search_btype_in_expr expr =
  match expr with
  | B.ExprSet (B.SetPredefined s) -> setname_to_btype s
  | B.ExprParen(e) -> search_btype_in_expr e
(*DP 30/9/03  | B.ExprSeq(exprSeq) -> B.Sequence(search_btype_in_expr exprSeq)*)
  | B.ExprSet(exprSet) -> search_btype_in_exprSet exprSet
(*  | RelSet(expr1,expr2) -> ??? *)
(*  | RelSeqComp(expr1,expr2) -> ??? *)
(*
  | ExprId(id) -> 
  | ExprPred of predicate 
*)
  | _ -> Error.error "This expression is not a type expression "

and search_btype p id =
  match p with
    | B.PredAtom (Bbop.In,e1,e2) -> 
        let i = 
          (match e1 with
	  | B.ExprId iden -> iden
	  | _ -> Error.error "Need an identifiant")
        in
	if equalid i (Pident(id)) then search_btype_in_expr e2
	else raise (NoInvariant "search_btype in constructor PredAtom")
    | B.PredBin (Bbop.And,p1,p2) ->                                  
        (try search_btype p1 id with NoInvariant s -> search_btype p2 id)
    | B.PredParen(p) -> search_btype p id 
    | B.PredNegation(p) -> search_btype p id
    | _ -> raise (NoInvariant "pattern non exhaustif")

and string_of_id id = Ident.name id
(*DP 30/09/03
  match id with
  | Blast.Id(path) -> BTyping.name_of_path path
  | Blast.IdTy(id,_) -> string_of_id id
*)

and equalid x y =
  (path_to_string x) = (path_to_string y) 

and mem_id_in_idlist id idlist = 
  match idlist with 
    [] -> false
  | h::t -> 
      (equalid id h) || (mem_id_in_idlist id t) 


and search_sub s id =
  match s with
    | B.SubstBecomeSuch (i,p) -> 
        if (mem_id_in_idlist (Pident(id)) i) then substitution s
        else raise (NoInitialisation "BecomeSuch")
    | B.SubstSetIn (i,e) -> 
        if (mem_id_in_idlist (Pident(id)) i)  then substitution s 
        else raise (NoInitialisation "SetIn")
    | B.SubstSequence (s1,s2) -> 
        (try search_sub s1 id with NoInitialisation s -> search_sub s2 id) 
    | B.SubstParallel (s1,s2) -> 
        (try search_sub s1 id with NoInitialisation s -> search_sub s2 id)
    | _ -> 
        raise (NoInitialisation "pattern non exaustif dans search_sub")
	
and operation (oper_name,oper_out,oper_in,body) =
  BMod.Value_str (id oper_name,B.Operations ([],[],substitution body)) (* provisoire *)
						    
(* voir pb sur l'oper_out *)	  

(*DP Grand netoyage...*)						    
and predicate p = p

and instance (pathident,expr_list) = pathident (* pour l instant....*)
and expr exp = exp
and exprSeq exp =
  match exp with
  | B.SeqEnum expr_list -> B.SeqEnum (List.map expr expr_list) 
  | B.SeqEmpty -> B.SeqEmpty
and exprSet exp = exp
and setname s = s
and number num = num
and substitution subst = subst
and set s =
  match s with
    | Blast.SetAbstDec i -> 
        BMod.Type_str (id i,(),B.SetAbstract(None))
    | Blast.SetEnumDec (i,id_list) -> 
	BMod.Type_str (id i,(),B.SetEnumerate(List.map id id_list))
    | Blast.SetRecordsDec (i,l) -> raise (NotYetImplemented("set", "SetRecordsDec"))
(*	BMod.Type_str (id i,(),B.RecordsType (List.map (fun x -> (Pident (id (fst x)),B.Untyped)) l))
*)

(*DP 30/9/03 Il serait int�ressant d'int�grer la phase de scoping dans cette phase ainsi que paut etre une phase 
   de transformation des Pident en Pdot pour les entit�s ext�rieures au composant.
*)
and id i = i
(*DP 30/9/03
  match i with
    | Blast.Id p ->
	let s = BTyping.name_of_path p in
 	  Ident.create s
    | Blast.IdTy (i',ty) -> raise (NotYetImplemented ("id","IdTy"))
*)


(*and path i =
  match i with
    | Blast.Id p ->
	let s = name_of_path p in
 	  Pident(Ident.create s)  (* voir pour faire le scoping � ce moment ci *)
    | Blast.IdTy (i',ty) -> raise (NotYetImplemented ("id","IdTy"))
	*)

let processing t = amn t















