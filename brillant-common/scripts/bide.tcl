#!/usr/bin/wish -f


# Georges Mariano (georges.mariano@inrets.fr)

proc balloon {w help} {
    bind $w <Any-Enter> "after 500 [list balloon:show %W [list $help]]"
    bind $w <Any-Leave> "destroy %W.balloon"
}

proc balloon:show {w arg} {
    if {[eval winfo containing  [winfo pointerxy .]]!=$w} {return}
    set top $w.balloon
    catch {destroy $top}
    toplevel $top -bd 1 -bg black
    wm overrideredirect $top 1
#    if {[string equal [tk windowingsystem] aqua]}  {
#	::tk::unsupported::MacWindowStyle style $top help none
#    }
    pack [message $top.txt -aspect 10000 -bg lightyellow \
	      -font fixed -text $arg]
    set wmx [winfo rootx $w]
    set wmy [expr [winfo rooty $w]+[winfo height $w]]
    wm geometry $top \
      [winfo reqwidth $top.txt]x[winfo reqheight $top.txt]+$wmx+$wmy
    raise $top
}

# Example:
#button  .b -text Exit -command exit
#balloon .b "Push me if you're done with this"
#pack    .b


# Copyright 2010

set base "/users/estas/mariano/AB/"

set specdir "spec"

set gopdir "bgop"

set latexdir "tex"

set projects [lsort -ascii [glob -type {d r w}  -nocomplain -tail -directory $base *]]







label .top  -text "Boulinette DashBoard" -relief raised -width 40
pack .top -side top  


set b boulinette
frame .$b -relief raised
pack .$b -side left

#pack .$b.all -side left

set actions "all dirs parsing latex txt typing gop clean"
   
# Super laid.
set project ""


proc project { } { 
    global project
    return $project
    
}

proc Boulinette { action } {
    global base project
    debug "boulinette -C $base/$project $action "
    set result [exec boulinette -C $base/$project $action ]
    debug $result
}

foreach action $actions {
    label .$b._$action -text $action -relief groove
    balloon .$b._$action "boulinette $action"
    pack .$b._$action -fill x -side top
    bind .$b._$action <Button-1>  "Boulinette $action "
}

set actions "P T G B X tX"

set call("P") "boulinette btyper"
set call("T") "boulinette btyper"
set call("G") "boulinette bgop "
set call("B") "boulinette bicoax"
set call("X") "boulinette check"
set call("tX") "boulinette latex"

set i 0
set colwidth 20


# foreach item  $mchs {
#     frame .$i -relief raised
#     # Component label
#     pack  .$i -side top 
#     button .$i.l -text $item -width 20 -anchor w
#     balloon .$i.l "edit with emacs $item"
#     pack .$i.l -side left 
#     bind .$i.l <Button-1> "exec emacs $item &" 

#     # Actions
#     foreach action $actions {
# 	label .$i._$action -text " $action " -relief groove
# 	balloon .$i._$action " $call("$action")"
# 	pack .$i._$action -fill x -side left
# 	bind .$i._$action <Button-1> "exec  $call("$action") $item" 
#     }
#     incr i
# }

#set model "$mchs $refs $imps"
#set bgopdirs [glob -nocomplain "$base/$gopdir/*/"]

set name "projects"
frame .$name -relief raised -padx 1 -pady 1
label .$name.label -text "PRJ" -relief raised -padx 1 -pady 1 -width $colwidth
pack .$name.label -side top
pack .$name -side left

listbox .lprojects  -selectbackground green  -listvariable projects -selectmode single  -height 40 -width 20
pack .lprojects -side left -in .$name

bind .lprojects <<ListboxSelect>> selection_projects
bind .lprojects <Double-Button-1> run_projects

set debugon "ok"

proc debug { msg } {
    global debugon
    if {$debugon != ""} {
	puts $msg
    }
}



proc selection_projects {} {
    debug "<<ListboxSelect>> .projects"
    foreach i [.lprojects curselection] { 
#	puts [lindex $::projects $i] 
    }
}

proc run_projects {} {
    global model xmls texs bgopdirs base specdir project
    foreach i [.lprojects curselection] { 
	set project [lindex $::projects $i] 
	set model [lsort -ascii [glob -type f -nocomplain -tail -directory $base/$project/spec  *.mch *.ref *.imp]]
	set xmls [lsort -ascii [glob  -type f -nocomplain -tail -directory $base/$project/xml        *.xml]]
	set texs [lsort -ascii [glob  -type f -nocomplain -tail -directory $base/$project/tex  *.tex]]
	set bgopdirs [lsort -ascii [glob -type d -nocomplain -tail -directory $base/$project/bgop/ *]]
    }
}


set mchs [glob -type f -nocomplain -tail -directory $base/$specdir *.mch]
set refs [glob -type f -nocomplain -tail -directory $base/$specdir *.ref]
set imps [glob -type f -nocomplain -tail -directory $base/$specdir *.imp]

set texs [glob -nocomplain "$base/$latexdir/*.tex"]





set name "spec"
frame .$name -relief raised -padx 1 -pady 1
label .$name.label -text "SPECS" -relief raised -padx 1 -pady 1 -width $colwidth
pack .$name.label -side top
pack .$name -side left

listbox .lb  -selectbackground green  -listvariable model -selectmode extended -height 40 -width $colwidth
pack .lb -side left -in .$name


bind .lb <<ListboxSelect>> selection_model
bind .lb <Double-Button-1> run_model


proc selection_model {} {
    debug "<<ListboxSelect>> .texs"
    foreach i [.lb curselection] { 
	debug [lindex $::model $i] 
    }
}

proc run_model {} {
    global base project
   foreach i [.lb curselection] { 
       set xmlfile "$base/$project/spec/[lindex $::model $i]"
	exec xdg-open  $xmlfile &
    }
}


# Dealing with xml files
set name "xml"
frame .$name -relief raised -padx 1 -pady 1
label .$name.label -text "XML" -relief raised -padx 1 -pady 1 -width $colwidth
pack .$name.label -side top
pack .$name -side left

set xmls [glob -nocomplain "$base/xml/*.xml"]

listbox .lxmls  -selectbackground green  -listvariable xmls -selectmode extended  -height 40 -width $colwidth
pack .lxmls -side left -in .$name


bind .lxmls <<ListboxSelect>> selection_xmls
bind .lxmls <Double-Button-1> run_xmls

balloon .lxmls "<Return> to XML view"

proc selection_xmls {} {
    debug "<<ListboxSelect>> .xmls"
    foreach i [.lxmls curselection] { 
	debug [lindex $::xmls $i] 
    }
}

proc run_xmls {} {
    global base project
   foreach i [.lxmls curselection] { 
       set file "$base/$project/xml/[lindex $::xmls $i]"
	exec xdg-open  $file &
    }
}




# Dealing with LaTeX files (in $base/tex directory)

set name "latex"
frame .$name -relief raised -padx 1 -pady 1
label .$name.label -text "LaTeX" -relief raised -padx 1 -pady 1 -width $colwidth
pack .$name.label -side top
pack .$name -side left

listbox .ltexs  -selectbackground green  -listvariable texs -selectmode extended  -height 40 -width $colwidth
pack .ltexs -side left -in .$name


bind .ltexs <<ListboxSelect>> selection_texs
bind .ltexs <Double-Button-1> run_texs

balloon .ltexs "<Return> to LaTeX edit"


proc selection_texs {} {
    debug "<<ListboxSelect>> .texs"
    foreach i [.ltexs curselection] { 
	debug [lindex $::texs $i] 
    }
}

proc run_texs {} {
    global base project
    foreach i [.ltexs curselection] { 
	set file "$base/$project/tex/[lindex $::texs $i]"
	exec xdg-open  $file &
    }
}



set name "bgopdirs"
frame .$name -relief raised -padx 1 -pady 1
label .$name.label -text "BGOP" -relief raised -padx 1 -pady 1 -width $colwidth
pack .$name.label -side top
pack .$name -side left

listbox .lbgopdirs -selectbackground green -selectmode single -listvariable bgopdirs   -height 40 -width $colwidth
pack .lbgopdirs -side left -in .$name

bind .lbgopdirs <Double-Button-1> run_bgopdirs



# Dealing with COQ files (in $base/bgop/<selected-dir>) directory)

set name "coq"
frame .$name -relief raised -padx 1 -pady 1
label .$name.label -text "COQ" -relief raised -padx 1 -pady 1 -width $colwidth
pack .$name.label 
pack .$name -side top

listbox .lcoqs  -selectbackground green  -listvariable coqs -selectmode extended  -height 40 -width $colwidth
pack .lcoqs -in .coq -side left


bind .lcoqs <<ListboxSelect>> selection_coqs

bind .lcoqs <Return> run_coqs
bind .lcoqs <Double-Button-1> run_coqs

balloon .lcoqs "(<Return>/<Button-2>) to (multipe/single) edit"


proc selection_coqs {} {
    debug "<<ListboxSelect>> .coqs"
    foreach i [.lcoqs curselection] { 
	debug [lindex $::coqs $i] 
    }
}

proc run_coqs {} {
    global project base gopdir
 #   set files ""
    foreach i [.lcoqs curselection] { 
	set file $base/$project/bgop/$gopdir/[lindex $::coqs $i] 
#	set files "$files $file"
    }
    exec coqide -I /usr/share/brillant/bicoax/lib  $file &
}





proc run_bgopdirs {} {
    global coqs base project gopdir
    foreach i [.lbgopdirs curselection] { 
	set dir [lindex $::bgopdirs $i]
	set gopdir $dir
	set coqs [lsort -ascii  [glob -nocomplain  -type f -nocomplain -tail -directory $base/$project/bgop/$dir *.v]]
    }
}