#!/usr/bin/make -sf

## $Date$   
## $Revision$  
## $Author$

# In the current directory

# MACHINE components
MCHGLOB=*
mchs=$(wildcard $(MCHGLOB).mch)
# REFINEMENT components
REFGLOB=*
refs=$(wildcard $(REFGLOB).ref)
# IMPLEMENTATION components
IMPGLOB=*
imps=$(wildcard $(IMPGLOB).imp)
# BXML files 
xmls=$(wildcard *.xml)

POGLOB=*  # selection des PO par globulation
phxs=$(wildcard $(POGLOB).phx)


# System commands
MKDIR=@mkdir -p
MV=mv -u -v
RM=@rm -f
XARGS=xargs --no-run-if-empty

# BRILLANT tools & stuff
XSLDIR=/usr/share/brillant/
# 
xsldir=/usr/share/brillant/


BINCLUDES=

BTYPER=-@btyper $(BINCLUDES) 
#BTYPER=-@btyper2
BPARSER=-@bparser $(BINCLUDES) 
BGOP_OPTS=--AB
BGOP_OPTS+=--debug
BGOP=-@bgop $(BINCLUDES) $(BGOP_OPTS) 

XSLTPROC=xsltproc


specdir=./spec/

xmldir=./xml/

texdir=./tex/

texs=$(wildcard $(texdir)/*.tex)

txtdir=./txt/

gopdir=./bgop/

# System level configuration (optional)
#-include /etc/boulinette.conf

# User level configuration (optional)
#-include $(HOME)/.boulinette.conf

BOULINETTE_FILE=/usr/local/bin/boulinette
BOULINETTE=boulinette

TAG=$(shell date -I)

help:
	@grep \#\# $(BOULINETTE_FILE)
	@echo "BPARSER is $(BPARSER)"
	@echo "BGOP is $(BGOP)"

autocheck: # check if everything is in place for boulinette to work.
	$(BPARSER) --help 2>/dev/null ; echo "bparser ok" 
	$(BGOP) --help 2>/dev/null ; echo "bgop ok" 
	@echo "autocheck ok" 

all: dirs parsing latex txt typing gop clean

dirs: #
	$(MKDIR)  $(gopdir)
	$(MKDIR)  $(xmldir)
	$(MKDIR)  $(texdir)
	$(MKDIR)  $(txtdir)
	-@chmod a-w $(specdir)/*.mch $(specdir)/*.ref $(specdir)/*.imp


#-# Target template

target: target-pre  target-run target-post

target-pre:
	@echo "Preparing for target"
target-run:
	@echo "Running for target"
target-post:
	@echo "Cleaning for target"

bparser:parsing ## Parsing 
parser: parsing

parsing-pre: #parsing-do parsing-post
	@echo "Preparing for parsing $(mchs)"
	@mkdir -p $(xmldir)
	@$(BOULINETTE) -C $(specdir)  parsing-do

parsing-do: $(mchs:.mch=.mch.xml) $(refs:.ref=.ref.xml) $(imps:.imp=.imp.xml)

parsing: parsing-pre parsing-do parsing-post
#	@cd $(specdir) ; mv *.*.xml ../$(xmldir)

parsing-post:
	@find $(xmldir)/*.*.xml -size 0 | wc -l



check: ## XML checking with rxp
	-@$(BOULINETTE) -C $(xmldir) xmlcheck 
	-cd $(xmldir) ; find *.rxp -size +0

xmlcheck: $(xmls:.xml=.rxp)
	# echo $(xmls)

preuve: preuve-clean bphox phox 

preuve-clean:
	-@find . -name *.ph* | $(XARGS) rm

prove: preuve

DIRS=$(shell find . -type d | grep -v svn )

proof:
	$(BOULINETTE) -C $(gopdir) lapreuve

lapreuve:
	-@for i in $(DIRS); \
	do \
	echo $$i; \
	boulinette -s -C $$i phox  ; \
	boulinette -s -C $$i bphox  ; \
	done

stats:
	@echo -n $(TAG) >> STATS
	@echo -n "  v " `find . -name *.v | wc -l` >> STATS
	@echo -n "  vo " `find . -name *.vo | wc -l` >> STATS
	@echo -n "  poxml " `find bgop -name *.xml | wc -l` >> STATS
	@echo " " >> STATS
	@uniq STATS > STATS.1 ; mv STATS.1 STATS
	cat STATS

notproved:
	 @find . -name "*.v*" | grep -v log | sort | awk -F "." '{print $$2}' | uniq -c | grep ' 1 ' | awk '{print "."$$2".v"}'

unprove: # Remove all proofs (*.pho)
	-@rm -rf *.vo

bphox:  $(xmls:.xml=.phx) # Conversion XSLT po/xml->phx

phox:  $(phxs:.phx=.phi) # Conversion XSLT po/xml->phx


typer: typing


btyper: typing
typer: typing

retyping: typing-clean typing


typing: typing-do ## Typing with btyper
	-@$(BOULINETTE) -C $(specdir) typing-do

typing-do: $(mchs:.mch=.mtc) $(refs:.ref=.rtc) $(imps:.imp=.itc)

typing-clean:
	@rm -rf $(specdir)/*.?tc

# Default : xml input to LaTeX is provided by bparser
prelatex=bparser

latex: $(prelatex) latex-clean latex-do latex-post latex-gop ## LaTeX 

latex-clean:
	#-$(RM) $(texdir)/*.tex

latex-do:
	-@$(BOULINETTE)  -C $(xmldir) latex-clean
	-@$(BOULINETTE)  -C $(xmldir) latex-run	
	-@$(BOULINETTE)  -C $(xmldir) latex-post

latex-run: $(xmls:.xml=.tex)

latex-post:
#	-$(MKDIR) ../$(texdir) ; $(MV) *.tex ../$(texdir)
	echo > list-of-files.tex
	for file in $(texs); \
	do \
	  echo \\Binput{$$file} >>list-of-files.tex; \
	done

latex-gop:
	cat $(gopdir)/*/*.tex | sort > pos.tex

text:  txt-clean txt-do txt-post

txt-clean:  # removing previously generated txt-files
	@$(RM) $(txtdir)/$@

txt-do: ## Converting BXML files into Btxt files
	-$(BOULINETTE) -s -C $(xmldir) txt-clean
	-$(BOULINETTE) -s -C $(xmldir) txt-run	
	-$(BOULINETTE) -s -C $(xmldir) txt-post

txt-run: $(xmls:.xml=.txt)

txt-post:
	-@$(MKDIR)  ../$(txtdir) ; $(MV) *.txt ../$(txtdir)


# Proof obligation generation


bgop: gop

gop: gop-pre gop-do gop-post

regop: gop-clean gop

gop-clean:
	-@rm -rf $(specdir)/*.gop ; 
	-@rm -rf $(gopdir)/*/*.xml ; 
	-@rm -rf $(gopdir)/*/*.log ; 

gop: gop-pre gop-post

gop-pre:
	$(BOULINETTE) -C $(specdir) gop-do

gop-do: $(mchs:.mch=.gop) $(refs:.ref=.gop) $(imps:.imp=.gop)

gop-post:
	@touch $(gopdir)/.bgop$(TAG)
	grep "error:" $(gopdir)/*.log | awk -F ":" '{print $$3}' | sort | uniq -c > report-$(TAG).bgop
	@cat report-$(TAG).bgop

clean: latex-clean gop-clean 
#	-rm *.log

.SUFFIXES: .mch .ref .imp .xml .tex .txt .gop .mtc .rtc .itc .rxp .phx .pho .phi



.xml.txt:
	-$(XSLTPROC) --novalid $(XSLDIR)/bxml2txt.xsl $< > $@ 
#	-bxml2b -s xsldir=$(XSLDIR) file=$< > $@ 2> $<.log

.xml.tex: $<
	-$(XSLTPROC) --novalid $(XSLDIR)/bxml2tex-ng.xsl $< > $@
	-mv  $@ ../$(texdir)/
#	-bxml2tex -s xsldir=$(XSLDIR) file=$< > $@ 2> $<.log

.xml.rxp: $<
#	-@rxp  -V $< 2> $@
	-@xmlstarlet val $< 2> $@

.xml.phx: $<
#	@echo $<
	$(XSLTPROC) --novalid $(XSLDIR)/bphox/bgop2phox.xsl $< > $@ 

PHOXTIMEOUT=5

PHOX=timeout  $(PHOXTIMEOUT) phox -c
#PHOX=phox -c
.phx.phi: $<
	@echo $<
	-$(PHOX) $<
#	-phox -c $<  & sleep $(PHOXTIMEOUT) ; kill  $$!

# %.gop == log of bgop run
#%.gop: %.mch
#
.mch.gop: $<
	@echo $<
	-$(BGOP)  --dest ../$(gopdir)  $< 2> ../$(gopdir)/$@.log
	@touch $@

%.gop: %.ref
	@echo $<
	-$(BGOP) --dest ../$(gopdir)  $<  2>../$(gopdir)/$@.log

%.gop: %.imp
	@echo $<
	-$(BGOP) --dest ../$(gopdir)  $<  2>../$(gopdir)/$@.log

# Type checking files

.mch.mtc: $<
	@echo typing $<
	-@$(BTYPER) $< > ../xml/$<.xml  2>$@.log

.ref.rtc: $<
	@echo typing $<
	-@$(BTYPER) $< > ../xml/$<.xml  2>$@.log

.imp.itc: $<
	@echo typing $<
	-@$(BTYPER) $< > ../xml/$<.xml  2>$@.log


# Parsing files

%.mch.xml: %.mch
	@echo "parsing $<  $<.xml"
	-$(BPARSER) $< | xmlstarlet fo  > $<.xml ; mv  $<.xml ../$(xmldir)

%.ref.xml: %.ref
	@echo "parsing $<"
	-$(BPARSER) $<  | xmlstarlet fo  > $<.xml ; mv  $<.xml ../$(xmldir)


%.imp.xml: %.imp
	@echo "parsing $<"
	-$(BPARSER) $<  | xmlstarlet fo > $<.xml  ; mv  $<.xml ../$(xmldir)

