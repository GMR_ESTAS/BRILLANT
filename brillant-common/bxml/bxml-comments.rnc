# -*- coding: utf-8 -*-

#########################
###### Expressions ######
#########################
variable = Variable

# TODO, still missing (from Rodin)
# BOOL  FALSE TRUE
# bool  card  dom  finite id
# inter max   min  mod    pred
# prj1  prj2  ran  succ   union
# (from classical B)
# Sequences, trees, etc

# Basically:
# U+0030 U+0031 U+0032 U+0033 U+0034
# U+0035 U+0036 U+0037 U+0038 U+0039
number = Number

# TODO:
# ℙ  U+2119        powerset
# ℙ1 U+2119 U+0031 set of non-empty subsets

constant =
    "BOOL"
  | "NAT"
  | "NAT1"
  | "INT"
#TODO: Darn, NAT is ℕ in Rodin, while it's NATURAL that is ℕ in classical B
  | "NATURAL" | "ℕ" # U+2115
  | "NATURAL1" | "ℕ1" # U+2115 U+0031
  | "INTEGER" | "ℤ" # U+2124
#TODO: conflict with set_manipulation
  | "EmptySet" | "∅" # U+2205
  | "TRUE"
  | "FALSE"

unary_function = 
    "Succ" 
  | "Pred" 
  | "Minus"

binary_function =
    "Add" | "+" | "+" # U+002B
  | "Sub"  | "-" | "−" # U+2212
  | "Mult"  | "*" | "∗" # U+2217
  | "Div"  | "/" | "÷" # U+00F7
#TODO: not sure about ASCII for pow 
  | "Pow"  | "**" | "^" # U+005E
  | "Mod" | "mod" # No unicode 
  | "UpTo" | ".." | "‥" # U+2025

relation =
   "Relation" | "<->" | "↔" # U+2194 B,Bev
 | "TotalRelation" | "<<->" | "" # U+E100 Bev
 | "SurjRelation" | "<->>" | "" # U+E101 Bev
 | "TotalSurjRelation" | "<<->>" | "" # U+E102 Bev
 | "PartialFunction" | "+->" | "⇸" # U+21F8 B,Bev
 | "TotalFunction" | "-->" | "→" # U+2192 B,Bev
 | "PartialInjection" | ">+>" | "⤔" # U+2914 B,Bev
 | "TotalInjection" | ">->" | "↣" # U+21A3 B,Bev
 | "PartialSurjection" | "+->>" | "⤀" # U+2900 B,Bev
 | "TotalSurjection" | "-->>" | "↠" # U+21A0 B,Bev
 | "PartialBijection" | ">+>>" | "" # B
 | "Bijection" | ">->>" | "⤖" # U+2916 B,Bev

# With { U+007B left curly bracket
# With } U+007D right curly bracket
set_manipulation =
   "Maplet" | "|->" | "↦" # U+21A6
 | "EmptySet" | "{}" | "∅" # U+2205
 | "Intersection" | "/\" | "∩" # U+2229
 | "Union" | "\/" | "∪" # U+222A
 | "SetMinus" | "\" | "∖" # U+2216
# TODO: not sure about the ASCII of the product
 | "Product" | "><" | "×" # U+00D7

# With [ U+005B left square bracket
# With ] U+005D right square bracket
relation_manipulation = 
#TODO: duplicate Maplet ? (see set_manipulation)
   "Maplet" | "|->" | "↦" # U+21A6
 | "Override" | "<+" | "" # U+E103
 | "BackwardComposition" | "" | "∘" # U+2218 Bev
 | "ForwardComposition" | ";" | ";" # U+003B
#TODO: not sure about the ASCII (see cartesian product in set_manipulation)
 | "DirectProduct" | "(><)" | "⊗" # U+2297
 | "ParallelProduct" | "||" | "∥" # U+2225
 | "Inverse" | "~" | "∼" # U+223C
 | "DomainRestriction" | "<|" | "◁" # U+25C1
 | "DomainSubstraction" | "<<|" | "⩤" # U+2A64
 | "RangeRestriction" | "|>" | "▷" # U+25B7
 | "RangeSubstraction" | "|>>" | "⩥" # U+2A65

binder = 
    "Lambda" | "%" | "λ" # U+03BB
#TODO: not sure about those two
  | "GenIntersection" | "INTER" | "⋂" # U+22C2
  | "GenUnion" | "UNION" | "⋃" # U+22C3
  | "SetComprehension" | "|" | "∣" # U+2223

binary_relation =
    "Equal" | "=" # U+003D
  | "NotEqual" | "≠" # U+2260
  | "Less" | "<" # U+003C
  | "LessEqual" | "≤" # U+2264
  | "Greater" | ">" # U+003E
  | "GreaterEqual" | "≥" # U+2265
  | "In" | "∈" # U+2208
  | "NotIn" | "∉" # U+2209
  | "Subset" | "⊂" # U+2282
  | "SubsetEqual" | "⊆" # U+2286
  | "NotSubset" | "⊄" # U+2284
  | "NotSubsetEqual" | "⊈" # U+2288

#TODO: text ? Why not xsd:integer ?...
Number = 
   element Number { 
      attribute value { text } }
#TODO: variables with ID and IDREF attributes
#TODO: try to enforce some rules on the text, maybe ? (not starting with [0-9], etc)
Variable = 
   element Variable { 
      attribute name { text } }
FunctionCall =
  element FunctionCall { 
     attribute name { text }, 
     expression* }
BoundExpression =
  element BoundExpression {
    attribute name { binder }, 
     variable+, 
     expression
  }
BinaryExpression =
  element BinaryExpression {
    attribute name { binary_function }, 
     expression, 
     expression
  }
UnaryExpression =
  element UnaryExpression { 
     attribute name { unary_function }, 
     predicate }
Constant = 
   element Constant { 
      attribute name { constant } }

expression =
    Variable
  | Constant
  | Number
  | UnaryExpression
  | BinaryExpression
  | BoundExpression
  | FunctionCall

########################
###### Predicates ######
########################

# Still missing (as appearing in the spec, or as a comment like the dot)
# ⊤  U+22A4 true predicate 
# ⊥  U+22A5 false predicate
# , U+002C comma

unary_connector = 
   "Not" | "not" | "¬" # U+00AC
#TODO: is it relevant to have parentheses ? In the ASCII syntax, yes,
# but XML is structured, we might want not to retain all the parsing
# information
  | "Paren" | "()" # U+0028 U+0029
binary_connector = 
   "And" | "&" | "∧" # U+2227
  | "Or" | "or" | "∨" # U+2228
  | "Implies" | "=>" | "⇒" # U+21D2
  | "Equiv"| "<=>" | "⇔" # U+21D4

# With middle dot "·" U+00B7
quantifier = 
    "Forall" | "!" | "∀" # U+2200
  | "Exists" | "#" | "∃" # U+2203

BoundPredicate =
   element BoundPredicate {
      attribute name { quantifier }, 
      variable+, 
      predicate
   }
BinaryPredicate =
   element BinaryPredicate {
      attribute name { binary_connector }, 
      predicate, 
      predicate
   }
UnaryPredicate =
   element UnaryPredicate { 
      attribute name { unary_connector }, predicate }
AtomicPredicate =
  element AtomicPredicate {
     attribute name { binary_relation }, 
     expression, 
     expression
  }
predicate =
    AtomicPredicate
  | UnaryPredicate
  | BinaryPredicate
  | BoundPredicate



###############################
###### Proof obligations ######
###############################

#TODO: Hmmm, rather keep only ProofObligations ?
ProofObligation =
  element ProofObligation { predicate }
proof_obligation = ProofObligation
#Force to have at least 2 ?
ProofObligations =
  element ProofObligations {
    proof_obligation,
    proof_obligation+
  }
start = BoundPredicate | ProofObligations
