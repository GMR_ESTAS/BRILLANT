<xsl:stylesheet version="1.0"
		xmlns:xsl = "http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/TR/REC-html40">

 <xsl:output method = "html"
	    omit-xml-declaration = "yes"
	    indent = "yes"
	    doctype-public = "-//w3c//dtd html 4.0 transitional//en"/>

<xsl:template match = "AMN">
  <HTML>
  <HEAD>
  <TITLE></TITLE>
  <META NAME = "Author" CONTENT = "Mariano Georges"/>
  <META NAME = "Copyright" CONTENT = "INRETS-ESTAS 2001"/>
  <META NAME = "E-mail" CONTENT = "georges.mariano@inrets.fr"/>
  <META NAME = "Keywords" CONTENT = "méthode B methode formelle"/>
  <script language="JavaScript">
       function navigation1() {
         lieu =
       document.formulaire1.liste1.options[document.formulaire1.liste1.options.
       selectedIndex].value;
         top.frames[2].location.href = lieu;
         }
	function navigation2() {
         lieu =
       document.formulaire2.liste2.options[document.formulaire2.liste2.options.
       selectedIndex].value;
         top.location.href = lieu;
         }
        function navigation3() {
         lieu =
       document.formulaire3.liste3.options[document.formulaire3.liste3.options.
       selectedIndex].value;
         top.location.href = lieu;
         }
    function navigation4() {
    lieu =
    document.formulaire4.liste4.options[document.formulaire4.liste4.options.
    selectedIndex].value;
    top.location.href = lieu;
    }
    function name_of_file() {
    var s1 = window.name;
    var s2 = s1.substring(6,s1.length);
    return(s2);
    }
  </script> 
</HEAD>
<BODY BGCOLOR = "white" VLINK = "blue" LANG = "EN">
  <BR/>
  <xsl:apply-templates select="descendant::REFINES"/>
  <xsl:apply-templates select="descendant::SEES"/>
  <xsl:apply-templates select="descendant::INCLUDES"/>
  <xsl:apply-templates select="descendant::OPERATIONS"/>
</BODY>
</HTML>
</xsl:template>

<xsl:template match="REFINES">
  <B>Refines:</B><BR/>
  <form name="formulaire3">
  <select name="liste3" OnChange="navigation3();">
  <xsl:call-template name="link_sees"/>
</select>
  </form>
</xsl:template>

<xsl:template match="INCLUDES">
  <B>Includes:</B><BR/>
  <form name="formulaire4">
  <select name="liste4" OnChange="navigation4();">
  <xsl:call-template name="link_sees"/>
  </select>
  </form>
</xsl:template>

<xsl:template match="SEES">
  <B>Sees:</B><BR/>
  <form name="formulaire2">
  <select name="liste2" OnChange="navigation2();">
  <xsl:call-template name="link_sees"/>
  </select>
  </form>
</xsl:template>

<xsl:template name="link_sees">
<xsl:for-each select="descendant::id">
<OPTION>
   <xsl:attribute name="VALUE">
   <xsl:value-of select="."/><xsl:text>.mch.frames.html</xsl:text>
   </xsl:attribute>
   <xsl:value-of select="."/>
</OPTION>
</xsl:for-each>
</xsl:template>

<xsl:template match="OPERATIONS">
  <B>Opérations:</B><BR/>
  <form name="formulaire1">
  <select name="liste1" OnChange="navigation1();">
  <xsl:apply-templates select="descendant::OperName"/>
  </select>
  </form>
</xsl:template>

<xsl:template match="OperName">
 <xsl:call-template name="link_op"/>
</xsl:template>

<xsl:template name="link_op">
  <OPTION>
   <xsl:attribute name="VALUE">
   <xsl:text>&amp;{name_of_file()};#</xsl:text><xsl:apply-templates select="id"/>
   </xsl:attribute>
   <xsl:apply-templates select="id"/>
  </OPTION>
  
</xsl:template>
</xsl:stylesheet>













