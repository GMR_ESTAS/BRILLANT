<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>

<!--                    $Id:                                       -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="xml"

	    version="1.0"
	    encoding="ISO-8859-1"
            omit-xml-declaration="no"
	    standalone="no"
	    indent="yes"
	    media-type="text/xml"/>

<xsl:template match="/">

<xsl:copy-of select="."/>

</xsl:template>

</xsl:stylesheet>