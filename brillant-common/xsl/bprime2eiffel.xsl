<!-- Feuille de style B/XML vers Eiffel -->
<!-- $Id$ -->

  	

<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns="http://www.w3.org/TR/REC-html40">
  
<xsl:output method="text" omit-xml-declaration="yes"/>
<!-- selectionne la tete -->

<xsl:template match="AMN">
  <xsl:apply-templates select="child::*"/>
</xsl:template>


<!-- MRB 7 p 109 -->
<xsl:template match="MACHINE">
  <xsl:apply-templates select="Head" mode="open"/>
  <xsl:apply-templates select="ClausesList"/>
  <xsl:apply-templates select="Head" mode="close"/>
</xsl:template>

<!-- LISTE DES CLAUSES A EXECUTER -->

<xsl:template match="ClausesList">
  <xsl:text>feature
</xsl:text>
  <xsl:apply-templates select="CVARIABLES"/>
  <xsl:apply-templates select="/AMN/MACHINE/Head/HeadPar"/>
<xsl:apply-templates select="INITIALISATION"/>
<xsl:apply-templates select="OPERATIONS"/>
<xsl:apply-templates select="INVARIANT"/>

</xsl:template>

<!-- entete de la machine (Clause Head) -->

<xsl:template match="Head" mode="open">
 <xsl:text>class  </xsl:text>
<xsl:apply-templates select="id"/>
<xsl:text> 

</xsl:text>
</xsl:template>

<xsl:template match="Head" mode="close">
  <xsl:text>
end </xsl:text>
</xsl:template>

<!-- determine soit le nom soit le type de la variable -->
<xsl:template match="id">
  <xsl:choose>
    <xsl:when test="text()='NAT'">
       <xsl:text> INTEGER </xsl:text>
    </xsl:when>
    <xsl:when test="text()='INT' or text()='INTEGER'">
      <xsl:text> INTEGER </xsl:text>
    </xsl:when>
    <xsl:when test="text()='BOOL'">
      <xsl:text>BOOLEAN </xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="."/>
    </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<!-- DIFFERENTS ACCES AU VALEURS TYPEES -->


<xsl:template match="idtyped">
 <xsl:apply-templates select="child::*[position()=1]"/>
 <xsl:text> : </xsl:text>
 <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="idtyped" mode="param">
 <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>_param</xsl:text>
 <xsl:text> : </xsl:text>
 <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="idtyped" mode="affectation">
 <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> := </xsl:text>
 <xsl:apply-templates select="child::*[position()=1]"/>
 <xsl:text>_param</xsl:text>
</xsl:template>

<xsl:template match="idtyped" mode="type">
  <xsl:apply-templates select="child::*[position()=2]"/>
 </xsl:template>

<xsl:template match="idtyped" mode="nom">
  <xsl:apply-templates select="child::*[position()=1]"/>
 </xsl:template>

<!-- determine le codage pour les liste d'id -->

<xsl:template match="idList">
  <xsl:for-each select="child::*">
    <xsl:apply-templates select="."/>
    <xsl:text>
</xsl:text>
  </xsl:for-each>
</xsl:template>

<xsl:template match="idList" mode="subst">
  <xsl:for-each select="child::*">
    <xsl:apply-templates select="."/>
  </xsl:for-each>
</xsl:template>

<!-- Declaration des tableaux -->

<xsl:template match="FunType">
  <xsl:text>ARRAY[</xsl:text>
       <xsl:apply-templates select="child::*[position()=2]"/>
<xsl:text>]</xsl:text>
</xsl:template>

<!-- declaration des variables concretes -->
<!-- MRB 7.18 p 153 -->

<xsl:template match="CVARIABLES">
  <xsl:apply-templates select="child::*"/>
</xsl:template>

<xsl:template match="HeadPar">
  <xsl:for-each select="child::*">
     <xsl:apply-templates select="."/>
     <xsl:if test="position()!=last()">
       <xsl:text>;</xsl:text>
     </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="HeadPar" mode="param">
  <xsl:for-each select="child::*">
     <xsl:apply-templates select="." mode="param"/>
     <xsl:if test="position()!=last()">
       <xsl:text>;</xsl:text>
     </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="HeadPar" mode="affectation">
  <xsl:for-each select="child::*">
     <xsl:apply-templates select="." mode="affectation"/>
    </xsl:for-each>
</xsl:template>

<!-- declaration d'intialisation -->
<!-- MRB 7.22 p 161 -->

<xsl:template match="INITIALISATION">
<xsl:text>
make</xsl:text>
<xsl:call-template name="parametre"/>
<xsl:text>is
do
</xsl:text>
<xsl:call-template name="init_parametre"/>
<xsl:apply-templates select="child::*"/>
<xsl:text>
end
</xsl:text>
</xsl:template>

<xsl:template name="parametre">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="/AMN/MACHINE/Head/HeadPar" mode="param"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template name="init_parametre">
  <xsl:apply-templates select="/AMN/MACHINE/Head/HeadPar" mode="affectation"/>
 <xsl:text>
</xsl:text>
 </xsl:template>



 <!-- affectation 'normale' -->
 <!-- MRB 6.3 p 86 -->

<xsl:template match="SetEqualIds">
  <xsl:apply-templates select="child::*[position()=1]" mode="subst"/>
  <xsl:text>:=</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match="Expr">
  <xsl:apply-templates select="child::*"/>
</xsl:template>

<xsl:template match="ExprList">
  <xsl:apply-templates select="child::*"/>
</xsl:template>


<xsl:template match="Number">
  <xsl:value-of select="."/>
</xsl:template>

<!-- MRB 6.15 p 101 -->

<xsl:template match="SubstSeq">
  <xsl:for-each select="child::*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
    <xsl:text>
</xsl:text>
   </xsl:if>
   </xsl:for-each>
</xsl:template>

<!-- pour le produit cartesien -->
<!-- PAS ENCORE IMPLEMENTE -->

<xsl:template match="SetMul">
  <xsl:text disable-output-escaping="yes"> &lt;--- partie a effacer pour eviter erreur de compilation</xsl:text>
</xsl:template>


<!-- MRB 7.23 p 164 -->

<!-- les Operations -->
<xsl:template match="OPERATIONS">
  <xsl:apply-templates select="child::*"/>
</xsl:template>

<xsl:template match="Operation">
  <xsl:apply-templates select="OperHead"/>
   <xsl:apply-templates select="OperBody"/>
  <xsl:text>
end
</xsl:text>
</xsl:template>

<xsl:template match="OperHead">
  <xsl:choose>
    <xsl:when test="count(OperOut/child::*)=1">
      <xsl:apply-templates select="OperName"/>
      <xsl:if test="count(OperIn/child::*)>0">
      <xsl:text>(</xsl:text>
      <xsl:apply-templates select="OperIn"/>
      <xsl:text>)</xsl:text>
    </xsl:if>
      <xsl:text>:</xsl:text>
      <xsl:apply-templates select="OperOut" mode="parametre"/>
      <xsl:text> is
</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="OperName"/>
      <xsl:if test="count(child::*)>1">
        <xsl:text> (</xsl:text>
        <xsl:apply-templates select="OperIn" />
          <xsl:apply-templates select="OperOut"/>
          <xsl:text>)</xsl:text>
        </xsl:if>
      <xsl:text> is
</xsl:text>
    </xsl:otherwise>
  </xsl:choose>

</xsl:template>

<xsl:template match="OperName">
  <xsl:apply-templates select="id"/>
</xsl:template>

<!-- pour les fonctions -->

<xsl:template match="OperIn">
  <xsl:for-each select="idList/child::*">
    <xsl:apply-templates select="."/>
  </xsl:for-each>
</xsl:template>

<xsl:template match="OperOut" mode="parametre">
  <xsl:apply-templates select="idList/idtyped" mode="type" />
 </xsl:template>

<xsl:template match="OperOut">
  <xsl:apply-templates select="idList/idtyped"/>
 </xsl:template>


<!-- corps des fonctions -->

<xsl:template match="OperBody">
  <xsl:choose>
    <xsl:when test="count(../OperHead/OperOut)=1">
      <xsl:choose>
        <xsl:when test="SubstPre">
          <xsl:apply-templates select="SubstPre" mode="locale"/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:text>local </xsl:text>
          <xsl:apply-templates select="../OperHead/OperOut"/>
<xsl:text>
do
</xsl:text>
          <xsl:apply-templates select="child::*"/>
    <xsl:text>
Result :=  </xsl:text> 
    <xsl:apply-templates select="../OperHead/OperOut/idList/idtyped" mode="nom"/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:when>
    <xsl:otherwise>
          <xsl:apply-templates select="child::*"/>
    </xsl:otherwise>
  </xsl:choose>

</xsl:template>


<!-- MRB 6.4 p 88 -->
<!-- substitution precondition -->

<xsl:template match="SubstPre" >
  <xsl:text>require
 </xsl:text>  
  <xsl:apply-templates select="PRE"/>
    <xsl:text>
do
  </xsl:text> 
  <xsl:apply-templates select="THEN"/>
</xsl:template>

<xsl:template match="SubstPre" mode="locale">
  <xsl:text>require
 </xsl:text>  
 <xsl:apply-templates select="PRE"/>
 <xsl:text>
local </xsl:text>
 <xsl:apply-templates select="../../OperHead/OperOut"/>
    <xsl:text>
do
  </xsl:text> 
  <xsl:apply-templates select="THEN"/>
    <xsl:text>
Result :=  </xsl:text> 
    <xsl:apply-templates select="../../OperHead/OperOut/idList/idtyped" mode="nom"/>
</xsl:template>

<xsl:template match="PRE">
  <xsl:apply-templates select="child::*" mode="Pre"/>
</xsl:template>

<xsl:template match="THEN">
  <xsl:apply-templates select="child::*"/>
</xsl:template>


<!-- MRB 4 p 27 -->
<xsl:template match="Predicate">
  <xsl:apply-templates select="child::*"/>
</xsl:template>

<xsl:template match="Predicate" mode="Pre">
  <xsl:apply-templates select="child::*" mode="Pre"/>
</xsl:template>

<!-- MRB 4.1 p 28 -->
<!-- proposition AND -->

<xsl:template match="And">   
<xsl:apply-templates select="child::*[position()=1]"/>
<xsl:text> and then  </xsl:text>
<xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="And" mode="Pre">
  <xsl:choose>
    <xsl:when test="name(child::*[position()=1])='And'">
      <xsl:apply-templates select="child::*[position()=1]" mode="Pre"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="child::*[position()=1]"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text>
</xsl:text>
  <xsl:choose>
    <xsl:when test="name(child::*[position()=2])='And'">
      <xsl:apply-templates select="child::*[position()=2]" mode="Pre"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="child::*[position()=2]"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<!--  MRB 4.3 p 30 -->
<!-- test d egalite -->

<xsl:template match="Equal">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>=</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotEqual">
    <xsl:apply-templates select="child::*[position()=1]"/>
    <xsl:text disable-output-escaping="yes"> /=  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- MRB 4.6 p 33 -->
<xsl:template match="Less">
    <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &lt;  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="LessEqual">
    <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &lt;=  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="GreaterEqual">
    <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &gt;=  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>


<xsl:template match="Greater">
    <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &gt;  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- fin des references au MRB 4.6 -->


<!-- MRB 5.3 p 39 -->

<xsl:template match="Div">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> / </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Mul">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> *  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Plus">
   <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> + </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Minus">
   <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> - </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="UMinus">
  <xsl:text> -</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
</xsl:template>

<xsl:template match="Modulo">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> mod  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Puissance">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>**</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>




<xsl:template match="In">
  <!-- <xsl:choose>
    <xsl:when test="name(child::*[position()=2])='TotalFunc'">    
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="child::*[position()=1]"/>
      <xsl:text> : </xsl:text>
      <xsl:apply-templates select="child::*[position()=2]"/>
    </xsl:otherwise>
  </xsl:choose>-->
</xsl:template>

<xsl:template match="PredParen">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*"/>
   <xsl:text>)</xsl:text>
</xsl:template>


<xsl:template match="Neg">
  <xsl:text>not </xsl:text>
  <xsl:apply-templates select="child::*"/>  
</xsl:template>


<!-- traite l'invariant -->


<xsl:template match="INVARIANT">
  <xsl:text>
invariant
</xsl:text>
 <xsl:for-each select="child::*">
     <xsl:apply-templates select="." mode="Pre"/>
     <xsl:text>
</xsl:text>
   </xsl:for-each>
</xsl:template>


<xsl:template match="BoolEvaluation">
  <xsl:apply-templates select="child::*"/>
</xsl:template>


<!-- MRB 6.16 p 103 -->

<!-- ATTENTION ! Uniquement implemente pour les tableaux -->


<xsl:template match="SetEqualFun">
   <xsl:apply-templates select="child::*[position()=1]" mode="SetEqualFun"/>
   <xsl:apply-templates select="child::*[position()=2]"/>
   <xsl:text>)
</xsl:text>
</xsl:template>

<!-- ACCES A UN ELEMENT DU TABLEAU -->

<xsl:template match="ExprFunCall" >
  <xsl:apply-templates select="child::*[position()=1]"/>
 <xsl:text>.item(</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
<xsl:text>)</xsl:text>
</xsl:template>

<!-- AJOUT DANS UN TABLEAU -->
<xsl:template match="ExprFunCall" mode="SetEqualFun">
  <xsl:apply-templates select="child::*[position()=1]"/>
 <xsl:text>.put( </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
 <xsl:text>, </xsl:text>
</xsl:template>

</xsl:stylesheet>
