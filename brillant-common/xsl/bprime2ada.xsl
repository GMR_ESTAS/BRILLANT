<!-- Feuille de style B/XML vers Ada -->
<!-- modif du 13/05/2003 -->
  	
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns="http://www.w3.org/TR/REC-html40">
  
<xsl:output method="text" omit-xml-declaration="yes"/>
<!-- selectionne la tete -->

<xsl:template match="AMN">
  <xsl:apply-templates select="child::*"/>
</xsl:template>


<!-- MRB 7 p 109 -->
<xsl:template match="MACHINE">
  <xsl:apply-templates select="Head" mode="open"/>
  <xsl:apply-templates select="ClausesList"/>
  <xsl:apply-templates select="Head" mode="close"/>
</xsl:template>


<xsl:template match="ClausesList">
  <xsl:apply-templates select="child::*"/>
</xsl:template>



<!-- entete de la machine (Clause Head) -->

<xsl:template match="Head" mode="open">
  <xsl:if test="HeadPar">
    <xsl:apply-templates select="HeadPar"/>
  </xsl:if>
<xsl:text>package </xsl:text>
<xsl:apply-templates select="id"/>
<xsl:text> is
</xsl:text>
</xsl:template>

<xsl:template match="Head" mode="close">
  <xsl:text>end </xsl:text>
  <xsl:apply-templates select="id"/>
<xsl:text>;
</xsl:text>
</xsl:template>

<!--genere les parametres de la tete-->
<xsl:template match="HeadPar">
<xsl:text>generic
</xsl:text>
<xsl:for-each select="child::*">
  <xsl:apply-templates select="."/>
  <xsl:text>;
</xsl:text>
</xsl:for-each>
</xsl:template>


<!-- valable pour toute la feuille -->

<xsl:template match="idtyped">
 <xsl:apply-templates select="child::*[position()=1]"/>
 <xsl:text> : </xsl:text>
 <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- MRB 5.6 p 44 -->
<!-- determine soit le nom soit le type de la variable -->
<xsl:template match="id">
  <xsl:choose>
    <xsl:when test="text()='NAT'">
       <xsl:text> natural </xsl:text>
    </xsl:when>
    <xsl:when test="text()='INT' or text()='INTEGER'">
      <xsl:text> integer </xsl:text>
    </xsl:when>
    <xsl:when test="text()='BOOL'">
      <xsl:text> boolean </xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="."/>
    </xsl:otherwise>
 </xsl:choose>
</xsl:template>

<xsl:template match="idList">
  <xsl:for-each select="child::*">
    <xsl:apply-templates select="."/>
    <xsl:text>;
</xsl:text>
  </xsl:for-each>
</xsl:template>

<xsl:template match="FunType">
  <xsl:apply-templates select="child::*[position()=1]"/>
   <xsl:text> of </xsl:text>
   <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NatSetRange">
 <xsl:text> array (</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
   <xsl:text> .. </xsl:text>
   <xsl:apply-templates select="child::*[position()=2]"/>
    <xsl:text>)</xsl:text>
</xsl:template>


<xsl:template match="Expr">
  <xsl:apply-templates select="child::*"/>
</xsl:template>

<!-- Balises n'etant pas traite -->

<xsl:template match="CONSTRAINTS | INVARIANT | INITIALISATION">

</xsl:template>

<!-- pour la definition des variables -->

<!-- MRB 7.19 p 154 -->
<xsl:template match="CVARIABLES">
  <xsl:apply-templates select="child::*"/>
</xsl:template>

<!-- MRB 7.23 p 164 -->
<xsl:template match="OPERATIONS">
  <xsl:for-each select="child::*">
    <xsl:apply-templates select="."/>
  </xsl:for-each>
</xsl:template>

<!-- Genere les prototypes des fonctions -->

<xsl:template match="Operation">
  <xsl:apply-templates select="OperHead"/>
</xsl:template>



<!-- determine si c'est une fonction ou une procedure -->

<xsl:template match="OperHead">
  <xsl:choose>
    <xsl:when test="count(OperOut/child::*)=1">
      <xsl:text>function </xsl:text>
      <xsl:apply-templates select="OperName"/>
      <xsl:apply-templates select="OperIn" mode="fonction"/>
      <xsl:apply-templates select="OperOut" mode="fonction"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>procedure </xsl:text>
      <xsl:apply-templates select="OperName"/>
      <xsl:if test="count(child::*)>1">
        <xsl:text> (</xsl:text>
      <xsl:apply-templates select="OperIn" mode="procedure"/>
      <xsl:apply-templates select="OperOut" mode="procedure"/>
      <xsl:text>)</xsl:text>
      </xsl:if>
    </xsl:otherwise>
  </xsl:choose>
<xsl:text>;
</xsl:text>
</xsl:template>

<xsl:template match="OperName">
  <xsl:apply-templates select="id"/>
</xsl:template>

<!-- pour les fonctions -->

<xsl:template match="OperIn" mode="fonction">
  <xsl:text>(</xsl:text> 
  <xsl:apply-templates select="idList/idtyped" mode="fonctionIn"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="OperOut" mode="fonction">
  <xsl:text> return </xsl:text>
  <xsl:apply-templates select="idList/idtyped" mode="fonction"/>
</xsl:template>

<!-- pour les procedures -->

<xsl:template match="OperIn" mode="procedure">
  <xsl:apply-templates select="idList/idtyped" mode="procedureIn"/>
</xsl:template>

<xsl:template match="OperOut" mode="procedure">
  <xsl:apply-templates select="idList/idtyped" mode="procedureOut"/>
</xsl:template>


<!-- traite le cas de variables typees dans une fonction-->

<xsl:template match="idtyped" mode="fonctionIn">
 <xsl:value-of select="child::*[position()=1]"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:if test="position()!=last()">
    <xsl:text> ; </xsl:text>  
  </xsl:if>
 </xsl:template>


<xsl:template match="idtyped" mode="fonction">
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- pour les procedures -->

<xsl:template match="idtyped" mode="procedureIn">
<xsl:value-of select="child::*[position()=1]"/>
  <xsl:text> : in </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:if test="position()!=last()">
    <xsl:text> ;</xsl:text>  
  </xsl:if>
</xsl:template>

<xsl:template match="idtyped" mode="procedureOut">
<xsl:value-of select="child::*[position()=1]"/>
  <xsl:text> : Out </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/> 
 <xsl:if test="position()!=last()">
    <xsl:text> ; </xsl:text>  
  </xsl:if>
</xsl:template>

</xsl:stylesheet>