<xsl:stylesheet version="1.0"
	        xmlns:xsl = "http://www.w3.org/1999/XSL/Transform"
		xmlns="http://www.w3.org/TR/REC-html40">

<xsl:output method = "html"
	    omit-xml-declaration = "yes"
	    indent = "yes"
	    doctype-public = "-//w3c//dtd html 4.0 transitional//en"/>

<xsl:template match = "AMN">
  <HTML>
  <HEAD>
  <TITLE></TITLE>
  <META NAME = "Author" CONTENT = "Mariano Georges"/>
  <META NAME = "Copyright" CONTENT = "INRETS-ESTAS 2001"/>
  <META NAME = "E-mail" CONTENT = "georges.mariano@inrets.fr"/>
  <META NAME = "Keywords" CONTENT = "m�thode B methode formelle"/>
  </HEAD>
  <BODY BGCOLOR = "white" VLINK = "blue" LANG = "EN">
  <IMG SRC="http://www3.inrets.fr/logo/INRETS.gif" HEIGHT="44" WIDTH="121" align="MIDDLE" ALT="INRETS"/>
<I>
  <FONT COLOR="black">
	Copyright INRETS - ESTAS   2000~2001 :
  </FONT>
  </I>
  <A HREF="http://www3.inrets.fr/ESTAS/B@INRETS/" TARGET="_blank">
  BCaml
  </A>
</BODY>
</HTML>
</xsl:template>
</xsl:stylesheet>