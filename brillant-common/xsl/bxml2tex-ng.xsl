<!-- Feuille de style B/XML vers B/TXT -->

<!-- BRILLANT project -->
<!-- https://gna.org/projects/brillant/ -->

<!-- Auteur : Magaly CLEMENT-->
<!-- $Id: bxml2tex.xsl 492 2006-03-20 11:50:56Z gmariano $ -->



<xsl:stylesheet 
	version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns="http://www.w3.org/TR/REC-html40">

<xsl:output method="text" omit-xml-declaration="yes"/>

<xsl:template match="AMN">
<xsl:text>
\begin{AMN}%
</xsl:text>
<xsl:apply-templates/>
<xsl:text>
\end{AMN}%
</xsl:text>
</xsl:template>

<xsl:template match="ProofObligation">
<xsl:text>
\begin{AMNPO}%
</xsl:text>
 <!-- Do not process IdList (first child of PO xml clause) -->
 <!-- Jump direcly to 2nd child -->
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
<xsl:text>
\end{AMNPO}%
</xsl:text>
</xsl:template>

<!-- MACHINE ou REFINEMENT ou IMPLEMENTATION-->

<xsl:template match="MACHINE | REFINEMENT | IMPLEMENTATION | EmptyTree">
  <xsl:choose>
    <xsl:when test="self::MACHINE">
      <xsl:text> \BMACHINE 
	</xsl:text>
    </xsl:when>
    <xsl:when test="self::REFINEMENT">
      <xsl:text> \BREFINEMENT 
	</xsl:text>
    </xsl:when>
    <xsl:when test="self::IMPLEMENTATION">
      <xsl:text> \BIMPLEMENTATION 
	</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>
ARBRE VIDE
      </xsl:text>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:apply-templates select="Head"/>
  <xsl:apply-templates select="ClausesList"/>
  <xsl:text> 
  \BENDC %% of component 
</xsl:text>
</xsl:template>
  
<xsl:template match="Head">
<xsl:text>\Bcomponent{</xsl:text>
  <xsl:value-of select="Id"/>
<xsl:text>}</xsl:text>
  <xsl:if test="child::HeadPar and string(child::HeadPar)">
    <xsl:apply-templates select="HeadPar"/>
  </xsl:if>
</xsl:template>
  
<xsl:template match="HeadPar">
  <xsl:text>(</xsl:text>
    <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position() != last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
    </xsl:for-each>
  <xsl:text>)</xsl:text>
</xsl:template>
  
<xsl:template match="IdentListPar">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
  </xsl:for-each>
</xsl:template>
  
<xsl:template match="IdentTypedListPar">
  <xsl:apply-templates select="IdentTypedPar"/>
</xsl:template>
  
<xsl:template match="IdentTypedPar">
  <xsl:apply-templates select="Id"/>
  <xsl:apply-templates select="Type"/>
</xsl:template>

<xsl:template match="Type">
  <xsl:choose>
    <xsl:when test="child::ExprNatRange">
      <xsl:apply-templates select="ExprNatRange"/>
    </xsl:when>
    <xsl:when test="child::PredType">
	<!-- Pas trouve dans la DTD-->
    </xsl:when>
    <xsl:when test="child::Record">
      <xsl:apply-templates select="Record"/>
    </xsl:when>
    <xsl:when test="child::TypeIdentifier">
      <xsl:apply-templates select="Id"/>
    </xsl:when>
  </xsl:choose>
</xsl:template>
  
<xsl:template match="ExprNatRange">
  <xsl:text> (</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>) </xsl:text>
</xsl:template>
  
<!--   les clauses  -->

<xsl:template match="ClausesList">
  <xsl:call-template name="clauses"/>
</xsl:template>
  
<!-- Clause SETS -->

<xsl:template match="SETS">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position() != last()">
      <xsl:text> ; </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>
  
<xsl:template match="SetEnumDec">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> = \{</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>\}</xsl:text>
</xsl:template>
  
<xsl:template match="SetAbstract">
  <xsl:apply-templates select="Id"/>
</xsl:template>
  
<!-- INITIALISATION -->

<xsl:template match="INITIALISATION">
  <xsl:apply-templates/>
</xsl:template>
  
<!-- Clause VALUES -->

<xsl:template match="VALUES">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text> 
      \Bsemisep </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>
  
<xsl:template match="Valuation">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> = </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- Clause DEFINITIONS -->

<xsl:template match="DEFINITIONS">
  <!-- xsl:for-each select="*" -->
    <xsl:apply-templates select="Definition"/>
  <!-- /xsl:for-each -->
</xsl:template>

<xsl:template match="Definition">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> == </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:if test="position()!=last()">
    <xsl:text>
    \Bsemisep </xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="DefFile">
  <xsl:apply-templates select="STRING"/>
  <xsl:text>;</xsl:text>
</xsl:template>

<xsl:template match="DefHead">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:if test="child::IdList and string(child::IdList)">
    <xsl:text>(</xsl:text>
    <xsl:apply-templates select="child::*[position()=2]"/>
    <xsl:text>)</xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="DefBody">
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="DefBodyExpr | DefBodyPred">
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- Clause REFINES -->

<xsl:template match="REFINES">
  <xsl:apply-templates select="*"/>
</xsl:template>

<!-- Clause SEES -->

<xsl:template match="SEES">

  <xsl:for-each select="*">
    <xsl:text>\Bseen{</xsl:text>
    <xsl:value-of select="."/>
    <xsl:text>}</xsl:text>
    <xsl:if test="position() != last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- Clauses CONSTANTS, PROMOTES, USES, var, TypeIdentifier -->

<xsl:template match="CONSTANTS | var | TypeIdentifier">
  <xsl:apply-templates select="Id"/>
</xsl:template>

<xsl:template match="PROMOTES | USES">
  <xsl:for-each select="*">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- Variable typee -->

<xsl:template match="varTyped">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- Clauses avec une liste d'identifiant -->

<xsl:template match="VARIABLES | CVARIABLES | AVARIABLES | HVARIABLES | HCONSTANTS | CCONSTANTS | ACONSTANTS">
  <xsl:apply-templates select="IdList"/>
</xsl:template>

<!-- clause PROPERTIES, CONSTRAINTS, INVARIANT, ASSERTIONS -->

<xsl:template match="PROPERTIES">
  <xsl:apply-templates select="Predicate"/>
</xsl:template>

<xsl:template match="CONSTRAINTS | INVARIANT">
  <xsl:apply-templates select="Predicate"/>
</xsl:template>

<xsl:template match="ASSERTIONS">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position() != last()">
      <xsl:text>; </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>


<!-- clause EXTENDS, INCLUDES, IMPORTS -->

<xsl:template match="EXTENDS | INCLUDES">
  <xsl:for-each select="*">
    <xsl:call-template name="instance"/>
    <xsl:if test="position() != last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="IMPORTS">
  <xsl:for-each select="*">
<xsl:text>\Bimported{</xsl:text><xsl:call-template name="instance"/><xsl:text>}</xsl:text>
    <xsl:if test="position() != last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>



<xsl:template name="instance">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::Id"><xsl:apply-templates select="../Id"/></xsl:when>
      <xsl:when test="self::ExprList">
        <xsl:if test="count(*)!=0">
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="../ExprList"/>
        <xsl:text>)</xsl:text>
        </xsl:if>
      </xsl:when>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>


<!-- Predicats -->

<xsl:template match="Predicate">
  <xsl:apply-templates select="*"/>
</xsl:template>

<!-- 4.1 -->
<!-- Propositions -->

<xsl:template match="SubstApply">
  <xsl:text> [ </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> ] ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="SubstInstanciation">

  <xsl:apply-templates select="child::*[position()=1]"/>

  <xsl:apply-templates select="child::*[position()=2]"/>

</xsl:template>

<xsl:template match="PredParen">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="*"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Neg">
  <xsl:text> \Bnot </xsl:text>
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="And">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:choose>
    <xsl:when test="parent::ExprParen">
    <!-- xxxxxxxxxx -->
    </xsl:when>
    <xsl:otherwise>
    <!-- xxxxxxxxxx -->
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text disable-output-escaping="yes"> \Band </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Or">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> or </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Implies">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Bimplies </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Equiv">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping = "yes"> \Bequiv </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 4.2 -->
<!-- Predicats Quantifies -->

<xsl:template match="Exists">
  <xsl:text> \Bexists </xsl:text>
  <xsl:apply-templates select="IdList"/>
  <xsl:text>.(</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ForAll">
  <xsl:text> \Bforall </xsl:text>
  <xsl:apply-templates select="IdList"/>
  <xsl:text> . ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<!-- 4.3 -->
<!-- Predicats egalites -->

<xsl:template match="Equal">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> = </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotEqual">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \Bneq </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 4.4 -->
<!-- Predicats d'appartenance -->

<xsl:template match="In">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \Bin </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotIn">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> /: </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 4.5 -->
<!-- Predicats d'inclusions -->

<xsl:template match="SubSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> &lt;: </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="StrictSubSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \BSSubS </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotSubSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> /&lt;: </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotStrictSubSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> /&lt;&lt;: </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 4.6 -->
<!-- Predicats de comparaison -->

<xsl:template match="LessEqual">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Blesseq </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Less">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Bless </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="GreaterEqual">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Bgreateq </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Greater">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Bgreat </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- Expressions -->

<xsl:template match="Expr">
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="ExprParen">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="*"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<!-- Expressions arithmetiques -->

<xsl:template match="MAXINT">
  <xsl:text> MAXINT </xsl:text>
</xsl:template>

<xsl:template match="MININT">
  <xsl:text> MININT </xsl:text>
</xsl:template>

<xsl:template match="Minus">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> - </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="UMinus">
  <xsl:text>-</xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Plus">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> + </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Mul">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \Btimes </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Div">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> / </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Modulo">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> mod </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Puissance">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \Bexpo </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Succ">
  <xsl:apply-templates select="Expr"/> + 1
</xsl:template>

<!-- 5.4 -->

<xsl:template match="Min">
  <xsl:text> min(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Max">
  <xsl:text> max(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Card">
  <xsl:text> card(</xsl:text>
  <xsl:apply-templates select="Expr"/>
<xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="PI">
  <xsl:text> PI(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>).(</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> | </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="SIGMA">
  <xsl:text> SIGMA </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>.(</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> | </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="LAMBDA">
<xsl:text> Somme </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:apply-templates select="child::*[position()=3]"/>
</xsl:template>

<!-- 5.5 -->
<!-- Expressions de couples -->

<xsl:template match="Maplet">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="position()=1">
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text> \BMap </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<!-- 5.6 -->
<!-- Ensembles Predefinis -->

<xsl:template match="SetPredefined">
  <xsl:apply-templates select="child::*[position()=1]"/>
</xsl:template>

<xsl:template match="NAT">
  <xsl:text>\BNAT </xsl:text>
</xsl:template>

<xsl:template match="INT">
  <xsl:text>\BINT </xsl:text>
</xsl:template>

<xsl:template match="BOOL">
  <xsl:text>\BBOOL </xsl:text>
</xsl:template>

<xsl:template match="INTEGER">
  <xsl:text>\BINTEGER </xsl:text>
</xsl:template>

<xsl:template match="NATURAL">
  <xsl:text>\BNATURAL </xsl:text>
</xsl:template>

<xsl:template match="NAT1">
  <xsl:text>\BNAT1 </xsl:text>
</xsl:template>

<xsl:template match="NATURAL1">
  <xsl:text>\BNATURAL1 </xsl:text>
</xsl:template>

<xsl:template match="SetEmpty">
  <xsl:text> \BSetEmpty </xsl:text>
</xsl:template>

<xsl:template match="STRING">
  <xsl:text>\verb+"</xsl:text><xsl:value-of select="."/><xsl:text>"+ </xsl:text>
</xsl:template>

<!-- 5.7 -->
<!-- Expressions ensemblistes -->

<xsl:template match="Pow">
  <xsl:text> POW(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="PowOne">
  <xsl:text> PowOne </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Fin">
  <xsl:text> FIN(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="FinOne">
  <xsl:text> FinOne </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.8 -->
<!-- Expressions ensemblistes -->

<xsl:template match="InterSets">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> /\ </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="UnionSets">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \Union </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- Intersection et union Quantifiee -->

<xsl:template match="InterQ">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> | </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) = inter\{</xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>\}</xsl:text>
</xsl:template>

<xsl:template match="UnionQ">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> | </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> ) = union\{</xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>\}</xsl:text>
</xsl:template>

<!-- intersection et union generalisee -->

<xsl:template match="UnionG">
  <xsl:text> union\{</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>\}</xsl:text>
</xsl:template>

<xsl:template match="InterG">
  <xsl:text> inter\{</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>\}</xsl:text>
</xsl:template>

<!-- 5.9 -->
<!-- Expressions de records -->

<xsl:template match="Records">
    <xsl:text> rec(</xsl:text>
    <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
    <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RecordAccess">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>'</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="RecWithFields">
  <xsl:text>
struct(
  </xsl:text>
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RecordItem">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Field">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="SetEqualRecord">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> := </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.10 -->
<!-- Ensemble de Relations -->

<xsl:template match="RelSet">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \BRelSet </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.11 -->
<!-- Expressions de relations -->

<xsl:template match="Identity">
  <xsl:text> id(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Tilde">
  <xsl:apply-templates select="Expr"/>
  <xsl:text> ~ </xsl:text>
</xsl:template>

<xsl:template match="PrjOne">
  <xsl:text> prj1 </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="PrjTwo">
  <xsl:text> prj2 </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="RelSeqComp">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ParallelComp">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> || </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="RelProd">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \BRelProd </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.12 -->
<!-- Expressions de relations -->

<xsl:template match="RelIter">
  <xsl:text> \BRelIter </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>, </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- fermeture -->

<xsl:template match="Closure">
  <xsl:text> Closure </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- fermeture reflexive transitive -->

<xsl:template match="ClosureOne">
  <xsl:text> closure1 </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.13 -->
<!-- Expressions de relations -->

<xsl:template match="Dom"> <!-- Domaine -->
  <xsl:text> dom(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="Ran"> <!-- Image -->
  <xsl:text> ran(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<!-- 5.14 -->
<!-- Expressions de relations -->

<xsl:template match="DomSubstract">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \BDomSubs </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="RanSubstract">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \BRanSubs </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="DomRestrict">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \BDomRest </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="RanRestrict">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \BRanRest </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.15 -->
<!-- Ensembles de fonctions -->

<xsl:template match="PartialFunc">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Bpfun </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="TotalFunc">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Btfun </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PartialInj">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Bpinj </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="TotalInj">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Btinj </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PartialSurj">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Bpsurj </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="TotalSurj">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Btsurj </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PartialBij">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Bbij </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="TotalBij">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> \Bbij </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>


<!-- 5.16 -->
<!-- Expressions de fonctions -->

<xsl:template match="Lambda">
  <xsl:text> % </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>.(</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> | </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="ExprFunCall">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:if test="child::ExprList and string(child::ExprList)">
    <xsl:text>(</xsl:text>
    <xsl:apply-templates select="child::*[position()=2]"/>
    <xsl:text>)</xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="RelFnc">
  <xsl:text> RelFnc </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="FncRel">
  <xsl:text> FncRel </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.17 -->
<!-- Ensembles de sequences -->

<xsl:template match="Seq">
  <xsl:text> seq(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="SeqOne">
  <xsl:text> seq1(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="ISeq">
  <xsl:text> iseq(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="ISeqOne">
  <xsl:text> iseq1(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="Perm">
  <xsl:text> perm </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.18 -->
<!-- Expressions de sequences -->

<xsl:template match="Size">
  <xsl:text> size(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="First">
  <xsl:text> first(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="Last">
  <xsl:text> last(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="Tail">
  <xsl:text> tail(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="Front">
  <xsl:text> front(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="Rev">
  <xsl:text> rev(</xsl:text>
  <xsl:apply-templates select="Expr"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<!-- 5.19 -->
<!-- Expressions de sequences -->

<xsl:template match="AppendSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &lt;- </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="ConcatSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \Bexpo </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PrependSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> -&gt; </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PrefixSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \SeqPref </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="SuffixSeq">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \SeqSuff </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Conc">
  <xsl:text> conc </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.20 -->
<!-- Ensembles d'arbres -->

<xsl:template match="Tree">
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="BTree">
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.21 -->
<!-- Expressions d'arbres -->

<xsl:template match="Const">
  <xsl:text> const </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>, </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Top">
  <xsl:text> top </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Sons">
  <xsl:text> sons </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Prefix">
  <xsl:text> prefix </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Postfix">
  <xsl:text> postfix </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="SizeT">
  <xsl:text> sizet </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Mirror">
  <xsl:text> mirror </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<!-- 5.22 -->
<!-- Expressions de noeuds d'arbres -->

<xsl:template match="Rank">
  <xsl:text> rank </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Father">
  <xsl:text> father </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Son">
  <xsl:text> son </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Subtree">
  <xsl:text> subtree </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Arity">
  <xsl:text> arity </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 5.23 -->
<!-- Expressions d'arbre binaires -->

<xsl:template match="Bin">
  <xsl:text> bin </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> , </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
</xsl:template>

<xsl:template match="Left">
  <xsl:text> left </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Right">
  <xsl:text> right </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="Infix">*
  <xsl:text> infix </xsl:text>
  <xsl:apply-templates select="Expr"/>
</xsl:template>

<xsl:template match="ExprSequence">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>,
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NatSetRange">
  <xsl:text> (</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>..</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>) </xsl:text>
</xsl:template>

<xsl:template match="OverRideFwd">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>&lt;+</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="OverRideBck">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="ExprId">
  <xsl:apply-templates select="Id"/>
</xsl:template>

<xsl:template match="ExprList">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="Assign">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- OPERATIONS -->

<xsl:template match="OPERATIONS">
  <xsl:apply-templates select="Operation"/>
</xsl:template>

<xsl:template match="Operation">
  <xsl:text>
\begin{Operation} </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>
\end{Operation} 
</xsl:text>
  <xsl:if test="position()!=last()">
    <xsl:text> \Bopersep </xsl:text>
  </xsl:if>
</xsl:template>

<xsl:template match="OperHead">
  <xsl:if test="child::OperOut and string(child::OperOut)">
    <xsl:apply-templates select="OperOut"/>
    <xsl:text disable-output-escaping = "yes"> \Breturn </xsl:text>
  </xsl:if>
  <xsl:apply-templates select="OperName" />
  <xsl:if test="child::OperIn and string(child::OperIn)">
    <xsl:text>(</xsl:text>
    <xsl:apply-templates select="OperIn"/>
    <xsl:text>)</xsl:text>
  </xsl:if>
    <xsl:text> \Boperdef </xsl:text>
</xsl:template>

<xsl:template match="OperName">
  <xsl:text>\Bopername{</xsl:text>
  <xsl:apply-templates select="Id"/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="OperOut">
  <xsl:apply-templates select="IdList"/>
</xsl:template>

<xsl:template match="OperIn">
  <xsl:apply-templates select="IdList"/>
</xsl:template>

<xsl:template match="OperBody">
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="IdList">
  <xsl:for-each select="*">
    <xsl:value-of select="."/>
    <xsl:if test="position() != last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<!-- 6 -->
<!-- Substitutions -->
<!-- 6.1 -->

<xsl:template match="SubstBlock">
  <xsl:text>
    \BBEGIN
  </xsl:text>
  <xsl:apply-templates select="*"/>
  <xsl:text>
    \BEND %% of BEGIN
  </xsl:text>
</xsl:template>

<!-- 6.2 -->

<xsl:template match="SubstSkip">
  <xsl:text> \Bskip </xsl:text>
</xsl:template>

<!-- 6.3 -->

<xsl:template match="SetEqualIds">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> := </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="SetEqualFun">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> := </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 6.4 -->
<!-- Precondition substitution -->

<xsl:template match="Precondition">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="PRE">
  <xsl:text> \BPRE  </xsl:text>
  <xsl:apply-templates select="Predicate"/>
</xsl:template>

<xsl:template match="THEN">
  <xsl:text> \BTHEN </xsl:text>
  <xsl:apply-templates select="*"/>
  <xsl:text>\BEND %% of THEN   </xsl:text>
</xsl:template>

<!-- 6.5 -->
<!-- ASSERT Substitution -->

<xsl:template match="SubstAssert">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 6.6 -->
<!-- CHOICE Substitution -->

<xsl:template match="SubstChoice">
  <xsl:text>
    \BCHOICE
  </xsl:text>
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text> \BOR </xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text>
    \BEND %% of CHOICE
  </xsl:text>
</xsl:template>

<!-- 6.7 -->
<!-- IF Substitution -->

<xsl:template match="SubstIf">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::IfThen and position()=1">
        <xsl:call-template name="IfThen1"/>
      </xsl:when>
      <xsl:when test="self::IfThen and position()!=1">
        <xsl:call-template name="IfThen2"/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>
    \BELSE
        </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
  <xsl:text>
   \BEND %% of IF
  </xsl:text>
</xsl:template>

<xsl:template name="IfThen1">
    <xsl:text>
    \BIF
    </xsl:text>
    <xsl:apply-templates select="child::*[position()=1]"/>
    <xsl:text>
    \BTHEN
    </xsl:text>
    <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template name="IfThen2">
    <xsl:text>
    \BELSIF
    </xsl:text>
    <xsl:apply-templates select="child::*[position()=1]"/>
    <xsl:text>
    \BTHEN
    </xsl:text>
    <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 6.8 -->
<!-- SELECT Substitution -->

<xsl:template match="SubstSelect">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::WhenPart and position()=1">
        <xsl:text>
    \BSELECT
        </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="self::WhenPart">
        <xsl:text>
    \BWHEN
        </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:otherwise>
        <xsl:text>
    \BELSE
        </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
     <xsl:text>
    \BEND %% of SELECT
        </xsl:text>
</xsl:template>

<xsl:template match="WhenPart">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>
    \BTHEN
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 6.9 -->
<!-- CASE Substitution -->

<xsl:template match="SubstCase">
  <xsl:text>
    \BCASE
  </xsl:text>
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="position()=1">
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="position()=2">
        <xsl:text>
    \BOF
        </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="position()=3">
        <xsl:text>
    \BELSE
        </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:when>
    </xsl:choose>
  </xsl:for-each>
  <xsl:text>
    \BEND %% of CASE
    \BEND %% of CASE 
  </xsl:text>
</xsl:template>

<xsl:template match="OrPartList">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::OrPart and position()=1">
        <xsl:call-template name="OrPart1"/>
      </xsl:when>
      <xsl:when test="self::OrPart and position()!=1 ">
        <xsl:call-template name="OrPart2"/>
      </xsl:when>
      <xsl:otherwise/>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>

<xsl:template name="OrPart1">
  <xsl:text> \BEITHER </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \BTHEN </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template name="OrPart2">
  <xsl:text> \BOR </xsl:text>
  <xsl:call-template name="ExprOrPart"/>
  <xsl:text> \BTHEN </xsl:text>
  <xsl:apply-templates select="child::*[position()=last()]"/>
</xsl:template>

<xsl:template name="ExprOrPart">
  <xsl:for-each select="Expr">
    <xsl:apply-templates/>
    <xsl:if test="position()!=last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>


<!-- 6.10 -->
<!-- ANY Substitution -->

<xsl:template match="SubstAny">
  <xsl:text> \BANY 
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>
    \BWHERE
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>
    \BTHEN
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text> 
    \BEND %% of ANY
  </xsl:text>
</xsl:template>

<!-- 6.11 -->
<!-- LET Substitution -->

<xsl:template match="SubstLet">
  <xsl:text> \BLET </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>
    \BBE
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>
    \BIN
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text> 
    \BEND %% of LET
  </xsl:text>
</xsl:template>

<!-- 6.12 -->

<xsl:template match="SetIn">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> :: </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- 6.13 -->

<xsl:template match="BecomeSuch">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> :(</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="SubstAssert">
  <xsl:text> \BASSERT </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \BTHEN </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> 
\BEND 
  </xsl:text>
</xsl:template>

<!-- 6.14 -->
<!-- VAR Substitution -->

<xsl:template match="SubstVar">
  <xsl:text> \BVAR </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>
    \BIN
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> 
    \BEND %% of VAR
  </xsl:text>
</xsl:template>

<!-- 6.15 -->

<xsl:template match="SubstSequence">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position() != last()">
      <xsl:text> \Bsemisep </xsl:text>
    </xsl:if>
    </xsl:for-each>
</xsl:template>


<!-- 6.16 -->
<!--  
   <SubstOperCall>
     <IdList>...</IdList>
     <Expr>...</Expr>???<ExprList/>???
   </SubstOperCall>
-->
<xsl:template match="SubstOperCall">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::IdList and string(self::IdList)">
        <xsl:apply-templates select="."/>
	<xsl:text disable-output-escaping="yes"> \Breturn </xsl:text>
      </xsl:when>
      <xsl:when test="self::Expr">
        <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:when test="self::ExprList and count(child::*)!=0">
        <xsl:text>(</xsl:text>
        <xsl:apply-templates select="."/>
        <xsl:text>)</xsl:text>
      </xsl:when>
      <xsl:otherwise>
        <xsl:apply-templates select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
</xsl:template>
  
<!-- 6.17 -->
<xsl:template match="SubstWhile">
  <xsl:text> 
      \BWHILE 
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>
      \BDO
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>
      \BINVARIANTW
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text>
      \BVARIANT
  </xsl:text>
  <xsl:apply-templates select="child::*[position()=4]"/>
  <xsl:text> 
      \BEND %% of WHILE
  </xsl:text>
</xsl:template>

<xsl:template match="WhileTest">
  <xsl:apply-templates select="Predicate"/>
</xsl:template>
  
<xsl:template match="WhileInvariant">
  <xsl:apply-templates select="Predicate"/>
</xsl:template>
  
<xsl:template match="WhileVariant">
  <xsl:apply-templates select="Expr"/>
</xsl:template>
  
<xsl:template match="WhileBody">
  <xsl:apply-templates select="*"/>
</xsl:template>
  
<!-- 6.18 -->
<xsl:template match="SubstParallel">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> \Bpara </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>
  
<!-- 6.19 -->
<xsl:template match="SubstInstanciation">
  <xsl:text> \[ </xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> := </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text> \] ( </xsl:text>
  <xsl:apply-templates select="child::*[position()=3]"/>
  <xsl:text> ) </xsl:text>
</xsl:template>
  

<xsl:template match="Id">
  <xsl:for-each select="self::Id">
    <xsl:value-of select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>
  
<xsl:template match="IdRename">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>
  
<xsl:template match="Constant">
  <xsl:choose>
    <xsl:when test="child::number">
      <xsl:apply-templates select="number"/>
    </xsl:when>
    <xsl:when test="child::string">
      <xsl:apply-templates select="string"/>
    </xsl:when>
  </xsl:choose>
</xsl:template>
  
<xsl:template match="string">
  <xsl:value-of select="."/>
</xsl:template>
  
<xsl:template match="SeqEnum">
  <xsl:text>[</xsl:text>
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text>]</xsl:text>
</xsl:template>
  
<xsl:template match="SeqEmpty">
  <xsl:text> \BSeqEmpty </xsl:text>
</xsl:template>
  
<xsl:template match="Set">
  <xsl:apply-templates select="*"/>
</xsl:template>
  
<xsl:template match="SetRange">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>
  
<xsl:template match="SetEnum">
  <xsl:apply-templates select="Expr"/>
</xsl:template>
  
<xsl:template match="SetCompr">
  <xsl:text>\{</xsl:text>
  <xsl:for-each select="*">
    <xsl:apply-templates/>
    <xsl:if test="position() != last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
  <xsl:text>\}</xsl:text>
</xsl:template>
  
<xsl:template match="SetComprPred">
  <xsl:text> \{</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> | </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:text>\} </xsl:text>
</xsl:template>
  
<xsl:template match="INTER">
 <xsl:apply-templates select="child::*[position()=1]"/>
 <xsl:apply-templates select="child::*[position()=2]"/>
 <xsl:apply-templates select="child::*[position()=3]"/>
</xsl:template>
  
<xsl:template match="BoolEvaluation">
 <xsl:text> bool(</xsl:text>
 <xsl:apply-templates select="*"/>
 <xsl:text>) </xsl:text>
</xsl:template>
  
<xsl:template match="TRUE">
  <xsl:text>TRUE</xsl:text>
</xsl:template>
  
<xsl:template match="FALSE">
  <xsl:text>FALSE</xsl:text>
</xsl:template>
  
<xsl:template match="Number">
  <xsl:value-of select="."/>
</xsl:template>
  
<xsl:template match="number">
  <xsl:value-of select="."/>
</xsl:template>
  
    
<!-- Entite clause -->

<xsl:template name="clauses">
  <xsl:for-each select="*">
    <xsl:choose>
      <xsl:when test="self::SEES">
        <xsl:text>
\SEES
      </xsl:text>
        <xsl:apply-templates select="//SEES"/>
      </xsl:when>
      <xsl:when test="self::DEFINITIONS">
        <xsl:text>
\DEFINITIONS
      </xsl:text>
        <xsl:apply-templates select="//DEFINITIONS"/>
      </xsl:when>
      <xsl:when test="self::CONSTRAINTS">
        <xsl:text>
\CONSTRAINTS
      </xsl:text>
        <xsl:apply-templates select="//CONSTRAINTS"/>
      </xsl:when>
      <xsl:when test="self::AVARIABLES">
        <xsl:text> \VARIABLES </xsl:text>
	<xsl:apply-templates select="//AVARIABLES"/>
      </xsl:when>
      <xsl:when test="self::INVARIANT">	  
        <xsl:text> \INVARIANT </xsl:text>
        <xsl:apply-templates select="//INVARIANT"/>
      </xsl:when>
      <xsl:when test="self::SETS">	  
        <xsl:text> \SETS </xsl:text>
        <xsl:apply-templates select="//SETS"/>
      </xsl:when>
      <xsl:when test="self::INITIALISATION">
        <xsl:text> \INITIALISATION  </xsl:text>
        <xsl:apply-templates select="//INITIALISATION"/>
      </xsl:when>
      <xsl:when test="self::CONSTANTS">	  
        <xsl:text>
\CONSTANTS
      </xsl:text>
        <xsl:apply-templates select="//CONSTANTS"/>
      </xsl:when>
      <xsl:when test="self::CCONSTANTS">
        <xsl:text>
\CONSTANTS
      </xsl:text>
        <xsl:apply-templates select="//CCONSTANTS"/>
      </xsl:when>
      <xsl:when test="self::ACONSTANTS">	  
        <xsl:text>\ABSTRACTCONSTANTS      </xsl:text>
        <xsl:apply-templates select="//ACONSTANTS"/>
      </xsl:when>
      <xsl:when test="self::HCONSTANTS">
        <xsl:text>\HIDDENCONSTANTS      </xsl:text>
        <xsl:apply-templates select="//HCONSTANTS"/>
      </xsl:when>
      <xsl:when test="self::PROPERTIES">	  
        <xsl:text>\PROPERTIES      </xsl:text>
        <xsl:apply-templates select="//PROPERTIES"/>
      </xsl:when>
      <xsl:when test="self::VARIABLES">
        <xsl:text>\VARIABLES      </xsl:text>
        <xsl:apply-templates select="//VARIABLES"/>
      </xsl:when>
      <xsl:when test="self::CVARIABLES">
        <xsl:text>\VISIBLEVARIABLES      </xsl:text>
        <xsl:apply-templates select="//CVARIABLES"/>
      </xsl:when>
      <xsl:when test="self::HVARIABLES">
        <xsl:text>\HIDDENVARIABLES      </xsl:text>
        <xsl:apply-templates select="//HVARIABLES"/>
      </xsl:when>
      <xsl:when test="self::PROMOTES">
        <xsl:text>\PROMOTES      </xsl:text>
        <xsl:apply-templates select="//PROMOTES"/>
      </xsl:when>
      <xsl:when test="self::EXTENDS">	  
        <xsl:text>\EXTENDS      </xsl:text>
        <xsl:apply-templates select="//EXTENDS"/>
      </xsl:when>
      <xsl:when test="self::ASSERTIONS">	  
        <xsl:text>\ASSERTIONS      </xsl:text>
        <xsl:apply-templates select="//ASSERTIONS"/>
      </xsl:when>
      <xsl:when test="self::OPERATIONS">
        <xsl:text> \OPERATIONS </xsl:text>
        <xsl:apply-templates select="//OPERATIONS"/>
      </xsl:when>
      <xsl:when test="self::USES">
        <xsl:text> \USES </xsl:text>
        <xsl:apply-templates select="//USES"/>
      </xsl:when>
      <xsl:when test="self::INCLUDES">
        <xsl:text> \INCLUDES </xsl:text>
        <xsl:apply-templates select="//INCLUDES"/>
      </xsl:when>
      <xsl:when test="self::REFINES">
        <xsl:text> \REFINES </xsl:text>
        <xsl:apply-templates select="//REFINES"/>
      </xsl:when>
      <xsl:when test="self::IMPORTS">	  
        <xsl:text>
\IMPORTS
      </xsl:text>
        <xsl:apply-templates select="//IMPORTS"/>
      </xsl:when>
      <xsl:when test="self::VALUES">
        <xsl:text>
\VALUES
      </xsl:text>
        <xsl:apply-templates select="//VALUES"/>
      </xsl:when>
      </xsl:choose>
    </xsl:for-each>
  </xsl:template>


<!-- Proof Obligation 

<xsl:template match="ProofObligation">
  <xsl:apply-templates select="*"/>
</xsl:template>

-->

</xsl:stylesheet>
