<!-- Feuille de style B/XML vers Ada -->
<!-- 
$Id$
-->

  	
<xsl:stylesheet 
  version="1.0" 
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
  xmlns="http://www.w3.org/TR/REC-html40">
  
<xsl:output method="text" omit-xml-declaration="yes"/>
<!-- selectionne la tete -->

<xsl:template match="AMN">
  <xsl:apply-templates select="child::*"/>
</xsl:template>


<!-- MRB 7 p 109 -->
<xsl:template match="MACHINE">
  <xsl:apply-templates select="Head" mode="open"/>
  <xsl:apply-templates select="ClausesList"/>
  <xsl:apply-templates select="Head" mode="close"/>
</xsl:template>


<xsl:template match="ClausesList">
  <xsl:apply-templates select="child::*"/>
</xsl:template>



<!-- entete de la machine (Clause Head) -->

<xsl:template match="Head" mode="open">
 <xsl:text>package body  </xsl:text>
<xsl:apply-templates select="id"/>
<xsl:text> is

</xsl:text>
</xsl:template>

<xsl:template match="Head" mode="close">
  <xsl:text>end </xsl:text>
  <xsl:apply-templates select="id"/>
<xsl:text>;
</xsl:text>
</xsl:template>

<!-- valable pour toute la feuille -->

<xsl:template match="idtyped">
 <xsl:apply-templates select="child::*[position()=1]"/>
 <xsl:text> : </xsl:text>
 <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="idList">
  <xsl:for-each select="child::*">
    <xsl:apply-templates select="."/>
    <xsl:text>;
</xsl:text>
  </xsl:for-each>
</xsl:template>

<xsl:template match="idList" mode="subst">
  <xsl:for-each select="child::*">
    <xsl:apply-templates select="."/>
   </xsl:for-each>
</xsl:template>


<xsl:template match="FunType">
  <xsl:apply-templates select="child::*[position()=1]"/>
   <xsl:text> of </xsl:text>
   <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>


<!-- MRB 5.7 p 46 -->

<xsl:template match="NatSetRange">
 <xsl:text> array (</xsl:text>
  <xsl:apply-templates select="child::*[position()=1]"/>
   <xsl:text> .. </xsl:text>
   <xsl:apply-templates select="child::*[position()=2]"/>
    <xsl:text>)</xsl:text>
</xsl:template>


<xsl:template match="Expr">
  <xsl:apply-templates select="child::*"/>
</xsl:template>

<xsl:template match="ExprList" mode="subst">
  <xsl:apply-templates select="child::*"/>
</xsl:template>

<xsl:template match="ExprList">
  <xsl:apply-templates select="child::*"/>
</xsl:template>


<!-- Balises n'etant pas traite -->
<xsl:template match="CVARIABLES | CONSTRAINTS | INVARIANT | INITIALISATION">

</xsl:template>

<!-- pour la definition des variables -->



<!-- MRB 7.23 p 164 -->
<xsl:template match="OPERATIONS">
  <xsl:for-each select="child::*">
    <xsl:apply-templates select="."/>
  </xsl:for-each>
</xsl:template>

<!-- Genere les prototypes des fonctions -->

<xsl:template match="Operation">
  <xsl:apply-templates select="OperHead"/>
</xsl:template>


<!-- genere les annotations par defaut In Out -->

<xsl:template name="Annotations">
  <xsl:text>
--# Global In Out </xsl:text>
    <xsl:apply-templates select="/AMN/MACHINE/ClausesList/CVARIABLES" mode="annotations"/>
    <xsl:for-each select="/AMN/MACHINE/Head/HeadPar/idtyped">
         <xsl:text>, </xsl:text>
       <xsl:apply-templates select="." mode="nom"/>
    </xsl:for-each>
    <xsl:text>;
</xsl:text>
</xsl:template>


<xsl:template match="CVARIABLES" mode="annotations">
  <xsl:apply-templates select="child::*" mode="annotations"/>
</xsl:template>

<xsl:template match="idList" mode="annotations">
  <xsl:for-each select="child::*">
    <xsl:apply-templates select="." mode="nom"/>
    <xsl:if test="position()!=last()">
      <xsl:text>, </xsl:text>
    </xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="idtyped | id " mode="nom">
 <xsl:apply-templates select="child::*[position()=1]"/>
</xsl:template>


<!-- determine si c'est une fonction ou une procedure -->

<xsl:template match="OperHead">
  <xsl:choose>
    <xsl:when test="count(OperOut/child::*)=1">
      <xsl:text>function </xsl:text>
      <xsl:apply-templates select="OperName"/>
      <xsl:apply-templates select="OperIn" mode="fonction"/>
      <xsl:apply-templates select="OperOut" mode="fonction"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>procedure </xsl:text>
      <xsl:apply-templates select="OperName"/>
      <xsl:if test="count(child::*)>1">
        <xsl:text> (</xsl:text>
      <xsl:apply-templates select="OperIn" mode="procedure"/>
      <xsl:apply-templates select="OperOut" mode="procedure"/>
      <xsl:text>)</xsl:text>
      </xsl:if>
      <xsl:call-template name="Annotations"/>
    </xsl:otherwise>
  </xsl:choose>
<xsl:text> is
</xsl:text>
<xsl:apply-templates select="../OperBody"/>
<xsl:text>
end </xsl:text>
 <xsl:apply-templates select="OperName"/>
<xsl:text>;

</xsl:text>
</xsl:template>

<xsl:template match="OperName">
  <xsl:apply-templates select="id"/>
</xsl:template>

<!-- pour les fonctions -->

<xsl:template match="OperIn" mode="fonction">
  <xsl:text>(</xsl:text> 
  <xsl:apply-templates select="idList/idtyped" mode="fonctionIn"/>
  <xsl:text>)</xsl:text>
</xsl:template>

<xsl:template match="OperOut" mode="fonction">
  <xsl:text> return </xsl:text>
  <xsl:apply-templates select="idList/idtyped" mode="fonction"/>
</xsl:template>

<!-- pour les procedures -->

<xsl:template match="OperIn" mode="procedure">
  <xsl:apply-templates select="idList/idtyped" mode="procedureIn"/>
</xsl:template>

<xsl:template match="OperOut" mode="procedure">
  <xsl:apply-templates select="idList/idtyped" mode="procedureOut"/>
</xsl:template>


<!-- traite le cas de variables typees dans une fonction-->

<xsl:template match="idtyped" mode="fonctionIn">
 <xsl:value-of select="child::*[position()=1]"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:if test="position()!=last()">
    <xsl:text> ; </xsl:text>  
  </xsl:if>
 </xsl:template>


<xsl:template match="idtyped" mode="fonction">
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- pour les procedures -->

<xsl:template match="idtyped" mode="procedureIn">
<xsl:value-of select="child::*[position()=1]"/>
  <xsl:text> : in </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
  <xsl:if test="position()!=last()">
    <xsl:text> ;</xsl:text>  
  </xsl:if>
</xsl:template>

<xsl:template match="idtyped" mode="procedureOut">
<xsl:value-of select="child::*[position()=1]"/>
  <xsl:text> : Out </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/> 
 <xsl:if test="position()!=last()">
    <xsl:text> ; </xsl:text>  
  </xsl:if>
</xsl:template>


<!-- LE CORPS DES FONCTIONS -->

 <!-- pour OperBody -->  

<xsl:template match="OperBody">
<xsl:if test="count(../OperHead/OperOut)=1">
  <xsl:apply-templates select="../OperHead/OperOut/idList"/>
</xsl:if>
  <xsl:text>begin
</xsl:text>
<xsl:apply-templates select="child::*"/>
<xsl:if test="count(../OperHead/OperOut)=1">
  <xsl:text>
return  </xsl:text>
  <xsl:value-of select="../OperHead/OperOut/idList/idtyped/id"/>
<xsl:text>;
</xsl:text>
</xsl:if>
</xsl:template>

<!-- MRB 5.16 p 64 -->
<xsl:template match="ExprFunCall">
  <xsl:apply-templates select="child::*[position()=1]"/>
   <xsl:text>( </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
<xsl:text>)</xsl:text>
</xsl:template>

<!-- MRB 4 p 27 -->
<xsl:template match="Predicate">
  <xsl:apply-templates select="child::*"/>
</xsl:template>

<!-- MRB 4.1 p 28 -->
<!-- proposition AND -->

<xsl:template match="And">   
<xsl:apply-templates select="child::*[position()=1]"/>
<xsl:text> and then  </xsl:text>
<xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!--  MRB 4.3 p 30 -->
<!-- test d egalite -->

<xsl:template match="Equal">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>=</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="NotEqual">
    <xsl:apply-templates select="child::*[position()=1]"/>
    <xsl:text disable-output-escaping="yes"> /=  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- MRB 4.6 p 33 -->
<xsl:template match="Less">
    <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &lt;  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="LessEqual">
    <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &lt;=  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="GreaterEqual">
    <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &gt;=  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>


<xsl:template match="Greater">
    <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text disable-output-escaping="yes"> &gt;  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- fin des references au MRB 4.6 -->


<!-- MRB 5.3 p 39 -->

<xsl:template match="Div">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> / </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Mul">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> *  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Plus">
   <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> + </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Minus">
   <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> - </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Modulo">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> mod  </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="Puissance">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text>**</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- MRB 5.6 p 44 -->
<!-- determine soit le nom soit le type de la variable -->
<xsl:template match="id">
  <xsl:choose>
    <xsl:when test="text()='NAT'">
       <xsl:text> natural </xsl:text>
    </xsl:when>
    <xsl:when test="text()='INT' or text()='INTEGER'">
      <xsl:text> integer </xsl:text>
    </xsl:when>
    <xsl:when test="text()='BOOL'">
      <xsl:text> boolean </xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="."/>
    </xsl:otherwise>
 </xsl:choose>
</xsl:template>



<!-- MRB 6.3 p 86 -->

<xsl:template match="SetEqualIds">
  <xsl:apply-templates select="child::*[position()=1]" mode="subst"/>
  <xsl:text>:=</xsl:text>
  <xsl:apply-templates select="child::*[position()=2]" mode="subst"/>
  <xsl:text>;</xsl:text>
</xsl:template>



<xsl:template match="BoolEvaluation">
  <xsl:text>(</xsl:text>
  <xsl:apply-templates select="child::*"/>
  <xsl:text>)</xsl:text> 
</xsl:template>


<!-- MRB 6.4 p 88 -->
<!-- substitutions preconditions -->

<xsl:template match="SubstPre"> 
<xsl:apply-templates select="PRE"/>
<xsl:apply-templates select="THEN"/>
</xsl:template>

<!-- traitement de la precondition -->

<xsl:template match="PRE">
  <xsl:text>--# Pre </xsl:text>
<xsl:apply-templates select="child::*" mode="Pre"/>
 <xsl:text>;
</xsl:text>
</xsl:template>

<xsl:template match="Predicate" mode="Pre">
  <xsl:choose>
    <xsl:when test="And">
      <xsl:apply-templates select="And" mode="Pre"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="child::*"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="And" mode="Pre">
  <xsl:choose>
    <xsl:when test="name(child::*[position()=1])='And'">
      <xsl:apply-templates select="child::*[position()=1]" mode="Pre"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="child::*[position()=1]"/>
    </xsl:otherwise>
  </xsl:choose>
  <xsl:text>
--# Pre </xsl:text>
  <xsl:choose>
    <xsl:when test="name(child::*[position()=2])='And'">
      <xsl:apply-templates select="child::*[position()=2]" mode="Pre"/>
    </xsl:when>
    <xsl:otherwise>
      <xsl:apply-templates select="child::*[position()=2]"/>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="TotalFunc">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> of </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<xsl:template match="ExprParen">
  <xsl:text> ( </xsl:text>
  <xsl:apply-templates select="child::*"/>
  <xsl:text> ) </xsl:text>
</xsl:template>

<xsl:template match="In">
  <xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:text> : </xsl:text>
  <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>

<!-- traitement du corps de la precondition -->

<xsl:template match="THEN">
  <xsl:apply-templates select="child::*"/>
</xsl:template>


<!-- MRB 6.15 p 101 -->
<xsl:template match="SubstSeq">
  <xsl:for-each select="child::*">
    <xsl:apply-templates select="."/>
    <xsl:if test="position()!=last()">
    <xsl:text>
</xsl:text>
   </xsl:if>
   </xsl:for-each>
</xsl:template>

<!-- MRB 6.1 p 84 -->
<xsl:template match="SubstBlock">
<xsl:text>begin
</xsl:text>
<xsl:for-each select="child::*">
<xsl:apply-templates select="."/>
</xsl:for-each>
<xsl:text>
end;
</xsl:text>
  </xsl:template>

<!-- MRB 6.16 p 103 -->
<xsl:template match="SetEqualFun">
   <xsl:apply-templates select="child::*[position()=1]"/>
   <xsl:text> := </xsl:text>
   <xsl:apply-templates select="child::*[position()=2]"/>
   <xsl:text>;</xsl:text>
</xsl:template>

<!-- MRB 4.1 p 28 -->
<!-- negation -->

<xsl:template match="Neg">
  <xsl:text>not </xsl:text>
  <xsl:apply-templates select="child::*"/>
</xsl:template>

<!-- parentheses -->
<xsl:template match="PredParen">
   <xsl:text>( </xsl:text>
   <xsl:apply-templates select="child::*"/>
    <xsl:text> )</xsl:text>
</xsl:template>



<!-- MRB 6.17 p 105 -->
<xsl:template match="SubstWhile">
   <xsl:text>while </xsl:text>
    <xsl:apply-templates select="WhileTest"/>
<xsl:text> loop
</xsl:text>
 <xsl:apply-templates select="WhileBody"/>
    <xsl:text>
end loop ;
</xsl:text>
</xsl:template>

<xsl:template match="WhileTest">
  <xsl:apply-templates select="child::*"/>
</xsl:template>

<xsl:template match="WhileBody">
  <xsl:apply-templates select="child::*"/>
</xsl:template>

<!-- MRB 6.7 p 91 -->

<xsl:template match="SubstIf">
 <xsl:text>if </xsl:text>
<xsl:apply-templates select="child::*[position()=1]"/>
  <xsl:for-each select="child::*[position()>1]">
    <xsl:choose>
      <xsl:when test="name()='IfThen'">
        <xsl:text>
elsif </xsl:text>
          <xsl:apply-templates select="."/>
      </xsl:when>
      <xsl:otherwise>
              <xsl:text>
else </xsl:text>
        <xsl:apply-templates select="."/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:for-each>
  <xsl:text>
end if ;
 </xsl:text>
</xsl:template>
  
<xsl:template match="IfThen">
      <xsl:apply-templates select="child::*[position()=1]"/>
      <xsl:text> then
</xsl:text>
      <xsl:apply-templates select="child::*[position()=2]"/>
</xsl:template>


</xsl:stylesheet>