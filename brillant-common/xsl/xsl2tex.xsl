<!-- 
$Id$
-->
<xsl:stylesheet version="1.0"
     xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text"/>

<xsl:template match="xsl:stylesheet">
\documentclass[a4paper]{report}
\usepackage{verbatim}
\usepackage{moreverb}
\usepackage{fullpage}
\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}
\usepackage[frenchb]{babel}
\input{xsl.sty}
\usepackage{makeidx}
\makeindex
\begin{document}
\begin{stylesheet}{<xsl:value-of select="./@version"/>}
<xsl:apply-templates/>
\end{stylesheet}
\appendix
\printindex
\end{document}
</xsl:template>

<xsl:template match="xsl:output">
\begin{output1}{<xsl:value-of select="./@method"/>}{<xsl:value-of select="./@omit-xml-declaration"/>}
\end{output1}
</xsl:template>

<xsl:template match="xsl:template">
\begin{template}{<xsl:value-of select="name(./@*)"/>}{<xsl:value-of select="./@*"/>}
<xsl:apply-templates/>
\end{template}
</xsl:template>

<xsl:template match="xsl:apply-templates">
<xsl:if test="name(./@*)='select'">
\begin{apply-templates2}{<xsl:value-of select="./@select"/>}
\end{apply-templates2}
</xsl:if>
<xsl:if test="name(./@*)=''">
\begin{apply-templates1}
\end{apply-templates1}
</xsl:if>
</xsl:template>

<xsl:template match="xsl:choose">
\begin{choose1}
<xsl:apply-templates/>
\end{choose1}
</xsl:template>

<xsl:template match="xsl:when">
\begin{when}{<xsl:value-of select="./@test"/>}
<xsl:apply-templates/>
\end{when}
</xsl:template>

<xsl:template match="xsl:otherwise">
\begin{otherwise}
<xsl:apply-templates/>
\end{otherwise}
</xsl:template>

<xsl:template match="xsl:text">
\begin{text1}
\verb+<xsl:value-of select="normalize-space()"/>+
\end{text1}
</xsl:template>

<xsl:template match="xsl:value-of">
\begin{value-of}{<xsl:value-of select="./@select"/>}
\end{value-of}
</xsl:template>

<xsl:template match="xsl:if">
\begin{if1}{<xsl:value-of select="./@test"/>}
<xsl:apply-templates/>
\end{if1}
</xsl:template>

<xsl:template match="xsl:for-each">
\begin{foreach}{<xsl:value-of select="./@select"/>}
<xsl:apply-templates/>
\end{foreach}
</xsl:template>

<xsl:template match="xsl:call-template">
\begin{call-template}{<xsl:value-of select="./@name"/>}
<xsl:apply-templates/>
\end{call-template}
</xsl:template>

</xsl:stylesheet>






