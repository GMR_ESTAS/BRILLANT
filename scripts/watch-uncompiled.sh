#!/bin/sh

files=`find parser-watch/ -regex "^.*\(\.mch\|\.ref\|\.imp\)$"`

for i in $files
do
    file=`echo "$i" | sed -e 's!^.*/\([a-zA-Z0-9_]\+\(\.mch\|\.ref\|\.imp\)\)$!\1!g'`
    path=`echo "$i" | sed -e 's!^\(.*\)/\([a-zA-Z0-9_]\+\(\.mch\|\.ref\|\.imp\)\)$!\1!g'`
    xmlpath=`echo "$path" | sed -e 's!spec$!xml!'`
    if [ ! -f "$xmlpath"/"$file".xml ]
    then
	echo "$i"
    fi
done
