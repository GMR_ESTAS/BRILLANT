(*==================================================================================================
  Project : Study of the concept of refinement
  Module :gen_ref
  This is our gen_refinement, combining data-refinement, choice-refinement with preconditions and
  guards
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: July 2009, Md: July 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export relation_gen.

(* A Few Quick Checks ----------------------------------------------------------------------------*)

Theorem PreDomCom:forall (Da Dc Rc:Set)(P:⇑Da)(S:Dc↔Rc)(I:Da↔Dc), P⊲(S∘I)≡S∘(P⊲I).
Proof.
 intros Da Dc Rc P S I; split; intros [da rc] H; inversion_clear H; inversion_clear H0;
  rename b into dc.
 apply RCmp_ with (b:=dc).
  apply (RDomR_ _ _ _ _ _ _ H H1).
  apply H2.
 apply RDomR_.
  apply RCmp_ with (b:=dc); [apply H | apply H1].
  apply H2.
Qed.

Theorem RDomR_distr:forall (Da Ra Rc:Set)(P:⇑Da)(S:Da↔Ra)(I:Ra↔Rc), P⊲(I∘S)≡(S@P⊲I)∘(P⊲S).
Proof.
 intros Da Ra Rc P S I; split; intros [da rc] H; inversion_clear H.
  inversion_clear H0; rename b into ra; apply RCmp_ with (b:=ra).
   apply RDomR_.
    apply H.
    apply H1.
   apply RDomR_.
    apply H2.
    apply RImg_ with (a:=da).
     apply H1.
     apply H.
  inversion_clear H0; rename b into ra.
  inversion_clear H1.
  inversion_clear H3; rename a into da'.
  apply RDomR_.
   apply RCmp_ with (b:=ra).
    apply H.
    apply H0.
   apply H2.
Qed.

Theorem RDomR_fact1:forall (Da Ra Rc:Set)(P:⇑Da)(S:Da↔Ra)(I:Ra↔Rc), (S@P⊲I)∘(P⊲S)⊆(S@P⊲I)∘S.
Proof.
 intros Da Ra Rc P S I [da rc] H; inversion_clear H; inversion_clear H0; inversion_clear H1;
  rename b into ra; apply RCmp_ with (b:=ra).
  apply H.
  apply RDomR_.
   apply H0.
   apply H3.
Qed.

(* Refinement with prioritary preconditions and guards -------------------------------------------*)

Definition GenRef(Da Ra Dc Rc:Set)
                 (Sa:Da↔Ra)(Pa:⇑Da)
                 (Sc:Dc↔Rc)(Pc:⇑Dc)
                 (Id:Da↔Dc)(Ir:Ra↔Rc):=Id@Pa⊆Pc /\ Pa⊲(Sc∘Id)⊆Ir∘Sa.
Notation "'〈' Pa '|' Sa '⊑' Pc '|' Sc '〉[' Id ',' Ir ']'":=
 (GenRef _ _ _ _ Sa Pa Sc Pc Id Ir) (no associativity, at level 90).

Definition GOpRef(Da Ra Dc Rc:Set)(Sa:Da↔Ra)(Sc:Dc↔Rc)(Pc:⇑Dc)(Id:Da↔Dc)(Ir:Ra↔Rc):=
 ∏2(Id)⊆Pc /\ Sc∘Id⊆Ir∘Sa.
Notation "'〈' Sa '⊑' Pc '|' Sc '〉[' Id ',' Ir ']'":=
 (GOpRef _ _ _ _ Sa Sc Pc Id Ir) (no associativity, at level 90).

(* Properties of GenRef --------------------------------------------------------------------------*)

Theorem GenRef_opt:forall (Da Ra Dc Rc:Set)(Sa:Da↔Ra)(Pa:⇑Da)(Sc:Dc↔Rc)(Pc:⇑Dc)(Id:Da↔Dc)(Ir:Ra↔Rc),
                   〈Pa|Sa⊑Pc|Sc〉[Id,Ir]<->〈Sa⊑Pc|Sc〉[Pa⊲Id,Ir].
Proof.
 intros Da Ra Dc Rc Sa Pa Sc Pc Id Ir; split.
  intros [HPre HRel]; simpl; split.
   intros dc Hdc; inversion_clear Hdc; rename a into da; apply HPre; inversion_clear H;
    apply RImg_ with (a:=da).
    apply H1.
    apply H0.
   apply (SInc_trans _ _ _ _ (proj2 (PreDomCom _ _ _ Pa Sc Id)) HRel).
 simpl; intros [HPre HRel]; split.
  intros dc H; apply HPre; inversion_clear H; rename a into da; apply RRan_ with (a:=da);
   apply RDomR_.
   apply H1.
   apply H0.
  apply (SInc_trans _ _ _ _ (proj1 (PreDomCom _ _ _ Pa Sc Id)) HRel).
Qed.
(* Nota: this illustrates that Id can be used to weaken the precondition ; ideally one should
   consider a total Id and then reduce its scope with Pa *)

Theorem GOpRef_refl:forall (D R:Set)(S:D↔R)(Pa Pc:⇑D), Pa⊆Pc->〈S⊑Pc|S〉[Pa⊲Ⅰ(D),Ⅰ(R)].
Proof.
 intros D R S Pa Pc HInc; split.
  intros d Hd; inversion_clear Hd; inversion_clear H; inversion H0; rewrite H3 in *;
   apply (HInc _ H1).
  intros [d r] Hdr; inversion_clear Hdr; inversion_clear H; inversion H1; rewrite H4 in *;
   apply RCmp_ with (b:=r); [apply H0 | apply RId_].
Qed.

Theorem GenRef_refl:forall (D R:Set)(S:D↔R)(Pa Pc:⇑D), Pa⊆Pc->〈Pa|S⊑Pc|S〉[Ⅰ(D),Ⅰ(R)].
Proof.
 intros D R S Pa Pc HInc.
 apply ((proj2 (GenRef_opt _ _ _ _ S Pa S Pc (Ⅰ(D)) (Ⅰ(R)))) (GOpRef_refl _ _ S _ _ HInc)).
Qed.

Theorem GOpRef_tran:
 forall (Da Ra Di Ri Dc Rc:Set)(Sa:Da↔Ra)(Si:Di↔Ri)(Sc:Dc↔Rc)(Pi:⇑Di)(Pc:⇑Dc)
        (Id:Da↔Di)(Ir:Ra↔Ri)(Id':Di↔Dc)(Ir':Ri↔Rc),
 〈Sa⊑Pi|Si〉[Id,Ir]->〈Si⊑Pc|Sc〉[Pi⊲Id',Ir']->〈Sa⊑Pc|Sc〉[(Pi⊲Id')∘Id,Ir'∘Ir].
Proof.
 intros Da Ra Di Ri Dc Rc Sa Si Sc Pi Pc Id Ir Id' Ir' [HP HR] [HP' HR']; split.
  intros dc Hdc; apply HP'; inversion_clear Hdc; inversion_clear H; apply (RRan_ _ _ _ _ _ H1).
  intros [da rc] H; inversion_clear H; rename b into dc; inversion_clear H0; rename b into di.
  generalize (RCmp_ _ _ _ _ _ _ _ H2 _ H1); intros H0.
  generalize (HR' _ H0); intros H3; inversion_clear H3; rename b into ri.
  apply (proj1 (RCmp_assoc _ _ _ _ Sa Ir Ir')).
  apply RCmp_ with (b:=ri); [ | apply H5].
   apply HR; apply RCmp_ with (b:=di); [apply H | apply H4].
Qed.

Theorem GenRef_tran:
 forall (Da Ra Di Ri Dc Rc:Set)(Sa:Da↔Ra)(Si:Di↔Ri)(Sc:Dc↔Rc)(Pa:⇑Da)(Pi:⇑Di)(Pc:⇑Dc)
        (Id:Da↔Di)(Ir:Ra↔Ri)(Id':Di↔Dc)(Ir':Ri↔Rc),
 〈Pa|Sa⊑Pi|Si〉[Id,Ir]->〈Pi|Si⊑Pc|Sc〉[Id',Ir']->〈Pa|Sa⊑Pc|Sc〉[Id'∘Id,Ir'∘Ir].
Proof.
 intros Da Ra Di Ri Dc Rc Sa Si Sc Pa Pi Pc Id Ir Id' Ir' [HP HR] [HP' HR']; split.
  intros dc Hdc; inversion_clear Hdc; rename a into da; inversion_clear H0; rename b into di.
  apply HP'; generalize (HP _ (RImg_ _ _ _ _ _ H _ H1)); intros H0; apply (RImg_ _ _ _ _ _ H0 _ H2).
  intros [da rc] H; inversion_clear H; inversion_clear H0; rename b into dc; inversion_clear H;
   rename b into di.
  apply (proj1 (RCmp_assoc _ _ _ _ Sa Ir Ir')).
  generalize (RImg_ _ _ _ _ _ H1 _ H0); intros H.
  generalize (HP _ H); intros H4.
  generalize (RImg_ _ _ _ _ _ H4 _ H3); intros H5.
  generalize (RCmp_ _ _ _ _ _ _ _ H3 _ H2); intros H7.
  generalize (RDomR_ _ _ _ _ _ _ H7 H4); intros H8.
  generalize (HR' _ H8); intros H9.
  inversion_clear H9; rename b into ri.
  generalize (RCmp_ _ _ _ _ _ _ _ H0 _ H6); intros H9.
  generalize (RDomR_ _ _ _ _ _ _ H9 H1); intros H12.
  generalize (HR _ H12); intros H13.
  apply (RCmp_ _ _ _ _ _ _ _ H13 _ H10).
Qed.

Theorem GOpRef_composer:
 forall (Da Ia Ra Dc I1c I2c Rc:Set)
        (Sa:Da↔Ia)(Sa':Ia↔Ra)
        (Sc:Dc↔I1c)(Pc:⇑Dc)(Sc':I2c↔Rc)(Pc':⇑I2c)
        (G:I1c↔I2c)
        (Id:Da↔Dc)(Ii1:Ia↔I1c)(Ii2:Ia↔I2c)(Ir:Ra↔Rc),
        〈Sa⊳∏1(Ii2)⊑Pc|Sc〉[Id,Ii1]->〈Sa'⊑Pc'|Sc'〉[Ii2,Ir]->G∘Ii1⊆Ii2->
        〈Sa'∘Sa⊑Pc|Sc'∘G∘Sc〉[Id,Ir].
Proof.
 intros Da Ia Ra Dc I1c I2c Rc Sa Sa' Sc Pc Sc' Pc' G Id Ii1 Ii2 Ir [HP1 HR1] [HP2 HR2] HInc; split.
  intros dc Hdc; apply HP1; apply Hdc.
  intros [da rc] H.
  generalize (proj2 (RCmp_assoc _ _ _ _ Id Sc (Sc'∘G)) _ H); intros H0; inversion_clear H0;
   rename b into i1c.
  generalize (HR1 _ H1); intros H0; inversion_clear H0; rename b into ia.
  inversion_clear H3.
  apply (proj2 (RCmp_assoc _ _ _ _ Sa Sa' Ir)); apply RCmp_ with (b:=ia).
   apply H0.
   apply HR2; inversion_clear H2; rename b into i2c.
   apply RCmp_ with (b:=i2c); [ | apply H6].
    apply HInc.
    apply RCmp_ with (b:=i1c).
     apply H4.
     apply H3.
Qed.

Theorem GenRef_composer:
 forall (Da Ia Ra Dc I1c I2c Rc:Set)
        (Sa:Da↔Ia)(Pa:⇑Da)(Sa':Ia↔Ra)(Pa':⇑Ia)
        (Sc:Dc↔I1c)(Pc:⇑Dc)(Sc':I2c↔Rc)(Pc':⇑I2c)
        (G:I1c↔I2c)
        (Id:Da↔Dc)(Ii1:Ia↔I1c)(Ii2:Ia↔I2c)(Ir:Ra↔Rc),
        〈Pa|Sa⊳∏1(Pa'⊲Ii2)⊑Pc|Sc〉[Id,Ii1]->〈Pa'|Sa'⊑Pc'|Sc'〉[Ii2,Ir]->G∘Ii1⊆Ii2->
        〈Pa|(Sa'∘Sa)⊑Pc|Sc'∘G∘Sc〉[Id,Ir].
Proof.
 intros Da Ia Ra Dc I1c I2c Rc Sa Pa Sa' Pa' Sc Pc Sc' Pc' G Id Ii1 Ii2 Ir [HP1 HR1] [HP2 HR2] HInc;
  split.
  intros dc Hdc; apply HP1; apply Hdc.
  intros [da rc] H; inversion_clear H.
  generalize (proj2 (RCmp_assoc _ _ _ _ Id Sc (Sc'∘G)) _ H0); intros H; inversion_clear H;
   rename b into i1c.
  generalize (HR1 _ (RDomR_ _ _ _ _ _ _ H2 H1)); intros H4.
  inversion_clear H4; rename b into ia; inversion_clear H.
  apply (proj2 (RCmp_assoc _ _ _ _ Sa Sa' Ir)).
  apply RCmp_ with (b:=ia).
   apply H4.
   apply HR2; inversion_clear H3; rename b into i2c; inversion_clear H6; inversion_clear H3.
   apply RDomR_; [ | apply H8].
    apply RCmp_ with (b:=i2c); [ | apply H7].
     apply HInc.
     apply RCmp_ with (b:=i1c).
      apply H5.
      apply H.
Qed.

(* Relations with other Refinements --------------------------------------------------------------*)

Require Export data_ref.
Require Export choice_ref.

Theorem DatRef_rel:forall (Da Ra Dc Rc:Set)(fa:Da->Ra)(fc:Dc->Rc)(Id:Da↔Dc)(Ir:Ra↔Rc),
                   §(fa)⇝§(fc)[Id,Ir]<->(⇔fc)∘Id⊆Ir∘(⇔fa).
Proof.
 intros Da Ra Dc Rc fa fc Id Ir; unfold DatRef; unfold SInc; simpl; split; intros Href.
  intros [xa yc] Hrel; inversion_clear Hrel; rename b into xc; inversion_clear H0;
   apply RCmp_ with (b:=fa(xa)); [apply RofF_ | apply Href; apply H].
  intros xa xc Hrel; generalize (RCmp_ _ _ _ _ (⇔fc) _ _ Hrel (fc xc) (RofF_ _ _ fc xc));
   intros Hrel'; generalize (Href (xa,fc xc) Hrel'); intros Hrel''; inversion_clear Hrel'';
   inversion H; rewrite <- H3 in H0; apply H0.
Qed.

Theorem DatRef_GOpRef:forall (Da Ra Dc Rc:Set)(fa:Da->Ra)(fc:Dc->Rc)(Pc:⇑Dc)(Id:Da↔Dc)(Ir:Ra↔Rc),
                      §(fa)⇝§(fc)[Id,Ir]->∏2(Id)⊆Pc->〈(⇔fa)⊑Pc|(⇔fc)〉[Id,Ir].
Proof.
 intros Da Ra Dc Rc fa fc Pc Id Ir HRef HPre; split.
  apply HPre.
  apply (proj1 (DatRef_rel Da Ra Dc Rc fa fc Id Ir)); apply HRef.
Qed.

Theorem ChPRef_CmbRef:forall (D R:Set)(Sa Sc:D↔R)(Pa Pc:⇑D),
                      〈Pa|Sa≻Pc|Sc〉->〈Sa⊑Pc|Sc〉[Pa⊲Ⅰ(D),Ⅰ(R)].
Proof.
 intros D R Sa Sc Pa Pc [HP HR]; split.
  intros d H; inversion_clear H; inversion_clear H0; inversion H; rewrite H3 in *;
   apply (HP _ H1).
  intros [d r] H; inversion_clear H; inversion_clear H0; inversion H; rewrite H4 in *;
   apply RCmp_ with (b:=r); [ | apply RId_].
   apply HR; apply RDomR_.
    apply H1.
    apply H2.
Qed.

(* Illustration 1 : Boolean Choice --------------------------------------------------------------*)
(* ANY function from nat to bool can refine the specification true [] false *)

Section bool_choice.
 Definition Sa_bc:unit↔bool:=⊛(unit*bool).
 Variable Sc_bc:nat->bool.
 Definition Id_bc:unit↔nat:=⊛(unit*nat).
 Definition Ir_bc:bool↔bool:=Ⅰ(bool).
 Theorem bool_choice_ref:〈Sa_bc⊑⊛(nat)|(⇔Sc_bc)〉[Id_bc,Ir_bc].
 Proof.
  split.
   intros n _; apply SFul_.
   intros [u b] H; inversion_clear H; rename b0 into n; clear H0; inversion_clear H1.
   apply RCmp_ with (b:=Sc_bc n).
    unfold Sa_bc; apply SFul_.
    unfold Ir_bc; apply RId_.
 Qed.
End bool_choice.

(* Illustration 2 : Maximum of a list ------------------------------------------------------------*)
(* This example, derived from the Maximier from the B-Book, illustrates optimisation through the use
   of a non injective interpretation over the domain. To represent a B machine, we represent the
   state as part of both inputs and outputs; multiple operations are represented through a single
   composite function with an additional parameter acting as a selector. *)

Section maximier.
 Require Import List.
 Require Import Max.
 Definition lmax(l:list nat):=fold_left max l 0.
 Theorem lmax1:forall (l:list nat)(n:nat), lmax (n::l)=(fold_left max l n).
 Proof.
  intros l n; unfold lmax; apply refl_equal.
 Qed.
 Theorem lmax2:forall (l:list nat)(n:nat), lmax (n::l)=max (lmax l) n.
 Proof.
  induction l as [| h l]; intros n; unfold lmax; simpl.
   apply refl_equal.
   repeat (rewrite <- lmax1); repeat (rewrite IHl); induction (Compare_dec.le_lt_dec n h).
    rewrite (max_r _ _ a); induction (Compare_dec.le_lt_dec (lmax l) h).
     rewrite (max_r _ _ a0); rewrite max_comm; rewrite (max_r _ _ a); apply refl_equal.
     rewrite (max_l _ _ (Lt.lt_le_weak _ _ b));
      rewrite (max_l _ _ (Le.le_trans _ _ _ a (Lt.lt_le_weak _ _ b))); apply refl_equal.
    rewrite (max_l _ _ (Lt.lt_le_weak _ _ b)); induction (Compare_dec.le_lt_dec (lmax l) n).
     rewrite (max_r _ _ a); induction (Compare_dec.le_lt_dec (lmax l) h).
      rewrite (max_r _ _ a0); rewrite (max_r _ _ (Lt.lt_le_weak _ _ b)); apply refl_equal.
      rewrite (max_l _ _ (Lt.lt_le_weak _ _ b0)); rewrite (max_r _ _ a); apply refl_equal.
    rewrite (max_l _ _ (Lt.lt_le_weak _ _ b0)).
    rewrite (max_l _ _ (Le.le_trans _ _ _ (Lt.lt_le_weak _ _ b) (Lt.lt_le_weak _ _ b0)));
     rewrite (max_l _ _ (Lt.lt_le_weak _ _ b0)); apply refl_equal.
 Qed.
 Inductive Pa_mx:(nat*(list nat)*bool)↔((list nat)*nat):=
                 (* (to store,store,selector),(new store,max) *)
  | Pa_mx_stor:forall (n:nat)(l:list nat), Pa_mx((n,l,true),(n::l,0))
  | Pa_mx_read:forall (n h:nat)(l:list nat), Pa_mx((n,h::l,false),(h::l,lmax (h::l))).
 Definition Pc_mx(nmb:nat*nat*bool):nat*nat:=
  match nmb with
  | (n,m,true) => match (Compare_dec.le_lt_dec n m) with left _ => (0,m) | right _ => (0,n) end
  | (n,m,false) => (m,m)
  end.
 Inductive Id_mx:(nat*(list nat)*bool)↔(nat*nat*bool):=
  Id_mx_:forall (n h:nat)(l:list nat)(b:bool), Id_mx((n,h::l,b),(n,lmax (h::l),b)).
 Inductive Ir_mx:((list nat)*nat)↔(nat*nat):=
  Ir_mx_:forall (n h:nat)(l:list nat), Ir_mx((h::l,n),(n,lmax (h::l))).
 Theorem maximier_ref:〈Pa_mx⊑⊛(nat*nat*bool)|(⇔Pc_mx)〉[Id_mx,Ir_mx].
 Proof.
  split.
   intros nmb _; apply SFul_.
   intros [[[n l] b] [n' m']] H; inversion_clear H; destruct b0 as [[n'' m''] b''].
   inversion H0.
   inversion_clear H1.
   destruct b'' as [ | ]; simpl.
    apply RCmp_ with (b:=(n''::h::l0,0)).
     apply Pa_mx_stor.
     destruct (Compare_dec.le_lt_dec n'' m'').
      assert (Hmax:m''=lmax (n''::h::l0)).
       rewrite lmax2; rewrite H6; rewrite (max_l _ _ l1); apply refl_equal.
      rewrite Hmax; apply Ir_mx_.
      assert (Hmax:n''=lmax (n''::h::l0)).
       rewrite lmax2; rewrite H6; rewrite (max_r _ _ (Lt.lt_le_weak _ _ l1)); apply refl_equal.
      pattern n'' at 2; rewrite Hmax; apply Ir_mx_.
    apply RCmp_ with (b:=(h::l0,m'')).
     rewrite <- H6; apply Pa_mx_read.
     pattern m'' at 3; rewrite <- H6; apply Ir_mx_.
 Qed.
End maximier.

(* Illustration 3 : Miracles ---------------------------------------------------------------------*)

Section miracle.
 Variable D:Set.
 Variable R:Set.
 Variable Pa_mir:D↔R.
 Definition Pc_mir:unit↔unit:=∅(unit*unit).
 Definition Id_mir:D↔unit:=⊛(D*unit).
 Definition Ir_mir:R↔unit:=⊛(R*unit).
 Theorem miracle_ref:〈Pa_mir⊑⊛(unit)|Pc_mir〉[Id_mir,Ir_mir].
 Proof.
  split.
   intros u _; apply SFul_.
   intros [d u] Hdu; inversion Hdu; inversion H2; destruct H3.
 Qed.
End miracle.

(* Illustration 4 : Natural addition -------------------------------------------------------------*)
(* The specification is the addition over (unbounded) naturals. The implementation is partial, using
   a 4-bits concrete representation and a partial interpretation. *)

Section nat_to_int.
 Inductive Pa_ni:(nat*nat)↔nat:= Pa_ni_:forall (a b:nat), Pa_ni((a,b),a+b).
 Inductive bit:Set:= v0:bit | v1:bit.
 Definition bit2nat(b:bit):nat:=match b with v0 => 0 | v1 => 1 end.
 Coercion bit2nat:bit>->nat.
 Definition comp1(b:bit):bit:=match b with v0 => v1 | v1 => v0 end.
 Definition add11(b1 b2 b3:bit):=
  match (b1,b2,b3) with (v0,v0,_) => (v0,b3) | (v1,v1,_) => (v1,b3) | _ => (b3,comp1 b3) end.
 Inductive w4:Set:=W:bit->bit->bit->bit->w4.
 Definition nat_of_w4(w:w4):nat:=match w with W b3 b2 b1 b0 => (((b3*2+b2)*2+b1)*2+b0) end.
 Coercion nat_of_w4:w4>->nat.
 Definition add(w w':w4):=
  match (w,w') with (W b13 b12 b11 b10,W b23 b22 b21 b20) =>
   match add11 b10 b20 v0 with (c0,b30) =>
    match add11 b11 b21 c0 with (c1,b31) =>
     match add11 b12 b22 c1 with (c2,b32) =>
      match add11 b13 b23 c2 with (c3,b33) =>
       (c3,W b33 b32 b31 b30) end end end end end.
 Inductive Pc_ni:(w4*w4)↔w4:=Pc_ni_:forall (ww:w4*w4), Pc_ni(ww,snd (add (fst ww) (snd ww))).
 Inductive Id_ni:(nat*nat)↔(w4*w4):=
  Id_ni_:forall (w w':w4), w+w'<=15->Id_ni(pair (nat_of_w4 w) (nat_of_w4 w'),pair w w').
 Inductive Ir_ni:nat↔w4:=Ir_ni_:forall (w:w4), Ir_ni(nat_of_w4 w,w).
 Theorem nat_to_int_ref:〈Pa_ni⊑⊛(w4*w4)|Pc_ni〉[Id_ni,Ir_ni].
 Proof.
  split.
   intros w _; apply SFul_.
   intros [[n1 n2] w12] H; inversion_clear H; destruct b as [w1 w2]; apply RCmp_ with (b:=n1+n2).
    apply Pa_ni_.
    inversion_clear H1; simpl.
    inversion_clear H0.
    destruct w1 as [[ | ] [ | ] [ | ] [ | ]]; destruct w2 as [[ | ] [ | ] [ | ] [ | ]]; simpl in *;
    try (change 0 with (nat_of_w4 (W v0 v0 v0 v0)); apply Ir_ni_);
    try (change 1 with (nat_of_w4 (W v0 v0 v0 v1)); apply Ir_ni_);
    try (change 2 with (nat_of_w4 (W v0 v0 v1 v0)); apply Ir_ni_);
    try (change 3 with (nat_of_w4 (W v0 v0 v1 v1)); apply Ir_ni_);
    try (change 4 with (nat_of_w4 (W v0 v1 v0 v0)); apply Ir_ni_);
    try (change 5 with (nat_of_w4 (W v0 v1 v0 v1)); apply Ir_ni_);
    try (change 6 with (nat_of_w4 (W v0 v1 v1 v0)); apply Ir_ni_);
    try (change 7 with (nat_of_w4 (W v0 v1 v1 v1)); apply Ir_ni_);
    try (change 8 with (nat_of_w4 (W v1 v0 v0 v0)); apply Ir_ni_);
    try (change 9 with (nat_of_w4 (W v1 v0 v0 v1)); apply Ir_ni_);
    try (change 10 with (nat_of_w4 (W v1 v0 v1 v0)); apply Ir_ni_);
    try (change 11 with (nat_of_w4 (W v1 v0 v1 v1)); apply Ir_ni_);
    try (change 12 with (nat_of_w4 (W v1 v1 v0 v0)); apply Ir_ni_);
    try (change 13 with (nat_of_w4 (W v1 v1 v0 v1)); apply Ir_ni_);
    try (change 14 with (nat_of_w4 (W v1 v1 v1 v0)); apply Ir_ni_);
    try (change 15 with (nat_of_w4 (W v1 v1 v1 v1)); apply Ir_ni_);
    repeat (inversion_clear H; inversion_clear H0).
 Qed.
End nat_to_int.

(*================================================================================================*)