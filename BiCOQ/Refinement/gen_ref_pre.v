(*==================================================================================================
  Project : Study of the concept of refinement
  Module : gen_ref_pre
  We study the concept of gen-refinement, in the sense that it combines data-refinement and
  choice-refinement: starting from a relation between abstract supports, we aim at producing an
  "compliant" function (a functional relation) between concrete supports. We start by providing a
  relational presentation of data-refinement to have a common universe of discourse.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: April 2008, Md: Mar 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export data_ref.
Require Export choice_ref_pre.

(* Data-refinement equivalent definition ---------------------------------------------------------*)

Theorem DatRef_rel:forall (Da Ra Dc Rc:Set)(fa:Da->Ra)(fc:Dc->Rc)(Id:Da↔Dc)(Ir:Ra↔Rc),
                   §(fa)⇝§(fc)[Id,Ir]<->(⇔fc)∘Id⊆Ir∘(⇔fa).
Proof.
 intros Da Ra Dc Rc fa fc Id Ir; unfold DatRef; unfold SInc; simpl; split; intros Href.
  intros [xa yc] Hrel; inversion_clear Hrel; rename b into xc; inversion_clear H0;
   apply RCmp_ with (b:=fa(xa)); [apply RofF_ | apply Href; apply H].
  intros xa xc Hrel; generalize (RCmp_ _ _ _ _ (⇔fc) _ _ Hrel (fc xc) (RofF_ _ _ fc xc));
   intros Hrel'; generalize (Href (xa,fc xc) Hrel'); intros Hrel''; inversion_clear Hrel'';
   inversion H; rewrite <- H3 in H0; apply H0.
Qed.

(* Gen-refinement definition ---------------------------------------------------------------------*)
(* Basically, considering definitions of support refinement and sharpening refinement, the combined
   refinement could be defined by Id@∏1(Pa)⊆∏1(Pc) /\ (Id@∏1(Pa)⊲Pc)∘Id⊆Ir∘Pa. Id@∏1(Pa)⊆∏1(Pc)
   ensures that the refinement Pc covers the case specified by the specification. Indeed we noticed
   that if Id is nearly empty, then it is a very weak specification because we just ask for a
   partial refinement of Pa. Similarly, if Pa is not total, then it is a partial specification
   letting freedom for the refinement. So Id@∏1(Pa) is the specified domain. However the second part
   is more tricky. (Id@∏1(Pa)⊲Pc)∘Id⊆Ir∘Pa is complex, and one could consider a simpler requirement
   ∏1(Pa)⊲(Pc∘Id)⊆Ir∘Pa. Note that ∏1(Pa)⊲(Pc∘Id)⊆(Id@∏1(Pa)⊲Pc)∘Id but not the contrary.
   Considering the intuitive example of square root over relatives numbers, the simpler version
   seems to better reflect what we expect from a refinement. *)

Theorem GePRef_simpl:forall (Da Ra Dc Rc:Set)(Pa:Da↔Ra)(Pc:Dc↔Rc)(Id:Da↔Dc),
                     ∏1(Pa)⊲(Pc∘Id)⊆(Id@∏1(Pa)⊲Pc)∘Id.
Proof.
 intros Da Ra Dc Rc Pa Pc Id; unfold SInc; intros [xa yc] Hr; inversion_clear Hr;
  inversion_clear H; rename b into xc; apply RCmp_ with (b:=xc).
   apply H1.
   apply RDomR_; [apply H2 | apply RImg_ with (a:=xa); [apply H0 | apply H1]].
Qed.

Definition GePRef(Da Ra Dc Rc:Set)(Pa:Da↔Ra)(Pc:Dc↔Rc)(Id:Da↔Dc)(Ir:Ra↔Rc):=
 Id@∏1(Pa)⊆∏1(Pc) /\ ∏1(Pa)⊲(Pc∘Id)⊆Ir∘Pa.
Notation "Pa '⊑' Pc '[' Id ',' Ir ']'":=
 (GePRef _ _ _ _ Pa Pc Id Ir) (no associativity, at level 90).

(* Relations between refinements -----------------------------------------------------------------*)

Theorem DatRef_GePRef:forall (Da Ra Dc Rc:Set)(fa:Da->Ra)(fc:Dc->Rc)(Id:Da↔Dc)(Ir:Ra↔Rc),
                      §(fa)⇝§(fc)[Id,Ir]->(⇔fa)⊑(⇔fc)[Id,Ir].
Proof.
 intros Da Ra Dc Rc fa fc Id Ir Href; unfold GePRef;
  generalize (proj1 (DatRef_rel Da Ra Dc Rc fa fc Id Ir) Href); intros Href'; split.
  intros xc _; apply RDom_ with (b:=fc xc); apply RofF_.
  intros [xa yc] Hrel; apply Href'; inversion_clear Hrel; apply H.
Qed.

Theorem ChPRef_GePRef:forall (D R:Set)(Pa:D↔R)(Pc:D↔R), Pa≻Pc->Pa⊑Pc[Ⅰ(D),Ⅰ(R)].
Proof.
 intros D R Pa Pc; unfold ChPRef; intros [HD HR]; unfold GePRef; split.
  intros d Hd; inversion_clear Hd; inversion H0; rewrite H3 in H; apply HD; apply H.
  intros [d r] Hdr; inversion_clear Hdr; inversion_clear H; inversion H1; rewrite <- H4 in *;
   clear b H4 a H3 H1; apply RCmp_ with (b:=r).
   apply HR; apply RDomR_; [apply H2 | apply H0].
   apply RId_.
Qed.

Theorem GePRef_ChPRef:forall (Da Ra Dc Rc:Set)(Pa:Da↔Ra)(Pc:Dc↔Rc)(Id:Da↔Dc)(Ir:Ra↔Rc),
                      Pa⊑Pc[Id,Ir]->∏1(Pa)⊆∏1(Id)->Ir∘Pa≻Pc∘Id.
Proof.
 intros Da Ra Dc Rc Pa Pc Id Ir; unfold GePRef; unfold ChPRef; intros [HD HR] HI; split.
  intros xa Hxa; inversion_clear Hxa; rename b into yc; inversion_clear H; rename b into ya.
  generalize (RDom_ _ _ _ _ _ H0); intros HPa.
  generalize (HI _ HPa); intros HId; inversion_clear HId; rename b into xc.
  generalize (HD _ (RImg_ _ _ _ _ _ HPa _ H)); intros HPc.
  generalize HPc; intros HPc'; inversion_clear HPc'; rename b into yc'.
  apply RDom_ with (b:=yc'); apply RCmp_ with (b:=xc); [apply H | apply H2].
  intros [xa yc] Hrel; inversion_clear Hrel; inversion_clear H0; rename b into yc'.
  inversion_clear H1; rename b into ya.
  generalize (RDom_ _ _ _ _ _ H0); intros HPa.
  apply HR; apply RDomR_.
  apply H.
  apply HPa.
Qed.
(* Nota: The condition ∏1(Pa)⊆∏1(Id) just expresses that the scope of the specification Pa has not
   been reduced by the choice of a partial interpretation Id ; in other word, the requirement is to
   refine totally Pa, not just a part of it. *)

(* Combined refinement properties ----------------------------------------------------------------*)

Theorem GePRef_refl:forall (D R:Set)(P:D↔R), P⊑P[Ⅰ(D),Ⅰ(R)]. (* Reflexivity *)
Proof.
 intros D R P; unfold GePRef; unfold SInc; split.
  intros d Hd; inversion_clear Hd; inversion H0; rewrite <- H3; apply H.
  intros [d r] Hdr; inversion_clear Hdr; inversion_clear H; inversion H1; apply RCmp_ with (b:=r).
   apply H2.
   apply RId_.
Qed.

Theorem GePRef_trans:forall (Da Ra Di Ri Dc Rc:Set)(Pa:Da↔Ra)(Pi:Di↔Ri)(Pc:Dc↔Rc) (* Transitivity *)
                            (Id1:Da↔Di)(Id2:Di↔Dc)(Ir1:Ra↔Ri)(Ir2:Ri↔Rc),
                     Pa⊑Pi[Id1,Ir1]->Pi⊑Pc[Id2,Ir2]->Pa⊑Pc[Id2∘Id1,Ir2∘Ir1].
Proof.
 unfold GePRef; intros Da Ra Di Ri Dc Rc Pa Pi Pc Id1 Id2 Ir1 Ir2 [Hai1 Hai2] [Hir1 Hir2]; split.
  apply (SInc_trans _ ((Id2∘Id1)@∏1(Pa)) (Id2@∏1(Pi)) ∏1(Pc)).
   intros xc Hxc; inversion_clear Hxc; rename a into xa; inversion_clear H0; rename b into xi;
    apply RImg_ with (a:=xi); [apply (Hai1 _ (RImg_ _ _ _ _ _ H _ H1)) | apply H2].
   apply Hir1.
  intros [xa yc] Hac; inversion_clear Hac; generalize ((proj1 (RCmp_assoc _ _ _ _ Id1 Id2 Pc)) _ H);
   intros Hac'; inversion_clear Hac'; rename b into xi.
  generalize (Hir2 _ (RDomR_ _ _ _ ∏1(Pi) _ _ H2 (Hai1 _ (RImg_ _ _ Id1 _ _ H0 _ H1)))); intros H3;
   inversion_clear H3; rename b into yi.
  apply (proj1 (RCmp_assoc _ _ _ _ Pa Ir1 Ir2)); apply RCmp_ with (b:=yi).
   apply Hai2; apply RDomR_; [apply RCmp_ with (b:=xi); [apply H1 | apply H4] | apply H0].
   apply H5.
Qed.

Theorem GePRef_SInc_l:forall (Da Ra Dc Rc:Set)(Id Id':Da↔Dc)(Ir:Ra↔Rc)(Pa:Da↔Ra)(Pc:Dc↔Rc),
                      Pa⊑Pc[Id,Ir]->Id'⊆Id->Pa⊑Pc[Id',Ir].
Proof.
 intros Da Ra Dc Rc Id Id' Ir Pa Pc [HR1 HR2] HId; split.
  intros xc Hxc; inversion_clear Hxc; rename a into xa; apply HR1;
   apply (RImg_ _ _ _ _ _ H _ (HId _ H0)).
  intros [xa yc] H; inversion_clear H; inversion_clear H0; rename b into xc.
  apply (HR2 _ (RDomR_ _ _ _ _ _ _ (RCmp_ _ _ _ _ _ _ _ (HId _ H) _ H2) H1)).
Qed.

Theorem GePRef_SInc_r:forall (Da Ra Dc Rc:Set)(Id:Da↔Dc)(Ir Ir':Ra↔Rc)(Pa:Da↔Ra)(Pc:Dc↔Rc),
                      Pa⊑Pc[Id,Ir]->Ir⊆Ir'->Pa⊑Pc[Id,Ir'].
Proof.
 intros Da Ra Dc Rc Id Ir Ir' Pa Pc [HR1 HR2] HIr; split.
  apply HR1.
  apply (SInc_trans _ _ _ (Ir'∘Pa) HR2); apply (RCmp_SInc_l); apply HIr.
Qed.

Theorem GePRef_RCmp_al:forall (Da Da' Ra Dc Rc:Set)
                              (Id:Da↔Dc)(Ir:Ra↔Rc)(Pa:Da↔Ra)(Pc:Dc↔Rc)(G:Da'↔Da),
                              Pa⊑Pc[Id,Ir]->∏2(G)⊆∏1(Pa)->Pa∘G⊑Pc[Id∘G,Ir].
Proof.
 intros Da Da' Ra Dc Rc Id Ir Pa Pc G [HR1 HR2] HG; split.
  intros xc Hxc; inversion_clear Hxc; rename a into xa'; inversion_clear H; rename b into ya;
   inversion_clear H0; rename b into xa.
  apply HR1; apply RImg_ with (a:=xa); [apply HG; apply (RRan_ _ _ _ _ _ H) | apply H2].
  intros [xa' yc] H; inversion_clear H.
  apply (proj2 (RCmp_assoc _ _ _ _ G Pa Ir)).
  generalize (proj1 (RCmp_assoc _ _ _ _ G Id Pc) _ H0); intros H0'; inversion_clear H0';
   rename b into xa.
  apply RCmp_ with (b:=xa);
   [apply H | apply HR2; apply RDomR_; [apply H2 | apply HG; apply (RRan_ _ _ _ _ _ H)]].
Qed.

Theorem GePRef_RCmp_cr:forall (Da Ra Dc Rc Rc':Set)
                              (Id:Da↔Dc)(Ir:Ra↔Rc)(Pa:Da↔Ra)(Pc:Dc↔Rc)(G:Rc↔Rc'),
                              Pa⊑Pc[Id,Ir]->∏2(Pc)⊆∏1(G)->Pa⊑G∘Pc[Id,G∘Ir].
Proof.
 intros Da Ra Dc Rc Rc' Id Ir Pa Pc G [HR1 HR2] HG; split.
  intros xc Hxc; inversion_clear Hxc; rename a into xa.
  generalize (HR1 _ (RImg_ _ _ _ _ _ H _ H0)); intros H1; inversion_clear H1; rename b into yc.
  generalize (HG _ (RRan_ _ _ _ _ _ H2)); intros Hyc; inversion_clear Hyc; rename b into yc'.
  apply RDom_ with (b:=yc'); apply RCmp_ with (b:=yc); [apply H2 | apply H1].
  intros [xa yc'] H; inversion_clear H.
  generalize (proj2 (RCmp_assoc _ _ _ _ Id Pc G) _ H0); intros H0'; inversion_clear H0';
   rename b into yc.
  generalize (HR2 _ (RDomR_ _ _ _ _ _ _ H H1)); intros H3.
  apply (proj1 (RCmp_assoc _ _ _ _ Pa Ir G)); apply RCmp_ with (b:=yc); [apply H3 | apply H2].
Qed.

Theorem GePRef_RFnc:forall (Da Ra Dc Rc:Set)(Id:Da↔Dc)(Ir:Ra↔Rc)(Pa:Da↔Ra)(Pc:Dc↔Rc),
                    Pa⊑Pc[Id,Ir]->RFnc Pa->forall (S:⇑Ra), Pa⊳S⊑Pc[Id,Ir].
Proof.
 unfold GePRef; intros Da Ra Dc Rc Id Ir Pa Pc [HR1 HR2] HF S; split.
  apply SInc_trans with (S2:=Id@∏1(Pa)).
   intros xc H; inversion_clear H; rename a into xa; inversion_clear H0; rename b into ya;
    inversion_clear H; apply RImg_ with (a:=xa); [apply (RDom_ _ _ _ _ _ H0) | apply H1].
   apply HR1.
  intros [xa yc] H; inversion_clear H; inversion_clear H1; rename b into ya; inversion_clear H;
   apply RCmp_ with (b:=ya).
   apply RRanR_; [apply H1 | apply H2].
   generalize (HR2 _ (RDomR_ _ _ _ _ _ _ H0 (RDom_ _ _ _ _ _ H1))); intros H; inversion_clear H;
    rename b into ya'; rewrite (HF _ _ _ H1 H3) in *; apply H4.
Qed.

Theorem GePRef_SEqu_l:forall (Da Ra Dc Rc:Set)(Id:Da↔Dc)(Ir:Ra↔Rc)(Pa Pa':Da↔Ra)(Pc:Dc↔Rc),
                      Pa⊑Pc[Id,Ir]->Pa≡Pa'->Pa'⊑Pc[Id,Ir].
Proof.
 intros Da Ra Dc Rc Id Ir Pa Pa' Pc [HR1 HR2] HEq; split.
  intros xc H; inversion_clear H; rename a into xa; inversion_clear H0; rename b into ya; apply HR1;
   apply RImg_ with (a:=xa).
   apply RDom_ with (b:=ya); apply (proj2 HEq); apply H.
   apply H1.
  intros [xa yc] H; inversion_clear H; inversion_clear H1; rename b into ya;
   apply (RCmp_SInc_r _ _ _ _ (proj1 HEq) _ Ir); apply HR2; apply RDomR_.
   apply H0.
   apply RDom_ with (b:=ya); apply (proj2 HEq); apply H.
Qed.

Theorem GePRef_SEqu_r:forall (Da Ra Dc Rc:Set)(Id:Da↔Dc)(Ir:Ra↔Rc)(Pa:Da↔Ra)(Pc Pc':Dc↔Rc),
                      Pa⊑Pc[Id,Ir]->Pc≡Pc'->Pa⊑Pc'[Id,Ir].
Proof.
 intros Da Ra Dc Rc Id Ir Pa Pc Pc' [HR1 HR2] HEq; split.
  apply SInc_trans with (S2:=∏1(Pc)).
   apply HR1.
   intros xc H; inversion_clear H; rename b into yc; apply RDom_ with (b:=yc); apply (proj1 HEq);
    apply H0.
  intros [xa yc] H; inversion_clear H;
   apply (HR2 _ (RDomR_ _ _ _ _ _ _ ((RCmp_SInc_l _ _ _ _ (proj2 HEq) _ Id) _ H0) H1)).
Qed.

(* The Distracted Composer problem ---------------------------------------------------------------*)
(* We expect the composition of two refinements to be a refinement of the composition, and identify
   sufficient conditions, as for support refinement and sharpening refinement. However, we add a
   small difficulty by not enforcing the use of a common intermediate concrete support. To compose
   the refinements, we therefore need in addition a glue for which we identify sufficient conditions
   as well. *)

Theorem GePRef_mon:forall (Da Ia Ra Dc Ic Rc:Set)
                          (Id:Da↔Dc)(Ii:Ia↔Ic)(Ii':Ia↔Ic)(Ir:Ra↔Rc)
                          (Pa1:Da↔Ia)(Pa2:Ia↔Ra)(Pc1:Dc↔Ic)(Pc2:Ic↔Rc),
                          Pa1⊳∏1(Pa2)⊑Pc1[Id,Ii]->Pa2⊑Pc2[Ii',Ir]->Ii⊆Ii'->Pa2∘Pa1⊑Pc2∘Pc1[Id,Ir].
Proof.
 intros Da Ia Ra Dc Ic Rc Id Ii Ii' Ir Pa1 Pa2 Pc1 Pc2 [Hdi1 Hdi2] [Hir1 Hir2] HI; split.
  (* First refinement condition *)
  intros xc Hxc; inversion_clear Hxc; rename a into xa; inversion_clear H; rename b into za;
   inversion_clear H1; rename b into ya.
  generalize (Hdi1 _ (RImg_ _ _ Id _ _
                            (RDom_ _ _ _ _ _ (RRanR_ _ _ _ _ _ _ H (RDom_ _ _ _ _ _ H2))) _ H0));
   intros H1; inversion_clear H1; rename b into yc.
  generalize (RCmp_ _ _ _ _ _ _ _ H0 _ H3); intros H4.
  generalize (RRanR_ _ _ Pa1 _ _ _ H (RDom_ _ _ _ _ _ H2)); intros H5.
  generalize (Hdi2 _ (RDomR_ _ _ _ _ _ _ H4 (RDom_ _ _ _ _ _ H5))); intros H6.
  inversion_clear H6; rename b into ya'.
  generalize (HI _ H7); intros H8.
  inversion_clear H1.
  generalize (Hir1 _ (RImg_ _ _ _ _ _ H9 _ H8)); intros H10.
  inversion_clear H10; rename b into zc.
  apply RDom_ with (b:=zc); apply RCmp_ with (b:=yc); [apply H3 | apply H1].
  (* Second refinement condition *)
  intros [xa zc] Hac; inversion_clear Hac; inversion_clear H0; rename b into za.
  generalize (proj2 (RCmp_assoc _ _ _ _ Id Pc1 Pc2) _ H); intros H0; inversion_clear H0;
   rename b into yc.
  generalize H1; intros H0; inversion_clear H0; rename b into ya.
  generalize (RDom_ _ _ _ _ _ (RRanR_ _ _ _ _ _ _ H4 (RDom_ _ _ _ _ _ H5))); intros H0.
  generalize (Hdi2 _ (RDomR_ _ _ _ _ _ _ H2 H0)); intros H6.
  inversion_clear H6; rename b into ya'.
  generalize (RCmp_ _ _ _ _ _ _ _ (HI _ H8) _ H3); intros H6.
  inversion_clear H7.
  generalize (Hir2 _ (RDomR_ _ _ _ _ _ _ H6 H10)); intros H7.
  apply (proj2 (RCmp_assoc _ _ _ _ Pa1 Pa2 Ir)).
  apply RCmp_ with (b:=ya'); [apply H9 | apply H7].
Qed.

Theorem GePRef_id:forall (Da Ra Dc Rc Rc':Set)
                         (Id:Da↔Dc)(Ir:Ra↔Rc)(Ir':Ra↔Rc')(Pa:Da↔Ra)(Pc:Dc↔Rc)(G:Rc↔Rc'),
                         Pa⊑Pc[Id,Ir]->∏2(Pc)⊆∏1(G)->G∘Ir⊆Ir'->Pa⊑G∘Pc[Id,Ir'].
Proof.
 intros Da Ra Dc Rc Rc' Id Ir Ir' Pa Pc G HR HG1 HG2.
 apply (GePRef_SInc_r _ _ _ _ Id (G∘Ir) Ir' Pa (G∘Pc)).
  apply (GePRef_RCmp_cr _ _ _ _ _ _ _ _ _ _ HR HG1).
  apply HG2.
Qed.

Theorem GePRef_Distract:forall (Da Ia Ra Dc Ic Jc Rc:Set)
                               (Id:Da↔Dc)(Ii:Ia↔Ic)(Ij:Ia↔Jc)(Ir:Ra↔Rc)
                               (Pa1:Da↔Ia)(Pa2:Ia↔Ra)(Pc1:Dc↔Ic)(Pc2:Jc↔Rc)(G:Ic↔Jc),
                               Pa1⊳∏1(Pa2)⊑Pc1[Id,Ii]-> (* First constrained refinement *)
                               Pa2⊑Pc2[Ij,Ir]->         (* Second refinement *)
                               ∏2(Pc1)⊆∏1(G)->G∘Ii⊆Ij-> (* Glue conditions *)
                               Pa2∘Pa1⊑Pc2∘G∘Pc1[Id,Ir].
Proof.
 intros Da Ia Ra Dc Ic Jc Rc Id Ii Ij Ir Pa1 Pa2 Pc1 Pc2 G [HR11 HR12] HR2 HG1 HG2.
 generalize (GePRef_RCmp_cr _ _ _ _ _ _ _ _ _ _ (conj HR11 HR12) HG1); intros HR1'.
 generalize (GePRef_mon _ _ _ _ _ _ _ _ _ _ _ _ _ _ HR1' HR2 HG2); intros [HR21 HR22].
 split.
  apply (SInc_trans _ _ _ ∏1((Pc2∘G)∘Pc1) HR21); intros xc Hxc; inversion_clear Hxc;
   rename b into yc; apply RDom_ with (b:=yc); apply (proj1 (RCmp_assoc _ _ _ _ Pc1 G Pc2) _ H).
  apply (SInc_trans _ (∏1(Pa2∘Pa1)⊲((Pc2∘G)∘Pc1)∘Id) (∏1(Pa2∘Pa1)⊲(Pc2∘(G∘Pc1))∘Id) (Ir∘(Pa2∘Pa1))).
   intros [xa yc] H; inversion_clear H; inversion_clear H0; rename b into xc; apply RDomR_.
    apply RCmp_ with (b:=xc); [apply H | apply (proj2 (RCmp_assoc _ _ _ _ Pc1 G Pc2) _ H2)].
    apply H1.
   apply HR22.
Qed.

(* Decomposing Combined Refinement ---------------------------------------------------------------*)
(* Given a combined refinement, is it possible to decompose this refinement into the sequence of
   a (extended) support refinement and a sharpening refinement? *)

Theorem GePRef_dec:forall (Da Ra Dc Rc:Set)(Id:Da↔Dc)(Ir:Ra↔Rc)(Pa Pa':Da↔Ra)(Gd:Dc->Da)(Gr:Ra->Rc),
                   §(fun x:Da=>x)⇝§(Gd)[Id,Ⅰ(Da)]->
                   Pa≻Pa'->
                   §(fun x:Ra=>x)⇝§(Gr)[Ⅰ(Ra),Ir]->
                   Pa⊑((⇔Gr)∘Pa'∘(⇔Gd))[Id,Ir].
Proof.
 intros Da Ra Dc Rc Id Ir Pa Pa' Gd Gr HR1 HR2 HR3.
 generalize (DatRef_GePRef _ _ _ _ _ _ _ _ HR1); clear HR1; intros HR1.
 generalize (ChPRef_GePRef _ _ _ _ HR2); clear HR2; intros HR2.
 generalize (DatRef_GePRef _ _ _ _ _ _ _ _ HR3); clear HR3; intros HR3.
 generalize (GePRef_RFnc _ _ _ _ _ _ _ _ HR1 (RofF_RFnc _ _ (fun x : Da => x)) ∏1(Pa)); clear HR1;
  intros HR1.
 generalize (GePRef_mon _ _ _ _ _ _ _ _ _ _ _ _ _ _ HR1 HR2 (SInc_refl _ (Ⅰ(Da)))); intros HR12.
 assert (HEq:Pa∘(⇔(fun x:Da=>x))≡Pa).
  split; intros [xa ya] Ha.
   inversion_clear Ha; inversion_clear H; apply H0.
   apply RCmp_ with (b:=xa); [apply (RofF_ _ _ (fun x:Da=>x)) | apply Ha].
 generalize (GePRef_SEqu_l _ _ _ _ _ _ _ _ _ HR12 HEq); clear HEq HR12; intros HR12.
 assert (HEq:Pa≡Pa⊳∏1(⇔(fun x:Ra=>x))).
  split; intros [xa ya] Ha.
   apply RRanR_; [apply Ha | apply RDom_ with (b:=ya); apply (RofF_ _ _ (fun x:Ra=>x))].
   inversion_clear Ha; apply H.
 generalize (GePRef_SEqu_l _ _ _ _ _ _ _ _ _ HR12 HEq); clear HR12 HEq; intros HR12.
 generalize (GePRef_mon _ _ _ _ _ _ _ _ _ _ _ _ _ _ HR12 HR3 (SInc_refl _ (Ⅰ(Ra)))); intros HR123.
 assert (HEq:(⇔(fun x:Ra=>x))∘Pa≡Pa).
  split; intros [xa ya] Ha.
   inversion_clear Ha; inversion H0; rewrite H3 in *; clear a b H2 H3; apply H.
   apply RCmp_ with (b:=ya); [apply Ha | apply (RofF_ _ _ (fun x:Ra=>x))].
 generalize (GePRef_SEqu_l _ _ _ _ _ _ _ _ _ HR123 HEq); clear HR123 HEq; intros HR123.
 apply (GePRef_SEqu_r _ _ _ _ _ _ _ _ _ HR123 (RCmp_assoc _ _ _ _ (⇔Gd) Pa' (⇔Gr))).
Qed.

(* Illustration 1 : Boolean choice ---------------------------------------------------------------*)
(* The specification is the total relation between unit and bool. The functional interpretation is
   that the implementation is a constant, either true or false; but in our case, we implement unit
   as nat, introducing hidden variables, and the implementation is any function from nat to bool. *)

Section bool_choice.
 Definition Pa_bc:unit↔bool:=⊛(unit*bool).
 Definition Pc_bc(n:nat):bool:=match n with 0 => false | _ => true end.
 Definition Id_bc:unit↔nat:=⊛(unit*nat).
 Definition Ir_bc:bool↔bool:=Ⅰ(bool).
 Theorem bool_choice_ref:Pa_bc⊑(⇔Pc_bc)[Id_bc,Ir_bc].
 Proof.
  split.
   intros [ | n] _; [apply RDom_ with (b:=false); apply (RofF_ _ _ Pc_bc 0) |
                     apply RDom_ with (b:=true); apply (RofF_ _ _ Pc_bc (S n))].
   intros [[] b] _; apply RCmp_ with (b:=b); [unfold Pa_bc; apply SFul_ | unfold Ir_bc; apply RId_].
 Qed.
End bool_choice.

(* Illustration 2 : Maximum of a list ------------------------------------------------------------*)
(* This example, derived from the Maximier from the B-Book, illustrates optimisation through the use
   of a non injective interpretation over the domain. To represent a B machine, we represent the
   state as part of both inputs and outputs; multiple operations are represented through a single
   composite function with an additional parameter acting as a selector. *)

Section maximier.
 Require Import List.
 Require Import Max.
 Definition lmax(l:list nat):=fold_left max l 0.
 Theorem lmax1:forall (l:list nat)(n:nat), lmax (n::l)=(fold_left max l n).
 Proof.
  intros l n; unfold lmax; apply refl_equal.
 Qed.
 Theorem lmax2:forall (l:list nat)(n:nat), lmax (n::l)=max (lmax l) n.
 Proof.
  induction l as [| h l]; intros n; unfold lmax; simpl.
   apply refl_equal.
   repeat (rewrite <- lmax1); repeat (rewrite IHl); induction (Compare_dec.le_lt_dec n h).
    rewrite (max_r _ _ a); induction (Compare_dec.le_lt_dec (lmax l) h).
     rewrite (max_r _ _ a0); rewrite max_comm; rewrite (max_r _ _ a); apply refl_equal.
     rewrite (max_l _ _ (Lt.lt_le_weak _ _ b));
      rewrite (max_l _ _ (Le.le_trans _ _ _ a (Lt.lt_le_weak _ _ b))); apply refl_equal.
    rewrite (max_l _ _ (Lt.lt_le_weak _ _ b)); induction (Compare_dec.le_lt_dec (lmax l) n).
     rewrite (max_r _ _ a); induction (Compare_dec.le_lt_dec (lmax l) h).
      rewrite (max_r _ _ a0); rewrite (max_r _ _ (Lt.lt_le_weak _ _ b)); apply refl_equal.
      rewrite (max_l _ _ (Lt.lt_le_weak _ _ b0)); rewrite (max_r _ _ a); apply refl_equal.
    rewrite (max_l _ _ (Lt.lt_le_weak _ _ b0)).
    rewrite (max_l _ _ (Le.le_trans _ _ _ (Lt.lt_le_weak _ _ b) (Lt.lt_le_weak _ _ b0)));
     rewrite (max_l _ _ (Lt.lt_le_weak _ _ b0)); apply refl_equal.
 Qed.
 Inductive Pa_mx:(nat*(list nat)*bool)↔((list nat)*nat):=
  | Pa_mx_stor:forall (n:nat)(l:list nat), Pa_mx((n,l,true),(n::l,0))
  | Pa_mx_read:forall (n:nat)(l:list nat), Pa_mx((n,l,false),(l,fold_left max l 0)).
 Definition Pc_mx(nmb:nat*nat*bool):nat*nat:=
  match nmb with
  | (n,m,true) => match (Compare_dec.le_lt_dec n m) with left _ => (0,m) | right _ => (0,n) end
  | (n,m,false) => (m,m)
  end.
 Inductive Id_mx:(nat*(list nat)*bool)↔(nat*nat*bool):=
  Id_mx_:forall (n:nat)(l:list nat)(b:bool), Id_mx((n,l,b),(n,fold_left max l 0,b)).
 Inductive Ir_mx:((list nat)*nat)↔(nat*nat):=
  Ir_mx_:forall (n:nat)(l:list nat), Ir_mx((l,n),(n,fold_left max l 0)).
 Theorem maximier_ref:Pa_mx⊑(⇔Pc_mx)[Id_mx,Ir_mx].
 Proof.
  split.
   intros [[n m] [|]] _.
    apply RDom_ with
     (b:=match (Compare_dec.le_lt_dec n m) with left _ => (0,m) | right _ => (0,n) end);
     apply (RofF_ _ _ Pc_mx (n,m,true)).
    apply RDom_ with (b:=(m,m)); apply (RofF_ _ _ Pc_mx (n,m,false)).
   intros [[[i l] [|]] [m' o]] H; inversion_clear H; inversion_clear H0; induction b as [[i' m] b];
    inversion H.
    rewrite <- H8 in *; rewrite H6 in *; clear b H8 H5 b0 n H3 l0 H4 i H6.
    inversion_clear H2; rewrite <- H7 in *; clear m H7.
    apply RCmp_ with (b:=(i'::l,0)).
     apply Pa_mx_stor.
     unfold Pc_mx; simpl; induction (Compare_dec.le_lt_dec i' (fold_left max l 0)).
      replace (fold_left max l 0) with (fold_left max (i'::l) 0).
       apply Ir_mx_.
       fold (lmax l) in a; fold (lmax (i'::l)); rewrite lmax2; rewrite (max_l _ _ a);
        apply refl_equal.
      replace (0,i') with (0,fold_left max (i'::l) 0).
       apply Ir_mx_.
       fold (lmax (i'::l)); fold (lmax l) in b; rewrite lmax2;
        rewrite (max_r _ _ (Lt.lt_le_weak _ _ b)); apply refl_equal.
    rewrite <- H8 in *; rewrite <- H7 in *; rewrite <- H6 in *;
     clear b H8 m H7 i' H6 b0 H5 l0 H4 n H3.
    apply RCmp_ with (b:=(l,fold_left max l 0)).
     apply Pa_mx_read.
     inversion_clear H2; unfold Pc_mx; simpl; apply Ir_mx_.
 Qed.
End maximier.

(* Illustration 3 : Natural addition -------------------------------------------------------------*)
(* The specification is the addition over (unbounded) naturals. The implementation is partial, using
   a 4-bits concrete representation and a partial interpretation. *)

Section nat_to_int.
 Inductive Pa_ni:(nat*nat)↔nat:= Pa_ni_:forall (a b:nat), Pa_ni((a,b),a+b).
 Inductive bit:Set:= v0:bit | v1:bit.
 Definition bit2nat(b:bit):nat:=match b with v0 => 0 | v1 => 1 end.
 Coercion bit2nat:bit>->nat.
 Definition comp1(b:bit):bit:=match b with v0 => v1 | v1 => v0 end.
 Definition add11(b1 b2 b3:bit):=
  match (b1,b2,b3) with (v0,v0,_) => (v0,b3) | (v1,v1,_) => (v1,b3) | _ => (b3,comp1 b3) end.
 Inductive w4:Set:=W:bit->bit->bit->bit->w4.
 Definition nat_of_w4(w:w4):nat:=match w with W b3 b2 b1 b0 => (((b3*2+b2)*2+b1)*2+b0) end.
 Coercion nat_of_w4:w4>->nat.
 Definition add(w w':w4):=
  match (w,w') with (W b13 b12 b11 b10,W b23 b22 b21 b20) =>
   match add11 b10 b20 v0 with (c0,b30) =>
    match add11 b11 b21 c0 with (c1,b31) =>
     match add11 b12 b22 c1 with (c2,b32) =>
      match add11 b13 b23 c2 with (c3,b33) =>
       (c3,W b33 b32 b31 b30) end end end end end.
 Inductive Pc_ni:(w4*w4)↔w4:=Pc_ni_:forall (ww:w4*w4), Pc_ni(ww,snd (add (fst ww) (snd ww))).
 Inductive Id_ni:(nat*nat)↔(w4*w4):=
  Id_ni_:forall (w w':w4), w+w'<=15->Id_ni(pair (nat_of_w4 w) (nat_of_w4 w'),pair w w').
 Inductive Ir_ni:nat↔w4:=Ir_ni_:forall (w:w4), Ir_ni(nat_of_w4 w,w).
(* SLOOOOOOOOW
 Theorem nat_to_int_ref:Pa_ni⊑Pc_ni[Id_ni,Ir_ni].
 Proof.
  split.
   intros ww _; apply RDom_ with (b:=snd (add (fst ww) (snd ww))); apply Pc_ni_.
   intros [[n1 n2] w3] H; inversion_clear H; clear H1; inversion_clear H0; destruct b as [w1 w2];
    inversion_clear H; inversion_clear H1; apply RCmp_ with (b:=w1+w2).
    apply Pa_ni_.
    simpl; assert (HA:w1+w2=snd (add w1 w2)).
     induction w1 as [[|] [|] [|] [|]]; induction w2 as [[|] [|] [|] [|]]; simpl;
      (apply refl_equal || simpl in H0; repeat (inversion_clear H0; inversion_clear H)).
    rewrite HA; apply Ir_ni_.
 Qed.
*)
End nat_to_int.

(* Miracles --------------------------------------------------------------------------------------*)

Section miracle.
 Inductive empty:Set:=.
 Theorem empty_false:forall (e:empty), False.
 Proof.
  intros e; inversion e.
 Qed.
 Definition Pa_mir:unit↔empty:=⊛(unit*empty).
 Definition Pc_mir(u:unit):unit:=u.
 Definition Id_mir:unit↔unit:=⊛(unit*unit).
 Variable Id_mir':unit↔unit.
 Definition Ir_mir:empty↔unit:=⊛(empty*unit).
 Variable Ir_mir':empty↔unit.
 Theorem miracle_ref:Pa_mir⊑(⇔Pc_mir)[Id_mir',Ir_mir'].
 Proof.
  split.
   intros [] Htt; apply RDom_ with (b:=tt); change tt with (Pc_mir tt); unfold Pc_mir at 2;
    apply RofF_.
   intros [[] []] Htt; inversion_clear Htt; inversion_clear H0; inversion b.
 Qed.
End miracle.
(* We can refine miracles by non-miracles using any Id and any Ir - including non-empty Id or
   non saturated Ir *)

(*================================================================================================*)