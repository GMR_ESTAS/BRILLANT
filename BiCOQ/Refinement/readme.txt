(*==================================================================================================
  Project : Refinement
  Developed for Coq 8.2, using UTF-8 and the Everson Mono Unicode font
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: Feb 2007, Md: Jul 2009 (approximately)
  Provided under licence CeCILL-B
  ================================================================================================*)

This is the source code of the study of the concept of refinement.

The file make.txt describes the dependencies between the files.
