(*==================================================================================================
  Project : Study of the concept of refinement
  Module : choice_ref_try2
  Still refining our analysis of refinement, we simplify the definition of choice_ref to allow
  stronger preconditions at the cost of weaker guards... But the result is not transitive
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: July 2009, Md: July 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export relation_gen.

(* Refinement with prioritary preconditions and guards -------------------------------------------*)

Definition CT2Ref(dom ran:Set)(Sa Sc:dom↔ran)(Pa Pc:⇑dom):=Pa⊲Sc⊆Pc⊲Sa.
Notation "'〈' Pa '|' Sa '≻' Pc '|' Sc '〉'":=(CT2Ref _ _ Sa Sc Pa Pc) (no associativity, at level 90).

(* Properties of CT2Ref ---------------------------------------------------------------------------*)

Theorem CT2Ref_refl_rel:forall (dom ran:Set)(S:dom↔ran)(Pa Pc:⇑dom), Pa⊆Pc->〈Pa|S≻Pc|S〉.
Proof.
 intros dom ran S Pa Pc HInc; unfold CT2Ref; intros [d r] Hdr; inversion_clear Hdr; apply RDomR_.
  apply H.
  apply (HInc _ H0).
Qed.

Theorem CT2Ref_refl_pre:forall (dom ran:Set)(Sa Sc:dom↔ran)(P:⇑dom), Sc⊆Sa->〈P|Sa≻P|Sc〉.
Proof.
 intros dom ran Sa Sc P HInc [d r] Hdr; inversion_clear Hdr; apply RDomR_.
  apply (HInc _ H).
  apply H0.
Qed.

Theorem CT2Ref_tran:forall (dom ran:Set)(Sa Si Sc:dom↔ran)(Pa Pi Pc:⇑dom),
                   Pa⊆Pi->〈Pa|Sa≻Pi|Si〉->〈Pi|Si≻Pc|Sc〉->〈Pa|Sa≻Pc|Sc〉.
Proof.
 intros dom ran Sa Si Sc Pa Pi Pc Hinc Hai Hic [d r] Hdr; inversion_clear Hdr.
 generalize (Hinc _ H0); intros H1.
 generalize (Hic _ (RDomR_ _ _ _ _ _ _ H H1)); intros H2; inversion_clear H2.
 generalize (Hai _ (RDomR_ _ _ _ _ _ _ H3 H0)); intros H2; inversion_clear H2; clear H6.
 apply RDomR_; [apply H5 | apply H4].
Qed.

Theorem CT2Ref_mon:forall (dom int ran:Set)
                         (Sa Sc:dom↔int)(Sa' Sc':int↔ran)(Pa Pc:⇑dom)(Pa' Pc':⇑int),
                  〈Pa|(Sa⊳Pa')≻Pc|Sc〉->〈Pa'|Sa'≻Pc'|Sc'〉->〈Pa|Sa'∘Sa≻Pc|Sc'∘Sc〉.
Proof.
 intros dom int ran Sa Sc Sa' Sc' Pa Pc Pa' Pc' HRef HRef'; intros [d r] Hdr; inversion_clear Hdr;
  inversion_clear H; rename b into i.
 generalize (HRef _ (RDomR_ _ _ _ _ _ _ H1 H0)); intros Hdi; inversion_clear Hdi; inversion_clear H.
 generalize (HRef' _ (RDomR_ _ _ _ _ _ _ H2 H5)); intros Hir; inversion_clear Hir.
 apply RDomR_.
  apply RCmp_ with (b:=i); [apply H4 | apply H].
  apply H3.
Qed.

(*================================================================================================*)