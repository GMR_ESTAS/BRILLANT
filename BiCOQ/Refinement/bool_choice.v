(*==================================================================================================
  Project : Study of the concept of refinement
  Module : bool_choice
  Building upon the learnings of our analysis of refinement, we propose here an illustration of the
  refinement paradox in Coq
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: July 2009, Md: July 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Module Type Boolean.
 Parameter U:Set.
 Parameter eq:U->U->Prop.
 Axiom eq_refl:forall (u:U), eq u u.
 Axiom eq_sym:forall (u1 u2:U), eq u1 u2->eq u2 u1.
 Axiom eq_trans:forall (u1 u2 u3:U), eq u1 u2 ->eq u2 u3->eq u1 u3.
 Axiom U_is_Unit:forall (u1 u2:U), eq u1 u2.
 Parameter fnc:U->bool.
End Boolean.

Module Covert_Channel:Boolean.
 Definition U:=nat.
 Definition eq(u1 u2:U):Prop:=True.
 Theorem eq_refl:forall (u:U), eq u u.
 Proof.
  intros u; apply I.
 Qed.
 Theorem eq_sym:forall (u1 u2:U), eq u1 u2->eq u2 u1.
 Proof.
  intros u1 u2 _; apply I.
 Qed.
 Theorem eq_trans:forall (u1 u2 u3:U), eq u1 u2->eq u2 u3->eq u1 u3.
 Proof.
  intros u1 u2 u3 _ _; apply I.
 Qed.
 Theorem U_is_Unit:forall (u1 u2:U), eq u1 u2.
 Proof.
  intros u1 u2; apply I.
 Qed.
 Definition fnc(u:U):=match u with 0 => false | _ => true end.
End Covert_Channel.

Module Type Boolean_Function.
 Parameter B:Set.
 Parameter t:B.
 Parameter f:B.
 Parameter eq:B->B->Prop.
 Axiom eq_refl:forall (b:B), eq b b.
 Axiom eq_sym:forall (b1 b2:B), eq b1 b2->eq b2 b1.
 Axiom eq_trans:forall (b1 b2 b3:B), eq b1 b2 ->eq b2 b3->eq b1 b3.
 Axiom t_noteq_f:~eq t f.
 Parameter B_is_bool:forall (b:B), {eq b t}+{eq b f}.
 Parameter fnc:B->bool.
End Boolean_Function.

Module Covert_Channel_2:Boolean_Function.
 Definition B:=(prod bool nat).
 Definition t:=(pair true 0).
 Definition f:=(pair false 0).
 Definition eq(b1 b2:B):=
  match (pair b1 b2) with
  | pair (pair true _) (pair true _) => True
  | pair (pair false _) (pair false _) => True
  | _ => False
  end.
 Theorem eq_refl:forall (b:B), eq b b.
 Proof.
  intros [[|] n]; apply I.
 Qed.
 Theorem eq_sym:forall (b1 b2:B), eq b1 b2->eq b2 b1.
 Proof.
  intros [[|] n1] [[|] n2] H; apply H.
 Qed.
 Theorem eq_trans:forall (b1 b2 b3:B), eq b1 b2->eq b2 b3->eq b1 b3.
 Proof.
  intros [[|] n1] [[|] n2] [[|] n3] H12 H23; (apply H12 || apply H23 || inversion H12).
 Qed.
 Theorem t_noteq_f:~eq t f.
 Proof.
  unfold f; unfold t; unfold eq; intros H; apply H.
 Qed.
 Theorem B_is_bool:forall (b:B), {eq b t}+{eq b f}.
 Proof.
  unfold f; unfold t; unfold eq; intros [[|] _]; [left; apply I | right; apply I].
 Qed.
 Definition fnc(b:B):bool:=
  match b with
  | pair _ 0 => false
  | pair _ 1 => true
  | pair b' 2 => b'
  | pair b' _ => if b' then false else true
  end.
End Covert_Channel_2.
