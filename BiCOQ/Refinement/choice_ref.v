(*==================================================================================================
  Project : Study of the concept of refinement
  Module :choice_ref
  Still refining our analysis of refinement, we consider here a much much more simple definition of
  choice refinement (inclusion) which encompasses guards, before introducing preconditions. This is
  our representation of choice-refinement.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: July 2009, Md: July 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export relation_gen.

(* Monotony of inclusion -------------------------------------------------------------------------*)

Theorem IncRef_mon:forall (D I R:Set)(A1 C1:D↔I)(A2 C2:I↔R),
                   C1⊆A1->C2⊆A2->C2∘C1⊆A2∘A1.
Proof.
 intros D I R A1 C1 A2 C2 HRef1 HRef2 [d r] Hdr; inversion_clear Hdr; rename b into i;
  generalize (HRef1 _ H); intros Hdi; generalize (HRef2 _ H0); intros Hir.
 apply RCmp_ with (b:=i); [apply Hdi | apply Hir].
Qed.

(* A Quick Check about preconditions -------------------------------------------------------------*)

Theorem IncPre:forall (D R:Set)(Pa Pc:⇑D)(Sa Sc:D↔R), Pa⊆Pc->Pa⊲Sc⊆Sa->Pa⊲Sc⊆Pc⊲Sa.
Proof.
 intros D R Pa Pc Sa Sc HPre HRef [d r] Hdr.
 generalize (HRef _ Hdr); intros H'dr.
 inversion_clear Hdr.
 apply RDomR_; [apply H'dr | apply HPre; apply H0].
Qed.

(* Refinement with prioritary preconditions and guards -------------------------------------------*)
(* Here we consider that ⊆ encodes choice-refinement, the guard being represented by domain of the
   specification. The represent a precondition as well, we need an additional subset parameter. We
   then just require precondition to be weakened during refinement, and relations to be included in
   the scope of the precondition. *)

Definition ChcRef(dom ran:Set)(Sa Sc:dom↔ran)(Pa Pc:⇑dom):=Pa⊆Pc /\ Pa⊲Sc⊆Sa.
Notation "'〈' Pa '|' Sa '≻' Pc '|' Sc '〉'":=(ChcRef _ _ Sa Sc Pa Pc) (no associativity, at level 90).

(* Properties of ChcRef ---------------------------------------------------------------------------*)

Theorem ChcRef_rich:forall (dom ran:Set)(Sa Sc:dom↔ran)(Pa Pc:⇑dom), 〈Pa|Sa≻Pc|Sc〉->Pa⊲Sc⊆Pc⊲Sa.
Proof.
 intros dom ran Sa Dc Pa Pc [HPre HRel]; apply IncPre; [apply HPre | apply HRel].
Qed.

Theorem ChcRef_refl:forall (dom ran:Set)(S:dom↔ran)(Pa Pc:⇑dom), Pa⊆Pc->〈Pa|S≻Pc|S〉.
Proof.
 intros dom ran S Pa Pc HInc; unfold ChcRef; split.
  apply HInc.
  intros [d r] Hdr; inversion_clear Hdr; apply H.
Qed.

Theorem ChcRef_asym_pre:forall (dom ran:Set)(Sa Sc:dom↔ran)(Pa Pc:⇑dom),
                       〈Pa|Sa≻Pc|Sc〉->〈Pc|Sc≻Pa|Sc〉->Pa≡Pc.
Proof.
 intros dom ran Sa Sc Pa Pc [Hac _] [Hca _]; unfold SEqu; split; [apply Hac | apply Hca].
Qed.

Theorem ChcRef_asym_rel:forall (dom ran:Set)(Sa Sc:dom↔ran)(P:⇑dom),
                       〈P|Sa≻P|Sc〉->〈P|Sc≻P|Sa〉->P⊲Sa≡P⊲Sc.
Proof.
 intros dom ran Sa Sc P [HPac HRac] [HPca HRca].
 unfold SEqu; split; intros [d r] Hdr; inversion_clear Hdr; apply RDomR_.
  apply HRca; apply RDomR_; [apply H | apply H0].
  apply H0.
  apply HRac; apply RDomR_; [apply H | apply H0].
  apply H0.
Qed.

Theorem ChcRef_tran:forall (dom ran:Set)(Sa Si Sc:dom↔ran)(Pa Pi Pc:⇑dom),
                   〈Pa|Sa≻Pi|Si〉->〈Pi|Si≻Pc|Sc〉->〈Pa|Sa≻Pc|Sc〉.
Proof.
 intros dom ran Sa Si Sc Pa Pi Pc [HPai HRai] [HPic HRic]; split.
  intros d Hd; apply HPic; apply HPai; apply Hd.
  intros [d r] Hdr; inversion_clear Hdr; apply HRai; apply RDomR_.
   apply HRic; apply RDomR_; [apply H | apply HPai; apply H0].
   apply H0.
Qed.

Theorem ChcRef_dom:forall (dom int ran:Set)
                         (Sa Sc:dom↔int)(Sa' Sc':int↔ran)(Pa Pc:⇑dom)(Pa' Pc':⇑int),
                  〈Pa|(Sa⊳Pa')≻Pc|Sc〉->〈Pa'|Sa'≻Pc'|Sc'〉->〈Pa|Sa'∘Sa≻Pc|Sc'∘Sc〉.
Proof.
 intros dom int ran Sa Sc Sa' Sc' Pa Pc Pa' Pc' [HP HR] [HP' HR']; split.
  apply HP.
  intros [d r] Hdr; inversion_clear Hdr; inversion_clear H; rename b into i.
  generalize (HR _ (RDomR_ _ _ _ _ _ _ H1 H0)); intros H; inversion_clear H.
  apply RCmp_ with (b:=i).
   apply H3.
   apply HR'; apply RDomR_.
    apply H2.
    apply H4.
Qed.

(*================================================================================================*)