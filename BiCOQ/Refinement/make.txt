; Fichier de dependance pour le project MLS
; Syntaxe : X Y1 ... Yn pour produire X.vo � partir de X.v Y1 ... Yn
subset_gen
relation_gen compiled\subset_gen.vo
data_ref compiled\relation_gen.vo
choice_ref_pre compiled\relation_gen.vo
choice_ref_guard compiled\relation_gen.vo
choice_ref_try1 compiled\relation_gen.vo
gen_ref_pre compiled\data_ref.vo compiled\choice_ref_pre.vo
gen_ref_guard compiled\data_ref.vo compiled\choice_ref_pre.vo compiled\choice_ref_guard.vo
choice_ref compiled\relation_gen.vo
choice_ref_try2 compiled\relation_gen.vo
gen_ref compiled\relation_gen.vo
bool_choice
