(*==================================================================================================
  Project : Study of the concept of refinement
  Module : gen_ref_guard
  We study the concept of gen-refinement, combining data-refinement and choice-refinement: starting
  from a relation between abstract supports, we aim at producing an "compliant" function (a
  functional relation) between concrete supports. We start by providing a relational presentation of
  support refinement to have a common universe of discourse. In addition we also question the
  definition of sharpening refinement, to allow for the representation of guards as well as of
  preconditions.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: July 2009, Md: July 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export data_ref.
Require Export choice_ref_pre.
Require Export choice_ref_guard.

(* Data-refinement equivalent definition ---------------------------------------------------------*)

Theorem DatRef_rel:forall (Da Ra Dc Rc:Set)(fa:Da->Ra)(fc:Dc->Rc)(Id:Da↔Dc)(Ir:Ra↔Rc),
                   §(fa)⇝§(fc)[Id,Ir]<->(⇔fc)∘Id⊆Ir∘(⇔fa).
Proof.
 intros Da Ra Dc Rc fa fc Id Ir; unfold DatRef; unfold SInc; simpl; split; intros Href.
  intros [xa yc] Hrel; inversion_clear Hrel; rename b into xc; inversion_clear H0;
   apply RCmp_ with (b:=fa(xa)); [apply RofF_ | apply Href; apply H].
  intros xa xc Hrel; generalize (RCmp_ _ _ _ _ (⇔fc) _ _ Hrel (fc xc) (RofF_ _ _ fc xc));
   intros Hrel'; generalize (Href (xa,fc xc) Hrel'); intros Hrel''; inversion_clear Hrel'';
   inversion H; rewrite <- H3 in H0; apply H0.
Qed.

(* gen-refinement definition ---------------------------------------------------------------------*)
(* Basically, considering definitions of support refinement and sharpening refinement, the combined
   refinement could be defined by Id@∏1(Pa)⊆∏1(Pc) /\ Pc∘Id⊆Ir∘Pa. Id@∏1(Pa)⊆∏1(Pc) ensures that the
   refinement Pc covers the case specified by the specification, and Pc∘Id⊆Ir∘Pa the compatibility
   of the refinement modulo the interpretations. *)

Definition GeGRef(Da Ra Dc Rc:Set)(Pa:Da↔Ra)(Pc:Dc↔Rc)(Id:Da↔Dc)(Ir:Ra↔Rc):=
 Id@∏1(Pa)⊆∏1(Pc) /\ Pc∘Id⊆Ir∘Pa.
Notation "Pa '⊑' Pc '[' Id ',' Ir ']'":=
 (GeGRef _ _ _ _ Pa Pc Id Ir) (no associativity, at level 90).

(* Relations between refinements -----------------------------------------------------------------*)

Theorem DatRef_GeGRef:forall (Da Ra Dc Rc:Set)(fa:Da->Ra)(fc:Dc->Rc)(Id:Da↔Dc)(Ir:Ra↔Rc),
                      §(fa)⇝§(fc)[Id,Ir]->(⇔fa)⊑(⇔fc)[Id,Ir].
Proof.
 intros Da Ra Dc Rc fa fc Id Ir Href; unfold GeGRef;
  generalize (proj1 (DatRef_rel Da Ra Dc Rc fa fc Id Ir) Href); intros Href'; split.
  intros xc _; apply RDom_ with (b:=fc xc); apply RofF_.
  intros [xa yc] Hrel; apply Href'; apply Hrel.
Qed.

Theorem ChGRef_GeGRef:forall (D R:Set)(Pa:D↔R)(Pc:D↔R), Pa=≻Pc->Pa⊑Pc[Ⅰ(D),Ⅰ(R)].
Proof.
 intros D R Pa Pc HCR; generalize (ChGRef_dom _ _ _ _ HCR); intros HD;
  destruct HCR as [_ HR]; unfold GeGRef; split.
  intros d Hd; inversion_clear Hd; inversion H0; rewrite H3 in H; apply (proj1 HD); apply H.
  intros [d r] Hdr; inversion_clear Hdr; inversion_clear H; clear d.
  apply (RCmp_ _ _ _ _ (Ⅰ(R)) _ _ (HR _ H0) r); apply RId_.
Qed.

Theorem ChPRef_GeGRef:forall (D R:Set)(Pa:D↔R)(Pc:D↔R), Pa≻Pc->Pa⊑Pc[Ⅰ(D)⊳∏1(Pa),Ⅰ(R)].
Proof.
 intros D R Pa Pc [HD HR]; unfold GeGRef; split.
  apply (SInc_trans D ((Ⅰ(D)⊳∏1(Pa))@∏1(Pa)) ∏1(Pa) ∏1(Pc)) with (2:=HD); intros d Hd;
   inversion_clear Hd; inversion_clear H0; apply H2.
  intros d Hd; inversion_clear Hd; inversion_clear H; inversion_clear H1; clear a.
  apply (RCmp_ D R R Pa (Ⅰ(R)) b c); [ | apply RId_].
   apply HR; apply RDomR_; [apply H0 | apply H2].
Qed.

(* Combined refinement properties ----------------------------------------------------------------*)

Theorem GeGRef_refl:forall (D R:Set)(P:D↔R), P⊑P[Ⅰ(D),Ⅰ(R)]. (* Reflexivity *)
Proof.
 intros D R P; unfold GeGRef; split.
  intros d Hd; inversion_clear Hd; inversion H0; rewrite <- H3; apply H.
  intros d Hd; inversion_clear Hd; inversion_clear H;
   apply (RCmp_ _ _ R _ (Ⅰ(R)) _ _ H0 c (RId_ _ c)).
Qed.

Theorem GeGRef_trans:forall (Da Ra Di Ri Dc Rc:Set)(Pa:Da↔Ra)(Pi:Di↔Ri)(Pc:Dc↔Rc)
                            (Id1:Da↔Di)(Id2:Di↔Dc)(Ir1:Ra↔Ri)(Ir2:Ri↔Rc),
                     Pa⊑Pi[Id1,Ir1]->Pi⊑Pc[Id2,Ir2]->Pa⊑Pc[Id2∘Id1,Ir2∘Ir1].
Proof.
 unfold GeGRef; intros Da Ra Di Ri Dc Rc Pa Pi Pc Id1 Id2 Ir1 Ir2 [Hai1 Hai2] [Hir1 Hir2]; split.
  apply (SInc_trans _ ((Id2∘Id1)@∏1(Pa)) (Id2@∏1(Pi)) ∏1(Pc)).
   intros xc Hxc; inversion_clear Hxc; rename a into xa; inversion_clear H0; rename b into xi;
    apply RImg_ with (a:=xi); [apply (Hai1 _ (RImg_ _ _ _ _ _ H _ H1)) | apply H2].
   apply Hir1.
  intros [xa yc] Hac; generalize ((proj1 (RCmp_assoc _ _ _ _ Id1 Id2 Pc)) _ Hac);
   intros H'ac; inversion_clear H'ac; rename b into xi.
  generalize (Hir2 _ H0); intros H1; inversion_clear H1; rename b into yi.
  apply (proj1 (RCmp_assoc _ _ _ _ Pa Ir1 Ir2)); apply RCmp_ with (b:=yi); [ | apply H3].
   apply Hai2; apply RCmp_ with (b:=xi); [apply H | apply H2].
Qed.

Theorem GeGRef_SInc_l:forall (Da Ra Dc Rc:Set)(Id Id':Da↔Dc)(Ir:Ra↔Rc)(Pa:Da↔Ra)(Pc:Dc↔Rc),
                      Pa⊑Pc[Id,Ir]->Id'⊆Id->Pa⊑Pc[Id',Ir].
Proof.
 intros Da Ra Dc Rc Id Id' Ir Pa Pc [HR1 HR2] HId; split.
  intros xc Hxc; inversion_clear Hxc; rename a into xa; apply HR1;
   apply (RImg_ _ _ _ _ _ H _ (HId _ H0)).
  intros [xa yc] H; inversion_clear H; apply HR2; apply RCmp_ with (b:=b).
   apply HId; apply H0.
   apply H1.
Qed.

Theorem GeGRef_SInc_r:forall (Da Ra Dc Rc:Set)(Id:Da↔Dc)(Ir Ir':Ra↔Rc)(Pa:Da↔Ra)(Pc:Dc↔Rc),
                      Pa⊑Pc[Id,Ir]->Ir⊆Ir'->Pa⊑Pc[Id,Ir'].
Proof.
 intros Da Ra Dc Rc Id Ir Ir' Pa Pc [HR1 HR2] HIr; split.
  apply HR1.
  apply (SInc_trans _ _ _ (Ir'∘Pa) HR2); apply (RCmp_SInc_l); apply HIr.
Qed.

Theorem GeGRef_RCmp_al:forall (Da Da' Ra Dc Rc:Set)
                              (Id:Da↔Dc)(Ir:Ra↔Rc)(Pa:Da↔Ra)(Pc:Dc↔Rc)(G:Da'↔Da),
                              Pa⊑Pc[Id,Ir]->∏2(G)⊆∏1(Pa)->Pa∘G⊑Pc[Id∘G,Ir].
Proof.
 intros Da Da' Ra Dc Rc Id Ir Pa Pc G [HR1 HR2] HG; split.
  intros xc Hxc; inversion_clear Hxc; rename a into xa'; inversion_clear H; rename b into ya;
   inversion_clear H0; rename b into xa.
  apply HR1; apply RImg_ with (a:=xa); [apply HG; apply (RRan_ _ _ _ _ _ H) | apply H2].
  intros [xa' yc] H.
  generalize ((proj1 (RCmp_assoc _ _ _ _ G Id Pc)) _ H); intros H0; inversion_clear H0.
  apply (proj2 (RCmp_assoc _ _ _ _ G Pa Ir)).
  apply RCmp_ with (b:=b); [apply H1 | apply HR2; apply H2].
Qed.

Theorem GeGRef_RCmp_cr:forall (Da Ra Dc Rc Rc':Set)
                              (Id:Da↔Dc)(Ir:Ra↔Rc)(Pa:Da↔Ra)(Pc:Dc↔Rc)(G:Rc↔Rc'),
                              Pa⊑Pc[Id,Ir]->∏2(Pc)⊆∏1(G)->Pa⊑G∘Pc[Id,G∘Ir].
Proof.
 intros Da Ra Dc Rc Rc' Id Ir Pa Pc G [HR1 HR2] HG; split.
  intros xc Hxc; inversion_clear Hxc; rename a into xa.
  generalize (HR1 _ (RImg_ _ _ _ _ _ H _ H0)); intros H1; inversion_clear H1; rename b into yc.
  generalize (HG _ (RRan_ _ _ _ _ _ H2)); intros Hyc; inversion_clear Hyc; rename b into yc'.
  apply RDom_ with (b:=yc'); apply RCmp_ with (b:=yc); [apply H2 | apply H1].
  intros [xa yc'] H.
  generalize ((proj2 (RCmp_assoc _ _ _ _ Id Pc G)) _ H); intros H0; inversion_clear H0.
  apply (proj1 (RCmp_assoc _ _ _ _ Pa Ir G)).
  apply RCmp_ with (b:=b); [apply HR2; apply H1 | apply H2].
Qed.

Theorem GeGRef_SEqu_l:forall (Da Ra Dc Rc:Set)(Id:Da↔Dc)(Ir:Ra↔Rc)(Pa Pa':Da↔Ra)(Pc:Dc↔Rc),
                      Pa⊑Pc[Id,Ir]->Pa≡Pa'->Pa'⊑Pc[Id,Ir].
Proof.
 intros Da Ra Dc Rc Id Ir Pa Pa' Pc [HR1 HR2] HEq; split.
  intros xc H; inversion_clear H; rename a into xa; inversion_clear H0; rename b into ya; apply HR1;
   apply RImg_ with (a:=xa).
   apply RDom_ with (b:=ya); apply (proj2 HEq); apply H.
   apply H1.
  intros [xa yc] H; generalize (HR2 _ H); intros H0; inversion_clear H0;
   apply RCmp_ with (b:=b); [apply (proj1 HEq); apply H1 | apply H2].
Qed.

Theorem GeGRef_SEqu_r:forall (Da Ra Dc Rc:Set)(Id:Da↔Dc)(Ir:Ra↔Rc)(Pa:Da↔Ra)(Pc Pc':Dc↔Rc),
                      Pa⊑Pc[Id,Ir]->Pc≡Pc'->Pa⊑Pc'[Id,Ir].
Proof.
 intros Da Ra Dc Rc Id Ir Pa Pc Pc' [HR1 HR2] HEq; split.
  apply SInc_trans with (S2:=∏1(Pc)).
   apply HR1.
   intros xc H; inversion_clear H; rename b into yc; apply RDom_ with (b:=yc); apply (proj1 HEq);
    apply H0.
  intros [xa yc] H; inversion_clear H; apply HR2; apply RCmp_ with (b:=b);
  [apply H0 | apply (proj2 HEq); apply H1].
Qed.

(* The Distracted Composer problem ---------------------------------------------------------------*)
(* We expect the composition of two refinements to be a refinement of the composition, and identify
   sufficient conditions, as for support refinement and sharpening refinement. However, we add a
   small difficulty by not enforcing the use of a common intermediate concrete support. To compose
   the refinements, we therefore need in addition a glue for which we identify sufficient conditions
   as well. *)

Theorem GeGRef_Distract:forall (Da Ia Ra Dc Ic Jc Rc:Set)
                               (Id:Da↔Dc)(Ii:Ia↔Ic)(Ij:Ia↔Jc)(Ir:Ra↔Rc)
                               (Pa1:Da↔Ia)(Pa2:Ia↔Ra)(Pc1:Dc↔Ic)(Pc2:Jc↔Rc)(G:Ic↔Jc),
                               Pa1⊳∏1(Pa2)⊑Pc1[Id,Ii]-> (* First constrained refinement *)
                               Pa2⊑Pc2[Ij,Ir]->         (* Second refinement *)
                               ∏2(Pc1)⊆∏1(G)->G∘Ii⊆Ij-> (* Glue conditions *)
                               Pa2∘Pa1⊑Pc2∘G∘Pc1[Id,Ir].
Proof.
 intros Da Ia Ra Dc Ic Jc Rc Id Ii Ij Ir Pa1 Pa2 Pc1 Pc2 G [HD1 HR1] [HD2 HR2] HG1 HG2; split.
  (* Completion : domain coverage *)
  intros dc Hdc; inversion_clear Hdc; rename a into da; inversion_clear H; rename b into ra;
   inversion_clear H1; rename b into ia.
  assert (Hdc:∏1(Pc1) dc).
   apply HD1; apply RImg_ with (a:=da).
    apply RDom_ with (b:=ia); apply RRanR_; [apply H | apply RDom_ with (b:=ra); apply H2].
    apply H0.
  inversion_clear Hdc; rename b into ic.
  generalize (HG1 _ (RRan_ _ _ _ _ _ H1)); intros Hic; inversion_clear Hic; rename b into jc.
  generalize (HR1 _ (RCmp_ Da Dc Ic Id Pc1 _ _ H0 _ H1)); intros Hdaic; inversion_clear Hdaic;
   rename b into ia'.
  inversion_clear H4.
  generalize (HG2 _ (RCmp_ _ _ _ Ii G _ _ H5 _ H3)); intros Hia'jc.
  generalize (HD2 _ (RImg_ _ _ _ _ _ H7 _ Hia'jc)); intros Hjc; inversion_clear Hjc;
   rename b into rc.
  apply RDom_ with (b:=rc); apply RCmp_ with (b:=ic); [apply H1 | ];apply RCmp_ with (b:=jc);
  [apply H3 | apply H4].
  (* Correction : refinement *)
  intros [da rc] H; inversion_clear H; rename b into dc; inversion_clear H1; rename b into ic;
   inversion_clear H2; rename b into jc.
  generalize (HR1 _ (RCmp_ _ _ _ Id Pc1 _ _ H0 _ H)); intros Hdaic; inversion_clear Hdaic;
   rename b into ia; inversion_clear H2; inversion_clear H6; rename b into ra.
  apply (proj2 (RCmp_assoc _ _ _ _ Pa1 Pa2 Ir)).
  apply RCmp_ with (b:=ia); [apply H5 | ]; apply HR2.
  apply RCmp_ with (b:=jc); [ | apply H3].
  apply HG2; apply RCmp_ with (b:=ic); [apply H4 | apply H1].
Qed.

(*================================================================================================*)