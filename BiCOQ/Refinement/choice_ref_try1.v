(*==================================================================================================
  Project : Study of the concept of refinement
  Module : choice_ref_try1
  We study the concept of choice-refinement, the elimination of non-determinism ; building upon the
  definitions of choice_ref_guard.v, we introduce preconditions as an additional parameter of
  specification.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: July 2009, Md: July 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export relation_gen.

(* Choice refinement definition ------------------------------------------------------------------*)

Definition CT1Ref(dom ran:Set)(Sa Sc:dom↔ran)(Pre:⇑dom):=Pre∩∏1(Sa)⊆∏1(Sc) /\ Pre⊲Sc⊆Sa.
Notation "Sa ',' P '|=≻' Sc":=(CT1Ref _ _ Sa Sc P) (no associativity, at level 90).
(* Nota: ∏1(Sa)⊆∏1(Sc) ensures that the refinement addresses all the specification and Pre⊲Sc⊆Sa
  that the refinement is compliant in the scope of the precondition. If the precondition is
  trivially true, that is P=dom, then we find back the choice refinement with guard. As long as the
  precondition includes the domain of Sc, we have the same constraints as for the choice refinement.
  And if P is not trivial and smaller than the domain of Sc, then we have a relaxed refinement
  (precondition is prioritary in this definition upon guard).
  To introduce explicitely a guard as well, we propose Pre∩∏1(Sa)⊆Grd∩∏1(Sc) /\ Pre⊲Sc⊆Grd⊲Sa,
  noting that in CT1Ref Grd=∏1(Sa)
*)

(* Choice refinement properties ------------------------------------------------------------------*)

Theorem CT1Ref_refl:forall (dom ran:Set)(S:dom↔ran)(P:⇑dom), S,P|=≻S. (* Reflexivity *)
Proof.
 intros dom ran S P; unfold CT1Ref; split.
  intros d Hd; inversion_clear Hd; apply H0.
  intros [d r] Hdr; inversion_clear Hdr; apply H.
Qed.

Theorem CT1Ref_asym:forall (dom ran:Set)(S1 S2:dom↔ran)(P:⇑dom),  S1,P|=≻S2->S2,P|=≻S1->P⊲S1≡P⊲S2.
Proof.
 intros dom ran S1 S2 P; unfold CT1Ref; unfold SEqu; unfold SInc; intros [Hd12 Hi12] [Hd21 Hi21];
  split; intros [d r] Hdr; [generalize (Hi21 _ Hdr) | generalize (Hi12 _ Hdr)]; intros H'dr;
  inversion_clear Hdr; apply (RDomR_ _ _ _ _ _ _ H'dr H0).
Qed.

Theorem ChcRef_tran:forall (dom ran:Set)(Sa Si Sc:dom↔ran)(P1 P2:⇑dom),
                    Sa,P1|=≻Si->Si,P2|=≻Sc->Sa,P1∩P2|=≻Sc.
Proof.
 intros dom ran Sa Si Sc P1 P2; unfold CT1Ref; intros [Hdai Hrai] [Hdic Hric]; split.
  intros d Hd; inversion_clear Hd; inversion_clear H; apply Hdic; apply SInt_.
   apply H2.
   apply Hdai; apply SInt_.
    apply H1.
    apply H0.
  intros [d r] Hdr; inversion_clear Hdr; inversion_clear H0; apply Hrai; apply RDomR_.
   apply Hric; apply RDomR_.
    apply H.
    apply H2.
   apply H1.
Qed.

(*================================================================================================*)