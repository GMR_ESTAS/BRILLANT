(*==================================================================================================
  Project : Study of the concept of refinement
  Module : relation_gen (Generic notions about relations)
  While the standard library defines a relation as A->B->Prop, this module defines a relation as
  (A*B)->Prop, that is a subset of the cartesian product.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI, UPMC/LIP6 - St: February 2007, Md: March 2008
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export subset_gen.

(* Relation definitions --------------------------------------------------------------------------*)

Definition Relation(A B:Set):=⇑(A*B). (* Relation is a subset of the cartesian product *)
Infix "↔":=Relation (at level 30).

Inductive RId(A:Set):A↔A:= RId_:forall (a:A), (RId A)(a,a). (* Identity *)
Notation "'Ⅰ(' A ')'":=(RId A) (at level 30).

Inductive RDom(A B:Set)(R:A↔B):⇑A:=RDom_:forall (a:A)(b:B), R(a,b)->(RDom A B R) a. (* Domain *)
Notation "'∏1(' R ')'":=(RDom _ _ R).

Inductive RRan(A B:Set)(R:A↔B):⇑B:=RRan_:forall (a:A)(b:B), R(a,b)->(RRan A B R) b. (* Range *)
Notation "'∏2(' R ')'":=(RRan _ _ R).

Inductive RofF(A B:Set)(f:A->B):A↔B:=RofF_:forall (a:A), (RofF A B f)(a,f(a)). (* Function to Rel *)
Notation "'⇔' f":=(RofF _ _ f) (no associativity, at level 50).

Inductive RInv(A B:Set)(R:A↔B):B↔A:=RInv_:forall (a:A)(b:B), R(a,b)->(RInv A B R)(b,a). (* Inver. *)
Notation "'↩' R":=(RInv _ _ R) (right associativity, at level 45).

Inductive RDomR(A B:Set)(R:A↔B)(S:⇑A):(A↔B):= (* Domain restriction *)
 | RDomR_:forall (a:A)(b:B), R(a,b)->S a->(RDomR A B R S)(a,b).
Notation "S '⊲' R":=(RDomR _ _ R S) (right associativity, at level 45).

Inductive RRanR(A B:Set)(R:A↔B)(S:⇑B):(A↔B):= (* Range restriction *)
 | RRanR_:forall (a:A)(b:B), R(a,b)->S b->(RRanR A B R S)(a,b).
Notation "R '⊳' S":=(RRanR _ _ R S) (right associativity, at level 45).

Inductive RCmp(A B C:Set)(R1:A↔B)(R2:B↔C):A↔C:= (* Composition *)
 | RCmp_:forall (a:A)(b:B), R1(a,b)->forall (c:C), R2(b,c)->(RCmp A B C R1 R2)(a,c).
Notation "R2 '∘' R1":=(RCmp _ _ _ R1 R2) (at level 40).

Inductive RImg(A B:Set)(R:A↔B)(S:⇑A):⇑B:= (* Image of a subset by a relation  *)
 | RImg_:forall (a:A), S a->forall (b:B), R(a,b)->(RImg A B R S) b.
Notation "R '@' S":=(RImg _ _ R S) (at level 40).

Inductive SCart(A B:Set)(SA:⇑A)(SB:⇑B):A↔B:= (* Subset cartesian product *)
 | SCart_:forall (a:A), SA a->forall (b:B), SB b->(SCart A B SA SB)(a,b).
Notation "S1 '∙' S2":=(SCart _ _ S1 S2) (at level 40).

Inductive RDPd(A B1 B2:Set)(R1:A↔B1)(R2:A↔B2):A↔(B1*B2):= (* Direct product *)
 | RDPd_:forall (a:A)(b1:B1), R1(a,b1)->forall (b2:B2), R2(a,b2)->(RDPd A B1 B2 R1 R2)(a,(b1,b2)).
Notation "R1 '⊗' R2":=(RDPd _ _ _ R1 R2) (at level 40).

Inductive RPPd(A1 A2 B1 B2:Set)(R1:A1↔B1)(R2:A2↔B2):(A1*A2)↔(B1*B2):= (* Parallel product *)
 | RPPd_:forall (a1:A1)(b1:B1), R1(a1,b1)->forall (a2:A2)(b2:B2), R2(a2,b2)->
         (RPPd A1 A2 B1 B2 R1 R2)((a1,a2),(b1,b2)).
Notation "R '∥' S":=(RPPd _ _ _ _ R S) (at level 40).

(* Relation predicates ---------------------------------------------------------------------------*)

Definition RFnc(A B:Set)(R:A↔B):=forall (a:A)(b1 b2:B), R(a,b1)->R(a,b2)->b1=b2. (* Functional *)
Implicit Arguments RFnc [A B].

Definition RInj(A B:Set)(R:A↔B):=forall (a1 a2:A)(b:B), R(a1,b)->R(a2,b)->a1=a2. (* Injective *)
Implicit Arguments RInj [A B].

Definition RTot(A B:Set)(R:A↔B):=⊛(A)⊆∏1(R). (* Total *)
Implicit Arguments RTot [A B].

Definition RSur(A B:Set)(R:A↔B):=⊛(B)⊆∏2(R). (* Surjective *)
Implicit Arguments RSur [A B].

Definition RRefl(A:Set)(R:A↔A):=forall (a:A), R(a,a). (* Reflexive *)
Implicit Arguments RRefl [A].

Definition RSym(A:Set)(R:A↔A):=forall (a1 a2:A), R(a1,a2)->R(a2,a1). (* Symmetric *)
Implicit Arguments RSym [A].

Definition RTrans(A:Set)(R:A↔A):=forall (a1 a2 a3:A), R(a1,a2)->R(a2,a3)->R(a1,a3). (* Transitive *)
Implicit Arguments RTrans [A].

Definition REquiv(A:Set)(R:A↔A):=RRefl R /\ RSym R /\ RTrans R.
Implicit Arguments REquiv [A].

(* A remark about the standard library relations -------------------------------------------------*)

Inductive SRofR(A B:Set)(R:A↔B):A->B->Prop:=
 SRofR_:forall (a:A)(b:B), R(a,b)->(SRofR A B R) a b.

Theorem SRofR_valid:forall (A B:Set)(R:A↔B),
                    exists R':A->B->Prop, (forall (a:A)(b:B), R(a,b)<->R' a b).
Proof.
 intros A B R; exists (SRofR A B R); intros a b; split; intros HR.
  apply SRofR_; apply HR.
  destruct HR as [a b HR']; apply HR'.
Qed.

Inductive RofSR(A B:Set)(R:A->B->Prop):A↔B:=
 RofSR_:forall (a:A)(b:B), R a b->(RofSR A B R)(a,b).

Theorem RofSR_valid:forall (A B:Set)(R:A->B->Prop),
                    exists R':A↔B, (forall (a:A)(b:B), R a b<->R'(a,b)).
Proof.
 intros A B R; exists (RofSR A B R); intros a b; split; intros HR.
  apply RofSR_; apply HR.
  inversion_clear HR; apply H.
Qed.

(* Relation results ------------------------------------------------------------------------------*)

Theorem RId_RTot:forall (A:Set), RTot (Ⅰ(A)).
Proof.
 intros A; unfold RTot; unfold SInc; intros a Ha; apply RDom_ with (b:=a); apply RId_.
Qed.

Theorem RId_RSur:forall (A:Set), RSur (Ⅰ(A)).
Proof.
 intros A; unfold RSur; unfold SInc; intros a Ha; apply RRan_ with (a:=a); apply RId_.
Qed.

Theorem RId_RInj:forall (A:Set), RInj (Ⅰ(A)).
Proof.
 intros A; unfold RInj; intros a1 a2 b Ha1b Ha2b; inversion_clear Ha1b; inversion_clear Ha2b;
  apply refl_equal.
Qed.

Theorem RId_RFnc:forall (A:Set), RFnc (Ⅰ(A)).
Proof.
 intros A; unfold RFnc; intros a b1 b2 Hab1 Hab2; inversion Hab1; inversion Hab2; rewrite <- H1;
  rewrite <- H3; apply refl_equal.
Qed.

Theorem RId_RInv:forall (A:Set), Ⅰ(A)≡↩Ⅰ(A).
Proof.
 intros A; unfold SEqu; unfold SInc; split; intros [a b] Hab.
  apply RInv_; inversion_clear Hab; apply RId_.
  inversion_clear Hab; inversion_clear H; apply RId_.
Qed.

Theorem RId_REquiv:forall (A:Set), REquiv (Ⅰ(A)).
Proof.
 intros A; unfold REquiv; split ;[ unfold RRefl | split; [unfold RSym | unfold RTrans]].
  intros a; apply RId_.
  intros a1 a2 H12; inversion_clear H12; apply RId_.
  intros a1 a2 a3 H12 H23; inversion_clear H12; inversion_clear H23; apply RId_.
Qed.

Theorem RofF_RFnc:forall (A B:Set)(f:A->B), RFnc (⇔f).
Proof.
 intros A B F; unfold RFnc; intros a b1 b2 Hab1 Hab2; inversion_clear Hab1; inversion_clear Hab2;
  apply refl_equal.
Qed.

Theorem RofF_RTot:forall (A B:Set)(f:A->B), RTot (⇔f).
Proof.
 intros A B f; unfold RTot; intros a; exists (f a); apply RofF_.
Qed.

Theorem RInv_idem:forall (A B:Set)(R:A↔B), R≡↩(↩R).
Proof.
 intros A B R; unfold SEqu; unfold SInc; split; intros [a b] Hab.
  apply RInv_; apply RInv_; apply Hab.
  destruct Hab as [a' b' Hab]; inversion_clear Hab; apply H.
Qed.

Theorem RInv_RDom_RRan:forall (A B:Set)(R:A↔B), ∏1(R)≡∏2(↩R).
Proof.
 intros A B R; unfold SEqu; unfold SInc; split; intros a Hab; destruct Hab as [a b Hab].
  apply RRan_ with (a:=b); apply RInv_; apply Hab.
  apply RDom_ with (b:=a); inversion_clear Hab; apply H.
Qed.

Theorem RInv_RRan_RDom:forall (A B:Set)(R:A↔B), ∏2(R)≡∏1(↩R).
Proof.
 intros A B R; unfold SEqu; unfold SInc; split; intros a Hab; destruct Hab as [a b Hab].
  apply RDom_ with (b:=a); apply RInv_; apply Hab.
  apply RRan_ with (a:=b); inversion_clear Hab; apply H.
Qed.

Theorem RInv_RTot:forall (A B:Set)(R:A↔B), RTot R->RSur (↩R).
Proof.
 intros A B R; unfold RTot; unfold RSur; intros HDom; apply (SInc_SEqu_trans _ _ _ ∏2(↩R) HDom);
  apply RInv_RDom_RRan.
Qed.

Theorem RInv_RInj:forall (A B:Set)(R:A↔B), RInj R->RFnc (↩R).
Proof.
 intros A B R; unfold RInj; unfold RFnc; intros HRan; intros b a1 a2 Hba1 Hba2;
  apply (HRan a1 a2 b).
  inversion_clear Hba1; apply H.
  inversion_clear Hba2; apply H.
Qed.

Theorem RDomR_self:forall (A B:Set)(R:A↔B), ∏1(R)⊲R≡R.
Proof.
 intros A B R; unfold SEqu; unfold SInc; split; intros [a b] Hab.
  inversion_clear Hab; apply H.
  apply RDomR_; [ | apply RDom_ with (b:=b)]; apply Hab.
Qed.
   
Theorem RRanR_self:forall (A B:Set)(R:A↔B), R⊳∏2(R)≡R.
Proof.
 intros A B R; unfold SEqu; unfold SInc; split; intros [a b] Hab.
  inversion_clear Hab; apply H.
  apply RRanR_; [ | apply RRan_ with (a:=a)]; apply Hab.
Qed.

Theorem RCmp_RDom_RRan:forall (A B C:Set)(R1:A↔B)(R2:B↔C), ∏2(R1)∩∏1(R2)⊆∅(B)->R2∘R1⊆∅(A*C).
Proof.
 intros A B C R1 R2; unfold SInc; intros HB a Ha; apply SEmp_; 
  inversion_clear Ha as [a0 b Hab c Hbc]; clear a; rename a0 into a; cut (∅(B) b).
  intros Hb; destruct Hb as [b HF]; apply HF.
  apply HB; apply SInt_.
   apply RRan_ with (a:=a); apply Hab.
   apply RDom_ with (b:=c); apply Hbc.
Qed.

Theorem RCmp_RDom_SInc:forall (A B C:Set)(R1:A↔B)(R2:B↔C), ∏1(R2∘R1)⊆∏1(R1).
Proof.
 intros A B C R1 R2; unfold SInc; intros a Ha; destruct Ha as [a c Hac]; inversion_clear Hac;
  apply RDom_ with (b:=b); apply H.
Qed.

Theorem RCmp_RRan_SInc:forall (A B C:Set)(R1:A↔B)(R2:B↔C), ∏2(R2∘R1)⊆∏2(R2).
Proof.
 intros A B C R1 R2; unfold SInc; intros c Hc; destruct Hc as [a c Hac]; inversion_clear Hac;
  apply RRan_ with (a:=b); apply H0.
Qed.

Theorem RCmp_RInv:forall (A B C:Set)(R1:A↔B)(R2:B↔C), ↩(R2∘R1)≡(↩R1)∘(↩R2).
Proof.
 intros A B C R1 R2; unfold SEqu; unfold SInc; split; intros [c a] Ha.
  inversion_clear Ha; inversion_clear H; apply RCmp_ with (b:=b); apply RInv_;
   [apply H1 | apply H0].
  apply RInv_; inversion_clear Ha; inversion_clear H; inversion_clear H0; apply RCmp_ with (b:=b);
   [apply H | apply H1].
Qed.

Theorem RImg_RDom:forall (A B:Set)(R:A↔B), R@∏1(R)≡∏2(R).
Proof.
 intros A B R; unfold SEqu; unfold SInc; split; intros b Hb.
  inversion_clear Hb; apply RRan_ with (a:=a); apply H0.
  inversion_clear Hb; apply RImg_ with (a:=a); [apply RDom_ with (b:=b) | ]; apply H.
Qed.

Theorem RDPd_RDom:forall (A B1 B2:Set)(R1:A↔B1)(R2:A↔B2), ∏1(R1⊗R2)≡∏1(R1)∩∏1(R2).
Proof.
 unfold SInc; intros A B1 B2 R1 R2; split; intros a Ha.
  inversion_clear Ha; induction b as [b1 b2]; inversion_clear H; apply SInt_;
   [apply RDom_ with (b:=b1); apply H0 | apply RDom_ with (b:=b2); apply H1].
  inversion_clear Ha; inversion_clear H; inversion_clear H0; apply RDom_ with (b:=(b,b0));
   apply RDPd_; [apply H1 | apply H].
Qed.

Theorem RDPd_RRan:forall (A B1 B2:Set)(R1:A↔B1)(R2:A↔B2), ∏2(R1⊗R2)⊆∏2(∏1(R2)⊲R1)∙∏2(∏1(R1)⊲R2).
Proof.
 unfold SInc; intros A B1 B2 R1 R2; intros [b1 b2] Hb; inversion_clear Hb; inversion_clear H;
  apply SCart_; apply RRan_ with (a:=a); apply RDomR_;
  [apply H0 | apply RDom_ with (b:=b2); apply H1 | apply H1 | apply RDom_ with (b:=b1); apply H0].
Qed.

Theorem RPPd_RDom:forall (A1 A2 B1 B2:Set)(R1:A1↔B1)(R2:A2↔B2), ∏1(R1∥R2)≡∏1(R1)∙∏1(R2).
Proof.
 intros A1 A2 B1 B2 R1 R2; unfold SEqu; unfold SInc; split; intros [a1 a2] Ha; inversion_clear Ha.
  induction b as [b1 b2]; inversion_clear H; apply SCart_;
   [apply RDom_ with (b:=b1); apply H0 | apply RDom_ with (b:=b2); apply H1].
  inversion_clear H; rename b into b1; inversion_clear H0; rename b into b2;
   apply RDom_ with (b:=(b1,b2)); apply RPPd_; [apply H1 | apply H].
Qed.

Theorem RPPd_RRan:forall (A1 A2 B1 B2:Set)(R1:A1↔B1)(R2:A2↔B2), ∏2(R1∥R2)≡∏2(R1)∙∏2(R2).
Proof.
 intros A1 A2 B1 B2 R1 R2; unfold SEqu; unfold SInc; split; intros [b1 b2] Hb; inversion_clear Hb.
  induction a as [a1 a2]; inversion_clear H; apply SCart_;
   [apply RRan_ with (a:=a1); apply H0 | apply RRan_ with (a:=a2); apply H1].
  inversion_clear H; rename a into a1; inversion_clear H0; rename a into a2;
   apply RRan_ with (a:=(a1,a2)); apply RPPd_; [apply H1 | apply H].
Qed.

Theorem RCmp_RDomR_RRanR:forall (A B C:Set)(R1:A↔B)(R2:B↔C), (R2∘R1)≡(∏2(R1)⊲R2)∘(R1⊳∏1(R2)).
Proof.
 intros A B C R1 R2; unfold SEqu; unfold SInc; split; intros [a c] Hac; inversion_clear Hac;
  apply RCmp_ with (b:=b).
  apply RRanR_; [apply H | apply RDom_ with (b:=c); apply H0].
  apply RDomR_; [apply H0 | apply RRan_ with (a:=a); apply H].
  inversion_clear H; apply H1.
  inversion_clear H0; apply H1.
Qed.

Theorem RCmp_SInc_r:forall (A B:Set)(R1 R1':A↔B), R1⊆R1'->forall (C:Set)(R2:B↔C), R2∘R1⊆R2∘R1'.
Proof.
 intros A B R1 R1' HInc C R2; unfold SInc; intros [a c] Hac; inversion_clear Hac;
  apply RCmp_ with (b:=b); [apply HInc; apply H | apply H0].
Qed.

Theorem RCmp_SInc_l:forall (B C:Set)(R2 R2':B↔C), R2⊆R2'->forall (A:Set)(R1:A↔B), R2∘R1⊆R2'∘R1.
Proof.
 intros B C R2 R2' HInc A R1; unfold SInc; intros [a c] Hac; inversion_clear Hac;
  apply RCmp_ with (b:=b) ; [apply H | apply HInc; apply H0].
Qed.

Theorem RCmp_assoc:forall (A B C D:Set)(R1:A↔B)(R2:B↔C)(R3:C↔D), R3∘(R2∘R1)≡(R3∘R2)∘R1.
Proof.
 intros A B C D R1 R2 R3; unfold SEqu; unfold SInc; split; intros [a d] Had.
  inversion_clear Had; rename b into c; inversion_clear H; apply RCmp_ with (b:=b);
   [apply H1 | apply RCmp_ with (b:=c); [apply H2 | apply H0]].
  inversion_clear Had; inversion_clear H0; rename b0 into c; apply RCmp_ with (b:=c);
   [apply RCmp_ with (b:=b); [apply H | apply H1] | apply H2].
Qed.

(* Congruence rules ------------------------------------------------------------------------------*)

Theorem SEqu_RDom:forall (A B:Set), SEqu_congr_fun (RDom A B).
Proof.
 intros A B; unfold SEqu_congr_fun; unfold SEqu; intros S1 S2 [H12 H21]; split; unfold SInc;
  intros a Ha; inversion_clear Ha; apply RDom_ with (b:=b); [apply H12 | apply H21]; apply H.
Qed.

Theorem SEqu_RRan:forall (A B:Set), SEqu_congr_fun (RRan A B).
Proof.
 intros A B; unfold SEqu_congr_fun; unfold SEqu; intros S1 S2 [H12 H21]; split; unfold SInc;
  intros b Hb; inversion_clear Hb; apply RRan_ with (a:=a); [apply H12 | apply H21]; apply H.
Qed.

Theorem SEqu_RInv:forall (A B:Set), SEqu_congr_fun (RInv A B).
Proof.
 intros A B; unfold SEqu_congr_fun; unfold SEqu; intros S1 S2 [H12 H21]; split; unfold SInc;
  intros [b a] Hba; inversion_clear Hba; apply RInv_; [apply H12 | apply H21]; apply H.
Qed.

Theorem SEqu_RDomR_l:forall (A B:Set)(R:A↔B), SEqu_congr_fun (RDomR A B R).
Proof.
 intros A B; unfold SEqu_congr_fun; unfold SEqu; intros R S1 S2 [H12 H21]; split; unfold SInc;
  intros [a b] Hab; inversion_clear Hab; apply RDomR_;
  [apply H | apply H12; apply H0 | apply H | apply H21; apply H0].
Qed.

Theorem SEqu_RDomR_r:forall (A B:Set)(S:⇑A), SEqu_congr_fun (fun R:A↔B=>RDomR A B R S).
Proof.
 intros A B; unfold SEqu_congr_fun; unfold SEqu; intros S R1 R2 [H12 H21]; split; unfold SInc;
  intros [a b] Hab; inversion_clear Hab; apply RDomR_;
  [apply H12; apply H | apply H0 | apply H21; apply H | apply H0].
Qed.

Theorem SEqu_RRanR_l:forall (A B:Set)(R:A↔B), SEqu_congr_fun (RRanR A B R).
Proof.
 intros A B; unfold SEqu_congr_fun; unfold SEqu; intros R S1 S2 [H12 H21]; split; unfold SInc;
  intros [a b] Hab; inversion_clear Hab; apply RRanR_;
  [apply H | apply H12; apply H0 | apply H | apply H21; apply H0].
Qed.

Theorem SEqu_RRanR_r:forall (A B:Set)(S:⇑B), SEqu_congr_fun (fun R:A↔B=>RRanR A B R S).
Proof.
 intros A B; unfold SEqu_congr_fun; unfold SEqu; intros S R1 R2 [H12 H21]; split; unfold SInc;
  intros [a b] Hab; inversion_clear Hab; apply RRanR_;
  [apply H12; apply H | apply H0 | apply H21; apply H | apply H0].
Qed.

Theorem SEqu_RCmp_l:forall (A B C:Set)(R:A↔B), SEqu_congr_fun (RCmp A B C R).
Proof.
 intros A B C; unfold SEqu_congr_fun; unfold SEqu; intros R R1 R2 [H12 H21]; split; unfold SInc;
  intros [a c] Hac; inversion_clear Hac; apply RCmp_ with (b:=b);
  [apply H | apply H12; apply H0 | apply H | apply H21; apply H0].
Qed.

Theorem SEqu_RCmp_r:forall (A B C:Set)(R:B↔C), SEqu_congr_fun (fun R':A↔B=>RCmp A B C R' R).
Proof.
 intros A B C; unfold SEqu_congr_fun; unfold SEqu; intros R R1 R2 [H12 H21]; split; unfold SInc;
  intros [a c] Hac; inversion_clear Hac; apply RCmp_ with (b:=b);
  [apply H12; apply H | apply H0 | apply H21; apply H | apply H0].
Qed.

Theorem SEqu_RImg_l:forall (A B:Set)(R:A↔B), SEqu_congr_fun (RImg A B R).
Proof.
 intros A B R; unfold SEqu_congr_fun; unfold SEqu; intros S1 S2 [H12 H21]; split; unfold SInc;
  intros b Hb; inversion_clear Hb; apply RImg_ with (a:=a);
  [apply H12; apply H | apply H0 | apply H21; apply H | apply H0].
Qed.

Theorem SEqu_RImg_r:forall (A B:Set)(S:⇑A), SEqu_congr_fun (fun R:A↔B=>RImg A B R S).
Proof.
 intros A B R; unfold SEqu_congr_fun; unfold SEqu; intros S1 S2 [H12 H21]; split; unfold SInc;
  intros b Hb; inversion_clear Hb; apply RImg_ with (a:=a);
  [apply H | apply H12; apply H0 | apply H | apply H21; apply H0].
Qed.

Theorem SEqu_SCart_l:forall (A B:Set)(S:⇑A), SEqu_congr_fun (SCart A B S).
Proof.
 intros A B S; unfold SEqu_congr_fun; unfold SEqu; intros S1 S2 [H12 H21]; split; unfold SInc;
  intros [a b] Hab; inversion_clear Hab; apply SCart_;
  [apply H | apply H12; apply H0 | apply H | apply H21; apply H0].
Qed.

Theorem SEqu_SCart_r:forall (A B:Set)(S:⇑B), SEqu_congr_fun (fun S':⇑A=>SCart A B S' S).
Proof.
 intros A B S; unfold SEqu_congr_fun; unfold SEqu; intros S1 S2 [H12 H21]; split; unfold SInc;
  intros [a b] Hab; inversion_clear Hab; apply SCart_;
  [apply H12; apply H | apply H0 | apply H21; apply H | apply H0].
Qed.

Theorem SEqu_RDPd_l:forall (A B1 B2:Set)(R:A↔B1), SEqu_congr_fun (RDPd A B1 B2 R).
Proof.
 intros A B1 B2 R; unfold SEqu_congr_fun; unfold SEqu; intros R1 R2 [H12 H21]; split; unfold SInc;
  intros [a [b1 b2]] Hab; inversion_clear Hab; apply RDPd_;
  [apply H | apply H12; apply H0 | apply H | apply H21; apply H0].
Qed.

Theorem SEqu_RDPd_r:forall (A B1 B2:Set)(R:A↔B2), SEqu_congr_fun (fun R':A↔B1=>RDPd A B1 B2 R' R).
Proof.
 intros A B1 B2 R; unfold SEqu_congr_fun; unfold SEqu; intros R1 R2 [H12 H21]; split; unfold SInc;
  intros [a [b1 b2]] Hab; inversion_clear Hab; apply RDPd_;
  [apply H12; apply H | apply H0 | apply H21; apply H | apply H0].
Qed.

Theorem SEqu_RPPd_l:forall (A1 A2 B1 B2:Set)(R:A1↔B1), SEqu_congr_fun (RPPd A1 A2 B1 B2 R).
Proof.
 intros A1 A2 B1 B2 R; unfold SEqu_congr_fun; unfold SEqu; intros R1 R2 [H12 H21]; split;
  unfold SInc; intros [[a1 a2] [b1 b2]] Hab; inversion_clear Hab; apply RPPd_;
  [apply H | apply H12; apply H0 | apply H | apply H21; apply H0].
Qed.

Theorem SEqu_RPPd_r:forall (A1 A2 B1 B2:Set)(R:A2↔B2),
                    SEqu_congr_fun (fun R':A1↔B1=>RPPd A1 A2 B1 B2 R' R).
Proof.
 intros A1 A2 B1 B2 R; unfold SEqu_congr_fun; unfold SEqu; intros R1 R2 [H12 H21]; split;
  unfold SInc; intros [[a1 a2] [b1 b2]] Hab; inversion_clear Hab; apply RPPd_;
  [apply H12; apply H | apply H0 | apply H21; apply H | apply H0].
Qed.

(*================================================================================================*)