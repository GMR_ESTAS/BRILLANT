(*==================================================================================================
  Project : Study of the concept of refinement
  Module : choice_ref_pre
  We study the concept of choice-refinement, the elimination of non-determinism. In this approach, a
  specification is a relation between a domain and a range, and a refinement is any relation
  included in the previous one and covering the domain of specification. The objective is, through
  one or several refinements, to progress from a relation toward a function.
  Our definition being too simplistic for now, we capture the concept of preconditions, but not of
  guards.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: February 2007, Md: March 2008
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export relation_gen.

(* Choice-refinement definition ------------------------------------------------------------------*)

Definition ChPRef(dom ran:Set)(Sa Sc:dom↔ran):=∏1(Sa)⊆∏1(Sc) /\ (∏1(Sa)⊲Sc)⊆Sa.
Notation "Sa '≻' Sc":=(ChPRef _ _ Sa Sc) (no associativity, at level 90).
(* Nota: ∏1(Sa)⊆∏1(Sc) ensures that the refinement addresses all the specification and Sc⊆Sa that
   the refinement is compliant. The restriction to ∏1(Sa) allows to refine with a completed (up to
   total) relation from a partial specification; for elements not specified, refinement is  free. *)

(* Choice-refinement properties ------------------------------------------------------------------*)

Theorem SEqu_ChPRef_l:forall (dom ran:Set)(S:dom↔ran), SEqu_congr_prop (ChPRef dom ran S).
Proof.
 intros dom ran S; unfold SEqu_congr_prop; unfold ChPRef; intros S1 S2 HEq [HD1 HR1]; split.
  apply (SEqu_SInc_l _ ∏1(S) ∏1(S1) ∏1(S2)); [apply SEqu_RDom; apply HEq | apply HD1].
  apply (SEqu_SInc_r _ S (∏1(S)⊲S1) (∏1(S)⊲S2));
   [apply (SEqu_RDomR_r _ _ ∏1(S) S1 S2); apply HEq | apply HR1].
Qed.

Theorem SEqu_ChPRef_r:forall (dom ran:Set)(S:dom↔ran),
                      SEqu_congr_prop (fun S':dom↔ran=>ChPRef dom ran S' S).
Proof.
 intros dom ran S; unfold SEqu_congr_prop; unfold ChPRef; intros S1 S2 HEq [HD1 HR1]; split.
  apply (SEqu_SInc_r _ ∏1(S) ∏1(S1) ∏1(S2)); [apply (SEqu_RDom _ _ S1 S2); apply HEq | apply HD1].
  apply (SEqu_SInc_l _ (∏1(S2)⊲S) _ _ HEq); apply (SEqu_SInc_r _ S1 (∏1(S1)⊲S) (∏1(S2)⊲S));
   [apply SEqu_RDomR_l; apply SEqu_RDom; apply HEq | apply HR1].
Qed.

Theorem ChPRef_refl:forall (dom ran:Set)(S:dom↔ran), S≻S. (* Reflexivity *)
Proof.
 intros dom ran S; unfold ChPRef; split; [apply SInc_refl | apply (proj1 (RDomR_self dom ran S))].
Qed.

Theorem ChPRef_asym:forall (dom ran:Set)(S1 S2:dom↔ran),  S1≻S2->S2≻S1->S1≡S2.
Proof.
 intros dom ran S1 S2; unfold ChPRef; unfold SEqu; unfold SInc; intros [Hd12 Hi12] [Hd21 Hi21];
  split; intros [d r] Hdr.
  assert (Hd:∏1(S1) d). apply RDom_ with (b:=r); apply Hdr.
  apply Hi21; apply RDomR_; [apply Hdr | apply Hd12; apply Hd].
  assert (Hd:∏1(S2) d). apply RDom_ with (b:=r); apply Hdr.
  apply Hi12; apply RDomR_; [apply Hdr | apply Hd21; apply Hd].
Qed.

Theorem ChPRef_tran:forall (dom ran:Set)(S1 S2 S3:dom↔ran), S1≻S2->S2≻S3->S1≻S3.
Proof.
 intros dom ran S1 S2 S3; unfold ChPRef; unfold SInc; intros [Hd12 Hr21] [Hd23 Hr23]; split;
  intros d Hd.
   apply Hd23; apply Hd12; apply Hd.
   induction d as [d r]; inversion_clear Hd; apply Hr21; apply RDomR_.
    apply Hr23; apply RDomR_; [apply H | apply Hd12; apply H0].
    apply H0.
Qed.

Theorem ChPRef_mon:forall (dom int ran:Set)(Sa Sc:dom↔int)(Sa' Sc':int↔ran),
                   Sa⊳∏1(Sa')≻Sc->∏2(Sa)⊲Sa'≻Sc'->Sa'∘Sa≻Sc'∘Sc.
Proof.
 intros dom int ran Sa Sc Sa' Sc' [HD HR] [HD' HR'];
  apply (SEqu_ChPRef_r _ _ (Sc'∘Sc) ((∏2(Sa)⊲Sa')∘(Sa⊳∏1(Sa'))) (Sa'∘Sa)).
  apply SEqu_sym; apply RCmp_RDomR_RRanR.
  unfold ChPRef; unfold SInc; split.
   intros d Hd; inversion_clear Hd; rename b into r; inversion_clear H; rename b into i.
   generalize (HD _ (RDom_ _ _ _ _ _ H0)); intro Hd.
   inversion_clear Hd; rename b into i'.
   generalize (HR _ (RDomR_ _ _ _ _ _ _ H (RDom_ _ _ _ _ _ H0))); intros Hd.
   inversion_clear Hd; inversion_clear H3; rename b into r'.
   generalize (RDomR_ _ _ _ _ _ _ H4 (RRan_ _ _ _ _ _ H2)); intros Hd.
   generalize (HD' _ (RDom_ _ _ _ _ _ Hd)); intros Hd'.
   inversion_clear Hd'; rename b into r''.
   apply (RDom_ _ _ (Sc'∘Sc) d r'').
   apply RCmp_ with (b:=i'); [apply H | apply H3].
   intros [d r] Hdr; inversion_clear Hdr; inversion_clear H; rename b into i.
   inversion_clear H0; rename b into r'; inversion_clear H; rename b into i'.
   generalize (HR _ (RDomR_ _ _ _ _ _ _ H1 (RDom_ _ _ _ _ _ H0))); intros Hd.
   inversion_clear Hd; inversion_clear H4; rename b into r''.
   generalize (RDom_ _ _ _ _ _ (RDomR_ _ _ _ _ _ _ H5 (RRan_ _ _ _ _ _ H))); intros Hd.
   generalize (HR' _ (RDomR_ _ _ _ _ _ _ H2 Hd)); clear Hd; intros Hd.
   apply RCmp_ with (b:=i).
    apply RRanR_; [apply H | apply RDom_ with (b:=r''); apply H5].
    apply Hd.
Qed.
(* Nota: (R2∘R1)≡(∏2(R1)⊲R2)∘(R1⊳∏1(R2)) therefore refining the composition of two specifications is
   equivalent to refining their restrictions. Yet this is an optimal approach, as refining the
   restriction of Sa is required (to avoid refining to dead ends), and refining the restriction of
   Sa' is easier (part of the specification is just forgotten). *)

Theorem SInc_ChPRef:forall (dom ran:Set)(Sa Sc:dom↔ran), Sc⊆Sa->∏1(Sc)⊲Sa≻Sc.
Proof.
 intros dom ran Sa Sc HInc; unfold ChPRef; split.
  intros d Hd; inversion_clear Hd; rename b into r; inversion_clear H; apply H1.
  intros [d r] Hdr; inversion_clear Hdr; apply RDomR_; [apply HInc | apply RDom_ with (b:=r)];
   apply H.
Qed.

Theorem RFnc_ChPRef:forall (dom ran:Set)(Sa Sc:dom↔ran), RFnc Sa->Sa≻Sc->∏1(Sa)⊲Sc≡Sa.
Proof.
 intros dom ran Sa Sc; unfold RFnc; intros HFnc [HD HR]; unfold SEqu; split.
  apply HR.
  intros [d r] Hdr.
  generalize (RDom_ _ _ _ _ _ Hdr); intros HDSa.
  generalize (HD _ HDSa); intros H; inversion_clear H; rename b into r'.
  generalize (HR _ (RDomR_ _ _ _ _ _ _ H0 HDSa)); intros Hdr'.
  rewrite (HFnc d r r' Hdr Hdr') in *.
  apply RDomR_; [apply H0 | apply HDSa].
Qed.

Theorem Final_ChPRef:forall (dom ran:Set)(Sa Sc:dom↔ran), RFnc Sa->RTot Sa->Sa≻Sc->Sc≡Sa.
Proof.
 intros dom ran Sa Sc HFnc HTot HRef.
 assert (HEq:Sc≡(∏1(Sa)⊲Sc)).
  unfold SEqu; unfold SInc; split; intros [d r] Hdr.
   apply RDomR_; [apply Hdr | apply HTot; apply SFul_].
   inversion_clear Hdr; apply H.
 apply SEqu_trans with (S2:=(∏1(Sa)⊲Sc)).
  apply HEq.
  apply RFnc_ChPRef; [apply HFnc | apply HRef].
Qed.

Section ChPRef_Bd.
 Variable dom ran:Set.
 Variable Sa:dom↔ran.
 Variable Choice:⇑ran->ran.
 Variable Choice_spec:forall R:⇑ran, (exists r:ran, R r)->R(Choice R).
 Definition Sc(d:dom):ran:=Choice(Sa@⊙(d)).
 Theorem Sc_ChPRef:Sa≻(⇔Sc).
 Proof.
  unfold ChPRef; unfold SInc; split.
   intros d _; apply RDom_ with (b:=Sc d); apply RofF_.
   intros [d r] Hdr; inversion_clear Hdr; inversion_clear H; clear r.
   assert (Hex:exists r:ran, (Sa@⊙(d)) r).
    inversion_clear H0; rename b into r; exists r; apply RImg_ with (a:=d).
     apply SSin_.
     apply H.
   generalize (Choice_spec (Sa@⊙(d)) Hex); clear H0 Hex; intros HSa.
   inversion_clear HSa.
   inversion H; rewrite H1 in *.
   apply H0.
 Qed.
End ChPRef_Bd.

(*================================================================================================*)