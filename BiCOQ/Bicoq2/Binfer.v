(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Binfer, inference rules
   The inference rules of the logical system of the B-Book are described in this module,
   first only the propositional calculus rules (for which some of the rules defined by
   Abrial are proved to be, in fact, theorems), then the whole set of predicate calculus
   rules. A first set of theorems is provided, to be used in further proofs.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - October 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Bgamma.

(* Inference rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Inference rules are described through a inductive type; basically, each proof (or     *)
(* proposal) defines a type inhabiting the sort Set ; the set of all such types defines  *)
(* the set of all (valid) B proofs.                                                                                                  *)
(* The first inductive definition is limited to the propositional calculus proofs,      *)
(* while the second describes the predicate calculus proofs. In order to be as close     *)
(* as possible to the B-Book definitions, the rules related to binders are defined        *)
(* using in some cases functions on terms, rather than constructors (e.g. bifori).            *)
(* Finally, some rules, either for correction or simplification, have been added. For    *)
(* example the BIG rule allowing to derive infinite(♄) from ∅☑, using the yet undefined  *)
(* infinite property, is replaced by several rules indicating that the expressions ωi *)
(* provide a (numberable) set of members of ♄. Another example is the introduction of     *)
(* additional rules about maplet decomposition...                                                                    *)

 (* Propositional calculus inference rules *)
 Inductive Bpinf:☐->☊->Set:=
 | bpiin:forall (G:☐)(P:☊), P∈☑G->Bpinf G P
 | bpiinc:forall (H G:☐)(P:☊), Bpinf G P->G⊆☑H->Bpinf H P
 | bpiandi:forall (G:☐)(P Q:☊), Bpinf G P->Bpinf G Q->Bpinf G (P∧Q)
 | bpiandl:forall (G:☐)(P Q:☊), Bpinf G (P∧Q)->Bpinf G P
 | bpiandr:forall (G:☐)(P Q:☊), Bpinf G (P∧Q)->Bpinf G Q
 | bpiimpi:forall (G:☐)(P Q:☊), Bpinf (G☑★P) Q->Bpinf G (P☲Q)
 | bpiimpe:forall (G:☐)(P Q:☊), Bpinf G (P☲Q)->Bpinf (G☑★P) Q
 | bpiabsn:forall (G:☐)(P Q:☊), Bpinf (G☑★¬Q) P->Bpinf (G☑★¬Q) (¬P)->Bpinf G Q
 | bpiabsp:forall (G:☐)(P Q:☊), Bpinf (G☑★Q) P->Bpinf (G☑★Q) (¬P)->Bpinf G (¬Q).
 Notation "G '⊣☋' P" := (Bpinf G P) (at level 70).
 Notation "'⊣☋' P" := (Bpinf ∅☑ P) (at level 70).

 (* Predicate calculus inference rules *)
 Inductive Binf:☐->☊->Set:=
 | biin:forall (G:☐)(P:☊), P∈☑G->Binf G P
 | biinc:forall (H G:☐)(P:☊), Binf G P->G⊆☑H->Binf H P
 | biandi:forall (G:☐)(P Q:☊), Binf G P->Binf G Q->Binf G (P∧Q)
 | biandl:forall (G:☐)(P Q:☊), Binf G (P∧Q)->Binf G P
 | biandr:forall (G:☐)(P Q:☊), Binf G (P∧Q)->Binf G Q
 | biimpi:forall (G:☐)(P Q:☊), Binf (G☑★P) Q->Binf G (P☲Q)
 | biimpe:forall (G:☐)(P Q:☊), Binf G (P☲Q)->Binf (G☑★P) Q
 | biabsn:forall (G:☐)(P Q:☊), Binf (G☑★¬Q) P->Binf (G☑★¬Q) (¬P)->Binf G Q
 | biabsp:forall (G:☐)(P Q:☊), Binf (G☑★Q) P->Binf (G☑★Q) (¬P)->Binf G (¬Q)
 | bifori:forall (G:☐)(f:☈)(P:☊), f⍀☑G->Binf G P->Binf G ☬☢(f∙P)
 | bifore:forall (G:☐)(f:☈)(P:☊)(E:☌), Binf G ☬☢(f∙P)->Binf G 〈f♇☍E@☋P〉
 | biequi:forall (G:☐)(E:☌), Binf G (E♃E)
 | bieque:forall (G:☐)(f:☈)(P:☊)(E F:☌), Binf G E♃F->Binf G 〈f♇☍E@☋P〉->Binf G 〈f♇☍F@☋P〉
 | bipro:forall (G:☐)(f1 f2:☈)(E S T:☌), f1⍀☋(E∈(S♂T))->!(f1≡f2)->f2⍀☋(E∈(S♂T))->
              Binf G (☬☣(f1∙☬☣(f2∙χ(f1)∈S ∧ χ(f2)∈T ∧ E♃(χ(f1)☵χ(f2))))☳(E∈(S♂T)))
 | bipow:forall (G:☐)(f:☈)(S T:☌), f⍀☍S->f⍀☍T->Binf G (☬☢(f∙χ(f)∈S☲χ(f)∈T)☳(S∈(☬T)))
 | bicmp:forall (G:☐)(f:☈)(P:☊)(E S:☌), Binf G ((E∈S ∧ 〈f♇☍E@☋P〉)☳(E∈ ☬☤(f∙S∙P)))
 | bieqsi:forall (G:☐)(S T:☌), Binf G S∈(☬T)->Binf G T∈(☬S)->Binf G S♃T
 | bichsi:forall (G:☐)(f:☈)(S:☌), f⍀☍S->Binf G ☬☣(f∙χ(f)∈S)->Binf G (☭S) ∈ S
 | bibigi:forall (G:☐)(f:☈), Binf G ω(f)∈ ♄
 | bibied:forall (G:☐)(f g:☈), !(f≡g)->Binf G ω(f)♃☃ω(g)
 | bicpll:forall (G:☐)(E F E' F':☌), Binf G (E☵F)♃(E'☵F')->Binf G E♃E'
 | bicplr:forall (G:☐)(E F E' F':☌), Binf G (E☵F)♃(E'☵F')->Binf G F♃F'.
 Notation "G '⊣' P" := (Binf G P) (at level 70).
 Notation "'⊣' P" := (Binf ∅☑ P) (at level 70).
 (* Nota: rather than introducing two additional rules bicpll and bicplr to correct      *)
 (* the proof of B-Book theorem 1.5.5, p51, it is also possible to define constructors     *)
 (* Proj1 and Proj2 for expressions and to associate adequate semantic through the       *)
 (* rules here. One may also consider a direct definition of such projections, e.g.          *)
 (* Proj1(S,T)(E☵F)=☭(☬☤(x∙x∈S∧☬☣(y∙y∈T∧x☵y♃E☵F))), but this definition does not help as        *)
 (* while E is in the defined comprehension set, there may be other elements...                  *)

(* Relationship between propositional calculus proofs and predicate calculus proofs *)

 Theorem proptopred:forall (G:☐)(P:☊), G⊣☋P->G⊣P.
 Proof.
  intros G P H; induction H.
   apply biin; apply l.
   apply biinc with (G:=G); [apply IHBpinf | apply l].
   apply biandi; [apply IHBpinf1 | apply IHBpinf2].
   apply biandl with (Q:=Q); apply IHBpinf.
   apply biandr with (P:=P); apply IHBpinf.
   apply biimpi; apply IHBpinf.
   apply biimpe; apply IHBpinf.
   apply biabsn with (P:=P); [apply IHBpinf1 | apply IHBpinf2].
   apply biabsp with (P:=P); [apply IHBpinf1 | apply IHBpinf2].
 Defined.
 Implicit Arguments proptopred [G P].
 (* Nota: proptopred is not Qed-ed but Defined in order to ensure it is non-opaque ; it   *)
 (* can therefore be uses as a function rewriting propositional calculus proofs into   *)
 (* predicate calculus.                                                                                                                    *)

 Fixpoint fiprop(G:☐)(P:☊)(H:G⊣P){struct H}:☆:=
  match H with
   | biin _ _ _ => ⊤
   | biinc _ G' P' H' _ => fiprop G' P' H'
   | biandi G' P1 P2 H1 H2 => (fiprop G' P1 H1)∧☁(fiprop G' P2 H2)
   | biandl G' P1 P2 H' => fiprop G' (P1∧P2) H'
   | biandr G' P1 P2 H' => fiprop G' (P1∧P2) H'
   | biimpi G' P1 P2 H' => fiprop (G'☑★P1) P2 H'
   | biimpe G' P1 P2 H' => fiprop G' (P1☲P2) H'
   | biabsn G' P1 P2 H1 H2 => (fiprop (G'☑★¬P2) P1 H1)∧☁(fiprop (G'☑★¬P2) (¬P1) H2)
   | biabsp G' P1 P2 H1 H2 => (fiprop (G'☑★P2) P1 H1)∧☁(fiprop (G'☑★P2) (¬P1) H2)
   | _ => ⊥
  end.
 Implicit Arguments fiprop [G P].

 Theorem fipropproptopred:forall (G:☐)(P:☊)(H:G⊣☋P), fiprop (proptopred H)≡⊤.
 Proof.
  intros G P; induction H; simpl; try (apply IHBpinf);
   try (rewrite IHBpinf1; rewrite IHBpinf2); apply refl_id.
 Qed.

 Theorem pinffiprop:forall (G:☐)(P:☊)(H:G⊣P), fiprop H≡⊤->G⊣☋P.
 Proof.
  intros G P; induction H; simpl; intros Hp;
   try (invset Hp; discriminate H || discriminate H0 ||  discriminate H1).
   apply (bpiin _ _ l).
   apply (bpiinc H _ _ (IHBinf Hp) l).
   destruct (dedfbandt _ _ Hp); apply (bpiandi _ _ _ (IHBinf1 i) (IHBinf2 i0)).
   apply (bpiandl _ _ _ (IHBinf Hp)).
   apply (bpiandr _ _ _ (IHBinf Hp)).
   apply (bpiimpi _ _ _ (IHBinf Hp)).
   apply (bpiimpe _ _ _ (IHBinf Hp)).
   destruct (dedfbandt _ _ Hp); apply (bpiabsn _ _ _ (IHBinf1 i) (IHBinf2 i0)).
   destruct (dedfbandt _ _ Hp); apply (bpiabsp _ _ _ (IHBinf1 i) (IHBinf2 i0)).
 Defined.
 (* Nota: again, pinffiprop is not Qed-ed but Defined in order to use it as a function     *)
 (* rewriting predicate calculus proofs into propositional calculus.                               *)

(* B-Book inference rules appearing to be theorems... ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Lemma bpiaxm:forall (G:☐)(P:☊), G☑★P⊣☋P.
 Proof.
  intros G P; apply bpiin; apply giyes.
 Qed.

 Lemma biaxm:forall (G:☐)(P:☊), G☑★P⊣P.
 Proof.
  intros G P; apply proptopred; apply bpiaxm.
 Qed.

 Lemma bpiimp:forall (G:☐)(P Q:☊), G⊣☋P->G☑★P⊣☋Q->G⊣☋Q.
 Proof.
  intros G P Q HP HQ; apply bpiabsn with (P:=P).
   apply bpiinc with (G:=G); [apply HP | apply gincover; apply reflginc].
   apply bpiabsp with (P:=Q).
    apply bpiinc with (G:=G☑★P); [apply HQ | apply gincdbl; apply gincover; apply reflginc ].
    apply bpiinc with (G:=G☑★¬Q); [apply bpiaxm | apply gincover; apply reflginc].
 Qed.
 
 Lemma biimp:forall (G:☐)(P Q:☊), G⊣P->G☑★P⊣Q->G⊣Q.
 Proof.
  intros G P Q HP HQ; apply biabsn with (P:=P).
   apply biinc with (G:=G); [apply HP | apply gincover; apply reflginc].
   apply biabsp with (P:=Q).
    apply biinc with (G:=G☑★P); [apply HQ | apply gincdbl; apply gincover; apply reflginc ].
    apply biinc with (G:=G☑★¬Q); [apply biaxm | apply gincover; apply reflginc].
 Qed.
 (* Nota: bpiimp and biimp therefore prove the cut-elimination principle for B proofs. *)

(* Lemmas to ease further proofs ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem bpiins:forall (G:☐)(P Q:☊), G⊣☋P->G☑★Q⊣☋P.
 Proof.
  intros G P Q H; apply bpiinc with (G:=G); [apply H | apply gincover; apply reflginc].
 Qed.

 Theorem biins:forall (G:☐)(P Q:☊), G⊣P->G☑★Q⊣P.
 Proof.
  intros G P Q H; apply biinc with (G:=G); [apply H | apply gincover; apply reflginc].
 Qed.

 (* Tactic to prove G☑★_...☑★P☑★_...⊣P *)
 Ltac BThyp:=repeat (apply biaxm || apply biins).
 Ltac BThyp':=repeat (apply bpiaxm || apply bpiins).

 Theorem bpirem:forall (G:☐)(P Q:☊), G-☑Q⊣☋P->G⊣☋P.
 Proof.
  induction G; simpl; intros P Q H.
   apply H.
   destruct (decpequ Q d).
    rewrite <- (sym_id (pequpt _ _ i)) in H; apply bpiins; apply (IHG P Q); apply H.
    rewrite <- (sym_id (pequrf _ _ e)) in H; apply bpiimpe; apply IHG with (Q:=Q);
     apply bpiimpi; apply H.
 Qed.

 Theorem birem:forall (G:☐)(P Q:☊), G-☑Q⊣P->G⊣P.
 Proof.
  induction G; simpl; intros P Q H.
   apply H.
   destruct (decpequ Q d).
    rewrite <- (sym_id (pequpt _ _ i)) in H; apply biins; apply (IHG P Q); apply H.
    rewrite <- (sym_id (pequrf _ _ e)) in H; apply biimpe; apply IHG with (Q:=Q); apply biimpi;
     apply H.
 Qed.

 Theorem bpiemp:forall (G:☐)(P:☊), ⊣☋P->G⊣☋P.
 Proof.
  induction G; intros P H; [idtac | apply bpiins; apply IHG]; apply H.
 Qed.

 Theorem biemp:forall (G:☐)(P:☊), ⊣P->G⊣P.
 Proof.
  induction G; intros P H; [idtac | apply biins; apply IHG]; apply H.
 Qed.

(* Splitting of definition theorems ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem biproi:forall (G:☐)(f1 f2:☈)(E S T:☌),
                             f1⍀☋(E∈(S♂T))->!(f1≡f2)->f2⍀☋(E∈(S♂T))->
                             G⊣ ☬☣(f1∙☬☣(f2∙χ(f1)∈S ∧ χ(f2)∈T ∧ E♃(χ(f1)☵χ(f2))))->G⊣E∈(S♂T).
 Proof.
  intros G f1 f2 E S T Hf1 Hfd Hf2 H; apply (biimp _ _ (E∈(S♂T)) H); apply biimpe.
  apply biandl with (Q:=E∈(S♂T)☲☬☣(f1∙☬☣(f2∙χ(f1)∈S ∧ χ(f2)∈T ∧ E♃(χ(f1)☵χ(f2))))).
  fold (piff ☬☣(f1∙☬☣(f2∙χ(f1)∈S ∧ χ(f2)∈T ∧ E♃(χ(f1)☵χ(f2)))) E∈(S♂T)); apply bipro; assumption.
 Qed.

 Theorem biproe:forall (G:☐)(f1 f2:☈)(E S T:☌),
                             f1⍀☋(E∈(S♂T))->!(f1≡f2)->f2⍀☋(E∈(S♂T))->
                             G⊣E∈(S♂T)->G⊣ ☬☣(f1∙☬☣(f2∙χ(f1)∈S ∧ χ(f2)∈T ∧ E♃(χ(f1)☵χ(f2)))).
 Proof.
  intros G f1 f2 E S T Hf1 Hfd Hf2 H;
   apply (biimp _ _ ☬☣(f1∙☬☣(f2∙χ(f1)∈S ∧ χ(f2)∈T ∧ E♃(χ(f1)☵χ(f2)))) H); apply biimpe.
  apply biandr with (P:=☬☣(f1∙☬☣(f2∙χ(f1)∈S ∧ χ(f2)∈T ∧ E♃(χ(f1)☵χ(f2))))☲E∈(S♂T)).
  fold (piff ☬☣(f1∙☬☣(f2∙χ(f1)∈S ∧ χ(f2)∈T ∧ E♃(χ(f1)☵χ(f2)))) E∈(S♂T)); apply bipro; assumption.
 Qed.

 Theorem bipowi:forall (G:☐)(f:☈)(S T:☌), f⍀☍S->f⍀☍T->G⊣ ☬☢(f∙χ(f)∈S☲χ(f)∈T)->G⊣S∈(☬T).
 Proof.
  intros G f S T HS HT H; apply (biimp _ _ (S∈(☬T)) H); apply biimpe.
  apply biandl with (Q:=S∈(☬T)☲ ☬☢(f∙χ(f)∈S☲χ(f)∈T)).
  fold (piff ☬☢(f∙χ(f)∈S☲χ(f)∈T) (S∈(☬T))); apply bipow; assumption.
 Qed.

 Theorem bipowe:forall (G:☐)(f:☈)(S T:☌), f⍀☍S->f⍀☍T->G⊣S∈(☬T)->G⊣ ☬☢(f∙χ(f)∈S☲χ(f)∈T).
 Proof.
  intros G f S T HS HT H; apply (biimp _ _ ☬☢(f∙χ(f)∈S☲χ(f)∈T) H); apply biimpe.
  apply biandr with (P:= ☬☢(f∙χ(f)∈S☲χ(f)∈T)☲S∈(☬T)).
  fold (piff ☬☢(f∙χ(f)∈S☲χ(f)∈T) (S∈(☬T))); apply bipow; assumption.
 Qed.

 Theorem bicmpi:forall (G:☐)(f:☈)(P:☊)(E S:☌), G⊣E∈S->G⊣ 〈f♇☍E@☋P〉->G⊣E∈ ☬☤(f∙S∙P).
 Proof.
  intros G f P E S H H0; apply (biimp _ _ (E∈ ☬☤(f∙S∙P)) (biandi _ _ _ H H0)); apply biimpe.
  apply biandl with (Q:=E∈ ☬☤(f∙S∙P)☲(E∈S∧ 〈f♇☍E@☋P〉)).
  fold (piff (E∈S∧ 〈f♇☍E@☋P〉) (E∈ ☬☤(f∙S∙P))); apply bicmp.
 Qed.

 Theorem bicmpl:forall (G:☐)(f:☈)(P:☊)(E S:☌), G⊣E∈ ☬☤(f∙S∙P)->G⊣E∈S.
 Proof.
  intros G f P E S H; apply biandl with (Q:=〈f♇☍E@☋P〉); apply (biimp _ _ (E∈S∧ 〈f♇☍E@☋P〉) H);
   apply biimpe.
  apply biandr with (P:=(E∈S∧ 〈f♇☍E@☋P〉)☲E∈ ☬☤(f∙S∙P)).
  fold (piff (E∈S∧ 〈f♇☍E@☋P〉) (E∈ ☬☤(f∙S∙P))); apply bicmp.
 Qed.

 Theorem bicmpr:forall (G:☐)(f:☈)(P:☊)(E S:☌), G⊣E∈ ☬☤(f∙S∙P)->G⊣ 〈f♇☍E@☋P〉.
 Proof.
  intros G f P E S H; apply biandr with (P:=E∈S); apply (biimp _ _ (E∈S∧ 〈f♇☍E@☋P〉) H);
   apply biimpe.
  apply biandr with (P:=(E∈S∧ 〈f♇☍E@☋P〉)☲E∈ ☬☤(f∙S∙P)).
  fold (piff (E∈S∧ 〈f♇☍E@☋P〉) (E∈ ☬☤(f∙S∙P))); apply bicmp.
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
