; Fichier de dependance pour le project BiCoq
; Syntaxe : X Y1 ... Yn pour produire X.vo � partir de X.v Y1 ... Yn
Mscratch
Mbool compiled\Mscratch.vo
Mlist compiled\Mbool.vo
Mindex compiled\Mbool.vo
Bterm compiled\Mindex.vo
Blift compiled\Bterm.vo
Binst compiled\Blift.vo
Bbind compiled\Binst.vo
Baffec compiled\Bbind.vo
Btrmind compiled\Baffec.vo
Bgamma compiled\Btrmind.vo compiled\Mlist.vo
Binfer compiled\Bgamma.vo
Pinfer compiled\Binfer.vo compiled\Mlist.vo
Binfind compiled\Pinfer.vo
Bprop compiled\Binfind.vo
Pprop compiled\Bprop.vo
Bcoqinfer compiled\Pprop.vo
Binfer2 compiled\Bcoqinfer.vo
Pinfer2 compiled\Binfer2.vo
Biffres compiled\Pinfer2.vo
Bdbinfer compiled\Biffres.vo
Baffprd compiled\Bdbinfer.vo
Paffprd compiled\Baffprd.vo
Bmaffec compiled\Paffprd.vo compiled\Mlist.vo
Brawprd compiled\Bmaffec.vo
Prawprd compiled\Brawprd.vo
Bpred compiled\Prawprd.vo
Ppred compiled\Bpred.vo
Bset compiled\Ppred.vo
Pset compiled\Bset.vo
