(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Blift, terms lifting
   Definition of the lifting operations, i.e. increasing or decreasing free indexes
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - September 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Bterm.

(* Uplift function (increment floating indexes), local version ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fpuploc(d:☈)(P:☊){struct P}:☊:=
  match P with
  | ¬P' => ¬ (fpuploc d P')
  | ∀P' => ∀ (fpuploc ☠d P')
  | P1∧P2 => (fpuploc d P1) ∧ (fpuploc d P2)
  | P1☲P2 => (fpuploc d P1) ☲ (fpuploc d P2)
  | π i => π i
  | E1♃E2 => (feuploc d E1) ♃ (feuploc d E2)
  | E1∈E2 => (feuploc d E1) ∈ (feuploc d E2)
  end
 with feuploc(d:☈)(E:☌){struct E}:☌:=
  match E with
  | ♄ => ♄
  | ☭E' => ☭ (feuploc d E')
  | ☬E' => ☬ (feuploc d E')
  | (E1☵E2) => (feuploc d E1☵feuploc d E2)
  | (E1♂E2) => (feuploc d E1♂feuploc d E2)
  | ω i => ω i
  | χ i => χ (match d≤☉☂i with ⊤ => ☠i | ⊥ => i end)
  | E'♀ P' => (feuploc d E')♀ (fpuploc ☠d P')
  end.

 (* fpuploc and feuploc increment the free variables *)
 Theorem ftuplocidx:(forall (P:☊)(i d:☈), d≤☉i->☠i⍀☋☂(fpuploc d P)≡i⍀☋☂P)*
                                     (forall (E:☌)(i d:☈), d≤☉i->☠i⍀☍☂(feuploc d E)≡i⍀☍☂E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (i d:☈), d≤☉i->☠i⍀☋☂(fpuploc d P)≡i⍀☋☂P)
                             (fun E:☌=>forall (i d:☈), d≤☉i->☠i⍀☍☂(feuploc d E)≡i⍀☍☂E)); simpl.
  intros i d H; apply refl_id.
  intros i; split; [split | idtac]; intros j d H; try (apply refl_id).
   destruct (decige d i).
    rewrite (igept _ _ i0); simpl; apply refl_id.
    rewrite (igerf _ _ e); simpl; generalize (nigeigt _ _ e); intros H0;
     generalize (transigtige _ _ _ H0 H); intros H1; destruct (deciequ j i).
     rewrite <- i0 in H1; destruct (nrefligt _ H1).
     rewrite (iequrf _ _ e0); simpl; destruct i.
      apply refl_id.
      destruct (deciequ j i).
       rewrite <- i0 in H1; destruct (nrefligt _ (nxtrigt _ _ H1)).
       rewrite (iequrf _ _ e1); apply refl_id.
  intros P H; split; intros i d H0; [apply (H _ _ H0) | apply (H _ _ (genxt _ _ H0))].
  intros E H; split; intros i d H0; apply (H _ _ H0).
  intros P1 P2 H1 H2; split; intros i d H0; rewrite (H1 _ _ H0); rewrite (H2 _ _ H0);
   apply refl_id.
  intros E1 E2 H1 H2; split; split; intros i d H0; rewrite (H1 _ _ H0); rewrite (H2 _ _ H0);
   apply refl_id.
  intros E P H1 H2 i d H0; rewrite (H1 _ _ H0); rewrite (H2 _ _ (genxt _ _ H0)); apply refl_id.
 Qed.
 (* Nota: this is an example of the use of the implementation principle to prove by         *)
 (* reflection, i.e. using (automatic) computations instead of (manual) proof. That's why *)
 (* this theorem is expressed using functions, while the logic version are provided      *)
 (* thereafter.                                                                                                                                   *)

 Definition fpuplocidx:=fst ftuplocidx.
 Definition feuplocidx:=snd ftuplocidx.

 Theorem fpuplocinf:forall (P:☊)(i d:☈), d≤☉i->i⍀☋P->☠i⍀☋fpuploc d P.
 Proof.
  intros P i d H H0; generalize (fpuplocidx P _ _ H); intros H1; apply pinftp; rewrite H1;
   apply (pinfpt _ _ H0).
 Qed.

 Theorem fpuplocifr:forall (P:☊)(i d:☈), d≤☉i->!(i⍀☋P)->!(☠i⍀☋fpuploc d P).
 Proof.
  intros P i d H H0; generalize (fpuplocidx P _ _ H); intros H1; apply pinffr; rewrite H1;
   apply (pinfrf _ _ H0).
 Qed.

 Theorem feuplocinf:forall (E:☌)(i d:☈), d≤☉i->i⍀☍E->☠i⍀☍feuploc d E.
 Proof.
  intros E i d H H0; generalize (feuplocidx E _ _ H); intros H1; apply einftp; rewrite H1;
   apply (einfpt _ _ H0).
 Qed.

 Theorem feuplocifr:forall (E:☌)(i d:☈), d≤☉i->!(i⍀☍E)->!(☠i⍀☍feuploc d E).
 Proof.
  intros E i d H H0; generalize (feuplocidx E _ _ H); intros H1; apply einffr; rewrite H1;
   apply (einfrf _ _ H0).
 Qed.

 (* fpuploc and feuploc are non dependant over their depth *)
 Theorem ftuplocinfdep:(forall (P:☊)(i:☈), i⍀☋☂fpuploc i P≡⊤)*
                                           (forall (E:☌)(i:☈), i⍀☍☂feuploc i E≡⊤).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (i:☈), i⍀☋☂fpuploc i P≡⊤)
                             (fun E:☌=>forall (i:☈), i⍀☍☂feuploc i E≡⊤)); simpl.
   intros; apply refl_id.
   intros i; split; [split | idtac]; intros j; try (apply refl_id); destruct (decige j i).
    rewrite (igept _ _ i0); simpl; destruct (deciequ j ☠i);
     [rewrite <- (sym_id i1) in i0; destruct (nreflnige _ i0)
     | rewrite (iequrf _ _ e); apply refl_id].
    rewrite (igerf _ _ e); simpl; destruct (deciequ j i);
     [rewrite <- (sym_id i0) in e; destruct e; apply reflige
     | rewrite (iequrf _ _ e0); apply refl_id].
  intros P H; split; intros i; apply H.
  intros E H; split; intros i; apply H.
  intros P1 P2 H1 H2; split; intros i; rewrite H1; rewrite H2; apply refl_id.
  intros E1 E2 H1 H2; split; split; intros i; rewrite H1; rewrite H2; apply refl_id.
  intros E P H1 H2 i; rewrite H1; rewrite H2; apply refl_id.
 Qed.

 Definition fpuplocinfdep:=fst ftuplocinfdep.
 Definition feuplocinfdep:=snd ftuplocinfdep.

 (* fpuploc and feuploc have no effect if there are no adequate free variables *)
 Theorem ftuplocgrf:(forall (P:☊)(i d:☈), i≤☉d->i>☋☂P≡⊤->fpuploc d P≡P)*
                                     (forall (E:☌)(i d:☈), i≤☉d->i>☍☂E≡⊤->feuploc d E≡E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (i d:☈), i≤☉d->i>☋☂P≡⊤->fpuploc d P≡P)
                             (fun E:☌=>forall (i d:☈), i≤☉d->i>☍☂E≡⊤->feuploc d E≡E)); simpl.
   intros i d H H0; apply refl_id.
   intros i; split; [split | idtac]; intros j d H H0; try (apply refl_id);
    destruct (decige d i).
    destruct j;
     [invset H0; discriminate H1
     | destruct (nreflnige _ (transige _ _ _ H (transige _ _ _ i0 (igetp _ _ H0))))].
    rewrite (igerf _ _ e); apply refl_id.
   intros P H; split; intros j d H0 H1;
    [rewrite (H _ _ H0 H1) | rewrite (H _ _ (genxt _ _ H0) H1)]; apply refl_id.
   intros E H; split; intros j d H0 H1; rewrite (H _ _ H0 H1); apply refl_id.
   intros P1 P2 H1 H2; split; intros j d H H0; destruct (dedfbandt _ _ H0);
    rewrite (H1 _ _ H i); rewrite (H2 _ _ H i0); apply refl_id.
   intros P1 P2 H1 H2; split; split; intros j d H H0; destruct (dedfbandt _ _ H0);
    rewrite (H1 _ _ H i); rewrite (H2 _ _ H i0); apply refl_id.
   intros E P H1 H2 j d H H0; destruct (dedfbandt _ _ H0); rewrite (H1 _ _ H i);
    rewrite (H2 _ _ (genxt _ _ H) i0); apply refl_id.
 Qed.

 Definition fpuplocgrf:=fst ftuplocgrf.
 Definition feuplocgrf:=snd ftuplocgrf.

 Theorem fpuplocgr:forall (P:☊)(i d:☈), i≤☉d->i>☋P->fpuploc d P≡P.
 Proof.
  intros P i d H H0; apply (fpuplocgrf _ _ _ H (pmajpt _ _ H0)).
 Qed.

 Theorem feuplocgr:forall (E:☌)(i d:☈), i≤☉d->i>☍E->feuploc d E≡E.
 Proof.
  intros E i d H H0; apply (feuplocgrf _ _ _ H (emajpt _ _ H0)).
 Qed.

 (* fpuploc and feuploc have no effect over π-dependance *)
 Theorem ftuplocftnpd:(forall (P:☊)(d p:☈), fpnpd p (fpuploc d P)≡fpnpd p P)*
                                         (forall (E:☌)(d p:☈), fenpd p (feuploc d E)≡fenpd p E).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (d p:☈), fpnpd p (fpuploc d P)≡fpnpd p P)
                             (fun (E:☌)=>forall (d p:☈), fenpd p (feuploc d E)≡fenpd p E)); simpl.
   intros; apply refl_id.
   intros i; split; [split | idtac]; intros d p;
    [apply refl_id | apply refl_id | destruct (d≤☉☂i); simpl; apply refl_id].
   intros P H; split; intros d p; apply H.
   intros E H; split; intros d p; apply H.
   intros P1 P2 H1 H2; split; intros d p; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros d p; rewrite H1; rewrite H2; apply refl_id.
   intros E P H1 H2 d p; rewrite H1; rewrite H2; apply refl_id.
 Qed.

 Definition fpuplocfpnpd:=fst ftuplocftnpd.
 Definition feuplocfenpd:=snd ftuplocftnpd.

 (* Injectivity of fpuploc *)
 Theorem injftuploc:(forall (P Q:☊)(d:☈), fpuploc d P≡fpuploc d Q->P≡Q)*
                                     (forall (E F:☌)(d:☈), feuploc d E≡feuploc d F->E≡F).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (Q:☊)(d:☈), fpuploc d P≡fpuploc d Q->P≡Q)
                             (fun (E:☌)=>forall (F:☌)(d:☈), feuploc d E≡feuploc d F->E≡F)).
   destruct F; intros d Hup; simpl in Hup; invset Hup; apply refl_id.
   intros i; split; [split | idtac].
    destruct Q; intros d Hup; simpl in Hup; invset Hup. apply refl_id.
    destruct F; intros d Hup; simpl in Hup; invset Hup. apply refl_id.
    intros F d Hup; simpl in Hup; destruct (decige d i).
     rewrite <- (sym_id (igept _ _ i0)) in Hup; destruct F; simpl in Hup; invset Hup.
      destruct (decige d b).
       rewrite <- (sym_id (igept _ _ i1)) in Hup; invset_clear Hup; apply refl_id.
       rewrite <- (sym_id (igerf _ _ e)) in Hup; invset Hup; rewrite <- H1 in e; destruct e;
        apply nxtrige; apply i0.
     rewrite <- (sym_id (igerf _ _ e)) in Hup; destruct F; simpl in Hup; invset Hup.
      destruct (decige d b).
       rewrite <- (sym_id (igept _ _ i0)) in Hup; invset Hup; rewrite <- (sym_id H1) in e;
        destruct e; apply nxtrige; apply i0.
       rewrite (igerf _ _ e0); apply refl_id.
   intros P HP; split; destruct Q; intros d Hup; simpl in Hup; invset Hup;
    rewrite (HP _ _ H0); apply refl_id.
   intros E HE; split; destruct F; intros d Hup; simpl in Hup; invset Hup;
    rewrite (HE _ _ H0); apply refl_id.
   intros P1 P2 H1 H2; split; destruct Q; intros d Hup; simpl in Hup; invset Hup;
    rewrite (H1 _ _ H0); rewrite (H2 _ _ H3); apply refl_id.
   intros E1 E2 H1 H2; split; split; (destruct Q || destruct F); intros d Hup; simpl in Hup;
    invset Hup; rewrite (H1 _ _ H0); rewrite (H2 _ _ H3); apply refl_id.
   intros E P H1 H2; destruct F; intros d Hup; simpl in Hup; invset Hup; rewrite (H1 _ _ H0);
    rewrite (H2 _ _ H3); apply refl_id.
 Qed.

 Definition injfpuploc:=fst injftuploc.
 Definition injfeuploc:=snd injftuploc.

 Theorem injfpuploc':forall (Q P:☊)(d:☈), !(P≡Q)->!(fpuploc d P≡fpuploc d Q).
 Proof.
  intros Q P d H H0; apply H; generalize H0; apply injfpuploc.
 Qed.

 Theorem injfeuploc':forall (E F:☌)(d:☈), !(E≡F)->!(feuploc d E≡feuploc d F).
 Proof.
  intros E F d H H0; apply H; generalize H0; apply injfeuploc.
 Qed.

 Theorem ftuplocmaj:(forall (P:☊)(d m:☈), m>☋P->☠m>☋fpuploc d P)*
                                     (forall (E:☌)(d m:☈), m>☍E->☠m>☍feuploc d E).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (d m:☈), m>☋P->☠m>☋fpuploc d P)
                             (fun (E:☌)=>forall (d m:☈), m>☍E->☠m>☍feuploc d E)); simpl.
   intros d m Hmaj; apply majbig.
   intros i; split; [split | idtac]; intros d m Hmaj.
    apply majpvr.
    apply majbie.
    apply majvar; invset Hmaj; destruct (d≤☉☂i); [apply (gtnxt _ _ H1) | apply (nxtrigt _ _ H1)].
   intros P HP; split; intros d m Hmaj; invset Hmaj;
    [apply majnot; apply (HP d _ H1) | apply majfor; apply (HP ☠d _ H1)].
   intros E HE; split; intros d m Hmaj; invset Hmaj; [apply majchs | apply majpow];
    apply (HE d _ H1).
   intros P1 P2 H1 H2; split; intros d m Hmaj; invset Hmaj.
    apply majand; [apply (H1 d _ H4) | apply (H2 d _ H5)].
    apply majimp; [apply (H1 d _ H4) | apply (H2 d _ H5)].
   intros P1 P2 H1 H2; split; split; intros d m Hmaj; invset Hmaj.
    apply majequ; [apply (H1 d _ H4) | apply (H2 d _ H5)].
    apply majins; [apply (H1 d _ H4) | apply (H2 d _ H5)].
    apply majcpl; [apply (H1 d _ H4) | apply (H2 d _ H5)].
    apply majpro; [apply (H1 d _ H4) | apply (H2 d _ H5)].
   intros E P H1 H2 d m Hmaj; invset Hmaj; apply majcmp;
    [apply (H1 d _ H4) | apply (H2 ☠d _ H5)].
 Qed.

 Definition fpuplocmaj:=fst ftuplocmaj.
 Definition feuplocmaj:=snd ftuplocmaj.

(* Uplift function (increment floating indexes) ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fpuplft(P:☊):=fpuploc ☦ P.
 Notation "'☯☋(' P ')'":=(fpuplft P).

 Definition feuplft(E:☌):=feuploc ☦ E.
 Notation "'☯☍(' E ')'":=(feuplft E).

 Theorem fpuplftidx:forall (P:☊)(i:☈), ☠i⍀☋☂ ☯☋(P)≡i⍀☋☂P.
 Proof.
  intros P i; unfold fpuplft; apply fpuplocidx; apply gefst.
 Qed.

 Theorem fpuplftinf:forall (P:☊)(i:☈), i⍀☋P->☠i⍀☋ ☯☋(P).
 Proof.
  intros P i; unfold fpuplft; apply fpuplocinf; apply gefst.
 Qed.

 Theorem fpuplftifr:forall (P:☊)(i:☈), !(i⍀☋P)->!(☠i⍀☋ ☯☋(P)).
 Proof.
  intros P i; unfold fpuplft; apply fpuplocifr; apply gefst.
 Qed.

 Theorem feuplftidx:forall (E:☌)(i:☈), ☠i⍀☍☂ ☯☍(E)≡i⍀☍☂E.
 Proof.
  intros E i; unfold feuplft; apply feuplocidx; apply gefst.
 Qed.

 Theorem feuplftinf:forall (E:☌)(i:☈), i⍀☍E->☠i⍀☍ ☯☍(E).
 Proof.
  intros E i; unfold feuplft; apply feuplocinf; apply gefst.
 Qed.

 Theorem feuplftifr:forall (E:☌)(i:☈), !(i⍀☍E)->!(☠i⍀☍ ☯☍(E)).
 Proof.
  intros E i; unfold feuplft; apply feuplocifr; apply gefst.
 Qed.

 Theorem fpuplftgr:forall (P:☊), *⍀☋(P)->☯☋(P)≡P.
 Proof.
  intros P H; unfold fpuplft; rewrite (fpuplocgr P _ _ (gefst ☦) H); apply refl_id.
 Qed.

 Theorem feuplftgr:forall (E:☌), *⍀☍(E)->☯☍(E)≡E.
 Proof.
  intros E H; unfold feuplft; rewrite (feuplocgr E _ _ (gefst ☦) H); apply refl_id.
 Qed.

 Theorem fpuplftfpnpd:forall (P:☊)(p:☈), fpnpd p ☯☋(P)≡fpnpd p P.
 Proof.
  intros P p; unfold fpuplft; apply fpuplocfpnpd.
 Qed.

 Theorem feuplftfenpd:forall (E:☌)(p:☈), fenpd p ☯☍(E)≡fenpd p E.
 Proof.
  intros P p; unfold feuplft; apply feuplocfenpd.
 Qed.

 Theorem  fpuplftmaj:forall (P:☊)(m:☈), m>☋P->☠m>☋ ☯☋(P).
 Proof.
  intros P m; unfold fpuplft; apply fpuplocmaj.
 Qed.

 Theorem  feuplftmaj:forall (E:☌)(m:☈), m>☍E->☠m>☍ ☯☍(E).
 Proof.
  intros E m; unfold feuplft; apply feuplocmaj.
 Qed.

(* Downlift function (decrement floating indexes), local version ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fpdnloc(d:☈)(P:☊){struct P}:☊:=
  match P with
  | ¬P' => ¬ (fpdnloc d P')
  | ∀P' => ∀ (fpdnloc ☠d P')
  | P1∧P2 => (fpdnloc d P1) ∧ (fpdnloc d P2)
  | P1☲P2 => (fpdnloc d P1) ☲ (fpdnloc d P2)
  | π i => π i
  | E1♃E2 => (fednloc d E1) ♃ (fednloc d E2)
  | E1∈E2 => (fednloc d E1) ∈ (fednloc d E2)
  end
 with fednloc(d:☈)(E:☌){struct E}:☌:=
  match E with
  | ♄ => ♄
  | ☭E' => ☭ (fednloc d E')
  | ☬E' => ☬ (fednloc d E')
  | (E1☵E2) => (fednloc d E1☵fednloc d E2)
  | (E1♂E2) => (fednloc d E1♂fednloc d E2)
  | ω i => ω i
  | χ i => χ(match d≤☉☂i with ⊤ => ☋i | ⊥ => i end)
  | E'♀P' => (fednloc d E')♀(fpdnloc ☠d P')
  end.

 (* fpdnloc and fednloc are inverse of fpuploc and feuploc *)
 Theorem ftdnlocuploc:(forall (P:☊)(d:☈), fpdnloc d (fpuploc d P)≡P)*
                                         (forall (E:☌)(d:☈), fednloc d (feuploc d E)≡E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (d:☈), fpdnloc d (fpuploc d P)≡P)
                             (fun E:☌=>forall (d:☈), fednloc d (feuploc d E)≡E)); simpl.
   intros; apply refl_id.
   intros i; split; [split | idtac]; intros d;
    [apply refl_id | apply refl_id | destruct (decige d i)].
     rewrite (igept _ _ i0); simpl; rewrite (igept _ _ (nxtrige _ _ i0)); apply refl_id.
     rewrite (igerf _ _ e); simpl; rewrite (igerf _ _ e); apply refl_id.
   intros P H; split; intros d; rewrite H; apply refl_id.
   intros E H; split; intros d; rewrite H; apply refl_id.
   intros P1 P2 H1 H2; split; intros d; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros d; rewrite H1; rewrite H2; apply refl_id.
   intros E P H1 H2 d; rewrite H1; rewrite H2; apply refl_id.
 Qed.

 Definition fpdnlocuploc:=fst ftdnlocuploc.
 Definition fednlocuploc:=snd ftdnlocuploc.

 Theorem ftuplocdnloc:(forall (P:☊)(d:☈), d⍀☋P->fpuploc d (fpdnloc d P)≡P)*
                                         (forall (E:☌)(d:☈), d⍀☍E->feuploc d (fednloc d E)≡E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (d:☈), d⍀☋P->fpuploc d (fpdnloc d P)≡P)
                             (fun E:☌=>forall (d:☈), d⍀☍E->feuploc d (fednloc d E)≡E)); simpl.
   intros d H; apply refl_id.
   intros i; split; [split | idtac]; intros d H;
    [apply refl_id | apply refl_id | invset H; destruct (decige d i)].
    rewrite (igept _ _ i1); simpl; destruct i.
     invset i1; destruct H2; rewrite H3; apply refl_id.
     simpl; generalize (igeneq _ _ H2 i1); unfold igt; intros H3; invset H3;
      rewrite (igept _ _ H6); apply refl_id.
    rewrite (igerf _ _ e); simpl; rewrite (igerf _ _ e); apply refl_id.
   intros P H; split; intros d H0; invset H0; rewrite (H _ H3); apply refl_id.
   intros E H; split; intros d H0; invset H0; rewrite (H _ H3); apply refl_id.
   intros P1 P2 H1 H2; split; intros d H0; invset H0; rewrite (H1 _ H5); rewrite (H2 _ H6);
    apply refl_id.
   intros E1 E2 H1 H2; split; split; intros d H0; invset H0; rewrite (H1 _ H5);
    rewrite (H2 _ H6); apply refl_id.
   intros E P H1 H2 d H0; invset H0; rewrite (H1 _ H5); rewrite (H2 _ H6); apply refl_id.
 Qed.

 Definition fpuplocdnloc:=fst ftuplocdnloc.
 Definition feuplocdnloc:=snd ftuplocdnloc.

 (* fpdnloc and fednloc decrement the free variables *)
 Theorem ftdnlocidx:(forall (P:☊)(i d:☈), d≤☉ ☠i->☠☠i⍀☋☂P≡☠i⍀☋☂(fpdnloc d P))*
                                     (forall (E:☌)(i d:☈), d≤☉ ☠i->☠☠i⍀☍☂E≡☠i⍀☍☂(fednloc d E)).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (i d:☈), d≤☉ ☠i->☠☠i⍀☋☂P≡☠i⍀☋☂(fpdnloc d P))
                             (fun E:☌=>forall (i d:☈), d≤☉ ☠i->☠☠i⍀☍☂E≡☠i⍀☍☂(fednloc d E))); simpl.
   intros i d H; apply refl_id.
   intros i; split; [split | idtac]; intros j d H;
    [apply refl_id | apply refl_id | destruct (decige d i)].
     rewrite (igept _ _ i0); simpl; destruct i; simpl; apply refl_id.
     rewrite (igerf _ _ e); simpl; destruct i; simpl; [apply refl_id | destruct i; simpl].
      generalize (nigeigt _ _ e); unfold igt; intros H0; generalize (transige _ _ _ H0 H);
       intros H1; invset H1; destruct j; [invset H4 | apply refl_id].
      generalize (nigeigt _ _ e); intros H0; generalize (transigtige _ _ _ H0 H); intros H1;
       invset H1; destruct (deciequ j i).
       rewrite <- (sym_id i1) in H4; generalize (nxtrige _ _ H4); intros H5;
        destruct (nreflnige _ H5).
       rewrite (iequrf _ _ e0); destruct (deciequ j ☠i);
        [rewrite <- (sym_id i1) in H4; destruct (nreflnige _ H4)
        | rewrite (iequrf _ _ e1); apply refl_id].
   intros P H; split; intros j d H0;
    [rewrite (H _ _ H0) | rewrite (H _ _ (genxt _ _ H0))]; apply refl_id.
   intros E H; split; intros j d H0; rewrite (H _ _ H0); apply refl_id.
   intros P1 P2 H1 H2; split; intros j d H; rewrite (H1 _ _ H); rewrite (H2 _ _ H);
    apply refl_id.
   intros E1 E2 H1 H2; split; split; intros j d H; rewrite (H1 _ _ H); rewrite (H2 _ _ H);
    apply refl_id.
   intros E P H1 H2 j d H; rewrite (H1 _ _ H); rewrite (H2 _ _ (genxt _ _ H)); apply refl_id.
 Qed.

 Definition fpdnlocidx:=fst ftdnlocidx.
 Definition fednlocidx:=snd ftdnlocidx.

 (* fpdnloc and fednloc have no effect if there are no adequate free variables *)
 Theorem frdnlocgrf:(forall (P:☊)(i d:☈), i≤☉d->i>☋☂P≡⊤->fpdnloc d P≡P)*
                                     (forall (E:☌)(i d:☈), i≤☉d->i>☍☂E≡⊤->fednloc d E≡E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (i d:☈), i≤☉d->i>☋☂P≡⊤->fpdnloc d P≡P)
                             (fun E:☌=>forall (i d:☈), i≤☉d->i>☍☂E≡⊤->fednloc d E≡E)); simpl.
   intros i d H H0; apply refl_id.
   intros i; split; [split | idtac]; intros j d H H0; try (apply refl_id); destruct j.
    invset H0; discriminate H1.
    destruct (decige d i).
     generalize (igtige _ _ (transige _ _ _ H i0)); intros H1;
      rewrite <- (symige _ _ H1 (igetp _ _ H0)) in i0;
      destruct (nrefligt _ (transigtige _ _ _ H i0)).
     rewrite (igerf _ _ e); apply refl_id.
   intros P H; split; intros j d H0 H1;
    [rewrite (H _ _ H0 H1) | rewrite (H _ _ (genxt _ _ H0) H1)]; apply refl_id.
   intros E H; split; intros j d H0 H1; rewrite (H _ _ H0 H1); apply refl_id.
   intros P1 P2 H1 H2; split; intros j d H H0; destruct (dedfbandt _ _ H0);
    rewrite (H1 _ _ H i); rewrite (H2 _ _ H i0); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros j d H H0; destruct (dedfbandt _ _ H0);
    rewrite (H1 _ _ H i); rewrite (H2 _ _ H i0); apply refl_id.
   intros E P H1 H2 j d H H0; destruct (dedfbandt _ _ H0); rewrite (H1 _ _ H i);
    rewrite (H2 _ _ (genxt _ _ H) i0); apply refl_id.
 Qed.

 Definition fpdnlocgrf:=fst frdnlocgrf.
 Definition fednlocgrf:=snd frdnlocgrf.

 Theorem fpdnlocgr:forall (P:☊)(i d:☈), i≤☉d->i>☋P->fpdnloc d P≡P.
 Proof.
  intros P i d H H0; apply (fpdnlocgrf _ _ _ H (pmajpt _ _ H0)).
 Qed.

 Theorem fednlocgr:forall (E:☌)(i d:☈), i≤☉d->i>☍E->fednloc d E≡E.
 Proof.
  intros E i d H H0; apply (fednlocgrf _ _ _ H (emajpt _ _ H0)).
 Qed.

 (* fpdnloc and fednloc have no effect over π-dependance *)
 Theorem ftdnlocftnpd:(forall (P:☊)(d p:☈), fpnpd p (fpdnloc d P)≡fpnpd p P)*
                                         (forall (E:☌)(d p:☈), fenpd p (fednloc d E)≡fenpd p E).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (d p:☈), fpnpd p (fpdnloc d P)≡fpnpd p P)
                             (fun (E:☌)=>forall (d p:☈), fenpd p (fednloc d E)≡fenpd p E)); simpl.
   intros _ _; apply refl_id.
   intros i; split; [split | idtac]; intros d p;
    [apply refl_id | apply refl_id | destruct (d≤☉☂i); apply refl_id].
   intros P H; split; intros d p; apply H.
   intros E H; split; intros d p; apply H.
   intros P1 P2 H1 H2; split; intros d p; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros d p; rewrite H1; rewrite H2; apply refl_id.
   intros E P H1 H2 d p; rewrite H1; rewrite H2; apply refl_id.
 Qed.

 Definition fpdnlocfpnpd:=fst ftdnlocftnpd.
 Definition fednlocfenpd:=snd ftdnlocftnpd.

(* Downlift function (decrement floating indexes) ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fpdnlft(P:☊):=fpdnloc ☦ P.
 Notation "'☮☋(' P ')'":=(fpdnlft P).

 Definition fednlft(E:☌):=fednloc ☦ E.
 Notation "'☮☍(' E ')'":=(fednlft E).

 Theorem fpdnlftuplft:forall (P:☊), ☮☋(☯☋(P))≡P.
 Proof.
  intros P; unfold fpdnlft; unfold fpuplft; apply fpdnlocuploc.
 Qed.

 Theorem fpuplftdnlft:forall (P:☊), ☦⍀☋P->☯☋(☮☋(P))≡P.
 Proof.
  intros P; unfold fpdnlft; unfold fpuplft; apply fpuplocdnloc.
 Qed.

 Theorem fpdnlftidx:forall (P:☊)(i:☈), ☠☠i⍀☋☂P≡☠i⍀☋☂ ☮☋(P).
 Proof.
  intros P i; unfold fpdnlft; apply fpdnlocidx; apply gefst.
 Qed.

 Theorem fednlftuplft:forall (E:☌), ☮☍(☯☍(E))≡E.
 Proof.
  intros E; unfold fednlft; unfold feuplft; apply fednlocuploc.
 Qed.

 Theorem feuplftdnlft:forall (E:☌), ☦⍀☍E->☯☍(☮☍(E))≡E.
 Proof.
  intros E; unfold fednlft; unfold feuplft; apply feuplocdnloc.
 Qed.

 Theorem fednlftidx:forall (E:☌)(i:☈), ☠☠i⍀☍☂E≡☠i⍀☍☂ ☮☍(E).
 Proof.
  intros E i; unfold fednlft; apply fednlocidx; apply gefst.
 Qed.

 Theorem fpdnlftgr:forall (P:☊), *⍀☋(P)->☮☋(P)≡P.
 Proof.
  intros P H; unfold fpdnlft; rewrite (fpdnlocgr P _ _ (gefst ☦) H); apply refl_id.
 Qed.

 Theorem fednlftgr:forall (E:☌), *⍀☍(E)->☮☍(E)≡E.
 Proof.
  intros E H; unfold fednlft; rewrite (fednlocgr E _ _ (gefst ☦) H); apply refl_id.
 Qed.

 Theorem fpdnlftfpnpd:forall (P:☊)(p:☈), fpnpd p ☮☋(P)≡fpnpd p P.
 Proof.
  intros P p; unfold fpdnlft; apply fpdnlocfpnpd.
 Qed.

 Theorem fednlftfenpd:forall (E:☌)(p:☈), fenpd p ☮☍(E)≡fenpd p E.
 Proof.
  intros P p; unfold fednlft; apply fednlocfenpd.
 Qed.

(* Commutation lemma ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Lemma ftuplocdbl:(forall (P:☊)(c d:☈),
                                  c≤☉d->fpuploc c (fpuploc d P)≡fpuploc ☠d (fpuploc c P))*
                                 (forall (E:☌)(c d:☈),
                                  c≤☉d->feuploc c (feuploc d E)≡feuploc ☠d (feuploc c E)).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (c d:☈),
                                              c≤☉d->fpuploc c (fpuploc d P)≡fpuploc ☠d (fpuploc c P))
                             (fun E:☌=>forall (c d:☈),
                                              c≤☉d->feuploc c (feuploc d E)≡feuploc ☠d (feuploc c E))); simpl.
   intros c d H; apply refl_id.
   intros i; split; [split | idtac]; intros c d H; try (apply refl_id); destruct (decige d i).
    rewrite (igept _ _ i0); rewrite (igept _ _ (transige _ _ _ H i0)); simpl;
     rewrite (igept _ _ i0); rewrite (igept _ _ (nxtrige _ _ (transige _ _ _ H i0)));
     apply refl_id.
    rewrite (igerf _ _ e); destruct (decige c i).
     rewrite (igept _ _ i0); simpl; rewrite (igerf _ _ e); apply refl_id.
     rewrite (igerf _ _ e0); simpl; destruct i.
      apply refl_id.
      destruct (decige d i).
       generalize (nxtrigt _ _ (nigeigt _ _ e)); intros H0; invset H0;
         destruct (nreflnige _ (transige _ _ _ H3 i0)).
       rewrite (igerf _ _ e1); apply refl_id.
   intros P H; split; intros c d H0; [rewrite (H _ _ H0) | rewrite (H _ _ (genxt _ _ H0))];
    apply refl_id.
   intros E H; split; intros c d H0; rewrite (H _ _ H0); apply refl_id.
   intros P1 P2 H1 H2; split; intros c d H; rewrite (H1 _ _ H); rewrite (H2 _ _ H);
    apply refl_id.
   intros E1 E2 H1 H2; split; split; intros c d H; rewrite (H1 _ _ H); rewrite (H2 _ _ H);
    apply refl_id.
   intros E P H1 H2 c d H; rewrite (H1 _ _ H); rewrite (H2 _ _ (genxt _ _ H)); apply refl_id.
 Qed.

 Definition fpuplocdbl:=fst ftuplocdbl.
 Definition feuplocdbl:=snd ftuplocdbl.

 Lemma ftuplocftdnloc:(forall (P:☊)(c d:☈),
                                          c≤☉d->d⍀☋P->fpuploc c (fpdnloc d P)≡fpdnloc ☠d (fpuploc c P))*
                                         (forall (E:☌)(c d:☈),
                                          c≤☉d->d⍀☍E->feuploc c (fednloc d E)≡fednloc ☠d (feuploc c E)).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (c d:☈),
                                              c≤☉d->d⍀☋P->fpuploc c (fpdnloc d P)≡fpdnloc ☠d (fpuploc c P))
                             (fun E:☌=>forall (c d:☈),
                                              c≤☉d->d⍀☍E->feuploc c (fednloc d E)≡fednloc ☠d (feuploc c E)));
   simpl.
   intros c d H H0; apply refl_id.
   intros i; split; [split | idtac]; intros c d H H0; invset H0;
    [apply refl_id | apply refl_id | destruct (decige d i)].
    rewrite (igept _ _ i1); rewrite (igept _ _ (transige _ _ _ H i1)); simpl; rewrite (igept _ _ i1);
     destruct i; simpl.
     invset i1; destruct H3; rewrite H4; apply refl_id.
     generalize (igeneq _ _ H3 i1); intros H4; invset H4;
      rewrite (igept _ _ (transige _ _ _ H H7)); apply refl_id.
    rewrite (igerf _ _ e); destruct (decige c i).
     rewrite (igept _ _ i1); simpl; rewrite (igerf _ _ e); apply refl_id.
     rewrite (igerf _ _ e0); simpl; destruct i.
      apply refl_id.
      destruct (decige d i).
       generalize (nxtrigt _ _ (transigtige _ _ _ (nigeigt _ _ e) i1)); intros H4; invset H4;
        destruct (nrefligt _ H7).
       rewrite (igerf _ _ e1); apply refl_id.
   intros P H; split; intros c d H0 H1; invset H1;
    [rewrite (H _ _ H0 H4) | rewrite (H _ _ (genxt _ _ H0) H4)]; apply refl_id.
   intros E H; split; intros c d H0 H1; invset H1; rewrite (H _ _ H0 H4); apply refl_id.
   intros P1 P2 H1 H2; split; intros c d H H0; invset H0; rewrite (H1 _ _ H H6);
    rewrite (H2 _ _ H H7); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros c d H H0; invset H0; rewrite (H1 _ _ H H6);
    rewrite (H2 _ _ H H7); apply refl_id.
   intros E P H1 H2 c d H H0; invset H0; rewrite (H1 _ _ H H6); rewrite (H2 _ _ (genxt _ _ H) H7);
    apply refl_id.
 Qed.

 Definition fpuplocfpdnloc:=fst ftuplocftdnloc.
 Definition feuplocfednloc:=snd ftuplocftdnloc.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
