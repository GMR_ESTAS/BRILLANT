(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq2, formalisation of the B theory into the Coq system
   Module: Mindex, indexes
   Indexes are a simplified version of inductive naturals, used as De Bruijn indexes for
   B terms but also as generators of elements for the infinite set BIG of the B-Book; the
   well funded induction and strong induction principles are also provided as results.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - September 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Mbool.

(* Indexes definition ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Inductive Bidx:Set:=ifst:Bidx | inxt:Bidx->Bidx.
 Notation "☈":=Bidx.
 Notation "☦":=ifst.
 Notation "'☠' i":=(inxt i)  (at level 0, right associativity).
 Notation "☧":=(☠☦).
 Notation "☧☦":=(☠☧).
 Notation "☧☧":=(☠☧☦).
 Notation "☧☦☦":=(☠☧☧).
 Notation "☧☦☧":=(☠☧☦☦).
 Notation "☧☧☦":=(☠☧☦☧).
 Notation "☧☧☧":=(☠☧☧☦).
 Notation "☧☦☦☦":=(☠☧☧☧).
 Notation "☧☦☦☧":=(☠☧☦☦☦).
 Notation "☧☦☧☦":=(☠☧☦☦☧).
 Notation "☧☦☧☧":=(☠☧☦☧☦).
 Notation "☧☧☦☦":=(☠☧☦☧☧).
 Notation "☧☧☦☧":=(☠☧☧☦☦).
 Notation "☧☧☧☦":=(☠☧☧☦☧).
 Notation "☧☧☧☧":=(☠☧☧☧☦).

(* Equality function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fiequ(i j:☈){struct i}:☆:=
  match i with
  | ☦ => match j with ☦ => ⊤ | _ => ⊥ end
  | ☠i' => match j with ☠j' => fiequ i' j' | _ => ⊥ end
  end.
 Infix "=☉☂":=fiequ (at level 20, right associativity).

 Theorem reflfiequ:forall (i:☈), i=☉☂i≡⊤.
 Proof.
  induction i; [idtac | apply IHi]; apply refl_id.
 Qed.

 Theorem impiequ:(identity (A:=☈)☵☨☂fiequ).
 Proof.
  unfold Implem2; intros s t; split; generalize s t; clear s t; double induction s t.
   intros H; apply refl_id.
   intros b _ H0; invset H0; discriminate H.
   intros b _ H0; invset H0; discriminate H.
   intros b _ b0 H0 H1; rewrite (H0 _ H1); apply refl_id.
   intros H _; invset H; discriminate H0.
   intros b _ _ H1; invset_clear H1; discriminate H.
   intros b _ _ H1; invset_clear H1; discriminate H.
   intros b _ b0 _ H1 H2; rewrite <- (sym_id H2) in H1;
    rewrite <- (sym_id (reflfiequ ☠b)) in H1; invset H1; discriminate H.
 Qed.

 Definition deciequ:=decimp2 impiequ.
 Definition iequpt:=imp2pt impiequ.
 Definition iequrf:=imp2rf impiequ.
 Definition iequtp:=imp2tp impiequ.
 Definition iequfr:=imp2fr impiequ.

(* Comparison ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Inductive ige:☈->☈->Set:=
  | gefst : forall (i:☈),  ige ☦ i
  | genxt : forall (i j:☈), ige i j->ige ☠i ☠j.
 Infix "≤☉":=ige (at level 6, right associativity).

 Definition igt (i j:☈):Set:=ige ☠i j.
 Infix "<☉":=igt (at level 6, right associativity).

 Theorem gtnxt:forall (i j:☈), i<☉j->☠i<☉☠j.
 Proof.
  intros i j H; unfold igt; apply genxt; apply H.
 Qed.

 Theorem fstige:forall (i:☈), i≤☉☦->i≡☦.
 Proof.
  induction i; intros H; [apply refl_id | invset H].
 Qed.

 Theorem fstigt:forall (i:☈), !(i<☉☦).
 Proof.
  intros i H; generalize (fstige _ H); intros H0; invset H0.
 Qed.

 Theorem reflige:forall (i:☈), i≤☉i.
 Proof.
  induction i; [apply gefst | apply genxt; apply IHi].
 Qed.

 Theorem nrefligt:forall (i:☈), !(i<☉i).
 Proof.
  induction i; [apply fstigt | intros H; invset_clear H; apply (IHi H0)].
 Qed.

 Theorem nxtrige:forall (i j:☈), i≤☉j->i≤☉☠j.
 Proof.
  induction i; intros j H;
   [apply gefst | induction j; [invset H | apply genxt; apply IHi; invset H; apply H2]].
 Qed.

 Theorem igtneq:forall (i j:☈), i<☉j->!(i≡j).
 Proof.
  intros i j H; destruct (deciequ i j);
   [rewrite <- (sym_id i0) in H; destruct (nrefligt _ H) | apply e].
 Qed.

 Theorem nxtrigt:forall (i j:☈), i<☉j->i<☉☠j.
 Proof.
  intros i j H; unfold igt; apply nxtrige; apply H.
 Qed.

 Theorem reflnxtige:forall (i:☈), i≤☉☠i.
 Proof.
  intros i; apply nxtrige; apply reflige.
 Qed.

 Theorem reflnxtigt:forall (i:☈), i<☉☠i.
 Proof.
  intros i; unfold igt; apply reflige.
 Qed.

 Theorem igtige:forall (i j:☈), i<☉j->i≤☉j.
 Proof.
  induction i; intros j H;
   [apply gefst | induction j; [invset H | apply genxt; apply IHi; invset H; apply H2]].
 Qed.

 Theorem symige:forall (i j:☈), i≤☉j->j≤☉i->i≡j.
 Proof.
  induction i; intros j H H0.
   invset H0; apply refl_id.
   induction j; invset_clear H; invset_clear H0; rewrite (IHi _ H1 H); apply refl_id.
 Qed.

 Theorem nigeigt:forall (i j:☈), !(i≤☉j)->j<☉i.
 Proof.
  induction i; intros j H.
   elim H; apply gefst.
   unfold igt; apply genxt; induction j;
    [apply gefst | apply IHi; intros H0; apply H; apply genxt; apply H0].
 Qed.

 Theorem nigtige:forall (i j:☈), !(i<☉j)->j≤☉i.
 Proof.
  intros i j H; unfold igt in H; generalize (nigeigt _ _ H); intros H'; invset_clear H';
   apply H0.
 Qed.

 Theorem asymige:forall (i j:☈), !(i≤☉j)->j≤☉i.
 Proof.
  intros i j H; apply (igtige _ _ (nigeigt _ _ H)).
 Qed.

 Theorem asymigt:forall (i j:☈), i<☉j->!(j<☉i).
 Proof.
  induction i; intros j H H0.
   apply (fstigt _ H0).
   induction j;
    [apply (fstigt _ H) | invset_clear H; invset_clear H0; apply (IHi _ H1 H)].
 Qed.

 Theorem igeeq:forall (i j:☈), i≤☉j->!(i<☉j)->i≡j.
 Proof.
  double induction i j.
   intros _ _; apply refl_id.
   intros b _ _ H1; elim H1; unfold igt; apply genxt; apply gefst.
   intros b _ H0 _; invset H0.
   intros b _ b0 H0 H1 H2; invset_clear H1; assert (H1:!(☠b0≤☉b)).
    intros H1; apply H2; unfold igt; apply genxt; apply H1.
   rewrite (H0 _ H H1); apply refl_id.
 Qed.

 Theorem igeneq:forall (i j:☈), !(i≡j)->i≤☉j->i<☉j.
 Proof.
  double induction i j.
   intros H _; elim H; apply refl_id.
   intros b _  _ _; unfold igt; apply genxt; apply gefst.
   intros b _ _ H1; invset H1.
   intros b _ b0 H0 H1 H2; unfold igt; apply genxt; destruct (deciequ b0 b);
    [rewrite <- (sym_id i0) in H1; elim H1; apply refl_id | invset_clear H2; apply (H0 b e H)].
 Qed.

 Theorem nreflnige:forall (j:☈), !(☠j≤☉j).
 Proof.
  induction j; intros H; invset H; apply IHj; apply H2.
 Qed.

 Theorem transige:forall (i j k:☈), i≤☉j->j≤☉k->i≤☉k.
 Proof.
  induction i; intros j k H H0.
   apply gefst.
   induction j; invset_clear H; destruct (deciequ i j);
     [rewrite i0; apply H0 | apply (IHj (igeneq _ _ e H1) (igtige _ _ H0))].
 Qed.

(* Comparison function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fige(i j:☈){struct i}:☆:=
  match i with ☦ => ⊤ | ☠i' => match j with ☦ => ⊥ | ☠j' => fige i' j' end end.
 Infix "≤☉☂":=fige (at level 20, right associativity).

 Theorem impige:(ige☵☨☂fige).
 Proof.
  unfold Implem2; intros i1; induction i1; split.
   intros _; apply gefst.
   intros H _; invset H; discriminate H0.
   intros H; induction t; [invset H; discriminate H0
                                           | apply genxt; apply (fst (IHi1 t)); apply H].
   intros H H0; induction t; invset_clear H0; apply (snd (IHi1 t) H H1).
 Qed.

 Definition decige:=decimp2 impige.
 Definition igept:=imp2pt impige.
 Definition igerf:=imp2rf impige.
 Definition igetp:=imp2tp impige.
 Definition igefr:=imp2fr impige.

 Definition figt(i j:☈):☆:=☠i≤☉☂j.
 Infix "<☉☂":=figt (at level 20, right associativity).

 Theorem impigt:(igt☵☨☂figt).
 Proof.
  unfold Implem2; unfold igt; unfold figt; intros s t; apply impige.
 Qed.

 Definition decigt:=decimp2 impigt.
 Definition igtpt:=imp2pt impigt.
 Definition igtrf:=imp2rf impigt.
 Definition igttp:=imp2tp impigt.
 Definition igtfr:=imp2fr impigt.

 Theorem transigt:forall (i j k:☈), i<☉j->j<☉k->i<☉k.
 Proof.
  intros i j k H H0; generalize (transige _ _ _ (igtige _ _ H) (igtige _ _ H0)); intros H1;
   destruct (decigt i k);
   [apply i0 | rewrite <- (sym_id (igeeq _ _ H1 e)) in H; elim (asymigt _ _ H H0)].
 Qed.

 Theorem transigeigt:forall (i j k:☈), i≤☉j->j<☉k->i<☉k.
 Proof.
  intros i j k H H0; destruct (decigt i k).
   apply i0.
   generalize (transige _ _ _ H (igtige _ _ H0)); intros H1;
    rewrite <- (sym_id (igeeq _ _ H1 e)) in H;
    rewrite <- (sym_id (symige _ _ H (igtige _ _ H0))) in H0; destruct (nrefligt _ H0).
 Qed.

 Theorem transigtige:forall (i j k:☈), i<☉j->j≤☉k->i<☉k.
 Proof.
  intros i j k H H0; destruct (decigt i k).
   apply i0.
   generalize (transige _ _ _ H0 (nigtige _ _ e)); clear e H0; intros H0.
   rewrite <- (sym_id (symige _ _ (igtige _ _ H) H0)) in H; destruct (nrefligt _ H).
 Qed.

(* Maximum function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fimax(i j:☈):☈:=match i≤☉☂j with ⊤ => j | ⊥ => i end.
 Infix "☰☉":=fimax (at level 20, right associativity).

 Theorem dedfimax:forall (i j:☈), i☰☉j≡j->i≤☉j.
 Proof.
  intros i j H; unfold fimax in H; destruct (decige i j);
   [apply i0 | rewrite <- (sym_id (igerf i j e)) in H; rewrite H; apply reflige].
 Qed.

 Theorem invfimax:forall (i j:☈), i≤☉j->i☰☉j≡j.
 Proof.
  intros i j H; unfold fimax; rewrite (igept i j H); apply refl_id.
 Qed.

 Theorem commfimax:forall (i j:☈), i☰☉j≡j☰☉i.
 Proof.
  intros i j; destruct (decige i j).
   rewrite (invfimax _ _ i0); destruct (decige j i).
    rewrite (invfimax _ _ i1); apply (symige _ _ i1 i0).
    unfold fimax; rewrite (igerf _ _ e); apply refl_id.
   rewrite (invfimax _ _ (asymige _ _ e)); unfold fimax; rewrite (igerf _ _ e); apply refl_id.
 Qed.

 Theorem fimaxgel:forall (i j:☈), i≤☉(i☰☉j).
 Proof.
  intros i j; unfold fimax; destruct (deciequ i j).
   rewrite i0; rewrite (igept _ _ (reflige j)); apply reflige.
   destruct (decige i j);
    [rewrite (igept _ _ i0); apply i0 | rewrite (igerf _ _ e0); apply reflige].
 Qed.

 Theorem fimaxger:forall (i j:☈), j≤☉(i☰☉j).
 Proof.
  intros i j; rewrite (commfimax i j); apply fimaxgel.
 Qed.

 Theorem fimaxnxt:forall (i j:☈), ☠(i☰☉j)≡(☠i☰☉☠j).
 Proof.
  intros i j; unfold fimax; destruct (decige i j).
   rewrite (igept _ _ i0); rewrite (igept _ _ (genxt _ _ i0)); apply refl_id.
   assert (g:!(☠i≤☉ ☠j)). intros H; invset_clear H; contradiction.
   rewrite (igerf _ _ e); rewrite (igerf _ _ g); apply refl_id.
 Qed.

 Theorem casefimax:forall (i j:☈), (i☰☉j≡i)+(i☰☉j≡j).
 Proof.
  intros i j; destruct (decige i j);
   [right; apply (invfimax _ _ i0)
   | left; rewrite commfimax; apply (invfimax _ _ (asymige _ _ e))].
 Qed.

 Theorem igemax:forall (i j k:☈), i≤☉k->j≤☉k->(i☰☉j)≤☉k.
 Proof.
  intros i j k H H0; destruct (casefimax i j); rewrite i0; [apply H | apply H0].
 Qed.

 Theorem igemax2:forall (i1 i2 j1 j2:☈), i1≤☉j1->i2≤☉j2->(i1☰☉i2)≤☉(j1☰☉j2).
 Proof.
  intros i1 i2 j1 j2 H H0; destruct (casefimax i1 i2); destruct (casefimax j1 j2);
   rewrite i; rewrite i0.
   apply H.
   generalize (dedfimax _ _ i0); intros H1; apply (transige _ _ _ H H1).
   rewrite <- (sym_id (commfimax j1 j2)) in i0; apply (transige _ _ _ H0 (dedfimax _ _ i0)).
   apply H0.
 Qed.

(* Previous function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fiprev(i:☈):☈:=match i with ☦ => ☦ | ☠i' => i' end.
 Notation "'☋' i":=(fiprev i) (at level 5).

 Theorem iprevnxt:forall (i:☈), ☋☠i≡i.
 Proof.
  intros i; apply refl_id.
 Qed.

 Theorem inxtprev:forall (i:☈), !(i≡☦)->☠(☋i)≡i.
 Proof.
  induction i; intros H; [destruct H | simpl]; apply refl_id.
 Qed.

 Theorem iprevige:forall (i:☈), ☋i≤☉i.
 Proof.
  induction i; simpl; [apply gefst | apply reflnxtige].
 Qed.

 Theorem igeprev:forall (i j:☈), i≤☉j->☋i≤☉j.
 Proof.
  induction i; intros j H; simpl; [apply gefst | apply (igtige _ _ H)].
 Qed.

 Theorem iprevdbl:forall (i j:☈), i≤☉j->☋i≤☉ ☋j.
 Proof.
  induction i; intros j H; [apply gefst | simpl; induction j; invset_clear H; apply H0].
 Qed.

(* Well-founded induction principle ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* ☈ being well-founded, it may be used to build measures allowing induction proofs...   *)

 Theorem WFIND1:forall (S:Set)(M:S->☈)(P:S->Set),
                             (forall (s:S), (forall (t:S), (M t)<☉(M s)->P t)->P s)->forall (s:S), P s.
 Proof.
  intros S M P Hind; assert (H:forall (i:☈)(s:S), (M s)<☉i->P s).
   induction i; intros s H.
    invset H.
    apply Hind; intros t H0; apply IHi; invset_clear H; apply (transigtige _ _ _ H0 H1).
  intros s; apply Hind; intros t H0; apply (H (M s)); apply H0.
 Qed.
 Implicit Arguments WFIND1 [S].

 Theorem WFIND2:forall (S T:Set)(MS:S->☈)(MT:T->☈)(PS:S->Set)(PT:T->Set),
                              (forall (s:S), (forall (s':S), (MS s')<☉(MS s)->PS s')->
                                                     (forall (t':T), (MT t')<☉(MS s)->PT t')->PS s)->
                              (forall (t:T), (forall (t':T), (MT t')<☉(MT t)->PT t')->
                                                     (forall (s':S), (MS s')<☉(MT t)->PS s')->PT t)->
                              (forall (s:S), PS s)*(forall (t:T), PT t).
 Proof.
  intros S T MS MT PS PT HS HT;
   assert (H:forall (i:☈),(forall (s:S), (MS s)<☉i->PS s)*(forall (t:T), (MT t)<☉i->PT t)).
   induction i; split.
    intros s H; invset H.
    intros t H; invset H.
    destruct IHi; intros s H1; apply HS.
     intros s' H2; apply p; invset_clear H1; apply (transigtige _ _ _ H2 H).
     intros t' H2; apply p0; invset_clear H1; apply (transigtige _ _ _ H2 H).
    destruct IHi; intros t H1; apply HT.
     intros t' H2; apply p0; invset_clear H1; apply (transigtige _ _ _ H2 H).
     intros s' H2; apply p; invset_clear H1; apply (transigtige _ _ _ H2 H).
  split.
  intros s; apply HS.
   intros s' H0; destruct (H (MS s)); apply p; apply H0.
   intros t' H0; destruct (H (MS s)); apply p0; apply H0.
  intros t; apply HT.
   intros t' H0; destruct (H (MT t)); apply p0; apply H0.
   intros s' H0; destruct (H (MT t)); apply p; apply H0.
 Qed.
 Implicit Arguments WFIND2 [S T].

(* Strong induction principle ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* The strong induction principle for ☈ is just an application of the WFIND1, using       *)
(* identity as the measure over ☈.                                                                                                 *)

 Theorem idx_stind:forall (P:☈->Set),
                                   (forall (i:☈), (forall (j:☈), j<☉i->P j)->P i)->forall (i:☈), P i.
 Proof.
  intros P H; apply (WFIND1 (fun x:☈=>x) P); apply H.
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
