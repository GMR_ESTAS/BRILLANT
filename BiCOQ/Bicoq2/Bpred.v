(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Bpred, predicates calculus
   B-Book predicate calculus proofs are reproduced in this module.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - October 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Prawprd.

(* Remarks about ¬¬ occurrences ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* We provide a few results frequently used in what follows, using the raw predicate     *)
(* replacement results of Brawprd.                                                                                               *)

 Theorem notnotelim:forall (G:☐)(m:☈)(P Q:☊), G⊣ 〈m⍇P@☋Q〉->G⊣ 〈m⍇¬¬P@☋Q〉.
 Proof.
  intros G m P Q HQ; apply (mp _ _ 〈m⍇¬¬P@☋Q〉 HQ); apply diffimp; apply ifffprawprd'; BTprop.
 Qed.

 Theorem notnotintro:forall (G:☐)(m:☈)(P Q:☊), G⊣ 〈m⍇¬¬P@☋Q〉->G⊣ 〈m⍇P@☋Q〉.
 Proof.
  intros G m P Q HQ; apply (mp _ _ 〈m⍇P@☋Q〉 HQ); apply riffimp; apply ifffprawprd'; BTprop.
 Qed.

 Theorem fpbdfornotnote:forall (G:☐)(v:☈)(P:☊), G⊣ ☬☢(v∙P)->G⊣ ☬☢(v∙¬¬P).
 Proof.
  intros G v P; unfold fpbdfor; intros H; simpl;
   replace (∀¬¬(fpbdloc v ☦ P)) with 〈 ☦⍇¬¬(fpbdloc v ☦ P)@☋ ∀π(☦)〉;
   [apply notnotelim; apply H | apply refl_equal].
 Qed.

 Theorem fpbdfornotnoti:forall (G:☐)(v:☈)(P:☊), G⊣ ☬☢(v∙¬¬P)->G⊣ ☬☢(v∙P).
 Proof.
  intros G v P; unfold fpbdfor; intros H; simpl;
   replace (∀fpbdloc v ☦ P) with 〈 ☦⍇fpbdloc v ☦ P@☋ ∀π(☦)〉;
   [apply notnotintro; apply H | apply refl_equal].
 Qed.

 Theorem fpbdexsnotnote:forall (G:☐)(v:☈)(P:☊), G⊣ ☬☣(v∙P)->G⊣ ☬☣(v∙¬¬P).
 Proof.
  intros G v P; unfold fpbdexs; intros H; simpl;
   replace (∃¬¬(fpbdloc v ☦ P)) with 〈 ☦⍇¬¬(fpbdloc v ☦ P)@☋ ∃π(☦)〉;
   [apply notnotelim; apply H | apply refl_equal].
 Qed.

 Theorem fpbdexsnotnoti:forall (G:☐)(v:☈)(P:☊), G⊣ ☬☣(v∙¬¬P)->G⊣ ☬☣(v∙P).
 Proof.
  intros G v P; unfold fpbdexs; intros H; simpl;
   replace (∃fpbdloc v ☦ P) with 〈 ☦⍇fpbdloc v ☦ P@☋ ∃π(☦)〉;
   [apply notnotintro; apply H | apply refl_equal].
 Qed.

(* B-Book theorems ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* BB p36 §1.3.6 Th1.3.1 *)
 Theorem bbt1_3_1:forall (x:☈)(P R:☊), x⍀☋R->⊣ ☬☢(x∙¬P☲R) ☲ (¬ ☬☢(x∙P) ☲ R).
 Proof.
  intros x P R H; BTprop; apply biabsn with (P:=☬☢(x∙P)); BTprop.
   apply bifori; BTinf; apply biimpe; apply contrapn; apply iffrepimpr with (Q:=R).
    BTprop.
    rewrite (sym_id (fpaffexpid (¬P☲R) x)); apply bifore; rewrite (fpaffexpid (¬P☲R) x);
     BTprop.
 Qed.

 (* Coq    BTprop.
 version of Th1.3.1 *)
 Theorem bct1_3_1:forall (G:☐)(x:☈)(P Q:☊), x⍀☑(G ☑★ Q)->G⊣ ☬☢(x∙¬P☲Q)->G⊣¬(☬☢(x∙P)) ☲Q.
 Proof.
  intros G x P Q H H0; apply mp with (P:=☬☢(x∙¬P☲Q));
   [apply H0 | apply biemp; apply bbt1_3_1; apply H; BTin].
 Qed.

 (* BB p37 §1.3.6 Th1.3.2 *)
 Theorem bbt1_3_2:forall (x:☈)(E:☌)(P:☊), ⊣ 〈x♇☍E@☋¬P〉 ☲¬ ☬☢(x∙P).
 Proof.
  intros x E P; apply contrapn; apply iffrepimpl with (P:=☬☢(x∙P)); BTprop.
   simpl; BTprop; apply bifore; BTprop.
 Qed.

 (* BB p38 §1.3.7 DR9 *)
 Theorem bbdr9:forall (G:☐)(x:☈)(P R:☊), x⍀☑(G ☑★ R)->G⊣¬P☲R->G⊣¬(☬☢(x∙P)) ☲R.
 Proof.
  intros G x P R H H0; apply bct1_3_1;
   [apply H | apply bifori; [destruct (ginfinc _ _ _ H); apply g | apply H0]].
 Qed.

 (* BB p38 §1.3.7 DR10 *)
 Theorem bbdr10:forall (G:☐)(x:☈)(E:☌)(P:☊), G⊣ 〈x♇☍E@☋¬P〉->G⊣¬ ☬☢(x∙P).
 Proof.
  intros G x E P H; apply mp with (P:=〈x♇☍E@☋¬P〉); [apply H | apply biemp; apply bbt1_3_2].
 Qed.

 (* BB p38 §1.3.7 DR11 *)
 Theorem bbdr11:forall (G:☐)(x:☈)(E:☌)(P R:☊),☬☢(x∙P) ∈☑ G->G⊣ 〈x♇☍E@☋P〉 ☲ R->G⊣R.
 Proof.
  intros G x E P R H H0; apply mp with (P:=〈x♇☍E@☋P〉);
   [apply bifore; apply biin; apply H | apply H0].
 Qed.

 (* BB p39 §1.3.7 Th1.3.3 *)
 Theorem bbt1_3_3:forall (x:☈)(P Q:☊), ⊣ ☬☢(x∙P☲Q) ☲ (☬☢(x∙P) ☲ ☬☢(x∙Q)).
 Proof.
  intros x P Q; BTprop; apply bifori; BTinf.
   apply (bbdr11 ((∅☑ ☑★ ☬☢(x∙P☲Q)) ☑★ ☬☢(x∙P)) x χ(x) P Q);
    [BTin | rewrite (fpaffexpid P x); apply bifore2 with (v:=x); BTprop].
 Qed.

 (* BB p40 §1.3.8 DR12 *)
 Theorem bbdr12:forall (G:☐)(x:☈)(P R:☊), x⍀☑(G ☑★ R)->G⊣P☲R->G⊣ ☬☣(x∙P) ☲R.
 Proof.
  intros G x P Q H H0; rewrite (fpbdforexs P x); apply bbdr9;
   [apply H | apply iffrepimpl with (P:=P); [BTprop | apply H0]].
 Qed.
 (* Nota : this is a result providing a form of ∃-elimination, as shown by the following *)
 (* representation G☑★P⊣R->G☑★☬☣(x∙P)⊣R as long as x is fresh for G and R.                                  *)

 (* BB p40 §1.3.8 DR13 *)
 Theorem bbdr13:forall (G:☐)(x:☈)(E:☌)(P:☊), G⊣ 〈x♇☍E@☋P〉->G⊣ ☬☣(x∙P).
 Proof.
  intros G x E P H; rewrite (fpbdforexs P x); apply bbdr10 with (E:=E); simpl;
   apply iffrepgoal with (P:=〈x♇☍E@☋P〉); [BTprop | apply H].
 Qed.

 (* BB p40 §1.3.8 DR14 *)
 Theorem bbdr14:forall (G:☐)(x:☈)(P R:☊), G⊣ ☬☢(x∙¬P) ☲ R->G⊣¬(☬☣(x∙P)) ☲ R.
 Proof.
  intros G x P R H; rewrite (fpbdforexs P x); apply iffrepimpl with (P:=☬☢(x∙¬P));
   [BTprop | apply H].
 Qed.

 (* BB p40 §1.3.8 DR15 *)
 Theorem bbdr15:forall (G:☐)(x:☈)(P:☊), G⊣ ☬☢(x∙¬P)->G⊣¬ ☬☣(x∙P).
 Proof.
  intros G x P H; rewrite (fpbdforexs P x); apply iffrepgoal with (P:=☬☢(x∙¬P));
   [BTprop | apply H].
 Qed.

 (* A slightly different version of DR15 *)
 Theorem bbdr15':forall (G:☐)(v:☈)(P:☊), G⊣ ☬☢(v∙P)->G⊣¬ ☬☣(v∙¬P).
 Proof.
  intros G v P H; rewrite fpbdforexs; BTprop; apply fpbdfornotnote; apply H.
 Qed.

 (* BB p40 §1.3.8 DR16 *)
 Theorem bbdr16:forall (G:☐)(x:☈)(P:☊), G⊣ ☬☣(x∙¬P)->G⊣¬ ☬☢(x∙P).
 Proof.
  intros G x P H; rewrite <- (sym_id (fpbdforexs (¬P) x)) in H; apply biabsn with (P:=☬☢(x∙¬¬P));
   [apply fpbdfornotnote; apply biimpe; BTprop | apply biins; apply H].
 Qed.

 (* BB p40 §1.3.8 Th1.3.4 *)
 Theorem bbt1_3_4:forall (x:☈)(P Q:☊), ⊣ ☬☣(x∙P☲Q) ☳ (☬☢(x∙P) ☲ ☬☣(x∙Q)).
 Proof.
  intros x P Q; unfold piff; apply biandi; rewrite (fpbdforexs (P☲Q) x);
   rewrite (fpbdforexs Q x).
   apply biimpi; apply contrapn; apply iffrepimpl with (P:=☬☢(x∙¬Q)); [BTprop | idtac];
    apply biimpi; apply swaphyp; apply biimpe; apply contrapp; apply biimpi.
    destruct (exfgnewf (((∅☑ ☑★ ☬☢(x∙¬Q)) ☑★ ☬☢(x∙P)) ☑★ ¬(P☲Q))).
    apply bifori2 with (w:=x0); BTinf.
     simpl; apply mp with (P:=〈x♇☍χ (x0)@☋P〉);
      [apply bifore; BTprop | apply mp with (P:=〈x♇☍χ (x0)@☋¬Q〉);
                                                [apply bifore; BTprop | idtac]]; simpl; BTprop.
   apply bbdr7; rewrite (sym_id (fpbdforexs (P☲Q) x));
    [ apply bbdr9 | rewrite (sym_id (fpbdforexs Q x)); apply bbdr12]; BTinf; apply biimpi;
    apply bbdr13 with (E:=χ x); rewrite (fpaffexpid (P☲Q) x); BTprop.
 Qed.

(* B-Book classical results of §1.3.9 ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Coq version of the ☬☢ commutativity *)
 Theorem commfpbdfor':forall (G:☐)(x y:☈)(P:☊), G⊣ ☬☢(x∙☬☢(y∙P))->G⊣ ☬☢(y∙☬☢(x∙P)).
 Proof.
  intros G x y P H; destruct (deciequ x y).
   rewrite i; rewrite <- (sym_id i) in H; apply H.
   destruct (exfgnewf (G ☑★ P ☑★ χ(x) ∈ χ(y))).
   rename x0 into y0; assert (H1:y0⍀☑(G ☑★ ☬☢(x∙P))). BTinf.
   destruct (exfgnewf (G ☑★ 〈y♇☍χ(y0)@☋P〉 ☑★ χ(x) ∈ χ(y))).
   assert (H3:x0⍀☑(G ☑★ ☬☢(x∙〈y♇☍χ(y0)@☋P〉))). BTinf.
   destruct (ginfinc _ _ _ g0); apply (bifori2 G y y0 ☬☢(x∙P) H1).
   destruct (ginfinc _ _ _ g); invset_clear p0; invset_clear H0.
   assert (H4':!(x≡y0)). intros H4'; apply H4; rewrite H4'; apply refl_id.
   rewrite (commfpaffbdfor P χ(y0) y x (sym_nid e) (infvar _ _ H4')).
   apply (bifori2 G x x0 〈y♇☍χ(y0)@☋P〉 g1).
   invset_clear p; invset_clear H5.
   assert (H6':!(y≡x0)). intros H6'; apply H6; rewrite H6'; apply refl_id.
   rewrite (commfpaffexp P χ(x0) χ(y0) x y e (infvar _ _ H4') (infvar _ _ H6')).
   apply bifore.
   rewrite (sym_id (commfpaffbdfor P χ(x0) x y e (infvar _ _ H6'))).
   apply bifore.
   apply H.
 Qed.

 (* BB p41 §1.3.9, ☬☢ commutativity *)
 Theorem commfpbdfor:forall (P:☊)(x y:☈), ⊣ ☬☢(x∙☬☢(y∙P)) ☳ ☬☢(y∙☬☢(x∙P)).
 Proof.
  intros P x y; BTprop; apply commfpbdfor'; BTprop.
 Qed.

 (* Coq version of the ☬☣ commutativity *)
 Theorem commfpbdexs':forall (G:☐)(x y:☈)(P:☊), G⊣ ☬☣(x∙☬☣(y∙P))->G⊣ ☬☣(y∙☬☣(x∙P)).
 Proof.
  intros G x y P H; apply (mp _ _ ☬☣(y∙☬☣(x∙P)) H); rewrite (fpbdforexs ☬☣(y∙P) x);
   rewrite (fpbdforexs ☬☣(x∙P) y); rewrite (fpbdforexs P x); rewrite (fpbdforexs P y);
   apply contrapp; apply biimpi; apply fpbdfornotnote; apply commfpbdfor';
   apply fpbdfornotnoti; apply biimpe; apply contrapn; repeat (rewrite <- fpbdforexs);
   BTprop.
 Qed.

 (* BB p41 §1.3.9, ☬☣ commutativity *)
 Theorem commfpbdexs:forall (P:☊)(x y:☈), ⊣ ☬☣(x∙☬☣(y∙P)) ☳ ☬☣(y∙☬☣(x∙P)).
 Proof.
  intros P x y; BTprop; apply commfpbdexs'; BTprop.
 Qed.

 (* Coq version of the ☬☢ over ∧ distributivity *)
 Theorem disfpbdforand':forall (G:☐)(P Q:☊)(v:☈), G⊣ ☬☢(v∙P∧Q)->G⊣(☬☢(v∙P) ∧ ☬☢(v∙Q)).
 Proof.
  intros G P Q v H; destruct (exfgnewf (G ☑★ P ☑★ Q)); destruct (ginfinc _ _ _ g); clear g;
   assert (H0:x⍀☑(G ☑★ Q)). BTinf.
  apply biandi; [apply (bifori2 _ v _ _ g0)  | apply (bifori2 _ v _ _ H0)];
   generalize (bifore _ _ _ χ(x) H); intros H2; simpl in H2;
   [apply (biandl _ _ _ H2) | apply (biandr _ _ _ H2)].
 Qed.

 Theorem disfpbdforand'':forall (G:☐)(P Q:☊)(x:☈), G⊣(☬☢(x∙P) ∧ ☬☢(x∙Q))->G⊣ ☬☢(x∙P∧Q).
 Proof.
  intros G P Q v H; destruct (exfgnewf (G ☑★ P∧Q)); apply (bifori2 _ v _ _ g);
   generalize (bifore _ _ _ χ(x) (biandl _ _ _ H)); intros H1;
   generalize (bifore _ _ _ χ(x) (biandr _ _ _ H)); intros H2; apply (biandi _ _ _ H1 H2).
 Qed.

 (* BB p41 §1.3.9, ☬☢ over ∧ distributivity *)
 Theorem disfpbdforand:forall (P Q:☊)(x:☈), ⊣ ☬☢(x∙P∧Q)☳(☬☢(x∙P) ∧ ☬☢(x∙Q)).
 Proof.
  intros P x y; unfold piff; apply biandi; apply biimpi;
   [apply disfpbdforand' | apply disfpbdforand'']; BTprop.
 Qed.

 (* Coq version of the ☬☣ over ∨ distributivity *)
 Theorem disfpbdexsior':forall (G:☐)(P Q:☊)(v:☈), G⊣ ☬☣(v∙P∨Q)->G⊣ ☬☣(v∙P) ∨ ☬☣(v∙Q).
 Proof.
  intros G P Q v; unfold pior; intros H; rewrite (fpbdforexs P v); rewrite (fpbdforexs Q v);
   apply contrapp; rewrite (sym_id (fpbdforexs P v));
   apply (iffrepgoal _ _ _ (biinc _ _ _ (bbt1_3_4 v (¬Q) P) (empginc G)));
   apply (mp _ _ ☬☣(v∙¬Q☲P) H); rewrite (fpbdforexs (¬P☲Q) v); rewrite (fpbdforexs (¬Q☲P) v);
   apply contrapp; apply biimpi; apply (bfimprepbf (G ☑★ ☬☢(v∙¬(¬Q☲P))) v (¬(¬Q☲P)) (¬(¬P☲Q))).
    apply biins; destruct (exfgnewf (G ☑★ (¬(¬Q☲P)☲¬(¬P☲Q))));
     apply (bifori2 G v x (¬(¬Q☲P)☲¬(¬P☲Q)) g); simpl; BTprop.
   BTprop.
 Qed.

 Theorem disfpbdexsior'':forall (G:☐)(P Q:☊)(x:☈), G⊣ ☬☣(x∙P) ∨ ☬☣(x∙Q)->G⊣ ☬☣(x∙P∨Q).
 Proof.
  intros G P Q v; unfold pior; rewrite (fpbdforexs P v); rewrite (fpbdforexs Q v);
   rewrite (fpbdforexs (¬P☲Q) v); intros H; generalize (contrapn _ _ _ H); clear H; intros H;
   apply (mp _ _ (¬ ☬☢(v∙¬(¬P☲Q))) H); apply contrapn; apply biimpi;
   apply iffrephyp with (P:=☬☢(v∙¬(¬P☲Q))).
   BTprop.
   apply iffrepgoal with (P:=☬☢(v∙¬Q) ∧ ☬☢(v∙¬P)).
    BTprop.
    apply disfpbdforand'; apply bfimprepbf with (P:=¬(¬P☲Q)).
     apply biins; destruct (exfgnewf (G ☑★ ¬(¬P☲Q)☲¬Q∧¬P));
      apply (bifori2 G v x (¬(¬P☲Q)☲¬Q∧¬P) g); simpl; BTprop.
    BTprop.
 Qed.

 (* BB p41 §1.3.9, ☬☣ over ∨ distributivity *)
 Theorem disfpbdexsior:forall (P Q:☊)(x:☈), ⊣ ☬☣(x∙P∨Q)☳(☬☣(x∙P) ∨ ☬☣(x∙Q)).
 Proof.
  intros P x y; unfold piff; apply biandi; apply biimpi;
   [apply disfpbdexsior' | apply disfpbdexsior'']; BTprop.
 Qed.

 (* Variation of ☬☢ over ∧ distributivity with non freeness *)
 Theorem disfpbdforandnf:forall (P Q:☊)(x:☈), x⍀☋P->⊣ (P ∧ ☬☢(x∙Q))☳☬☢(x∙P∧Q).
 Proof.
  intros P Q x H; BTprop.
   apply disfpbdforand''; BTprop; apply bifori3; [apply H | BTprop].
   apply biandl with (Q:=Q); apply bifore2 with (v:=x); BTprop.
   apply biandr with (P:=☬☢(x∙P)); apply disfpbdforand'; BTprop.
 Qed.

 (* Coq version of the ☬☢ over ∨ distributivity with non freeness *)
 Theorem disfpbdforiornf':forall (G:☐)(P Q:☊)(v:☈), v⍀☋P->G⊣(P ∨ ☬☢(v∙Q))->G⊣ ☬☢(v∙P∨Q).
 Proof.
  intros G P Q v H; unfold pior; intros H0; destruct (exfgnewf (G ☑★ ¬P☲Q));
   apply (bifori2 _ v _ _ g); simpl; rewrite (fpaffexpnul P χ(x) v H); apply biimpi;
   apply (bifore (G ☑★ ¬P) v Q χ(x) (biimpe _ _ _ H0)).
 Qed.

 Theorem disfpbdforiornf'':forall (G:☐)(P Q:☊)(v:☈), v⍀☋P->G⊣ ☬☢(v∙P∨Q)->G⊣(P ∨ ☬☢(v∙Q)).
 Proof.
  intros G P Q v H; unfold pior; intros H0; destruct (exfgnewf (G☑★¬P☑★Q)); apply biimpi;
   apply (bifori2 _ v _ _ g); rewrite (sym_id (fpaffexpnul P χ(x) v H)); apply biimpe;
   apply (bifore G v (¬P☲Q) χ(x) H0).
 Qed.

 (* BB p42 §1.3.9, ☬☢ over ∨ distributivity with non freeness *)
 Theorem disfpbdforiornf:forall (P Q:☊)(x:☈), x⍀☋P->⊣ (P ∨ ☬☢(x∙Q))☳☬☢(x∙P∨Q).
 Proof.
  intros P Q x H ; unfold piff; apply biandi; apply biimpi.
   apply disfpbdforiornf'; [apply H | BTprop].
   apply disfpbdforiornf''; [apply H | BTprop].
 Qed.

 (* Coq version of the ☭☣ over ∧ distributivity with non freeness *)
 Theorem disfpbdexsiornf':forall (G:☐)(P Q:☊)(v:☈), v⍀☋P->G⊣P∧ ☬☣(v∙Q)->G⊣ ☬☣(v∙P∧Q).
 Proof.
  intros G P Q v H; rewrite (fpbdforexs Q v); rewrite (fpbdforexs (P∧Q) v); intros H0;
   apply (mp _ _ (¬ ☬☢(v∙¬(P∧Q))) H0); BTprop; apply biimpe; apply contrapp; apply biimpi;
   destruct (exfgnewf (G☑★P☑★ ☬☢(v∙¬(P∧Q))☑★¬Q)); apply (bifori2 _ v _ _ g); apply swaphyp;
   apply biimpe; assert (H2:forall (G:☐), G⊣ 〈v♇☍χ(x)@☋¬(P∧Q)〉->G⊣P☲〈v♇☍χ(x)@☋¬Q〉).
   intros G0 H2; simpl in H2; rewrite <- (sym_id (fpaffexpnul P χ(x) v H)) in H2;
    apply (mp _ _ (P☲〈v♇☍χ(x)@☋¬Q〉) H2); simpl; BTprop.
   apply H2; apply bifore; BTprop.
 Qed.

 Theorem disfpbdexsiornf'':forall (G:☐)(P Q:☊)(v:☈), v⍀☋P->G⊣ ☬☣(v∙P∧Q)->G⊣(P∧ ☬☣(v∙Q)).
 Proof.
  intros G P Q v H; rewrite (fpbdforexs Q v); rewrite (fpbdforexs (P∧Q) v); intros H0;
   apply biandi.
   apply (mp _ _ P H0); apply contrapn; BTprop; destruct (exfgnewf (G ☑★ ¬P ☑★ ¬(P∧Q)));
   apply (bifori2 _ v _ _ g); simpl; rewrite (fpaffexpnul P χ(x) v H); BTprop.
  apply (mp _ _ (¬ ☬☢(v∙¬Q)) H0); apply contrapp; apply biimpi; apply bfimprepbf with (P:=¬Q).
   apply biins; destruct (exfgnewf (G ☑★ ¬Q☲¬(P ∧ Q))); apply (bifori2 _ v _ _ g); simpl;
   BTprop.
   BTprop.
 Qed.

 (* BB p42 §1.3.9, ☭☣ over ∧ distributivity with non freeness *)
 Theorem disfpbdexsiornf:forall (P Q:☊)(x:☈), x⍀☋P->⊣ (P ∧ ☬☣(x∙Q))☳☬☣(x∙P∧Q).
 Proof.
  intros P Q x H; unfold piff; apply biandi; apply biimpi.
   apply disfpbdexsiornf'; [apply H | BTprop].
   apply disfpbdexsiornf''; [apply H | BTprop].
 Qed.

 (* Coq version of the ☬☢ over ☲ distributivity with non freeness *)
 Theorem disfpbdforimpnf':forall (G:☐)(P Q:☊)(v:☈), v⍀☋P->G⊣(P ☲ ☬☢(v∙Q))->G⊣ ☬☢(v∙P☲Q).
 Proof.
  intros G P Q v H H0; destruct (exfgnewf (G ☑★ P☲Q)); apply (bifori2 _ v _ _ g); simpl;
   rewrite (fpaffexpnul _ χ(x) _ H); apply biimpi;
   apply (bifore (G ☑★ P) v Q χ(x) (biimpe _ _ _ H0)).
 Qed.

 Theorem disfpbdforimpnf'':forall (G:☐)(P Q:☊)(v:☈), v⍀☋P->G⊣ ☬☢(v∙P☲Q)->G⊣(P ☲ ☬☢(v∙Q)).
 Proof.
  intros G P Q v H H0; apply biimpi; destruct (exfgnewf (G ☑★ P ☑★ Q)); apply (bifori2 _ v _ _ g);
   apply biimpe; rewrite (sym_id (fpaffexpnul _ χ(x) _ H)); apply (bifore _ _ _ χ(x) H0).
 Qed.

 (* BB p42 §1.3.9, ☬☢ over ☲ distributivity with non freeness *)
 Theorem disfpbdforimpnf:forall (P Q:☊)(x:☈), x⍀☋P->⊣ (P ☲ ☬☢(x∙Q))☳☬☢(x∙P☲Q).
 Proof.
  intros P Q x H; unfold piff; apply biandi; apply biimpi.
   apply disfpbdforimpnf'; [apply H | BTprop].
   apply disfpbdforimpnf''; [apply H | BTprop].
 Qed.

 (* Nota : other classical results of BB p42 §1.3.9 have been omitted.                                   *)

(* B-Book equality theorems ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* BB p43 §1.4 Th1.4.1 *)
 Theorem bbt1_4_1:forall (E F:☌), ⊣(E♃F)☲(F♃E).
 Proof.
  intros E F; apply biimpi; apply symequ; BTprop.
 Qed.

 (* BB p44 §1.4 Th1.4.2 *)
 Theorem bbt1_4_2:forall (E F:☌), ⊣(E♃F)☳(F♃E).
 Proof.
  intros E F; unfold piff; apply biandi; apply bbt1_4_1.
 Qed.

 (* BB p44 §1.4 Th1.4.3 *)
 Theorem bbt1_4_3:forall (E F G:☌), ⊣(E♃F)∧(F♃G)☲(E♃G).
 Proof.
  intros E F G; BTprop;  assert (H:{x:☈ & x⍀☍G}). exists ☰☍(G); apply fenewfinf.
  destruct H; assert (H0:E♃G= 〈x♇☍E@☋χ(x)♃G〉).
   simpl; rewrite (reflfiequ x); rewrite (feaffexpnul _ E _ e); apply refl_equal.
  rewrite H0; apply bieque with (E:=F);
   [apply symequ | simpl; rewrite (reflfiequ x); rewrite (feaffexpnul _ F _ e)]; BTprop.
 Qed.

 (* BB p45 §1.4 Th1.4.4 *)
 Theorem bbt1_4_4:forall (P:☊)(E:☌)(x:☈), x⍀☍E->⊣ ☬☢(x∙χ(x) ♃ E☲P)☲〈x♇☍E@☋P〉.
 Proof.
  intros P E x H; apply biimpi; apply mp with (〈x♇☍E@☋χ(x)♃E☲P〉).
   apply (bifore (∅☑ ☑★ ☬☢(x∙χ(x)♃E☲P)) x (χ(x)♃E☲P) E); BTprop.
   apply biins; simpl; rewrite (reflfiequ x); rewrite (feaffexpnul _ E _ H); apply biimpi;
    apply mp with (P:=E ♃ E); [apply biequi | BTprop].
 Qed.

 (* BB p45 §1.4 Th1.4.5 *)
 Theorem bbt1_4_5:forall (P:☊)(E:☌)(x:☈), x⍀☍E->⊣ 〈x♇☍E@☋P〉 ☲ ☬☢(x∙χ(x) ♃ E☲P).
 Proof.
  intros P E x H; apply biimpi; apply bifori; BTinf; apply biimpi;
   apply iffrephyp with (P:=E ♃ χ(x));
   [apply biins; apply bbt1_4_2
   | rewrite (sym_id (fpaffexpid P x)); apply bieque with (E:=E);
     [idtac | rewrite (fpaffexpid P x)]; BTprop].
 Qed.

 (* BB p46 §1.4 Th1.4.6 *)
 Theorem bbt1_4_6:forall (P:☊)(E:☌)(x:☈), x⍀☍E->⊣ ☬☢(x∙χ(x) ♃ E☲P) ☳ 〈x♇☍E@☋P〉.
 Proof.
  intros P E x H; unfold piff; apply biandi; [apply bbt1_4_4 | apply bbt1_4_5]; apply H.
 Qed.

 (* BB p46 §1.4 Th1.4.7 *)
 Theorem bbt1_4_7:forall (P:☊)(E:☌)(x:☈), x⍀☍E->⊣ ☬☣(x∙χ(x) ♃ E∧P) ☳ 〈x♇☍E@☋P〉.
 Proof.
  intros P E x H; rewrite (fpbdforexs (χ(x) ♃ E∧P) x); unfold piff; apply biandi;
   apply biimpi.
   apply iffrepgoal with (P:=¬ 〈x ♇☍ E @☋ ¬P〉).
    simpl; BTprop.
    apply biimpe; apply contrapp;
     apply (iffrepimpl _ _ _ ☬☢(x∙¬(χ(x)♃E∧P)) (bbt1_4_6 (¬P) E x H)); apply biimpi;
     apply bfimprepbf with (P:=χ(x)♃E☲¬P);
     [apply biins; apply bifori; [apply ginfempty | BTprop] | BTprop].
   apply iffrephyp with (P:=¬ 〈x ♇☍ E @☋ ¬P〉).
    simpl; BTprop.
    apply biimpe; apply contrapp;
     apply (iffrepimpr _ ☬☢(x∙¬(χ(x)♃E∧P)) _ _ (bbt1_4_6 (¬P) E x H));
     apply biimpi; apply bfimprepbf with (P:=¬((χ(x)) ♃ E ∧ P));
     [apply biins; apply bifori; [apply ginfempty | BTprop] | BTprop].
 Qed.

 (* BB p47 §1.4 EQL1 *)
 Theorem bbeql1:forall (G:☐)(P Q R:☊)(E F:☌)(x:☈),
                             E♃F∈☑G->P∈☑G->⊣P ☳ 〈x♇☍E@☋Q〉->G⊣ 〈x♇☍F@☋Q〉 ☲ R->G⊣R.
 Proof.
  intros G P Q R E F x H H0 H1 H2; apply mp with (P:=〈x♇☍F@☋Q〉).
   apply (bieque G x Q E F);
    [apply biin; apply H
    | apply iffrepgoal with (P:=P); [apply biemp; apply H1 | apply biin; apply H0]].
   apply H2.
 Qed.

 (* A slightly more powerful version of EQL1 *)
 Theorem bbeql1':forall (G:☐)(P Q R:☊)(E F:☌)(x:☈),
                              G⊣E♃F->G⊣P->G⊣P ☲ 〈x♇☍E@☋Q〉->G⊣ 〈x♇☍F@☋Q〉 ☲ R->G⊣R.
 Proof.
  intros G P Q R E F x H H0 H1 H2; apply mp with (P:=〈x♇☍F@☋Q〉);
   [apply (bieque G x Q E F H); apply mp with (P:=P); [apply H0 | apply H1] | apply H2].
 Qed.

 (* BB p47 §1.4 EQL2 *)
 Theorem bbeql2:forall (G:☐)(P R:☊)(E F:☌)(x:☈),E♃F∈☑G->⊣R ☳ 〈x♇☍E@☋P〉->G⊣ 〈x♇☍F@☋P〉->G⊣R.
 Proof.
  intros G P R E F x H H0 H1; apply iffrepgoal with (P:=〈x♇☍E@☋P〉);
   [apply symiff; apply biemp; apply H0
   | apply (bieque G x P F E); [apply symequ; apply biin; apply H | apply H1]].
 Qed.

 (* A slightly more powerful version of EQL2 *)
 Theorem bbeql2':forall (G:☐)(P R:☊)(E F:☌)(x:☈),G⊣E♃F->G⊣ 〈x♇☍E@☋P〉 ☲ R->G⊣ 〈x♇☍F@☋P〉->G⊣R.
 Proof.
  intros G P R E F x H H0 H1; apply mp with (P:=〈x♇☍E@☋P〉);
   [apply (bieque G x P F E); [apply symequ; apply H | apply H1] | apply H0].
 Qed.

 (* BB p47 §1.4 EQL3 *)
 Theorem bbeql4:forall (G:☐)(R:☊)(E F:☌),E♃F∈☑G->G☑★(F♃E)⊣R->G⊣R.
 Proof.
  intros G R E F H H0; apply mp with (P:=F♃E);
   [apply symequ; apply biin; apply H | apply biimpi; apply H0].
 Qed.

(* B-Book ordered pair theorems ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* The ordered pair, noted in the B-Book both with ☵ or , is used to extend some of the     *)
(* notations. For example, affectation over list of variables is now defined, as well    *)
(* as quantification over multiples variables. Yet all those notations are defined by *)
(* using the previous definitions over single variables and are not required. The         *)
(* interesting result is the equality of pairs... but the use of both the ☵ and , symbols *)
(* leads to a confusion, making the B-Book proof invalid.                                                       *)

 (* BB p51 §1.5 Th1.5.5 *)
 Theorem bbt1_5_5:forall (G:☐)(E F E' F':☌), G⊣(E☵F)♃(E'☵F')->G⊣E♃E'∧F♃F'.
 Proof.
  intros G E F E' F' H; apply biandi; [apply (bicpll _ _ _ _ _ H) | apply (bicplr _ _ _ _ _ H)].
 Qed.

(* B-Book still missing inference rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem biprol:forall (G:☐)(E F S T:☌), G⊣(E☵F)∈(S♂T)->G⊣E∈S.
 Proof.
  intros G E F S T H; destruct (exfgnewf (G☑★(E☵F)∈(S♂T))); destruct (ginfinc _ _ _ g); clear g;
   destruct (exfgnewf (G☑★(E☵F)∈(S♂T)☑★χ(x)∈χ(x))); destruct (ginfinc _ _ _ g);
   destruct (ginfinc _ _ _ g1); invset_clear p0; invset_clear H0; clear g g1 H1.
  apply mp with (P:= ☬☣(x0∙☬☣(x∙χ(x0)∈S∧χ(x)∈T∧(E☵F)♃(χ(x0)☵χ(x))))).
   apply mp with (P:=(E☵F)∈(S♂T));
    [apply H | apply riffimp; apply bipro; [apply p1 | apply H2 | apply p]].
   apply bbdr12; BTinf; apply bbdr12; BTinf; BTprop.
   apply mp with (P:=E♃χ(x0)).
    apply bicpll with (F:=F)(F':=χ(x)); BTprop.
    apply biimpi; replace (E∈S) with 〈x0♇☍E@☋χ(x0)∈S〉.
     apply bieque with (E:=χ(x0)); [apply symequ | rewrite fpaffexpid]; BTprop.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul S E x0); [apply refl_equal | BTinf].
 Qed.

 Theorem bipror:forall (G:☐)(E F S T:☌), G⊣(E☵F)∈(S♂T)->G⊣F∈T.
 Proof.
  intros G E F S T H; destruct (exfgnewf (G☑★(E☵F)∈(S♂T))); destruct (ginfinc _ _ _ g); clear g;
   destruct (exfgnewf (G☑★(E☵F)∈(S♂T)☑★χ(x)∈χ(x))); destruct (ginfinc _ _ _ g);
   destruct (ginfinc _ _ _ g1); invset_clear p0; invset_clear H0; clear g g1 H1.
  apply mp with (P:= ☬☣(x0∙☬☣(x∙χ(x0)∈S∧χ(x)∈T∧(E☵F)♃(χ(x0)☵χ(x))))).
   apply mp with (P:=(E☵F)∈(S♂T));
    [apply H | apply riffimp; apply bipro; [apply p1 | apply H2 | apply p]].
   apply bbdr12; BTinf; apply bbdr12; BTinf; BTprop.
   apply mp with (P:=F♃χ(x)).
    apply bicplr with (E:=E)(E':=χ(x0)); BTprop.
    apply biimpi; replace (F∈T) with 〈x♇☍F@☋χ(x)∈T〉.
     apply bieque with (E:=χ(x)); [apply symequ | rewrite fpaffexpid]; BTprop.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul T F x); [apply refl_equal | BTinf].
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
