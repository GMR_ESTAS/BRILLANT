(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Bset, set notations and results
   The predicate calculus is extended with new definitions related to sets, for which
   various theorems are provided.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : July 2006 - October 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Ppred.

(* Inclusion rule ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem incins:forall (G:☐)(S T:☌), G⊣S∈(☬T)->(forall (E:☌), G⊣E∈S->G⊣E∈T).
 Proof.
  intros G S T H E H0; apply mp with (P:=E∈S);
   [apply H0 | generalize E; clear E H0; apply bipoweexp; apply H].
 Qed.

(* B-Book properties ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* BB p62 §2.1.3 Property 1 *)
 Theorem reflinc:forall (S:☌), ⊣S⊆S.
 Proof.
  intros S; unfold pinc; apply bipowi2; intros i; BTprop.
 Qed.

 (* BB p62 §2.1.3 Property 2 *)
 Theorem transinc:forall (S T U:☌), ⊣S⊆T∧T⊆U☲S⊆U.
 Proof.
  intros S T U; unfold pinc; BTprop; apply bipowi2; intros i; apply biimpi;
   apply mp with (P:=χ(i)∈T).
   apply biimpe; apply biins; apply bipowe2; unfold pinc; BTprop.
   apply biins; apply bipowe2; unfold pinc; BTprop.
 Qed.

 (* BB p62 §2.1.3 Property 3 *)
 Theorem asympinc:forall (S T:☌), ⊣S⊆T∧T⊆S☲S♃T.
 Proof.
  intros S T; unfold pinc; apply diffimp; apply bieqs.
 Qed.

 (* BB p62 §2.1.3 Property 4 *)
 Theorem monoincpro:forall (S T U V:☌), ⊣U⊆S∧V⊆T☲(U♂V)⊆(S♂T).
 Proof.
  intros S T U V; BTprop; unfold pinc.
  destruct (exfgnewf (∅☑☑★(U♂V)∈(☬(S♂T)))); destruct (ginfinc _ _ _ g); clear g g0.
  apply bipowi with (f:=x); BTinf; apply bifori; BTinf; apply biimpi.
  destruct (exfgnewf (∅☑☑★χ(x)∈(U♂V)☑★χ(x)∈(S♂T))); destruct (ginfinc _ _ _ g);
   destruct (ginfinc _ _ _ g0); clear g g0 g1.
  destruct (exfgnewf (∅☑☑★χ(x)∈(U♂V)☑★χ(x)∈(S♂T)☑★χ(x0)∈χ(x0))); destruct (ginfinc _ _ _ g);
   destruct (ginfinc _ _ _ g0); destruct (ginfinc _ _ _ g1); clear g g0 g1 g2;
   invset_clear p2; clear H; invset_clear H0.
  apply iffrephyp with (P:=(☬☣(x1∙☬☣(x0∙χ(x1)∈U∧χ(x0)∈V∧χ(x)♃(χ(x1)☵χ(x0)))))).
   apply bipro; BTinf.
   apply biimpe; apply bbdr12; BTinf; apply bbdr12; BTinf; BTprop.
   replace (χ(x)∈(S♂T)) with 〈x0♇☍χ(x)@☋χ(x0)∈(S♂T)〉.
    apply bieque with (E:=(χ(x1)☵χ(x0))).
     apply symequ; BTprop.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul S (χ(x1)☵χ(x0)) x0).
      rewrite (feaffexpnul T (χ(x1)☵χ(x0)) x0).
       apply biproi2.
        apply incins with (S:=U); BTprop.
        apply incins with (S:=V); BTprop.
       BTinf.
      BTinf.
    simpl; rewrite reflfiequ; rewrite (feaffexpnul S χ(x) x0).
     rewrite (feaffexpnul T χ(x) x0).
      apply refl_equal.
      BTinf.
     BTinf.
 Qed.
(* Nota: this theorem does not appear to be provable with the standard B rules; indeed,  *)
(* the rules includes a statement (E☵F)∈(S♂T) but no rules allow to deal with E∈(S♂T)...       *)
(* It seems that what we need is a rule of the form E∈(S♂T)☳∃E',E''.E'∈S∧E''∈T∧E♃(E'☵E'').       *)

 (* BB p62 §2.1.3 Property 5 *)
 Theorem monoincpow:forall (S T:☌), ⊣S⊆T☲(☬S)⊆(☬T).
 Proof.
  intros S T; apply biimpi; unfold pinc; apply bipowiexp; intros E; apply biimpi;
   apply bipowiexp; intros F; apply biimpi; apply incins with (S:=S);
   [idtac | apply incins with (S:=E)]; BTprop.
 Qed.

 (* BB p62 §2.1.3 Property 8 *)
 Theorem incintrans:forall (S T E:☌), ⊣E∈S∧S⊆T☲E∈T.
 Proof.
  intros S T E; BTprop; apply swaphyp; apply biimpe; unfold pinc; apply bipoweexp; BTprop.
 Qed.

(* Derived constructs ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* The union defined in the B-Book has several rather astonishing particularities.      *)
(* First of all it is defined for two expressions S and T under the assumption that        *)
(* there is a "superset" U including both S and T, by {x|x∈U∧(x∈S∨x∈T)}; second one may      *)
(* note that while x should not be free in U (in which case no result about membership    *)
(* can be derived), x may be free in S and T. So union({y|y∈ℕ∧y>x},{y|y∈ℕ∧y>2x}) may be       *)
(* defined as {x|x∈ℕ∧(x∈{y|y∈ℕ∧y>x}∨x∈{y|y∈ℕ∧y>2x}}={x|x∈ℕ∧(x>x)∨(x>2x)}.                         *)
(* This does not appear to be right, as the choice of another name for the bound               *)
(* variable would lead to a different result... We therefore choose a new approach.         *)

 Definition Bempty:=♄♀(¬χ(☦)∈ ♄).
 Notation "∅☇":=(Bempty).

 Theorem bemptyin:forall (G:☐)(E:☌), G⊣¬E∈ ∅☇.
 Proof.
  intros G E; unfold Bempty; apply biabsp with (P:=E∈ ♄);
   [apply dbcmpl with (P:=¬χ(☦)∈ ♄) | apply (dbcmpr (G☑★E∈(♄♀(¬χ(☦)∈ ♄))) E ♄ (¬χ(☦)∈ ♄))]; BTprop.
 Qed.

 Theorem bemptyunique:forall (G:☐)(S:☌), (forall (E:☌), G⊣¬E∈S)->G⊣S♃ ∅☇.
 Proof.
  intros G S H; apply bieqsi; apply bipowiexp; intros E; apply biimpi.
   apply biabsn with (P:=E∈S); [BTprop | apply biins; apply biins; apply H].
   apply biabsn with (P:=E∈ ∅☇); [BTprop | apply biins; apply biins; apply bemptyin].
 Qed.

 Theorem bemptypow:forall (G:☐)(S:☌), G⊣ ∅☇ ∈(☬S).
 Proof.
  intros G S; apply bipowiexp; intros E; apply biimpi; apply biabsn with (P:=E∈ ∅☇);
   [BTprop | apply biins; apply biins; apply bemptyin].
 Qed.

 Definition Bunion(U:☌)(S T:☌):=let x:=☰☍((S♂T)) in ☬☤(x∙U∙χ(x)∈S∨χ(x)∈T).
 Notation "'∪☇(' U ':>' S ',' T ')'":=(Bunion U S T).

 Theorem bunioneexp:forall (G:☐)(S T U E:☌), G⊣E∈ ∪☇(U:>S,T)->G⊣E∈S∨E∈T.
 Proof.
  intros G S T U E; unfold Bunion; intros H1; generalize (bicmpr _ _ _ _ _ H1); unfold pior;
   unfold fpaffexp; fold feaffexp; rewrite reflfiequ.
  rewrite (feaffexpnul S E ☰☍((S♂T))).
   rewrite (feaffexpnul T E ☰☍((S♂T))).
    intros H2; apply H2.
    apply emajinf; simpl; apply emajige with (i:=☰☍(T)); [apply fimaxger | apply fenewfmaj].
    apply emajinf; simpl; apply emajige with (i:=☰☍(S)); [apply fimaxgel | apply fenewfmaj].
 Qed.

 Theorem bunioniexp:forall (G:☐)(S T E:☌),
                                     (G⊣E∈S)+(G⊣E∈T)->forall (U:☌), G⊣S⊆U->G⊣T⊆U->G⊣E∈ ∪☇(U:>S,T).
 Proof.
  intros G S T E H U HS HT; unfold Bunion; apply bicmpi.
   destruct H; [apply incins with (S:=S); [apply HS | apply b]
                        | apply incins with (S:=T); [apply HT | apply b]].
   unfold pior; unfold fpaffexp; fold feaffexp; rewrite reflfiequ;
    rewrite (feaffexpnul S E ☰☍((S♂T))).
    rewrite (feaffexpnul T E ☰☍((S♂T))).
     apply biimpi; destruct H.
      apply biabsn with (P:=E∈S); [apply biins; apply biins; apply b | BTprop].
      apply biins; apply b.
     apply emajinf; simpl; apply emajige with (i:=☰☍(T)); [apply fimaxger | apply fenewfmaj].
    apply emajinf; simpl; apply emajige with (i:=☰☍(S)); [apply fimaxgel | apply fenewfmaj].
 Qed.

 Definition Binter(S T:☌):=S♀(χ(☦)∈ ☯☍(T)).
 Notation "'∩☇(' S ',' T ')'":=(Binter S T).

 Definition Bdiff(S T:☌):=S♀(¬χ(☦)∈ ☯☍(T)).
 Notation "'∕☇(' S ',' T ')'":=(Bdiff S T).

 Definition Bpow1(S:☌):=∕☇(☬S,∅☇).
 Notation "☬☧(S)":=(Bpow1 S).

(* Binary relations ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition Brel(U V:☌):=☬(U♂V).
 Notation "'(' U '☶' V ')'":=(Brel U V).

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
