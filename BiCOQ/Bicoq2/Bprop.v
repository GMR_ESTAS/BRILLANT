(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Bprop, propositional calculus
   Various proofs of the B-Book, related to propositional calculus, are reproduced in
   this module; furthermore, the automated proof method proposed by Abrial is
   implemented both as a Coq tactic and a prover function.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - April 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Binfind.

(* Simple theorems for optimising proofs ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Excluded middle *)
 Theorem pem:forall (G:☐)(P:☊), G⊣☋P∨¬P.
 Proof.
   intros G P; unfold pior; apply bpiimpi; BThyp'.
 Qed.

 Theorem em:forall (G:☐)(P:☊), G⊣P∨¬P.
 Proof.
   intros G P; apply (proptopred (pem G P)).
 Qed.

 (* Absurd hypothesis *)
 Theorem pabsurdn:forall (G:☐)(P Q:☊), G⊣☋P->G☑★¬P⊣☋Q.
 Proof.
  intros G P Q H; apply bpiabsn with (P:=P); BThyp'; apply H.
 Qed.

 Theorem absurdn:forall (G:☐)(P Q:☊), G⊣P->G☑★¬P⊣Q.
 Proof.
  intros G P Q H; apply biabsn with (P:=P); BThyp; apply H.
 Qed.

 Theorem pabsurdp:forall (G:☐)(P Q:☊), G⊣☋¬P->G☑★P⊣☋Q.
 Proof.
  intros G P Q H; apply bpiabsn with (P:=P); BThyp'; apply H.
 Qed.

 Theorem absurdp:forall (G:☐)(P Q:☊), G⊣¬P->G ☑★ P⊣Q.
 Proof.
  intros G P Q H; apply biabsn with (P:=P); BThyp; apply H.
 Qed.

 (* ∨-intro left *)
 Theorem bpioril:forall (G:☐)(P Q:☊), G⊣☋P->G⊣☋P∨Q.
 Proof.
  intros G P Q H; unfold pior; apply bpiimpi; apply pabsurdn; apply H.
 Qed.

 Theorem bioril:forall (G:☐)(P Q:☊), G⊣P->G⊣P∨Q.
 Proof.
  intros G P Q H; unfold pior; apply biimpi; apply absurdn; apply H.
 Qed.

 (* ∨-intro right *)
 Theorem bpiorir:forall (G:☐)(P Q:☊), G⊣☋Q->G⊣☋P∨Q.
 Proof.
  intros G P Q H; unfold pior; apply bpiimpi; apply bpiins; apply H.
 Qed.

 Theorem biorir:forall (G:☐)(P Q:☊), G⊣Q->G⊣P∨Q.
 Proof.
  intros G P Q H; unfold pior; apply biimpi; apply biins; apply H.
 Qed.

 (* ¬¬ elimination *)
 Theorem pnotnotinf:forall (G:☐)(P:☊), G⊣☋¬¬P->G⊣☋P.
 Proof.
  intros G P H; apply bpiabsn with (P:=¬P); BThyp'; apply H.
 Qed.

 Theorem notnotinf:forall (G:☐)(P:☊), G⊣¬¬P->G⊣P.
 Proof.
  intros G P H; apply biabsn with (P:=¬P); BThyp; apply H.
 Qed.

 (* Contraposition *)
 Theorem pcontrapn:forall (G:☐)(P Q:☊), G⊣☋¬Q☲¬P->G⊣☋P☲Q.
 Proof.
  intros G P Q H; apply bpiimpi; apply bpiabsn with (P:=P);
   [BThyp' | apply bpiimpe; BThyp'; apply H].
 Qed.

 Theorem contrapn:forall (G:☐)(P Q:☊), G⊣¬Q☲¬P->G⊣P☲Q.
 Proof.
  intros G P Q H; apply biimpi; apply biabsn with (P:=P);
   [BThyp | apply biimpe; BThyp; apply H].
 Qed.

 Theorem pcontrapn':forall (G:☐)(P Q:☊), G⊣☋¬Q☲P->G⊣☋¬P☲Q.
 Proof.
  intros G P Q H; apply bpiimpi; apply bpiabsn with (P:=P);
   [apply bpiimpe; apply bpiins; apply H | apply bpiins; apply bpiaxm].
 Qed.

 Theorem contrapn':forall (G:☐)(P Q:☊), G⊣¬Q☲P->G⊣¬P☲Q.
 Proof.
  intros G P Q H; apply biimpi; apply biabsn with (P:=P);
   [apply biimpe; apply biins; apply H | apply biins; apply biaxm].
 Qed.

 Theorem pcontrapp:forall (G:☐)(P Q:☊), G⊣☋P☲Q->G⊣☋¬Q☲¬P.
 Proof.
  intros G P Q H; apply bpiimpi; apply bpiabsp with (P:=Q);
   [apply bpiimpe; BThyp'; apply H | BThyp'].
 Qed.

 Theorem contrapp:forall (G:☐)(P Q:☊), G⊣P☲Q->G⊣¬Q☲¬P.
 Proof.
  intros G P Q H; apply biimpi; apply biabsp with (P:=Q);
   [apply biimpe; BThyp; apply H | BThyp].
 Qed.

 Theorem pcontrapp':forall (G:☐)(P Q:☊), G⊣☋P☲¬Q->G⊣☋Q☲¬P.
 Proof.
  intros G P Q H; apply bpiimpi; apply bpiabsp with (P:=Q);
   [apply bpiins; apply bpiaxm | apply bpiimpe; apply bpiins; apply H].
 Qed.

 Theorem contrapp':forall (G:☐)(P Q:☊), G⊣P☲¬Q->G⊣Q☲¬P.
 Proof.
  intros G P Q H; apply biimpi; apply biabsp with (P:=Q);
   [apply biins; apply biaxm | apply biimpe; apply biins; apply H].
 Qed.

(* First proofs, most from the B-Book ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* BB p12 §1.2.2, a.k.a. Modus Ponens *)
 Theorem pmp:forall (G:☐)(P Q:☊), G⊣☋P->G⊣☋P☲Q->G⊣☋Q.
 Proof.
  intros G P Q H H0; apply bpiimp with (P:=P); [apply H | apply bpiimpe; apply H0].
 Qed.

 Theorem mp:forall (G:☐)(P Q:☊), G⊣P->G⊣P☲Q->G⊣Q.
 Proof.
  intros G P Q H H0; apply biimp with (P:=P); [apply H | apply biimpe; apply H0].
 Qed.

 (* ∧-elim in ☐ *)
 Theorem pandhypins:forall (G:☐)(P Q R:☊), G☑★P∧Q⊣☋R->G☑★P☑★Q⊣☋R.
 Proof.
  intros G P Q R H; apply pmp with (P:=P∧Q);
   [apply bpiandi; BThyp' | BThyp'; apply (bpiimpi _ _ _ H)].
 Qed.

 Theorem andhypins:forall (G:☐)(P Q R:☊), G☑★P∧Q⊣R->G☑★P☑★Q⊣R.
 Proof.
  intros G P Q R H; apply mp with (P:=P∧Q);
   [apply biandi; BThyp | BThyp; apply (biimpi _ _ _ H)].
 Qed.

 (* ☐ swap *)
 Theorem pswaphyp:forall (G:☐)(P Q R:☊), G☑★P☑★Q⊣☋R->G☑★Q☑★P⊣☋R.
 Proof.
  intros G P Q R H; apply (bpiinc (G☑★Q☑★P) (G☑★P☑★Q));[apply H | apply gincswp; apply reflginc].
 Qed.

 Theorem swaphyp:forall (G:☐)(P Q R:☊), G☑★P☑★Q⊣R->G☑★Q☑★P⊣R.
 Proof.
  intros G P Q R H; apply (biinc (G☑★Q☑★P) (G☑★P☑★Q));[apply H | apply gincswp; apply reflginc].
 Qed.

 (* ∧-intro in ☐ *)
 Theorem pinshypand:forall (G:☐)(P Q R:☊), G☑★P☑★Q⊣☋R->G☑★P∧Q⊣☋R.
 Proof.
  intros G P Q R H; apply bpiabsn with (P:=P).
   apply bpiins; apply bpiandl with (Q:=Q); BThyp'.
   apply bpiimpe; apply pcontrapp; apply bpiimpi; apply pswaphyp;
    apply bpiabsn with (P:=Q);
    [apply bpiins; apply bpiandr with (P:=P); BThyp'
    | apply bpiimpe; apply pcontrapp; apply bpiimpi; apply pswaphyp; apply bpiins;
      apply H].
 Qed.

 Theorem inshypand:forall (G:☐)(P Q R:☊), G☑★P☑★Q⊣R->G☑★P∧Q⊣R.
 Proof.
  intros G P Q R H; apply biabsn with (P:=P).
   apply biins; apply biandl with (Q:=Q); BThyp.
   apply biimpe; apply contrapp; apply biimpi; apply swaphyp; apply biabsn with (P:=Q);
    [apply biins; apply biandr with (P:=P); BThyp
    | apply biimpe; apply contrapp; apply biimpi; apply swaphyp; apply biins; apply H].
 Qed.

 (* BB p15 §1.2.3 Th1.2.1 *)
 Theorem pbbt1_2_1:forall (P:☊), ⊣☋P☲¬¬P.
 Proof.
  intros P; apply bpiimpi; apply bpiabsp with (P:=P); BThyp'.
 Qed.

 Theorem bbt1_2_1:forall (P:☊), ⊣P☲¬¬P.
 Proof.
  intros P; apply (proptopred (pbbt1_2_1 P)).
 Qed.

 (* BB p17 §1.2.3 Th1.2.2 *)
 Theorem pbbt1_2_2:forall (P Q:☊), ⊣☋P∧¬Q☲¬(P☲Q).
 Proof.
  intros P Q; apply bpiimpi; apply bpiabsp with (P:=Q);
   [apply pmp with (P:=P); [apply bpiandl with (Q:=¬Q) | idtac]| apply bpiandr with (P:=P)];
   BThyp'.
 Qed.

 Theorem bbt1_2_2:forall (P Q:☊), ⊣P∧¬Q☲¬(P☲Q).
 Proof.
  intros P Q; apply (proptopred (pbbt1_2_2 P Q)).
 Qed.

 (* BB p18 §1.2.3 Th1.2.3 *)
 Theorem pbbt1_2_3:forall (P Q:☊), ⊣☋(P☲¬Q) ☲ ¬(P∧Q).
 Proof.
  intros P Q; apply bpiimpi; apply bpiabsp with (P:=Q);
   [apply bpiandr  with (P:=P)
   |  apply pinshypand; apply bpiins; apply pmp with (P:=P)]; BThyp'.
 Qed.

 Theorem bbt1_2_3:forall (P Q:☊), ⊣(P☲¬Q) ☲ ¬(P∧Q).
 Proof.
  intros P Q; apply (proptopred (pbbt1_2_3 P Q)).
 Qed.

 (* BB p19 §1.2.3 Th 1.2.4 *)
 Theorem pbbt1_2_4:forall (P Q:☊), ⊣☋(P☲Q) ☲ (¬¬P☲Q).
 Proof.
  intros P Q; apply bpiimpi; apply bpiimpi; apply pmp with (P:=P);
   [apply pnotnotinf | idtac]; BThyp'.
 Qed.

 Theorem bbt1_2_4:forall (P Q:☊), ⊣(P☲Q) ☲ (¬¬P☲Q).
 Proof.
  intros P Q; apply (proptopred (pbbt1_2_4 P Q)).
 Qed.

 (* BB p19 §1.2.3 Th1.2.5 *)
 Theorem pbbt1_2_5:forall (P Q R:☊), ⊣☋(P☲(¬Q☲R)) ☲ (¬(P☲Q) ☲ R).
 Proof.
  intros P Q R; apply bpiimpi; apply bpiimpi; apply bpiabsn with (P:=P☲Q).
   apply bpiimpi; apply bpiabsn with (P:=R);
    [apply pmp with (P:=¬Q); [idtac | apply pmp with (P:=P)] | idtac]; BThyp'.
   BThyp'.
 Qed.

 Theorem bbt1_2_5:forall (P Q R:☊), ⊣(P☲(¬Q☲R)) ☲ (¬(P☲Q) ☲ R).
 Proof.
  intros P Q R; apply (proptopred (pbbt1_2_5 P Q R)).
 Qed.

 (* BB p20 §1.2.3 Th1.2.6 *)
 Theorem pbbt1_2_6:forall (P Q R:☊), ⊣☋(¬P☲R) ∧ (¬Q☲R) ☲ (¬(P∧Q) ☲ R).
 Proof.
  intros P Q R; apply bpiimpi; apply pinshypand; apply bpiimpi;
   apply bpiabsn with (P:=P∧Q).
   apply bpiandi; apply bpiabsn with (P:=R);
    [apply pmp with (P:=¬P) | idtac | apply pmp with (P:=¬Q) | idtac]; BThyp'.
   BThyp'.
 Qed.

 Theorem bbt1_2_6:forall (P Q R:☊), ⊣(¬P☲R) ∧ (¬Q☲R) ☲ (¬(P∧Q) ☲ R).
 Proof.
  intros P Q R; apply (proptopred (pbbt1_2_6 P Q R)).
 Qed.

 (* BB p21 §1.2.3 Th1.2.7 *)
 Theorem pbbt1_2_7:forall (P Q R:☊), ⊣☋(¬P☲R) ∧ (Q☲R) ☲ ((P☲Q) ☲ R).
 Proof.
  intros P Q R; apply bpiimpi; apply pinshypand; apply bpiimpi;
   apply bpiabsn with (P:=P☲Q).
   BThyp'.
   apply pmp with (P:=P∧¬Q).
    apply bpiandi;
     [apply bpiabsn with (P:=R); [apply pmp with (P:=¬P) | idtac]
     | apply bpiabsp with (P:=R); [apply pmp with (P:=Q) | idtac]]; BThyp'.
    BThyp'; apply pbbt1_2_2.
 Qed.

 Theorem bbt1_2_7:forall (P Q R:☊), ⊣(¬P☲R) ∧ (Q☲R) ☲ ((P☲Q) ☲ R).
 Proof.
  intros P Q R; apply (proptopred (pbbt1_2_7 P Q R)).
 Qed.

 (* BB p22 §1.2.3 Th1.2.8 *)
 Theorem pbbt1_2_8:forall (P Q R:☊), ⊣☋(P☲(Q☲R)) ☲ (P∧Q☲R).
 Proof.
  intros P Q R; apply bpiimpi; apply bpiimpi; apply pinshypand; apply pmp with (P:=Q);
   [idtac | apply pmp with (P:=P)]; BThyp'.
 Qed.

 Theorem bbt1_2_8:forall (P Q R:☊), ⊣(P☲(Q☲R)) ☲ (P∧Q☲R).
 Proof.
  intros P Q R; apply (proptopred (pbbt1_2_8 P Q R)).
 Qed.

(* B-Book rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* BB p23 §1.2.4 DR1 *)
 Theorem pbbdr1:forall (G:☐)(P:☊), G⊣☋P->G⊣☋¬¬P.
 Proof.
  intros G P H; apply pmp with (P:=P); [apply H | apply bpiemp; apply pbbt1_2_1].
 Qed.

 Theorem bbdr1:forall (G:☐)(P:☊), G⊣P->G⊣¬¬P.
 Proof.
  intros G P H; apply mp with (P:=P); [apply H | apply biemp; apply bbt1_2_1].
 Qed.

 (* BB p23 §1.2.4 DR2 *)
 Theorem pbbdr2:forall (G:☐)(P Q:☊), G⊣☋P->G⊣☋¬Q->G⊣☋¬(P☲Q).
 Proof.
  intros G P Q H H0; apply pmp with (P:=P∧¬Q);
   [apply bpiandi; [apply H | apply H0] | apply bpiemp; apply pbbt1_2_2].
 Qed.

 Theorem bbdr2:forall (G:☐)(P Q:☊), G⊣P->G⊣¬Q->G⊣¬(P☲Q).
 Proof.
  intros G P Q H H0; apply mp with (P:=P∧¬Q);
   [apply biandi; [apply H | apply H0] | apply biemp; apply bbt1_2_2].
 Qed.

 (* BB p23 §1.2.4 DR3 *)
 Theorem pbbdr3:forall (G:☐)(P Q:☊), G⊣☋(P☲¬Q)->G⊣☋¬(P∧Q).
 Proof.
  intros G P Q H; apply pmp with (P:=P☲¬Q); [apply H | apply bpiemp; apply pbbt1_2_3].
 Qed.

 Theorem bbdr3:forall (G:☐)(P Q:☊), G⊣(P☲¬Q)->G⊣¬(P∧Q).
 Proof.
  intros G P Q H; apply mp with (P:=P☲¬Q); [apply H | apply biemp; apply bbt1_2_3].
 Qed.

 (* BB p23 §1.2.4 DR4 *)
 Theorem pbbdr4:forall (G:☐)(P Q:☊), G⊣☋P☲Q->G⊣☋¬¬P☲Q.
 Proof.
  intros G P Q H; apply pmp with (P:=P☲Q); [apply H | apply bpiemp; apply pbbt1_2_4].
 Qed.

 Theorem bbdr4:forall (G:☐)(P Q:☊), G⊣P☲Q->G⊣¬¬P☲Q.
 Proof.
  intros G P Q H; apply mp with (P:=P☲Q); [apply H | apply biemp; apply bbt1_2_4].
 Qed.

 (* BB p23 §1.2.4 DR5 *)
 Theorem pbbdr5:forall (G:☐)(P Q R:☊), G⊣☋P☲(¬Q☲R)->G⊣☋¬(P☲Q) ☲ R.
 Proof.
  intros G P Q R H; apply pmp with (P:=P☲(¬Q☲R)); [apply H | apply bpiemp; apply pbbt1_2_5].
 Qed.

 Theorem bbdr5:forall (G:☐)(P Q R:☊), G⊣P☲(¬Q☲R)->G⊣¬(P☲Q) ☲ R.
 Proof.
  intros G P Q R H; apply mp with (P:=P☲(¬Q☲R)); [apply H | apply biemp; apply bbt1_2_5].
 Qed.

 (* BB p23 §1.2.4 DR6 *)
 Theorem pbbdr6:forall (G:☐)(P Q R:☊), G⊣☋¬P☲R->G⊣☋¬Q☲R->G⊣☋¬(P∧Q) ☲ R.
 Proof.
  intros G P Q R H H0; apply pmp with (P:=(¬P☲R) ∧ (¬Q☲R));
   [apply bpiandi; [apply H | apply H0] | apply bpiemp; apply pbbt1_2_6].
 Qed.

 Theorem bbdr6:forall (G:☐)(P Q R:☊), G⊣¬P☲R->G⊣¬Q☲R->G⊣¬(P∧Q) ☲ R.
 Proof.
  intros G P Q R H H0; apply mp with (P:=(¬P☲R) ∧ (¬Q☲R));
   [apply biandi; [apply H | apply H0] | apply biemp; apply bbt1_2_6].
 Qed.

 (* BB p23 §1.2.4 DR7 *)
 Theorem pbbdr7:forall (G:☐)(P Q R:☊), G⊣☋¬P☲R->G⊣☋Q☲R->G⊣☋(P☲Q) ☲ R.
 Proof.
  intros G P Q R H H0; apply pmp with (P:=(¬P☲R) ∧ (Q☲R));
   [apply bpiandi; [apply H | apply H0] | apply bpiemp; apply pbbt1_2_7].
 Qed.

 Theorem bbdr7:forall (G:☐)(P Q R:☊), G⊣¬P☲R->G⊣Q☲R->G⊣(P☲Q) ☲ R.
 Proof.
  intros G P Q R H H0; apply mp with (P:=(¬P☲R) ∧ (Q☲R));
   [apply biandi; [apply H | apply H0] | apply biemp; apply bbt1_2_7].
 Qed.

 (* BB p23 §1.2.4 DR8 *)
 Theorem pbbdr8:forall (G:☐)(P Q R:☊), G⊣☋P☲(Q☲R)->G⊣☋P∧Q☲R.
 Proof.
  intros G P Q R H; apply pmp with (P:=P☲(Q☲R)); [apply H | apply bpiemp; apply pbbt1_2_8].
 Qed.

 Theorem bbdr8:forall (G:☐)(P Q R:☊), G⊣P☲(Q☲R)->G⊣P∧Q☲R.
 Proof.
  intros G P Q R H; apply mp with (P:=P☲(Q☲R)); [apply H | apply biemp; apply bbt1_2_8].
 Qed.

 (* BB p24 §1.2.4 DB1 *)
 Theorem pbbdb1:forall (G:☐)(P Q:☊), P∈☑G->G⊣☋¬P☲Q.
 Proof.
  intros G P Q H; apply bpiimpi; apply pabsurdn; apply bpiin; apply H.
 Qed.

 Theorem bbdb1:forall (G:☐)(P Q:☊), P∈☑G->G⊣¬P☲Q.
 Proof.
  intros G P Q H; apply (proptopred (pbbdb1 G P Q H)).
 Qed.

 (* BB p24 §1.2.4 DB2 *)
 Theorem pbbdb2:forall (G:☐)(P Q:☊), ¬P∈☑G->G⊣☋P☲Q.
 Proof.
  intros G P Q H; apply bpiimpi; apply pabsurdp; apply bpiin; apply H.
 Qed.

 Theorem bbdb2:forall (G:☐)(P Q:☊), ¬P∈☑G->G⊣P☲Q.
 Proof.
  intros G P Q H; apply (proptopred (pbbdb2 G P Q H)).
 Qed.

 (* BB p24 §1.2.4 BS1 *)
 Theorem pbbbs1:forall (G:☐)(P Q:☊), P∈☑G->G⊣☋Q☲P.
 Proof.
  intros G P Q H; apply bpiimpi; BThyp'; apply bpiin; apply H.
 Qed.

 Theorem bbbs1:forall (G:☐)(P Q:☊), P∈☑G->G⊣Q☲P.
 Proof.
  intros G P Q H; apply (proptopred (pbbbs1 G P Q H)).
 Qed.

 (* BB p24 §1.2.4 BS2 *)
 Theorem pbbbs2:forall (G:☐)(P:☊), P∈☑G->G⊣☋P.
 Proof.
  apply bpiin.
 Qed.

 Theorem bbbs2:forall (G:☐)(P:☊), P∈☑G->G⊣P.
 Proof.
  intros G P H; apply (proptopred (pbbbs2 G P H)).
 Qed.

 (* Other notations *)
 Definition pded:=bpiimpi.
 Definition ded:=biimpi.
 Definition pcnj:=bpiandi.
 Definition cnj:=biandi.

(* B-Book tactic proof propositional calculus ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Ltac BTprop:=
  unfold Bterm.pior;
  unfold Bterm.piff;
  intros;
  repeat (first [BS1 | BS2 | DB1 | DB2 | DR])
 with BS1:=
  match goal with
  | |- ?G⊣_☲?Q => match G with
                  | ?G' ☑★ Q => apply bbbs1; BTin
                  | ?G' ☑★ _ => apply biins; BS1
                  end
  end
 with BS2:=
  match goal with
  | |- ?G⊣?P => match G with
                | ?G' ☑★ P => apply bbbs2; BTin
                | ?G' ☑★ _ => apply biins; BS2
                  end
  end
 with DB1:=
  match goal with
  | |- ?G⊣¬?P☲_ => match G with
                   | ?G' ☑★ P => apply bbdb1; BTin
                   | ?G' ☑★ _ => apply biins; DB1
                   end
  end
 with DB2:=
  match goal with
  | |- ?G⊣?P☲_ => match G with
                  | ?G' ☑★ ¬P => apply bbdb2; BTin
                  | ?G' ☑★ _ => apply biins; DB2
                   end
  end
 with DR:=match goal with |- ?G⊣¬¬?P => apply bbdr1
                        | |- _⊣¬(_☲_) => apply bbdr2
                        | |- _⊣¬(_∧_) => apply bbdr3
                        | |- _⊣¬¬_☲_ => apply bbdr4
                        | |- _⊣¬(_☲_) ☲_ => apply bbdr5
                        | |- _⊣¬(_∧_) ☲_ => apply bbdr6
                        | |- _⊣(_☲_) ☲_ => apply bbdr7
                        | |- _⊣_∧_☲_ => apply bbdr8
                        | |- _⊣_∧_ => apply cnj
                        | |- _⊣_☲_ => apply ded
                        end.

(* B-Book proofs based on the propositional calculus tactic ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* BB p25 §1.2.4 Th1.2.9 *)
 Theorem bbt1_2_9:forall (P Q R:☊), ⊣(¬P☲Q) ∧ R☲(¬(P∧R) ☲ (Q∧R)).
 Proof.
  BTprop.
 Qed.

 (* BB p26 §1.2.5 Th1.2.10 *)
 Theorem bbt1_2_10:forall (P Q R:☊), ⊣(P☲R) ∧ (Q☲R) ∧ (P∨Q) ☲ R.
 Proof.
  BTprop.
 Qed.

(* B-Book case tactic ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* BB p27 §1.2.5, a.k.a. CASE *)
 Theorem case:forall (G:☐)(P Q R:☊), G⊣P∨Q->G ☑★ P⊣R->G ☑★ Q⊣R->G⊣R.
 Proof.
  intros G P Q R H H0 H1; apply mp with (P:=(P☲R) ∧ (Q☲R) ∧ (P∨Q)); BTprop;
   [apply H0 | apply H1 | apply biimpe; apply H].
 Qed.

(* B-Book proofs based on the propositional calculus tactic ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* BB p28 §1.2.6, ∧-commutativity *)
 Theorem command:forall (P Q:☊), ⊣(P∧Q) ☳ (Q∧P).
 Proof.
  BTprop.
 Qed.

 (* BB p28 §1.2.6, ∨-commutativity *)
 Theorem commor:forall (P Q:☊), ⊣(P∨Q) ☳ (Q∨P).
 Proof.
  BTprop.
 Qed.

 (* BB p28 §1.2.6, ☳-commutativity *)
 Theorem commiff:forall (P Q:☊), ⊣(P☳Q) ☳ (Q☳P).
 Proof.
  BTprop.
 Qed.

 (* BB p28 §1.2.6, ∧-associativity *)
 Theorem assand:forall (P Q R:☊), ⊣((P∧Q) ∧ R) ☳ (P ∧ (Q∧R)).
 Proof.
  BTprop.
 Qed.

 (* BB p28 §1.2.6, ∨-associativity *)
 Theorem assor:forall (P Q R:☊), ⊣((P∨Q) ∨ R) ☳ (P ∨ (Q∨R)).
 Proof.
  BTprop.
 Qed.

 (* BB p28 §1.2.6, ☳-associativity *)
 Theorem assiff:forall (P Q R:☊), ⊣((P☳Q) ☳ R) ☳ (P ☳ (Q☳R)).
 Proof.
  BTprop.
 Qed.

 (* BB p28 §1.2.6, ∧∨-distributivity *)
 Theorem disandor:forall (P Q R:☊), ⊣(P∧(Q∨R)) ☳ ((P∧Q) ∨ (P∧R)).
 Proof.
  BTprop.
 Qed.

 (* BB p28 §1.2.6, ∨∧-distributivity *)
 Theorem disorand:forall (P Q R:☊), ⊣(P∨(Q∧R)) ☳ ((P∨Q) ∧ (P∨R)).
 Proof.
  BTprop.
 Qed.

 (* BB p28 §1.2.6, ∧-reflexivity *)
 Theorem refland:forall (P Q R:☊), ⊣(P∧P) ☳ P.
 Proof.
  BTprop.
 Qed.

 (* BB p28 §1.2.6, ∨-reflexivity *)
 Theorem reflor:forall (P Q R:☊), ⊣(P∨P) ☳ P.
 Proof.
  BTprop.
 Qed.

 (* BB p28 §1.2.6, Morgan's laws *)
 Theorem morgannotand:forall (P Q:☊), ⊣¬(P∧Q) ☳ (¬P∨¬Q).
 Proof.
  BTprop.
 Qed.

 Theorem morgannotor:forall (P Q:☊), ⊣¬(P∨Q) ☳ (¬P∧¬Q).
 Proof.
  BTprop.
 Qed.

(* Additional technical lemmas related to ☳ ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* ☳-symmetry *)
 Theorem symiff:forall (G:☐)(P P':☊), G⊣P☳P'->G⊣P'☳P.
 Proof.
  intros G P P' H; unfold piff; apply biandi;
   [apply biandr with (P:=P☲P') | apply biandl with (Q:=P'☲P)]; apply H.
 Qed.

 (* ☳ to ☲ *)
 Theorem diffimp:forall (G:☐)(P P':☊), G⊣P☳P'->G⊣P☲P'.
 Proof.
  intros G P P' H; apply biandl with (Q:=P'☲P); apply H.
 Qed.

 (* Reverse ☳ to ☲ *)
 Theorem riffimp:forall (G:☐)(P P':☊), G⊣P☳P'->G⊣P'☲P.
 Proof.
  intros G P P' H; apply (diffimp _ _ _ (symiff _ _ _ H)).
 Qed.

(* Interesting results about ∨ ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem ornotl:forall (G:☐)(P Q:☊), G⊣¬P->G⊣P∨Q->G⊣Q.
 Proof.
  intros G P Q H H0; unfold pior in H0; apply mp with (P:=¬P); [apply H | apply H0].
 Qed.

 Theorem ornotr:forall (G:☐)(P Q:☊), G⊣¬Q->G⊣P∨Q->G⊣P.
 Proof.
  intros G P Q H H0; unfold pior in H0; apply mp with (P:=¬Q);
   [apply H | apply contrapn'; apply H0].
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
