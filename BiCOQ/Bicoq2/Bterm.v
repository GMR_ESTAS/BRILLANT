(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq2, formalisation of the B theory into the Coq system
   Module: Bterm, terms, expressions and predicates
   The B-Book defines several syntactic types, predicates, expressions, sets and
   variables. In this module, however, this approach is simplified as expressions and
   sets are merged, while variables are reduced to a single expression constructor.
   Lists of variables are also ommitted, forbidding multiple quantification, yet with
   no reduction of expressiveness.
   To limit the occurrences of infamous side-conditions, De Bruijn indexes are used.
   Finally, a few additional constructors are proposed to provide BIG elements, or to
   allow powerful predicate replacement operations.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - September 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Mindex.

(* Predicates and expression definitions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Inductive Bprd:Set:=
  | pnot:Bprd->Bprd              (* ¬, negation *)
  | pfor:Bprd->Bprd              (* ∀, universal quantification *)
  | pand:Bprd->Bprd->Bprd  (* ∧, conjunction *)
  | pimp:Bprd->Bprd->Bprd  (* ☲, implication *)
  | ppvr:☈->Bprd                    (* π, predicate-variable *)
  | pequ:Bexp->Bexp->Bprd  (* ♃, equality *)
  | pins:Bexp->Bexp->Bprd  (* ∈, membership *)
 with Bexp:Set:=
  | ebig:Bexp                          (* ♄, BIG *)
  | echs:Bexp->Bexp              (* ☭, choice *)
  | epow:Bexp->Bexp              (* ☬, powerset *)
  | ecpl:Bexp->Bexp->Bexp  (* ☵, pair *)
  | epro:Bexp->Bexp->Bexp  (* ♂, cartesian product *)
  | ebie:☈->Bexp                    (* ω, BIG element *)
  | evar:☈->Bexp                    (* χ, variable *)
  | ecmp:Bexp->Bprd->Bexp. (* ♀, comprehension set *)
 (* Nota: various modifications, simplifications and improvements are proposed...          *)
 (* - Sets and expressions of the B-Book are merged into Bexp, noting that the B-Book     *)
 (* definitions does NOT allow to write e.g. x∈choice(☊(S));                                                      *)
 (* - Quantification are limited to a single variable (through De Bruijn notation), yet *)
 (*  ∀x,y.P is equivalent to ∀x.(∀y.P) and {x,y|P} is equivalent to {z|∃x.∃y.z=x☵y∧P};           *)
 (* - πi are predicate variables (to substitute predicates);                                                  *)
 (* - ωi provide an enumeration of BIG elements - an easy way to replace infinite(BIG).   *)
 (* - ♀ is defined as a half binder between an expression (not bound) and a predicate.      *)

 Notation "☊":=Bprd.
 Notation "'¬' P":=(pnot P)  (at level 11, right associativity).
 Notation "'∀' P":=(pfor P) (at level 60, right associativity).
 Infix "∧":=pand (at level 20, right associativity).
 Infix "☲":=pimp (at level 41, right associativity).
 Notation "'π' i":=(ppvr i) (at level 0, no associativity).
 Infix "♃":=pequ (at level 1, no associativity).
 Infix "∈":=pins (at level 1, no associativity).

 Notation "☌":=Bexp.
 Notation "♄":=ebig.
 Notation "'☭' E":=(echs E) (at level 60, right associativity).
 Notation "'☬' E":=(epow E) (at level 60, right associativity).
 Notation "'(' E1 '☵' E2 ')'":=(ecpl E1 E2).
 Notation "'(' E1 '♂' E2 ')'":=(epro E1 E2).
 Notation "'ω' i":=(ebie i) (at level 0, no associativity).
 Notation "'χ' i":=(evar i) (at level 0, no associativity).
 Infix "♀":=ecmp (at level 2, left associativity).

 (* Two mutual induction schemes, one for results on ☊, the other for results on ☌ *)
 Scheme Bsp_ind:=Induction for Bprd Sort Set
 with Bse_ind:=Induction for Bexp Sort Set.

 Theorem Bst_ind:forall (PP:☊->Set)(PE:☌->Set),
                               PE ♄ ->
                               (forall i:☈, PP π(i)*PE ω(i)*PE χ(i))->
                               (forall p:☊, PP p->PP (¬p)*PP (∀p))->
                               (forall e:☌, PE e->PE (☭e)*PE (☬e))->
                               (forall p1 p2:☊, PP p1->PP p2->PP (p1∧p2)*PP (p1☲p2))->
                               (forall e1 e2:☌, PE e1->PE e2->(PP (e1♃e2)*PP (e1∈e2))*
                                                                                     (PE (e1☵e2)*PE (e1♂e2)))->
                               (forall (e:☌)(p:☊), PE e->PP p->PE (e♀p))->
                               (forall p:☊, PP p)*(forall e:☌, PE e).
 Proof.
  intros PP PE Hbig Hvar Hprd Hexp Hprd2 Hexp2 Hcmp; split.
   apply (Bsp_ind PP PE).
    intros p H; apply (fst (Hprd p H)).
    intros p H; apply (snd (Hprd p H)).
    intros p1 H1 p2 H2; apply (fst (Hprd2 p1 p2 H1 H2)).
    intros p1 H1 p2 H2; apply (snd (Hprd2 p1 p2 H1 H2)).
    intros i; apply (fst (fst (Hvar i))).
    intros e1 H1 e2 H2; apply (fst (fst (Hexp2 e1 e2 H1 H2))).
    intros e1 H1 e2 H2; apply (snd (fst (Hexp2 e1 e2 H1 H2))).
    apply Hbig.
    intros e H; apply (fst (Hexp e H)).
    intros e H; apply (snd (Hexp e H)).
    intros e1 H1 e2 H2; apply (fst (snd (Hexp2 e1 e2 H1 H2))).
    intros e1 H1 e2 H2; apply (snd (snd (Hexp2 e1 e2 H1 H2))).
    intros i; apply (snd (fst (Hvar i))).
    intros i; apply (snd (Hvar i)).
    intros e H1 p H2; apply (Hcmp e p H1 H2).
   apply (Bse_ind PP PE).
    intros p H; apply (fst (Hprd p H)).
    intros p H; apply (snd (Hprd p H)).
    intros p1 H1 p2 H2; apply (fst (Hprd2 p1 p2 H1 H2)).
    intros p1 H1 p2 H2; apply (snd (Hprd2 p1 p2 H1 H2)).
    intros i; apply (fst (fst (Hvar i))).
    intros e1 H1 e2 H2; apply (fst (fst (Hexp2 e1 e2 H1 H2))).
    intros e1 H1 e2 H2; apply (snd (fst (Hexp2 e1 e2 H1 H2))).
    apply Hbig.
    intros e H; apply (fst (Hexp e H)).
    intros e H; apply (snd (Hexp e H)).
    intros e1 H1 e2 H2; apply (fst (snd (Hexp2 e1 e2 H1 H2))).
    intros e1 H1 e2 H2; apply (snd (snd (Hexp2 e1 e2 H1 H2))).
    intros i; apply (snd (fst (Hvar i))).
    intros i; apply (snd (Hvar i)).
    intros e H1 p H2; apply (Hcmp e p H1 H2).
 Qed.
 (* Nota: this derived induction scheme allow to make better use of proofs; indeed, the  *)
 (* schemes Bsp_ind and Bse_ind require the same proofs to derive different results,    *)
 (* and using those instead of Bst_ind would lead to proof duplication.                             *)

(* Additional definitions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition pior P1 P2:=(pimp (pnot P1) P2).
 Infix "∨":=pior (at level 30, right associativity).

 Definition piff P1 P2:=(pand (pimp P1 P2) (pimp P2 P1)).
 Infix "☳":=piff (at level 39, right associativity).

 Definition pexs P:=(pnot (pfor (pnot P))).
 Notation "'∃' P":=(pexs P) (at level 60, right associativity).

 Definition pdif E1 E2:=(pnot (pequ E1 E2)).
 Infix "♃☃":=pdif (at level 1, no associativity).

 Definition pnin E1 E2:=(pnot (pins E1 E2)).
 Infix "∈☃":=pnin (at level 1, no associativity).

 Definition pinc E1 E2:=(pins E1 (epow E2)).
 Infix "⊆":=pinc (at level 1, no associativity).

(* Equality functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fpequ(P1 P2:☊){struct P1}:☆:=
  match P1 with
  | ¬P1' => match P2 with ¬P2' => fpequ P1' P2' | _ => ⊥ end
  | ∀P1' => match P2 with ∀P2' => fpequ P1' P2' | _ => ⊥ end
  | P11∧P12 => match P2 with P21∧P22 => (fpequ P11 P21)∧☁(fpequ P12 P22) | _ => ⊥ end
  | P11☲P12 => match P2 with P21☲P22 => (fpequ P11 P21)∧☁(fpequ P12 P22) | _ => ⊥ end
  | π i1 => match P2 with π i2 => i1=☉☂i2 | _ => ⊥ end
  | E11♃E12 => match P2 with E21♃E22 => (feequ E11 E21)∧☁(feequ E12 E22) | _ => ⊥ end
  | E11∈E12 => match P2 with E21∈E22 => (feequ E11 E21)∧☁(feequ E12 E22) | _ => ⊥ end
  end
 with feequ(E1 E2:☌){struct E1}:☆:=
  match E1 with
  | ♄ => match E2 with ♄ => ⊤ | _ => ⊥ end
  | ☭E1' => match E2 with ☭E2' => feequ E1' E2' | _ => ⊥ end
  | ☬E1' => match E2 with ☬E2' => feequ E1' E2' | _ => ⊥ end
  | (E11☵E12) => match E2 with (E21☵E22) => (feequ E11 E21)∧☁(feequ E12 E22) | _ => ⊥ end
  | (E11♂E12) => match E2 with (E21♂E22) => (feequ E11 E21)∧☁(feequ E12 E22) | _ => ⊥ end
  | ω i1 => match E2 with ω i2 => i1=☉☂i2 | _ => ⊥ end
  | χ i1 => match E2 with χ i2 => i1=☉☂i2 | _ => ⊥ end
  | S1'♀P1' => match E2 with S2'♀P2' => (feequ S1' S2')∧☁(fpequ P1' P2') | _ => ⊥ end
  end.
  Infix "=☋☂":=fpequ (at level 70, no associativity).
  Infix "=☍☂":=feequ (at level 70, no associativity).
 (* Nota: special functional schemes for those equality can be defined by the commands *)
 (* - Functional Scheme peequ_ind:=Induction for fpequ with fpequ feequ.                          *)
 (* - Functional Scheme epequ_ind:=Induction for feequ with fpequ feequ.                          *)
 (* but are not required in this project.                                                                                     *)

 Theorem reflftequ:(forall (P:☊), P=☋☂P≡⊤)*(forall (E:☌), E=☍☂E≡⊤).
 Proof.
  apply (Bst_ind (fun P:☊=>P=☋☂P≡⊤) (fun E:☌=>E=☍☂E≡⊤)); simpl; try (apply refl_id).
   intros i; split; [split | idtac]; apply reflfiequ.
   intros p H; split; apply H.
   intros e H; split; apply H.
   intros p p' H H'; rewrite H; rewrite H'; split; apply refl_id.
   intros e e' H H'; rewrite H; rewrite H'; split; split; apply refl_id.
   intros p e H H'; rewrite H; rewrite H'; apply refl_id.
 Qed.

 Definition reflfpequ:=fst reflftequ.
 Definition reflfeequ:=snd reflftequ.

 (* =☋☂ and =☍☂ implement = *)
 Theorem imptequ:(forall (P1 P2:☊), (P1=☋☂P2≡⊤->P1≡P2)*(P1=☋☂P2≡⊥->!(P1≡P2)))*
                               (forall (E1 E2:☌), (E1=☍☂E2≡⊤->E1≡E2)*(E1=☍☂E2≡⊥->!(E1≡E2))).
 Proof.
  apply (Bst_ind (fun P1:☊=>forall (P2:☊), (P1=☋☂P2≡⊤->P1≡P2)*(P1=☋☂P2≡⊥->!(P1≡P2)))
                             (fun E1:☌=>forall (E2:☌), (E1=☍☂E2≡⊤->E1≡E2)*(E1=☍☂E2≡⊥->!(E1≡E2)))).
   destruct E2; split; intros H; try (intros H0; invset H0; discriminate H1);
    try (invset H; discriminate H0); apply refl_id.
   intros i; split; [split; [destruct P2 | destruct E2] | destruct E2]; split;
    (intros H' H || intros H); try (invset H; discriminate H0);
    try (simpl in H; rewrite (iequtp _ _ H)); try (simpl in H'; apply (iequfr _ _ H'); invset H);
    apply refl_id.
   intros P H; split; destruct P2; split; (intros H' H0 || intros H0);
    try (invset H0; discriminate H1);
    try (simpl in H0; (rewrite (fst (H P2) H0) || rewrite (fst (H b) H0)));
    try (simpl in H'; (apply (snd (H P2) H') || apply (snd (H b) H')); invset H0);
    apply refl_id.
   intros E; split; destruct E2; split; (intros H' H0 || intros H0);
    try (invset H0; discriminate H1); try (simpl in H0; rewrite (fst (H E2) H0));
    try (simpl in H'; apply (snd (H E2) H'); invset H0); apply refl_id.
   intros P P' H H'; split; destruct P2; split; (intros HF H0 || intros H0); try (invset H0);
    try (simpl in H0; destruct (dedfbandt _ _ H0); rewrite (fst (H P2_1) i);
             rewrite (fst (H' P2_2) i0); apply refl_id);
    try (simpl in HF; invset H0; destruct (dedfbandf _ _ HF);
             [apply (snd (H P2_1) i); apply H4 | apply (snd (H' P2_2) i); apply H5]).
   intros E E' H H'; split; split; [destruct P2 | destruct P2 | destruct E2 | destruct E2];
    split; (intros HF H0 || intros H0); try (invset H0);
    try (simpl in H0; destruct (dedfbandt _ _ H0);
             (rewrite (fst (H E2_1) i) || rewrite (fst (H b) i));
             (rewrite (fst (H' E2_2) i0) || rewrite (fst (H' b0) i0)); apply refl_id);
    try (simpl in HF; invset H0; destruct (dedfbandf _ _ HF);
             [(apply (snd (H E2_1) i) || apply (snd (H b) i)); apply H4
             | (apply (snd (H' E2_2) i) || apply (snd (H' b0) i)); apply H5]).
   intros E P H H'; split; destruct E2; (intros HF H0 || intros H0); try (invset H0);
    try (simpl in H0; destruct (dedfbandt _ _ H0); rewrite (fst (H _) i); rewrite (fst (H' _) i0);
            apply refl_id);
    try (simpl in HF; invset H0; destruct (dedfbandf _ _ HF);
            [apply (snd (H _) i); apply H4 | apply (snd (H' _) i); apply H5]).
 Qed.

 Definition imppequ:(identity (A:=☊)☵☨☂fpequ).
 Proof.
  unfold Implem2; intros s t; apply (fst imptequ).
 Qed.

 Definition decpequ:=decimp2 imppequ.
 Definition pequpt:=imp2pt imppequ.
 Definition pequrf:=imp2rf imppequ.
 Definition pequtp:=imp2tp imppequ.
 Definition pequfr:=imp2fr imppequ.

 Theorem impeequ:(identity (A:=☌)☵☨☂feequ).
 Proof.
  unfold Implem2; intros s t; apply (snd imptequ).
 Qed.

 Definition deceequ:=decimp2 impeequ.
 Definition eequpt:=imp2pt impeequ.
 Definition eequrf:=imp2rf impeequ.
 Definition eequtp:=imp2tp impeequ.
 Definition eequfr:=imp2fr impeequ.

(* Non-freeness ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* The B-Book does not define freeness of a variable in a term, but non-freeness. A         *)
(* variable is not free if it does not appear free, i.e. not at all or only bound. The use *)
(* of De Bruijn indexes leads to a slightly different definition.                                       *)

 Inductive pinf:☈->☊->Set:=
  | infnot: forall (i:☈)(P:☊), pinf i P->pinf i (¬P)
  | inffor: forall (i:☈)(P:☊), pinf ☠i P->pinf i (∀P)
  | infand: forall (i:☈)(P1 P2:☊), pinf i P1->pinf i P2->pinf i (P1∧P2)
  | infimp: forall (i:☈)(P1 P2:☊), pinf i P1->pinf i P2->pinf i (P1☲P2)
  | infpvr: forall (i j:☈), pinf i (π j)
  | infequ: forall (i:☈)(E1 E2:☌), einf i E1->einf i E2->pinf i (E1♃E2)
  | infins: forall (i:☈)(E1 E2:☌), einf i E1->einf i E2->pinf i (E1∈E2)
 with einf:☈->☌->Set:=
  | infbig: forall (i:☈), einf i ♄
  | infchs: forall (i:☈)(E:☌), einf i E->einf i (☭E)
  | infpow: forall (i:☈)(E:☌), einf i E->einf i (☬E)
  | infcpl: forall (i:☈)(E1 E2:☌), einf i E1->einf i E2->einf i (E1☵E2)
  | infpro: forall (i:☈)(E1 E2:☌), einf i E1->einf i E2->einf i (E1♂E2)
  | infbie: forall (i j:☈), einf i (ω j)
  | infvar: forall (i j:☈), !(i≡j)->einf i (χ j)
  | infcmp: forall (i:☈)(S:☌)(P:☊), einf i S->pinf ☠i P->einf i (S♀P).
 Infix "⍀☋":=pinf (at level 69, no associativity).
 Infix "⍀☍":=einf (at level 69, no associativity).

(* Non-freeness functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fpinf(i:☈)(P:☊){struct P}:☆:=
  match P with
  | ¬P' => fpinf i P'
  | ∀P' => fpinf ☠i P'
  | P1∧P2 => (fpinf i P1)∧☁(fpinf i P2)
  | P1☲P2 => (fpinf i P1)∧☁(fpinf i P2)
  | π j => ⊤
  | E1♃E2 => (feinf i E1)∧☁(feinf i E2)
  | E1∈E2 => (feinf i E1)∧☁(feinf i E2)
  end
 with feinf(i:☈)(E:☌){struct E}:☆:=
  match E with
  | ♄ => ⊤
  | ☭E' => feinf i E'
  | ☬E' => feinf i E'
  | (E1☵E2) => (feinf i E1)∧☁(feinf i E2)
  | (E1♂E2) => (feinf i E1)∧☁(feinf i E2)
  | ω j => ⊤
  | χ j => ¬☁(i=☉☂j)
  | S'♀P' => (feinf i S')∧☁(fpinf ☠i P')
  end.
 Infix "⍀☋☂":=fpinf (at level 69, no associativity).
 Infix "⍀☍☂":=feinf (at level 69, no associativity).

 (* ⍀☋☂ and ⍀☍☂ implement ⍀☋ and ⍀☍ *)
 Theorem imptinf:(forall (P:☊)(i:☈), (i⍀☋☂P≡⊤->i⍀☋P)*(i⍀☋☂P≡⊥->!(i⍀☋P)))*
                               (forall (E:☌)(i:☈), (i⍀☍☂E≡⊤->i⍀☍E)*(i⍀☍☂E≡⊥->!(i⍀☍E))).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (i:☈), (i⍀☋☂P≡⊤->i⍀☋P)*(i⍀☋☂P≡⊥->!(i⍀☋P)))
                             (fun E:☌=>forall (i:☈), (i⍀☍☂E≡⊤->i⍀☍E)*(i⍀☍☂E≡⊥->!(i⍀☍E)))); simpl.
   intros i; split; intros H; [apply infbig | invset H].
   intros i; split; [split | idtac]; intros j; split; (intros H H' || intros H); try (invset H).
    apply infpvr.
    apply infbie.
    apply infvar; apply (iequfr _ _ (dedfbnott _ H)).
    invset H'; apply H3; apply (iequtp _ _ (dedfbnotf _ H)).
   intros P H; split; intros i; split; (intros H0 H' || intros H0); try (invset H0);
    try ((apply infnot || apply inffor); apply (fst (H _) H0));
    apply (snd (H _) H0); invset H'; apply H4.
   intros E H; split; intros i; split; (intros H0 H' || intros H0); try (invset H0);
    try ((apply infchs || apply infpow); apply (fst (H _) H0));
    try (apply (snd (H _) H0); invset H'; apply H4).
   intros P P' H H'; split; intros i; split; (intros H0 HN || intros H0); try (invset H0);
    try (destruct (dedfbandt _ _ H0); (apply infand || apply infimp);
             (apply (fst (H _) i0) || apply (fst (H' _) i1)));
    try (invset HN; destruct (dedfbandf _ _ H0);
             [apply (snd (H _) i1 H5) | apply (snd (H' _) i1 H6)]).
   intros E E' H H'; split; split; intros i; split; (intros H0 HN || intros H0); try (invset H0);
    try (destruct (dedfbandt _ _ H0);
             (apply infequ || apply infins || apply infcpl || apply infpro );
             (apply (fst (H _) i0) || apply (fst (H' _) i1)));
    try (invset HN; destruct (dedfbandf _ _ H0);
             [apply (snd (H _) i1 H5) |  apply (snd (H' _) i1 H6)]).
   intros E P HE HP i; split; (intros H0 HN || intros H0); try (invset H0).
    destruct (dedfbandt _ _ H0); apply infcmp; [apply (fst (HE _) i0) | apply (fst (HP _) i1)].
    invset HN; destruct (dedfbandf _ _ H0); [apply (snd (HE _) i1 H4) | apply (snd (HP _) i1 H5)].
 Qed.

 Theorem imppinf:(pinf☵☨☂fpinf).
 Proof.
  unfold Implem2; intros s t; apply (fst imptinf).
 Qed.

 Definition decpinf:=decimp2 imppinf.
 Definition pinfpt:=imp2pt imppinf.
 Definition pinfrf:=imp2rf imppinf.
 Definition pinftp:=imp2tp imppinf.
 Definition pinffr:=imp2fr imppinf.

 Theorem impeinf:(einf☵☨☂feinf).
 Proof.
  unfold Implem2; intros s t; apply (snd imptinf).
 Qed.

 Definition deceinf:=decimp2 impeinf.
 Definition einfpt:=imp2pt impeinf.
 Definition einfrf:=imp2rf impeinf.
 Definition einftp:=imp2tp impeinf.
 Definition einffr:=imp2fr impeinf.

(* Strict majoration of indexes ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* The majoration property indicates if an index is greater than indexes appearing      *)
(* in a term. Using De Bruijn notations, the index is incremented when crossing one of   *)
(* the two binders; therefore ☦ majors a term without free index, and any majorant of a   *)
(* term does not appear free in this term.                                                                                   *)

 Inductive pmaj:☈->☊->Set:=
  | majnot: forall (i:☈)(P:☊), pmaj i P->pmaj i (¬P)
  | majfor: forall (i:☈)(P:☊), pmaj ☠i P->pmaj i (∀P)
  | majand: forall (i:☈)(P1 P2:☊), pmaj i P1->pmaj i P2->pmaj i (P1∧P2)
  | majimp: forall (i:☈)(P1 P2:☊), pmaj i P1->pmaj i P2->pmaj i (P1☲P2)
  | majpvr: forall (i j:☈), pmaj i (π j)
  | majequ: forall (i:☈)(E1 E2:☌), emaj i E1->emaj i E2->pmaj i (E1♃E2)
  | majins: forall (i:☈)(E1 E2:☌), emaj i E1->emaj i E2->pmaj i (E1∈E2)
 with emaj:☈->☌->Set:=
  | majbig: forall (i:☈), emaj i ♄
  | majchs: forall (i:☈)(E:☌), emaj i E->emaj i (☭E)
  | majpow: forall (i:☈)(E:☌), emaj i E->emaj i (☬E)
  | majcpl: forall (i:☈)(E1 E2:☌), emaj i E1->emaj i E2->emaj i (E1☵E2)
  | majpro: forall (i:☈)(E1 E2:☌), emaj i E1->emaj i E2->emaj i (E1♂E2)
  | majbie: forall (i j:☈), emaj i (ω j)
  | majvar: forall (i j:☈), j<☉i->emaj i (χ j)
  | majcmp: forall (i:☈)(S:☌)(P:☊), emaj i S->pmaj ☠i P->emaj i (S♀P).
 Infix ">☋":=pmaj (at level 69, no associativity).
 Infix ">☍":=emaj (at level 69, no associativity).
 (* Nota: the approach is, even for those notions not defined in the B-Book (such as         *)
 (* majoration), to both describe a property and provide a function implementing it.      *)

 (* >☋ and >☍ imply ⍀☋ and ⍀☍ *)
 Theorem tmajinf:(forall (P:☊)(i:☈), i>☋P->i⍀☋P)*(forall (E:☌)(i:☈), i>☍E->i⍀☍E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (i:☈), i>☋P->i⍀☋P) (fun E:☌=>forall (i:☈), i>☍E->i⍀☍E)).
   intros i H; apply infbig.
   intros i; split; [split | idtac]; intros i0 H;
    [apply infpvr
    | apply infbie
    | apply infvar; intros H0; rewrite <- (sym_id H0) in H; invset H; apply (nrefligt _ H3) ].
   intros p; split; intros i H0; [apply infnot | apply inffor]; invset H0; apply (H _ H3).
   intros e; split; intros i H0; [apply infchs | apply infpow]; invset H0; apply (H _ H3).
   intros p1 p2 H1 H2; split; intros i H; invset H;
    [apply infand; [apply (H1 _ H5) | apply (H2 _ H6)]
    | apply infimp; [apply (H1 _ H5) | apply (H2 _ H6)]].
   intros e1 e2 H1 H2; split; split; intros i H; invset H;
    [apply infequ; [apply (H1 _ H5) | apply (H2 _ H6)]
    | apply infins; [apply (H1 _ H5) | apply (H2 _ H6)]
    | apply infcpl; [apply (H1 _ H5) | apply (H2 _ H6)]
    | apply infpro; [apply (H1 _ H5) | apply (H2 _ H6)]].
   intros e p He Hp i H; invset H; apply infcmp; [apply (He _ H3) | apply (Hp _ H4)].
 Qed.

 Definition pmajinf:=fst tmajinf.
 Definition emajinf:=snd tmajinf.

 (* Any index greater than a majorant is a majorant *)
 Theorem tmajige:(forall (P:☊)(i j:☈), i≤☉j->i>☋P->j>☋P)*
                               (forall (E:☌)(i j:☈), i≤☉j->i>☍E->j>☍E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (i j:☈), i≤☉j->i>☋P->j>☋P)
                             (fun E:☌=>forall (i j:☈), i≤☉j->i>☍E->j>☍E)).
   intros i j _ _; apply majbig.
   intros i; split; [split | idtac]; intros j k Hjk HMj;
    [apply majpvr
    | apply majbie
    | apply majvar; invset HMj; apply (transigtige _ _ _ H1 Hjk)].
   intros p; split; intros i j Hij HMi; invset HMi;
    [apply majnot; apply (H _ _ Hij H2) | apply majfor; apply (H _ _ (genxt _ _ Hij) H2)].
   intros e; split; intros i j Hij HMi; invset HMi; [apply majchs | apply majpow ];
    apply (H _ _ Hij H2).
   intros p1 p2 H1 H2; split; intros i j Hij HMi; invset HMi;
    [apply majand; [apply (H1 _ _ Hij H4) | apply (H2 _ _ Hij H5)]
    | apply majimp; [apply (H1 _ _ Hij H4) | apply (H2 _ _ Hij H5)]].
   intros e1 e2 H1 H2; split; split; intros i j Hij HMi; invset HMi;
    [apply majequ; [apply (H1 _ _ Hij H4) | apply (H2 _ _ Hij H5)]
    | apply majins; [apply (H1 _ _ Hij H4) | apply (H2 _ _ Hij H5)]
    | apply majcpl; [apply (H1 _ _ Hij H4) | apply (H2 _ _ Hij H5)]
    | apply majpro; [apply (H1 _ _ Hij H4) | apply (H2 _ _ Hij H5)]].
   intros e p He Hp i j Hij HMi; invset HMi; apply majcmp;
    [apply (He _ _ Hij H2) | apply (Hp _ _ (genxt _ _ Hij) H3)].
 Qed.

 Definition pmajige:=fst tmajige.
 Definition emajige:=snd tmajige.

 (* A majorant is always greater than all free variables *)
 Theorem igetmaj:(forall (P:☊)(i:☈), (forall (j:☈), i≤☉j->j⍀☋P)->i>☋P)*
                               (forall (E:☌)(i:☈), (forall (j:☈), i≤☉j->j⍀☍E)->i>☍E).
 Proof.
  apply  (Bst_ind (fun P:☊=>forall (i:☈), (forall (j:☈), i≤☉j->j⍀☋P)->i>☋P)
                              (fun E:☌=>forall (i:☈), (forall (j:☈), i≤☉j->j⍀☍E)->i>☍E)); simpl.
   intros i H; apply majbig.
   intros i; split; [split | idtac]; intros i0 H;
    [apply majpvr | apply majbie | apply majvar]; destruct (decigt i i0).
    apply i1.
    generalize (H i (nigtige _ _ e)); intros H0; invset H0; destruct H3; apply refl_id.
   intros p; split; intros i H0.
    apply majnot; apply H; intros j H1; generalize (H0 _ H1); intros H2; invset H2;
     apply H5.
    apply majfor; apply H; intros j H1; induction j; invset H1; generalize (H0 _ H4);
     intros H5; invset H5; apply H8.
   intros e H; split; intros i H0; [apply majchs | apply majpow]; apply H; intros j H1;
    generalize (H0 _ H1); intros H2; invset H2; apply H5.
   intros p1 p2 H1 H2; split; intros i H0;
    [apply majand; [apply H1 | apply H2] | apply majimp; [apply H1 | apply H2]];
    intros j H3; generalize (H0 _ H3); intros H4; invset H4;
    [apply H7 | apply H8 | apply H7 | apply H8].
   intros e1 e2 H1 H2; split; split; intros i H0;
    [apply majequ; [apply H1 | apply H2] | apply majins; [apply H1 | apply H2]
    | apply majcpl; [apply H1 | apply H2] | apply majpro; [apply H1 | apply H2]];
      intros j H3; generalize (H0 _ H3); intros H4; invset H4;
      [apply H7 | apply H8 | apply H7 | apply H8| apply H7 | apply H8 | apply H7 | apply H8].
   intros e p He Hp i H; apply majcmp; [apply He | apply Hp]; intros j H0.
    generalize (H _ H0); intros H1; invset H1; apply H5.
    invset H0; generalize (H _ H2); intros H4; invset H4; apply H9.
 Qed.

 Definition igepmaj:=fst igetmaj.
 Definition igeemaj:=snd igetmaj.

(* Strict majoration of indexes function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fpmaj(i:☈)(P:☊){struct P}:☆:=
  match P with
  | ¬P' => fpmaj i P'
  | ∀P' => fpmaj ☠i P'
  | P1∧P2 => (fpmaj i P1)∧☁(fpmaj i P2)
  | P1☲P2 => (fpmaj i P1)∧☁(fpmaj i P2)
  | π j => ⊤
  | E1♃E2 => (femaj i E1)∧☁(femaj i E2)
  | E1∈E2 => (femaj i E1)∧☁(femaj i E2)
  end
 with femaj(i:☈)(E:☌){struct E}:☆:=
  match E with
  | ♄ => ⊤
  | ☭E' => femaj i E'
  | ☬E' => femaj i E'
  | (E1☵E2) => (femaj i E1)∧☁(femaj i E2)
  | (E1♂E2) => (femaj i E1)∧☁(femaj i E2)
  | ω j => ⊤
  | χ j => j<☉☂i
  | S'♀P' => (femaj i S')∧☁(fpmaj ☠i P')
  end.
 Infix ">☋☂":=fpmaj (at level 69, no associativity).
 Infix ">☍☂":=femaj (at level 69, no associativity).

 (* >☋☂ and >☍☂ implement >☋ and >☍ *)
 Theorem imptmaj:(forall (P:☊)(i:☈), (i>☋☂P≡⊤->i>☋P)*(i>☋☂P≡⊥->!(i>☋P)))*
                               (forall (E:☌)(i:☈), (i>☍☂E≡⊤->i>☍E)*(i>☍☂E≡⊥->!(i>☍E))).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall(i:☈), (i>☋☂P≡⊤->i>☋P)*(i>☋☂P≡⊥->!(i>☋P)))
                             (fun (E:☌)=>forall(i:☈), (i>☍☂E≡⊤->i>☍E)*(i>☍☂E≡⊥->!(i>☍E)))); simpl.
   intros i; split; intros H; [apply majbig | invset H].
   intros i; split; [split; intros j | intros j]; split; (intros H H' || intros H);
    try (invset H); [apply majpvr | apply majbie | apply majvar | invset H'].
    apply (igttp _ _ H1).
    destruct (igtfr _ _ H H3).
   intros P H; split; intros i; split; (intros H0 H' || intros H0); try (invset H0);
    try ((apply majnot || apply majfor); apply (fst (H _) H0));
    try (invset H'; apply (snd (H _) H0 H4)).
   intros E H; split; intros i; split; (intros H0 H' || intros H0); try (invset H0);
    try ((apply majchs || apply majpow); apply (fst (H _) H0));
    try (invset H'; apply (snd (H _) H0 H4)).
   intros P1 P2 H1 H2; split; intros i; split; (intros H0 HN || intros H0); try (invset H0);
    try ((apply majand || apply majimp); destruct (dedfbandt _ _ H0);
            [apply (fst (H1 _) i0) | apply (fst (H2 _) i1)]);
    try (invset HN; destruct (dedfbandf _ _ H0);
            [apply (snd (H1 _) i1 H6) | apply (snd (H2 _) i1 H7)]).
   intros E1 E2 H1 H2; split; split; intros i; split; (intros H0 HN || intros H0);
    try (invset H0);
    try ((apply majequ || apply majins || apply majcpl || apply majpro);
            destruct (dedfbandt _ _ H0); [apply (fst (H1 _) i0) | apply (fst (H2 _) i1)]);
    try (invset HN; destruct (dedfbandf _ _ H0);
            [apply (snd (H1 _) i1 H6) | apply (snd (H2 _) i1 H7)]).
   intros E P HE HP i; split; (intros H0 HN || intros H0).
    apply majcmp; destruct (dedfbandt _ _ H0); [apply (fst (HE _) i0) | apply (fst (HP _) i1)].
    invset HN; destruct (dedfbandf _ _ H0); [apply (snd (HE _) i1 H3) | apply (snd (HP _) i1 H4)].
 Qed.

 Theorem imppmaj:(pmaj☵☨☂fpmaj).
 Proof.
  unfold Implem2; intros s t; apply (fst imptmaj).
 Qed.

 Definition decpmaj:=decimp2 imppmaj.
 Definition pmajpt:=imp2pt imppmaj.
 Definition pmajrf:=imp2rf imppmaj.
 Definition pmajtp:=imp2tp imppmaj.
 Definition pmajfr:=imp2fr imppmaj.

 Theorem impemaj:(emaj☵☨☂femaj).
 Proof.
  unfold Implem2; intros s t; apply (snd imptmaj).
 Qed.

 Definition decemaj:=decimp2 impemaj.
 Definition emajpt:=imp2pt impemaj.
 Definition emajrf:=imp2rf impemaj.
 Definition emajtp:=imp2tp impemaj.
 Definition emajfr:=imp2fr impemaj.

(* Fresh free index function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Returns an index that majors a term; this index is therefore a fresh free variable.   *)

 Fixpoint fpnewf(P:☊){struct P}:☈:=
  match P with
  | ¬P' => fpnewf P'
  | ∀P' => ☋(fpnewf P')
  | P1∧P2 => (fpnewf P1)☰☉(fpnewf P2)
  | P1☲P2 => (fpnewf P1)☰☉(fpnewf P2) 
  | π j => ☦
  | E1♃E2 => (fenewf E1)☰☉(fenewf E2)
  | E1∈E2 => (fenewf E1)☰☉(fenewf E2)
  end
 with fenewf(E:☌){struct E}:☈:=
  match E with
  | ♄ => ☦
  | ☭E' => fenewf E'
  | ☬E' => fenewf E'
  | (E1☵E2) => (fenewf E1)☰☉(fenewf E2)
  | (E1♂E2) => (fenewf E1)☰☉(fenewf E2)
  | ω j => ☦
  | χ j => ☠j
  | S'♀P' => (fenewf S')☰☉☋(fpnewf P')
  end.
 Notation "'☰☋(' P ')'":=(fpnewf P).
 Notation "'☰☍(' E ')'":=(fenewf E).
 (* Nota : ☰☋ and ☰☍ return a fresh free index, but not the smallest one. Once ☰☋ and ☰☍ are      *)
 (* defined however it is possible to use them to define the function returning the       *)
 (* minimal index, by starting from ☦ and incrementing up to the first free index. The     *)
 (* termination of this function is ensured by the use of the value returned by ☰☋ or ☰☍    *)
 (* as a majorant. Yet the expression of this optimal function complexify later proofs *)
 (* with no real benefit as the set of indexes is, whatever, infinite.                                  *)

 (* ☰☋ and ☰☍ return an index >☋ and >☍ *)
 Theorem ftnewfmaj: (forall (P:☊), ☰☋(P)>☋P)*(forall (E:☌), ☰☍(E)>☍E).
 Proof.
  apply (Bst_ind (fun P:☊=>☰☋(P)>☋P) (fun E:☌=>☰☍(E)>☍E)); simpl.
   apply majbig.
   intros i; split; [split | idtac];
    [apply majpvr | apply majbie | apply majvar; unfold igt; apply reflige].
   intros P H; split;
    [apply majnot; apply H
    | apply majfor; destruct ☰☋(P); simpl; [apply (pmajige P _ _ (gefst ☧) H) | apply H]].
   intros E H; split; [apply majchs | apply majpow]; apply H.
   intros P1 P2 H1 H2; split; [apply majand | apply majimp];
    try (apply (pmajige _ _ _ (fimaxgel ☰☋(P1) ☰☋(P2)) H1));
    try (apply (pmajige _ _ _ (fimaxger ☰☋(P1) ☰☋(P2)) H2)).
   intros E1 E2 H1 H2; split; split;
    [apply majequ | apply majins | apply majcpl | apply majpro];
    try (apply (emajige _ _ _ (fimaxgel ☰☍(E1) ☰☍(E2)) H1));
    try (apply (emajige _ _ _ (fimaxger ☰☍(E1) ☰☍(E2)) H2)).
   intros E P HE HP; apply majcmp.
    apply (emajige _ _ _ (fimaxgel ☰☍(E) ☋☰☋(P)) HE).
    destruct ☰☋(P); simpl;
     [apply (pmajige _ _ _ (gefst ☠(☰☍(E)☰☉☦)) HP)
     | rewrite fimaxnxt; apply (pmajige _ _ _ (fimaxger ☠☰☍(E) ☠b) HP)].
 Qed.

 Definition fpnewfmaj:=fst ftnewfmaj.
 Definition fenewfmaj:=snd ftnewfmaj.

 Theorem fpnewfinf: forall (P:☊), ☰☋(P)⍀☋P.
 Proof.
  intros P; apply pmajinf; apply fpnewfmaj.
 Qed.

 Theorem fenewfinf: forall (E:☌), ☰☍(E)⍀☍E.
 Proof.
  intros E; apply emajinf; apply fenewfmaj.
 Qed.

(* Ground terms ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* A ground term has no free variables.                                                                                        *)

 Definition pground(P:☊):=☦>☋P.
 Notation "'*⍀☋(' P ')'":=(pground P).

 Definition eground(E:☌):=☦>☍E.
 Notation "'*⍀☍(' E ')'":=(eground E).

 (* *⍀☋(_) and *⍀☍(_) can also be defined with quantification over ☈ *)
 Theorem pgroundinf:forall (P:☊), *⍀☋(P)->forall (i:☈), i⍀☋P.
 Proof.
  intros P H i; unfold pground in H; apply pmajinf; apply (pmajige _ _ _ (gefst i) H).
 Qed.

 Theorem egroundinf:forall (E:☌), *⍀☍(E)->forall (i:☈), i⍀☍E.
 Proof.
  intros E H i; unfold eground in H; apply emajinf; apply (emajige _ _ _ (gefst i) H).
 Qed.

 Theorem infpground:forall (P:☊), (forall (i:☈), i⍀☋P)->*⍀☋(P).
 Proof.
  intros P H; unfold pground; apply igepmaj; intros j H0; apply H.
 Qed.

 Theorem infeground:forall (E:☌), (forall (i:☈), i⍀☍E)->*⍀☍(E).
 Proof.
  intros E H; unfold eground; apply igeemaj; intros j H0; apply H.
 Qed.

(* Ground terms functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fpground:=fpmaj ☦.
 Notation "'*⍀☋☂(' P ')'":=(fpground P).

 Definition feground:=femaj ☦.
 Notation "'*⍀☍☂(' E ')'":=(feground E).

 (* *⍀☋☂(_) and *⍀☍☂(_) implement *⍀☋(_) and *⍀☍(_) *)
 Theorem imppground:(pground☵☧☂fpground).
 Proof.
  unfold Implem1; intros P; unfold pground; unfold fpground; apply imppmaj.
 Qed.

 Definition decpground:=decimp1 imppground.
 Definition pgroundpt:=imp1pt imppground.
 Definition pgroundrf:=imp1rf imppground.
 Definition pgroundtp:=imp1tp imppground.
 Definition pgroundfr:=imp1fr imppground.

 Theorem impeground:(eground☵☧☂feground).
 Proof.
  unfold Implem1; intros E; unfold eground; unfold feground; apply impemaj.
 Qed.

 Definition deceground:=decimp1 impeground.
 Definition egroundpt:=imp1pt impeground.
 Definition egroundrf:=imp1rf impeground.
 Definition egroundtp:=imp1tp impeground.
 Definition egroundfr:=imp1fr impeground.

(* Characteristation functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Some terms functions may just be defined for specific forms of terms and request an *)
(* adequate proof term as parameter, provided by the following definitions.                    *)

 Inductive pisfor:☊->Set:=pif: forall (P:☊), pisfor (∀P).
 Notation "'?∀(' P ')'":=(pisfor P).

 Inductive pisexs:☊->Set:=pie: forall (P:☊), pisexs (¬(∀(¬P))).
 Notation "'?∃(' P ')'":=(pisexs P).

 Inductive eiscmp:☌->Set:=eic: forall (S:☌)(P:☊), eiscmp (S♀P).
 Notation "'?♀(' E ')'":=(eiscmp E).

 Inductive eiscpl:☌->Set:=eim: forall (E1 E2:☌), eiscpl (E1☵E2).
 Notation "'?☵(' E ')'":=(eiscpl E).

 Inductive eispro:☌->Set:=eip: forall (E1 E2:☌), eispro (E1♂E2).
 Notation "'?♂(' E ')'":=(eispro E).

(*Projection functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fpprjfor(P:☊)(H:?∀(P)):☊:=match P with ∀P' => P' | _ => π☦ end.

 Definition fpprjexs(P:☊)(H:?∃(P)):☊:=match P with ¬(∀¬P') => P' | _ => π☦ end.

 Definition feprjcmpl(E:☌)(H:?♀(E)):☌:=match E with E'♀_ => E' | _ => ω☦ end.

 Definition feprjcmpr(E:☌)(H:?♀(E)):☊:=match E with _♀P' => P' | _ => π☦  end.

 Definition feprjcpll(E:☌)(H:?☵(E)):☌:=match E with (E'☵_) => E' | _ => ω☦ end.
 Notation "'⍃☵(' E '⊢' H ')'":=(feprjcpll E H).

 Definition feprjcplr(E:☌)(H:?☵(E)):☌:=match E with (_☵E') => E' | _ => ω☦ end.
 Notation "'⍄☵(' E '⊢' H ')'":=(feprjcplr E H).

 Definition feprjprol(E:☌)(H:?♂(E)):☌:=match E with (E'♂_) => E' | _ => ω☦ end.
 Notation "'⍃♂(' E '⊢' H ')'":=(feprjprol E H).

 Definition feprjpror(E:☌)(H:?♂(E)):☌:=match E with (_♂E') => E' | _ => ω☦ end.
 Notation "'⍄♂(' E '⊢' H ')'":=(feprjpror E H).

(* π-independance ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Indicates whether or not a specific meta-variable predicate appears in a term.         *)

 Inductive pnpd:☈->☊->Set:=
 | npdnot: forall (i:☈)(P:☊), pnpd i P->pnpd i (¬P)
 | npdfor: forall (i:☈)(P:☊), pnpd i P->pnpd i (∀P)
 | npdand: forall (i:☈)(P1 P2:☊), pnpd i P1->pnpd i P2->pnpd i (P1∧P2)
 | npdimp: forall (i:☈)(P1 P2:☊), pnpd i P1->pnpd i P2->pnpd i (P1☲P2)
 | npdpvr: forall (i j:☈), !(i≡j)->pnpd i (π j)
 | npdequ: forall (i:☈)(E1 E2:☌), enpd i E1->enpd i E2->pnpd i (E1♃E2)
 | npdins: forall (i:☈)(E1 E2:☌), enpd i E1->enpd i E2->pnpd i (E1∈E2)
 with enpd:☈->☌->Set:=
 | npdbig: forall (i:☈), enpd i ♄
 | npdchs: forall (i:☈)(E:☌), enpd i E->enpd i (☭E)
 | npdpow: forall (i:☈)(E:☌), enpd i E->enpd i (☬E)
 | npdcpl: forall (i:☈)(E1 E2:☌), enpd i E1->enpd i E2->enpd i (E1☵E2)
 | npdpro: forall (i:☈)(E1 E2:☌), enpd i E1->enpd i E2->enpd i (E1♂E2)
 | npdbie: forall (i j:☈), enpd i (ω j)
 | npdvar: forall (i j:☈), enpd i (χ j)
 | npdcmp: forall (i:☈)(S:☌)(P:☊), enpd i S->pnpd i P->enpd i (S♀P).
 Infix "⍀π☋":=pnpd (at level 69, no associativity).
 Infix "⍀π☍":=enpd (at level 69, no associativity).

(* π-independance function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fpnpd(i:☈)(P:☊){struct P}:☆:=
  match P with
  | ¬P' => fpnpd i P'
  | ∀P' => fpnpd i P'
  | P1∧P2 => (fpnpd i P1) ∧☁ (fpnpd i P2)
  | P1☲P2 => (fpnpd i P1) ∧☁ (fpnpd i P2)
  | π j => ¬☁(i=☉☂j)
  | E1♃E2 => (fenpd i E1) ∧☁ (fenpd i E2)
  | E1∈E2 => (fenpd i E1) ∧☁ (fenpd i E2)
  end
 with fenpd(i:☈)(E:☌){struct E}:☆:=
  match E with
  | ♄ => ⊤
  | ☭E' => fenpd i E'
  | ☬E' => fenpd i E'
  | (E1☵E2) => (fenpd i E1) ∧☁ (fenpd i E2)
  | (E1♂E2) => (fenpd i E1) ∧☁ (fenpd i E2)
  | ω j => ⊤
  | χ j => ⊤
  | E'♀P' => (fenpd i E') ∧☁ (fpnpd i P')
  end.

 (* fpnpd and fenpd implement ⍀π☋ and ⍀π☍ *)
 Theorem imptnpd:(forall (P:☊)(i:☈), (fpnpd i P≡⊤->i⍀π☋P)*(fpnpd i P≡⊥->!(i⍀π☋P)))*
                               (forall (E:☌)(i:☈), (fenpd i E≡⊤->i⍀π☍E)*(fenpd i E≡⊥->!(i⍀π☍E))).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (i:☈), (fpnpd i P≡⊤->i⍀π☋P)*(fpnpd i P≡⊥->!(i⍀π☋P)))
                             (fun (E:☌)=>forall (i:☈), (fenpd i E≡⊤->i⍀π☍E)*(fenpd i E≡⊥->!(i⍀π☍E)))); simpl.
   intros i; split; intros H; [apply npdbig | invset H].
   intros i; split; [split | idtac]; intros j; split; intros H; try (invset H; fail).
    apply npdpvr; intros H0; apply (iequfr _ _ (dedfbnott _ H)); apply H0.
    intros H0; invset_clear H0; apply H1; apply iequtp; apply (dedfbnotf _ H).
    apply npdbie.
    apply npdvar.
   intros P H; split; intros i; split; intros H0;
    try (intros H1; invset_clear H1; apply (snd (H i) H0 H2));
    [apply npdnot; apply (fst (H i) H0) | apply npdfor; apply (fst (H i) H0)].
   intros E H; split; intros i; split; intros H0;
    try (intros H1; invset_clear H1; apply (snd (H i) H0 H2));
    [apply npdchs; apply (fst (H i) H0) | apply npdpow; apply (fst (H i) H0)].
   intros P1 P2 H1 H2; split; intros i; split; intros H;
    (destruct (dedfbandt _ _ H) || (destruct (dedfbandf _ _ H); intros H0; invset H0)).
    apply npdand; [apply (fst (H1 i) i0) | apply (fst (H2 i) i1)].
    apply (snd (H1 i) i0 H6). apply (snd (H2 i) i0 H7).
    apply npdimp; [apply (fst (H1 i) i0) | apply (fst (H2 i) i1)].
    apply (snd (H1 i) i0 H6). apply (snd (H2 i) i0 H7).
   intros E1 E2 H1 H2; split; split; intros i; split; intros H;
    (destruct (dedfbandt _ _ H) || (destruct (dedfbandf _ _ H); intros H0; invset H0)).
    apply npdequ; [apply (fst (H1 i) i0) | apply (fst (H2 i) i1)].
    apply (snd (H1 i) i0 H6). apply (snd (H2 i) i0 H7).
    apply npdins; [apply (fst (H1 i) i0) | apply (fst (H2 i) i1)].
    apply (snd (H1 i) i0 H6). apply (snd (H2 i) i0 H7).
    apply npdcpl; [apply (fst (H1 i) i0) | apply (fst (H2 i) i1)].
    apply (snd (H1 i) i0 H6). apply (snd (H2 i) i0 H7).
    apply npdpro; [apply (fst (H1 i) i0) | apply (fst (H2 i) i1)].
    apply (snd (H1 i) i0 H6). apply (snd (H2 i) i0 H7).
   intros S P H1 H2 i; split; intros H;
    (destruct (dedfbandt _ _ H) || (destruct (dedfbandf _ _ H); intros H0; invset H0)).
    apply npdcmp; [apply (fst (H1 i) i0) | apply (fst (H2 i) i1)].
    apply (snd (H1 i) i0 H6). apply (snd (H2 i) i0 H7).
 Qed.

 Theorem imppnpd:(pnpd☵☨☂fpnpd).
 Proof.
  unfold Implem2; intros s t; apply (fst imptnpd).
 Qed.

 Definition decpnpd:=decimp2 imppnpd.
 Definition pnpdpt:=imp2pt imppnpd.
 Definition pnpdrf:=imp2rf imppnpd.
 Definition pnpdtp:=imp2tp imppnpd.
 Definition pnpdfr:=imp2fr imppnpd.

 Theorem impenpd:(enpd☵☨☂fenpd).
 Proof.
  unfold Implem2; intros s t; apply (snd imptnpd).
 Qed.

 Definition decenpd:=decimp2 impenpd.
 Definition enpdpt:=imp2pt impenpd.
 Definition enpdrf:=imp2rf impenpd.
 Definition enpdtp:=imp2tp impenpd.
 Definition enpdfr:=imp2fr impenpd.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)