(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq2, formalisation of the B theory into the Coq system
   Module: Mbool, booleans
   Booleans are used as return type for functions implementing decidable properties.
   The main operators are described, and formal definition of the implementation of a
   decidable property is provided, with associated results.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - September 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Mscratch.

(* Definition of the boolean type ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Inductive Bbol:Set:=btrue | bfalse.
 Notation "☆":=Bbol.
 Notation "⊤":=btrue.
 Notation "⊥":=bfalse.

(* Not function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fbnot(b:☆):☆:=match b with ⊤ => ⊥ | ⊥ => ⊤ end.
 Notation "¬☁":=fbnot (right associativity, at level 1).

 Theorem idemfbnot:forall (b:☆), ¬☁(¬☁b)≡b.
 Proof.
  induction b; simpl; apply refl_id.
 Qed.

 Theorem dedfbnott:forall (b:☆), ¬☁b≡⊤->b≡⊥.
 Proof.
  induction b; simpl; intros H; [invset_clear H; discriminate H0 | apply refl_id].
 Qed.

 Theorem dedfbnotf:forall (b:☆), ¬☁b≡⊥->b≡⊤.
 Proof.
  induction b; simpl; intros H; [apply refl_id | invset_clear H; discriminate H0].
 Qed.

 Theorem invfbnott:forall (b:☆), b≡⊥->¬☁b≡⊤.
 Proof.
  intros b H; rewrite H; apply refl_id.
 Qed.

 Theorem invfbnotf:forall (b:☆), b≡⊤->¬☁b≡⊥.
 Proof.
  intros b H; rewrite H; apply refl_id.
 Qed.

(* And function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fband(b1 b2:☆):☆:=match b1 with ⊥ => ⊥ | ⊤ => b2 end.
 Infix "∧☁":=fband (at level 20, right associativity).

 Theorem commfband:forall (b1 b2:☆), b1∧☁b2≡b2∧☁b1.
 Proof.
  induction b1; induction b2; apply refl_id.
 Qed.

 Theorem shortfbandr:forall (b:☆), b∧☁⊥≡⊥.
 Proof.
  induction b; apply refl_id.
 Qed.

 Theorem dedfbandt:forall (b1 b2:☆), b1∧☁b2≡⊤->(b1≡⊤)*(b2≡⊤).
 Proof.
  induction b1; intros b2 H;
   [simpl in H; rewrite H; split; apply refl_id | invset H; invset H0].
 Qed.

 Theorem dedfbandf:forall (b1 b2:☆), b1∧☁b2≡⊥->(b1≡⊥)+(b2≡⊥).
 Proof.
  induction b1; intros b2 H; [right; simpl in H; rewrite H | left]; apply refl_id.
 Qed.

 Theorem invfbandt:forall (b1 b2:☆), b1≡⊤->b2≡⊤->b1∧☁b2≡⊤.
 Proof.
  intros b1 b2 H H0; rewrite H; rewrite H0; apply refl_id.
 Qed.

 Theorem invfbandf:forall (b1 b2:☆), (b1≡⊥)+(b2≡⊥)->b1∧☁b2≡⊥.
 Proof.
  intros b1 b2 H; destruct H; rewrite i; [idtac | induction b1]; apply refl_id.
 Qed.

(* Or function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fbor(b1 b2:☆):☆:=match b1 with ⊤ => ⊤ | ⊥ => b2 end.
 Infix "∨☁":=fbor (at level 20, right associativity).

 Theorem commfbor:forall (b1 b2:☆), b1∨☁b2≡b2∨☁b1.
 Proof.
  induction b1; induction b2; apply refl_id.
 Qed.

 Theorem shortfborr:forall (b:☆), b∨☁⊤≡⊤.
 Proof.
  induction b; apply refl_id.
 Qed.

 Theorem dedfbort:forall (b1 b2:☆), b1∨☁b2≡⊤->(b1≡⊤)+(b2≡⊤).
 Proof.
  induction b1; intros b2 H; [left | simpl in H; rewrite H; right]; apply refl_id.
 Qed.

 Theorem dedfborf:forall (b1 b2:☆), b1∨☁b2≡⊥->(b1≡⊥)*(b2≡⊥).
 Proof.
  induction b1; intros b2 H;
   [invset H; invset H0 | simpl in H; rewrite H; split; apply refl_id].
 Qed.

 Theorem invfbort:forall (b1 b2:☆), (b1≡⊤)+(b2≡⊤)->(b1∨☁b2≡⊤).
 Proof.
  intros b1 b2 H; destruct H; rewrite i; [idtac | induction b1]; simpl; apply refl_id.
 Qed.

 Theorem invfborf:forall (b1 b2:☆), b1≡⊥->b2≡⊥->b1∨☁b2≡⊥.
 Proof.
  intros b1 b2 H H0; rewrite H; rewrite H0; apply refl_id.
 Qed.

(* Elimination of boolean functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem ElimB1:forall (S:Set)(f:S->☆)(s:S), (f s≡⊤)+(f s≡⊥).
 Proof.
  intros S f s; elim f; [left | right] ; apply refl_id.
 Qed.
 Implicit Arguments ElimB1 [S].

 Theorem ElimB2:forall (S T:Set)(f:S->T->☆)(s:S)(t:T), (f s t≡⊤)+(f s t≡⊥).
 Proof.
  intros S T f s t; elim f; [left | right] ; apply refl_id.
 Qed.
 Implicit Arguments ElimB2 [S T].

 Theorem ElimB3:forall (S T U:Set)(f:S->T->U->☆)(s:S)(t:T)(u:U), (f s t u≡⊤)+(f s t u≡⊥).
 Proof.
  intros S T U f s t u; elim f; [left | right] ; apply refl_id.
 Qed.
 Implicit Arguments ElimB3 [S T U].

(* Implementation of decidable properties ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Implementation establishes a relationship between a property (a proof object) and   *)
(* a ☆ function of the same arity. The ☆ function f is said to implements the property P *)
(* iff f ⊤ implies P provable (inhabited) and f ⊥ P refutable - f being therefore a           *)
(* computation deciding P.                                                                                                              *)

 Definition Implem1(S:Set)(f:S->☆)(P:S->Set):Set:=forall (s:S), (f s≡⊤->P s)*(f s≡⊥->!(P s)).
 Implicit Arguments Implem1 [S].
 Notation "'(' P '☵☧☂' f ')'":=(Implem1 f P).

 Definition Implem2(S T:Set)(f:S->T->☆)(P:S->T->Set):Set:=
  forall (s:S)(t:T), (f s t≡⊤->P s t)*(f s t≡⊥->!(P s t)).
 Implicit Arguments Implem2 [S T].
 Notation "'(' P '☵☨☂' f ')'":=(Implem2 f P).

 Definition Implem3(S T U:Set)(f:S->T->U->☆)(P:S->T->U->Set):Set:=
  forall (s:S)(t:T)(u:U), (f s t u≡⊤->P s t u)*(f s t u≡⊥->!(P s t u)).
 Implicit Arguments Implem3 [S T U].
 Notation "'(' P '☵☩☂' f ')'":=(Implem3 f P).

(* Implementation utilities ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* The following results allows to easily switch between functions and properties...    *)

 Theorem imp1tp:forall (S:Set)(f:S->☆)(P:S->Set), (P☵☧☂f)->forall (s:S), f s≡⊤->P s.
 Proof.
  intros S f P; unfold Implem1; intros H s H0; destruct (H s); apply (p H0).
 Qed.
 Implicit Arguments imp1tp [S f P].

 Theorem imp1fr:forall (S:Set)(f:S->☆)(P:S->Set), (P☵☧☂f)->forall (s:S), f s≡⊥->!(P s).
 Proof.
  intros S f P; unfold Implem1; intros H s H0 H1; destruct (H s); apply (e H0 H1).
 Qed.
 Implicit Arguments imp1fr [S f P].

 Theorem imp1pt:forall (S:Set)(f:S->☆)(P:S->Set), (P☵☧☂f)->forall (s:S), P s->f s≡⊤.
 Proof.
  intros S f P H s H0; elim (H s); clear H; intros H1 H2; destruct (f s);
   [apply refl_id | elim H2; [apply refl_id | apply H0]].
 Qed.
 Implicit Arguments imp1pt [S f P].

 Theorem imp1rf:forall (S:Set)(f:S->☆)(P:S->Set), (P☵☧☂f)->forall (s:S), !(P s)->f s≡⊥.
 Proof.
  intros S f P H s H0; elim (H s); clear H; intros H H1; destruct (f s);
   [elim H0; apply H | idtac]; apply refl_id.
 Qed.
 Implicit Arguments imp1rf [S f P].

 Theorem imp2tp:forall (S T:Set)(f:S->T->☆)(P:S->T->Set),
                             (P☵☨☂f)->forall (s:S)(t:T), f s t≡⊤->P s t.
 Proof.
  intros S T f P; unfold Implem2; intros H s t H0; destruct (H s t); apply (p H0).
 Qed.
 Implicit Arguments imp2tp [S T f P].

 Theorem imp2fr:forall (S T:Set)(f:S->T->☆)(P:S->T->Set),
                             (P☵☨☂f)->forall (s:S)(t:T), f s t≡⊥->!(P s t).
 Proof.
  intros S T f P; unfold Implem2; intros H s t H0 H1; destruct (H s t); apply (e H0 H1).
 Qed.
 Implicit Arguments imp2fr [S T f P].

 Theorem imp2pt:forall (S T:Set)(f:S->T->☆)(P:S->T->Set),
                             (P☵☨☂f)->forall (s:S)(t:T), P s t->f s t≡⊤.
 Proof.
  intros S T f P H s t H0; elim (H s t); clear H; intros H H1; destruct (f s t); 
   [apply refl_id | elim H1; [apply refl_id | apply H0]].
 Qed.
 Implicit Arguments imp2pt [S T f P].

 Theorem imp2rf:forall (S T:Set)(f:S->T->☆)(P:S->T->Set),
                             (P☵☨☂f)->forall (s:S)(t:T), !(P s t)->f s t≡⊥.
 Proof.
  intros S T f P H s t H0; elim (H s t); clear H; intros H H1; destruct (f s t);
   [elim H0; apply H | idtac]; apply refl_id.
 Qed.
 Implicit Arguments imp2rf [S T f P].

 Theorem imp3tp:forall (S T U:Set)(f:S->T->U->☆)(P:S->T->U->Set),
                             (P☵☩☂f)->forall (s:S)(t:T)(u:U), f s t u≡⊤->P s t u.
 Proof.
  intros S T U f P; unfold Implem3; intros H s t u H0; destruct (H s t u); apply (p H0).
 Qed.
 Implicit Arguments imp3tp [S T U f P].

 Theorem imp3fr:forall (S T U:Set)(f:S->T->U->☆)(P:S->T->U->Set),
                             (P☵☩☂f)->forall (s:S)(t:T)(u:U), f s t u≡⊥->!(P s t u).
 Proof.
  intros S T U f P; unfold Implem3; intros H s t u H0 H1; destruct (H s t u); apply (e H0 H1).
 Qed.
 Implicit Arguments imp3fr [S T U f P].

 Theorem imp3pt:forall (S T U:Set)(f:S->T->U->☆)(P:S->T->U->Set),
                             (P☵☩☂f)->forall (s:S)(t:T)(u:U), P s t u->f s t u≡⊤.
 Proof.
  intros S T U f P H s t u H0; elim (H s t u); clear H; intros H H1; destruct (f s t u); 
   [apply refl_id | elim H1; [apply refl_id | apply H0]].
 Qed.
 Implicit Arguments imp3pt [S T U f P].

 Theorem imp3rf:forall (S T U:Set)(f:S->T->U->☆)(P:S->T->U->Set),
                             (P☵☩☂f)->forall (s:S)(t:T)(u:U), !(P s t u)->f s t u≡⊥.
 Proof.
  intros S T U f P H s t u H0; elim (H s t u); clear H; intros H H1; destruct (f s t u);
   [elim H0; apply H | idtac]; apply refl_id.
 Qed.
 Implicit Arguments imp3rf [S T U f P].

(* Decidability of implemented properties ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* As indicated, if f implements P, f decides P therefore P is decidable.                            *)

 Theorem decimp1:forall (S:Set)(f:S->☆)(P:S->Set), (P☵☧☂f)->forall (s:S), (P s)+!(P s).
 Proof.
  intros S f P H s; elim (H s); clear H; intros H H0; destruct (f s);
   [left; apply H | right; apply H0]; apply refl_id.
 Qed.
 Implicit Arguments decimp1 [S f P].

 Theorem decimp2:forall (S T:Set)(f:S->T->☆)(P:S->T->Set),
                               (P☵☨☂f)->forall (s:S)(t:T), (P s t)+!(P s t).
 Proof.
  intros S T f P H s t; elim (H s t); clear H; intros H H0; destruct (f s t);
   [left; apply H | right; apply H0]; apply refl_id.
 Qed.
 Implicit Arguments decimp2 [S T f P].

 Theorem decimp3:forall (S T U:Set)(f:S->T->U->☆)(P:S->T->U->Set),
                               (P☵☩☂f)->forall (s:S)(t:T)(u:U), (P s t u)+!(P s t u).
 Proof.
  intros S T U f P H s t u; elim (H s t u); clear H; intros H H0; destruct (f s t u);
   [left; apply H | right; apply H0]; apply refl_id.
 Qed.
 Implicit Arguments decimp3 [S T U f P].

(* Commutation of parameters of functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* The use of the implementation principle will, in some case, require commutation of   *)
(* the parameters of functions of arity 2 or 3 ; these operations are provided here.       *)

 Theorem implemswap21:forall (S T:Set)(P:S->T->Set)(f:S->T->☆),
                                         (P☵☨☂f)->((fun (t:T)(s:S)=>P s t)☵☨☂(fun (t:T)(s:S)=>f s t)).
 Proof.
  intros S T P f H; unfold Implem2; intros t s; apply H.
 Qed.
 Implicit Arguments implemswap21 [S T P f].

 Theorem implemswap132:forall (S T U:Set)(P:S->T->U->Set)(f:S->T->U->☆),
                                           (P☵☩☂f)->((fun (s:S)(u:U)(t:T)=>P s t u)☵☩☂(fun (s:S)(u:U)(t:T)=>f s t u)).
 Proof.
  intros S T U P f H; unfold Implem3; intros s u t; apply H.
 Qed.
 Implicit Arguments implemswap132 [S T U P f].

 Theorem implemswap213:forall (S T U:Set)(P:S->T->U->Set)(f:S->T->U->☆),
                                           (P☵☩☂f)->((fun (t:T)(s:S)(u:U)=>P s t u)☵☩☂(fun (t:T)(s:S)(u:U)=>f s t u)).
 Proof.
  intros S T U P f H; unfold Implem3; intros t s u; apply H.
 Qed.
 Implicit Arguments implemswap213 [S T U P f].

 Theorem implemswap231:forall (S T U:Set)(P:S->T->U->Set)(f:S->T->U->☆),
                                           (P☵☩☂f)->((fun (t:T)(u:U)(s:S)=>P s t u)☵☩☂(fun (t:T)(u:U)(s:S)=>f s t u)).
 Proof.
  intros S T U P f H; unfold Implem3; intros t u s; apply H.
 Qed.
 Implicit Arguments implemswap231 [S T U P f].

 Theorem implemswap312:forall (S T U:Set)(P:S->T->U->Set)(f:S->T->U->☆),
                                           (P☵☩☂f)->((fun (u:U)(s:S)(t:T)=>P s t u)☵☩☂(fun (u:U)(s:S)(t:T)=>f s t u)).
 Proof.
  intros S T U P f H; unfold Implem3; intros u s t; apply H.
 Qed.
 Implicit Arguments implemswap312 [S T U P f].

 Theorem implemswap321:forall (S T U:Set)(P:S->T->U->Set)(f:S->T->U->☆),
                                           (P☵☩☂f)->((fun (u:U)(t:T)(s:S)=>P s t u)☵☩☂(fun (u:U)(t:T)(s:S)=>f s t u)).
 Proof.
  intros S T U P f H; unfold Implem3; intros u t s; apply H.
 Qed.
 Implicit Arguments implemswap321 [S T U P f].

(* Illustration of the implementation principle ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fbequ(b1 b2:☆):☆:=match b1 with ⊤ => b2 | ⊥ => ¬☁ b2 end.
 Infix "=☇☂":=fbequ (no associativity, at level 20).

 Theorem impbequ:(identity (A:=☆)☵☨☂fbequ).
 Proof.
  unfold Implem2; induction s; induction t; simpl; split; intros H; invset_clear H;
   try (discriminate H0); try (apply refl_id);
   try (intros H'; invset_clear H'; discriminate H).
 Qed.

 Definition decbequ:=decimp2 impbequ.
 Definition bequpt:=imp2pt impbequ.
 Definition bequrf:=imp2rf impbequ.
 Definition bequtp:=imp2tp impbequ.
 Definition bequfr:=imp2fr impbequ.

(* Remark about the implementation principle ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Another approach is possible to implement decidable properties, using Coq proofs    *)
(* as computations... The first step is to prove decidability, then to define a boolean  *)
(* function associated to the decidability proof. Remember that to do so, theorems        *)
(* have to be Defined and not Qed-ed, to keep transparent the computational content.     *)
(* This approach of implementation HAS NOT be retained in this project, in order to       *)
(* keep control of the extracted code (both for performances and readibility).                *)

 Definition Decidable2(S T:Set)(P:S->T->Set):=forall (s:S)(t:T), (P s t)+!(P s t).
 Implicit Arguments Decidable2 [S T].

 Theorem BoolDecidable2:forall (S T:Set)(P:S->T->Set),
                                             Decidable2 P->
                                             {f:S->T->☆ & forall (s:S)(t:T), (P s t->f s t≡ ⊤)*(!(P s t)->f s t≡ ⊥)}.
 Proof.
  intros S T P H; unfold Decidable2 in H;
   exists (fun (s:S)(t:T)=>match (H s t) with inl _ => ⊤ | inr _ => ⊥ end).
   intros s t; split; intros H'; destruct (H s t);
    [apply refl_id | contradiction | contradiction | apply refl_id].
 Defined.
 Implicit Arguments BoolDecidable2 [S T P].

 Theorem decbequ':Decidable2 (identity (A:=☆)).
 Proof.
  unfold Decidable2; induction s; induction t.
   left; apply refl_id.
   right; intros H; invset H; discriminate H0.
   right; intros H; invset H; discriminate H0.
   left; apply refl_id.
 Defined.
 (* Nota: decbequ' is defined and not Qed-ed to use its computational content.                  *)

 Definition fbequ':=projS1 (BoolDecidable2 decbequ').
 (* Nota: this function can be tested by e.g. Eval compute in (fbequ' ⊤ ⊤)                               *)
 (* The code of the function is obtained by Recursive Extraction fbequ' (but the result *)
 (* is, of course, rather terrible...).                                                                                             *)

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
