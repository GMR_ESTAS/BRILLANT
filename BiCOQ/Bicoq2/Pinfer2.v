(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Pinfer2, Binfer2 associated prover functions
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - October 2006
   ♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Binfer2.

(* Alternate ∀ prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fbkfori2(S:☒)(v w:☈)(P:☊):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,Q) => match (☬☢(v∙P)=☋☂Q) with
                                               | ⊤ => match w⍀☑☂(G ☑★ P) with ⊤ => ☬☓(G,〈v♇☍χ w@☋P〉) | ⊥ => S end
                                               | ⊥ => S end end).

 Theorem corrfbkfori2:forall (S:☒)(v w:☈)(P:☊), ∧☓(fbkfori2 S v w P)->☭☓(S).
 Proof.
  induction S; intros v w P; unfold fbkfori2; intros H; destruct (lsinfins _ _ H);
   clear H l; destruct (decpequ ☬☢(v∙P) b0).
   rewrite <- (sym_id (pequpt _ _ i)) in s; rewrite <- i in s; rewrite (sym_id i); clear i b0;
    destruct (decginf w (b ☑★ P)); unfold fginf in s.
    rewrite <- (sym_id (ginfpt _ _ l)) in s; apply (bifori2 _ _ _ _ l s).
    rewrite <- (sym_id (ginfrf _ _ e)) in s; apply s.
   rewrite <- (sym_id (pequrf _ _ e)) in s; apply s.
 Qed.

 Definition fbkfori3(S:☒)(v:☈)(P:☊):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,Q) => match (☬☢(v∙P)=☋☂Q) with
                                               | ⊤ => match v⍀☋☂P with ⊤ => ☬☓(G,P) | ⊥ => S end
                                               | ⊥ => S end end).

 Theorem corrfbkfori3:forall (S:☒)(v:☈)(P:☊), ∧☓(fbkfori3 S v P)->☭☓(S).
 Proof.
  induction S; intros v P; unfold fbkfori3; intros H; destruct (lsinfins _ _ H); clear H l;
   destruct (decpequ ☬☢(v∙P) b0).
   rewrite <- (sym_id (pequpt _ _ i)) in s; rewrite <- i in s; rewrite (sym_id i); clear i b0;
    destruct (decpinf v P).
    rewrite <- (sym_id (pinfpt  _ _ p)) in s; apply (bifori3 _ _ _ p s).
    rewrite <- (sym_id (pinfrf  _ _ e)) in s; apply s.
   rewrite <- (sym_id (pequrf _ _ e)) in s; apply s.
 Qed.

 Definition fbkfore2(S:☒)(v:☈):[☒*]:=match S with ☬☓(G,P) => ∅☓ ☓★ ☬☓(G,☬☢(v∙P)) end.

 Theorem corrfbkfore2:forall (S:☒)(v:☈), ∧☓(fbkfore2 S v)->☭☓(S).
 Proof.
  induction S; intros v; unfold fbkfore2; intros H; simpl; apply (bifore2 b v b0);
   destruct (lsinfins _ _ H); apply s.
 Qed.

(* Renaming of free variables rules prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fbkidxalpha(S:☒)(v w:☈)(P:☊):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,Q) => match v⍀☑☂G with
                                               | ⊤ => match 〈v♇☍χ(w)@☋P〉=☋☂Q with ⊤ => ☬☓(G,P) | ⊥ => S end
                                               | ⊥ => S end end).

 Theorem corrfbkidxalpha:forall (S:☒)(v w:☈)(P:☊), ∧☓(fbkidxalpha S v w P)->☭☓(S).
 Proof.
  induction S; intros v w P; unfold fbkidxalpha; intros H; destruct (lsinfins _ _ H);
   clear H l; destruct (decginf v b).
   unfold fginf in s; rewrite <- (sym_id (ginfpt _ _ l)) in s;
    destruct (decpequ 〈v♇☍χ(w)@☋P〉 b0).
    rewrite <- (sym_id (pequpt _ _ i)) in s; rewrite (sym_id i); simpl;
     apply (idxalpha _ _ w P l); apply s.
    rewrite <- (sym_id (pequrf _ _ e)) in s; simpl; apply s.
   unfold fginf in s; rewrite <- (sym_id (ginfrf _ _ e)) in s; apply s.
 Qed.

 Definition fbkidxalpha'(S:☒)(v w:☈):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,Q) => match w⍀☑☂(G☑★Q) with ⊤ => ☬☓(G,〈v♇☍χ(w)@☋Q〉) | ⊥ => S end end).

 Theorem corrfbkidxalpha':forall (S:☒)(v w:☈), ∧☓(fbkidxalpha' S v w)->☭☓(S).
 Proof.
  induction S; intros v w; unfold fbkidxalpha; intros H; destruct (lsinfins _ _ H);
   clear H l; destruct (decginf w (b☑★b0)).
   unfold fginf in s; rewrite <- (sym_id (ginfpt _ _ l)) in s; simpl;
    apply (idxalpha' _ v _ _ l); apply s.
   unfold fginf in s; rewrite <- (sym_id (ginfrf _ _ e)) in s; apply s.
 Qed.

(* Alternate ♂ prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fbkproi2(S:☒):[☒*]:=
  match S with ☬☓(G,(E☵F)∈(T♂U)) => ∅☓ ☓★ ☬☓(G,E∈T) ☓★ ☬☓(G,F∈U) | _ => ∅☓ ☓★ S end.

 Theorem corrfbkproi2:forall (S:☒), ∧☓(fbkproi2 S)->☭☓(S).
 Proof.
  induction S; unfold fbkproi2; intros H; destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b1;
   destruct (lsinfins _ _ H); try (apply s); destruct (lsinfins _ _ l); clear H l l0.
  simpl; apply biproi2; [apply s0 | apply s].
 Qed.

(* Alternate ♀ prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fbkcmpe(S:☒)(f:☈)(P:☊):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,E∈T∧P') => match P'=☋☂〈f♇☍E@☋P〉 with ⊤ => ☬☓(G,E∈ ☬☤(f∙T∙P)) | ⊥ => S end
                              | _ => S end).

 Theorem corrfbkcmpe:forall (S:☒)(f:☈)(P:☊), ∧☓(fbkcmpe S f P)->☭☓(S).
 Proof.
  induction S; intros f P; unfold fbkcmpe; intros H; destruct (lsinfins _ _ H); clear H l;
   destruct b0; try (apply s); destruct b0_1; try (apply s);
   destruct (decpequ b0_2 〈f♇☍b0@☋P〉).
   rewrite <- (sym_id (pequpt _ _ i)) in s; rewrite i; simpl; apply bicmpe; apply s.
   rewrite <- (sym_id (pequrf _ _ e)) in s; apply s.
 Qed.

(* Derived ∃ prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fbkexsi(S:☒)(v:☈)(P:☊)(E:☌):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,P') => match P'=☋☂☬☣(v∙P) with ⊤ => ☬☓(G,〈v♇☍E@☋P〉) | ⊥ => S end end).

 Theorem corrfbkexsi:forall (S:☒)(v:☈)(P:☊)(E:☌), ∧☓(fbkexsi S v P E)->☭☓(S).
 Proof.
  induction S; intros v P E; unfold fbkexsi; intros H; destruct (lsinfins _ _ H); clear H l;
   destruct (decpequ b0 ☬☣(v∙P)).
   rewrite <- (sym_id (pequpt _ _ i)) in s; rewrite i; simpl; apply biexsi with (E:=E); apply s.
   rewrite <- (sym_id (pequrf _ _ e)) in s; apply s.
 Qed.

(* Derived ♃ prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fbksymeq(S:☒):[☒*]:=∅☓ ☓★ (match S with ☬☓(G,E♃F) => ☬☓(G,F♃E) | _ =>S end).

 Theorem corrfbksymeq:forall (S:☒), ∧☓(fbksymeq S)->☭☓(S).
 Proof.
  induction S; unfold fbksymeq; intros H; destruct (lsinfins _ _ H); clear H l; destruct b0;
   try (apply s); simpl; apply symequ; apply s.
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
