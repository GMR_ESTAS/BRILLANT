(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Baffec, terms affectation (expressions and predicates)
   The B-Book defines from the start the simple substitution (replacing a free variable
   by an expression), and use it to define predicates semantic. In this project, this
   simple substitution is not considered as a term constructor or as a predicate
   transformer, but directly as a function on terms.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - October 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Bbind.

(* Expression affectation function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fpaffexp(v:☈)(F:☌)(P:☊){struct P}:☊:=
  match P with
  | ¬P' => ¬ (fpaffexp v F P')
  | ∀P' => ∀ (fpaffexp ☠v ☯☍(F) P')
  | P1∧P2 => (fpaffexp v F P1) ∧ (fpaffexp v F P2)
  | P1☲P2 => (fpaffexp v F P1) ☲ (fpaffexp v F P2)
  | π i => π i
  | E1♃E2 => (feaffexp v F E1) ♃ (feaffexp v F E2)
  | E1∈E2 => (feaffexp v F E1) ∈ (feaffexp v F E2)
  end
 with feaffexp(v:☈)(F E:☌){struct E}:☌:=
  match E with
  | ♄ => ♄
  | ☭E' => ☭ (feaffexp v F E')
  | ☬E' => ☬ (feaffexp v F E')
  | (E1☵E2) => ((feaffexp v F E1) ☵ (feaffexp v F E2))
  | (E1♂E2) => ((feaffexp v F E1) ♂ (feaffexp v F E2))
  | ω i => ω i
  | χ i => match (v=☉☂i) with ⊤ => F | ⊥ => χ i end
  | E'♀P' => (feaffexp v F E')♀(fpaffexp ☠v ☯☍(F) P')
  end.
 Notation "'〈' v '♇☍' F '@☋' P '〉'":=(fpaffexp v F P).
 Notation "'〈' v '♇☍' F '@☍' E '〉'":=(feaffexp v F E).

 (* [♇☍@] to the variable itself has no effect *)
 Theorem ftaffexpid:(forall (P:☊)(v:☈), 〈v♇☍χ(v)@☋P〉≡P)*(forall (E:☌)(v:☈), 〈v♇☍χ(v)@☍E〉≡E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (v:☈), 〈v♇☍χ(v)@☋P〉≡P)
                             (fun E:☌=>forall (v:☈), 〈v♇☍χ(v)@☍E〉≡E)); simpl.
   intros v; apply refl_id.
   intros i; split; [split | idtac]; intros v; try (apply refl_id); destruct (deciequ v i);
    [rewrite (iequpt _ _ i0); rewrite i0 | rewrite (iequrf _ _ e)]; apply refl_id.
   intros P H; split; intros v;
    [rewrite H | unfold feuplft; simpl; rewrite H]; apply refl_id.
   intros E H; split; intros v; rewrite H; apply refl_id.
   intros P1 P2 H1 H2; split; intros v; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros v; rewrite H1; rewrite H2; apply refl_id.
   intros E P H1 H2 v; rewrite H1; unfold feuplft; simpl; rewrite H2; apply refl_id.
 Qed.

 Definition fpaffexpid:=fst ftaffexpid.
 Definition feaffexpid:=snd ftaffexpid.

 (* 〈♇☍@〉 on a non-free variable has no effect *)
 Theorem ftaffexpnulf:(forall (P:☊)(F:☌)(v:☈), v⍀☋☂P≡⊤->〈v♇☍F@☋P〉≡P)*
                                         (forall (E F:☌)(v:☈), v⍀☍☂E≡⊤->〈v♇☍F@☍E〉≡E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F:☌)(v:☈), v⍀☋☂P≡⊤->〈v♇☍F@☋P〉≡P)
                             (fun E:☌=>forall (F:☌)(v:☈), v⍀☍☂E≡⊤->〈v♇☍F@☍E〉≡E)); simpl.
   intros F v H; apply refl_id.
   intros i; split; [split | idtac]; intros F v H; try (rewrite (dedfbnott _ H));
    apply refl_id.
   intros P HP; split; intros F v H; [rewrite (HP F _ H) | rewrite (HP ☯☍(F) _ H)]; apply refl_id.
   intros E HE; split; intros F v H; rewrite (HE F _ H); apply refl_id.
   intros P1 P2 H1 H2; split; intros F v H; destruct (dedfbandt _ _ H); rewrite (H1 F _ i);
    rewrite (H2 F _ i0); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F v H; destruct (dedfbandt _ _ H);
    rewrite (H1 F _ i); rewrite (H2 F _ i0); apply refl_id.
   intros E P H1 H2 F v H; destruct (dedfbandt _ _ H); rewrite (H1 F _ i); rewrite (H2 ☯☍(F) _ i0);
    apply refl_id.
 Qed.

 Definition fpaffexpnulf:=fst ftaffexpnulf.
 Definition feaffexpnulf:=snd ftaffexpnulf.

 Theorem fpaffexpnul:forall (P:☊)(F:☌)(v:☈), v⍀☋P->〈v♇☍F@☋P〉≡P.
 Proof.
  intros P F v H; apply (fpaffexpnulf _ F _ (pinfpt _ _ H)).
 Qed.

 Theorem feaffexpnul:forall (E F:☌)(v:☈), v⍀☍E->〈v♇☍F@☍E〉≡E.
 Proof.
  intros E F v H; apply (feaffexpnulf _ F _ (einfpt _ _ H)).
 Qed.

 (* 〈♇☍@〉 replaces all occurrences of a free variable, except those from H *)
 Theorem ftaffexpinff:(forall (P:☊)(F:☌)(v:☈), v⍀☍☂F≡⊤->v⍀☋☂〈v♇☍F@☋P〉≡⊤)*
                                         (forall (E F:☌)(v:☈), v⍀☍☂F≡⊤->v⍀☍☂〈v♇☍F@☍E〉≡⊤).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F:☌)(v:☈), v⍀☍☂F≡⊤->v⍀☋☂〈v♇☍F@☋P〉≡⊤)
                             (fun E:☌=>forall (F:☌)(v:☈), v⍀☍☂F≡⊤->v⍀☍☂〈v♇☍F@☍E〉≡⊤)); simpl.
   intros F v H; apply refl_id.
   intros i; split; [split | idtac]; intros F v H; try (apply refl_id); destruct (deciequ v i).
    rewrite (iequpt _ _ i0); apply H.
    rewrite (iequrf _ _ e); simpl; rewrite (iequrf _ _ e); apply refl_id.
   intros P HP; split; intros F v H;
    [rewrite (HP F _ H) | rewrite <- (feuplftidx F v) in H; rewrite (HP ☯☍(F) _ H)];
    apply refl_id.
   intros E HE; split; intros F v H; rewrite (HE F _ H); apply refl_id.
   intros P1 P2 H1 H2; split; intros F v H; rewrite (H1 F _ H); rewrite (H2 F _ H);
    apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F v H; rewrite (H1 F _ H); rewrite (H2 F _ H);
    apply refl_id.
   intros E P H1 H2 F v H; rewrite (H1 F _ H); rewrite <- (feuplftidx F v) in H;
    rewrite (H2 ☯☍(F) _ H); apply refl_id.
 Qed.

 Definition fpaffexpinff:=fst ftaffexpinff.
 Definition feaffexpinff:=snd ftaffexpinff.

 Theorem fpaffexpinf:forall (P:☊)(F:☌)(v:☈), v⍀☍F->v⍀☋ 〈v♇☍F@☋P〉.
 Proof.
  intros P F v H; apply (pinftp _ _ (fpaffexpinff P _ _ (einfpt _ _ H))).
 Qed.

 Theorem feaffexpinf:forall (E F:☌)(v:☈), v⍀☍F->v⍀☍ 〈v♇☍F@☍E〉.
 Proof.
  intros E F v H; apply (einftp _ _ (feaffexpinff E _ _ (einfpt _ _ H))).
 Qed.

 (* 〈♇☍@〉 does not introduce free variables, except those from F *)
 Theorem ftaffexpinff':(forall (P:☊)(F:☌)(f v:☈), f⍀☍☂F≡⊤->f⍀☋☂P≡⊤->f⍀☋☂〈v♇☍F@☋P〉≡⊤)*
                                          (forall (E F:☌)(f v:☈), f⍀☍☂F≡⊤->f⍀☍☂E≡⊤->f⍀☍☂〈v♇☍F@☍E〉≡⊤).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F:☌)(f v:☈), f⍀☍☂F≡⊤->f⍀☋☂P≡⊤->f⍀☋☂〈v♇☍F@☋P〉≡⊤)
                             (fun E:☌=>forall (F:☌)(f v:☈), f⍀☍☂F≡⊤->f⍀☍☂E≡⊤->f⍀☍☂〈v♇☍F@☍E〉≡⊤)); simpl.
   intros F f v H H0; apply refl_id.
   intros i; split; [split | idtac]; intros F f v H H0; try (apply refl_id);
    destruct (deciequ v i);
    [rewrite (iequpt _ _ i0); apply H | rewrite (iequrf _ _ e); apply H0].
   intros P HP; split; intros F f v H H0;
    [rewrite (HP _ _ v H H0) | rewrite <- (feuplftidx F f) in H; rewrite (HP _ _ ☠v H H0)];
    apply refl_id.
   intros E HE; split; intros F f v H H0; rewrite (HE _ _ v H H0); apply refl_id.
   intros P1 P2 H1 H2; split; intros F f v H H0; destruct (dedfbandt _ _ H0);
    rewrite (H1 _ _ v H i); rewrite (H2 _ _ v H i0); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F f v H H0; destruct (dedfbandt _ _ H0);
    rewrite (H1 _ _ v H i); rewrite (H2 _ _ v H i0); apply refl_id.
   intros E P H1 H2 F f v H H0; destruct (dedfbandt _ _ H0); rewrite (H1 _ _ v H i);
    rewrite <- (feuplftidx F f) in H; rewrite (H2 _ _ ☠v H i0); apply refl_id.
 Qed.

 Definition fpaffexpinff':=fst ftaffexpinff'.
 Definition feaffexpinff':=snd ftaffexpinff'.

 Theorem fpaffexpinf':forall (P:☊)(F:☌)(f v:☈), f⍀☍F->f⍀☋P->f⍀☋ 〈v♇☍F@☋P〉.
 Proof.
  intros P F f v H H0; apply (pinftp _ _ (fpaffexpinff' _ _ _ v (einfpt _ _ H) (pinfpt _ _ H0))).
 Qed.

 Theorem feaffexpinf':forall (E F:☌)(f v:☈), f⍀☍F->f⍀☍E->f⍀☍ 〈v♇☍F@☍E〉.
 Proof.
  intros E F f v H H0; apply (einftp _ _ (feaffexpinff' _ _ _ v (einfpt _ _ H) (einfpt _ _ H0))).
 Qed.

 (* 〈♇☍@〉 decomposition using an intermediary non-free variable *)
 Theorem ftaffexpintf:(forall (P:☊)(F:☌)(f v:☈), f⍀☋☂P≡⊤->〈f♇☍F@☋〈v♇☍(χ f)@☋P〉〉≡〈v♇☍F@☋P〉)*
                                         (forall (E F:☌)(f v:☈), f⍀☍☂E≡⊤->〈f♇☍F@☍〈v♇☍(χ f)@☍E〉〉≡〈v♇☍F@☍E〉).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F:☌)(f v:☈), f⍀☋☂P≡⊤->〈f♇☍F@☋〈v♇☍(χ f)@☋P〉〉≡〈v♇☍F@☋P〉)
                             (fun E:☌=>forall (F:☌)(f v:☈), f⍀☍☂E≡⊤->〈f♇☍F@☍〈v♇☍(χ f)@☍E〉〉≡〈v♇☍F@☍E〉));
   simpl.
   intros F f v H; apply refl_id.
   intros i; split; [split | idtac]; intros F f v H; try (apply refl_id);
    destruct (deciequ v i).
    rewrite (iequpt _ _ i0); simpl; rewrite (reflfiequ f); apply refl_id.
    rewrite (iequrf _ _ e); simpl; rewrite (dedfbnott _ H); apply refl_id.
   intros P HP; split; intros F f v H;
    [rewrite (HP F _ v H) | unfold feuplft at 2; simpl; rewrite (HP ☯☍(F) _ ☠v H)]; apply refl_id.
   intros E HE; split; intros F f v H; rewrite (HE F _ v H); apply refl_id.
   intros P1 P2 H1 H2; split; intros F f v H; destruct (dedfbandt _ _ H); rewrite (H1 F _ v i);
    rewrite (H2 F _ v i0); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F f v H; destruct (dedfbandt _ _ H);
    rewrite (H1 F _ v i); rewrite (H2 F _ v i0); apply refl_id.
   intros E P H1 H2 F f v H; destruct (dedfbandt _ _ H); rewrite (H1 F _ v i);
    unfold feuplft at 2; simpl; rewrite (H2 ☯☍(F) _ ☠v i0); apply refl_id.
 Qed.

 Definition fpaffexpintf:=fst ftaffexpintf.
 Definition feaffexpintf:=snd ftaffexpintf.

 Theorem fpaffexpint:forall (P:☊)(F:☌)(f v:☈), f⍀☋P->〈f♇☍F@☋〈v♇☍(χ f)@☋P〉〉≡〈v♇☍F@☋P〉.
 Proof.
  intros P F f v H; apply (fpaffexpintf _ F _ v (pinfpt _ _ H)).
 Qed.

 Theorem feaffexpint:forall (E F:☌)(f v:☈), f⍀☍E->〈f♇☍F@☍〈v♇☍(χ f)@☍E〉〉≡〈v♇☍F@☍E〉.
 Proof.
  intros E F f v H; apply (feaffexpintf _ F _ v (einfpt _ _ H)).
 Qed.

 (* 〈♇☍@〉 commutativity *)
 Theorem commftaffexp:(forall (P:☊)(F G:☌)(f g:☈),
                                          !(f≡g)->f⍀☍G->g⍀☍F->〈f♇☍F@☋〈g♇☍G@☋P〉〉≡〈g♇☍G@☋〈f♇☍F@☋P〉〉)*
                                         (forall (E F G:☌)(f g:☈),
                                          !(f≡g)->f⍀☍G->g⍀☍F->〈f♇☍F@☍〈g♇☍G@☍E〉〉≡〈g♇☍G@☍〈f♇☍F@☍E〉〉).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F G:☌)(f g:☈),
                                              !(f≡g)->f⍀☍G->g⍀☍F->〈f♇☍F@☋〈g♇☍G@☋P〉〉≡〈g♇☍G@☋〈f♇☍F@☋P〉〉)
                             (fun E:☌=>forall (F G:☌)(f g:☈),
                                              !(f≡g)->f⍀☍G->g⍀☍F->〈f♇☍F@☍〈g♇☍G@☍E〉〉≡〈g♇☍G@☍〈f♇☍F@☍E〉〉)); simpl.
   intros F G f g Hd HG HF; apply refl_id.
   intros i; split; [split | idtac]; intros F G f g Hd HG HF; try (apply refl_id);
    destruct (deciequ g i).
    rewrite (iequpt _ _ i0); rewrite i0; rewrite <- (sym_id i0) in Hd; rewrite (iequrf _ _ Hd);
     simpl; rewrite (reflfiequ i); apply (feaffexpnul _ F _ HG).
    rewrite (iequrf _ _ e); destruct (deciequ f i).
     rewrite (iequpt _ _ i0); simpl; rewrite (iequpt _ _ i0); rewrite (feaffexpnul _ G _ HF);
      apply refl_id.
     rewrite (iequrf _ _ e0); simpl;  rewrite (iequrf _ _ e0);  rewrite (iequrf _ _ e);
      apply refl_id.
   intros P HP; split; intros F G f g Hd HG HF.
    rewrite (HP _ _ _ _ Hd HG HF); apply refl_id.
    assert (Hd': !(☠f≡ ☠g)).
     intros H; invset H; apply Hd; rewrite H1; apply refl_id.
    rewrite (HP _ _ _ _ Hd' (feuplftinf G f HG) (feuplftinf F g HF)); apply refl_id.
   intros E HE; split; intros F G f g Hd HG HF; rewrite (HE _ _ _ _ Hd HG HF); apply refl_id.
   intros P1 P2 H1 H2; split; intros F G f g Hd HG HF; rewrite (H1 _ _ _ _ Hd HG HF);
    rewrite (H2 _ _ _ _ Hd HG HF); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F G f g Hd HG HF; rewrite (H1 _ _ _ _ Hd HG HF);
    rewrite (H2 _ _ _ _ Hd HG HF); apply refl_id.
   intros E P H1 H2 F G f g Hd HG HF; rewrite (H1 _ _ _ _ Hd HG HF); assert (Hd': !(☠f≡ ☠g)).
     intros H; invset H; apply Hd; rewrite H3; apply refl_id.
    rewrite (H2 _ _ _ _ Hd' (feuplftinf G f HG) (feuplftinf F g HF)); apply refl_id.
 Qed.

 Definition commfpaffexp:=fst commftaffexp.
 Definition commfeaffexp:=snd commftaffexp.

 Theorem ftaffexpmaj:(forall (P:☊)(F:☌)(m:☈), m>☍F->☠m>☋P->m>☋ 〈m♇☍F@☋P〉)*
                                       (forall (E F:☌)(m:☈), m>☍F->☠m>☍E->m>☍ 〈m♇☍F@☍E〉).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (F:☌)(m:☈), m>☍F->☠m>☋P->m>☋ 〈m♇☍F@☋P〉)
                             (fun (E:☌)=>forall (F:☌)(m:☈), m>☍F->☠m>☍E->m>☍ 〈m♇☍F@☍E〉)); simpl.
   intros F m HF HT; apply majbig.
   intros i; split; [split | idtac]; intros F m HF HT.
    apply majpvr.
    apply majbie.
    destruct (deciequ m i).
     rewrite (iequpt _ _ i0); apply HF.
     rewrite (iequrf _ _ e); apply majvar; invset_clear HT; invset_clear H;
      apply (igeneq _ _ (sym_nid e) H0).
   intros P HP; split; intros F m HF HT; invset HT.
    apply majnot; apply (HP _ _ HF H1).
    apply majfor; apply (HP _ _ (feuplftmaj _ _ HF) H1).
   intros E HE; split; intros F m HF HT; invset HT; [apply majchs | apply majpow];
    apply (HE _ _ HF H1).
   intros P1 P2 H1 H2; split; intros F m HF HT; invset HT.
    apply majand; [apply (H1 _ _ HF H4) | apply (H2 _ _ HF H5)].
    apply majimp; [apply (H1 _ _ HF H4) | apply (H2 _ _ HF H5)].
   intros E1 E2 H1 H2; split; split; intros F m HF HT; invset HT.
    apply majequ; [apply (H1 _ _ HF H4) | apply (H2 _ _ HF H5)].
    apply majins; [apply (H1 _ _ HF H4) | apply (H2 _ _ HF H5)].
    apply majcpl; [apply (H1 _ _ HF H4) | apply (H2 _ _ HF H5)].
    apply majpro; [apply (H1 _ _ HF H4) | apply (H2 _ _ HF H5)].
   intros E P H1 H2 F m HF HT; invset HT; apply majcmp;
    [apply (H1 _ _ HF H4) | apply (H2 _ _ (feuplftmaj _ _ HF) H5)].
 Qed.

 Definition fpaffexpmaj:=fst ftaffexpmaj.
 Definition feaffexpmaj:=snd ftaffexpmaj.

 Theorem ftaffexpmaj':(forall (P:☊)(F:☌)(m v:☈), m>☍F->m>☋P->m>☋ 〈v♇☍F@☋P〉)*
                                        (forall (E F:☌)(m v:☈), m>☍F->m>☍E->m>☍ 〈v♇☍F@☍E〉).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (F:☌)(m v:☈), m>☍F->m>☋P->m>☋ 〈v♇☍F@☋P〉)
                             (fun (E:☌)=>forall (F:☌)(m v:☈), m>☍F->m>☍E->m>☍ 〈v♇☍F@☍E〉)); simpl.
   intros F m v HF HT; apply majbig.
   intros i; split; [split | idtac]; intros F m v HF HT; invset HT.
    apply majpvr.
    apply majbie.
    destruct (deciequ v i);
     [rewrite (iequpt _ _ i1); apply HF | rewrite (iequrf _ _ e); apply majvar; apply H1].
   intros P HP; split; intros F m v HF HT; invset HT.
    apply majnot; apply (HP _ _ v HF H1).
    apply majfor; apply (HP _ _ ☠v (feuplftmaj _ _ HF) H1).
   intros E HE; split; intros F m v HF HT; invset HT; [apply majchs | apply majpow];
    apply (HE _ _ v HF H1).
   intros P1 P2 H1 H2; split; intros F m v HF HT; invset HT.
    apply majand; [apply (H1 _ _ v HF H4) | apply (H2 _ _ v HF H5)].
    apply majimp; [apply (H1 _ _ v HF H4) | apply (H2 _ _ v HF H5)].
   intros E1 E2 H1 H2; split; split; intros F m v HF HT; invset HT.
    apply majequ; [apply (H1 _ _ v HF H4) | apply (H2 _ _ v HF H5)].
    apply majins; [apply (H1 _ _ v HF H4) | apply (H2 _ _ v HF H5)].
    apply majcpl; [apply (H1 _ _ v HF H4) | apply (H2 _ _ v HF H5)].
    apply majpro; [apply (H1 _ _ v HF H4) | apply (H2 _ _ v HF H5)].
   intros E P H1 H2 F m v HF HT; invset HT; apply majcmp;
    [apply (H1 _ _ v HF H4) | apply (H2 _ _ ☠v (feuplftmaj _ _ HF) H5)].
 Qed.

 Definition fpaffexpmaj':=fst ftaffexpmaj'.
 Definition feaffexpmaj':=snd ftaffexpmaj'.

(* Binding, instantiation and affectation ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* In what follows, we show an interesting result, i.e. that affectation is not a new       *)
(* function but a composition of binding and instantiation. However, to simplify the    *)
(* related proofs, we keep the direct affectation definition.                                              *)

 Theorem ftafflocdec:(forall (P:☊)(F:☌)(v d:☈), d≤☉v->fpinloc d F (fpbdloc v d P)≡〈v♇☍F@☋P〉)*
                                       (forall (E F:☌)(v d:☈), d≤☉v->feinloc d F (febdloc v d E)≡〈v♇☍F@☍E〉).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F:☌)(v d:☈), d≤☉v->fpinloc d F (fpbdloc v d P)≡〈v♇☍F@☋P〉)
                             (fun E:☌=>forall (F:☌)(v d:☈), d≤☉v->feinloc d F (febdloc v d E)≡〈v♇☍F@☍E〉));
   simpl.
   intros F v d H; apply refl_id.
   intros i; split; [split | idtac]; intros F v d H; try (apply refl_id);
    destruct (decige d i).
    rewrite (igept _ _ i0); destruct (deciequ v i).
     rewrite (iequpt _ _ i1); simpl; rewrite (igept _ _ (reflige d)); rewrite (reflfiequ d);
      apply refl_id.
     rewrite (iequrf _ _ e); simpl; rewrite (igept _ _ (nxtrige _ _ i0));
      destruct (deciequ d ☠i).
      rewrite <- (sym_id i1) in i0; destruct (nreflnige _ i0).
      rewrite (iequrf _ _ e0); apply refl_id.
    rewrite (igerf _ _ e); destruct (deciequ v i).
     rewrite <- (sym_id i0) in H; destruct (nrefligt _ (transigeigt _ _ _ H (nigeigt _ _ e))).
     rewrite (iequrf _ _ e0); simpl; rewrite (igerf _ _ e); apply refl_id.
   intros P HP; split; intros F v d H;
    [rewrite (HP F _ _ H) | rewrite (HP ☯☍(F) _ _ (genxt _ _ H))]; apply refl_id.
   intros E HE; split; intros F v d H; rewrite (HE F _ _ H); apply refl_id.
   intros P1 P2 H1 H2; split; intros F v d H; rewrite (H1 F _ _ H); rewrite (H2 F _ _ H);
    apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F v d H;
    rewrite (H1 F _ _ H); rewrite (H2 F _ _ H); apply refl_id.
   intros E P H1 H2 F v d H; rewrite (H1 F _ _ H); rewrite (H2 ☯☍(F) _ _ (genxt _ _ H));
    apply refl_id.
 Qed.

 Definition fpafflocdec:=fst ftafflocdec.
 Definition feafflocdec:=snd ftafflocdec.

 Theorem fpafffordec:forall (P:☊)(F:☌)(v:☈), ☭☢(☬☢(v∙P)←F⊢fbf v P)≡〈v♇☍F@☋P〉.
 Proof.
  intros P F v; unfold fpinfor; unfold fpbdfor; simpl; apply fpafflocdec; apply gefst.
 Qed.

 Theorem fpaffexsdec:forall (P:☊)(F:☌)(v:☈), ☭☣(☬☣(v∙P)←F⊢fbe v P)≡〈v♇☍F@☋P〉.
 Proof.
  intros P F v; unfold fpinexs; unfold fpbdexs; unfold pexs; simpl; apply fpafflocdec;
   apply gefst.
 Qed.

 Theorem fpaffcmpdec:forall (P:☊)(E F:☌)(v:☈), ☭☤(☬☤(v∙E∙P)←F⊢fbc v E P)≡F∈E∧ 〈v♇☍F@☋P〉.
 Proof.
  intros P E F v; unfold feincmp; unfold fpbdcmp; simpl;
   rewrite (fpafflocdec P F _ _ (gefst v)); apply refl_id.
Qed.

(* Commutation Lemma ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem ftaffexpftuploc:(forall (P:☊)(F:☌)(d v:☈), d≤☉v->
                                                fpuploc d (〈v♇☍F@☋P〉)≡〈☠v♇☍feuploc d F@☋fpuploc d P〉)*
                                               (forall (E F:☌)(d v:☈), d≤☉v->
                                                feuploc d (〈v♇☍F@☍E〉)≡〈☠v♇☍feuploc d F@☍feuploc d E〉).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (F:☌)(d v:☈), d≤☉v->
                              fpuploc d (〈v♇☍F@☋P〉)≡〈☠v♇☍feuploc d F@☋fpuploc d P〉)
                             (fun (E:☌)=>forall (F:☌)(d v:☈), d≤☉v->
                              feuploc d (〈v♇☍F@☍E〉)≡〈☠v♇☍feuploc d F@☍feuploc d E〉)); simpl.
  intros F d v Hdv; apply refl_id.
  intros i; split; [split | idtac]; intros F d v Hdv; try (apply refl_id);
   destruct (decige d i).
   rewrite (igept _ _ i0); destruct (deciequ v i).
    rewrite (iequpt _ _ i1); simpl; apply refl_id.
    rewrite (iequrf _ _ e); simpl; rewrite (igept _ _ i0); apply refl_id.
   rewrite (igerf _ _ e); destruct (deciequ v i).
    rewrite <- (sym_id i0) in Hdv; destruct (e Hdv).
    rewrite (iequrf _ _ e0); simpl; rewrite (igerf _ _ e); destruct i.
     apply refl_id.
     destruct (deciequ v i);
      [rewrite <- (sym_id i0) in Hdv; destruct (e (nxtrige _ _ Hdv))
      | rewrite (iequrf _ _ e1); apply refl_id].
  intros P HP; split; intros F d v Hdv.
   rewrite (HP F _ _ Hdv); apply refl_id.
   rewrite (HP ☯☍(F) _ _ (genxt _ _ Hdv)); unfold feuplft; rewrite (feuplocdbl F _ _ (gefst d));
    apply refl_id.
  intros E HE; split; intros F d v Hdv; rewrite (HE F _ _ Hdv); apply refl_id.
  intros P1 P2 H1 H2; split; intros F d v Hdv; rewrite (H1 F _ _ Hdv); rewrite (H2 F _ _ Hdv);
   apply refl_id.
  intros E1 E2 H1 H2; split; split; intros F d v Hdv; rewrite (H1 F _ _ Hdv);
   rewrite (H2 F _ _ Hdv); apply refl_id.
  intros E P H1 H2 F d v Hdv; rewrite (H1 F _ _ Hdv); rewrite (H2 ☯☍(F) _ _ (genxt _ _ Hdv));
   unfold feuplft; rewrite (feuplocdbl F _ _ (gefst d)); apply refl_id.
 Qed.

 Definition fpaffexpfpuploc:=fst ftaffexpftuploc.
 Definition feaffexpfeuploc:=snd ftaffexpftuploc.

 Theorem commftaffbdloc:(forall (P:☊)(F:☌)(d x y:☈),
                                              d≤☉x->d≤☉y->!(x≡y)->y⍀☍F-> 
                                              〈☠x♇☍(feuploc d F)@☋fpbdloc y d P〉≡fpbdloc y d 〈x♇☍F@☋P〉)*
                                             (forall (E F:☌)(d x y:☈),
                                              d≤☉x->d≤☉y->!(x≡y)->y⍀☍F-> 
                                              〈☠x♇☍(feuploc d F)@☍febdloc y d E〉≡febdloc y d 〈x♇☍F@☍E〉).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F:☌)(d x y:☈),
                                              d≤☉x->d≤☉y->!(x≡y)->y⍀☍F->
                                              〈☠x♇☍(feuploc d F)@☋fpbdloc y d P〉≡fpbdloc y d 〈x♇☍F@☋P〉)
                             (fun E:☌=>forall (F:☌)(d x y:☈),
                                              d≤☉x->d≤☉y->!(x≡y)->y⍀☍F->
                                              〈☠x♇☍(feuploc d F)@☍febdloc y d E〉≡febdloc y d 〈x♇☍F@☍E〉));
   simpl.
   intros F d x y Hx Hy Hd HF; apply refl_id.
   intros i; split; [split | idtac]; intros F d x y Hx Hy Hd HF; try (apply refl_id);
    destruct (decige d i).
    rewrite (igept _ _ i0); destruct (deciequ y i).
     rewrite (iequpt _ _ i1); rewrite i1; rewrite <- (sym_id i1) in Hd;
      rewrite (iequrf _ _ Hd); simpl; rewrite (igept _ _ i0); rewrite (reflfiequ i);
      destruct d.
      apply refl_id.
      destruct (deciequ x d).
       rewrite <- (sym_id i2) in Hx; destruct (nreflnige _ Hx).
       rewrite (iequrf _ _ e); apply refl_id.
     rewrite (iequrf _ _ e); destruct (deciequ x i).
      rewrite (iequpt _ _ i1); simpl; rewrite (iequpt _ _ i1); rewrite (febdlocnul _ _ d HF);
       apply refl_id.
      rewrite (iequrf _ _ e0); simpl; rewrite (iequrf _ _ e0); rewrite (igept _ _ i0); rewrite (iequrf _ _ e);
       apply refl_id.
    rewrite (igerf _ _ e); assert (H:!(x≡i)*!(y≡i)).
     split;
      [destruct (deciequ x i); [rewrite <- (sym_id i0) in Hx; contradiction | apply e0]
      | destruct (deciequ y i); [rewrite <- (sym_id i0) in Hy; contradiction | apply e0]].
    destruct H; rewrite (iequrf _ _ e0); simpl; rewrite (iequrf _ _ e1); rewrite (igerf _ _ e);
     destruct i; simpl.
     apply refl_id.
     destruct (deciequ x i).
      rewrite <- (sym_id i0) in Hx; destruct e; apply nxtrige; apply Hx.
      rewrite (iequrf _ _ e2); apply refl_id.
   intros P HP; split; intros F d x y Hx Hy Hd HF.
    rewrite (HP _ _ _ _ Hx Hy Hd HF); apply refl_id.
    assert (Hd':!(☠x≡☠y)).
     intros H; invset H; apply Hd; rewrite H1; apply refl_id.
    unfold feuplft; rewrite (feuplocdbl F _ _ (gefst d));
     rewrite (HP (feuploc ☦ F) _ _ _ (genxt _ _ Hx) (genxt _ _ Hy) Hd'
                          (feuplocinf _ _ _ (gefst y) HF)); apply refl_id.
   intros E HE; split; intros F d x y Hx Hy Hd HF; rewrite (HE _ _ _ _ Hx Hy Hd HF);
    apply refl_id.
   intros P1 P2 H1 H2; split; intros F d x y Hx Hy Hd HF; rewrite (H1 _ _ _ _ Hx Hy Hd HF);
    rewrite (H2 _ _ _ _ Hx Hy Hd HF); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F d x y Hx Hy Hd HF;
    rewrite (H1 _ _ _ _ Hx Hy Hd HF); rewrite (H2 _ _ _ _ Hx Hy Hd HF); apply refl_id.
   intros E P H1 H2 F d x y Hx Hy Hd HF; rewrite (H1 _ _ _ _ Hx Hy Hd HF); assert (Hd':!(☠x≡☠y)).
    intros H; invset H; apply Hd; rewrite H3; apply refl_id.
   unfold feuplft; rewrite (feuplocdbl F _ _ (gefst d));
    rewrite (H2 (feuploc ☦ F) _ _ _ (genxt _ _ Hx) (genxt _ _ Hy) Hd'
                         (feuplocinf _ _ _ (gefst y) HF)); apply refl_id.
 Qed.

 Definition commfpaffbdloc:=fst commftaffbdloc.
 Definition commfeaffbdloc:=snd commftaffbdloc.

 Theorem commfpaffbdfor:forall (P:☊)(E:☌)(x y:☈), !(x≡y)->y⍀☍E->〈x♇☍E@☋☬☢(y∙P)〉≡☬☢(y∙〈x♇☍E@☋P〉).
 Proof.
  intros P E x y Hd Hf; unfold fpbdfor; simpl; unfold feuplft;
   rewrite (commfpaffbdloc P _ _ _ _ (gefst _) (gefst _) Hd Hf); apply refl_id.
 Qed.

 Theorem commfpaffbdexs:forall (P:☊)(E:☌)(x y:☈), !(x≡y)->y⍀☍E->〈x♇☍E@☋☬☣(y∙P)〉≡☬☣(y∙〈x♇☍E@☋P〉).
 Proof.
  intros P E x y Hd Hf; unfold fpbdexs; unfold pexs; simpl; unfold feuplft;
   rewrite (commfpaffbdloc P _ _ _ _ (gefst _) (gefst _) Hd Hf); apply refl_id.
 Qed.

 Theorem commfeaffbdcmp:forall (P:☊)(E F:☌)(x y:☈),
                                             !(x≡y)->y⍀☍F->〈x♇☍F@☍☬☤(y∙E∙P)〉≡☬☤(y∙〈x♇☍F@☍E〉∙〈x♇☍F@☋P〉).
 Proof.
  intros P E F x y Hd Hf; unfold fpbdcmp; simpl; unfold feuplft;
   rewrite (commfpaffbdloc P _ _ _ _ (gefst _) (gefst _) Hd Hf); apply refl_id.
 Qed.

(* Inversion of binding ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* These results allow, for a given ∀P, to find v and P' such that ∀P=☬☢(v∙P').                        *)

 Theorem ftbdlocinv:(forall (P:☊)(v d:☈),
                                      d≤☉v->☠v⍀☋P->fpbdloc v d (fpdnloc d 〈d♇☍χ(☠v)@☋P〉)≡P)*
                                     (forall (E:☌)(v d:☈),
                                      d≤☉v->☠v⍀☍E->febdloc v d (fednloc d 〈d♇☍χ(☠v)@☍E〉)≡E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (v d:☈),
                                              d≤☉v->☠v⍀☋P->fpbdloc v d (fpdnloc d 〈d♇☍χ(☠v)@☋P〉)≡P)
                             (fun E:☌=>forall (v d:☈),
                                              d≤☉v->☠v⍀☍E->febdloc v d (fednloc d 〈d♇☍χ(☠v)@☍E〉)≡E)); simpl.
   intros v d Hg Hf; apply refl_id.
   intros i; split; [split | idtac]; intros v d Hg Hf; try (apply refl_id);
    destruct (deciequ d i).
    rewrite (iequpt _ _ i0); rewrite i0; rewrite <- (sym_id i0) in Hg; simpl;
     rewrite (igept _ _ (nxtrige _ _ Hg)); simpl; rewrite (igept _ _ Hg); rewrite (reflfiequ v);
     apply refl_id.
    rewrite (iequrf _ _ e); simpl; destruct (decige d i).
     rewrite (igept _ _ i0); simpl; destruct i; simpl.
      destruct e; invset i0; apply refl_id.
      assert (Hf':!(v≡i)). intros H; invset Hf; apply H2; rewrite H; apply refl_id.
      destruct (deciequ d i).
       rewrite i1; rewrite (igept _ _ (reflige i)); rewrite (iequrf _ _ Hf'); apply refl_id.
       generalize (igeneq _ _ e i0); intros Hg'; invset Hg'; clear Hg'; rewrite (igept _ _ H1);
        rewrite (iequrf _ _ Hf'); apply refl_id.
     rewrite (igerf _ _ e0); simpl; rewrite (igerf _ _ e0); apply refl_id.
   intros P HP; split; intros v d Hg Hf; invset Hf;
   [rewrite (HP _ _ Hg H1) | unfold feuplft; simpl; rewrite (HP _ _ (genxt _ _ Hg) H1)];
   apply refl_id.
   intros E HE; split; intros v d Hg Hf; invset Hf; rewrite (HE _ _ Hg H1); apply refl_id.
   intros P1 P2 H1 H2; split; intros v d Hg Hf; invset Hf; rewrite (H1 _ _ Hg H4);
    rewrite (H2 _ _ Hg H5); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros v d Hg Hf; invset Hf; rewrite (H1 _ _ Hg H4);
    rewrite (H2 _ _ Hg H5); apply refl_id.
   intros E P H1 H2 v d Hg Hf; invset Hf; rewrite (H1 _ _ Hg H4); unfold feuplft; simpl; 
    rewrite (H2 _ _ (genxt _ _ Hg) H5); apply refl_id.
 Qed.

 Definition fpbdlocinv:=fst ftbdlocinv.
 Definition febdlocinv:=snd ftbdlocinv.

 Theorem fpbdforinv:forall (v:☈)(P:☊), ☠v⍀☋P->∀P≡☬☢(v∙☮☋(〈☦♇☍χ(☠v)@☋P〉)).
 Proof.
  intros v P H; unfold fpbdfor; unfold fpdnlft; rewrite (fpbdlocinv _ _ _ (gefst v) H);
   apply refl_id.
 Qed.

 Theorem fpbdexsinv:forall (v:☈)(P:☊), ☠v⍀☋P->∃P≡☬☣(v∙☮☋(〈☦♇☍χ(☠v)@☋P〉)).
 Proof.
  intros v P H; unfold fpbdexs; unfold fpdnlft; rewrite (fpbdlocinv _ _ _ (gefst v) H);
   apply refl_id.
 Qed.

 Theorem fpbdcmpinv:forall (v:☈)(E:☌)(P:☊), v⍀☍E->☠v⍀☋P->E♀P≡☬☤(v∙E∙☮☋(〈☦♇☍χ(☠v)@☋P〉)).
 Proof.
  intros v E P H H0; unfold fpbdcmp; unfold fpdnlft; rewrite (fpbdlocinv _ _ _ (gefst v) H0);
   apply refl_id.
 Qed.

(* Alpha-renaming ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Alpha-renaming states that the name of bound variables is meaningless; in this         *)
(* module we are in fact able to prove Coq equality, i.e. terms identity.                             *)

 Theorem fpbdforalpha:forall (P:☊)(v w:☈), w⍀☋P->☬☢(v∙P)≡☬☢(w∙〈v♇☍χ(w)@☋P〉).
 Proof.
  intros P v w H; rewrite (sym_id (fpafffordec P χ(w) v)); unfold fpbdfor at 3; unfold fbf;
   rewrite (fpinforbd (fpbdloc v ☦ P) w (fpbdforinf _ _ v H)); apply refl_id.
 Qed.

 Theorem fpbdexsalpha:forall (P:☊)(v w:☈), w⍀☋P->☬☣(v∙P)≡☬☣(w∙〈v♇☍χ(w)@☋P〉).
 Proof.
  intros P v w H; rewrite (sym_id (fpaffexsdec P χ(w) v)); unfold fpbdexs at 3; unfold fbe;
   rewrite (fpinexsbd (fpbdloc v ☦ P) w (fpbdexsinf _ _ v H)); apply refl_id.
 Qed.

 Theorem fpbdcmpalpha:forall (E:☌)(P:☊)(v w:☈), w⍀☋P->☬☤(v∙E∙P)≡☬☤(w∙E∙〈v♇☍χ(w)@☋P〉).
 Proof.
  intros E P v w H; unfold fpbdcmp; generalize (fpbdforalpha P v w H); intros H0;
   invset H0; apply refl_id.
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)