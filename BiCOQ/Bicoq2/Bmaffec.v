(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Bmaffec, multiple parallel affectations
   The fpaffexp and feaffexp provided in the module Baffec are extended to deal with
   multiple parallel affectations (e.g. [x,y:=y,x](x☵y)=y☵x). To do so, a new type ☛ is
   defined as the list of pairs (v,E), each one representing a single affectation [v:=E].
   Interesting results about pseudo-ground sequents are also provided...
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - August 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Load Mlist.
 Require Export Paffprd.

(* Single affectation ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Inductive Bsaff:Set:=sab:☈->☌->Bsaff.

 Definition fsaequ(s1 s2:Bsaff):☆:=
  match s1 with sab i1 E1 => match s2 with sab i2 E2 => (i1=☉☂i2) ∧☁ (E1=☍☂E2) end end.

 Theorem impsaequ:(identity (A:=Bsaff)☵☨☂fsaequ).
 Proof.
  unfold Implem2; intros s t; split; induction s; induction t; simpl; intros H.
   destruct (dedfbandt _ _ H); rewrite (iequtp _ _ i); rewrite (eequtp _ _ i0); apply refl_id.
   intros H0; invset H0; rewrite <- H2 in H; rewrite <- H3 in H;
    rewrite <- (sym_id (iequpt _ _ (refl_id b))) in H;
    rewrite <- (sym_id (eequpt _ _ (refl_id b0))) in H; invset_clear H.
 Qed.

 Definition decsaequ:=decimp2 impsaequ.
 Definition saequpt:=imp2pt impsaequ.
 Definition saequrf:=imp2rf impsaequ.
 Definition saequtp:=imp2tp impsaequ.
 Definition saequfr:=imp2fr impsaequ.

(* List of single affectations ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Type *)
 Definition Bmaff:=Blst Bsaff. Notation "☛":=(Blst Bsaff).
 Definition memp:=lemp Bsaff. Notation "∅☜":=(lemp Bsaff).
 Definition mins:=lins Bsaff. Notation "M '☜★' s":=(lins Bsaff M s) (at level 50).

 (* Equality *)
 Definition fmequ:=flequ Bsaff fsaequ.
 Definition impmequ:=implequ Bsaff fsaequ impsaequ.

 Definition decmequ:=decimp2 impmequ.
 Definition mequtp:=imp2tp impmequ.
 Definition mequfr:=imp2fr impmequ.
 Definition mequpt:=imp2pt impmequ.
 Definition mequrf:=imp2rf impmequ.

 (* Membership *)
 Definition miyes:=liyes Bsaff.
 Definition minxt:=linxt Bsaff.
 Definition min:=lin Bsaff. Notation "sa '∈☜' M":=(lin Bsaff sa M) (at level 50).

 Definition minemp:=linemp Bsaff.
 Definition casemin:=caselin Bsaff.

 (* Membership function *)
 Definition fmin:=flin Bsaff fsaequ.
 Notation "sa '∈☜☂' M":=(flin Bsaff fsaequ sa M) (at level 50).

 Definition impmin:=implin Bsaff fsaequ impsaequ.
 Definition decmin:=declin Bsaff fsaequ impsaequ.
 Definition minpt:=linpt Bsaff fsaequ impsaequ.
 Definition minrf:=linrf Bsaff fsaequ impsaequ.
 Definition mintp:=lintp Bsaff fsaequ impsaequ.
 Definition minfr:=linfr Bsaff fsaequ impsaequ.

 (* Inclusion *)
 Definition minc:=linc Bsaff. Notation "M '⊆☜' N":=(linc Bsaff M N) (at level 50).

 Definition reflminc:=refllinc Bsaff.
 Definition transminc:=translinc Bsaff.
 Definition empminc:=emplinc Bsaff.
 Definition mincover:=lincover Bsaff.
 Definition mincemp:=lincemp Bsaff.
 Definition mincsub:=lincsub Bsaff.
 Definition mincdbl:=lincdbl Bsaff fsaequ impsaequ.
 Definition mincswp:=lincswp Bsaff.
 Definition mincnot:=lincnot Bsaff fsaequ impsaequ.

 (* Inclusion function *)
 Definition fminc:=flinc Bsaff fsaequ.
 Notation "M '⊆☜☂' N":=(flinc Bsaff fsaequ M N) (at level 50).

 Definition impminc:=implinc Bsaff fsaequ impsaequ.
 Definition decminc:=declinc Bsaff fsaequ impsaequ.
 Definition mincpt:=lincpt Bsaff fsaequ impsaequ.
 Definition mincrf:=lincrf Bsaff fsaequ impsaequ.
 Definition minctp:=linctp Bsaff fsaequ impsaequ.
 Definition mincfr:=lincfr Bsaff fsaequ impsaequ.

(* Check if a key appears in a list ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fmhaskey(M:☛)(i:☈){struct M}:☆:=
  match M with
  | ∅☜ => ⊥
  | M' ☜★ (sab j E) => match (i=☉☂j) with ⊤ => ⊤ | ⊥ => fmhaskey M' i end
  end.

 Theorem fmhaskeymin:forall (M:☛)(i:☈)(E:☌),  (sab i E)∈☜M->fmhaskey M i≡⊤.
 Proof.
  induction M; simpl; intros i E Hin.
   invset Hin.
   induction d; destruct (casemin _ _ _ Hin).
    invset i0; rewrite reflfiequ; apply refl_id.
    destruct (deciequ i b).
     rewrite (iequpt _ _ i0); apply refl_id.
     rewrite (iequrf _ _ e); apply (IHM i E l).
 Qed.

 Theorem fmhaskeyget:forall (M:☛)(i:☈), fmhaskey M i≡⊤->{E:☌ & (sab i E)∈☜M}.
 Proof.
  induction M; simpl; intros i Hkey.
   invset Hkey.
   induction d; destruct (deciequ i b).
    exists b0; rewrite i0; apply miyes.
    rewrite <- (sym_id (iequrf _ _ e)) in Hkey; destruct (IHM i Hkey); exists x; apply minxt;
     apply l.
 Qed.

(* Majoration of the keys of a list ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* An index is said to major a list iff it majors any key or expression of the list.         *)

 Fixpoint fmmaj(i:☈)(M:☛){struct M}:☆:=
  match M with
  | ∅☜ => ⊤
  | M' ☜★ (sab j E) => match j<☉☂i with
                              | ⊤ => match i>☍☂E with ⊤ => fmmaj i M' | ⊥ => ⊥ end
                              | ⊥ => ⊥
                              end
  end.
 Infix ">☜☂":=fmmaj (at level 70, right associativity).

 Theorem fmmajdec:forall (M:☛)(i:☈), (i>☜☂M)≡⊤->forall (k:☈)(E:☌), (sab k E)∈☜M->(i>☍E)*(k<☉i).
 Proof.
  induction M; simpl; intros i Hmaj k E Hin.
   invset Hin.
   induction d; destruct (decigt b i).
    rewrite <- (sym_id (igtpt _ _ i0)) in Hmaj; destruct (decemaj i b0).
     rewrite <- (sym_id (emajpt _ _ e)) in Hmaj; destruct (casemin _ _ _ Hin).
      invset i1; split; [apply e | apply i0].
      apply (IHM _ Hmaj _ _ l).
     rewrite <- (sym_id (emajrf _ _ e)) in Hmaj; invset Hmaj.
    rewrite <- (sym_id (igtrf _ _ e)) in Hmaj; invset Hmaj.
 Qed.

 Theorem fmmajige:forall (M:☛)(i j:☈), (i>☜☂M)≡⊤->i≤☉j->(j>☜☂M)≡⊤.
 Proof.
  induction M; simpl; intros i j Hmaj Hij.
   apply refl_id.
   induction d; destruct (decigt b i).
    rewrite <- (sym_id (igtpt _ _ i0)) in Hmaj; rewrite (igtpt _ _ (transigtige _ _ _ i0 Hij));
     destruct (decemaj i b0).
     rewrite <- (sym_id (emajpt _ _ e)) in Hmaj; rewrite (emajpt _ _ (emajige _ _ _ Hij e));
      apply (IHM _ _ Hmaj Hij).
     rewrite <- (sym_id (emajrf _ _ e)) in Hmaj; invset Hmaj.
    rewrite <- (sym_id (igtrf _ _ e)) in Hmaj; invset Hmaj.
 Qed.

 Theorem fmmajfmhaskey:forall (M:☛)(i:☈), (i>☜☂M)≡⊤->fmhaskey M i≡⊥.
 Proof.
  induction M; simpl; intros i Hmaj.
   apply refl_id.
   induction d; destruct (decigt b i).
    rewrite <- (sym_id (igtpt _ _ i0)) in Hmaj; destruct (decemaj i b0).
     rewrite <- (sym_id (emajpt _ _ e)) in Hmaj; destruct (deciequ i b).
      rewrite <- (sym_id i1) in i0; destruct (nrefligt _ i0).
      rewrite (iequrf _ _ e0); apply (IHM _ Hmaj).
     rewrite <- (sym_id (emajrf _ _ e)) in Hmaj; invset Hmaj.
    rewrite <- (sym_id (igtrf _ _ e)) in Hmaj; invset Hmaj.
 Qed.

 Theorem fmmajminc:forall (N M:☛)(i:☈), (i>☜☂M)≡⊤->N⊆☜M->(i>☜☂N)≡⊤.
 Proof.
  induction N; simpl; intros M i Hmaj Hinc.
   apply refl_id.
   induction d; assert (Hin:sab b b0 ∈☜ M). apply Hinc; apply miyes.
   assert (Hkey:fmhaskey M b≡⊤). apply fmhaskeymin with (E:=b0); apply Hin.
   rewrite (igtpt _ _ (snd (fmmajdec _ _ Hmaj _ _ Hin))).
   rewrite (emajpt _ _ (fst (fmmajdec _ _ Hmaj _ _ Hin))).
   apply (IHN _ _ Hmaj); intros x Hx; apply Hinc; apply minxt; apply Hx.
 Qed.

(* Get a fresh key for a list ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* fmfresh returns an index fresh for both keys and expressions of the list.                    *)

 Fixpoint fmfresh(M:☛){struct M}:☈:=
  match M with
  | ∅☜ => ☦
  | M' ☜★ (sab j E) => (fmfresh M') ☰☉ (☠j ☰☉ ☰☍(E))
  end.
 Notation "'☰☜(' M ')'":=(fmfresh M).

 Theorem fmfreshfmmaj:forall (M:☛), (☰☜(M)>☜☂M)≡⊤.
 Proof.
  induction M; simpl.
   apply refl_id.
   induction d; rewrite (commfimax ☰☜(M) (☠b ☰☉ ☰☍(b0))).
   assert (H:b<☉(☠b ☰☉ ☰☍(b0))). unfold igt; apply fimaxgel.
   assert (H0:(☠b ☰☉ ☰☍(b0)) ≤☉ ((☠b ☰☉ ☰☍(b0)) ☰☉ ☰☜(M))). apply fimaxgel.
   rewrite (igtpt _ _ (transigtige _ _ _ H H0)); clear H H0.
   assert (H:(☠b ☰☉ ☰☍(b0))>☍b0).
    rewrite (commfimax ☠b ☰☍(b0)); apply (emajige b0 _ _ (fimaxgel ☰☍(b0) ☠b)); apply fenewfmaj.
   rewrite (emajpt _ _ (emajige b0 _ _ (fimaxgel _ ☰☜(M)) H)).
   apply fmmajige with (i:=fmfresh M);
    [apply IHM | rewrite (commfimax (☠b ☰☉ ☰☍(b0)) ☰☜(M)); apply fimaxgel].
 Qed.

 Theorem exfreshkey:forall (M:☛), {i:☈ & forall (k:☈)(E:☌), (sab k E)∈☜M->(i>☍E)*(k<☉i)}.
 Proof.
  intros M; exists ☰☜(M); apply fmmajdec; apply fmfreshfmmaj.
 Qed.

(* Removal of (all occurences of) a key from a list ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fmremkey(M:☛)(i:☈){struct M}:☛:=
  match M with
  | ∅☜ => ∅☜
  | M' ☜★ (sab j E) => match (i=☉☂j) with ⊤ => fmremkey M' i | ⊥ => (fmremkey M' i) ☜★ (sab j E) end
  end.

 Theorem fmremkeyminc:forall (M:☛)(i:☈),  fmremkey M i⊆☜M.
 Proof.
  induction M; intros i; simpl.
   apply reflminc.
   induction d; destruct (deciequ i b);
    [rewrite (iequpt _ _ i0); apply mincover
    | rewrite (iequrf _ _ e); apply mincdbl]; apply IHM.
 Qed.

 Theorem fmremkeyrem:forall (M:☛)(i:☈)(E:☌),  !((sab i E)∈☜(fmremkey M i)).
 Proof.
  induction M; intros i E Hin.
   invset Hin.
   induction d; simpl in Hin; destruct (deciequ i b).
    rewrite <- (sym_id (iequpt _ _ i0)) in Hin; apply (IHM i E Hin).
    rewrite <- (sym_id (iequrf _ _ e)) in Hin; destruct (casemin _ _ _ Hin);
     [invset i0; rewrite <- H0 in e; apply e; apply refl_id | apply (IHM i E l)].
 Qed.

 Theorem fmremkeyneq:forall (M:☛)(i j:☈)(E:☌),  (sab j E)∈☜M->!(i≡j)->(sab j E)∈☜fmremkey M i.
 Proof.
  induction M; intros i j E Hin Hij.
   invset Hin.
   induction d; destruct (casemin _ _ _ Hin).
    rewrite (sym_id i0); simpl; rewrite (iequrf _ _ Hij); apply miyes.
    simpl; destruct (deciequ i b);
     [rewrite (iequpt _ _ i0) | rewrite (iequrf _ _ e); apply minxt]; apply (IHM i j E l Hij).
 Qed.

 Theorem fmremkeyhaskey:forall (M:☛)(i:☈), fmhaskey (fmremkey M i) i≡⊥.
 Proof.
  intros M i; destruct ((ElimB2 fmhaskey) (fmremkey M i) i);
   [destruct (fmhaskeyget _ _ i0); destruct (fmremkeyrem _ _ _ l) | apply i0].
 Qed.

(* Get the expression associated to (the first occurrence of) a key ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fmget(M:☛)(i:☈){struct M}:☌:=
  match M with
  | ∅☜ => χ(i)
  | M' ☜★ (sab j E) => match (i=☉☂j) with ⊤ => E | ⊥ => fmget M' i end
  end.

 Theorem fmgetmin:forall (M:☛)(i:☈)(E:☌), E≡fmget M i->((sab i E)∈☜M)+(E≡χ(i)).
 Proof.
  induction M; simpl; intros i E Hget.
   right; apply Hget.
   induction d; destruct (deciequ i b).
    rewrite <- (sym_id (iequpt _ _ i0)) in Hget; left; rewrite i0; rewrite Hget; apply miyes.
    rewrite <- (sym_id (iequrf _ _ e)) in Hget; destruct (IHM i E Hget);
     [left; apply minxt; apply l | right; apply i0].
 Qed.

 Theorem fmgetfmremkey:forall (M:☛)(i:☈), fmget (fmremkey M i) i≡χ(i).
 Proof.
  induction M; simpl; intros i.
   apply refl_id.
   induction d; destruct (deciequ i b);
    [rewrite (iequpt _ _ i0) | rewrite (iequrf _ _ e); simpl; rewrite (iequrf _ _ e)];
    apply IHM.
 Qed.

 Theorem fmgetfmremneq:forall (M:☛)(i j:☈), !(i≡j)->fmget (fmremkey M i) j≡fmget M j.
 Proof.
  induction M; simpl; intros i j Hij.
   apply refl_id.
   induction d; destruct (deciequ i b).
    rewrite (iequpt _ _ i0); destruct (deciequ j b).
     rewrite <- i1 in i0; destruct (Hij i0).
     rewrite (iequrf _ _ e); apply (IHM _ _ Hij).
    rewrite (iequrf _ _ e); destruct (deciequ j b).
     rewrite (iequpt _ _ i0); simpl; rewrite (iequpt _ _ i0); apply refl_id.
     rewrite (iequrf _ _ e0); simpl; rewrite (iequrf _ _ e0); apply (IHM _ _ Hij).
 Qed.

 Theorem fmfreshmaj:forall (M:☛)(i:☈), fmhaskey M i≡ ⊤->☰☜(M)>☍(fmget M i).
 Proof.
  induction M; simpl; intros i Hkey.
   invset Hkey; invset H.
   induction d; destruct (deciequ i b).
    rewrite (iequpt _ _ i0); rewrite (commfimax ☠b ☰☍(b0)); rewrite (commfimax ☰☜(M) (☰☍(b0) ☰☉ ☠b)).
    assert (H:☰☍(b0)≤☉((☰☍(b0) ☰☉ ☠b) ☰☉ ☰☜(M))).
     apply (transige _ _ ((☰☍(b0) ☰☉ ☠b) ☰☉ ☰☜(M)) (fimaxgel ☰☍(b0) ☠b)); apply fimaxgel.
    apply emajige with (i:=☰☍(b0)); [apply H | apply fenewfmaj].
    rewrite (iequrf _ _ e); rewrite <- (sym_id (iequrf _ _ e)) in Hkey;
     apply emajige with (i:=☰☜(M)); [apply fimaxgel | apply (IHM _ Hkey)].
 Qed.

 Theorem fmgetidx:forall (M:☛)(i:☈), fmhaskey M i≡⊥->fmget M i≡χ(i).
 Proof.
  induction M; simpl; intros i Hkey.
   apply refl_id.
   induction d; destruct (deciequ i b).
    rewrite <- (sym_id (iequpt _ _ i0)) in Hkey; invset Hkey; invset H.
    rewrite <- (sym_id (iequrf _ _ e)) in Hkey; rewrite (iequrf _ _ e); apply (IHM _ Hkey).
 Qed.

 Theorem fmgetmaj:forall (M:☛)(f:☈), (f>☜☂M)≡⊤->forall (k:☈), fmhaskey M k≡⊤->f>☍fmget M k.
 Proof.
  induction M; simpl; intros f Hmaj k Hkey.
   invset Hkey; invset H.
   induction d; destruct (decigt b f).
    rewrite <- (sym_id (igtpt _ _ i)) in Hmaj; destruct (decemaj f b0).
     rewrite <- (sym_id (emajpt _ _ e)) in Hmaj; destruct (deciequ k b).
      rewrite (iequpt _ _ i0); apply e.
      rewrite (iequrf _ _ e0); rewrite <- (sym_id (iequrf _ _ e0)) in Hkey;
       apply (IHM _ Hmaj _ Hkey).
     rewrite <- (sym_id (emajrf _ _ e)) in Hmaj; invset Hmaj.
    rewrite <- (sym_id (igtrf _ _ e)) in Hmaj; invset Hmaj.
 Qed.

(* Normalisation a list ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fmnorm(M:☛){struct M}:☛:=
  match M with
  | ∅☜ => ∅☜
  | M' ☜★ (sab i E) => match (E=☍☂χ(i)) with
                              | ⊤ => fmremkey (fmnorm M') i
                              | ⊥ => fmremkey (fmnorm M') i ☜★ (sab i E)
                              end
  end.

 Theorem fmnorminc:forall (M:☛), fmnorm M⊆☜M.
 Proof.
  induction M; intros i; simpl.
   apply reflminc.
   induction d; destruct (deceequ b0 χ(b)).
    rewrite (eequpt _ _ i0); intros H; apply minxt; apply IHM;
     apply (fmremkeyminc (fmnorm M) b); apply H.
    rewrite (eequrf _ _ e); intros H; destruct (casemin _ _ _ H).
     rewrite i0; apply miyes.
     apply minxt; apply IHM; apply (fmremkeyminc (fmnorm M) b); apply l.
 Qed.

 Theorem fmnormget:forall (M:☛)(i:☈), fmget (fmnorm M) i≡fmget M i.
 Proof.
  induction M; intros i; simpl.
   apply refl_id.
   induction d; destruct (deceequ b0 χ(b)).
    rewrite (eequpt _ _ i0); rewrite i0; destruct (deciequ i b).
     rewrite (iequpt _ _ i1); rewrite i1; apply fmgetfmremkey.
     rewrite (iequrf _ _ e); rewrite (fmgetfmremneq (fmnorm M) b i (sym_nid e)); apply IHM.
    rewrite (eequrf _ _ e); destruct (deciequ i b).
     rewrite (iequpt _ _ i0); simpl; rewrite (iequpt _ _ i0); apply refl_id.
     rewrite (iequrf _ _ e0); simpl; rewrite (iequrf _ _ e0);
      rewrite (fmgetfmremneq (fmnorm M) b i (sym_nid e0)); apply IHM.
 Qed.

(* Uplift of a list ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fmuplft(M:☛){struct M}:☛:=
  match M with
  | ∅☜ => ∅☜
  | M' ☜★ (sab j E) => (fmuplft M') ☜★ (sab ☠j ☯☍(E))
  end.
 Notation "'☯☜(' M ')'":=(fmuplft M).

 Theorem fmuplftmaj:forall (M:☛)(f:☈), (f>☜☂M)≡⊤->(☠f>☜☂ ☯☜(M))≡⊤.
 Proof.
  induction M; simpl; intros f Hmaj.
   apply refl_id.
   induction d; destruct (decigt b f).
    rewrite <- (sym_id (igtpt _ _ i)) in Hmaj; destruct (decemaj f b0).
     rewrite <- (sym_id (emajpt _ _ e)) in Hmaj; simpl; rewrite (igtpt _ _  (gtnxt _ _ i));
      rewrite (emajpt _ _ (feuplftmaj _ _ e)); apply (IHM _ Hmaj).
     rewrite <- (sym_id (emajrf _ _ e)) in Hmaj; invset Hmaj.
    rewrite <- (sym_id (igtrf _ _ e)) in Hmaj; invset Hmaj.
 Qed.

 Theorem fmuplftmhaskey:forall (M:☛)(f:☈), fmhaskey M f≡fmhaskey ☯☜(M) ☠f.
 Proof.
  induction M; simpl; intros f.
   apply refl_id.
   induction d; destruct (deciequ f b).
    rewrite (iequpt _ _ i); simpl; rewrite (iequpt _ _ i); apply refl_id.
    rewrite (iequrf _ _ e); simpl; rewrite (iequrf _ _ e); apply IHM.
 Qed.

(* Multiple affectations function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* The functions fpmulmaff and femulaff implements parallel multiple affectations,    *)
(* i.e. that there is no interference between different affectations.                                *)

 Fixpoint fpmaffexp(M:☛)(P:☊){struct P}:☊:=
  match P with
  | ¬P' => ¬(fpmaffexp M P')
  | ∀P' => ∀(fpmaffexp ☯☜(M) P')
  | P1∧P2 => (fpmaffexp M P1)∧(fpmaffexp M P2)
  | P1☲P2 => (fpmaffexp M P1)☲(fpmaffexp M P2)
  | π j => π j
  | E1♃E2 => (femaffexp M E1)♃(femaffexp M E2)
  | E1∈E2 => (femaffexp M E1)∈(femaffexp M E2)
  end
 with femaffexp(M:☛)(E:☌){struct E}:☌:=
  match E with
  | ♄ => ♄
  | ☭E' => ☭(femaffexp M E')
  | ☬E' => ☬(femaffexp M E')
  | (E1☵E2) => (femaffexp M E1☵femaffexp M E2)
  | (E1♂E2) => (femaffexp M E1♂femaffexp M E2)
  | ω j => ω j
  | χ j => fmget M j
  | E'♀P' => (femaffexp M E')♀(fpmaffexp ☯☜(M) P')
  end.
 Notation "'〈〈' M '@☋' P '〉〉'":=(fpmaffexp M P).
 Notation "'〈〈' M '@☍' E '〉〉'":=(femaffexp M E).

 Theorem fmaffexpidx:forall (M:☛)(v:☈), fmhaskey M v≡⊥->〈〈M@☍χ(v)〉〉≡χ(v).
 Proof.
  induction M; simpl; intros v Hkey.
   apply refl_id.
   induction d; destruct (deciequ v b).
    rewrite <- (sym_id (iequpt _ _ i)) in Hkey; invset Hkey.
    rewrite <- (sym_id (iequrf _ _ e)) in Hkey; rewrite (iequrf _ _ e); apply (IHM _ Hkey).
 Qed.

 Theorem fmaffexpmmaj:forall (M:☛), 〈〈M@☍χ(☰☜(M))〉〉≡χ(☰☜(M)).
 Proof.
  intros M; apply fmaffexpidx; apply fmmajfmhaskey; apply fmfreshfmmaj.
 Qed.

 Theorem ftaffexpmemp:(forall (P:☊), 〈〈∅☜@☋P〉〉≡P)*(forall (E:☌), 〈〈∅☜@☍E〉〉≡E).
 Proof.
  apply (Bst_ind (fun (P:☊)=>〈〈∅☜@☋P〉〉≡P) (fun (E:☌)=>〈〈∅☜@☍E〉〉≡E)).
   apply refl_id.
   intros i; split; [split | idtac]; apply refl_id.
   intros P HP; split; simpl; rewrite HP; apply refl_id.
   intros E HE; split; simpl; rewrite HE; apply refl_id.
   intros P1 P2 H1 H2; split; simpl; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; simpl; rewrite H1; rewrite H2; apply refl_id.
   intros E P HE HP; simpl; rewrite HE; rewrite HP; apply refl_id.
 Qed.

 Definition fpaffexpmemp:=fst ftaffexpmemp.
 Definition feaffexpmemp:=snd ftaffexpmemp.

 Theorem ftaffexpmsin:(forall (P:☊)(F:☌)(k:☈), 〈〈∅☜ ☜★ sab k F@☋P〉〉≡〈k♇☍F@☋P〉)*
                                         (forall (E F:☌)(k:☈), 〈〈∅☜ ☜★ sab k F@☍E〉〉≡〈k♇☍F@☍E〉).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (F:☌)(k:☈), 〈〈∅☜ ☜★ sab k F@☋P〉〉≡〈k♇☍F@☋P〉)
                             (fun (E:☌)=>forall (F:☌)(k:☈), 〈〈∅☜ ☜★ sab k F@☍E〉〉≡〈k♇☍F@☍E〉)).
   intros F k; apply refl_id.
   intros i; split; [split | idtac]; intros F k; simpl; try (apply refl_id);
    destruct (deciequ i k).
    rewrite i0; rewrite (iequpt _ _ (refl_id k)); apply refl_id.
    rewrite (iequrf _ _ e); rewrite (iequrf _ _ (sym_nid e)); apply refl_id.
   intros P; split; intros F k; simpl; rewrite H; apply refl_id.
   intros E HE; split; intros F k; simpl; rewrite HE; apply refl_id.
   intros P1 P2 H1 H2; split; intros F k; simpl; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F k; simpl; rewrite H1; rewrite H2;
    apply refl_id.
   intros E P HE HP F k; simpl; rewrite HE; rewrite HP; apply refl_id.
 Qed.

 Definition fpaffexpmsin:=fst ftaffexpmsin.
 Definition feaffexpmsin:=snd ftaffexpmsin.

 Theorem ftaffexpdec:(forall (P:☊)(F:☌)(k f:☈)(M:☛), (f>☜☂M)≡⊤->f>☋P->
                                        〈〈M ☜★ sab k F@☋P〉〉≡〈f♇☍F@☋ 〈〈M@☋ 〈k♇☍χ(f)@☋P〉 〉〉 〉)*
                                       (forall (E F:☌)(k f:☈)(M:☛), (f>☜☂M)≡⊤->f>☍E->
                                        〈〈M ☜★ sab k F@☍E〉〉≡〈f♇☍F@☍ 〈〈M@☍ 〈k♇☍χ(f)@☍E〉 〉〉 〉).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (F:☌)(k f:☈)(M:☛), (f>☜☂M)≡⊤->f>☋P->
                              〈〈M ☜★ sab k F@☋P〉〉≡〈f♇☍F@☋ 〈〈M@☋ 〈k♇☍χ(f)@☋P〉 〉〉 〉)
                             (fun (E:☌)=>forall (F:☌)(k f:☈)(M:☛), (f>☜☂M)≡⊤->f>☍E->
                              〈〈M ☜★ sab k F@☍E〉〉≡〈f♇☍F@☍ 〈〈M@☍ 〈k♇☍χ(f)@☍E〉 〉〉 〉)).
   intros F k f M HfM HfT; apply refl_id.
   intros i; split; [split | idtac]; intros F k f M HfM HfT; simpl; try (apply refl_id);
    destruct (deciequ i k).
    rewrite i0; rewrite (iequpt _ _ (refl_id k));
     rewrite (fmaffexpidx M f (fmmajfmhaskey _ _ HfM)).
    simpl; rewrite (iequpt _ _ (refl_id f)); apply refl_id.
    rewrite (iequrf _ _ e); rewrite (iequrf _ _ (sym_nid e)); simpl;
     destruct ((ElimB2 fmhaskey) M i).
     rewrite (feaffexpnul (fmget M i) F f (emajinf (fmget M i) f (fmgetmaj _ _ HfM _ i0)));
      apply refl_id.
    rewrite (fmgetidx _ _ i0); simpl; invset HfT; destruct (deciequ f i).
     rewrite <- (sym_id i2) in H1; destruct (nrefligt _ H1).
     rewrite (iequrf _ _ e0); apply refl_id.
   intros P HP; split; intros F k f M HfM HfT; invset_clear HfT; simpl.
    rewrite (HP F k _ _ HfM H); apply refl_id.
    rewrite (HP ☯☍(F) ☠k _ _ (fmuplftmaj _ _ HfM) H); apply refl_id.
   intros E HE; split; intros F k f M HfM HfT; invset_clear HfT; simpl;
    rewrite (HE F k _ _ HfM H); apply refl_id.
   intros P1 P2 H1 H2; split; intros F k f M HfM HfT; invset_clear HfT; simpl;
    rewrite (H1 F k _ _ HfM H); rewrite (H2 F k _ _ HfM H0); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F k f M HfM HfT; invset_clear HfT; simpl;
    rewrite (H1 F k _ _ HfM H); rewrite (H2 F k _ _ HfM H0); apply refl_id.
   intros E P HE HP F k f M HfM HfT; invset_clear HfT; simpl;
    rewrite (HE F k _ _ HfM H); rewrite (HP ☯☍(F) ☠k _ _ (fmuplftmaj _ _ HfM) H0); apply refl_id.
 Qed.

 Definition fpaffexpdec:=fst ftaffexpdec.
 Definition feaffexpdec:=snd ftaffexpdec.

 Theorem fpaffexpmaj:forall (M:☛)(P:☊)(f:☈), (f>☜☂M)≡⊤->f>☋P->f>☋ 〈〈M@☋P〉〉.
 Proof.
  induction M; simpl; intros P f HM HP.
   rewrite (fpaffexpmemp P); apply HP.
   induction d; destruct (decigt b f).
    rewrite <- (sym_id (igtpt _ _ i)) in HM; destruct (decemaj f b0).
     rewrite <- (sym_id (emajpt _ _ e)) in HM; rewrite (fpaffexpdec _ b0 b _ _ HM HP);
      apply fpaffexpmaj.
      apply e.
      apply IHM.
       apply (fmmajige _ _ _ HM (nxtrige _ _ (reflige f))).
       apply fpaffexpmaj'.
        apply majvar; unfold igt; apply reflige.
        apply (pmajige _ _ _ (nxtrige _ _ (reflige f)) HP).
     rewrite <- (sym_id (emajrf _ _ e)) in HM; invset HM.
    rewrite <- (sym_id (igtrf _ _ e)) in HM; invset HM.
 Qed.

 Theorem fpmaffexpfpuplft:forall (M:☛)(P:☊), ☯☋(〈〈M@☋P〉〉)≡〈〈☯☜(M)@☋☯☋(P)〉〉.
 Proof.
  induction M; intros P; simpl.
   rewrite (fpaffexpmemp P); rewrite (fpaffexpmemp ☯☋(P)); apply refl_id.
   induction d; unfold fmuplft at 1; fold fmuplft.
   assert (H:{f:☈ & ((f>☜☂M)≡⊤)*(f>☋P)}). exists (☰☜(M) ☰☉ ☰☋(P)); split.
    apply (fmmajige _ _ _ (fmfreshfmmaj M) (fimaxgel _ ☰☋(P))).
    rewrite (commfimax ☰☜(M) ☰☋(P)); apply (pmajige _ _ _ (fimaxgel ☰☋(P) ☰☜(M)) (fpnewfmaj P)).
   destruct H; destruct p.
   rewrite (fpaffexpdec _ b0 b _ _ i p); unfold fpuplft at 1;
    rewrite (fpaffexpfpuploc 〈〈M @☋ 〈b♇☍χ(x)@☋P〉 〉〉 b0 _ _ (gefst x)); fold (feuplft b0);
    fold (fpuplft 〈〈M @☋ 〈b♇☍χ(x)@☋P〉 〉〉).
   rewrite (IHM 〈b♇☍χ(x)@☋P〉); unfold fpuplft at 1;
    rewrite (fpaffexpfpuploc P χ(x) _ _ (gefst b)); unfold feuploc;
    rewrite (igept _ _ (gefst x)); fold (fpuplft P).
   rewrite (fpaffexpdec ☯☋(P) ☯☍(b0) ☠b ☠x ☯☜(M) (fmuplftmaj _ _ i) (fpuplftmaj _ _ p));
    apply refl_id.
 Qed.

 Theorem femaffexpfeuplft:forall (M:☛)(E:☌), ☯☍(〈〈M@☍E〉〉)≡〈〈☯☜(M)@☍☯☍(E)〉〉.
 Proof.
  induction M; intros E; simpl.
   rewrite (feaffexpmemp E); rewrite (feaffexpmemp ☯☍(E)); apply refl_id.
   induction d; unfold fmuplft at 1; fold fmuplft.
   assert (H:{f:☈ & ((f>☜☂M)≡⊤)*(f>☍E)}). exists (☰☜(M) ☰☉ ☰☍(E)); split.
    apply (fmmajige _ _ _ (fmfreshfmmaj M) (fimaxgel _ ☰☍(E))).
    rewrite (commfimax ☰☜(M) ☰☍(E)); apply (emajige _ _ _ (fimaxgel ☰☍(E) ☰☜(M)) (fenewfmaj E)).
   destruct H; destruct p.
   rewrite (feaffexpdec _ b0 b _ _ i e); unfold feuplft at 1;
    rewrite (feaffexpfeuploc 〈〈M @☍ 〈b♇☍χ(x)@☍E〉 〉〉 b0 _ _ (gefst x)); fold (feuplft b0);
    fold (feuplft 〈〈M @☍ 〈b♇☍χ(x)@☍E〉 〉〉).
   rewrite (IHM 〈b♇☍χ(x)@☍E〉); unfold feuplft at 2; rewrite (feaffexpfeuploc E χ(x) _ _ (gefst b));
    unfold feuploc; rewrite (igept _ _ (gefst x)); fold (feuplft E).
   rewrite (feaffexpdec ☯☍(E) ☯☍(b0) ☠b ☠x ☯☜(M) (fmuplftmaj _ _ i) (feuplftmaj _ _ e));
    apply refl_id.
 Qed.

(* Summary of some interesting pseudo-ground sequents results ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition spgr(G:☐)(P:☊):Set:=forall (i:☈), (i⍀☑G)+(i⍀☋P).
 Infix "⌿⍀":=spgr (at level 70, no associativity).
 (* Nota: G⌿⍀P->G⊣P DOES NOT IMPLIES ⊣P, e.g. consider P⊣P with P a ground predicate.        *)

 Fixpoint fspgloc(G:☐)(P:☊)(n:☈){struct n}:☆:=
  match n with ☦ => (☦⍀☑☂G)∨☁(☦⍀☋☂P) | ☠n' => ((☠n'⍀☑☂G)∨☁(☠n'⍀☋☂P))∧☁(fspgloc G P n') end.

 Theorem fspglocige:forall (n i:☈)(G:☐)(P:☊), fspgloc G P n≡⊤->i≤☉n->(i⍀☑G)+(i⍀☋P).
 Proof.
  induction n; simpl; intros i G P Hf Hg.
   inversion Hg; destruct (dedfbort _ _ Hf);
    [left; apply (ginftp _ _ i1) | right; apply (pinftp _ _ i1)].
   destruct (dedfbandt _ _ Hf); destruct (deciequ i ☠n).
    rewrite i2; destruct (dedfbort _ _ i0);
     [left; apply (ginftp _ _ i3) | right; apply (pinftp _ _ i3)].
    generalize (igeneq _ _ e Hg); intros Hg'; inversion_clear Hg'; apply (IHn i G P i1 H).
 Qed.

 Theorem fspglocexs:forall (n:☈)(G:☐)(P:☊), fspgloc G P n≡⊥->{i:☈ & i≤☉n*!(i⍀☑G)*!(i⍀☋P)}.
 Proof.
  induction n; simpl; intros G P Hf.
   exists ☦; split; [split | idtac];destruct (dedfborf _ _ Hf);
    [apply reflige | apply (ginffr _ _ i) |  apply (pinffr _ _ i0)].
   destruct (dedfbandf _ _ Hf).
    exists ☠n; split; [split | idtac];destruct (dedfborf _ _ i);
    [apply reflige | apply (ginffr _ _ i0) |  apply (pinffr _ _ i1)].
    destruct (IHn _ _ i); destruct p; destruct p; exists x; split; [split | idtac];
     [apply nxtrige; apply i0 | apply e0 | apply e].
 Qed.

 Definition fspgr(G:☐)(P:☊):☆:=fspgloc G P ☰☋(P).
 Infix "⌿☁⍀☁":=fspgr (at level 70, no associativity).

 Theorem impspgr:(spgr☵☨☂fspgr).
 Proof.
  unfold Implem2; intros s t; split; unfold spgr; unfold fspgr; intros H.
   intros i; destruct (decige i  ☰☋(t)).
    apply (fspglocige _ _ _ _ H i0).
    right; apply (pmajinf _ _ (pmajige _ _ i (asymige _ _ e) (fpnewfmaj t))).
   intros H'; destruct (fspglocexs _ _ _ H); destruct p; destruct p; destruct (H' x);
    [apply (e0 g) | apply (e p)].
 Qed.

 Definition decspgr:=decimp2 impspgr.
 Definition spgrpt:=imp2pt impspgr.
 Definition spgrrf:=imp2rf impspgr.
 Definition spgrtp:=imp2tp impspgr.
 Definition spgrfr:=imp2fr impspgr.

 Theorem spgrfpbdfor:forall (G:☐)(P:☊), G⊣P->forall (v:☈), (v⍀☑G)+(v⍀☋P)->G⊣ ☬☢(v∙P).
 Proof.
  intros G P HP v Hpg; destruct Hpg; [apply (bifori _ _ _ g HP) | apply (bifori3 _ _ _ p HP)].
 Qed.

 Theorem spgrfpaffexp:forall (G:☐)(P:☊), G⊣P->
                                         forall (v:☈), (v⍀☑G)+(v⍀☋P)->forall (E:☌), G⊣ 〈v♇☍E@☋P〉.
 Proof.
  intros G P HP v Hpg E; apply bifore; apply (spgrfpbdfor _ _ HP v Hpg).
 Qed.

 Theorem spgrfpincidx:forall (G:☐)(P:☊), G⊣P->forall (v:☈), (v⍀☑G)+(v⍀☋P)->G⊣ 〈v♇☍χ(☠v)@☋P〉.
 Proof.
  intros G P HP v Hpg; apply (spgrfpaffexp _ _ HP v Hpg).
 Qed.

(* Multiple affectations and pseudo-ground sequents ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem spgrfpmaffexp:forall (M:☛)(G:☐)(P:☊),
                                           G⊣P->(forall (x:☈), fmhaskey M x≡⊤->(x⍀☑G)+(x⍀☋P))->G⊣ 〈〈M@☋P〉〉.
 Proof.
  induction M; simpl; intros G P HP Hpg.
   rewrite (fpaffexpmemp P); apply HP.
   induction d.
   assert (H:{f:☈ & (((f>☜☂(M  ☜★ sab b b0))≡⊤)*(f>☋P))*(f>☑G)}).
    exists ((☰☜(M  ☜★ sab b b0) ☰☉ ☰☋(P)) ☰☉ ☰☑(G)); destruct (decige (☰☜(M  ☜★ sab b b0) ☰☉ ☰☋(P)) ☰☑(G)).
     rewrite (invfimax _ _ i); split; [split | idtac].
      generalize (fmmajige _ _ (☰☜(M☜★sab b b0) ☰☉ ☰☋(P)) (fmfreshfmmaj (M☜★sab b b0)) (fimaxgel _ _));
       intros Hmaj; apply (fmmajige _ _ _ Hmaj i).
      generalize (pmajige P _ _ (fimaxgel ☰☋(P) ☰☜(M☜★sab b b0)) (fpnewfmaj P)); intros Hmaj;
       rewrite <- (sym_id (commfimax ☰☋(P) ☰☜(M☜★sab b b0))) in Hmaj; apply (pmajige P _ _ i Hmaj).
      apply fgnewfmaj.
     assert (e':☰☑(G)≤☉(☰☜(M  ☜★ sab b b0) ☰☉ ☰☋(P))). apply igtige; apply nigeigt; apply e.
     rewrite (commfimax (☰☜(M  ☜★ sab b b0) ☰☉ ☰☋(P)) ☰☑(G)); rewrite (invfimax _ _ e'); split;
      [split | idtac].
      apply (fmmajige _ _ _ (fmfreshfmmaj _) (fimaxgel ☰☜(M  ☜★ sab b b0) ☰☋(P))).
      rewrite (commfimax ☰☜(M  ☜★ sab b b0) ☰☋(P));
       apply (pmajige _ _ _ (fimaxgel ☰☋(P) ☰☜(M  ☜★ sab b b0)) (fpnewfmaj P)).
      apply (gmajige G _ _ (fgnewfmaj G) e').
   destruct H; rename x into f; destruct p; destruct p.
   assert (p':(f>☜☂M)≡⊤). apply (fmmajminc M _ _ i); apply mincover; apply reflminc.
   assert (p1:b<☉f). destruct (decigt b f);
    [apply i0 | simpl in i; rewrite <- (sym_id (igtrf _ _ e)) in i; invset i].
   assert (p2:f>☍b0). destruct (decemaj f b0);
    [apply e
    | simpl in i; rewrite <- (sym_id (igtpt _ _ p1)) in i;
      rewrite <- (sym_id (emajrf _ _ e)) in i; invset i].
   rewrite (fpaffexpdec _ b0 b _ _ p' p); apply spgrfpaffexp.
    apply IHM.
     apply spgrfpaffexp;
      [apply HP | apply (Hpg b); rewrite (iequpt _ _ (refl_id b)); apply refl_id].
     intros y Hkey; destruct (deciequ y b).
      rewrite <- (sym_id i0) in Hkey; rewrite i0; clear i0 y; generalize (Hpg b);
       rewrite (iequpt _ _ (refl_id b)); intros H; generalize (H (refl_id ⊤)); clear H; intros H;
       destruct H.
       left; apply g0.
       right; simpl in p; destruct (deciequ b f).
        rewrite <- (sym_id i0) in p1; destruct (nrefligt f p1).
        apply fpaffexpinf; apply infvar; apply e.
      generalize (Hpg y); rewrite (iequrf _ _ e); intros H; generalize (H Hkey); clear H;
       intros H; destruct H.
       left; apply g0.
       right; apply fpaffexpinf'.
        apply infvar; destruct (deciequ y f).
         rewrite <- (sym_id i0) in Hkey; rewrite <- (sym_id (fmmajfmhaskey _ _ p')) in Hkey;
          invset Hkey.
         apply e0.
        apply p0.
     left; apply gmajinf; apply g.
 Qed.

(* Description of fpuploc as multiple affectations ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* In this part, we show that previously defined operations such as lifting or binding *)
(* can be represented as special case for multiple affectations - to begin with the      *)
(* functiond fpuploc and feuploc.                                                                                                 *)

 Fixpoint fmginc(f t:☈){struct t}:☛:=
  match f≤☉☂t with
  | ⊤ => match t with ☦ => ∅☜ ☜★ sab ☦ χ(☧) | ☠t' => (fmginc f t') ☜★ sab t χ(☠t) end
  | ⊥ => ∅☜
  end.

 Theorem fmgincnul:forall (f t:☈), t<☉f->fmginc f t≡∅☜.
 Proof.
  intros f; induction t; intros H; simpl.
   invset H; simpl; apply refl_id.
   destruct (decige f ☠t);
    [destruct (nrefligt _ (transigeigt _ _ _ i H)) | rewrite (igerf _ _ e); apply refl_id].
 Qed.

 Theorem fmgincin:forall (f t:☈), f≤☉t->
                                     forall (v:☈), (f≤☉v->v≤☉t->fmget (fmginc f t) v≡χ(☠v))*
                                                           (v<☉f + t<☉v->fmhaskey (fmginc f t) v≡⊥).
 Proof.
  intros f; induction t; intros Hft v; simpl; split;
   [intros Hfv Hvt | intros Hv; destruct Hv | intros Hfv Hvt | intros Hv; destruct Hv].
   invset Hft; invset Hvt; simpl; apply refl_id.
   invset Hft; rewrite <- H in i; invset i.
   invset Hft; invset i; simpl; apply refl_id.
   rewrite (igept _ _ (transige _ _ _ Hfv Hvt)); simpl; destruct (deciequ v ☠t).
    rewrite (sym_id i); rewrite (iequpt _ _ (refl_id v)); apply refl_id.
    rewrite (iequrf _ _ e); destruct (deciequ f ☠t).
     rewrite <- (sym_id i) in Hfv; destruct (e (symige _ _ Hvt Hfv)).
     generalize (igeneq _ _ e0 Hft); intros H; invset_clear H;
      generalize (igeneq _ _ e Hvt); intros H; invset_clear H; apply (fst (IHt H0 v) Hfv H1).
   rewrite (igept _ _ Hft); simpl; destruct (deciequ v ☠t).
    rewrite <- (sym_id i0) in i; destruct (nrefligt _ (transigtige _ _ _ i Hft)).
    rewrite (iequrf _ _ e); destruct (deciequ f ☠t).
     rewrite i0; rewrite (fmgincnul _ _ (reflige ☠t)); simpl; apply refl_id.
     generalize (igeneq _ _ e0 Hft); intros H; invset_clear H;
      apply (snd (IHt H0 v) (inl _ i)).
   rewrite (igept _ _ Hft); simpl; destruct (deciequ v ☠t).
    rewrite <- (sym_id i0) in i; destruct (nrefligt _ i).
    rewrite (iequrf _ _ e); destruct (deciequ f ☠t).
     rewrite i0; rewrite (fmgincnul _ _ (reflige ☠t)); simpl; apply refl_id.
     generalize (igeneq _ _ e0 Hft); intros H; invset_clear H; generalize (nxtrigt _ _ i);
      intros H; invset_clear H; apply (snd (IHt H0 v) (inr _ H1)).
 Qed.

 Theorem fmgincfmuplft:forall (f t:☈), ☯☜(fmginc f t)≡fmginc ☠f ☠t.
 Proof.
  intros f; induction t; simpl.
   destruct f; [unfold feuplft | idtac]; simpl; apply refl_id.
   destruct (decige f ☠t).
    rewrite (igept _ _ i); destruct (decige f t).
     rewrite (igept _ _ i0); simpl; unfold feuplft; simpl; rewrite IHt; simpl;
      rewrite (igept _ _ i0); apply refl_id.
     rewrite (symige _ _ i (nigeigt _ _ e)); rewrite  (fmgincnul _ _ (reflige ☠t));
      rewrite (igerf _ _ (nreflnige t)); simpl; unfold feuplft; simpl; apply refl_id.
    rewrite (igerf _ _ e); simpl; apply refl_id.
 Qed.

 Theorem fmgincftuploc:(forall (P:☊)(f t:☈), ☰☋(P)≤☉t->〈〈fmginc f t@☋P〉〉≡fpuploc f P)*
                                           (forall (E:☌)(f t:☈), ☰☍(E)≤☉t->〈〈fmginc f t@☍E〉〉≡feuploc f E).
 Proof.
  assert (H:forall (i:☈), !(i≡☠i)). induction i; intros H; invset H; apply IHi;
   rewrite (sym_id H1); apply refl_id.
  apply (Bst_ind (fun (P:☊)=>forall (f t:☈), ☰☋(P)≤☉t->〈〈fmginc f t@☋P〉〉≡fpuploc f P)
                             (fun (E:☌)=>forall (f t:☈), ☰☍(E)≤☉t->〈〈fmginc f t@☍E〉〉≡feuploc f E)); simpl.
   intros f t HT; apply refl_id.
   intros i; split; [split | idtac]; intros f t HT; try (apply refl_id);
    destruct (decige f i).
    rewrite (igept _ _ i0); generalize (nxtrige _ _ HT); intros H1; invset_clear H1;
     apply (fst (fmgincin _ _ (transige _ _ _ i0 H0) _) i0 H0).
    rewrite (igerf _ _ e); destruct (decige f t).
     apply (fmgetidx _ _ (snd (fmgincin _ _ i0 i) (inl _ (nigeigt _ _ e)))).
     rewrite (fmgincnul _ _ (nigeigt _ _ e0)); simpl; apply refl_id.
   intros P HP; split; intros f t HT.
    rewrite (HP f t HT); apply refl_id.
    rewrite (fmgincfmuplft f t).
    assert (HT':☰☋(P)≤☉ ☠t). destruct ☰☋(P); simpl; [apply gefst | apply genxt; apply HT].
    rewrite (HP ☠f _ HT'); apply refl_id.
   intros E HE; split; intros f t HT; rewrite (HE f t HT); apply refl_id.
   intros P1 P2 H1 H2; split; intros f t HT;
    rewrite (H1 f t (transige _ _ t (fimaxgel ☰☋(P1) ☰☋(P2)) HT));
    rewrite <- (sym_id (commfimax ☰☋(P1) ☰☋(P2))) in HT;
    rewrite (H2 f t (transige _ _ t (fimaxgel ☰☋(P2) ☰☋(P1)) HT)); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros f t HT;
    rewrite (H1 f t (transige _ _ t (fimaxgel ☰☍(E1) ☰☍(E2)) HT));
    rewrite <- (sym_id (commfimax ☰☍(E1) ☰☍(E2))) in HT;
    rewrite (H2 f t (transige _ _ t (fimaxgel ☰☍(E2) ☰☍(E1)) HT)); apply refl_id.
   intros E P HE HP f t HT;
    rewrite (HE f t (transige _ _ t (fimaxgel ☰☍(E) (☋☰☋(P))) HT));
    rewrite <- (sym_id (commfimax ☰☍(E) (☋☰☋(P)))) in HT; assert (HT':☰☋(P)≤☉ ☠t).
     destruct ☰☋(P); simpl; [apply gefst
                                          | apply genxt; apply (transige b _ t (fimaxgel b  ☰☍(E)) HT)].
    rewrite (fmgincfmuplft f t); rewrite (HP ☠f _ HT'); apply refl_id.
 Qed.

 Definition fmgincfpuploc:=fst fmgincftuploc.
 Definition fmgincfeuploc:=snd fmgincftuploc.

 Theorem spgrfpuploc:forall (G:☐)(P:☊)(d:☈),
                                       G⊣P->(forall (x:☈), d≤☉x->(x⍀☑G)+(x⍀☋P))->G⊣fpuploc d P.
 Proof.
  intros G P d HP Hpg; rewrite (sym_id (fmgincfpuploc P d _ (reflige ☰☋(P))));
   apply spgrfpmaffexp.
   apply HP.
   intros x Hkey; destruct (decige d x).
    apply (Hpg _ i).
    destruct (decige d ☰☋(P));
     [rewrite <- (sym_id (snd (fmgincin _ _ i x) (inl _ (nigeigt _ _ e)))) in Hkey
     | rewrite <- (sym_id (fmgincnul _ _ (nigeigt _ _ e0))) in Hkey]; invset Hkey.
 Qed.

 Theorem spgrfpuplft:forall (G:☐)(P:☊), G⊣P->G⌿⍀P->G⊣ ☯☋(P).
 Proof.
  intros G P HP Hpg; unfold fpuplft; apply (spgrfpuploc _ _ ☦ HP); intros x H; apply Hpg.
 Qed.

(* Description of fpbdloc as multiple affectations ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fmgbd(f t v:☈){struct t}:☛:=
  match f≤☉☂t with
  | ⊤ => (match t with ☦ => ∅☜ | ☠t' => fmgbd f t' v end) ☜★
            sab t χ(match t=☉☂v with ⊤ => f | ⊥ => ☠t end)
  | ⊥ => ∅☜
  end.

 Theorem fmgbdnul:forall (f t v:☈), t<☉f->fmgbd f t v≡∅☜.
 Proof.
  intros f; induction t; intros v H; simpl.
   invset H; simpl; apply refl_id.
   destruct (decige f ☠t);
    [destruct (nrefligt _ (transigeigt _ _ _ i H)) | rewrite (igerf _ _ e); apply refl_id].
 Qed.

 Theorem fmgbduploc:forall (f t v:☈), (v<☉f)+(t<☉v)->fmgbd f t v≡fmginc f t.
 Proof.
  intros f; induction t; intros v H; simpl.
   destruct f; simpl.
    destruct v; simpl; [destruct H; invset i | apply refl_id].
    apply refl_id.
   destruct (decige f ☠t).
    rewrite (igept _ _ i); destruct v; simpl.
     destruct H; [rewrite (IHt ☦ (inl _ i0)); apply refl_id | invset i0].
     destruct (deciequ t v).
      rewrite <- (sym_id i0) in H; destruct H;
       [rewrite <- (sym_id i0) in i; destruct (nrefligt _ (transigtige _ _ _ i1 i))
       | destruct (nrefligt _ i1)].
      rewrite (iequrf _ _ e); destruct H.
       rewrite (IHt ☠v (inl _ i0)); apply refl_id.
       generalize (nxtrigt _ _ i0); intros H; invset_clear H; rewrite (IHt ☠v (inr _ H0));
        apply refl_id.
    rewrite (igerf _ _ e); apply refl_id.
 Qed.

 Theorem fmgbdin:forall (f t v:☈),
                               (forall (x:☈), (f≤☉x->x≤☉t->!(x≡v)->fmget (fmgbd f t v) x≡χ(☠x))*
                                                      (x<☉f+t<☉x->fmhaskey (fmgbd f t v) x≡ ⊥))*
                               (f≤☉v->v≤☉t->fmget (fmgbd f t v) v≡χ(f)).
 Proof.
  intros f; induction t; intros v; simpl; split; (intros Hfv Hvt) || (intros x; split).
   intros Hfx Hxt Hxv; invset Hxt; rewrite <- H in Hfx; rewrite <- H in Hxv;
    invset Hfx; simpl; destruct v; [destruct (Hxv (refl_id ☦)) | apply refl_id].
   intros Hx; destruct f; simpl; invset Hx; invset H; simpl; apply refl_id.
   invset Hvt; rewrite <- H in Hfv; invset Hfv; simpl; apply refl_id.
   intros Hfx Hxt Hxv; rewrite (igept _ _ (transige _ _ _ Hfx Hxt)); destruct v; simpl;
    destruct (deciequ x ☠t).
     rewrite (iequpt _ _ i); rewrite i; apply refl_id.
     rewrite (iequrf _ _ e); generalize (igeneq _ _ e Hxt); intros H; invset_clear H;
      apply (fst (fst (IHt _) _) Hfx H0 Hxv).
     rewrite (iequpt _ _ i); rewrite i; rewrite <- (sym_id i) in Hxv; destruct (deciequ t v);
      [rewrite <- (sym_id i0) in Hxv; destruct Hxv | rewrite (iequrf _ _ e)]; apply refl_id.
     rewrite (iequrf _ _ e); generalize (igeneq _ _ e Hxt); intros H; invset_clear H;
      apply (fst (fst (IHt _) _) Hfx H0 Hxv).
   intros Hx; destruct (decige f ☠t).
    rewrite (igept _ _ i); simpl; destruct (deciequ x ☠t).
     rewrite <- (sym_id i0) in Hx; invset Hx;
      [destruct (nrefligt _ (transigtige _ _ _ H i)) | destruct (nrefligt _ H)].
     rewrite (iequrf _ _ e); invset Hx.
      apply (snd (fst (IHt v) x) (inl _ H)).
      generalize (nxtrigt _ _ H); intros H0; invset H0; apply (snd (fst (IHt v) x) (inr _ H3)).
    rewrite (igerf _ _ e); apply refl_id.
   rewrite (igept _ _ (transige _ _ _ Hfv Hvt)); simpl; destruct (deciequ v ☠t).
    rewrite (iequpt _ _ i); rewrite i; rewrite (iequpt _ _ (refl_id t)); apply refl_id.
    rewrite (iequrf _ _ e); generalize (igeneq _ _ e Hvt); intros H; invset_clear H;
     apply (snd (IHt v) Hfv H0).
 Qed.

 Theorem fmgbdfmuplft:forall (f t v:☈), ☯☜(fmgbd f t v)≡(fmgbd ☠f ☠t ☠v).
 Proof.
  assert (H:forall (i:☈), !(i≡☠i)). induction i; intros H; invset H; apply IHi;
   rewrite (sym_id H1); apply refl_id.
  intros f; induction t; intros v.
   destruct f; simpl;
    [destruct v; simpl; unfold feuplft; simpl | idtac]; apply refl_id.
   assert (H0:{t':☈ & (☠t'≡☠☠t)}). exists ☠t; apply refl_id.
   destruct H0; rewrite (sym_id i); unfold fmgbd; fold fmgbd; rewrite i; invset i;
    invset H1; clear x i H1 H0.
   destruct (decige f ☠t).
    rewrite (igept _ _ i); rewrite (igept _ _ (genxt _ _ i)); destruct (deciequ ☠t v).
     rewrite (iequpt _ _ i0); rewrite i0; rewrite (iequpt _ _ (refl_id ☠v)); unfold fmuplft;
      fold fmuplft; unfold feuplft; unfold feuploc; rewrite (igept _ _ (gefst f));
      rewrite (IHt v); rewrite i0; apply refl_id.
     assert (e':!(☠☠t≡☠v)). intros e'; invset e'; apply e; rewrite H1; apply refl_id.
     rewrite (iequrf _ _ e); rewrite (iequrf _ _ e'); unfold fmuplft; fold fmuplft; unfold feuplft;
      unfold feuploc; rewrite (igept _ _ (gefst ☠☠t)); rewrite (IHt v); apply refl_id.
    assert (e':!(☠f ≤☉ ☠☠t)). intros e'; apply e; invset e'; apply H2.
    rewrite (igerf _ _ e); rewrite (igerf _ _ e'); apply refl_id.
 Qed.

 Theorem fmgbdftbdloc:(forall (P:☊)(f t v:☈), ☰☋(P)≤☉t->〈〈fmgbd f t v@☋P〉〉≡fpbdloc v f P)*
                                         (forall (E:☌)(f t v:☈), ☰☍(E)≤☉t->〈〈fmgbd f t v@☍E〉〉≡febdloc v f E).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (f t v:☈), ☰☋(P)≤☉t->〈〈fmgbd f t v@☋P〉〉≡fpbdloc v f P)
                             (fun (E:☌)=>forall (f t v:☈), ☰☍(E)≤☉t->〈〈fmgbd f t v@☍E〉〉≡febdloc v f E)).
   intros f t v Ht; simpl; apply refl_id.
   intros i; split; [split | idtac]; simpl; intros f t v Ht; try (apply refl_id);
    destruct (decige f i).
    generalize (nxtrige _ _ Ht); intros H; invset_clear H.
    rewrite (igept _ _ i0); destruct (deciequ v i).
     rewrite (iequpt _ _ i1); rewrite i1; apply (snd (fmgbdin  _ _ _) i0 H0).
     rewrite (iequrf _ _ e); apply (fst (fst (fmgbdin  _ _ v) _) i0 H0 (sym_nid e)).
    rewrite (igerf _ _ e);
     apply (fmgetidx _ _(snd (fst (fmgbdin f t v) i) (inl _ (nigeigt _ _ e)))).
   intros P HP; split; simpl; intros f t v Ht.
    rewrite (HP f _ v Ht); apply refl_id.
    assert (Ht':☰☋(P)≤☉ ☠t). destruct ☰☋(P); simpl; [apply gefst | apply (genxt _ _ Ht)].
    rewrite (fmgbdfmuplft f t v); rewrite (HP ☠f _ ☠v Ht'); apply refl_id.
   intros E HE; split; simpl; intros f t v Ht; rewrite (HE f _ v Ht); apply refl_id.
   intros P1 P2 H1 H2; split; simpl; intros f t v Ht;
    rewrite (H1 f _ v (transige _ _ t (fimaxgel ☰☋(P1) ☰☋(P2)) Ht));
    rewrite <- (sym_id (commfimax ☰☋(P1) ☰☋(P2))) in Ht;
    rewrite (H2 f t v (transige _ _ t (fimaxgel ☰☋(P2) ☰☋(P1)) Ht)); apply refl_id.
   intros E1 E2 H1 H2; split; split; simpl; intros f t v Ht;
    rewrite (H1 f _ v (transige _ _ t (fimaxgel ☰☍(E1) ☰☍(E2)) Ht));
    rewrite <- (sym_id (commfimax ☰☍(E1) ☰☍(E2))) in Ht;
    rewrite (H2 f t v (transige _ _ t (fimaxgel ☰☍(E2) ☰☍(E1)) Ht)); apply refl_id.
   intros E P HE HP f t v Ht; simpl.
    rewrite (HE f _ v (transige _ _ t (fimaxgel ☰☍(E) ☋☰☋(P)) Ht)).
    simpl in Ht; rewrite <- (sym_id (commfimax ☰☍(E) ☋☰☋(P))) in Ht.
    assert (Ht':☰☋(P)≤☉ ☠t). destruct ☰☋(P); simpl;
     [apply gefst | apply genxt; apply (transige b _ t (fimaxgel b ☰☍(E)) Ht)].
    rewrite (fmgbdfmuplft f t v); rewrite (HP ☠f _ ☠v Ht'); apply refl_id.
 Qed.

 Definition fmgbdfpbdloc:=fst fmgbdftbdloc.
 Definition fmgbdfebdloc:=snd fmgbdftbdloc.

 Theorem spgrfpbdloc:forall (G:☐)(P:☊)(v d:☈),
                                       G⊣P->(forall (x:☈), d≤☉x->(x⍀☑G)+(x⍀☋P))->G⊣fpbdloc v d P.
 Proof.
  intros G P v d HP Hpg; rewrite (sym_id (fmgbdfpbdloc P d _ v (reflige ☰☋(P))));
   apply spgrfpmaffexp.
   apply HP.
   intros x Hkey; destruct (decige d x).
    apply (Hpg _ i).
    destruct (decige d ☰☋(P));
     [rewrite <- (sym_id (snd (fst (fmgbdin _ ☰☋(P) v) _) (inl _ (nigeigt _ _ e)))) in Hkey
     | rewrite <- (sym_id (fmgbdnul _ _ v (nigeigt _ _ e0))) in Hkey]; invset Hkey.
 Qed.

(* Description of fpinloc as multiple affectations ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fmginst(f t:☈)(F:☌){struct t}:☛:=
  match f<☉☂t with
  | ⊤ => match t with ☦ => ∅☜ (*⊥*) | ☠t' => fmginst f t' F ☜★ sab t χ(t') end
  | ⊥ => match f=☉☂t with ⊤ => ∅☜ ☜★ sab t F | ⊥ => ∅☜ end
  end.

 Theorem fmginstnul:forall (f t:☈)(F:☌), t<☉f->fmginst f t F≡∅☜.
 Proof.
  intros f; induction t; intros F H; simpl.
   invset H; simpl; apply refl_id.
   rewrite (igtrf _ _ (asymigt _ _ H)); destruct (deciequ f ☠t);
    [rewrite <- (sym_id i) in H; destruct (nrefligt _ H)
    | rewrite (iequrf _ _ e); apply refl_id].
 Qed.

 Theorem fmginstin:forall (f t:☈)(F:☌),
                                   (forall (x:☈), (f≤☉x->☠x≤☉t->fmget (fmginst f t F) ☠x≡χ(x))*
                                                          (x<☉f+t<☉x->fmhaskey (fmginst f t F) x≡⊥))*
                                   (f≤☉t->fmget (fmginst f t F) f≡F).
 Proof.
  intros f; induction t; intros F; simpl; split;
   (intros x; split; intros Hfx Hxt || intros Hx) || (intros Hft).
   invset Hxt.
   destruct f; simpl; destruct Hx; invset i; apply refl_id.
   invset Hft; simpl; apply refl_id.
   rewrite (igtpt _ _ (transigeigt _ _ _ Hfx Hxt)); simpl; destruct (deciequ x t).
    rewrite (iequpt _ _ i); rewrite i; apply refl_id.
    rewrite (iequrf _ _ e); invset Hxt; apply (fst (fst (IHt F) x) Hfx (igeneq _ _ e H1)).
   destruct (decige f t).
    rewrite (igtpt _ _ (genxt _ _ i)); simpl; destruct Hx.
     destruct (deciequ x ☠t).
      rewrite <- (sym_id i1) in i0;
       destruct (nrefligt _ (nxtrigt _ _ (transigtige _ _ _ i0 i))).
      rewrite (iequrf _ _ e); apply (snd (fst (IHt F) _) (inl _ i0)).
     destruct (deciequ x ☠t).
      rewrite <- (sym_id i1) in i0; destruct (nrefligt _ i0).
      generalize (nxtrigt _ _ i0); intros H; invset_clear H; rewrite (iequrf _ _ e);
       apply (snd (fst (IHt F) x) (inr _ H0)).
   assert (e':!(f<☉ ☠t)). intros H; invset H; apply e; apply H2.
   rewrite (igtrf _ _ e'); destruct (deciequ f ☠t).
    rewrite (iequpt _ _ i); rewrite <- (sym_id i) in Hx; simpl; destruct Hx;
     destruct (deciequ x ☠t); try (rewrite <- (sym_id i1) in i0; destruct (nrefligt _ i0));
     rewrite (iequrf _ _ e0); apply refl_id.
    rewrite (iequrf _ _ e0); apply refl_id.
   destruct (deciequ f ☠t).
    destruct (decigt f ☠t); try (rewrite <- (sym_id i) in i0; destruct (nrefligt _ i0));
     rewrite (igtrf _ _ e); rewrite (iequpt _ _ i); simpl; rewrite (iequpt _ _ i);
     apply refl_id.
   rewrite (igtpt _ _ (igeneq _ _ e Hft)); simpl; rewrite (iequrf _ _ e);
    generalize (igeneq _ _ e Hft); intros H; invset_clear H; apply (snd (IHt F) H0).
 Qed.

 Theorem fmginstfmuplft:forall (f t:☈)(F:☌), ☯☜(fmginst f t F)≡(fmginst ☠f ☠t ☯☍(F)).
 Proof.
  intros f; induction t; intros F.
   destruct f; simpl; apply refl_id.
   assert (H0:{t':☈ & ☠t'≡☠☠t}). exists ☠t; apply refl_id.
   destruct H0; rewrite (sym_id i); unfold fmginst; fold fmginst; rewrite i; invset i;
    clear x i H0.
   destruct (decigt f ☠t).
    rewrite (igtpt _ _ i); rewrite (igtpt _ _ (gtnxt _ _ i)); unfold fmuplft; fold fmuplft;
     rewrite (IHt F); apply refl_id.
    assert (e':!(☠f<☉ ☠☠t)). intros e'; apply e; invset e'; apply H1.
    rewrite (igtrf _ _ e); rewrite (igtrf _ _ e'); simpl; destruct (f=☉☂☠t); unfold fmuplft;
     apply refl_id.
 Qed.

 Theorem fmginstftinloc:(forall (P:☊)(F:☌)(f t:☈), ☰☋(P)≤☉t->
                                              〈〈fmginst f t F@☋P〉〉≡fpinloc f F P)*
                                             (forall (E F:☌)(f t:☈), ☰☍(E)≤☉t->
                                              〈〈fmginst f t F@☍E〉〉≡feinloc f F E).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (F:☌)(f t:☈), ☰☋(P)≤☉t->
                              〈〈fmginst f t F@☋P〉〉≡fpinloc f F P)
                             (fun (E:☌)=>forall (F:☌)(f t:☈), ☰☍(E)≤☉t->
                              〈〈fmginst f t F@☍E〉〉≡feinloc f F E)); simpl.
   intros F f t Ht; apply refl_id.
   intros i; split; [split | idtac]; simpl; intros F f t Ht; try (apply refl_id);
    destruct (decige f i).
    rewrite (igept _ _ i0); destruct (deciequ f i).
     rewrite (iequpt _ _ i1); rewrite (sym_id i1);
      apply (snd (fmginstin f t F) (igtige _ _ (transige _ _ _ (genxt _ _ i0) Ht))).
     rewrite(iequrf _ _ e); generalize (igeneq _ _ e i0); intros H; invset H; simpl;
      rewrite <- H2 in Ht; apply (fst (fst (fmginstin _ t F) _) H1 (igtige _ _ Ht)).
    rewrite (igerf _ _ e);
     apply (fmgetidx _ _ (snd (fst (fmginstin f t F) i) (inl _ (nigeigt _ _ e)))).
   intros P HP; split; simpl; intros F f t Ht.
    rewrite (HP F f _ Ht); apply refl_id.
    assert (Ht':☰☋(P)≤☉ ☠t). destruct ☰☋(P); [apply gefst | apply (genxt _ _ Ht)].
    rewrite (fmginstfmuplft f t F); rewrite (HP ☯☍(F) ☠f _ Ht'); apply refl_id.
   intros E HE; split; simpl; intros F f t Ht; rewrite (HE F f _ Ht); apply refl_id.
   intros P1 P2 H1 H2; split; simpl; intros F f t Ht;
    rewrite (H1 F f _ (transige _ _ t (fimaxgel ☰☋(P1) ☰☋(P2)) Ht));
    rewrite <- (sym_id (commfimax ☰☋(P1) ☰☋(P2))) in Ht;
    rewrite (H2 F f _ (transige _ _ t (fimaxgel ☰☋(P2) ☰☋(P1)) Ht)); apply refl_id.
   intros E1 E2 H1 H2; split; split; simpl; intros F f t Ht;
    rewrite (H1 F f _ (transige _ _ t (fimaxgel ☰☍(E1) ☰☍(E2)) Ht));
    rewrite <- (sym_id (commfimax ☰☍(E1) ☰☍(E2))) in Ht;
    rewrite (H2 F f _ (transige _ _ t (fimaxgel ☰☍(E2) ☰☍(E1)) Ht)); apply refl_id.
   intros E P HE HP F f t Ht; simpl; simpl in Ht;
    rewrite (HE F f _ (transige _ _ t (fimaxgel ☰☍(E) ☋☰☋(P)) Ht));
    rewrite <- (sym_id (commfimax ☰☍(E) ☋☰☋(P))) in Ht.
    assert (Ht':☰☋(P)≤☉ ☠t). destruct ☰☋(P);
     [apply gefst | apply genxt; apply (transige _ _ _ (fimaxgel b ☰☍(E)) Ht)].
    rewrite (fmginstfmuplft f t F); rewrite (HP ☯☍(F) ☠f _ Ht'); apply refl_id.
 Qed.

 Definition fmginstfpinloc:=fst fmginstftinloc.
 Definition fmginstfeinloc:=snd fmginstftinloc.

 Theorem spgrfpinloc:forall (G:☐)(P:☊)(d:☈)(F:☌),
                                       G⊣P->(forall (x:☈), d≤☉x->(x⍀☑G)+(x⍀☋P))->G⊣fpinloc d F P.
 Proof.
  intros G P d F HP Hpg; rewrite (sym_id (fmginstfpinloc P F d _ (reflige ☰☋(P))));
   apply spgrfpmaffexp.
   apply HP.
   intros x Hkey; destruct (decige d x).
    apply (Hpg _ i).
    destruct (decige d ☰☋(P));
     [rewrite <- (sym_id (snd (fst (fmginstin _ ☰☋(P) F) _) (inl _ (nigeigt _ _ e)))) in Hkey
     | rewrite <- (sym_id (fmginstnul _ _ F (nigeigt _ _ e0))) in Hkey]; invset Hkey.
 Qed.

(* Composition of multiple affectations ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* fmcmp M N returns a multiple affectation M' such as 〈〈M@〈〈N@T〉〉〉〉=〈〈M'@T〉〉                 *)

 Fixpoint fmcmpapp(M N:☛){struct N}:☛:=
  match N with
  | ∅☜ => ∅☜
  | N' ☜★ (sab x E) =>(fmcmpapp M N') ☜★ (sab x 〈〈M@☍E〉〉)
  end.

 Theorem fmcmpappnull:forall (N:☛), fmcmpapp ∅☜ N≡N.
 Proof.
  induction N; unfold fmcmpapp; fold fmcmpapp.
   apply refl_id.
   rewrite IHN; induction d; rewrite (feaffexpmemp b0); apply refl_id.
 Qed.

 Theorem fmcmpappnulr:forall (M:☛), fmcmpapp M ∅☜≡∅☜.
 Proof.
  intros M; apply refl_id.
 Qed.

 Theorem fmcmpapphk:forall (M N:☛)(k:☈),fmhaskey N k≡fmhaskey (fmcmpapp M N) k.
 Proof.
  intros M; induction N; intros k; simpl.
   apply refl_id.
   induction d; destruct (deciequ k b).
    rewrite (iequpt _ _ i); rewrite i; simpl; rewrite (iequpt _ _ (refl_id b)); apply refl_id.
    rewrite (iequrf _ _ e); simpl; rewrite (iequrf _ _ e); apply IHN.
 Qed.

 Theorem fmcmpappget:forall (M N:☛)(k:☈),
                                       (fmhaskey N k≡⊤->fmget (fmcmpapp M N) k≡〈〈M@☍fmget N k〉〉)*
                                       (fmhaskey N k≡⊥->fmget (fmcmpapp M N) k≡χ(k)).
 Proof.
  intros M; induction N; intros k; simpl; split; intros Hk.
   invset Hk; invset H.
   apply refl_id.
   induction d; destruct (deciequ k b).
    rewrite (iequpt _ _ i); simpl; rewrite (iequpt _ _ i); apply refl_id.
    rewrite <- (sym_id (iequrf _ _ e)) in Hk; rewrite (iequrf _ _ e); simpl;
     rewrite (iequrf _ _ e); apply (fst (IHN k) Hk).
   induction d; destruct (deciequ k b).
    rewrite <- (sym_id (iequpt _ _ i)) in Hk; invset Hk; invset H.
    rewrite <- (sym_id (iequrf _ _ e)) in Hk; simpl; rewrite (iequrf _ _ e);
     apply (snd (IHN k) Hk).
 Qed.

 Theorem fmcmpappuplft:forall (M N:☛), ☯☜(fmcmpapp M N)≡fmcmpapp ☯☜(M) ☯☜(N).
 Proof.
  intros M; induction N; simpl.
   apply refl_id.
   induction d; unfold fmuplft; fold fmuplft; rewrite IHN; simpl;
    rewrite (femaffexpfeuplft M b0); apply refl_id.
 Qed.

 Fixpoint fmcmpins(M N:☛){struct M}:☛:=
  match M with
  | ∅☜ => N
  | M' ☜★ (sab x E) => match fmhaskey N x with
                              | ⊤ => fmcmpins M' N
                              | ⊥ => (fmcmpins M' N) ☜★ (sab x E)
                              end
  end.

 Theorem fmcmpinsnull:forall (N:☛), fmcmpins ∅☜ N≡N.
 Proof.
  intros N; apply refl_id.
 Qed.

 Theorem fmcmpinsnulr:forall (M:☛), fmcmpins M ∅☜≡M.
 Proof.
  induction M; simpl.
   apply refl_id.
   rewrite IHM; induction d; apply refl_id.
 Qed.

 Theorem fmcmpinshk:forall (M N:☛)(k:☈),
                                     fmhaskey N k ∨☁ fmhaskey M k≡fmhaskey (fmcmpins M N) k.
 Proof.
  induction M; intros N k; simpl.
   destruct (fmhaskey N k); apply refl_id.
   induction d; destruct (deciequ k b).
    rewrite (iequpt _ _ i); rewrite i; rewrite (shortfborr (fmhaskey N b));
     destruct ((ElimB2 fmhaskey) N b); rewrite i0;
      [rewrite (sym_id (IHM N b)); rewrite i0
      | simpl; rewrite (iequpt _ _ (refl_id b))]; apply refl_id.
    rewrite (iequrf _ _ e); destruct ((ElimB2 fmhaskey) N b); rewrite i; 
     [idtac | simpl; rewrite (iequrf _ _ e)]; apply (IHM N k).
 Qed.

 Theorem fmcmpinsget:forall (M N:☛)(k:☈),
                                       (fmhaskey N k≡⊤->fmget (fmcmpins M N) k≡fmget N k)*
                                       (fmhaskey N k≡⊥->fmget (fmcmpins M N) k≡fmget M k).
 Proof.
  induction M; intros N k; simpl; split; intros Hk.
   apply refl_id.
   apply (fmgetidx _ _ Hk).
   induction d; destruct ((ElimB2 fmhaskey) N b); rewrite i.
    apply (fst (IHM N k) Hk).
    simpl; destruct (deciequ k b);
     [rewrite <- (sym_id i0) in Hk; rewrite <- (sym_id Hk) in i; invset i
     | rewrite (iequrf _ _ e); apply (fst (IHM N k) Hk)].
   induction d; destruct ((ElimB2 fmhaskey) N b); rewrite i.
    destruct (deciequ k b).
     rewrite <- (sym_id i0) in Hk; rewrite <- (sym_id Hk) in i; invset i.
     rewrite (iequrf _ _ e); apply (snd (IHM N k) Hk).
    simpl; destruct (deciequ k b).
     rewrite (iequpt _ _ i0); apply refl_id.
     rewrite (iequrf _ _ e); apply (snd (IHM N k) Hk).
 Qed.

 Theorem fmcmpinskeyinc:forall (M N:☛),
                                             (forall (k:☈), fmhaskey M k≡⊤->fmhaskey N k≡⊤)->fmcmpins M N≡N.
 Proof.
  induction M; intros N Hkey; unfold fmcmpins; fold fmcmpins.
   apply refl_id.
   induction d; rewrite (Hkey b (fmhaskeymin _ _ _ (miyes (sab b b0) M))).
   apply IHM; intros k Hkey'; apply Hkey; simpl; destruct (k=☉☂b);
    [apply refl_id | apply Hkey'].
 Qed.

 Theorem fmcmpinsinc:forall (M N:☛), M⊆☜N->fmcmpins M N≡N.
 Proof.
  intros M N Hinc.
  assert (Hkey:forall (k:☈), fmhaskey M k≡⊤->fmhaskey N k≡⊤).
   intros k Hkey; destruct (fmhaskeyget _ _ Hkey).
   apply (fmhaskeymin N k x); apply Hinc; apply l.
  apply (fmcmpinskeyinc _ _ Hkey).
 Qed.

 Theorem fmcmpinseq:forall (M:☛), fmcmpins M M≡M.
 Proof.
  induction M; unfold fmcmpins; fold fmcmpins; simpl.
   apply refl_id.
   induction d; rewrite (iequpt _ _ (refl_id b));
    rewrite (fmcmpinsinc _ _ (mincover (sab b b0) _ _ (reflminc M))); apply refl_id.
 Qed.

 Theorem fmcmpinsuplft:forall (M N:☛), ☯☜(fmcmpins M N)≡fmcmpins ☯☜(M) ☯☜(N).
 Proof.
  induction M; intros N; simpl.
   apply refl_id.
   induction d; destruct ((ElimB2 fmhaskey) N b); simpl;
    rewrite (sym_id (fmuplftmhaskey N b)); rewrite i.
    apply IHM.
    unfold fmuplft; fold fmuplft; rewrite (IHM N); apply refl_id.
 Qed.

 Definition fmcmp(M N:☛):=fmcmpins M (fmcmpapp M N).
 Notation "'(' M '∘☜' N ')'":=(fmcmp M N).

 Theorem fmcmpnull:forall (N:☛), (∅☜∘☜N)≡N.
 Proof.
  intros N; unfold fmcmp; rewrite fmcmpappnull; rewrite fmcmpinsnull; apply refl_id.
 Qed.

 Theorem fmcmpnulr:forall (M:☛), (M∘☜∅☜)≡M.
 Proof.
  intros M; unfold fmcmp; rewrite fmcmpappnulr; rewrite fmcmpinsnulr; apply refl_id.
 Qed.

 Theorem fmcmphk:forall (M N:☛)(k:☈), fmhaskey N k ∨☁ fmhaskey M k≡fmhaskey (M∘☜N) k.
 Proof.
  intros M N k; unfold fmcmp; rewrite (sym_id (fmcmpinshk M (fmcmpapp M N) k));
   rewrite (sym_id (fmcmpapphk M N k)); apply refl_id.
 Qed.

 Theorem fmcmpget:forall (M N:☛)(k:☈), fmget (M∘☜N) k≡〈〈M@☍fmget N k〉〉.
 Proof.
  intros M N k; unfold fmcmp; destruct ((ElimB2 fmhaskey) (fmcmpapp M N) k).
   rewrite (fst (fmcmpinsget M (fmcmpapp M N) k) i); rewrite <- (fmcmpapphk M N k) in i;
    apply (fst (fmcmpappget M N k) i).
   rewrite (snd (fmcmpinsget M (fmcmpapp M N) k) i); rewrite <- (fmcmpapphk M N k) in i;
    rewrite (fmgetidx _ _ i); apply refl_id.
 Qed.

 Theorem fmcmpuplft:forall (M N:☛), ☯☜((M ∘☜ N))≡(☯☜(M) ∘☜ ☯☜(N)).
 Proof.
  intros M N; unfold fmcmp; rewrite (fmcmpinsuplft M (fmcmpapp M N));
   rewrite (fmcmpappuplft M N); apply refl_id.
 Qed.

 Theorem cmpftmaffexp:(forall (P:☊)(M N:☛), 〈〈M@☋ 〈〈N@☋P〉〉 〉〉≡〈〈(M∘☜N)@☋P〉〉)*
                                         (forall (E:☌)(M N:☛), 〈〈M@☍ 〈〈N@☍E〉〉 〉〉≡〈〈(M∘☜N)@☍E〉〉).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (M N:☛), 〈〈M@☋ 〈〈N@☋P〉〉 〉〉≡〈〈(M∘☜N)@☋P〉〉)
                             (fun (E:☌)=>forall (M N:☛), 〈〈M@☍ 〈〈N@☍E〉〉 〉〉≡〈〈(M∘☜N)@☍E〉〉)).
   intros M N; simpl; apply refl_id.
   intros i; split; [split | idtac]; intros M N; simpl; try (rewrite (fmcmpget M N i));
    apply refl_id.
   intros P HP; split; intros M N; simpl.
    rewrite HP; apply refl_id.
    rewrite (fmcmpuplft M N); rewrite HP; apply refl_id.
   intros E HE; split; intros M N; simpl; rewrite HE; apply refl_id.
   intros P1 P2 H1 H2; split; intros M N; simpl; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros M N; simpl; rewrite H1; rewrite H2;
    apply refl_id.
   intros E P HE HP M N; simpl; rewrite HE; rewrite (fmcmpuplft M N); rewrite HP;
    apply refl_id.
 Qed.

 Definition cmpfpmaffexp:=fst cmpftmaffexp.
 Definition cmpfemaffexp:=snd cmpftmaffexp.

 Theorem exfmcmp:forall (M N:☛), {O:☛ & (forall (P:☊), 〈〈M@☋ 〈〈N@☋P〉〉 〉〉≡〈〈O@☋P〉〉)*
                                                                   (forall (E:☌), 〈〈M@☍ 〈〈N@☍E〉〉 〉〉≡〈〈O@☍E〉〉)}.
 Proof.
  intros M N; exists (M∘☜N); split;
   [intros P; apply cmpfpmaffexp | intros E; apply cmpfemaffexp].
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
