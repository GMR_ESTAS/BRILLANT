(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Ppred, predicates calculus prover functions
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - October 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Bpred.

(* B-Book theorems ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Theorem bbt1_3_3:forall (x:☈)(P Q:☊), ⊣ ☬☢(x∙P☲Q) ☲ (☬☢(x∙P) ☲ ☬☢(x∙Q)). *)

(* Theorem bbdr12:forall (G:☐)(x:☈)(P R:☊), x⍀☑(G ☑★ R)->G⊣P☲R->G⊣ ☬☣(x∙P) ☲R. *)

(* Theorem bbdr15:forall (G:☐)(x:☈)(P:☊), G⊣ ☬☢(x∙¬P)->G⊣¬ ☬☣(x∙P). *)

(* Theorem bbt1_3_4:forall (x:☈)(P Q:☊), ⊣ ☬☣(x∙P☲Q) ☳ (☬☢(x∙P) ☲ ☬☣(x∙Q)). *)

(* Theorem commfpbdfor':forall (G:☐)(x y:☈)(P:☊), G⊣ ☬☢(x∙☬☢(y∙P))->G⊣ ☬☢(y∙☬☢(x∙P)). *)

(* Theorem commfpbdexs':forall (G:☐)(x y:☈)(P:☊), G⊣ ☬☣(x∙☬☣(y∙P))->G⊣ ☬☣(y∙☬☣(x∙P)). *)

(* Theorem disfpbdforand':forall (G:☐)(P Q:☊)(v:☈), G⊣ ☬☢(v∙P∧Q)->G⊣(☬☢(v∙P) ∧ ☬☢(v∙Q)).
    Theorem disfpbdforand'':forall (G:☐)(P Q:☊)(x:☈), G⊣(☬☢(x∙P) ∧ ☬☢(x∙Q))->G⊣ ☬☢(x∙P∧Q). *)

(* Theorem disfpbdexsior':forall (G:☐)(P Q:☊)(v:☈), G⊣ ☬☣(v∙P∨Q)->G⊣ ☬☣(v∙P) ∨ ☬☣(v∙Q).
    Theorem disfpbdexsior'':forall (G:☐)(P Q:☊)(x:☈), G⊣ ☬☣(x∙P) ∨ ☬☣(x∙Q)->G⊣ ☬☣(x∙P∨Q). *)

(* Theorem disfpbdforiornf':forall (G:☐)(P Q:☊)(v:☈), v⍀☋P->G⊣(P ∨ ☬☢(v∙Q))->G⊣ ☬☢(v∙P∨Q).
    Theorem disfpbdforiornf'':forall (G:☐)(P Q:☊)(v:☈), v⍀☋P->G⊣ ☬☢(v∙P∨Q)->G⊣(P ∨ ☬☢(v∙Q)). *)

(* Theorem disfpbdexsiornf':forall (G:☐)(P Q:☊)(v:☈), v⍀☋P->G⊣P∧ ☬☣(v∙Q)->G⊣ ☬☣(v∙P∧Q).
    Theorem disfpbdexsiornf'':forall (G:☐)(P Q:☊)(v:☈), v⍀☋P->G⊣ ☬☣(v∙P∧Q)->G⊣(P∧ ☬☣(v∙Q)). *)

(* Theorem disfpbdforimpnf':forall (G:☐)(P Q:☊)(v:☈), v⍀☋P->G⊣(P ☲ ☬☢(v∙Q))->G⊣ ☬☢(v∙P☲Q).
    Theorem disfpbdforimpnf'':forall (G:☐)(P Q:☊)(v:☈), v⍀☋P->G⊣ ☬☢(v∙P☲Q)->G⊣(P ☲ ☬☢(v∙Q)). *)

(* Theorem bbt1_4_3:forall (E F G:☌), ⊣(E♃F)∧(F♃G)☲(E♃G). *)

(* Theorem bbt1_4_4:forall (P:☊)(E:☌)(x:☈), x⍀☍E->⊣ ☬☢(x∙χ(x) ♃ E☲P)☲〈x♇☍E@☋P〉. *)
(* Theorem bbt1_4_5:forall (P:☊)(E:☌)(x:☈), x⍀☍E->⊣ 〈x♇☍E@☋P〉☲☬☢(x∙χ(x) ♃ E☲P). *)

(* Theorem bbt1_4_6:forall (P:☊)(E:☌)(x:☈), x⍀☍E->⊣ ☬☢(x∙χ(x) ♃ E☲P)☳〈x♇☍E@☋P〉.
    Theorem bbt1_4_7:forall (P:☊)(E:☌)(x:☈), x⍀☍E->⊣ ☬☣(x∙χ(x) ♃ E∧P)☳〈x♇☍E@☋P〉. *)

(* Theorem bbeql2':forall (G:☐)(P R:☊)(E F:☌)(x:☈),G⊣E♃F->G⊣ 〈x♇☍E@☋P〉☲R->G⊣ 〈x♇☍F@☋P〉->G⊣R. *)

(* Theorem bbt1_5_5:forall (G:☐)(E F E' F':☌), G⊣(E☵F)♃(E'☵F')->G⊣E♃E'∧F♃F'. *)

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
