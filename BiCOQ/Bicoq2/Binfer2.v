(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Binfer2, derived inference rules
   In this module, derived inference rules are proposed - mainly for predicate calculus,
   the new rules allowing simplification of manipulations using Bcoqinfer results.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - October 2006
   ♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Bcoqinfer.

(* Alternate ∀ rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem bifori2:forall (G:☐)(v w:☈)(P:☊),w⍀☑(G ☑★ P)->G⊣ 〈v♇☍χ w@☋P〉->G⊣ ☬☢(v∙P).
 Proof.
  intros G v w P H H0; destruct (ginfinc _ _ _ H); rewrite (fpbdforalpha _ v _ p);
   apply (bifori _ _ _ g H0).
 Qed.
 (* Nota : the condition v⍀☑(G ☑★ P) instead of v⍀☑G, is truly a semantic one as shown by the *)
 (* example v⍀☑G, G⊣[v♇☍χw@χv♃χw] - reducing to G⊣χw♃χw - yet G⊣☬☢(v∙χv♃χw) is refutable. *)

 Theorem bifori3:forall (G:☐)(v:☈)(P:☊),v⍀☋P->G⊣P->G⊣ ☬☢(v∙P).
 Proof.
  intros G v P H H0; destruct (exfgnewf (G☑★P)); destruct (ginfinc _ _ _ g);
   rewrite (fpbdforalpha _ v _ p); rewrite (fpaffexpnul _ χ(x) _ H);
   apply (bifori _ _ _ g0 H0).
 Qed.

 Theorem bifore2:forall (G:☐)(v:☈)(P:☊), G⊣ ☬☢(v∙P)->G⊣P.
 Proof.
  intros G v P H; rewrite (sym_id (fpaffexpid P v)); apply bifore; apply H.
 Qed.

(* Renaming of free variables rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Those results are interesting as they allow manipulations on pure variables (i.e.     *)
(* indexes not appearing in the proof environment).                                                                 *)

 Theorem idxalpha:forall (G:☐)(v w:☈)(P:☊), v⍀☑G->G⊣P->G⊣ 〈v♇☍χ(w)@☋P〉.
 Proof.
  intros G v w P H H0; apply (bifore G v P χ(w)); apply (bifori G v P H H0).
 Qed.

 Theorem idxalpha':forall (G:☐)(v w:☈)(P:☊), w⍀☑G☑★P->G⊣ 〈v♇☍χ(w)@☋P〉->G⊣P.
 Proof.
  intros G v w P H H0; apply (bifore2 G v P); apply (bifori2 G v w P H H0).
 Qed.

(* Derived ∃ rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem biexsi:forall (G:☐)(v:☈)(E:☌)(P:☊), G⊣ 〈v♇☍E@☋P〉->G⊣ ☬☣(v∙P).
 Proof.
  intros G v E P H; apply existcbexp; exists E; apply H.
 Qed.

(* Alternate ♂ rules (reconstruction of B-Book rules) ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem biproi2:forall (G:☐)(E F S T:☌), G⊣E∈S->G⊣F∈T->G⊣(E☵F)∈(S♂T).
 Proof.
  intros G E F S T H H0; destruct (exfgnewf (G☑★(E☵F)∈(S♂T))); destruct (ginfinc _ _ _ g);
   clear g; destruct (exfgnewf (G☑★(E☵F)∈(S♂T)☑★χ(x)∈χ(x))); destruct (ginfinc _ _ _ g);
   destruct (ginfinc _ _ _ g1); invset_clear p0; invset_clear H1; clear g g1 H2;
   apply (biproi G _ _ _ _ _ p1 H3 p).
  invset_clear p; invset_clear p1; invset_clear H1; invset_clear H2; invset_clear H4;
   invset_clear H5.
  apply existcbexp; exists E; unfold fpbdexs; unfold pexs; unfold fpaffexp;
   fold fpaffexp; unfold feuplft; rewrite commfpaffbdloc;
   fold (pexs (fpbdloc x ☦ 〈x0♇☍E@☋χ(x0)∈S∧χ(x)∈T∧(E☵F)♃(χ(x0)☵χ(x))〉));
   fold (fpbdexs x 〈x0♇☍E@☋χ(x0)∈S∧χ(x)∈T∧(E☵F)♃(χ(x0)☵χ(x))〉).
   apply existcbexp; exists F; simpl; rewrite reflfiequ; rewrite (iequrf _ _ H3); simpl;
    rewrite reflfiequ; rewrite (feaffexpnul _ F _ H6); rewrite (feaffexpnul _ E _ H4);
    rewrite (feaffexpnul _ F _ H1); rewrite (feaffexpnul _ E _ H10);
    rewrite (feaffexpnul _ F _ H8); rewrite (feaffexpnul _ E _ H2);
    rewrite (feaffexpnul _ F _ H6); rewrite (feaffexpnul _ E _ H9);
    rewrite (feaffexpnul _ F _ H7).
   apply biandi; [apply H | apply biandi; [apply H0 | apply biequi]].
   apply gefst.
   apply gefst.
   apply H3.
   apply H6.
 Qed.

(* Alternate ♀ rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem bicmpi2:forall (G:☐)(f:☈)(P:☊)(E S:☌), G⊣ ☭☤(☬☤(f∙S∙P)←E⊢fbc f S P)->G⊣E∈ ☬☤(f∙S∙P).
 Proof.
  intros G f P E S; rewrite fpaffcmpdec; intro H; apply bicmpi;
   [apply biandl with (Q:=〈f♇☍E@☋P〉) | apply biandr with (P:=E∈S)]; apply H.
 Qed.

 Theorem bicmpe:forall (G:☐)(f:☈)(P:☊)(E S:☌), G⊣E∈ ☬☤(f∙S∙P)-> G⊣E∈S∧ 〈f♇☍E@☋P〉.
 Proof.
  intros G f P E S H; apply biandi;
   [apply bicmpl with (f:=f)(P:=P) | apply bicmpr with (S:=S)]; apply H.
 Qed.

(* Derived ♃ rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem symequ:forall (G:☐)(E F:☌), G⊣E♃F->G⊣F♃E.
 Proof.
  intros G E F H; assert (H0:{f:☈ & f⍀☍E}). exists ☰☍(E); apply fenewfinf.
  destruct H0; generalize (bieque G x (χ x♃E) E F H); intros H1; unfold fpaffexp in H1;
   fold feaffexp in H1; rewrite <- (sym_id (feaffexpnul _ E _ e)) in H1;
   rewrite <- (sym_id (feaffexpnul _ F _ e)) in H1; rewrite <- (sym_id (reflfiequ x)) in H1;
   apply H1; apply biequi.
 Qed.

(* Pseudo-ground terms ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* The various side conditions regarding freeness for the ∀ rules indicates that the   *)
(* main question is to know wether a sequent has common free variables appearing on      *)
(* both sides of the ⊣.                                                                                                                      *)

 Theorem truenffpbdfor:forall (G:☐)(P:☊)(i:☈), (i⍀☑G)+(i⍀☋P)->G⊣P->G⊣ ☬☢(i∙P).
 Proof.
  intros G P i H H0; destruct H; [apply (bifori _ _ _ g H0) | apply (bifori3 _ _ _ p H0)].
 Qed.

 Theorem truenfcatchfor:forall (G:☐)(P:☊)(i:☈), (i⍀☑G)+(i⍀☋P)->G⊣P->G⊣fpcatchfor P i.
 Proof.
  intros G P i H H0; unfold fpcatchfor; destruct (i⍀☋☂P);
   [apply H0 | apply (truenffpbdfor _ _ _ H H0)].
 Qed.

 Theorem pseudocatchforall:forall (i:☈)(G:☐)(P:☊),
                                                   (forall (d:☈), d≤☉i->(d⍀☑G)+(d⍀☋P))->G⊣P->G⊣fpcatchforall P i.
 Proof.
  induction i; intros G P H H0; simpl; unfold fpcatchforall; fold fpcatchforall.
   apply truenfcatchfor; [apply (H _ (gefst ☦)) | apply H0].
   unfold fpcatchfor; fold fpcatchfor; destruct (decpinf ☠i P).
    rewrite (pinfpt _ _ p); apply IHi;
     [intros d H1; apply H; apply (nxtrige _ _ H1) | apply H0].
    rewrite (pinfrf _ _ e); apply IHi.
     intros d H1; destruct (H d (nxtrige _ _ H1));
      [left; apply g | right; apply fpbdforinf; apply p].
     destruct (H _ (reflige ☠i)); [apply (bifori _ _ _ g H0) | destruct (e p)].
 Qed.

 Theorem pseudoground:forall (G:☐)(P:☊), (forall i:☈, (i⍀☑G)+(i⍀☋P))->G⊣P->G⊣ ☰☢(P).
 Proof.
  intros G P H H0; unfold fpgroundfor; apply pseudocatchforall;
   [intros d H1; apply H | apply H0].
 Qed.

(* Alternate ☬ rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem bipowi2:forall (G:☐)(S T:☌), (forall (i:☈), G⊣χ(i)∈S☲χ(i)∈T)->G⊣S∈ ☬T.
 Proof.
  intros G S T H; destruct (exfgnewf (∅☑☑★S♃T)); destruct (ginfinc _ _ _ g); clear g g0;
   invset_clear p.
  apply (bipowi G _ _ _ H0 H1); apply forallcb.
  intros w; simpl; rewrite (reflfiequ x); rewrite (feaffexpnul _ χ(w) _ H0);
   rewrite (feaffexpnul _ χ(w) _ H1); apply H.
 Qed.

 Theorem bipowiexp:forall (G:☐)(S T:☌), (forall (E:☌), G⊣E∈S☲E∈T)->G⊣S∈ ☬T.
 Proof.
  intros G S T H; apply bipowi2; intros i; apply H.
 Qed.

 Theorem bipoweexp:forall (G:☐)(S T:☌), (G⊣S∈ ☬T)->forall (E:☌), G⊣E∈S☲E∈T.
 Proof.
  intros G S T H E; destruct (exfgnewf (∅☑☑★S♃T)); destruct (ginfinc _ _ _ g); clear g g0;
   invset_clear p.
  replace (E ∈ S☲E ∈ T) with 〈x♇☍E@☋χ(x) ∈ S☲χ(x) ∈ T〉.
   apply forallbcexp; apply (bipowe G _ _ _ H0 H1 H).
   simpl; rewrite (reflfiequ x); rewrite (feaffexpnul _ E _ H0);
    rewrite (feaffexpnul _ E _ H1); apply refl_equal.
 Qed.

 Theorem bipowe2:forall (G:☐)(S T:☌), (G⊣S∈ ☬T)->forall (i:☈), G⊣χ(i)∈S☲χ(i)∈T.
 Proof.
  intros G S T H i; apply bipoweexp; apply H.
 Qed.

(* Alternate ☭-rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem bichsiexp:forall (G:☐)(S:☌), {E:☌ & G⊣E∈S}->G⊣(☭S)∈S.
 Proof.
  intros G S H; destruct H; rename x into E; assert (H0:{x:☈ & x⍀☍S}).
   exists ☰☍(S); apply fenewfinf.
  destruct H0; apply (bichsi G _ _ e); apply existcbexp; exists E.
  simpl; rewrite (reflfiequ x); rewrite (feaffexpnul _ E _ e); apply b.
 Qed.

 Theorem bichsi2:forall (G:☐)(S:☌), {x:☈ & G⊣χ(x)∈S}->G⊣(☭S) ∈ S.
 Proof.
   intros G S H; destruct H; apply bichsiexp; exists χ(x); apply b.
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
