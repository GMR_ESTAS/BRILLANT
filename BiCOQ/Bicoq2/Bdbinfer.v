(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Bdbinfer, De Bruijn inference rules
   The B logic, as described in Binfer, is described using functional notations; in this
   module derived inference rules are provided, expressed with structural notations.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - October 2006
   ♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Biffres.

(* De Bruijn ∀ rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem dbfori:forall (G:☐)(v:☈)(P:☊), v⍀☑(G ☑★ ∀P)->G⊣ ☭☢(∀P←χ v⊢pif P)->G⊣ ∀P.
 Proof.
  intros G v P H H0; destruct (ginfinc _ _ _ H); rewrite (sym_id (fpinforbd _ _ p));
   apply (bifori _ _ _ g H0).
 Qed.
 (* Nota : the condition v⍀☑(G ☑★ ∀P), instead of v⍀☑G, is truly a semantic one as shown by     *)
 (* the example v⍀☑G, G⊣☭☢(∀χ☦♃χv←χv) - reducing to G⊣χv♃χv - yet G⊣∀(χ☦♃χv) is refutable.   *)

 Theorem dbfore:forall (G:☐)(P:☊)(E:☌), G⊣ ∀P->G⊣ ☭☢(∀P←E⊢pif P).
 Proof.
  intros G P E H; destruct (exfgnewf (G☑★∀P)); destruct (ginfinc _ _ _ g); invset p;
   clear g H0 H1 P0 i; rewrite <- (fpinforbd _ _ p) in H; unfold fpinfor; simpl.
  generalize (bifore _ _ _ E H); clear H; intros H;
   rewrite <- (fpafffordec ☭☢(∀P←χ(x)⊢pif P) E x) in H; unfold fpbdfor in H;
   unfold fpinfor in H; simpl in H; rewrite <- (sym_id (fpinlocbd _ _ _ (gefst _) H2)) in H;
   apply H.
 Qed.

(* Alternate ♀-intro rule ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem dbcmpi:forall (G:☐)(E S:☌)(P:☊), G⊣E∈S->G⊣ ☭☢(∀P←E⊢pif P)->G⊣E∈(S♀P).
 Proof.
  intros G E S P H H0; destruct (exfgnewf (G☑★E∈(S♀P))); destruct (ginfinc _ _ _ g);
   invset_clear p; invset_clear H2; clear g.
  rewrite (fpbdcmpinv _ _ _ H3 H4); apply bicmpi.
   apply H.
   replace ☭☢(∀P←E⊢pif P) with ☭☢( ☬☢(x∙☮☋(〈☦♇☍χ(☠x)@☋P〉)) ←E⊢fbf x ☮☋(〈☦♇☍χ(☠x)@☋P〉)) in H0.
    rewrite <- (sym_id (fpafffordec ☮☋(〈☦♇☍χ(☠x)@☋P〉) E x)) in H0; apply H0.
    unfold fpbdfor; unfold fpinfor; unfold fpdnlft; simpl;
     rewrite (fpbdlocinv _ _ _ (gefst _) H4); apply refl_equal.
 Qed.

 Theorem dbcmpl:forall (G:☐)(E S:☌)(P:☊), G⊣E∈(S♀P)->G⊣E∈S.
 Proof.
  intros G E S P H; assert ({x:☈ & x⍀☋E∈(S♀P)}). exists ☰☋(E∈(S♀P)); apply fpnewfinf.
  destruct H0; invset_clear p; invset_clear H1.
  rewrite <- (sym_id (fpbdcmpinv _ _ _ H2 H3)) in H.
  apply bicmpl with (f:=x)(P:=☮☋(〈☦ ♇☍ χ(☠x) @☋ P 〉)); apply H.
 Qed.

 Theorem dbcmpr:forall (G:☐)(E S:☌)(P:☊), G⊣E∈(S♀P)->G⊣ ☭☢(∀P←E⊢pif P).
 Proof.
  intros G E S P H; assert ({x:☈ & x⍀☋E∈(S♀P)}). exists ☰☋(E∈(S♀P)); apply fpnewfinf.
  destruct H0; invset_clear p; invset_clear H1.
  replace ☭☢(∀P←E⊢pif P) with ☭☢( ☬☢(x∙☮☋(〈☦♇☍χ(☠x)@☋P〉)) ←E⊢fbf x ☮☋(〈☦♇☍χ(☠x)@☋P〉)).
   rewrite (fpafffordec ☮☋(〈☦♇☍χ(☠x)@☋P〉) E x); rewrite <- (sym_id (fpbdcmpinv _ _ _ H2 H3)) in H;
    apply bicmpr with (S:=S); apply H.
   unfold fpbdfor; unfold fpinfor; unfold fpdnlft; simpl;
    rewrite (fpbdlocinv _ _ _ (gefst _) H3); apply refl_equal.
 Qed.

(* Replacement rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem forimprepfor:forall (G:☐)(P P':☊), G⊣ ∀(P☲P')->G⊣ ∀P->G⊣ ∀P'.
 Proof.
  intros G P P' H H0; destruct (exfgnewf (G☑★∀P')); apply (dbfori _ x _ g); unfold fpinfor;
   apply mp with (P:=fpinloc ☦ (χ x) P);
   [apply (dbfore G P (χ x) H0) | apply (dbfore G (P☲P') (χ x) H)].
 Qed.

 Theorem forimprepcmp:forall (G:☐)(S:☌)(P P':☊),
                                         G⊣ ∀(P☲P')->G⊣(S♀P) ⊆ (S♀P').
 Proof.
  intros G S P P' H; destruct (exfgnewf (G☑★(∀P)☲(∀P')☑★(S♀P)∈(S♀P'))); destruct (ginfinc _ _ _ g);
   clear g; destruct (ginfinc _ _ _ g0); invset_clear p.
  unfold pinc; apply (bipowi G _ _ _ H0 H1); apply bifori; [apply g | idtac].
  apply biimpi; apply dbcmpi.
   apply dbcmpl with (P:=P); BTprop.
   apply mp with (P:=☭☢(∀P←χ(x)⊢pif P)).
    apply dbcmpr with (S:=S); BTprop.
    apply biins; replace (☭☢(∀P←χ(x)⊢pif P)☲ ☭☢(∀P'←χ(x)⊢pif P')) with ☭☢(∀(P☲P')←χ(x)⊢pif (P☲P')).
     apply dbfore; apply H.
     unfold fpinfor; simpl; apply refl_equal.
 Qed.

(* ∀(☳)-conversion into ∀(☲) ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem foriffimp:forall (G:☐)(P P':☊), G⊣ ∀(P☳P')->G⊣ ∀(P☲P').
 Proof.
  intros G P P' H; destruct (exfgnewf (G☑★∀(P☲P'))); apply (dbfori _ _ _ g); simpl;
   apply (diffimp _ _ _ (dbfore _ _ (χ x) H)).
 Qed.

(* ∀(☳)-symmetry ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem foriffsym:forall (G:☐)(P P':☊), G⊣ ∀(P☳P')->G⊣ ∀(P'☳P).
 Proof.
  intros G P P' H; destruct (exfgnewf (G☑★∀(P'☳P))); apply (dbfori _ _ _ g); simpl;
   apply (symiff _ _ _ (dbfore _ _ (χ x) H)).
 Qed.

(* ∀(☳)-replacement rules ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem foriffrepifffor:forall (G:☐)(P P':☊), G⊣ ∀(P☳P')->G⊣(∀P) ☳ (∀P').
 Proof.
  intros G P P' H; BTprop.
   apply forimprepfor with (P:=P); [apply foriffimp; apply biins; apply H | BTprop].
   apply forimprepfor with (P:=P');
    [apply foriffimp; apply biins; apply (foriffsym _ _ _ H) | BTprop].
 Qed.

 Theorem foriffrepeqs:forall (G:☐)(S:☌)(P P':☊), G⊣ ∀(P☳P')->G⊣(S♀P)♃(S♀P').
 Proof.
  intros G S P P' H; apply bieqsi.
   apply (forimprepcmp _ S _ _ (foriffimp _ _ _ H)).
   apply (forimprepcmp _ S _ _ (foriffimp _ _ _ (foriffsym _ _ _ H))).
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
