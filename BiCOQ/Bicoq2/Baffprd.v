(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Baffprd, generalised replacement rules for predicates
   In this module, a first version of generic predicate replacement rule is provided,
   based on the use of the 〈_♇☋_@_〉 functions.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : June 2006 - October 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Bdbinfer.

(* Predicate affectation function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* The meta-variable predicates can be replaced by a predicate using this function.     *)

 Fixpoint fpaffprd(v:☈)(Q P:☊){struct P}:☊:=
  match P with
  | ¬P' => ¬ (fpaffprd v Q P')
  | ∀P' => ∀ (fpaffprd v ☯☋(Q) P')
  | P1∧P2 => (fpaffprd v Q P1) ∧ (fpaffprd v Q P2)
  | P1☲P2 => (fpaffprd v Q P1) ☲ (fpaffprd v Q P2)
  | π i => match (v=☉☂i) with ⊤ => Q | ⊥ => π i end
  | E1♃E2 => (feaffprd v Q E1) ♃ (feaffprd v Q E2)
  | E1∈E2 => (feaffprd v Q E1) ∈ (feaffprd v Q E2)
  end
 with feaffprd(v:☈)(Q:☊)(E:☌){struct E}:☌:=
  match E with
  | ♄ => ♄
  | ☭E' => ☭ (feaffprd v Q E')
  | ☬E' => ☬ (feaffprd v Q E')
  | (E1☵E2) => ((feaffprd v Q E1) ☵ (feaffprd v Q E2))
  | (E1♂E2) => ((feaffprd v Q E1) ♂ (feaffprd v Q E2))
  | ω i => ω i
  | χ i => χ i
  | E' ♀ P' => (feaffprd v Q E') ♀ (fpaffprd v ☯☋(Q) P')
  end.
 Notation "'〈' v '♇☋' Q '@☋' P '〉'":=(fpaffprd v Q P).
 Notation "'〈' v '♇☋' Q '@☍' E '〉'":=(feaffprd v Q E).

 (* 〈♇☋@〉 to the meta-variable itself has no effect *)
 Theorem ftaffprdid:(forall (P:☊)(m:☈), 〈m♇☋π(m)@☋P〉≡P)*(forall (E:☌)(m:☈), 〈m♇☋π(m)@☍E〉≡E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (m:☈), 〈m♇☋π(m)@☋P〉≡P) (fun E:☌=>forall (m:☈), 〈m♇☋π(m)@☍E〉≡E));
   simpl.
   intros m; apply refl_id.
   intros i; split; [split | idtac]; intros m; try (apply refl_id); destruct (deciequ m i);
    [rewrite (iequpt _ _ i0); rewrite i0 | rewrite (iequrf _ _ e)]; apply refl_id.
   intros P HP; split; intros m; [idtac | unfold fpuplft; simpl]; rewrite HP; apply refl_id.
   intros E HE; split; intros m; rewrite HE; apply refl_id.
   intros P1 P2 H1 H2; split; intros m; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros m; rewrite H1; rewrite H2; apply refl_id.
   intros E P H1 H2 m; rewrite H1; unfold fpuplft; simpl; rewrite H2; apply refl_id.
 Qed.

 Definition fpaffprdid:=fst ftaffprdid.
 Definition feaffprdid:=snd ftaffprdid.

 (* 〈♇☋@〉 on a non pi-dependant term has no effect *)
 Theorem ftaffprdnulf:(forall (P Q:☊)(m:☈), m⍀π☋P->〈m♇☋Q@☋P〉≡P)*
                                         (forall (E:☌)(Q:☊)(m:☈), m⍀π☍E->〈m♇☋Q@☍E〉≡E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (Q:☊)(m:☈), m⍀π☋P->〈m♇☋Q@☋P〉≡P)
                             (fun E:☌=>forall (Q:☊)(m:☈), m⍀π☍E->〈m♇☋Q@☍E〉≡E)); simpl.
   intros Q m Hm; apply refl_id.
   intros i; split; [split | idtac]; intros Q m Hm; try (apply refl_id); invset Hm;
    rewrite (iequrf _ _ H1); apply refl_id.
   intros P HP; split; intros Q m Hm; invset_clear Hm;
    [rewrite (HP Q _ H) | rewrite (HP ☯☋(Q) _ H)]; apply refl_id.
   intros E HE; split; intros Q m Hm; invset_clear Hm; rewrite (HE Q _ H); apply refl_id.
   intros P1 P2 H1 H2; split; intros Q m Hm; invset_clear Hm; rewrite (H1 Q _ H);
    rewrite (H2 Q _ H0); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros Q m Hm; invset_clear Hm; rewrite (H1 Q _ H);
    rewrite (H2 Q _ H0); apply refl_id.
   intros E P H1 H2 Q m Hm; invset_clear Hm; rewrite (H1 Q _ H); rewrite (H2 ☯☋(Q) _ H0);
    apply refl_id.
 Qed.

 Definition fpaffprdnulf:=fst ftaffprdnulf.
 Definition feaffprdnulf:=snd ftaffprdnulf.

(* Commutation lemma ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem ftaffprdfpuploc:(forall (P Q:☊)(p d:☈),
                                                〈p♇☋fpuploc d Q@☋fpuploc d P〉≡fpuploc d 〈p♇☋Q@☋P〉)*
                                               (forall (E:☌)(Q:☊)(p d:☈),
                                                〈p♇☋fpuploc d Q@☍feuploc d E〉≡feuploc d 〈p♇☋Q@☍E〉).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (Q:☊)(p d:☈),
                                              〈p♇☋fpuploc d Q@☋fpuploc d P〉≡fpuploc d 〈p♇☋Q@☋P〉)
                             (fun E:☌=>forall (Q:☊)(p d:☈),
                                              〈p♇☋fpuploc d Q@☍feuploc d E〉≡feuploc d 〈p♇☋Q@☍E〉)); simpl.
   intros Q p d; apply refl_id.
   intros i; split; [split | idtac]; intros Q p d.
    destruct (deciequ p i); [rewrite (iequpt _ _ i0) | rewrite (iequrf _ _ e)];  apply refl_id.
    apply refl_id.
    destruct (decige d i); [rewrite (igept _ _ i0) | rewrite (igerf _ _ e)]; apply refl_id.
   intros P HP; split; intros Q p d;
    [rewrite (HP Q p d)
    | unfold fpuplft; rewrite (fpuplocdbl Q _ _ (gefst d)); rewrite (HP (fpuploc ☦ Q) p ☠d)];
    apply refl_id.
   intros E HE; split; intros Q p d; rewrite (HE Q p d); apply refl_id.
   intros P1 P2 H1 H2; split; intros Q p d; rewrite (H1 Q p d); rewrite (H2 Q p d);
    apply refl_id.
   intros E1 E2 H1 H2; split; split; intros Q p d; rewrite (H1 Q p d); rewrite (H2 Q p d);
    apply refl_id.
   intros E P H1 H2 Q p d; rewrite (H1 Q p d); unfold fpuplft;
    rewrite (fpuplocdbl Q _ _ (gefst d)); rewrite (H2 (fpuploc ☦ Q) p ☠d); apply refl_id.
 Qed.

 Definition fpaffprdfpuploc:=fst ftaffprdfpuploc.
 Definition feaffprdfpuploc:=snd ftaffprdfpuploc.

 Theorem ftaffprdftinloc:(forall (P Q:☊)(F:☌)(p v:☈),
                                                p⍀π☍F->〈p♇☋Q@☋fpinloc v F P〉≡fpinloc v F 〈p♇☋fpuploc v Q@☋P〉)*
                                               (forall (E:☌)(Q:☊)(F:☌)(p v:☈),
                                                p⍀π☍F->〈p♇☋Q@☍feinloc v F E〉≡feinloc v F 〈p♇☋fpuploc v Q@☍E〉).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (Q:☊)(F:☌)(p v:☈),
                                              p⍀π☍F->〈p♇☋Q@☋fpinloc v F P〉≡fpinloc v F 〈p♇☋fpuploc v Q@☋P〉)
                             (fun E:☌=>forall (Q:☊)(F:☌)(p v:☈),
                                              p⍀π☍F->〈p♇☋Q@☍feinloc v F E〉≡feinloc v F 〈p♇☋fpuploc v Q@☍E〉));
   simpl.
   intros Q F p v Hp; apply refl_id.
   intros i; split; [split | idtac]; intros Q F p v HP.
    destruct (deciequ p i).
     rewrite (iequpt _ _ i0); simpl;
      rewrite (fpinlocfpdnloc (fpuploc v Q) F v (fpuplocinfdep Q v));
      rewrite (fpdnlocuploc Q v); apply refl_id.
     rewrite (iequrf _ _ e); apply refl_id.
     apply refl_id.
     destruct (decige v i).
      rewrite (igept _ _ i0); destruct (deciequ v i).
       rewrite (iequpt _ _ i1); apply (feaffprdnulf F Q p HP).
       rewrite (iequrf _ _ e); apply refl_id.
      rewrite (igerf _ _ e); apply refl_id.
   intros P HP; split; intros Q F p v Hp.
    rewrite (HP Q F p v Hp); apply refl_id.
    unfold fpuplft; rewrite (fpuplocdbl Q _ _ (gefst v)); assert (Hp':p⍀π☍☯☍(F)).
     apply enpdtp; unfold feuplft; rewrite (feuplocfenpd F ☦ p); apply (enpdpt _ _ Hp).
    rewrite (HP (fpuploc ☦ Q) ☯☍(F) p ☠v Hp'); apply refl_id.
   intros E HE; split; intros Q F p v Hp; rewrite (HE Q F p v Hp); apply refl_id.
   intros P1 P2 H1 H2; split; intros Q F p v Hp; rewrite (H1 Q F p v Hp); rewrite (H2 Q F p v Hp);
    apply refl_id.
   intros E1 E2 H1 H2; split; split; intros Q F p v Hp; rewrite (H1 Q F p v Hp);
    rewrite (H2 Q F p v Hp); apply refl_id.
   intros E P H1 H2 Q F p v Hp; rewrite (H1 Q F p v Hp); unfold fpuplft;
    rewrite (fpuplocdbl Q _ _ (gefst v)); assert (Hp':p⍀π☍☯☍(F)).
    apply enpdtp; unfold feuplft; rewrite (feuplocfenpd F ☦ p); apply (enpdpt _ _ Hp).
   rewrite (H2 (fpuploc ☦ Q) ☯☍(F) p ☠v Hp'); apply refl_id.
 Qed.

 Definition fpaffprdfpinloc:=fst ftaffprdftinloc.
 Definition feaffprdfeinloc:=snd ftaffprdftinloc.

 Theorem ftaffprdftbdloc:(forall (P Q:☊)(p v d:☈),
                                                v⍀☋Q->〈p♇☋fpuploc d Q@☋fpbdloc v d P〉≡fpbdloc v d 〈p♇☋Q@☋P〉)*
                                               (forall (E:☌)(Q:☊)(p v d:☈),
                                                v⍀☋Q->〈p♇☋fpuploc d Q@☍febdloc v d E〉≡febdloc v d 〈p♇☋Q@☍E〉).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (Q:☊)(p v d:☈),
                                              v⍀☋Q->〈p♇☋fpuploc d Q@☋fpbdloc v d P〉≡fpbdloc v d 〈p♇☋Q@☋P〉)
                             (fun E:☌=>forall (Q:☊)(p v d:☈),
                                              v⍀☋Q->〈p♇☋fpuploc d Q@☍febdloc v d E〉≡febdloc v d 〈p♇☋Q@☍E〉));
   simpl.
   intros Q p v d HF; apply refl_id.
   intros i; split; [split | idtac]; intros Q p v d HF.
    destruct (deciequ p i).
     rewrite (iequpt _ _ i0); simpl; rewrite (fpbdlocnul _ _ d HF); apply refl_id.
     rewrite (iequrf _ _ e); apply refl_id.
    apply refl_id.
    destruct (decige d i).
     rewrite (igept _ _ i0); destruct (v=☉☂i); apply refl_id.
     rewrite (igerf _ _ e); apply refl_id.
   intros P HP; split; intros Q p v d HF.
    rewrite (HP Q p v d HF); apply refl_id.
    unfold fpuplft; rewrite (fpuplocdbl Q _ _ (gefst d)); 
     rewrite (HP (fpuploc ☦ Q) p ☠v ☠d (fpuplocinf _ _ _ (gefst v) HF)); apply refl_id.
   intros E HE; split; intros Q p v d HF; rewrite (HE Q p v d HF); apply refl_id.
   intros P1 P2 H1 H2; split; intros Q p v d HF; rewrite (H1 Q p v d HF); rewrite (H2 Q p v d HF);
    apply refl_id.
   intros E1 E2 H1 H2; split; split; intros Q p v d HF; rewrite (H1 Q p v d HF);
    rewrite (H2 Q p v d HF); apply refl_id.
   intros E P H1 H2 Q p v d HF; rewrite (H1 Q p v d HF); unfold fpuplft;
    rewrite (fpuplocdbl Q _ _ (gefst d));
    rewrite (H2 (fpuploc ☦ Q) p ☠v ☠d (fpuplocinf _ _ _ (gefst v) HF)); apply refl_id.
 Qed.

 Definition fpaffprdfpbdloc:=fst ftaffprdftbdloc.
 Definition feaffprdfebdloc:=snd ftaffprdftbdloc.

 Theorem fpaffprdfpbdfor:forall (Q P:☊)(p i:☈), i⍀☋P->〈p♇☋P@☋☬☢(i∙Q)〉≡☬☢(i∙〈p♇☋P@☋Q〉).
 Proof.
  intros Q P p i H; unfold fpbdfor; simpl; unfold fpuplft;
   rewrite (fpaffprdfpbdloc Q _ p _ ☦ H); apply refl_id.
 Qed.

 Theorem fpaffprdfpbdexs:forall (Q P:☊)(p i:☈), i⍀☋P->〈p♇☋P@☋☬☣(i∙Q)〉≡☬☣(i∙〈p♇☋P@☋Q〉).
 Proof.
  intros Q P p i H; unfold fpbdexs; unfold pexs; simpl; unfold fpuplft;
   rewrite (fpaffprdfpbdloc Q _ p _ ☦ H); apply refl_id.
 Qed.

 Theorem feaffprdfpbdcmp:forall (Q P:☊)(E:☌)(p i:☈),
                                               i⍀☋P->〈p♇☋P@☍☬☤(i∙E∙Q)〉≡☬☤(i∙〈p♇☋P@☍E〉∙〈p♇☋P@☋Q〉).
 Proof.
  intros Q P E p i H; unfold fpbdcmp; simpl; unfold fpuplft;
   rewrite (fpaffprdfpbdloc Q _ p _ ☦ H); apply refl_id.
 Qed.

 Theorem ftaffprdftaffexp:(forall (P Q:☊)(F:☌)(v p:☈), fenpd p F≡⊤->v⍀☋☂Q≡⊤->
                                                  〈v♇☍F@☋〈p♇☋Q@☋P〉〉≡〈p♇☋Q@☋〈v♇☍F@☋P〉〉)*
                                                 (forall (E:☌)(Q:☊)(F:☌)(v p:☈), fenpd p F≡⊤->v⍀☋☂Q≡⊤->
                                                  〈v♇☍F@☍〈p♇☋Q@☍E〉〉≡〈p♇☋Q@☍〈v♇☍F@☍E〉〉).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (Q:☊)(F:☌)(v p:☈), fenpd p F≡⊤->v⍀☋☂Q≡⊤->
                                              〈v♇☍F@☋〈p♇☋Q@☋P〉〉≡〈p♇☋Q@☋〈v♇☍F@☋P〉〉)
                             (fun E:☌=>forall (Q:☊)(F:☌)(v p:☈), fenpd p F≡⊤->v⍀☋☂Q≡⊤->
                                              〈v♇☍F@☍〈p♇☋Q@☍E〉〉≡〈p♇☋Q@☍〈v♇☍F@☍E〉〉)); simpl.
   intros Q F v p Hp HQ; apply refl_id.
   intros i; split; [split | idtac]; intros Q F v p Hp HQ.
    destruct (deciequ p i).
     rewrite (iequpt _ _ i0); apply (fpaffexpnulf _ F _ HQ).
     rewrite (iequrf _ _ e); apply refl_id.
    apply refl_id.
    destruct (deciequ v i).
     rewrite (iequpt _ _ i0); rewrite (feaffprdnulf F Q p (enpdtp _ _ Hp)); apply refl_id.
     rewrite (iequrf _ _ e); apply refl_id.
   intros P HP; split; intros Q F v p Hp HQ.
    rewrite (HP Q F v p Hp HQ); apply refl_id.
    assert (Hp':fenpd p ☯☍(F)≡⊤). rewrite (feuplftfenpd F p); apply Hp.
    assert (HQ':☠v⍀☋☂☯☋(Q)≡⊤). rewrite (fpuplftidx Q v); apply HQ.
    rewrite (HP ☯☋(Q) ☯☍(F) ☠v p Hp' HQ'); apply refl_id.
   intros E HE; split; intros Q F v p Hp HQ; rewrite (HE Q F v p Hp HQ); apply refl_id.
   intros P1 P2 H1 H2; split; intros Q F v p Hp HQ; rewrite (H1 Q F v p Hp HQ);
    rewrite (H2 Q F v p Hp HQ); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros Q F v p Hp HQ; rewrite (H1 Q F v p Hp HQ);
    rewrite (H2 Q F v p Hp HQ); apply refl_id.
   intros E P H1 H2 Q F v p Hp HQ; rewrite (H1 Q F v p Hp HQ);
    assert (Hp':fenpd p ☯☍(F)≡⊤). rewrite (feuplftfenpd F p); apply Hp.
    assert (HQ':☠v⍀☋☂☯☋(Q)≡⊤). rewrite (fpuplftidx Q v); apply HQ.
    rewrite (H2 ☯☋(Q) ☯☍(F) ☠v p Hp' HQ'); apply refl_id.
 Qed.

 Definition fpaffprdfpaffexp:=fst ftaffprdftaffexp.
 Definition feaffprdfeaffexp:=snd ftaffprdftaffexp.

 Theorem ftaffprdftbdloc':(forall (P Q:☊)(p v d:☈),
                                                 〈p♇☋fpbdloc v d Q@☋fpbdloc v d P〉≡fpbdloc v d 〈p♇☋Q@☋P〉)*
                                                (forall (E:☌)(Q:☊)(p v d:☈),
                                                 〈p♇☋fpbdloc v d Q@☍febdloc v d E〉≡febdloc v d 〈p♇☋Q@☍E〉).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (Q:☊)(p v d:☈),
                                              〈p♇☋fpbdloc v d Q@☋fpbdloc v d P〉≡fpbdloc v d 〈p♇☋Q@☋P〉)
                             (fun E:☌=>forall (Q:☊)(p v d:☈),
                                              〈p♇☋fpbdloc v d Q@☍febdloc v d E〉≡febdloc v d 〈p♇☋Q@☍E〉));
   simpl.
   intros Q p v d; apply refl_id.
   intros i; split; [split | idtac]; intros Q p v d; try (apply refl_id).
    destruct (p=☉☂i); apply refl_id.
    destruct (d≤☉☂i); [destruct (v=☉☂i) | idtac]; apply refl_id.
   intros P HP; split; intros Q p v d.
    rewrite HP; apply refl_id.
    unfold fpuplft; rewrite (sym_id (fpbdlocfpuploc Q _ _ v (gefst d))); rewrite HP;
     apply refl_id.
   intros E HE; split; intros Q p v d; rewrite HE; apply refl_id.
   intros P1 P2 H1 H2; split; intros Q p v d; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros Q p v d; rewrite H1; rewrite H2; apply refl_id.
   intros E P HE HP Q p v d; rewrite HE; unfold fpuplft;
    rewrite (sym_id (fpbdlocfpuploc Q _ _ v (gefst d))); rewrite HP; apply refl_id.
 Qed.

 Definition fpaffprdfpbdloc':=fst ftaffprdftbdloc'.
 Definition feaffprdfebdloc':=snd ftaffprdftbdloc'.

(* Predicate replacement scheme ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem iffftaffprd:(forall (Q P P':☊)(G:☐)(x:☈), G⊣P☳P'->G⊣ 〈x♇☋P@☋Q〉☳〈x♇☋P'@☋Q〉)*
                                       (forall (E:☌)(P P':☊)(G:☐)(x:☈), G⊣P☳P'->G⊣ 〈x♇☋P@☍E〉♃ 〈x♇☋P'@☍E〉).
 Proof.
  apply (Bdt_ind (fun Q:☊=>forall (P P':☊)(G:☐)(x:☈), G⊣P☳P'->G⊣ 〈x♇☋P@☋Q〉☳〈x♇☋P'@☋Q〉)
                             (fun E:☌=>forall (P P':☊)(G:☐)(x:☈), G⊣P☳P'->G⊣ 〈x♇☋P@☍E〉♃ 〈x♇☋P'@☍E〉)).
   destruct s; simpl; intros HP HE P P' G x Hiff.
    (* Negation *)
    apply iffcontrapp; apply HP; [unfold igt; apply reflige | apply Hiff].
    (* Universal quantification *)
    apply foriffrepifffor; destruct (exfgnewf (G☑★(∀〈x♇☋☯☋(P)@☋s〉☳〈x♇☋☯☋(P')@☋s〉)));
     apply (dbfori _ _ _ g); unfold fpinfor; unfold fpprjfor;
     replace (fpinloc ☦ χ(x0) (〈x♇☋☯☋(P)@☋s〉☳〈x♇☋☯☋(P')@☋s〉)) with
                    ((fpinloc ☦ χ(x0) 〈x♇☋☯☋(P)@☋s〉)☳(fpinloc ☦ χ(x0) 〈x♇☋☯☋(P')@☋s〉)).
      unfold fpuplft; rewrite (sym_id (fpaffprdfpinloc s P χ(x0) x ☦ (npdvar x x0))).
      rewrite (sym_id (fpaffprdfpinloc s P' χ(x0) x ☦ (npdvar x x0))); apply HP.
       rewrite (fpinlocdpeq s ☦ x0); unfold igt; apply reflige.
       apply Hiff.
      apply refl_equal.
    (* Conjonction *)
    BTprop.
     apply biins; apply biimpe; apply diffimp; apply HP;
      [unfold igt; apply genxt; apply fimaxgel | apply Hiff].
     apply biimpe; apply biins; apply diffimp; apply HP;
      [unfold igt; apply genxt; apply fimaxger | apply Hiff].
     apply biins; apply biimpe; apply riffimp; apply HP;
      [unfold igt; apply genxt; apply fimaxgel | apply Hiff].
     apply biimpe; apply biins; apply riffimp; apply HP;
      [unfold igt; apply genxt; apply fimaxger | apply Hiff].
    (* Implication *)
    BTprop.
     apply biimpe; apply contrapn; apply biimpi; apply biins; apply biimpe;
      apply contrapp; apply riffimp; apply HP;
      [unfold igt; apply genxt; apply fimaxgel | apply Hiff].
     apply biins; apply biimpe; apply diffimp; apply HP;
      [unfold igt; apply genxt; apply fimaxger | apply Hiff].
     apply biimpe; apply contrapn; apply biimpi; apply biins; apply biimpe;
      apply contrapp; apply diffimp; apply HP;
      [unfold igt; apply genxt; apply fimaxgel | apply Hiff].
     apply biins; apply biimpe; apply riffimp; apply HP;
      [unfold igt; apply genxt; apply fimaxger | apply Hiff].
    (* Predicate variable *)
    destruct (x=☉☂b); [apply Hiff | BTprop].
    (* Equality *)
    destruct (exfgnewf (G☑★(〈x♇☋P@☍b〉♃ 〈x♇☋P@☍b0〉☳ 〈x♇☋P'@☍b〉♃ 〈x♇☋P'@☍b0〉)));
     destruct (ginfinc _ _ _ g); clear g; invset_clear p; clear H0; invset_clear H;
     invset_clear H0; invset_clear H1.
    replace (〈x♇☋P@☍b〉♃ 〈x♇☋P@☍b0〉☳ 〈x♇☋P'@☍b〉♃ 〈x♇☋P'@☍b0〉) with
                   〈x0♇☍〈x♇☋P@☍b〉@☋χ(x0)♃ 〈x♇☋P@☍b0〉☳ 〈x♇☋P'@☍b〉♃ 〈x♇☋P'@☍b0〉〉.
     assert (Heq:G⊣ 〈x♇☋P@☍b〉♃ 〈x♇☋P'@☍b〉). apply HE;
      [unfold igt; apply genxt; apply fimaxgel | apply Hiff].
     apply (bieque _ x0 (χ(x0)♃ 〈x♇☋P@☍b0〉☳ 〈x♇☋P'@☍b〉♃ 〈x♇☋P'@☍b0〉) _ _
                              (symequ _ _ _ Heq)); simpl; rewrite reflfiequ;
      rewrite (feaffexpnul _ 〈x♇☋P'@☍b〉 _ H0); rewrite (feaffexpnul _ 〈x♇☋P'@☍b〉 _ H2);
      rewrite (feaffexpnul _ 〈x♇☋P'@☍b〉 _ H3);
      fold (piff (〈x♇☋P'@☍b〉♃ 〈x♇☋P@☍b0〉) (〈x♇☋P'@☍b〉♃ 〈x♇☋P'@☍b0〉)).
     clear Heq; assert (Heq:G⊣ 〈x♇☋P@☍b0〉♃ 〈x♇☋P'@☍b0〉). apply HE;
      [unfold igt; apply genxt; apply fimaxger | apply Hiff].
     replace (〈x♇☋P'@☍b〉♃ 〈x♇☋P@☍b0〉☳ 〈x♇☋P'@☍b〉♃ 〈x♇☋P'@☍b0〉) with
                    〈x0♇☍〈x♇☋P@☍b0〉@☋ 〈x♇☋P'@☍b〉♃χ(x0)☳ 〈x♇☋P'@☍b〉♃ 〈x♇☋P'@☍b0〉〉.
      apply (bieque _ x0 (〈x♇☋P'@☍b〉♃χ(x0)☳ 〈x♇☋P'@☍b〉♃ 〈x♇☋P'@☍b0〉) _ _
                               (symequ _ _ _ Heq)); simpl; rewrite reflfiequ;
       rewrite (feaffexpnul _ 〈x♇☋P'@☍b0〉 _ H0); rewrite (feaffexpnul _ 〈x♇☋P'@☍b0〉 _ H3);
       BTprop.
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P@☍b0〉 _ H0);
       rewrite (feaffexpnul _ 〈x♇☋P@☍b0〉 _ H3); apply refl_equal.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P@☍b〉 _ H0);
      rewrite (feaffexpnul _ 〈x♇☋P@☍b〉 _ H2); rewrite (feaffexpnul _ 〈x♇☋P@☍b〉 _ H3);
      apply refl_equal.
    (* Membership *)
    destruct (exfgnewf (G☑★(〈x♇☋P@☍b〉∈ 〈x♇☋P@☍b0〉☳ 〈x♇☋P'@☍b〉∈ 〈x♇☋P'@☍b0〉)));
     destruct (ginfinc _ _ _ g); clear g; invset_clear p; clear H0; invset_clear H;
     invset_clear H0; invset_clear H1.
    replace (〈x♇☋P@☍b〉∈ 〈x♇☋P@☍b0〉☳ 〈x♇☋P'@☍b〉∈ 〈x♇☋P'@☍b0〉) with
                   〈x0♇☍〈x♇☋P@☍b〉@☋χ(x0)∈ 〈x♇☋P@☍b0〉☳ 〈x♇☋P'@☍b〉∈ 〈x♇☋P'@☍b0〉〉.
     assert (Heq:G⊣ 〈x♇☋P@☍b〉♃ 〈x♇☋P'@☍b〉). apply HE;
      [unfold igt; apply genxt; apply fimaxgel | apply Hiff].
     apply (bieque _ x0 (χ(x0)∈ 〈x♇☋P@☍b0〉☳ 〈x♇☋P'@☍b〉∈ 〈x♇☋P'@☍b0〉) _ _
                              (symequ _ _ _ Heq)); simpl; rewrite reflfiequ;
      rewrite (feaffexpnul _ 〈x♇☋P'@☍b〉 _ H0); rewrite (feaffexpnul _ 〈x♇☋P'@☍b〉 _ H2);
      rewrite (feaffexpnul _ 〈x♇☋P'@☍b〉 _ H3);
      fold (piff (〈x♇☋P'@☍b〉∈ 〈x♇☋P@☍b0〉) (〈x♇☋P'@☍b〉∈ 〈x♇☋P'@☍b0〉)).
     clear Heq; assert (Heq:G⊣ 〈x♇☋P@☍b0〉♃ 〈x♇☋P'@☍b0〉). apply HE;
      [unfold igt; apply genxt; apply fimaxger | apply Hiff].
     replace (〈x♇☋P'@☍b〉∈ 〈x♇☋P@☍b0〉☳ 〈x♇☋P'@☍b〉∈ 〈x♇☋P'@☍b0〉) with
                    〈x0♇☍〈x♇☋P@☍b0〉@☋ 〈x♇☋P'@☍b〉∈χ(x0)☳ 〈x♇☋P'@☍b〉∈ 〈x♇☋P'@☍b0〉〉.
      apply (bieque _ x0 (〈x♇☋P'@☍b〉∈χ(x0)☳ 〈x♇☋P'@☍b〉∈ 〈x♇☋P'@☍b0〉) _ _
                               (symequ _ _ _ Heq)); simpl; rewrite reflfiequ;
       rewrite (feaffexpnul _ 〈x♇☋P'@☍b0〉 _ H0); rewrite (feaffexpnul _ 〈x♇☋P'@☍b0〉 _ H3);
       BTprop.
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P@☍b0〉 _ H0);
       rewrite (feaffexpnul _ 〈x♇☋P@☍b0〉 _ H3); apply refl_equal.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P@☍b〉 _ H0);
      rewrite (feaffexpnul _ 〈x♇☋P@☍b〉 _ H2); rewrite (feaffexpnul _ 〈x♇☋P@☍b〉 _ H3);
      apply refl_equal.
   destruct t; simpl; intros HE HP P P' G x Hiff.
    (* BIG set *)
    apply biequi.
    (* Choice *)
    destruct (exfgnewf (G☑★((☭ 〈x ♇☋P@☍t〉)♃(☭ 〈x ♇☋P'@☍t〉)))); destruct (ginfinc _ _ _ g); clear g;
    invset_clear p; invset_clear H; invset_clear H0.
    replace ((☭ 〈x♇☋P@☍t〉)♃(☭ 〈x♇☋P'@☍t〉)) with 〈x0♇☍〈x♇☋P@☍t〉@☋(☭ χ(x0))♃(☭ 〈x♇☋P'@☍t〉)〉.
     assert (Heq:G⊣ 〈x♇☋P@☍t〉♃ 〈x♇☋P'@☍t〉). apply HE; [unfold igt; apply reflige | apply Hiff].
     apply (bieque _ x0 ((☭ χ(x0))♃(☭ 〈x♇☋P'@☍t〉)) _ _ (symequ _ _ _ Heq));
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P'@☍t〉 _ H); apply biequi.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P@☍t〉 _ H); apply refl_equal.
    (* Powerset *)
    destruct (exfgnewf (G☑★((☬ 〈x ♇☋P@☍t〉)♃(☬ 〈x ♇☋P'@☍t〉)))); destruct (ginfinc _ _ _ g); clear g;
    invset_clear p; invset_clear H; invset_clear H0.
    replace ((☬ 〈x♇☋P@☍t〉)♃(☬ 〈x♇☋P'@☍t〉)) with 〈x0♇☍〈x♇☋P@☍t〉@☋(☬ χ(x0))♃(☬ 〈x♇☋P'@☍t〉)〉.
     assert (Heq:G⊣ 〈x♇☋P@☍t〉♃ 〈x♇☋P'@☍t〉). apply HE; [unfold igt; apply reflige | apply Hiff].
     apply (bieque _ x0 ((☬ χ(x0))♃(☬ 〈x♇☋P'@☍t〉)) _ _ (symequ _ _ _ Heq));
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P'@☍t〉 _ H); apply biequi.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P@☍t〉 _ H); apply refl_equal.
    (* Maplet *)
    destruct (exfgnewf (G☑★((〈x♇☋P@☍t1〉☵ 〈x♇☋P@☍t2〉)♃(〈x♇☋P'@☍t1〉☵ 〈x♇☋P'@☍t2〉))));
     destruct (ginfinc _ _ _ g); clear g; invset_clear p; invset_clear H; invset_clear H0.
    replace ((〈x♇☋P@☍t1〉☵ 〈x♇☋P@☍t2〉)♃(〈x♇☋P'@☍t1〉☵ 〈x♇☋P'@☍t2〉)) with
                   〈x0♇☍〈x♇☋P@☍t1〉@☋(χ(x0)☵ 〈x♇☋P@☍t2〉)♃(〈x♇☋P'@☍t1〉☵ 〈x♇☋P'@☍t2〉)〉.
     assert (Heq:G⊣ 〈x♇☋P@☍t1〉♃ 〈x♇☋P'@☍t1〉). apply HE;
      [unfold igt; apply genxt; apply fimaxgel | apply Hiff].
     apply (bieque _ x0 ((χ(x0)☵ 〈x♇☋P@☍t2〉)♃(〈x♇☋P'@☍t1〉☵ 〈x♇☋P'@☍t2〉)) _ _
                              (symequ _ _ _ Heq)); simpl; rewrite reflfiequ;
      rewrite (feaffexpnul _ 〈x♇☋P'@☍t1〉 _ H); rewrite (feaffexpnul _ 〈x♇☋P'@☍t1〉 _ H2);
      rewrite (feaffexpnul _ 〈x♇☋P'@☍t1〉 _ H3).
     clear Heq; assert (Heq:G⊣ 〈x♇☋P@☍t2〉♃ 〈x♇☋P'@☍t2〉). apply HE;
      [unfold igt; apply genxt; apply fimaxger | apply Hiff].
     replace ((〈x♇☋P'@☍t1〉☵ 〈x♇☋P@☍t2〉)♃(〈x♇☋P'@☍t1〉☵ 〈x♇☋P'@☍t2〉)) with
                    〈x0♇☍〈x♇☋P@☍t2〉@☋(〈x♇☋P'@☍t1〉☵χ(x0))♃(〈x♇☋P'@☍t1〉☵ 〈x♇☋P'@☍t2〉)〉.
      apply (bieque _ x0 ((〈x♇☋P'@☍t1〉☵χ(x0))♃(〈x♇☋P'@☍t1〉☵ 〈x♇☋P'@☍t2〉)) _ _
                               (symequ _ _ _ Heq)); simpl; rewrite reflfiequ;
       rewrite (feaffexpnul _ 〈x♇☋P'@☍t2〉 _ H); rewrite (feaffexpnul _ 〈x♇☋P'@☍t2〉 _ H3);
       apply biequi.
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P@☍t2〉 _ H);
       rewrite (feaffexpnul _ 〈x♇☋P@☍t2〉 _ H3); apply refl_equal.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P@☍t1〉 _ H);
      rewrite (feaffexpnul _ 〈x♇☋P@☍t1〉 _ H2); rewrite (feaffexpnul _ 〈x♇☋P@☍t1〉 _ H3);
      apply refl_equal.
    (* Cartesian product *)
    destruct (exfgnewf (G☑★((〈x♇☋P@☍t1〉♂ 〈x♇☋P@☍t2〉)♃(〈x♇☋P'@☍t1〉♂ 〈x♇☋P'@☍t2〉))));
     destruct (ginfinc _ _ _ g); clear g; invset_clear p; invset_clear H; invset_clear H0.
    replace ((〈x♇☋P@☍t1〉♂ 〈x♇☋P@☍t2〉)♃(〈x♇☋P'@☍t1〉♂ 〈x♇☋P'@☍t2〉)) with
                   〈x0♇☍〈x♇☋P@☍t1〉@☋(χ(x0)♂ 〈x♇☋P@☍t2〉)♃(〈x♇☋P'@☍t1〉♂ 〈x♇☋P'@☍t2〉)〉.
     assert (Heq:G⊣ 〈x♇☋P@☍t1〉♃ 〈x♇☋P'@☍t1〉). apply HE;
      [unfold igt; apply genxt; apply fimaxgel | apply Hiff].
     apply (bieque _ x0 ((χ(x0)♂ 〈x♇☋P@☍t2〉)♃(〈x♇☋P'@☍t1〉♂ 〈x♇☋P'@☍t2〉)) _ _
                              (symequ _ _ _ Heq)); simpl; rewrite reflfiequ;
      rewrite (feaffexpnul _ 〈x♇☋P'@☍t1〉 _ H); rewrite (feaffexpnul _ 〈x♇☋P'@☍t1〉 _ H2);
      rewrite (feaffexpnul _ 〈x♇☋P'@☍t1〉 _ H3).
     clear Heq; assert (Heq:G⊣ 〈x♇☋P@☍t2〉♃ 〈x♇☋P'@☍t2〉). apply HE;
      [unfold igt; apply genxt; apply fimaxger | apply Hiff].
     replace ((〈x♇☋P'@☍t1〉♂ 〈x♇☋P@☍t2〉)♃(〈x♇☋P'@☍t1〉♂ 〈x♇☋P'@☍t2〉)) with
                    〈x0♇☍〈x♇☋P@☍t2〉@☋(〈x♇☋P'@☍t1〉♂χ(x0))♃(〈x♇☋P'@☍t1〉♂ 〈x♇☋P'@☍t2〉)〉.
      apply (bieque _ x0 ((〈x♇☋P'@☍t1〉♂χ(x0))♃(〈x♇☋P'@☍t1〉♂ 〈x♇☋P'@☍t2〉)) _ _
                               (symequ _ _ _ Heq)); simpl; rewrite reflfiequ;
       rewrite (feaffexpnul _ 〈x♇☋P'@☍t2〉 _ H); rewrite (feaffexpnul _ 〈x♇☋P'@☍t2〉 _ H3);
       apply biequi.
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P@☍t2〉 _ H);
       rewrite (feaffexpnul _ 〈x♇☋P@☍t2〉 _ H3); apply refl_equal.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P@☍t1〉 _ H);
      rewrite (feaffexpnul _ 〈x♇☋P@☍t1〉 _ H2); rewrite (feaffexpnul _ 〈x♇☋P@☍t1〉 _ H3);
      apply refl_equal.
    (* BIG element *)
    apply biequi.
    (* Variable *)
    apply biequi.
    (* Comprehension Set *)
    destruct (exfgnewf (G☑★(〈x♇☋P@☍t〉 ♀ 〈x♇☋☯☋(P)@☋b〉)♃(〈x♇☋P'@☍t〉 ♀ 〈x♇☋☯☋(P')@☋b〉)));
     destruct (ginfinc _ _ _ g); clear g; invset_clear p; invset_clear H; invset_clear H0.
    replace ((〈x♇☋P@☍t〉 ♀ 〈x♇☋☯☋(P)@☋b〉)♃(〈x♇☋P'@☍t〉 ♀  〈x♇☋☯☋(P')@☋b〉)) with
                   〈x0♇☍〈x♇☋P@☍t〉@☋(χ(x0) ♀ 〈x♇☋☯☋(P)@☋b〉)♃(〈x♇☋P'@☍t〉 ♀ 〈x♇☋☯☋(P')@☋b〉)〉;
     [idtac
     | simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈x♇☋P@☍t〉 _ H);
       rewrite (fpaffexpnul _ ☯☍(〈x♇☋P@☍t〉) _ H2); rewrite (fpaffexpnul _ ☯☍(〈x♇☋P@☍t〉) _ H3);
       apply refl_equal].
    assert (Heq:G⊣ 〈x♇☋P@☍t〉♃ 〈x♇☋P'@☍t〉). apply HE;
     [unfold igt; apply genxt; apply fimaxgel | apply Hiff].
    apply (bieque _ x0 ((χ(x0) ♀ 〈x♇☋☯☋(P)@☋b〉)♃(〈x♇☋P'@☍t〉 ♀ 〈x♇☋☯☋(P')@☋b〉)) _ _
                             (symequ _ _ _ Heq)); simpl; rewrite reflfiequ;
     rewrite (feaffexpnul _ 〈x♇☋P'@☍t〉 _ H); rewrite (fpaffexpnul _ ☯☍(〈x♇☋P'@☍t〉) _ H2);
     rewrite (fpaffexpnul _ ☯☍(〈x♇☋P'@☍t〉) _ H3).
     apply bieqsi; apply (bipowi G x0); try (apply infcmp; [apply H | apply H2]);
      try (apply infcmp; [apply H | apply H3]); apply (bifori G x0); try (apply g0);
      apply biimpi; apply dbcmpi.
      apply dbcmpl with (P:=〈x♇☋☯☋(P)@☋b〉); BTprop.
      unfold fpinfor; simpl; unfold fpuplft;
       rewrite (sym_id (fpaffprdfpinloc b P' _ _ ☦ (npdvar x x0)));
       apply iffrepgoal with (P:=〈x♇☋P@☋fpinloc ☦ χ(x0) b〉).
       apply HP.
        rewrite fpinlocdpeq; unfold igt; apply genxt; apply fimaxger.
        apply biins; apply Hiff.
       rewrite (fpaffprdfpinloc b P _ _ ☦ (npdvar x x0));
        replace (fpinloc ☦ χ(x0) 〈x♇☋fpuploc ☦ P@☋b〉) with
                       ☭☢(∀〈x♇☋☯☋(P)@☋b〉←χ(x0)⊢pif 〈x♇☋fpuploc ☦ P@☋b〉);
        [idtac | unfold fpinfor; apply refl_equal]; apply dbcmpr with (S:=〈x♇☋P'@☍t〉); BTprop.
      apply dbcmpl with (P:=〈x♇☋☯☋(P')@☋b〉); BTprop.
      unfold fpinfor; simpl; unfold fpuplft;
       rewrite (sym_id (fpaffprdfpinloc b P _ _ ☦ (npdvar x x0)));
       apply iffrepgoal with (P:=〈x♇☋P'@☋fpinloc ☦ χ(x0) b〉).
       apply symiff; apply HP.
        rewrite fpinlocdpeq; unfold igt; apply genxt; apply fimaxger.
        apply biins; apply Hiff.
       rewrite (fpaffprdfpinloc b P' _ _ ☦ (npdvar x x0));
        replace (fpinloc ☦ χ(x0) 〈x♇☋fpuploc ☦ P'@☋b〉) with
                       ☭☢(∀〈x♇☋☯☋(P')@☋b〉←χ(x0)⊢pif 〈x♇☋fpuploc ☦ P'@☋b〉);
        [idtac | unfold fpinfor; apply refl_equal]; apply dbcmpr with (S:=〈x♇☋P'@☍t〉); BTprop.
 Qed.
 (* Nota: A few points worth noticing in this theorem...                                                            *)
 (* - P and P' are not ground, and may have free variables appearing in the context G        *)
 (* - Due to the definition of 〈x♇☋@〉, however, P and P' variables cannot be captured by   *)
 (* a quantifer appearing in Q or E. Therefore it does not provide e.g. ☬☢(x∙P)☳☬☢(x∙¬¬P).    *)

 Definition ifffpaffprd:=fst iffftaffprd.
 Definition ifffeaffprd:=snd iffftaffprd.

 Theorem ifffpaffprd':forall (Q P P':☊)(G:☐)(x:☈), G⊣P☳P'->G⊣ 〈x♇☋P@☋Q〉->G⊣ 〈x♇☋P'@☋Q〉.
 Proof.
  intros Q P P' G x H H0; apply iffrepgoal with (P:=〈x♇☋P@☋Q〉);
   [apply (ifffpaffprd Q _ _ _ x H) | apply H0].
 Qed.

 Theorem ifffeaffprd':forall (E:☌)(Q P P':☊)(G:☐)(m v:☈),
                                        G⊣P☳P'->G⊣ 〈v♇☍〈m♇☋P@☍E〉@☋Q〉-> G⊣ 〈v♇☍〈m♇☋P'@☍E〉@☋Q〉.
 Proof.
  intros E Q P P' G m v Hiff HP; apply (bieque G v Q 〈m♇☋P@☍E〉 〈m♇☋P'@☍E〉);
   [apply (ifffeaffprd E _ _ _ m Hiff) | apply HP].
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
