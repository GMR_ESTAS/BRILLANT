(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Binst, terms instantiation
   Definition of the instantiation function, and related properties
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - August 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Blift.

(* Instantiation function, local version ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace the previously bounded index d by expression F *)
 Fixpoint fpinloc(d:☈)(F:☌)(P:☊){struct P}:☊:=
  match P with
  | ¬P' => ¬ (fpinloc d F P')
  | ∀P' => ∀ (fpinloc ☠d ☯☍(F) P')
  | P1∧P2 => (fpinloc d F P1) ∧ (fpinloc d F P2)
  | P1☲P2 => (fpinloc d F P1) ☲ (fpinloc d F P2)
  | π i => π i
  | E1♃E2 => (feinloc d F E1) ♃ (feinloc d F E2)
  | E1∈E2 => (feinloc d F E1) ∈ (feinloc d F E2)
  end
 with feinloc(d:☈)(F E:☌){struct E}:☌:=
  match E with
  | ♄ => ♄
  | ☭E' => ☭ (feinloc d F E')
  | ☬E' => ☬ (feinloc d F E')
  | (E1☵E2) => ((feinloc d F E1) ☵ (feinloc d F E2))
  | (E1♂E2) => ((feinloc d F E1) ♂ (feinloc d F E2))
  | ω i => ω i
  | χ i => match d≤☉☂i with ⊤ => match (d=☉☂i) with ⊤ => F | ⊥ => χ ☋i end | ⊥ => χ i end
  | E'♀ P' => (feinloc d F E')♀(fpinloc ☠d ☯☍(F) P')
  end.

 (* fpinloc and feinloc preserve free variables *)
 Theorem ftinlocifrf:(forall (P:☊)(F:☌)(i d:☈), d≤☉i->☠i⍀☋☂P≡⊥->i⍀☋☂(fpinloc d F P)≡⊥)*
                                       (forall (E F:☌)(i d:☈), d≤☉i->☠i⍀☍☂E≡⊥->i⍀☍☂(feinloc d F E)≡⊥).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F:☌)(i d:☈), d≤☉i->☠i⍀☋☂P≡⊥->i⍀☋☂(fpinloc d F P)≡⊥)
                             (fun E:☌=>forall (F:☌)(i d:☈), d≤☉i->☠i⍀☍☂E≡⊥->i⍀☍☂(feinloc d F E)≡⊥)); simpl.
   intros _ i d H H0; invset H0.
   intros v; split; [split | idtac]; intros F i d H H0; try (invset H0; fail ); destruct v;
    simpl.
    invset H0.
    rewrite (sym_id (iequtp _ _ (dedfbnotf _ H0))); clear H0 v;
     rewrite (igept _ _ (nxtrige _ _ H)); destruct (deciequ d ☠i).
     rewrite <- (sym_id i0) in H; destruct (nreflnige _ H).
     rewrite (iequrf _ _ e); simpl; rewrite (reflfiequ i); apply refl_id.
   intros P H; split; intros F i d H0 H1;
    [apply (H F _ _ H0 H1) | apply (H ☯☍(F) _ _ (genxt _ _ H0) H1)].
   intros E H; split; intros F i d H0 H1; apply (H F _ _ H0 H1).
   intros P1 P2 H1 H2; split; intros F i d H H0; destruct (dedfbandf _ _ H0);
    try (rewrite (H2 F _ _ H i0); rewrite (shortfbandr (i⍀☋☂fpinloc d F P1)));
    try (rewrite (H1 F _ _ H i0)); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F i d H H0; destruct (dedfbandf _ _ H0);
    try (rewrite (H1 F _ _ H i0));
    try (rewrite (H2 F _ _ H i0); rewrite (shortfbandr (i⍀☍☂feinloc d F E1))); apply refl_id.
   intros E P H1 H2; intros F i d H H0; destruct (dedfbandf _ _ H0);
    try (rewrite (H1 F _ _ H i0));
    try (rewrite (H2 ☯☍(F) _ _ (genxt _ _ H) i0); rewrite (shortfbandr (i⍀☍☂feinloc d F E)));
    apply refl_id.
 Qed.

 Definition fpinlocifrf:=fst ftinlocifrf.
 Definition feinlocifrf:=snd ftinlocifrf.

 Theorem fpinlocifr:forall (P:☊)(F:☌)(i d:☈), d≤☉i->!(☠i⍀☋P)->!(i⍀☋(fpinloc d F P)).
 Proof.
  intros P F i d H H0; apply (pinffr _ _ (fpinlocifrf _ F _ _ H (pinfrf _ _ H0))).
 Qed.

 Theorem feinlocifr:forall (E F:☌)(i d:☈), d≤☉i->!(☠i⍀☍E)->!(i⍀☍(feinloc d F E)).
 Proof.
  intros E F i d H H0; apply (einffr _ _ (feinlocifrf _ F _ _ H (einfrf _ _ H0))).
 Qed.

 (* fpinloc and feinloc does not create free variables, except those from F *)
 Theorem ftinlocinff:(forall (P:☊)(F:☌)(i d:☈), d≤☉i->i⍀☍☂F≡⊤->☠i⍀☋☂P≡⊤->i⍀☋☂(fpinloc d F P)≡⊤)*
                                       (forall (E F:☌)(i d:☈), d≤☉i->i⍀☍☂F≡⊤->☠i⍀☍☂E≡⊤->i⍀☍☂(feinloc d F E)≡⊤).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F:☌)(i d:☈), d≤☉i->i⍀☍☂F≡⊤->☠i⍀☋☂P≡⊤->i⍀☋☂(fpinloc d F P)≡⊤)
                             (fun E:☌=>forall (F:☌)(i d:☈), d≤☉i->i⍀☍☂F≡⊤->☠i⍀☍☂E≡⊤->i⍀☍☂(feinloc d F E)≡⊤));
   simpl.
   intros F i d Hd HF HT; apply refl_id.
   intros v; split; [split | idtac]; intros F i d Hd HF HT; try (apply refl_id); destruct v;
    simpl.
    destruct (decige d ☦).
     rewrite (igept _ _ i0); invset i0; rewrite (reflfiequ ☦); apply HF.
     rewrite (igerf _ _ e); simpl; destruct i; [contradiction | apply refl_id].
    destruct (decige d ☠v).
     rewrite (igept _ _ i0); destruct (deciequ d ☠v);
      [rewrite (iequpt _ _ i1); apply HF | rewrite (iequrf _ _ e); simpl; apply HT].
     rewrite (igerf _ _ e); simpl; destruct (deciequ i ☠v);
      [rewrite <- i0 in e; contradiction | rewrite (iequrf _ _ e0); apply refl_id].
   intros P H; split; intros F i d Hd HF HT;
    [apply (H _ _ _ Hd HF HT)
    | rewrite <- (feuplftidx F i) in HF; apply (H _ _ _ (genxt _ _ Hd) HF HT)].
   intros E H; split; intros F i d Hd HF HT; apply (H _ _ _ Hd HF HT).
   intros P1 P2 H1 H2; split; intros F i d Hd HF HT; destruct (dedfbandt _ _ HT);
    rewrite (H1 _ _ _ Hd HF i0); rewrite (H2 _ _ _ Hd HF i1); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F i d Hd HF HT; destruct (dedfbandt _ _ HT);
    rewrite (H1 _ _ _ Hd HF i0); rewrite (H2 _ _ _ Hd HF i1); apply refl_id.
   intros E P H1 H2; intros F i d Hd HF HT; destruct (dedfbandt _ _ HT);
    rewrite (H1 _ _ _ Hd HF i0); rewrite <- (feuplftidx F i) in HF;
    apply (H2 _ _ _ (genxt _ _ Hd) HF i1).
 Qed.

 Definition fpinlocinff:=fst ftinlocinff.
 Definition feinlocinff:=snd ftinlocinff.

 Theorem fpinlocinf:forall (P:☊)(F:☌)(i d:☈), d≤☉i->i⍀☍F->☠i⍀☋P->i⍀☋(fpinloc d F P).
 Proof.
  intros P F i d H H0 H1; apply pinftp;
   apply (fpinlocinff _ _ _ _ H (einfpt _ _ H0) (pinfpt _ _ H1)).
 Qed.

 Theorem feinlocinf:forall (E F:☌)(i d:☈), d≤☉i->i⍀☍F->☠i⍀☍E->i⍀☍(feinloc d F E).
 Proof.
  intros E F i d H H0 H1; apply einftp;
   apply (feinlocinff _ _ _ _ H (einfpt _ _ H0) (einfpt _ _ H1)).
 Qed.

 (* fpinloc and feinloc have no effect over non-free variables *)
 Theorem ftinlocftdnloc:(forall (P:☊)(F:☌)(d:☈), d⍀☋☂P≡⊤->fpinloc d F P≡fpdnloc d P)*
                                             (forall (E F:☌)(d:☈), d⍀☍☂E≡⊤->feinloc d F E≡fednloc d E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F:☌)(d:☈), d⍀☋☂P≡⊤->fpinloc d F P≡fpdnloc d P)
                             (fun E:☌=>forall (F:☌)(d:☈), d⍀☍☂E≡⊤->feinloc d F E≡fednloc d E)); simpl.
   intros F d H; apply refl_id.
   intros v; split; [split | idtac]; intros F d H; try (apply refl_id); destruct (decige d v);
    simpl.
    rewrite (igept _ _ i); rewrite (dedfbnott _ H); apply refl_id.
    rewrite (igerf _ _ e); apply refl_id.
   intros P H; split; intros F d H0; [rewrite (H F _ H0) | rewrite (H ☯☍(F) _ H0)]; apply refl_id.
   intros E H; split; intros F d H0; rewrite (H F _ H0); apply refl_id.
   intros P1 P2 H1 H2; split; intros F d H; destruct (dedfbandt _ _ H); rewrite (H1 F _ i);
    rewrite (H2 F _ i0); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F d H; destruct (dedfbandt _ _ H);
    rewrite (H1 F _ i); rewrite (H2 F _ i0); apply refl_id.
   intros E P H1 H2; intros F d H; destruct (dedfbandt _ _ H); rewrite (H1 F _ i);
    rewrite (H2 ☯☍(F) _ i0); apply refl_id.
 Qed.

 Definition fpinlocfpdnloc:=fst ftinlocftdnloc.
 Definition feinlocfednloc:=snd ftinlocftdnloc.

 (* fpinloc and feinloc have no effect on terms without adequate free variables *)
 Theorem ftinlocgrf:(forall (P:☊)(F:☌)(d:☈), d>☋☂P≡⊤->fpinloc d F P≡P)*
                                     (forall (E F:☌)(d:☈), d>☍☂E≡⊤->feinloc d F E≡E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F:☌)(d:☈), d>☋☂P≡⊤->fpinloc d F P≡P)
                             (fun E:☌=>forall (F:☌)(d:☈), d>☍☂E≡⊤->feinloc d F E≡E)); simpl.
   intros F d H; apply refl_id.
   intros i; split; [split | idtac]; intros F d H; try (apply refl_id); destruct (decige d i);
    simpl.
    destruct d.
     invset H.
     rewrite <- (sym_id (symige _ _ (igtige _ _ i0) (igetp _ _ H))) in i0;
      destruct (nreflnige _ i0).
     rewrite (igerf _ _ e); apply refl_id.
   intros P H; split; intros F d H0; [rewrite (H F _ H0) | rewrite (H ☯☍(F) _ H0)]; apply refl_id.
   intros E H; split; intros F d H0; rewrite (H F _ H0); apply refl_id.
   intros P1 P2 H1 H2; split; intros F d H; destruct (dedfbandt _ _ H); rewrite (H1 F _ i);
    rewrite (H2 F _ i0); apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F d H; destruct (dedfbandt _ _ H);
    rewrite (H1 F _ i); rewrite (H2 F _ i0); apply refl_id.
   intros E P H1 H2; intros F d H; destruct (dedfbandt _ _ H); rewrite (H1 F _ i);
    rewrite (H2 ☯☍(F) _ i0); apply refl_id.
 Qed.

 Definition fpinlocgrf:=fst ftinlocgrf.
 Definition feinlocgrf:=snd ftinlocgrf.

 Theorem fpinlocgr:forall (P:☊)(F:☌)(d:☈), d>☋P->fpinloc d F P≡P.
 Proof.
  intros P F d H; apply (fpinlocgrf _ F _ (pmajpt _ _ H)).
 Qed.

 Theorem feinlocgr:forall (E F:☌)(d:☈), d>☍E->feinloc d F E≡E.
 Proof.
  intros E F d H; apply (feinlocgrf _ F _ (emajpt _ _ H)).
 Qed.

 (* fpinloc and feinloc does not create π-dependance but merge them *)
 Theorem ftinlocftnpd:(forall (P:☊)(F:☌)(d p:☈),
                                              fpnpd p (fpinloc d F P)≡fpnpd p P∧☁((d⍀☋☂P)∨☁fenpd p F))*
                                             (forall (E F:☌)(d p:☈),
                                              fenpd p (feinloc d F E)≡fenpd p E∧☁((d⍀☍☂E)∨☁fenpd p F)).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F:☌)(d p:☈),
                              fpnpd p (fpinloc d F P)≡fpnpd p P∧☁((d⍀☋☂P)∨☁fenpd p F))
                             (fun E:☌=>forall (F:☌)(d p:☈),
                              fenpd p (feinloc d F E)≡fenpd p E∧☁((d⍀☍☂E)∨☁fenpd p F))); simpl.
   intros F d p; apply refl_id.
   intros i; split; [split | idtac]; intros F d p.
    destruct (p=☉☂i); apply refl_id.
    apply refl_id.
    destruct (decige d i).
     rewrite (igept _ _ i0); destruct (d=☉☂i); apply refl_id.
     rewrite (igerf _ _ e); simpl; destruct (deciequ d i).
      rewrite <- (sym_id i0) in e; destruct e; apply reflige.
      rewrite (iequrf _ _ e0); apply refl_id.
   intros P H; split; intros F d p;
    [apply H | rewrite H; rewrite (feuplftfenpd F p); apply refl_id].
   intros E H; split; intros F d p; apply H.
   intros P1 P2 H1 H2; split; intros F d p; rewrite H1; rewrite H2; destruct (fpnpd p P1);
    destruct (fpnpd p P2); destruct (fenpd p F); destruct (d⍀☋☂P1); destruct (d⍀☋☂P2);
    apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F d p; rewrite H1; rewrite H2;
    destruct (fenpd p E1); destruct (fenpd p E2); destruct (fenpd p F); destruct (d⍀☍☂E1);
    destruct (d⍀☍☂E2); apply refl_id.
   intros E P H1 H2 F d p; rewrite H1; rewrite H2; destruct (fenpd p E); destruct (fpnpd p P);
    rewrite (feuplftfenpd F p); destruct (fenpd p F); destruct (d⍀☍☂E); destruct (☠d⍀☋☂P);
    apply refl_id.
 Qed.

 Definition fpinlocfpnpd:=fst ftinlocftnpd.
 Definition feinlocfenpd:=snd ftinlocftnpd.

(* ∀-instantiation function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fpinfor(P:☊)(F:☌)(H:?∀(P)):=fpinloc ☦ F (fpprjfor P H).
 Notation "'☭☢(' P '←' F '⊢' H ')'":=(fpinfor P F H).

 Theorem fpinforifrf:forall (P:☊)(F:☌)(i:☈), i⍀☋☂∀P≡⊥->i⍀☋☂ ☭☢(∀P←F⊢pif P)≡⊥.
 Proof.
  intros P F i H; unfold fpinfor; apply (fpinlocifrf _ F _ _ (gefst i) H).
 Qed.

 Theorem fpinforifr:forall (P:☊)(F:☌)(i:☈), !(i⍀☋ ∀P)->!(i⍀☋ ☭☢(∀P←F⊢pif P)).
 Proof.
  intros P F i H; unfold fpinfor; apply (fpinlocifr P F _ ☦ (gefst i)); intros H0; apply H;
   apply inffor; apply H0.
 Qed.

 Theorem fpinforinff:forall (P:☊)(F:☌)(i:☈), i⍀☍☂F≡⊤->i⍀☋☂∀P≡⊤->i⍀☋☂ ☭☢(∀P←F⊢pif P)≡⊤.
 Proof.
  intros P F i H H0; unfold fpinfor; apply (fpinlocinff _ _ _ _ (gefst i) H H0).
 Qed.

 Theorem fpinforinf:forall (P:☊)(F:☌)(i:☈), i⍀☍F->i⍀☋ ∀P->i⍀☋ ☭☢(∀P←F⊢pif P).
 Proof.
  intros P F i H H0; unfold fpinfor; apply (fpinlocinf P _ _ _ (gefst i) H); invset_clear H0;
   apply H1.
 Qed.

 Theorem fpinforcl:forall (P:☊)(E:☌), *⍀☋(P)->☭☢(∀P←E⊢pif P)≡P.
 Proof.
  intros P E H; unfold fpinfor; apply (fpinlocgr P E _ H).
 Qed.

 Theorem fpinforfpnpd:forall (P:☊)(F:☌)(p:☈),
                                         fpnpd p ☭☢(∀P←F⊢pif P)≡fpnpd p P∧☁((☦⍀☋☂P)∨☁fenpd p F).
 Proof.
  intros P F p; unfold fpinfor; apply fpinlocfpnpd.
 Qed.

 (* Proof irrelevance for ☭☢ *)
 Theorem fpinforpi:forall (P:☊)(E:☌)(H1 H2:?∀(P)), ☭☢(P←E⊢H1)≡☭☢(P←E⊢H2).
 Proof.
  intros P E H1 H2; unfold fpinfor; apply refl_id.
 Qed.

(* ∃-instantiation function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fpinexs(P:☊)(F:☌)(H:?∃(P)):=fpinloc ☦ F (fpprjexs P H).
 Notation "'☭☣(' P '←' F '⊢' H ')'":=(fpinexs P F H).

 Theorem fpinexsifrf:forall (P:☊)(F:☌)(i:☈), i⍀☋☂∃P≡⊥->i⍀☋☂ ☭☣(∃P←F⊢pie P)≡⊥.
 Proof.
  intros P F i H; unfold fpinexs; apply (fpinlocifrf _ F _ _ (gefst i) H).
 Qed.

 Theorem fpinexsifr:forall (P:☊)(F:☌)(i:☈), !(i⍀☋ ∃P)->!(i⍀☋ ☭☣(∃P←F⊢pie P)).
 Proof.
  intros P F i H; unfold fpinexs; apply (fpinlocifr P F _ ☦ (gefst i)); intros H0; apply H;
   unfold pexs; apply infnot; apply inffor; apply infnot; apply H0.
 Qed.

 Theorem fpinexsinff:forall (P:☊)(F:☌)(i:☈), i⍀☍☂F≡⊤->i⍀☋☂∃P≡⊤->i⍀☋☂ ☭☣(∃P←F⊢pie P)≡⊤.
 Proof.
  intros P F i H H0; unfold fpinexs; apply (fpinlocinff _ _ _ _ (gefst i) H H0).
 Qed.

 Theorem fpinexsinf:forall (P:☊)(F:☌)(i:☈), i⍀☍F->i⍀☋ ∃P->i⍀☋ ☭☣(∃P←F⊢pie P).
 Proof.
  intros P F i H H0; unfold fpinexs; apply (fpinlocinf P _ _ _ (gefst i) H); 
   invset_clear H0; invset_clear H1; invset_clear H0; apply H1.
 Qed.

 Theorem fpinexscl:forall (P:☊)(E:☌), *⍀☋(P)->☭☣(∃P←E⊢pie P)≡P.
 Proof.
  intros P E H; unfold fpinexs; unfold pexs; apply (fpinlocgr P E _ H).
 Qed.

 Theorem fpinexsfpnpd:forall (P:☊)(F:☌)(p:☈),
                                         fpnpd p ☭☣(∃P←F⊢pie P)≡fpnpd p P∧☁((☦⍀☋☂P)∨☁fenpd p F).
 Proof.
  intros P F p; unfold fpinexs; unfold pexs; apply fpinlocfpnpd.
 Qed.

 (* Proof irrelevance for ☭☣ *)
 Theorem fpinexspi:forall (P:☊)(E:☌)(H1 H2:?∃(P)), ☭☣(P←E⊢H1)≡☭☣(P←E⊢H2).
 Proof.
  intros P E H1 H2; unfold fpinexs; apply refl_id.
 Qed.

(* ♀-instantiation function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition feincmp(E F:☌)(H:?♀(E)):=F∈(feprjcmpl E H)∧fpinloc ☦ F (feprjcmpr E H).
 Notation "'☭☤(' E '←' F '⊢' H ')'":=(feincmp E F H).

 Theorem feincmpifrf:forall (P:☊)(E F:☌)(i:☈), i⍀☍☂E♀P≡⊥->i⍀☋☂ ☭☤(E♀P←F⊢eic E P)≡⊥.
 Proof.
  intros P E F i H; unfold feincmp; invset H; rewrite H1; apply pinfrf; intros H2;
   invset_clear H2; invset_clear H0; destruct (dedfbandf _ _ H1);
   [apply (einffr _ _ i0 H4) | apply (pinffr _ _ (fpinlocifrf _ F _ _ (gefst i) i0) H3)].
 Qed.

 Theorem feincmpifr:forall (P:☊)(E F:☌)(i:☈), !(i⍀☍E♀P)->!(i⍀☋ ☭☤(E♀P←F⊢eic E P)).
 Proof.
  intros P E F i H; unfold feincmp; intros H0; invset_clear H0; invset_clear H1; apply H;
   clear H; apply infcmp.
   apply H3.
   destruct (decpinf ☠i P); [apply p | destruct (fpinlocifr P F _ _ (gefst i) e H2)].
 Qed.

 Theorem feincmpinff:forall (P:☊)(E F:☌)(i:☈), i⍀☍☂F≡⊤->i⍀☍☂E♀P≡⊤->i⍀☋☂ ☭☤(E♀P←F⊢eic E P)≡⊤.
 Proof.
  intros P E F i H H0; unfold feincmp; simpl; simpl in H0; destruct (dedfbandt _ _ H0);
   rewrite H; rewrite i0; apply (fpinlocinff P F _ _ (gefst i) H i1).
 Qed.

 Theorem feincmpinf:forall (P:☊)(E F:☌)(i:☈), i⍀☍F->i⍀☍E♀P->i⍀☋ ☭☤(E♀P←F⊢eic E P).
 Proof.
  intros P E F i H H0; unfold feincmp; invset_clear H0; apply infand;
   [apply infins; [apply H | apply H1] | apply (fpinlocinf P _ _ _ (gefst i) H H2)].
 Qed.

 Theorem feincmpcl:forall (P:☊)(E F:☌), *⍀☋(P)->☭☤(E♀P←F⊢eic E P)≡F∈E∧P.
 Proof.
  intros P E F H; unfold feincmp; simpl; rewrite (fpinlocgr P F _ H); apply refl_id.
 Qed.

 Theorem feincmpfpnpd:forall (P:☊)(E F:☌)(p:☈),
                                         fpnpd p ☭☤(E♀P←F⊢eic E P)≡fenpd p E∧☁fpnpd p P∧☁fenpd p F.
 Proof.
  intros P E F p; unfold feincmp; simpl; rewrite (fpinlocfpnpd P F ☦ p);
   destruct (fenpd p F); [rewrite shortfborr | repeat rewrite shortfbandr]; apply refl_id.
 Qed.

 (* Proof irrelevance for ☭☤ *)
 Theorem feincmppi:forall (E F:☌)(H1 H2:?♀(E)), ☭☤(E←F⊢H1)≡☭☤(E←F⊢H2).
 Proof.
  intros E F H1 H2; unfold feincmp; apply refl_id.
 Qed.

(* Commutation lemma ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Lemma ftinlocftuploc:(forall (P:☊)(F:☌)(i j:☈), j≤☉i->
                                          fpinloc ☠i (feuploc j F) (fpuploc j P)≡fpuploc j (fpinloc i F P))*
                                         (forall (E F:☌)(i j:☈), j≤☉i->
                                          feinloc ☠i (feuploc j F) (feuploc j E)≡feuploc j (feinloc i F E)).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (F:☌)(i j:☈), j≤☉i->fpinloc ☠i (feuploc j F) (fpuploc j P)≡
                                                                                         fpuploc j (fpinloc i F P))
                             (fun E:☌=>forall (F:☌)(i j:☈), j≤☉i->feinloc ☠i (feuploc j F) (feuploc j E)≡
                                                                                         feuploc j (feinloc i F E))); simpl.
   intros F i j H; apply refl_id.
   intros v; split; [split | idtac]; intros F i j H;
    [apply refl_id | apply refl_id | destruct (decige i v); simpl].
    rewrite (igept _ _ i0); rewrite (igept _ _ (transige _ _ _ H i0)); destruct (deciequ i v).
     rewrite (iequpt _ _ i1); rewrite i1; simpl; rewrite (igept _ _ (reflige v));
      apply refl_id.
     rewrite (iequrf _ _ e); simpl; rewrite (igept _ _ i0); destruct v; simpl.
      invset i0; destruct e; rewrite H0; apply refl_id.
      generalize (transigeigt _ _ _ H (igeneq _ _ e i0)); intros H0; invset H0;
       rewrite (igept _ _ H3); apply refl_id.
    rewrite (igerf _ _ e); destruct (decige j v).
     rewrite (igept _ _ i0); simpl; rewrite (igerf _ _ e); rewrite (igept _ _ i0);
      apply refl_id.
     rewrite (igerf _ _ e0); simpl; rewrite (igerf _ _ e0); destruct v.
      apply refl_id.
      destruct (decige i v);
       [destruct (e (nxtrige _ _ i0)) | rewrite (igerf _ _ e1); apply refl_id].
   intros P H; split; intros F i j H0.
    rewrite (H F _ _ H0); apply refl_id.
    unfold feuplft; rewrite (feuplocdbl F _ _ (gefst j));
     rewrite (H (feuploc ☦ F) _ _ (genxt _ _ H0)); apply refl_id.
   intros E H; split; intros F i j H0; rewrite (H F _ _ H0); apply refl_id.
   intros P1 P2 H1 H2; split; intros F i j H; rewrite (H1 F _ _ H); rewrite (H2 F _ _ H);
    apply refl_id.
   intros E1 E2 H1 H2; split; split; intros F i j H; rewrite (H1 F _ _ H); rewrite (H2 F _ _ H);
    apply refl_id.
   intros E P H1 H2; intros F i j H; rewrite (H1 F _ _ H); unfold feuplft;
    rewrite (feuplocdbl F _ _ (gefst j)); rewrite (H2 (feuploc ☦ F) _ _ (genxt _ _ H));
    apply refl_id.
 Qed.

 Definition fpinlocfpuploc:=fst ftinlocftuploc.
 Definition feinlocfeuploc:=snd ftinlocftuploc.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
