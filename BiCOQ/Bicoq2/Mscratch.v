(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq2, formalisation of the B theory into the Coq system
   Module: Mscratch, Coq reinitialisation
   Coq allows to use sort Set or Prop, the second being generally preferred for proofs
   types and allowing to use the proof irrelevance principle as well as of impredicative
   definitions. Both of these being useless in BiCoq2, Prop is not considered and
   standard notations are redefined to be applied to Set instead.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : August 2005 - September 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* We don't care about previously defined notations *)
 Close Scope nat_scope.
 Close Scope type_scope.
 Close Scope core_scope.

 (* Should Prop emerge, we will use p2s to go back to Set *)
 Inductive p2s(A:Prop):Set:=bp2s:A->p2s A.
 Notation "[ A ]":=(p2s A).

 (* Empty is the empty type ; it is the Set equivalent of False *)
 Inductive Empty:Set:=.

 (* Standards notations of Coq are redefined here *)
 Notation "A * B":=(prod A B).
 Notation "A + B":=(sum A B).
 Notation "! A":=(A->Empty) (right associativity, at level 0).
 Notation "{ x : T & S }":=(sigS (fun x:T=>S)).

 (* Equality (Prop) and Identity (Set) *)
 Notation "A = B":=(@eq _ A B) (no associativity, at level 70).
 Notation "A ≡ B":=(@identity _ A B) (no associativity, at level 70).

 Lemma id2eq:forall (T:Type)(a b:T), a≡b->a=b.
 Proof.
  intros T a b H; rewrite H; apply refl_equal.
 Qed.
 Implicit Arguments id2eq [T a b].

 Lemma eq2id:forall (T:Type)(a b:T), a=b->a≡b.
 Proof.
  intros T a b H; rewrite H; apply refl_identity.
 Qed.
 Implicit Arguments eq2id [T a b].

 Definition refl_id:=refl_identity.
 Implicit Arguments refl_id [A].

 Lemma sym_nid:forall (T:Type)(a b:T), !(a≡b)->!(b≡a).
 Proof.
  intros T a b H H'; apply H; rewrite H'; apply refl_id.
 Qed.
 Implicit Arguments sym_nid [T a b].

 (* Inversion tactic producing ≡ - Suggested by Hugo Herbelin *)
 Ltac repeat_on_hyps f:=repeat (match goal with [H:_|-_] => f H end).
 Ltac coerce3 f H:=let H':=fresh in (assert (H':=f _ _ _ H); clear H; rename H' into H).
 Ltac invset c:=inversion c; repeat_on_hyps ltac:(coerce3 eq2id).
 Ltac invset_clear c:=inversion_clear c; repeat_on_hyps ltac:(coerce3 eq2id).

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
