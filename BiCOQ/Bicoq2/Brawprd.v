(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Brawprd, raw predicate replacement results
   The predicate replacement defined by the fpaffprd and feaffprd functions is a
   De Bruijn compatible replacement, avoiding capture of free variables of the replaced
   predicate. In this module, we define a raw predicate replacement function allowing
   such captures, and provide associated results.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : July 2006 - October 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Bmaffec.

(* Strategy to deal with non-freeness ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Ltac BTinf:=
  (assumption || intros); match goal with | |- _ ⍀☋ _ => repeat STEPINF
                                                                       | |- _ ⍀☍ _ => repeat STEPINF
                                                                       | |- _ ⍀☑ _ => repeat STEPINF
                                                                       | |- _ * _  => split; BTinf
                                                                       | |- _ + _  => solve [left; BTinf | right; BTinf]
                                                                       | _ => idtac end
  with STEPINF:=
     solve [apply fpnewfinf | apply fenewfinf | apply fgnewfinf | apply infpvr
                | apply infbig | apply infbie | apply fpbdforifr | apply fpbdexsifr
                | apply fpbdcmpifr | apply ginfempty | assumption]
  || first [apply infnot | apply inffor | apply infand | apply infimp
                | apply infequ | apply infins | apply infchs | apply infpow
                | apply infcpl | apply infpro | apply infvar | apply infcmp
                | apply ginfdec | apply fpbdforinf | apply fpbdexsinf | apply fpbdcmpinf
                | apply fpaffexpinf | apply feaffexpinf]
  || match goal with | [H:(_ ⍀☑ (_ ☑★ _)) |- _] => destruct (ginfinc _ _ _ H); clear H
                                  | [H:(_ ⍀☋ (¬_)) |- _] => inversion_clear H
                                  | [H:(_ ⍀☋ (∀_)) |- _] => inversion_clear H
                                  | [H:(_ ⍀☋ (_∧_)) |- _] => inversion_clear H
                                  | [H:(_ ⍀☋ (_☲_)) |- _] => inversion_clear H
                                  | [H:(_ ⍀☋ (_♃_)) |- _] => inversion_clear H
                                  | [H:(_ ⍀☋ (_∈_)) |- _] => inversion_clear H
                                  | [H:(_ ⍀☍ (☭_)) |- _] => inversion_clear H
                                  | [H:(_ ⍀☍ (☬_)) |- _] => inversion_clear H
                                  | [H:(_ ⍀☍ (_☵_)) |- _] => inversion_clear H
                                  | [H:(_ ⍀☍ (_♂_)) |- _] => inversion_clear H
                                  | [H:(_ ⍀☍ (χ _)) |- _] => inversion_clear H
                                  | [H:(_ ⍀☍ (_♀_)) |- _] => inversion_clear H end.

(* Raw predicate replacement ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fprawprd(P Q:☊)(m:☈){struct P}:☊:=
  match P with 
  | ¬P' => ¬(fprawprd P' Q m)
  | ∀P' => ∀(fprawprd P' Q m)
  | P1∧P2 => (fprawprd P1 Q m)∧(fprawprd P2 Q m)
  | P1☲P2 => (fprawprd P1 Q m)☲(fprawprd P2 Q m)
  | π i => match (m=☉☂i) with ⊤ => Q | ⊥ => π i end
  | E1♃E2 => (ferawprd E1 Q m)♃(ferawprd E2 Q m)
  | E1∈E2 => (ferawprd E1 Q m)∈(ferawprd E2 Q m)
  end
 with ferawprd(E:☌)(Q:☊)(m:☈){struct E}:☌:=
  match E with
  | ♄ => ♄
  | ☭E' => ☭(ferawprd E' Q m)
  | ☬E' => ☬(ferawprd E' Q m)
  | (E1☵E2) => ((ferawprd E1 Q m)☵(ferawprd E2 Q m))
  | (E1♂E2) => ((ferawprd E1 Q m)♂(ferawprd E2 Q m))
  | ω i => ω i
  | χ i => χ i
  | E'♀P' => (ferawprd E' Q m)♀(fprawprd P' Q m)
  end.
 Notation "'〈' m '⍇' Q '@☋' P '〉'":=(fprawprd P Q m).
 Notation "'〈' m '⍇' Q '@☍' E '〉'":=(ferawprd E Q m).
 (* Nota: it is important to understand the difference between the 〈♇☋@〉functions and   *)
 (* the 〈⍇@〉 functions. The formers are De Bruijn compatible, in the sense that we have  *)
 (* 〈m♇☋Q@∀P〉=∀〈m♇☋☯☋(Q)@P〉, therefore avoiding capture of free variables, while the        *)
 (* latters are pure sub-terms replacement, for which 〈m⍇Q@∀P〉=∀〈m⍇Q@P〉, allowing        *)
 (* capture... Other terms functions have been proved to commute between themselves;      *)
 (* such results are very useful for B logic theorems proved by induction over terms.    *)
 (* However, no commutation results can be exihibited here because of the very nature   *)
 (* of 〈⍇@〉. For example, 〈m⍇fpuploc d Q@☋fpuploc d P〉=fpuploc d 〈m⍇Q@☋P〉 is not true it  *)
 (* P is quantified; one may also try to prove the more generic and flexible result         *)
 (* ∃d'∙〈m⍇fpuploc d' Q@☋fpuploc d P〉=fpuploc d 〈m⍇Q@☋P〉 but this leads to further           *)
 (* difficulties if P is composed of two sub-terms, having no reasons to admit the same *)
 (* value d'. Basically 〈⍇@〉 does not commute well with the other functions, and one may *)
 (* even say that 〈⍇@〉 is incompatible with other term functions, 〈m⍇P@☋☬☢(x∙π(m))〉 being *)
 (* reduced to 〈m⍇P@☋∀π(m)〉 then to ∀P for example...                                                                   *)

 Theorem ftrawprdid:(forall (P:☊)(m:☈), 〈m⍇π(m)@☋P〉 ≡P)*(forall (E:☌)(m:☈), 〈m⍇π(m)@☍E〉 ≡E).
 Proof.
  apply (Bst_ind (fun (P:☊)=>forall (m:☈), 〈m⍇π(m)@☋P〉 ≡P)
                             (fun (E:☌)=>forall (m:☈), 〈m⍇π(m)@☍E〉 ≡E)).
   intros m; apply refl_id.
   intros i; split; [split | idtac]; intros m; try (apply refl_id); simpl;
    destruct (deciequ m i); [rewrite (iequpt _ _ i0); rewrite i0 | rewrite (iequrf _ _ e)];
    apply refl_id.
   intros P HP; split; intros m; simpl; rewrite HP; apply refl_id.
   intros E HE; split; intros m; simpl; rewrite HE; apply refl_id.
   intros P1 P2 H1 H2; split; intros m; simpl; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros m; simpl; rewrite H1; rewrite H2; apply refl_id.
   intros E P HE HP m; simpl; rewrite HE; rewrite HP; apply refl_id.
 Qed.

 Definition fprawprdid:=fst ftrawprdid.
 Definition ferawprdid:=snd ftrawprdid.

(* Technical lemma to ease iffftrawprd proof ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem ifffpmaffexp:forall (M:☛)(P1 P2:☊), 〈〈M@☋P1☳P2〉〉 ≡ (〈〈M@☋P1〉〉☳〈〈M@☋P2〉〉).
 Proof.
  intros M  P1 P2; unfold fpmaffexp; unfold piff; fold fpmaffexp; apply refl_id.
 Qed.

 Theorem ifffpmaffexp':forall (M:☛)(P1 P2:☊), (〈〈M@☋P1〉〉☳〈〈M@☋P2〉〉) ≡ 〈〈M@☋P1☳P2〉〉.
 Proof.
  intros M  P1 P2; unfold fpmaffexp; unfold piff; fold fpmaffexp; apply refl_identity.
 Qed.

(* Raw predicate replacement equivalence ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem iffftrawprd:forall (Q Q':☊)(H:☐), H⊣Q☳Q'->H⌿⍀Q☳Q'->
                                       (forall (P:☊)(G:☐)(m:☈)(M:☛), H⊆☑G->G⊣ 〈〈M@☋〈m⍇Q@☋P〉 ☳ 〈m⍇Q'@☋P〉 〉〉)*
                                       (forall (E:☌)(G:☐)(m:☈)(M:☛), H⊆☑G->G⊣ 〈〈M@☋〈m⍇Q@☍E〉 ♃ 〈m⍇Q'@☍E〉 〉〉).
 Proof.
  intros Q Q' H Hiff Hpg;
   apply (Bdt_ind (fun (P:☊)=>forall (G:☐)(m:☈)(M:☛), H⊆☑G->G⊣ 〈〈M@☋〈m⍇Q@☋P〉 ☳ 〈m⍇Q'@☋P〉 〉〉)
                              (fun (E:☌)=>forall (G:☐)(m:☈)(M:☛), H⊆☑G->G⊣ 〈〈M@☋〈m⍇Q@☍E〉 ♃ 〈m⍇Q'@☍E〉 〉〉)).
   destruct s.
    (* Negation *)
    intros Hprd Hexp G m M Hinc; rewrite (ifffpmaffexp M); unfold fprawprd; fold fprawprd;
     unfold fpmaffexp; fold fpmaffexp; apply iffcontrapp; rewrite (ifffpmaffexp' M).
    apply (Hprd _ (reflige ☠?☋∣s∣) G m M Hinc).
    (* Universal quantification *)
    intros Hprd Hexp G m M Hinc; rewrite (ifffpmaffexp M); unfold fprawprd; fold fprawprd;
     unfold fpmaffexp; fold fpmaffexp; apply foriffrepifffor; rewrite (ifffpmaffexp' ☯☜(M)).
    destruct (exfgnewf (G☑★∀(〈〈☯☜(M)@☋(〈m⍇Q@☋s〉 ☳ 〈m⍇Q'@☋s〉)〉〉))).
    apply (dbfori _ _ _ g).
    unfold fpinfor; unfold fpprjfor;
     rewrite (sym_id (fmginstfpinloc 〈〈☯☜(M)@☋(〈m⍇Q@☋s〉 ☳ 〈m⍇Q'@☋s〉)〉〉 χ(x) ☦ _
                                                                (reflige ☰☋(〈〈☯☜(M)@☋(〈m⍇Q@☋s〉 ☳ 〈m⍇Q'@☋s〉)〉〉)))).
    destruct (exfmcmp (fmginst ☦ ☰☋(〈〈☯☜(M)@☋(〈m⍇Q@☋s〉 ☳ 〈m⍇Q'@☋s〉)〉〉) χ(x)) ☯☜(M)); destruct p;
     rewrite (i (〈m⍇Q@☋s〉 ☳ 〈m⍇Q'@☋s〉)); apply (Hprd _ (reflige ☠?☋∣s∣) G m x0 Hinc).
    (* Conjunction *)
    intros Hprd Hexp G m M Hinc; simpl in Hprd; rewrite (ifffpmaffexp M); unfold fprawprd;
     fold fprawprd; unfold fpmaffexp; fold fpmaffexp; BTprop.
     apply biins; apply iffrepgoal with (P:=〈〈M@☋ 〈m⍇Q@☋s1〉 〉〉).
      apply biins; apply (Hprd _ (genxt _ _ (fimaxgel ?☋∣s1∣ ?☋∣s2∣)) G m M Hinc).
      BTprop.
     apply swaphyp; apply biins; apply iffrepgoal with (P:=〈〈M@☋ 〈m⍇Q@☋s2〉 〉〉).
      apply biins; apply (Hprd _ (genxt _ _ (fimaxger ?☋∣s1∣ ?☋∣s2∣)) G m M Hinc).
      BTprop.
     apply biins; apply iffrepgoal with (P:=〈〈M@☋ 〈m⍇Q'@☋s1〉 〉〉).
      apply biins; apply symiff; apply (Hprd _ (genxt _ _ (fimaxgel ?☋∣s1∣ ?☋∣s2∣)) G m M Hinc).
      BTprop.
     apply swaphyp; apply biins; apply iffrepgoal with (P:=〈〈M@☋ 〈m⍇Q'@☋s2〉 〉〉).
      apply biins; apply symiff; apply (Hprd _ (genxt _ _ (fimaxger ?☋∣s1∣ ?☋∣s2∣)) G m M Hinc).
      BTprop.
    (* Implication *)
    intros Hprd Hexp G m M Hinc; simpl in Hprd; rewrite (ifffpmaffexp M); unfold fprawprd;
     fold fprawprd; unfold fpmaffexp; fold fpmaffexp; unfold piff; apply biandi;
     apply biimpi.
     apply iffrepimpl with (P:=〈〈M@☋ 〈m⍇Q@☋s1〉 〉〉).
      apply biins; apply (Hprd _ (genxt _ _ (fimaxgel ?☋∣s1∣ ?☋∣s2∣)) G m M Hinc).
      apply iffrepimpr with (Q:=〈〈M@☋ 〈m⍇Q@☋s2〉 〉〉).
       apply biins; apply (Hprd _ (genxt _ _ (fimaxger ?☋∣s1∣ ?☋∣s2∣)) G m M Hinc).
       BTprop.
     apply iffrepimpl with (P:=〈〈M@☋ 〈m⍇Q'@☋s1〉 〉〉).
      apply biins; apply symiff; apply (Hprd _ (genxt _ _ (fimaxgel ?☋∣s1∣ ?☋∣s2∣)) G m M Hinc).
      apply iffrepimpr with (Q:=〈〈M@☋ 〈m⍇Q'@☋s2〉 〉〉).
       apply biins; apply symiff; apply (Hprd _ (genxt _ _ (fimaxger ?☋∣s1∣ ?☋∣s2∣)) G m M Hinc).
       BTprop.
    (* Predicate variable *)
    intros Hprd Hexp G m M Hinc; rewrite (ifffpmaffexp M); simpl; destruct (m=☉☂b).
     rewrite (ifffpmaffexp' M); apply (biinc G H (〈〈M@☋Q☳Q'〉〉));
     [apply spgrfpmaffexp; [apply Hiff |  intros x Hx; apply Hpg] | apply Hinc].
     BTprop.
    (* Equality *)
    intros Hprd Hexp G m M Hinc; simpl in Hexp; rewrite (ifffpmaffexp M); unfold fprawprd;
     fold ferawprd; unfold fpmaffexp; fold femaffexp; unfold piff; apply biandi;
     apply biimpi.
     destruct (exfgnewf (∅☑☑★〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 ♃ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉));
      destruct (ginfinc _ _ _ g); clear g g0; inversion_clear p.
     replace (〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 ♃ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉) with
                    〈x♇☍ 〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 @☋ χ(x) ♃ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉 〉.
      apply bieque with (E:=〈〈M@☍ 〈m⍇Q@☍b〉 〉〉).
       apply biins; apply (Hexp _ (genxt _ _ (fimaxgel ?☍∣b∣ ?☍∣b0∣)) G m M Hinc).
       simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 _ H1).
       replace (〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 ♃ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉) with
                      〈x♇☍ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉 @☋ 〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 ♃ χ(x)〉.
        apply bieque with (E:=〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉).
         apply biins; apply (Hexp _ (genxt _ _ (fimaxger ?☍∣b∣ ?☍∣b0∣)) G m M Hinc).
         simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉 _ H0).
         BTprop.
        simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉 _ H0);
         apply refl_equal.
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 _ H1);
       apply refl_equal.
     destruct (exfgnewf (∅☑☑★〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 ♃ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉));
      destruct (ginfinc _ _ _ g); clear g g0; inversion_clear p.
     replace (〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 ♃ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉) with
                    〈x♇☍ 〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 @☋ χ(x) ♃ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉 〉.
      apply bieque with (E:=〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉).
       apply biins; apply symequ; apply (Hexp _ (genxt _ _ (fimaxgel ?☍∣b∣ ?☍∣b0∣)) G m M Hinc).
       simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 _ H1).
       replace (〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 ♃ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉) with
                      〈x♇☍ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉 @☋ 〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 ♃ χ(x)〉.
        apply bieque with (E:=〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉).
         apply biins; apply symequ; apply (Hexp _ (genxt _ _ (fimaxger ?☍∣b∣ ?☍∣b0∣)) G m M Hinc).
         simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉 _ H0).
         BTprop.
        simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉 _ H0);
         apply refl_equal.
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 _ H1);
       apply refl_equal.
    (* Membership *)
    intros Hprd Hexp G m M Hinc; simpl in Hexp; rewrite (ifffpmaffexp M); unfold fprawprd;
     fold ferawprd; unfold fpmaffexp; fold femaffexp; unfold piff; apply biandi;
     apply biimpi.
     destruct (exfgnewf (∅☑☑★〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 ∈ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉));
      destruct (ginfinc _ _ _ g); clear g g0; inversion_clear p.
     replace (〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 ∈ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉) with
                    〈x♇☍ 〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 @☋ χ(x) ∈ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉 〉.
      apply bieque with (E:=〈〈M@☍ 〈m⍇Q@☍b〉 〉〉).
       apply biins; apply (Hexp _ (genxt _ _ (fimaxgel ?☍∣b∣ ?☍∣b0∣)) G m M Hinc).
       simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 _ H1).
       replace (〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 ∈ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉) with
                      〈x♇☍ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉 @☋ 〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 ∈ χ(x)〉.
        apply bieque with (E:=〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉).
         apply biins; apply (Hexp _ (genxt _ _ (fimaxger ?☍∣b∣ ?☍∣b0∣)) G m M Hinc).
         simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉 _ H0).
         BTprop.
        simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉 _ H0);
         apply refl_equal.
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 _ H1);
       apply refl_equal.
     destruct (exfgnewf (∅☑☑★〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 ∈ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉));
      destruct (ginfinc _ _ _ g); clear g g0; inversion_clear p.
     replace (〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 ∈ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉) with
                    〈x♇☍ 〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 @☋ χ(x) ∈ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉 〉.
      apply bieque with (E:=〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉).
       apply biins; apply symequ; apply (Hexp _ (genxt _ _ (fimaxgel ?☍∣b∣ ?☍∣b0∣)) G m M Hinc).
       simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 _ H1).
       replace (〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 ∈ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉) with
                      〈x♇☍ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉 @☋ 〈〈M@☍ 〈m⍇Q'@☍b〉 〉〉 ∈ χ(x)〉.
        apply bieque with (E:=〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉).
         apply biins; apply symequ; apply (Hexp _ (genxt _ _ (fimaxger ?☍∣b∣ ?☍∣b0∣)) G m M Hinc).
         simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍b0〉 〉〉 _ H0).
         BTprop.
        simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍b0〉 〉〉 _ H0);
         apply refl_equal.
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍b〉 〉〉 _ H1);
       apply refl_equal.
   destruct t.
    (* BIG set *)
    intros Hexp Hprd G m M Hinc; simpl; apply biequi.
    (* Choice *)
    intros Hexp Hprd G m M Hinc; simpl.
    destruct (exfgnewf (∅☑☑★〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 ∈ 〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉));
      destruct (ginfinc _ _ _ g); clear g g0; inversion_clear p.
    replace ((☭ 〈〈M@☍ 〈m⍇Q@☍t〉 〉〉) ♃ (☭ 〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉)) with
                   〈x♇☍ 〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 @☋ (☭χ(x)) ♃  (☭ 〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉) 〉.
     apply bieque with (E:=〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉).
      apply symequ; apply (Hexp _ (reflige ☠?☍∣t∣) G m M Hinc).
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉 _ H1);
       apply biequi.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 _ H1);
      apply refl_equal.
    (* Powerset *)
    intros Hexp Hprd G m M Hinc; simpl.
    destruct (exfgnewf (∅☑☑★〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 ∈ 〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉));
      destruct (ginfinc _ _ _ g); clear g g0; inversion_clear p.
    replace ((☬ 〈〈M@☍ 〈m⍇Q@☍t〉 〉〉) ♃ (☬ 〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉)) with
                   〈x♇☍ 〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 @☋ (☬χ(x)) ♃  (☬ 〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉) 〉.
     apply bieque with (E:=〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉).
      apply symequ; apply (Hexp _ (reflige ☠?☍∣t∣) G m M Hinc).
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉 _ H1);
       apply biequi.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 _ H1);
      apply refl_equal.
    (* Maplet *)
    intros Hexp Hprd G m M Hinc; simpl; simpl in Hexp.
    destruct (exfgnewf (∅☑☑★〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ∈ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉
                                           ☑★〈〈M@☍ 〈m⍇Q'@☍t1〉 〉〉 ∈ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉));
     destruct (ginfinc _ _ _ g); destruct (ginfinc _ _ _ g0); clear g g0 g1; inversion_clear p;
     inversion_clear p0.
    replace ((〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ☵ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉)♃
                    (〈〈M@☍ 〈m⍇Q'@☍t1〉 〉〉 ☵ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉)) with
                   〈x♇☍ 〈〈M@☍ 〈m⍇Q'@☍t1〉 〉〉 @☋ (〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ☵ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉)♃
                                                                 (χ(x) ☵ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉)〉.
     apply bieque with (E:=〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉).
      apply (Hexp _ (genxt _ _ (fimaxgel ?☍∣t1∣ ?☍∣t2∣)) G m M Hinc).
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 _ H1);
       rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 _ H2);
       rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 _ H3).
      replace ((〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ☵ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉)♃
                     (〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ☵ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉)) with
                     〈x♇☍ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉 @☋ (〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ☵ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉)♃
                                                                   (〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ☵ χ(x))〉.
       apply bieque with (E:=〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉).
        apply (Hexp _ (genxt _ _ (fimaxger ?☍∣t1∣ ?☍∣t2∣)) G m M Hinc).
        simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉 _ H2);
         rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉 _ H3); apply biequi.
       simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉 _ H2);
         rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉 _ H3); apply refl_equal.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t1〉 〉〉 _ H1);
      rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t1〉 〉〉 _ H2);
      rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t1〉 〉〉 _ H3); apply refl_equal.
    (* Cartesian product *)
    intros Hexp Hprd G m M Hinc; simpl; simpl in Hexp.
    destruct (exfgnewf (∅☑☑★〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ∈ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉
                                           ☑★〈〈M@☍ 〈m⍇Q'@☍t1〉 〉〉 ∈ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉));
     destruct (ginfinc _ _ _ g); destruct (ginfinc _ _ _ g0); clear g g0 g1; inversion_clear p;
     inversion_clear p0.
    replace ((〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ♂ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉)♃
                    (〈〈M@☍ 〈m⍇Q'@☍t1〉 〉〉 ♂ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉)) with
                   〈x♇☍ 〈〈M@☍ 〈m⍇Q'@☍t1〉 〉〉 @☋ (〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ♂ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉)♃
                                                                 (χ(x) ♂ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉)〉.
     apply bieque with (E:=〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉).
      apply (Hexp _ (genxt _ _ (fimaxgel ?☍∣t1∣ ?☍∣t2∣)) G m M Hinc).
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 _ H1);
       rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 _ H2);
       rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 _ H3).
      replace ((〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ♂ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉)♃
                     (〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ♂ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉)) with
                     〈x♇☍ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉 @☋ (〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ♂ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉)♃
                                                                   (〈〈M@☍ 〈m⍇Q@☍t1〉 〉〉 ♂ χ(x))〉.
       apply bieque with (E:=〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉).
        apply (Hexp _ (genxt _ _ (fimaxger ?☍∣t1∣ ?☍∣t2∣)) G m M Hinc).
        simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉 _ H2);
         rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t2〉 〉〉 _ H3); apply biequi.
       simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉 _ H2);
         rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t2〉 〉〉 _ H3); apply refl_equal.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t1〉 〉〉 _ H1);
      rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t1〉 〉〉 _ H2);
      rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t1〉 〉〉 _ H3); apply refl_equal.
    (* BIG element *)
    intros Hexp Hprd G m M Hinc; simpl; apply biequi.
    (* Variable *)
    intros Hexp Hprd G m M Hinc; simpl; apply biequi.
    (* Comprehension set *)
    intros Hexp Hprd G m M Hinc; simpl; simpl in Hexp; simpl in Hprd.
    destruct (exfgnewf (G☑★(〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 ♀ 〈〈☯☜(M)@☋ 〈m⍇Q@☋b〉 〉〉) ♃
                                            (〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉 ♀ 〈〈☯☜(M)@☋ 〈m⍇Q'@☋b〉 〉〉)));
     destruct (ginfinc _ _ _ g); clear g; invset_clear p; invset_clear H0; invset_clear H1.
    replace ((〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 ♀ 〈〈☯☜(M)@☋ 〈m⍇Q@☋b〉 〉〉)♃
                    (〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉 ♀ 〈〈☯☜(M)@☋ 〈m⍇Q'@☋b〉 〉〉)) with
                   〈x♇☍ 〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉 @☋ (〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 ♀ 〈〈☯☜(M)@☋ 〈m⍇Q@☋b〉 〉〉)♃
                                                                 (χ(x) ♀ 〈〈☯☜(M)@☋ 〈m⍇Q'@☋b〉 〉〉)〉.
     apply bieque with (E:=〈〈M@☍ 〈m⍇Q@☍t〉 〉〉).
      apply (Hexp _ (genxt _ _ (fimaxgel ?☍∣t∣ ?☋∣b∣)) G m M Hinc).
      simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 _ H2);
       rewrite (fpaffexpnul _ ☯☍(〈〈M@☍ 〈m⍇Q@☍t〉 〉〉) _ H3);
       rewrite (fpaffexpnul _ ☯☍(〈〈M@☍ 〈m⍇Q@☍t〉 〉〉) _ H4).
     assert (Hmaj:{t:☈ & (☰☋(〈〈☯☜(M)@☋ 〈m⍇Q@☋b〉 〉〉)≤☉t)*(☰☋(〈〈☯☜(M)@☋ 〈m⍇Q'@☋b〉 〉〉)≤☉t)}).
      exists (☰☋(〈〈☯☜(M)@☋ 〈m⍇Q@☋b〉 〉〉) ☰☉ ☰☋(〈〈☯☜(M)@☋ 〈m⍇Q'@☋b〉 〉〉)); split;
       [apply fimaxgel | apply fimaxger].
     destruct Hmaj; destruct p.
     apply bieqsi;
      [apply (bipowi G x _ _ (infcmp _ 〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 〈〈☯☜(M)@☋ 〈m⍇Q@☋b〉 〉〉 H2 H3)
                                             (infcmp _ 〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 〈〈☯☜(M)@☋ 〈m⍇Q'@☋b〉 〉〉 H2 H4))
      | apply (bipowi G x _ _ (infcmp _ 〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 〈〈☯☜(M)@☋ 〈m⍇Q'@☋b〉 〉〉 H2 H4)
                                             (infcmp _ 〈〈M@☍ 〈m⍇Q@☍t〉 〉〉 〈〈☯☜(M)@☋ 〈m⍇Q@☋b〉 〉〉 H2 H3))];
      apply bifori; BTinf; apply biimpi; apply dbcmpi.
      apply dbcmpl with (P:=〈〈☯☜(M)@☋ 〈m⍇Q@☋b〉 〉〉); BTprop.
      unfold fpinfor; unfold fpprjfor; rewrite (sym_id (fmginstfpinloc _ χ(x) ☦ _ i0)).
      destruct (exfmcmp (fmginst ☦ x0 χ(x)) ☯☜(M)); destruct p; rewrite (i1 〈m⍇Q'@☋b〉).
      apply iffrepgoal with (P:=〈〈x1@☋〈m⍇Q@☋b〉 〉〉).
       apply biins; apply (Hprd b (genxt _ _ (fimaxger _ _)) G m x1 Hinc).
       replace 〈〈x1@☋〈m⍇Q@☋b〉 〉〉 with ☭☢(∀〈〈☯☜(M)@☋〈m⍇Q@☋b〉 〉〉←χ(x)⊢pif 〈〈☯☜(M)@☋〈m⍇Q@☋b〉 〉〉).
        apply dbcmpr with (S:=〈〈M@☍ 〈m⍇Q@☍t〉 〉〉); BTprop.
        unfold fpinfor; unfold fpprjfor; rewrite (sym_id (fmginstfpinloc _ χ(x) ☦ _ i));
         rewrite (sym_id (i1 〈m⍇Q@☋b〉)); apply refl_equal.
      apply dbcmpl with (P:=〈〈☯☜(M)@☋ 〈m⍇Q'@☋b〉 〉〉); BTprop.
      unfold fpinfor; unfold fpprjfor; rewrite (sym_id (fmginstfpinloc _ χ(x) ☦ _ i)).
      destruct (exfmcmp (fmginst ☦ x0 χ(x)) ☯☜(M)); destruct p; rewrite (i1 〈m⍇Q@☋b〉).
      apply iffrepgoal with (P:=〈〈x1@☋〈m⍇Q'@☋b〉 〉〉).
       apply biins; apply symiff; apply (Hprd b (genxt _ _ (fimaxger _ _)) G m x1 Hinc).
       replace 〈〈x1@☋〈m⍇Q'@☋b〉 〉〉 with ☭☢(∀〈〈☯☜(M)@☋〈m⍇Q'@☋b〉 〉〉←χ(x)⊢pif 〈〈☯☜(M)@☋〈m⍇Q'@☋b〉 〉〉).
        apply dbcmpr with (S:=〈〈M@☍ 〈m⍇Q@☍t〉 〉〉); BTprop.
        unfold fpinfor; unfold fpprjfor; rewrite (sym_id (fmginstfpinloc _ χ(x) ☦ _ i0));
         rewrite (sym_id (i1 〈m⍇Q'@☋b〉)); apply refl_equal.
     simpl; rewrite reflfiequ; rewrite (feaffexpnul _ 〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉 _ H2);
      rewrite (fpaffexpnul _ ☯☍(〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉) _ H3);
      rewrite (fpaffexpnul _ ☯☍(〈〈M@☍ 〈m⍇Q'@☍t〉 〉〉) _ H4); apply refl_equal.
 Qed.

 Theorem ifffprawprd:forall (P Q Q':☊)(G H:☐)(m:☈)(M:☛),
                                       H⊣Q☳Q'->H⌿⍀Q☳Q'->H⊆☑G->G⊣ 〈〈M@☋〈m⍇Q@☋P〉 ☳ 〈m⍇Q'@☋P〉 〉〉.
 Proof.
  intros P Q Q' G H m M Hiff Hpg; apply (fst (iffftrawprd Q Q' H Hiff Hpg)).
 Qed.

 Theorem ifffprawprd':forall (P Q Q':☊)(G:☐)(m:☈), ⊣Q☳Q'->G⊣ 〈m⍇Q@☋P〉 ☳ 〈m⍇Q'@☋P〉.
 Proof.
  intros P Q Q' G m Hiff.
  assert (Hpg:∅☑⌿⍀Q☳Q'). unfold spgr; intros i; left; apply ginfempty.
  rewrite (sym_id (fpaffexpmemp (〈m⍇Q@☋P〉 ☳ 〈m⍇Q'@☋P〉))).
  apply (fst (iffftrawprd Q Q' ∅☑ Hiff Hpg) P G m ∅☜ (empginc G)).
 Qed.

 Theorem iffferawprd:forall (E:☌)(Q Q':☊)(G H:☐)(m:☈)(M:☛),
                                       H⊣Q☳Q'->H⌿⍀Q☳Q'->H⊆☑G->G⊣ 〈〈M@☋〈m⍇Q@☍E〉 ♃ 〈m⍇Q'@☍E〉 〉〉.
 Proof.
  intros E Q Q' G H m M Hiff Hpg; apply (snd (iffftrawprd Q Q' H Hiff Hpg)).
 Qed.

 Theorem iffferawprd':forall (E:☌)(Q Q':☊)(G:☐)(m:☈), ⊣Q☳Q'->G⊣ 〈m⍇Q@☍E〉 ♃ 〈m⍇Q'@☍E〉.
 Proof.
  intros E Q Q' G m Hiff.
  assert (Hpg:∅☑⌿⍀Q☳Q'). unfold spgr; intros i; left; apply ginfempty.
  rewrite (sym_id (fpaffexpmemp (〈m⍇Q@☍E〉 ♃ 〈m⍇Q'@☍E〉))).
  apply (snd (iffftrawprd Q Q' ∅☑ Hiff Hpg) E G m ∅☜ (empginc G)).
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)