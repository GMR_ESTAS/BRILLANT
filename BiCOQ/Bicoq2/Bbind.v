(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Bbind, terms binding
   Definition of the binding functions, and related properties
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - Augist 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Binst.

(* Binding function, local version ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Bind the free variable v to a to-be added quantifier (at depth d) *)
 Fixpoint fpbdloc(v d:☈)(P:☊){struct P}:☊:=
  match P with
  | ¬P' => ¬ (fpbdloc v d P')
  | ∀P' => ∀ (fpbdloc ☠v ☠d P')
  | P1∧P2 => (fpbdloc v d P1) ∧ (fpbdloc v d P2)
  | P1☲P2 => (fpbdloc v d P1) ☲ (fpbdloc v d P2)
  | π i => π i
  | E1♃E2 => (febdloc v d E1) ♃ (febdloc v d E2)
  | E1∈E2 => (febdloc v d E1) ∈ (febdloc v d E2)
  end
 with febdloc(v d:☈)(E:☌){struct E}:☌:=
  match E with
  | ♄ => ♄
  | ☭E' => ☭ (febdloc v d E')
  | ☬E' => ☬ (febdloc v d E')
  | (E1☵E2) => ((febdloc v d E1) ☵ (febdloc v d E2))
  | (E1♂E2) => ((febdloc v d E1) ♂ (febdloc v d E2))
  | ω i => ω i
  | χ i => match d≤☉☂i with ⊤ => match (v=☉☂i) with ⊤ => χ d | ⊥ => χ ☠i end | ⊥ => χ i end
  | E'♀P' => (febdloc v d E')♀(fpbdloc ☠v ☠d P')
  end.

 (* Binding of a non-free variable has no effect *)
 Theorem ftbdlocnulf:(forall (P:☊)(v d:☈), v⍀☋☂P≡⊤->fpbdloc v d P≡fpuploc d P)*
                                       (forall (E:☌)(v d:☈), v⍀☍☂E≡⊤->febdloc v d E≡feuploc d E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (v d:☈), v⍀☋☂P≡⊤->fpbdloc v d P≡fpuploc d P)
                             (fun E:☌=>forall (v d:☈), v⍀☍☂E≡⊤->febdloc v d E≡feuploc d E)); simpl.
   intros v d H; apply refl_id.
   intros i; split; [split | idtac]; intros v d H; try (apply refl_id); destruct (decige d i).
    rewrite (igept _ _ i0); simpl; rewrite (dedfbnott _ H); apply refl_id.
    rewrite (igerf _ _ e); simpl; apply refl_id.
   intros P H; split; intros i d H0; [rewrite (H _ d H0) | rewrite (H _ ☠d H0)]; apply refl_id.
  intros E H; split; intros i d H0; rewrite (H _ d H0); apply refl_id.
  intros P1 P2 H1 H2; split; intros i d H0; destruct (dedfbandt _ _ H0); rewrite (H1 _ d i0);
   rewrite (H2 _ d i1); apply refl_id.
  intros E1 E2 H1 H2; split; split; intros i d H0; destruct (dedfbandt _ _ H0);
   rewrite (H1 _ d i0); rewrite (H2 _ d i1); apply refl_id.
  intros E P H1 H2 i d H0; destruct (dedfbandt _ _ H0); rewrite (H1 _ d i0);
   rewrite (H2 _ ☠d i1); apply refl_id.
 Qed.

 Definition fpbdlocnulf:=fst ftbdlocnulf.
 Definition febdlocnulf:=snd ftbdlocnulf.

 Theorem fpbdlocnul:forall (P:☊)(v d:☈), v⍀☋P->fpbdloc v d P≡fpuploc d P.
 Proof.
  intros P v d H; apply (fpbdlocnulf _ _ d (pinfpt _ _ H)).
 Qed.

 Theorem febdlocnul:forall (E:☌)(v d:☈), v⍀☍E->febdloc v d E≡feuploc d E.
 Proof.
  intros E v d H; apply (febdlocnulf _ _ d (einfpt _ _ H)).
 Qed.

 (* Binding of a variable removes it *)
 Theorem ftbdlocifrf:(forall (P:☊)(v d:☈), d≤☉v->☠v⍀☋☂fpbdloc v d P≡⊤)*
                                       (forall (E:☌)(v d:☈), d≤☉v->☠v⍀☍☂febdloc v d E≡⊤).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (v d:☈), d≤☉v->☠v⍀☋☂fpbdloc v d P≡⊤)
                             (fun E:☌=>forall (v d:☈), d≤☉v->☠v⍀☍☂febdloc v d E≡⊤)); simpl.
   intros v d H; apply refl_id.
   intros i; split; [split | idtac]; intros v d H; try (apply refl_id); destruct (decige d i).
    rewrite (igept _ _ i0); destruct (deciequ v i).
     rewrite (iequpt _ _ i1); simpl; destruct d.
      apply refl_id.
      destruct (deciequ v d);
       [rewrite <- (sym_id i2) in H; destruct (nreflnige _ H)
       | rewrite (iequrf _ _ e); apply refl_id].
     rewrite (iequrf _ _ e); simpl; rewrite (iequrf _ _ e); apply refl_id.
    rewrite (igerf _ _ e); simpl; destruct i.
     apply refl_id.
     destruct (deciequ v i).
      rewrite <- (sym_id i0) in H; generalize (transigtige _ _ _ (nigeigt _ _ e) H); intros H0;
       invset H0; generalize (nxtrige _ _ H2); intros H4; destruct (nreflnige _ H4).
      rewrite (iequrf _ _ e0); apply refl_id.
   intros P H; split; intros i d H0; [apply (H _ _ H0) | apply (H _ _ (genxt _ _ H0))].
  intros E H; split; intros i d H0; apply (H _ _ H0).
  intros P1 P2 H1 H2; split; intros i d H0; rewrite (H1 _ _ H0); rewrite (H2 _ _ H0);
   apply refl_id.
  intros E1 E2 H1 H2; split; split; intros i d H0; rewrite (H1 _ _ H0); rewrite (H2 _ _ H0);
   apply refl_id.
  intros E P H1 H2 i d H0; rewrite (H1 _ _ H0); rewrite (H2 _ _ (genxt _ _ H0)); apply refl_id.
 Qed.

 Definition fpbdlocifrf:=fst ftbdlocifrf.
 Definition febdlocifrf:=snd ftbdlocifrf.

 Theorem fpbdlocifr:forall (P:☊)(v d:☈), d≤☉v->☠v⍀☋fpbdloc v d P.
 Proof.
  intros P v d H; apply (pinftp _ _ (fpbdlocifrf P _ _ H)).
 Qed.

 Theorem febdlocifr:forall (E:☌)(v d:☈), d≤☉v->☠v⍀☍febdloc v d E.
 Proof.
  intros E v d H; apply (einftp _ _ (febdlocifrf E _ _ H)).
 Qed.

 (* Binding does not introduce free variables *)
 Theorem ftbdlocinff:(forall (P:☊)(f v d:☈), d≤☉f->f⍀☋☂P≡⊤->☠f⍀☋☂fpbdloc v d P≡⊤)*
                                       (forall (E:☌)(f v d:☈), d≤☉f->f⍀☍☂E≡⊤->☠f⍀☍☂febdloc v d E≡⊤).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (f v d:☈), d≤☉f->f⍀☋☂P≡⊤->☠f⍀☋☂fpbdloc v d P≡⊤)
                             (fun E:☌=>forall (f v d:☈), d≤☉f->f⍀☍☂E≡⊤->☠f⍀☍☂febdloc v d E≡⊤)); simpl.
   intros f v d H H0; apply refl_id.
   intros i; split; [split | idtac]; intros f v d H H0; try (apply refl_id);
    destruct (decige d i).
    rewrite (igept _ _ i0); destruct (deciequ v i).
     rewrite (iequpt _ _ i1); simpl; destruct d.
      apply refl_id.
      destruct (deciequ f d);
       [ rewrite <- (sym_id i2) in H; destruct (nreflnige _ H)
       | rewrite (iequrf _ _ e); apply refl_id].
     rewrite (iequrf _ _ e); simpl; apply H0.
    rewrite (igerf _ _ e); simpl; destruct i.
     apply refl_id.
     destruct (deciequ f i).
      rewrite <- (sym_id i0) in H; generalize (transigtige _ _ _ (nigeigt _ _ e) H); intros H1;
       invset H1; generalize (nxtrige _ _ H3); intros H5; destruct (nreflnige _ H5).
      rewrite (iequrf _ _ e0); apply refl_id.
   intros P HP; split; intros f v d H H0;
    [apply (HP _ v _ H H0) | apply (HP _ ☠v _ (genxt _ _ H) H0)].
   intros E HE; split; intros f v d H H0; apply (HE _ v _ H H0).
   intros P1 P2 H1 H2; split; intros f v d H H0; destruct (dedfbandt _ _ H0); 
    rewrite (H1 _ v _ H i); rewrite (H2 _ v _ H i0); apply refl_id.
   intros E1 E2 H1 H2; split; split;  intros f v d H H0; destruct (dedfbandt _ _ H0);
    rewrite (H1 _ v _ H i); rewrite (H2 _ v _ H i0); apply refl_id.
   intros E P H1 H2 f v d H H0; destruct (dedfbandt _ _ H0); rewrite (H1 _ v _ H i);
    rewrite (H2 _ ☠v _ (genxt _ _ H) i0); apply refl_id.
 Qed.

 Definition fpbdlocinff:=fst ftbdlocinff.
 Definition febdlocinff:=snd ftbdlocinff.

 Theorem fpbdlocinf:forall (P:☊)(f v d:☈), d≤☉f->f⍀☋P->☠f⍀☋fpbdloc v d P.
 Proof.
  intros P f v d H H0; apply (pinftp _ _ (fpbdlocinff _ _ v _ H (pinfpt _ _ H0))).
 Qed.

 Theorem febdlocinf:forall (E:☌)(f v d:☈), d≤☉f->f⍀☍E->☠f⍀☍febdloc v d E.
 Proof.
  intros E f v d H H0; apply (einftp _ _ (febdlocinff _ _ v _ H (einfpt _ _ H0))).
 Qed.

 (* fpbdloc and febdloc are the inverse of fpinloc and feinloc *)
 Theorem ftbdlocin:(forall (P:☊)(v d:☈), fpinloc d χ(v) (fpbdloc v d P)≡P)*
                                   (forall (E:☌)(v d:☈), feinloc d χ(v) (febdloc v d E)≡E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (v d:☈), fpinloc d χ(v) (fpbdloc v d P)≡P)
                             (fun E:☌=>forall (v d:☈), feinloc d χ(v) (febdloc v d E)≡E)); simpl.
   intros v d; apply refl_id.
   intros i; split; [split | idtac]; intros v d; try (apply refl_id); destruct (decige d i).
    rewrite (igept _ _ i0); destruct (deciequ v i).
     rewrite (iequpt _ _ i1); rewrite i1; simpl; rewrite (igept _ _ (reflige d));
      rewrite (reflfiequ d); apply refl_id.
     rewrite (iequrf _ _ e); simpl; rewrite (igept _ _ (nxtrige _ _ i0));
      destruct (deciequ d ☠i).
      rewrite <- (sym_id i1) in i0; destruct (nreflnige _ i0).
      rewrite (iequrf _ _ e0); apply refl_id.
    rewrite (igerf _ _ e); simpl; rewrite (igerf _ _ e); apply refl_id.
   intros P HP; split; intros v d;
    [idtac | unfold feuplft; simpl]; rewrite HP; apply refl_id.
   intros E HE; split; intros v d; rewrite HE; apply refl_id.
   intros P1 P2 H1 H2; split; intros v d; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros v d; rewrite H1; rewrite H2; apply refl_id.
   intros E P H1 H2 v d; rewrite H1; unfold feuplft; simpl; rewrite H2; apply refl_id.
 Qed.

 Definition fpbdlocin:=fst ftbdlocin.
 Definition febdlocin:=snd ftbdlocin.

 (* fpinloc and feinloc are the inverse of fpbdloc and febdloc *)
 Theorem ftinlocbdf:(forall (P:☊)(v d:☈), d≤☉v->☠v⍀☋☂P≡⊤->fpbdloc v d (fpinloc d (χ v) P)≡P)*
                                     (forall (E:☌)(v d:☈), d≤☉v->☠v⍀☍☂E≡⊤->febdloc v d (feinloc d (χ v) E)≡E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (v d:☈), d≤☉v->☠v⍀☋☂P≡⊤->fpbdloc v d (fpinloc d (χ v) P)≡P)
                             (fun E:☌=>forall (v d:☈), d≤☉v->☠v⍀☍☂E≡⊤->febdloc v d (feinloc d (χ v) E)≡E));
   simpl.
   intros v d H H0; apply refl_id.
   intros i; split; [split | idtac]; intros v d H H0; try (apply refl_id);
    destruct (decige d i).
    rewrite (igept _ _ i0); destruct (deciequ d i).
     rewrite (iequpt _ _ i1); rewrite i1; rewrite <- (sym_id i1) in H; simpl;
      rewrite (igept _ _ H); rewrite (reflfiequ v); apply refl_id.
     rewrite (iequrf _ _ e); destruct i; simpl.
      invset i0; destruct e; rewrite H1; apply refl_id.
      generalize (igeneq _ _ e i0); intros H1; invset H1; rewrite (igept _ _ H4);
       rewrite (dedfbnott _ H0); apply refl_id.
    rewrite (igerf _ _ e); simpl; rewrite (igerf _ _ e); apply refl_id.
   intros P HP; split; intros v d H H0;
    [rewrite (HP _ _ H H0) | unfold feuplft; simpl; rewrite (HP _ _ (genxt _ _ H) H0)];
    apply refl_id.
  intros E HE; split; intros v d H H0; rewrite (HE _ _ H H0); apply refl_id.
  intros P1 P2 H1 H2; split; intros v d H H0; destruct (dedfbandt _ _ H0); rewrite (H1 _ _ H i);
   rewrite (H2 _ _ H i0); apply refl_id.
  intros E1 E2 H1 H2; split; split; intros v d H H0; destruct (dedfbandt _ _ H0);
   rewrite (H1 _ _ H i); rewrite (H2 _ _ H i0); apply refl_id.
  intros E P H1 H2 v d H H0; destruct (dedfbandt _ _ H0); rewrite (H1 _ _ H i); unfold feuplft;
   simpl; rewrite (H2 _ _ (genxt _ _ H) i0); apply refl_id.
 Qed.

 Definition fpinlocbdf:=fst ftinlocbdf.
 Definition feinlocbdf:=snd ftinlocbdf.

 Theorem fpinlocbd:forall (P:☊)(v d:☈), d≤☉v->☠v⍀☋P->fpbdloc v d (fpinloc d χ(v) P)≡P.
 Proof.
  intros P v d H H0; apply (fpinlocbdf _ _ _ H (pinfpt _ _ H0)).
 Qed.

 Theorem feinlocbd:forall (E:☌)(v d:☈), d≤☉v->☠v⍀☍E->febdloc v d (feinloc d χ(v) E)≡E.
 Proof.
  intros E v d H H0; apply (feinlocbdf _ _ _ H (einfpt _ _ H0)).
 Qed.

 (* fpbdloc and febdloc does not create π-dependance *)
 Theorem ftbdlocftnpd:(forall (P:☊)(v d p:☈), fpnpd p (fpbdloc v d P)≡fpnpd p P)*
                                         (forall (E:☌)(v d p:☈), fenpd p (febdloc v d E)≡fenpd p E).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (v d p:☈), fpnpd p (fpbdloc v d P)≡fpnpd p P)
                             (fun E:☌=>forall (v d p:☈), fenpd p (febdloc v d E)≡fenpd p E)); simpl.
   intros v d p; apply refl_id.
   intros i; split; [split | idtac]; intros v d p; try (apply refl_id);
    destruct (decige d i).
    rewrite (igept _ _ i0); destruct (deciequ v i);
     [rewrite (iequpt _ _ i1) | rewrite (iequrf _ _ e)]; apply refl_id.
    rewrite (igerf _ _ e); apply refl_id.
   intros P HP; split; intros v d p; apply HP.
  intros E HE; split; intros v d p; apply HE.
  intros P1 P2 H1 H2; split; intros v d p; rewrite H1; rewrite H2; apply refl_id.
  intros E1 E2 H1 H2; split; split; intros v d p; rewrite H1; rewrite H2; apply refl_id.
  intros E P H1 H2 v d p; rewrite H1; rewrite H2; apply refl_id.
 Qed.

 Definition fpbdlocfpnpd:=fst ftbdlocftnpd.
 Definition febdlocfenpd:=snd ftbdlocftnpd.

(* ∀-Binding function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fpbdfor(v:☈)(P:☊):☊:=∀(fpbdloc v ☦ P).
 Notation "'☬☢(' v '∙' P ')'":=(fpbdfor v P).

 (* Generation of ?☢ proofs *)
 Definition fbf:=fun (v:☈)(P:☊)=>pif (fpbdloc v ☦ P).

 Theorem fpbdforisfor:forall (v:☈)(P:☊), ?∀(☬☢(v∙P)).
 Proof.
  intros v P; unfold fpbdfor; apply pif.
 Qed.

 Theorem fpbdfornul:forall (P:☊)(v:☈), v⍀☋P->☬☢(v∙P)≡∀☯☋(P).
 Proof.
  intros P v H; unfold fpbdfor; unfold fpuplft; rewrite (fpbdlocnul _ _ ☦ H); apply refl_id.
 Qed.

 Theorem fpbdforifr:forall (P:☊)(v:☈), v⍀☋ ☬☢(v∙P).
 Proof.
  intros P v; unfold fpbdfor; apply inffor; apply fpbdlocifr; apply gefst.
 Qed.

 Theorem fpbdforinf:forall (P:☊)(f v:☈), f⍀☋P->f⍀☋ ☬☢(v∙P).
 Proof.
  intros P f v H; unfold fpbdfor; apply inffor; apply fpbdlocinf; [apply gefst | apply H].
 Qed.

 Theorem fpbdforin:forall (P:☊)(v:☈), ☭☢(☬☢(v∙P)←χ(v)⊢fbf v P)≡P.
 Proof.
  intros P v; unfold fpbdfor; unfold fpinfor; simpl; apply fpbdlocin.
 Qed.

 Theorem fpinforbd:forall (P:☊)(v:☈), v⍀☋ ∀P->☬☢(v∙☭☢(∀P←χ(v)⊢pif P))≡∀P.
 Proof.
  intros P v H; invset_clear H; unfold fpbdfor; unfold fpinfor; simpl;
   rewrite (fpinlocbd _ _ _ (gefst v) H0); apply refl_id.
 Qed.

 Theorem fpbdforfpnpd:forall (P:☊)(v p:☈), fpnpd p ☬☢(v∙P)≡fpnpd p P.
 Proof.
  intros P v p; unfold fpbdfor; simpl; apply fpbdlocfpnpd.
 Qed.

(* ∃-Binding function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fpbdexs(v:☈)(P:☊):☊:=∃(fpbdloc v ☦ P).
 Notation "'☬☣(' v '∙' P ')'":=(fpbdexs v P).

 (* Generation of ?☣ proofs *)
 Definition fbe:=fun (v:☈)(P:☊)=>pie (fpbdloc v ☦ P).

 Theorem fpbdexsisexs:forall (v:☈)(P:☊), ?∃(☬☣(v∙P)).
 Proof.
  intros v P; unfold fpbdexs; unfold pexs; apply pie.
 Qed.

 Theorem fpbdexsnul:forall (P:☊)(v:☈), v⍀☋P->☬☣(v∙P)≡∃☯☋(P).
 Proof.
  intros P v H; unfold fpbdexs; unfold fpuplft; rewrite (fpbdlocnul _ _ ☦ H); apply refl_id.
 Qed.

 Theorem fpbdexsifr:forall (P:☊)(v:☈), v⍀☋ ☬☣(v∙P).
 Proof.
  intros P v; unfold fpbdexs; unfold pexs; apply infnot; apply inffor; apply infnot;
   apply fpbdlocifr; apply gefst.
 Qed.

 Theorem fpbdexsinf:forall (P:☊)(f v:☈), f⍀☋P->f⍀☋ ☬☣(v∙P).
 Proof.
  intros P f v H; unfold fpbdexs; unfold pexs; apply infnot; apply inffor; apply infnot;
   apply fpbdlocinf; [apply gefst | apply H].
 Qed.

 Theorem fpbdexsin:forall (P:☊)(v:☈), ☭☣( ☬☣(v∙P)←χ(v)⊢fbe v P)≡P.
 Proof.
  intros P v; unfold fpbdexs; unfold fpinexs; unfold pexs; simpl; apply fpbdlocin.
 Qed.

 Theorem fpinexsbd:forall (P:☊)(v:☈), v⍀☋ ∃P->☬☣(v∙☭☣(∃P←χ(v)⊢pie P))≡∃P.
 Proof.
  intros P v H; invset_clear H; invset_clear H0; invset_clear H; unfold fpbdexs;
   unfold fpinexs; unfold pexs; simpl; rewrite (fpinlocbd _ _ _ (gefst v) H0); apply refl_id.
 Qed.

 Theorem fpbdexsfpnpd:forall (P:☊)(v p:☈), fpnpd p ☬☣(v∙P)≡fpnpd p P.
 Proof.
  intros P v p; unfold fpbdexs; simpl; apply fpbdlocfpnpd.
 Qed.

(* Rewriting rule to swap between ☬☣ and ☬☢ ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem fpbdforexs:forall (P:☊)(x:☈), ☬☣(x∙P)≡¬ ☬☢(x∙¬P).
 Proof.
  intros P x; unfold fpbdexs; unfold pexs; unfold fpbdfor; apply refl_id.
 Qed.

(* ♀-Binding function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fpbdcmp(v:☈)(S:☌)(P:☊):☌:=S♀(fpbdloc v ☦ P).
 Notation "'☬☤(' v '∙' S '∙' P ')'":=(fpbdcmp v S P).

 (* Generation of ?☤ proofs *)
 Definition fbc:=fun (v:☈)(S:☌)(P:☊)=>eic S (fpbdloc v ☦ P).

 Theorem fpbdcmpiscmp:forall (v:☈)(S:☌)(P:☊), ?♀(☬☤(v∙S∙P)).
 Proof.
  intros v S P; unfold fpbdcmp; apply eic.
 Qed.

 Theorem fpbdcmpnul:forall (S:☌)(P:☊)(v:☈), v⍀☋P->☬☤(v∙S∙P)≡S♀☯☋(P).
 Proof.
  intros S P v H; unfold fpbdcmp; unfold fpuplft; rewrite (fpbdlocnul _ _ ☦ H);
   apply refl_id.
 Qed.

 Theorem fpbdcmpifr:forall (S:☌)(P:☊)(v:☈), v⍀☍S->v⍀☍ ☬☤(v∙S∙P).
 Proof.
  intros S P v H; unfold fpbdcmp; apply infcmp;
   [apply H | apply fpbdlocifr; apply gefst].
 Qed.

 Theorem fpbdcmpinf:forall (S:☌)(P:☊)(f v:☈), f⍀☍S->f⍀☋P->f⍀☍ ☬☤(v∙S∙P).
 Proof.
  intros S P f v H H0; unfold fpbdcmp; apply infcmp;
   [apply H | apply fpbdlocinf; [apply gefst | apply H0]].
 Qed.

 Theorem fpbdcmpin:forall (S:☌)(P:☊)(v:☈), ☭☤(☬☤(v∙S∙P)←χ(v)⊢fbc v S P)≡χ(v)∈S∧P.
 Proof.
  intros S P v; unfold fpbdcmp; unfold feincmp; simpl; rewrite fpbdlocin; apply refl_id.
 Qed.

 Theorem fpincmpbd:forall (S:☌)(P:☊)(v:☈), v⍀☍S♀P->☬☤(v∙S∙☭☤(S♀P←χ(v)⊢eic S P))≡S♀(χ(☦)∈ ☯☍(S)∧P).
 Proof.
  intros S P v H; invset_clear H; unfold fpbdcmp; unfold feincmp; simpl;
   rewrite (fpinlocbd _ _ _ (gefst v) H1); rewrite (reflfiequ v); 
   rewrite (febdlocnul _ _ ☦ H0); apply refl_id.
 Qed.

 Theorem fpbdcmpfpnpd:forall (S:☌)(P:☊)(v p:☈), fenpd p ☬☤(v∙S∙P)≡fenpd p S∧☁fpnpd p P.
 Proof.
  intros S P v p; unfold fpbdcmp; simpl; rewrite fpbdlocfpnpd; apply refl_id.
 Qed.

(* Closure ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Repeat ☬☢ over free variables to obtain a ground term.                                                         *)

 Definition fpcatchfor(P:☊)(i:☈):☊:=match i⍀☋☂P with ⊤ => P | ⊥ => ☬☢(i∙P) end.

 Theorem fpcatchforinf:forall (P:☊)(i:☈), i⍀☋fpcatchfor P i.
 Proof.
  intros P i; unfold fpcatchfor; destruct (decpinf i P);
   [rewrite (pinfpt _ _ p); apply p | rewrite (pinfrf _ _ e); apply fpbdforifr].
 Qed.

 Theorem fpcatchforifr:forall (P:☊)(i j:☈), j⍀☋P->j⍀☋fpcatchfor P i.
 Proof.
  intros P i j H; unfold fpcatchfor; destruct (decpinf i P);
   [rewrite (pinfpt _ _ p) | rewrite (pinfrf _ _ e); apply fpbdforinf]; apply H.
 Qed.

 Theorem fpcatchformaj:forall (i j:☈)(P:☊), j>☋P->j>☋fpcatchfor P i.
 Proof.
  intros i j P H; apply igepmaj; intros k H0; apply fpcatchforifr; apply pmajinf;
   apply (pmajige P _ _ H0 H).
 Qed.

 Theorem fpcatchforground:forall (P:☊), *⍀☋(P)->forall (i:☈), fpcatchfor P i≡P.
 Proof.
  intros P H i; generalize (pgroundinf _ H); intros H0; unfold fpcatchfor;
   rewrite (pinfpt _ _ (H0 i)); apply refl_id.
 Qed.

 Theorem fpcatchforfpnpd:forall (P:☊)(i p:☈), fpnpd p (fpcatchfor P i)≡fpnpd p P.
 Proof.
  intros P i p; unfold fpcatchfor; destruct (i⍀☋☂P); [idtac | rewrite (fpbdforfpnpd P i p)];
   apply refl_id.
 Qed.

 Fixpoint fpcatchforall(P:☊)(i:☈){struct i}:☊:=
  match i with ☦ => fpcatchfor P i | ☠i' => fpcatchforall (fpcatchfor P i) i' end.

 Theorem fpcatchforallinf:forall (i j:☈)(P:☊), j⍀☋P->j⍀☋fpcatchforall P i.
 Proof.
  induction i; simpl; intros j P H; unfold fpcatchfor.
   destruct (☦⍀☋☂P); [idtac | apply fpbdforinf]; apply H.
   destruct (☠i⍀☋☂P); [apply (IHi _ _ H) | apply IHi; apply fpbdforinf; apply H].
 Qed.

 Theorem fpcatchforallinf':forall (i:☈)(P:☊), i⍀☋fpcatchforall P i.
 Proof.
  induction i; simpl; intros P; [idtac | apply fpcatchforallinf]; apply fpcatchforinf.
 Qed.

 Theorem fpcatchforallinf'':forall (i j:☈)(P:☊), j≤☉i->j⍀☋fpcatchforall P i.
 Proof.
  induction i; simpl; intros j P H.
   invset_clear H; apply fpcatchforinf.
   destruct (deciequ j ☠i).
    rewrite i0; apply fpcatchforallinf; apply fpcatchforinf.
    apply IHi; destruct (decige j i).
     apply i0.
     destruct e; apply (igeeq _ _ H); intros H0; apply e0; invset_clear H0; apply H1.
 Qed.

 Theorem fpcatchforallmaj:forall (i j:☈)(P:☊), j>☋P->j>☋fpcatchforall P i.
 Proof.
  induction i; simpl; intros j P H; [idtac | apply IHi]; apply fpcatchformaj; apply H.
 Qed.

 Theorem fpcatchforallground:forall (P:☊), *⍀☋(P)->forall (i:☈), fpcatchforall P i≡P.
 Proof.
  intros P H; induction i; simpl;
   [apply (fpcatchforground _ H) | rewrite (fpcatchforground _ H ☠i); apply IHi].
 Qed.

 Theorem fpcatchforallfpnpd:forall (i p:☈)(P:☊), fpnpd p (fpcatchforall P i)≡fpnpd p P.
 Proof.
  induction i; simpl; intros p P; unfold fpcatchforall.
   apply fpcatchforfpnpd.
   unfold fpcatchfor; destruct (decpinf ☠i P);
    [rewrite (pinfpt _ _ p0); apply IHi
    | rewrite (pinfrf _ _ e); fold fpcatchforall; rewrite (IHi p ☬☢(☠i∙P));
      apply fpbdforfpnpd].
 Qed.

 Definition fpgroundfor(P:☊):☊:=fpcatchforall P ☰☋(P).
 Notation "'☰☢(' P ')'":=(fpgroundfor P).

 Theorem fpgroundforground:forall (P:☊), *⍀☋(☰☢(P)).
 Proof.
  intros P; apply infpground; unfold fpgroundfor; intros i; destruct (decige i ☰☋(P)).
   apply fpcatchforallinf''; apply i0.
   apply pmajinf; apply fpcatchforallmaj; apply (pmajige P _ _ (asymige _ _ e));
    apply fpnewfmaj.
 Qed.

 Theorem fpgroundfpgroundfor:forall (P:☊), *⍀☋(P)->☰☢(P)≡P.
 Proof.
  intros P H; unfold fpgroundfor; apply fpcatchforallground; apply H.
 Qed.

 Theorem fpgroundforfpnpd:forall (P:☊)(v p:☈), fpnpd p ☰☢(P)≡fpnpd p P.
 Proof.
  intros P v p; unfold fpgroundfor; apply fpcatchforallfpnpd.
 Qed.

(* Commutation Lemma ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Theorem ftbdlocftuploc:(forall (P:☊)(d d' v:☈),
                                              d'≤☉d->fpbdloc ☠v ☠d (fpuploc d' P)≡fpuploc d' (fpbdloc v d P))*
                                             (forall (E:☌)(d d' v:☈),
                                              d'≤☉d->febdloc ☠v ☠d (feuploc d' E)≡feuploc d' (febdloc v d E)).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (d d' v:☈),
                                              d'≤☉d->fpbdloc ☠v ☠d (fpuploc d' P)≡fpuploc d' (fpbdloc v d P))
                             (fun E:☌=>forall (d d' v:☈),
                                              d'≤☉d->febdloc ☠v ☠d (feuploc d' E)≡feuploc d' (febdloc v d E)));
   simpl.
   intros d d' v Hd; apply refl_id.
   intros i; split; [split | idtac]; intros d d' v Hd; try (apply refl_id);
    destruct (decige d i).
    rewrite (igept _ _ i0); rewrite (igept _ _ (transige _ _ _ Hd i0)); destruct (deciequ v i).
     rewrite (iequpt _ _ i1); simpl; rewrite (igept _ _ i0); rewrite (igept _ _ Hd);
      apply refl_id.
     rewrite (iequrf _ _ e); simpl; rewrite (igept _ _ i0); 
      rewrite (igept _ _ (nxtrige _ _ (transige _ _ _ Hd i0))); apply refl_id.
    rewrite (igerf _ _ e); destruct (decige d' i).
     rewrite (igept _ _ i0); simpl; rewrite (igerf _ _ e); rewrite (igept _ _ i0); apply refl_id.
     rewrite (igerf _ _ e0); simpl; rewrite (igerf _ _ e0); destruct i; simpl.
      apply refl_id.
      destruct (decige d i).
       destruct e; apply nxtrige; apply i0.
       rewrite (igerf _ _ e1); apply refl_id.
   intros P HP; split; intros d d' v Hd;
    [rewrite (HP d d' v Hd) | rewrite (HP ☠d ☠d' ☠v (genxt _ _ Hd))]; apply refl_id.
   intros E HE; split; intros d d' v Hd; rewrite (HE d d' v Hd); apply refl_id.
   intros P1 P2 H1 H2; split; intros d d' v Hd; rewrite (H1 d d' v Hd); rewrite (H2 d d' v Hd);
    apply refl_id.
   intros E1 E2 H1 H2; split; split; intros d d' v Hd; rewrite (H1 d d' v Hd);
    rewrite (H2 d d' v Hd); apply refl_id.
   intros E P H1 H2 d d' v Hd; rewrite (H1 d d' v Hd); rewrite (H2 ☠d ☠d' ☠v (genxt _ _ Hd));
    apply refl_id.
 Qed.

 Definition fpbdlocfpuploc:=fst ftbdlocftuploc.
 Definition febdlocfeuploc:=snd ftbdlocftuploc.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
