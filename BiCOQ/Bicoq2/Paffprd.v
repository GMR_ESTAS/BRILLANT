(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Paffprd, prover functions associated to Baffprd results
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : June 2006 - October 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Baffprd.

(* Prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace a complex ☳ goal by a simpler ☳ sub-goal *)
 Definition fbksubiff(S:☒)(x:☈)(Q P P':☊):[☒*]:=
  ∅☓☓★(match S with ☬☓(G,R) => match R=☋☂〈x♇☋P@☋Q〉☳〈x♇☋P'@☋Q〉 with ⊤ => ☬☓(G,P☳P') | ⊥ => S end end).

 Theorem corrfbksubiff:forall (S:☒)(x:☈)(Q P P':☊), ∧☓(fbksubiff S x Q P P')->☭☓(S).
 Proof.
  induction S; unfold fbksubiff; intros x Q P P' H; destruct (lsinfins _ _ H);
   clear H l; destruct (decpequ b0 (〈x♇☋P@☋Q〉☳〈x♇☋P'@☋Q〉)).
    rewrite <- (sym_id (pequpt _ _ i)) in s; rewrite i; simpl; apply (ifffpaffprd Q P P' b x s).
    rewrite <- (sym_id (pequrf _ _ e)) in s; apply s.
 Qed.

 (* Replace a complex ♃ goal by a simpler ☳ sub-goal *)
 Definition fbksubequ(S:☒)(x:☈)(E:☌)(P P':☊):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,R) =>
          match R=☋☂〈x♇☋P@☍E〉♃ 〈x♇☋P'@☍E〉 with ⊤ => ☬☓(G,P☳P') | ⊥ => S end end).

 Theorem corrfbksubequ:forall (S:☒)(x:☈)(E:☌)(P P':☊), ∧☓(fbksubequ S x E P P')->☭☓(S).
 Proof.
  induction S; unfold fbksubequ; intros x E P P' H; destruct (lsinfins _ _ H);
   clear H l; destruct (decpequ b0 (〈x♇☋P@☍E〉♃ 〈x♇☋P'@☍E〉)).
   rewrite <- (sym_id (pequpt _ _ i)) in s; rewrite i; simpl; apply (ifffeaffprd E P P' b x s).
   rewrite <- (sym_id (pequrf _ _ e)) in s; apply s.
 Qed.

 (* Replace a goal by a double goal, one being an ☳ *)
 Definition fbkrepiff(S:☒)(x:☈)(Q P P':☊):[☒*]:=
  match S with ☬☓(G,R) =>
   match R=☋☂〈x♇☋P'@☋Q〉 with ⊤ => ∅☓☓★☬☓(G,〈x♇☋P@☋Q〉)☓★☬☓(G,P☳P') | ⊥ => ∅☓☓★S end end.

 Theorem corrfbkrepiff:forall (S:☒)(x:☈)(Q P P':☊), ∧☓(fbkrepiff S x Q P P')->☭☓(S).
 Proof.
  induction S; unfold fbkrepiff; intros x Q P P' H; destruct (decpequ b0 〈x♇☋P'@☋Q〉).
   rewrite <- (sym_id (pequpt _ _ i)) in H; rewrite i; destruct (lsinfins _ _ H);
    destruct (lsinfins _ _ l); simpl; apply (ifffpaffprd' Q P P' b x s s0).
   rewrite <- (sym_id (pequrf _ _ e)) in H; destruct (lsinfins _ _ H); apply s.
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
