(*☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Pinfer, inference rules
   The inference rules defined in Binfer are associated to so-called prover functions
   allowing manipulation of sequents according to inference rules (with adequate
   correction proofs).
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : October 2005 - October 2006
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Load Mlist.
 Require Export Binfer.

(* Sequents ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Sequents definition just provides an easy way to build "proposals" ; the inductive    *)
(* definition is non-dependant, there is a single, non-conditional constructor. One     *)
(* may look at Bseq as the type of all possible sequents, while Binf is the type of all   *)
(* provable ones...                                                                                                                             *)

 Inductive Bseq:Set:=sbd:☐->☊->Bseq.
 Notation "☒":=Bseq.
 Notation "'☬☓(' G ',' P ')'":=(sbd G P).

 (* Transformation of a sequent into a proof *)
 Definition sinf(S:☒):Set:=match S with ☬☓(G,P) => G⊣P end.
 Notation "'☭☓(' S ')'":=(sinf S).

(* Sequent equality ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition fsequ(S1 S2:☒):=
  match S1 with ☬☓(G1,P1) => match S2 with ☬☓(G2,P2) => (P1=☋☂P2) ∧☁ (G1=☑☂G2) end end.
 Infix "=☓☂":=fsequ (at level 20, right associativity).

 Theorem impsequ:(identity (A:=☒) ☵☨☂fsequ).
 Proof.
  unfold Implem2; intros S1 S2; split; generalize S1 S2; clear S1 S2; induction S1;
   induction S2; simpl; intros H.
   destruct (dedfbandt _ _ H); rewrite (pequtp _ _ i); rewrite (gequtp _ _ i0);
    apply refl_id.
   intros H0; invset H0; rewrite <- (sym_id H2) in H; rewrite <- (sym_id H3) in H;
    rewrite <- (sym_id (pequpt _ _ (refl_id b2))) in H;
    rewrite <- (sym_id (gequpt _ _ (refl_id b1))) in H; invset H.
 Qed.

 Definition decsequ:=decimp2 impsequ.
 Definition sequpt:=imp2pt impsequ.
 Definition sequrf:=imp2rf impsequ.
 Definition sequtp:=imp2tp impsequ.
 Definition sequfr:=imp2fr impsequ.

(* Lists of sequents ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* Lists of sequents are used as object in the proof process; starting from a single      *)
(* goal (a sequent), application of inference rules leads to multiple intermediate        *)
(* goals who have to be proved. The proof process is completed when the list is empty.    *)

 (* Type *)
 Definition Blsq:=Blst ☒. Notation "[☒*]":=(Blst ☒).
 Definition lsemp:=lemp ☒. Notation "∅☓":=(lemp ☒).
 Definition lsins:=lins ☒. Notation "L '☓★' S":=(lins ☒ L S) (at level 50).

 (* Membership *)
 Definition lsiyes:=liyes ☒.
 Definition lsinxt:=linxt ☒.
 Definition lsin:=lin ☒. Notation "S '∈☓' L":=(lin ☒ S L) (at level 50).

 Definition lsinemp:=linemp ☒.
 Definition caselsin:=caselin ☒.

 (* Membership function *)
 Definition flsin:=flin ☒ fsequ. Notation "P '∈☓☂' G":=(flsin ☒ fsequ P G) (at level 50).

 Definition implsin:=implin ☒ fsequ impsequ.
 Definition declsin:=declin ☒ fsequ impsequ.
 Definition lsinpt:=linpt ☒ fsequ impsequ.
 Definition lsinrf:=linrf ☒ fsequ impsequ.
 Definition lsintp:=lintp ☒ fsequ impsequ.
 Definition lsinfr:=linfr ☒ fsequ impsequ.
 
 (* Inclusion *)
 Definition lsinc:=linc ☒. Notation "L '⊆☓' M":=(linc ☒ L M) (at level 50).

 Definition refllsinc:=refllinc ☒.
 Definition translsinc:=translinc ☒.
 Definition emplsinc:=emplinc ☒.
 Definition lsincover:=lincover ☒.
 Definition lsincemp:=lincemp ☒.
 Definition lsincsub:=lincsub ☒.
 Definition lsincdbl:=lincdbl ☒ fsequ impsequ.
 Definition lsincswp:=lincswp ☒.
 Definition lsincnot:=lincnot ☒ fsequ impsequ.

 (* Inclusion function *)
 Definition flsinc:=flinc ☒ fsequ. Notation "L '⊆☓☂' M":=(flinc ☒ fsequ L M) (at level 50).

 Definition implsinc:=implinc ☒ fsequ impsequ.
 Definition declsinc:=declinc ☒ fsequ impsequ.
 Definition lsincpt:=lincpt ☒ fsequ impsequ.
 Definition lsincrf:=lincrf ☒ fsequ impsequ.
 Definition lsinctp:=linctp ☒ fsequ impsequ.
 Definition lsincfr:=lincfr ☒ fsequ impsequ.

 (* Remove element function *)
 Definition flsrem:=flrem ☒ fsequ. Notation "L '-☓' S":=(flrem ☒ fsequ L S) (at level 50).

 Definition flsremin:=flremin ☒ fsequ impsequ.
 Definition flsreminc:=flreminc ☒ fsequ impsequ.
 Definition inflsrem:=inflrem ☒ fsequ impsequ.
 Definition lsinceqrem:=linceqrem ☒ fsequ impsequ.

 (* Concatenation of two lists of sequents *)
 Definition flsconc:=flapp ☒. Notation "L1 '+☓' L2":=(flapp ☒ L1 L2) (at level 50).

 Definition flsconcincr:=flappincr ☒.
 Definition flsconcincl:=flappincl ☒.
 Definition flsconcin:=flappin ☒.
 Definition flsconcnin:=flappnin ☒.

(* Transformation of a list of sequents into a logical proposition ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition lsinf:=lpmap1 ☒ sinf.
 Notation "'∧☓(' L ')'":=(lsinf L).

 Theorem lsinfemp:∧☓(∅☓).
 Proof.
  unfold lsinf; unfold lpmap1; intros d H; generalize (lsinpt _ _ H); simpl; intros H0;
   invset H0; discriminate H1.
 Qed.

 Theorem inslsinf:forall (S:☒)(L:[☒*]), ∧☓(L)* ☭☓(S)->∧☓(L☓★S).
 Proof.
  intros S L H; destruct H; unfold lsinf; unfold lpmap1; intros S' H1;
   destruct (caselsin _ _ _ H1); [rewrite i; apply s | apply l; apply l0].
 Qed.

 Theorem lsinfins:forall (S:☒)(L:[☒*]), ∧☓(L☓★S)->∧☓(L)* ☭☓(S).
 Proof.
  intros S L H; split; unfold lsinf in H; unfold lpmap1 in H.
   unfold lsinf; intros S' H0; apply H; apply lsinxt; apply H0.
   apply H; apply lsiyes.
 Qed.

 Theorem lsinfconc:forall (L1 L2:[☒*]), ∧☓(L1+☓L2)->∧☓(L1)* ∧☓(L2).
 Proof.
  induction L1; intros L2 H.
   split; [apply lsinfemp | apply H].
   simpl in H; destruct (IHL1 _ H); destruct (lsinfins _ _ l0); split;
    [apply inslsinf; split; [apply l | apply s] | apply l1].
 Qed.

 Theorem conclsinf:forall (L1 L2:[☒*]), ∧☓(L1)* ∧☓(L2)->∧☓(L1+☓L2).
 Proof.
  induction L1; intros L2 H; destruct H; simpl.
   apply l0.
   apply IHL1; destruct (lsinfins _ _ l); split;
    [apply l1 | apply inslsinf; split; [apply l0 | apply s]].
 Qed.

(* biin prover function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* The first so-called prover function are described thereafter, for the rule biin.      *)
(* Prover functions operates on sequents, by emulating inference rules application,    *)
(* either backward (replace the current goal by a list of new goals from which the          *)
(* former can be derived) or forward (introduce new hypothesis by using hypothesis).      *)
(* In this case, the backward application of biin check if the goal predicate appears   *)
(* in the proof environment, then returning the empty list of sequents. The forward       *)
(* application is not provided but would allow hypothesis duplication.                            *)

 Definition fbkin(S:☒):[☒*]:=
  match S with ☬☓(G,P) => match (P∈☑☂G) with ⊤ => ∅☓ | ⊥ => ∅☓☓★S end end.

 Theorem corrfbkin:forall (S:☒), ∧☓(fbkin S)->☭☓(S).
 Proof.
  induction S; unfold fbkin; destruct (decgin b0 b).
   rewrite (ginpt _ _ l); simpl; intros H; apply biin; apply l.
   rewrite (ginrf _ _ e); simpl; intros H; destruct (lsinfins _ _ H); apply s.
 Qed.

(* biinc prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace the current hypothesis of the sequent S by a subset G *)
 Definition fbkinc(S:☒)(G:☐):[☒*]:=
  ∅☓☓★(match S with ☬☓(H,P) => match (G⊆☑☂H) with ⊤ => ☬☓(G,P) | ⊥ => S end end).

 Theorem corrfbkinc:forall (S:☒)(G:☐), ∧☓(fbkinc S G)->☭☓(S).
 Proof.
  induction S; unfold fbkinc; intros G H; destruct (lsinfins _ _ H); clear H l;
   destruct (decginc b G); simpl; unfold flinc in s.
   rewrite <- (sym_id (gincpt _ _ l)) in s; apply (biinc b _ _ s l).
   rewrite <- (sym_id (gincrf _ _ e)) in s; apply s.
 Qed.

 (* Remove all occurrences of an hypothesis *)
 Definition fbkrem(S:☒)(P:☊):[☒*]:=match S with ☬☓(G,Q) => ∅☓☓★ ☬☓(G-☑P,Q) end.

 Theorem corrfbkrem:forall (S:☒)(P:☊), ∧☓(fbkrem S P)->☭☓(S).
 Proof.
  induction S; intros P; unfold fbkrem; intros H; destruct (lsinfins _ _ H); clear H l;
   apply (biinc _ _ _ s (fgreminc b P)).
 Qed.

(* biimp prover function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Introduce a new hypothesis that has to be proved - a.k.a. cut rule *)
 Definition fbkimp(S:☒)(P:☊):[☒*]:=match S with ☬☓(G,Q) => ∅☓☓★☬☓(G,P) ☓★ ☬☓(G☑★P,Q) end.

 Theorem corrfbkimp:forall (S:☒)(P:☊), ∧☓(fbkimp S P)->☭☓(S).
 Proof.
  induction S; intros P; unfold fbkimp; simpl; intros H; destruct (lsinfins _ _ H);
   clear H; destruct (lsinfins _ _ l); clear l l0; apply (biimp _ _ _ s0 s).
 Qed.

(* biandi prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Split an ∧ goal *)
 Definition fbkandi(S:☒):[☒*]:=match S with ☬☓(G,P1∧P2) => ∅☓ ☓★ ☬☓(G,P1) ☓★ ☬☓(G,P2) | _ => ∅☓ ☓★ S end.

 Theorem corrfbkandi:forall (S:☒), ∧☓(fbkandi S)->☭☓(S).
 Proof.
  induction S; destruct b0; simpl; intros H; destruct (lsinfins _ _ H); try (apply s).
   clear H; destruct (lsinfins _ _ l); clear l l0; apply (biandi _ _ _ s0 s).
 Qed.

(* biandl and biandr prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Combine the goal and a predicate into an ∧ *)
 Definition fbkandl(S:☒)(Q:☊):[☒*]:=match S with ☬☓(G,P) => ∅☓☓★☬☓(G,P∧Q) end.

 Theorem corrfbkandl:forall (S:☒)(Q:☊), ∧☓(fbkandl S Q)->☭☓(S).
 Proof.
  induction S; intros Q; unfold fbkandl; simpl; intros H; destruct (lsinfins _ _ H);
   apply (biandl _ _ _ s).
 Qed.

 (* Combine a predicate and the goal into an ∧ *)
 Definition fbkandr(S:☒)(P:☊):[☒*]:=match S with ☬☓(G,Q) => ∅☓☓★☬☓(G,P∧Q) end.

 Theorem corrfbkandr:forall (S:☒)(P:☊), ∧☓(fbkandr S P)->☭☓(S).
 Proof.
  induction S; intros Q; unfold fbkandr; simpl; intros H; destruct (lsinfins _ _ H);
   apply (biandr _ _ _ s).
 Qed.

(* biimpi prover function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Decompose an ☲ goal *)
 Definition fbkimpi(S:☒):[☒*]:=∅☓ ☓★ (match S with ☬☓(G,P☲Q) => ☬☓(G☑★P,Q) | _ => S end).

 Theorem corrfbkimpi:forall (S:☒), ∧☓(fbkimpi S)->☭☓(S).
 Proof.
  induction S; destruct b0; unfold fbkimpi; simpl; intros H; destruct (lsinfins _ _ H);
   try (apply s); clear H l; apply (biimpi _ _ _ s).
 Qed.

(* biimpe prover function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Swap an hypothesis to create an ☲ goal  *)
 Definition fbkimpe(S:☒):[☒*]:=∅☓ ☓★ (match S with ☬☓(G☑★P,Q) => ☬☓(G,P☲Q) | _ => S end).

 Theorem corrfbkimpe:forall (S:☒), ∧☓(fbkimpe S)->☭☓(S).
 Proof.
  induction S; destruct b; unfold fbkimpe; simpl; intros H; destruct (lsinfins _ _ H);
   clear H; [apply s | apply (biimpe _ _ _ s)].
 Qed.

(* biabsn and biabsp prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition smartneg(P:☊):☊:=match P with ¬P' => P' | _ => ¬P end.
 Notation "'#(' T ')'":=(smartneg T).

 (* Proof by absurd, using P as the new goal *)
 Definition fbkabs(S:☒)(P:☊):[☒*]:=
  match S with ☬☓(G,Q) => ∅☓ ☓★ ☬☓(G☑★#(Q),P) ☓★ ☬☓(G☑★#(Q),#(P)) end.

 Theorem corrfbkabsn:forall (S:☒)(P:☊), ∧☓(fbkabs S P)->☭☓(S).
 Proof.
  induction S; intros P; unfold fbkabs; simpl; intros H; destruct (lsinfins _ _ H);
   clear H; destruct (lsinfins _ _ l); clear l l0; destruct b0; destruct P; simpl in s;
   simpl in s0; try (apply (biabsp _ _ _ s s0)); try (apply (biabsp _ _ _ s0 s));
   try (apply (biabsn _ _ _ s s0)); try (apply (biabsn _ _ _ s0 s)).
 Qed.

(* bifori prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace a goal of the form ☬☢(f∙P) by P *)
 Definition fbkfori(S:☒)(f:☈)(P:☊):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,P') => match f⍀☑☂G with
                                                | ⊤ => match P'=☋☂☬☢(f∙P) with ⊤ =>  ☬☓(G,P) | ⊥ => S end
                                                | ⊥ => S
                                                end end).

 Theorem corrfbkfori:forall (S:☒)(f:☈)(P:☊), ∧☓(fbkfori S f P)->☭☓(S).
 Proof.
  induction S; intros f P; unfold fbkfori; simpl; destruct (decginf f b).
   unfold fginf; rewrite (ginfpt _ _ l); destruct (decpequ b0 ☬☢(f∙P)).
    rewrite (pequpt _ _ i); rewrite i; intros H; destruct (lsinfins _ _ H);
     apply (bifori _ _ _ l s).
    rewrite (pequrf _ _ e); intros H; destruct (lsinfins _ _ H); apply s.
   unfold fginf; rewrite (ginfrf _ _ e); intros H; destruct (lsinfins _ _ H); apply s.
 Qed.

(* bifore prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace a goal of the form [f♇☍E@☋P] by ☬☢(f∙P) *)
 Definition fbkfore(S:☒)(f:☈)(E:☌)(P:☊):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,P') => match P'=☋☂〈f♇☍E@☋P〉 with ⊤ => ☬☓(G,☬☢(f∙P)) | ⊥ => S end end).

 Theorem corrfbkfore:forall (S:☒)(f:☈)(E:☌)(P:☊), ∧☓(fbkfore S f E P)->☭☓(S).
 Proof.
  induction S; intros f E P; unfold fbkfore; destruct (decpequ b0 〈f♇☍E@☋P〉).
   rewrite (pequpt _ _ i); rewrite i; intros H; destruct (lsinfins _ _ H);
    apply (bifore _ _ _ E s).
   rewrite (pequrf _ _ e); intros H; destruct (lsinfins _ _ H); apply s.
 Qed.

(* biequi prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Check if the goal is a trivial equality *)
 Definition fbkequi(S:☒):[☒*]:=
  match S with ☬☓(G,E1♃E2) => match (E1=☍☂E2) with ⊤ => ∅☓ | ⊥ => ∅☓☓★S end | _ => ∅☓☓★S end.

 Theorem corrfbkequi:forall (S:☒), ∧☓(fbkequi S)->☭☓(S).
 Proof.
  induction S; destruct b0; unfold fbkequi; simpl; intros H;
   try (destruct (lsinfins _ _ H); apply s); destruct (deceequ b0 b1).
   rewrite i; apply biequi.
   rewrite <- (sym_id (eequrf _ _ e)) in H; simpl in H; destruct (lsinfins _ _ H); apply s.
 Qed.

(* bieque prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace a goal of the form [f♇☍F@☋P] by [f♇☍E@☋P] and E♃F *)
 Definition fbkeque(S:☒)(f:☈)(E F:☌)(P:☊):[☒*]:=
  match S with ☬☓(G,P') => match P'=☋☂〈f♇☍F@☋P〉 with
                                         | ⊤ => ∅☓ ☓★ ☬☓(G,E♃F) ☓★ ☬☓(G,〈f♇☍E@☋P〉)
                                         | ⊥ => ∅☓ ☓★ S
                                         end end.

 Theorem corrfbkeque:forall (S:☒)(f:☈)(E F:☌)(P:☊), ∧☓(fbkeque S f E F P)->☭☓(S).
 Proof.
  induction S; intros f E F P; unfold fbkeque; destruct (decpequ b0 〈f♇☍F@☋P〉).
   rewrite (pequpt _ _ i); rewrite i; intros H; destruct (lsinfins _ _ H);
    destruct (lsinfins _ _ l); apply (bieque _ _ P E F s0 s).
   rewrite (pequrf _ _ e); intros H; destruct (lsinfins _ _ H); apply s.
 Qed.

(* biproi prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace a goal of the form E∈(T♂U) *)
 Definition fbkproi(S:☒)(f1 f2:☈):[☒*]:=
  ∅☓ ☓★ (match S with
          | ☬☓(G,E∈(T♂U)) => match (f1⍀☋☂E∈(T♂U))∧☁(¬☁(f1=☉☂f2))∧☁(f2⍀☋☂E∈(T♂U)) with
                                     | ⊤ => ☬☓(G,☬☣(f1∙☬☣(f2∙χ(f1)∈T ∧ χ(f2)∈U ∧ E♃(χ(f1)☵χ(f2)))))
                                     | ⊥ => S
                                    end
          | _ => S
         end).

 Theorem corrfbkproi:forall (S:☒)(f1 f2:☈), ∧☓(fbkproi S f1 f2)->☭☓(S).
 Proof.
  induction S; intros f1 f2; unfold fbkproi; intros H; destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b1;
   destruct (lsinfins _ _ H); clear l H; try (apply s); destruct (decpinf f1 (b0∈(b1_1♂b1_2))).
   rewrite <- (sym_id (pinfpt _ _ p)) in s; destruct (deciequ f1 f2).
    rewrite <- (sym_id (iequpt _ _ i)) in s; simpl; apply s.
    rewrite <- (sym_id (iequrf _ _ e)) in s; destruct (decpinf f2 (b0∈(b1_1♂b1_2))).
     rewrite <- (sym_id (pinfpt _ _ p0)) in s; simpl; apply (biproi b _ _ _ _ _ p e p0); apply s.
     rewrite <- (sym_id (pinfrf _ _ e0)) in s; simpl; apply s.
   rewrite <- (sym_id (pinfrf _ _ e)) in s; simpl; apply s.
 Qed.

(* bipowi prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace a goal S∈☬T by ☬☢(f∙χf∈S☲χf∈T) *)
 Definition fbkpowi(S:☒):[☒*]:=
  match S with ☬☓(G,S' ∈ ☬T') => let f:=☰☑(∅☑ ☑★ S' ∈ T') in ∅☓☓★☬☓(G,☬☢(f∙(χ f) ∈ S'☲(χ f) ∈ T'))
                       | _ => ∅☓☓★S end.

 Theorem corrfbkpowi:forall (S:☒), ∧☓(fbkpowi S)->☭☓(S).
 Proof.
  induction S; destruct b0; unfold fbkpowi; intros H;
   try (destruct (lsinfins _ _ H); apply s).
   destruct b1; try (destruct (lsinfins _ _ H); apply s).
   simpl; apply bipowi with (f:=☰☑(∅☑ ☑★ b0 ∈ b1)).
    assert (H0:forall (i:☈), i⍀☋(b0∈b1)->i⍀☍b0). intros i H0; invset_clear H0; apply H1.
    apply H0; apply fgnewfinf; BTin.
    assert (H0:forall (i:☈), i⍀☋(b0∈b1)->i⍀☍b1). intros i H0; invset_clear H0; apply H2.
    apply H0; apply fgnewfinf; BTin.
   destruct (lsinfins _ _ H); apply s.
 Qed.

(* bipowe prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace a goal ☬☢(f∙χf∈S☲χf∈T) by S∈☬T *)
 Definition fbkpowe(S:☒)(T U:☌):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,∀(χ☦∈T'☲χ☦∈U')) => match (T' =☍☂ ☯☍(T)) with
                                                                    | ⊤ => match (U'=☍☂ ☯☍(U)) with ⊤ => ☬☓(G,T∈ ☬U) | ⊥ => S end
                                                                    | ⊥ => S
                                                                    end
                              | _ => S
                              end).

 Theorem corrfbkpowe:forall (S:☒)(T U:☌), ∧☓(fbkpowe S T U)->☭☓(S).
 Proof.
  induction S; intros T U; destruct b0; unfold fbkpowe; intros H;
   try (destruct (lsinfins _ _ H); apply s); destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b0_1;
   try (destruct (lsinfins _ _ H); apply s); destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b0; destruct b0_2;
   try (destruct (lsinfins _ _ H); apply s); destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b0; destruct (deceequ b1 ☯☍(T)).
    rewrite <- (sym_id (eequpt _ _ i)) in H; rewrite i; rewrite <- (sym_id i) in H; clear i b1;
     destruct (deceequ b2 ☯☍(U)).
     rewrite <- (sym_id (eequpt _ _ i)) in H; rewrite i; clear i b2;
      destruct (exfgnewf (∅☑☑★T∈U)); destruct (ginfinc _ _ _ g); clear g0; invset p.
     replace (∀(χ(☦)∈ ☯☍(T) ☲ χ(☦)∈ ☯☍(U))) with ☬☢(x∙χ(x)∈T☲χ(x)∈U).
      destruct (lsinfins _ _ H); apply (bipowe b _ _ _ H3 H4 s).
      unfold fpbdfor; unfold fpbdloc; rewrite (reflfiequ x); rewrite (igept _ _ (gefst x));
       fold febdloc.
      rewrite (febdlocnul T x ☦ H3); rewrite (febdlocnul U x ☦ H4); unfold feuplft; apply refl_equal.
     rewrite <- (sym_id (eequrf _ _ e)) in H; destruct (lsinfins _ _ H); apply s.
    rewrite <- (sym_id (eequrf _ _ e)) in H; destruct (lsinfins _ _ H); apply s.
   destruct (lsinfins _ _ H); apply s.
  destruct (lsinfins _ _ H); apply s.
 Qed.

(* bicmpi prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace a goal E∈☬☤(f∙S∙P) by E∈S and [f♇☍E@☋P] *)
 Definition fbkcmpi(S:☒)(f:☈)(P:☊):[☒*]:=
  match S with
  | ☬☓(G,E'∈(T'♀P')) => match P'=☋☂(fpbdloc f ☦ P) with
                                | ⊤ =>∅☓ ☓★ ☬☓(G,E'∈T') ☓★ ☬☓(G,〈f♇☍E'@☋P〉)
                                | ⊥ => ∅☓ ☓★ S end
  | _ => ∅☓ ☓★ S end.

 Theorem corrfbkcmpi:forall (S:☒)(f:☈)(P:☊), ∧☓(fbkcmpi S f P)->☭☓(S).
 Proof.
  induction S; intros f P; unfold fbkcmpi; intros H; simpl; destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b1;
   try (destruct (lsinfins _ _ H); apply s); destruct (decpequ b2 (fpbdloc f ☦ P)).
   rewrite <- (sym_id (pequpt _ _ i)) in H; rewrite i; destruct (lsinfins _ _ H);
    destruct (lsinfins _ _ l); clear H l l0; apply (bicmpi b f P b0 b1).
    apply s0.
    apply s.
   rewrite <- (sym_id (pequrf _ _ e)) in H; destruct (lsinfins _ _ H); apply s.
 Qed.

(* bicmpl and bicmpr prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace a goal E∈T by E∈☬☤(f∙T∙P) *)
 Definition fbkcmpl(S:☒)(f:☈)(P:☊):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,E'∈S') => ☬☓(G,E'∈ ☬☤(f∙S'∙P)) | _ => S end).

 Theorem corrfbkcmpl:forall (S:☒)(f:☈)(P:☊), ∧☓(fbkcmpl S f P)->☭☓(S).
 Proof.
  induction S; intros f P; unfold fbkcmpl; intros H; destruct b0; destruct (lsinfins _ _ H);
   clear H l;try (apply s); apply (bicmpl b f P b0 b1); apply s.
 Qed.

 (* Replace a goal 〈f♇☍E@☋P〉 by E∈☬☤(f∙S∙P) *)
 Definition fbkcmpr(S:☒)(f:☈)(E T:☌)(P:☊):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,P') => match 〈f♇☍E@☋P〉=☋☂P' with ⊤ => ☬☓(G,E ∈ ☬☤(f∙T∙P)) | ⊥ => S end end).

 Theorem corrfbkcmpr:forall (S:☒)(f:☈)(E T:☌)(P:☊), ∧☓(fbkcmpr S f E T P)->☭☓(S).
 Proof.
  induction S; intros f E T P; unfold fbkcmpr; intros H; destruct (lsinfins _ _ H); clear H l;
   destruct (decpequ 〈f♇☍E@☋P〉 b0).
   rewrite <- (sym_id (pequpt _ _ i)) in s; simpl; rewrite (sym_id i); apply (bicmpr b f P E T);
    apply s.
   rewrite <- (sym_id (pequrf _ _ e)) in s; simpl; apply s.
 Qed.

(* bieqsi prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace a goal S♃T by S∈(☬T) and T∈(☬S) *)
 Definition fbkeqsi(S:☒):[☒*]:=
  match S with ☬☓(G,T♃U) => ∅☓ ☓★ ☬☓(G,T∈(☬U)) ☓★ ☬☓(G,U∈(☬T)) | _ => ∅☓ ☓★ S end.

 Theorem corrfbkeqsi:forall (S:☒), ∧☓(fbkeqsi S)->☭☓(S).
 Proof.
  induction S; unfold fbkeqsi; intros H; destruct b0; destruct (lsinfins _ _ H); clear H;
   try (apply s); destruct (lsinfins _ _ l); clear l l0.
   apply (bieqsi _ _ _ s0 s).
 Qed.

(* bichsi prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Replace a goal (☭S)∈S by ☬☣(f∙χf∈S) *)
 Definition fbkchsi(S:☒):[☒*]:=
  ∅☓ ☓★ (match S with ☬☓(G,(☭T) ∈T') => match (T=☍☂T') with ⊤ => ☬☓(G,∃(χ ☦) ∈ ☯☍(T)) | ⊥ => S end
                              | _ => S end).

 Theorem corrfbkchsi:forall (S:☒), ∧☓(fbkchsi S)->☭☓(S).
 Proof.
  induction S; unfold fbkchsi; intros H; destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct (deceequ b0 b1).
   rewrite <- (sym_id (eequpt _ _ i)) in H; rewrite <- (sym_id i) in H; rewrite i;
    apply (bichsi b _ _ (fenewfinf b1)); unfold fpbdexs; simpl; rewrite (reflfiequ ☰☍(b1));
    rewrite (febdlocnul _ _ ☦ (fenewfinf b1)); destruct (lsinfins _ _ H); apply s.
   rewrite <- (sym_id (eequrf _ _ e)) in H; destruct (lsinfins _ _ H); apply s.
 Qed.

(* bibigi prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Concludes for a goal (ω f) ∈ ♄ *)
 Definition fbkbigi(S:☒):[☒*]:=match S with ☬☓(G,(ω f) ∈ ♄) => ∅☓ | _ => ∅☓ ☓★ S end.

 Theorem corrfbkbigi:forall (S:☒), ∧☓(fbkbigi S)->☭☓(S).
 Proof.
  induction S; unfold fbkchsi; intros H; destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b1;
   try (destruct (lsinfins _ _ H); apply s); simpl; apply bibigi.
 Qed.

(* bibied prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Concludes for a goal (ω f) ♃☃ (ω g) *)
 Definition fbkbied(S:☒):[☒*]:=
  match S with ☬☓(G,¬((ω f) ♃ (ω g))) => match (f=☉☂g) with ⊤ => ∅☓ ☓★ S | ⊥ => ∅☓ end | _ => ∅☓ ☓★ S end.

 Theorem corrfbkbied:forall (S:☒), ∧☓(fbkbied S)->☭☓(S).
 Proof.
  induction S; unfold fbkchsi; intros H; destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b0;
   try (destruct (lsinfins _ _ H); apply s); destruct b1;
   try (destruct (lsinfins _ _ H); apply s); destruct (deciequ b0 b1).
   rewrite <- (sym_id i) in H; rewrite i; simpl in H; rewrite <- (sym_id (reflfiequ b1)) in H;
    destruct (lsinfins _ _ H); apply s.
   apply (bibied b b0 b1); apply e.
 Qed.

(* bicpll and bicplr prover functions ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 (* Combine an ♃ goal into a ♃ between maplets *)
 Definition fbkcpll(S:☒)(F F':☌):[☒*]:=
  ∅☓☓★(match S with ☬☓(G,E♃E') => ☬☓(G,(E☵F)♃(E'☵F')) | _ => S end).

 Theorem corrfbkcpll:forall (S:☒)(F F':☌), ∧☓(fbkcpll S F F')->☭☓(S).
 Proof.
  induction S; induction b0; intros F F'; unfold fbkcpll; simpl; intros H;
   destruct (lsinfins _ _ H); try (apply s); apply (bicpll _ _ _ _ _ s).
 Qed.

 (* Combine an ♃ goal into a ♃ between maplets *)
 Definition fbkcplr(S:☒)(E E':☌):[☒*]:=
  ∅☓☓★(match S with ☬☓(G,F♃F') => ☬☓(G,(E☵F)♃(E'☵F')) | _ => S end).

 Theorem corrfbkcplr:forall (S:☒)(E E':☌), ∧☓(fbkcplr S E E')->☭☓(S).
 Proof.
  induction S; induction b0; intros E E'; unfold fbkcplr; simpl; intros H;
   destruct (lsinfins _ _ H); try (apply s); apply (bicplr _ _ _ _ _ s).
 Qed.

 (* Nota: extraction command for a proved B prover is                                                               *)
 (* Recursive Extraction fbkin                                                                                                      *)
 (*                                         fbknrm fbkrem                                                                                       *)
 (*                                         fbkimp                                                                                                    *)
 (*                                         fbkandi ffwandi fbkandl fbkandr ffwande                                      *)
 (*                                         ...                                                                                                             *)

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
