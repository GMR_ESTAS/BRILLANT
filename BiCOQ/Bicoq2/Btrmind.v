(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅
   Project: BiCoq, formalisation of the B theory into the Coq system
   Module: Btrmind, new induction principles
   The Coq induction principle on terms is generally inadequate to proof logical B
   results (e.g. proof of ⊣Q☳Q by induction on Q); therefore we define some new induction
   principles, using the well-found induction principles provided in module Mindex.
   ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽
   IPA Eric Jaeger - SGDN / DCSSI / LTI
   Development : August 2005 - October 2006
   ♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)

(* Environment ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Add LoadPath "Z:\dev\coq\BiCoq2\compiled".
 Require Export Baffec.

(* Syntactical depth function ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Fixpoint fpdepth(P:☊){struct P}:☈:=
  match P with
  | ¬P' => ☠(fpdepth P')
  | ∀P' => ☠(fpdepth P')
  | P1∧P2 => ☠((fpdepth P1)☰☉(fpdepth P2))
  | P1☲P2 => ☠((fpdepth P1)☰☉(fpdepth P2))
  | π i => ☦
  | E1♃E2 => ☠((fedepth E1)☰☉(fedepth E2))
  | E1∈E2 => ☠((fedepth E1)☰☉(fedepth E2))
  end
 with fedepth(E:☌){struct E}:☈:=
  match E with
  | ♄ => ☦
  | ☭E' => ☠(fedepth E')
  | ☬E' => ☠(fedepth E')
  | (E1☵E2) => ☠((fedepth E1)☰☉(fedepth E2))
  | (E1♂E2) => ☠((fedepth E1)☰☉(fedepth E2))
  | ω i => ☦
  | χ i => ☦
  | E' ♀ P' => ☠((fedepth E')☰☉(fpdepth P'))
  end.
 Notation "'?☋∣' P '∣'":=(fpdepth P).
 Notation "'?☍∣' E '∣'":=(fedepth E).

 (* Stability of ?☋∣∣ and ?☍∣∣ for ☯☋ and ☯☍ *)
 Theorem ftuplocdpeq:(forall (P:☊)(d:☈), ?☋∣fpuploc d P∣≡?☋∣P∣)*
                                       (forall (E:☌)(d:☈), ?☍∣feuploc d E∣≡?☍∣E∣).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (d:☈), ?☋∣fpuploc d P∣≡?☋∣P∣)
                             (fun E:☌=>forall (d:☈), ?☍∣feuploc d E∣≡?☍∣E∣)); simpl.
   intros d; apply refl_id.
   intros i; split; [split | idtac]; intros d; try (destruct (d≤☉☂i)); apply refl_id.
   intros P HP; split; intros d; rewrite HP; apply refl_id.
   intros E HE; split; intros d; rewrite HE; apply refl_id.
   intros P1 P2 H1 H2; split; intros d; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros d; rewrite H1; rewrite H2; apply refl_id.
   intros E P H1 H2 d; rewrite H1; rewrite H2; apply refl_id.
 Qed.

 Definition fpuplocdpeq:=fst ftuplocdpeq.
 Definition feuplocdpeq:=snd ftuplocdpeq.

 Theorem fpuplftdpeq:forall (P:☊), ?☋∣☯☋(P)∣≡?☋∣P∣.
 Proof.
  intros P; unfold fpuplft; apply fpuplocdpeq.
 Qed.

 Theorem feuplftdpeq:forall (E:☌), ?☍∣☯☍(E)∣≡?☍∣E∣.
 Proof.
  intros E; unfold feuplft; apply feuplocdpeq.
 Qed.

 (* Stability of ?☋∣∣ and ?☍∣∣ for ☮☋ and ☮☍ *)
 Theorem ftdnlocdpeq:(forall (P:☊)(d:☈), ?☋∣fpdnloc d P∣≡?☋∣P∣)*
                                       (forall (E:☌)(d:☈), ?☍∣fednloc d E∣≡?☍∣E∣).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (d:☈), ?☋∣fpdnloc d P∣≡?☋∣P∣)
                             (fun E:☌=> forall(d:☈), ?☍∣fednloc d E∣≡?☍∣E∣)); simpl.
   intros d; apply refl_id.
   intros i; split; [split | idtac]; intros d; try (destruct (d≤☉☂i)); apply refl_id.
   intros P HP; split; intros d; rewrite HP; apply refl_id.
   intros E HE; split; intros d; rewrite HE; apply refl_id.
   intros P1 P2 H1 H2; split; intros d; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros d; rewrite H1; rewrite H2; apply refl_id.
   intros E P H1 H2 d; rewrite H1; rewrite H2; apply refl_id.
 Qed.

 Definition fpdnlocdpeq:=fst ftdnlocdpeq.
 Definition fednlocdpeq:=snd ftdnlocdpeq.

 Theorem fpdnlftdpeq:forall (P:☊), ?☋∣☮☋(P)∣≡?☋∣P∣.
 Proof.
  intros P; unfold fpdnlft; apply fpdnlocdpeq.
 Qed.

 Theorem fednlftdpeq:forall (E:☌), ?☍∣☮☍(E)∣≡?☍∣E∣.
 Proof.
  intros E; unfold fednlft; apply fednlocdpeq.
 Qed.

 (* Stability of ?☋∣∣ and ?☍∣∣ for ☭☢, ☭☣ and ☭☤ *)
 Theorem ftinlocdpeq:(forall (P:☊)(d x:☈), ?☋∣fpinloc d χ(x) P∣≡?☋∣P∣)*
                                       (forall (E:☌)(d x:☈), ?☍∣feinloc d χ(x) E∣≡?☍∣E∣).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (d x:☈), ?☋∣fpinloc d χ(x) P∣≡?☋∣P∣)
                             (fun E:☌=>forall (d x:☈), ?☍∣feinloc d χ(x) E∣≡?☍∣E∣)); simpl.
   intros d x; apply refl_id.
   intros i; split; [split | idtac]; intros d x;
    try (destruct (d≤☉☂i); [destruct (d=☉☂i) | idtac]); apply refl_id.
   intros P HP; split; intros d x;
    [idtac | unfold feuplft; simpl]; rewrite HP ; apply refl_id.
   intros E HE; split; intros d x; rewrite HE; apply refl_id.
   intros P1 P2 H1 H2; split; intros d x; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros d x; rewrite H1; rewrite H2; apply refl_id.
   intros E P H1 H2 d x; rewrite H1; unfold feuplft; simpl; rewrite H2; apply refl_id.
 Qed.

 Definition fpinlocdpeq:=fst ftinlocdpeq.
 Definition feinlocdpeq:=snd ftinlocdpeq.

 Theorem fpinfordpeq:forall (P:☊)(x:☈)(H:?∀(P)), ☠?☋∣☭☢(P←χ(x)⊢H)∣≡?☋∣P∣.
 Proof.
  destruct P; simpl; intros x H; invset_clear H; unfold fpinfor; unfold fpprjfor;
   rewrite (fpinlocdpeq P ☦ x); apply refl_id.
 Qed.
 
 Theorem fpinexsdpeq:forall (P:☊)(x:☈)(H:?∃(P)), ☠☠☠?☋∣☭☣(P←χ(x)⊢H)∣≡?☋∣P∣.
 Proof.
  destruct P; intros x H; try (invset_clear H); destruct P; try (invset_clear H);
   destruct P; try (invset_clear H); unfold fpinexs; unfold fpprjexs;
   rewrite (fpinlocdpeq P ☦ x); apply refl_id.
 Qed.

 Theorem feincmpdpeq:forall (E:☌)(x:☈)(H:?♀(E)), ?☋∣☭☤(E←χ(x)⊢H)∣≤☉ ☠?☍∣E∣.
 Proof.
  destruct E; simpl; intros x H; invset_clear H; unfold feincmp; unfold feprjcmpl;
   unfold feprjcmpr; rewrite (fpinlocdpeq b ☦ x); replace (☦☰☉?☍∣E∣) with ?☍∣E∣.
   apply genxt; destruct (decige ?☍∣E∣ ?☋∣b∣).
    rewrite (invfimax _ _ i); apply igemax;
     [apply genxt; apply i | apply nxtrige; apply reflige].
    rewrite (commfimax ?☍∣E∣ ?☋∣b∣); rewrite (invfimax _ _ (igtige _ _ (nigeigt _ _ e)));
     apply igemax.
     apply reflige.
     generalize (nxtrige _ _ (nxtrige _ _ (nigeigt _ _ e))); intros H; invset_clear H;
      apply H0.
   unfold fimax; simpl; apply refl_equal.
 Qed.

 (* Stability of ?☋∣∣ and ?☍∣∣ for ☬☢, ☬☣ and ☬☤ *)
 Theorem ftbdlocdpeq:(forall (P:☊)(d x:☈), ?☋∣fpbdloc x d P∣≡?☋∣P∣)*
                                       (forall (E:☌)(d x:☈), ?☍∣febdloc x d E∣≡?☍∣E∣).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (d x:☈), ?☋∣fpbdloc x d P∣≡?☋∣P∣)
                             (fun E:☌=>forall (d x:☈), ?☍∣febdloc x d E∣≡?☍∣E∣)); simpl.
   intros d x; apply refl_id.
   intros i; split; [split | idtac]; intros d x;
    try (destruct (d≤☉☂i); [destruct (x=☉☂i) | idtac]); apply refl_id.
   intros P HP; split; intros d x;
    [idtac | unfold feuplft; simpl]; rewrite HP ; apply refl_id.
   intros E HE; split; intros d x; rewrite HE; apply refl_id.
   intros P1 P2 H1 H2; split; intros d x; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros d x; rewrite H1; rewrite H2; apply refl_id.
   intros E P H1 H2 d x; rewrite H1; rewrite H2; apply refl_id.
 Qed.

 Definition fpbdlocdpeq:=fst ftbdlocdpeq.
 Definition febdlocdpeq:=snd ftbdlocdpeq.

 Theorem fpbdfordpeq:forall (P:☊)(x:☈), ?☋∣☬☢(x∙P)∣≡☠?☋∣P∣.
 Proof.
  intros P x; unfold fpbdfor; simpl; rewrite (fpbdlocdpeq P ☦ x); apply refl_id.
 Qed.

 Theorem fpbdexsdpeq:forall (P:☊)(x:☈), ?☋∣☬☣(x∙P)∣≡☠☠☠?☋∣P∣.
 Proof.
  intros P x; unfold fpbdexs; unfold pexs; simpl; rewrite fpbdlocdpeq; apply refl_id.
 Qed.

 Theorem fpbdcmpdpeq:forall (P:☊)(E:☌)(x:☈), ?☍∣☬☤(x∙E∙P)∣≡☠(?☍∣E∣ ☰☉ ?☋∣P∣).
 Proof.
  intros P E x; unfold fpbdcmp; simpl; rewrite fpbdlocdpeq; apply refl_id.
 Qed.

 (* Stability of ?☋∣∣ and ?☍∣∣ for [♇☍@☋] and [♇☍@☍] *)
 Theorem ftaffexpdpeq:(forall (P:☊)(x y:☈), ?☋∣〈x♇☍χ(y)@☋P〉∣≡?☋∣P∣)*
                                         (forall (E:☌)(x y:☈), ?☍∣〈x♇☍χ(y)@☍E〉∣≡?☍∣E∣).
 Proof.
  apply (Bst_ind (fun P:☊=>forall (x y:☈), ?☋∣〈x♇☍χ(y)@☋P〉∣≡?☋∣P∣)
                             (fun E:☌=>forall (x y:☈), ?☍∣〈x♇☍χ(y)@☍E〉∣≡?☍∣E∣)); simpl.
   intros d x; apply refl_id.
   intros i; split; [split | idtac]; intros d x; try (destruct (d=☉☂i)); apply refl_id.
   intros P HP; split; intros d x; [idtac | unfold feuplft; simpl]; rewrite HP;
    apply refl_id.
   intros E HE; split; intros d x; rewrite HE; apply refl_id.
   intros P1 P2 H1 H2; split; intros d x; rewrite H1; rewrite H2; apply refl_id.
   intros E1 E2 H1 H2; split; split; intros d x; rewrite H1; rewrite H2; apply refl_id.
   intros E P H1 H2 d x; rewrite H1; unfold feuplft; simpl; rewrite H2; apply refl_id.
 Qed.

 Definition fpaffexpdpeq:=fst ftaffexpdpeq.
 Definition feaffexpdpeq:=snd ftaffexpdpeq.

(* Syntactical depth induction ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)

 Definition Bdt_ind(PP:☊->Set)(PE:☌->Set):=WFIND2 fpdepth fedepth PP PE.

(* Functional induction ☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽☽*)
(* This principle is introduced as an example, as well as a candidate for replacing       *)
(* the syntactical induction principle automatically generated by Coq over terms.       *)
(* Using functional constructions instead of constructors is expected to greatly        *)
(* ease some proof about semantic properties...                                                                         *)

 Theorem Bft_ind:forall (PP:☊->Set)(PE:☌->Set),
                               (PE ♄)->
                               (forall (i:☈), PP π(i)*PE ω(i)*PE χ(i))->
                               (forall (p:☊), PP p->PP (¬p))->
                               (forall (e:☌), PE e->PE (☭e)*PE (☬e))->
                               (forall (p1 p2:☊), PP p1->PP p2->PP (p1∧p2)*PP (p1☲p2))->
                               (forall (e1 e2:☌), PE e1->PE e2->
                                                             (PP (e1♃e2)*PP (e1 ∈ e2))*(PE (e1☵e2)*PE (e1♂e2)))->
                               (forall (p:☊)(i:☈), PP p->PP ☬☢(i∙p))->
                               (forall (e:☌)(p:☊)(i:☈), PE e->PP p->PE ☬☤(i∙e∙p))->
                               (forall (p:☊), PP p)*(forall (e:☌), PE e).
 Proof.
  intros PP PE Hbig Hi Hnot He Hp2 He2 Hfor Hcmp; apply (Bdt_ind PP PE).
   destruct s; simpl; intros HP HE.
    apply Hnot; apply HP; unfold igt; apply reflige.
    assert (Hx:{x:☈ & x⍀☋(∀s)}). exists ☰☋(∀s); apply fpnewfinf.
    destruct Hx; invset_clear p; rewrite (fpbdforinv _ _ H); apply Hfor; apply HP; simpl;
     rewrite (fpdnlftdpeq 〈☦♇☍χ(☠x)@☋s〉); rewrite (fpaffexpdpeq s ☦ ☠x); unfold igt;
     apply reflige.
    apply (fst (Hp2 _ _ (HP _ (genxt _ _ (fimaxgel ?☋∣s1∣ ?☋∣s2∣)))
                                     (HP _ (genxt _ _ (fimaxger ?☋∣s1∣ ?☋∣s2∣))))).
    apply (snd (Hp2 _ _ (HP _ (genxt _ _ (fimaxgel ?☋∣s1∣ ?☋∣s2∣)))
                                     (HP _ (genxt _ _ (fimaxger ?☋∣s1∣ ?☋∣s2∣))))).
    apply (fst (fst (Hi b))).
    apply (fst (fst (He2 _ _ (HE _ (genxt _ _ (fimaxgel ?☍∣b∣ ?☍∣b0∣)))
                                             (HE _ (genxt _ _ (fimaxger ?☍∣b∣ ?☍∣b0∣)))))).
    apply (snd (fst (He2 _ _ (HE _ (genxt _ _ (fimaxgel ?☍∣b∣ ?☍∣b0∣)))
                                             (HE _ (genxt _ _ (fimaxger ?☍∣b∣ ?☍∣b0∣)))))).
   destruct t; simpl; intros HE HP.
    apply Hbig.
    apply (fst (He _ (HE _ (reflige ☠?☍∣t∣)))).
    apply (snd (He _ (HE _ (reflige ☠?☍∣t∣)))).
    apply (fst (snd (He2 _ _ (HE _ (genxt _ _ (fimaxgel ?☍∣t1∣ ?☍∣t2∣)))
                                             (HE _ (genxt _ _ (fimaxger ?☍∣t1∣ ?☍∣t2∣)))))).
    apply (snd (snd (He2 _ _ (HE _ (genxt _ _ (fimaxgel ?☍∣t1∣ ?☍∣t2∣)))
                                             (HE _ (genxt _ _ (fimaxger ?☍∣t1∣ ?☍∣t2∣)))))).
    apply (snd (fst (Hi b))).
    apply (snd (Hi b)).
    assert (Hx:{x:☈ & x⍀☍t♀b}). exists ☰☍(t♀b); apply fenewfinf.
    destruct Hx; invset_clear e; rewrite (fpbdcmpinv _ _ _ H H0); apply Hcmp.
     apply HE; unfold igt; apply genxt; apply fimaxgel.
     apply HP; rewrite (fpdnlftdpeq 〈☦♇☍χ(☠x)@☋b〉); rewrite (fpaffexpdpeq b ☦ ☠x); unfold igt;
      apply genxt; apply fimaxger.
 Qed.

(*♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅♅*)
