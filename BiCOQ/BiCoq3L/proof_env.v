(*==================================================================================================
  Project : BiCoq3
  Module : proof_env.v
  This module specify (without implementing) B proofs environments, that is sets of predicates.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Sep 2008
  ================================================================================================*)

Require Export term.

(* Proof environment -----------------------------------------------------------------------------*)

Variable Bgam:Set.
Notation "'Γ'" := (Bgam).

Variable Bg_mem:Π->Γ->Β.
Notation "p '∈ˠ' G" := (Bg_mem p G) (at level 30, no associativity).

Variable Bg_empty:Γ.
Notation "'∅ˠ'" := (Bg_empty).
Variable Bg_empty_mem:forall (p:Π), p∈ˠ∅ˠ=⊥.

Variable Bg_add:Π->Γ->Γ.
Notation "G '+ˠ' p" := (Bg_add p G) (at level 28, left associativity).
Notation "G '·' p" := (Bg_add p G) (at level 86, left associativity).
Variable Bg_add_mem:forall (p1 p2:Π)(G:Γ), p1∈ˠ(G+ˠp2)=p1∈ˠG || Bp_eq p1 p2.

Variable Bg_rem:Π->Γ->Γ.
Notation "G '-ˠ' p" := (Bg_rem p G) (at level 28, left associativity).
Variable Bg_rem_mem:forall (p1 p2:Π)(G:Γ), p1∈ˠ(G-ˠp2)=p1∈ˠG && (negb (Bp_eq p1 p2)).

Variable Bg_all:(Π->Β)->Γ->Β.
Variable Bg_all_imp:((fun (f:Π->Β)(G:Γ)=>forall (p:Π), p∈ˠG=⊤->f p=⊤)⇝²Bg_all).

Variable Bg_inc:Γ->Γ->Β.
Notation "G1 '⊆ˠ' G2" := (Bg_inc G1 G2) (at level 30, no associativity).
Variable Bg_inc_imp:((fun (G1 G2:Γ)=>forall (p:Π), p∈ˠG1=⊤->p∈ˠG2=⊤)⇝²Bg_inc).

Variable Bg_eq:Γ->Γ->Β.
Notation "G1 '=ˠ' G2" := (Bg_eq G1 G2) (at level 30, no associativity).
Variable Bg_eq_ext:((fun (G1 G2:Γ)=>forall (p:Π), p∈ˠG1=p∈ˠG2)⇝²Bg_eq).
Variable Bg_eq_imp:((fun (G1 G2:Γ)=>G1=G2)⇝²Bg_eq).

(* Pointwise extensions --------------------------------------------------------------------------*)

Variable Bg_free:Ι->Γ->Β.
Notation "i '∖ˠ' G":=(Bg_free i G=false) (at level 96, no associativity).
Variable Bg_free_empty:forall (i:Ι), i∖ˠ∅ˠ.
Variable Bg_free_mem:forall (G:Γ)(i:Ι), (i∖ˠG)->forall (p:Π), p∈ˠG=⊤->(i∖p).
Variable Bg_mem_free:forall (G:Γ)(i:Ι), (forall (p:Π), p∈ˠG=⊤->(i∖p))->(i∖ˠG).

Variable Bg_fresh:nat->Γ->nat.
Notation "'⋕ˠ(' u ',' G ')'":= (Bg_fresh u G).
Variable Bg_fresh_free:forall (G:Γ)(u:nat), ¡(u,⋕ˠ(u,G))∖ˠG.

Variable Bg_ground:nat->Γ->Β.
Notation "'∖∖ˠ(' u ',' G ')'":= (Bg_ground u G=⊤).
Variable Bg_ground_free:forall (G:Γ)(u:nat), ∖∖ˠ(u,G)->forall (n:nat), ¡(u,n)∖ˠG.
Variable Bg_free_ground:forall (G:Γ)(u:nat), (forall (n:nat), ¡(u,n)∖ˠG)->∖∖ˠ(u,G).

Variable Bg_fshun:Γ->nat.
Notation "'⋇ˠ(' G ')'":= (Bg_fshun G).
Variable Bg_fshun_ground:forall (G:Γ)(n:nat), ¡(⋇ˠ(G),n)∖ˠG.

(* Theorems --------------------------------------------------------------------------------------*)

Theorem Bg_add_mem_old:forall (p1 p2:Π)(G:Γ), p1∈ˠG=⊤->p1∈ˠ(G+ˠp2)=⊤.
Proof.
 intros p1 p2 G Hp1; rewrite Bg_add_mem; rewrite Hp1; apply refl_equal.
Qed.

Theorem Bg_add_mem_new:forall (p:Π)(G:Γ), p∈ˠ(G+ˠp)=⊤.
Proof.
 intros p G; rewrite Bg_add_mem; rewrite (imppt2 Bp_eq_imp _ _ (refl_equal p)); apply orb_true_r.
Qed.

Theorem Bg_inc_refl:forall (G:Γ), G⊆ˠG=⊤.
Proof.
 intros G; reflect Bg_inc_imp; intros p Hp; apply Hp.
Qed.

Theorem Bg_inc_trans:forall (G1 G2 G3:Γ), G1⊆ˠG2=⊤->G2⊆ˠG3=⊤->G1⊆ˠG3=⊤.
Proof.
 intros G1 G2 G3 H12 H23; reflect Bg_inc_imp; reflect_in Bg_inc_imp H12; reflect_in Bg_inc_imp H23;
  intros p Hp; apply (H23 _ (H12 _ Hp)).
Qed.

Theorem Bg_inc_empty:forall (G:Γ), ∅ˠ⊆ˠG=⊤.
Proof.
 intros G; reflect Bg_inc_imp; intros p Hp; rewrite (Bg_empty_mem p) in Hp; inversion Hp.
Qed.

Theorem Bg_empty_inc:forall (G:Γ), G⊆ˠ∅ˠ=⊤->G=ˠ∅ˠ=⊤.
Proof.
 intros G Hinc; reflect_in Bg_inc_imp Hinc; reflect Bg_eq_ext; intros p; case_eq (p∈ˠG); simpl;
  intros Hp.
  rewrite (Hinc _ Hp); apply refl_equal.
  apply sym_equal; apply Bg_empty_mem.
Qed.

Theorem Bg_eq_refl:forall (G:Γ), G=ˠG=⊤.
Proof.
 intros G; reflect Bg_eq_imp; apply refl_equal.
Qed.

Theorem Bg_eq_sym:forall (G1 G2:Γ), G1=ˠG2=G2=ˠG1.
Proof.
 intros G1 G2; case_eq (G2=ˠG1); intros Heq; reflect Bg_eq_imp; reflect_in Bg_eq_imp Heq.
  rewrite Heq; apply refl_equal.
  intros Heq'; apply Heq; rewrite Heq'; apply refl_equal.
Qed.

Theorem Bg_eq_trans:forall (G1 G2 G3:Γ), G1=ˠG2=⊤->G2=ˠG3=⊤->G1=ˠG3=⊤.
Proof.
 intros G1 G2 G3 H12 H23; reflect Bg_eq_imp; reflect_in Bg_eq_imp H12; reflect_in Bg_eq_imp H23;
  rewrite H12; apply H23.
Qed.

Theorem Bg_inc_add:forall (G:Γ)(p:Π), G⊆ˠG+ˠp=⊤.
Proof.
 intros G p; reflect Bg_inc_imp; intros p' Hp'; rewrite Bg_add_mem; rewrite Hp'; apply refl_equal.
Qed.

Theorem Bg_add_comm:forall (G:Γ)(p1 p2:Π), G+ˠp1+ˠp2=G+ˠp2+ˠp1.
Proof.
 intros G p1 p2; reflect Bg_eq_imp; reflect Bg_eq_ext; intros p'; repeat (rewrite Bg_add_mem);
  destruct (p'∈ˠG); destruct (Bp_eq p' p1); destruct (Bp_eq p' p2); apply refl_equal.
Qed.

Theorem Bg_free_inc:forall (G1 G2:Γ)(i:Ι), (i∖ˠG1)->G2⊆ˠG1=⊤->(i∖ˠG2).
Proof.
 intros G1 G2 i HG1 Hinc; reflect_in Bg_inc_imp Hinc; apply Bg_mem_free; intros p Hp;
  apply (Bg_free_mem _ _ HG1 p); apply Hinc; apply  Hp.
Qed.

Theorem gfresh:forall (G:Γ)(u:nat), {v:nat | ¡(u,v)∖ˠG}.
Proof.
 intros G u; exists ⋕ˠ(u,G); apply Bg_fresh_free.
Qed.

Theorem gfshun:forall (G:Γ), {u:nat | ∖∖ˠ(u,G)}.
Proof.
 intros G; exists (Bg_fshun G); apply Bg_free_ground; apply Bg_fshun_ground.
Qed.

Theorem Bg_free_add:forall (G:Γ)(p:Π)(i:Ι), (i∖ˠG)->(i∖p)->(i∖ˠG·p).
Proof.
 intros G p i  Hg Hp; apply Bg_mem_free; intros p' Hp'; rewrite (Bg_add_mem p' p G) in Hp';
  destruct (orb_true_elim _ _ Hp') as [H'p' | H'p'].
  apply (Bg_free_mem _ _ Hg _ H'p').
  reflect_in Bp_eq_imp H'p'; rewrite H'p'; apply Hp.
Qed.

Theorem Bg_inc_add_del:forall (G1 G2:Γ)(p:Π), G1⊆ˠG2=⊤->G1⊆ˠ(G2·p)=⊤.
Proof.
 intros G1 G2 p Hinc; apply Bg_inc_trans with (1:=Hinc); apply Bg_inc_add.
Qed.

Theorem Bg_inc_add_del':forall (G1 G2:Γ)(p1 p2:Π), G1⊆ˠ(G2·p2)=⊤->G1⊆ˠ(G2·p1·p2)=⊤.
Proof.
 intros G1 G2 p1 p2 Hinc; rewrite Bg_add_comm; apply Bg_inc_add_del; apply Hinc.
Qed.

Theorem Bg_add_mem_old':forall (p1 p2 p3:Π)(G:Γ), p1∈ˠ(G·p3)=⊤->p1∈ˠ(G·p2·p3)=⊤.
Proof.
 intros p1 p2 p3 G Hmem; rewrite Bg_add_comm; apply Bg_add_mem_old; apply Hmem.
Qed.

Theorem Bg_inc_mem:forall (p:Π)(G1 G2:Γ), p∈ˠG1=⊤->G1⊆ˠG2=⊤->p∈ˠG2=⊤.
Proof.
 intros p G1 G2 Hmem Hinc; reflect_in Bg_inc_imp Hinc; apply Hinc; apply Hmem.
Qed.

(* Tactics ---------------------------------------------------------------------------------------*)

Ltac Btac_gamma :=
 match goal with
 | |-_∈ˠ_=⊤ => Btac_gmem
 | |-_⊆ˠ_=⊤ => Btac_ginc
 end
 with Btac_gmem := assumption ||
                   apply Bg_add_mem_new ||
                   (apply Bg_add_mem_old; Btac_gmem) ||
                   (apply Bg_add_mem_old'; Btac_gmem) ||
                   (match goal with
                    | Hinc:_⊆ˠ_=⊤|-_ => apply Bg_inc_mem with (2:=Hinc); Btac_gmem
                    end)
 with Btac_ginc := assumption ||
                   apply Bg_inc_refl ||
                   (apply Bg_inc_add_del; Btac_ginc) ||
                   (apply Bg_inc_add_del'; Btac_ginc) ||
                   (match goal with
                    | Hinc:_⊆ˠ_=⊤|-_ => apply Bg_inc_trans with (1:=Hinc); Btac_ginc
                    end).

(*================================================================================================*)