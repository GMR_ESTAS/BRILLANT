(*==================================================================================================
  Project : BiCoq3
  Module : psubst.v
  As we have defined an extended form of substitution for expressions, we describe here a function
  to substitute a propositional variable by a predicate. Note that as we have NOT defined a binder
  for propositional variables, propositional variables are all dangling and can be considered as
  named variables.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Sep 2008
  ================================================================================================*)

Require Export lift.

(* Substitution function -------------------------------------------------------------------------*)

Fixpoint Bp_psubst(n:nat)(p a:Π){struct p}:Π:=
 match p with
 | ¬p' => ¬(Bp_psubst n p' a)
 | ∀Δ(p') => ∀Δ(Bp_psubst n p' (Bp_lift a))
 | p1⋀p2 => (Bp_psubst n p1 a)⋀(Bp_psubst n p2 a)
 | (p1⇒p2) => (Bp_psubst n p1 a)⇒(Bp_psubst n p2 a)
 | ∐(n') => if nat_eq n n' then a else p
 | e1≡e2 => (Be_psubst n e1 a)≡(Be_psubst n e2 a)
 | e1∈e2 => (Be_psubst n e1 a)∈(Be_psubst n e2 a)
 end
with Be_psubst(n:nat)(e:Ε)(a:Π){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_psubst n e' a)
 | ↑(e') => ↑(Be_psubst n e' a)
 | e1↦e2 => (Be_psubst n e1 a)↦(Be_psubst n e2 a)
 | e1×e2 => (Be_psubst n e1 a)×(Be_psubst n e2 a)
 | ∞(_) => e
 | ∂(_) => e
 | ⅭΔ(e',p') => ⅭΔ(Be_psubst n e' a,Bp_psubst n p' (Bp_lift a))
 end.
Notation "'⁅' n '←' a '@Π@' p '⁆'" := (Bp_psubst n p a).
Notation "'⁅' n '←' a '@Ε@' e '⁆'" := (Be_psubst n e a).

Definition Bt_psubst(n:nat)(t:Τ)(a:Π):Τ :=
 match t with be_ e' => Be_psubst n e' a | bp_ p' => Bp_psubst n p' a end.
Notation "'⁅' n '←' a '@' t '⁆'" := (Bt_psubst n t a).

(* Theorems for substitution ---------------------------------------------------------------------*)

Theorem Bt_psubst_occur:forall (t:Τ)(n:nat)(a:Π), Bt_poccur n t=⊥->⁅n←a@t⁆=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (n:nat)(a:Π), Bt_poccur n t=⊥->⁅n←a@t⁆=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind n a Ht;
  try (apply refl_equal);
  try (irewrite (Hind _ (sdepth t) n a Ht); apply refl_equal);
  try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2];
       irewrite (Hind _ (sdepth_l t1 t2) n a Ht1);
       irewrite (Hind _ (sdepth_r t1 t2) n a Ht2); apply refl_equal).
  destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2]; irewrite (Hind _ (sdepth_l t1 t2) n a Ht1);
   irewrite (Hind _ (sdepth_r t1 t2) n (Bp_lift a) Ht2); apply refl_equal.
  irewrite (Hind _ (sdepth t) n (Bp_lift a) Ht); apply refl_equal.
  rewrite Ht; apply refl_equal.
Qed.

(*================================================================================================*)