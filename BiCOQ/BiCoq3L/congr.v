(*==================================================================================================
  Project : BiCoq3
  Module : congr.v
  In this module we provide a few results related to congruence (replacement of related propositions
  in a term)
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Sep 2008, Md: Sep 2008
  ================================================================================================*)

Require Export raw_proof.

(* Auxiliary results -----------------------------------------------------------------------------*)

Theorem repl_esubst:forall (G:Γ)(p p':Π)(i:Ι)(e:Ε), G⊢p⇒p'->(i∖ˠG)->G⊢〈i←e@Π@p〉->G⊢〈i←e@Π@p'〉.
Proof.
 intros G p p' i e Himp HiG Hp; apply (Bpr_cutr _ _ 〈i←e@Π@p'〉 Hp); apply Bpr_impe;
  change (〈i←e@Π@p〉⇒〈i←e@Π@p'〉) with 〈i←e@Π@p⇒p'〉; apply Bpr_fore; apply (Bpr_fori _ _ _ Himp HiG).
Qed.

Theorem Bpr_pro_dbli:forall (G:Γ)(e e1 e2:Ε), G⊢e∈e1×e2->
                     forall (i1 i2:Ι), G·e≡∂(i1)↦∂(i2)⊢(∂(i1)↦∂(i2))∈e1×e2.
Proof.
 intros G e e1 e2 Hmem i1 i2; destruct (fresh (e1×e2) 1) as [n Hi]; set (i:=¡(1,n)); fold i in Hi;
  replace ((∂(i1)↦∂(i2))∈e1×e2) with 〈i←∂(i1)↦∂(i2)@Π@∂(i)∈e1×e2〉; 
  destruct (orb_false_elim _ _ Hi) as [Hie1 Hie2].
  apply (Bpr_leib) with (1:=Bpr_axom G (e≡∂(i1)↦∂(i2))); apply Bpr_clear; simpl; nat_simpl;
   bool_simpl; simpl in Hi; irewrite (Bt_esubst_free e1 i Hie1 e);
   irewrite (Bt_esubst_free e2 i Hie2 e); apply Hmem.
  simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free e1 i Hie1 (∂(i1)↦∂(i2)));
   irewrite (Bt_esubst_free e2 i Hie2 (∂(i1)↦∂(i2))); apply refl_equal.
Qed.

Theorem Bpr_prol:forall (G:Γ)(e1 e2 E1 E2:Ε), G⊢e1↦e2∈E1×E2->G⊢e1∈E1.
Proof.
 intros G e1 e2 E1 E2 Hmem; destruct (gfresh (G·e1↦e2∈E1×E2) 1) as [n1 Hi1]; set (i1:=¡(1,n1));
  fold i1 in Hi1; assert (Hi1es:i1∖e1↦e2∈E1×E2).
  apply Bg_free_mem with (G:=G·e1↦e2∈E1×E2); [apply Hi1 | apply Bg_add_mem_new].
 simpl in Hi1es; destruct (orb_false_elim _ _ Hi1es) as [H'i1 H''i1];
  destruct (orb_false_elim _ _ H'i1) as [Hi1e1 Hi1e2];
  destruct (orb_false_elim _ _ H''i1) as [Hi1E1 Hi1E2]; clear Hi1es H'i1 H''i1.
 destruct (gfresh (G·e1↦e2∈E1×E2) 2) as [n2 Hi2]; set (i2:=¡(2,n2)); fold i2 in Hi2;
  assert (Hi2es:i2∖e1↦e2∈E1×E2).
  apply Bg_free_mem with (G:=G·e1↦e2∈E1×E2); [apply Hi2 | apply Bg_add_mem_new].
 simpl in Hi2es; destruct (orb_false_elim _ _ Hi2es) as [H'i2 H''i2];
  destruct (orb_false_elim _ _ H'i2) as [Hi2e1 Hi2e2];
  destruct (orb_false_elim _ _ H''i2) as [Hi2E1 Hi2E2]; clear Hi2es H'i2 H''i2.
 apply Bpr_cutr with (p1:=∃(i1∙∂(i1)∈E1⋀∃(i2∙∂(i2)∈E2⋀e1↦e2≡∂(i1)↦∂(i2)))).
  apply Bpr_proe.
   reflect DB_eq_imp; simpl; nat_simpl; apply refl_equal.
   apply Bg_free_mem with (G:=G·e1↦e2∈E1×E2); [apply Hi1 | apply Bg_add_mem_new].
   apply Bg_free_mem with (G:=G·e1↦e2∈E1×E2); [apply Hi2 | apply Bg_add_mem_new].
   apply Hmem.
  apply Bpr_impe; apply Bpr_ctnp; apply Bpr_impi; apply Bpr_fori.
   apply Bpr_impe; apply Bpr_ctnn; apply bbdr8; apply Bpr_impi; apply Bpr_ctnp; apply Bpr_impi;
    apply Bpr_fori.
    apply Bpr_impe; apply Bpr_ctnn; apply bbdr8; apply Bpr_impi; apply Bpr_impi;
     apply Bpr_cutr with (p1:=e1≡∂(i1)).
     apply Bpr_cpll with (e2:=e2)(e4:=∂(i2)); apply Bpr_axom.
     replace (e1∈E1) with (〈i1←e1@Π@∂(i1)∈E1〉).
      apply Bpr_leib with (e1:=∂(i1)).
       apply Bpr_equ_sym; Btac_hyp.
       replace 〈i1←∂(i1)@Π@∂(i1)∈E1〉 with (∂(i1)∈E1).
        Btac_hyp.
        irewrite (Bt_esubst_id (∂(i1)∈E1) i1); nat_simpl; bool_simpl; intros _; apply refl_equal.
      simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free E1 i1 Hi1E1 e1); apply refl_equal.
    apply Bg_mem_free; intros p Hp; repeat (rewrite Bg_add_mem in Hp);
     destruct (orb_true_elim _ _ Hp) as [H'p | H''p]; clear Hp;
    [destruct (orb_true_elim _ _ H'p) as [H''p | H''p]; clear H'p | ].
     apply Bg_free_mem with (2:=H''p); apply Bg_free_inc with (1:=Hi2); apply Bg_inc_add.
     reflect_in Bp_eq_imp H''p; rewrite H''p; simpl; nat_simpl; bool_simpl; apply Hi2E1.
     reflect_in Bp_eq_imp H''p; rewrite H''p; simpl; rewrite Hi2e1; rewrite Hi2E1; apply refl_equal.
   apply Bg_mem_free; intros p Hp; rewrite Bg_add_mem in Hp;
    destruct (orb_true_elim _ _ Hp) as [H'p | H'p].
    apply Bg_free_mem with (2:=H'p); apply Bg_free_inc with (1:=Hi1); apply Bg_inc_add.
    reflect_in Bp_eq_imp H'p; rewrite H'p; simpl; rewrite Hi1e1; rewrite Hi1E1; apply refl_equal.
Qed.

Theorem Bpr_pror:forall (G:Γ)(e1 e2 E1 E2:Ε), G⊢e1↦e2∈E1×E2->G⊢e2∈E2.
Proof.
 intros G e1 e2 E1 E2 Hmem; destruct (gfresh (G·e1↦e2∈E1×E2) 1) as [n1 Hi1]; set (i1:=¡(1,n1));
  fold i1 in Hi1; assert (Hi1es:i1∖e1↦e2∈E1×E2).
  apply Bg_free_mem with (G:=G·e1↦e2∈E1×E2); [apply Hi1 | apply Bg_add_mem_new].
 simpl in Hi1es; destruct (orb_false_elim _ _ Hi1es) as [H'i1 H''i1];
  destruct (orb_false_elim _ _ H'i1) as [Hi1e1 Hi1e2];
  destruct (orb_false_elim _ _ H''i1) as [Hi1E1 Hi1E2]; clear Hi1es H'i1 H''i1.
 destruct (gfresh (G·e1↦e2∈E1×E2) 2) as [n2 Hi2]; set (i2:=¡(2,n2)); fold i2 in Hi2;
  assert (Hi2es:i2∖e1↦e2∈E1×E2).
  apply Bg_free_mem with (G:=G·e1↦e2∈E1×E2); [apply Hi2 | apply Bg_add_mem_new].
 simpl in Hi2es; destruct (orb_false_elim _ _ Hi2es) as [H'i2 H''i2];
  destruct (orb_false_elim _ _ H'i2) as [Hi2e1 Hi2e2];
  destruct (orb_false_elim _ _ H''i2) as [Hi2E1 Hi2E2]; clear Hi2es H'i2 H''i2.
 apply Bpr_cutr with (p1:=∃(i1∙∂(i1)∈E1⋀∃(i2∙∂(i2)∈E2⋀e1↦e2≡∂(i1)↦∂(i2)))).
  apply Bpr_proe.
   reflect DB_eq_imp; simpl; nat_simpl; apply refl_equal.
   apply Bg_free_mem with (G:=G·e1↦e2∈E1×E2); [apply Hi1 | apply Bg_add_mem_new].
   apply Bg_free_mem with (G:=G·e1↦e2∈E1×E2); [apply Hi2 | apply Bg_add_mem_new].
   apply Hmem.
  apply Bpr_impe; apply Bpr_ctnp; apply Bpr_impi; apply Bpr_fori.
   apply Bpr_impe; apply Bpr_ctnn; apply bbdr8; apply Bpr_impi; apply Bpr_ctnp; apply Bpr_impi;
    apply Bpr_fori.
    apply Bpr_impe; apply Bpr_ctnn; apply bbdr8; apply Bpr_impi; apply Bpr_impi;
     apply Bpr_cutr with (p1:=e2≡∂(i2)).
     apply Bpr_cplr with (e1:=e1)(e3:=∂(i1)); apply Bpr_axom.
     replace (e2∈E2) with (〈i2←e2@Π@∂(i2)∈E2〉).
      apply Bpr_leib with (e1:=∂(i2)).
       apply Bpr_equ_sym; Btac_hyp.
       replace 〈i2←∂(i2)@Π@∂(i2)∈E2〉 with (∂(i2)∈E2).
        Btac_hyp.
        irewrite (Bt_esubst_id (∂(i2)∈E2) i2); nat_simpl; bool_simpl; intros _; apply refl_equal.
      simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free E2 i2 Hi2E2 e2); apply refl_equal.
    apply Bg_mem_free; intros p Hp; repeat (rewrite Bg_add_mem in Hp);
     destruct (orb_true_elim _ _ Hp) as [H'p | H''p]; clear Hp;
    [destruct (orb_true_elim _ _ H'p) as [H''p | H''p]; clear H'p | ].
     apply Bg_free_mem with (2:=H''p); apply Bg_free_inc with (1:=Hi2); apply Bg_inc_add.
     reflect_in Bp_eq_imp H''p; rewrite H''p; simpl; nat_simpl; bool_simpl; apply Hi2E1.
     reflect_in Bp_eq_imp H''p; rewrite H''p; simpl; rewrite Hi2e2; rewrite Hi2E2; apply refl_equal.
   apply Bg_mem_free; intros p Hp; rewrite Bg_add_mem in Hp;
    destruct (orb_true_elim _ _ Hp) as [H'p | H'p].
    apply Bg_free_mem with (2:=H'p); apply Bg_free_inc with (1:=Hi1); apply Bg_inc_add.
    reflect_in Bp_eq_imp H'p; rewrite H'p; simpl; rewrite Hi1e2; rewrite Hi1E2; apply refl_equal.
Qed.

Theorem Bpr_prodec:forall (G:Γ)(e1 e2 E1 E2:Ε), G⊢e1∈E1->G⊢e2∈E2->G⊢e1↦e2∈E1×E2.
Proof.
 intros G e1 e2 E1 E2 He1E1 He2E2.
 destruct (fresh (e1↦e2∈E1×E2) 1) as [n1 Hi1]; set (i1:=¡(1,n1)); fold i1 in Hi1; simpl in Hi1;
  destruct (orb_false_elim _ _ Hi1) as [H'i1 H''i1];
  destruct (orb_false_elim _ _ H'i1) as [Hi1e1 Hi1e2];
  destruct (orb_false_elim _ _ H''i1) as [Hi1E1 Hi1E2]; clear H'i1 H''i1.
 destruct (fresh (e1↦e2∈E1×E2) 2) as [n2 Hi2]; set (i2:=¡(2,n2)); fold i2 in Hi2; simpl in Hi2;
  destruct (orb_false_elim _ _ Hi2) as [H'i2 H''i2];
  destruct (orb_false_elim _ _ H'i2) as [Hi2e1 Hi2e2];
  destruct (orb_false_elim _ _ H''i2) as [Hi2E1 Hi2E2]; clear H'i2 H''i2.
 assert (Hdiff:i1<>i2). reflect DB_eq_imp; simpl; nat_simpl; bool_simpl; apply refl_equal.
 apply (Bpr_proi G (e1↦e2) E1 E2 i1 i2 Hdiff Hi1 Hi2).
 apply Bpr_exsi with (e:=e1).
 unfold Bt_esubst; unfold Bp_esubst; fold Be_esubst Bp_esubst; DB_simpl;
  rewrite (Bp_forall_esubst_diff (¬(∂(i2)∈E2⋀e1↦e2≡∂(i1)↦∂(i2))) e1 i2 i1 (sym_not_eq Hdiff) Hi2e1);
  apply Bpr_andi.
  irewrite (Bt_esubst_free E1 i1 Hi1E1 e1); apply He1E1.
  replace 〈i1←e1@Π@¬(∂(i2)∈E2⋀e1↦e2≡∂(i1)↦∂(i2))〉 with (¬(∂(i2)∈E2⋀e1↦e2≡e1↦∂(i2))).
   apply Bpr_exsi with (e:=e2); simpl; nat_simpl; bool_simpl;
    irewrite (Bt_esubst_free E2 i2 Hi2E2 e2); irewrite (Bt_esubst_free e1 i2 Hi2e1 e2);
    irewrite (Bt_esubst_free e2 i2 Hi2e2 e2); apply Bpr_andi; [apply He2E2 | apply Bpr_refl].
   simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free E2 i1 Hi1E2 e1);
    irewrite (Bt_esubst_free e1 i1 Hi1e1 e1); irewrite (Bt_esubst_free e2 i1 Hi1e2 e1);
    apply refl_equal.
Qed.

(* Elementary replacements -----------------------------------------------------------------------*)

Theorem repl_not:forall (G:Γ)(p p':Π), G⊢p⇒p'->G⊢¬p'->G⊢¬p.
Proof.
 intros G p p' Himp Hp'; apply (Bpr_absn _ _ _ (Bpr_impe _ _ _ Himp)); apply Bpr_clear; apply Hp'.
Qed.

Theorem repl_for:forall (G:Γ)(p p':Π)(i:Ι), G⊢p⇒p'->(i∖ˠG)->G⊢∀(i∙p)->G⊢∀(i∙p').
Proof.
 intros G p p' i Himp HiG Hp; apply (Bpr_fori G p' i) with (2:=HiG);
  apply (Bpr_cutr G p p' (Bpr_fore_id _ _ _ Hp)); apply Bpr_impe; apply Himp.
Qed.

Theorem repl_imp_l:forall (G:Γ)(p p' pr:Π), G⊢p⇒p'->G⊢p'⇒pr->G⊢p⇒pr.
Proof.
 intros G p p' pr Himp Hp'; apply Bpr_modp with (1:=Hp'); apply Bpr_modp with (1:=Himp); Btac_prop.
Qed.

Theorem repl_imp_r:forall (G:Γ)(p p' pl:Π), G⊢p⇒p'->G⊢pl⇒p->G⊢pl⇒p'.
Proof.
 intros G p p' pl Himp Hp; apply Bpr_modp with (1:=Hp); apply Bpr_modp with (1:=Himp); Btac_prop.
Qed.

Theorem repl_and_l:forall (G:Γ)(p p' pr:Π), G⊢p⇒p'->G⊢p⋀pr->G⊢p'⋀pr.
Proof.
 intros G p p' pr Himp Hp; apply Bpr_andi; [ | apply (Bpr_andr _ _ _ Hp)].
  apply (Bpr_cutr _ _ _ (Bpr_andl _ _ _ Hp) (Bpr_impe _ _ _ Himp)).
Qed.

Theorem repl_and_r:forall (G:Γ)(p p' pl:Π), G⊢p⇒p'->G⊢pl⋀p->G⊢pl⋀p'.
Proof.
 intros G p p' pl Himp Hp; apply Bpr_andi; [apply (Bpr_andl _ _ _ Hp) | ].
  apply (Bpr_cutr _ _ _ (Bpr_andr _ _ _ Hp) (Bpr_impe _ _ _ Himp)).
Qed.

Theorem repl_equ_l:forall (G:Γ)(e e' er:Ε), G⊢e≡e'->G⊢e≡er->G⊢e'≡er.
Proof.
 intros G e e' er Hequ He; apply (Bpr_equ_trans _ _ _ _ (Bpr_equ_sym _ _ _ Hequ) He).
Qed.

Theorem repl_equ_r:forall (G:Γ)(e e' el:Ε), G⊢e≡e'->G⊢el≡e->G⊢el≡e'.
Proof.
 intros G e e' el Hequ He; apply Bpr_equ_sym; apply (repl_equ_l _ _ _ el Hequ); apply Bpr_equ_sym;
  apply He.
Qed.

Theorem repl_ins_l:forall (G:Γ)(e e' er:Ε), G⊢e≡e'->G⊢e∈er->G⊢e'∈er.
Proof.
 intros G e e' er Hequ He; destruct (fresh er 1) as [n Hier]; set (i:=¡(1,n)); fold i in Hier;
  replace (e'∈er) with 〈i←e'@Π@∂(i)∈er〉.
  apply (Bpr_leib G (∂(i)∈er) _ _ i Hequ); simpl; nat_simpl; simpl;
   irewrite (Bt_esubst_free _ _ Hier e); apply He.
  simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ Hier e'); apply refl_equal.
Qed.

Theorem repl_ins_r:forall (G:Γ)(e e' el:Ε), G⊢e⊆e'->G⊢el∈e->G⊢el∈e'.
Proof.
 intros G e e' el Hinc He; apply Bpr_cutr with (1:=He); apply Bpr_impe;
  destruct (fresh (e∈e') 1) as [n Hi]; set (i:=¡(1,n)); fold i in Hi;simpl in Hi;
  destruct (orb_false_elim _ _ Hi) as [Hie Hie'];
  replace  (el∈e⇒el∈e') with 〈i←el@Π@∂(i)∈e⇒∂(i)∈e'〉.
  apply Bpr_fore; unfold binc in Hinc; apply (Bpr_powe G _ _ _ Hie Hie' Hinc).
  simpl; nat_simpl; simpl; irewrite (Bt_esubst_free e i Hie el);
   irewrite (Bt_esubst_free e' i Hie' el); apply refl_equal.
Qed.

Theorem repl_chc:forall (G:Γ)(e e':Ε), G⊢e≡e'->G⊢↓(e)≡↓(e').
Proof.
 intros G e e' Hequ; destruct (fresh e 1) as [n Hi]; set (i:=¡(1,n)); fold i in Hi;
  replace (↓(e)≡↓(e')) with 〈i←e'@Π@↓(e)≡↓(∂(i))〉.
  apply Bpr_leib with (1:=Hequ); simpl; nat_simpl; simpl; irewrite (Bt_esubst_free e i Hi e);
   apply Bpr_refl.
  simpl; nat_simpl; simpl; irewrite (Bt_esubst_free e i Hi e'); apply refl_equal.
Qed.

Theorem repl_pow:forall (G:Γ)(e e':Ε), G⊢e⊆e'->G⊢↑(e)⊆↑(e').
Proof.
 intros G e e'; unfold binc; intros Hinc; destruct (gfresh (G·e∈e') 1) as [n Hi]; set (i:=¡(1,n));
  fold i in Hi.
 assert (HiG:i∖ˠG). apply Bg_free_inc with (1:=Hi); apply Bg_inc_add.
 assert (Hiee':i∖e∈e'). apply Bg_free_mem with (1:=Hi); apply Bg_add_mem_new.
 simpl in Hiee'; destruct (orb_false_elim _ _ Hiee') as [Hie Hie'];
  apply (Bpr_powi G ↑(e) ↑(e') i Hie Hie').
 apply Bpr_fori with (2:=HiG); apply Bpr_impi.
 destruct (gfresh (G·∂(i)∈(e×e')) 2) as [n' Hi']; set (i':=¡(2,n')); fold i' in Hi'.
 assert (Hi'G:i'∖ˠG). apply Bg_free_inc with (1:=Hi'); apply Bg_inc_add.
 assert (Hi'iee':i'∖∂(i)∈(e×e')). apply Bg_free_mem with (1:=Hi'); apply Bg_add_mem_new.
 simpl in Hi'iee'; nat_simpl; bool_simpl; destruct (orb_false_elim _ _ Hi'iee') as [Hi'e Hi'e'].
 assert (Hi'i:i'∖∂(i)). simpl; nat_simpl; bool_simpl; apply refl_equal.
 apply (Bpr_powi (G·∂(i)∈↑(e)) ∂(i) e' i' Hi'i Hi'e').
 assert (Hi'Gie:i'∖ˠG·∂(i)∈↑(e)).
  apply Bg_mem_free; intros p Hp; rewrite Bg_add_mem in Hp;
   destruct (orb_true_elim _ _ Hp) as [H'p | H'p].
   apply Bg_free_mem with (1:=Hi'G)(2:=H'p).
   reflect_in Bp_eq_imp H'p; rewrite H'p; simpl; nat_simpl; bool_simpl; apply Hi'e.
 apply Bpr_fori with (2:=Hi'Gie); apply Bpr_impi;
  apply (Bpr_cutr (G·∂(i)∈↑(e)·∂(i')∈∂(i)) (∂(i')∈e) (∂(i')∈e')).
  apply Bpr_impe; replace (∂(i')∈∂(i)⇒∂(i')∈e) with 〈i'←∂(i')@Π@∂(i')∈∂(i)⇒∂(i')∈e〉;
  [ | simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free e i' Hi'e ∂(i')); apply refl_equal].
   apply Bpr_fore; apply Bpr_powe; [apply Hi'i | apply Hi'e | apply Bpr_axom].
  apply Bpr_impe; apply Bpr_clear; apply Bpr_clear;
  replace (∂(i')∈e⇒∂(i')∈e') with 〈i'←∂(i')@Π@∂(i')∈e⇒∂(i')∈e'〉;
  [ | simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free e i' Hi'e ∂(i'));
      irewrite (Bt_esubst_free e' i' Hi'e' ∂(i')); apply refl_equal].
   apply Bpr_fore; apply Bpr_powe; [apply Hi'e | apply Hi'e' | apply Hinc].
Qed.

Theorem repl_cpl_l:forall (G:Γ)(e e' er:Ε), G⊢e≡e'-> G⊢e↦er≡e'↦er.
Proof.
 intros G e e' er Hequ; destruct (fresh (e↦er) 1) as [n Hi]; set (i:=¡(1,n)); fold i in Hi;
  simpl in Hi; destruct (orb_false_elim _ _ Hi) as [Hie Hier];
  replace (e↦er≡e'↦er) with 〈i←e'@Π@e↦er≡∂(i)↦er〉.
   apply Bpr_leib with (1:=Hequ); simpl; nat_simpl; simpl; irewrite (Bt_esubst_free e i Hie e);
    irewrite (Bt_esubst_free er i Hier e); apply Bpr_refl.
   simpl; nat_simpl; simpl; irewrite (Bt_esubst_free e i Hie e');
    irewrite (Bt_esubst_free er i Hier e'); apply refl_equal.
Qed.

Theorem repl_cpl_r:forall (G:Γ)(e e' el:Ε), G⊢e≡e'->G⊢el↦e≡el↦e'.
Proof.
 intros G e e' el Hequ; destruct (fresh (el↦e) 1) as [n Hi]; set (i:=¡(1,n)); fold i in Hi;
  simpl in Hi; destruct (orb_false_elim _ _ Hi) as [Hiel Hie];
  replace (el↦e≡el↦e') with 〈i←e'@Π@el↦e≡el↦∂(i)〉.
   apply Bpr_leib with (1:=Hequ); simpl; nat_simpl; simpl; irewrite (Bt_esubst_free el i Hiel e);
    irewrite (Bt_esubst_free e i Hie e); apply Bpr_refl.
   simpl; nat_simpl; simpl; irewrite (Bt_esubst_free el i Hiel e');
    irewrite (Bt_esubst_free e i Hie e'); apply refl_equal.
Qed.

Theorem repl_pro_l:forall (G:Γ)(e e' er:Ε), G⊢e⊆e'->G⊢e×er⊆e'×er.
Proof.
 intros G e e' er; unfold binc; intros Hinc; destruct (gfresh (G·e∈e'×er) 1) as [n Hi];
  set (i:=¡(1,n)); fold i in Hi; assert (HiG:i∖ˠG).
  apply Bg_free_inc with (G1:=G·e∈e'×er); [apply Hi | apply Bg_inc_add].
 assert (Hies:i∖e∈e'×er). apply Bg_free_mem with (G:=G·e∈e'×er); [apply Hi | apply Bg_add_mem_new].
 simpl in Hies; destruct (orb_false_elim _ _ Hies) as [Hie H'ies];
  destruct (orb_false_elim _ _ H'ies) as [Hie' Hier]; clear Hies H'ies;
  apply (Bpr_powi G (e×er) (e'×er) i (orb_false_intro _ _ Hie Hier)
                                      (orb_false_intro _ _ Hie' Hier));
  apply Bpr_fori with (2:=HiG); apply Bpr_impi.
 destruct (gfresh (G·∂(i)×e∈e'×er) 2) as [n1 Hi1]; set (i1:=¡(2,n1)); fold i1 in Hi1;
  assert (Hi1G:i1∖ˠG). apply Bg_free_inc with (G1:=G·∂(i)×e∈e'×er); [apply Hi1 | apply Bg_inc_add].
 assert (Hi1es:i1∖∂(i)×e∈e'×er). apply Bg_free_mem with (G:=G·∂(i)×e∈e'×er);
                                 [apply Hi1 | apply Bg_add_mem_new].
 destruct (orb_false_elim _ _ Hi1es) as [H'i1es H''i1es];  fold Be_free Bp_free in H'i1es;
  fold Be_free Bp_free in H''i1es; destruct (orb_false_elim _ _ H'i1es) as [Hi1i Hi1e];
  destruct (orb_false_elim _ _ H''i1es) as [Hi1e' Hi1er]; clear Hi1es H'i1es H''i1es.
 destruct (gfresh (G·∂(i1)×∂(i)×e∈e'×er) 3) as [n2 Hi2]; set (i2:=¡(3,n2)); fold i2 in Hi2;
  assert (Hi2G:i2∖ˠG). apply Bg_free_inc with (G1:=G·∂(i1)×∂(i)×e∈e'×er);
                       [apply Hi2 | apply Bg_inc_add].
 assert (Hi2es:i2∖∂(i1)×∂(i)×e∈e'×er). apply Bg_free_mem with (G:=G·∂(i1)×∂(i)×e∈e'×er);
                                       [apply Hi2 | apply Bg_add_mem_new].
 destruct (orb_false_elim _ _ Hi2es) as [H'i2es H''i2es]; fold Be_free Bp_free in H'i2es;
  fold Be_free Bp_free in H''i2es; destruct (orb_false_elim _ _ H'i2es) as [H'''i2es Hi2e];
  destruct (orb_false_elim _ _ H''i2es) as [Hi2e' Hi2er];
  destruct (orb_false_elim _ _ H'''i2es) as [Hi2i1 Hi2i]; clear Hi2es H'i2es H''i2es H'''i2es;
  apply (Bpr_proi (G·∂(i)∈e×er) ∂(i) e' er i2 i1 ((snd DB_eq_imp) _ _ Hi2i1)
                  (orb_false_intro _ _ Hi2i (orb_false_intro _ _ Hi2e' Hi2er))
                  (orb_false_intro _ _ Hi1i (orb_false_intro _ _ Hi1e' Hi1er))).
 apply Bpr_cutr with (p1:=¬∀(i2∙¬(∂(i2)∈e⋀¬∀(i1∙¬(∂(i1)∈er⋀∂(i)≡∂(i2)↦∂(i1)))))).
  apply (Bpr_proe (G·∂(i)∈e×er) ∂(i) e er _ _
                  ((snd DB_eq_imp) _ _ Hi2i1)
                  (orb_false_intro _ _ Hi2i (orb_false_intro _ _ Hi2e Hi2er))
                  (orb_false_intro _ _ Hi1i (orb_false_intro _ _ Hi1e Hi1er))); apply Bpr_axom.
  apply Bpr_impe; apply Bpr_clear; apply Bpr_ctnn; apply Bpr_impi;
   assert (H'i2G:i2∖ˠG·∀(i2∙¬(∂(i2)∈e'⋀¬∀(i1∙¬(∂(i1)∈er⋀∂(i)≡∂(i2)↦∂(i1)))))).
   apply Bg_mem_free; intros p Hp; rewrite Bg_add_mem in Hp;
    destruct (orb_true_elim _ _ Hp) as [HpG | Hpp'].
    apply (Bg_free_mem _ _ Hi2G _ HpG).
    reflect_in Bp_eq_imp Hpp'; rewrite Hpp';
     apply (Bp_forall_free i2 (¬(∂(i2)∈e'⋀¬∀(i1∙¬(∂(i1)∈er⋀∂(i)≡∂(i2)↦∂(i1)))))).
  apply Bpr_fori with (2:=H'i2G);
   apply Bpr_cutr with (p1:=¬(∂(i2)∈e'⋀¬∀(i1∙¬(∂(i1)∈er⋀∂(i)≡∂(i2)↦∂(i1))))).
   apply Bpr_fore_id with (i:=i2); apply Bpr_axom.
   apply Bpr_impe; apply Bpr_ctnn; apply Bpr_clear; apply Bpr_impi;
    apply repl_and_l with (p:=∂(i2)∈e); [ | apply Bpr_axom].
    apply Bpr_clear; apply Bpr_fore_id with (i:=i2); apply Bpr_powe with (3:=Hinc);
     [apply Hi2e | apply Hi2e'].
Qed.

Theorem repl_pro_r:forall (G:Γ)(e e' el:Ε), G⊢e⊆e'->G⊢el×e⊆el×e'.
Proof.
 intros G e e' el; unfold binc; intros Hinc; destruct (gfresh (G·e∈e'×el) 1) as [n Hi];
  set (i:=¡(1,n)); fold i in Hi; assert (HiG:i∖ˠG).
  apply Bg_free_inc with (G1:=G·e∈e'×el); [apply Hi | apply Bg_inc_add].
 assert (Hies:i∖e∈e'×el). apply Bg_free_mem with (G:=G·e∈e'×el); [apply Hi | apply Bg_add_mem_new].
 simpl in Hies; destruct (orb_false_elim _ _ Hies) as [Hie H'ies];
  destruct (orb_false_elim _ _ H'ies) as [Hie' Hiel]; clear Hies H'ies;
  apply (Bpr_powi G (el×e) (el×e') i (orb_false_intro _ _ Hiel Hie)
                                      (orb_false_intro _ _ Hiel Hie'));
  apply Bpr_fori with (2:=HiG); apply Bpr_impi.
 destruct (gfresh (G·∂(i)×e∈e'×el) 2) as [n1 Hi1]; set (i1:=¡(2,n1)); fold i1 in Hi1;
  assert (Hi1G:i1∖ˠG). apply Bg_free_inc with (G1:=G·∂(i)×e∈e'×el); [apply Hi1 | apply Bg_inc_add].
 assert (Hi1es:i1∖∂(i)×e∈e'×el). apply Bg_free_mem with (G:=G·∂(i)×e∈e'×el);
                                 [apply Hi1 | apply Bg_add_mem_new].
 destruct (orb_false_elim _ _ Hi1es) as [H'i1es H''i1es];  fold Be_free Bp_free in H'i1es;
  fold Be_free Bp_free in H''i1es; destruct (orb_false_elim _ _ H'i1es) as [Hi1i Hi1e];
  destruct (orb_false_elim _ _ H''i1es) as [Hi1e' Hi1el]; clear Hi1es H'i1es H''i1es.
 destruct (gfresh (G·∂(i1)×∂(i)×e∈e'×el) 3) as [n2 Hi2]; set (i2:=¡(3,n2)); fold i2 in Hi2;
  assert (Hi2G:i2∖ˠG). apply Bg_free_inc with (G1:=G·∂(i1)×∂(i)×e∈e'×el);
                       [apply Hi2 | apply Bg_inc_add].
 assert (Hi2es:i2∖∂(i1)×∂(i)×e∈e'×el). apply Bg_free_mem with (G:=G·∂(i1)×∂(i)×e∈e'×el);
                                       [apply Hi2 | apply Bg_add_mem_new].
 destruct (orb_false_elim _ _ Hi2es) as [H'i2es H''i2es]; fold Be_free Bp_free in H'i2es;
  fold Be_free Bp_free in H''i2es; destruct (orb_false_elim _ _ H'i2es) as [H'''i2es Hi2e];
  destruct (orb_false_elim _ _ H''i2es) as [Hi2e' Hi2el];
  destruct (orb_false_elim _ _ H'''i2es) as [Hi2i1 Hi2i]; clear Hi2es H'i2es H''i2es H'''i2es;
  apply (Bpr_proi (G·∂(i)∈el×e) ∂(i) el e' i2 i1 ((snd DB_eq_imp) _ _ Hi2i1)
                  (orb_false_intro _ _ Hi2i (orb_false_intro _ _ Hi2el Hi2e'))
                  (orb_false_intro _ _ Hi1i (orb_false_intro _ _ Hi1el Hi1e'))).
 apply Bpr_cutr with (p1:=¬∀(i2∙¬(∂(i2)∈el⋀¬∀(i1∙¬(∂(i1)∈e⋀∂(i)≡∂(i2)↦∂(i1)))))).
  apply (Bpr_proe (G·∂(i)∈el×e) ∂(i) el e _ _
                  ((snd DB_eq_imp) _ _ Hi2i1)
                  (orb_false_intro _ _ Hi2i (orb_false_intro _ _ Hi2el Hi2e))
                  (orb_false_intro _ _ Hi1i (orb_false_intro _ _ Hi1el Hi1e))); apply Bpr_axom.
  apply Bpr_impe; apply Bpr_clear; apply Bpr_ctnn; apply Bpr_impi;
   assert (H'i2G:i2∖ˠG·∀(i2∙¬(∂(i2)∈el⋀¬∀(i1∙¬(∂(i1)∈e'⋀∂(i)≡∂(i2)↦∂(i1)))))).
   apply Bg_mem_free; intros p Hp; rewrite Bg_add_mem in Hp;
    destruct (orb_true_elim _ _ Hp) as [HpG | Hpp'].
    apply (Bg_free_mem _ _ Hi2G _ HpG).
    reflect_in Bp_eq_imp Hpp'; rewrite Hpp';
     apply (Bp_forall_free i2 (¬(∂(i2)∈el⋀¬∀(i1∙¬(∂(i1)∈e'⋀∂(i)≡∂(i2)↦∂(i1)))))).
  apply Bpr_fori with (2:=H'i2G);
   apply Bpr_cutr with (p1:=¬(∂(i2)∈el⋀¬∀(i1∙¬(∂(i1)∈e'⋀∂(i)≡∂(i2)↦∂(i1))))).
   apply Bpr_fore_id with (i:=i2); apply Bpr_axom.
   apply Bpr_impe; apply Bpr_ctnn; apply Bpr_clear; Btac_prop; apply Bpr_impe; apply Bpr_clear;
    apply Bpr_ctnn; apply Bpr_impi; apply Bpr_fori.
    apply Bpr_cutr with (p1:=¬(∂(i1)∈e'⋀∂(i)≡∂(i2)↦∂(i1))).
     apply Bpr_fore_id with (i:=i1); apply Bpr_axom.
     apply Bpr_impe; apply Bpr_clear; apply Bpr_ctnn; Btac_prop; apply Bpr_clear; apply Bpr_impe;
      apply Bpr_fore_id with (i:=i1); apply (Bpr_powe _ _ _ _ Hi1e Hi1e' Hinc).
    apply Bg_mem_free; intros p Hp; rewrite Bg_add_mem in Hp;
     destruct (orb_true_elim _ _ Hp) as [H'p | H'p].
     apply Bg_free_mem with (2:=H'p); apply Hi1G.
     reflect_in Bp_eq_imp H'p; rewrite H'p; apply Bp_forall_free.
Qed.

Theorem repl_cmp_l:forall (G:Γ)(p p':Π)(e:Ε)(i:Ι), G⊢p⇒p'->(i∖ˠG)->G⊢{i∊e∣p}⊆{i∊e∣p'}.
Proof.
 intros G p p' e i Himp HiG; unfold binc; destruct (gfresh (G·∂(i)∈{i∊e∣p}×{i∊e∣p'}) 1) as [n' Hi'];
  set (i':=¡(1,n')); fold i' in Hi'.
 assert (Hi'G:i'∖ˠG). apply Bg_free_inc with (G1:=G·∂(i)∈{i∊e∣p}×{i∊e∣p'});
                      [apply Hi' | apply Bg_inc_add].
 assert (H'i':i'∖∂(i)∈{i∊e∣p}×{i∊e∣p'}). apply Bg_free_mem with (1:=Hi'); apply Bg_add_mem_new.
 unfold Bt_free in H'i'; unfold Bp_free in H'i';
  fold Be_free Bp_free in H'i'; destruct (orb_false_elim _ _ H'i') as [Hi'i H''i'];
  destruct (orb_false_elim _ _ H''i') as [Hi'ep Hi'ep']; clear H'i' H''i'.
 apply (Bpr_powi G _ _ _ Hi'ep Hi'ep'); apply Bpr_fori with (2:=Hi'G); apply Bpr_impi;
  apply Bpr_cmpi.
  apply Bpr_cmpl with (i:=i)(p:=p); apply Bpr_axom.
  apply Bpr_cutr with (p1:=〈i←∂(i')@Π@p〉).
   apply Bpr_cmpr with (e2:=e); apply Bpr_axom.
   apply Bpr_impe; apply Bpr_clear; change (〈i←∂(i')@Π@p〉⇒〈i←∂(i')@Π@p'〉) with 〈i←∂(i')@Π@p⇒p'〉;
    apply (Bpr_sbsi _ _ ∂(i') _ Himp HiG).
Qed.

Theorem repl_cmp_r:forall (G:Γ)(p:Π)(e e':Ε)(i:Ι), G⊢e⊆e'->G⊢{i∊e∣p}⊆{i∊e'∣p}.
Proof.
 intros G p e e' i; unfold binc; intros Hinc;
  destruct (gfresh (G·∂(i)∈{i∊e∣p}×{i∊e'∣p}) 1) as [n' Hi']; set (i':=¡(1,n')); fold i' in Hi'.
 assert (Hi'G:i'∖ˠG). apply Bg_free_inc with (G1:=G·∂(i)∈{i∊e∣p}×{i∊e'∣p});
                      [apply Hi' | apply Bg_inc_add].
 assert (H'i':i'∖∂(i)∈{i∊e∣p}×{i∊e'∣p}). apply Bg_free_mem with (1:=Hi'); apply Bg_add_mem_new.
 unfold Bt_free in H'i'; unfold Bp_free in H'i';
  fold Be_free Bp_free in H'i'; destruct (orb_false_elim _ _ H'i') as [Hi'i H''i'];
  destruct (orb_false_elim _ _ H''i') as [Hi'ep Hi'e'p]; clear H'i' H''i'.
 apply (Bpr_powi G _ _ _ Hi'ep Hi'e'p); apply Bpr_fori with (2:=Hi'G); apply Bpr_impi;
  apply Bpr_cmpi.
  apply Bpr_cutr with (p1:=∂(i')∈e).
   apply Bpr_cmpl with (i:=i)(p:=p); apply Bpr_axom.
   apply Bpr_impe; apply Bpr_clear; apply Bpr_fore_id with (i:=i'); apply Bpr_powe with (3:=Hinc);
    simpl in *; [apply (proj1 (orb_false_elim _ _ Hi'ep)) |
                 apply (proj1 (orb_false_elim _ _ Hi'e'p))].
  apply Bpr_cmpr with (e2:=e); apply Bpr_axom.
Qed.

(* Congruence-like results -----------------------------------------------------------------------*)

Theorem congr_not:forall (G:Γ)(p1 p2:Π), G⊢p1⇔p2->G⊢¬p1⇔¬p2.
Proof.
 intros G p1 p2 Hiff; apply (Bpr_cutr _ _ (¬p1⇔¬p2) Hiff); apply Bpr_impe; Btac_prop.
Qed.

Theorem congr_for:forall (G:Γ)(p1 p2:Π), G⊢p1⇔p2->forall (i:Ι), (i∖ˠG)->G⊢∀(i∙p1)⇔∀(i∙p2).
Proof.
 intros G p1 p2 Hiff i Hi; unfold biff; apply Bpr_andi; apply Bpr_impi.
  apply Bpr_fori.
   apply Bpr_cutr with (p1:=p1).
    pattern p1 at 2; replace p1 with 〈i←∂(i)@Π@p1〉.
     apply Bpr_fore; apply Bpr_axom.
     irewrite (Bt_esubst_id p1 i); apply refl_equal.
    apply Bpr_impe; apply Bpr_clear; apply (Bpr_andl _ _ _ Hiff).
   apply Bg_free_add.
    apply Hi.
    apply Bp_forall_free.
  apply Bpr_fori.
   apply Bpr_cutr with (p1:=p2).
    pattern p2 at 2; replace p2 with 〈i←∂(i)@Π@p2〉.
     apply Bpr_fore; apply Bpr_axom.
     irewrite (Bt_esubst_id p2 i); apply refl_equal.
    apply Bpr_impe; apply Bpr_clear; apply (Bpr_andr _ _ _ Hiff).
   apply Bg_free_add.
    apply Hi.
    apply Bp_forall_free.
Qed.

Theorem congr_for_raw:forall (G:Γ)(p1 p2:Π)(u n:nat), (¡(S u,n)∖ˠG·p1·p2)->
                      G⊢(Bp_app p1 ∂(¡(S u,n)))⇔(Bp_app p2 ∂(¡(S u,n)))->G⊢∀Δ(p1)⇔∀Δ(p2).
Proof.
 intros G p1 p2 u n Hfree Hiff; replace ∀Δ(p1) with ∀(¡(S u,n)∙Bp_app p1 ∂(¡(S u,n))).
  replace ∀Δ(p2) with ∀(¡(S u,n)∙Bp_app p2 ∂(¡(S u,n))).
   apply congr_for.
    apply Hiff.
    apply (Bg_free_inc _ G _ Hfree); reflect Bg_inc_imp; intros p Hp; Btac_gamma.
   apply sym_equal; apply (Bp_forall_inv p2 ¡(S u,n)); apply (Bg_free_mem _ _ Hfree); Btac_gamma.
  apply sym_equal; apply (Bp_forall_inv p1 ¡(S u,n)); apply (Bg_free_mem _ _ Hfree); Btac_gamma.
Qed.

Theorem congr_and:forall (G:Γ)(p1 p1' p2 p2':Π), G⊢p1⇔p1'->G⊢p2⇔p2'->G⊢(p1⋀p2)⇔(p1'⋀p2').
Proof.
 intros G p1 p1' p2 p2' Hiff1 Hiff2; apply (Bpr_cutr _ _ ((p1⋀p2)⇔(p1'⋀p2')) Hiff1); apply Bpr_impe;
  apply (Bpr_cutr _ _ ((p1⇔p1')⇒((p1⋀p2)⇔(p1'⋀p2'))) Hiff2); apply Bpr_impe; Btac_prop.
Qed.

Theorem congr_imp:forall (G:Γ)(p1 p1' p2 p2':Π), G⊢p1⇔p1'->G⊢p2⇔p2'->G⊢(p1⇒p2)⇔(p1'⇒p2').
Proof.
 intros G p1 p1' p2 p2' Hiff1 Hiff2; apply (Bpr_cutr _ _ ((p1⇒p2)⇔(p1'⇒p2')) Hiff1); apply Bpr_impe;
  apply (Bpr_cutr _ _ ((p1⇔p1')⇒((p1⇒p2)⇔(p1'⇒p2'))) Hiff2); apply Bpr_impe; Btac_prop.
Qed.

Theorem congr_equ:forall (G:Γ)(e1 e1' e2 e2':Ε), G⊢e1≡e1'->G⊢e2≡e2'->G⊢(e1≡e2)⇔(e1'≡e2').
Proof.
 intros G e1 e1' e2 e2' Hequ1 Hequ2; unfold biff; apply Bpr_andi; apply Bpr_impi.
  destruct (fresh (e1'≡e2') 0) as [i Hi]; replace (e1'≡e2') with 〈¡(0,i)←e1'@Π@∂(¡(0,i))≡e2'〉.
   apply Bpr_leib with (e1:=e1).
    apply Bpr_clear; apply Hequ1.
    simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1);
     clear i Hi; destruct (fresh (e1≡e2') 0) as [i Hi];
     replace (e1≡e2') with 〈¡(0,i)←e2'@Π@e1≡∂(¡(0,i))〉.
      apply Bpr_leib with (e1:=e2).
       apply  Bpr_clear; apply Hequ2.
       simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
        apply Bpr_axom.
      simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2');
       apply refl_equal.
     simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1');
      apply refl_equal.
  destruct (fresh (e1≡e2) 0) as [i Hi]; replace (e1≡e2) with 〈¡(0,i)←e1@Π@∂(¡(0,i))≡e2〉.
   apply Bpr_leib with (e1:=e1').
    apply Bpr_clear; apply Bpr_equ_sym; apply Hequ1.
    simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1');
     clear i Hi; destruct (fresh (e1'≡e2) 0) as [i Hi];
     replace (e1'≡e2) with 〈¡(0,i)←e2@Π@e1'≡∂(¡(0,i))〉.
      apply Bpr_leib with (e1:=e2').
       apply  Bpr_clear; apply Bpr_equ_sym; apply Hequ2.
       simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2');
        apply Bpr_axom.
      simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
       apply refl_equal.
     simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1);
      apply refl_equal.
Qed.

Theorem congr_ins:forall (G:Γ)(e1 e1' e2 e2':Ε), G⊢e1≡e1'->G⊢e2≡e2'->G⊢(e1∈e2)⇔(e1'∈e2').
Proof.
 intros G e1 e1' e2 e2' Hequ1 Hequ2; unfold biff; apply Bpr_andi; apply Bpr_impi.
  destruct (fresh (e1'∈e2') 0) as [i Hi]; replace (e1'∈e2') with 〈¡(0,i)←e1'@Π@∂(¡(0,i))∈e2'〉.
   apply Bpr_leib with (e1:=e1).
    apply Bpr_clear; apply Hequ1.
    simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1);
     clear i Hi; destruct (fresh (e1∈e2') 0) as [i Hi];
     replace (e1∈e2') with 〈¡(0,i)←e2'@Π@e1∈∂(¡(0,i))〉.
      apply Bpr_leib with (e1:=e2).
       apply  Bpr_clear; apply Hequ2.
       simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
        apply Bpr_axom.
      simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2');
       apply refl_equal.
     simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1');
      apply refl_equal.
  destruct (fresh (e1∈e2) 0) as [i Hi]; replace (e1∈e2) with 〈¡(0,i)←e1@Π@∂(¡(0,i))∈e2〉.
   apply Bpr_leib with (e1:=e1').
    apply Bpr_clear; apply Bpr_equ_sym; apply Hequ1.
    simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1');
     clear i Hi; destruct (fresh (e1'∈e2) 0) as [i Hi];
     replace (e1'∈e2) with 〈¡(0,i)←e2@Π@e1'∈∂(¡(0,i))〉.
      apply Bpr_leib with (e1:=e2').
       apply  Bpr_clear; apply Bpr_equ_sym; apply Hequ2.
       simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2');
        apply Bpr_axom.
      simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
       apply refl_equal.
     simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ Hi)) e1);
      apply refl_equal.
Qed.

Theorem congr_chc:forall (G:Γ)(e1 e2:Ε), G⊢e1≡e2->G⊢↓(e1)≡↓(e2).
Proof.
 intros G e1 e2 Hequ; destruct (fresh (e1≡e2) 0) as [i Hi];
  replace (↓(e1)≡↓(e2)) with 〈¡(0,i)←e2@Π@↓(e1)≡↓(∂(¡(0,i)))〉.
   apply Bpr_leib with (e1:=e1).
    apply Hequ.
    simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e1);
     apply Bpr_refl.
   simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
    apply refl_equal.
Qed.

Theorem congr_pow:forall (G:Γ)(e1 e2:Ε), G⊢e1≡e2->G⊢↑(e1)≡↑(e2).
Proof.
 intros G e1 e2 Hequ; destruct (fresh (e1≡e2) 0) as [i Hi];
  replace (↑(e1)≡↑(e2)) with 〈¡(0,i)←e2@Π@↑(e1)≡↑(∂(¡(0,i)))〉.
   apply Bpr_leib with (e1:=e1).
    apply Hequ.
    simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e1);
     apply Bpr_refl.
   simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
    apply refl_equal.
Qed.

Theorem congr_cpl:forall (G:Γ)(e1 e1' e2 e2':Ε), G⊢e1≡e1'->G⊢e2≡e2'->G⊢(e1↦e2)≡(e1'↦e2').
Proof.
 intros G e1 e1' e2 e2' Hequ1 Hequ2; destruct (fresh ((e1↦e2)≡(e1'↦e2')) 0) as [i Hi];
  replace ((e1↦e2)≡(e1'↦e2')) with 〈¡(0,i)←e1'@Π@(e1↦e2)≡∂(¡(0,i))↦e2'〉.
   apply Bpr_leib with (e1:=e1).
    apply Hequ1.
    simpl; nat_simpl; simpl;
     irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1);
     irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1);
     irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ Hi)))) e1);
     clear i Hi; destruct (fresh ((e1↦e2)≡(e1↦e2')) 0) as [i Hi];
      replace ((e1↦e2)≡(e1↦e2')) with 〈¡(0,i)←e2'@Π@(e1↦e2)≡e1↦∂(¡(0,i))〉.
      apply Bpr_leib with (e1:=e2).
       apply Hequ2.
       simpl; nat_simpl; simpl;
        irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2);
        irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2);
        apply Bpr_refl.
      simpl; nat_simpl; simpl;
       irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2');
       irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2');
       apply refl_equal.
  simpl; nat_simpl; simpl;
   irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1');
   irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1');
   irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ Hi)))) e1');
   apply refl_equal.
Qed.

Theorem congr_pro:forall (G:Γ)(e1 e1' e2 e2':Ε), G⊢e1≡e1'->G⊢e2≡e2'->G⊢(e1×e2)≡(e1'×e2').
Proof.
 intros G e1 e1' e2 e2' Hequ1 Hequ2; destruct (fresh ((e1×e2)≡(e1'×e2')) 0) as [i Hi];
  replace ((e1×e2)≡(e1'×e2')) with 〈¡(0,i)←e1'@Π@(e1×e2)≡∂(¡(0,i))×e2'〉.
   apply Bpr_leib with (e1:=e1).
    apply Hequ1.
    simpl; nat_simpl; simpl;
     irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1);
     irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1);
     irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ Hi)))) e1);
     clear i Hi; destruct (fresh ((e1×e2)≡(e1×e2')) 0) as [i Hi];
      replace ((e1×e2)≡(e1×e2')) with 〈¡(0,i)←e2'@Π@(e1×e2)≡e1×∂(¡(0,i))〉.
      apply Bpr_leib with (e1:=e2).
       apply Hequ2.
       simpl; nat_simpl; simpl;
        irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2);
        irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2);
        apply Bpr_refl.
      simpl; nat_simpl; simpl;
       irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2');
       irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e2');
       apply refl_equal.
  simpl; nat_simpl; simpl;
   irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1');
   irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi)))) e1');
   irewrite (Bt_esubst_free _ _ (proj2 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ Hi)))) e1');
   apply refl_equal.
Qed.

Theorem congr_imp_cmp:forall (G:Γ)(e:Ε)(p p':Π), G⊢p⇒p'->
                      forall (i:Ι), (i∖ˠG)->(i∖e)->G⊢{i∊e∣p}⊆{i∊e∣p'}.
Proof.
 intros G e p p' Himp i HiG Hie; unfold binc; apply (Bpr_powi G {i∊e∣p} {i∊e∣p'} i).
  apply Be_cmpset_free; apply Hie.
  apply Be_cmpset_free; apply Hie.
  apply Bpr_fori; [ | apply HiG].
   apply Bpr_impi; apply Bpr_cmpi.
    apply (Bpr_cmpl (G·∂(i)∈{i∊e∣p}) p ∂(i) e i); apply Bpr_axom.
    irewrite (Bt_esubst_id p' i); apply Bpr_cutr with (p1:=p).
     generalize (Bt_esubst_id p i); intros Heq; injection Heq; clear Heq; intros Heq;
      pattern p at 2; rewrite <- Heq; apply (Bpr_cmpr (G·∂(i)∈{i∊e∣p}) p ∂(i) e i); apply Bpr_axom.
     apply Bpr_impe; apply Bpr_clear; apply Himp.
Qed.

Theorem congr_cmp:forall (G:Γ)(e e':Ε)(p p':Π), G⊢e≡e'->G⊢p⇔p'->
                  forall (i:Ι), (i∖ˠG)->(i∖e)->G⊢{i∊e∣p}≡{i∊e'∣p'}.
Proof.
 intros G e e' p p' Hequ Hiff i HiG Hie; destruct (fresh (∂(i)∈(e×e')⋀(p⋀p')) 0) as [i' Hi'];
  assert (Heq:{i∊e∣p}≡{i∊e'∣p'}=〈¡(0,i')←e'@Π@{i∊e∣p}≡{i∊∂(¡(0,i'))∣p'}〉).
  simpl; nat_simpl; simpl;
   irewrite (Bt_esubst_free _ _ 
             (proj1 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi')))))) e');
   irewrite (Bt_esubst_free _ _
              (Bt_abstr_free_free _ _ i (proj1 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ Hi')))))
              (Be_lift e'));
   irewrite (Bt_esubst_free _ _
              (Bt_abstr_free_free _ _ i (proj2 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ Hi')))))
              (Be_lift e')); apply refl_equal.
 rewrite Heq; clear Heq; apply Bpr_leib with (e1:=e).
  apply Hequ.
  simpl; nat_simpl; simpl;
   irewrite (Bt_esubst_free _ _ 
             (proj1 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ (proj1 (Bt_free_sub _ _ Hi')))))) e);
   irewrite (Bt_esubst_free _ _
              (Bt_abstr_free_free _ _ i (proj1 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ Hi')))))
              (Be_lift e));
   irewrite (Bt_esubst_free _ _
              (Bt_abstr_free_free _ _ i (proj2 (Bt_free_sub _ _ (proj2 (Bt_free_sub _ _ Hi')))))
              (Be_lift e)); apply Bpr_extn.
   fold (Be_cmpset i e p); fold (Be_cmpset i e p');
    apply (congr_imp_cmp _ e _ _ (Bpr_andl _ _ _ Hiff) _ HiG Hie).
   fold (Be_cmpset i e p); fold (Be_cmpset i e p');
    apply (congr_imp_cmp _ e _ _ (Bpr_andr _ _ _ Hiff) _ HiG Hie).
Qed.

(*================================================================================================*)