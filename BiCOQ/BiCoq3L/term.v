(*==================================================================================================
  Project : BiCoq3
  Module : term.v
  This module defines B terms, that is expressions and predicates, using a De Bruijn representation.
  We associate to De Bruijn levels (not indexes) a natural value indicating the namespace. An
  infinity of namespaces is defined, 0 being the classical one in which our binders capture
  variables; the other namespaces (1 and greater) have no associated binders so all such De Bruijn
  levels are dangling and represent free variables. The idea can be extended by associating to any
  binder a natural parameter to represent the namespace which is bound, but at the cost of
  (slightly) more complex functions over terms and more importantly of alpha-equivalence (unless
  namespaces represent constant sorts), but these aspects are still to be explored.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jun 2008, Md: Sep 2008
  ================================================================================================*)

Require Export basic_nat.
Require Export Le.
Require Export Peano_dec.
Require Export Max.
Require Export Compare_dec.

(* Marked De Bruijn indexes ----------------------------------------------------------------------*)

Inductive DBi:Set := DBi_:nat->nat->DBi.
Notation "'Ι'":= (DBi).
Notation "'¡(' u ',' n ')'" := (DBi_ u n).

Definition DB_eq(i1 i2:Ι):Β :=
 match i1 with ¡(u1,n1) => match i2 with ¡(u2,n2) => (nat_eq u1 u2) && (nat_eq n1 n2) end end.

Theorem DB_eq_imp:((fun x y:Ι=>x=y)⇝²DB_eq).
Proof.
 unfold implements2; split; unfold DB_eq; intros [u1 n1] [u2 n2] Hf.
  destruct (andb_prop _ _ Hf) as [Hu Hn]; reflect_in nat_eq_imp Hn; rewrite Hn;
   reflect_in nat_eq_imp Hu; rewrite Hu; apply refl_equal.
  injection; intros Hn Hu; destruct (andb_false_elim _ _ Hf) as [Hu' | Hn'].
   rewrite Hu in Hu'; reflect_in nat_eq_imp Hu'; apply Hu'; apply refl_equal.
   rewrite Hn in Hn'; reflect_in nat_eq_imp Hn'; apply Hn'; apply refl_equal.
Qed.

Theorem DB_eq_refl:forall (i:Ι), DB_eq i i=⊤.
Proof.
 intros i; reflect DB_eq_imp; apply refl_equal.
Qed.

Definition ulft(i:Ι):Ι := match i with ¡(u,n') => ¡(u,if eq_0 u then S n' else n') end.

Theorem DB_eq_ulft:forall (i1 i2:Ι), DB_eq (ulft i1) (ulft i2)=DB_eq i1 i2.
Proof.
 intros [[| u1] n1] [[| u2] n2]; simpl; nat_simpl; bool_simpl; apply refl_equal.
Qed.

Ltac DB_simpl := repeat (rewrite DB_eq_refl in * ||
                         rewrite DB_eq_ulft  in *).

(* B terms ---------------------------------------------------------------------------------------*)

Inductive Bprd:Set :=
 | bnot:Bprd->Bprd        (* Negation *)
 | bfor:Bprd->Bprd        (* Universal De Bruijn quantifier *)
 | band:Bprd->Bprd->Bprd  (* Conjunction *)
 | bimp:Bprd->Bprd->Bprd  (* Implication *)
 | bprp:nat->Bprd         (* Propositional variable *)
 | bequ:Bexp->Bexp->Bprd  (* Equality *)
 | bins:Bexp->Bexp->Bprd  (* Membership *)
with Bexp:Set :=
 | bbig:Bexp              (* BIG set *)
 | bchc:Bexp->Bexp        (* Choice *)
 | bpow:Bexp->Bexp        (* Powerset *)
 | bcpl:Bexp->Bexp->Bexp  (* Pair *)
 | bpro:Bexp->Bexp->Bexp  (* Cartesian product *)
 | bbge:nat->Bexp         (* Element of big *)
 | bvar:Ι->Bexp           (* De Bruijn index *)
 | bcmp:Bexp->Bprd->Bexp. (* Comprehension set binder *)

Notation "'Π'" := (Bprd).
Notation "'¬' p" := (bnot p) (at level 75, right associativity).
Notation "'∀Δ(' p ')'" := (bfor p).
Notation "p '⋀' q" := (band p q) (at level 80, right associativity).
Notation "p '⇒' q" := (bimp p q) (at level 85, right associativity).
Notation "'∐' n" := (bprp n) (at level 30, no associativity).
Notation "e '≡' f" := (bequ e f) (at level 70, no associativity).
Notation "e '∈' f" := (bins e f) (at level 70, no associativity).

Notation "'Ε'" := (Bexp).
Notation "'Ω'" := (bbig).
Notation "'↓(' e ')'" := (bchc e).
Notation "'↑(' e ')'" := (bpow e).
Notation "e '↦' f" := (bcpl e f) (at level 50, left associativity).
Notation "e '×' f" := (bpro e f) (at level 40, left associativity).
Notation "'∞' n" := (bbge n) (at level 30, no associativity).
Notation "'∂(' i ')'" := (bvar i).
Notation "'ⅭΔ(' e ',' p ')'" := (bcmp e p).

(* Additional operators --------------------------------------------------------------------------*)

Definition blor(p q:Π) := ¬p⇒q. (* Disjunction *)
Notation "p '⋁' q" := (blor p q) (at level 85, right associativity).

Definition biff(p q:Π) := (p⇒q)⋀(q⇒p). (* Equivalence *)
Notation "p '⇔' q" := (biff p q) (at level 84, no associativity).

Definition bneq(e f:Ε) := ¬e≡f. (* Difference *)
Notation "e '≢' f" := (bneq e f) (at level 70, no associativity).

Definition bnin(e f:Ε) := ¬e∈f. (* Negation of membership *)
Notation "e '∉' f" := (bnin e f) (at level 70, no associativity).

Definition binc(e f:Ε) := e∈↑(f). (* Inclusion *)
Notation "e '⊆' f" := (binc e f) (at level 70, no associativity).

(* B terms ---------------------------------------------------------------------------------------*)

Inductive Btrm:Set := be_:Ε->Btrm | bp_:Π->Btrm.
Notation "'Τ'" := (Btrm).
Coercion be_:Bexp>->Btrm.
Coercion bp_:Bprd>->Btrm.

(* Interface for built-in Ocaml functions --------------------------------------------------------*)

Variable Bp_eq:Π->Π->Β.
Variable Bp_eq_imp:((fun x y:Π=>x=y)⇝²Bp_eq).

Variable Be_eq:Ε->Ε->Β.
Variable Be_eq_imp:((fun x y:Ε=>x=y)⇝²Be_eq).

Definition Bt_eq(t1 t2:Τ):=
 match (t1,t2) with
 | (be_ e1',be_ e2') => Be_eq e1' e2'
 | (bp_ p1',bp_ p2') => Bp_eq p1' p2'
 | _ => false
 end.

Theorem Bt_eq_imp:((fun x y:Τ=>x=y)⇝²Bt_eq).
Proof.
 unfold implements2; split; unfold Bt_eq; intros [e1 | p1] [e2 | p2] Hf.
  rewrite (fst Be_eq_imp _ _ Hf); apply refl_equal.
  inversion Hf.
  inversion Hf.
  rewrite (fst Bp_eq_imp _ _ Hf); apply refl_equal.
  intros Heq; inversion Heq as [Heq']; rewrite (imppt2 Be_eq_imp _ _ Heq') in Hf;
   inversion Hf.
  intros Heq; inversion Heq.
  intros Heq; inversion Heq.
  intros Heq; inversion Heq as [Heq']; rewrite (imppt2 Bp_eq_imp _ _ Heq') in Hf;
   inversion Hf.
Qed.

(* Depth function --------------------------------------------------------------------------------*)

Fixpoint Bp_depth(p:Π){struct p}:nat :=
 match p with
 | ¬p' => S(Bp_depth p')
 | ∀Δ(p') => S(Bp_depth p')
 | p1⋀p2 => S(max (Bp_depth p1) (Bp_depth p2))
 | (p1⇒p2) => S(max (Bp_depth p1) (Bp_depth p2))
 | ∐(_) => 0
 | e1≡e2 => S(max (Be_depth e1) (Be_depth e2))
 | e1∈e2 => S(max (Be_depth e1) (Be_depth e2))
 end
with Be_depth(e:Ε){struct e}:nat :=
 match e with
 | Ω => 0
 | ↓(e') => S(Be_depth e')
 | ↑(e') => S(Be_depth e')
 | e1↦e2 => S(max (Be_depth e1) (Be_depth e2))
 | e1×e2 => S(max (Be_depth e1) (Be_depth e2))
 | ∞(_) => 0
 | ∂(_) => 0
 | ⅭΔ(e',p') => S(max (Be_depth e') (Bp_depth p'))
 end.

Definition Bt_depth(t:Τ):nat := match t with be_ e' => Be_depth e' | bp_ p' => Bp_depth p' end.
Notation "'↧(' t ')'":=(Bt_depth t).
(* Returns the depth of a term *)

(* Induction over terms --------------------------------------------------------------------------*)

Theorem wf_term:forall (P:Τ->Type),
                (forall (t:Τ), (forall (t':Τ), ↧(t')<↧(t)->P t')->P t)->forall (t:Τ), P t.
Proof.
 intros P Hind; cut  ((forall (p:Π), P p)*(forall (e:Ε), P e)).
  intros [Hp He] [e | p]; [apply He | apply Hp].
  apply (wf_measure_dbl Bp_depth Be_depth P P).
   intros p Hp He; apply (Hind p); intros [e' | p']; [apply (He e') | apply  (Hp p')].
   intros e Hp He; apply (Hind e); intros [e' | p']; [apply (He e') | apply  (Hp p')].
Qed.

Definition sdepth(t:Τ) := (lt_n_Sn ↧(t)).
Definition sdepth_l(t1 t2:Τ) := lt_le_trans _ _ _ (lt_n_Sn _) (le_n_S _ _ (le_max_l ↧(t1) ↧(t2))).
Definition sdepth_r(t1 t2:Τ) := lt_le_trans _ _ _ (lt_n_Sn _) (le_n_S _ _ (le_max_r ↧(t1) ↧(t2))).
(* Nota: These definitions are just "macros" for proofs using wf_term; they allows to build a proof
   that a direct subterm has a depth strictly smaller than the term of which it is extracted. *)

(* Free variables --------------------------------------------------------------------------------*)

Fixpoint Bp_free(i:Ι)(p:Π){struct p}:Β :=
 match p with
 | ¬p' => Bp_free i p'
 | ∀Δ(p') => Bp_free (ulft i) p'
 | p1⋀p2 => (Bp_free i p1) || (Bp_free i p2)
 | (p1⇒p2) => (Bp_free i p1) || (Bp_free i p2)
 | ∐(_) => ⊥
 | e1≡e2 => (Be_free i e1) || (Be_free i e2)
 | e1∈e2 => (Be_free i e1) || (Be_free i e2)
 end
with Be_free(i:Ι)(e:Ε){struct e}:Β :=
 match e with
 | Ω => ⊥
 | ↓(e') => Be_free i e'
 | ↑(e') => Be_free i e'
 | e1↦e2 => (Be_free i e1) || (Be_free i e2)
 | e1×e2 => (Be_free i e1) || (Be_free i e2)
 | ∞(_) => ⊥
 | ∂(i') => DB_eq i i'
 | ⅭΔ(e',p') => (Be_free i e') || (Bp_free (ulft i) p')
 end.

Definition Bt_free(i:Ι)(t:Τ):Β := match t with be_ e' => Be_free i e' | bp_ p' => Bp_free i p' end.
(* Indicates if a variable appears free in a term *)
Notation "i '∖' t":=(Bt_free i t=false) (at level 96, no associativity).

(* Fresh free variables --------------------------------------------------------------------------*)
(* A natural parameter is passed to indicate which namespace should be searched. *)

Fixpoint Bp_fresh(u:nat)(p:Π){struct p}:nat :=
 match p with
 | ¬p' => Bp_fresh u p'
 | ∀Δ(p') => if eq_0 u then pred (Bp_fresh u p') else (Bp_fresh u p')
 | p1⋀p2 => max (Bp_fresh u p1) (Bp_fresh u p2)
 | (p1⇒p2) => max (Bp_fresh u p1) (Bp_fresh u p2)
 | ∐(_) => 0
 | e1≡e2 => max (Be_fresh u e1) (Be_fresh u e2)
 | e1∈e2 => max (Be_fresh u e1) (Be_fresh u e2)
 end
with Be_fresh(u:nat)(e:Ε){struct e}:nat :=
 match e with
 | Ω => 0
 | ↓(e') => Be_fresh u e'
 | ↑(e') => Be_fresh u e'
 | e1↦e2 => max (Be_fresh u e1) (Be_fresh u e2)
 | e1×e2 => max (Be_fresh u e1) (Be_fresh u e2)
 | ∞(_) => 0
 | ∂(¡(u',n')) => if nat_eq u u' then (S n') else 0
 | ⅭΔ(e',p') => max (Be_fresh u e') (if eq_0 u then pred (Bp_fresh u p') else (Bp_fresh u p'))
 end.

Definition Bt_fresh(u:nat)(t:Τ):nat :=
 match t with be_ e' => Be_fresh u e' | bp_ p' => Bp_fresh u p' end.
Notation "'⋕(' u ',' t ')'":= (Bt_fresh u t).
(* Provides a fresh free variable *)

Theorem Bt_fresh_free:forall (t:Τ)(u n:nat), ⋕(u,t)<=n->(¡(u,n)∖t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (u n:nat), ⋕(u,t)<=n->(¡(u,n)∖t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind u n Hn;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) _ _ Hn));
  try (simpl; apply orb_false_intro;
       [apply (Hind _ (sdepth_l t1 t2) u n); apply (le_trans _ _ _ (le_max_l ⋕(u,t1) ⋕(u,t2)) Hn) |
        apply (Hind _ (sdepth_r t1 t2) u n); apply (le_trans _ _ _ (le_max_r ⋕(u,t1) ⋕(u,t2)) Hn)]).
  simpl; simpl in Hn; case_eq (nat_eq u u'); intros Heq; simpl; rewrite Heq in Hn.
   reflect nat_eq_imp; intros Heq'; rewrite Heq' in Hn; destruct (le_Sn_n _ Hn).
   apply refl_equal.
  simpl; apply orb_false_intro.
   apply (Hind _ (sdepth_l t1 t2) u n);
    apply (le_trans _ _ _ (le_max_l ⋕(u,t1) (if (eq_0 u) then pred ⋕(u,t2) else ⋕(u,t2))) Hn).
   destruct u as [ | u].
    apply (Hind _ (sdepth_r t1 t2) 0 (S n)); apply (le_trans ⋕(0,t2) (S (pred ⋕(0,t2))) (S n)).
     destruct ⋕(0,t2) as [ | n']; [apply le_O_n | apply le_refl].
     apply (le_n_S _ _ (le_trans _ _ _ (le_max_r ⋕(0,t1) (pred ⋕(0,t2))) Hn)).
    apply (Hind _ (sdepth_r t1 t2) (S u) n);
     apply (le_trans _ _ _ (le_max_r ⋕(S u,t1) ⋕(S u,t2)) Hn).
  destruct u as [ | u]; simpl; simpl in Hn.
   apply (Hind _ (sdepth t) 0 (S n)); simpl; destruct (Bp_fresh 0 t);
   [apply le_O_n | simpl in Hn; apply (le_n_S _ _ Hn)].
   apply (Hind _ (sdepth t) (S u) n Hn).
Qed.

Theorem Bt_fresh_pred:forall (t:Τ)(u:nat), 0<⋕(u,t)->Bt_free ¡(u,pred ⋕(u,t)) t=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (u:nat), 0<⋕(u,t)->Bt_free ¡(u,pred ⋕(u,t)) t=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind u Hlt;
  try (destruct (lt_irrefl _ Hlt)); try (apply (Hind _ (sdepth t) u Hlt));
  try (destruct (max_dec ⋕(u,t1) ⋕(u,t2)) as [Heq | Heq]; simpl in Heq; rewrite Heq in *;
       apply orb_true_intro;
       [apply or_introl; apply (Hind _ (sdepth_l t1 t2) u Hlt) |
        apply or_intror; apply (Hind _ (sdepth_r t1 t2) u Hlt)]).
  case_eq (nat_eq u u'); intros Hu; simpl;
   [reflect nat_eq_imp; apply refl_equal | rewrite Hu in Hlt; destruct (lt_irrefl _ Hlt)].
  destruct u as [ | u].
   destruct (max_dec ⋕(0,t1) (pred ⋕(0,t2))) as [Heq | Heq]; simpl; simpl in Heq; simpl in Hlt;
    rewrite Heq in *; apply orb_true_intro.
    apply or_introl; apply (Hind _ (sdepth_l t1 t2) 0 Hlt).
    apply or_intror; generalize (Hind _ (sdepth_r t1 t2) 0); simpl;
     destruct (Bp_fresh 0 t2) as [ | n];
     [inversion Hlt |
      simpl; simpl in Hlt; intros Hi'; generalize (Hi' (lt_S _ _ Hlt)); clear Hi'; intros Hi';
      destruct n; [inversion Hlt | apply Hi']].
   destruct (max_dec ⋕(S u,t1) ⋕(S u,t2)) as [Heq | Heq]; simpl; simpl in Heq; simpl in Hlt;
    rewrite Heq in *; apply orb_true_intro;
    [apply or_introl; apply (Hind _ (sdepth_l t1 t2) (S u) Hlt) |
     apply or_intror; apply (Hind _ (sdepth_r t1 t2) (S u) Hlt)].
  destruct u as [ | u].
   generalize (Hind _ (sdepth t) 0); simpl; destruct (Bp_fresh 0 t); simpl; intros Hi;
    [inversion Hlt | simpl in Hlt; destruct n; [inversion Hlt | simpl; apply (Hi (lt_S _ _ Hlt))]].
   apply (Hind _ (sdepth t) (S u) Hlt).
Qed.

Theorem fresh:forall (t:Τ)(u:nat), {v:nat | ¡(u,v)∖t}.
Proof.
 intros t u; exists ⋕(u,t); apply Bt_fresh_free; apply le_refl.
Qed.

Definition Bt_ground(u:nat)(t:Τ):Β :=match ⋕(u,t) with 0 => true | _ => false end.
Notation "'∖∖(' u ',' t ')'":=(Bt_ground u t=⊤) (at level 30, no associativity).
(* Check whether or not a term has no free variable in a given namespace *)

Theorem Bt_ground_free:forall (t:Τ)(u:nat), ∖∖(u,t)->forall (n:nat), ¡(u,n)∖t.
Proof.
 intros t u Hg n; apply Bt_fresh_free; unfold Bt_ground in Hg; destruct ⋕(u,t);
  [apply le_O_n | inversion Hg].
Qed.

Theorem Bt_free_ground:forall (t:Τ)(u:nat), (forall (n:nat), ¡(u,n)∖t)->∖∖(u,t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (u:nat), (forall (n:nat), ¡(u,n)∖t)->∖∖(u,t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind u Hf;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) u Hf));
  try (unfold Bt_ground; simpl; case_eq ⋕(u,t1); case_eq ⋕(u,t2); simpl;
       [intros Ht2 Ht1; rewrite Ht1 in *; rewrite Ht2 in *; apply refl_equal |
        intros n2 Ht2 Ht1; generalize (Bt_fresh_pred t2 u); simpl; rewrite Ht2; simpl; clear Ht2;
         intros Ht2; generalize (Hf n2); intros Hn2; rewrite (Ht2 (lt_O_Sn n2)) in Hn2;
         case_eq (Bt_free ¡(u,n2) t1); simpl; intros Heq; rewrite Heq in *; simpl in Hn2;
         inversion Hn2 |
        intros Ht2 n1 Ht1; generalize (Bt_fresh_pred t1 u); simpl; rewrite Ht1; simpl; clear Ht1;
         intros Ht1; generalize (Hf n1); intros Hn1; rewrite (Ht1 (lt_O_Sn n1)) in Hn1;
         simpl in Hn1; inversion Hn1 |
        intros n2 Ht2 n1 Ht1; generalize (Bt_fresh_pred t1 u); simpl; rewrite Ht1; simpl; clear Ht1;
         intros Ht1; generalize (Hf n1); intros Hn1; rewrite (Ht1 (lt_O_Sn n1)) in Hn1;
         simpl in Hn1; inversion Hn1]).
  unfold Bt_ground; simpl;  case_eq (nat_eq u u').
   intros Heq; rewrite Heq in Hf; clear Heq; generalize (Hf n'); simpl; intros Heq;
    reflect_in nat_eq_imp Heq; destruct Heq; apply refl_equal.
   intros _; apply refl_equal.
  unfold Bt_ground; simpl; case_eq (Be_fresh u t1);
   case_eq (if eq_0 u then pred (Bp_fresh u t2) else Bp_fresh u t2); simpl.
   intros _ _; apply refl_equal.
   intros n2 Ht2 Ht1; assert (Ht2':0<⋕(u,t2)).
    simpl; case_eq (Bp_fresh u t2);
    [intros Ht2'; simpl in Ht2; rewrite Ht2' in *; destruct u as [ | u]; inversion Ht2 |
     intros n Ht2'; apply lt_O_Sn].
   generalize (Bt_fresh_pred t2 u Ht2'); simpl; destruct u as [ | u]; simpl in Ht2; simpl in Hf;
    rewrite Ht2; simpl; clear Ht2; intros Ht2; destruct (orb_false_elim _ _ (Hf n2)) as [_ Ht2''];
    rewrite Ht2'' in Ht2; apply Ht2.
   intros Ht2 n1 Ht1; assert (Ht1':0<⋕(u,t1)). simpl; rewrite Ht1; apply lt_O_Sn.
   generalize (Bt_fresh_pred t1 u Ht1'); simpl; rewrite Ht1; clear Ht1; simpl; intros Ht1;
    destruct (orb_false_elim _ _ (Hf n1)) as [Ht1'' _]; rewrite Ht1'' in Ht1; apply Ht1.
   intros n2 Ht2 n1 Ht1; assert (Ht1':0<⋕(u,t1)). simpl; rewrite Ht1; apply lt_O_Sn.
   generalize (Bt_fresh_pred t1 u Ht1'); simpl; rewrite Ht1; clear Ht1; simpl; intros Ht1;
    destruct (orb_false_elim _ _ (Hf n1)) as [Ht1'' _]; rewrite Ht1'' in Ht1; apply Ht1.
  unfold Bt_ground; destruct u as [ | u]; simpl; simpl in Hf.
   case_eq (Bp_fresh 0 t); simpl.
    intros _; apply refl_equal.
    intros [ | n] Ht.
     apply refl_equal.
     generalize (Bt_fresh_pred t 0); simpl; rewrite Ht; intros Hf';
      generalize (Hf' (lt_S _ _ (lt_O_Sn n))); clear Hf'; simpl; intros Hf'; rewrite (Hf n) in Hf';
      apply Hf'.
   case_eq (Bp_fresh (S u) t); simpl.
    intros _; apply refl_equal.
    intros n Ht; generalize (Bt_fresh_pred t (S u)); simpl; rewrite Ht; intros Hf';
     generalize (Hf' (lt_O_Sn n)); clear Hf'; simpl; intros Hf'; rewrite (Hf n) in Hf'; apply Hf'.
Qed.

(* Fresh universe --------------------------------------------------------------------------------*)

Fixpoint Bp_fshun(p:Π){struct p}:nat :=
 match p with
 | ¬p' => Bp_fshun p'
 | ∀Δ(p') => Bp_fshun p'
 | p1⋀p2 => max (Bp_fshun p1) (Bp_fshun p2)
 | (p1⇒p2) => max (Bp_fshun p1) (Bp_fshun p2)
 | ∐(_) => 0
 | e1≡e2 => max (Be_fshun e1) (Be_fshun e2)
 | e1∈e2 => max (Be_fshun e1) (Be_fshun e2)
 end
with Be_fshun(e:Ε){struct e}:nat :=
 match e with
 | Ω => 0
 | ↓(e') => Be_fshun e'
 | ↑(e') => Be_fshun e'
 | e1↦e2 => max (Be_fshun e1) (Be_fshun e2)
 | e1×e2 => max (Be_fshun e1) (Be_fshun e2)
 | ∞(_) => 0
 | ∂(¡(u',n')) => S u'
 | ⅭΔ(e',p') => max (Be_fshun e') (Bp_fshun p')
 end.

Definition Bt_fshun(t:Τ):nat := match t with be_ e' => Be_fshun e' | bp_ p' => Bp_fshun p' end.
Notation "'⋇(' t ')'":= (Bt_fshun t).
(* Provides a fresh universe *)

Theorem Bt_fshun_free:forall (t:Τ)(u n:nat), ⋇(t)<=u->(¡(u,n)∖t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (u n:nat), ⋇(t)<=u->(¡(u,n)∖t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind u n Hu;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) u n Hu));
  try (apply orb_false_intro;
       [apply (Hind _ (sdepth_l t1 t2) u n); simpl; apply (le_trans _ _ _ (le_max_l _ _ ) Hu) |
        apply (Hind _ (sdepth_r t1 t2) u n); simpl; apply (le_trans _ _ _ (le_max_r _ _ ) Hu)]).
  apply andb_false_intro1; reflect nat_eq_imp; intros Heq; rewrite Heq in Hu;
   destruct (le_Sn_n _ Hu).
  apply orb_false_intro.
   apply (Hind _ (sdepth_l t1 t2) u n); simpl; apply (le_trans _ _ _ (le_max_l _ _ ) Hu).
   apply (Hind _ (sdepth_r t1 t2) u (if eq_0 u then S n else n)); simpl;
    apply (le_trans _ _ _ (le_max_r _ _ ) Hu).
  apply (Hind _ (sdepth t) u (if eq_0 u then S n else n) Hu).
Qed.

Theorem Bt_fshun_ground:forall (t:Τ)(u:nat), ⋇(t)<=u->∖∖(u,t).
Proof.
 intros t u Hu; apply Bt_free_ground; intros n; apply (Bt_fshun_free t u n Hu).
Qed.

Theorem Bt_fshun_fresh:forall (t:Τ)(u:nat), ⋇(t)<=u->⋕(u,t)=0.
Proof.
 intros t u Hu; generalize (Bt_fshun_ground t u Hu); intros Hg; unfold Bt_ground in Hg;
  destruct ⋕(u,t) as [ | n]; [apply refl_equal | inversion Hg].
Qed.

(* Variable occurrence ---------------------------------------------------------------------------*)
(* Behind the usual freeness definition (with lifting), we also need to check occurrence of a
   variable (without lifting). This is justified by the use of De Bruijn levels instead of De Bruijn
   index: consider the term ∀Δ(∂(¡(0,0))≡E⇒∀Δ(∂(¡(0,0))≡∂(¡(0,1)))), for which a natural
   representation is ∀x.(x≡E⇒∀y.x≡y). To represent application, we need to remove the head binder
   before making substitution, that is the term becomes ∂(¡(0,0))≡E⇒∀Δ(∂(¡(0,0))≡∂(¡(0,1))) and we
   want to substitute ¡(0,0). But the natural representation of this second term is x≡E⇒∀y.y≡x,
   the second occurrence of ¡(0,0) is now captured again. This is easy to deal with, but to allow
   for interesting theorems we also need to know WHEN and WHERE ¡(0,0) appears in the term. *)

Fixpoint Bp_voccur(i:Ι)(p:Π){struct p}:Β :=
 match p with
 | ¬p' => Bp_voccur i p'
 | ∀Δ(p') => Bp_voccur i p'
 | p1⋀p2 => (Bp_voccur i p1) || (Bp_voccur i p2)
 | (p1⇒p2) => (Bp_voccur i p1) || (Bp_voccur i p2)
 | ∐(_) => ⊥
 | e1≡e2 => (Be_voccur i e1) || (Be_voccur i e2)
 | e1∈e2 => (Be_voccur i e1) || (Be_voccur i e2)
 end
with Be_voccur(i:Ι)(e:Ε){struct e}:Β :=
 match e with
 | Ω => ⊥
 | ↓(e') => Be_voccur i e'
 | ↑(e') => Be_voccur i e'
 | e1↦e2 => (Be_voccur i e1) || (Be_voccur i e2)
 | e1×e2 => (Be_voccur i e1) || (Be_voccur i e2)
 | ∞(_) => ⊥
 | ∂(i') => DB_eq i i'
 | ⅭΔ(e',p') => (Be_voccur i e') || (Bp_voccur i p')
 end.

Definition Bt_voccur(i:Ι)(t:Τ):Β :=
 match t with be_ e' => Be_voccur i e' | bp_ p' => Bp_voccur i p' end.
(* Indicates if a variable appears in a term *)

(* Propositional variable occurrence -------------------------------------------------------------*)

Fixpoint Bp_poccur(n:nat)(p:Π){struct p}:Β :=
 match p with
 | ¬p' => Bp_poccur n p'
 | ∀Δ(p') => Bp_poccur n p'
 | p1⋀p2 => (Bp_poccur n p1) || (Bp_poccur n p2)
 | (p1⇒p2) => (Bp_poccur n p1) || (Bp_poccur n p2)
 | ∐(n') => nat_eq n n'
 | e1≡e2 => (Be_poccur n e1) || (Be_poccur n e2)
 | e1∈e2 => (Be_poccur n e1) || (Be_poccur n e2)
 end
with Be_poccur(n:nat)(e:Ε){struct e}:Β :=
 match e with
 | Ω => ⊥
 | ↓(e') => Be_poccur n e'
 | ↑(e') => Be_poccur n e'
 | e1↦e2 => (Be_poccur n e1) || (Be_poccur n e2)
 | e1×e2 => (Be_poccur n e1) || (Be_poccur n e2)
 | ∞(_) => ⊥
 | ∂(_) => ⊥
 | ⅭΔ(e',p') => (Be_poccur n e') || (Bp_poccur n p')
 end.

Definition Bt_poccur(n:nat)(t:Τ):Β :=
 match t with be_ e' => Be_poccur n e' | bp_ p' => Bp_poccur n p' end.
(* Indicates if a propositional variable appears in a term *)

(* Specific form characterisation ----------------------------------------------------------------*)

Inductive is_bfor:Τ->Type := is_bfor_:forall (p:Π), is_bfor ∀Δ(p).
Notation "'?∀Δ(' p ')'":= (is_bfor p).

Inductive is_bcmp:Τ->Type := is_bcmp_:forall (e:Ε)(p:Π), is_bcmp ⅭΔ(e,p).
Notation "'?ⅭΔ(' p ')'":= (is_bcmp p).

(* Tactic for easier rewriting -------------------------------------------------------------------*)
(* It is often the case, when using equality on terms, than those equality cannot be used for
   predicates or expressions, due to the use of coercions. We provide this tactic to help in such
   cases. *)

Ltac srewrite E :=
 let H' := fresh in
 (generalize E; simpl; intros H'; rewrite H'; clear H').

Ltac srewrite_in E H :=
 let H' := fresh in
 (generalize E; simpl; intros H'; rewrite H' in H; clear H').

Ltac irewrite E :=
 let H' := fresh in
 (generalize E; simpl; intros H'; injection H'; clear H'; intros H'; rewrite H'; clear H').

Ltac irewrite_in E H :=
 let H' := fresh in
 (generalize E; simpl; intros H'; injection H'; clear H'; intros H'; rewrite H' in H; clear H').

(* Decomposition functions -----------------------------------------------------------------------*)
(* It is frequent that we have to derive from f(t) the fact that f(t'), where t' is a subterm of t,
   e.g. in induction reasoning. We propose here a function to do that automatically, completed by
   by lemmas. *)

Definition Bp_l(p:Π):Τ :=
 match p with
 | ¬t' => t'
 | t'⋀_ => t'
 | (t'⇒_) => t'
 | t'≡_ => t'
 | t'∈_ => t'
 | _ => p (* Note that we DO NOT project bounded subterms as most lemmas are not valid for them *)
 end.

Definition Be_l(e:Ε):Τ :=
 match e with
 | ↓(t') => t'
 | ↑(t') => t'
 | t'↦_ => t'
 | t'×_ => t'
 | ⅭΔ(t',_) => t'
 | _ => e
 end.

Definition Bt_l(t:Τ):Τ := match t with be_ e' => Be_l e' | bp_ p' => Bp_l p' end.

Definition Bp_r(p:Π):Τ :=
 match p with
 | ¬t' => t'
 | _⋀t' => t'
 | (_⇒t') => t'
 | _≡t' => t'
 | _∈t' => t'
 | _ => p (* Note that we DO NOT project bounded subterms as most lemmas are not valid for them *)
 end.

Definition Be_r(e:Ε):Τ :=
 match e with
 | ↓(t') => t'
 | ↑(t') => t'
 | _↦t' => t'
 | _×t' => t'
 | _ => e (* Note that we DO NOT project bounded subterms as most lemmas are not valid for them *)
 end.

Definition Bt_r(t:Τ):Τ := match t with be_ e' => Be_r e' | bp_ p' => Bp_r p' end.

Theorem Bt_free_sub:forall (t:Τ)(i:Ι), (i∖t)->((i∖Bt_l t) /\ (i∖Bt_r t)).
Proof.
 intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
         [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]] i Ht; simpl in *; try (split; apply Ht);
 try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2]; apply (conj Ht1 Ht2)).
 destruct (orb_false_elim _ _ Ht) as [Ht1 _]; apply (conj Ht1 Ht).
Qed.

Theorem Bt_ground_sub:forall (t:Τ)(u:nat), ∖∖(u,t)->(∖∖(u,Bt_l t) /\ ∖∖(u,Bt_r t)).
Proof.
 intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
         [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]] u Ht; simpl in *;
 try (split; apply Ht);
 try (generalize Ht; unfold Bt_ground; simpl;
      (destruct (Be_fresh u t1) || destruct (Bp_fresh u t1));
      (destruct (Be_fresh u t2) || destruct (Bp_fresh u t2)); simpl; intros H't; split;
      (apply refl_equal || inversion H't); fail).
 split; [ | apply Ht].
 generalize Ht; unfold Bt_ground; simpl; destruct (Be_fresh u t1) as [| n1]; simpl;
  destruct (Bp_fresh u t2) as [| n2]; simpl; intros H't.
  apply refl_equal.
  apply refl_equal.
  destruct u; inversion H't.
  destruct u; destruct n2; inversion H't.
Qed.

(*================================================================================================*)