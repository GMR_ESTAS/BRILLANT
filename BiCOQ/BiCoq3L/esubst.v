(*==================================================================================================
  Project : BiCoq3
  Module : esubst.v
  Similarly to what was done for application, we represent here substitution of a variable by an
  expression in a term; again substitutions are NOT (for now) part of the embedded language, and
  these situations are encoded by applying dedicated functions to terms.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jun 2008, Md: Sep 2008
  ================================================================================================*)

Require Export lift.

(* Substitution function -------------------------------------------------------------------------*)

Fixpoint Bp_esubst(i:Ι)(p:Π)(a:Ε){struct p}:Π:=
 match p with
 | ¬p' => ¬(Bp_esubst i p' a)
 | ∀Δ(p') => ∀Δ(Bp_esubst (ulft i) p' (Be_lift a))
 | p1⋀p2 => (Bp_esubst i p1 a)⋀(Bp_esubst i p2 a)
 | (p1⇒p2) => (Bp_esubst i p1 a)⇒(Bp_esubst i p2 a)
 | ∐(_) => p
 | e1≡e2 => (Be_esubst i e1 a)≡(Be_esubst i e2 a)
 | e1∈e2 => (Be_esubst i e1 a)∈(Be_esubst i e2 a)
 end
with Be_esubst(i:Ι)(e a:Ε){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_esubst i e' a)
 | ↑(e') => ↑(Be_esubst i e' a)
 | e1↦e2 => (Be_esubst i e1 a)↦(Be_esubst i e2 a)
 | e1×e2 => (Be_esubst i e1 a)×(Be_esubst i e2 a)
 | ∞(_) => e
 | ∂(i') => if (DB_eq i i') then a else e
 | ⅭΔ(e',p') => ⅭΔ(Be_esubst i e' a,Bp_esubst (ulft i) p' (Be_lift a))
 end.
Notation "'〈' i '←' a '@Π@' p '〉'" := (Bp_esubst i p a).
Notation "'〈' i '←' a '@Ε@' e '〉'" := (Be_esubst i e a).

Definition Bt_esubst(i:Ι)(t:Τ)(a:Ε):Τ :=
 match t with be_ e' => Be_esubst i e' a | bp_ p' => Bp_esubst i p' a end.
Notation "'〈' i '←' a '@' t '〉'" := (Bt_esubst i t a).

(* Theorems for substitution ---------------------------------------------------------------------*)

Theorem Bt_esubst_free:forall (t:Τ)(i:Ι), (i∖t)->forall (a:Ε), 〈i←a@t〉=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι), (i∖t)->forall (a:Ε), 〈i←a@t〉=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i Ht a;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) i Ht a); apply refl_equal);
  try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2];
       irewrite (Hind _ (sdepth_l t1 t2) i Ht1 a);
       irewrite (Hind _ (sdepth_r t1 t2) i Ht2 a); apply refl_equal).
  rewrite Ht; apply refl_equal.
  destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2]; irewrite (Hind _ (sdepth_l t1 t2) i Ht1 a);
   irewrite (Hind _ (sdepth_r t1 t2) (ulft i) Ht2 (Be_lift a)); apply refl_equal.
  irewrite (Hind _ (sdepth t) (ulft i) Ht (Be_lift a)); apply refl_equal.
Qed.

Theorem Bt_esubst_id:forall (t:Τ)(i:Ι), 〈i←∂(i)@t〉=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι), 〈i←∂(i)@t〉=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) i); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) i); irewrite (Hind _ (sdepth_r t1 t2) i);
       apply refl_equal).
  case_eq (DB_eq i ¡(u', n')); simpl; intros Heq; reflect_in DB_eq_imp Heq; destruct i as [u n];
   [rewrite Heq | ]; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) i); destruct i as [u n]; nat_simpl; bool_simpl;
   irewrite (Hind _ (sdepth_r t1 t2) (ulft ¡(u,n))); apply refl_equal.
  destruct i as [u n]; nat_simpl; bool_simpl; irewrite (Hind _ (sdepth t) (ulft ¡(u,n)));
   apply refl_equal.
Qed.

Theorem Bt_esubst_dbl:forall (t:Τ)(i1 i2:Ι)(e:Ε), (i1∖t)->〈i1←e@〈i2←∂(i1)@t〉〉=〈i2←e@t〉.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i1 i2:Ι)(e:Ε), (i1∖t)->〈i1←e@〈i2←∂(i1)@t〉〉=〈i2←e@t〉));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i1 i2 e Ht;
  try (apply refl_equal);
  try (irewrite (Hind _ (sdepth t) i1 i2 e Ht); apply refl_equal);
  try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2];
       irewrite (Hind _ (sdepth_l t1 t2) i1 i2 e Ht1);
       irewrite (Hind _ (sdepth_r t1 t2) i1 i2 e Ht2); apply refl_equal).
  case_eq (DB_eq i2 ¡(u',n')); simpl; intros Hi.
   destruct i1 as [[ | u1] n1]; nat_simpl; simpl; nat_simpl; bool_simpl; apply refl_equal.
   rewrite Ht; apply refl_equal.
  destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2];
   irewrite (Hind _ (sdepth_l t1 t2) i1 i2 e Ht1); induction i1 as [[| u1] n1]; nat_simpl;
   bool_simpl; simpl;
   [irewrite (Hind _ (sdepth_r t1 t2) ¡(0,S n1) (ulft i2) (Be_lift e) Ht2) |
    irewrite (Hind _ (sdepth_r t1 t2) ¡(S u1,n1) (ulft i2) (Be_lift e) Ht2)]; apply refl_equal.
  induction i1 as [[| u1] n1]; nat_simpl; bool_simpl; simpl;
   [irewrite (Hind _ (sdepth t) ¡(0,S n1) (ulft i2) (Be_lift e) Ht) |
    irewrite (Hind _ (sdepth t) ¡(S u1,n1) (ulft i2) (Be_lift e) Ht)]; apply refl_equal.
Qed.

Theorem Bt_esubst_eq:forall (t:Τ)(i:Ι)(e1 e2:Ε), (i∖e1)->〈i←e2@〈i←e1@t〉〉=〈i←e1@t〉.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(e1 e2:Ε), (i∖e1)->〈i←e2@〈i←e1@t〉〉=〈i←e1@t〉));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i e1 e2 Hie1;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) i e1 e2 Hie1); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) i e1 e2 Hie1);
       irewrite (Hind _ (sdepth_r t1 t2) i e1 e2 Hie1); apply refl_equal).
  simpl; case_eq (DB_eq i ¡(u',n')); intros Hiu'n'.
   apply (Bt_esubst_free e1 i Hie1 e2).
   simpl; rewrite Hiu'n'; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) i e1 e2 Hie1); unfold Bt_free in Hie1;
   irewrite_in (sym_equal (Bt_lift_free e1 i)) Hie1;
   irewrite (Hind _ (sdepth_r t1 t2) (ulft i) (Be_lift e1) (Be_lift e2) Hie1); apply refl_equal.
  unfold Bt_free in Hie1; irewrite_in (sym_equal (Bt_lift_free e1 i)) Hie1;
   irewrite (Hind _ (sdepth t) (ulft i) (Be_lift e1) (Be_lift e2) Hie1); apply refl_equal.
Qed.

Theorem Bt_esubst_depth:forall (t:Τ)(i1 i2:Ι), ↧(〈i1←∂(i2)@t〉)=↧(t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i1 i2:Ι), ↧(〈i1←∂(i2)@t〉)=↧(t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i1 i2;
  try (apply refl_equal); try (srewrite (Hind _ (sdepth t) i1 i2); apply refl_equal);
  try (srewrite (Hind _ (sdepth_l t1 t2) i1 i2); srewrite (Hind _ (sdepth_r t1 t2) i1 i2);
       apply refl_equal).
  destruct (DB_eq i1 ¡(u',n')); simpl; apply refl_equal.
  srewrite (Hind _ (sdepth_l t1 t2) i1 i2); srewrite (Hind _ (sdepth_r t1 t2) (ulft i1) (ulft i2));
   apply refl_equal.
  srewrite (Hind _ (sdepth t) (ulft i1) (ulft i2)); apply refl_equal.
Qed.

Theorem Bt_free_esubst:forall (t:Τ)(i i':Ι)(e:Ε), (i∖t)->(i∖e)->(i∖〈i'←e@t〉).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i i':Ι)(e:Ε), (i∖t)->(i∖e)->(i∖〈i'←e@t〉)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i i' e Hit Hie;
  try (apply refl_equal); try (srewrite (Hind _ (sdepth t) _ i' _ Hit Hie); apply refl_equal);
  try (destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2];
       srewrite (Hind _ (sdepth_l t1 t2) _ i' _ Hit1 Hie);
       srewrite (Hind _ (sdepth_r t1 t2) _ i' _ Hit2 Hie); apply refl_equal).
  case_eq (DB_eq i' ¡(u',n')); simpl; intros Hi'u'n'; [apply Hie | apply Hit].
  destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2];
   srewrite (Hind _ (sdepth_l t1 t2) _ i' _ Hit1 Hie).
  assert (H'ie:ulft i∖Be_lift e). srewrite (Bt_lift_free e i); apply Hie.
  srewrite (Hind _ (sdepth_r t1 t2) _ (ulft i') _ Hit2 H'ie); apply refl_equal.
  assert (H'ie:ulft i∖Be_lift e). srewrite (Bt_lift_free e i); apply Hie.
  srewrite (Hind _ (sdepth t) _ (ulft i') _ Hit H'ie); apply refl_equal.
Qed.

Theorem Bt_esubst_multi:forall (t:Τ)(i1 i2:Ι)(e:Ε), (i1∖e)->〈i1←e@〈i2←∂(i1)@t〉〉=〈i1←e@〈i2←e@t〉〉.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i1 i2:Ι)(e:Ε), (i1∖e)->〈i1←e@〈i2←∂(i1)@t〉〉=〈i1←e@〈i2←e@t〉〉));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i1 i2 e Hi1e;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) i1 i2 e Hi1e); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) i1 i2 e Hi1e);
       irewrite (Hind _ (sdepth_r t1 t2) i1 i2 e Hi1e); apply refl_equal).
  case_eq (DB_eq i2 ¡(u',n')); intros Hi2u'n'.
   simpl; DB_simpl; apply sym_equal; apply (Bt_esubst_free e i1 Hi1e e).
   apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) i1 i2 e Hi1e); irewrite_in (sym_equal (Bt_lift_free e i1)) Hi1e;
   irewrite (Hind _ (sdepth_r t1 t2) (ulft i1) (ulft i2) (Be_lift e) Hi1e); apply refl_equal.
  irewrite_in (sym_equal (Bt_lift_free e i1)) Hi1e;
   irewrite (Hind _ (sdepth t) (ulft i1) (ulft i2) (Be_lift e) Hi1e); apply refl_equal.
Qed.

Theorem Bt_esubst_comm:forall (t:Τ)(i1 i2:Ι)(e1 e2:Ε), (i1∖e2)->(i2∖e1)->(i1<>i2 \/ e1=e2)->
                       〈i1←e1@〈i2←e2@t〉〉=〈i2←e2@〈i1←e1@t〉〉.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i1 i2:Ι)(e1 e2:Ε), (i1∖e2)->(i2∖e1)->(i1<>i2 \/ e1=e2)->
                            〈i1←e1@〈i2←e2@t〉〉=〈i2←e2@〈i1←e1@t〉〉));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl;
  intros Hind i1 i2 e1 e2 Hi1e2 Hi2e1 Hcmp; try (apply refl_equal);
  try (irewrite (Hind _ (sdepth t) i1 i2 e1 e2 Hi1e2 Hi2e1 Hcmp); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) i1 i2 e1 e2 Hi1e2 Hi2e1 Hcmp);
       irewrite (Hind _ (sdepth_r t1 t2) i1 i2 e1 e2 Hi1e2 Hi2e1 Hcmp); apply refl_equal).
  case_eq (DB_eq i2 ¡(u',n')); intros Hi2i'.
   irewrite (Bt_esubst_free e2 i1 Hi1e2 e1); case_eq (DB_eq i1 ¡(u',n')); intros Hi1i'.
    irewrite (Bt_esubst_free e1 i2 Hi2e1 e2); destruct Hcmp as [Hcmp | Hcmp].
     reflect_in DB_eq_imp Hi1i'; rewrite Hi1i' in Hcmp; reflect_in DB_eq_imp Hi2i';
      rewrite Hi2i' in Hcmp; destruct Hcmp; apply refl_equal.
     rewrite Hcmp; apply refl_equal.
    simpl; rewrite Hi2i'; apply refl_equal.
   case_eq (DB_eq i1 ¡(u',n')); intros Hi1i'.
    simpl; rewrite Hi1i'; irewrite (Bt_esubst_free e1 i2 Hi2e1 e2); apply refl_equal.
    simpl; rewrite Hi1i'; rewrite Hi2i'; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) _ _ _ _ Hi1e2 Hi2e1 Hcmp);
   irewrite_in (sym_equal (Bt_lift_free e1 i2)) Hi2e1;
   irewrite_in (sym_equal (Bt_lift_free e2 i1)) Hi1e2.
  assert (H'cmp:ulft i1<>ulft i2 \/ Be_lift e1=Be_lift e2).
   destruct Hcmp as [Hcmp | Hcmp]; [left | right].
    intros Heq; apply Hcmp; destruct i1 as [[| u1] n1]; destruct i2 as [[| u2] n2]; simpl in Heq;
     inversion Heq; apply refl_equal.
    rewrite Hcmp; apply refl_equal.
   irewrite (Hind _ (sdepth_r t1 t2) _ _ _ _ Hi1e2 Hi2e1 H'cmp); apply refl_equal.
  irewrite_in (sym_equal (Bt_lift_free e1 i2)) Hi2e1;
   irewrite_in (sym_equal (Bt_lift_free e2 i1)) Hi1e2.
  assert (H'cmp:ulft i1<>ulft i2 \/ Be_lift e1=Be_lift e2).
   destruct Hcmp as [Hcmp | Hcmp]; [left | right].
    intros Heq; apply Hcmp; destruct i1 as [[| u1] n1]; destruct i2 as [[| u2] n2]; simpl in Heq;
     inversion Heq; apply refl_equal.
    rewrite Hcmp; apply refl_equal.
   irewrite (Hind _ (sdepth t) _ _ _ _ Hi1e2 Hi2e1 H'cmp); apply refl_equal.
Qed.

Theorem Bt_esubst_repl:forall (t:Τ)(i:Ι)(e:Ε), (i∖e)->(i∖〈i←e@t〉).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(e:Ε), (i∖e)->(i∖〈i←e@t〉)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i e Hie;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) i e Hie); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) i e Hie); irewrite (Hind _ (sdepth_r t1 t2) i e Hie);
       apply refl_equal).
  case_eq (DB_eq i ¡(u',n')); intros Hiu'n'; [apply Hie | apply Hiu'n'].
  irewrite (Hind _ (sdepth_l t1 t2) _ _ Hie); irewrite_in (sym_equal (Bt_lift_free e i)) Hie;
   irewrite (Hind _ (sdepth_r t1 t2) _ _ Hie); apply refl_equal.
 irewrite_in (sym_equal (Bt_lift_free e i)) Hie; irewrite (Hind _ (sdepth t) _ _ Hie);
  apply refl_equal.
Qed.

(*================================================================================================*)