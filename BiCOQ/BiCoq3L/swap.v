(*==================================================================================================
  Project : BiCoq3
  Module : swap.v
  In this module we study the relationships between Coq logic and B logic; behind the theoretical
  interest, this also allows to use the deep embedding to define a form of shallow embedding, used as
  a form of tactics : to prove results in B, it is sufficient to project the goal in Coq, then to
  prove it in Coq using all the tools available.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Oct 2008
  ================================================================================================*)

Require Export bbprop.
Require Export cmp.

(* Propositional calculus ------------------------------------------------------------------------*)

Theorem bc_and:forall (G:Γ)(p1 p2:Π), G⊢p1⋀p2->(G⊢p1)*(G⊢p2).
Proof.
 intros G p1 p2 Hand; split; [apply (Bpr_andl _ _ _ Hand) | apply (Bpr_andr _ _ _ Hand)].
Qed.

Theorem cb_and:forall (G:Γ)(p1 p2:Π), (G⊢p1)*(G⊢p2)->G⊢p1⋀p2.
Proof.
 intros G p1 p2 [Hp1 Hp2]; apply Bpr_andi; [apply Hp1 | apply Hp2].
Qed.

Theorem bc_imp:forall (G:Γ)(p1 p2:Π), G⊢p1⇒p2->G⊢p1->G⊢p2.
Proof.
 intros G p1 p2 Himp Hp1; apply Bpr_modp with (p1:=p1); [apply Hp1 | apply Himp].
Qed.

Theorem cb_lor:forall (G:Γ)(p1 p2:Π), (G⊢p1)+(G⊢p2)->G⊢p1⋁p2.
Proof.
 intros G p1 p2 [Hp1 | Hp2]; [apply (Bpr_lorl _ _ p2 Hp1) | apply (Bpr_lorr _ p1 _ Hp2)].
Qed.

(* Nota: the following ones are not provable, as justified by the differences between the B logic
   admitting excluded middle and the constructive Coq logic...
   - bc_not:forall (G:Γ)(p:Π), (G⊢¬p1)->notT (G⊢p1) => G can be inconsistent
   - cb_not:forall (G:Γ)(p:Π), notT (G⊢p1)->G⊢¬p1 => That would mean forall (G:Γ)(p:Π), (G⊢p)+(G⊢¬p)
   - bc_lor:forall (G:Γ)(p1 p2:Π), G⊢p1⋁p2->(G⊢p1)+(G⊢p2) => Consider p2=¬p1
   - cb_imp:forall (G:Γ)(p1 p2:Π), (G⊢p1->G⊢p2)->G⊢p1⇒p2 *)

(* Predicate calcululs ---------------------------------------------------------------------------*)

Theorem bc_for:forall (G:Γ)(p:Π)(i:Ι), G⊢∀(i∙p)->(forall (e:Ε), G⊢〈i←e@Π@p〉).
Proof.
 intros G p i Hfor e; apply Bpr_fore; apply Hfor.
Qed.

Theorem bc_for':forall (G:Γ)(p:Π)(i:Ι), G⊢∀(i∙p)->(forall (i':Ι), G⊢〈i←∂(i')@Π@p〉).
Proof.
 intros G p i Hfor i'; apply (bc_for G p i Hfor ∂(i')).
Qed.

Theorem cb_for:forall (G:Γ)(p:Π)(i:Ι), (forall (i':Ι), G⊢〈i←∂(i')@Π@p〉)->G⊢∀(i∙p).
Proof.
 intros G p i Hfor; destruct (gfshun (G·p)) as [u Hu]; generalize (Bg_ground_free _ _ Hu); clear Hu;
  intros Hu.
 assert (Hp:¡(u,0)∖p). apply Bg_free_mem with (G:=G·p); [apply Hu | apply Bg_add_mem_new].
 assert (HG:¡(u,0)∖ˠG). apply Bg_free_inc with (G1:=G·p); [apply Hu | apply Bg_inc_add].
 rewrite (Bp_forall_alpha _ _ i Hp); apply Bpr_fori; [apply Hfor | apply HG].
Qed.

Theorem cb_for':forall (G:Γ)(p:Π)(i:Ι), (forall (e:Ε), G⊢〈i←e@Π@p〉)->G⊢∀(i∙p).
Proof.
 intros G p i Hfor; apply cb_for; intros i'; apply Hfor.
Qed.

Theorem cb_exs:forall (G:Γ)(p:Π)(i:Ι), sigS (fun e:Ε=>G⊢〈i←e@Π@p〉)->G⊢∃(i∙p).
Proof.
 intros G p i [e He]; apply Bpr_absn with (p2:=〈i←e@Π@p〉).
  apply Bpr_clear; apply He.
  apply (Bpr_fore (G·∀(i∙¬p)) (¬p) e i); apply Bpr_axom.
Qed.

Theorem cb_exs':forall (G:Γ)(p:Π)(i:Ι), sigS (fun i':Ι=>G⊢〈i←∂(i')@Π@p〉)->G⊢∃(i∙p).
Proof.
 intros G p i [i' Hi']; apply cb_exs; exists ∂(i'); apply Hi'.
Qed.
(* Nota: this theorem is normally written {i':Ι & G⊢〈i←∂(i')|0@Π@p〉}->G⊢∃(i∙p), that is in standard
   logic notation (∃ i':I, G⊢〈i←∂(i')|0@Π@p〉)->G⊢∃(i∙p) *)

Theorem cb_equ:forall (G:Γ)(e1 e2:Ε), e1=e2->G⊢e1≡e2.
Proof.
 intros G e1 e2 Heq; rewrite Heq; apply Bpr_refl.
Qed.

(* Nota: the following ones are not provable, as justified by the differences between the B logic
   admitting excluded middle and extensionality, while the Coq logic is constructive
   - bc_exs:forall (G:Γ)(p:Π)(i:Ι), G⊢∃(i∙p)->∃ i':I, G⊢〈i←∂(i')|0@Π@p〉
   - bc_equ:forall (G:Γ)(e1 e2:Ε), G⊢e1≡e2->e1=e2 *)

(*================================================================================================*)