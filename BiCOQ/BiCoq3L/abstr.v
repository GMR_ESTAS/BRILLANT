(*==================================================================================================
  Project : BiCoq3
  Module : abstr.v
  Beta-abstraction, capturing a free variable in a term.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jun 2008, Md: Sep 2008
  ================================================================================================*)

Require Export lift.

(* Abstraction function --------------------------------------------------------------------------*)
(* The abstraction function prepares a term by lifting variables and replacing one of them by a 0 to
   be captured by a binder. *)

Fixpoint Bp_abstr(i:Ι)(p:Π){struct p}:Π:=
 match p with
 | ¬p' => ¬(Bp_abstr i p')
 | ∀Δ(p') => ∀Δ(Bp_abstr (ulft i) p')
 | p1⋀p2 => (Bp_abstr i p1)⋀(Bp_abstr i p2)
 | (p1⇒p2) => (Bp_abstr i p1)⇒(Bp_abstr i p2)
 | ∐(_) => p
 | e1≡e2 => (Be_abstr i e1)≡(Be_abstr i e2)
 | e1∈e2 => (Be_abstr i e1)∈(Be_abstr i e2)
 end
with Be_abstr(i:Ι)(e:Ε){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_abstr i e')
 | ↑(e') => ↑(Be_abstr i e')
 | e1↦e2 => (Be_abstr i e1)↦(Be_abstr i e2)
 | e1×e2 => (Be_abstr i e1)×(Be_abstr i e2)
 | ∞(n') => e
 | ∂(i') => ∂(if DB_eq i i' then ¡(0,0) else ulft i')
 | ⅭΔ(e',p') => ⅭΔ(Be_abstr i e',Bp_abstr (ulft i) p')
 end.

Definition Bt_abstr(i:Ι)(t:Τ):Τ := match t with be_ e => Be_abstr i e | bp_ p => Bp_abstr i p end.

Definition Bp_forall(i:Ι)(p:Π):Π := ∀Δ(Bp_abstr i p).
Notation "'∀(' v '∙' p ')'" := (Bp_forall v p).
Notation "'∃(' v '∙' p ')'" := (¬∀(v∙¬p)).

Definition Be_cmpset(i:Ι)(e:Ε)(p:Π):Ε := ⅭΔ(e,Bp_abstr i p).
Notation "'{' v '∊' e '∣' p '}'" := (Be_cmpset v e p).

(* Theorems --------------------------------------------------------------------------------------*)

Theorem Bt_abstr_lift:forall (t:Τ)(i:Ι), (i∖t)->Bt_abstr i t=⇑(t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι), (i∖t)->Bt_abstr i t=⇑(t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i Hi;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) i Hi); apply refl_equal);
  try (destruct (orb_false_elim _ _ Hi) as [Hi1 Hi2]; irewrite (Hind _ (sdepth_l t1 t2) i Hi1);
       irewrite (Hind _ (sdepth_r t1 t2) i Hi2); apply refl_equal).
  rewrite Hi; apply refl_equal.
  destruct (orb_false_elim _ _ Hi) as [Hi1 Hi2]; irewrite (Hind _ (sdepth_l t1 t2) i Hi1);
   irewrite (Hind _ (sdepth_r t1 t2) (ulft i) Hi2); apply refl_equal.
  irewrite (Hind _ (sdepth t) (ulft i) Hi); apply refl_equal.
Qed.

Theorem Bt_abstr_free:forall (t:Τ)(i:Ι), (ulft i∖Bt_abstr i t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι), (ulft i∖Bt_abstr i t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) i));
  try (srewrite (Hind _ (sdepth_l t1 t2) i); srewrite (Hind _ (sdepth_r t1 t2) i);
       apply refl_equal).
  case_eq (DB_eq i ¡(u',n')); simpl; intros Hi.
   destruct i as [[| u] n]; simpl; nat_simpl; apply refl_equal.
   destruct u' as [| u']; simpl; reflect DB_eq_imp; reflect_in DB_eq_imp Hi; intros H'i; apply Hi;
    destruct i as [[| u] n]; simpl in H'i; inversion H'i; apply refl_equal.
  srewrite (Hind _ (sdepth_l t1 t2) i); srewrite (Hind _ (sdepth_r t1 t2) (ulft i));
       apply refl_equal.
  apply (Hind _ (sdepth t) (ulft i)).
Qed.

Theorem Bp_forall_free:forall (i:Ι)(p:Π), (i∖∀(i∙p)).
Proof.
 intros i p; simpl; apply (Bt_abstr_free p i).
Qed.

Theorem Be_cmpset_free:forall (i:Ι)(e:Ε)(p:Π), (i∖e)->(i∖{i∊e∣p}).
Proof.
 intros i e p; simpl; intros Hie; rewrite Hie; simpl; apply (Bt_abstr_free p i).
Qed.

Theorem Bt_abstr_free_free:forall (t:Τ)(i i':Ι), (i∖t)->(ulft i∖(Bt_abstr i' t)).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i i':Ι), (i∖t)->(ulft i∖(Bt_abstr i' t))));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i i' Hit;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) i i' Hit));
  try (destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2];
       srewrite (Hind _ (sdepth_l t1 t2) i i' Hit1); apply (Hind _ (sdepth_r t1 t2) i i' Hit2)).
  case_eq (DB_eq i' ¡(u',n')); simpl; intros Hi'.
   destruct i as [[| u] n]; simpl; nat_simpl; apply refl_equal.
   destruct u' as [| u']; simpl; destruct i as [[| u] n]; simpl; nat_simpl; try (apply refl_equal);
    inversion Hit; nat_simpl; apply refl_equal.
  destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2]; srewrite (Hind _ (sdepth_l t1 t2) i i' Hit1);
   apply (Hind _ (sdepth_r t1 t2) (ulft i) (ulft i') Hit2).
  apply (Hind _ (sdepth t) (ulft i) (ulft i') Hit).
Qed.

Theorem Bp_forall_free_free:forall (i i':Ι)(p:Π), (i'∖p)->(i'∖∀(i∙p)).
Proof.
 intros i i' p Hi'p; simpl; apply (Bt_abstr_free_free p i' i Hi'p).
Qed.

Theorem Be_cmpset_free_free:forall (i i':Ι)(e:Ε)(p:Π), (i'∖e)->(i'∖p)->(i'∖{i∊e∣p}).
Proof.
 intros i i' e p Hi'e Hi'p; simpl; irewrite Hi'e; simpl; apply (Bt_abstr_free_free p i' i Hi'p).
Qed.

Theorem Bt_abstr_free_eq:forall (t:Τ)(i i':Ι),
                         i<>i'->Bt_free (ulft i) (Bt_abstr i' t)=Bt_free i t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i i':Ι),
                            i<>i'->Bt_free (ulft i) (Bt_abstr i' t)=Bt_free i t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i i' Hii';
  try (apply refl_equal); try (apply (Hind _ (sdepth t) _ _ Hii'));
  try (srewrite (Hind _ (sdepth_l t1 t2) _ _ Hii'); srewrite (Hind _ (sdepth_r t1 t2) _ _ Hii');
       apply refl_equal).
  case_eq (DB_eq i' ¡(u',n')); simpl; intros Hi'u'n'.
   destruct i as [[| u] n]; simpl; nat_simpl; bool_simpl; destruct u' as [| u']; simpl.
    case_eq (nat_eq n n'); simpl; intros Hnn'.
     reflect_in nat_eq_imp Hnn'; rewrite Hnn' in *; clear n Hnn'; reflect_in DB_eq_imp Hi'u'n';
      rewrite Hi'u'n' in Hii'; destruct Hii'; apply refl_equal.
     apply refl_equal.
    apply refl_equal.
    nat_simpl; apply refl_equal.
    nat_simpl; case_eq (nat_eq u u'); simpl; intros Huu'; [ | apply refl_equal];
     case_eq (nat_eq n n'); simpl; intros Hnn'; [ | apply refl_equal].
     reflect_in nat_eq_imp Huu'; rewrite Huu' in *; reflect_in nat_eq_imp Hnn'; rewrite Hnn' in *;
      reflect_in DB_eq_imp Hi'u'n'; rewrite Hi'u'n' in Hii'; destruct Hii'; apply refl_equal.
   destruct u' as [| u']; simpl; destruct i as [[| u] n]; simpl; nat_simpl; bool_simpl;
    apply refl_equal.
  srewrite (Hind _ (sdepth_l t1 t2) _ _ Hii'); reflect_in DB_eq_imp Hii';
   rewrite <- (DB_eq_ulft i i') in Hii'; reflect_in DB_eq_imp Hii';
   srewrite (Hind _ (sdepth_r t1 t2) _ _ Hii'); apply refl_equal.
  reflect_in DB_eq_imp Hii';
   rewrite <- (DB_eq_ulft i i') in Hii'; reflect_in DB_eq_imp Hii';
   srewrite (Hind _ (sdepth t) _ _ Hii'); apply refl_equal.
Qed.

Theorem Bt_abstr_depth:forall (t:Τ)(i:Ι), ↧(Bt_abstr i t)=↧(t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι), ↧(Bt_abstr i t)=↧(t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i;
  try (apply refl_equal); try (srewrite (Hind _ (sdepth t) i); apply refl_equal);
  try (srewrite (Hind _ (sdepth_l t1 t2) i); srewrite (Hind _ (sdepth_r t1 t2) i);
       apply refl_equal).
  srewrite (Hind _ (sdepth_l t1 t2) i); srewrite (Hind _ (sdepth_r t1 t2) (ulft i));
   apply refl_equal.
  srewrite (Hind _ (sdepth t) (ulft i)); apply refl_equal.
Qed.

(* Characterisation ------------------------------------------------------------------------------*)

Theorem is_forall:forall (i:Ι)(p:Π), ?∀Δ(∀(i∙p)).
Proof.
 intros i p; unfold Bp_forall; apply is_bfor_.
Qed.
Notation "'!∀(' i '∙' p ')'":= (is_forall i p).

Theorem is_cmpset:forall (i:Ι)(e:Ε)(p:Π), ?ⅭΔ({i∊e∣p}).
 intros i e p; unfold Be_cmpset; apply is_bcmp_.
Qed.
Notation "'!{' i '∊' e '∣' p ')'":= (is_cmpset i e p).

(*================================================================================================*)
