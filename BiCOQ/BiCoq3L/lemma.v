(*==================================================================================================
  Project : BiCoq3
  Module : lemma.v
  This module is about some trivial results about B propositional calculus.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Jul 2008
  ================================================================================================*)

Require Export proof.
Require Export cmp.

(* Derived rules ---------------------------------------------------------------------------------*)

Theorem Bpr_axom:forall (G:Γ)(p:Π), G·p⊢p.
Proof.
 intros G p; apply (Bpr_memb (G·p) p); Btac_gamma.
Qed.

Theorem Bpr_cutr:forall (G:Γ)(p1 p2:Π), G⊢p1->G·p1⊢p2->G⊢p2.
Proof.
 intros G p1 p2 Hp1 Hp2; apply Bpr_absp with (p2:=p1).
  apply Bpr_weak with (G1:=G); [apply Hp1 | apply Bg_inc_add].
  apply Bpr_absn with (p2:=p2).
   rewrite Bg_add_comm; apply Bpr_weak with (G1:=G·p1); [apply Hp2 | apply Bg_inc_add].
   apply Bpr_weak with (G1:=G·¬p2); [apply Bpr_axom | apply Bg_inc_add].
Qed.

Theorem Bpr_modp:forall (G:Γ)(p1 p2:Π), G⊢p1->G⊢p1⇒p2->G⊢p2.
Proof.
 intros G p1 p2 Hp1 Himp; apply Bpr_cutr with (p1:=p1); [apply Hp1 | apply Bpr_impe; apply Himp].
Qed.

(* Elementary lemmas -----------------------------------------------------------------------------*)

Theorem Bpr_emid:forall (G:Γ)(p:Π), G⊢p⋁¬p.
Proof.
 intros G p; unfold blor; apply Bpr_impi; apply Bpr_axom.
Qed.

Theorem Bpr_clear:forall (G:Γ)(p1 p2:Π), G⊢p1->G·p2⊢p1.
Proof.
 intros G p1 p2 Hp1; apply Bpr_weak with (G1:=G); [apply Hp1 | apply Bg_inc_add].
Qed.

Theorem Bpr_fngh:forall (G:Γ)(p1 p2:Π), G⊢p1->G·¬p1⊢p2.
Proof.
 intros G p1 p2 Hp1; apply Bpr_absp with (p2:=p1); apply Bpr_clear;
  [apply Bpr_clear; apply Hp1 | apply Bpr_axom].
Qed.

Theorem Bpr_fpsh:forall (G:Γ)(p1 p2:Π), G⊢¬p1->G·p1⊢p2.
Proof.
 intros G p1 p2 Hp1; apply Bpr_absp with (p2:=p1); apply Bpr_clear;
  [apply Bpr_axom | apply Bpr_clear; apply Hp1].
Qed.

Theorem Bpr_lorl:forall (G:Γ)(p1 p2:Π), G⊢p1->G⊢p1⋁p2.
Proof.
 intros G p1 p2 Hp1; unfold blor; apply Bpr_impi; apply Bpr_fngh; apply Hp1.
Qed.

Theorem Bpr_lorr:forall (G:Γ)(p1 p2:Π), G⊢p2->G⊢p1⋁p2.
Proof.
 intros G p1 p2 Hp1; unfold blor; apply Bpr_impi; apply Bpr_clear; apply Hp1.
Qed.
(* Nota: There is no Bpr_lore:G⊢p1⋁p2->(G⊢p1 \/ G⊢p2), see module swap for details. *)

Theorem Bpr_not2:forall (G:Γ)(p:Π), G⊢p->G⊢¬¬p.
Proof.
 intros G p Hp; apply Bpr_absn with (p2:=p); [apply Bpr_clear; apply Hp | apply Bpr_axom].
Qed.

Theorem Bpr_ctnn:forall (G:Γ)(p1 p2:Π), G⊢p1⇒p2->G⊢¬p2⇒¬p1.
Proof.
 intros H p1 p2 Himp; apply Bpr_impi; apply Bpr_absn with (p2:=p2).
  rewrite Bg_add_comm; apply Bpr_clear; apply Bpr_impe; apply Himp.
  apply Bpr_clear; apply Bpr_axom.
Qed.

Theorem Bpr_ctnp:forall (G:Γ)(p1 p2:Π), G⊢¬p1⇒p2->G⊢¬p2⇒p1.
Proof.
 intros G p1 p2 Himp; apply Bpr_impi; apply Bpr_absp with (p2:=p2).
  apply Bpr_impe; apply Bpr_clear; apply Himp.
  apply Bpr_clear; apply Bpr_axom.
Qed.

Theorem Bpr_ctpn:forall (G:Γ)(p1 p2:Π), G⊢p1⇒¬p2->G⊢p2⇒¬p1.
Proof.
 intros G p1 p2 Himp; apply Bpr_impi; apply Bpr_absn with (p2:=p2).
  apply Bpr_clear; apply Bpr_axom.
  apply Bpr_impe; apply Bpr_clear; apply Himp.
Qed.

Theorem Bpr_ctpp:forall (G:Γ)(p1 p2:Π), G⊢¬p1⇒¬p2->G⊢p2⇒p1.
Proof.
 intros G p1 p2 Himp; apply Bpr_impi; apply Bpr_absp with (p2:=p2).
  apply Bpr_clear; apply Bpr_axom.
  apply Bpr_impe; apply Bpr_clear; apply Himp.
Qed.

Theorem Bpr_ands:forall (G:Γ)(p1 p2 p3:Π), G·p1·p2⊢p3->G·p1⋀p2⊢p3.
Proof.
 intros G p1 p2 p3 Hsp; apply Bpr_cutr with (p1:=p1).
  apply Bpr_andl with (p2:=p2); apply Bpr_axom.
  rewrite Bg_add_comm; apply Bpr_cutr with (p1:=p2).
   apply Bpr_andr with (p1:=p1); apply Bpr_axom.
   rewrite Bg_add_comm; apply Bpr_clear; apply Hsp.
Qed.

Theorem Bpr_nulg:forall (G:Γ)(p:Π), ⊢p->G⊢p.
Proof.
 intros G p Hp; apply Bpr_weak with (G1:=∅ˠ); [apply Hp | apply Bg_inc_empty].
Qed.

Theorem Bpr_exsi:forall (G:Γ)(p:Π)(i:Ι)(e:Ε), G⊢〈i←e@Π@p〉->G⊢∃(i∙p).
Proof.
 intros G p i e He.
 apply Bpr_absn with (p2:=〈i←e@Π@p〉).
  apply Bpr_clear; apply He.
  apply (Bpr_fore (G·∀(i∙¬p)) (¬p) e i); apply Bpr_axom.
Qed.

Theorem Bpr_fore_id:forall (G:Γ)(p:Π)(i:Ι), G⊢∀(i∙p)->G⊢p.
Proof.
 intros G p i Hfor; replace p with 〈i←∂(i)@Π@p〉;
  [apply Bpr_fore; apply Hfor | irewrite (Bt_esubst_id p i); apply refl_equal].
Qed.

Theorem Bpr_fori_diff:forall (G:Γ)(p:Π)(i i':Ι), (i'∖ˠG)->(i'∖p)->G⊢〈i←∂(i')@Π@p〉->G⊢∀(i∙p).
Proof.
 intros G p i i' Hi'G Hi'p Hinst;  rewrite (Bp_forall_alpha p i' i Hi'p);
  apply (Bpr_fori _ _ _ Hinst Hi'G).
Qed.

Theorem Bpr_sbsi:forall (G:Γ)(p:Π)(e:Ε)(i:Ι), G⊢p->(i∖ˠG)->G⊢〈i←e@Π@p〉.
Proof.
 intros G p e i HGp HiG; apply Bpr_fore; apply (Bpr_fori _ _ _ HGp HiG).
Qed.

Theorem Bpr_equ_sym:forall (G:Γ)(e1 e2:Ε), G⊢e1≡e2->G⊢e2≡e1.
Proof.
 intros G e1 e2 Heq; destruct (fresh (e1≡e2) 0) as [n Hn];
  replace (e2≡e1) with 〈¡(0,n)←e2@Π@∂(¡(0,n))≡e1〉.
  apply (Bpr_leib _ (∂(¡(0,n))≡e1) _ _ ¡(0,n) Heq); simpl; nat_simpl; simpl;
   irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hn)) e1); apply Bpr_refl.
  simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hn)) e2);
   apply refl_equal.
Qed.

Theorem Bpr_equ_trans:forall (G:Γ)(e1 e2 e3:Ε), G⊢e1≡e2->G⊢e2≡e3->G⊢e1≡e3.
Proof.
 intros G e1 e2 e3 He1e2 He2e3; destruct (fresh e3 1) as [n Hne3]; set (i:=¡(1,n)); fold i in Hne3;
  replace (e1≡e3) with 〈i←e1@Π@∂(i)≡e3〉.
  apply (Bpr_leib _ (∂(i)≡e3) _ _ i (Bpr_equ_sym _ _ _ He1e2)); simpl; nat_simpl; simpl;
   irewrite (Bt_esubst_free _ _ Hne3 e2); apply He2e3.
  simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ Hne3 e1); apply refl_equal.
Qed.

(* Associated tactics ----------------------------------------------------------------------------*)

Ltac Btac_hyp := (apply Bpr_memb; Btac_gamma).

(*================================================================================================*)