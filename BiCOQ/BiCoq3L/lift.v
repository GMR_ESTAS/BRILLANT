(*==================================================================================================
  Project : BiCoq3
  Module : lift.v
  Uplift all De Bruijn levels (of the namespace 0) in a term. Note that when using De Bruijn
  indexes, only dangling indexes are lifted (an operation called when crossing a binder to preserve
  the semantic) ; this requires the use of a local variable to record the current depth for
  recursive calls.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jun 2008, Md: Sep 2008
  ================================================================================================*)

Require Export term.

(* Uplift ----------------------------------------------------------------------------------------*)

Fixpoint Bp_lift(p:Π){struct p}:Π:=
 match p with
 | ¬p' => ¬(Bp_lift p')
 | ∀Δ(p') => ∀Δ(Bp_lift p')
 | p1⋀p2 => (Bp_lift p1)⋀(Bp_lift p2)
 | (p1⇒p2) => bimp (Bp_lift p1) (Bp_lift p2)
 | ∐(_) => p
 | e1≡e2 => (Be_lift e1)≡(Be_lift e2)
 | e1∈e2 => (Be_lift e1)∈(Be_lift e2)
 end
with Be_lift(e:Ε){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_lift e')
 | ↑(e') => ↑(Be_lift e')
 | e1↦e2 => (Be_lift e1)↦(Be_lift e2)
 | e1×e2 => (Be_lift e1)×(Be_lift e2)
 | ∞(n') => e
 | ∂(i') => ∂(ulft i')
 | ⅭΔ(e',p') => ⅭΔ(Be_lift e',Bp_lift p')
 end.

Definition Bt_lift(t:Τ):Τ := match t with be_ e => Be_lift e | bp_ p => Bp_lift p end.
Notation "'⇑(' t ')'":=(Bt_lift t).

(* Theorems --------------------------------------------------------------------------------------*)

Lemma Bt_lift_free:forall (t:Τ)(i:Ι), Bt_free (ulft i) ⇑(t)=Bt_free i t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι), Bt_free (ulft i) ⇑(t)=Bt_free i t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) i));
  try (srewrite (Hind _ (sdepth_l t1 t2) i); srewrite (Hind _ (sdepth_r t1 t2) i);
       apply refl_equal).
  destruct u'; simpl; destruct i as [[| u] n]; simpl; nat_simpl; bool_simpl; apply refl_equal.
  srewrite (Hind _ (sdepth_l t1 t2) i); srewrite (Hind _ (sdepth_r t1 t2) (ulft i));
   apply refl_equal.
  apply (Hind _ (sdepth t) (ulft i)).
Qed.

Theorem Bt_lift_fresh_S:forall (t:Τ)(u:nat), ⋕(S u,⇑(t))=⋕(S u,t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (u:nat), ⋕(S u,⇑(t))=⋕(S u,t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind u;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) u));
  try (srewrite (Hind _ (sdepth_l t1 t2) u); srewrite (Hind _ (sdepth_r t1 t2) u);
       apply refl_equal).
  destruct u' as [| u']; nat_simpl; bool_simpl; apply refl_equal.
Qed.

(*================================================================================================*)