(*==================================================================================================
  Project : BiCoq3
  Module : proof.v
  This module defines B proofs as a dependent type ; that is, H⊢p, where H is a list of predicates
  (the hypothesis) and p a predicate (the goal) defines a type, inhabited by its proofs.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Jul 2008
  ================================================================================================*)

Require Export proof_env.
Require Export abstr.
Require Export esubst.

(* Proof environment -----------------------------------------------------------------------------*)

Definition Bseq:Set := (prod Γ Π).
Notation "G '⊦' p" := (pair G p) (no associativity, at level 89).

(* Proof -----------------------------------------------------------------------------------------*)

Inductive Bproof:Bseq->Set :=
 | Bpr_memb:forall (G:Γ)(p:Π), p∈ˠG=⊤->Bproof (G⊦p)
 | Bpr_weak:forall (G1 G2:Γ)(p:Π), Bproof (G1⊦p)->G1⊆ˠG2=⊤->Bproof (G2⊦p)
 | Bpr_andi:forall (G:Γ)(p1 p2:Π), Bproof (G⊦p1)->Bproof (G⊦p2)->Bproof (G⊦p1⋀p2)
 | Bpr_andl:forall (G:Γ)(p1 p2:Π), Bproof (G⊦p1⋀p2)->Bproof (G⊦p1)
 | Bpr_andr:forall (G:Γ)(p1 p2:Π), Bproof (G⊦p1⋀p2)->Bproof (G⊦p2)
 | Bpr_impi:forall (G:Γ)(p1 p2:Π), Bproof (G·p1⊦p2)->Bproof (G⊦p1⇒p2)
 | Bpr_impe:forall (G:Γ)(p1 p2:Π), Bproof (G⊦p1⇒p2)->Bproof (G·p1⊦p2)
 | Bpr_absn:forall (G:Γ)(p1 p2:Π), Bproof (G·p1⊦p2)->Bproof (G·p1⊦¬p2)->Bproof (G⊦¬p1)
 | Bpr_absp:forall (G:Γ)(p1 p2:Π), Bproof (G·¬p1⊦p2)->Bproof (G·¬p1⊦¬p2)->Bproof (G⊦p1)
 | Bpr_refl:forall (G:Γ)(e:Ε), Bproof (G⊦e≡e)
 | Bpr_fori:forall (G:Γ)(p:Π)(i:Ι), Bproof (G⊦p)->(i∖ˠG)->Bproof (G⊦∀(i∙p))
 | Bpr_fore:forall (G:Γ)(p:Π)(e:Ε)(i:Ι), Bproof (G⊦∀(i∙p))->Bproof (G⊦〈i←e@Π@p〉)
 | Bpr_cmpi:forall (G:Γ)(p:Π)(e1 e2:Ε)(i:Ι),
            Bproof (G⊦e1∈e2)->Bproof (G⊦〈i←e1@Π@p〉)->Bproof (G⊦e1∈{i∊e2∣p})
 | Bpr_cmpl:forall (G:Γ)(p:Π)(e1 e2:Ε)(i:Ι), Bproof (G⊦e1∈{i∊e2∣p})->Bproof (G⊦e1∈e2)
 | Bpr_cmpr:forall (G:Γ)(p:Π)(e1 e2:Ε)(i:Ι), Bproof (G⊦e1∈{i∊e2∣p})->Bproof (G⊦〈i←e1@Π@p〉)
 | Bpr_leib:forall (G:Γ)(p:Π)(e1 e2:Ε)(i:Ι),
            Bproof (G⊦e1≡e2)->Bproof (G⊦〈i←e1@Π@p〉)->Bproof (G⊦〈i←e2@Π@p〉)
 | Bpr_chos:forall (G:Γ)(e:Ε)(i:Ι), (i∖e)->Bproof (G⊦∃(i∙∂(i)∈e))->Bproof (G⊦↓(e)∈e)
 | Bpr_powi:forall (G:Γ)(e1 e2:Ε)(i:Ι),
            (i∖e1)->(i∖e2)->Bproof (G⊦∀(i∙∂(i)∈e1⇒∂(i)∈e2))->Bproof (G⊦e1∈↑(e2))
 | Bpr_powe:forall (G:Γ)(e1 e2:Ε)(i:Ι),
            (i∖e1)->(i∖e2)->Bproof (G⊦e1∈↑(e2))->Bproof (G⊦∀(i∙∂(i)∈e1⇒∂(i)∈e2))
 | Bpr_extn:forall (G:Γ)(e1 e2:Ε), Bproof (G⊦e1∈↑(e2))->Bproof (G⊦e2∈↑(e1))->Bproof (G⊦e1≡e2)
 | Bpr_bige:forall (G:Γ)(n:nat), Bproof (G⊦∞(n)∈Ω)
 | Bpr_bigd:forall (G:Γ)(n1 n2:nat), n1<>n2->Bproof (G⊦∞(n1)≢∞(n2))
 | Bpr_proi:forall (G:Γ)(e e1 e2:Ε)(i1 i2:Ι),
            i1<>i2->(i1∖e∈e1×e2)->(i2∖e∈e1×e2)->
            Bproof (G⊦∃(i1∙∂(i1)∈e1⋀∃(i2∙∂(i2)∈e2⋀e≡∂(i1)↦∂(i2))))->Bproof (G⊦e∈e1×e2)
 | Bpr_proe:forall (G:Γ)(e e1 e2:Ε)(i1 i2:Ι),
            i1<>i2->(i1∖e∈e1×e2)->(i2∖e∈e1×e2)->
            Bproof (G⊦e∈e1×e2)->Bproof (G⊦∃(i1∙∂(i1)∈e1⋀∃(i2∙∂(i2)∈e2⋀e≡∂(i1)↦∂(i2))))
 | Bpr_cpll:forall (G:Γ)(e1 e2 e3 e4:Ε), Bproof (G⊦e1↦e2≡e3↦e4)->Bproof (G⊦e1≡e3)
 | Bpr_cplr:forall (G:Γ)(e1 e2 e3 e4:Ε), Bproof (G⊦e1↦e2≡e3↦e4)->Bproof (G⊦e2≡e4).

Notation "G '⊢' p" := (Bproof (G⊦p)) (no associativity, at level 89).
Notation "'⊢' p" := (Bproof (∅ˠ⊦p)) (no associativity, at level 89).

(*================================================================================================*)