(*==================================================================================================
  Project : BiCoq3
  Module : apply.v
  We represent here application of a term to an abstraction, followed by reduction; neither the
  application nor the reduction being part of the embedded language, we code the corresponding
  situations by applying dedicated functions to terms.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jun 2008, Md: Sep 2008
  ================================================================================================*)

Require Export lift.

(* Application function --------------------------------------------------------------------------*)

Fixpoint Bp_app(p:Π)(a:Ε){struct p}:Π:=
 match p with
 | ¬p' => ¬(Bp_app p' a)
 | ∀Δ(p') => ∀Δ(Bp_app p' (Be_lift a))
 | p1⋀p2 => (Bp_app p1 a)⋀(Bp_app p2 a)
 | (p1⇒p2) => (Bp_app p1 a)⇒(Bp_app p2 a)
 | ∐(_) => p
 | e1≡e2 => (Be_app e1 a)≡(Be_app e2 a)
 | e1∈e2 => (Be_app e1 a)∈(Be_app e2 a)
 end
with Be_app(e a:Ε){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_app e' a)
 | ↑(e') => ↑(Be_app e' a)
 | e1↦e2 => (Be_app e1 a)↦(Be_app e2 a)
 | e1×e2 => (Be_app e1 a)×(Be_app e2 a)
 | ∞(_) => e
 | ∂(¡(0,0)) => a
 | ∂(¡(0,S n')) => ∂(¡(0,n'))
 | ∂(_) => e
 | ⅭΔ(e',p') => ⅭΔ(Be_app e' a,Bp_app p' (Be_lift a))
 end.

Definition Bt_app(t:Τ)(a:Ε):Τ := match t with be_ e => Be_app e a | bp_ p => Bp_app p a end.

Definition B_forapp(p:Π)(a:Ε)(H:?∀Δ(p)):Π := match p with ∀Δ(p') => Bp_app p' a | _ => ∐(0) end.
Notation "'(' p '@' a '|' H ')'" := (B_forapp p a H).

Definition B_cmpapp(e a:Ε)(H:?ⅭΔ(e)):Π :=
 match e with ⅭΔ(e',p') => (a∈e')⋀(Bp_app p' a) | _ => ∐(0) end.
Notation "'(' e '∋' a '|' H ')'" := (B_cmpapp e a H).

(* Theorems --------------------------------------------------------------------------------------*)

Theorem Bt_app_free_old:forall (t:Τ)(i:Ι)(a:Ε), Bt_free (ulft i) t=⊤->Bt_free i (Bt_app t a)=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(a:Ε), Bt_free (ulft i) t=⊤->Bt_free i (Bt_app t a)=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i a Hit;
  try (inversion Hit; fail); try (apply (Hind _ (sdepth t) i a Hit));
  try (destruct (orb_true_elim _ _ Hit) as [H | H];
       [srewrite (Hind _ (sdepth_l t1 t2) i a H) | srewrite (Hind _ (sdepth_r t1 t2) i a H)];
       bool_simpl; apply refl_equal).
  destruct u' as [| u']; simpl.
   destruct n' as [| n']; simpl; destruct i as [[| u] n]; simpl; simpl in Hit; nat_simpl;
    inversion Hit; rewrite H0; apply refl_equal.
   destruct i as [[| u] n]; inversion Hit; nat_simpl; inversion H0.
    simpl; simpl in Hit; nat_simpl; apply refl_equal.
  destruct (orb_true_elim _ _ Hit) as [H | H];
   [srewrite (Hind _ (sdepth_l t1 t2) i a H) |
    srewrite (Hind _ (sdepth_r t1 t2) (ulft i) (Be_lift a) H)]; bool_simpl; apply refl_equal.
  apply (Hind _ (sdepth t) (ulft i) (Be_lift a) Hit).
Qed.

Theorem Bt_app_free_new:forall (t:Τ)(i:Ι)(a:Ε),
                        Be_free i a=⊤->Bt_voccur ¡(0,0) t=⊤->Bt_free i (Bt_app t a)=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(a:Ε),
                            Be_free i a=⊤->Bt_voccur ¡(0,0) t=⊤->Bt_free i (Bt_app t a)=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i a Hia Ht;
  try (inversion Ht; fail); try (apply (Hind _ (sdepth t) i a Hia Ht));
  try (destruct (orb_true_elim _ _ Ht) as [H | H];
       [srewrite (Hind _ (sdepth_l t1 t2) i a Hia H) |
        srewrite (Hind _ (sdepth_r t1 t2) i a Hia H)]; bool_simpl; apply refl_equal).
  destruct u' as [| u']; simpl; nat_simpl; bool_simpl.
   destruct n' as [| n']; [apply Hia | inversion Ht].
   inversion Ht.
  destruct (orb_true_elim _ _ Ht) as [H | H].
   srewrite (Hind _ (sdepth_l t1 t2) i a Hia H); apply refl_equal.
   srewrite (Hind _ (sdepth_r t1 t2) (ulft i) (Be_lift a)).
    bool_simpl; apply refl_equal.
    srewrite (Bt_lift_free a i); apply Hia.
    apply H.
  apply (Hind _ (sdepth t) (ulft i) (Be_lift a)).
   srewrite (Bt_lift_free a i); apply Hia.
   apply Ht.
Qed.

Theorem Bt_app_lift:forall (t:Τ)(a:Ε), Bt_voccur ¡(0,0) t=⊥->⇑(Bt_app t a)=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (a:Ε), Bt_voccur ¡(0,0) t=⊥->⇑(Bt_app t a)=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind a Ht;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) a Ht); apply refl_equal);
  try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2]; irewrite (Hind _ (sdepth_l t1 t2) a Ht1);
       irewrite (Hind _ (sdepth_r t1 t2) a Ht2); apply refl_equal).
  destruct u' as [| u']; simpl; nat_simpl.
   destruct n' as [| n']; simpl; nat_simpl; [inversion Ht | apply refl_equal].
   apply refl_equal.
  destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2]; irewrite (Hind _ (sdepth_l t1 t2) a Ht1);
   irewrite (Hind _ (sdepth_r t1 t2) (Be_lift a) Ht2); apply refl_equal.
  irewrite (Hind _ (sdepth t) (Be_lift a) Ht); apply refl_equal.
Qed.

Theorem Bt_app_depth:forall (t:Τ)(i:Ι), ↧(Bt_app t ∂(i))=↧(t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι), ↧(Bt_app t ∂(i))=↧(t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i;
  try (apply refl_equal); try (srewrite (Hind _ (sdepth t) i); apply refl_equal);
  try (srewrite (Hind _ (sdepth_l t1 t2) i); srewrite (Hind _ (sdepth_r t1 t2) i);
       apply refl_equal).
  destruct i as [[| u] [| n]]; simpl; destruct u' as [| u']; simpl; try (apply refl_equal);
   destruct n' as [| n']; apply refl_equal.
  simpl; irewrite (Hind _ (sdepth_l t1 t2) i); irewrite (Hind _ (sdepth_r t1 t2) (ulft i));
   apply refl_equal.
  simpl; irewrite (Hind _ (sdepth t) (ulft i)); apply refl_equal.
Qed.

(*================================================================================================*)