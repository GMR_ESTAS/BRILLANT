(*==================================================================================================
  Project : LambdaTdB
  Module : results.v
  We study here the main properties of the calculus defined in term.v, building upon technical
  results provided in tech.v.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: Aug 2009, Md: Sep 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export tech.

(* Typing and beta -------------------------------------------------------------------------------*)

Theorem typeof_beta:forall (l l':Λ)(t:Τ),  ⊢l∶t->l→βl'->typeof l=typeof l'.
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros l' tl Htl Hll'.
  inversion Hll'.
  inversion Htl; rewrite <- H0 in *; clear H t' H1 l0 H0; rewrite (typeof_complete _ _ H2).
  inversion Hll'; rewrite <- H0 in *; clear H H0 H1 l' l0 t1; simpl.
  rewrite <- (Hl _ _ H2 H3); rewrite (typeof_complete _ _ H2); apply refl_equal.
  inversion Htl; clear H H0 H2 l3 l0 t2; rewrite (typeof_complete _ _ H1);
   rewrite (typeof_complete _ _ H3); my_simpl.
  inversion Hll'.
   rewrite <- H0 in *; rewrite <- H4 in *; clear l3 H0 H2 H4; rewrite typeof_elim_; inversion H3.
    rewrite (typeof_complete _ _ H0); apply refl_equal.
    apply H1.
   rewrite <- H0 in *; clear H H0 H2 l' l3 l0; simpl; rewrite <- (Hl1 _ _ H3 H4);
    rewrite (typeof_complete _ _ H3); rewrite (typeof_complete _ _ H1); my_simpl; apply refl_equal.
   rewrite <- H0 in *; clear H H0 H2 l' l3 l0; simpl; rewrite (typeof_complete _ _ H3);
    rewrite <- (Hl2 _ _ H1 H4); rewrite (typeof_complete _ _ H1); my_simpl; apply refl_equal.
Qed.
(* Nota: ill-formed terms can be transformed into typable terms by reduction, e.g. if l' is ill-
   formed, then so is ∆(t,l)@l'; yet [t1↙l']l can still be typable in the absence of the appropriate
   variable in l, the elimination being the reduced to identity. *)

(* Normal form and beta --------------------------------------------------------------------------*)

Theorem normal_or_reduce:forall (l:Λ), (normal l)+{l':Λ & l→βl'}.
Proof.
 intros l; destruct (normal_dec l) as [Hn | Hn];
 [left; apply Hn | right; apply (not_normal_beta _ Hn)].
Qed.
(* Nota: this theorems means first that one can decide whether or not a term is in normal form,
   and that if it is not, then he can exhibit a reduction.                                        *)

(* Commutation of elimination and beta -----------------------------------------------------------*)

Theorem beta_elim_le:forall (te:Τ)(l le le':Λ), le→βle'->[te↙le]l→*β[te↙le']l.
Proof.
 intros te l le le' Hbeta; apply beta_elim_le_; apply Hbeta.
Qed.

Theorem beta_elim_le_star:forall (te:Τ)(l le le':Λ), le→*βle'->[te↙le]l→*β[te↙le']l.
Proof.
 intros te l le le' Hbeta; apply beta_elim_le_star_; apply Hbeta.
Qed.

Theorem beta_elim_l:forall (te:Τ)(l l' le:Λ), l→βl'->[te↙le]l→β[te↙le]l'.
Proof.
 intros te l l' le Hll'; apply (beta_elim_l_ te _ _ Hll'); apply valid_eq_.
Qed.

Theorem beta_elim_l_star:forall (te:Τ)(l l' le:Λ), l→*βl'->[te↙le]l→*β[te↙le]l'.
Proof.
 intros te l l' le Hll'; induction Hll'.
  apply bs_beta; apply beta_elim_l; apply b.
  apply bs_refl.
  apply bs_tran with (l2:=[te↙le]l2); [apply IHHll' | apply beta_elim_l; apply b].
Qed.

(* Confluence of beta ----------------------------------------------------------------------------*)

Theorem beta_weak_confl:forall (l l1' l2':Λ),
                        l→βl1'->l→βl2'->{l':Λ  & ((l1'→*βl')*(l2'→*βl'))%type}.
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros l1' l2' Hll1' Hll2'.
  inversion Hll1'.
  inversion Hll1'; rewrite <- H0 in *; inversion Hll2'; rewrite <- H4 in *;
   destruct (Hl _ _ H2 H6) as [lj [Hlj H'lj]]; exists ∆(t,lj); split.
   apply beta_star_lam; apply Hlj.
   apply beta_star_lam; apply H'lj.
  inversion Hll1'; inversion Hll2'.
   rewrite <- H0 in *; rewrite <- H2 in *; rewrite <- H5 in *; inversion H3; rewrite H6 in *;
    rewrite H7 in *; exists ([t1↙l2]l0); split; apply bs_refl.
   rewrite <- H0 in *; rewrite <- H2 in *; rewrite <- H3 in *; inversion H5; rewrite <- H7 in *;
    exists ([t1↙l2]l'); split.
    apply bs_beta; apply beta_elim_l; apply H9.
    apply bs_beta; apply beta_red.
   rewrite <- H0 in *; rewrite <- H2 in *; rewrite <- H3 in *; exists ([t1↙l2'0]l0); split.
    apply beta_elim_le; apply H5.
    apply bs_beta; apply beta_red.
   rewrite <- H0 in *; rewrite <- H3 in *; rewrite <- H4 in *; inversion H2; rewrite <- H7 in *;
    exists ([t1↙l2]l'); split.
    apply bs_beta; apply beta_red.
    apply bs_beta; apply beta_elim_l; apply H9.
   rewrite <- H0 in *; rewrite <- H4 in *; destruct (Hl1 _ _ H2 H6) as [lj [Hlj H'lj]];
    exists (lj@l2); split.
    apply beta_star_app_l; apply Hlj.
    apply beta_star_app_l; apply H'lj.
   rewrite <- H0 in *; rewrite <- H4 in *; exists (l1'0@l2'0); split.
    apply bs_beta; apply beta_app_r; apply H6.
    apply bs_beta; apply beta_app_l; apply H2.
   rewrite <- H0 in *; rewrite <- H3 in *; rewrite <- H4 in *; exists ([t1↙l2'0]l4); split.
    apply bs_beta; apply beta_red.
    apply beta_elim_le; apply H2.
   rewrite <- H0 in *; rewrite <- H4 in *; exists (l1'0@l2'0); split.
    apply bs_beta; apply beta_app_l; apply H6.
    apply bs_beta; apply beta_app_r; apply H2.
   rewrite <- H0 in *; rewrite <- H4 in *; destruct (Hl2 _ _ H2 H6) as [lj [Hlj H'lj]];
    exists (l1@lj); split.
    apply beta_star_app_r; apply Hlj.
    apply beta_star_app_r; apply H'lj.
Qed.
(* Nota: it is in fact strongly confluent, that is if l→βl1' and l→βl2' then there exists l' s.t.
   l1'→=βl' and l2→*βl', we do not need the transitive closure on both sides *)

(*================================================================================================*)