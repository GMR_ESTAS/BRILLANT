(*==================================================================================================
  Project : LambdaTdB
  Developed for Coq 8.2, using UTF-8 and the Everson Mono Unicode font
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: Aug 2009, Md: Sep 2009 (approximately)
  Provided under licence CeCILL-B
  ================================================================================================*)

This is the source code of LambdaTdB, a deep embedding of a simply typed lambda-calculus.

We extend the idea of namespaces used in BiCoq3I to define parameterised de Bruijn indexes, the
parameter being here the type. This representation does not require a context management, as all
the required information are embedded in the term.

The file make.txt describes the dependencies between the files.

The file notations.txt provides all the UTF-8 notations introduced in the development, with their
associated definition.
