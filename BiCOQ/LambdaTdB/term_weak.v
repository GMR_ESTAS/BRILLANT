(*==================================================================================================
  Project : LambdaTdB
  Module : term_weak.v
  We define a new form of typed lambda-calculus, which is directly inspired by the de Bruijn
  notation with namespaces introduced in BiCoq (a deep embedding of B in Coq). Namespaces were
  introduced as sorts associated to an index, and binders were binding only some namespaces - that
  is index of unbounded namespaces were merely names. Here we extend the idea to types, noting that
  with this approach there is no need to manage a typing context... and we apply it to a standard
  lambda calculus. Compared with term.v, we consider here a weaker version with a less precise
  context management; this appear to be the standard version, and we encode it for the sake of
  comparison with our improved version.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: Sep 2009, Md: Sep 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export Bool.
Require Export Peano_dec.
Require Export Compare_dec.

(* Tools -----------------------------------------------------------------------------------------*)

Theorem eq_refl:forall (T:Type)(T_eq_dec:forall (t1 t2:T), {t1=t2}+{t1<>t2})(t:T)(S:Type)(s1 s2:S),
                (if T_eq_dec t t then s1 else s2)=s1.
Proof.
 intros T T_eq_dec t S s1 s2; destruct (T_eq_dec t t) as [_ | Ht]; [ | destruct Ht];
  apply refl_equal.
Qed.
Implicit Arguments eq_refl [T].

Theorem eq_refl_simpl:forall (T:Type)(t1 t2:T), t1=t2->
                      forall (T_eq_dec:forall (t1 t2:T), {t1=t2}+{t1<>t2})(S:Type)(s1 s2:S),
                      (if T_eq_dec t1 t2 then s1 else s2)=s1.
Proof.
 intros T t1 t2 Heq T_eq_dec S s1 s2; rewrite Heq; apply eq_refl.
Qed.
Implicit Arguments eq_refl_simpl [T t1 t2].

Theorem sum_simpl:forall (A B T:Type)(sum:A+B)(t:T), (if sum then t else t)=t.
Proof.
 intros A B T [_ | _] t; apply refl_equal.
Qed.

Theorem dec_simpl:forall (A B:Prop)(T:Type)(sum:{A}+{B})(t:T), (if sum then t else t)=t.
Proof.
 intros A B T [_ | _] t; apply refl_equal.
Qed.

Theorem bool_simpl:forall (A B:Prop)(sum:{A}+{B})(T:Type)(t1 t2:T),
                   (if (if sum then true else false) then t1 else t2)=if sum then t1 else t2.
Proof.
 intros A B [_ | _] T t1 t2; apply refl_equal.
Qed.

Theorem eq_symm:forall (T:Type)(T_eq_dec:forall (t1 t2:T), {t1=t2}+{t1<>t2})(t1 t2:T)
                       (S:Type)(s1 s2:S),
                (if T_eq_dec t1 t2 then s1 else s2)=(if T_eq_dec t2 t1 then s1 else s2).
Proof.
 intros T T_eq_dec t1 t2 S s1 s2; destruct (T_eq_dec t1 t2) as [H12 | H12].
  rewrite H12; rewrite eq_refl; apply refl_equal.
  destruct (T_eq_dec t2 t1) as [H21 | H21].
   rewrite H21 in H12; destruct H12; apply refl_equal.
   apply refl_equal.
Qed.
Implicit Arguments eq_symm [T].

Theorem left_simpl:forall (A:Prop), A->
                   forall (dec:{A}+{~A})(T:Type)(t1 t2:T), (if dec then t1 else t2)=t1.
Proof.
 intros A HA [H'A | H'A] T t1 t2; [apply refl_equal | destruct (H'A HA)].
Qed.
Implicit Arguments left_simpl [A].

Theorem right_simpl:forall (A:Prop), ~A-> 
                    forall (dec:{A}+{~A})(T:Type)(t1 t2:T), (if dec then t1 else t2)=t2.
Proof.
 intros A HA [H'A | H'A] T t1 t2; [destruct (HA H'A) | apply refl_equal].
Qed.
Implicit Arguments right_simpl [A].

Theorem sum_simpl_distr:forall (A B T T':Type)(sum:A+B)(f:T->T')(t1 t2:T),
                        (if sum then f t1 else f t2)=f(if sum then t1 else t2).
Proof.
 intros A B T T' [_ | _] f t1 t2; apply refl_equal.
Qed.

Theorem dec_simpl_distr:forall (A B:Prop)(T T':Type)(sum:{A}+{B})(f:T->T')(t1 t2:T),
                        (if sum then f t1 else f t2)=f(if sum then t1 else t2).
Proof.
 intros A B T T' [_ | _] f t1 t2; apply refl_equal.
Qed.

(* Types -----------------------------------------------------------------------------------------*)

Variable LSrt:Set. (* Sorts that are atomic types *)
Notation "'Φ'" := (LSrt).

Variable iota:Φ. (* There is at least one sort *)
Notation "'φ'" := (iota).

Variable LSrt_eq:forall (s1 s2:Φ), {s1=s2}+{s1<>s2}. (* Decidable equality for sorts *)

Inductive LTyp:Set :=
 | atom:Φ->LTyp          (* Any sort is a type *)
 | arrow:LTyp->LTyp->LTyp (* Functional type *)
 .
Coercion atom:LSrt>->LTyp. (* Any sort is seen as a type *)
Notation "'Τ'" := (LTyp).
Notation "t1 '→' t2" := (arrow t1 t2) (right associativity, at level 60).

Theorem LTyp_eq:forall (t1 t2:Τ), {t1=t2}+{t1<>t2}.
Proof.
 decide equality; apply LSrt_eq.
Defined.

(* Indexes ---------------------------------------------------------------------------------------*)

Notation "'Ι'" := (nat). (* Use restricted to de Bruijn indexes, which are numbered from 0 *)

Definition LIdx_eq := eq_nat_dec.

Definition LIdx_le := le_lt_dec.

Theorem le_simpl:forall (m n:Ι), m<=n->
                 forall (T:Type)(t1 t2:T), (if LIdx_le m n then t1 else t2)=t1.
Proof.
 intros m n Hle T t1 t2; destruct (LIdx_le m n) as [_ | Hmn].
  apply refl_equal.
  destruct ((Lt.lt_not_le _ _ Hmn) Hle).
Qed.
Implicit Arguments le_simpl [m n].

Theorem le_0_simpl:forall (n:Ι)(T:Type)(t1 t2:T), (if LIdx_le 0 n then t1 else t2)=t1.
Proof.
 intros n T t1 t2; rewrite (le_simpl (Le.le_O_n n)); apply refl_equal.
Qed.

Theorem gt_simpl:forall (m n:Ι), n<m->
                 forall (T:Type)(t1 t2:T), (if LIdx_le m n then t1 else t2)=t2.
Proof.
 intros m n Hgt T t1 t2; destruct (LIdx_le m n) as [Hmn | _].
  destruct ((Lt.lt_not_le _ _ Hgt) Hmn).
  apply refl_equal.
Qed.
Implicit Arguments gt_simpl [m n].

Theorem eq_le_simpl:forall (n:Ι)(T:Type)(t1 t2:T), (if LIdx_le n n then t1 else t2)=t1.
Proof.
 intros n T t1 t2; destruct (LIdx_le n n) as [Hn | Hn];
 [apply refl_equal | destruct (Lt.lt_irrefl _ Hn)].
Qed.

Theorem eq_S_le_simpl:forall (n:Ι)(T:Type)(t1 t2:T), (if LIdx_le (S n) n then t1 else t2)=t2.
Proof.
 intros n T t1 t2; rewrite (gt_simpl (not_ge _ _ (Le.le_Sn_n n))); apply refl_equal.
Qed.

Theorem eq_le_S_simpl:forall (n:Ι)(T:Type)(t1 t2:T), (if LIdx_le n (S n) then t1 else t2)=t1.
Proof.
 intros n T t1 t2; rewrite (le_simpl (Le.le_n_Sn n)); apply refl_equal.
Qed.

Theorem eq_S_simpl:forall (n:Ι)(T:Type)(t1 t2:T), (if LIdx_eq (S n) n then t1 else t2)=t2.
Proof.
 intros n T t1 t2; destruct (LIdx_eq (S n) n).
  induction n; inversion e; apply IHn; apply H0.
  apply refl_equal.
Qed.

Theorem eq_S_simpl':forall (n:Ι)(T:Type)(t1 t2:T), (if LIdx_eq n (S n) then t1 else t2)=t2.
Proof.
 intros n T t1 t2; destruct (LIdx_eq n (S n)).
  induction n; inversion e; apply IHn; apply H0.
  apply refl_equal.
Qed.

Theorem eq_S_S_simpl:forall (n m:Ι)(T:Type)(t1 t2:T),
                     (if LIdx_eq (S m) (S n) then t1 else t2)=
                     (if LIdx_eq m n then t1 else t2).
Proof.
 intros n m T t1 t2; destruct (LIdx_eq m n).
  rewrite e; rewrite eq_refl; apply refl_equal.
  destruct (LIdx_eq (S m) (S n)).
   inversion e; destruct (n0 H0).
   apply refl_equal.
Qed.

Theorem le_S_S_simpl:forall (n m:Ι)(T:Type)(t1 t2:T),
                     (if LIdx_le (S m) (S n) then t1 else t2)=
                     (if LIdx_le m n then t1 else t2).
Proof.
 intros n m T t1 t2; destruct (LIdx_le m n); destruct (LIdx_le (S m) (S n)).
  apply refl_equal.
  destruct ((Lt.lt_not_le _ _ (Lt.lt_S_n _ _ l0)) l).
  destruct ((Lt.lt_not_le _ _ l) (Le.le_S_n _ _ l0)).
  apply refl_equal.
Qed.

(* Tactics ---------------------------------------------------------------------------------------*)

Ltac my_simpl :=
 repeat (rewrite eq_refl in * ||
         rewrite dec_simpl in * ||
         rewrite sum_simpl in * ||
         rewrite bool_simpl in * ||
         rewrite le_0_simpl in * ||
         rewrite eq_le_simpl in * ||
         rewrite eq_le_S_simpl in * ||
         rewrite eq_S_le_simpl in * ||
         rewrite eq_S_simpl in * ||
         rewrite eq_S_simpl' in * ||
         rewrite eq_S_S_simpl in * ||
         rewrite le_S_S_simpl in * ||
         rewrite sum_simpl_distr in * ||
         rewrite dec_simpl_distr in * ||
         my_simpl_1)
with my_simpl_1 :=
 match goal with
 | H:_=_ |- _ => rewrite (eq_refl_simpl H) in * ||
                 rewrite (eq_refl_simpl (sym_equal H)) in *
 | H:(S _)<=(S _) |- _ => (rewrite (le_simpl (Le.le_S_n _ _ H)) in * ||
                           rewrite (le_simpl H) in *)
 | H:_<=_ |- _ => (rewrite (le_simpl H) in * ||
                   rewrite (le_simpl (Le.le_n_S _ _ H)) in * ||
                   rewrite (le_simpl (le_S _ _ H)) in *)
 | H1:_<=_, H2:_<=_ |- _ => (rewrite (le_simpl (Le.le_trans _ _ _ H1 H2)) in * ||
                             rewrite (le_simpl (Le.le_trans _ _ _ H2 H1)) in *)
 | H1:_<=_, H2:_<_ |- _ => (rewrite (gt_simpl (Lt.le_lt_trans _ _ _ H1 H2)) in * ||
                             rewrite (gt_simpl (Lt.lt_le_trans _ _ _ H2 H1)) in *)
 | H1:_<_, H2:_<_ |- _ => (rewrite (gt_simpl (Lt.lt_trans _ _ _ H1 H2)) in * ||
                           rewrite (gt_simpl (Lt.lt_trans _ _ _ H2 H1)) in *)
 | H:_<_ |- _ => (rewrite (gt_simpl H) in * ||
                  rewrite (gt_simpl (Lt.lt_n_S _ _ H)) in * ||
                  rewrite (gt_simpl (Lt.lt_S _ _ H)) in * ||
                  rewrite (le_simpl H) in *)
 | H:_<>_ |- _ => (rewrite (right_simpl (sym_not_eq H)) in *)
 | H:~_ |- _ => rewrite (right_simpl H) in *
 | H:_ |- _ => rewrite (left_simpl H) in *
 end
.

(* Terms -----------------------------------------------------------------------------------------*)
(* Note that this is a first-order lambda-calculus, in the sense that types are not terms         *)

Inductive LLam:Set :=
 | var:Τ->Ι->LLam       (* Variables, that is a type and an index *)
 | lam:Τ->LLam->LLam    (* Abstraction *)
 | app:LLam->LLam->LLam (* Application *)
 .
Notation "'Λ'" := (LLam).
Notation "'√(' t ',' i ')'" := (var t i).
Notation "'∆(' t ',' l ')'" := (lam t l).
Notation "l1 '@' l2" := (app l1 l2) (left associativity, at level 50).

Theorem LLam_eq:forall (l1 l2:Λ), {l1=l2}+{l1<>l2}.
Proof.
 decide equality; [apply eq_nat_dec | apply LTyp_eq | apply LTyp_eq].
Defined.

(* Typing ----------------------------------------------------------------------------------------*)
(* We describe how to check a type judgment and how to compute a type, before showing equivalence
   between the two approaches. Note that because we represent application, we can build ill-typed
   term, therefore there is not always a type...                                                  *)

Inductive IsofType:Λ->Τ->Prop:=
 | IoTvar:forall (t:Τ)(i:Ι), IsofType (√(t,i)) t
 | IoTlam:forall (t t':Τ)(l:Λ), IsofType l t->IsofType ∆(t',l) (t'→t)
 | IoTapp:forall (t1 t2:Τ)(l1 l2:Λ), IsofType l1 t1->IsofType l2 (t1→t2)->IsofType (l2@l1) t2
.
Notation "'⊢' l '∶' t" := (IsofType l t) (no associativity, at level 40).

Fixpoint typeof(l:Λ):option Τ:=
 match l with
 | √(t',_) => Some t'
 | ∆(t',l) => match typeof l with None => None | Some t => Some (t'→t) end
 | l1@l2 => match typeof l1 with
            | Some (t2→t1) => match typeof l2 with
                              | Some t'2 => if LTyp_eq t2 t'2 then Some t1 else None
                              | _ => None
                              end
            | _ => None
            end
 end.

Theorem typeof_complete:forall (l:Λ)(t:Τ), ⊢l∶t->typeof l=Some t.
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; intros t' Ht'; simpl.
  inversion_clear Ht'; apply refl_equal.
  inversion_clear Ht'; rewrite (Hl _ H); apply refl_equal.
  inversion_clear Ht'.
   rewrite (Hl1 _ H0); rewrite (Hl2 _ H).
   rewrite eq_refl; apply refl_equal.
Qed.

Theorem typeof_correct:forall (l:Λ), match typeof l with None => True | Some t => ⊢l∶t end.
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl.
  apply IoTvar.
  destruct (typeof l) as [t' | ]; [ | apply I].
   apply IoTlam; apply Hl.
  destruct (typeof l1) as [t1 | ]; [ | apply I].
   destruct (typeof l2) as [t2 | ]; [ | destruct t1; apply I].
    destruct t1 as [ | t2' t1]; [apply I | ].
     destruct (LTyp_eq t2' t2) as [Heq | _]; [ | apply I].
      apply IoTapp with (t1:=t2).
       apply Hl2.
       rewrite Heq in Hl1; apply Hl1.
Qed.

Theorem typeof_fail:forall (l:Λ),
                    match typeof l with None => forall (t:Τ), ~⊢l∶t | Some _ => True end.
Proof.
 intros l; case_eq (typeof l).
  intros _ _; apply I.
  intros Hl t Ht; rewrite (typeof_complete _ _ Ht) in Hl; inversion Hl.
Qed.

Theorem not_IsofType:forall (l:Λ), (forall (t:Τ), ~⊢l∶t)->typeof l=None.
Proof.
 intros l Hl; case_eq (typeof l).
  intros t Ht; destruct (Hl t); generalize (typeof_correct l); rewrite Ht; intros H; apply H.
  intros _; apply refl_equal.
Qed.

(* Lifting ---------------------------------------------------------------------------------------*)

Fixpoint lift(t:Τ)(h:Ι)(l:Λ){struct l}:Λ :=
 match l with
 | √(t',i') => √(t', if LTyp_eq t t'
                     then (if LIdx_le h i' then S i' else i')
                     else i')
 | ∆(t',l') => ∆(t',lift t (if LTyp_eq t t' then S h else h) l')
 | l1@l2 => (lift t h l1)@(lift t h l2)
 end.
Notation "'↑(' t ',' le ')'" := (lift t 0 le).

(* Binder Elimination ----------------------------------------------------------------------------*)

Fixpoint elim(t:Τ)(h:Ι)(le l:Λ){struct l}:Λ:=
 match l with
 | √(t',i') => if LTyp_eq t t'
               then (if LIdx_le h i'
                     then (if LIdx_eq h i' then le else √(t',pred i'))
                     else √(t',i'))
               else √(t',i')
 | ∆(t',l') => ∆(t',elim t (if LTyp_eq t t' then S h else h) ↑(t',le) l')
 | l1@l2 => (elim t h le l1)@(elim t h le l2)
 end.
Notation "'[' t '↙' le ']'" := (elim t 0 le).

Theorem elim_lift:forall (te:Τ)(l le:Λ)(h:Ι), elim te h le (lift te h l)=l.
Proof.
 intros te; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros le h.
  destruct (LTyp_eq te t) as [Htet | Htet]; [ | apply refl_equal].
   destruct (LIdx_le h i) as [Hhi | Hhi]; my_simpl; [ | apply refl_equal].
    destruct (LIdx_eq h (S i)) as [HhSi | _];
    [rewrite HhSi in * ; destruct (Le.le_Sn_n _ Hhi) | apply refl_equal].
  rewrite Hl; apply refl_equal.
  rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.

(* Abstraction -----------------------------------------------------------------------------------*)

Fixpoint bind(t:Τ)(h i:Ι)(l:Λ){struct l}:Λ:=
 match l with
 | √(t',i') => √(t',if LTyp_eq t t'
                    then (if LIdx_le h i'
                          then (if LIdx_eq i i' then h else S i')
                          else i')
                    else i')
 | ∆(t',l') => ∆(t',bind t (if LTyp_eq t t' then S h else h)
                           (if LTyp_eq t t' then S i else i) l')
 | l1@l2 => (bind t h i l1)@(bind t h i l2)
 end.

Definition lambda_abstract(t:Τ)(i:Ι)(l:Λ) := ∆(t,bind t 0 i l).
Notation "'‹' i '∶' t '↦' l '›'" := (lambda_abstract t i l).

(* Substitution ----------------------------------------------------------------------------------*)

Fixpoint subst(t:Τ)(i:Ι)(ls l:Λ){struct l}:Λ :=
 match l with
 | √(t',i') => if LTyp_eq t t'
               then (if LIdx_eq i i' then ls else √(t',i'))
               else √(t',i')
 | ∆(t',l') => ∆(t',subst t (if LTyp_eq t t' then S i else i) ↑(t',ls) l')
 | l1@l2 => (subst t i ls l1)@(subst t i ls l2)
 end.
Notation "'[' t ',' i '≔' ls ']'" := (subst t i ls).

Theorem subst_not_primitive_:forall (t:Τ)(l ls:Λ)(h i:Ι), h<=i->
                             elim t h ls (bind t h i l)=subst t i ls l.
Proof.
 intros ts; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros ls hs is Hle.
  destruct (LTyp_eq ts t) as [Htst | _]; [ | apply refl_equal].
   destruct (LIdx_le hs i) as [Hhsi | Hhsi].
    destruct (LIdx_eq is i) as [Hisi | Hisi]; my_simpl.
     apply refl_equal.
     destruct (LIdx_eq hs (S i)) as [HhsSi | HhsSi].
      rewrite HhsSi in *; destruct (Le.le_Sn_n _ Hhsi).
      apply refl_equal.
    my_simpl; destruct (LIdx_eq is i) as [Hisi | Hisi].
     rewrite Hisi in *; destruct ((Lt.lt_not_le _ _ Hhsi) Hle).
     apply refl_equal.
  rewrite Hl.
   apply refl_equal.
   destruct (LTyp_eq ts t); [apply Le.le_n_S | ]; apply Hle.
  rewrite Hl1.
   rewrite Hl2.
    apply refl_equal.
    apply Hle.
   apply Hle.
Qed.

Theorem subst_neutral_:forall (t:Τ)(l:Λ)(i:Ι), subst t i √(t,i) l=l.
Proof.
 intros ts; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; intros is; simpl.
  destruct (LTyp_eq ts t) as [Htst | Htst]; [ | apply refl_equal].
   rewrite Htst in *; destruct (LIdx_eq is i) as [Hisi | Hisi]; [rewrite Hisi | ];
    apply refl_equal.
  destruct (LTyp_eq ts t); my_simpl; rewrite Hl; apply refl_equal.
  rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.

(* Freeness --------------------------------------------------------------------------------------*)

Fixpoint free(t:Τ)(i:Ι)(l:Λ){struct l}:bool :=
 match l with
 | √(t',i') => if LTyp_eq t t'
               then (if LIdx_eq i i' then true else false)
               else false
 | ∆(t',l') => free t (if LTyp_eq t t' then S i else i) l'
 | l1@l2 => (free t i l1) || (free t i l2)
 end.

Theorem free_abstract:forall (t:Τ)(l:Λ)(h i:Ι), free t i l=false->bind t h i l=lift t h l.
Proof.
 intros ta; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros ha ia Hfree.
  destruct (LTyp_eq ta t) as [Htat | Htat].
   destruct (LIdx_le ha i) as [Hhai | Hhai].
    destruct (LIdx_eq ia i) as [Hiai | Hiai].
     inversion Hfree.
     apply refl_equal.
    apply refl_equal.
   apply refl_equal.
  rewrite Hl.
   apply refl_equal.
   apply Hfree.
  destruct (orb_false_elim _ _ Hfree) as [Hfree1 Hfree2]; rewrite Hl1.
   rewrite Hl2.
    apply refl_equal.
    apply Hfree2.
   apply Hfree1.
Qed.

Theorem free_subst:forall (ts:Τ)(l ls:Λ)(is:Ι), free ts is l=false->subst ts is ls l=l.
Proof.
 intros ts l ls is Hfree.
 rewrite <- subst_not_primitive_ with (1:=Le.le_refl is).
 rewrite (free_abstract ts l is is Hfree).
 apply elim_lift.
Qed.

(* RESULT FALSE: CONSIDER t<>t' AND l=∆(t',l')...
Theorem free_S_:forall (t t':Τ)(l:Λ)(h i:Ι), h<=i->
                free t (if LTyp_eq t t' then S i else i) (lift t' h l)=free t i l.
*)

Theorem free_S_1:forall (t:Τ)(l:Λ)(h i:Ι), h<=i->free t (S i) (lift t h l)=free t i l.
Proof.
 intros t'; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; intros h ii Hle.
  unfold lift; unfold free; destruct (LTyp_eq t' t) as [Ht't | Ht't]; my_simpl.
   destruct (LIdx_le h i) as [Hhi | Hhi]; my_simpl.
     destruct (LIdx_eq ii i) as [Hiii | Hiii]; [rewrite Hiii in * | ]; my_simpl;
      apply refl_equal.
     destruct (LIdx_eq (S ii) i) as [HSiii | HSiii]; [rewrite <- HSiii in * | ]; my_simpl.
      destruct (Le.le_Sn_n _ (Lt.lt_le_weak _ _ (Lt.lt_le_trans _ _ _ Hhi Hle))).
      destruct (LIdx_eq ii i) as [Hiii | Hiii]; [rewrite Hiii in * | ]; my_simpl.
       destruct (Le.le_Sn_n _ (Lt.lt_le_trans _ _ _ Hhi Hle)).
       apply refl_equal.
   apply refl_equal.
  simpl; destruct (LTyp_eq t' t) as [Ht't | Ht't]; apply Hl; [apply Le.le_n_S | ]; apply Hle.
  simpl; rewrite Hl1.
   rewrite Hl2.
    apply refl_equal.
    apply Hle.
   apply Hle.
Qed.

Theorem free_S_2:forall (t t':Τ), t<>t'->
                 forall (l:Λ)(h i:Ι), free t i (lift t' h l)=free t i l.
Proof.
 intros t1 t2 Ht; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros h ii.
  destruct (LTyp_eq t1 t) as [Ht1t | Ht1t]; [rewrite Ht1t in * | ]; my_simpl; apply refl_equal.
 apply Hl.
 rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.

(* Technical results -----------------------------------------------------------------------------*)

Theorem lift_lift_1:forall (t:Τ)(l:Λ)(h1 h2:Ι),
                    h1<=h2->lift t h1 (lift t h2 l)=lift t (S h2) (lift t h1 l).
Proof.
 intros t'; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; intros h1 h2 Hle.
  unfold lift; destruct (LTyp_eq t' t) as [Ht't | Ht't]; my_simpl; [ | try apply refl_equal].
   destruct (LIdx_le h2 i) as [Hh2i | Hh2i]; my_simpl.
    destruct (LIdx_le h1 (S i)) as [Hh1Si | Hh1Si]; my_simpl.
     apply refl_equal.
     destruct (Le.le_Sn_n _
               (Le.le_trans _ _ _ (Lt.lt_le_weak _ _ (Lt.lt_le_trans _ _ _ Hh1Si Hle)) Hh2i)).
    destruct (LIdx_le h1 i) as [Hh1i | Hh1i]; my_simpl; apply refl_equal.
  simpl; destruct (LTyp_eq t' t); rewrite Hl.
   apply refl_equal.
   apply Le.le_n_S; apply Hle.
   apply refl_equal.
   apply Hle.
  simpl; rewrite Hl1.
   rewrite Hl2.
    apply refl_equal.
    apply Hle.
   apply Hle.
Qed.

Theorem lift_lift_2:forall (t1 t2:Τ), t1<>t2->
                    forall (l:Λ)(h1 h2:Ι), lift t1 h1 (lift t2 h2 l)=lift t2 h2 (lift t1 h1 l).
Proof.
 intros t1 t2 Ht; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros h1 h2.
  destruct (LTyp_eq t1 t) as [Ht1t | Ht1t]; [rewrite Ht1t in * | ];
   destruct (LTyp_eq t2 t) as [Ht2t | Ht2t]; try (rewrite Ht2t in *); my_simpl;
   try (apply refl_equal); destruct Ht; apply refl_equal.
  destruct (LTyp_eq t1 t) as [Ht1t | Ht1t]; [rewrite Ht1t in * | ];
   destruct (LTyp_eq t2 t) as [Ht2t | Ht2t]; try (rewrite Ht2t in *); my_simpl; rewrite Hl;
   apply refl_equal.
  simpl; rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.

(* Commutation of lifting and elimination --------------------------------------------------------*)

Theorem lift_elim_1:forall (t:Τ)(l le:Λ)(he hl:Ι), he<=hl->
                    lift t hl (elim t he le l)=elim t he (lift t hl le) (lift t (S hl) l).
Proof.
 intros t'; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; intros le he hl Hle.
  unfold lift, elim; fold lift; destruct (LTyp_eq t' t) as [Ht't | Ht't];
  [rewrite Ht't in * | simpl; my_simpl; apply refl_equal].
   destruct (LIdx_le he i) as [Hhei | Hhei]; my_simpl.
    destruct (LIdx_eq he i) as [H'hei | H'hei]; [rewrite H'hei in * | ]; my_simpl.
     rewrite (gt_simpl (Le.le_n_S _ _ Hle)); my_simpl; apply refl_equal.
     destruct i as [| i]; [inversion Hhei; destruct H'hei; apply H | ]; my_simpl;
      destruct (LIdx_le hl i) as [Hhli | Hhli]; my_simpl.
      simpl; my_simpl; destruct (LIdx_eq he (S (S i))) as [HheSSi | HheSSi].
       rewrite HheSSi in Hhei; destruct (Le.le_Sn_n _ Hhei).
       apply refl_equal.
      simpl; my_simpl; apply refl_equal.
    rewrite (gt_simpl (Lt.lt_S _ _ (Lt.lt_le_trans _ _ _ Hhei Hle))); rewrite (gt_simpl Hhei);
     simpl; my_simpl; apply refl_equal.
  simpl; rewrite Hl; destruct (LTyp_eq t' t) as [Ht't | Ht't]; try (rewrite Ht't in *);
   my_simpl.
    rewrite lift_lift_1 with (1:=Le.le_O_n hl); apply refl_equal.
    rewrite lift_lift_2 with (1:=Ht't); apply refl_equal.
    apply Le.le_n_S; apply Hle.
    apply Hle.
  simpl; rewrite Hl1.
   rewrite Hl2.
    apply refl_equal.
    apply Hle.
   apply Hle.
Qed.

Theorem lift_elim_2:forall (te tl:Τ), te<>tl->forall (l le:Λ)(he hl:Ι),
                    lift tl hl (elim te he le l)=elim te he (lift tl hl le) (lift tl hl l).
Proof.
 intros te tl Ht; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros le he hl.
  destruct (LTyp_eq te t) as [Htet | Htet]; [rewrite Htet in * | ];
   destruct (LTyp_eq tl t) as [Htlt | Htlt]; try (rewrite Htlt in *); my_simpl; simpl;
   my_simpl; try (apply refl_equal).
   destruct Ht; apply refl_equal.
   destruct (LIdx_le he i) as [Hhei | Hhei]; my_simpl.
    destruct (LIdx_eq he i) as [H'hei | H'hei]; [rewrite H'hei in * | ]; my_simpl; simpl;
     my_simpl; apply refl_equal.
    simpl; my_simpl; apply refl_equal.
  rewrite Hl; destruct (LTyp_eq te t) as [Htet | Htet]; [rewrite Htet in * | ];
   destruct (LTyp_eq tl t) as [Htlt | Htlt]; try (rewrite Htlt in *); my_simpl; simpl.
   rewrite lift_lift_1 with (1:=Le.le_O_n hl); apply refl_equal.
   rewrite lift_lift_2 with (1:=Htlt); apply refl_equal.
   rewrite lift_lift_1 with (1:=Le.le_O_n hl); apply refl_equal.
   rewrite lift_lift_2 with (1:=Htlt); apply refl_equal.
  rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.

(*================================================================================================*)