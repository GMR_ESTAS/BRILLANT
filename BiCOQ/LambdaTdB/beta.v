(*==================================================================================================
  Project : LambdaTdB
  Module : beta.v
  We complete the definitions provided in term.v with definition of beta-reduction.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: Sep 2009, Md: Sep 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export term.

(* Normal form -----------------------------------------------------------------------------------*)

Inductive normal:Λ->Set :=
 | normal_var:forall (t:Τ)(i:Ι), normal √(t,i)
 | normal_lam:forall (t:Τ)(l:Λ), normal l->normal ∆(t,l)
 | normal_app:forall (l1 l2:Λ),
              normal l1->normal l2->(match l1 with ∆(_,_) => False | _ => True end)->normal (l1@l2).

Theorem normal_lift:forall (l:Λ)(t:Τ)(m:Μ), normal l->normal (lift m t l).
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros t' d' Hnrm.
  apply normal_var.
  apply normal_lam; inversion_clear Hnrm; apply Hl; apply H.
  inversion_clear Hnrm; apply normal_app.
   apply Hl1; apply H.
   apply Hl2; apply H0.
   destruct l1; simpl; apply H1.
Qed.

Theorem normal_dec:forall (l:Λ), (normal l)+(normal l->False).
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl.
  left; apply normal_var.
  destruct Hl as [Hl | Hl].
   left; apply normal_lam; apply Hl.
   right; intros H'l; apply Hl; inversion_clear H'l; apply H.
  destruct l1 as [t1 i1 | t1 l1 | l11 l12].
   destruct Hl2 as [Hl2 | Hl2].
    left; apply normal_app; [apply normal_var | apply Hl2 | apply I].
    right; intros Hn; apply Hl2; inversion Hn; apply H2.
   right; intros Hn; inversion Hn; apply H3.
   destruct Hl1 as [Hl1 | Hl1].
    destruct Hl2 as [Hl2 | Hl2].
     left; apply normal_app; [apply Hl1 | apply Hl2 | apply  I].
     right; intros Hn; apply Hl2; inversion Hn; apply H2.
    right; intros Hn; apply Hl1; inversion Hn; apply H1.
Qed.

(* Beta-reduction --------------------------------------------------------------------------------*)

Inductive beta:Λ->Λ->Set:=
| beta_red:forall (t1:Τ)(l1 l2:Λ), beta (∆(t1,l1)@l2) ([t1↙l2] l1)
| beta_lam:forall (t:Τ)(l l':Λ), beta l l'->beta ∆(t,l) ∆(t,l')
| beta_app_l:forall (l1 l1' l2:Λ), beta l1 l1'->beta (l1@l2) (l1'@l2)
| beta_app_r:forall (l1 l2 l2':Λ), beta l2 l2'->beta (l1@l2) (l1@l2').

Notation "l1 '→β' l2" := (beta l1 l2) (no associativity, at level 40).

Theorem no_beta_normal:forall (l:Λ), (forall (l':Λ), l→βl'->False)->normal l.
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros Hb.
  apply normal_var.
  apply normal_lam; apply Hl; intros l' Hll'; apply (Hb ∆(t,l')); apply beta_lam; apply Hll'.
  apply normal_app.
   apply Hl1; intros l' Hll'; apply (Hb (l'@l2)); apply beta_app_l; apply Hll'.
   apply Hl2; intros l' Hll'; apply (Hb (l1@l')); apply beta_app_r; apply Hll'.
   destruct l1 as [t i | t1 l1 | l11 l12]; [apply I | | apply I].
    apply (Hb ([t1↙l2] l1)); apply beta_red.
Qed.

Theorem normal_no_beta:forall (l:Λ), normal l->(forall (l':Λ), l→βl'->False).
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros Hn l' Hb; inversion Hb.
  inversion_clear Hn; apply (Hl H3 _ H2).
  rewrite <- H0 in *; inversion Hn; inversion H6.
  rewrite <- H0 in *; inversion Hn; apply (Hl1 H5 _ H2).
  rewrite <- H0 in *; inversion Hn; apply (Hl2 H6 _ H2).
Qed.

Theorem not_normal_beta:forall (l:Λ), (normal l->False)->{l':Λ & l→βl'}.
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros Hn.
  destruct Hn; apply normal_var.
  destruct (normal_dec l) as [Hnl | Hnl].
   destruct (Hn (normal_lam t _ Hnl)).
   destruct (Hl Hnl) as [l' Hl']; exists ∆(t,l'); apply beta_lam; apply Hl'.
  destruct (normal_dec l1) as [Hnl1 | Hnl1].
   destruct (normal_dec l2) as [Hnl2 | Hnl2].
    destruct l1 as [t i | t1 l1 | l11 l12].
     destruct (Hn (normal_app _ _ (normal_var t i) Hnl2 I)).
     exists ([t1↙l2]l1); apply beta_red.
     destruct (Hn (normal_app _ _ Hnl1 Hnl2 I)).
    destruct (Hl2 Hnl2) as [l2' Hl2']; exists (l1@l2'); apply beta_app_r; apply Hl2'.
   destruct (Hl1 Hnl1) as [l1' Hl1']; exists (l1'@l2); apply beta_app_l; apply Hl1'.
Qed.

(* Extensions of beta-reduction ------------------------------------------------------------------*)

Inductive beta_refl:Λ->Λ->Set:=
| br_beta:forall (l1 l2:Λ), l1→βl2->beta_refl l1 l2
| br_refl:forall (l:Λ), beta_refl l l.
Notation "l1 '→=β' l2" := (beta_refl l1 l2) (no associativity, at level 40).

Inductive beta_star:Λ->Λ->Set:=
| bs_beta:forall (l1 l2:Λ), l1→βl2->beta_star l1 l2
| bs_refl:forall (l:Λ), beta_star l l
| bs_tran:forall (l1 l2 l3:Λ), beta_star l1 l2->l2→βl3->beta_star l1 l3.
Notation "l1 '→*β' l2" := (beta_star l1 l2) (no associativity, at level 40).

Inductive beta_equiv:Λ->Λ->Set:=
| be_beta:forall (l1 l2:Λ), l1→βl2->beta_equiv l1 l2
| be_symm:forall (l1 l2:Λ), l1→βl2->beta_equiv l2 l1
| be_refl:forall (l:Λ), beta_equiv l l
| be_tran:forall (l1 l2 l3:Λ), beta_equiv l1 l2->l2→βl3->beta_equiv l1 l3.
Notation "l1 '≡β' l2" := (beta_equiv l1 l2) (no associativity, at level 40).

Theorem beta_star_lam:forall (l1 l2:Λ), l1→*βl2->forall (t:Τ), ∆(t,l1)→*β∆(t,l2).
Proof.
 intros l1 l2 Hbeta; induction Hbeta; intros t.
  apply bs_beta; apply beta_lam; apply b.
  apply bs_refl.
  apply bs_tran with (l2:=∆(t,l2)).
   apply IHHbeta.
   apply beta_lam; apply b.
Qed.

Theorem beta_star_app_l:forall (l1 l2:Λ), l1→*βl2->forall (l:Λ), (l1@l)→*β(l2@l).
Proof.
 intros l1 l2 Hbeta; induction Hbeta; intros l'.
  apply bs_beta; apply beta_app_l; apply b.
  apply bs_refl.
  apply bs_tran with (l2:=l2@l').
   apply IHHbeta.
   apply beta_app_l; apply b.
Qed.

Theorem beta_star_app_r:forall (l1 l2:Λ), l1→*βl2->forall (l:Λ), (l@l1)→*β(l@l2).
Proof.
 intros l1 l2 Hbeta; induction Hbeta; intros l'.
  apply bs_beta; apply beta_app_r; apply b.
  apply bs_refl.
  apply bs_tran with (l2:=l'@l2).
   apply IHHbeta.
   apply beta_app_r; apply b.
Qed.

Theorem beta_star_tran:forall (l1 l2 l3:Λ), l1→βl2->l2→*βl3->l1→*βl3.
Proof.
 intros l1 l2 l3 H12 H23; induction H23.
  apply bs_tran with (l2:=l0); [apply bs_beta; apply H12 | apply b].
  apply bs_beta; apply H12.
  apply bs_tran with (l2:=l2); [apply IHbeta_star; apply H12 | apply b].
Qed.

Theorem beta_star_tran':forall (l1 l2 l3:Λ), l1→*βl2->l2→*βl3->l1→*βl3.
Proof.
 intros l1 l2 l3 H12; induction H12; intros H23.
  apply beta_star_tran with (l2:=l2); [apply b | apply H23].
  apply H23.
  apply IHbeta_star; apply beta_star_tran with (l2:=l0); [apply b | apply H23].
Qed.

(*================================================================================================*)