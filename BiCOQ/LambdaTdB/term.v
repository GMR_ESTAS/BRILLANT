(*==================================================================================================
  Project : LambdaTdB
  Module : term.v
  We define a new form of typed lambda-calculus, which is directly inspired by the de Bruijn
  notation with namespaces introduced in BiCoq (a deep embedding of B in Coq). Namespaces were
  introduced as sorts associated to an index, and binders were binding only some namespaces - that
  is index of unbounded namespaces were merely names. Here we extend the idea to types, noting that
  with this approach there is no need to manage a typing context... and we apply it to a standard
  lambda calculus.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: Aug 2009, Md: Sep 2009
  Provided under licence CeCILL-B
  ================================================================================================*)

Require Export Bool.
Require Export Peano_dec.
Require Export Compare_dec.

(* Tools -----------------------------------------------------------------------------------------*)

Lemma eq_refl:forall (T:Type)(T_eq_dec:forall (t1 t2:T), {t1=t2}+{t1<>t2})(t:T)(S:Type)(s1 s2:S),
              (if T_eq_dec t t then s1 else s2)=s1.
Proof.
 intros T T_eq_dec t S s1 s2; destruct (T_eq_dec t t) as [_ | Ht]; [ | destruct Ht];
  apply refl_equal.
Qed.
Implicit Arguments eq_refl [T].

Lemma eq_refl_simpl:forall (T:Type)(t1 t2:T), t1=t2->
                    forall (T_eq_dec:forall (t1 t2:T), {t1=t2}+{t1<>t2})(S:Type)(s1 s2:S),
                    (if T_eq_dec t1 t2 then s1 else s2)=s1.
Proof.
 intros T t1 t2 Heq T_eq_dec S s1 s2; rewrite Heq; apply eq_refl.
Qed.
Implicit Arguments eq_refl_simpl [T t1 t2].

Lemma sum_simpl:forall (A B T:Type)(sum:A+B)(t:T), (if sum then t else t)=t.
Proof.
 intros A B T [_ | _] t; apply refl_equal.
Qed.

Lemma dec_simpl:forall (A B:Prop)(T:Type)(sum:{A}+{B})(t:T), (if sum then t else t)=t.
Proof.
 intros A B T [_ | _] t; apply refl_equal.
Qed.

Lemma bool_simpl:forall (A B:Prop)(sum:{A}+{B})(T:Type)(t1 t2:T),
                   (if (if sum then true else false) then t1 else t2)=if sum then t1 else t2.
Proof.
 intros A B [_ | _] T t1 t2; apply refl_equal.
Qed.

Lemma eq_symm:forall (T:Type)(T_eq_dec:forall (t1 t2:T), {t1=t2}+{t1<>t2})(t1 t2:T)
                     (S:Type)(s1 s2:S),
                (if T_eq_dec t1 t2 then s1 else s2)=(if T_eq_dec t2 t1 then s1 else s2).
Proof.
 intros T T_eq_dec t1 t2 S s1 s2; destruct (T_eq_dec t1 t2) as [H12 | H12].
  rewrite H12; rewrite eq_refl; apply refl_equal.
  destruct (T_eq_dec t2 t1) as [H21 | H21].
   rewrite H21 in H12; destruct H12; apply refl_equal.
   apply refl_equal.
Qed.
Implicit Arguments eq_symm [T].

Lemma left_simpl:forall (A:Prop), A->
                 forall (dec:{A}+{~A})(T:Type)(t1 t2:T), (if dec then t1 else t2)=t1.
Proof.
 intros A HA [H'A | H'A] T t1 t2; [apply refl_equal | destruct (H'A HA)].
Qed.
Implicit Arguments left_simpl [A].

Lemma right_simpl:forall (A:Prop), ~A-> 
                  forall (dec:{A}+{~A})(T:Type)(t1 t2:T), (if dec then t1 else t2)=t2.
Proof.
 intros A HA [H'A | H'A] T t1 t2; [destruct (HA H'A) | apply refl_equal].
Qed.
Implicit Arguments right_simpl [A].

Lemma sum_simpl_distr:forall (A B T T':Type)(sum:A+B)(f:T->T')(t1 t2:T),
                      (if sum then f t1 else f t2)=f(if sum then t1 else t2).
Proof.
 intros A B T T' [_ | _] f t1 t2; apply refl_equal.
Qed.
Implicit Arguments sum_simpl_distr [A B T T'].

Lemma dec_simpl_distr:forall (A B:Prop)(T T':Type)(sum:{A}+{B})(f:T->T')(t1 t2:T),
                      (if sum then f t1 else f t2)=f(if sum then t1 else t2).
Proof.
 intros A B T T' [_ | _] f t1 t2; apply refl_equal.
Qed.
Implicit Arguments dec_simpl_distr [A B T T'].

(* Types -----------------------------------------------------------------------------------------*)

Variable LSrt:Set. (* LSrts that are atomic types *)
Notation "'Φ'" := (LSrt).

Variable LSrt_eq:forall (s1 s2:Φ), {s1=s2}+{s1<>s2}. (* Decidable equality for sorts *)

Inductive LTyp:Set :=
 | atom:Φ->LTyp          (* Any sort is a type *)
 | arrow:LTyp->LTyp->LTyp (* Functional type *)
 .
Coercion atom:LSrt>->LTyp. (* Any sort is seen as a type *)
Notation "'Τ'" := (LTyp).
Notation "t1 '→' t2" := (arrow t1 t2) (right associativity, at level 60).

Lemma LTyp_eq:forall (t1 t2:Τ), {t1=t2}+{t1<>t2}.
Proof.
 decide equality; apply LSrt_eq.
Defined.

(* Indexes ---------------------------------------------------------------------------------------*)

Notation "'Ι'" := (nat). (* Use restricted to de Bruijn indexes, which are numbered from 0 *)

Definition LIdx_eq := eq_nat_dec.

Definition LIdx_le := le_lt_dec.

Lemma le_simpl:forall (m n:Ι), m<=n->forall (T:Type)(t1 t2:T), (if LIdx_le m n then t1 else t2)=t1.
Proof.
 intros m n Hle T t1 t2; destruct (LIdx_le m n) as [_ | Hmn].
  apply refl_equal.
  destruct ((Lt.lt_not_le _ _ Hmn) Hle).
Qed.
Implicit Arguments le_simpl [m n].

Lemma le_0_simpl:forall (n:Ι)(T:Type)(t1 t2:T), (if LIdx_le 0 n then t1 else t2)=t1.
Proof.
 intros n T t1 t2; rewrite (le_simpl (Le.le_O_n n)); apply refl_equal.
Qed.

Lemma gt_simpl:forall (m n:Ι), n<m->forall (T:Type)(t1 t2:T), (if LIdx_le m n then t1 else t2)=t2.
Proof.
 intros m n Hgt T t1 t2; destruct (LIdx_le m n) as [Hmn | _].
  destruct ((Lt.lt_not_le _ _ Hgt) Hmn).
  apply refl_equal.
Qed.
Implicit Arguments gt_simpl [m n].

Lemma eq_le_simpl:forall (n:Ι)(T:Type)(t1 t2:T), (if LIdx_le n n then t1 else t2)=t1.
Proof.
 intros n T t1 t2; destruct (LIdx_le n n) as [Hn | Hn];
 [apply refl_equal | destruct (Lt.lt_irrefl _ Hn)].
Qed.

Lemma eq_S_le_simpl:forall (n:Ι)(T:Type)(t1 t2:T), (if LIdx_le (S n) n then t1 else t2)=t2.
Proof.
 intros n T t1 t2; rewrite (gt_simpl (not_ge _ _ (Le.le_Sn_n n))); apply refl_equal.
Qed.

Lemma eq_le_S_simpl:forall (n:Ι)(T:Type)(t1 t2:T), (if LIdx_le n (S n) then t1 else t2)=t1.
Proof.
 intros n T t1 t2; rewrite (le_simpl (Le.le_n_Sn n)); apply refl_equal.
Qed.

Lemma eq_S_simpl:forall (n:Ι)(T:Type)(t1 t2:T), (if LIdx_eq (S n) n then t1 else t2)=t2.
Proof.
 intros n T t1 t2; destruct (LIdx_eq (S n) n).
  induction n; inversion e; apply IHn; apply H0.
  apply refl_equal.
Qed.

Lemma eq_S_simpl':forall (n:Ι)(T:Type)(t1 t2:T), (if LIdx_eq n (S n) then t1 else t2)=t2.
Proof.
 intros n T t1 t2; destruct (LIdx_eq n (S n)).
  induction n; inversion e; apply IHn; apply H0.
  apply refl_equal.
Qed.

Lemma eq_S_S_simpl:forall (n m:Ι)(T:Type)(t1 t2:T),
                   (if LIdx_eq (S m) (S n) then t1 else t2)=
                   (if LIdx_eq m n then t1 else t2).
Proof.
 intros n m T t1 t2; destruct (LIdx_eq m n).
  rewrite e; rewrite eq_refl; apply refl_equal.
  destruct (LIdx_eq (S m) (S n)).
   inversion e; destruct (n0 H0).
   apply refl_equal.
Qed.

Theorem le_S_S_simpl:forall (n m:Ι)(T:Type)(t1 t2:T),
                     (if LIdx_le (S m) (S n) then t1 else t2)=(if LIdx_le m n then t1 else t2).
Proof.
 intros n m T t1 t2; destruct (LIdx_le m n); destruct (LIdx_le (S m) (S n)).
  apply refl_equal.
  destruct ((Lt.lt_not_le _ _ (Lt.lt_S_n _ _ l0)) l).
  destruct ((Lt.lt_not_le _ _ l) (Le.le_S_n _ _ l0)).
  apply refl_equal.
Qed.

Theorem le_eq_S_simpl:forall (m n:Ι), n<=m->
                      forall (T:Type)(t1 t2:T), (if LIdx_eq n (S m) then t1 else t2)=t2.
Proof.
 intros m n Hle T t1 t2; destruct (LIdx_eq n (S m)) as [HnSm | HnSm].
  rewrite HnSm in Hle; destruct (Le.le_Sn_n _ Hle).
  apply refl_equal.
Qed.
Implicit Arguments le_eq_S_simpl [m n].

(* Maps ------------------------------------------------------------------------------------------*)
(* Maps associates to any type a lambda-height. Because we WANT to have a very fine management of
   the lambda-height parameter, and because we have in fact a context per type, we need this form
   of entity. Note however that maps have NO computational interest - it is pretty easy to replace
   lifting with maps by lifting from lambda-height 0, without consequences over computed results. *)

Definition LMaps:Set := Τ->Ι.
Notation "'Μ'" := (LMaps).

Definition root:Μ := fun (t:Τ) => 0.
Notation "'μ'" := (root).

Definition minc(m:Μ)(t:Τ) := fun (t':Τ) => if LTyp_eq t t' then S (m t') else m t'.
Notation "m '⊕' t" := (minc m t)  (left associativity, at level 50).

Lemma minc_eq:forall (t:Τ)(m:Μ), (m⊕t) t=S (m t).
Proof.
 intros t m; unfold minc; rewrite eq_refl; apply refl_equal.
Qed.

Lemma minc_diff:forall (t1 t2:Τ), t1<>t2->forall (m:Μ), (m⊕t1) t2=m t2.
Proof.
 intros t1 t2 Hdiff m; unfold minc; rewrite (right_simpl Hdiff); apply refl_equal.
Qed.
Implicit Arguments minc_diff [t1 t2].

Lemma minc_comm:forall (m:Μ)(t1 t2 t3:Τ), (m⊕t2⊕t1) t3=(m⊕t1⊕t2) t3.
Proof.
 intros m t1 t2 t3; unfold minc; destruct (LTyp_eq t1 t3) as [H13 | H13];
  destruct (LTyp_eq t2 t3) as [H23 | H23]; apply refl_equal.
Qed.

Lemma minc_le:forall (m1 m2:Μ)(t:Τ), m1 t<=m2 t->forall (t':Τ), (m1⊕t') t<=(m2⊕t') t.
Proof.
 intros m1 m2 t Hm t'; unfold minc; destruct (LTyp_eq t' t) as [_ | _];
 [apply Le.le_n_S | ]; apply Hm.
Qed.
Implicit Arguments minc_le [m1 m2 t].

Lemma minc_eq_eq:forall (m1 m2:Μ)(t:Τ), m1 t=m2 t->forall (t':Τ), (m1⊕t') t=(m2⊕t') t.
Proof.
 intros m1 m2 t Hm t'; unfold minc; destruct (LTyp_eq t' t) as [_ | _]; rewrite Hm;
  apply refl_equal.
Qed.
Implicit Arguments minc_eq_eq [m1 m2 t].

Axiom minc_ext:forall (m1 m2:Μ), (forall (t:Τ), m1 t=m2 t)->m1=m2.
Implicit Arguments minc_ext [m1 m2].
(* Nota: this is the only axiom in this development, and as far as we know it does not introduce any
   inconsistency in Coq. We use this axiom for slightly simpler development. In fact, what we need
   is a set of congruence results such as (forall (t:Τ), m1 t=m2 t)->elim m1 ...=elim m2 ..., etc.
   which are all trivial to prove. Yet this introduce numerous lemmas whose names have to be used in
   proofs, etc.                                                                                   *)

Definition dangling(m:Μ)(t ti:Τ)(i:Ι):bool :=
 if LTyp_eq t ti then (if LIdx_le (m t) i then true else false) else false.
Notation "'Ⅾ(' m ',' t ',' ti ',' i ')'" := (dangling m t ti i).

Lemma dangling_simpl:forall (t:Τ)(m:Μ)(i:Ι)(T:Type)(t1 t2:T),
                     (if Ⅾ(m,t,t,i) then t1 else t2)=(if LIdx_le (m t) i then t1 else t2).
Proof.
 intros t m i T t1 t2; unfold dangling; rewrite eq_refl; rewrite bool_simpl; apply refl_equal.
Qed.

(* Tactics ---------------------------------------------------------------------------------------*)

Ltac my_simpl :=
 repeat (rewrite eq_refl in * ||
         rewrite dec_simpl in * ||
         rewrite sum_simpl in * ||
         rewrite bool_simpl in * ||
         rewrite le_0_simpl in * ||
         rewrite eq_le_simpl in * ||
         rewrite eq_le_S_simpl in * ||
         rewrite eq_S_le_simpl in * ||
         rewrite dangling_simpl in * ||
         rewrite eq_S_simpl in * ||
         rewrite eq_S_simpl' in * ||
         rewrite eq_S_S_simpl in * ||
         rewrite le_S_S_simpl in * ||
         rewrite sum_simpl_distr in * ||
         rewrite dec_simpl_distr in * ||
         my_simpl_1)
with my_simpl_1 :=
 match goal with
 | H:_=_ |- _ => rewrite (eq_refl_simpl H) in * ||
                 rewrite (eq_refl_simpl (sym_equal H)) in *
 | H:(S _)<=(S _) |- _ => (rewrite (le_simpl (Le.le_S_n _ _ H)) in * ||
                           rewrite (le_simpl H) in *)
 | H:_<=_ |- _ => (rewrite (le_simpl H) in * ||
                   rewrite (le_simpl (Le.le_n_S _ _ H)) in * ||
                   rewrite (le_simpl (le_S _ _ H)) in * ||
                   rewrite (le_eq_S_simpl H) in *)
 | H1:_<=_, H2:_<=_ |- _ => (rewrite (le_simpl (Le.le_trans _ _ _ H1 H2)) in * ||
                             rewrite (le_simpl (Le.le_trans _ _ _ H2 H1)) in *)
 | H1:_<=_, H2:_<_ |- _ => (rewrite (gt_simpl (Lt.le_lt_trans _ _ _ H1 H2)) in * ||
                             rewrite (gt_simpl (Lt.lt_le_trans _ _ _ H2 H1)) in *)
 | H1:_<_, H2:_<_ |- _ => (rewrite (gt_simpl (Lt.lt_trans _ _ _ H1 H2)) in * ||
                           rewrite (gt_simpl (Lt.lt_trans _ _ _ H2 H1)) in *)
 | H:_<_ |- _ => (rewrite (gt_simpl H) in * ||
                  rewrite (gt_simpl (Lt.lt_n_S _ _ H)) in * ||
                  rewrite (gt_simpl (Lt.lt_S _ _ H)) in * ||
                  rewrite (le_simpl H) in *)
 | H:_<>_ |- _ => (rewrite (right_simpl (sym_not_eq H)) in * || rewrite (minc_diff H) in * ||
                   rewrite (minc_diff (sym_not_eq H)) in *)
 | H:~_ |- _ => rewrite (right_simpl H) in *
 | H:_ |- _ => rewrite (left_simpl H) in *
 end
.

(* Terms -----------------------------------------------------------------------------------------*)
(* Note that this is a first-order lambda-calculus, in the sense that types are not terms         *)

Inductive LLam:Set :=
 | var:Τ->Ι->LLam             (* Variables, that is a type and an index *)
 | lam:Τ->LLam->LLam       (* Abstraction *)
 | app:LLam->LLam->LLam (* Application *)
 .
Notation "'Λ'" := (LLam).
Notation "'√(' t ',' i ')'" := (var t i).
Notation "'∆(' t ',' l ')'" := (lam t l).
Notation "l1 '@' l2" := (app l1 l2) (left associativity, at level 50).

Lemma LLam_eq:forall (l1 l2:Λ), {l1=l2}+{l1<>l2}.
Proof.
 decide equality; [apply eq_nat_dec | apply LTyp_eq | apply LTyp_eq].
Defined.
(* Nota: Do we need that? Is such a theory possible on terms without a decidable equality?        *)

(* Typing ----------------------------------------------------------------------------------------*)
(* We describe how to check a type judgment and how to compute a type, before showing equivalence
   between the two approaches. Note that because we represent application, we can build ill-typed
   term, therefore there is not always a type...                                                  *)

Inductive IsofType:Λ->Τ->Prop:=
 | IoTvar:forall (t:Τ)(i:Ι), IsofType (√(t,i)) t
 | IoTlam:forall (t t':Τ)(l:Λ), IsofType l t->IsofType ∆(t',l) (t'→t)
 | IoTapp:forall (t1 t2:Τ)(l1 l2:Λ), IsofType l1 t1->IsofType l2 (t1→t2)->IsofType (l2@l1) t2
.
Notation "'⊢' l '∶' t" := (IsofType l t) (no associativity, at level 40).

Fixpoint typeof(l:Λ):option Τ:=
 match l with
 | √(t',_) => Some t'
 | ∆(t',l) => match typeof l with None => None | Some t => Some (t'→t) end
 | l1@l2 => match typeof l1 with
            | Some (t2→t1) => match typeof l2 with
                              | Some t'2 => if LTyp_eq t2 t'2 then Some t1 else None
                              | _ => None
                              end
            | _ => None
            end
 end.

Theorem typeof_complete:forall (l:Λ)(t:Τ), ⊢l∶t->typeof l=Some t.
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; intros t' Ht'; simpl.
  inversion_clear Ht'; apply refl_equal.
  inversion_clear Ht'; rewrite (Hl _ H); apply refl_equal.
  inversion_clear Ht'.
   rewrite (Hl1 _ H0); rewrite (Hl2 _ H).
   rewrite eq_refl; apply refl_equal.
Qed.

Theorem typeof_correct:forall (l:Λ), match typeof l with None => True | Some t => ⊢l∶t end.
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl.
  apply IoTvar.
  destruct (typeof l) as [t' | ]; [ | apply I].
   apply IoTlam; apply Hl.
  destruct (typeof l1) as [t1 | ]; [ | apply I].
   destruct (typeof l2) as [t2 | ]; [ | destruct t1; apply I].
    destruct t1 as [ | t2' t1]; [apply I | ].
     destruct (LTyp_eq t2' t2) as [Heq | _]; [ | apply I].
      apply IoTapp with (t1:=t2).
       apply Hl2.
       rewrite Heq in Hl1; apply Hl1.
Qed.

Theorem typeof_fail:forall (l:Λ),
                    match typeof l with None => forall (t:Τ), ~⊢l∶t | Some _ => True end.
Proof.
 intros l; case_eq (typeof l).
  intros _ _; apply I.
  intros Hl t Ht; rewrite (typeof_complete _ _ Ht) in Hl; inversion Hl.
Qed.

Theorem not_IsofType:forall (l:Λ), (forall (t:Τ), ~⊢l∶t)->typeof l=None.
Proof.
 intros l Hl; case_eq (typeof l).
  intros t Ht; destruct (Hl t); generalize (typeof_correct l); rewrite Ht; intros H; apply H.
  intros _; apply refl_equal.
Qed.

(* Lifting ---------------------------------------------------------------------------------------*)
(* Lifting is the standard operator that increments dangling indexes. In our case however, we
   generalize the lambda-height parameter to make it first-class, and furthermore we add a type in
   which lifting operates. That is, an index is incremented iff it is in the right type and if it
   is greater or equal to the current lambda-height. Note also that crossing a binder only
   increases the lambda-height if the bound type is the lifted one. Consistently with what is
   required for elimination, defined later, we also generalize the lambda-height parameter to be a
   map. This is computationnally useless, but having a common map parameter for all our functions
   is very interesting when expressing some complex theorems.                                     *)

Fixpoint lift(m:Μ)(t:Τ)(l:Λ){struct l}:Λ :=
 match l with
 | √(t',i') => √(t', if Ⅾ(m,t,t',i') then S i' else i')
 | ∆(t',l') => ∆(t',lift (m⊕t') t l')
 | l1@l2 => (lift m t l1)@(lift m t l2)
 end.
Notation "'↑(' m ',' t ',' le ')'" := (lift m t le).

Theorem typeof_lift_:forall (l:Λ)(t:Τ)(m:Μ), typeof ↑(m,t,l)=typeof l.
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl.
  intros _ _; apply refl_equal.
  intros t' m; rewrite Hl; apply refl_equal.
  intros t m; rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.
(* Nota: this theorem justifies why types are never part of the induction in proofs; the association
   of a type to any variable indeed ensure that type will be preserved whatever.                  *)

Theorem IsofType_lift_:forall (l:Λ)(tl t:Τ)(m:Μ), ⊢l∶t->⊢↑(m,tl,l)∶t.
Proof.
 intros l tl t m Hlt.
 generalize (typeof_correct (lift m tl l)); intros H'lt.
 generalize (typeof_complete _ _ Hlt).
 rewrite <- (typeof_lift_ l tl m).
 intros Hlift; rewrite Hlift in H'lt; apply H'lt.
Qed.

(* Binder Elimination ----------------------------------------------------------------------------*)
(* Elimination is a form of higher-order substitution. It is the operation that solves the standard
   equation (λx.t1)t2={x:=t2}t1 in our calculus; note however that it is not a classical
   substitution, because dangling indexes need to be adapted.  But we have a problem here. Consider
   {ti,i:=L1}d(λt)L2, that is replacing index (ti,i) at lambda-height d by the term L1 in (λt)L2.
   Crossing the binder increases the lambda-height of type t - this has influence on how we lift
   L1. So either we manage a "map" of lambda-height (one per type), either we just lift L1 at
   lambda-height 0. We adopt here the former approach.                                            *)

Fixpoint elim(m:Μ)(t:Τ)(le l:Λ){struct l}:Λ:=
 match l with
 | √(t',i') => if Ⅾ(m,t,t',i') then (if LIdx_eq (m t) i' then le else √(t',pred i')) else √(t',i')
 | ∆(t',l') => ∆(t',elim (m⊕t') t ↑(m,t',le) l')
 | l1@l2 => (elim m t le l1)@(elim m t le l2)
 end.
Notation "'[' m ',' t '↙' le ']'" := (elim m t le).
Notation "'[' t '↙' le ']'" := (elim μ t le).

Theorem typeof_elim_:forall (te:Τ)(l le:Λ)(m:Μ), ⊢le∶te->typeof ([m,te↙le]l)=typeof l.
Proof.
 intros te; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; intros le me Hle; simpl; unfold dangling.
  destruct (LTyp_eq te t) as [Htet | _]; [rewrite Htet in * | apply refl_equal].
   destruct (LIdx_le (me t) i) as [Hmeti | _]; [ | apply refl_equal].
    destruct (LIdx_eq (me t) i) as [H'meti | _]; [ | apply refl_equal].
     apply typeof_complete; apply Hle.
  simpl; rewrite Hl; [apply refl_equal | apply IsofType_lift_; apply Hle].
  rewrite Hl1; [rewrite Hl2; [apply refl_equal | apply Hle] | apply Hle].
Qed.
(* Nota: if le is not of type te, then [te↙le]l is ill-formed, except if the eliminated dangling
   index (te,0) does not appear in m.                                                             *)

Theorem elim_lift_:forall (te:Τ)(l le:Λ)(me:Μ), [me,te↙le]↑(me,te,l)=l.
Proof.
 intros te; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros le me.
  unfold dangling; destruct (LTyp_eq te t) as [Htet | Htet]; [rewrite Htet in * | apply refl_equal].
   destruct (LIdx_le (me t) i) as [Hmeti | Hmeti]; my_simpl; apply refl_equal.
  rewrite Hl; apply refl_equal.
  rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.

Definition headred(l:Λ) := match l with ∆(t1,l1)@l2 => [t1↙l2]l1 | _ => l end.
(* Nota: this is a beta-reduction limited to a redex being the root of the term.                  *)

Theorem typeof_headred:forall (t:Τ)(l:Λ), ⊢l∶t->typeof (headred l)=typeof l.
Proof.
 intros t'; intros [t i | t l | [t1 i1 | t1 l1 | l11 l12] l2]; intros Hl; simpl;
  try (apply refl_equal).
  inversion_clear Hl; inversion_clear H0; rewrite (typeof_elim_ t0 l1 _ μ H);
   rewrite (typeof_complete _ _ H1); rewrite (typeof_complete _ _ H); my_simpl; apply refl_equal.
Qed.

Theorem IsofType_headred:forall (t:Τ)(l:Λ), ⊢l∶t->⊢headred l∶t.
Proof.
 intros t l Hl; generalize (typeof_correct (headred l)); rewrite (typeof_headred t l Hl);
  rewrite (typeof_complete _ _ Hl); intros H'l; apply H'l.
Qed.

(* Binding ---------------------------------------------------------------------------------------*)
(* Binding is the operation that transform a term in order to prepare it for a head binder
   insertion: it captures a given index and increments the others. As previously, while do not
   need a lambda-height parameter, we introduce one, and we further extend it to a map. In addition
   we also provide a for of user-friendly notation masking de Bruijn computations.                *)

Fixpoint bind(m:Μ)(t:Τ)(i:Ι)(l:Λ){struct l}:Λ:=
 match l with
 | √(t',i') => √(t',if Ⅾ(m,t,t',i') then (if LIdx_eq i i' then (m t) else S i') else i')
 | ∆(t',l') => ∆(t',bind (m⊕t') t (if Ⅾ(m,t',t,i) then S i else i) l')
 | l1@l2 => (bind m t i l1)@(bind m t i l2)
 end.
Notation "'‹' i '∶' t '↦' l '›'" := ∆(t,bind μ t i l).

Theorem typeof_bind_:forall (t:Τ)(l:Λ)(m:Μ)(i:Ι), typeof (bind m t i l)=typeof l.
Proof.
 intros ta; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; intros ma ia; simpl.
  apply refl_equal.
  destruct (LTyp_eq ta t); rewrite Hl; apply refl_equal.
  rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.

Theorem typeof_lambda_:forall (l:Λ)(t tl:Τ)(i:Ι),
                       IsofType l tl->IsofType ‹i∶t↦l› (t→tl).
Proof.
 intros l t tl i Hl; apply IoTlam; generalize (typeof_complete _ _ Hl).
 rewrite <- (typeof_bind_ t l μ i).
 intros H'l; generalize (typeof_correct (bind μ t i l)); rewrite H'l.
 intros Ht; apply Ht.
Qed.

(* Substitution ----------------------------------------------------------------------------------*)
(* We present standard substitution without capture as a primitive operation for efficiency reasons
   but also as a composition of abstraction and reduction. *)

Fixpoint subst(m:Μ)(t:Τ)(i:Ι)(ls l:Λ){struct l}:Λ :=
 match l with
 | √(t',i') => if Ⅾ(m,t,t',i') then (if LIdx_eq i i' then ls else l) else l
 | ∆(t',l') => ∆(t',subst (m⊕t') t (if Ⅾ(m,t',t,i) then S i else i) ↑(m,t',ls) l')
 | l1@l2 => (subst m t i ls l1)@(subst m t i ls l2)
 end.
Notation "'[' t ',' i '≔' ls ']'" := (subst μ t i ls).

Theorem subst_not_primitive_:forall (t:Τ)(l ls:Λ)(m:Μ)(i:Ι),
                             [m,t↙ls](bind m t i l)=subst m t i ls l.
Proof.
 intros ts; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; intros ls ms is.
  simpl; unfold dangling; destruct (LTyp_eq ts t) as [Htst | _];
  [rewrite Htst in * | apply refl_equal].
   destruct (LIdx_le (ms t) i) as [Hmsti | Hmsti]; [ | my_simpl; apply refl_equal].
    destruct (LIdx_eq is i) as [Hisi | Hisi]; my_simpl; apply refl_equal.
  simpl; rewrite Hl; apply refl_equal.
  simpl; rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.

Theorem subst_not_primitive:forall (l ls:Λ)(t:Τ)(d i:Ι),
                            headred (‹i∶t↦l›@ls)=[t,i≔ls] l.
Proof.
 intros l ls t d i; simpl; apply subst_not_primitive_.
Qed.

Theorem typeof_subst_:forall (t:Τ)(l ls:Λ)(m:Μ)(i:Ι), ⊢ls∶t->typeof (subst m t i ls l)=typeof l.
Proof.
 intros ts l ls ms is Hls; rewrite <- subst_not_primitive_; rewrite typeof_elim_ with (1:=Hls);
  apply typeof_bind_.
Qed.

Theorem subst_neutral_:forall (t:Τ)(l:Λ)(i:Ι)(m:Μ), subst m t i √(t,i) l=l.
Proof.
 intros ts; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; intros is ms; simpl.
  unfold dangling; destruct (LTyp_eq ts t) as [Htst | _]; [rewrite Htst in * | apply refl_equal].
   destruct (LIdx_le (ms t) i) as [Hmsti | _]; [ | apply refl_equal].
    destruct (LIdx_eq is i) as [Hisi | _]; [rewrite Hisi in * | ]; apply refl_equal.
  rewrite Hl; apply refl_equal.
  rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.

(* Grafting --------------------------------------------------------------------------------------*)
(* Grafting is a form of first-order substitution. It knows about abstractions because crossing one
   he will increments the free variable it is looking for; yet it will not lift the term that has
   to be inserted, allowing captures. This is the operation of rewriting, provided we do not try
   to replace bound variables. *)

Fixpoint graft(m:Μ)(t:Τ)(i:Ι)(lg l:Λ){struct l}:Λ:=
 match l with
 | √(t',i') => if Ⅾ(m,t,t',i') then (if LIdx_eq i i' then lg else l) else l
 | ∆(t',l') => ∆(t', graft (m⊕t') t (if Ⅾ(m,t',t,i) then S i else i) lg l')
 | l1@l2 => (graft m t i lg l1)@(graft m t i lg l2)
 end.
Notation "'[' m ',' t ',' i '⊲' lg ']'" := (graft m t i lg).
Notation "'[' t ',' i '⊲' lg ']'" := (graft μ t i lg).

Theorem typeof_graft_:forall (lg:Λ)(tg:Τ), ⊢lg∶tg->
                      forall (l:Λ)(m:Μ)(i:Ι), typeof ([m,tg,i⊲lg]l)=typeof l.
Proof.
 intros lg tg Htg; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; intros mg ig; simpl;
  unfold dangling.
  destruct (LTyp_eq tg t) as [Htgt | _]; [rewrite Htgt in *; my_simpl | apply refl_equal].
   destruct (LIdx_le (mg t) i) as [Hmgi | _]; [ | apply refl_equal].
    destruct (LIdx_eq ig i) as [Higi | _]; [ | apply refl_equal].
     apply typeof_complete; apply Htg.
 rewrite Hl; apply refl_equal.
 rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.

Theorem typeof_graft:forall (lg:Λ)(tg:Τ), ⊢lg∶tg->forall (l:Λ)(i:Ι), typeof ([tg,i⊲lg] l)=typeof l.
Proof.
 intros lg tg Htg l i; apply typeof_graft_; apply Htg.
Qed.

(* Freeness --------------------------------------------------------------------------------------*)

Fixpoint free(m:Μ)(t:Τ)(i:Ι)(l:Λ){struct l}:bool :=
 match l with
 | √(t',i') => if Ⅾ(m,t,t',i') then (if LIdx_eq i i' then true else false) else false
 | ∆(t',l') => free (m⊕t') t (if Ⅾ(m,t',t,i) then S i else i) l'
 | l1@l2 => (free m t i l1) || (free m t i l2)
 end.

Theorem free_bind_:forall (t:Τ)(l:Λ)(m:Μ)(i:Ι), free m t i l=false->bind m t i l=↑(m,t,l).
Proof.
 intros ta; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros ma ia Hfree.
  destruct Ⅾ(ma,ta,t,i); [ | apply refl_equal].
   destruct (LIdx_eq ia i) as [Hiai | Hiai]; [inversion Hfree | apply refl_equal].
  destruct (LTyp_eq ta t) as [Htat | Htat]; rewrite (Hl _ _ Hfree); apply refl_equal.
  destruct (orb_false_elim _ _ Hfree) as [Hfree1 Hfree2]; rewrite (Hl1 _ _ Hfree1);
   rewrite (Hl2 _ _ Hfree2); apply refl_equal.
Qed.

Theorem free_subst_:forall (ts:Τ)(l ls:Λ)(ms:Μ)(is:Ι), free ms ts is l=false->subst ms ts is ls l=l.
Proof.
 intros ts l ls ms is Hfree; rewrite <- subst_not_primitive_; rewrite (free_bind_ _ _ _ _ Hfree).
 apply elim_lift_.
Qed.

Theorem free_minc_:forall (t t':Τ)(l:Λ)(m:Μ)(i:Ι),
                   free (m⊕t') t (if Ⅾ(m,t,t',i) then S i else i) ↑(m,t',l)=free m t i l.
Proof.
 intros t1 t2; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros m ii.
  unfold dangling; destruct (LTyp_eq t1 t) as [Ht1t | Ht1t]; [rewrite Ht1t in * | apply refl_equal];
   my_simpl.
   destruct (LTyp_eq t2 t) as [Ht2t | Ht2t]; [rewrite Ht2t in * | ]; my_simpl;
   [ | apply refl_equal].
    rewrite minc_eq; destruct (LIdx_le (m t) i) as [Hmti | Hmti]; my_simpl; [ | apply refl_equal].
     destruct (LIdx_le (m t) ii) as [Hmtii | Hmtii]; my_simpl; [apply refl_equal | ].
      destruct (LIdx_eq ii i) as [Hiii | Hiii]; [rewrite <- Hiii in * | ]; my_simpl.
       destruct (Lt.lt_irrefl _ (Lt.lt_le_trans _ _ _ Hmtii Hmti)).
       destruct (LIdx_eq ii (S i)) as [HiiSi | HiiSi]; [rewrite HiiSi in * | ]; my_simpl.
        destruct (Lt.lt_irrefl _ (Lt.lt_S _ _ (Lt.lt_le_trans _ _ _ Hmtii Hmti))).
        apply refl_equal.
  rewrite (minc_ext (minc_comm m t t2)); rewrite <- Hl; unfold dangling;
   destruct (LTyp_eq t t1) as [Htt1 | Htt1]; [rewrite Htt1 in * | ]; my_simpl.
   destruct (LTyp_eq t1 t2) as [Ht1t2 | Ht1t2]; [rewrite Ht1t2 in * | ]; my_simpl;
    apply refl_equal.
   apply refl_equal.
  rewrite Hl1; rewrite Hl2; apply refl_equal.
Qed.

Theorem bound_not_free_:forall (t:Τ)(l:Λ)(i:Ι)(m:Μ), i<m t->free m t i l=false.
Proof.
 intros tf; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros ii m Hiimt.
  unfold dangling; destruct (LTyp_eq tf t) as [Htft | Htft]; [rewrite Htft in * | apply refl_equal].
   destruct (LIdx_eq ii i) as [Hiii | Hiii]; [rewrite Hiii in * | ]; my_simpl; apply refl_equal.
  rewrite Hl.
   apply refl_equal.
   unfold dangling; my_simpl; destruct (LTyp_eq t tf) as [Httf | Httf]; [rewrite Httf in * | ].
    my_simpl; rewrite minc_eq; apply (Lt.lt_S _ _ Hiimt).
    rewrite (minc_diff Httf); apply Hiimt.
  rewrite (Hl1 _ _ Hiimt); rewrite (Hl2 _ _ Hiimt); apply refl_equal.
Qed.

(* Validity --------------------------------------------------------------------------------------*)
(* Validity is a technical tool; it indicates if a term is well-formed with regard to a map, that *)
(* is intuitively for example when dealing with elim t m le l it checks that le has been lifted   *)
(* sufficiently to be compatible with m.                                                          *)

Fixpoint valid(m1 m2:Μ)(l:Λ):bool :=
 match l with
 | √(t',i') => if Ⅾ(m1,t',t',i') then (if Ⅾ(m2,t',t',i') then true else false) else true
 | ∆(t',l') => valid (m1⊕t') (m2⊕t') l'
 | l1@l2 => (valid m1 m2 l1) && (valid m1 m2 l2)
 end.

Theorem valid_lift_:forall (tl:Τ)(l:Λ)(m1 m2:Μ), m1 tl<=m2 tl->
                    valid m1 m2 l=true->valid (m1⊕tl) (m2⊕tl) ↑(m2,tl,l)=true.
Proof.
 intros tl; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros m1 m2 Htl Hval.
  unfold dangling; my_simpl; destruct (LIdx_le (m2 t) i) as [Hm2ti | Hm2ti]; my_simpl.
   destruct (LTyp_eq tl t) as [Htlt | Htlt]; [rewrite Htlt in * | ]; my_simpl.
    rewrite minc_eq; rewrite (le_simpl (Le.le_n_S _ _ (Le.le_trans _ _ _ Htl Hm2ti)));
     rewrite minc_eq; my_simpl; apply refl_equal.
    apply refl_equal.
   destruct (LIdx_le (m1 t) i) as [Hm1ti | Hm1ti]; my_simpl.
    inversion Hval.
    destruct (LTyp_eq tl t) as [Htlt | Htlt]; [rewrite Htlt in * | ]; my_simpl.
     rewrite minc_eq; my_simpl; destruct (LIdx_le (S (m1 t)) i) as [HSm1ti | HSm1ti];
      my_simpl; apply refl_equal.
     apply refl_equal.
  rewrite (minc_ext (minc_comm m1 t tl)); rewrite (minc_ext (minc_comm m2 t tl));
   apply Hl; [apply minc_le; apply Htl | apply Hval].
  destruct (andb_prop _ _ Hval) as [Hval1 Hval2]; rewrite (Hl1 _ _ Htl Hval1);
   rewrite (Hl2 _ _ Htl Hval2); apply refl_equal.
Qed.
(* Nota: intuitively, valid m1 m2 l is used only when m1<=m2 for any t. If this is not the case,
   then valid returns true... at least for the considered types, that is for the variables appearing
   free in the term passed as a parameter. *)

(* Theorem valid_lift'_:forall (tl:Τ)(l:Λ)(m1 m2:Μ), m1 tl<=m2 tl->
                        valid m1 m2 l=valid (minc tl m1) (minc tl m2) (lift tl m1 l).             *)

Theorem lift_valid_:forall (tl:Τ)(l:Λ)(m1 m2:Μ),
                    m1 tl<=m2 tl->valid m1 m2 l=true->lift m1 tl l=lift m2 tl l.
Proof.
 intros tl; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros m1 m2 Htl Hval.
  unfold dangling in *; destruct (LTyp_eq tl t) as [Htlt | Htlt]; [rewrite Htlt in * | ].
   my_simpl; destruct (LIdx_le (m2 t) i) as [Hm2ti | Hm2ti]; my_simpl.
    apply refl_equal.
    destruct (LIdx_le (m1 t) i) as [Hm1ti | Hm1ti]; my_simpl.
     inversion Hval.
     apply refl_equal.
   apply refl_equal.
  rewrite (Hl (minc m1 t) (minc m2 t)).
   apply refl_equal.
   apply minc_le; apply Htl.
   apply Hval.
  destruct (andb_prop _ _ Hval) as [Hval1 Hval2]; rewrite (Hl1 _ _ Htl Hval1);
   rewrite (Hl2 _ _ Htl Hval2); apply refl_equal.
Qed.

Theorem valid_free_:forall (l:Λ)(m1 m2:Μ),
                    valid m1 m2 l=true->forall (t:Τ)(i:Ι), i<m2 t->free m1 t i l=false.
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros m1 m2 Hval tf ii Hm2.
  unfold dangling in *; destruct (LTyp_eq tf t) as [Htft | Htft]; [rewrite Htft in * | ];
   my_simpl.
   destruct (LIdx_le (m1 t) i) as [Hm1ti | Hm1ti]; my_simpl.
    destruct (LIdx_eq ii i) as [Hiii | Hiii]; [rewrite Hiii in * | ]; my_simpl.
     inversion Hval.
     apply refl_equal.
    apply refl_equal.
   apply refl_equal; rewrite (Hl _ _ Hval).
  apply (Hl _ _ Hval); unfold dangling; destruct (LTyp_eq t tf) as [Httf | Httf];
  [rewrite Httf in * | ].
   rewrite minc_eq; destruct (LIdx_le (m1 tf) ii) as [Hm1tfii | Hm1tfii].
    apply (Lt.lt_n_S _ _ Hm2).
    apply (Lt.lt_S _ _ Hm2).
   rewrite (minc_diff Httf); apply Hm2.
  destruct (andb_prop _ _ Hval) as [Hval1 Hval2]; rewrite (Hl1 _ _ Hval1 _ _ Hm2);
   rewrite (Hl2 _ _ Hval2 _ _ Hm2); apply refl_equal.
Qed.

Theorem valid_is_lift_:forall (t:Τ)(l:Λ)(m1 m2:Μ), m1 t<=m2 t->valid m1 (minc m2 t) l=true->
                       {l':Λ | l=lift m2 t l' /\ valid m1 m2 l'=true}.
Proof.
 intros t'; induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; unfold dangling;
  intros m1 m2 Ht Hval.
  my_simpl; destruct (LTyp_eq t t') as [Htt' | Htt']; [rewrite Htt' in * | ].
   rewrite minc_eq in *; destruct (LIdx_le (m1 t') i) as [Hm1t'i | Hm1t'i].
    destruct (LIdx_le (S (m2 t')) i) as [HSm2t'i | HSm2t'i].
     destruct i as [| i].
      exists √(t',0); inversion HSm2t'i.
      exists √(t',i); simpl; unfold dangling; my_simpl; split; apply refl_equal.
     inversion Hval.
    exists √(t',i); simpl; unfold dangling; my_simpl; split; apply refl_equal.
   exists √(t,i); simpl; unfold dangling; my_simpl; split; [apply refl_equal | apply Hval].
  rewrite (minc_ext (minc_comm m2 t t')) in Hval;
   destruct (Hl _ _ ((minc_le Ht) t) Hval) as [l' [Hl' H'l']]; exists ∆(t,l'); simpl; split.
   rewrite Hl'; apply refl_equal.
   apply H'l'.
  destruct (andb_prop _ _ Hval) as [Hval1 Hval2]; destruct (Hl1 _ _ Ht Hval1) as [l'1 [Hl'1 H'l'1]];
   destruct (Hl2 _ _ Ht Hval2) as [l'2 [Hl'2 H'l'2]]; exists (l'1@l'2); simpl; split.
   rewrite Hl'1; rewrite Hl'2; apply refl_equal.
   rewrite H'l'1; rewrite H'l'2; apply refl_equal.
Qed.

Theorem free_valid_:forall (l:Λ)(m1 m2:Μ), (forall (t:Τ), m1 t<=m2 t)->
                    (forall (t:Τ)(i:Ι), i<m2 t->free m1 t i l=false)->valid m1 m2 l=true.
Proof.
 induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros m1 m2 Hm Hfree.
  generalize (Hfree t i); clear Hfree; intros Hfree; unfold dangling in *; my_simpl;
   destruct (LIdx_le (m1 t) i) as [Hm1ti | Hm1ti].
   destruct (LIdx_le (m2 t) i) as [Hm2ti | Hm2ti].
    apply refl_equal.
    apply sym_equal; apply (Hfree Hm2ti).
   apply refl_equal.
 apply Hl; [intros t'; apply minc_le; apply Hm | ]; intros t' i' Hm2t'i'; generalize (Hfree t');
  clear Hfree; intros Hfree; destruct (LTyp_eq t t') as [Htt' | Htt']; [rewrite Htt' in * | ].
  rewrite minc_eq in Hm2t'i'; destruct i' as [| i'].
   apply bound_not_free_; rewrite minc_eq; apply Lt.lt_O_Sn.
   simpl in *; generalize (Lt.lt_S_n _ _ Hm2t'i'); clear Hm2t'i'; intros Hm2t'i';
    case_eq (Ⅾ(m1,t',t',i')); intros Hdang.
    generalize (Hfree _ Hm2t'i'); rewrite Hdang; intros Hf; apply Hf.
    unfold dangling in Hdang; my_simpl; destruct (LIdx_le (m1 t') i') as [Hm1t'i' | Hm1t'i'].
     inversion Hdang.
     apply bound_not_free_; rewrite minc_eq; apply Lt.lt_n_S; apply Hm1t'i'.
  rewrite (minc_diff Htt') in Hm2t'i'; generalize (Hfree _ Hm2t'i'); unfold dangling; my_simpl;
   intros Hf; apply Hf.
 rewrite Hl1.
  rewrite Hl2.
   apply refl_equal.
   apply Hm.
   intros t' i' Hm2t'i'; destruct (orb_false_elim _ _ (Hfree _ _ Hm2t'i')); apply H0.
  apply Hm.
  intros t' i' Hm2t'i'; destruct (orb_false_elim _ _ (Hfree _ _ Hm2t'i')); apply H.
Qed.

Theorem lift_valid_root_:forall (t:Τ)(l:Λ)(m1 m2:Μ), (forall (t':Τ), m1 t'<=m2 t')->
                        valid m1 m2 l=true->valid m1 (minc m2 t) (lift m2 t l)=true.
Proof.
 intros t' l m1 m2 Hm Hval; apply free_valid_.
  intros t''; destruct (LTyp_eq t' t'') as [Ht | Ht].
   rewrite Ht; rewrite minc_eq; apply le_S; apply Hm.
   rewrite (minc_diff Ht); apply Hm.
  generalize (valid_free_ _ _ _ Hval).
  generalize Hm; generalize m2; generalize m1; generalize l; clear l m1 m2 Hm Hval.
  induction l as [t i | t l Hl | l1 Hl1 l2 Hl2]; simpl; intros m1 m2 Hm Hfree t'' i' Hi'.
   unfold dangling in *; destruct (LTyp_eq t'' t) as [Ht''t | Ht''t]; [rewrite Ht''t in * | ];
    my_simpl.
    destruct (LTyp_eq t' t) as [Ht't | Ht't]; [rewrite Ht't in * | ]; my_simpl.
     destruct (LIdx_le (m2 t) i) as [Hm2ti | Hm2ti]; my_simpl.
      destruct (LIdx_le (m1 t) (S i)) as [Hm1tSi | Hm1tSi]; my_simpl.
       destruct (LIdx_eq i' (S i)) as [Hi'Si | Hi'Si]; my_simpl.
        rewrite Hi'Si in Hi'; rewrite minc_eq in Hi';
         destruct (Lt.lt_irrefl _ (Lt.lt_le_trans _ _ _ (Lt.lt_S_n _ _ Hi') Hm2ti)).
        apply refl_equal.
       apply refl_equal.
      destruct (LIdx_le (m1 t) i) as [Hm1ti | Hm1ti]; my_simpl.
       destruct (LIdx_eq i' i) as [Hi'i | Hi'i]; my_simpl.
        rewrite Hi'i in *; generalize (Hfree _ _ Hm2ti); my_simpl; intros Hf; apply Hf.
        apply refl_equal.
      apply refl_equal.
     destruct (LIdx_le (m1 t) i) as [Hm1ti | Hm1ti]; my_simpl.
      destruct (LIdx_eq i' i) as [Hi'i | Hi'i]; my_simpl.
       rewrite Hi'i in *; generalize (Hfree _ _ Hi'); my_simpl; intros Hf; apply Hf.
       apply refl_equal.
      apply refl_equal.
     apply refl_equal.
  rewrite Hl.
   apply refl_equal.
   intros t'''; apply minc_le; apply Hm.
   intros t''' i Hi; generalize (Hfree t'''); clear Hfree; unfold dangling;
    destruct (LTyp_eq t t''') as [Htt''' | Htt''']; [rewrite <- Htt''' in * | ]; intros Hfree.
    destruct i as [| i].
     apply bound_not_free_; rewrite minc_eq; apply Lt.lt_O_Sn.
     rewrite minc_eq in Hi; generalize (Lt.lt_S_n _ _ Hi); clear Hi; intros Hi;
      destruct (LIdx_le (m1 t) i) as [Hm1ti | Hm1ti].
      generalize (Hfree _ Hi); my_simpl; intros Hf; apply Hf.
      apply bound_not_free_; rewrite minc_eq; apply Lt.lt_n_S; apply Hm1ti.
    rewrite (minc_diff Htt''') in Hi; apply (Hfree _ Hi).
    unfold dangling; destruct (LTyp_eq t t'') as [Htt'' | Htt'']; [rewrite Htt'' in * | ].
     destruct (LIdx_le (m1 t'') i') as [Hm1t''i' | Hm1t''i'].
      rewrite minc_comm; rewrite minc_eq; apply Lt.lt_n_S; apply Hi'.
      rewrite minc_comm; rewrite minc_eq; apply Lt.lt_S; apply Hi'.
     rewrite minc_comm; rewrite (minc_diff Htt''); apply Hi'.
   rewrite Hl1.
    rewrite Hl2.
     apply refl_equal.
     apply Hm.
     intros t0 i0 Hi0; destruct (orb_false_elim _ _ (Hfree _ _ Hi0)); apply H0.
     apply Hi'.
    apply Hm.
    intros t0 i0 Hi0; destruct (orb_false_elim _ _ (Hfree _ _ Hi0)); apply H.
    apply Hi'.
Qed.

Theorem lift_valid_root:forall (t:Τ)(l:Λ)(m:Μ),
                        valid μ m l=true->valid μ (minc m t) (lift m t l)=true.
Proof.
 intros t l m Hval; apply (lift_valid_root_ t l μ m).
  intros t'; unfold root; apply Le.le_O_n.
  apply Hval.
Qed.

Theorem valid_eq_:forall (l:Λ)(m:Μ), valid m m l=true.
Proof.
 intros l m; apply free_valid_.
  intros t; apply Le.le_refl.
  intros t i Hi; apply bound_not_free_; apply Hi.
Qed.

(*================================================================================================*)