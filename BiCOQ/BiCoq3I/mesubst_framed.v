(*==================================================================================================
  Project : BiCoq3
  Module : mesubst_framed.v
  This modules considers framed (scoped) multiple parallel substitutions that is substitutions in
  Ι->Ε constrained by a list of indexes.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Feb 2009, Md: Mar 2009
  ================================================================================================*)

Require Export sem_ind.
Require Export mesubst.
Require Export congr.
Require Import List.

(* List operations -------------------------------------------------------------------------------*)
(* We define frames as list of indexes. *)

Fixpoint LI_mem(l:list Ι)(i:Ι){struct l}:Β :=
 match l with nil => ⊥ | cons i' l' => DB_eq i i' || LI_mem l' i end.

Fixpoint LI_rem(l:list Ι)(i:Ι){struct l}:list Ι :=
 match l with
 | nil => nil
 | cons i' l' => if (DB_eq i i') then (LI_rem l' i) else cons i' (LI_rem l' i)
 end.

Fixpoint LI_lift_(d:nat)(l:list Ι){struct l}:list Ι :=
 match l with nil => nil | cons i' l' => cons (DB_lift_ d i') (LI_lift_ d l') end.

Fixpoint Bp_LIinc_(d:nat)(p:Π)(l:list Ι){struct p}:Β :=
 match p with
 | ¬p' => Bp_LIinc_ d p' l
 | ∀Δ(p') => Bp_LIinc_ (S d) p' (LI_lift_ d l)
 | p1⋀p2 => (Bp_LIinc_ d p1 l) && (Bp_LIinc_ d p2 l)
 | (p1⇒p2) => (Bp_LIinc_ d p1 l) && (Bp_LIinc_ d p2 l)
 | ∐(_) => ⊤
 | e1≡e2 => (Be_LIinc_ d e1 l) && (Be_LIinc_ d e2 l)
 | e1∈e2 => (Be_LIinc_ d e1 l) && (Be_LIinc_ d e2 l)
 end
with Be_LIinc_(d:nat)(e:Ε)(l:list Ι){struct e}:Β :=
 match e with
 | Ω => ⊤
 | ↓(e') => Be_LIinc_ d e' l
 | ↑(e') => Be_LIinc_ d e' l
 | e1↦e2 => (Be_LIinc_ d e1 l) && (Be_LIinc_ d e2 l)
 | e1×e2 => (Be_LIinc_ d e1 l) && (Be_LIinc_ d e2 l)
 | ∞(n') => ⊤
 | ∂(i') => negb (DB_le d i') || LI_mem l i'
 | ⅭΔ(e',p') => (Be_LIinc_ d e' l) && (Bp_LIinc_ (S d) p' (LI_lift_ d l))
 end.

Definition Bt_LIinc_(d:nat)(t:Τ)(l:list Ι):Β :=
 match t with be_ e => Be_LIinc_ d e l | bp_ p => Bp_LIinc_ d p l end.
Notation "'(' t '⊆Φ' l ')'" := (Bt_LIinc_ 0 t l=⊤).

Fixpoint LI_app(l1 l2:list Ι){struct l1}:list Ι :=
 match l1 with nil => l2 | cons i' l' => i'::(LI_app l' l2) end.

Fixpoint LI_inject(l:list Ι){struct l}:list Ι :=
 match l with nil => nil | i'::l' => if LI_mem l' i' then LI_inject l' else i'::LI_inject l' end.

(* Theorems for frames ---------------------------------------------------------------------------*)

Theorem LI_rem_mem:forall (l:list Ι)(i:Ι), LI_mem (LI_rem l i) i=⊥.
Proof.
 induction l as [| i' l' Hl']; intros i; simpl;
 [apply refl_equal | case_eq (DB_eq i i'); intros Hii'; [ | simpl; rewrite Hii'; simpl]; apply Hl'].
Qed.

Theorem LI_lift_mem_:forall (l:list Ι)(i:Ι)(d:nat), LI_mem (LI_lift_ d l) (DB_lift_ d i)=LI_mem l i.
Proof.
 induction l as [| i' l' Hl']; intros i d; simpl; [ | DB_simpl; rewrite Hl']; apply refl_equal.
Qed.

Theorem Bt_LIinc_free_:forall (t:Τ)(l:list Ι)(d:nat),
                       Bt_LIinc_ d t l=⊤->forall (i:Ι), Bt_free_ d i t=⊤->LI_mem l i=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l:list Ι)(d:nat), Bt_LIinc_ d t l=⊤->
                            (forall (i:Ι), Bt_free_ d i t=⊤->LI_mem l i=⊤)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind l d Hinc i Hfree;
  try (inversion Hfree; fail); try (apply (Hind _ (sdepth t) _ _ Hinc _ Hfree));
  try (destruct (andb_prop _ _ Hinc) as [Hinc1 Hinc2];
       destruct (orb_true_elim _ _ Hfree) as [Hfree1 | Hfree2];
       [apply (Hind _ (sdepth_l t1 t2) _ _ Hinc1 _ Hfree1) |
        apply (Hind _ (sdepth_r t1 t2) _ _ Hinc2 _ Hfree2)]).
  destruct (DB_le d i'); bool_simpl;
  [reflect_in DB_eq_imp Hfree; rewrite Hfree; apply Hinc | inversion Hfree].
  destruct (andb_prop _ _ Hinc) as [Hinc1 Hinc2];
   destruct (orb_true_elim _ _ Hfree) as [Hfree1 | Hfree2].
   apply (Hind _ (sdepth_l t1 t2) _ _ Hinc1 _ Hfree1).
   rewrite <- LI_lift_mem_ with (d:=d); apply (Hind _ (sdepth_r t1 t2) _ _ Hinc2 _ Hfree2).
  rewrite <- LI_lift_mem_ with (d:=d); apply (Hind _ (sdepth t) _ _ Hinc _ Hfree).
Qed.

Theorem Bt_free_LIinc_:forall (t:Τ)(l:list Ι)(d:nat),
                       (forall (i:Ι), Bt_free_ d i t=⊤->LI_mem l i=⊤)->Bt_LIinc_ d t l=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l:list Ι)(d:nat),
                            (forall (i:Ι), Bt_free_ d i t=⊤->LI_mem l i=⊤)->Bt_LIinc_ d t l=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind l d Ht;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) _ _ Ht));
  try (apply andb_true_intro; split;
       [cut (forall (i:Ι), Bt_free_ d i t1=⊤->LI_mem l i=⊤);
        [intros Ht1; apply (Hind _ (sdepth_l t1 t2) _ _ Ht1) |
         intros i Ht1; apply Ht; srewrite Ht1; bool_simpl; apply refl_equal] |
        cut (forall (i:Ι), Bt_free_ d i t2=⊤->LI_mem l i=⊤);
        [intros Ht2; apply (Hind _ (sdepth_r t1 t2) _ _ Ht2) |
         intros i Ht2; apply Ht; srewrite Ht2; bool_simpl; apply refl_equal]]).
  destruct (DB_le d i'); bool_simpl; [apply Ht; DB_simpl | ]; apply refl_equal.
  apply andb_true_intro; split.
   cut (forall (i:Ι), Bt_free_ d i t1=⊤->LI_mem l i=⊤);
   [intros Ht1; apply (Hind _ (sdepth_l t1 t2) _ _ Ht1) |
    intros i Ht1; apply Ht; srewrite Ht1; bool_simpl; apply refl_equal].
   cut (forall (i:Ι), Bt_free_ (S d) i t2=⊤->LI_mem (LI_lift_ d l) i=⊤).
    intros Ht2; apply (Hind _ (sdepth_r t1 t2) (LI_lift_ d l) (S d) Ht2).
    intros i Hit2; simpl in *; case_eq (DB_le (S d) i); intros Hdi;
    [ | srewrite_in (Bt_le_free_ t2 _ _ Hdi) Hit2; inversion Hit2].
     destruct i as [[| u] n]; simpl in *; nat_simpl.
      destruct n as [| n]; simpl in *; nat_simpl; [inversion Hdi | ].
      replace ¡(0,S n) with (DB_lift_ d ¡(0,n)) in *; [ | simpl; rewrite Hdi; apply refl_equal];
       rewrite LI_lift_mem_; apply Ht; rewrite Hit2; bool_simpl; apply refl_equal.
      change ¡(S u,n) with (DB_lift_ d ¡(S u,n)); rewrite LI_lift_mem_; apply Ht;
       change ¡(S u,n) with (DB_lift_ d ¡(S u,n)) in Hit2; rewrite Hit2; bool_simpl;
       apply refl_equal.
  cut (forall (i:Ι), Bt_free_ (S d) i t=⊤->LI_mem (LI_lift_ d l) i=⊤).
   intros H't; apply (Hind _ (sdepth t) (LI_lift_ d l) (S d) H't).
   intros i Hit; simpl in *; case_eq (DB_le (S d) i); intros Hdi;
   [ | srewrite_in (Bt_le_free_ t _ _ Hdi) Hit; inversion Hit].
    destruct i as [[| u] n]; simpl in *; nat_simpl.
     destruct n as [| n]; simpl in *; nat_simpl; [inversion Hdi | ].
     replace ¡(0,S n) with (DB_lift_ d ¡(0,n)) in *; [ | simpl; rewrite Hdi; apply refl_equal];
      rewrite LI_lift_mem_; apply Ht; rewrite Hit; bool_simpl; apply refl_equal.
     change ¡(S u,n) with (DB_lift_ d ¡(S u,n)); rewrite LI_lift_mem_; apply Ht;
      change ¡(S u,n) with (DB_lift_ d ¡(S u,n)) in Hit; rewrite Hit; bool_simpl;
      apply refl_equal.
Qed.

Theorem Bt_LIinc_lift_:forall (t:Τ)(l:list Ι)(d:nat),
                       Bt_LIinc_ (S d) (Bt_lift_ d t) (LI_lift_ d l)=Bt_LIinc_ d t l.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l:list Ι)(d:nat),
                            Bt_LIinc_ (S d) (Bt_lift_ d t) (LI_lift_ d l)=Bt_LIinc_ d t l));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind l d;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) l d));
  try (srewrite (Hind _ (sdepth_l t1 t2) l d); srewrite (Hind _ (sdepth_r t1 t2) l d);
       apply refl_equal).
  DB_simpl; rewrite LI_lift_mem_; apply refl_equal.
  srewrite (Hind _ (sdepth_l t1 t2) l d); srewrite (Hind _ (sdepth_r t1 t2) (LI_lift_ d l) (S d));
   apply refl_equal.
  srewrite (Hind _ (sdepth t) (LI_lift_ d l) (S d)); apply refl_equal.
Qed.

Theorem Bt_LIinc_abstr_:forall (t:Τ)(l:list Ι)(i:Ι)(d:nat),
                        Bt_LIinc_ d t l=⊤->Bt_LIinc_ (S d) (Bt_abstr_ d i t) (LI_lift_ d l)=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l:list Ι)(i:Ι)(d:nat),
                            Bt_LIinc_ d t l=⊤->Bt_LIinc_ (S d) (Bt_abstr_ d i t) (LI_lift_ d l)=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind l i d Ht;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) _ i _ Ht));
  try (destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; srewrite (Hind _ (sdepth_l t1 t2) _ i _ Ht1);
       apply (Hind _ (sdepth_r t1 t2) _ i _ Ht2)).
  case_eq (DB_le d i'); intros Hdi'; rewrite Hdi' in *; bool_simpl; DB_simpl;
  [ | rewrite Hdi'; apply refl_equal].
   case_eq (DB_eq i i'); intros Hii'; DB_simpl; simpl; nat_simpl; [apply refl_equal | ].
    rewrite Hdi'; simpl; rewrite LI_lift_mem_; apply Ht.
  destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; srewrite (Hind _ (sdepth_l t1 t2) _ i _ Ht1);
   apply (Hind _ (sdepth_r t1 t2) _ (DB_lift_ d i) _ Ht2).
  apply (Hind _ (sdepth t) _ (DB_lift_ d i) _ Ht).
Qed.

Theorem LI_app_mem:forall (l1 l2:list Ι)(i:Ι), LI_mem (LI_app l1 l2) i=LI_mem l1 i || LI_mem l2 i.
Proof.
 induction l1 as [| i1 l1]; simpl; intros l2 i; [ | rewrite IHl1; destruct (DB_eq i i1)];
  apply refl_equal.
Qed.

Theorem LI_app_lift_:forall (l1 l2:list Ι)(d:nat),
                     LI_lift_ d (LI_app l1 l2)=LI_app (LI_lift_ d l1) (LI_lift_ d l2).
Proof.
 induction l1 as [| i1 l1 Hl1]; simpl; intros l2 d; [ | rewrite Hl1]; apply refl_equal.
Qed.

Theorem Bt_LIinc_app_l_:forall (t:Τ)(l1 l2:list Ι)(d:nat),
                        Bt_LIinc_ d t l1=⊤->Bt_LIinc_ d t (LI_app l1 l2)=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l1 l2:list Ι)(d:nat),
                            Bt_LIinc_ d t l1=⊤->Bt_LIinc_ d t (LI_app l1 l2)=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind l1 l2 d Ht;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) _ l2 _ Ht));
  try (destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; srewrite (Hind _ (sdepth_l t1 t2) _ l2 _ Ht1);
       apply (Hind _ (sdepth_r t1 t2) _ l2 _ Ht2)).
  rewrite LI_app_mem; destruct (orb_true_elim _ _ Ht) as [Hi' | Hi']; rewrite Hi'; bool_simpl;
   apply refl_equal.
  destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; srewrite (Hind _ (sdepth_l t1 t2) _ l2 _ Ht1);
   rewrite LI_app_lift_; apply (Hind _ (sdepth_r t1 t2) _ (LI_lift_ d l2) _ Ht2).
  rewrite LI_app_lift_; apply (Hind _ (sdepth t) _ (LI_lift_ d l2) _ Ht).
Qed.

Theorem Bt_LIinc_app_r_:forall (t:Τ)(l1 l2:list Ι)(d:nat),
                        Bt_LIinc_ d t l2=⊤->Bt_LIinc_ d t (LI_app l1 l2)=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l1 l2:list Ι)(d:nat),
                            Bt_LIinc_ d t l2=⊤->Bt_LIinc_ d t (LI_app l1 l2)=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind l1 l2 d Ht;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) l1 _ _ Ht));
  try (destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; srewrite (Hind _ (sdepth_l t1 t2) l1 _ _ Ht1);
       apply (Hind _ (sdepth_r t1 t2) l1 _ _ Ht2)).
  rewrite LI_app_mem; destruct (orb_true_elim _ _ Ht) as [Hi' | Hi']; rewrite Hi'; bool_simpl;
   apply refl_equal.
  destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; srewrite (Hind _ (sdepth_l t1 t2) l1 _ _ Ht1);
   rewrite LI_app_lift_; apply (Hind _ (sdepth_r t1 t2) (LI_lift_ d l1) _ _ Ht2).
  rewrite LI_app_lift_; apply (Hind _ (sdepth t) (LI_lift_ d l1) _ _ Ht).
Qed.

Theorem LI_inject_mem:forall (l:list Ι)(i:Ι), LI_mem l i=LI_mem (LI_inject l) i.
Proof.
 induction l as [| i' l' Hl']; intros i; simpl.
  apply refl_equal.
  case_eq (DB_eq i i'); intros Hii'; bool_simpl.
   reflect_in DB_eq_imp Hii'; rewrite Hii' in *; case_eq (LI_mem l' i'); intros Hl'i';
   [rewrite <- Hl'; rewrite Hl'i' | simpl; DB_simpl]; apply refl_equal.
   case_eq (LI_mem l' i'); intros Hl'i'; [ | simpl; rewrite Hii'; bool_simpl]; apply Hl'.
Qed.

(* Framed multiple parallel substitutions --------------------------------------------------------*)

Definition FS:Set := prod (list Ι) Μ.
Notation "'Φ'" := FS.

Notation "'(' m '⊲' l ')'" := (pair l m).

Definition FS_nil:Φ := (⊚⊲nil).

Definition FS_ins(f:Φ)(i:Ι)(e:Ε) := match f with (m'⊲l') => (≺i←e≻⊕m'⊲i::l') end.

Definition FS_rem(f:Φ)(i:Ι) := match f with (m'⊲l') => (m'⊖i⊲LI_rem l' i) end.

Definition FS_lift_(d:nat)(f:Φ) := match f with (m'⊲l') => (MS_lift_ d m'⊲LI_lift_ d l') end.
Notation "'⇑Φ(' f ')'":=(FS_lift_ 0 f).

(* Application to a term -------------------------------------------------------------------------*)

Fixpoint Bp_fesubst_(d:nat)(f:Φ)(p:Π){struct p}:Π :=
 match p with
 | ¬p' => ¬(Bp_fesubst_ d f p')
 | ∀Δ(p') => ∀Δ(Bp_fesubst_ (S d) (FS_lift_ d f) p')
 | p1⋀p2 => (Bp_fesubst_ d f p1)⋀(Bp_fesubst_ d f p2)
 | (p1⇒p2) => (Bp_fesubst_ d f p1)⇒(Bp_fesubst_ d f p2)
 | ∐(_) => p
 | e1≡e2 => (Be_fesubst_ d f e1)≡(Be_fesubst_ d f e2)
 | e1∈e2 => (Be_fesubst_ d f e1)∈(Be_fesubst_ d f e2)
 end
with Be_fesubst_(d:nat)(f:Φ)(e:Ε){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_fesubst_ d f e')
 | ↑(e') => ↑(Be_fesubst_ d f e')
 | e1↦e2 => (Be_fesubst_ d f e1)↦(Be_fesubst_ d f e2)
 | e1×e2 => (Be_fesubst_ d f e1)×(Be_fesubst_ d f e2)
 | ∞(_) => e
 | ∂(i') => match f with (m'⊲l') => if DB_le d i' && LI_mem l' i' then m' i' else e end
 | ⅭΔ(e',p') => ⅭΔ(Be_fesubst_ d f e',Bp_fesubst_ (S d) (FS_lift_ d f) p')
 end.
Notation "'〈|' f '@Π@' p '|〉'" := (Bp_fesubst_ 0 f p).
Notation "'〈|' f '@Ε@' e '|〉'" := (Be_fesubst_ 0 f e).

Definition Bt_fesubst_(d:nat)(f:Φ)(t:Τ):Τ :=
 match t with be_ e' => Be_fesubst_ d f e' | bp_ p' => Bp_fesubst_ d f p' end.
Notation "'〈|' f '@' t '|〉'" := (Bt_fesubst_ 0 f t).

(* Framed substitutions main theorem -------------------------------------------------------------*)

Theorem Bt_mesubst_fesubst_:forall (t:Τ)(l:list Ι)(m:Μ)(d:nat),
                            Bt_LIinc_ d t l=⊤->Bt_fesubst_ d (m⊲l) t=Bt_mesubst_ d m t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l:list Ι)(m:Μ)(d:nat),
                            Bt_LIinc_ d t l=⊤->Bt_fesubst_ d (m⊲l) t=Bt_mesubst_ d m t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind l m d Ht;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ m _ Ht); apply refl_equal);
  try (destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; irewrite (Hind _ (sdepth_l t1 t2) _ m _ Ht1);
       irewrite (Hind _ (sdepth_r t1 t2) _ m _ Ht2); apply refl_equal).
  case_eq (DB_le d i'); intros Hdi'; rewrite Hdi' in *; bool_simpl; [rewrite Ht | ];
   apply refl_equal.
  destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; irewrite (Hind _ (sdepth_l t1 t2) _ m _ Ht1);
   irewrite (Hind _ (sdepth_r t1 t2) _ (MS_lift_ d m) _ Ht2); apply refl_equal.
  irewrite (Hind _ (sdepth t) _ (MS_lift_ d m) _ Ht); apply refl_equal.
Qed.

Theorem Bt_mesubst_fesubst:forall (t:Τ)(l:list Ι)(m:Μ), (t⊆Φl)->〈|(m⊲l)@t|〉=〈〈m@t〉〉.
Proof.
 intros t l m Hinc; apply (Bt_mesubst_fesubst_ _ _ m 0 Hinc).
Qed.

Theorem Bt_fesubst_nil_:forall (t:Τ)(m:Μ)(d:nat), Bt_fesubst_ d (m⊲nil) t=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (m:Μ)(d:nat), Bt_fesubst_ d (m⊲nil) t=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind m d;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) m d); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) m d); irewrite (Hind _ (sdepth_r t1 t2) m d);
       apply refl_equal).
  bool_simpl; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) m d);
   generalize (sym_equal (Hind _ (sdepth_r t1 t2) (MS_lift_ d m) (S d))); intros Ht2; injection Ht2;
   clear Ht2; intros Ht2; pattern t2 at 2; rewrite Ht2; apply refl_equal.
  generalize (sym_equal (Hind _ (sdepth t) (MS_lift_ d m) (S d))); intros Ht; injection Ht;
   clear Ht; intros Ht; pattern t at 2; rewrite Ht; apply refl_equal.
Qed.

Theorem Bt_fesubst_nil:forall (t:Τ)(m:Μ), 〈|(m⊲nil)@t|〉=t.
Proof.
 intros t m; apply Bt_fesubst_nil_.
Qed.

Theorem Bt_fesubst_sin_:forall (t:Τ)(m:Μ)(i:Ι)(d:nat), ≤Ι(d,i)->
                        Bt_fesubst_ d (m⊲(i::nil)) t=Bt_esubst_ d i t (m i).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (m:Μ)(i:Ι)(d:nat), ≤Ι(d,i)->
                            Bt_fesubst_ d (m⊲(i::nil)) t=Bt_esubst_ d i t (m i)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind m i d Hdi;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) m _ _ Hdi); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) m _ _ Hdi); irewrite (Hind _ (sdepth_r t1 t2) m _ _ Hdi);
       apply refl_equal).
  case_eq (DB_eq i i'); intros Hii'; bool_simpl; reflect_in DB_eq_imp Hii'.
   rewrite Hii' in *; rewrite Hdi; DB_simpl; apply refl_equal.
   rewrite (imprf2 DB_eq_imp _ _ (sym_not_eq Hii')); bool_simpl; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) m _ _ Hdi); rewrite <- DB_le_lift in Hdi;
   irewrite (Hind _ (sdepth_r t1 t2) (MS_lift_ d m) _ _ Hdi); rewrite DB_le_lift in Hdi;
   rewrite (MS_get_lift_ m _ _ Hdi); apply refl_equal.
  rewrite <- DB_le_lift in Hdi; irewrite (Hind _ (sdepth t) (MS_lift_ d m) _ _ Hdi);
   rewrite DB_le_lift in Hdi; rewrite (MS_get_lift_ m _ _ Hdi); apply refl_equal.
Qed.

Theorem Bt_fesubst_sin:forall (t:Τ)(m:Μ)(i:Ι), 〈|(m⊲(i::nil))@t|〉=〈i←m i@t〉.
Proof.
 intros t m i; apply Bt_fesubst_sin_; apply DB_le_0.
Qed.

(* Frame extraction ------------------------------------------------------------------------------*)

Inductive Bt_framed_:nat->Τ->list Ι->Type :=
 | Bt_framed_not:forall (d:nat)(p:Π)(l:list Ι), Bt_framed_ d p l->Bt_framed_ d (¬p) l
 | Bt_framed_for:forall (d:nat)(p:Π)(l:list Ι),
                 Bt_framed_ (S d) p (LI_lift_ d l)->Bt_framed_ d ∀Δ(p) l
 | Bt_framed_and:forall (d:nat)(p1 p2:Π)(l:list Ι),
                 Bt_framed_ d p1 l->Bt_framed_ d p2 l->Bt_framed_ d (p1⋀p2) l
 | Bt_framed_imp:forall (d:nat)(p1 p2:Π)(l:list Ι),
                 Bt_framed_ d p1 l->Bt_framed_ d p2 l->Bt_framed_ d (p1⇒p2) l
 | Bt_framed_prp:forall (d n:nat)(l:list Ι), Bt_framed_ d (∐(n)) l
 | Bt_framed_equ:forall (d:nat)(e1 e2:Ε)(l:list Ι),
                 Bt_framed_ d e1 l->Bt_framed_ d e2 l->Bt_framed_ d (e1≡e2) l
 | Bt_framed_ins:forall (d:nat)(e1 e2:Ε)(l:list Ι),
                 Bt_framed_ d e1 l->Bt_framed_ d e2 l->Bt_framed_ d (e1∈e2) l
 | Bt_framed_big:forall (d:nat)(l:list Ι), Bt_framed_ d Ω l
 | Bt_framed_chc:forall (d:nat)(e:Ε)(l:list Ι), Bt_framed_ d e l->Bt_framed_ d ↓(e) l
 | Bt_framed_pow:forall (d:nat)(e:Ε)(l:list Ι), Bt_framed_ d e l->Bt_framed_ d ↑(e) l
 | Bt_framed_cpl:forall (d:nat)(e1 e2:Ε)(l:list Ι),
                 Bt_framed_ d e1 l->Bt_framed_ d e2 l->Bt_framed_ d (e1↦e2) l
 | Bt_framed_pro:forall (d:nat)(e1 e2:Ε)(l:list Ι),
                 Bt_framed_ d e1 l->Bt_framed_ d e2 l->Bt_framed_ d (e1×e2) l
 | Bt_framed_bge:forall (d:nat)(n:nat)(l:list Ι), Bt_framed_ d (∞(n)) l
 | Bt_framed_var:forall (d:nat)(i:Ι)(l:list Ι), ≤Ι(d,i)->LI_mem l i=⊤->Bt_framed_ d ∂(i) l
 | Bt_framed_bnd:forall (d:nat)(i:Ι)(l:list Ι), DB_le d i=⊥->Bt_framed_ d ∂(i) l
 | Bt_framed_cmp:forall (d:nat)(p:Π)(e:Ε)(l:list Ι),
                 Bt_framed_ d e l->Bt_framed_ (S d) p (LI_lift_ d l)->Bt_framed_ d ⅭΔ(e,p) l.
(* Nota: we prove just after that Bt_framed_ d t l<->Bt_LIinc_ d t l=⊤; the interest of this
   inductive relation is just to have an elegant way to build the appropriate list, as the direct
   approach through a function enforces to define an ugly "reverse" lifting of frames. *)

Theorem Bt_framed_inc_:forall (t:Τ)(l:list Ι)(d:nat), Bt_framed_ d t l->Bt_LIinc_ d t l=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l:list Ι)(d:nat), Bt_framed_ d t l->Bt_LIinc_ d t l=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind l d Ht;
  try (apply refl_equal); try (inversion_clear Ht; apply (Hind _ (sdepth t) _ _ H));
  try (inversion_clear Ht; srewrite (Hind _ (sdepth_l t1 t2) _ _ H);
       apply (Hind _ (sdepth_r t1 t2) _ _ H0)).
  inversion_clear Ht; [rewrite H0 | rewrite H]; bool_simpl; apply refl_equal.
Qed.

Theorem Bt_inc_framed_:forall (t:Τ)(l:list Ι)(d:nat), Bt_LIinc_ d t l=⊤->Bt_framed_ d t l.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l:list Ι)(d:nat), Bt_LIinc_ d t l=⊤->Bt_framed_ d t l));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind l d Ht.
  apply Bt_framed_big.
  apply Bt_framed_chc; apply (Hind _ (sdepth t) _ _ Ht).
  apply Bt_framed_pow; apply (Hind _ (sdepth t) _ _ Ht).
  destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; apply Bt_framed_cpl;
  [apply (Hind _ (sdepth_l t1 t2) _ _ Ht1) | apply (Hind _ (sdepth_r t1 t2) _ _ Ht2)].
  destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; apply Bt_framed_pro;
  [apply (Hind _ (sdepth_l t1 t2) _ _ Ht1) | apply (Hind _ (sdepth_r t1 t2) _ _ Ht2)].
  apply Bt_framed_bge.
  case_eq (DB_le d i'); intros Hdi'; rewrite Hdi' in Ht; bool_simpl.
   apply Bt_framed_var; [apply Hdi' | apply Ht].
   apply Bt_framed_bnd; apply Hdi'.
  destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; apply Bt_framed_cmp;
  [apply (Hind _ (sdepth_l t1 t2) _ _ Ht1) | apply (Hind _ (sdepth_r t1 t2) _ _ Ht2)].
  apply Bt_framed_not; apply (Hind _ (sdepth t) _ _ Ht).
  apply Bt_framed_for; apply (Hind _ (sdepth t) _ _ Ht).
  destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; apply Bt_framed_and;
  [apply (Hind _ (sdepth_l t1 t2) _ _ Ht1) | apply (Hind _ (sdepth_r t1 t2) _ _ Ht2)].
  destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; apply Bt_framed_imp;
  [apply (Hind _ (sdepth_l t1 t2) _ _ Ht1) | apply (Hind _ (sdepth_r t1 t2) _ _ Ht2)].
  apply Bt_framed_prp.
  destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; apply Bt_framed_equ;
  [apply (Hind _ (sdepth_l t1 t2) _ _ Ht1) | apply (Hind _ (sdepth_r t1 t2) _ _ Ht2)].
  destruct (andb_prop _ _ Ht) as [Ht1 Ht2]; apply Bt_framed_ins;
  [apply (Hind _ (sdepth_l t1 t2) _ _ Ht1) | apply (Hind _ (sdepth_r t1 t2) _ _ Ht2)].
Qed.

Theorem Bt_framed_app_l_:forall (t:Τ)(l1 l2:list Ι)(d:nat),
                         Bt_framed_ d t l1->Bt_framed_ d t (LI_app l1 l2).
Proof.
 intros t l1 l2 d Ht; apply Bt_inc_framed_; apply Bt_LIinc_app_l_; apply Bt_framed_inc_; apply Ht.
Qed.

Theorem Bt_framed_app_r_:forall (t:Τ)(l1 l2:list Ι)(d:nat),
                         Bt_framed_ d t l2->Bt_framed_ d t (LI_app l1 l2).
Proof.
 intros t l1 l2 d Ht; apply Bt_inc_framed_; apply Bt_LIinc_app_r_; apply Bt_framed_inc_; apply Ht.
Qed.

Theorem Bt_framed_exs:forall (t:Τ)(d:nat), sigS (fun (l:list Ι) =>Bt_framed_ d t l).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d:nat), sigS (fun (l:list Ι) => Bt_framed_ d t l)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind d.
  exists (nil (A:=Ι)); apply Bt_framed_big.
  destruct (Hind _ (sdepth t) d) as [l Ht]; exists l; apply Bt_framed_chc; apply Ht.
  destruct (Hind _ (sdepth t) d) as [l Ht]; exists l; apply Bt_framed_pow; apply Ht.
  destruct (Hind _ (sdepth_l t1 t2) d) as [l1 Ht1]; destruct (Hind _ (sdepth_r t1 t2) d) as [l2 Ht2];
   exists (LI_app l1 l2); apply Bt_framed_cpl;
  [apply Bt_framed_app_l_; apply Ht1 | apply Bt_framed_app_r_; apply Ht2].
  destruct (Hind _ (sdepth_l t1 t2) d) as [l1 Ht1]; destruct (Hind _ (sdepth_r t1 t2) d) as [l2 Ht2];
   exists (LI_app l1 l2); apply Bt_framed_pro;
  [apply Bt_framed_app_l_; apply Ht1 | apply Bt_framed_app_r_; apply Ht2].
  exists (nil (A:=Ι)); apply Bt_framed_bge.
  case_eq (DB_le d i'); intros Hdi'. (* "exists (cons i' nil)" sufficient but this is nicer *)
   exists (cons i' nil); apply Bt_framed_var; [apply Hdi' | simpl; DB_simpl; apply refl_equal].
   exists (nil (A:=Ι)); apply Bt_framed_bnd; apply Hdi'.
  destruct (Hind _ (sdepth_l t1 t2) d) as [l1 Ht1];
   generalize (Bt_fshun_free t2 _ (le_n_Sn _) 0 (S d)); set (i:=¡(S ⋇(t2),0)); intros Hit2;
   change i with (DB_lift_ d i) in Hit2;
   irewrite (sym_equal (Bt_abstr_app_ t2 i d (refl_equal _) Hit2));
   assert (Hdpt:↧(Bt_app_ d t2 ∂(i)) < S (max ↧(t1) ↧(t2))).
    rewrite (Bt_app_depth t2 d i); apply sdepth_r.
   destruct (Hind _ Hdpt d) as [l2 Ht2]; exists (LI_app l1 l2); apply Bt_framed_cmp.
    apply Bt_framed_app_l_; apply Ht1.
    rewrite LI_app_lift_; apply Bt_framed_app_r_; apply  Bt_inc_framed_;
     apply (Bt_LIinc_abstr_ (Bp_app_ d t2 ∂(i)) l2 i d); apply  Bt_framed_inc_; apply Ht2.
  destruct (Hind _ (sdepth t) d) as [l Ht]; exists l; apply Bt_framed_not; apply Ht.
  generalize (Bt_fshun_free t _ (le_n_Sn _) 0 (S d)); set (i:=¡(S ⋇(t),0)); intros Hit;
   change i with (DB_lift_ d i) in Hit;
   irewrite (sym_equal (Bt_abstr_app_ t i d (refl_equal _) Hit));
   assert (Hdpt:↧(Bt_app_ d t ∂(i)) < S ↧(t)). rewrite (Bt_app_depth t d i); apply sdepth.
  destruct (Hind _ Hdpt d) as [l Ht]; exists l; apply Bt_framed_for; apply  Bt_inc_framed_;
   apply (Bt_LIinc_abstr_ (Bp_app_ d t ∂(i)) l i d); apply  Bt_framed_inc_; apply Ht.
  destruct (Hind _ (sdepth_l t1 t2) d) as [l1 Ht1]; destruct (Hind _ (sdepth_r t1 t2) d) as [l2 Ht2];
   exists (LI_app l1 l2); apply Bt_framed_and;
  [apply Bt_framed_app_l_; apply Ht1 | apply Bt_framed_app_r_; apply Ht2].
  destruct (Hind _ (sdepth_l t1 t2) d) as [l1 Ht1]; destruct (Hind _ (sdepth_r t1 t2) d) as [l2 Ht2];
   exists (LI_app l1 l2); apply Bt_framed_imp;
  [apply Bt_framed_app_l_; apply Ht1 | apply Bt_framed_app_r_; apply Ht2].
  exists (nil (A:=Ι)); apply Bt_framed_prp.
  destruct (Hind _ (sdepth_l t1 t2) d) as [l1 Ht1]; destruct (Hind _ (sdepth_r t1 t2) d) as [l2 Ht2];
   exists (LI_app l1 l2); apply Bt_framed_equ;
  [apply Bt_framed_app_l_; apply Ht1 | apply Bt_framed_app_r_; apply Ht2].
  destruct (Hind _ (sdepth_l t1 t2) d) as [l1 Ht1]; destruct (Hind _ (sdepth_r t1 t2) d) as [l2 Ht2];
   exists (LI_app l1 l2); apply Bt_framed_ins;
  [apply Bt_framed_app_l_; apply Ht1 | apply Bt_framed_app_r_; apply Ht2].
Qed.

Theorem frame_:forall (t:Τ)(d:nat), sigS (fun (l:list Ι) => Bt_LIinc_ d t l=⊤).
Proof.
 intros t d; destruct (Bt_framed_exs t d) as [l Ht]; exists l; apply Bt_framed_inc_; apply Ht.
Qed.

Theorem frame:forall (t:Τ), sigS (fun (l:list Ι) => (t⊆Φl)).
Proof.
 intros t; apply (frame_ t 0).
Qed.

Theorem Bt_fesubst_mesubst:forall (t:Τ), sigS (fun (l:list Ι) => forall (m:Μ), 〈〈m@t〉〉=〈|(m⊲l)@t|〉).
Proof.
 intros t; destruct (frame t) as [l Ht]; exists l; intros m;
  apply (sym_equal (Bt_mesubst_fesubst_ t l m 0 Ht)).
Qed.

(* Parallel substitition equivalent --------------------------------------------------------------*)

Fixpoint MS_framed(m:Μ)(l:list Ι){struct l}:Μ :=
 match l with nil => ⊚ | cons i' l' => ≺i'←m i'≻⊕(MS_framed m l') end.

Theorem MS_framed_mem:forall (l:list Ι)(m:Μ)(i:Ι), LI_mem l i=⊤->(MS_framed m l) i=m i.
Proof.
 induction l as [| i' l' Hl']; simpl; intros m i Hli.
  inversion Hli.
  case_eq (DB_eq i i'); intros Hii'; rewrite Hii' in *; simpl; reflect_in DB_eq_imp Hii'.
   rewrite Hii'; rewrite MS_ins_eq; apply refl_equal.
   rewrite (MS_ins_diff (MS_framed m l') i i' (m i') (sym_not_eq Hii')); apply Hl'; apply Hli.
Qed.

Theorem MS_framed_notmem:forall (l:list Ι)(m:Μ)(i:Ι), LI_mem l i=⊥->(MS_framed m l) i=∂(i).
Proof.
 induction l as [| i' l' Hl']; simpl; intros m i Hli.
  unfold MS_nil; apply refl_equal.
  destruct (orb_false_elim _ _ Hli) as [Hii' Hl'i]; reflect_in DB_eq_imp Hii';
   rewrite (MS_ins_diff (MS_framed m l') i i' (m i') (sym_not_eq Hii')); apply Hl'; apply Hl'i.
Qed.

Theorem MS_framed_lift_:forall (l:list Ι)(m:Μ)(d:nat), MS_eq (MS_lift_ d (MS_framed m l))
                                                          (MS_framed (MS_lift_ d m) (LI_lift_ d l)).
Proof.
 induction l as [| i l Hl]; intros m d.
  apply MS_nil_lift.
  intros [[| u'] [| n']]; destruct i as [[| u] [| n]]; destruct d as [| d]; simpl;
   nat_simpl; bool_simpl;
   try (rewrite MS_ins_diff; [rewrite <- Hl; unfold MS_lift_; apply refl_equal | discriminate]);
   try (repeat (rewrite MS_ins_diff); try (discriminate); rewrite <- Hl; unfold MS_lift_; nat_simpl;
        apply refl_equal).
   rewrite MS_ins_eq; apply refl_equal.
   case_eq (nat_le d n); intros Hdn; nat_simpl; rewrite MS_ins_diff; try (discriminate);
    rewrite <- Hl; unfold MS_lift_; apply refl_equal.
   destruct n' as [| n'].
    repeat (rewrite MS_ins_eq); apply refl_equal.
    repeat (rewrite (MS_ins_diff)); try (discriminate); rewrite <- Hl; unfold MS_lift_; nat_simpl;
     apply refl_equal.
   destruct n' as [| n']; nat_simpl; repeat (rewrite MS_ins_diff); try (discriminate);
    rewrite <- Hl; unfold MS_lift_; nat_simpl; apply refl_equal.
   destruct n' as [| n']; simpl.
    repeat (rewrite MS_ins_diff); try (discriminate); rewrite <- Hl; unfold MS_lift_; nat_simpl;
     apply refl_equal.
    case_eq (nat_eq n n'); intros Hnn'; simpl.
     reflect_in nat_eq_imp Hnn'; rewrite Hnn' in *; repeat (rewrite MS_ins_eq); apply refl_equal.
     repeat (rewrite MS_ins_diff); try (reflect DB_eq_imp; simpl; nat_simpl; apply Hnn');
      rewrite <- Hl; unfold MS_lift_; nat_simpl; apply refl_equal.
   destruct n' as [| n']; simpl; nat_simpl.
    case_eq (nat_le d n); intros Hdn; nat_simpl.
     rewrite Hdn; rewrite MS_ins_diff; [ | discriminate]; rewrite <- Hl; unfold MS_lift_; nat_simpl;
      apply refl_equal.
     reflect_in nat_le_imp Hdn;
      rewrite (imprf2 nat_le_imp _ _ (lt_not_le _ _ (lt_S _ _ (not_ge _ _ Hdn))));
      destruct n as [| n].
      rewrite MS_ins_eq; apply refl_equal.
      rewrite MS_ins_diff; [ | discriminate]; rewrite <- Hl; unfold MS_lift_; nat_simpl;
       apply refl_equal.
    case_eq (nat_eq n n'); intros Hnn'; simpl.
     reflect_in nat_eq_imp Hnn'; rewrite Hnn'; rewrite MS_ins_eq; case_eq (nat_le d n');
      intros Hdn'.
      rewrite MS_ins_eq; nat_simpl; rewrite Hdn'; apply refl_equal.
      reflect_in nat_le_imp Hdn';
       rewrite (imprf2 nat_le_imp _ _ (lt_not_le _ _ (lt_S _ _ (not_ge _ _ Hdn'))));
       rewrite MS_ins_diff.
       rewrite <- Hl; unfold MS_lift_; nat_simpl; rewrite (imprf2 nat_le_imp _ _ Hdn');
        apply refl_equal.
       reflect DB_eq_imp; simpl; nat_simpl; apply refl_equal.
     rewrite MS_ins_diff; [ | reflect DB_eq_imp; simpl; nat_simpl; apply Hnn'];
      case_eq (nat_le d n); intros Hdn; nat_simpl.
      rewrite MS_ins_diff; [ | reflect DB_eq_imp; simpl; nat_simpl; apply Hnn']; rewrite <- Hl;
       unfold MS_lift_; nat_simpl; apply refl_equal.
      reflect_in nat_le_imp Hdn;
       rewrite (imprf2 nat_le_imp _ _ (lt_not_le _ _ (lt_S _ _ (not_ge _ _ Hdn))));
       case_eq (nat_eq n (S n')); intros HnSn'.
       reflect_in nat_eq_imp HnSn'; rewrite HnSn' in *; rewrite MS_ins_eq;
        rewrite (imprf2 nat_le_imp _ _ (lt_not_le _ _ (lt_S_n _ _ (lt_S _ _ (not_ge _ _ Hdn)))));
        apply refl_equal.
       rewrite MS_ins_diff; [ | reflect DB_eq_imp; simpl; nat_simpl; apply HnSn'];
        case_eq (nat_le d n'); intros Hdn'; rewrite <- Hl; unfold MS_lift_; nat_simpl; rewrite Hdn';
        apply refl_equal.
   case_eq (nat_eq u u'); intros Huu'.
    reflect_in nat_eq_imp Huu'; rewrite Huu' in *; repeat (rewrite MS_ins_eq); apply refl_equal.
    repeat (rewrite MS_ins_diff); try (reflect DB_eq_imp; simpl; nat_simpl; bool_simpl; apply Huu');
     rewrite <- Hl; unfold MS_lift_; apply refl_equal.
   case_eq (nat_eq u u'); intros Huu'.
    reflect_in nat_eq_imp Huu'; rewrite Huu' in *; repeat (rewrite MS_ins_eq); apply refl_equal.
    repeat (rewrite MS_ins_diff); try (reflect DB_eq_imp; simpl; nat_simpl; bool_simpl; apply Huu');
     rewrite <- Hl; unfold MS_lift_; apply refl_equal.
   case_eq (DB_eq ¡(S u,S n) ¡(S u',S n')); intros Hii'.
    reflect_in DB_eq_imp Hii'; rewrite Hii' in *; repeat (rewrite MS_ins_eq); apply refl_equal.
    repeat (rewrite MS_ins_diff); try (reflect DB_eq_imp; apply Hii'); rewrite <- Hl;
     unfold MS_lift_; apply refl_equal.
   case_eq (DB_eq ¡(S u,S n) ¡(S u',S n')); intros Hii'.
    reflect_in DB_eq_imp Hii'; rewrite Hii' in *; repeat (rewrite MS_ins_eq); apply refl_equal.
    repeat (rewrite MS_ins_diff); try (reflect DB_eq_imp; apply Hii'); rewrite <- Hl;
     unfold MS_lift_; apply refl_equal.
Qed.
(* Nota: ugly, definitely not the most elegant proof, but sufficient enough. *)

Theorem Bt_mesubst_equiv_:forall (t:Τ)(m:Μ)(l:list Ι)(d:nat),
                          Bt_fesubst_ d (m⊲l) t=Bt_mesubst_ d (MS_framed m l) t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (m:Μ)(l:list Ι)(d:nat),
                            Bt_fesubst_ d (m⊲l) t=Bt_mesubst_ d (MS_framed m l) t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind m l d;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) m l d); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) m l d); irewrite (Hind _ (sdepth_r t1 t2) m l d);
       apply refl_equal).
  case_eq (DB_le d i'); intros Hdi'; simpl; [ | apply refl_equal].
   induction l as [| i l Hl]; simpl.
    unfold MS_nil; apply refl_equal.
    case_eq (DB_eq i' i); intros Hi'i; simpl; reflect_in DB_eq_imp Hi'i.
     rewrite Hi'i; rewrite MS_ins_eq; apply refl_equal.
     rewrite Hl; rewrite (MS_ins_diff (MS_framed m l) i' i (m i) (sym_not_equal Hi'i));
      apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) m l d);
   irewrite (Hind _ (sdepth_r t1 t2) (MS_lift_ d m) (LI_lift_ d l) (S d));
   irewrite (MS_eq_mesubst_ t2 _ _ (MS_framed_lift_ l m d) (S d)); apply refl_equal.
  irewrite (Hind _ (sdepth t) (MS_lift_ d m) (LI_lift_ d l) (S d));
   irewrite (MS_eq_mesubst_ t _ _ (MS_framed_lift_ l m d) (S d)); apply refl_equal.
Qed.

(* Well-foundedness ------------------------------------------------------------------------------*)

Theorem Bt_framed_ind:forall (t:Τ)(P:Τ->Type),
                      P t->
                      (forall (l:list Ι)(m:Μ), P 〈|(m⊲l)@t|〉->forall (i:Ι), P 〈|(m⊲cons i l)@t|〉)->
                      forall (l:list Ι)(m:Μ), P 〈|(m⊲l)@t|〉.
Proof.
 intros t P Ht Hind; induction l as [| i' l']; intros m;
 [rewrite (Bt_fesubst_nil t m); apply Ht | apply Hind; apply IHl'].
Qed.

Theorem Bt_restr_ind:forall (t:Τ)(P:Τ->Type),
                     P t->
                     (forall (m:Μ), P 〈〈m@t〉〉->forall (i:Ι)(e:Ε), P 〈〈≺i←e≻⊕m@t〉〉)->
                     forall (m:Μ), P 〈〈m@t〉〉.
Proof.
 intros t P Ht Hind m; destruct (Bt_fesubst_mesubst t) as [l Hl]; irewrite (Hl m).
 generalize m; clear m; apply (Bt_framed_ind t P Ht).
 intros l' m Hl' i'; rewrite Bt_mesubst_equiv_; simpl; apply Hind; rewrite <- Bt_mesubst_equiv_;
  apply Hl'.
Qed.

Theorem Bp_restr_ind:forall (p:Π)(P:Π->Type),
                     P p->
                     (forall (m:Μ), P 〈〈m@Π@p〉〉->forall (i:Ι)(e:Ε), P 〈〈≺i←e≻⊕m@Π@p〉〉)->
                     forall (m:Μ), P 〈〈m@Π@p〉〉.
Proof.
 intros p P Hp Hind m;
  apply (Bt_restr_ind p (fun (t:Τ) => match t with be_ _ => True | bp_ p => P p end) Hp Hind m).
Qed.

(* Semantical result -----------------------------------------------------------------------------*)

(*
Fixpoint Bp_mforall(l:list Ι)(p:Π){struct l}:Π :=
 match l with nil => p | i'::l' => ∀(i'∙Bp_mforall l' p) end.
Notation "'∀*(' l '∙' p ')'" := (Bp_mforall l p).

Theorem Bp_mforall_free:forall (l:list Ι)(p:Π)(i:Ι), (i∖p)->(i∖∀*(l∙p)).
Proof.
 induction l as [| i' l' Hl']; intros p i Hip; simpl.
  apply Hip.
  apply (Bt_abstr_free_free_ ∀*(l'∙p) 0 i i'); apply Hl'; apply Hip.
Qed.

Theorem Bp_mforall_gnd:forall (l:list Ι)(G:Γ)(p:Π),
                       (forall (i:Ι), (LI_mem l i=⊥) + (i∖p) + (i∖ˠG))->G⊢p->G⊢∀*(l∙p).
Proof.
 induction l as [| i' l' Hl']; simpl; intros G p Hgnd Hp.
  apply Hp.
  destruct (Hgnd i') as [[Hi'l' | Hi'p] | Hi'G].
   DB_simpl; inversion Hi'l'.
   apply cb_for; intros i; irewrite (Bt_esubst_free _ _ (Bp_mforall_free l' p i' Hi'p) ∂(i));
    clear i; apply Hl'.
    intros i; destruct (Hgnd i) as [[Hii'l' | Hip] | HiG].
     destruct (orb_false_elim _ _ Hii'l') as [_ Hil']; left; left; apply Hil'.
     left; right; apply Hip.
     right; apply HiG.
    apply Hp.
   apply Bpr_fori; Btac_free; apply Hl'.
    intros i; destruct (Hgnd i) as [[Hii'l' | Hip] | HiG].
     destruct (orb_false_elim _ _ Hii'l') as [_ Hil']; left; left; apply Hil'.
     left; right; apply Hip.
     right; apply HiG.
    apply Hp.
Qed.

Theorem Bp_mforall_app:forall (l:list Ι)(i:Ι)(G:Γ)(p:Π), G⊢∀*(i::l∙p)->
                       forall (e:Ε), G⊢〈i←e@Π@∀*(l∙p)〉.
Proof.
 intros l i G p Hil e; simpl in Hil; apply Bpr_fore; apply Hil.
Qed.
*)

Theorem Bt_fesubst_cons_:forall (t:Τ)(l:list Ι)(i f:Ι)(m:Μ)(d:nat), ≤Ι(d,i)->≤Ι(d,f)->
                         Bt_free_ d f t=⊥->
                         (forall (i':Ι), LI_mem l i'=⊤->Be_free_ d f (m i')=⊥)->
                         ((LI_mem l f=⊥) + (m f=∂(f)))->
                         Bt_fesubst_ d (m⊲i::l) t=
                         Bt_esubst_ d f (Bt_fesubst_ d (m⊲l) (Bt_esubst_ d i t ∂(f))) (m i).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l:list Ι)(i f:Ι)(m:Μ)(d:nat), ≤Ι(d,i)->≤Ι(d,f)->
                            Bt_free_ d f t=⊥->
                            (forall (i':Ι), LI_mem l i'=⊤->Be_free_ d f (m i')=⊥)->
                            ((LI_mem l f=⊥) + (m f=∂(f)))->
                            Bt_fesubst_ d (m⊲i::l) t=
                            Bt_esubst_ d f (Bt_fesubst_ d (m⊲l) (Bt_esubst_ d i t ∂(f))) (m i)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind l i f m d Hdi Hdf Hft Hf H'f;
  try (apply refl_equal);
  try (irewrite (Hind _ (sdepth t) _ _ _ _ _ Hdi Hdf Hft Hf H'f); apply refl_equal);
  try (destruct (orb_false_elim _ _ Hft) as [Hft1 Hft2];
       irewrite (Hind _ (sdepth_l t1 t2) _ _ _ _ _ Hdi Hdf Hft1 Hf H'f);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ _ _ _ Hdi Hdf Hft2 Hf H'f); apply refl_equal).
  simpl in *; case_eq (DB_le d i'); intros Hdi'; rewrite Hdi' in *; bool_simpl.
   case_eq (DB_eq i i'); intros Hii'; reflect_in DB_eq_imp Hii'.
    rewrite Hii' in *; DB_simpl; bool_simpl; unfold Be_fesubst_; rewrite Hdf; simpl;
     destruct H'f as [H'f | H'f]; rewrite H'f; bool_simpl; simpl; DB_simpl; rewrite Hdf; DB_simpl;
     apply refl_equal.
    rewrite (imprf2 DB_eq_imp _ _ (sym_not_eq Hii')); simpl; rewrite Hdi'; simpl;
     case_eq (LI_mem l i'); intros Hli'.
     apply (sym_equal (Bt_esubst_free_ (m i') _ _ (Hf _ Hli') (m i))).
     simpl; rewrite Hdi'; rewrite Hft; apply refl_equal.
   simpl; rewrite Hdi'; simpl; rewrite Hdi'; simpl; apply refl_equal.
  destruct (orb_false_elim _ _ Hft) as [Hft1 Hft2]; fold Bp_free_ Be_free_ in Hft1,Hft2;
   irewrite (Hind _ (sdepth_l t1 t2) _ _ _ _ _ Hdi Hdf Hft1 Hf H'f); rewrite <- (DB_le_lift) in Hdi;
   rewrite <- (DB_le_lift) in Hdf.
  assert (Hf2:forall (i':Ι),
              LI_mem (LI_lift_ d l) i'=⊤->Be_free_ (S d) (DB_lift_ d f) (MS_lift_ d m i')=⊥).
   intros [[| u'] n'] Hli'; simpl in *; nat_simpl; simpl.
    destruct n' as [| n']; simpl in *; nat_simpl; simpl.
     apply refl_equal.
     case_eq (nat_le d n'); intros Hdn'.
      srewrite (Bt_lift_free_ (m ¡(0,n')) f d); apply Hf; rewrite <- (LI_lift_mem_ l ¡(0,n') d);
       unfold DB_lift_; rewrite Hdn'; apply Hli'.
      simpl; nat_simpl; rewrite Hdn'; apply refl_equal.
    srewrite (Bt_lift_free_ (m ¡(S u',n')) f d); apply Hf; rewrite <- (LI_lift_mem_ l ¡(S u',n') d);
     apply Hli'.
  assert (H'f2:(LI_mem (LI_lift_ d l) (DB_lift_ d f)=⊥) + 
               (MS_lift_ d m (DB_lift_ d f)=∂(DB_lift_ d f))).
   destruct H'f as [H'f | H'f].
    left; rewrite (LI_lift_mem_ l f d); apply H'f.
    right; destruct f as [[| u] n]; [destruct n as [| n] | ]; simpl in *.
     destruct d; simpl in *; nat_simpl; [rewrite H'f; simpl; nat_simpl | ]; apply refl_equal.
     case_eq (nat_le d (S n)); intros HdSn; rewrite HdSn in *; nat_simpl.
      rewrite H'f; simpl; rewrite HdSn; apply refl_equal.
      reflect_in nat_le_imp Hdf; rewrite (imppt2 nat_le_imp _ _ (le_S _ _ Hdf)) in HdSn;
       inversion HdSn.
     rewrite H'f; simpl; apply refl_equal.
  irewrite (Hind _ (sdepth_r t1 t2) _ (DB_lift_ d i) _ _ _ Hdi Hdf Hft2 Hf2 H'f2);
   rewrite DB_le_lift in Hdi; rewrite (MS_get_lift_ m i d Hdi); apply refl_equal.
  rewrite <- (DB_le_lift) in Hdi; rewrite <- (DB_le_lift) in Hdf.
  assert (Hf2:forall (i':Ι),
              LI_mem (LI_lift_ d l) i'=⊤->Be_free_ (S d) (DB_lift_ d f) (MS_lift_ d m i')=⊥).
   intros [[| u'] n'] Hli'; simpl in *; nat_simpl; simpl.
    destruct n' as [| n']; simpl in *; nat_simpl; simpl.
     apply refl_equal.
     case_eq (nat_le d n'); intros Hdn'.
      srewrite (Bt_lift_free_ (m ¡(0,n')) f d); apply Hf; rewrite <- (LI_lift_mem_ l ¡(0,n') d);
       unfold DB_lift_; rewrite Hdn'; apply Hli'.
      simpl; nat_simpl; rewrite Hdn'; apply refl_equal.
    srewrite (Bt_lift_free_ (m ¡(S u',n')) f d); apply Hf; rewrite <- (LI_lift_mem_ l ¡(S u',n') d);
     apply Hli'.
  assert (H'f2:(LI_mem (LI_lift_ d l) (DB_lift_ d f)=⊥) + 
               (MS_lift_ d m (DB_lift_ d f)=∂(DB_lift_ d f))).
   destruct H'f as [H'f | H'f].
    left; rewrite (LI_lift_mem_ l f d); apply H'f.
    right; destruct f as [[| u] n]; [destruct n as [| n] | ]; simpl in *.
     destruct d; simpl in *; nat_simpl; [rewrite H'f; simpl; nat_simpl | ]; apply refl_equal.
     case_eq (nat_le d (S n)); intros HdSn; rewrite HdSn in *; nat_simpl.
      rewrite H'f; simpl; rewrite HdSn; apply refl_equal.
      reflect_in nat_le_imp Hdf; rewrite (imppt2 nat_le_imp _ _ (le_S _ _ Hdf)) in HdSn;
       inversion HdSn.
     rewrite H'f; simpl; apply refl_equal.
  irewrite (Hind _ (sdepth t) _ (DB_lift_ d i) _ _ _ Hdi Hdf Hft Hf2 H'f2);
   rewrite DB_le_lift in Hdi; rewrite (MS_get_lift_ m i d Hdi); apply refl_equal.
Qed.

Theorem Bt_fesubst_cons:forall (t:Τ)(l:list Ι)(i f:Ι)(m:Μ),
                        (f∖t)->(forall (i':Ι), LI_mem l i'=⊤->(f∖m i'))->
                        ((LI_mem l f=⊥) + (m f=∂(f)))->〈|(m⊲i::l)@t|〉=〈f←m i@〈|(m⊲l)@〈i←∂(f)@t〉|〉〉.
Proof.
 intros t l i f m Hft Hf H'f;
  apply (Bt_fesubst_cons_ t l i f m 0 (DB_le_0 _) (DB_le_0 _) Hft Hf H'f).
Qed.

Lemma ultimate_fshun:
 forall (G:Γ)(t:Τ)(l:list Ι)(m:Μ),
 sigS (fun (u:nat) =>
       (forall (u' n':nat),u<=u'->(¡(u',n')∖t)*(¡(u',n')∖ˠG)*(LI_mem l ¡(u',n')=⊥))*
       (forall (i':Ι), LI_mem l i'=⊤->forall (u' n':nat), u<=u'->(¡(u',n')∖m i')))%type.
Admitted.
(* Nota: trivial, but awkward. *)

Theorem Bt_fesubst_free_:forall (t:Τ)(l:list Ι)(m:Μ)(i:Ι)(d:nat),
                         Bt_free_ d i t=⊥->Bt_fesubst_ d (m⊲i::l) t=Bt_fesubst_ d (m⊲l) t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l:list Ι)(m:Μ)(i:Ι)(d:nat),
                            Bt_free_ d i t=⊥->Bt_fesubst_ d (m⊲i::l) t=Bt_fesubst_ d (m⊲l) t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind l m i d Ht;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) l m _ _ Ht); apply refl_equal);
  try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2];
       irewrite (Hind _ (sdepth_l t1 t2) l m _ _ Ht1);
       irewrite (Hind _ (sdepth_r t1 t2) l m _ _ Ht2); apply refl_equal).
  simpl in *; destruct (DB_le d i'); simpl in *; [ | apply refl_equal]; destruct (LI_mem l i');
   bool_simpl; [apply refl_equal | ]; reflect_in DB_eq_imp Ht;
   rewrite (imprf2 DB_eq_imp _ _ (sym_not_eq Ht)); apply refl_equal.
  destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2]; irewrite (Hind _ (sdepth_l t1 t2) l m _ _ Ht1);
   irewrite (Hind _ (sdepth_r t1 t2) (LI_lift_ d l) (MS_lift_ d m) _ _ Ht2); apply refl_equal.
  irewrite (Hind _ (sdepth t) (LI_lift_ d l) (MS_lift_ d m) _ _ Ht); apply refl_equal.
Qed.

Theorem Bt_fesubst_free:forall (t:Τ)(l:list Ι)(m:Μ)(i:Ι), (i∖t)->〈|(m⊲i::l)@t|〉=〈|(m⊲l)@t|〉.
Proof.
 intros t l m i Hit; apply (Bt_fesubst_free_ t l m i 0 Hit).
Qed.

Theorem Bt_fesubst_mem_:forall (t:Τ)(l:list Ι)(i:Ι)(m:Μ)(d:nat), LI_mem l i=⊤->
                        Bt_fesubst_ d (m⊲i::l) t=Bt_fesubst_ d (m⊲l) t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l:list Ι)(i:Ι)(m:Μ)(d:nat), LI_mem l i=⊤->
                            Bt_fesubst_ d (m⊲i::l) t=Bt_fesubst_ d (m⊲l) t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind l i m d Hli;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ _ m d Hli); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) _ _ m d Hli);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ m d Hli); apply refl_equal).
  simpl; destruct (DB_le d i'); simpl; [ | apply refl_equal]; case_eq (DB_eq i' i); intros Hi'i;
   simpl; [reflect_in DB_eq_imp Hi'i; rewrite Hi'i in *; rewrite Hli | ]; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) _ _ m d Hli); rewrite <- (LI_lift_mem_ l i d) in Hli;
   irewrite (Hind _ (sdepth_r t1 t2) _ _ (MS_lift_ d m) (S d) Hli); apply refl_equal.
  rewrite <- (LI_lift_mem_ l i d) in Hli; irewrite (Hind _ (sdepth t) _ _ (MS_lift_ d m) (S d) Hli);
   apply refl_equal.
Qed.

Theorem Bt_fesubst_mem:forall (t:Τ)(l:list Ι)(i:Ι)(m:Μ), LI_mem l i=⊤->〈|(m⊲i::l)@t|〉=〈|(m⊲l)@t|〉.
Proof.
 intros t l i m Hli; apply (Bt_fesubst_mem_ t l i m 0 Hli).
Qed.

Theorem Bt_fesubst_ins_:forall (t:Τ)(l1 l2:list Ι)(i:Ι)(m:Μ)(d:nat),
                       Bt_fesubst_ d (m⊲l1) t=Bt_fesubst_ d (m⊲l2) t->
                       Bt_fesubst_ d (m⊲i::l1) t=Bt_fesubst_ d (m⊲i::l2) t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l1 l2:list Ι)(i:Ι)(m:Μ)(d:nat),
                            Bt_fesubst_ d (m⊲l1) t=Bt_fesubst_ d (m⊲l2) t->
                            Bt_fesubst_ d (m⊲i::l1) t=Bt_fesubst_ d (m⊲i::l2) t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind l1 l2 i m d Ht;
  try (apply refl_equal);
  try (cut (Bt_fesubst_ d (m⊲l1) t=Bt_fesubst_ d (m⊲l2) t);
       [intros H't; irewrite (Hind _ (sdepth t) l1 l2 i m d H't) |
        simpl in *; injection Ht; intros H't; rewrite H't]; apply refl_equal);
  try (cut (Bt_fesubst_ d (m⊲l1) t1=Bt_fesubst_ d (m⊲l2) t1);
       [intros Ht1; irewrite (Hind _ (sdepth_l t1 t2) l1 l2 i m d Ht1);
        cut (Bt_fesubst_ d (m⊲l1) t2=Bt_fesubst_ d (m⊲l2) t2);
       [intros Ht2; irewrite (Hind _ (sdepth_r t1 t2) l1 l2 i m d Ht2); apply refl_equal |
        simpl in Ht; injection Ht; intros Ht2 _; srewrite Ht2; apply refl_equal] |
        simpl in Ht; injection Ht; intros _ Ht1; srewrite Ht1; apply refl_equal]).
  simpl in *; destruct (DB_le d i'); simpl in *; [ | apply refl_equal]; destruct (DB_eq i' i);
   simpl in *; [apply refl_equal | apply Ht].
  cut (Bt_fesubst_ d (m⊲l1) t1=Bt_fesubst_ d (m⊲l2) t1).
   intros Ht1; irewrite (Hind _ (sdepth_l t1 t2) l1 l2 i m d Ht1);
    cut (Bt_fesubst_ (S d) ((MS_lift_ d m)⊲(LI_lift_ d l1)) t2=
         Bt_fesubst_ (S d) ((MS_lift_ d m)⊲(LI_lift_ d l2)) t2).
    intros Ht2; irewrite (Hind _ (sdepth_r t1 t2) (LI_lift_ d l1) (LI_lift_ d l2)
                              (DB_lift_ d i) (MS_lift_ d m) (S d) Ht2); apply refl_equal.
    simpl in Ht; injection Ht; intros Ht2 _; srewrite Ht2; apply refl_equal.
   simpl in Ht; injection Ht; intros _ Ht1; srewrite Ht1; apply refl_equal.
  cut (Bt_fesubst_ (S d) ((MS_lift_ d m)⊲(LI_lift_ d l1)) t=
       Bt_fesubst_ (S d) ((MS_lift_ d m)⊲(LI_lift_ d l2)) t).
   intros H't; irewrite (Hind _ (sdepth t) (LI_lift_ d l1) (LI_lift_ d l2)
                                (DB_lift_ d i) (MS_lift_ d m) (S d) H't); apply refl_equal.
   simpl in *; injection Ht; intros H't; rewrite H't; apply refl_equal.
Qed.

Theorem Bt_fesubst_ins:forall (t:Τ)(l1 l2:list Ι)(i:Ι)(m:Μ),
                       〈|(m⊲l1)@t|〉=〈|(m⊲l2)@t|〉->〈|(m⊲i::l1)@t|〉=〈|(m⊲i::l2)@t|〉.
Proof.
 intros t l1 l2 i m Ht; apply (Bt_fesubst_ins_ t l1 l2 i m 0 Ht).
Qed.

Theorem Bt_fesubst_injec:forall (l:list Ι)(t:Τ)(m:Μ), 〈|(m⊲l)@t|〉=〈|(m⊲LI_inject l)@t|〉.
Proof.
 induction l as [| i l Hl]; intros t m; simpl.
  apply refl_equal.
  case_eq (LI_mem l i); intros Hli; [rewrite (Bt_fesubst_mem t l i m Hli) | apply Bt_fesubst_ins];
   apply Hl.
Qed.

Theorem Bt_fesubst_lift_:forall (t t':Τ)(l:list Ι)(m:Μ)(d:nat), Bt_fesubst_ d (m⊲l) t=t'->
                         Bt_fesubst_ (S d) (MS_lift_ d m⊲LI_lift_ d l) (Bt_lift_ d t)=Bt_lift_ d t'.
Proof.
 intros t t' l m d Hd; rewrite Bt_mesubst_equiv_;
  rewrite <- (MS_eq_mesubst_ (Bt_lift_ d t) _ _ (MS_framed_lift_ l m d) (S d));
  rewrite MS_lift_distr; rewrite <- Bt_mesubst_equiv_; rewrite Hd; apply refl_equal.
Qed.

Theorem Bt_fesubst_comm_:forall (t:Τ)(l:list Ι)(i:Ι)(e:Ε)(m:Μ)(d:nat),
                         LI_mem l i=⊥->Bt_fesubst_ d (m⊲l) e=e->
                         (forall (i':Ι), LI_mem l i'=⊤->Bt_free_ d i (m i')=⊥)->
                         Bt_fesubst_ d (m⊲l) (Bt_esubst_ d i t e)=
                         Bt_esubst_ d i (Bt_fesubst_ d (m⊲l) t) e.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l:list Ι)(i:Ι)(e:Ε)(m:Μ)(d:nat),
                            LI_mem l i=⊥->Bt_fesubst_ d (m⊲l) e=e->
                            (forall (i':Ι), LI_mem l i'=⊤->Bt_free_ d i (m i')=⊥)->
                            Bt_fesubst_ d (m⊲l) (Bt_esubst_ d i t e)=
                            Bt_esubst_ d i (Bt_fesubst_ d (m⊲l) t) e));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind l i e m d Hli He Hmi;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ _ _ _ _ Hli He Hmi); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) _ _ _ _ _ Hli He Hmi);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ _ _ _ Hli He Hmi); apply refl_equal).
  simpl in *; case_eq (DB_le d i'); intros Hdi'; simpl.
   case_eq (DB_eq i i'); intros Hii'; simpl.
    reflect_in DB_eq_imp Hii'; rewrite Hii' in *; rewrite Hli; rewrite He; simpl; rewrite Hdi';
     DB_simpl; apply refl_equal.
    rewrite Hdi'; simpl; case_eq (LI_mem l i'); intros Hli'; simpl.
     apply (sym_equal (Bt_esubst_free_ (m i') i d (Hmi _ Hli') e)).
     rewrite Hdi'; rewrite Hii'; apply refl_equal.
   rewrite Hdi'; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) _ _ _ _ _ Hli He Hmi); rewrite <- (LI_lift_mem_ l i d) in Hli.
  assert (H'mi:forall (i':Ι),
          LI_mem (LI_lift_ d l) i'=⊤->Bt_free_ (S d) (DB_lift_ d i) (MS_lift_ d m i')=⊥).
   intros [[| u'] n'] Hli'; [destruct n' as [| n'] | ]; simpl in *; nat_simpl.
    apply refl_equal.
    case_eq (nat_le d n'); intros Hdn'; simpl in *.
     irewrite (Bt_lift_free_ (m ¡(0,n')) i d); replace ¡(0,S n') with (DB_lift_ d ¡(0,n')) in Hli'.
      rewrite LI_lift_mem_ in Hli'; apply (Hmi _ Hli').
      simpl; rewrite Hdn'; apply refl_equal.
     nat_simpl; rewrite Hdn'; apply refl_equal.
    irewrite (Bt_lift_free_ (m ¡(S u',n')) i d);
     change ¡(S u',n') with (DB_lift_ d ¡(S u',n')) in Hli'; rewrite LI_lift_mem_ in Hli';
     apply (Hmi _ Hli').
  irewrite (Hind _ (sdepth_r t1 t2) _ _ _ _ _ Hli (Bt_fesubst_lift_ _ _ _ _ _ He) H'mi);
   apply refl_equal.
  rewrite <- (LI_lift_mem_ l i d) in Hli.
  assert (H'mi:forall (i':Ι),
          LI_mem (LI_lift_ d l) i'=⊤->Bt_free_ (S d) (DB_lift_ d i) (MS_lift_ d m i')=⊥).
   intros [[| u'] n'] Hli'; [destruct n' as [| n'] | ]; simpl in *; nat_simpl.
    apply refl_equal.
    case_eq (nat_le d n'); intros Hdn'; simpl in *.
     irewrite (Bt_lift_free_ (m ¡(0,n')) i d); replace ¡(0,S n') with (DB_lift_ d ¡(0,n')) in Hli'.
      rewrite LI_lift_mem_ in Hli'; apply (Hmi _ Hli').
      simpl; rewrite Hdn'; apply refl_equal.
     nat_simpl; rewrite Hdn'; apply refl_equal.
    irewrite (Bt_lift_free_ (m ¡(S u',n')) i d);
     change ¡(S u',n') with (DB_lift_ d ¡(S u',n')) in Hli'; rewrite LI_lift_mem_ in Hli';
     apply (Hmi _ Hli').
  irewrite (Hind _ (sdepth t) _ _ _ _ _ Hli (Bt_fesubst_lift_ _ _ _ _ _ He) H'mi); apply refl_equal.
Qed.

Theorem Bt_fesubst_comm:forall (t:Τ)(l:list Ι)(i:Ι)(e:Ε)(m:Μ),
                        LI_mem l i=⊥->〈|(m⊲l)@e|〉=e->(forall (i':Ι), LI_mem l i'=⊤->(i∖m i'))->
                        〈|(m⊲l)@〈i←e@t〉|〉=〈i←e@〈|(m⊲l)@t|〉〉.
Proof.
 intros t l i e m Hli He Hmi; apply (Bt_fesubst_comm_ t l i e m 0 Hli He Hmi).
Qed.

Theorem Bpr_ground_framed:forall (l:list Ι)(m:Μ)(G:Γ)(p:Π),
                          (forall (i:Ι), (LI_mem l i=⊥) + ((i∖p) + (i∖ˠG)))->
                          G⊢p->G⊢〈|(m⊲l)@Π@p|〉.
Proof.
 induction l as [| i l Hl]; intros m G p Hgnd Hp.
  irewrite (Bt_fesubst_nil p m); apply Hp.
  irewrite (Bt_fesubst_injec (i::l) p m).
  case_eq (LI_mem l i); intros Hli.
   irewrite (sym_equal (Bt_fesubst_injec l p m)); apply Hl; [ | apply Hp]; intros i';
    destruct (Hgnd i') as [Hi'l | Hi'pG]; simpl in *.
    left; case_eq (DB_eq i' i); intros Hi'i; rewrite Hi'i in *; [inversion Hi'l | apply Hi'l].
    right; apply Hi'pG.
   destruct (ultimate_fshun G p (i::l) m) as [u [HupGl Hum]]; set (f:=¡(S u,0)).
   generalize (fst (fst (HupGl _ 0 (le_n_Sn _)))); fold f; intros Hfp.
   generalize (snd (fst (HupGl _ 0 (le_n_Sn _)))); fold f; intros HfG.
   destruct (orb_false_elim _ _ (snd (HupGl _ 0 (le_n_Sn _)))) as [Hif Hlf]; fold LI_mem in Hlf;
    fold f in Hif,Hlf.
   assert (Hflm:forall (i':Ι), LI_mem (LI_inject l) i'=⊤->(f∖m i')).
    intros i' Hli'; generalize (Hum i'); simpl; intros H'um; case_eq (DB_eq i' i); intros Hi'i;
     rewrite Hi'i in *.
     apply (H'um (refl_equal _) _ 0 (le_n_Sn _)).
     rewrite <- LI_inject_mem in *; apply (H'um Hli' _ 0 (le_n_Sn _)).
   assert (H'flm:(LI_mem (LI_inject l) f=⊥)+(m f=∂(f))).
    left; rewrite <- LI_inject_mem; apply Hlf.
   irewrite (Bt_fesubst_cons p (LI_inject l) i f m Hfp Hflm H'flm).
   apply Bpr_fore; apply Bpr_fori; Btac_free;
    irewrite (sym_equal (Bt_fesubst_injec l 〈i←∂(f)@Π@p〉 m)); apply Hl.
    intros i'; case_eq (LI_mem l i'); intros Hli'; [ | left; apply refl_equal].
     case_eq (Bg_free i' G); intros HGi'; [ | right; right; apply refl_equal].
      destruct (Hgnd i') as [Hi'l | [Hi'p | Hi'G]]; simpl in *;
      [rewrite Hli' in Hi'l; bool_simpl; inversion Hi'l | |
       rewrite HGi' in Hi'G; inversion Hi'G].
       right; left; Btac_free; rewrite LI_inject_mem in Hli'; case_eq (DB_eq i' f); intros Hi'f.
        reflect_in DB_eq_imp Hi'f; rewrite Hi'f in *; rewrite <- LI_inject_mem in Hli';
         generalize (snd (HupGl _ 0 (le_n_Sn _))); fold f; rewrite Hli'; bool_simpl; intros H;
         inversion H.
        apply (Bt_free_esubst' p i' i ∂(f)); [apply Hi'f | apply Hi'p].
    destruct (Hgnd i) as [Hil | [Hip | HiG]].
     simpl in Hil; DB_simpl; inversion Hil.
     irewrite (Bt_esubst_free p i Hip ∂(f)); apply Hp.
     apply Bpr_fore; apply Bpr_fori; [apply Hp | Btac_free].
Qed.

Theorem Bt_fesubst_light_:forall (t:Τ)(l1 l2:list Ι)(m:Μ)(d:nat),
                          (forall (i:Ι), (LI_mem l1 i=LI_mem l2 i) + (m i=∂(i)))->
                          Bt_fesubst_ d (m⊲l1) t=Bt_fesubst_ d (m⊲l2) t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l1 l2:list Ι)(m:Μ)(d:nat),
                            (forall (i:Ι), (LI_mem l1 i=LI_mem l2 i) + (m i=∂(i)))->
                            Bt_fesubst_ d (m⊲l1) t=Bt_fesubst_ d (m⊲l2) t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind l1 l2 m d Hm;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ _ _ d Hm); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) _ _ _ d Hm);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ _ d Hm); apply refl_equal).
  simpl; destruct (DB_le d i'); simpl; [ | apply refl_equal]; destruct (Hm i') as [Hli' | Hmi'];
  [rewrite Hli' | rewrite Hmi'; bool_simpl]; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) _ _ _ d Hm).
  assert (H'm:forall (i:Ι), (LI_mem (LI_lift_ d l1) i=LI_mem (LI_lift_ d l2) i) +
                            (MS_lift_ d m i = ∂(i))).
   intros [[| u] n]; [destruct n as [| n] | ]; simpl in *.
    right; apply refl_equal.
    case_eq (nat_le d n); intros Hdn.
     destruct (Hm ¡(0,n)) as [Hln | Hmn].
      left; replace ¡(0,S n) with (DB_lift_ d ¡(0,n)); [ | simpl; rewrite Hdn; apply refl_equal];
       repeat (rewrite LI_lift_mem_); apply Hln.
      right; rewrite Hmn; simpl; rewrite Hdn; apply refl_equal.
     right; apply refl_equal.
    destruct (Hm ¡(S u,n)) as [HlSun | HmSun].
     left; change ¡(S u,n) with (DB_lift_ d ¡(S u,n)); repeat (rewrite LI_lift_mem_); apply HlSun.
     right; rewrite HmSun; apply refl_equal.
  irewrite (Hind _ (sdepth_r t1 t2) _ _ _ (S d) H'm); apply refl_equal.
  assert (H'm:forall (i:Ι), (LI_mem (LI_lift_ d l1) i=LI_mem (LI_lift_ d l2) i) +
                            (MS_lift_ d m i = ∂(i))).
   intros [[| u] n]; [destruct n as [| n] | ]; simpl in *.
    right; apply refl_equal.
    case_eq (nat_le d n); intros Hdn.
     destruct (Hm ¡(0,n)) as [Hln | Hmn].
      left; replace ¡(0,S n) with (DB_lift_ d ¡(0,n)); [ | simpl; rewrite Hdn; apply refl_equal];
       repeat (rewrite LI_lift_mem_); apply Hln.
      right; rewrite Hmn; simpl; rewrite Hdn; apply refl_equal.
     right; apply refl_equal.
    destruct (Hm ¡(S u,n)) as [HlSun | HmSun].
     left; change ¡(S u,n) with (DB_lift_ d ¡(S u,n)); repeat (rewrite LI_lift_mem_); apply HlSun.
     right; rewrite HmSun; apply refl_equal.
  irewrite (Hind _ (sdepth t) _ _ _ (S d) H'm); apply refl_equal.
Qed.

Theorem Bt_fesubst_light:forall (t:Τ)(l1 l2:list Ι)(m:Μ),
                         (forall (i:Ι), (LI_mem l1 i=LI_mem l2 i) + (m i=∂(i)))->
                         〈|(m⊲l1)@t|〉=〈|(m⊲l2)@t|〉.
Proof.
 intros t l1 l2 m Hm; apply (Bt_fesubst_light_ t l1 l2 m 0 Hm).
Qed.

Fixpoint LI_light(l:list Ι)(m:Μ):list Ι :=
 match l with
 | nil => nil
 | i'::l' => if Be_eq (m i') ∂(i') then LI_light l' m else i'::(LI_light l' m)
 end.

Theorem Bt_fesubst_LI_light:forall (t:Τ)(l:list Ι)(m:Μ),
                            〈|(m⊲l)@t|〉=〈|(m⊲LI_light l m)@t|〉.
Proof.
 intros t l m; apply Bt_fesubst_light; intros i; induction l as [| i' l' Hl']; simpl.
  left; apply refl_equal.
  destruct Hl' as [Hl' | Hl']; [ | right; apply Hl'].
   case_eq (DB_eq i i'); intros Hii'; simpl.
    reflect_in DB_eq_imp Hii'; rewrite Hii' in *; case_eq (Be_eq (m i') ∂(i')); intros Hmi'; simpl.
     right; reflect Be_eq_imp; apply Hmi'.
     left; DB_simpl; apply refl_equal.
    left; case_eq (Be_eq (m i') ∂(i')); intros Hmi'; simpl; [ | rewrite Hii']; apply Hl'.
Qed.

Theorem LI_light_light:forall (l:list Ι)(m:Μ)(i:Ι), m i=∂(i)->LI_mem (LI_light l m) i=⊥.
Proof.
 induction l as [| i' l' Hl']; intros m i Hmi; simpl.
  apply refl_equal.
  case_eq (Be_eq (m i') ∂(i')); intros Hmi'; simpl.
   apply (Hl' _ _ Hmi).
   case_eq (DB_eq i i'); intros Hii'; simpl.
    reflect_in DB_eq_imp Hii'; rewrite Hii' in *; rewrite Hmi in Hmi'; reflect_in Be_eq_imp Hmi';
     destruct Hmi'; apply refl_equal.
     apply (Hl' _ _ Hmi).
Qed.

Theorem Bpr_ground:forall (p:Π)(m:Μ)(G:Γ),
                   (forall (i:Ι), (m i=∂(i)) + ((i∖p) + (i∖ˠG)))->G⊢p->G⊢〈〈m@Π@p〉〉.
Proof.
 intros p m G Hgnd Hp.
 destruct (Bt_fesubst_mesubst p) as [l Hl]; irewrite (Hl m).
 irewrite (Bt_fesubst_LI_light p l m).
 apply (Bpr_ground_framed (LI_light l m) m G p) with (2:=Hp).
 intros i; destruct (Hgnd i) as [Him | HipG].
  left; apply (LI_light_light l m i Him).
  right; apply HipG.
Qed.

(*================================================================================================*)