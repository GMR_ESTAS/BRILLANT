(*==================================================================================================
  Project : BiCoq3
  Module : index.v
  This module defines De Bruijn indexes. We associate to De Bruijn indexes a natural value to
  indicate a so-called namespace. 
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Oct 2008, Md: Mar 2009
  ================================================================================================*)

Require Export basic_nat.
Require Export Le.
Require Export Compare_dec.

(* Parameterised De Bruijn indexes ---------------------------------------------------------------*)

Inductive DBi:Set := DBi_:nat->nat->DBi.
Notation "'Ι'":= (DBi).
Notation "'¡(' u ',' n ')'" := (DBi_ u n).

(* Equality --------------------------------------------------------------------------------------*)

Definition DB_eq(i1 i2:Ι):Β :=
 match i1 with ¡(u1,n1) => match i2 with ¡(u2,n2) => (nat_eq u1 u2) && (nat_eq n1 n2) end end.

Theorem DB_eq_imp:((fun x y:Ι=>x=y)⇝²DB_eq).
Proof.
 unfold implements2; split; unfold DB_eq; intros [u1 n1] [u2 n2] Hf.
  destruct (andb_prop _ _ Hf) as [Hu Hn]; reflect_in nat_eq_imp Hn; rewrite Hn;
   reflect_in nat_eq_imp Hu; rewrite Hu; apply refl_equal.
  injection; intros Hn Hu; destruct (andb_false_elim _ _ Hf) as [Hu' | Hn'].
   rewrite Hu in Hu'; reflect_in nat_eq_imp Hu'; apply Hu'; apply refl_equal.
   rewrite Hn in Hn'; reflect_in nat_eq_imp Hn'; apply Hn'; apply refl_equal.
Qed.

Theorem DB_eq_refl:forall (i:Ι), DB_eq i i=⊤.
Proof.
 intros i; reflect DB_eq_imp; apply refl_equal.
Qed.

(* Lift ------------------------------------------------------------------------------------------*)

Definition DB_lift_(d:nat)(i:Ι):Ι :=
 match i with ¡(u,n') => ¡(u,if eq_0 u && nat_le d n' then S n' else n') end.
Notation "'↥(' i ')'" := (DB_lift_ 0 i).

Theorem DB_eq_DB_lift:forall (i1 i2:Ι)(d:nat), DB_eq (DB_lift_ d i1) (DB_lift_ d i2)=DB_eq i1 i2.
Proof.
 intros [[| u1] n1] [[| u2] n2] d; simpl; nat_simpl; bool_simpl; try (apply refl_equal).
  case_eq (nat_le d n1); intros Hdn1; case_eq (nat_le d n2); intros Hdn2; nat_simpl;
   try (apply refl_equal); reflect_in nat_le_imp Hdn1; reflect_in nat_le_imp Hdn2.
   case_eq (nat_eq (S n1) n2); intros HSn1n2; case_eq (nat_eq n1 n2); intros Hn1n2;
    try (apply refl_equal); reflect_in nat_eq_imp HSn1n2; reflect_in nat_eq_imp Hn1n2.
    rewrite <- HSn1n2 in Hdn2; destruct (Hdn2 (le_S _ _ Hdn1)).
    rewrite Hn1n2 in Hdn1; destruct (Hdn2 Hdn1).
   case_eq (nat_eq n1 (S n2)); intros Hn1Sn2; case_eq (nat_eq n1 n2); intros Hn1n2;
    try (apply refl_equal); reflect_in nat_eq_imp Hn1Sn2; reflect_in nat_eq_imp Hn1n2.
    rewrite Hn1Sn2 in Hdn1; destruct (Hdn1 (le_S _ _ Hdn2)).
    rewrite Hn1n2 in Hdn1; destruct (Hdn1 Hdn2).
Qed.

Theorem DB_eq_DB_lift_0:forall (i:Ι), DB_eq ↥(i) ¡(0,0)=⊥.
Proof.
 induction i as [[| u] n]; simpl; nat_simpl; bool_simpl; apply refl_equal.
Qed.

Theorem DB_eq_DB_lift_0':forall (n:nat), DB_eq ↥(¡(0,n)) ¡(0,n)=⊥.
Proof.
 intros n; simpl; nat_simpl; apply refl_equal.
Qed.

Theorem DB_lift_lift:forall (i:Ι)(d:nat), DB_lift_ d (DB_lift_ d i)=DB_lift_ (S d) (DB_lift_ d i).
Proof.
 intros [[| u] n] d; simpl; [ | apply refl_equal].
  case_eq (nat_le d n); intros Hdn.
   nat_simpl; reflect_in nat_le_imp Hdn; rewrite (imppt2 nat_le_imp _ _ (le_S _ _ Hdn));
    rewrite (imppt2 nat_le_imp _ _ Hdn); apply refl_equal.
   rewrite Hdn; reflect_in nat_le_imp Hdn; case_eq (nat_le (S d) n); intros HSdn.
    reflect_in nat_le_imp HSdn;
     destruct (lt_irrefl _ (lt_le_weak _ _ (le_lt_trans _ _ _ HSdn (not_le _ _ Hdn)))).
    apply refl_equal.
Qed.

Theorem DB_eq_lift_:forall (i:Ι)(d:nat), DB_eq (DB_lift_ d i) ¡(0,d)=⊥.
Proof.
 intros [[| u] n] d; simpl; nat_simpl; bool_simpl; [ | apply refl_equal].
  case_eq (nat_le d n); intros Hdn.
   case_eq (nat_eq (S n) d); intros HSnd; [ | apply refl_equal].
    reflect_in nat_eq_imp HSnd; rewrite <- HSnd in Hdn; nat_simpl; inversion Hdn.
   case_eq (nat_eq n d); intros Hnd; [ | apply refl_equal].
    reflect_in nat_eq_imp Hnd; rewrite <- Hnd in Hdn; nat_simpl; inversion Hdn.
Qed.

(* Comparison ------------------------------------------------------------------------------------*)

Definition DB_le(d:nat)(i:Ι):Β := match i with ¡(u',n') => if eq_0 u' then nat_le d n' else ⊤ end.
Notation "'≤Ι(' d ',' i ')'" := (DB_le d i=⊤).

Theorem DB_le_0:forall (i:Ι), ≤Ι(0,i).
Proof.
 intros [[| u] n]; simpl; nat_simpl; apply refl_equal.
Qed.

Theorem DB_le_lift:forall (d:nat)(i:Ι), DB_le (S d) (DB_lift_ d i)=DB_le d i.
Proof.
 intros d [[| u] n]; simpl in *; [ | apply refl_equal].
  case_eq (nat_le d n); intros Hdn; nat_simpl; [apply Hdn | ].
   reflect nat_le_imp; reflect_in nat_le_imp Hdn; intros Hdi;
    destruct (Hdn (le_S_n _ _ (le_S _ _ Hdi))).
Qed.

Theorem DB_le_trans:forall (i:Ι)(d d':nat), d<=d'->≤Ι(d',i)->≤Ι(d,i).
Proof.
 intros [[| u] n'] d d' Hdd' Hd'i; simpl in *; [ | apply refl_equal].
  reflect nat_le_imp; reflect_in nat_le_imp Hd'i; apply (le_trans _ _ _ Hdd' Hd'i).
Qed.

Theorem DB_lift_id:forall (i:Ι)(d:nat), DB_le d i=⊥->DB_lift_ d i=i.
Proof.
 intros [[| u] n] d; simpl; intros Hle; [rewrite Hle | ]; apply refl_equal.
Qed.

(* Tactic for De Bruijn indexes ------------------------------------------------------------------*)

Ltac DB_simpl := repeat (rewrite DB_eq_refl in * ||
                         rewrite DB_eq_DB_lift  in * ||
                         rewrite DB_eq_DB_lift_0 in * ||
                         rewrite DB_eq_DB_lift_0' in * ||
                         rewrite DB_eq_lift_ in * ||
                         rewrite DB_le_0 in * ||
                         rewrite DB_le_lift in *).

(*================================================================================================*)