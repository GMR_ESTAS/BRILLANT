(*==================================================================================================
  Project : BiCoq3
  Module : sem_ind.v
  This modules provides a semantical induction principle, that is induction according to the
  structure of the proofs rather than of the terms.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Aug 2008, Md: Oct 2008
  ================================================================================================*)

Require Export cmp.

Theorem semantic_ind:forall (Pp:Π->Type)(Pe:Ε->Type),
                     (forall (p:Π), Pp p->Pp (¬p))->
                     (forall (p:Π), Pp p->forall (i:Ι), Pp ∀(i∙p))->
                     (forall (p1 p2:Π), Pp p1->Pp p2->Pp (p1⋀p2))->
                     (forall (p1 p2:Π), Pp p1->Pp p2->Pp (p1⇒p2))->
                     (forall (n:nat), Pp (∐ n))->
                     (forall (e1 e2:Ε), Pe e1->Pe e2->Pp (e1≡e2))->
                     (forall (e1 e2:Ε), Pe e1->Pe e2->Pp (e1∈e2))->
                     (Pe Ω)->
                     (forall (e:Ε), Pe e->Pe ↓(e))->
                     (forall (e:Ε), Pe e->Pe ↑(e))->
                     (forall (e1 e2:Ε), Pe e1->Pe e2->Pe (e1↦e2))->
                     (forall (e1 e2:Ε), Pe e1->Pe e2->Pe (e1×e2))->
                     (forall (n:nat), Pe (∞ n))->
                     (forall (i:Ι), Pe ∂(i))->
                     (forall (e:Ε)(p:Π), Pe e->Pp p->forall (i:Ι), Pe {i∊e∣p})->
                     (forall (p:Π), Pp p)*(forall (e:Ε), Pe e).
Proof.
 intros Pp Pe Hnot  Hfor Hand Himp Hpvr Hequ Hins Hbig Hchc Hpow Hcpl Hpro Hbie Hvar Hcmp;
  apply (wf_measure_dbl Bp_depth Be_depth Pp Pe);
  [intros [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2] Hp He |
   intros [ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] Hp He ].
  apply Hnot; apply (Hp _ (sdepth t)).
  generalize (Bt_fshun_free t (S ⋇(t)) (le_n_Sn _) 0 0); intros Hit; set (i:=¡(S ⋇(t),0));
   change ¡(S ⋇(t),0) with ↥(¡(S ⋇(t),0)) in Hit; fold i in Hit; rewrite (Bp_forall_inv t i Hit);
   apply Hfor; apply (Hp (Bp_app_ 0 t ∂(i))); srewrite (Bt_app_depth t 0 i); unfold lt;
   apply le_refl.
  apply Hand; [apply (Hp _ (sdepth_l t1 t2)) | apply (Hp _ (sdepth_r t1 t2))].
  apply Himp; [apply (Hp _ (sdepth_l t1 t2)) | apply (Hp _ (sdepth_r t1 t2))].
  apply Hpvr.
  apply Hequ; [apply (He _ (sdepth_l t1 t2)) | apply (He _ (sdepth_r t1 t2))].
  apply Hins; [apply (He _ (sdepth_l t1 t2)) | apply (He _ (sdepth_r t1 t2))].
  apply Hbig.
  apply Hchc; apply (He _ (sdepth t)).
  apply Hpow; apply (He _ (sdepth t)).
  apply Hcpl; [apply (He _ (sdepth_l t1 t2)) | apply (He _ (sdepth_r t1 t2))].
  apply Hpro; [apply (He _ (sdepth_l t1 t2)) | apply (He _ (sdepth_r t1 t2))].
  apply Hbie.
  apply Hvar.
  generalize (Bt_fshun_free t2 (S ⋇(t2)) (le_n_Sn _) 0 0); intros Hit2; set (i:=¡(S ⋇(t2),0));
   change ¡(S ⋇(t2),0) with ↥(¡(S ⋇(t2),0)) in Hit2; fold i in Hit2;
   rewrite (Be_cmpset_inv t2 t1 i Hit2); apply Hcmp.
   apply (He _ (sdepth_l t1 t2)).
   apply (Hp (Bp_app_ 0 t2 ∂(i))); srewrite (Bt_app_depth t2 0 i); unfold lt; apply le_n_S;
    apply le_max_r.
Qed.
(* It is possible than a clever induction principle uses the hypothesis Pp p->{i:Ι & Pp ∀(i∙p)}
   instead of Pp p->forall (i:Ι), p ∀(i∙p), making more easy subsequent proofs. However we have
   not been able to derive this principle - while the intuition is that it should be possible to
   prove it, as the standard structural induction Pp p->Pp ∀Δp does not give any choice regarding
   the bound variable. *)

(* Semantical accessibility relation -------------------------------------------------------------*)
(* This relation is defined as a dedicated induction scheme to be used in conjunction with depth
   induction. *)

Inductive Bt_access:Τ->Type :=
 | Bt_access_not:forall (p:Π), Bt_access p->Bt_access (¬p)
 | Bt_access_for:forall (p:Π)(i:Ι), Bt_access p->Bt_access ∀(i∙p)
 | Bt_access_and:forall (p1 p2:Π), Bt_access p1->Bt_access p2->Bt_access (p1⋀p2)
 | Bt_access_imp:forall (p1 p2:Π), Bt_access p1->Bt_access p2->Bt_access (p1⇒p2)
 | Bt_access_prp:forall (n:nat), Bt_access (∐(n))
 | Bt_access_equ:forall (e1 e2:Ε), Bt_access e1->Bt_access e2->Bt_access (e1≡e2)
 | Bt_access_ins:forall (e1 e2:Ε), Bt_access e1->Bt_access e2->Bt_access (e1∈e2)
 | Bt_access_big:Bt_access Ω
 | Bt_access_chc:forall (e:Ε), Bt_access e->Bt_access ↓(e)
 | Bt_access_pow:forall (e:Ε), Bt_access e->Bt_access ↑(e)
 | Bt_access_cpl:forall (e1 e2:Ε), Bt_access e1->Bt_access e2->Bt_access (e1↦e2)
 | Bt_access_pro:forall (e1 e2:Ε), Bt_access e1->Bt_access e2->Bt_access (e1×e2)
 | Bt_access_bge:forall (n:nat), Bt_access (∞(n))
 | Bt_access_var:forall (i:Ι), Bt_access (∂(i))
 | Bt_access_cmp:forall (p:Π)(e:Ε)(i:Ι), Bt_access p->Bt_access e->Bt_access {i∊e∣p}.

Theorem Bt_access_all:forall (t:Τ), Bt_access t.
Proof.
 cut ((forall (p:Π), Bt_access p)*(forall (e:Ε), Bt_access e)).
  intros [Hp He] [e | p]; [apply (He e) | apply (Hp p)].
 apply (semantic_ind (fun (p:Π) => Bt_access p) (fun (e:Ε) => Bt_access e)).
 intros p Hp; apply Bt_access_not; apply Hp.
 intros p Hp i; apply Bt_access_for; apply Hp.
 intros p1 p2 Hp1 Hp2; apply Bt_access_and; [apply Hp1 | apply Hp2].
 intros p1 p2 Hp1 Hp2; apply Bt_access_imp; [apply Hp1 | apply Hp2].
 intros n; apply Bt_access_prp.
 intros e1 e2 He1 He2; apply Bt_access_equ; [apply He1 | apply He2].
 intros e1 e2 He1 He2; apply Bt_access_ins; [apply He1 | apply He2].
 apply Bt_access_big.
 intros e He; apply Bt_access_chc; apply He.
 intros e He; apply Bt_access_pow; apply He.
 intros e1 e2 He1 He2; apply Bt_access_cpl; [apply He1 | apply He2].
 intros e1 e2 He1 He2; apply Bt_access_pro; [apply He1 | apply He2].
 intros n; apply Bt_access_bge.
 intros i; apply Bt_access_var.
 intros e p He Hp i; apply Bt_access_cmp; [apply Hp | apply He].
Qed.
(* Nota: This access relation allows for semantical induction; following the application of the
   wf_term induction principle (well-founded induction using the depth of a term as a measure), one
   has to use the following tactic:
   intros t;
   destruct (Bt_access_all t) as
    [p _ | p i' _ | p1 p2 _ _ | p1 p2 _ _ | n' | e1 e2 _ _ | e1 e2 _ _ |
     | e _ | e _ | e1 e2 _ _ | e1 e2 _ _ | n' | i' | p e i' _ _]
*)

(*================================================================================================*)