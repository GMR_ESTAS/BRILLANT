(*==================================================================================================
  Project : BiCoq3
  Module : raw_proof.v
  In this module we provide so-called raw inference rules, that is inference rules expressed in the
  internal representation based on De Bruijn indexes.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Sep 2008, Md: Mar 2009
  ================================================================================================*)

Require Export swap.

Theorem Bpr_fori_raw:forall (G:Γ)(p:Π)(u n:nat),
                     (¡(S u,n)∖ˠG·p)->G⊢Bp_app_ 0 p ∂(¡(S u,n))->G⊢∀Δ(p).
Proof.
 intros G p u n Hi Happ.
 assert (Hip:¡(S u,n)∖p). apply (Bg_free_mem _ _ Hi p); Btac_gamma.
 assert (HiG:¡(S u,n)∖ˠG). apply (Bg_free_inc _ G _ Hi (Bg_inc_add _ _)).
 rewrite (Bp_forall_inv p ¡(S u,n) Hip); apply (Bpr_fori _ _ _ Happ HiG).
Qed.

Theorem Bpr_fore_raw:forall (G:Γ)(p:Π)(e:Ε), G⊢∀Δ(p)->G⊢Bp_app_ 0 p e.
Proof.
 intros G p e Hp; generalize (Bg_fshun_free (G·p) _ (le_n_Sn _) 0); set (i:=¡(S ⋇ˠ(G·p),0));
  intros Hi.
 assert (Hip:i∖p). apply (Bg_free_mem _ _ Hi p); Btac_gamma.
 assert (HiG:i∖ˠG). apply (Bg_free_inc _ G _ Hi); Btac_gamma.
 rewrite (Bp_forall_inv p i Hip) in Hp; generalize (Bpr_fore _ _ e _ Hp).
 irewrite (sym_equal (Bt_esubst_dec_ (Bp_app_ 0 p ∂(i)) e i 0)).
 change i with ↥(i) in Hip; rewrite (Bt_free_lift_S p i 0) in Hip;
  irewrite (Bt_abstr_app_ p i 0 (DB_le_0 _) Hip); intros H'p; apply H'p.
Qed.

Theorem Bpr_cmpi_raw:forall (G:Γ)(p:Π)(e e':Ε)(u n:nat),
                     (¡(S u,n)∖p)->G⊢e'∈e->G⊢Bp_app_ 0 p e'->G⊢e'∈ⅭΔ(e,p).
Proof.
 intros G p e e' u n Hunp He'e Happ; rewrite (Be_cmpset_inv p e ¡(S u,n) Hunp);
  apply (Bpr_cmpi _ (Bp_app_ 0 p ∂(¡(S u,n))) _ _ ¡(S u,n) He'e).
 irewrite (sym_equal (Bt_esubst_dec_ (Bp_app_ 0 p ∂(¡(S u,n))) e' ¡(S u,n) 0)).
 change ¡(S u,n) with ↥(¡(S u,n)) in Hunp; rewrite (Bt_free_lift_S p ¡(S u,n) 0) in Hunp;
  irewrite (Bt_abstr_app_ p ¡(S u,n) 0 (DB_le_0 _) Hunp).
 apply Happ.
Qed.

Theorem Bpr_cmpr_raw:forall (G:Γ)(p:Π)(e e':Ε), G⊢e'∈ⅭΔ(e,p)->G⊢Bp_app_ 0 p e'.
Proof.
 intros G p e e' Hcmp; generalize (Bt_fshun_free p _ (le_n_Sn _) 0 0); set (i:=¡(S ⋇(p),0));
  intros Hip; change i with ↥(i) in Hip; rewrite (Be_cmpset_inv _ e i Hip) in Hcmp.
 generalize (Bpr_cmpr _ _ _ _ _ Hcmp).
 irewrite (sym_equal (Bt_esubst_dec_ (Bp_app_ 0 p ∂(i)) e' i 0)).
 rewrite (Bt_free_lift_S p i 0) in Hip.
 irewrite (Bt_abstr_app_ _ _ _ (DB_le_0 _) Hip).
 intros Happ; apply Happ.
Qed.

(*================================================================================================*)