(*==================================================================================================
  Project : BiCoq3
  Module : esubst.v
  Similarly to what was done for application, we represent here substitution of a variable by an
  expression in a term; again substitutions are NOT (for now) part of the embedded language, and
  these situations are encoded by applying dedicated functions to terms.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jun 2008, Md: Feb 2009
  ================================================================================================*)

Require Export lift.

(* Substitution function -------------------------------------------------------------------------*)

Fixpoint Bp_esubst_(d:nat)(i:Ι)(p:Π)(a:Ε){struct p}:Π:=
 match p with
 | ¬p' => ¬(Bp_esubst_ d i p' a)
 | ∀Δ(p') => ∀Δ(Bp_esubst_ (S d) (DB_lift_ d i) p' (Be_lift_ d a))
 | p1⋀p2 => (Bp_esubst_ d i p1 a)⋀(Bp_esubst_ d i p2 a)
 | (p1⇒p2) => (Bp_esubst_ d i p1 a)⇒(Bp_esubst_ d i p2 a)
 | ∐(_) => p
 | e1≡e2 => (Be_esubst_ d i e1 a)≡(Be_esubst_ d i e2 a)
 | e1∈e2 => (Be_esubst_ d i e1 a)∈(Be_esubst_ d i e2 a)
 end
with Be_esubst_(d:nat)(i:Ι)(e a:Ε){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_esubst_ d i e' a)
 | ↑(e') => ↑(Be_esubst_ d i e' a)
 | e1↦e2 => (Be_esubst_ d i e1 a)↦(Be_esubst_ d i e2 a)
 | e1×e2 => (Be_esubst_ d i e1 a)×(Be_esubst_ d i e2 a)
 | ∞(_) => e
 | ∂(i') => if DB_le d i' && DB_eq i i' then a else e
 | ⅭΔ(e',p') => ⅭΔ(Be_esubst_ d i e' a,Bp_esubst_ (S d) (DB_lift_ d i) p' (Be_lift_ d a))
 end.
Notation "'〈' i '←' a '@Π@' p '〉'" := (Bp_esubst_ 0 i p a).
Notation "'〈' i '←' a '@Ε@' e '〉'" := (Be_esubst_ 0 i e a).

Definition Bt_esubst_(d:nat)(i:Ι)(t:Τ)(a:Ε):Τ :=
 match t with be_ e' => Be_esubst_ d i e' a | bp_ p' => Bp_esubst_ d i p' a end.
Notation "'〈' i '←' a '@' t '〉'" := (Bt_esubst_ 0 i t a).

(* Theorems for substitution ---------------------------------------------------------------------*)

Theorem Bt_esubst_free_:forall (t:Τ)(i:Ι)(d:nat), Bt_free_ d i t=⊥->
                        forall (a:Ε), Bt_esubst_ d i t a=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat), Bt_free_ d i t=⊥->
                            forall (a:Ε), Bt_esubst_ d i t a=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i d Ht a;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ _ Ht a); apply refl_equal);
  try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2];
       irewrite (Hind _ (sdepth_l t1 t2) _ _ Ht1 a);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ Ht2 a); apply refl_equal).
  unfold Bt_esubst_,Be_esubst_; unfold Bt_free_,Be_free_ in Ht; destruct (DB_le d i');
  [rewrite Ht | ]; apply refl_equal.
  destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2]; irewrite (Hind _ (sdepth_l t1 t2) _ _ Ht1 a);
   irewrite (Hind _ (sdepth_r t1 t2) _ _ Ht2 (Be_lift_ d a)); apply refl_equal.
  irewrite (Hind _ (sdepth t) _ _ Ht (Be_lift_ d a)); apply refl_equal.
Qed.

Theorem Bt_esubst_free:forall (t:Τ)(i:Ι), (i∖t)->forall (a:Ε), 〈i←a@t〉=t.
Proof.
 intros t i Hit; apply Bt_esubst_free_; apply Hit.
Qed.

Theorem Bt_esubst_id_:forall (t:Τ)(i:Ι)(d:nat), Bt_esubst_ d i t ∂(i)=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat), Bt_esubst_ d i t ∂(i)=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i d;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) i d); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) i d); irewrite (Hind _ (sdepth_r t1 t2) i d);
       apply refl_equal).
  unfold Bt_esubst_,Be_esubst_; case_eq (DB_le d i'); intros Hdi';
  [case_eq (DB_eq i i'); intros Hii'; [reflect_in DB_eq_imp Hii'; rewrite  Hii' | ] | ];
   apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) i d); irewrite (Hind _ (sdepth_r t1 t2) (DB_lift_ d i) (S d));
   apply refl_equal.
  irewrite (Hind _ (sdepth t) (DB_lift_ d i) (S d)); apply refl_equal.
Qed.

Theorem Bt_esubst_id:forall (t:Τ)(i:Ι), 〈i←∂(i)@t〉=t.
Proof.
 intros t i; apply Bt_esubst_id_.
Qed.

Theorem Bt_esubst_dbl_:forall (t:Τ)(i1 i2:Ι)(e:Ε)(d:nat), ≤Ι(d,i1)->Bt_free_ d i1 t=⊥->
                       Bt_esubst_ d i1 (Bt_esubst_ d i2 t ∂(i1)) e=Bt_esubst_ d i2 t e.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i1 i2:Ι)(e:Ε)(d:nat), ≤Ι(d,i1)->Bt_free_ d i1 t=⊥->
                            Bt_esubst_ d i1 (Bt_esubst_ d i2 t ∂(i1)) e=Bt_esubst_ d i2 t e));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i1 i2 e d Hdi1 Ht;
  try (apply refl_equal);
  try (irewrite (Hind _ (sdepth t) _ i2 e _ Hdi1 Ht); apply refl_equal);
  try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2];
       irewrite (Hind _ (sdepth_l t1 t2) _ i2 e _ Hdi1 Ht1);
       irewrite (Hind _ (sdepth_r t1 t2) _ i2 e _ Hdi1 Ht2); apply refl_equal).
  case_eq (DB_le d i'); intros Hdi'; rewrite Hdi' in *; simpl; [ | rewrite Hdi'; apply refl_equal].
   destruct (DB_eq i2 i'); simpl; [ | rewrite Hdi'; rewrite Ht; apply refl_equal].
   DB_simpl; case_eq (DB_le d i1); intros H'di1;
   [apply refl_equal | rewrite H'di1 in Hdi1; inversion Hdi1].
  destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2];
   irewrite (Hind _ (sdepth_l t1 t2) _ i2 e _ Hdi1 Ht1); rewrite <- (DB_le_lift d i1) in Hdi1;
   irewrite (Hind _ (sdepth_r t1 t2) _ (DB_lift_ d i2) (Be_lift_ d e) _ Hdi1 Ht2); apply refl_equal.
  rewrite <- (DB_le_lift d i1) in Hdi1;
   irewrite (Hind _ (sdepth t) _ (DB_lift_ d i2) (Be_lift_ d e) _ Hdi1 Ht); apply refl_equal.
Qed.
(* Nota: the side-condition ≤Ι(d,i1) appears as a result of its addition into the code of the
   substitution, required to comply with the generic policy to never touch any bound index. It is
   relevant, as i1 only appears on one side of the equation, its validity has to be ensured. More
   precisely, ≤Ι(d,i1) can be rewritten as Bt_comp_ 0 d ∂(i1)! *)

Theorem Bt_esubst_dbl:forall (t:Τ)(i1 i2:Ι)(e:Ε), (i1∖t)->〈i1←e@〈i2←∂(i1)@t〉〉=〈i2←e@t〉.
Proof.
 intros t i1 i2 e Ht; apply Bt_esubst_dbl_; [apply DB_le_0 | apply Ht].
Qed.

Theorem Bt_esubst_comm_:
 forall (t:Τ)(i1 i2:Ι)(e1 e2:Ε)(d:nat), i1<>i2->Bt_free_ d i1 e2=⊥->Bt_free_ d i2 e1=⊥->
 Bt_esubst_ d i1 (Bt_esubst_ d i2 t e2) e1=Bt_esubst_ d i2 (Bt_esubst_ d i1 t e1) e2.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i1 i2:Ι)(e1 e2:Ε)(d:nat),
                            i1<>i2->Bt_free_ d i1 e2=⊥->Bt_free_ d i2 e1=⊥->
                            Bt_esubst_ d i1 (Bt_esubst_ d i2 t e2) e1=
                            Bt_esubst_ d i2 (Bt_esubst_ d i1 t e1) e2));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl;
  intros Hind i1 i2 e1 e2 d His Hi1 Hi2; try (apply refl_equal);
  try (irewrite (Hind _ (sdepth t) _ _ _ _ _ His Hi1 Hi2); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) _ _ _ _ _ His Hi1 Hi2);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ _ _ _ His Hi1 Hi2); apply refl_equal).
  case_eq (DB_le d i'); intros Hdi'.
   case_eq (DB_eq i1 i'); simpl; intros Hi1i'; case_eq (DB_eq i2 i'); simpl; intros Hi2i'.
    reflect_in DB_eq_imp Hi1i'; rewrite Hi1i' in *; reflect_in DB_eq_imp Hi2i'; rewrite Hi2i' in *;
     destruct His; apply refl_equal.
    rewrite Hdi'; rewrite Hi1i'; apply (sym_equal (Bt_esubst_free_ e1 i2 d Hi2 e2)).
    rewrite Hdi'; apply (Bt_esubst_free_ e2 i1 d Hi1 e1).
    rewrite Hdi'; rewrite Hi1i'; apply refl_equal.
   simpl; rewrite Hdi'; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) _ _ _ _ _ His Hi1 Hi2);
    assert (H'is:DB_lift_ d i1<>DB_lift_ d i2).
   reflect DB_eq_imp; DB_simpl; reflect DB_eq_imp; apply His.
  assert (H'i1:Be_free_ (S d) (DB_lift_ d i1) (Be_lift_ d e2)=⊥).
   srewrite (Bt_lift_free_ e2 i1 d); apply Hi1.
  assert (H'i2:Be_free_ (S d) (DB_lift_ d i2) (Be_lift_ d e1)=⊥).
   srewrite (Bt_lift_free_ e1 i2 d); apply Hi2.
  irewrite (Hind _ (sdepth_r t1 t2) _ _ _ _ _ H'is H'i1 H'i2); apply refl_equal.
  assert (H'is:DB_lift_ d i1<>DB_lift_ d i2).
   reflect DB_eq_imp; DB_simpl; reflect DB_eq_imp; apply His.
  assert (H'i1:Be_free_ (S d) (DB_lift_ d i1) (Be_lift_ d e2)=⊥).
   srewrite (Bt_lift_free_ e2 i1 d); apply Hi1.
  assert (H'i2:Be_free_ (S d) (DB_lift_ d i2) (Be_lift_ d e1)=⊥).
   srewrite (Bt_lift_free_ e1 i2 d); apply Hi2.
  irewrite (Hind _ (sdepth t) _ _ _ _ _ H'is H'i1 H'i2); apply refl_equal.
Qed.

Theorem Bt_esubst_comm:forall (t:Τ)(i1 i2:Ι)(e1 e2:Ε), i1<>i2->(i1∖e2)->(i2∖e1)->
                       〈i1←e1@〈i2←e2@t〉〉=〈i2←e2@〈i1←e1@t〉〉.
Proof.
 intros t i1 i2 e1 e2 His Hi1 Hi2; apply (Bt_esubst_comm_ t i1 i2 e1 e2 0 His Hi1 Hi2).
Qed.

Theorem Bt_free_esubst_:forall (t:Τ)(i:Ι)(e:Ε)(d:nat),
                        Bt_free_ d i e=⊥->Bt_free_ d i (Bt_esubst_ d i t e)=⊥.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(e:Ε)(d:nat),
                            Bt_free_ d i e=⊥->Bt_free_ d i (Bt_esubst_ d i t e)=⊥));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i e d Hdie;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) _ _ _ Hdie));
  try (srewrite (Hind _ (sdepth_l t1 t2) _ _ _ Hdie); srewrite (Hind _ (sdepth_r t1 t2) _ _ _ Hdie);
       apply refl_equal).
  case_eq (DB_le d i'); intros Hdi'.
   case_eq (DB_eq i i'); intros Hii'; [apply Hdie | simpl; rewrite Hdi'; apply Hii'].
   simpl; rewrite Hdi'; apply refl_equal.
  srewrite (Hind _ (sdepth_l t1 t2) _ _ _ Hdie); srewrite_in (sym_equal (Bt_lift_free_ e i d)) Hdie;
   srewrite (Hind _ (sdepth_r t1 t2) _ (Be_lift_ d e) _ Hdie); apply refl_equal.
  srewrite_in (sym_equal (Bt_lift_free_ e i d)) Hdie;
   srewrite (Hind _ (sdepth t) _ (Be_lift_ d e) _ Hdie); apply refl_equal.
Qed.

Theorem Bt_free_esubst:forall (t:Τ)(i:Ι)(e:Ε), (i∖e)->(i∖〈i←e@t〉).
Proof.
 intros t i e Hie; apply (Bt_free_esubst_ t _ _ _ Hie).
Qed.

Theorem Bt_free_esubst'_:forall (t:Τ)(i1 i2:Ι)(e:Ε)(d:nat), Bt_free_ d i1 e=⊥->Bt_free_ d i1 t=⊥->
                         Bt_free_ d i1 (Bt_esubst_ d i2 t e)=⊥.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i1 i2:Ι)(e:Ε)(d:nat), Bt_free_ d i1 e=⊥->Bt_free_ d i1 t=⊥->
                            Bt_free_ d i1 (Bt_esubst_ d i2 t e)=⊥));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl;
  intros Hind i1 i2 e d Hi1e Hi1t; try (apply refl_equal);
  try (apply (Hind _ (sdepth t) _ i2 _ _ Hi1e Hi1t));
  try (destruct (orb_false_elim _ _ Hi1t) as [Hi1t1 Hi1t2];
       srewrite (Hind _ (sdepth_l t1 t2) _ i2 _ _ Hi1e Hi1t1);
       srewrite (Hind _ (sdepth_r t1 t2) _ i2 _ _ Hi1e Hi1t2); apply refl_equal).
  case_eq (DB_le d i'); intros Hdi'; rewrite Hdi' in *.
   case_eq (DB_eq i2 i'); intros Hi2i'; [apply Hi1e | simpl; rewrite Hdi'; apply Hi1t].
   simpl; rewrite Hdi'; apply refl_equal.
  destruct (orb_false_elim _ _ Hi1t) as [Hi1t1 Hi1t2];
   srewrite (Hind _ (sdepth_l t1 t2) _ i2 _ _ Hi1e Hi1t1);
   srewrite_in (sym_equal (Bt_lift_free_ e i1 d)) Hi1e;
   srewrite (Hind _ (sdepth_r t1 t2) _(DB_lift_ d i2) _ _ Hi1e Hi1t2); apply refl_equal.
  srewrite_in (sym_equal (Bt_lift_free_ e i1 d)) Hi1e;
   srewrite (Hind _ (sdepth t) _(DB_lift_ d i2) _ _ Hi1e Hi1t); apply refl_equal.
Qed.

Theorem Bt_free_esubst':forall (t:Τ)(i1 i2:Ι)(e:Ε), (i1∖e)->(i1∖t)->(i1∖〈i2←e@t〉).
Proof.
 intros t i1 i2 e Hi1e Hi1t; apply (Bt_free_esubst'_ _ _ i2 _ _ Hi1e Hi1t).
Qed.

(*================================================================================================*)