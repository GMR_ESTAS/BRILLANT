(*==================================================================================================
  Project : BiCoq3
  Module : mesubst.v
  This modules defines multiple substitutions for expressions; multiple substitutions are defined as
  a tool to prove congruence results, but are not expected to be used outside this scope. Ideally a
  (satisfiable) specification would be sufficient, as the code is not expected to be extracted and
  is just required for logical considerations. Yet for technical reasons it is easier to implement
  multiple substitutions, even in a very inefficient manner (at least from the computational point
  of view).
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Mar 2009
  ================================================================================================*)

Require Export cmp.

(* Multiple substitutions ------------------------------------------------------------------------*)

Definition MS:Set := Ι->Ε.
Notation "'Μ'" := MS.

Definition MS_nil:Μ := fun (i':Ι) => ∂(i').
Notation "'⊚'" := MS_nil.

Definition MS_ins(m:Μ)(i:Ι)(e:Ε) := fun (i':Ι) => if DB_eq i i' then e else m i'.
Notation "'≺' i '←' e '≻⊕' m" := (MS_ins m i e) (at level 27, right associativity).

Definition MS_rem(m:Μ)(i:Ι) := fun (i':Ι) => if DB_eq i i' then ∂(i) else m i'.
Notation "m '⊖' i" := (MS_rem m i) (at level 28, left associativity).

Theorem MS_ins_eq:forall (m:Μ)(i:Ι)(e:Ε), (≺i←e≻⊕m) i=e.
Proof.
 intros m i e; unfold MS_ins; DB_simpl; apply refl_equal.
Qed.

Theorem MS_ins_diff:forall (m:Μ)(i i':Ι)(e:Ε), i'<>i->(≺i'←e≻⊕m) i=m i.
Proof.
 intros m i i' e Hii'; unfold MS_ins; rewrite (imprf2 DB_eq_imp _ _ Hii'); apply refl_equal.
Qed.

Theorem MS_rem_eq:forall (m:Μ)(i:Ι), (m⊖i) i=∂(i).
Proof.
 intros m i; unfold MS_rem; DB_simpl; apply refl_equal.
Qed.

Theorem MS_rem_diff:forall (m:Μ)(i i':Ι), i'<>i->(m⊖i') i=m i.
Proof.
 intros m i i' Hii'; unfold MS_rem; rewrite (imprf2 DB_eq_imp _ _ Hii'); apply refl_equal.
Qed.

(* Lifting of multiple substitutions -------------------------------------------------------------*)

Definition MS_lift_(d:nat)(m:Μ):Μ := fun (i':Ι) => match i' with
                                                   | ¡(0,0) => ∂(i')
                                                   | ¡(0,S n') => if (nat_le d n')
                                                                  then Be_lift_ d (m ¡(0,n'))
                                                                  else ∂(i')
                                                   | ¡(S _,_) => Be_lift_ d (m i')
                                                   end.
Notation "'⇑Μ(' m ')'":=(MS_lift_ 0 m).
(* Nota: Again, we are here using a depth parameter which is not absolutely necessary from a pure
   computational point of view, but is useful to explicit a context in proofs. *)

Theorem MS_get_lift_:forall (m:Μ)(i:Ι)(d:nat), ≤Ι(d,i)->
                     (MS_lift_ d m) (DB_lift_ d i)=Be_lift_ d (m i).
Proof.
 intros m [[| u] n] d Hdi; simpl in *; [repeat (rewrite Hdi) | ]; apply refl_equal.
Qed.

Theorem MS_get_lift:forall (m:Μ)(i:Ι), ⇑Μ(m) ↥(i)=⇑Ε(m i).
Proof.
 intros m i; apply MS_get_lift_; apply DB_le_0.
Qed.

(* Application to a term -------------------------------------------------------------------------*)
(* We recurse on the term to represent parallel non-interfering substitutions *)

Fixpoint Bp_mesubst_(d:nat)(m:Μ)(p:Π){struct p}:Π :=
 match p with
 | ¬p' => ¬(Bp_mesubst_ d m p')
 | ∀Δ(p') => ∀Δ(Bp_mesubst_ (S d) (MS_lift_ d m) p')
 | p1⋀p2 => (Bp_mesubst_ d m p1)⋀(Bp_mesubst_ d m p2)
 | (p1⇒p2) => (Bp_mesubst_ d m p1)⇒(Bp_mesubst_ d m p2)
 | ∐(_) => p
 | e1≡e2 => (Be_mesubst_ d m e1)≡(Be_mesubst_ d m e2)
 | e1∈e2 => (Be_mesubst_ d m e1)∈(Be_mesubst_ d m e2)
 end
with Be_mesubst_(d:nat)(m:Μ)(e:Ε){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_mesubst_ d m e')
 | ↑(e') => ↑(Be_mesubst_ d m e')
 | e1↦e2 => (Be_mesubst_ d m e1)↦(Be_mesubst_ d m e2)
 | e1×e2 => (Be_mesubst_ d m e1)×(Be_mesubst_ d m e2)
 | ∞(_) => e
 | ∂(i') => if DB_le d i' then m i' else e
 | ⅭΔ(e',p') => ⅭΔ(Be_mesubst_ d m e',Bp_mesubst_ (S d) (MS_lift_ d m) p')
 end.
Notation "'〈〈' m '@Π@' p '〉〉'" := (Bp_mesubst_ 0 m p).
Notation "'〈〈' m '@Ε@' e '〉〉'" := (Be_mesubst_ 0 m e).

Definition Bt_mesubst_(d:nat)(m:Μ)(t:Τ):Τ :=
 match t with be_ e' => Be_mesubst_ d m e' | bp_ p' => Bp_mesubst_ d m p' end.
Notation "'〈〈' m '@' t '〉〉'" := (Bt_mesubst_ 0 m t).

(* Some extensionnal equivalence considerations --------------------------------------------------*)

Definition MS_eq(m1 m2:Μ):Prop := forall (i:Ι), m1 i=m2 i.

Theorem MS_eq_refl:forall (m:Μ), MS_eq m m.
Proof.
 intros m i; apply refl_equal.
Qed.

Theorem MS_eq_sym:forall (m1 m2:Μ), MS_eq m1 m2->MS_eq m2 m1.
Proof.
 intros m1 m2 Heq i; apply sym_equal; apply Heq.
Qed.

Theorem MS_eq_trans:forall (m1 m2 m3:Μ), MS_eq m1 m2->MS_eq m2 m3->MS_eq m1 m3.
Proof.
 intros m1 m2 m3 H12 H23 i; rewrite H12; apply H23.
Qed.

Theorem MS_eq_ins:forall (m:Μ)(i:Ι), MS_eq m (≺i←m i≻⊕m).
Proof.
 intros m i; unfold MS_ins; intros i'; case_eq (DB_eq i i'); intros Hii';
 [reflect_in DB_eq_imp Hii'; rewrite Hii' | ]; apply refl_equal.
Qed.

Theorem MS_eq_ins_eq:forall (m1 m2:Μ)(i:Ι)(e:Ε), MS_eq m1 m2->MS_eq (≺i←e≻⊕m1) (≺i←e≻⊕m2).
Proof.
 intros m1 m2 i e Heq i'; unfold MS_ins; destruct (DB_eq i i'); [apply refl_equal | apply Heq].
Qed.

Theorem MS_eq_ins_rem:forall (m:Μ)(i:Ι)(e:Ε), MS_eq (≺i←e≻⊕m) (≺i←e≻⊕(m⊖i)) .
Proof.
 intros m i e; unfold MS_ins; intros i'; case_eq (DB_eq i i'); intros Hii';
 [ | rewrite (MS_rem_diff m i' i (snd DB_eq_imp i i' Hii'))]; apply refl_equal.
Qed.

Theorem MS_ins_rem_diff:forall (m:Μ)(i i':Ι)(e:Ε), i<>i'->MS_eq (≺i←e≻⊕(m⊖i')) ((≺i←e≻⊕m)⊖i') .
Proof.
 intros m i i' e Hii' i''; case_eq (DB_eq i i''); intros Hii''; reflect_in DB_eq_imp Hii'';
  case_eq (DB_eq i' i''); intros Hi'i''; reflect_in DB_eq_imp Hi'i''.
  rewrite Hii'' in Hii'; rewrite Hi'i'' in Hii'; destruct Hii'; apply refl_equal.
  rewrite Hii''; rewrite (MS_rem_diff (≺i''←e≻⊕m) _ _ Hi'i''); repeat (rewrite MS_ins_eq);
   apply refl_equal.
  rewrite Hi'i''; rewrite (MS_ins_diff (m⊖i'') _ _ e Hii''); repeat (rewrite MS_rem_eq);
   apply refl_equal.
  rewrite (MS_ins_diff (m⊖i') _ _ e Hii''); rewrite (MS_rem_diff (≺i←e≻⊕m) _ _ Hi'i'');
   rewrite (MS_ins_diff m _ _ e Hii''); rewrite (MS_rem_diff m _ _ Hi'i''); apply refl_equal.
Qed.

Theorem MS_ins_rem_eq:forall (m:Μ)(i:Ι)(e:Ε), MS_eq ((≺i←e≻⊕m)⊖i) (m⊖i).
Proof.
 intros m i e i'; case_eq (DB_eq i i'); intros Hii'; reflect_in DB_eq_imp Hii'.
  rewrite Hii'; repeat (rewrite MS_rem_eq); apply refl_equal.
  rewrite (MS_rem_diff m _ _ Hii'); rewrite (MS_rem_diff (≺i←e≻⊕m) _ _ Hii');
   rewrite (MS_ins_diff m _ _ e Hii'); apply refl_equal.
Qed.

Theorem MS_dec:forall (m:Μ)(i:Ι), MS_eq m (≺i←m i≻⊕(m⊖i)).
Proof.
 intros m i i'; rewrite <- MS_eq_ins_rem; apply MS_eq_ins.
Qed.

Theorem MS_ins_lift_:forall (m:Μ)(i:Ι)(e:Ε)(d:nat), ≤Ι(d,i)->
                     MS_eq (MS_lift_ d (≺i←e≻⊕m)) (≺DB_lift_ d i←Be_lift_ d e≻⊕MS_lift_ d m).
Proof.
 intros m [[| u] n] e d Hdi [[| u'] [| n']]; unfold MS_ins; simpl in *; nat_simpl; bool_simpl;
  try (rewrite Hdi); nat_simpl; bool_simpl; try (apply refl_equal).
  case_eq (nat_eq n n'); intros Hnn';
  [reflect_in nat_eq_imp Hnn'; rewrite Hnn' in *; rewrite Hdi | ]; apply refl_equal.
  destruct (nat_eq u u' && nat_eq n 0); apply refl_equal.
  destruct (nat_eq u u' && nat_eq n (S n')); apply refl_equal.
Qed.

Theorem MS_ins_lift_0_:forall (m:Μ)(e:Ε)(n d:nat), n<d->
                       MS_eq (MS_lift_ d (≺¡(0,n)←e≻⊕m)) (MS_lift_ d m).
Proof.
 intros m e n d Hnd [[| u'] [| n']]; unfold MS_ins; simpl in *; nat_simpl; bool_simpl;
  try (apply refl_equal).
  case_eq (nat_le d n'); intros Hdn'.
   case_eq (nat_eq n n'); intros Hnn'.
    reflect_in nat_eq_imp Hnn'; rewrite Hnn' in *; reflect_in nat_le_imp Hdn';
     destruct ((lt_not_le _ _ Hnd) Hdn').
    apply refl_equal.
   apply refl_equal.
Qed.

Theorem MS_ins_lift:forall (m:Μ)(i:Ι)(e:Ε), MS_eq ⇑Μ(≺i←e≻⊕m) (≺↥(i)←⇑Ε(e)≻⊕⇑Μ(m)).
Proof.
 intros m i e; apply MS_ins_lift_; apply DB_le_0.
Qed.

Theorem MS_rem_lift_:forall (m:Μ)(i:Ι)(d:nat), ≤Ι(d,i)->
                     MS_eq (MS_lift_ d (m⊖i)) ((MS_lift_ d m)⊖(DB_lift_ d i)).
Proof.
 intros m [[| u] n] d Hdi [[| u'] [| n']]; simpl in *; unfold MS_rem; simpl; nat_simpl; bool_simpl;
  try (apply refl_equal).
  rewrite Hdi; nat_simpl; apply refl_equal.
  rewrite Hdi; nat_simpl; case_eq (nat_eq n n'); intros Hnn';
  [reflect_in nat_eq_imp Hnn'; rewrite Hnn' in *; rewrite Hdi; simpl; rewrite Hdi | ];
   apply refl_equal.
  destruct (nat_eq u u' && nat_eq n 0); apply refl_equal.
  destruct (nat_eq u u' && nat_eq n (S n')); apply refl_equal.
Qed.

Theorem MS_rem_lift:forall (m:Μ)(i:Ι), MS_eq ⇑Μ(m⊖i) (⇑Μ(m)⊖↥(i)).
Proof.
 intros m i; apply MS_rem_lift_; apply DB_le_0.
Qed.

Theorem MS_ins_rem_lift_:forall (m:Μ)(i i':Ι)(e:Ε)(d:nat), ≤Ι(d,i)->≤Ι(d,i')->
                         MS_eq (MS_lift_ d (≺i←e≻⊕(m⊖i')))
                               (≺(DB_lift_ d i)←(Be_lift_ d e)≻⊕
                               ((MS_lift_ d m)⊖(DB_lift_ d i'))).
Proof.
 intros m i i' e d Hdi Hdi' i''; unfold MS_ins; unfold MS_rem; unfold MS_lift_; simpl; DB_simpl;
  destruct i'' as [[| u''] [| n'']]; DB_simpl.
  case_eq (DB_eq (DB_lift_ d i) ¡(0,0)); intros Hi.
   destruct i as [[| u] n]; simpl in *; nat_simpl; bool_simpl; [rewrite Hdi in Hi; nat_simpl | ];
    inversion Hi.
   case_eq (DB_eq (DB_lift_ d i') ¡(0,0)); intros Hi'.
    destruct i' as [[| u'] n']; simpl in *; nat_simpl; bool_simpl;
    [rewrite Hdi' in Hi'; nat_simpl | ]; inversion Hi'.
    apply refl_equal.
  case_eq (DB_eq (DB_lift_ d i) ¡(0,n'')); intros Hin''.
   destruct i as [[| u] n]; simpl in *; nat_simpl; bool_simpl.
    rewrite Hdi in Hin''; nat_simpl; reflect_in nat_eq_imp Hin'';
     rewrite <- Hin'' in *; nat_simpl; rewrite Hdi; nat_simpl; reflect_in nat_le_imp Hdi;
     rewrite (imppt2 nat_le_imp _ _ (le_S _ _ Hdi)); simpl;
     case_eq (DB_eq i' ¡(0,S n)); intros Hi'n.
     reflect_in DB_eq_imp Hi'n; rewrite Hi'n in *; simpl in *; nat_simpl; bool_simpl; rewrite Hdi';
      nat_simpl; apply refl_equal.
     destruct i' as [[| u'] n']; simpl in *; nat_simpl; bool_simpl;
     [rewrite Hdi'; nat_simpl; rewrite Hi'n | ]; apply refl_equal.
    inversion Hin''.
  case_eq (DB_eq i ¡(0,n'')); intros H'in''.
   reflect_in DB_eq_imp H'in''; rewrite H'in'' in *; simpl in *; nat_simpl; bool_simpl; rewrite Hdi;
    nat_simpl; apply refl_equal.
   case_eq (DB_eq i' ¡(0,n'')); intros Hi'n''.
    reflect_in DB_eq_imp Hi'n''; rewrite Hi'n'' in *; simpl in *; nat_simpl;
     rewrite Hdi'; nat_simpl; bool_simpl; destruct i as [[| u] n]; simpl in *; nat_simpl;
      bool_simpl.
     rewrite Hdi; nat_simpl; rewrite H'in''; apply refl_equal.
     apply refl_equal.
    destruct i as [[| u] n]; simpl in *; nat_simpl; bool_simpl.
     rewrite Hdi in *; nat_simpl; rewrite H'in''; simpl; destruct i' as [[| u'] n']; simpl in *;
      nat_simpl; bool_simpl; [rewrite Hdi'; nat_simpl; rewrite Hi'n'' | ]; apply refl_equal.
     destruct i' as [[| u'] n']; simpl in *; nat_simpl; bool_simpl;
     [rewrite Hdi'; nat_simpl; rewrite Hi'n'' | ]; apply refl_equal.
  case_eq (DB_eq i ¡(S u'',0)); intros Hiu''.
   reflect_in DB_eq_imp Hiu''; rewrite Hiu'' in *; simpl in *; nat_simpl; apply refl_equal.
   destruct i as [[| u] n]; simpl in *; nat_simpl; bool_simpl; case_eq (DB_eq i' ¡(S u'',0));
    intros Hi'u''.
    reflect_in DB_eq_imp Hi'u''; rewrite Hi'u'' in *; simpl in *; nat_simpl; apply refl_equal.
    destruct i' as [[| u'] n']; simpl in *; nat_simpl; bool_simpl; [ | rewrite Hi'u''];
     apply refl_equal.
    rewrite Hiu''; reflect_in DB_eq_imp Hi'u''; rewrite Hi'u'' in *; simpl in *; nat_simpl;
     simpl; apply refl_equal.
    rewrite Hiu''; destruct i' as [[| u'] n']; simpl in *; nat_simpl; bool_simpl.
     apply refl_equal.
     rewrite Hi'u''; apply refl_equal.
  case_eq (DB_eq i ¡(S u'',S n'')); intros Hii''.
   reflect_in DB_eq_imp Hii''; rewrite Hii'' in *; simpl in *; nat_simpl; apply refl_equal.
   case_eq (DB_eq i' ¡(S u'',S n'')); intros Hi'i''.
    reflect_in DB_eq_imp Hi'i''; rewrite Hi'i'' in *; simpl in *; nat_simpl;
     destruct i as [[| u] n]; simpl in *; nat_simpl; bool_simpl; [ | rewrite Hii''];
     apply refl_equal.
    destruct i as [[| u] n]; destruct i' as [[| u'] n']; simpl in *; nat_simpl; bool_simpl.
     apply refl_equal.
     rewrite Hi'i''; apply refl_equal.
     rewrite Hii''; apply refl_equal.
     rewrite Hii''; rewrite Hi'i''; apply refl_equal.
Qed.

Theorem MS_dec_lift_:forall (m:Μ)(i:Ι)(d:nat), ≤Ι(d,i)->
                     MS_eq (MS_lift_ d (≺i←(m i)≻⊕(m⊖i)))
                           (≺(DB_lift_ d i)←((MS_lift_ d m) (DB_lift_ d i))≻⊕
                           ((MS_lift_ d m)⊖(DB_lift_ d i))).
Proof.
 intros m i d Hdi i'; rewrite (MS_ins_rem_lift_ m _ _ (m i) _ Hdi Hdi); unfold MS_ins;
  destruct (DB_eq (DB_lift_ d i) i').
  apply (sym_equal (MS_get_lift_ m _ _ Hdi)).
  apply refl_equal.
Qed.

Theorem MS_eq_lift_:forall (m1 m2:Μ), MS_eq m1 m2->
                    forall (d:nat), MS_eq (MS_lift_ d m1) (MS_lift_ d m2).
Proof.
 intros m1 m2 Heq d [[| u] [| n]]; unfold MS_lift_; try (apply refl_equal); rewrite Heq;
  apply refl_equal.
Qed.

Theorem MS_eq_mesubst_:forall (t:Τ)(m1 m2:Μ), MS_eq m1 m2->
                       forall (d:nat), Bt_mesubst_ d m1 t=Bt_mesubst_ d m2 t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (m1 m2:Μ), MS_eq m1 m2->
                            forall (d:nat), Bt_mesubst_ d m1 t=Bt_mesubst_ d m2 t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind m1 m2 Heq d;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ _ Heq d); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) _ _ Heq d); irewrite (Hind _ (sdepth_r t1 t2) _ _ Heq d);
       apply refl_equal).
  destruct u' as [| u']; rewrite Heq; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) _ _ Heq d);
   irewrite (Hind _ (sdepth_r t1 t2) _ _ (MS_eq_lift_ _ _ Heq d) (S d)); apply refl_equal.
  irewrite (Hind _ (sdepth t) _ _ (MS_eq_lift_ _ _ Heq d) (S d)); apply refl_equal.
Qed.

Theorem MS_eq_mesubst:forall (t:Τ)(m1 m2:Μ), MS_eq m1 m2->〈〈m1@t〉〉=〈〈m2@t〉〉.
Proof.
 intros t m1 m2 Heq; apply MS_eq_mesubst_; apply Heq.
Qed.

Theorem MS_lift_dbl:forall (m:Μ)(d d':nat), d'<=d->
                    MS_eq (MS_lift_ d' (MS_lift_ d m)) (MS_lift_ (S d) (MS_lift_ d' m)).
Proof.
 intros m d d' Hd'd [[| u] n].
  destruct n as [| n]; simpl; [apply refl_equal |].
   destruct n as [| n]; simpl; nat_simpl.
    destruct (nat_le d' 0); apply refl_equal.
    case_eq (nat_le d n); simpl; intros Hdn; nat_simpl.
     reflect_in nat_le_imp Hdn; rewrite (imppt2 nat_le_imp _ _ (le_trans _ _ _ Hd'd Hdn));
      rewrite (imppt2 nat_le_imp _ _ (le_S _ _ (le_trans _ _ _ Hd'd Hdn)));
      irewrite (Bt_lift_lift_ (m ¡(0,n)) _ _ Hd'd); apply refl_equal.
     destruct (nat_le d' (S n)); apply refl_equal.
  simpl; irewrite (Bt_lift_lift_ (m ¡(S u,n)) _ _ Hd'd); apply refl_equal.
Qed.

(* Nil substitution ------------------------------------------------------------------------------*)

Theorem MS_nil_lift:forall (d:nat), MS_eq (MS_lift_ d ⊚) ⊚.
Proof.
 intros d [[| u] [| n]]; unfold MS_lift_; unfold MS_nil; simpl; nat_simpl; try (apply refl_equal).
  destruct (nat_le d n); apply refl_equal.
Qed.

Theorem Bt_mesubst_nil_:forall (t:Τ)(d:nat), Bt_mesubst_ d ⊚ t=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d:nat), Bt_mesubst_ d ⊚ t=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind d;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) d); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) d); irewrite (Hind _ (sdepth_r t1 t2) d);
       apply refl_equal).
  destruct (nat_le d n'); [unfold MS_nil | ]; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) d); irewrite (MS_eq_mesubst_ t2 _ _ (MS_nil_lift d) (S d));
   irewrite (Hind _ (sdepth_r t1 t2) (S d)); apply refl_equal.
  irewrite (MS_eq_mesubst_ t _ _ (MS_nil_lift d) (S d)); irewrite (Hind _ (sdepth t) (S d));
   apply refl_equal.
Qed.

Theorem Bt_mesubst_nil:forall (t:Τ), 〈〈⊚@t〉〉=t.
Proof.
 intros t; apply Bt_mesubst_nil_.
Qed.

(* Simulation of elementary substitution  --------------------------------------------------------*)

Theorem Bt_mesubst_ins_:forall (t:Τ)(i:Ι)(e:Ε)(d:nat), ≤Ι(d,i)->
                        Bt_mesubst_ d (MS_ins ⊚ i e) t=Bt_esubst_ d i t e.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(e:Ε)(d:nat), ≤Ι(d,i)->
                            Bt_mesubst_ d (MS_ins ⊚ i e) t=Bt_esubst_ d i t e));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i e d Hdi;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ e _ Hdi); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) _ e _ Hdi); irewrite (Hind _ (sdepth_r t1 t2) _ e _ Hdi);
       apply refl_equal).
  case_eq (nat_le d n'); intros Hdn'.
   unfold MS_ins; unfold MS_nil; apply refl_equal.
   case_eq (DB_eq i ¡(0,n')); intros Hin'.
    reflect_in DB_eq_imp Hin'; rewrite Hin' in Hdi; simpl in Hdi; rewrite Hdi in Hdn';
     inversion Hdn'.
    apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) _ e _ Hdi);
   irewrite (MS_eq_mesubst_ t2 _ _ (MS_ins_lift_ ⊚ i e d Hdi) (S d));
   irewrite (MS_eq_mesubst_ t2 _ _
                            (MS_eq_ins_eq _ _ (DB_lift_ d i) (Be_lift_ d e) (MS_nil_lift d)) (S d));
   rewrite <- DB_le_lift in Hdi; irewrite (Hind _ (sdepth_r t1 t2) _ (Be_lift_ d e) _ Hdi);
   apply refl_equal.
  irewrite (MS_eq_mesubst_ t _ _ (MS_ins_lift_ ⊚ i e d Hdi) (S d));
   irewrite (MS_eq_mesubst_ t _ _
                            (MS_eq_ins_eq _ _ (DB_lift_ d i) (Be_lift_ d e) (MS_nil_lift d)) (S d));
   rewrite <- DB_le_lift in Hdi; irewrite (Hind _ (sdepth t) _ (Be_lift_ d e) _ Hdi);
   apply refl_equal.
Qed.

Theorem Bt_mesubst_ins:forall (t:Τ)(i:Ι)(e:Ε), 〈〈≺i←e≻⊕⊚@t〉〉=〈i←e@t〉.
Proof.
 intros t i e; apply Bt_mesubst_ins_ with (1:=DB_le_0 i).
Qed.

(* Simulation of lifting -------------------------------------------------------------------------*)

Definition MS_Bt_lift_(d:nat):Μ :=
 fun (i':Ι) => match i' with ¡(u',n') => ∂(¡(u',if eq_0 u' && nat_le d n' then S n' else n')) end.

Theorem MS_Bt_lift_lift_:forall (d d':nat), d'<=d->
                         MS_eq (MS_lift_ d' (MS_Bt_lift_ d)) (MS_Bt_lift_ (S d)).
Proof.
 intros d d' Hdd' [[| u] [| n]]; unfold MS_lift_; unfold MS_Bt_lift_; simpl; nat_simpl;
  try (apply refl_equal).
  case_eq (nat_le d n); intros Hdn; case_eq (nat_le d' n); intros Hd'n; try (apply refl_equal).
   reflect_in nat_le_imp Hd'n; rewrite (imppt2 nat_le_imp _ _ (le_S _ _ Hd'n)); apply refl_equal.
   reflect_in nat_le_imp Hdn; reflect_in nat_le_imp Hd'n;
    destruct (lt_irrefl _ (le_lt_trans _ _ _ Hdd' (le_lt_trans _ _ _ Hdn (not_le _ _ Hd'n)))).
Qed.

Theorem MS_lift_lift_:forall (t:Τ)(d d':nat), d'<=d->Bt_mesubst_ d' (MS_Bt_lift_ d) t=Bt_lift_ d t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d d':nat), d'<=d->
                            Bt_mesubst_ d' (MS_Bt_lift_ d) t=Bt_lift_ d t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind d d' Hdd';
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ _ Hdd'); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) _ _ Hdd'); irewrite (Hind _ (sdepth_r t1 t2) _ _ Hdd');
       apply refl_equal).
  case_eq (nat_le d n'); intros Hdn'; case_eq (nat_le d' n'); intros Hd'n'; try (apply refl_equal).
   reflect_in nat_le_imp Hdn'; reflect_in nat_le_imp Hd'n';
   destruct (lt_irrefl _ (le_lt_trans _ _ _ Hdd' (le_lt_trans _ _ _ Hdn' (not_le _ _ Hd'n')))).
  irewrite (Hind _ (sdepth_l t1 t2) _ _ Hdd');
   irewrite (MS_eq_mesubst_ t2 _ _ (MS_Bt_lift_lift_ _ _ Hdd') (S d'));
   irewrite (Hind _ (sdepth_r t1 t2) _ _ (le_n_S _ _ Hdd')); apply refl_equal.
  irewrite (MS_eq_mesubst_ t _ _ (MS_Bt_lift_lift_ _ _ Hdd') (S d'));
   irewrite (Hind _ (sdepth t) _ _ (le_n_S _ _ Hdd')); apply refl_equal.
Qed.

Theorem MS_lift_lift:forall (t:Τ)(d:nat), 〈〈MS_Bt_lift_ d@t〉〉=Bt_lift_ d t.
Proof.
 intros t d; apply MS_lift_lift_; apply le_O_n.
Qed.

Theorem MS_lift_distr:forall (t:Τ)(m:Μ)(d:nat),
                      Bt_mesubst_ (S d) (MS_lift_ d m) (Bt_lift_ d t)=
                      Bt_lift_ d (Bt_mesubst_ d m t).
Proof.
 apply (wf_term (fun (t:Τ)=> forall (m:Μ)(d:nat),
                             Bt_mesubst_ (S d) (MS_lift_ d m) (Bt_lift_ d t)=
                             Bt_lift_ d (Bt_mesubst_ d m t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind m d;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) m d); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) m d); irewrite (Hind _ (sdepth_r t1 t2) m d);
       apply refl_equal).
  case_eq (nat_le d n'); simpl; intros Hdn'; nat_simpl; rewrite Hdn'; [apply refl_equal | ].
   reflect_in nat_le_imp Hdn';
    rewrite (imprf2 nat_le_imp _ _ (lt_not_le _ _ (lt_S _ _ (not_le _ _ Hdn')))); apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) m d);
   irewrite (Hind _ (sdepth_r t1 t2) (MS_lift_ d m) (S d)); apply refl_equal.
  irewrite (Hind _ (sdepth t) (MS_lift_ d m) (S d)); apply refl_equal.
Qed.

(* Simulation of abstraction ---------------------------------------------------------------------*)

Definition MS_abstr_(d:nat)(i:Ι):Μ :=
 fun (i':Ι) =>
 match i' with ¡(u',n') =>
  ∂(if DB_eq i ¡(u',n') then ¡(0,d) else ¡(u',if (eq_0 u') && (nat_le d n') then S n' else n'))
 end.

Theorem MS_abstr_lift_:forall (d d':nat)(i:Ι), d'<=d->≤Ι(d,i)->
                       MS_eq (MS_lift_ d' (MS_abstr_ d i)) (MS_abstr_ (S d) (DB_lift_ d i)).
Proof.
 intros d d' [[| u] [| n]] Hd'd Hdi [[| u'] [| n']]; unfold MS_abstr_; unfold MS_lift_; simpl in *;
  nat_simpl; bool_simpl; try (apply refl_equal); try (rewrite (imppt2 nat_le_imp _ _ Hdi));
  nat_simpl; try (apply refl_equal); try (rewrite Hdi); nat_simpl; try (apply refl_equal).
  destruct d as [| d]; nat_simpl; [ | inversion Hdi]; inversion Hd'd; nat_simpl;
   destruct (nat_eq 0 n'); simpl; nat_simpl; apply refl_equal.
  case_eq (nat_le d n'); intros Hdn'.
   reflect_in nat_le_imp Hdn'; rewrite (imppt2 nat_le_imp _ _ (le_trans _ _ _ Hd'd Hdn'));
    destruct (nat_eq (S n) n'); simpl;
    [rewrite (imppt2 nat_le_imp _ _ Hd'd) |
     rewrite (imppt2 nat_le_imp _ _ (le_S _ _ (le_trans _ _ _ Hd'd Hdn')))]; apply refl_equal.
   case_eq (nat_le d' n'); intros Hd'n'; simpl.
    case_eq (nat_eq (S n) n'); intros HSnn'; simpl;
    [rewrite (imppt2 nat_le_imp _ _ Hd'd) | rewrite Hd'n']; apply refl_equal.
    case_eq (nat_eq (S n) n'); intros HSnn'; simpl; [ | apply refl_equal].
     reflect_in nat_eq_imp HSnn'; rewrite HSnn' in Hdi; rewrite Hdn' in Hdi; inversion Hdi.
  case_eq (nat_le d n'); intros Hdn'.
   reflect_in nat_le_imp Hdn'; rewrite (imppt2 nat_le_imp _ _ (le_trans _ _ _ Hd'd Hdn')); simpl;
    rewrite (imppt2 nat_le_imp _ _ (le_S _ _ (le_trans _ _ _ Hd'd Hdn'))); apply refl_equal.
   case_eq (nat_le d' n'); intros Hd'n'; simpl; [rewrite Hd'n' | ]; apply refl_equal.
  destruct (nat_eq u u'); simpl; [rewrite (imppt2 nat_le_imp _ _ Hd'd) | ]; apply refl_equal.
  case_eq (nat_le d n'); intros Hdn'; simpl.
   reflect_in nat_le_imp Hdn'; rewrite (imppt2 nat_le_imp _ _ (le_trans _ _ _ Hd'd Hdn')); simpl;
    rewrite (imppt2 nat_le_imp _ _ (le_S _ _ (le_trans _ _ _ Hd'd Hdn'))); apply refl_equal.
   case_eq (nat_le d' n'); intros Hd'n'; simpl; apply refl_equal.
  destruct (nat_eq u u'); simpl; [ | apply refl_equal].
   destruct (nat_eq n n'); simpl; [rewrite (imppt2 nat_le_imp _ _ Hd'd) | ]; apply refl_equal.
Qed.

Theorem MS_abstr_abstr:forall (t:Τ)(d d':nat)(i:Ι), d'<=d->≤Ι(d,i)->
                       Bt_mesubst_ d' (MS_abstr_ d i) t=Bt_abstr_ d i t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d d':nat)(i:Ι), d'<=d->≤Ι(d,i)->
                            Bt_mesubst_ d' (MS_abstr_ d i) t=Bt_abstr_ d i t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind d d' i Hd'd Hdi;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ _ _ Hd'd Hdi); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) _ _ _ Hd'd Hdi);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ _ Hd'd Hdi); apply refl_equal).
  case_eq (nat_le d n'); intros Hdn'; simpl.
   reflect_in nat_le_imp Hdn'; rewrite (imppt2 nat_le_imp _ _ (le_trans _ _ _ Hd'd Hdn'));
    apply refl_equal.
   case_eq (nat_le d' n'); intros Hd'n'; simpl; [ | apply refl_equal].
    case_eq (DB_eq i ¡(0,n')); intros Hin'; simpl; [ | apply refl_equal].
     reflect_in DB_eq_imp Hin'; rewrite Hin' in *; simpl in *; rewrite Hdn' in Hdi; inversion Hdi.
  irewrite (Hind _ (sdepth_l t1 t2) _ _ _ Hd'd Hdi);
   irewrite (MS_eq_mesubst_ t2 _ _ (MS_abstr_lift_ _ _ _ Hd'd Hdi) (S d'));
   rewrite <- DB_le_lift in Hdi; irewrite (Hind _ (sdepth_r t1 t2) _ _ _ (le_n_S _ _ Hd'd) Hdi);
   apply refl_equal.
  irewrite (MS_eq_mesubst_ t _ _ (MS_abstr_lift_ _ _ _ Hd'd Hdi) (S d'));
   rewrite <- DB_le_lift in Hdi; irewrite (Hind _ (sdepth t) _ _ _ (le_n_S _ _ Hd'd) Hdi);
   apply refl_equal.
Qed.

(* Simulation of application ---------------------------------------------------------------------*)

Definition MS_app_(d:nat)(a:Ε):Μ :=
 fun (i':Ι) =>
 match i' with
 | ¡(0,n') => if (nat_le d n') then (if (nat_eq d n') then a else ∂(¡(0,pred n'))) else ∂(i')
 | _ => ∂(i')
 end.

Theorem MS_app_lift_:forall (d d':nat)(a:Ε), d'<=d->
                     MS_eq (MS_lift_ d' (MS_app_ d a)) (MS_app_ (S d) (Be_lift_ d' a)).
Proof.
 intros d d' a Hdd' [[| u'] [| n']]; unfold MS_app_; unfold MS_lift_; simpl; nat_simpl; bool_simpl;
  try (apply refl_equal).
  case_eq (nat_le d n'); intros Hdn'; simpl.
   reflect_in nat_le_imp Hdn'; rewrite (imppt2 nat_le_imp _ _ (le_trans _ _ _ Hdd' Hdn'));
    case_eq (nat_eq d n'); intros H'dn'; simpl; [apply refl_equal | ].
    case_eq (nat_le d' (pred n')); intros Hd'n'; simpl.
     destruct n' as [| n']; simpl; [ | apply refl_equal].
      inversion Hdn'; rewrite H in H'dn'; nat_simpl; inversion H'dn'.
     destruct n' as [| n']; simpl in *; [apply refl_equal | ].
      reflect_in nat_le_imp Hd'n';
       rewrite (le_antisym _ _ (le_trans _ _ _ (not_le _ _ Hd'n') Hdd') Hdn') in H'dn'; nat_simpl;
       inversion H'dn'.
    destruct (nat_le d' n'); apply refl_equal.
Qed.

Theorem MS_app_app:forall (t:Τ)(d:nat)(a:Ε), Bt_mesubst_ d (MS_app_ d a) t=Bt_app_ d t a.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d:nat)(a:Ε), Bt_mesubst_ d (MS_app_ d a) t=Bt_app_ d t a));
  intros [[ | t | t | t1 t2 | t1 t2 | n' |  [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind d a;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) d a); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) d a); irewrite (Hind _ (sdepth_r t1 t2) d a);
       apply refl_equal).
  case_eq (nat_le d n'); intros Hdn'; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) d a);
   irewrite (MS_eq_mesubst_ t2 _ _ (MS_app_lift_ _ _ a (le_refl d)) (S d));
   irewrite (Hind _ (sdepth_r t1 t2) (S d) (Be_lift_ d a)); apply refl_equal.
  irewrite (MS_eq_mesubst_ t _ _ (MS_app_lift_ _ _ a (le_refl d)) (S d));
   irewrite (Hind _ (sdepth t) (S d) (Be_lift_ d a)); apply refl_equal.
Qed.

(* Simulation of substitution --------------------------------------------------------------------*)

Definition MS_esubst(i:Ι)(a:Ε):Μ := fun (i':Ι) => if (DB_eq i i') then a else ∂(i').

Theorem MS_esubst_lift_:forall (i:Ι)(a:Ε)(d d':nat), d'<=d->≤Ι(d,i)->
                        MS_eq (MS_lift_ d' (MS_esubst i a))
                              (MS_esubst (DB_lift_ d i) (Be_lift_ d' a)).
Proof.
 intros [[| u] [| n]] a d d' Hd'd Hdi [[| u'] [| n']]; unfold MS_esubst; unfold MS_lift_;
  simpl in *; nat_simpl; bool_simpl; simpl; nat_simpl; try (apply refl_equal);
  try (rewrite (imppt2 nat_le_imp _ _ Hdi)); nat_simpl; try (apply refl_equal);
  try (rewrite Hdi); nat_simpl; try (apply refl_equal).
  case_eq (nat_le d' n'); intros Hd'n'; simpl.
   destruct (nat_eq 0 n'); simpl; [ | rewrite Hd'n']; apply refl_equal.
   case_eq (nat_eq 0 n'); intros Hn'; simpl; [ | apply refl_equal].
    reflect_in nat_eq_imp Hn'; rewrite <- Hn' in *; reflect_in nat_le_imp Hdi; inversion Hdi;
     rewrite H in Hd'd; rewrite (imppt2 nat_le_imp _ _ Hd'd) in Hd'n'; inversion Hd'n'.
  case_eq (nat_le d' n'); intros Hd'n'; simpl.
   destruct (nat_eq (S n) n'); simpl; [ | rewrite Hd'n']; apply refl_equal.
   case_eq (nat_eq (S n) n'); intros Hnn'; [ | apply refl_equal].
    reflect_in nat_eq_imp Hnn'; rewrite Hnn' in *; reflect_in nat_le_imp Hdi;
     rewrite (imppt2 nat_le_imp _ _ (le_trans _ _ _ Hd'd Hdi)) in Hd'n'; inversion Hd'n'.
  destruct (nat_le d' n'); apply refl_equal.
  destruct (nat_eq u u'); apply refl_equal.
  destruct (nat_le d' n'); apply refl_equal.
  destruct (nat_eq u u' && nat_eq n n'); apply refl_equal.
Qed.

Theorem MS_esubst_esubst_:forall (t:Τ)(i:Ι)(a:Ε)(d:nat), ≤Ι(d,i)->
                          Bt_mesubst_ d (MS_esubst i a) t=Bt_esubst_ d i t a.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(a:Ε)(d:nat), ≤Ι(d,i)->
                            Bt_mesubst_ d (MS_esubst i a) t=Bt_esubst_ d i t a));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i a d Hdi;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ a _ Hdi); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) _ a _ Hdi); irewrite (Hind _ (sdepth_r t1 t2) _ a _ Hdi);
       apply refl_equal).
  case_eq (nat_le d n'); intros Hdn'.
   unfold MS_esubst; destruct (DB_eq i ¡(0,n')); apply refl_equal.
   case_eq(DB_eq i ¡(0,n')); intros Hin'; simpl; [ | apply refl_equal].
    reflect_in DB_eq_imp Hin'; rewrite Hin' in Hdi; simpl in *; rewrite Hdn' in Hdi; inversion Hdi.
  irewrite (Hind _ (sdepth_l t1 t2) _ a _ Hdi);
   irewrite (MS_eq_mesubst_ t2 _ _ (MS_esubst_lift_ _ a _ _ (le_refl d) Hdi) (S d));
   rewrite <- DB_le_lift in Hdi; irewrite (Hind _ (sdepth_r t1 t2) _ (Be_lift_ d a) _ Hdi);
   apply refl_equal.
  irewrite (MS_eq_mesubst_ t _ _ (MS_esubst_lift_ _ a _ _ (le_refl d) Hdi) (S d));
   rewrite <- DB_le_lift in Hdi; irewrite (Hind _ (sdepth t) _ (Be_lift_ d a) _ Hdi);
   apply refl_equal.
Qed.

(* Composition of multiple substitutions ---------------------------------------------------------*)

Definition MS_cmp_(d:nat)(m1 m2:Μ):Μ := fun (i':Ι) => Be_mesubst_ d m1 (m2 i').
Notation "m1 '⊙' m2" := (MS_cmp_ 0 m1 m2) (right associativity, at level 60).

Theorem MS_cmp_lift_:forall (m1 m2:Μ)(d:nat),
                     MS_eq (MS_lift_ d (MS_cmp_ d m2 m1))
                           (MS_cmp_ (S d) (MS_lift_ d m2) (MS_lift_ d m1)).
Proof.
 intros m1 m2 d [[| u] n]; unfold MS_cmp_; unfold MS_lift_ at 3.
  destruct n as [| n]; simpl; nat_simpl.
   destruct (nat_le d 0); apply refl_equal.
   case_eq (nat_le d n); intros Hdn; simpl.
    irewrite (MS_lift_distr (m1 ¡(0,n)) m2 d); apply refl_equal.
    nat_simpl; rewrite Hdn; apply refl_equal.
  irewrite (MS_lift_distr (m1 ¡(S u,n)) m2 d); apply refl_equal.
Qed.

Theorem MS_cmp_cmp_:forall (t:Τ)(m1 m2:Μ)(d:nat),
                    Bt_mesubst_ d (MS_cmp_ d m2 m1) t=Bt_mesubst_ d m2 (Bt_mesubst_ d m1 t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (m1 m2:Μ)(d:nat),
                 Bt_mesubst_ d (MS_cmp_ d m2 m1) t=Bt_mesubst_ d m2 (Bt_mesubst_ d m1 t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind m1 m2 d;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) m1 m2 d); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) m1 m2 d); irewrite (Hind _ (sdepth_r t1 t2) m1 m2 d);
       apply refl_equal).
  case_eq (nat_le d n'); intros Hdn'; [unfold MS_cmp_ | simpl; rewrite Hdn']; apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) m1 m2 d);
   irewrite (MS_eq_mesubst_ t2 _ _ (MS_cmp_lift_ m1 m2 d) (S d));
   irewrite (Hind _ (sdepth_r t1 t2) (MS_lift_ d m1) (MS_lift_ d m2) (S d)); apply refl_equal.
  irewrite (MS_eq_mesubst_ t _ _ (MS_cmp_lift_ m1 m2 d) (S d));
   irewrite (Hind _ (sdepth t) (MS_lift_ d m1) (MS_lift_ d m2) (S d)); apply refl_equal.
Qed.

Theorem MS_cmp_cmp:forall (t:Τ)(m1 m2:Μ), 〈〈m2⊙m1@t〉〉=〈〈m2@〈〈m1@t〉〉〉〉.
Proof.
 intros t m1 m2; apply MS_cmp_cmp_.
Qed.

(* Semantical vision -----------------------------------------------------------------------------*)
(* We provide here a semantical vision of multiple substitutions, expliciting their parallel nature
   by comparing them with multiple abstraction followed by multiple application. This generalises
   the result about the decomposition of substitution to multiple substitutions. *)

Theorem MS_sem:forall (t:Τ)(m:Μ)(i:Ι)(e:Ε)(d:nat), ≤Ι(d,i)->
               Bt_mesubst_ d (≺i←e≻⊕m) t=
               Bt_app_ d (Bt_mesubst_ (S d) (MS_lift_ d m) (Bt_abstr_ d i t)) e.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (m:Μ)(i:Ι)(e:Ε)(d:nat), ≤Ι(d,i)->
                            Bt_mesubst_ d (≺i←e≻⊕m) t=
                            Bt_app_ d (Bt_mesubst_ (S d) (MS_lift_ d m) (Bt_abstr_ d i t)) e));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind m i e d Hdi;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) m _ e _ Hdi); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) m _ e _ Hdi);
       irewrite (Hind _ (sdepth_r t1 t2) m _ e _ Hdi); apply refl_equal).
  case_eq (DB_eq i i'); intros Hii'; simpl; unfold MS_ins; repeat (rewrite Hii');
   destruct i' as [[| u'] n']; simpl in *; nat_simpl; bool_simpl; repeat (rewrite Hii').
   reflect_in DB_eq_imp Hii'; rewrite Hii' in *; clear i Hii'; simpl in Hdi; rewrite Hdi; nat_simpl;
    simpl; nat_simpl; DB_simpl; simpl; nat_simpl; apply refl_equal.
   simpl; nat_simpl; apply refl_equal.
   case_eq (nat_le d n'); intros Hdn'; simpl; nat_simpl.
    rewrite Hdn'; apply (sym_equal (Bt_lift_app_ (m ¡(0,n')) d e)).
    reflect_in nat_le_imp Hdn';
     rewrite (imprf2 nat_le_imp _ _ (lt_not_le _ _ (lt_S _ _ (not_le _ _ Hdn')))); simpl;
     reflect_in nat_le_imp Hdn'; rewrite Hdn'; apply refl_equal.
   apply (sym_equal (Bt_lift_app_ (m ¡(S u',n')) d e)).
   irewrite (Hind _ (sdepth_l t1 t2) m _ e _ Hdi);
    irewrite (MS_eq_mesubst_ t2 _ _  (MS_ins_lift_ m _ e _ Hdi) (S d));
    rewrite <- DB_le_lift in Hdi;
    irewrite (Hind _ (sdepth_r t1 t2) (MS_lift_ d m) _ (Be_lift_ d e) _ Hdi); apply refl_equal.
  irewrite (MS_eq_mesubst_ t _ _  (MS_ins_lift_ m _ e _ Hdi) (S d));
   rewrite <- DB_le_lift in Hdi; irewrite (Hind _ (sdepth t) (MS_lift_ d m) _ (Be_lift_ d e) _ Hdi);
   apply refl_equal.
Qed.

(* Additional results ----------------------------------------------------------------------------*)

Theorem Bt_mesubst_ins_free:forall (t:Τ)(i:Ι)(d:nat), ≤Ι(d,i)->Bt_free_ d i t=⊥->
                            forall (m:Μ)(e:Ε), Bt_mesubst_ d (≺i←e≻⊕m) t=Bt_mesubst_ d m t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat), ≤Ι(d,i)->Bt_free_ d i t=⊥->
                            forall (m:Μ)(e:Ε), Bt_mesubst_ d (≺i←e≻⊕m) t=Bt_mesubst_ d m t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i d Hdi Hit m e;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ _ Hdi Hit m e); apply refl_equal);
  try (destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2];
       irewrite (Hind _ (sdepth_l t1 t2) _ _ Hdi Hit1 m e);
       irewrite (Hind _ (sdepth_r t1 t2) _ _ Hdi Hit2 m e); apply refl_equal).
  induction i' as [[| u'] n']; simpl in *.
   case_eq (nat_le d n'); intros Hdn'; rewrite Hdn' in *; simpl in *; [ | apply refl_equal]. 
    reflect_in DB_eq_imp Hit; rewrite (MS_ins_diff m _ _ e Hit); apply refl_equal.
   reflect_in DB_eq_imp Hit; rewrite (MS_ins_diff m _ _ e Hit); apply refl_equal.
  destruct (orb_false_elim _ _ Hit) as [Hit1 Hit2];
   irewrite (Hind _ (sdepth_l t1 t2) _ _ Hdi Hit1 m e).
   irewrite (MS_eq_mesubst_ t2 _ _ (MS_ins_lift_ m _ e _ Hdi) (S d)); rewrite <- DB_le_lift in Hdi;
   irewrite (Hind _ (sdepth_r t1 t2) _ _ Hdi Hit2 (MS_lift_ d m) (Be_lift_ d e)); apply refl_equal.
  irewrite (MS_eq_mesubst_ t _ _ (MS_ins_lift_ m _ e _ Hdi) (S d)); rewrite <- DB_le_lift in Hdi;
   irewrite (Hind _ (sdepth t) _ _ Hdi Hit (MS_lift_ d m) (Be_lift_ d e)); apply refl_equal.
Qed.

Theorem Bt_mesubst_rem_free:forall (t:Τ)(i:Ι)(d:nat), ≤Ι(d,i)->Bt_free_ d i t=⊥->
                            forall (m:Μ), Bt_mesubst_ d (m⊖i) t=Bt_mesubst_ d m t.
Proof.
 intros t i d Hdi Hit m; change (m⊖i) with (≺i←∂(i)≻⊕m); apply (Bt_mesubst_ins_free _ _ _ Hdi Hit).
Qed.

(*================================================================================================*)
