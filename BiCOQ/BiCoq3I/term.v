(*==================================================================================================
  Project : BiCoq3
  Module : term.v
  This module defines B terms, that is expressions and predicates, using a De Bruijn representation.
  An infinity of namespaces is defined, 0 being the classical one in which our binders capture
  variables; the other namespaces (1 and greater) have no associated binders so all such De Bruijn
  indexes are dangling and represent free variables. The idea can be extended by associating to any
  binder a natural parameter to represent the namespace which is bound, but at the cost of
  (slightly) more complex functions over terms and more importantly of alpha-equivalence (unless
  namespaces represent constant sorts), but these aspects are still to be explored.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jun 2008, Md: Mar 2009
  ================================================================================================*)

Require Export index.
Require Export Peano_dec.
Require Export Max.

(* B terms ---------------------------------------------------------------------------------------*)

Inductive Bprd:Set :=
 | bnot:Bprd->Bprd        (* Negation *)
 | bfor:Bprd->Bprd        (* Universal De Bruijn quantifier *)
 | band:Bprd->Bprd->Bprd  (* Conjunction *)
 | bimp:Bprd->Bprd->Bprd  (* Implication *)
 | bprp:nat->Bprd         (* Propositional variable *)
 | bequ:Bexp->Bexp->Bprd  (* Equality *)
 | bins:Bexp->Bexp->Bprd  (* Membership *)
with Bexp:Set :=
 | bbig:Bexp              (* BIG set *)
 | bchc:Bexp->Bexp        (* Choice *)
 | bpow:Bexp->Bexp        (* Powerset *)
 | bcpl:Bexp->Bexp->Bexp  (* Pair *)
 | bpro:Bexp->Bexp->Bexp  (* Cartesian product *)
 | bbge:nat->Bexp         (* Element of big *)
 | bvar:Ι->Bexp           (* De Bruijn index *)
 | bcmp:Bexp->Bprd->Bexp. (* Comprehension set binder *)

Notation "'Π'" := (Bprd).
Notation "'¬' p" := (bnot p) (at level 75, right associativity).
Notation "'∀Δ(' p ')'" := (bfor p).
Notation "p '⋀' q" := (band p q) (at level 80, right associativity).
Notation "p '⇒' q" := (bimp p q) (at level 85, right associativity).
Notation "'∐' n" := (bprp n) (at level 30, no associativity).
Notation "e '≡' f" := (bequ e f) (at level 70, no associativity).
Notation "e '∈' f" := (bins e f) (at level 70, no associativity).

Notation "'Ε'" := (Bexp).
Notation "'Ω'" := (bbig).
Notation "'↓(' e ')'" := (bchc e).
Notation "'↑(' e ')'" := (bpow e).
Notation "e '↦' f" := (bcpl e f) (at level 50, left associativity).
Notation "e '×' f" := (bpro e f) (at level 40, left associativity).
Notation "'∞' n" := (bbge n) (at level 30, no associativity).
Notation "'∂(' i ')'" := (bvar i).
Notation "'ⅭΔ(' e ',' p ')'" := (bcmp e p).

(* Additional operators --------------------------------------------------------------------------*)

Definition blor(p q:Π) := ¬p⇒q. (* Disjunction *)
Notation "p '⋁' q" := (blor p q) (at level 85, right associativity).

Definition biff(p q:Π) := (p⇒q)⋀(q⇒p). (* Equivalence *)
Notation "p '⇔' q" := (biff p q) (at level 84, no associativity).

Definition bneq(e f:Ε) := ¬e≡f. (* Difference *)
Notation "e '≢' f" := (bneq e f) (at level 70, no associativity).

Definition bnin(e f:Ε) := ¬e∈f. (* Negation of membership *)
Notation "e '∉' f" := (bnin e f) (at level 70, no associativity).

Definition binc(e f:Ε) := e∈↑(f). (* Inclusion *)
Notation "e '⊆' f" := (binc e f) (at level 70, no associativity).

(* B terms ---------------------------------------------------------------------------------------*)

Inductive Btrm:Set := be_:Ε->Btrm | bp_:Π->Btrm.
Notation "'Τ'" := (Btrm).
Coercion be_:Bexp>->Btrm.
Coercion bp_:Bprd>->Btrm.

(* Tactic for easier rewriting -------------------------------------------------------------------*)
(* It is often the case, when using equality on terms, than those equality cannot be used for
   predicates or expressions, due to the use of coercions. We provide this tactic to help in such
   cases. *)

Ltac srewrite E :=
 let H' := fresh in
 (generalize E; simpl; intros H'; rewrite H'; clear H').

Ltac srewrite_in E H :=
 let H' := fresh in
 (generalize E; simpl; intros H'; rewrite H' in H; clear H').

Ltac irewrite E :=
 let H' := fresh in
 (generalize E; simpl; intros H'; injection H'; clear H'; intros H'; rewrite H'; clear H').

Ltac irewrite_in E H :=
 let H' := fresh in
 (generalize E; simpl; intros H'; injection H'; clear H'; intros H'; rewrite H' in H; clear H').

(* Interface for built-in Ocaml functions --------------------------------------------------------*)

Variable Bp_eq:Π->Π->Β.
Variable Bp_eq_imp:((fun x y:Π=>x=y)⇝²Bp_eq).

Variable Be_eq:Ε->Ε->Β.
Variable Be_eq_imp:((fun x y:Ε=>x=y)⇝²Be_eq).

Definition Bt_eq(t1 t2:Τ):=
 match (t1,t2) with
 | (be_ e1',be_ e2') => Be_eq e1' e2'
 | (bp_ p1',bp_ p2') => Bp_eq p1' p2'
 | _ => false
 end.

Theorem Bt_eq_imp:((fun x y:Τ=>x=y)⇝²Bt_eq).
Proof.
 unfold implements2; split; unfold Bt_eq; intros [e1 | p1] [e2 | p2] Hf.
  rewrite (fst Be_eq_imp _ _ Hf); apply refl_equal.
  inversion Hf.
  inversion Hf.
  rewrite (fst Bp_eq_imp _ _ Hf); apply refl_equal.
  intros Heq; inversion Heq as [Heq']; rewrite (imppt2 Be_eq_imp _ _ Heq') in Hf;
   inversion Hf.
  intros Heq; inversion Heq.
  intros Heq; inversion Heq.
  intros Heq; inversion Heq as [Heq']; rewrite (imppt2 Bp_eq_imp _ _ Heq') in Hf;
   inversion Hf.
Qed.

(* Depth function --------------------------------------------------------------------------------*)

Fixpoint Bp_depth(p:Π){struct p}:nat :=
 match p with
 | ¬p' => S(Bp_depth p')
 | ∀Δ(p') => S(Bp_depth p')
 | p1⋀p2 => S(max (Bp_depth p1) (Bp_depth p2))
 | (p1⇒p2) => S(max (Bp_depth p1) (Bp_depth p2))
 | ∐(_) => 0
 | e1≡e2 => S(max (Be_depth e1) (Be_depth e2))
 | e1∈e2 => S(max (Be_depth e1) (Be_depth e2))
 end
with Be_depth(e:Ε){struct e}:nat :=
 match e with
 | Ω => 0
 | ↓(e') => S(Be_depth e')
 | ↑(e') => S(Be_depth e')
 | e1↦e2 => S(max (Be_depth e1) (Be_depth e2))
 | e1×e2 => S(max (Be_depth e1) (Be_depth e2))
 | ∞(_) => 0
 | ∂(_) => 0
 | ⅭΔ(e',p') => S(max (Be_depth e') (Bp_depth p'))
 end.

Definition Bt_depth(t:Τ):nat := match t with be_ e' => Be_depth e' | bp_ p' => Bp_depth p' end.
Notation "'↧(' t ')'":=(Bt_depth t).
(* Returns the depth of a term. *)

(* Induction over terms --------------------------------------------------------------------------*)

Theorem wf_term:forall (P:Τ->Type),
                (forall (t:Τ), (forall (t':Τ), ↧(t')<↧(t)->P t')->P t)->forall (t:Τ), P t.
Proof.
 intros P Hind; cut  ((forall (p:Π), P p)*(forall (e:Ε), P e)).
  intros [Hp He] [e | p]; [apply He | apply Hp].
  apply (wf_measure_dbl Bp_depth Be_depth P P).
   intros p Hp He; apply (Hind p); intros [e' | p']; [apply (He e') | apply  (Hp p')].
   intros e Hp He; apply (Hind e); intros [e' | p']; [apply (He e') | apply  (Hp p')].
Qed.

Definition sdepth(t:Τ) := (lt_n_Sn ↧(t)).
Definition sdepth_l(t1 t2:Τ) := lt_le_trans _ _ _ (lt_n_Sn _) (le_n_S _ _ (le_max_l ↧(t1) ↧(t2))).
Definition sdepth_r(t1 t2:Τ) := lt_le_trans _ _ _ (lt_n_Sn _) (le_n_S _ _ (le_max_r ↧(t1) ↧(t2))).
(* Nota: These definitions are just "macros" for proofs using wf_term; they allows to build a proof
   that a direct subterm has a depth strictly smaller than the term of which it is extracted. *)

(* Compatibility at a given height ---------------------------------------------------------------*)
(* Check whether a term can be seen as the result of l successive liftings.  *)

Fixpoint Bp_comp_(d l:nat)(p:Π){struct p}:Β :=
 match p with
 | ¬p' => Bp_comp_ d l p'
 | ∀Δ(p') => Bp_comp_ (S d) (S l) p'
 | p1⋀p2 => (Bp_comp_ d l p1) && (Bp_comp_ d l p2)
 | (p1⇒p2) => (Bp_comp_ d l p1) && (Bp_comp_ d l p2)
 | ∐(_) => ⊤
 | e1≡e2 => (Be_comp_ d l e1) && (Be_comp_ d l e2)
 | e1∈e2 => (Be_comp_ d l e1) && (Be_comp_ d l e2)
 end
with Be_comp_(d l:nat)(e:Ε){struct e}:Β :=
 match e with
 | Ω => ⊤
 | ↓(e') => Be_comp_ d l e'
 | ↑(e') => Be_comp_ d l e'
 | e1↦e2 => (Be_comp_ d l e1) && (Be_comp_ d l e2)
 | e1×e2 => (Be_comp_ d l e1) && (Be_comp_ d l e2)
 | ∞(_) => ⊤
 | ∂(¡(u',n')) => if eq_0 u' then (if nat_le d n' then nat_le l n' else ⊤) else ⊤
 | ⅭΔ(e',p') => (Be_comp_ d l e') && (Bp_comp_ (S d) (S l) p')
 end.
(* Nota: the d parameter is here used as the lambda-height record for recursion, while the l
   parameter is the significant one. *)

Definition Bt_comp_(d l:nat)(t:Τ):Β :=
 match t with be_ e' => Be_comp_ d l e' | bp_ p' => Bp_comp_ d l p' end.
Notation "'≤Τ(' l ',' t ')'" := (Bt_comp_ 0 l t=⊤).

Theorem Bt_comp_refl:forall (t:Τ)(d:nat), Bt_comp_ d d t=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d:nat), Bt_comp_ d d t=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind d;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) d));
  try (srewrite (Hind _ (sdepth_l t1 t2)); apply (Hind _ (sdepth_r t1 t2) d)).
  simpl; nat_simpl; destruct (nat_le d n'); apply refl_equal.
  srewrite (Hind _ (sdepth_l t1 t2)); apply (Hind _ (sdepth_r t1 t2) (S d)).
  apply (Hind _ (sdepth t) (S d)).
Qed.

Theorem Bt_comp_invalid:forall (t:Τ)(l d:nat), l<d->Bt_comp_ d l t=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (l d:nat), l<d->Bt_comp_ d l t=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind l d Hld;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) _ _ Hld));
  try (srewrite (Hind _ (sdepth_l t1 t2) _ _ Hld); apply (Hind _ (sdepth_r t1 t2) _ _ Hld)).
  simpl; case_eq (nat_le d n'); intros Hdn'; [ | apply refl_equal].
   reflect_in nat_le_imp Hdn'; reflect nat_le_imp;
    apply (le_S_n _ _ (le_S _ _ (le_trans _ _ _ Hld Hdn'))).
  srewrite (Hind _ (sdepth_l t1 t2) _ _ Hld); apply (Hind _ (sdepth_r t1 t2) _ _ (le_n_S _ _ Hld)).
  apply (Hind _ (sdepth t) _ _ (le_n_S _ _ Hld)).
Qed.

Theorem Bt_comp_evar:forall (i:Ι)(l d:nat), Bt_comp_ d l ∂(i)=negb (DB_le d i) || DB_le l i.
Proof.
 intros [[| u] n] l d; simpl; [destruct (nat_le d n) | ]; apply refl_equal.
Qed.

Theorem Bt_comp_le:forall (i:Ι)(d:nat), Bt_comp_ 0 d ∂(i)=DB_le d i.
Proof.
 intros i d; rewrite Bt_comp_evar; DB_simpl; apply refl_equal.
Qed.

(* Free variables --------------------------------------------------------------------------------*)

Fixpoint Bp_free_(d:nat)(i:Ι)(p:Π){struct p}:Β :=
 match p with
 | ¬p' => Bp_free_ d i p'
 | ∀Δ(p') => Bp_free_ (S d) (DB_lift_ d i) p'
 | p1⋀p2 => (Bp_free_ d i p1) || (Bp_free_ d i p2)
 | (p1⇒p2) => (Bp_free_ d i p1) || (Bp_free_ d i p2)
 | ∐(_) => ⊥
 | e1≡e2 => (Be_free_ d i e1) || (Be_free_ d i e2)
 | e1∈e2 => (Be_free_ d i e1) || (Be_free_ d i e2)
 end
with Be_free_(d:nat)(i:Ι)(e:Ε){struct e}:Β :=
 match e with
 | Ω => ⊥
 | ↓(e') => Be_free_ d i e'
 | ↑(e') => Be_free_ d i e'
 | e1↦e2 => (Be_free_ d i e1) || (Be_free_ d i e2)
 | e1×e2 => (Be_free_ d i e1) || (Be_free_ d i e2)
 | ∞(_) => ⊥
 | ∂(i') => DB_le d i' && DB_eq i i'
 | ⅭΔ(e',p') => (Be_free_ d i e') || (Bp_free_ (S d) (DB_lift_ d i) p')
 end.
(* Nota: We have chosen the strictest version of the definition of the function, that can returns
   true iff i' is pending at lambda-height d *)

Definition Bt_free_(d:nat)(i:Ι)(t:Τ):Β :=
 match t with be_ e' => Be_free_ d i e' | bp_ p' => Bp_free_ d i p' end.
(* Indicates if a variable appears free in a term. *)
Notation "i '∖' t":=(Bt_free_ 0 i t=⊥) (at level 96, no associativity).

Theorem Bt_free_le_:forall (t:Τ)(i:Ι)(d:nat), Bt_free_ d i t=⊤->≤Ι(d,i).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat), Bt_free_ d i t=⊤->≤Ι(d,i)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i d Ht;
  try (inversion Ht; fail); try (apply (Hind _ (sdepth t) _ _ Ht));
  try (destruct (orb_true_elim _ _ Ht) as [Ht1 | Ht2];
       [apply (Hind _ (sdepth_l t1 t2) _ _ Ht1) | apply (Hind _ (sdepth_r t1 t2) _ _ Ht2)]).
  case_eq (DB_le d i); intros Hdi; [apply refl_equal | ].
   case_eq (DB_le d i'); intros Hdi'; rewrite Hdi' in Ht; [ | inversion Ht].
    reflect_in DB_eq_imp Ht; rewrite Ht in Hdi; rewrite Hdi in Hdi'; inversion Hdi'.
  destruct (orb_true_elim _ _ Ht) as [Ht1 | Ht2].
   apply (Hind _ (sdepth_l t1 t2) _ _ Ht1).
   rewrite <- (Hind _ (sdepth_r t1 t2) _ _ Ht2); DB_simpl; apply refl_equal.
  rewrite <- (Hind _ (sdepth t) _ _ Ht); DB_simpl; apply refl_equal.
Qed.

Theorem Bt_le_free_:forall (t:Τ)(i:Ι)(d:nat), DB_le d i=⊥->Bt_free_ d i t=⊥.
Proof.
 intros t i d Hdi; case_eq (Bt_free_ d i t); intros Hit; [ | apply refl_equal].
  rewrite (Bt_free_le_ _ _ _ Hit) in Hdi; inversion Hdi.
Qed.

Theorem Bt_free_free_:forall (t:Τ)(i:Ι)(d d':nat), d<=d'->Bt_free_ d i t=⊥->Bt_free_ d' i t=⊥.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d d':nat), d<=d'->Bt_free_ d i t=⊥->Bt_free_ d' i t=⊥));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i d d' Hdd' Hdt;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) _ _ _ Hdd' Hdt));
  try (destruct (orb_false_elim _ _ Hdt) as [Hdt1 Hdt2];
       srewrite (Hind _ (sdepth_l t1 t2) _ _ _ Hdd' Hdt1);
       apply (Hind _ (sdepth_r t1 t2) _ _ _ Hdd' Hdt2)).
  case_eq (DB_le d' i'); intros Hd'i';
  [rewrite (DB_le_trans _ _ _ Hdd' Hd'i') in Hdt; apply Hdt | apply refl_equal].
  destruct (orb_false_elim _ _ Hdt) as [Hdt1 Hdt2];
   srewrite (Hind _ (sdepth_l t1 t2) _ _ _ Hdd' Hdt1); simpl;
   case_eq (DB_le (S d') (DB_lift_ d' i)); intros Hd'i; [ | apply (Bt_le_free_ t2 _ _ Hd'i)].
    case_eq (DB_le (S d) (DB_lift_ d' i)); intros Hdi;
    [ | rewrite (DB_le_trans _ _ _ (le_n_S _ _ Hdd') Hd'i) in Hdi; inversion Hdi].
     replace (DB_lift_ d' i) with (DB_lift_ d i).
      apply (Hind _ (sdepth_r t1 t2) _ _ _ (le_n_S _ _ Hdd') Hdt2).
      destruct i as [[| u] n]; simpl in *; [ | apply refl_equal].
       case_eq (nat_le d' n); intros Hd'n; rewrite Hd'n in *; reflect_in nat_le_imp Hd'n.
        rewrite (imppt2 nat_le_imp _ _ (le_trans _ _ _ Hdd' Hd'n)); apply refl_equal.
        reflect_in nat_le_imp Hd'i; destruct (Hd'n (le_S_n _ _ (le_S _ _ Hd'i))).
  case_eq (DB_le (S d') (DB_lift_ d' i)); intros Hd'i; [ | apply (Bt_le_free_ t _ _ Hd'i)].
   case_eq (DB_le (S d) (DB_lift_ d' i)); intros Hdi;
   [ | rewrite (DB_le_trans _ _ _ (le_n_S _ _ Hdd') Hd'i) in Hdi; inversion Hdi].
    replace (DB_lift_ d' i) with (DB_lift_ d i).
     apply (Hind _ (sdepth t) _ _ _ (le_n_S _ _ Hdd') Hdt).
     destruct i as [[| u] n]; simpl in *; [ | apply refl_equal].
      case_eq (nat_le d' n); intros Hd'n; rewrite Hd'n in *; reflect_in nat_le_imp Hd'n.
       rewrite (imppt2 nat_le_imp _ _ (le_trans _ _ _ Hdd' Hd'n)); apply refl_equal.
       reflect_in nat_le_imp Hd'i; destruct (Hd'n (le_S_n _ _ (le_S _ _ Hd'i))).
Qed.

(* Fresh free variables --------------------------------------------------------------------------*)
(* For the sake of simplicity, fresh variables are chosen in fresh namespaces. *)

Fixpoint Bp_fshun(p:Π){struct p}:nat :=
 match p with
 | ¬p' => Bp_fshun p'
 | ∀Δ(p') => Bp_fshun p'
 | p1⋀p2 => max (Bp_fshun p1) (Bp_fshun p2)
 | (p1⇒p2) => max (Bp_fshun p1) (Bp_fshun p2)
 | ∐(_) => 0
 | e1≡e2 => max (Be_fshun e1) (Be_fshun e2)
 | e1∈e2 => max (Be_fshun e1) (Be_fshun e2)
 end
with Be_fshun(e:Ε){struct e}:nat :=
 match e with
 | Ω => 0
 | ↓(e') => Be_fshun e'
 | ↑(e') => Be_fshun e'
 | e1↦e2 => max (Be_fshun e1) (Be_fshun e2)
 | e1×e2 => max (Be_fshun e1) (Be_fshun e2)
 | ∞(_) => 0
 | ∂(¡(u',n')) => S u'
 | ⅭΔ(e',p') => max (Be_fshun e') (Bp_fshun p')
 end.

Definition Bt_fshun(t:Τ):nat := match t with be_ e' => Be_fshun e' | bp_ p' => Bp_fshun p' end.
Notation "'⋇(' t ')'":= (Bt_fshun t).
(* Provides a fresh universe. *)

Definition Bt_fresh(t:Τ):Ι := ¡(Bt_fshun t,0).
Notation "'⋕(' t ')'":= (Bt_fresh t).
(* Provides a fresh variable. *)

Theorem Bt_fshun_free:forall (t:Τ)(u:nat), ⋇(t)<=u->forall (n d:nat), Bt_free_ d ¡(u,n) t=⊥.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (u:nat), ⋇(t)<=u->forall (n d:nat), Bt_free_ d ¡(u,n) t=⊥));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind u Hut n d;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) _ Hut n d));
  try (srewrite (Hind _ (sdepth_l t1 t2) u (le_trans _ _ _ (le_max_l _ _) Hut) n d);
       apply (Hind _ (sdepth_r t1 t2) u (le_trans _ _ _ (le_max_r _ _) Hut) n d)).
  simpl in Hut; unfold Bt_free_,Be_free_; destruct (DB_le d ¡(u',n')); [ | apply refl_equal].
   simpl; case_eq (nat_eq u u'); intros Huu'; simpl; [ | apply refl_equal].
    reflect_in nat_eq_imp Huu'; rewrite Huu' in Hut; destruct (le_Sn_n _ Hut).
  simpl in Hut; srewrite (Hind _ (sdepth_l t1 t2) u (le_trans _ _ _ (le_max_l _ _) Hut) n d);
   apply (Hind _ (sdepth_r t1 t2) u (le_trans _ _ _ (le_max_r _ _) Hut)
               (if eq_0 u && nat_le d n then S n else n) (S d)).
  apply (Hind _ (sdepth t) u Hut (if eq_0 u && nat_le d n then S n else n) (S d)).
Qed.

Theorem Bt_fresh_free:forall (t:Τ), ⋕(t)∖t.
Proof.
 intros t; apply (Bt_fshun_free t _ (le_refl _) 0 0).
Qed.

Theorem fresh:forall (t:Τ), {i:Ι | i∖t}.
Proof.
 intros t; exists ⋕(t); apply Bt_fresh_free.
Qed.

(* Ground terms ----------------------------------------------------------------------------------*)

Fixpoint Bp_ground_(d:nat)(p:Π){struct p}:Β :=
 match p with
 | ¬p' => Bp_ground_ d p'
 | ∀Δ(p') => Bp_ground_ (S d) p'
 | p1⋀p2 => (Bp_ground_ d p1) && (Bp_ground_ d p2)
 | (p1⇒p2) => (Bp_ground_ d p1) && (Bp_ground_ d p2)
 | ∐(_) => ⊤
 | e1≡e2 => (Be_ground_ d e1) && (Be_ground_ d e2)
 | e1∈e2 => (Be_ground_ d e1) && (Be_ground_ d e2)
 end
with Be_ground_(d:nat)(e:Ε){struct e}:Β :=
 match e with
 | Ω => ⊤
 | ↓(e') => Be_ground_ d e'
 | ↑(e') => Be_ground_ d e'
 | e1↦e2 => (Be_ground_ d e1) && (Be_ground_ d e2)
 | e1×e2 => (Be_ground_ d e1) && (Be_ground_ d e2)
 | ∞(_) => ⊤
 | ∂(i') => negb (DB_le d i')
 | ⅭΔ(e',p') => (Be_ground_ d e') && (Bp_ground_ (S d) p')
 end.

Definition Bt_ground_(d:nat)(t:Τ):Β :=
 match t with be_ e' => Be_ground_ d e' | bp_ p' => Bp_ground_ d p' end.

Notation "'∖∖(' t ')'":=(Bt_ground_ 0 t=⊤) (at level 30).
(* Check whether or not a term has no free variable. *)

Theorem Bt_ground_free_:forall (t:Τ)(d:nat), Bt_ground_ d t=⊤->forall (i:Ι), Bt_free_ d i t=⊥.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d:nat), Bt_ground_ d t=⊤->forall (i:Ι), Bt_free_ d i t=⊥));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind d Hgr i;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) _ Hgr i));
  try (destruct (andb_prop _ _ Hgr) as [Hgr1 Hgr2]; srewrite (Hind _ (sdepth_l t1 t2) _ Hgr1 i);
       apply (Hind _ (sdepth_r t1 t2) _ Hgr2 i)).
  unfold Bt_ground_,Be_ground_ in Hgr; unfold Bt_free_,Be_free_; destruct (DB_le d i');
  [inversion Hgr | apply refl_equal].
  destruct (andb_prop _ _ Hgr) as [Hgr1 Hgr2]; srewrite (Hind _ (sdepth_l t1 t2) _ Hgr1 i);
   apply (Hind _ (sdepth_r t1 t2) _ Hgr2 (DB_lift_ d i)).
  apply (Hind _ (sdepth t) _ Hgr (DB_lift_ d i)).
Qed.

Theorem Bt_ground_free:forall (t:Τ), ∖∖(t)->forall (i:Ι), i∖t.
Proof.
 intros t Hgr i; apply (Bt_ground_free_ _ _ Hgr i).
Qed.

Theorem Bt_free_ground_:forall (t:Τ)(d:nat), (forall (i:Ι), Bt_free_ d i t=⊥)->Bt_ground_ d t=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d:nat), (forall (i:Ι), Bt_free_ d i t=⊥)->Bt_ground_ d t=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind d Hfr;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) _ Hfr));
  try (cut ((forall (i:Ι), Bt_free_ d i t1=⊥) /\ (forall (i:Ι), Bt_free_ d i t2=⊥));
       [intros [Hfr1 Hfr2]; srewrite (Hind _ (sdepth_l t1 t2) _ Hfr1);
        apply (Hind _ (sdepth_r t1 t2) _ Hfr2) |
        split; intros i; destruct (orb_false_elim _ _ (Hfr i)) as [H'fr1 H'fr2]; assumption]).
  unfold Bt_free_,Be_free_ in Hfr; unfold Bt_ground_,Be_ground_; destruct (DB_le d i');
  [generalize (Hfr i'); DB_simpl; intros Heq; inversion Heq | apply refl_equal].
  assert (Hfr1:forall (i:Ι), Bt_free_ d i t1=⊥).
   intros i; destruct (orb_false_elim _ _ (Hfr i)) as [H _]; apply H.
  assert (Hfr2:forall (i:Ι), Bt_free_ (S d) i t2=⊥).
   intros [[| u] n]; simpl; [ | destruct (orb_false_elim _ _ (Hfr ¡(S u,n))) as [_ H]; apply H].
    unfold Bt_free_,Be_free_ in Hfr;fold Be_free_ Bp_free_ in Hfr; case_eq (nat_le d n); intros Hdn;
    [ | destruct (orb_false_elim _ _ (Hfr ¡(0,n))) as [_ H]; simpl in H; rewrite Hdn in H; apply H].
     destruct n as [| n].
      reflect_in nat_le_imp Hdn; inversion Hdn; rewrite H in *; apply (Bt_le_free_ t2 ¡(0,0) 1);
       simpl; nat_simpl; apply refl_equal.
      case_eq (nat_le d n); intros H'dn.
       destruct (orb_false_elim _ _ (Hfr ¡(0,n))) as [_ H]; simpl in H; rewrite H'dn in H; apply H.
       reflect_in nat_le_imp Hdn; reflect_in nat_le_imp H'dn;
        destruct (le_lt_or_eq _ _ Hdn) as [H''dn | H''dn]; [destruct (H'dn (le_S_n _ _ H''dn)) | ].
        rewrite H''dn in *; apply (Bt_le_free_ t2 ¡(0,S n) (S (S n))); simpl; nat_simpl;
         apply refl_equal.
  srewrite (Hind _ (sdepth_l t1 t2) _ Hfr1); apply (Hind _ (sdepth_r t1 t2) _ Hfr2).
  assert (H'fr:forall (i:Ι), Bt_free_ (S d) i t=⊥).
   intros [[| u] n]; simpl; [ | apply (Hfr ¡(S u,n))].
    unfold Bt_free_,Be_free_ in Hfr;fold Be_free_ Bp_free_ in Hfr; case_eq (nat_le d n); intros Hdn;
    [ | generalize (Hfr ¡(0,n)); intros H; simpl in H; rewrite Hdn in H; apply H].
     destruct n as [| n].
      reflect_in nat_le_imp Hdn; inversion Hdn; apply (Bt_le_free_ t ¡(0,0) 1); simpl; nat_simpl;
       apply refl_equal.
      case_eq (nat_le d n); intros H'dn.
       generalize (Hfr ¡(0,n)); intros H; simpl in H; rewrite H'dn in H; apply H.
       reflect_in nat_le_imp Hdn; reflect_in nat_le_imp H'dn;
        destruct (le_lt_or_eq _ _ Hdn) as [H''dn | H''dn]; [destruct (H'dn (le_S_n _ _ H''dn)) | ].
        rewrite H''dn in *; apply (Bt_le_free_ t ¡(0,S n) (S (S n))); simpl; nat_simpl;
         apply refl_equal.
  apply (Hind _ (sdepth t) _ H'fr).
Qed.

(* Specific form characterisation ----------------------------------------------------------------*)

Inductive is_bfor:Τ->Type := is_bfor_:forall (p:Π), is_bfor ∀Δ(p).
Notation "'?∀Δ(' p ')'":= (is_bfor p).

Inductive is_bcmp:Τ->Type := is_bcmp_:forall (e:Ε)(p:Π), is_bcmp ⅭΔ(e,p).
Notation "'?ⅭΔ(' p ')'":= (is_bcmp p).

(* Decomposition functions -----------------------------------------------------------------------*)
(* It is frequent that we have to derive from f(t) the fact that f(t'), where t' is a subterm of t,
   e.g. in induction reasoning. We propose here a function to do that automatically, completed by
   by lemmas. *)

Definition Bp_l(p:Π):Τ :=
 match p with
 | ¬t' => t'
 | t'⋀_ => t'
 | (t'⇒_) => t'
 | t'≡_ => t'
 | t'∈_ => t'
 | _ => p (* Note that we DO NOT project bounded subterms as most lemmas are not valid for them *)
 end.

Definition Be_l(e:Ε):Τ :=
 match e with
 | ↓(t') => t'
 | ↑(t') => t'
 | t'↦_ => t'
 | t'×_ => t'
 | ⅭΔ(t',_) => t'
 | _ => e
 end.

Definition Bt_l(t:Τ):Τ := match t with be_ e' => Be_l e' | bp_ p' => Bp_l p' end.

Definition Bp_r(p:Π):Τ :=
 match p with
 | ¬t' => t'
 | _⋀t' => t'
 | (_⇒t') => t'
 | _≡t' => t'
 | _∈t' => t'
 | _ => p (* Note that we DO NOT project bounded subterms as most lemmas are not valid for them *)
 end.

Definition Be_r(e:Ε):Τ :=
 match e with
 | ↓(t') => t'
 | ↑(t') => t'
 | _↦t' => t'
 | _×t' => t'
 | _ => e (* Note that we DO NOT project bounded subterms as most lemmas are not valid for them *)
 end.

Definition Bt_r(t:Τ):Τ := match t with be_ e' => Be_r e' | bp_ p' => Bp_r p' end.

Theorem Bt_free_sub:forall (t:Τ)(i:Ι), (i∖t)->((i∖Bt_l t) /\ (i∖Bt_r t)).
Proof.
 intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
         [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]] i Ht; simpl in *; try (split; apply Ht);
 try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2]; apply (conj Ht1 Ht2)).
 destruct (orb_false_elim _ _ Ht) as [Ht1 _]; apply (conj Ht1 Ht).
Qed.

Theorem Bt_ground_sub:forall (t:Τ)(u:nat), ∖∖(t)->(∖∖(Bt_l t) /\ ∖∖(Bt_r t)).
Proof.
 intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
         [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]] u Ht; simpl in *;
 try (split; apply Ht); try (apply (andb_prop _ _ Ht)).
 split; [destruct (andb_prop _ _ Ht) as [Ht1 _]; apply Ht1 | apply Ht].
Qed.

(*================================================================================================*)