(*==================================================================================================
  Project : BiCoq3
  Module : bbpred.v
  Predicate calculus theorems from the B-Book
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Dec 2008, Md: Mar 2009
  ================================================================================================*)

Require Export egraft_congr.

(* A trivial result, using grafting, used here in the classical manner ---------------------------*)

Theorem Bpr_fornotnotintro:forall (G:Γ)(i:Ι)(p:Π), G⊢∀(i∙p)->G⊢∀(i∙¬¬p).
Proof.
 intros G i p Hp; destruct (gfresh (G·¬¬p)) as [i' Hi']; Btac_free;
  apply (Bpr_fori_diff _ _ i _ H H0); simpl; apply bbdr1; apply Bpr_fore; apply Hp.
Qed.

Theorem Bpr_fornotnotelim:forall (G:Γ)(i:Ι)(p:Π), G⊢∀(i∙¬¬p)->G⊢∀(i∙p).
Proof.
 intros G i p Hp; destruct (gfresh (G·p)) as [i' Hi']; Btac_free;
  apply (Bpr_fori_diff _ _ i _ H H0); simpl; apply Bpr_cutr with (1:=Bpr_fore _ _ ∂(i') _ Hp);
  apply Bpr_impe; simpl; Btac_prop.
Qed.

(* Theorems --------------------------------------------------------------------------------------*)

Theorem bbt_1_3_1:forall (i:Ι)(p q:Π), (i∖q)->⊢∀(i∙¬p⇒q)⇒(¬∀(i∙p)⇒q).
Proof.
 intros i p q Hi; Btac_prop; apply Bpr_absp with (p2:=∀(i∙p)); Btac_prop.
 apply Bpr_fori.
  apply Bpr_impe; apply Bpr_ctnp; apply Bpr_fore_id with (i:=i); Btac_hyp.
  Btac_free.
Qed.

Theorem bbt_1_3_2:forall (i:Ι)(e:Ε)(p:Π), ⊢〈i←e@Π@¬p〉⇒¬∀(i∙p).
Proof.
 intros i e p; apply Bpr_ctpn; simpl; Btac_prop; apply Bpr_fore; Btac_prop.
Qed.

Theorem bbdr9:forall (G:Γ)(i:Ι)(p q:Π), (i∖ˠG·q)->G⊢¬p⇒q->G⊢¬∀(i∙p)⇒q.
Proof.
 intros G i p q HiGq HGpq.
 assert (H':G⊢∀(i∙¬p⇒q)⇒(¬∀(i∙p)⇒q)). apply Bpr_nulg; apply bbt_1_3_1; Btac_free.
 apply Bpr_modp with (2:=H'); apply Bpr_fori; [apply HGpq | Btac_free].
Qed.

Theorem bbdr10:forall (G:Γ)(i:Ι)(e:Ε)(p:Π), G⊢〈i←e@Π@¬p〉->G⊢¬∀(i∙p).
Proof.
 intros G i e p HGiep; apply Bpr_modp with (1:=HGiep); apply Bpr_nulg; apply bbt_1_3_2.
Qed.

Theorem bbdr11:forall (G:Γ)(i:Ι)(e:Ε)(p q:Π), ∀(i∙p)∈ˠG->G⊢〈i←e@Π@p〉⇒q->G⊢q.
Proof.
 intros G i e p q Hin HGiepq; apply Bpr_modp with (2:=HGiepq); apply Bpr_fore; Btac_hyp.
Qed.

Theorem bbt_1_3_3:forall (i:Ι)(p q:Π), ⊢∀(i∙p⇒q)⇒(∀(i∙p)⇒∀(i∙q)).
Proof.
 intros i p q; Btac_prop; apply Bpr_fori.
  apply Bpr_modp with (p1:=p); apply Bpr_fore_id with (i:=i); Btac_hyp.
  Btac_free.
Qed.

Theorem bbdr12:forall (G:Γ)(i:Ι)(p q:Π), (i∖ˠG·q)->G⊢p⇒q->G⊢∃(i∙p)⇒q.
Proof.
 intros G i p q HiGq HGpq; apply bbdr9.
  apply HiGq.
  Btac_prop; apply Bpr_impe; apply HGpq.
Qed.

Theorem bbdr13:forall (G:Γ)(i:Ι)(e:Ε)(p:Π), G⊢〈i←e@Π@p〉->G⊢∃(i∙p).
Proof.
 intros G i e p HGiep; apply bbdr10 with (e:=e); simpl; Btac_prop; apply HGiep.
Qed.

Theorem bbdr14:forall (G:Γ)(i:Ι)(p q:Π), G⊢∀(i∙¬p)⇒q->G⊢¬∃(i∙p)⇒q.
Proof.
 intros G i p q HGipq; Btac_prop; apply Bpr_impe; apply HGipq.
Qed.

Theorem bbdr15:forall (G:Γ)(i:Ι)(p:Π), G⊢∀(i∙¬p)->G⊢¬∃(i∙p).
Proof.
 intros G i p HGip; Btac_prop; apply HGip.
Qed.

Theorem bbdr16:forall (G:Γ)(i:Ι)(p:Π), G⊢∃(i∙¬p)->G⊢¬∀(i∙p).
Proof.
 intros G i p HGip; apply Bpr_modp with (1:=HGip); apply Bpr_ctnn; apply Bpr_impi. 
 destruct (gfresh (G·p)) as [i' Hi']; apply Bpr_fori_diff with (i':=i');
  try (Btac_free; apply Hi'; Btac_gamma).
 simpl; Btac_prop; apply Bpr_fore; Btac_hyp.
Qed.

Theorem bbt_1_3_4:forall (i:Ι)(p q:Π), ⊢∃(i∙p⇒q)⇔(∀(i∙p)⇒∃(i∙q)).
Proof.
 intros i p q; unfold biff; apply Bpr_andi.
  Btac_prop; apply Bpr_swap; apply Bpr_impe; apply Bpr_ctnn; apply Bpr_impi; apply Bpr_fori;
  [ | Btac_free]; apply Bpr_absn with (p2:=q).
   apply Bpr_modp with (p1:=p); [apply Bpr_fore_id with (i:=i) | ]; Btac_hyp.
   apply Bpr_fore_id with (i:=i); Btac_hyp.
  Btac_prop.
   apply Bpr_modp with (p1:=¬∀(i∙¬¬p)).
    apply Bpr_impe; apply Bpr_ctnn; apply Bpr_modp with (p1:=∀(i∙¬¬p⇒p)).
     apply Bpr_fori; [Btac_prop | Btac_free].
     apply bbt_1_3_3.
    apply Bpr_ctnn; apply Bpr_modp with (p1:=∀(i∙¬(p⇒q)⇒¬¬p)).
     apply Bpr_fori; [Btac_prop | Btac_free].
     apply Bpr_nulg; apply bbt_1_3_3.
   apply Bpr_impe; apply Bpr_ctnn; apply Bpr_modp with (p1:=∀(i∙¬(p⇒q)⇒¬q)).
    apply Bpr_fori; [Btac_prop | Btac_free].
    apply bbt_1_3_3.
Qed.

(* Other classical results -----------------------------------------------------------------------*)

Theorem Bpr_forfor:forall (G:Γ)(i1 i2:Ι)(p:Π), G⊢∀(i1∙∀(i2∙p))->G⊢∀(i2∙∀(i1∙p)).
Proof.
 intros G i1 i2 p Hp; destruct (impdec2 DB_eq_imp i2 i1) as [Heq | Heq].
  rewrite Heq in *; apply Hp.
  destruct (gfresh (G·p·(∂(i1)≡∂(i2)))) as [i Hi]; Btac_free.
  assert (H'ip:i∖∀(i1∙p)). Btac_free.
  assert (Hi1i:i1∖∂(i)).
   simpl in *; DB_simpl; bool_simpl; reflect DB_eq_imp; apply sym_not_eq; reflect DB_eq_imp;
    apply H2.
  apply (Bpr_fori_diff _ _ i2 _ H H'ip); rewrite (Bp_forall_esubst_diff p _ _ _ Heq Hi1i).
  destruct (gfresh (G·〈i2←∂(i)@Π@p〉·(∂(i1)≡∂(i2)))) as [i' Hi']; Btac_free.
  assert (Hi2i':i2∖∂(i')).
   simpl in *; DB_simpl; bool_simpl; reflect DB_eq_imp; apply sym_not_eq; reflect DB_eq_imp;
    apply H7.
  apply (Bpr_fori_diff _ _ i1 _ H0 H5);
   irewrite (sym_equal (Bt_esubst_comm p _ _ _ _ Heq Hi2i' Hi1i)).
  apply Bpr_fore; rewrite (sym_equal (Bp_forall_esubst_diff p ∂(i') _ _ (sym_not_eq Heq) Hi2i'));
   apply Bpr_fore; apply Hp.
Qed.
(* Nota: Obviously less trivial than the proof of the B-Book, in which "magic" swap of variables
   in multiple quantifications is used. *)

Theorem Bpr_exsexs:forall (G:Γ)(i1 i2:Ι)(p:Π), G⊢∃(i1∙∃(i2∙p))->G⊢∃(i2∙∃(i1∙p)).
Proof.
 intros G i1 i2 p Hp; apply repl_not with (2:=Hp); apply Bpr_impi; apply Bpr_fornotnotintro;
  apply Bpr_forfor; apply Bpr_fornotnotelim; apply Bpr_axom.
Qed.

Theorem Bpr_forand:forall (G:Γ)(i:Ι)(p q:Π), G⊢∀(i∙p⋀q)->G⊢∀(i∙p)⋀∀(i∙q).
Proof.
 intros G i p q Hand; destruct (gfresh (G·p⋀q)) as [i' Hi']; Btac_free.
 Btac_prop; apply Bpr_fori_diff with (i':=i'); Btac_free.
  apply Bpr_andl with (p2:=〈i←∂(i')@Π@q〉);
   change (〈i←∂(i')@Π@p〉⋀〈i←∂(i')@Π@q〉) with (〈i←∂(i')@Π@p⋀q〉); apply Bpr_fore; apply Hand.
  apply Bpr_andr with (p1:=〈i←∂(i')@Π@p〉);
   change (〈i←∂(i')@Π@p〉⋀〈i←∂(i')@Π@q〉) with (〈i←∂(i')@Π@p⋀q〉); apply Bpr_fore; apply Hand.
Qed.

Theorem Bpr_andfor:forall (G:Γ)(i:Ι)(p q:Π), G⊢∀(i∙p)⋀∀(i∙q)->G⊢∀(i∙p⋀q).
Proof.
 intros G i p q Hand; generalize (Bpr_andl _ _ _ Hand); intros Handl;
  generalize (Bpr_andr _ _ _ Hand); intros Handr; clear Hand;
  destruct (gfresh (G·p⋀q)) as [i' Hi']; apply (Bpr_fori_diff G (p⋀q) i i'); Btac_free.
  simpl; apply Bpr_andi; apply Bpr_fore; [apply Handl | apply Handr].
Qed. 

Theorem Bpr_exslor:forall (G:Γ)(i:Ι)(p q:Π), G⊢∃(i∙p⋁q)->G⊢∃(i∙p)⋁∃(i∙q).
Admitted.

Theorem Bpr_lorexs:forall (G:Γ)(i:Ι)(p q:Π), G⊢∃(i∙p)⋁∃(i∙q)->G⊢∃(i∙p⋁q).
Admitted.

(* Theorems about equality -----------------------------------------------------------------------*)

Theorem bbt_1_4_1:forall (e f:Ε), ⊢e≡f⇒f≡e.
Proof.
 intros e f; apply Bpr_impi; destruct (fresh e) as [i Hi]; replace (f≡e) with (〈i←f@Π@∂(i)≡e〉).
  apply Bpr_leib with (e1:=e).
   Btac_hyp.
   simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free _ _ Hi e); DB_simpl; apply Bpr_refl.
  simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free _ _ Hi f); DB_simpl; apply refl_equal.
Qed.

Theorem bbt_1_4_2:forall (e f:Ε), ⊢e≡f⇔f≡e.
Proof.
 intros e f; unfold biff; apply Bpr_andi; apply bbt_1_4_1.
Qed.

Theorem bbt_1_4_3:forall (e f g:Ε), ⊢e≡f⋀f≡g⇒e≡g.
Proof.
 intros e f g; Btac_prop; destruct (fresh g) as [i Hi]; replace (e≡g) with (〈i←e@Π@∂(i)≡g〉).
  apply Bpr_leib with (e1:=f).
   apply Bpr_modp with (p1:=e≡f); [Btac_hyp | apply Bpr_clear; apply Bpr_clear; apply bbt_1_4_1].
   simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free _ _ Hi f); DB_simpl; Btac_hyp.
  simpl; nat_simpl; bool_simpl; irewrite (Bt_esubst_free _ _ Hi e); DB_simpl; apply refl_equal.
Qed.

Theorem bbt_1_4_4:forall (i:Ι)(p:Π)(e:Ε), (i∖e)->⊢∀(i∙∂(i)≡e⇒p)⇒〈i←e@Π@p〉.
Proof.
 intros i p e Hie; apply Bpr_impi; apply Bpr_cutr with (p1:=〈i←e@Π@∂(i)≡e⇒p〉).
  apply Bpr_fore; Btac_hyp.
  simpl; DB_simpl; irewrite (Bt_esubst_free _ _ Hie e); apply Bpr_impe; apply Bpr_clear; Btac_prop;
  apply Bpr_absp with (p2:=¬e≡e); [Btac_hyp | apply bbdr1; apply Bpr_refl].
Qed.

Theorem bbt_1_4_5:forall (i:Ι)(p:Π)(e:Ε), (i∖e)->⊢〈i←e@Π@p〉⇒∀(i∙∂(i)≡e⇒p).
Proof.
 intros i p e Hie; apply Bpr_impi; apply Bpr_fori.
  apply Bpr_impi; pattern p at 2; replace p with (〈i←∂(i)@Π@p〉).
   apply Bpr_leib with (e1:=e).
    apply Bpr_impe; apply Bpr_clear; apply bbt_1_4_1.
    Btac_hyp.
   irewrite (Bt_esubst_id p i); apply refl_equal.
  Btac_free; apply (Bt_free_esubst p _ _ Hie).
Qed.

Theorem bbt_1_4_6:forall (i:Ι)(p:Π)(e:Ε), (i∖e)->⊢∀(i∙∂(i)≡e⇒p)⇔〈i←e@Π@p〉.
Proof.
 intros i p e Hie; unfold biff; apply Bpr_andi;
 [apply (bbt_1_4_4 _ p _ Hie) | apply (bbt_1_4_5 _ p _ Hie)].
Qed.

Theorem bbt_1_4_7:forall (i:Ι)(p:Π)(e:Ε), (i∖e)->⊢∃(i∙∂(i)≡e⋀p)⇔〈i←e@Π@p〉.
Admitted.

Theorem bbeql1:forall (G:Γ)(i:Ι)(e f:Ε)(p q r:Π),
               (e≡f)∈ˠG->p∈ˠG->⊢p⇔〈i←e@Π@q〉->G⊢〈i←f@Π@q〉⇒r->G⊢r.
Proof.
 intros G i e f p q r Hef Hp Hpq Hqr; apply Bpr_modp with (2:=Hqr); apply Bpr_leib with (e1:=e).
  apply (Bpr_memb _ _ Hef).
  apply Bpr_modp with (p1:=p).
   apply (Bpr_memb _ _ Hp).
   apply Bpr_nulg; apply (Bpr_andl _ _ _ Hpq).
Qed.

Theorem bbeql2:forall (G:Γ)(i:Ι)(e f:Ε)(p q:Π), (e≡f)∈ˠG->⊢q⇔〈i←e@Π@p〉->G⊢〈i←f@Π@p〉->G⊢q.
Proof.
 intros G i e f p q Hef Hpq Hp; apply Bpr_modp with (p1:=〈i←e@Π@p〉).
  apply Bpr_leib with (e1:=f).
   apply Bpr_modp with (p1:=e≡f).
    apply (Bpr_memb _ _ Hef).
    apply Bpr_nulg; apply bbt_1_4_1.
   apply Hp.
  apply Bpr_nulg; apply (Bpr_andr _ _ _ Hpq).
Qed.

Theorem bbeql3:forall (G:Γ)(e f:Ε)(p:Π), (e≡f)∈ˠG->G·(f≡e)⊢p->G⊢p.
Proof.
 intros G e f p Hef Hp; apply Bpr_cutr with (2:=Hp); apply Bpr_modp with (p1:=e≡f).
  apply (Bpr_memb _ _ Hef).
  apply Bpr_nulg; apply bbt_1_4_1.
Qed.

(* Theorems about ordered pairs ------------------------------------------------------------------*)

Theorem bbt_1_5_5:forall (G:Γ)(e f e' f':Ε), G⊢(e↦f)≡(e'↦f')->G⊢e≡e'⋀f≡f'.
Proof.
 intros G e f e' f' Heq; apply Bpr_andi;
 [apply (Bpr_cpll _ _ _ _ _ Heq) | apply (Bpr_cplr _ _ _ _ _ Heq)].
Qed.

Theorem Bpr_prol:forall (G:Γ)(e f s t:Ε), G⊢(e↦f)∈(s×t)->G⊢e∈s.
Proof.
 intros G e f s t Hpro.
 generalize (Bg_fshun_free (G·(e↦f)∈(s×t)) _ (le_n_Sn _) 0); set (i1:=¡(S ⋇ˠ(G·e↦f∈s×t),0));
  intros Hi1; Btac_freehyp.
 generalize (Bg_fshun_free (G·(e↦f)∈(s×t)) _ (le_n_Sn _) 1); set (i2:=¡(S ⋇ˠ(G·e↦f∈s×t),1));
  intros Hi2; Btac_freehyp.
 assert (Hi1i2:i1<>i2). discriminate.
 apply Bpr_modp with (1:=Bpr_proe G (e↦f) s t i1 i2 Hi1i2 H0 H2 Hpro); apply bbdr12.
  Btac_free; split; Btac_free.
  Btac_prop; apply Bpr_impe; apply bbdr12.
   Btac_free; simpl; nat_simpl; apply refl_equal.
   Btac_prop; apply Bpr_cutr with (p1:=e≡∂(i1)).
    apply Bpr_andl with (p2:=f≡∂(i2)); apply bbt_1_5_5; Btac_hyp.
    replace (e∈s) with 〈i1←e@Π@∂(i1)∈s〉.
     apply Bpr_leib with (e1:=∂(i1)).
      apply Bpr_impe; apply Bpr_nulg; apply bbt_1_4_1.
      simpl; nat_simpl; simpl; irewrite (Bt_esubst_id s i1); Btac_hyp.
     simpl; nat_simpl; simpl; Btac_free; irewrite (Bt_esubst_free _ _ H0 e); apply refl_equal.
Qed.

Theorem Bpr_pror:forall (G:Γ)(e f s t:Ε), G⊢(e↦f)∈(s×t)->G⊢f∈t.
Admitted.

(*================================================================================================*)
