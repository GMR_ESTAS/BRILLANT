(*==================================================================================================
  Project : BiCoq3
  Module : psubst.v
  As we have defined an extended form of substitution for expressions, we describe here a function
  to substitute a propositional variable by a predicate. Note that as we have NOT defined a binder
  for propositional variables, propositional variables are all dangling and can be considered as
  named variables.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Aug 2008
  ================================================================================================*)

Require Export lift.

(* Substitution function -------------------------------------------------------------------------*)

Fixpoint Bp_psubst(n:nat)(p a:Π){struct p}:Π:=
 match p with
 | ¬p' => ¬(Bp_psubst n p' a)
 | ∀Δ(p') => ∀Δ(Bp_psubst n p' ⇑Π(a))
 | p1⋀p2 => (Bp_psubst n p1 a)⋀(Bp_psubst n p2 a)
 | (p1⇒p2) => (Bp_psubst n p1 a)⇒(Bp_psubst n p2 a)
 | ∐(n') => if nat_eq n n' then a else p
 | e1≡e2 => (Be_psubst n e1 a)≡(Be_psubst n e2 a)
 | e1∈e2 => (Be_psubst n e1 a)∈(Be_psubst n e2 a)
 end
with Be_psubst(n:nat)(e:Ε)(a:Π){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_psubst n e' a)
 | ↑(e') => ↑(Be_psubst n e' a)
 | e1↦e2 => (Be_psubst n e1 a)↦(Be_psubst n e2 a)
 | e1×e2 => (Be_psubst n e1 a)×(Be_psubst n e2 a)
 | ∞(_) => e
 | ∂(_) => e
 | ⅭΔ(e',p') => ⅭΔ(Be_psubst n e' a,Bp_psubst n p' ⇑Π(a))
 end.
Notation "'⁅' n '←' a '@Π@' p '⁆'" := (Bp_psubst n p a).
Notation "'⁅' n '←' a '@Ε@' e '⁆'" := (Be_psubst n e a).

Definition Bt_psubst(n:nat)(t:Τ)(a:Π):Τ :=
 match t with be_ e' => Be_psubst n e' a | bp_ p' => Bp_psubst n p' a end.
Notation "'⁅' n '←' a '@' t '⁆'" := (Bt_psubst n t a).

(*================================================================================================*)