(*==================================================================================================
  Project : BiCoq3
  Module : lemma.v
  This module is about some trivial results about B propositional calculus.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jul 2008, Md: Mar 2009
  ================================================================================================*)

Require Export proof.
Require Export cmp.

(* Derived rules ---------------------------------------------------------------------------------*)

Theorem Bpr_axom:forall (G:Γ)(p:Π), G·p⊢p.
Proof.
 intros G p; apply (Bpr_memb (G·p) p); Btac_gamma.
Qed.

Theorem Bpr_cutr:forall (G:Γ)(p1 p2:Π), G⊢p1->G·p1⊢p2->G⊢p2.
Proof.
 intros G p1 p2 Hp1 Hp2; apply Bpr_absp with (p2:=p1).
  apply Bpr_weak with (G1:=G); [apply Hp1 | Btac_gamma].
  apply Bpr_absn with (p2:=p2);
  [apply Bpr_weak with (G1:=G·p1); [apply Hp2 | Btac_gamma] | apply Bpr_memb; Btac_gamma].
Qed.

Theorem Bpr_modp:forall (G:Γ)(p1 p2:Π), G⊢p1->G⊢p1⇒p2->G⊢p2.
Proof.
 intros G p1 p2 Hp1 Himp; apply Bpr_cutr with (p1:=p1); [apply Hp1 | apply Bpr_impe; apply Himp].
Qed.

(* Elementary lemmas -----------------------------------------------------------------------------*)

Theorem Bpr_emid:forall (G:Γ)(p:Π), G⊢p⋁¬p.
Proof.
 intros G p; unfold blor; apply Bpr_impi; apply Bpr_axom.
Qed.

Theorem Bpr_clear:forall (G:Γ)(p1 p2:Π), G⊢p1->G·p2⊢p1.
Proof.
 intros G p1 p2 Hp1; apply Bpr_weak with (G1:=G); [apply Hp1 | apply Bg_inc_add].
Qed.

Theorem Bpr_swap:forall (G:Γ)(p1 p2 p3:Π), G·p1·p2⊢p3->G·p2·p1⊢p3.
Proof.
 intros G p1 p2 p3 Hp3; apply Bpr_weak with (G1:=G·p1·p2); [apply Hp3 | Btac_gamma].
Qed.

Theorem Bpr_fngh:forall (G:Γ)(p1 p2:Π), G⊢p1->G·¬p1⊢p2.
Proof.
 intros G p1 p2 Hp1; apply Bpr_absp with (p2:=p1); apply Bpr_clear;
  [apply Bpr_clear; apply Hp1 | apply Bpr_axom].
Qed.

Theorem Bpr_fpsh:forall (G:Γ)(p1 p2:Π), G⊢¬p1->G·p1⊢p2.
Proof.
 intros G p1 p2 Hp1; apply Bpr_absp with (p2:=p1); apply Bpr_clear;
  [apply Bpr_axom | apply Bpr_clear; apply Hp1].
Qed.

Theorem Bpr_lorl:forall (G:Γ)(p1 p2:Π), G⊢p1->G⊢p1⋁p2.
Proof.
 intros G p1 p2 Hp1; unfold blor; apply Bpr_impi; apply Bpr_fngh; apply Hp1.
Qed.

Theorem Bpr_lorr:forall (G:Γ)(p1 p2:Π), G⊢p2->G⊢p1⋁p2.
Proof.
 intros G p1 p2 Hp1; unfold blor; apply Bpr_impi; apply Bpr_clear; apply Hp1.
Qed.
(* Nota: There is no Bpr_lore:G⊢p1⋁p2->(G⊢p1 \/ G⊢p2), see module swap for details. *)

Theorem Bpr_not2i:forall (G:Γ)(p:Π), G⊢p->G⊢¬¬p.
Proof.
 intros G p Hp; apply Bpr_absn with (p2:=p); [apply Bpr_clear; apply Hp | apply Bpr_axom].
Qed.

Theorem Bpr_not2e:forall (G:Γ)(p:Π), G⊢¬¬p->G⊢p.
Proof.
 intros G p Hp; apply Bpr_absp with (p2:=¬p); [apply Bpr_axom | apply Bpr_clear; apply Hp].
Qed.

Theorem Bpr_ctnn:forall (G:Γ)(p1 p2:Π), G⊢p1⇒p2->G⊢¬p2⇒¬p1.
Proof.
 intros G p1 p2 Himp; apply Bpr_impi; apply Bpr_absn with (p2:=p2).
  apply Bpr_impe; apply Bpr_clear; apply Himp.
  apply Bpr_clear; apply Bpr_axom.
Qed.

Theorem Bpr_ctnp:forall (G:Γ)(p1 p2:Π), G⊢¬p1⇒p2->G⊢¬p2⇒p1.
Proof.
 intros G p1 p2 Himp; apply Bpr_impi; apply Bpr_absp with (p2:=p2).
  apply Bpr_impe; apply Bpr_clear; apply Himp.
  apply Bpr_clear; apply Bpr_axom.
Qed.

Theorem Bpr_ctpn:forall (G:Γ)(p1 p2:Π), G⊢p1⇒¬p2->G⊢p2⇒¬p1.
Proof.
 intros G p1 p2 Himp; apply Bpr_impi; apply Bpr_absn with (p2:=p2).
  apply Bpr_clear; apply Bpr_axom.
  apply Bpr_impe; apply Bpr_clear; apply Himp.
Qed.

Theorem Bpr_ctpp:forall (G:Γ)(p1 p2:Π), G⊢¬p1⇒¬p2->G⊢p2⇒p1.
Proof.
 intros G p1 p2 Himp; apply Bpr_impi; apply Bpr_absp with (p2:=p2).
  apply Bpr_clear; apply Bpr_axom.
  apply Bpr_impe; apply Bpr_clear; apply Himp.
Qed.

Theorem Bpr_ands:forall (G:Γ)(p1 p2 p3:Π), G·p1·p2⊢p3->G·p1⋀p2⊢p3.
Proof.
 intros G p1 p2 p3 Hsp; apply Bpr_cutr with (p1:=p1).
  apply Bpr_andl with (p2:=p2); apply Bpr_axom.
  apply Bpr_cutr with (p1:=p2).
   apply Bpr_andr with (p1:=p1); apply Bpr_memb; Btac_gamma.
   apply Bpr_weak with (1:=Hsp); Btac_gamma.
Qed.

Theorem Bpr_nulg:forall (G:Γ)(p:Π), ⊢p->G⊢p.
Proof.
 intros G p Hp; apply Bpr_weak with (G1:=∅ˠ); [apply Hp | Btac_gamma].
Qed.

Theorem Bpr_exsi:forall (G:Γ)(p:Π)(i:Ι)(e:Ε), G⊢〈i←e@Π@p〉->G⊢∃(i∙p).
Proof.
 intros G p i e He.
 apply Bpr_absn with (p2:=〈i←e@Π@p〉).
  apply Bpr_clear; apply He.
  apply (Bpr_fore (G·∀(i∙¬p)) (¬p) e i); apply Bpr_axom.
Qed.

Theorem Bpr_fore_id:forall (G:Γ)(p:Π)(i:Ι), G⊢∀(i∙p)->G⊢p.
Proof.
 intros G p i Hfor; replace p with 〈i←∂(i)@Π@p〉;
  [apply Bpr_fore; apply Hfor | irewrite (Bt_esubst_id p i); apply refl_equal].
Qed.

Theorem Bpr_fori_diff:forall (G:Γ)(p:Π)(i i':Ι), (i'∖ˠG)->(i'∖p)->G⊢〈i←∂(i')@Π@p〉->G⊢∀(i∙p).
Proof.
 intros G p i i' Hi'G Hi'p Hinst; rewrite (Bp_forall_alpha p i i' Hi'p);
  apply (Bpr_fori _ _ _ Hinst Hi'G).
Qed.

Theorem Bpr_sbsi:forall (G:Γ)(p:Π)(e:Ε)(i:Ι), G⊢p->(i∖ˠG)->G⊢〈i←e@Π@p〉.
Proof.
 intros G p e i HGp HiG; apply Bpr_fore; apply (Bpr_fori _ _ _ HGp HiG).
Qed.

Theorem Bpr_equ_sym:forall (G:Γ)(e1 e2:Ε), G⊢e1≡e2->G⊢e2≡e1.
Proof.
 intros G e1 e2 Heq; destruct (fresh (e1≡e2)) as [i Hi]; replace (e2≡e1) with 〈i←e2@Π@∂(i)≡e1〉.
  apply (Bpr_leib _ (∂(i)≡e1) _ _ i Heq); simpl; nat_simpl; simpl;
   irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e1); DB_simpl; apply Bpr_refl.
  simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ (proj1 (Bt_free_sub _ _ Hi)) e2);
   DB_simpl; apply refl_equal.
Qed.

Theorem Bpr_equ_trans:forall (G:Γ)(e1 e2 e3:Ε), G⊢e1≡e2->G⊢e2≡e3->G⊢e1≡e3.
Proof.
 intros G e1 e2 e3 He1e2 He2e3; destruct (fresh e3) as [i Hie3];
  replace (e1≡e3) with 〈i←e1@Π@∂(i)≡e3〉.
  apply (Bpr_leib _ (∂(i)≡e3) _ _ i (Bpr_equ_sym _ _ _ He1e2)); simpl; nat_simpl; simpl;
   irewrite (Bt_esubst_free _ _ Hie3 e2); DB_simpl; apply He2e3.
  simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ Hie3 e1); DB_simpl; apply refl_equal.
Qed.

(* Associated tactics ----------------------------------------------------------------------------*)

Ltac Btac_hyp := (apply Bpr_memb; Btac_gamma).

(*================================================================================================*)