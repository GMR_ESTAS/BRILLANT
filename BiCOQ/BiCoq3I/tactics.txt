﻿****** basic_logic.v ******
reflect H
- Conversion entre bool et Prop dans le but en utilisant la preuve d'implémentation H
reflect_in H H'
- Conversion entre bool et Prop dans l'hypothèse H en utilisant la preuve d'implémentation H
bool_simpl
- Simplification dans le but et les hypothèses des expressions booléennes
****** basic_nat.v ******
nat_simpl
- Simplification dans le but et les hypothèses des expressions sur les naturels
****** index.v ******
DB_simpl
- Simplification dans le but et les hypothèses des expressions sur les index de De Bruijn
****** term.v ******
srewrite E
- Réécrit la simplification de l'égalité E dans le but
srewrite_in E H
- Réécrit la simplification de l'égalité E dans l'hypothèse H
irewrite E
- Réécrit l'injection de l'égalité E dans le but
irewrite_in E H
- Réécrit l'injection de l'égalité E injectée dans l'hypothèse H
****** lift.v ******
****** proof_env.v ******
Btac_gamma
- Résoud les buts d'appartenance ou d'inclusion pour les environnements de preuve B
****** abstr.v ******
****** apply.v ******
****** esubst.v ******
****** psubst.v ******
****** egraft.v ******
****** cmp.v ******
****** mesubst.v ******
****** proof.v ******
****** sem_ind.v ******
****** wf_proof.v ******
****** lemma.v ******
Btac_hyp
- Termine une preuve B lorsque le but est une hypothèse
****** bbprop.v ******
Btac_prop
- Termine une preuve B pour le calcul propositionnel
****** swap.v ******
****** raw_proof.v ******
****** congr.v ******
Btac_free
- Résoud la plus grande partie des buts relatifs à la non-freeness****** mesubst_congr.v ******
****** egraft_congr.v ******
****** bbpred.v ******