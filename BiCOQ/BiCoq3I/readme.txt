(*==================================================================================================
  Project : BiCoq3I
  Developed for Coq 8.2, using UTF-8 and the Everson Mono Unicode font
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/ANSSI & UPMC/LIP6 - St: Jun 2008, Md: Mar 2009 (approximately)
  Provided under licence CeCILL-B
  ================================================================================================*)

This is the source code of BiCoq3, a deep embedding of B in Coq. The final I indicates that it is
based in de Bruijn indexes.

This version replaces BiCoq2. It introduces various improvements. On the other hand, it is not yet
as complete, and for example does not include the proven prover.

The file make.txt describes the dependencies between the files.

The file notations.txt provides all the UTF-8 notations introduced in the development, with their
associated definition.

The file tactics.txt identifies all the Coq tactics.