(*==================================================================================================
  Project : BiCoq3
  Module : bbset.v
  Set theory theorems from the B-Book
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Feb 2008, Md: Mar 2009
  ================================================================================================*)

Require Export bbpred.

(* Lemmas and tactics for sets -------------------------------------------------------------------*)

Theorem Bpr_incins:forall (G:Γ)(s t:Ε), G⊢s⊆t->(forall (e:Ε), G⊢e∈s->G⊢e∈t).
Proof.
 intros G s t Hinc e Hmem; unfold binc in Hinc; destruct (fresh (s⊆t)) as [i Hi]; Btac_free;
  apply Bpr_modp with (1:=Hmem).
 apply Bpr_cutr with (1:=Bpr_fore _ _ e _ (Bpr_powe _ _ _ _ H H0 Hinc)).
 simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ H e); irewrite (Bt_esubst_free _ _ H0 e);
  DB_simpl; simpl; Btac_hyp.
Qed.

Theorem Bpr_insinc:forall (G:Γ)(s t:Ε), (forall (e:Ε), G⊢e∈s⇒e∈t)->G⊢s⊆t.
Proof.
 intros G s t Hmem; unfold binc; destruct (fresh (s⊆t)) as [i Hi]; Btac_free;
  apply (Bpr_powi G _ _ _ H H0).
 apply (cb_for' G (∂(i)∈s⇒∂(i)∈t) i); intros e; simpl; nat_simpl; simpl;
  irewrite (Bt_esubst_free _ _ H e); irewrite (Bt_esubst_free _ _ H0 e); apply Hmem.
Qed.
(* Nota: As shown in the module swap, ⇒ can be used to prove ->, but not the contrary. That explains
   the differences between Bpr_incins and Bpr_insinc. *)

Theorem cb_pro:forall (G:Γ)(s t:Ε)(e:Ε),
               sigS (fun e1:Ε=>prod (G⊢e1∈s) (sigS (fun e2:Ε=>prod (G⊢e2∈t) (G⊢e1↦e2≡e))))->G⊢e∈s×t.
Proof.
 intros G s t e [e1 [He1 [e2 [He2 Heq]]]].
 generalize (Bg_fshun_free (G·e∈s×t·e1≡e2) _ (le_n_Sn _) 0); set (i1:=¡(S ⋇ˠ(G·e∈s×t·e1≡e2),0));
  intros Hi1.
 generalize (Bg_fshun_free (G·e∈s×t·e1≡e2) _ (le_n_Sn _) 1); set (i2:=¡(S ⋇ˠ(G·e∈s×t·e1≡e2),1));
  intros Hi2.
 Btac_freehyp; Btac_freehyp; Btac_freehyp; Btac_freehyp.
 assert (Hi1i2:i1<>i2). discriminate.
 apply (Bpr_proi G _ _ _ _ _ Hi1i2 H4 H1).
 apply cb_exs; exists e1.
 replace 〈i1←e1@Π@∂(i1)∈s⋀∃(i2∙∂(i2)∈t⋀e≡∂(i1)↦∂(i2))〉 with
         (e1∈s⋀¬〈i1←e1@Π@∀(i2∙¬(∂(i2)∈t⋀e≡∂(i1)↦∂(i2)))〉).
  apply Bpr_andi; [apply He1 | ].
  assert (Hi2e1:i2∖e1). Btac_free.
  rewrite (Bp_forall_esubst_diff (¬(∂(i2)∈t⋀e≡∂(i1)↦∂(i2))) _ _ _ Hi1i2 Hi2e1).
  change 〈i1←e1@Π@¬(∂(i2)∈t⋀e≡∂(i1)↦∂(i2))〉 with (¬〈i1←e1@Π@∂(i2)∈t⋀e≡∂(i1)↦∂(i2)〉).
  apply cb_exs; exists e2.
  simpl; nat_simpl; simpl; nat_simpl; simpl; Btac_free; irewrite (Bt_esubst_free _ _ H7 e1);
   irewrite (Bt_esubst_free _ _ H10 e2); irewrite (Bt_esubst_free _ _ H5 e1);
   irewrite (Bt_esubst_free _ _ H3 e2); irewrite (Bt_esubst_free _ _ H9 e2).
  apply Bpr_andi.
   apply He2.
   apply Bpr_modp with (1:=Heq); apply Bpr_nulg; apply bbt_1_4_1.
 unfold Bp_esubst_ at 2; fold Bp_esubst_ Be_esubst_; DB_simpl; Btac_free;
  irewrite (Bt_esubst_free _ _ H4 e1); apply refl_equal.
Qed.

(* Properties ------------------------------------------------------------------------------------*)

Theorem Bpr_inc_refl:forall (G:Γ)(s:Ε), G⊢s⊆s.
Proof.
 intros G s; apply Bpr_insinc; intros e; Btac_prop.
Qed.

Theorem Bpr_inc_asym:forall (G:Γ)(s t:Ε), G⊢s⊆t->G⊢t⊆s->G⊢s≡t.
Proof.
 intros G s t Hst Hts; apply Bpr_extn; [apply Hst | apply Hts].
Qed.

Theorem Bpr_inc_trans:forall (G:Γ)(s t u:Ε), G⊢s⊆t->G⊢t⊆u->G⊢s⊆u.
Proof.
 intros G s t u Hst Htu; apply Bpr_insinc; intros e; apply Bpr_impi.
 apply (Bpr_incins _ _ _ (Bpr_clear _ _ (e∈s) Htu) e);
  apply (Bpr_incins _ _ _ (Bpr_clear _ _ (e∈s) Hst) e); Btac_hyp.
Qed.

Theorem Bpr_inc_pro:forall (G:Γ)(s t u v:Ε), G⊢s⊆t->G⊢u⊆v->G⊢s×u⊆t×v.
Proof.
 intros G s t u v Hst Huv; apply Bpr_insinc; intros e; apply Bpr_impi.
 generalize (Bg_fshun_free (G·e∈t×v·e∈s×u) _ (le_n_Sn _) 0); set (i1:=¡(S ⋇ˠ(G·e∈t×v·e∈s×u),0));
  intros Hi1.
 generalize (Bg_fshun_free (G·e∈t×v·e∈s×u) _ (le_n_Sn _) 1); set (i2:=¡(S ⋇ˠ(G·e∈t×v·e∈s×u),1));
  intros Hi2.
 Btac_freehyp; Btac_freehyp; Btac_freehyp; Btac_freehyp; assert (Hi1i2:i1<>i2). discriminate.
 apply (Bpr_proi (G·e∈s×u) _ _ _ _ _ Hi1i2 H4 H1).
 apply Bpr_modp with (1:=Bpr_proe (G·e∈s×u) _ _ _ _ _ Hi1i2 H3 H0 (Bpr_axom G (e∈s×u)));
  apply Bpr_clear; apply Bpr_ctnn.
 apply Bpr_modp with (2:=Bpr_nulg G _ (bbt_1_3_3 i1 (¬(∂(i1)∈t⋀∃(i2∙∂(i2)∈v⋀e≡∂(i1)↦∂(i2))))
                                                    (¬(∂(i1)∈s⋀∃(i2∙∂(i2)∈u⋀e≡∂(i1)↦∂(i2)))))).
 apply Bpr_fori; [ | Btac_free].
  apply Bpr_ctnn; apply Bpr_impi; apply Bpr_andi; apply Bpr_impe; Btac_prop.
   apply Bpr_clear; apply (Bpr_incins _ _ _ (Bpr_clear _ _ (∂(i1)∈s) Hst)); Btac_hyp.
   apply Bpr_impe; apply Bpr_clear; apply Bpr_ctnn;
    apply Bpr_modp with (2:=Bpr_nulg G _ (bbt_1_3_3 i2 (¬(∂(i2)∈v⋀e≡∂(i1)↦∂(i2)))
                                                       (¬(∂(i2)∈u⋀e≡∂(i1)↦∂(i2)))));
    apply Bpr_fori; [ | Btac_free].
     apply Bpr_ctnn; Btac_prop; apply Bpr_clear;
      apply (Bpr_incins _ _ _ (Bpr_clear _ _ (∂(i2)∈u) Huv)); Btac_hyp.
Qed.

Theorem bbt_2_1_1:forall (G:Γ)(s t:Ε), G⊢s⊆t->G⊢↑(s)⊆↑(t).
Proof.
 intros G s t Hst; apply Bpr_insinc; intros e; fold (binc e s); fold (binc e t); apply Bpr_impi;
  apply Bpr_inc_trans with (t:=s); [Btac_hyp | apply Bpr_clear; apply Hst].
Qed.

(* Derived constructs ----------------------------------------------------------------------------*)

Definition Bsubset(a b:Ε) := a⊆b⋀a≢b.
Notation "a '⊂' b" := (Bsubset a b) (at level 70, no associativity).

Definition Bunion(s a b:Ε) := let i:=⋕(a×b) in {i∊s∣∂(i)∈a⋁∂(i)∈b}.
Notation "'∪(' s ',' a ',' b ')'" := (Bunion s a b).
(* Nota: while this is not the case in the B-Book, we forbid here to define union using a bound
   variable that appears free in either a or b. *)

Definition Binter(s a b:Ε) := let i:=⋕(a×b) in {i∊s∣∂(i)∈a⋀∂(i)∈b}.
Notation "'∩(' s ',' a ',' b ')'" := (Binter s a b).
(* Nota: it is also possible to define ∩(a,b) as {i∊a∣∂(i)∈b}. *)

Definition Bssub(s a b:Ε) := let i:=⋕(a×b) in {i∊s∣∂(i)∈a⋀∂(i)∉b}.
Notation "'−(' s ',' a ',' b ')'" := (Bssub s a b).
(* Nota: it is also possible to define −(a,b) as {i∊a∣∂(i)∉b}. *)

Definition Bempty := {¡(0,0)∊Ω∣∂(¡(0,0))∉Ω}.
Notation "'∅'" := (Bempty).

Definition Brel(s t:Ε) := ↑(s×t).
Notation "'(' s '↔' t ')'" := (Brel s t).

Definition Blambda(i:Ι)(s t e:Ε) := let ij:=⋕(∂(i)↦e) in {ij∊(s×t)∣∃(i∙∂(ij)≡∂(i)↦e)}.
Notation "'∫(' i '∊' s '⇢' e '∊' t ')'" := (Blambda i s t e).

(* Properties ------------------------------------------------------------------------------------*)

Theorem Bpr_union_comm:forall (G:Γ)(s t u:Ε), G⊢∪(s,t,u)≡∪(s,u,t).
Proof.
 intros G s t u; apply Bpr_extn; [fold (binc ∪(s,t,u) ∪(s,u,t)) | fold (binc ∪(s,u,t) ∪(s,t,u))];
  apply Bpr_insinc; intros e; apply Bpr_impi; unfold Bunion; set (i:=⋕(t×u)); set (i':=⋕(u×t));
  apply Bpr_cmpi.
  apply Bpr_cmpl with (i:=i)(p:=∂(i)∈t⋁∂(i)∈u); Btac_hyp.
  apply Bpr_cutr with (p1:=〈i←e@Π@∂(i)∈t⋁∂(i)∈u〉).
   apply Bpr_cmpr with (e2:=s); Btac_hyp.
   assert (Hi:i∖t×u). unfold i; apply Bt_fresh_free; apply le_refl.
   assert (Hi':i'∖u×t). unfold i'; apply Bt_fresh_free; apply le_refl.
   Btac_free; apply Bpr_impe; apply Bpr_clear; simpl; nat_simpl; simpl;
    irewrite (Bt_esubst_free _ _ H1 e); irewrite (Bt_esubst_free _ _ H2 e);
    irewrite (Bt_esubst_free _ _ H e); irewrite (Bt_esubst_free _ _ H0 e); bool_simpl; Btac_prop.
  apply Bpr_cmpl with (i:=i')(p:=∂(i')∈u⋁∂(i')∈t); Btac_hyp.
  apply Bpr_cutr with (p1:=〈i'←e@Π@∂(i')∈u⋁∂(i')∈t〉).
   apply Bpr_cmpr with (e2:=s); Btac_hyp.
   assert (Hi:i∖t×u). unfold i; apply Bt_fresh_free; apply le_refl.
   assert (Hi':i'∖u×t). unfold i'; apply Bt_fresh_free; apply le_refl.
   Btac_free; apply Bpr_impe; apply Bpr_clear; simpl; nat_simpl; simpl;
    irewrite (Bt_esubst_free _ _ H1 e); irewrite (Bt_esubst_free _ _ H2 e);
    irewrite (Bt_esubst_free _ _ H e); irewrite (Bt_esubst_free _ _ H0 e); bool_simpl; Btac_prop.
Qed.

Theorem Bpr_inter_comm:forall (G:Γ)(s t u:Ε), G⊢∩(s,t,u)≡∩(s,u,t).
Admitted.

Theorem Bpr_union_idem:forall (G:Γ)(s:Ε), G⊢∪(s,s,s)≡s.
Admitted.

Theorem Bpr_inter_idem:forall (G:Γ)(s:Ε), G⊢∩(s,s,s)≡s.
Admitted.

Theorem Blambda_fun:forall (G:Γ)(s t e x y y':Ε)(i:Ι),
                    (i∖ˠG)->(i∖x↦y)->(i∖x↦y')->G⊢x↦y∈∫(i∊s⇢e∊t)->G⊢x↦y'∈∫(i∊s⇢e∊t)->G⊢y≡y'.
Proof.
 intros G s t e x y y' i HiG Hi H'i Hxy Hxy'.
 generalize (Bpr_cmpl _ _ _ _ _ Hxy); intros Hxy1.
 generalize (Bpr_cmpr _ _ _ _ _ Hxy); set (j:=⋕(∂(i)↦e)).
 assert (Hj:j∖∂(i)↦e). unfold j; apply Bt_fresh_free; apply le_refl.
 assert (Hji:j<>i).
  Btac_free; case_eq (DB_eq j i); intros Hji; reflect_in DB_eq_imp Hji.
   rewrite <- Hji in H; simpl in H; nat_simpl; bool_simpl; inversion H.
   unfold not; apply Hji.
 replace 〈j←x↦y@Π@¬∀(i∙¬∂(j)≡∂(i)↦e)〉 with (¬∀(i∙〈j←x↦y@Π@¬∂(j)≡∂(i)↦e〉));
 [ | srewrite (Bp_forall_esubst_diff (¬∂(j)≡∂(i)↦e) _ _ _ Hji Hi); apply refl_equal].
 replace 〈j←x↦y@Π@¬∂(j)≡∂(i)↦e〉 with (¬(x↦y≡∂(i)↦e));
 [ | unfold Bp_esubst_; fold Bp_esubst_ Be_esubst_; DB_simpl; rewrite (imprf2 DB_eq_imp _ _ Hji);
     Btac_free; irewrite (Bt_esubst_free _ _ H0 (x↦y)); apply refl_equal].
 intros Hxy2.
 generalize (Bpr_cmpl _ _ _ _ _ Hxy'); intros Hxy'1.
 generalize (Bpr_cmpr _ _ _ _ _ Hxy'); set (j':=⋕(∂(i)↦e)).
 assert (Hj':j'∖∂(i)↦e). unfold j'; apply Bt_fresh_free; apply le_refl.
 assert (Hj'i:j'<>i). Btac_free.
 replace 〈j'←x↦y'@Π@¬∀(i∙¬∂(j')≡∂(i)↦e)〉 with (¬∀(i∙〈j'←x↦y'@Π@¬∂(j')≡∂(i)↦e〉));
 [ | srewrite (Bp_forall_esubst_diff (¬∂(j')≡∂(i)↦e) _ _ _ Hj'i H'i); apply refl_equal].
 replace 〈j'←x↦y'@Π@¬∂(j')≡∂(i)↦e〉 with (¬(x↦y'≡∂(i)↦e));
 [ | unfold Bp_esubst_; fold Bp_esubst_ Be_esubst_; DB_simpl; rewrite (imprf2 DB_eq_imp _ _ Hj'i);
     Btac_free; irewrite (Bt_esubst_free _ _ H0 (x↦y')); apply refl_equal].
 intros Hxy'2.
 clear Hxy Hxy' j Hj Hji j' Hj' Hj'i; apply Bpr_modp with (1:=Hxy2); apply Bpr_modp with (1:=Hxy'2).
 apply Bpr_impi; apply Bpr_ctnp; apply Bpr_impi.

 generalize (Bg_fshun_free (G·¬∀(i∙¬x↦y'≡∂(i)↦e)·¬y≡y'·¬x↦y≡∂(i)↦e) _ (le_n_Sn _) 0);
  set (i1:=¡(S ⋇ˠ(G·¬∀(i∙¬x↦y'≡∂(i)↦e)·¬y≡y'·¬x↦y≡∂(i)↦e),0)); intros Hi1; Btac_freehyp;
  apply (Bpr_fori_diff _ _ i _ H H0); Btac_free; simpl; DB_simpl;
  irewrite (Bt_esubst_free _ _ H1 ∂(i1)); irewrite (Bt_esubst_free _ _ H7 ∂(i1)).
 apply Bpr_impe; apply Bpr_ctnn; apply Bpr_impi; apply Bpr_swap; apply Bpr_impe; apply Bpr_ctnp;
  apply Bpr_impi.

 generalize (Bg_fshun_free (G·x↦y≡∂(i1)↦〈i←∂(i1)@Ε@e〉·¬y≡y'·¬x↦y'≡∂(i)↦e) _ (le_n_Sn _) 1);
  set (i2:=¡(S ⋇ˠ(G·x↦y≡∂(i1)↦〈i←∂(i1)@Ε@e〉·¬y≡y'·¬x↦y'≡∂(i)↦e),1)); intros Hi2; Btac_freehyp;
  apply (Bpr_fori_diff _ _ i _ H8 H9); Btac_free; simpl; DB_simpl;
  irewrite (Bt_esubst_free _ _ H6 ∂(i2)); irewrite (Bt_esubst_free _ _ H5 ∂(i2)).
 apply Bpr_impe; apply Bpr_ctnn; apply Bpr_impi.
 apply (Bpr_cutr (G·x↦y≡∂(i1)↦〈i←∂(i1)@Ε@e〉·x↦y'≡∂(i2)↦〈i←∂(i2)@Ε@e〉) (x≡∂(i1)⋀y≡〈i←∂(i1)@Ε@e〉));
 [apply (bbt_1_5_5); Btac_hyp | ].
 apply (Bpr_cutr (G·x↦y≡∂(i1)↦〈i←∂(i1)@Ε@e〉·x↦y'≡∂(i2)↦〈i←∂(i2)@Ε@e〉·x≡∂(i1)⋀y≡〈i←∂(i1)@Ε@e〉)
                 (x≡∂(i2)⋀y'≡〈i←∂(i2)@Ε@e〉)); [apply (bbt_1_5_5); Btac_hyp | ].
 apply Bpr_impe; apply Bpr_impe; apply Bpr_nulg; Btac_prop.


 generalize (Bg_fshun_free (∅ˠ·x≡∂(i1)·y≡〈i←∂(i1)@Ε@e〉·x≡∂(i2)·y'≡〈i←∂(i2)@Ε@e〉·y≡y'·e≡∂(i)) _
                           (le_n_Sn _) 2);
  set (i3:=¡(S ⋇ˠ(∅ˠ·x≡∂(i1)·y≡〈i←∂(i1)@Ε@e〉·x≡∂(i2)·y'≡〈i←∂(i2)@Ε@e〉·y≡y'·e≡∂(i)),2)); intros Hi3;
  Btac_freehyp; rename H17 into Hi3e.
 replace (y≡y') with 〈i3←y@Π@∂(i3)≡y'〉;
 [ | Btac_free; simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ H27 y); apply refl_equal].
 apply Bpr_leib with (e1:=〈i←∂(i2)@Ε@e〉);
 [ | Btac_free; simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ H19 〈i←∂(i2)@Ε@e〉);
     apply Bpr_impe; apply Bpr_nulg; apply bbt_1_4_1].
 replace (〈i←∂(i2)@Ε@e〉≡y) with 〈i3←y@Π@〈i←∂(i2)@Ε@e〉≡∂(i3)〉;
 [ | Btac_free; simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ H26 y); apply refl_equal].
 apply Bpr_leib with (e1:=〈i←∂(i1)@Ε@e〉).
  apply Bpr_clear; apply Bpr_clear; apply Bpr_impe; apply Bpr_nulg; apply bbt_1_4_1.
  Btac_free; simpl; nat_simpl; simpl; irewrite (Bt_esubst_free _ _ H26 〈i←∂(i1)@Ε@e〉).
  apply Bpr_clear; apply Bpr_swap; apply Bpr_clear.
  apply Bpr_modp with (p1:=∂(i1)≡∂(i2)).
   replace (∂(i1)≡∂(i2)) with 〈i3←∂(i1)@Π@∂(i3)≡∂(i2)〉;
   [ | simpl; nat_simpl; bool_simpl; apply refl_equal].
   apply Bpr_leib with (e1:=x); [Btac_hyp | ].
   simpl; nat_simpl; bool_simpl; simpl; Btac_hyp.
   apply Bpr_nulg; apply Bpr_impi;
    replace (〈i←∂(i2)@Ε@e〉≡〈i←∂(i1)@Ε@e〉) with 〈i3←∂(i2)@Π@〈i←∂(i3)@Ε@e〉≡〈i←∂(i1)@Ε@e〉 〉;
    [apply Bpr_leib with (e1:=∂(i1)); [Btac_hyp | ] | ]; simpl.
    irewrite (Bt_esubst_dbl e i3 i ∂(i1) H17).
    assert (Hi3i:i3<>i).
     case_eq (DB_eq i3 i); intros Hi3i; reflect_in DB_eq_imp Hi3i;
     [rewrite Hi3i in H28; simpl in H28; DB_simpl; inversion H28 | unfold not; apply Hi3i].
    assert (Hii1:i ∖ ∂(i1)).
     case_eq (DB_eq i i1); intros Heq.
      reflect_in DB_eq_imp Heq; rewrite Heq in H0;
       destruct (Bfree_equ_elim _ _ _ (Bfree_not_elim _ _ H0)) as [_ Hii1]; Btac_free;
       simpl in H29; nat_simpl; inversion H29.
      apply Heq.
    irewrite (Bt_esubst_comm e i3 i ∂(i1) ∂(i1) Hi3i H23 Hii1);
     irewrite (Bt_esubst_free _ _ H17 ∂(i1)); apply Bpr_refl.
    irewrite (Bt_esubst_dbl e i3 i ∂(i2) H17).
    assert (Hi3i:i3<>i).
     case_eq (DB_eq i3 i); intros Hi3i; reflect_in DB_eq_imp Hi3i;
     [rewrite Hi3i in H28; simpl in H28; DB_simpl; inversion H28 | unfold not; apply Hi3i].
    assert (Hii2:i ∖ ∂(i2)).
     case_eq (DB_eq i i2); intros Heq.
      reflect_in DB_eq_imp Heq; rewrite Heq in H9;
       destruct (Bfree_equ_elim _ _ _ (Bfree_not_elim _ _ H9)) as [_ Hii2]; Btac_free;
       simpl in H29; nat_simpl; inversion H29.
      apply Heq.
    irewrite (Bt_esubst_comm e i3 i ∂(i2) ∂(i1) Hi3i H23 Hii2);
     irewrite (Bt_esubst_free _ _ H17 ∂(i2)); apply refl_equal.
Qed.

Theorem Blambda_esubst:forall (G:Γ)(s t e x y:Ε)(i:Ι),
                       (i∖x↦〈i←x@Ε@e〉)->G⊢x∈s->G⊢〈i←x@Ε@e〉∈t->G⊢x↦〈i←x@Ε@e〉∈∫(i∊s⇢e∊t).
Proof.
 intros G s t e x y i Hi Hx Hy; unfold Blambda; set (ij:=⋕(∂(i)↦e)); apply Bpr_cmpi.
  apply Bpr_prodec; [apply Hx | apply Hy].
  generalize (Bt_fresh_free (∂(i)↦e)); fold ij; intros Hiji.
  assert (H'iji:ij<>i).
   Btac_free; case_eq (DB_eq ij i); intros Heq; reflect_in DB_eq_imp Heq.
    rewrite Heq in H; simpl in H; DB_simpl; inversion H.
    unfold not; apply Heq.
  unfold Bp_esubst_; fold Bp_esubst_.
  replace 〈ij←x↦〈i←x@Ε@e〉@Π@∀(i∙¬∂(ij)≡∂(i)↦e)〉 with ∀(i∙〈ij←x↦〈i←x@Ε@e〉@Π@¬∂(ij)≡∂(i)↦e〉);
  [ | srewrite (Bp_forall_esubst_diff (¬∂(ij)≡∂(i)↦e) _ _ _ H'iji Hi); apply refl_equal].
  unfold Bp_esubst_; fold Bp_esubst_ Be_esubst_; DB_simpl; rewrite (imprf2 DB_eq_imp _ _ H'iji).
  apply bbdr13 with (e:=x); simpl; DB_simpl; Btac_free; irewrite (Bt_esubst_free _ _ H1 x);
   irewrite (Bt_esubst_free _ _ (Bt_free_esubst e i x H1) x);
   irewrite (Bt_esubst_free _ _ H0 (x↦〈i←x@Ε@e〉)); apply Bpr_refl.
Qed.

(*================================================================================================*)
