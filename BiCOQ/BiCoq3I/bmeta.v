(*==================================================================================================
  Project : BiCoq3
  Module : bmeta.v
  We consider here some questions about meta-logic, various definitions of the B consistency, and so
  on.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Feb 2009, Md: Feb 2009
  ================================================================================================*)

Require Export bbfix.

(* True and false --------------------------------------------------------------------------------*)

Definition B_true := Ω≡Ω.

Definition B_false := ¬B_true.

Theorem B_true_true:⊢B_true.
Proof.
 unfold B_true; apply Bpr_refl.
Qed.

Theorem B_false_false:⊢¬B_false.
Proof.
 unfold B_false; Btac_prop; apply B_true_true.
Qed.

(* How to define B consistency -------------------------------------------------------------------*)

Definition B_cons0 := forall (p:Π), ⊢p->⊢¬p->False.
(* There is no predicate which is provable and refutable *)

Definition B_cons1 := sigS (fun p:Π=>⊢p->False).
(* There exist a predicate which is not provable *)

Definition B_cons2 := (forall (p:Π), ⊢p)->False.
(* It is false that all predicates are provable *)

Definition B_cons3 := sigS (fun p:Π=>prod (⊢p) (⊢¬p))->False.
(* It is false that there exists a predicate which is provable and refutable *)

Definition B_cons4 := ⊢B_false->False.
(* Ω≢Ω is provably not provable *)

Theorem B_cons01:B_cons0->B_cons1.
Proof.
 unfold B_cons0,B_cons1; intros HB; exists B_false; intros Hfalse; apply (HB B_true);
 [ apply B_true_true | apply Hfalse].
Qed.

Theorem B_cons12:B_cons1->B_cons2.
Proof.
 unfold B_cons1,B_cons2; intros [p Hp] HB; apply (Hp (HB p)).
Qed.

Theorem B_cons23:B_cons2->B_cons3.
Proof.
 unfold B_cons2,B_cons3; intros HB [p [Hp H'p]]; apply HB; intros p'; apply Bpr_absp with (p2:=p);
  apply Bpr_clear; [apply Hp | apply H'p].
Qed.

Theorem B_cons30:B_cons3->B_cons0.
Proof.
 unfold B_cons3,B_cons0; intros HB p Hp H'p; apply HB; exists p; apply (pair Hp H'p).
Qed.

Theorem B_cons41:B_cons4->B_cons1.
Proof.
 unfold B_cons4,B_cons1; intros Hfalse; exists B_false; apply Hfalse.
Qed.

Theorem B_cons04:B_cons0->B_cons4.
Proof.
 unfold B_cons0,B_cons4; intros HB Hfalse; apply (HB _ B_true_true Hfalse).
Qed.

(*================================================================================================*)