(*==================================================================================================
  Project : BiCoq3
  Module : lift.v
  Uplift of dangling De Bruijn indexes in a term.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jun 2008, Md: Mar 2009
  ================================================================================================*)

Require Export term.

(* Uplift ----------------------------------------------------------------------------------------*)
(* Uplift increments all dangling De Bruijn indexes of namespace 0. *)

Fixpoint Bp_lift_(d:nat)(p:Π){struct p}:Π:=
 match p with
 | ¬p' => ¬(Bp_lift_ d p')
 | ∀Δ(p') => ∀Δ(Bp_lift_ (S d) p')
 | p1⋀p2 => (Bp_lift_ d p1)⋀(Bp_lift_ d p2)
 | (p1⇒p2) => bimp (Bp_lift_ d p1) (Bp_lift_ d p2)
 | ∐(_) => p
 | e1≡e2 => (Be_lift_ d e1)≡(Be_lift_ d e2)
 | e1∈e2 => (Be_lift_ d e1)∈(Be_lift_ d e2)
 end
with Be_lift_(d:nat)(e:Ε){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_lift_ d e')
 | ↑(e') => ↑(Be_lift_ d e')
 | e1↦e2 => (Be_lift_ d e1)↦(Be_lift_ d e2)
 | e1×e2 => (Be_lift_ d e1)×(Be_lift_ d e2)
 | ∞(n') => e
 | ∂(i) => ∂(DB_lift_ d i)
 | ⅭΔ(e',p') => ⅭΔ(Be_lift_ d e',Bp_lift_ (S d) p')
 end.
Notation "'⇑Π(' p ')'":=(Bp_lift_ 0 p).
Notation "'⇑Ε(' e ')'":=(Be_lift_ 0 e).

Definition Bt_lift_(d:nat)(t:Τ):Τ := match t with be_ e => Be_lift_ d e | bp_ p => Bp_lift_ d p end.
Notation "'⇑(' t ')'":=(Bt_lift_ 0 t).

(* Theorems --------------------------------------------------------------------------------------*)

Lemma Bt_lift_free_:forall (t:Τ)(i:Ι)(d:nat),
                    Bt_free_ (S d) (DB_lift_ d i) (Bt_lift_ d t)=Bt_free_ d i t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat),
                            Bt_free_ (S d) (DB_lift_ d i) (Bt_lift_ d t)=Bt_free_ d i t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | i' | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i d;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) i d));
  try (srewrite (Hind _ (sdepth_l t1 t2) i d); srewrite (Hind _ (sdepth_r t1 t2) i d);
       apply refl_equal).
  unfold Bt_lift_,Be_lift_,Bt_free_,Be_free_; fold Be_free_; DB_simpl; destruct (DB_le d i');
   apply refl_equal.
  srewrite (Hind _ (sdepth_l t1 t2) i d); srewrite (Hind _ (sdepth_r t1 t2) (DB_lift_ d i) (S d));
   apply refl_equal.
  srewrite (Hind _ (sdepth t) (DB_lift_ d i) (S d)); apply refl_equal.
Qed.

Theorem Bt_lift_ground0_:forall (t:Τ)(d:nat),
                         (forall (n:nat), Bt_free_ d ¡(0,n) t=⊥)->Bt_lift_ d t=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d:nat),
                            (forall (n:nat), Bt_free_ d ¡(0,n) t=⊥)->Bt_lift_ d t=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind d Hgr;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ Hgr); apply refl_equal);
  try (cut ((forall (n:nat), Bt_free_ d ¡(0,n) t1=⊥) /\ (forall (n:nat), Bt_free_ d ¡(0,n) t2=⊥));
       [intros [Hgr1 Hgr2]; irewrite (Hind _ (sdepth_l t1 t2) _ Hgr1);
        irewrite (Hind _ (sdepth_r t1 t2) _ Hgr2); apply refl_equal |
        split; intros n; destruct (orb_false_elim _ _ (Hgr n)) as [H'gr1 H'gr2]; assumption]).
  generalize (Hgr n'); intros H'gr; unfold Bt_free_,Be_free_ in H'gr;
   unfold Bt_lift_,Be_lift_,DB_lift_; simpl in H'gr; nat_simpl; bool_simpl; case_eq (nat_le d n');
   intros Hdn'; rewrite Hdn' in *; [inversion H'gr | apply refl_equal].
  cut ((forall (n:nat), Bt_free_ d ¡(0,n) t1=⊥) /\ (forall (n:nat), Bt_free_ (S d) ¡(0,n) t2=⊥)).
   intros [Hgr1 Hgr2]; irewrite (Hind _ (sdepth_l t1 t2) _ Hgr1);
    irewrite (Hind _ (sdepth_r t1 t2) _ Hgr2); apply refl_equal.
   split; intros n.
    destruct (orb_false_elim _ _ (Hgr n)) as [H'gr1 _]; apply H'gr1.
    simpl in Hgr; case_eq (nat_le (S d) n); intros HSdn.
     reflect_in nat_le_imp HSdn; destruct n as [| n]; [inversion HSdn | ].
      destruct (orb_false_elim _ _ (Hgr n)) as [_ Hgr2];
       rewrite (imppt2 nat_le_imp _ _ (le_S_n _ _ HSdn)) in Hgr2; apply Hgr2.
     apply Bt_le_free_; DB_simpl; simpl; apply HSdn.
  cut (forall (n:nat), Bt_free_ (S d) ¡(0,n) t=⊥).
   intros H'gr; irewrite (Hind _ (sdepth t) _ H'gr); apply refl_equal.
   intros n; simpl in Hgr; case_eq (nat_le (S d) n); intros HSdn.
     reflect_in nat_le_imp HSdn; destruct n as [| n]; [inversion HSdn | ].
      generalize (Hgr n); rewrite (imppt2 nat_le_imp _ _ (le_S_n _ _ HSdn)); intros H'gr;
       apply H'gr.
     apply Bt_le_free_; DB_simpl; simpl; apply HSdn.
Qed.

Theorem Bt_lift_ground0:forall (t:Τ), (forall (n:nat), ¡(0,n)∖t)->⇑(t)=t.
Proof.
 intros t Hg; apply (Bt_lift_ground0_ _ _ Hg).
Qed.

Theorem Bt_lift_ground_:forall (t:Τ)(d:nat), Bt_ground_ d t=⊤->Bt_lift_ d t=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d:nat), Bt_ground_ d t=⊤->Bt_lift_ d t=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind d Hgr;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ Hgr); apply refl_equal);
  try (destruct (andb_prop _ _ Hgr) as [Hgr1 Hgr2]; irewrite (Hind _ (sdepth_l t1 t2) _ Hgr1);
       irewrite (Hind _ (sdepth_r t1 t2) _ Hgr2); apply refl_equal).
  unfold Bt_ground_,Be_ground_ in Hgr; case_eq (DB_le d ¡(0,n')); intros Hdn'; rewrite Hdn' in Hgr;
  [inversion Hgr | simpl in Hdn'; simpl; rewrite Hdn'; apply refl_equal].
Qed.

Theorem Bt_lift_ground:forall (t:Τ), ∖∖(t)->⇑(t)=t.
Proof.
 intros t Hg; apply (Bt_lift_ground_ _ _ Hg).
Qed.

Theorem Bt_free_lift_S:forall (t:Τ)(i:Ι)(d:nat),
                       Bt_free_ d (DB_lift_ d i) t=Bt_free_ (S d) (DB_lift_ d i) t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat),
                            Bt_free_ d (DB_lift_ d i) t=Bt_free_ (S d) (DB_lift_ d i) t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i d;
  try (apply refl_equal); try (srewrite (Hind _ (sdepth t) i d); apply refl_equal);
  try (srewrite (Hind _ (sdepth_l t1 t2) i d); srewrite (Hind _ (sdepth_r t1 t2) i d);
       apply refl_equal).
  simpl; case_eq (nat_le d n'); intros Hdn'; case_eq (nat_le (S d) n'); intros HSdn';
   try (apply refl_equal).
   reflect_in nat_le_imp Hdn'; reflect_in nat_le_imp HSdn';
    rewrite (le_antisym _ _ Hdn' (le_S_n _ _ (not_ge _ _ HSdn'))) in *; DB_simpl; apply refl_equal.
   reflect_in nat_le_imp HSdn';
    rewrite (imppt2 nat_le_imp _ _ (le_S_n _ _ (le_S _ _ HSdn'))) in Hdn'; inversion Hdn'.
  srewrite (Hind _ (sdepth_l t1 t2) i d); rewrite DB_lift_lift;
   srewrite (Hind _ (sdepth_r t1 t2) (DB_lift_ d i) (S d)); apply refl_equal.
  simpl; rewrite DB_lift_lift; srewrite (Hind _ (sdepth t) (DB_lift_ d i) (S d)); apply refl_equal.
Qed.

Theorem Bt_lift_lift_:forall (t:Τ)(d d':nat),
                      d'<=d->Bt_lift_ d' (Bt_lift_ d t)=Bt_lift_ (S d) (Bt_lift_ d' t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d d':nat), d'<=d->
                            Bt_lift_ d' (Bt_lift_ d t)=Bt_lift_ (S d) (Bt_lift_ d' t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind d d' Hd'd;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) _ _ Hd'd); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) _ _ Hd'd); irewrite (Hind _ (sdepth_r t1 t2) _ _ Hd'd);
       apply refl_equal).
  simpl; case_eq (nat_le d n'); intros Hdn'; reflect_in nat_le_imp Hdn'.
   rewrite (imppt2 nat_le_imp _ _ (le_trans _ _ _ Hd'd Hdn')); nat_simpl;
    rewrite (imppt2 nat_le_imp _ _ Hdn');
    rewrite (imppt2 nat_le_imp _ _ (le_S _ _ (le_trans _ _ _ Hd'd Hdn'))); apply refl_equal.
   case_eq (nat_le d' n'); intros Hd'n'; nat_simpl.
    rewrite (imprf2 nat_le_imp _ _ Hdn'); apply refl_equal.
    rewrite (imprf2 nat_le_imp _ _ (lt_not_le _ _ (lt_S _ _ (not_ge _ _ Hdn')))); apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) _ _ Hd'd);
   irewrite (Hind _ (sdepth_r t1 t2) _ _ (le_n_S _ _ Hd'd)); apply refl_equal.
  irewrite (Hind _ (sdepth t) _ _ (le_n_S _ _ Hd'd)); apply refl_equal.
Qed.

(* About compatibility ---------------------------------------------------------------------------*)

Theorem Bt_lift_comp_:forall (t:Τ)(d l:nat),
                      Bt_comp_ d (S l) (Bt_lift_ l t)=Bt_comp_ d l t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d l:nat), Bt_comp_ d (S l) (Bt_lift_ l t)=Bt_comp_ d l t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind d l;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) d l));
  try (srewrite (Hind _ (sdepth_l t1 t2)); srewrite (Hind _ (sdepth_r t1 t2)); apply refl_equal).
  simpl; nat_simpl; case_eq (nat_le l n'); intros Hln'; nat_simpl.
   rewrite Hln'; destruct (nat_le d (S n')); destruct (nat_le d n'); apply refl_equal.
   reflect_in nat_le_imp Hln';
    rewrite (imprf2 nat_le_imp _ _ (lt_not_le _ _ (lt_S _ _ (not_ge _ _ Hln')))); apply refl_equal.
  apply (Hind _ (sdepth t) (S d) (S l)).
Qed.

Theorem Bt_lift_comp_':forall (t:Τ)(d l:nat),
                       Bt_comp_ (S d) (S l) (Bt_lift_ d t)=Bt_comp_ d l t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d l:nat), Bt_comp_ (S d) (S l) (Bt_lift_ d t)=Bt_comp_ d l t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind d l;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) d l));
  try (srewrite (Hind _ (sdepth_l t1 t2)); srewrite (Hind _ (sdepth_r t1 t2)); apply refl_equal).
  simpl; nat_simpl; case_eq (nat_le d n'); intros Hdn'.
   nat_simpl; rewrite Hdn'; apply refl_equal.
   reflect_in nat_le_imp Hdn';
    rewrite (imprf2 nat_le_imp _ _ (lt_not_le _ _ (lt_S _ _ (not_ge _ _ Hdn')))); simpl;
    apply refl_equal.
  apply (Hind _ (sdepth t) (S d) (S l)).
Qed.

(*================================================================================================*)
