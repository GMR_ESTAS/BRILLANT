(*==================================================================================================
  Project : BiCoq3
  Module : apply.v
  We represent here application of a term to an abstraction, followed by reduction; neither the
  application nor the reduction being part of the embedded language, we code the corresponding
  situations by applying dedicated functions to terms.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Jun 2008, Md: Mar 2009
  ================================================================================================*)

Require Export lift.

(* Application function --------------------------------------------------------------------------*)

Fixpoint Bp_app_(d:nat)(p:Π)(a:Ε){struct p}:Π:=
 match p with
 | ¬p' => ¬(Bp_app_ d p' a)
 | ∀Δ(p') => ∀Δ(Bp_app_ (S d) p' (Be_lift_ d a))
 | p1⋀p2 => (Bp_app_ d p1 a)⋀(Bp_app_ d p2 a)
 | (p1⇒p2) => (Bp_app_ d p1 a)⇒(Bp_app_ d p2 a)
 | ∐(_) => p
 | e1≡e2 => (Be_app_ d e1 a)≡(Be_app_ d e2 a)
 | e1∈e2 => (Be_app_ d e1 a)∈(Be_app_ d e2 a)
 end
with Be_app_(d:nat)(e a:Ε){struct e}:Ε :=
 match e with
 | Ω => e
 | ↓(e') => ↓(Be_app_ d e' a)
 | ↑(e') => ↑(Be_app_ d e' a)
 | e1↦e2 => (Be_app_ d e1 a)↦(Be_app_ d e2 a)
 | e1×e2 => (Be_app_ d e1 a)×(Be_app_ d e2 a)
 | ∞(_) => e
 | ∂(¡(0,n')) => if (nat_le d n') then (if nat_eq d n' then a else ∂(¡(0,pred n'))) else e
 | ∂(_) => e
 | ⅭΔ(e',p') => ⅭΔ(Be_app_ d e' a,Bp_app_ (S d) p' (Be_lift_ d a))
 end.

Definition Bt_app_(d:nat)(t:Τ)(a:Ε):Τ :=
 match t with be_ e => Be_app_ d e a | bp_ p => Bp_app_ d p a end.

Definition B_forapp(p:Π)(a:Ε)(H:?∀Δ(p)):Π := match p with ∀Δ(p') => Bp_app_ 0 p' a | _ => ∐(0) end.
Notation "'(' p '@' a '|' H ')'" := (B_forapp p a H).

Definition B_cmpapp(e a:Ε)(H:?ⅭΔ(e)):Π :=
 match e with ⅭΔ(e',p') => (a∈e')⋀(Bp_app_ 0 p' a) | _ => ∐(0) end.
Notation "'(' e '∋' a '|' H ')'" := (B_cmpapp e a H).

(* Theorems --------------------------------------------------------------------------------------*)

Lemma Bt_app_free_old_:forall (t:Τ)(i:Ι)(d:nat)(a:Ε), Bt_free_ (S d) (DB_lift_ d i) t=⊤->
                       Bt_free_ d i (Bt_app_ d t a)=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat)(a:Ε), Bt_free_ (S d) (DB_lift_ d i) t=⊤->
                            Bt_free_ d i (Bt_app_ d t a)=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i d a Ht;
  try (inversion Ht; fail); try (apply (Hind _ (sdepth t) _ _ a Ht));
  try (destruct (orb_true_elim _ _ Ht) as [Ht1 | Ht2];
       [srewrite (Hind _ (sdepth_l t1 t2) _ _ a Ht1) |
        srewrite (Hind _ (sdepth_r t1 t2) _ _ a Ht2); bool_simpl]; apply refl_equal).
  simpl in *; unfold Bt_free_,Be_free_ in Ht; DB_simpl; case_eq (nat_le (S d) n'); intros HSdn';
   rewrite HSdn' in Ht; [simpl in Ht | inversion Ht].
   reflect_in nat_le_imp HSdn'; rewrite (imppt2 nat_le_imp _ _ (le_S_n _ _ (le_S _ _ HSdn')));
    case_eq (nat_eq d n'); intros Hdn'.
    reflect_in nat_eq_imp Hdn'; rewrite Hdn' in HSdn'; destruct (le_Sn_n _ HSdn').
    destruct n' as [| n']; [inversion HSdn' | ]; replace ¡(0,S n') with (DB_lift_ d ¡(0,n')) in Ht.
     unfold Be_free_; DB_simpl; unfold pred; simpl;
      rewrite (imppt2 nat_le_imp _ _ (le_S_n _ _ HSdn')); apply Ht.
     simpl; rewrite (imppt2 nat_le_imp _ _ (le_S_n _ _ HSdn')); apply refl_equal.
  unfold Bt_app_,Be_app_,Bt_free_,Be_free_; unfold Bt_free_,Be_free_ in Ht; DB_simpl;
   change ¡(S u',n') with (DB_lift_ d ¡(S u',n')) in Ht; DB_simpl; apply Ht.
  destruct (orb_true_elim _ _ Ht) as [Ht1 | Ht2]; fold Be_free_ Bp_free_ in *.
   srewrite (Hind _ (sdepth_l t1 t2) _ _ a Ht1); apply refl_equal.
   srewrite (Hind _ (sdepth_r t1 t2) _ _ (Be_lift_ d a) Ht2); bool_simpl; apply refl_equal.
  apply (Hind _ (sdepth t) _ _ (Be_lift_ d a) Ht).
Qed.

Theorem Bt_app_new_:forall (t:Τ)(i:Ι)(d:nat)(a:Ε), Bt_free_ d i a=⊤->Bt_free_ d ¡(0,d) t=⊤->
                    Bt_free_ d i (Bt_app_ d t a)=⊤.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(d:nat)(a:Ε), Bt_free_ d i a=⊤->Bt_free_ d ¡(0,d) t=⊤->
                            Bt_free_ d i (Bt_app_ d t a)=⊤));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind i d a Ha Ht;
  try (inversion Ht; fail); try (apply (Hind _ (sdepth t) _ _ _ Ha Ht));
  try (destruct (orb_true_elim _ _ Ht) as [Ht1 | Ht2];
       [srewrite (Hind _ (sdepth_l t1 t2) _ _ _ Ha Ht1) |
        srewrite (Hind _ (sdepth_r t1 t2) _ _ _ Ha Ht2); bool_simpl]; apply refl_equal).
  unfold Bt_free_,Be_free_,Bt_app_,Be_app_ in *; fold Be_free_ in *; DB_simpl;
   case_eq (nat_le d n'); intros Hdn'; simpl in *; rewrite Hdn' in *; [ | inversion Ht].
    case_eq (nat_eq d n'); intros H'dn'; reflect_in nat_eq_imp H'dn'.
     apply Ha.
     simpl in Ht; reflect_in nat_eq_imp H'dn'; rewrite H'dn' in Ht; nat_simpl; inversion Ht.
    simpl in Ht; nat_simpl; inversion Ht.
  destruct (orb_true_elim _ _ Ht) as [Ht1 | Ht2]; fold Be_free_ Bp_free_ in *.
   srewrite (Hind _ (sdepth_l t1 t2) _ _ _ Ha Ht1); apply refl_equal.
   rewrite <- (Bt_lift_free_ a i d) in Ha; unfold DB_lift_ in Ht2; nat_simpl; bool_simpl;
    srewrite (Hind _ (sdepth_r t1 t2) _ _ _ Ha Ht2); bool_simpl; apply refl_equal.
  rewrite <- (Bt_lift_free_ a i d) in Ha; unfold DB_lift_ in Ht; simpl in Ht; nat_simpl; bool_simpl;
   apply (Hind _ (sdepth t) _ _ _ Ha Ht).
Qed.

Lemma Bt_app_lift_:forall (t:Τ)(d:nat)(a:Ε), Bt_free_ d ¡(0,d) t=⊥->Bt_lift_ d (Bt_app_ d t a)=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d:nat)(a:Ε), Bt_free_ d ¡(0,d) t=⊥->
                            Bt_lift_ d (Bt_app_ d t a)=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [[| u'] n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind d a Ht;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) d a Ht); apply refl_equal);
  try (destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2]; irewrite (Hind _ (sdepth_l t1 t2) d a Ht1);
       irewrite (Hind _ (sdepth_r t1 t2) d a Ht2); apply refl_equal).
  unfold Bt_lift_,Be_lift_,Bt_app_,Be_app_; fold Be_lift_; unfold Bt_free_,Be_free_ in Ht;
   DB_simpl; case_eq (nat_le d n'); intros Hdn'; simpl in *; rewrite Hdn' in *.
   simpl in Ht; nat_simpl; bool_simpl; rewrite Ht; destruct n' as [| n'].
    reflect_in nat_le_imp Hdn'; inversion Hdn'; rewrite H in Ht; nat_simpl; inversion Ht.
    unfold pred; unfold Be_lift_,DB_lift_; nat_simpl; bool_simpl; case_eq (nat_le d n');
     intros H'dn'; [apply refl_equal | ].
     reflect_in nat_eq_imp Ht; reflect_in nat_le_imp Hdn'; reflect_in nat_le_imp H'dn';
      destruct (le_lt_or_eq _ _ Hdn'); [destruct (H'dn' (le_S_n _ _ H)) | destruct (Ht H)].
   apply refl_equal.
  destruct (orb_false_elim _ _ Ht) as [Ht1 Ht2]; fold Be_free_ Bp_free_ in *;
   irewrite (Hind _ (sdepth_l t1 t2) d a Ht1); unfold DB_lift_ in Ht2; nat_simpl; bool_simpl;
   irewrite (Hind _ (sdepth_r t1 t2) (S d) (Be_lift_ d a) Ht2); apply refl_equal.
  simpl in Ht; unfold DB_lift_ in Ht; nat_simpl; bool_simpl;
   irewrite (Hind _ (sdepth t) (S d) (Be_lift_ d a) Ht); apply refl_equal.
Qed.

Lemma Bt_lift_app_:forall (t:Τ)(d:nat)(a:Ε), Bt_app_ d (Bt_lift_ d t) a=t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d:nat)(a:Ε), Bt_app_ d (Bt_lift_ d t) a=t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind d a;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) d a); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) d a); irewrite (Hind _ (sdepth_r t1 t2) d a);
       apply refl_equal).
  destruct u' as [ | u']; nat_simpl; bool_simpl.
   case_eq (nat_le d n'); intros Hdn'.
    reflect_in nat_le_imp Hdn'; rewrite (imppt2 nat_le_imp _ _ (le_S _ _ Hdn'));
     case_eq (nat_eq d (S n')); intros Heq.
     reflect_in nat_eq_imp Heq; rewrite Heq in Hdn'; destruct (le_Sn_n _ Hdn').
     apply refl_equal.
    rewrite Hdn'; apply refl_equal.
   apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) d a); irewrite (Hind _ (sdepth_r t1 t2) (S d) (Be_lift_ d a));
   apply refl_equal.
  irewrite (Hind _ (sdepth t) (S d) (Be_lift_ d a)); apply refl_equal.
Qed.

Theorem Bt_app_depth:forall (t:Τ)(d:nat)(i:Ι), ↧(Bt_app_ d t ∂(i))=↧(t).
Proof.
 apply (wf_term (fun (t:Τ)=>forall (d:nat)(i:Ι), ↧(Bt_app_ d t ∂(i))=↧(t)));
  intros [[ | t | t | t1 t2 | t1 t2 | n' | [u' n'] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; intros Hind d i;
  try (apply refl_equal); try (srewrite (Hind _ (sdepth t) d i); apply refl_equal);
  try (srewrite (Hind _ (sdepth_l t1 t2) d i); srewrite (Hind _ (sdepth_r t1 t2) d i);
       apply refl_equal).
  destruct u' as [| u'].
   simpl in *; destruct (nat_le d n'); destruct (nat_eq d n'); apply refl_equal.
   apply refl_equal.
  unfold Bt_app_; unfold Be_app_; fold Be_app_; fold Bp_app_; replace ⇑Ε(∂(i)) with ∂(↥(i)).
   srewrite (Hind _ (sdepth_l t1 t2) d i); srewrite (Hind _ (sdepth_r t1 t2) (S d) (DB_lift_ d i));
    apply refl_equal.
   simpl; destruct i as [[| u] n]; nat_simpl; bool_simpl; simpl; apply refl_equal.
  unfold Bt_app_; unfold Bp_app_; fold Bp_app_; replace ⇑Ε(∂(i)) with ∂(↥(i)).
   srewrite (Hind _ (sdepth t) (S d) (DB_lift_ d i)); apply refl_equal.
   simpl; destruct i as [[| u] n]; nat_simpl; bool_simpl; simpl; apply refl_equal.
Qed.

(*================================================================================================*)