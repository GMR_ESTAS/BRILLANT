(*==================================================================================================
  Project : BiCoq3
  Module : egraft_congr.v
  We show here a very interesting property, that is equality is a congruence for equality. This
  considerably extends the Leibniz principle (a.k.a. Bpr_leib in this embedding) as it allows for
  substitution of bound subterms.
  --------------------------------------------------------------------------------------------------
  IPA E. Jaeger - SGDN/DCSSI/SDS/LTI, UPMC/LIP6/SPI - St: Oct 2008, Md: Mar 2009
  ================================================================================================*)

Require Export egraft.
Require Export sem_ind.
Require Export mesubst_framed.

(* Easy rewriting --------------------------------------------------------------------------------*)

Theorem MS_iff_distr:forall (p1 p2:Π)(m:Μ), 〈〈m@Π@(p1⇔p2)〉〉=(〈〈m@Π@p1〉〉⇔〈〈m@Π@p2〉〉).
Proof.
 intros p1 p2 m; apply refl_equal.
Qed.

Theorem MS_equ_distr:forall (e1 e2:Ε)(m:Μ), 〈〈m@Π@(e1≡e2)〉〉=(〈〈m@Ε@e1〉〉≡〈〈m@Ε@e2〉〉).
Proof.
 intros p1 p2 m; apply refl_equal.
Qed.

(* About depth, multiple substitutions and grafting ----------------------------------------------*)

Theorem Bt_mesubst_depth:forall (t:Τ)(m:Μ)(d:nat),
                         Bt_mesubst_ d (MS_lift_ d m) t=Bt_mesubst_ (S d) (MS_lift_ d m) t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (m:Μ)(d:nat),
                            Bt_mesubst_ d (MS_lift_ d m) t=Bt_mesubst_ (S d) (MS_lift_ d m) t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' |  [[| u'] [| n']] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind m d;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) m d); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) m d); irewrite (Hind _ (sdepth_r t1 t2) m d);
       apply refl_equal).
  destruct d; nat_simpl; apply refl_equal.
  nat_simpl; case_eq (nat_le d n'); intros Hdn'.
   reflect_in nat_le_imp Hdn'; rewrite (imppt2 nat_le_imp _ _ (le_S _ _ Hdn')); apply refl_equal.
   destruct (nat_le d (S n')); apply refl_equal.
  irewrite (Hind _ (sdepth_l t1 t2) m d);
   irewrite (MS_eq_mesubst_ t2 _ _ (MS_lift_dbl m _ _ (le_refl d)) (S d));
   irewrite (Hind _ (sdepth_r t1 t2) (MS_lift_ d m) (S d)); apply refl_equal.
  irewrite (MS_eq_mesubst_ t _ _ (MS_lift_dbl m _ _ (le_refl d)) (S d));
   irewrite (Hind _ (sdepth t) (MS_lift_ d m) (S d)); apply refl_equal.
Qed.

Theorem Bt_egraft_depth:forall (t:Τ)(i:Ι)(e:Ε)(d:nat),
                        Bt_egraft_ d (DB_lift_ d i) t e=Bt_egraft_ (S d) (DB_lift_ d i) t e.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (i:Ι)(e:Ε)(d:nat),
                            Bt_egraft_ d (DB_lift_ d i) t e=Bt_egraft_ (S d) (DB_lift_ d i) t e));
  intros [[ | t | t | t1 t2 | t1 t2 | n' |  [[| u'] [| n']] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind i e d;
  try (apply refl_equal); try (irewrite (Hind _ (sdepth t) i e d); apply refl_equal);
  try (irewrite (Hind _ (sdepth_l t1 t2) i e d); irewrite (Hind _ (sdepth_r t1 t2) i e d);
       apply refl_equal).
  nat_simpl; simpl; case_eq (nat_le d 0); intros Hd; simpl;
  [reflect_in nat_le_imp Hd; inversion Hd; DB_simpl | ]; apply refl_equal.
  nat_simpl; case_eq (nat_le d n'); intros Hdn'; simpl.
   reflect_in nat_le_imp Hdn'; rewrite (imppt2 nat_le_imp _ _ (le_S _ _ Hdn')); apply refl_equal.
   case_eq (nat_le d (S n')); intros HdSn'; simpl; [ | apply refl_equal].
    reflect_in nat_le_imp Hdn'; reflect_in nat_le_imp HdSn';
     rewrite (le_antisym _ _ HdSn' (not_ge _ _ Hdn')); DB_simpl; apply refl_equal.
 irewrite (Hind _ (sdepth_l t1 t2) i e d); rewrite DB_lift_lift;
  irewrite (Hind _ (sdepth_r t1 t2) (DB_lift_ d i) e (S d)); apply refl_equal.
 rewrite DB_lift_lift; irewrite (Hind _ (sdepth t) (DB_lift_ d i) e (S d)); apply refl_equal.
Qed.

Theorem Bt_free_depth:forall (t:Τ)(u n d:nat), Bt_free_ d ¡(S u,n) t=Bt_free_ 0 ¡(S u,n) t.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (u n d:nat), Bt_free_ d ¡(S u,n) t=Bt_free_ 0 ¡(S u,n) t));
  intros [[ | t | t | t1 t2 | t1 t2 | n' |  [[| u'] [| n']] | t1 t2 ] |
          [ t | t | t1 t2 | t1 t2 | n' | t1 t2 | t1 t2]]; simpl; intros Hind u n d;
  try (apply refl_equal); try (apply (Hind _ (sdepth t) u n d));
  try (srewrite (Hind _ (sdepth_l t1 t2) u n d); srewrite (Hind _ (sdepth_r t1 t2) u n d);
       apply refl_equal).
  nat_simpl; bool_simpl; apply refl_equal.
  nat_simpl; bool_simpl; apply refl_equal.
  srewrite (Hind _ (sdepth_l t1 t2) u n d); srewrite (Hind _ (sdepth_r t1 t2) u n (S d));
   srewrite (Hind _ (sdepth_r t1 t2) u n 1); apply refl_equal.
  srewrite (Hind _ (sdepth t) u n (S d)); srewrite (Hind _ (sdepth t) u n 1); apply refl_equal.
Qed.

(* Technical result for congruence ---------------------------------------------------------------*)

Theorem Bpr_egraft_:forall (t:Τ),
                    forall (G:Γ)(el er:Ε), G⊢el≡er->
                    (forall (n:nat), (¡(0,n)∖el≡er) + (¡(0,n)∖ˠG))->
                    forall (i:Ι)(m:Μ),
                    (forall (u n:nat),
                     (m ¡(S u,n)=∂(¡(S u,n))) + (¡(S u,n)∖el≡er) + (¡(S u,n)∖ˠG))->
                    match t with
                    | bp_ p => G⊢〈〈m@Π@〈i⇙el@Π@p〉⇔〈i⇙er@Π@p〉 〉〉
                    | be_ e => G⊢〈〈m@Π@〈i⇙el@Ε@e〉≡〈i⇙er@Ε@e〉 〉〉
                    end.
Proof.
 apply (wf_term (fun (t:Τ)=>forall (G:Γ)(el er:Ε),
                            G⊢el≡er->(forall (n:nat), (¡(0,n)∖el≡er) + (¡(0,n)∖ˠG))->
                            forall (i:Ι)(m:Μ),
                            (forall (u n:nat),
                             (m ¡(S u,n)=∂(¡(S u,n))) + (¡(S u,n)∖el≡er) + (¡(S u,n)∖ˠG))->
                             match t with
                             | bp_ p => G⊢〈〈m@Π@〈i⇙el@Π@p〉⇔〈i⇙er@Π@p〉 〉〉
                             | be_ e => G⊢〈〈m@Π@〈i⇙el@Ε@e〉≡〈i⇙er@Ε@e〉 〉〉
                             end)); intros t;
  destruct (Bt_access_all t) as [p _ | p i' _ | p1 p2 _ _ | p1 p2 _ _ | n' | e1 e2 _ _ | e1 e2 _ _ |
                                 | e _ | e _ | e1 e2 _ _ | e1 e2 _ _ | n' | i' | p e i' _ _];
  intros Hind G el er Hequ Hgnd i m Hm.

  rewrite MS_iff_distr; simpl; apply congr_not;  rewrite <- MS_iff_distr;
   apply (Hind _ (sdepth p) G el er Hequ Hgnd i m Hm).

  rewrite MS_iff_distr; simpl.
  generalize (Bg_fshun_free (G·(Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) el))·
                               (Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) er)))
                            _ (le_n_Sn _) 0);
   set (i1:=¡(S ⋇ˠ(G·(Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) el))·
                     (Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) er))),0));
   intros Hi1.
  assert (Hi1G:i1∖ˠG). apply Bg_free_inc with (1:=Hi1); Btac_gamma.
  assert (Hi1l:i1∖(Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) el))).
   apply Bg_free_mem with (1:=Hi1); Btac_gamma.
  assert (Hi1r:i1∖(Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) er))).
   apply Bg_free_mem with (1:=Hi1); Btac_gamma.
  rewrite (Bp_forall_inv _ i1 Hi1l); rewrite (Bp_forall_inv _ i1 Hi1r).
  apply congr_for with (2:=Hi1G); fold i1.
  irewrite (sym_equal (MS_app_app (Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) el))
                                  0 ∂(i1))).
  irewrite (sym_equal (MS_app_app (Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) er))
                                  0 ∂(i1))).
  irewrite (sym_equal (Bt_mesubst_depth (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) el) m 0)).
  irewrite (sym_equal (Bt_mesubst_depth (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) er) m 0)).
  irewrite (sym_equal (MS_cmp_cmp (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) el)
                                  ⇑Μ(m) (MS_app_ 0 ∂(i1)))).
  irewrite (sym_equal (MS_cmp_cmp (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) er)
                                  ⇑Μ(m) (MS_app_ 0 ∂(i1)))).
  irewrite (sym_equal (Bt_egraft_depth (Bp_abstr_ 0 i' p) i el 0)).
  irewrite (sym_equal (Bt_egraft_depth (Bp_abstr_ 0 i' p) i er 0)).
  rewrite <- MS_iff_distr.
  assert (Hdpt:↧(Bp_abstr_ 0 i' p)<↧(∀(i'∙p))). srewrite (Bt_abstr_depth p i' 0); apply (sdepth p).
  apply (Hind _ Hdpt G el er Hequ Hgnd ↥(i) (MS_app_ 0 ∂(i1)⊙⇑Μ(m))).
  intros u n; destruct (Hm u n) as [[Hmun | Helerun] | HGun].
   left; left; unfold MS_cmp_; unfold MS_lift_; rewrite Hmun; simpl; apply refl_equal.
   left; right; apply Helerun.
   right; apply HGun.

  rewrite MS_iff_distr; simpl; apply congr_and;
  [apply (Hind _ (sdepth_l p1 p2) G el er Hequ Hgnd i m) |
   apply (Hind _ (sdepth_r p1 p2) G el er Hequ Hgnd i m)]; intros u n;
   destruct (Hm u n) as [[Hmun | He] | HG].
   left; left; apply Hmun.
   left; right; apply He.
   right; apply HG.
   left; left; apply Hmun.
   left; right; apply He.
   right; apply HG.

  rewrite MS_iff_distr; simpl; apply congr_imp;
  [apply (Hind _ (sdepth_l p1 p2) G el er Hequ Hgnd i m) |
   apply (Hind _ (sdepth_r p1 p2) G el er Hequ Hgnd i m)]; intros u n;
   destruct (Hm u n) as [[Hmun | He] | HG].
   left; left; apply Hmun.
   left; right; apply He.
   right; apply HG.
   left; left; apply Hmun.
   left; right; apply He.
   right; apply HG.

  simpl; Btac_prop.

  rewrite MS_iff_distr; simpl; apply congr_equ;
  [apply (Hind _ (sdepth_l e1 e2) G el er Hequ Hgnd i m) |
   apply (Hind _ (sdepth_r e1 e2) G el er Hequ Hgnd i m)]; intros u n;
   destruct (Hm u n) as [[Hmun | He ] | HG].
   left; left; apply Hmun.
   left; right; apply He.
   right; apply HG.
   left; left; apply Hmun.
   left; right; apply He.
   right; apply HG.

  rewrite MS_iff_distr; simpl; apply congr_ins;
  [apply (Hind _ (sdepth_l e1 e2) G el er Hequ Hgnd i m) |
   apply (Hind _ (sdepth_r e1 e2) G el er Hequ Hgnd i m)]; intros u n;
   destruct (Hm u n) as [[Hmun | He ] | HG].
   left; left; apply Hmun.
   left; right; apply He.
   right; apply HG.
   left; left; apply Hmun.
   left; right; apply He.
   right; apply HG.

  simpl; apply Bpr_refl.

  rewrite MS_equ_distr; simpl; apply congr_chc;  rewrite <- MS_equ_distr;
   apply (Hind _ (sdepth e) G el er Hequ Hgnd i m Hm).

  rewrite MS_equ_distr; simpl; apply congr_pow;  rewrite <- MS_equ_distr;
   apply (Hind _ (sdepth e) G el er Hequ Hgnd i m Hm).

  rewrite MS_equ_distr; simpl; apply congr_cpl;
  [apply (Hind _ (sdepth_l e1 e2) G el er Hequ Hgnd i m) |
   apply (Hind _ (sdepth_r e1 e2) G el er Hequ Hgnd i m)]; intros u n;
   destruct (Hm u n) as [[Hmun | He ] | HG].
   left; left; apply Hmun.
   left; right; apply He.
   right; apply HG.
   left; left; apply Hmun.
   left; right; apply He.
   right; apply HG.

  rewrite MS_equ_distr; simpl; apply congr_pro;
  [apply (Hind _ (sdepth_l e1 e2) G el er Hequ Hgnd i m) |
   apply (Hind _ (sdepth_r e1 e2) G el er Hequ Hgnd i m)]; intros u n;
   destruct (Hm u n) as [[Hmun | He ] | HG].
   left; left; apply Hmun.
   left; right; apply He.
   right; apply HG.
   left; left; apply Hmun.
   left; right; apply He.
   right; apply HG.

  simpl; apply Bpr_refl.

  unfold Be_egraft_; case_eq (DB_eq i i'); intros Hii'; DB_simpl; bool_simpl.
   apply Bpr_ground; [ | apply Hequ]; intros [[| u] n].
    right; apply (Hgnd n).
    destruct (Hm u n) as [[Hmun | He] | HG].
     left; apply Hmun.
     right; left; apply He.
     right; right; apply HG.
   simpl; apply Bpr_refl.

  rewrite MS_equ_distr; simpl.
  generalize (Bg_fshun_free (G·
                             (Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) el))·
                             (Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) er))·
                             〈〈m@Ε@〈i⇙el@Ε@e〉 〉〉≡〈〈m@Ε@〈i⇙er@Ε@e〉 〉〉)
                            _ (le_n_Sn _) 0);
   set (i1:=¡(S ⋇ˠ(G·(Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) el))·
                     (Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) er))·
                     〈〈m@Ε@〈i⇙el@Ε@e〉 〉〉≡〈〈m@Ε@〈i⇙er@Ε@e〉 〉〉),0));
   intros Hi1.
  assert (Hi1G:i1∖ˠG). apply Bg_free_inc with (1:=Hi1); Btac_gamma.
  assert (Hi1l:i1∖(Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) el))).
   apply Bg_free_mem with (1:=Hi1); Btac_gamma.
  assert (Hi1r:i1∖(Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) er))).
   apply Bg_free_mem with (1:=Hi1); Btac_gamma.
  assert (Hi1e:i1∖〈〈m@Ε@〈i⇙el@Ε@e〉 〉〉≡〈〈m@Ε@〈i⇙er@Ε@e〉 〉〉).
   apply Bg_free_mem with (1:=Hi1); Btac_gamma.
  destruct (orb_false_elim _ _ Hi1e) as [Hi1el Hi1er].
  rewrite (Be_cmpset_inv _ 〈〈m@Ε@〈i⇙el@Ε@e〉 〉〉 i1 Hi1l);
   rewrite (Be_cmpset_inv _ 〈〈m@Ε@〈i⇙er@Ε@e〉 〉〉 i1 Hi1r).
  apply congr_cmp with (3:=Hi1G).
   assert(Hdpt:↧(e)<↧({i'∊e∣p})). srewrite (Bt_abstr_depth p i' 0); apply (sdepth_l e p).
   apply (Hind _ Hdpt G el er Hequ Hgnd i m); intros u n; destruct (Hm u n) as [[Hmun | He] | HG].
    left; left; apply Hmun.
    left; right; apply He.
    right; apply HG.
   irewrite (sym_equal (MS_app_app (Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) el))
                                   0 ∂(i1))).
   irewrite (sym_equal (MS_app_app (Bp_mesubst_ 1 ⇑Μ(m) (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) er))
                                   0 ∂(i1))).
   irewrite (sym_equal (Bt_mesubst_depth (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) el) m 0)).
   irewrite (sym_equal (Bt_mesubst_depth (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) er) m 0)).
   irewrite (sym_equal (MS_cmp_cmp (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) el)
                                   ⇑Μ(m) (MS_app_ 0 ∂(i1)))).
   irewrite (sym_equal (MS_cmp_cmp (Bp_egraft_ 1 ↥(i) (Bp_abstr_ 0 i' p) er)
                                   ⇑Μ(m) (MS_app_ 0 ∂(i1)))).
   irewrite (sym_equal (Bt_egraft_depth (Bp_abstr_ 0 i' p) i el 0)).
   irewrite (sym_equal (Bt_egraft_depth (Bp_abstr_ 0 i' p) i er 0)).
   rewrite <- MS_iff_distr.
   assert (Hdpt:↧(Bp_abstr_ 0 i' p)<↧({i'∊e∣p})).
    srewrite (Bt_abstr_depth p i' 0); apply (sdepth_r e p).
   apply (Hind _ Hdpt G el er Hequ Hgnd ↥(i) (MS_app_ 0 ∂(i1)⊙⇑Μ(m))).
   intros u n; destruct (Hm u n) as [[Hmun | Helerun] | HGun].
    left; left; unfold MS_cmp_; unfold MS_lift_; rewrite Hmun; simpl; apply refl_equal.
    left; right; apply Helerun.
    right; apply HGun.
   apply Hi1el.
Qed.

(* Technical result for congruence ---------------------------------------------------------------*)

Theorem Biff_egraft:forall (G:Γ)(el er:Ε), G⊢el≡er->
                    (forall (n:nat), (¡(0,n)∖el≡er) + (¡(0,n)∖ˠG))->
                    forall (p:Π)(i:Ι), G⊢〈i⇙el@Π@p〉⇔〈i⇙er@Π@p〉.
Proof.
 intros G el er Hequ Hgnd p i.
 irewrite (sym_equal (Bt_mesubst_nil 〈i⇙el@Π@p〉)); irewrite (sym_equal (Bt_mesubst_nil 〈i⇙er@Π@p〉)).
 rewrite <- MS_iff_distr; apply (Bpr_egraft_ p _ _ _ Hequ Hgnd i ⊚).
 intros u n; left; left; unfold MS_nil; apply refl_equal.
Qed.

Theorem Bequ_egraft:forall (G:Γ)(el er:Ε), G⊢el≡er->
                    (forall (n:nat), (¡(0,n)∖el≡er) + (¡(0,n)∖ˠG))->
                    forall (e:Ε)(i:Ι), G⊢〈i⇙el@Ε@e〉≡〈i⇙er@Ε@e〉.
Proof.
 intros G el er Hequ Hgnd e i.
 irewrite (sym_equal (Bt_mesubst_nil 〈i⇙el@Ε@e〉)); irewrite (sym_equal (Bt_mesubst_nil 〈i⇙er@Ε@e〉)).
 rewrite <- MS_equ_distr; apply (Bpr_egraft_ e _ _ _ Hequ Hgnd i ⊚).
 intros u n; left; left; unfold MS_nil; apply refl_equal.
Qed.

(*================================================================================================*)